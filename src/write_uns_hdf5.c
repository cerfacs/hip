/*
  write_uns_hdf5.c:
  Write to hdf5.

  Last update:
  ------------
  11Dec19; intro h5w_sliding_plane.
  26Feb19; fix bug with init of h5w_flag_dump.
  25Feb15: intro h5w_flag_asciiBound
  26Jun14; intro h5w_flag_reset.
  24Oct13: Added xmf output
  20Jul13; fix bug in h52_per with listing bc->nr, rather than the order of writing.
  8Jul12; consistently use dereferenced pointers in sizeof calls.
  15Dec12; intro h5w_bnd_patch_conn.
  9Feb11; add specifier v2 to H5Gcreate, etc.
  3Jul10; move h5_ utilities into new file h5_util.c
  14Dec08; write_hdf_bnd: write x_axis_verts also with topo = annCasc.
  16Oct06; conceived, solution only for the time being.

  This file contains:
  -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

#include "hdf5.h"
#include "proto_hdf.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern const Grids_struct Grids ;
extern const char version[] ;
extern const int hipversion[3] ;
extern const elemType_struct elemType[] ;
extern const doFc_struct doWarn, doRemove ;

extern const double Gamma, GammaM1 ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;
extern const char varCatNames[][LEN_VAR_C] ;
extern const char h5GrpNames[][LEN_GRPNAME] ;


/* Global control variables, what is written to file. */
#define DEFAULT_h5w_flag_all        1  /* write all variables. */
#define DEFAULT_h5w_flag_noVol      0  /* do not write the volume grid.  */
#define DEFAULT_h5w_flag_bnd        1  /* write bnd connectivity with own node set. */
#define DEFAULT_h5w_flag_dump        0  /* dump/debug mode. */
#define DEFAULT_h5w_flag_prim        0  /* write primitive vars. */
#define DEFAULT_h5w_flag_prim        0  /* retain primitive vars */
#define DEFAULT_h5w_flag_sol        0  /* write only solution */
#define DEFAULT_h5w_flag_edge       0  /* write edge graph for parMetis */
#define DEFAULT_h5w_flag_face       0  /* write list of faces */
#define DEFAULT_h5w_flag_hierarchy  0  /* write adaptive grid hierarchy */
#define DEFAULT_h5w_nLevel           0  /* write this grid level. */
#define DEFAULT_h5w_zip_lvl         3  /* use this compression level. */
#define DEFAULT_h5w_flag_zip        0  /* use zip compression. */
#define DEFAULT_h5w_flag_vx2El      0  /* write vx2El connectivity. */
#define DEFAULT_h5w_flag_zones      1  /* write zones to grid. */
#define DEFAULT_h5w_flag_signature  1  /* write md5 signature in mesh file. */
#define DEFAULT_h5w_flag_asciiBound 1  /* write 7.X asciibound format */
#define DEFAULT_h5w_flag_vrtxVol    1  /* write vertex volumes. */
static int      h5w_flag_all        = DEFAULT_h5w_flag_all       ;
static int      h5w_flag_noVol      = DEFAULT_h5w_flag_noVol     ;
static int      h5w_flag_bnd        = DEFAULT_h5w_flag_bnd       ;
static int      h5w_flag_dump       = DEFAULT_h5w_flag_dump       ;
static int      h5w_flag_prim        = DEFAULT_h5w_flag_prim       ;
static int      h5w_flag_sol        = DEFAULT_h5w_flag_sol       ;
static int      h5w_flag_edge       = DEFAULT_h5w_flag_edge      ;
static int      h5w_flag_face       = DEFAULT_h5w_flag_face      ;
static int      h5w_flag_hierarchy  = DEFAULT_h5w_flag_hierarchy      ;
static int      h5w_nLevel           = DEFAULT_h5w_nLevel          ;
static int      h5w_flag_zip        = DEFAULT_h5w_flag_zip       ;
static int      h5w_zip_lvl         = DEFAULT_h5w_zip_lvl        ;
static int      h5w_flag_vx2El      = DEFAULT_h5w_flag_vx2El  ;
static int      h5w_flag_zones      = DEFAULT_h5w_flag_zones  ;
static int      h5w_flag_signature  = DEFAULT_h5w_flag_signature ;
static int      h5w_flag_asciiBound = DEFAULT_h5w_flag_asciiBound ;
static int      h5w_flag_vrtxVol    = DEFAULT_h5w_flag_vrtxVol ;

void h5w_flag_reset () {
  h5w_flag_all       = DEFAULT_h5w_flag_all       ;
  h5w_flag_noVol     = DEFAULT_h5w_flag_noVol     ;
  h5w_flag_bnd       = DEFAULT_h5w_flag_bnd       ;
  h5w_flag_prim       = DEFAULT_h5w_flag_prim       ;
  h5w_flag_sol       = DEFAULT_h5w_flag_sol       ;
  h5w_flag_edge      = DEFAULT_h5w_flag_edge      ;
  h5w_flag_face      = DEFAULT_h5w_flag_face      ;
  h5w_flag_hierarchy = DEFAULT_h5w_flag_hierarchy      ;
  h5w_nLevel          = DEFAULT_h5w_nLevel          ;
  h5w_flag_zip       = DEFAULT_h5w_flag_zip       ;
  h5w_zip_lvl        = DEFAULT_h5w_zip_lvl        ;
  h5w_flag_vx2El     = DEFAULT_h5w_flag_vx2El     ;
  h5w_flag_zones     = DEFAULT_h5w_flag_zones     ;
  h5w_flag_signature = DEFAULT_h5w_flag_signature ;
  h5w_flag_asciiBound = DEFAULT_h5w_flag_asciiBound ;
  return ;
}



/******************************************************************************
  h5w_args:   */

/*! pull of arguments from the line with arguments.
 */

/*

  Last update:
  ------------
  6Sep18; add dump option for debugging.
  12Jan15; wrong  set and attribute name for xmf file.
  29Jun14; move zip option to -c, introduce -z0 to skip writing zones.
  introduc -l levels.
  28Jun14; introduce -a0, -b0 arguments to suppress writing all, bnd.
  14Dec13; turn flag_all on by default.
  9Apr13; turn flag_bnd on by default.
  JDM 14Dec13: But it isnt?
  31Mar12: conceived.

  Input:
  ------
  argLine: list of Unix-style argument flags.

  Changes to:
  -----------
  h5w-* (global variables).

  Output:
  -------
  rootFile: root part of file[s] to write.

*/

void h5w_args  ( char argLine[], char rootFile[] ) {

  /* Reset. */
  h5w_flag_reset () ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  /* Backward compatibility, parse augmented hdf identifier. */
  if ( !strncmp( ppArgs[0], "hdfa", 4 ) )
    h5w_flag_all = 1 ;
  else if  ( !strncmp( ppArgs[0], "hdfb", 4 ) )
    h5w_flag_bnd = 1 ;
  else if  ( !strncmp( ppArgs[0], "hdfd", 4 ) )
    h5w_flag_dump = 1 ;
  else if  ( !strncmp( ppArgs[0], "hdfe", 4 ) )
    h5w_flag_edge = 1 ;
  else if  ( !strncmp( ppArgs[0], "hdfs", 4 ) )
    h5w_flag_sol = 1 ;


  /* Parse line of unix-style optional args. */
  char c ;
  /* colon behind arg means argument to be read,
     double colon means optional argument, but needs to be joined: -a0, not -a 0. */
  while ((c = getopt_long ( mArgs, ppArgs, "a::b::c:defhl:npsvz::67",NULL,NULL)) != -1) {
    switch (c)  {
    case 'a':
      if ( optarg && atoi( optarg ) == 0 )
        /* 0 arg given. use -a0 */
        h5w_flag_all = 0;
      else
        /* No arg given, or arg not 0, use -a1 */
        h5w_flag_all = 1;
      break;
    case 'b':
      if ( optarg && atoi( optarg ) == 0 )
        h5w_flag_bnd = 0;
      else
        /* No arg or nonzero arg given. use -b1 */
        h5w_flag_bnd = 1;
      break;
    case 'c':
      h5w_zip_lvl = atoi( optarg ) ;
      if ( h5w_zip_lvl > 0 )
        h5w_flag_zip = 1;
      break;
    case 'd':
      h5w_flag_dump = 1 ;
      break;
    case 'e':
      h5w_flag_edge = 1;
      break;
    case 'f':
      h5w_flag_face = 1;
      break;
    case 'h':
      h5w_flag_hierarchy = 1;
      h5w_flag_bnd = 0; // bnd grid renumbers, 29.09 breaks prt->child numbering.
      break;
    case 'l':
      h5w_nLevel = atoi( optarg ) ;
      break;
    case 'n':
      h5w_flag_noVol = 1;
      h5w_flag_bnd = 1;
      break;
    case 'p':
      h5w_flag_prim = 1;
      break;
    case 's':
      h5w_flag_sol = 1;
      break;
    case 'v':
      h5w_flag_vx2El = 1;
      break;
    case 'z':
      if ( optarg && atoi( optarg ) == 0 )
        h5w_flag_zones = 0;
      else
        /* No arg or nonzero arg given. use -b1 */
        h5w_flag_zones = 1;
      break;
    case '6':
      h5w_flag_asciiBound = 0;
      break;
    case '7':
      h5w_flag_asciiBound = 1;
      break;

    case '?':
      if ( optopt == 'l' )
        fprintf (stderr, "Option -%c requires an argument.\n", optopt );
      else if (isprint (optopt)) {
        sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
      else {
        sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }

  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( rootFile, ppArgs[optind] ) ;
  else
    /* Standard filename. */
    strcpy ( rootFile, "grid\0" ) ;

  return ;
}



void test_write ( hid_t group_id ) {

  hid_t       dataset_id, dataspace_id;  /* identifiers */
  hsize_t     dims[1];
  herr_t      status;
  int         i, j, iBuf[3][3] ;
  double      dBuf[2][10];

  /* Initialize the first dataset. */
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      iBuf[i][j] = i + j + 1;

  /* Initialize the second dataset. */
  for (i = 0; i < 2; i++)
    for (j = 0; j < 10; j++)
      dBuf[i][j] = i + j + 1.;


  /* Create the data space for the second dataset. */
  dims[0] = 2;
  dataspace_id = H5Screate_simple(1, dims, NULL);

  /* Create the second dataset in group "Group_A". */
  dataset_id = H5Dcreate(group_id, "dset2", HH_DBL, dataspace_id,
                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

  /* Write the second dataset.
     status = H5Dwrite1(dataset_id, HH_DBL, H5S_ALL, H5S_ALL, H5P_DEFAULT,
     dBuf); */
  status = H5Dwrite(dataset_id, HH_DBL, H5S_ALL, H5S_ALL, H5P_DEFAULT,dBuf);

  /* Close the data space for the second dataset. */
  status = H5Sclose(dataspace_id);

  /* Close the second dataset */
  status = H5Dclose(dataset_id);






  /* Create the data space for the first dataset. */
  dims[0] = 3;
  dataspace_id = H5Screate_simple(1, dims, NULL);

  /* Create a dataset in group "MyGroup". */
  dataset_id = H5Dcreate(group_id, "dset1", HH_INT, dataspace_id,
                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

  /* Write the first dataset. */
  status = H5Dwrite(dataset_id, HH_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                    iBuf);

  /* Close the data space for the first dataset. */
  status = H5Sclose(dataspace_id);

  /* Close the first dataset. */
  status = H5Dclose(dataset_id);
}


/******************************************************************************

  h5_write_solfield:
  Write the fiels of a given type to hdf5 file.

  Last update:
  ------------
  25May09; check for proper size using ptrdiff_t,
           check for non-duplicity of name.
  23May09; pass varCat_e in the interface, check either grp or cat for a match.
  4Apr09; use grp out of varList to group the variables.
          replace varTypeS with varList.
  10Oct07: conceived.

  Input:
  ------
  grp_id: opened hdf5 group
  grp: the group label to write
  cat: the variable category to write
  pUns: grid
  mVx: number of vertices
  dBuf: work array of at least mVx size

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5_write_solfield ( const hid_t grp_id, const char *grp,
                        const varCat_e cat, const uns_s *pUns,
                        const int mBeg, const int mEnd, const int mVx, double *dBuf ){

  int nEqu, nBeg, nEnd ;
  double *pdBuf ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  const varList_s *pVL = &(pUns->varList) ;
  const var_s *pVar ;

  for ( nEqu = mBeg ; nEqu < mEnd ; nEqu++ ) {
    pVar = pVL->var+nEqu ;

    if ( !strcmp( pVL->var[nEqu].grp, grp ) ||
         pVar->cat == cat ) {
      /* This one is in the right group, dump it. */
      pChunk = NULL ;
      pdBuf = dBuf ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
        for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
          if ( pVrtx->number )
            *pdBuf++ = pVrtx->Punknown[nEqu] ;


      /* Validate. */
      if ( ( ptrdiff_t ) (pdBuf-dBuf) != mVx ) {
        sprintf ( hip_msg, "in hdf5_write_solfield:\n"
                  "         when writing variable: %s.\n"
                  "         expected %d, found %d variables.\n",
                  pVar->name, mVx, ( int ) (pdBuf-dBuf) ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      if ( h5_obj_exists ( grp_id, pVar->name  ) == H5I_DATASET ) {
        sprintf ( hip_msg, "in hdf5_write_solfield:\n"
                  "         dupliation of variable named: %s.\n",
                  pVar->name ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }


      /* Write. */
      h5_write_dbl ( grp_id, 0, pVar->name, mVx, dBuf ) ;
    }
  }

  return ( 1 ) ;
}


/******************************************************************************

  h5_check_grp:
  Check whether all variables have a group. If not, issue default groupnames.

  Last update:
  ------------
  4Apr09; cut out of check_var_name.

  Input:
  ------
  pVarList: the list of variables.

  Changes To:
  -----------
  pVarList.var[].grp

  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5_check_grp ( varList_s *pVarList ) {

  const int mEqu = pVarList->mUnknowns ;

  int k ;
  var_s *pVar ;
  /* Loop over all equ, count types and assume they are in order. */
  for ( k = 0 ; k < mEqu ; k++ ) {
    pVar = pVarList->var + k ;

    if ( !pVar->grp[0] ) {

      if ( !pVar->cat ) {
        sprintf ( hip_msg, " variable %d, named: %s, has neither category nor group.\n",
                  k+1, pVar->name ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }

      /* Build a group name out of  the category and number. */
      snprintf ( pVar->grp, LEN_VARNAME, "%s", h5GrpNames[ (int) pVar->cat ] ) ;

    }
  }

  return ( 1 ) ;
}
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) :

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>

  h5w_var_xmf:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

/* Write a formula attribute divided by /GaseousPhase/rho */
void h5w_var_by_rho ( FILE *Fxmf,
                      const ulong_t mNodes,
                      const char *solFileNmH5,
                      const char *attributename,
                      const char *groupname,
                      const char *setname,
                      const char *rhoname) {
  
  fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",attributename);
  fprintf(Fxmf,"       <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 / $1\">\n",mNodes);
  fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",
          mNodes,solFileNmH5,groupname,setname);
  fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:%s </DataItem>\n",
          mNodes,solFileNmH5,rhoname);
  fprintf(Fxmf,"       </DataItem>\n");
  fprintf(Fxmf,"    </Attribute>\n");
  return ;
}

/* Write a simple attribute prim var */
void h5w_var_prim ( FILE *Fxmf,
                    const ulong_t mNodes,
                    const char solFileNmH5[TEXT_LEN],
                    const char *groupname,
                    const char *setname ) {
  
  fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname);
  fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",
          mNodes, solFileNmH5, groupname, setname);
  fprintf(Fxmf,"    </Attribute>\n");
  
  return ;
}

ret_s h5w_var_xmf ( uns_s *pUns, FILE *Fxmf, const char solFileNmH5[TEXT_LEN] ) {
#undef FUNLOC
#define FUNLOC "in h5w_var_xmf"

  int mEqu = pUns->varList.mUnknowns ;
  int nEqu = 0;
  varList_s *pVL = &pUns->varList ;
  var_s *pVar = pVL->var ;
  char groupname[LEN_GRPNAME];
  char setname[LEN_GRPNAME];
  char attributename[LEN_GRPNAME];
 

  if ( h5w_flag_all ) {
    /* Dump all groups to file. */
    for ( nEqu = 0 ; nEqu < mEqu ; nEqu++ ) {
      if ( pVar[nEqu].flag ) {
        strcpy ( groupname, pVar[nEqu].grp ) ;
        
        if ( ( !strcmp ( pVar[nEqu].name ,"nl")) ||
             ( !strcmp(pVar[nEqu].name ,"alphalrhol")) ||
             ( !strcmp(pVar[nEqu].name,"d00sigmal")) ||
             ( !strcmp ( pVar[nEqu].name ,"rho"))) {
          strcpy ( setname, pVar[nEqu].name ) ;
          
          h5w_var_prim ( Fxmf, pUns->mVertsNumbered, solFileNmH5, groupname, setname ) ;

          /*
            fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname);
            fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",
            pUns->mVertsNumbered,solFileNmH5,groupname,setname);
            fprintf(Fxmf,"    </Attribute>\n");
          */
        }

        else if ( pVar[nEqu].cat == species ||
                  pVar[nEqu].cat == ns  ) {
          strcpy ( setname, pVar[nEqu].name ) ;
          
          if ( pVar[nEqu].cat == ns &&
               pUns->varList.varType == prim ) {
            // primitive variables, no div by rho
            strcpy ( attributename, pVar[nEqu].name ) ;
            h5w_var_prim ( Fxmf, pUns->mVertsNumbered, solFileNmH5, groupname, setname ) ;
          }
          else {
            // species and cons variables, div by rho
            if ( pVar[nEqu].cat == ns ) {
              strcpy ( attributename, pVar[nEqu].name+3 ) ; }
            else {
              // cons NS var have names starting with rho, strip the first 3. 
              strcpy ( attributename, pVar[nEqu].name+3 ) ; }
            h5w_var_by_rho ( Fxmf, pUns->mVertsNumbered, solFileNmH5, attributename, groupname,setname,
                             "/GaseousPhase/rho") ;
          }
       }

        else if (  pVar[nEqu].cat == tpf  )  {
          strcpy ( attributename, pVar[nEqu].name+10 ) ;
          strcpy ( setname, pVar[nEqu].name ) ;
          
          h5w_var_by_rho ( Fxmf, pUns->mVertsNumbered, solFileNmH5, attributename, groupname, setname,
                           "/LiquidPhase/alphalrhol" ) ;

          
          /* fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",attributename); */
          /* fprintf(Fxmf,"       <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 / $1\">\n",pUns->mVertsNumbered); */
          /* fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
          /* fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/LiquidPhase/alphalrhol </DataItem>\n",pUns->mVertsNumbered,solFileNmH5); */
          /* fprintf(Fxmf,"       </DataItem>\n"); */
          /* fprintf(Fxmf,"    </Attribute>\n"); */
        }

        else {
          // Other.
          strcpy ( setname, pVar[nEqu].name ) ;

          h5w_var_prim ( Fxmf, pUns->mVertsNumbered, solFileNmH5, groupname, setname ) ;
          
          /* fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname); */
          /* fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
          /* fprintf(Fxmf,"    </Attribute>\n"); */
        }
      }
    }
  }

  /* Describe the velocity vector. */
  /*
  fprintf(Fxmf, "     <Attribute Name=\"velocity\" AttributeType=\"Vector\" Dimensions=\"%"FMT_ULG" %d\">\n",
          pUns->mVertsNumbered, pUns->mDim ) ;
  fprintf(Fxmf, "       <DataItem ItemType=\"Function\" Dimension=\"%"FMT_ULG"\" Reference=\"XML\">\n",
          pUns->mVertsNumbered ) ;
  fprintf(Fxmf, "         /Xdmf/Domain/DataItem[@Name=\"/GaseousPhase/u\"]\n") ;
  fprintf(Fxmf, "       </DataItem>") ;
  fprintf(Fxmf, "       <DataItem ItemType=\"Function\" Dimension=\"%"FMT_ULG"\" Reference=\"XML\">\n",
          pUns->mVertsNumbered ) ;
  fprintf(Fxmf, "         /Xdmf/Domain/DataItem[@Name=\"/GaseousPhase/v\"]\n") ;
  fprintf(Fxmf, "       </DataItem>") ;
  fprintf(Fxmf, "       <DataItem ItemType=\"Function\" Dimension=\"%"FMT_ULG"\" Reference=\"XML\">\n",
          pUns->mVertsNumbered ) ;
  fprintf(Fxmf, "         /Xdmf/Domain/DataItem[@Name=\"/GaseousPhase/w\"]\n") ;
  fprintf(Fxmf, "       </DataItem>\n") ;
  fprintf(Fxmf, "     </Attribute>\n") ;
*/

  /*
    from
    https://github.com/mvanmoer/HDF5_examples/blob/master/orthonormalgrids/vector_comps/vector_comps.xdmf

     <Attribute Name="vectors" AttributeType="Vector" Center="Node">
        <DataItem ItemType="Function" Function="join($0, $1, $2)" Dimensions="128 192 256 3">
          <DataItem Dimensions="6291456" NumberType="Float" Precision="4" Format="HDF">
            vector_comps.h5:/xcomps
          </DataItem>
          <DataItem Dimensions="6291456" NumberType="Float" Precision="4" Format="HDF">
            vector_comps.h5:/ycomps
          </DataItem>
          <DataItem Dimensions="6291456" NumberType="Float" Precision="4" Format="HDF">
            vector_comps.h5:/zcomps
          </DataItem>
        </DataItem>
      </Attribute>

  */

  
  ret_s ret = ret_success () ;

  return ( ret ) ;
}


/******************************************************************************
  h5w_gridxmf:
  Write hdf5 mapping for volume grid to xmf ascii file.

  Intput:
  ------------
  pUns =  mesh
  Fxmf = opened xmf file
  meshFileNmH5 = name of the hdf5 mesh file
  solFileNmH5 = name of the hdf5 solution file

  Last update
  ------------
  18dec24; remove name+3 from ns variable names. Why?
           refactor into h5w_var_xmf.
  29jul20; use prism node odering with correct handedness
  25jul20; hybrid grid: write two bnd patch entries for tri and qua
  14jul17; evaluate Fxmf and skip writing if null.
  22Oct16: fix ungarded if block around if ( pVar[nEqu].cat == ns ) {
           raised by gcc compiler

  Returns:
  --------
  0 on failure, 1 on success.
*/

int  h5w_volGridXmf( uns_s *pUns, FILE *Fxmf,
                     const char meshFileNmH5[TEXT_LEN],
                     const char solFileNmH5[TEXT_LEN] ) {

  if ( !Fxmf )
    /* Skip writing to xmf, e.g. coarser levels. */
    return ( 0 ) ;

  const int mDim = pUns->mDim ;
  elType_e elType ;
  char xmf_element_name[TEXT_LEN] ;
  char datasetname[TEXT_LEN] ;
  char string[TEXT_LEN] ;
  int nEqu = 0;
  char groupname[LEN_GRPNAME];
  char setname[LEN_GRPNAME];
  char attributename[LEN_GRPNAME];
  varList_s *pVL = &pUns->varList ;
  var_s *pVar = pVL->var ;
  int mEqu = pUns->varList.mUnknowns ;

  ulong_t mElems ;
  ulong_t  mVerts;
  const char charDim[][2] = {"x", "y", "z" } ;
  int nDim;


  fprintf ( Fxmf, "<?xml version=\"1.0\" ?>\n" ) ;
  fprintf ( Fxmf, "<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.0\">\n");
  fprintf ( Fxmf, "  <Domain>\n");

  // Loop over element types ..
  for ( elType = tri ; elType <= hex ; elType++ ) {
    mElems = pUns->mElems_w_mVerts[elType] ;
    mVerts = elemType[elType].mVerts;
    if ( mElems ){
      // Get name of element to find XDMF equivalent
      if ( elType == tri )   strcpy(xmf_element_name,"Triangle");
      if ( elType == qua )   strcpy(xmf_element_name,"Quadrilateral");
      if ( elType == hex )   strcpy(xmf_element_name,"Hexahedron");
      if ( elType == pyr )   strcpy(xmf_element_name,"Pyramid");
      if ( elType == pri )   strcpy(xmf_element_name,"Wedge");
      if ( elType == tet )   strcpy(xmf_element_name,"Tetrahedron");
      // Generate string for the dataset
      sprintf ( datasetname, "%s->node", elemType[elType].name ) ;
      fprintf ( Fxmf,"    <Grid Collection=\"%s_Mesh\" Name=\"%s\">\n",xmf_element_name,xmf_element_name);
      //  For prisms Ordering is slightly different in XDMF
      if ( elType == pri ) {
        fprintf ( Fxmf,"    <Topology Type=\"%s\" "
                  "NumberOfElements=\"%"FMT_ULG"\" Order=\""
                  //"0 5 3 1 4 2"
                  "3 5 0 2 4 1"
                  "\">\n",

                  xmf_element_name,mElems);
      }
      else {
        fprintf ( Fxmf,"    <Topology Type=\"%s\" "
                  "NumberOfElements=\"%"FMT_ULG"\">\n",xmf_element_name,mElems);
      }
      fprintf ( Fxmf,"      <DataItem ItemType=\"Function\" "
                "Dimensions=\"%"FMT_ULG"\" Function=\"$0 - 1\">\n",mVerts*mElems);
      fprintf ( Fxmf,"        <DataItem Format=\"HDF\" "
                "DataType=\"Int\" Dimensions=\"%"FMT_ULG"\">\n",mVerts*mElems) ;
      fprintf ( Fxmf,"        %s:/Connectivity/%s\n",meshFileNmH5,datasetname);
      fprintf ( Fxmf,"        </DataItem>\n");
      fprintf ( Fxmf,"      </DataItem>\n");
      fprintf ( Fxmf,"    </Topology>\n");

      
      // Add geometry info
      if ( mDim == 2 ) {
        strcpy(string,"X_Y");
      }
      else {
        strcpy(string,"X_Y_Z");
      }
      fprintf ( Fxmf, "    <Geometry Type=\"%s\">\n",string);
      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
        fprintf ( Fxmf,"        <DataItem Format=\"HDF\" DataType=\"Float\" Dimensions=\"%"FMT_ULG"\">\n",pUns->mVertsNumbered) ;
        fprintf ( Fxmf,"        %s:/Coordinates/%s\n",meshFileNmH5,charDim[nDim]);
        fprintf ( Fxmf,"        </DataItem>\n");
      }
      fprintf ( Fxmf, "    </Geometry>\n");

      h5w_var_xmf ( pUns, Fxmf, solFileNmH5 ) ;
      /* if ( h5w_flag_all ) { */
      /*   /\* Dump all groups to file. *\/ */
      /*   for ( nEqu = 0 ; nEqu < mEqu ; nEqu++ ) { */
      /*     if ( pVar[nEqu].flag ) { */
      /*       strcpy ( groupname, pVar[nEqu].grp ) ; */
      /*       if ( ( !strcmp ( pVar[nEqu].name ,"nl")) || */
      /*            ( !strcmp(pVar[nEqu].name ,"alphalrhol")) || */
      /*            ( !strcmp(pVar[nEqu].name,"d00sigmal")) || */
      /*            ( !strcmp ( pVar[nEqu].name ,"rho"))) { */
      /*         strcpy ( setname, pVar[nEqu].name ) ; */
      /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname); */
      /*         fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
      /*       } */
      /*       else if ( pVar[nEqu].cat == species || */
      /*                 (pVar[nEqu].cat == ns ) ) { */
      /*         strcpy ( setname, pVar[nEqu].name ) ; */
      /*         if ( pVar[nEqu].cat == species ) { */
      /*           strcpy ( attributename, pVar[nEqu].name ) ; } */
      /*         if ( pVar[nEqu].cat == ns ) { */
      /*           // Why name + 3? */
      /*           // avbp no longer use cats, so can change here */
      /*           //strcpy ( attributename, pVar[nEqu].name+3 ) ; } */
      /*           strcpy ( attributename, pVar[nEqu].name ) ; } */
      /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",attributename); */
      /*         fprintf(Fxmf,"       <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 / $1\">\n",pUns->mVertsNumbered); */
      /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
      /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/GaseousPhase/rho </DataItem>\n",pUns->mVertsNumbered,solFileNmH5); */
      /*         fprintf(Fxmf,"       </DataItem>\n"); */
      /*       } */
      /*       else if (  pVar[nEqu].cat == tpf  )  { */
      /*         strcpy ( attributename, pVar[nEqu].name+10 ) ; */
      /*         strcpy ( setname, pVar[nEqu].name ) ; */
      /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",attributename); */
      /*         fprintf(Fxmf,"       <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 / $1\">\n",pUns->mVertsNumbered); */
      /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
      /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/LiquidPhase/alphalrhol </DataItem>\n",pUns->mVertsNumbered,solFileNmH5); */
      /*         fprintf(Fxmf,"       </DataItem>\n"); */
      /*       } */
      /*       else { */
      /*         strcpy ( setname, pVar[nEqu].name ) ; */
      /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname); */
      /*         fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
      /*       } */
      /*       fprintf(Fxmf,"    </Attribute>\n"); */
      /*     } */
      /*   } */
      fprintf ( Fxmf, "  </Grid>\n");
    }
  } // for  ( elType = tri ; elType <= hex ; elType++ ) {

  return(0);
}

/******************************************************************************
  h5w_bndxmf:   */

/*! Function to output the per patch xmf tags
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  18dec24; remove name+3 for NS vars.
  4Jyl17; refactor, pass opened xmf file pointer, pass h5 filenames.
  2014 Jul 01 : conceived.


  Input:
  ------
  Fxmf : xmf file id
  pUns : unstructured mesh
  meshFileNmH5 : meshFileNmH5 character string
  solFileNmH5 : solFileNmH5 character string
  PatchName: asciiBound patch label
  Type : Element type ( Triangle or Quadrilateral )
  set_nm : connectivity set name in hdf file
  typeVertex :  3 or 4 vertex per element
  patch_begin :  index of patch begin in connectivity set
  patch_lidx: last index of patch
  patch_Vx:  Nbr of Vertices in patch

  Changes To:
  -----------

  Output:
  ------

  Returns:
  --------
  1 on failure, 0 on success

*/

void h5w_bndXmf ( FILE *Fxmf, uns_s *pUns ,
                  const char *meshFileNmH5, const char *solFileNmH5,
                  char *PatchName, char *Type, char *set_nm,
                  ulong_t typeVertex, ulong_t patch_begin, ulong_t patch_lidx,
                  ulong_t patch_Vx ) {
  int nEqu = 0;
  char groupname[LEN_GRPNAME];
  char setname[LEN_GRPNAME];
  char attributename[LEN_GRPNAME];
  char string[TEXT_LEN] ;
  varList_s *pVL = &pUns->varList ;
  var_s *pVar = pVL->var ;
  int mEqu = pUns->varList.mUnknowns ;
  const char charDim[][2] = {"x", "y", "z" } ;
  int nDim;


  fprintf ( Fxmf,"  <Grid Collection=\"%s\" Name=\"%s\">\n",meshFileNmH5,PatchName);
  fprintf ( Fxmf,"    <Topology Type=\"%s\" NumberOfElements=\"%"FMT_ULG"\">\n",Type,patch_lidx-patch_begin);
  fprintf ( Fxmf,"      <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 - 1\">\n",typeVertex*(patch_lidx-patch_begin));
  fprintf ( Fxmf,"        <DataItem ItemType=\"Hyperslab\" Dimensions=\"%"FMT_ULG"\" Type=\"HyperSlab\" >\n", typeVertex*(patch_lidx-patch_begin));
  fprintf ( Fxmf,"          <DataItem Dimensions=\"3\" Format=\"XML\">\n");
  fprintf ( Fxmf,"          %"FMT_ULG" 1 %"FMT_ULG"\n",typeVertex*patch_begin,typeVertex*(patch_lidx-patch_begin));
  fprintf ( Fxmf,"          </DataItem>\n");
  fprintf ( Fxmf,"          <DataItem Format=\"HDF\" DataType=\"Int\" Dimensions=\"%"FMT_ULG"\">\n",patch_Vx );
  fprintf ( Fxmf,"          %s:/Boundary/%s\n",meshFileNmH5,set_nm);
  fprintf ( Fxmf,"          </DataItem>\n");
  fprintf ( Fxmf,"        </DataItem>\n");
  fprintf ( Fxmf,"      </DataItem>\n");
  fprintf ( Fxmf,"    </Topology>\n");
  // Add geometry info
  if ( pUns->mDim  == 2 ) {
    strcpy(string,"X_Y");
  }
  else {
    strcpy(string,"X_Y_Z");
  }
  fprintf ( Fxmf, "    <Geometry Type=\"%s\">\n",string);
  for ( nDim = 0 ; nDim < pUns->mDim ; nDim++ ) {
    fprintf ( Fxmf,"        <DataItem Format=\"HDF\" DataType=\"Float\" Dimensions=\"%"FMT_ULG"\">\n",pUns->mVertsNumbered) ;
    fprintf ( Fxmf,"        %s:/Coordinates/%s\n",meshFileNmH5,charDim[nDim]);
    fprintf ( Fxmf,"        </DataItem>\n");
  }
  fprintf ( Fxmf, "    </Geometry>\n");
  /*  fprintf ( Fxmf,"    <Geometry Reference=\"/Xdmf/Domain/Grid/Geometry\"/>\n");*/

  h5w_var_xmf ( pUns, Fxmf, solFileNmH5 ) ;
  /* if ( h5w_flag_all ) { */
  /*   /\* Dump all groups to file. *\/ */
  /*   for ( nEqu = 0 ; nEqu < mEqu ; nEqu++ ) { */
  /*     if ( pVar[nEqu].flag ) { */
  /*       strcpy ( groupname, pVar[nEqu].grp ) ; */
  /*       if ( ( !strcmp ( pVar[nEqu].name ,"nl")) || */
  /*            ( !strcmp(pVar[nEqu].name ,"alphalrhol")) || */
  /*            ( !strcmp(pVar[nEqu].name,"d00sigmal")) || */
  /*            ( !strcmp ( pVar[nEqu].name ,"rho"))) { */
  /*         strcpy ( setname, pVar[nEqu].name ) ; */
  /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname); */
  /*         fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
  /*       } */
  /*       else if ( pVar[nEqu].cat == species || */
  /*                 (pVar[nEqu].cat == ns ) ) { */
  /*         strcpy ( setname, pVar[nEqu].name ) ; */
  /*         if ( pVar[nEqu].cat == species ) */
  /*           strcpy ( attributename, pVar[nEqu].name ) ; */
  /*         if ( pVar[nEqu].cat == ns ) */
  /*           // strcpy ( attributename, pVar[nEqu].name + 3 ) ; */
  /*           strcpy ( attributename, pVar[nEqu].name ) ; */
  /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",attributename); */
  /*         fprintf(Fxmf,"       <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 / $1\">\n",pUns->mVertsNumbered); */
  /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
  /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/GaseousPhase/rho </DataItem>\n",pUns->mVertsNumbered,solFileNmH5); */
  /*         fprintf(Fxmf,"       </DataItem>\n"); */
  /*       } */
  /*       else if (  pVar[nEqu].cat == tpf  )  { */
  /*         strcpy ( setname, pVar[nEqu].name ) ; */
  /*         strcpy ( attributename, pVar[nEqu].name+10 ) ; */
  /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",attributename); */
  /*         fprintf(Fxmf,"       <DataItem ItemType=\"Function\" Dimensions=\"%"FMT_ULG"\" Function=\"$0 / $1\">\n",pUns->mVertsNumbered); */
  /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
  /*         fprintf(Fxmf,"         <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/LiquidPhase/alphalrhol </DataItem>\n",pUns->mVertsNumbered,solFileNmH5); */
  /*         fprintf(Fxmf,"       </DataItem>\n"); */
  /*       } */
  /*       else { */
  /*         strcpy ( setname, pVar[nEqu].name ) ; */
  /*         fprintf(Fxmf,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",setname); */
  /*         fprintf(Fxmf,"       <DataItem Format=\"HDF\" Dimensions=\"%"FMT_ULG"\" NumberType=\"Float\" Precision=\"8\">%s:/%s/%s</DataItem>\n",pUns->mVertsNumbered,solFileNmH5,groupname,setname); */
  /*       } */
  /*       fprintf(Fxmf,"    </Attribute>\n"); */
  /*     } */
  /*   } */
  /* } */
  fprintf ( Fxmf," </Grid>\n") ;
  return ;
}


/******************************************************************************

  h5w_hdr:
  Header, parameters.

  Last update:
  ------------
  11jul20; find name for coarsened grids, fallback for no name.
  7Apr20; write out a grid name.
  15Dec13; instead, use h5_write_one_fx_str.
  14Dec13; use renamed h5_write_one_str80
  9Jul13; add ll/urBox.
  3Jul10; dump hMin, hMax, volElemMax.
  19Sep09; write hMin, volElemMin to hdf.
  18Sep09; use write_str80.
  17Dec06: derived from avbp_hdf5_sol.

  Input:
  ------


  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_hdr ( uns_s *pUns, hid_t file_id ) {

  hid_t       grp_id;   /* identifier */
  herr_t      status;

  /* Parameters. */
  grp_id = H5Gcreate(file_id, "Parameters", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

  /* Identification string. */
  // JDM, Apr 2020: writes ok, looks fine in hdfview, but doesn't read back in.
  //h5_write_one_fxStr ( grp_id, "hipversion", fxStr240, (char *)version ) ;
  h5_write_fxStr ( grp_id, "hipversion", 1, fxStr240, (char *)version ) ;
  /* Identification integers */
  h5_write_int ( grp_id, 0, "version", 3, hipversion ) ;

  /* Identification string. */
  uns_s *pUnsFinest = pUns ;
  while ( pUnsFinest->pUnsFine )
    pUnsFinest = pUnsFinest->pUnsFine ;
  if ( pUns->pGrid && pUns->pGrid->uns.name[0] != '\0' )
    h5_write_fxStr ( grp_id, "gridName", 1, fxStr240, pUns->pGrid->uns.name ) ;
  else {
    h5_write_fxStr ( grp_id, "gridName", 1, fxStr240, "unnamed" ) ;
  }

  /* hMin, volMin. */
  h5_write_dbl ( grp_id, 0, "vol_elem_min",   1, &(pUns->volElemMin) ) ;
  h5_write_dbl ( grp_id, 0, "vol_elem_max",   1, &(pUns->volElemMax) ) ;
  h5_write_dbl ( grp_id, 0, "vol_domain",     1, &(pUns->volDomain) ) ;

  h5_write_dbl ( grp_id, 0, "h_min",          1, &(pUns->hMin) ) ;
  h5_write_dbl ( grp_id, 0, "h_max",          1, &(pUns->hMax) ) ;

  h5_write_dbl ( grp_id, 0, "x_min", pUns->mDim, pUns->llBox ) ;
  h5_write_dbl ( grp_id, 0, "x_max", pUns->mDim, pUns->urBox ) ;
  h5_write_dbl ( grp_id, 0, "r_min", 2, pUns->llBoxCyl ) ;
  h5_write_dbl ( grp_id, 0, "r_max", 2, pUns->urBoxCyl ) ;


  /* Close parameters. */
  status = H5Gclose(grp_id);


  return ( 1 ) ;
}

/******************************************************************************

  h5w_coor:
  Write coordinates to hdf5 mesh file.

  Last update:
  ------------
  17jul20; intro mVxWritten
  6Apr13: promote possibly large unsigned ints to ulong_t
  17Dec06: derived from avbp_hdf5_sol.

  Input:
  ------
  pUns = grid
  compress = if non-zero, write with compression
  patch_id = hdf5 id of bc to write.
  mVxWritten: vertices already written in a previous section, write from here.

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_coor ( uns_s *pUns, const int compress, hid_t file_id,
               const ulong_t mVxWritten ) {
#undef FUNLOC
#define FUNLOC "in h5w_coor"

  const int mDim = pUns->mDim ;
  const ulong_t mVx = pUns->mVertsNumbered - mVxWritten ;

  if ( !mVx )
    /* No additional vertices to write. */
    return ( 1 ) ;


  const char charDim[][2] = {"x", "y", "z" } ;

  hid_t       grp_id;   /* identifier */
  herr_t      status;

  int nDim, nBeg, nEnd ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  double *dBuf, *pdBuf ;




  /* All vars are written as vectors, so allocate one. */
  dBuf = arr_malloc ( "dBuf in h5w_coor", pUns->pFam, mVx,
                      sizeof( *dBuf ) ) ;

  grp_id = H5Gcreate(file_id, "Coordinates", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    pChunk = NULL ;
    pdBuf = dBuf ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( pVrtx->number && pVrtx->number >= mVxWritten )
          *pdBuf++ = pVrtx->Pcoor[nDim] ;

    if ( ( pdBuf - dBuf ) > mVx ) {
      sprintf ( hip_msg, "too many vertices "FUNLOC", expected %"FMT_ULG","
                " found %"FMT_ULG".", mVx,  ( pdBuf - dBuf ) ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    h5_write_dbl ( grp_id, 0, (char*) charDim[nDim], mVx, dBuf ) ;
  }


  arr_free ( dBuf ) ;

  /* Close parameters. */
  status = H5Gclose(grp_id);

  return ( 1 ) ;
}

/******************************************************************************

  h5w_vxData:
  Write other nodal data to hdf5 mesh file.

  Last update:
  ------------
  26Feb18; correct call arg -1 to compute_vrtxVol
  24Feb18; new interface to compute_vrtxVol
  6Sep18; Don't run if h5w_flag_dump != 0.
  20Dec16: derived from h5w_coor.

  Input:
  ------
  pUns = grid
  compress = if non-zero, write with compression
  patch_id = hdf5 id of bc to write.

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_vxData ( uns_s *pUns, const int compress, hid_t file_id) {

  const int mDim = pUns->mDim ;
  const ulong_t mVx = pUns->mVertsNumbered ;

  hid_t       grp_id;   /* identifier */
  herr_t      status;

  int nDim, nBeg, nEnd ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  double *dBuf, *pdBuf ;

  //if ( h5w_flag_dump || !( h5w_flag_vrtxVol
  if ( !( h5w_flag_vrtxVol
                           // || or other vrtx data flags in the future
                           ) )
    return ( 0 ) ;


  /* All vars are written as vectors, so allocate one. */
  dBuf = arr_malloc ( "dBuf in h5w_vxData", pUns->pFam, pUns->mVertsNumbered,
                      sizeof( *dBuf ) ) ;
  grp_id = H5Gcreate(file_id, "VertexData", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;


  if ( h5w_flag_vrtxVol ) {
    // this also allocates pChunk->PvrtxVo;
    compute_vrtxVol ( pUns, -1 ) ;


    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      pChunk = NULL ;
      pdBuf = dBuf ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
        for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
          if ( pVrtx->number )
            *pdBuf++ = pChunk->pVrtxVol[ pVrtx->vxCpt.nr ] ;
      }

      h5_write_dbl ( grp_id, 0, "volume" , mVx, dBuf ) ;
    }

    free_chunk_vrtxVol ( pUns ) ;
  }


  arr_free ( dBuf ) ;

  /* Close parameters. */
  status = H5Gclose(grp_id);

  return ( 1 ) ;
}


/******************************************************************************

  h5w_mesh_conn:
  Write connectivity to hdf5 mesh file.

  Last update:
  ------------
  17jul20; intro mElOfTypeWritten
  15Jul13; use zone_get_acive_number.
  4Apr13; modified interface to loop_elems.
  17Feb12; fix error in text label when allocating pEl2Zn.
           use ulong_t to compute size of pEl2Vx.
  18Jul11; write out zone info.
  3Apr11; use ptrdiff in error messages with %td format.
  14Apr08; reset pEV to reflect reusing the temp memory for each elType.
  18May07; initial bug fixes.
  17Dec06: derived from avbp_hdf5_sol.

  Input:
  ------
  pUns: grid
  file_id: hdf pointer to opened group section
  mElOfTypeWritten: number of elements of each type already written

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_conn ( uns_s *pUns, hid_t file_id,
               const ulong_t mElOfTypeWritten[MAX_ELEM_TYPES] ) {
#undef FUNLOC
#define FUNLOC "in h5w_conn"


  /* Alloc for max size of the conn record for each type. */
  elType_e elType ;
  ulong_t mElems ;
  ulong_t maxElType = 0 ;
  ulong_t mVerts ;
  ulong_t mConn = 0, mConnElType, mEN = 0 ;
  for ( elType = tri ; elType <= hex ; elType++ ) {
    mEN += mElOfTypeWritten[elType] ;
    mElems = pUns->mElemsOfType[elType] - mElOfTypeWritten[elType] ;
    maxElType = MAX( maxElType, mElems ) ;
    mVerts = elemType[elType].mVerts ;
    mConnElType = mVerts*mElems ;
    mConn = MAX( mConn, mElems*mVerts ) ;
  }
  ulong_t *pEl2Vx = arr_malloc ( "pEl2Vx in h5w_conn", pUns->pFam,
                                 mConn, sizeof( *pEl2Vx ) ) ;

  int *pEl2Zn = NULL ;
  if ( pUns->mZones )
    pEl2Zn = arr_malloc ( "pEl2Zn in h5w_conn", pUns->pFam,
                          maxElType, sizeof( *pEl2Zn ) ) ;

  hid_t grp_id = H5Gcreate(file_id, "Connectivity", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;


  ulong_t *pEV ;
  int *pEZ ;
  chunk_struct *pChunk ;
  vrtx_struct **ppVx ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  char string[LINE_LEN] ;
  /* Element connectivities. First all base elements. */
  for ( elType = tri ; elType <= hex ; elType++ ) {
    /* We want clean base elements only. These are counted in the first tri-hex
       positions of mElems_w_mVerts. */
    mElems = pUns->mElemsOfType[elType] - mElOfTypeWritten[elType]; //pUns->mElems_w_mVerts[elType] ;

    if ( mElems ) {
      mVerts = elemType[elType].mVerts ;
      pEV = pEl2Vx ;
      pEZ = pEl2Zn ;

      /* Make a consecutive list of pointers. */
      pChunk = NULL ;
      while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
        for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
          if ( pEl->number && pEl->elType == elType && pEl->number > mEN ) {
            if ( pEl->number != ++mEN )
              hip_err ( fatal, 0, "element number/order mismatch "FUNLOC"." ) ;
            for ( ppVx = pEl->PPvrtx ; ppVx < pEl->PPvrtx+mVerts ; ppVx++ )
              *pEV++ = (*ppVx)->number ;

            if ( pUns->mZones ){
              if ( pEl->iZone )
                /* Valid zone pointer, look up its number. */
                *pEZ++ = zone_get_active_number ( pUns, pEl->iZone ) ;
              else
                /* No zone for this element. */
                *pEZ++ = 0 ;

            }
          }

      if ( pEV-pEl2Vx != mVerts*mElems ) {
        arr_free ( pEl2Vx ) ;
        sprintf ( hip_msg,
                  "%"FMT_ULG" conn. entries expected, but %td found in h5w_conn.\n",
                  mVerts*mElems, (ptrdiff_t)(pEV-pEl2Vx) ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      /* Create the hdf entry, possibly empty. */
      sprintf ( string, "%s->node", elemType[elType].name ) ;
      h5_write_ulg ( grp_id, 0, string, mVerts*mElems, pEl2Vx ) ;

      if ( pUns->mZones ) {
        sprintf ( string, "%s->zone", elemType[elType].name ) ;
        h5_write_int ( grp_id, 0, string, mElems, pEl2Zn ) ;
      }
    }

  }


  /* Close parameters. */
  arr_free ( pEl2Vx ) ;
  arr_free ( pEl2Zn ) ;
  herr_t status = H5Gclose(grp_id);


  return ( 1 ) ;
}

/******************************************************************************
  h5w_zone_param:   */

/*! write a parameter to a zone.
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  27Aug11; modified group structure for zones.
  18Jul11: conceived.


  Input:
  ------
  zn_id: opened zone section
  pPar: parameter to write

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_zone_param ( hid_t zn_id, param_s *pPar ) {


  hid_t mtyp_id=0 ;
  if ( pPar->parType == parInt )
    mtyp_id = HH_INT ;
  else if ( pPar->parType == parDbl || pPar->parType == parVec )
    mtyp_id = HH_DBL ;
  else
    hip_err ( fatal, 0, "this hdf parameter type is not coded in h5w_zone_param" ) ;

  h5_write_vec ( zn_id, 0, mtyp_id, pPar->name, pPar->dim, pPar->pv ) ;

  if ( pPar->parType == parVec ) {
    /* Set the vector flag attribute. Reopen the above dataset. */
    hid_t par_id =  H5Dopen( zn_id, pPar->name, H5P_DEFAULT ) ;


    /* Create scalar attribute.
       http://www.hdfgroup.org/HDF5/doc/Intro/IntroExamples.html#ReadWriteAttributes
    */
    hid_t aid2  = H5Screate( H5S_SCALAR ) ;
    hid_t attr2 = H5Acreate( par_id, "IsVector", HH_INT, aid2, H5P_DEFAULT, H5P_DEFAULT ) ;
    int isVec = 1 ;
    H5Awrite( attr2, HH_INT, &isVec ) ;
    H5Aclose( attr2 ) ;
    H5Dclose( par_id ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************
  h5w_zone:   */

/*! write zonal information to hdf.
 */

/*

  Last update:
  ------------
  9Apr20; replace call to one_fxstr with fxStr and dim=1,
          reading one_fxStr fails for some reason.
  20Dec14; list all nodes formed by all elements of a zone.
  15Dec13; use renamed h5_write_one_fx_str
  12Dec13; intro is_sol to write solParams
  15Jul13; use zone_loop to hide internal data-structure.
  5Jul13; print zone number out with leading zeros, 3 digits wide.
  16Dec11; write also empty zones to be filled in external pre-proc.
  18Jul11: conceived.


  Input:
  ------
  pUns
  file_id: opened hdf file
  is_sol: is there a solution or not.

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_zone ( uns_s *pUns, hid_t file_id, int is_sol ) {

  if ( !pUns->mZones )
    /* No zones present. */
    return ( 0 ) ;

  int mZonesFound = 0 ;
  /* suppress a non-init compiler warning. */
  hid_t grp_id = H5I_INVALID_HID, zn_id, parGrp_id ;
  char tag[MAX_BC_CHAR] ;
  param_s *pPar ;
  herr_t status ;
  char parLabel[2][LINE_LEN] = { "Parameters", "SolParameters" } ;
  /* restrict is_sol to 0, 1 */
  is_sol = ( is_sol ? 1 : 0 ) ;
  ulong_t mZoneVx, *pZoneVx = NULL ;

  zone_s *pZ = NULL ;
  while ( zone_loop ( pUns, &pZ ) ) {
    /* This is a valid zone. Write zones without elements as well. */

    /* First zone found? */
    if ( !mZonesFound ) {
      mZonesFound = 1 ;
      grp_id = H5Gcreate(file_id, "Zones",
                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
      if ( !grp_id )
        hip_err ( fatal, 0, "failed to open zone section in h5w_zone." ) ;
    }

    sprintf ( tag, "%03d", pZ->number ) ;
    zn_id = H5Gcreate(grp_id, tag, H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
    if ( !zn_id )
      hip_err ( fatal, 0, "failed to open new zone in h5w_zone." ) ;



    /* Zone name. */
    //h5_write_one_fxStr ( zn_id, "ZoneName", fxStr240, pZ->name ) ;
    h5_write_fxStr ( zn_id, "ZoneName", 1, fxStr240, pZ->name ) ;



    /* Zone parameters. */
    if ( is_sol )
      pPar = pZ->pSolParam ;
    else
      pPar = pZ->pParam ;

    if ( pPar ) {
      /* Only open a Parameters section if there are params to list. */
      parGrp_id = H5Gcreate(zn_id, parLabel[is_sol],
                            H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
      while ( pPar ) {
        h5w_zone_param ( parGrp_id, pPar ) ;
        pPar = pPar->pNxtPar ;
      }
      status = H5Gclose( parGrp_id );
    }


    /* Nodes within/or touching the zone. */
    if ( ( mZoneVx = zone_list_nodes ( pUns, pZ, &pZoneVx ) ) ) {
      h5_write_ulg ( zn_id, 0, "znode->node", mZoneVx, pZoneVx ) ;
      arr_free ( pZoneVx ) ; pZoneVx = NULL ;
    }

    /* Done with writing to this zone. */
    status = H5Gclose( zn_id );
  }

  if ( mZonesFound )
    status = H5Gclose( grp_id );


  return ( 0 ) ;
}




/******************************************************************************

  h5w_one_bnd_patch_conn:
  Write boundary patch connectivity to hdf5 mesh file.

  Last update:
  ------------
  29jun17; rename to one_bnd to allow refactoring of h5w_bnd.
  15Dec12; cut out of h5w_bnd.

  Input:
  ------
  pUns:
  compress: if non-zero, compress the vector records.
  file_id: file/group to add Boundary under.
  nBc: number of the bc patch to write.
  mFcBc: numbers of faces per type on this patch.


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_one_bnd_patch_conn ( uns_s *pUns, const int compress, hid_t file_id,
                             int nBc, ulong_t mFcBc[] ) {

  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;

  hid_t       grp_id;   /* identifier */
  herr_t      status;

  ulong_t mB, mT, mQ, *pBv, *pBi2vx, *pTv, *pTri2vx, *pQv, *pQuad2vx, mVerts ;
  vrtx_struct **ppVx ;
  elem_struct *pElem ;
  bndPatch_struct *pBndPatch = NULL ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;


  /* Boundary faces. */
  mB = mFcBc[2] ;
  mT = mFcBc[3] ;
  mQ = mFcBc[4] ;


  pBv = pBi2vx   = arr_malloc ( "pBi2vx   h5w_bnd", pUns->pFam,2*mB, sizeof( *pBi2vx   ) );
  pTv = pTri2vx  = arr_malloc ( "pTri2vx  h5w_bnd", pUns->pFam,3*mT, sizeof( *pTri2vx  ) );
  pQv = pQuad2vx = arr_malloc ( "pQuad2vx h5w_bnd", pUns->pFam,4*mQ, sizeof( *pQuad2vx ) );


  /* List the faces of this patch by type. */
  while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
      pElem = pBndFc->Pelem ;
      if ( pElem && pElem->number && pBndFc->nFace ) {
        pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
        mVerts = pFoE->mVertsFace ;
        kVxFc = pFoE->kVxFace ;
        ppVx = pElem->PPvrtx ;

        if ( mVerts == 2 ) {
          *pBv++ = ppVx[ kVxFc[0] ]->number ;
          *pBv++ = ppVx[ kVxFc[1] ]->number ;
        }
        else if ( mVerts == 3 ) {
          *pTv++ = ppVx[ kVxFc[0] ]->number ;
          *pTv++ = ppVx[ kVxFc[1] ]->number ;
          *pTv++ = ppVx[ kVxFc[2] ]->number ;
        }
        else {
          *pQv++ = ppVx[ kVxFc[0] ]->number ;
          *pQv++ = ppVx[ kVxFc[1] ]->number ;
          *pQv++ = ppVx[ kVxFc[2] ]->number ;
          *pQv++ = ppVx[ kVxFc[3] ]->number ;
        }
      }
    }
  }


  if ( pBv - pBi2vx != 2*mB || pTv - pTri2vx != 3*mT || pQv - pQuad2vx != 4*mQ ) {
    sprintf ( hip_msg,
              "internal error: miscount of face connectivities in h5w_bnd_patch_conn.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }


  grp_id = H5Gcreate(file_id, "Boundary", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;


  if ( mB ) h5_write_ulg ( grp_id, 0, "bnd_bi->node",  2*mB,   pBi2vx ) ;
  if ( mT ) h5_write_ulg ( grp_id, 0, "bnd_tri->node", 3*mT,  pTri2vx ) ;
  if ( mQ ) h5_write_ulg ( grp_id, 0, "bnd_qua->node", 4*mQ, pQuad2vx ) ;

  /* Done with the boundary faces. */
  arr_free ( pBi2vx    ) ;
  arr_free ( pTri2vx   ) ;
  arr_free ( pQuad2vx  ) ;

  /* Close boundary. */
  status = H5Gclose(grp_id);


  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_bnd_patch_conn
*/
/*! write surface connectivity for all boundary patches with geoType=bnd.

 */

/*

  Last update:
  ------------
  9Jul19; allow grp Patch to already exist.
  29jun17: extracted from h5w_bnd.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_bnd_patch_conn ( uns_s *pUns, hid_t file_id ) {


  /* Write boundary connectivity. */
  hid_t       grp_id;   /* identifier for group.*/
  hid_t       patch_id;   /* identifier for each patch.*/
  int nBc ;
  ulong_t mFcBnd[5] ;
  char tag[MAX_BC_CHAR] ;
  herr_t      status ;
  /* Write the boundary surfaces with their own connectivity, so they
     can be read standalone e.g. for surface visualisation,
     Renumber the vertices omitting internal ones. Note that internal
     elements remain numbered, but will not be written.*/

  // JDM: 12Sep16; why do we need to renumber?
  number_uns_elem_leafs ( pUns ) ;


  if ( !h5_grp_exists ( file_id, "Patch" ) ) {
    grp_id = H5Gcreate( file_id, "Patch", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
  }
  else
    grp_id = h5_open_group ( file_id, "Patch" ) ;

  /* Loop only over actual bnd patches: geoType == bnd. They are listed first in ppBc.*/
  for ( nBc = 0 ; nBc < pUns->mBcBnd ; nBc++ ) {
    /* Number all vertices in this boundary patch. */
    if ( number_uns_vert_bc ( pUns, 0, 1, &nBc, mFcBnd ) ) {
      /* There are active faces in this patch. */
      sprintf ( tag, "%d", nBc+1 ) ;
      patch_id = H5Gcreate(grp_id, tag, H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

      const int compress  = 1 ;
      const ulong_t mVxWritten = 0 ;
      h5w_coor ( pUns, compress, patch_id, mVxWritten ) ;
      h5w_one_bnd_patch_conn ( pUns, compress, patch_id, nBc, mFcBnd ) ;

      status = H5Gclose( patch_id );
    }
  }
  status = H5Gclose( grp_id );


  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_bnd_faces_xmf:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  18dec24; refactor into h5w_var_xmf.
  23jul20; enable quad/tri face write for hybrid grids.
  4Jyl17: extracted from h5w_bnd.


  Input:
  ------
  pUns: grid
  meshFileNmH5: hdf5 filename for mesh
  solFileNmH5: hdf5 filename for sol

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_bnd_faces_xmf ( uns_s *pUns, FILE* Fxmf,
                        bcGeoType_e geoType, int mBcAct, string240 *bcLabels,
                        ulong_t mB, ulong_t *idxBi,
                        ulong_t mT, ulong_t *idxTri,
                        ulong_t mQ, ulong_t *idxQuad,
                        const char *meshFileNmH5, const char *solFileNmH5 ) {

  string240 patchLabel;
  ulong_t last_lidxTri, last_lidxQuad, last_lidxBi;
  int kBc ;
  last_lidxBi = 0 ;
  last_lidxTri = 0 ;
  last_lidxQuad = 0 ;
  if ( mQ || mT ) {  // if ( !(mQ && mT) ) {
    for ( kBc =0 ; kBc < mBcAct ; kBc++ ) {
      strcpy ( patchLabel, bcLabels[kBc] );
      if ( idxQuad[kBc]) {
        // If there are both tri and quad faces, specialise the bc label.
        if ( idxTri[kBc] )
          sprintf ( patchLabel, "%s_quaFc", bcLabels[kBc] ) ;
        h5w_bndXmf (Fxmf, pUns, meshFileNmH5,solFileNmH5,
                    patchLabel,"Quadrilateral","bnd_qua->node",
                    4,last_lidxQuad,idxQuad[kBc],4*mQ);
        last_lidxQuad =  idxQuad[kBc] ;
      }
      if ( idxTri[kBc]) {
        if ( idxQuad[kBc] )
          sprintf ( patchLabel, "%s_triFc", bcLabels[kBc] ) ;
        h5w_bndXmf (Fxmf,pUns, meshFileNmH5,solFileNmH5,
                    patchLabel,"Triangle","bnd_tri->node",
                    3,last_lidxTri,idxTri[kBc],3*mT);
        last_lidxTri =  idxTri[kBc] ;
      }
    }
  }
  /*
  else{
    if ( verbosity > 2 ) {
      sprintf ( hip_msg,
                "Multi element mesh, Patch XMF is not supported yet. Skipping .. \n");
      hip_err ( warning, 2, hip_msg ) ;
    }

    }*/

  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_bnd_faces:
*/
/*! write boundary faces to hdf.
 *
 */

/*

  Last update:
  -----------
  21dec21; include interface counts in mB-mQ for geoType=bndAndInter
  29jul20; fix typo with quad_fidx, which should be qua_fidx.
  20Jul20; move all args for the xmf call after FXMF.
  5May20; why is idx allocated to 2*mBcAct? Write out fidx.
  15jul19; use isMatch_geoType.
  15Jul17; init mB, mT, mQ to zero.
  4Jul17: extracted from h5w_bnd.


  Input:
  ------
  pUns: grid
  grp_id: handle to `Boundary' group in hdf5 database.
  mBcAct: number of active bcs
  Fxmf: opened and positioned xmf file
  bcLabels: list of bc labels
  meshFileNmH5: name of h5 mesh datatbase
  solFileNmH5: name of h5 sol datatbase
  mElWritten: write faces of elemens not yet written, i.e. number > mElWritten
  mFcWritten: number of faces with 2,3,4 vertices already written.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int  h5w_bnd_faces ( uns_s *pUns, hid_t grp_id, bcGeoType_e geoType, int mBcAct,
                     FILE* Fxmf, string240 *bcLabels,
                     const char *meshFileNmH5, const char *solFileNmH5,
                     const ulong_t mElWritten, const ulong_t mFcWritten[MAX_VX_FACE+1] ) {

  /* Count and allocate. */
  ulong_t mB=0, mT=0, mQ=0 ;
  if ( geoType == bnd || geoType == bndAndInter ) {
    mB = pUns->mBiAllInter   +pUns->mBiAllBc  -mFcWritten[2] ;
    mT = pUns->mTriAllInter  +pUns->mTriAllBc -mFcWritten[3] ;
    mQ = pUns->mQuadAllInter +pUns->mQuadAllBc-mFcWritten[4] ;
  }
  else if ( geoType == inter ) {
    mB = pUns->mBiAllInter  -mFcWritten[2] ;
    mT = pUns->mTriAllInter -mFcWritten[3] ;
    mQ = pUns->mQuadAllInter-mFcWritten[4] ;
  }
  else
    hip_err ( fatal, 0, "invalid geoType in h5w_bnd_faces." ) ;

  ulong_t  *idxBi, *fidxBi,  *pBv, *pBi2vx,    *pBe, *pBiEl,   *pBk, *pBikFc ;
  idxBi          = arr_malloc ( "idxBi    h5w_bnd", pUns->pFam,  mBcAct,sizeof( *idxBi    ));
  fidxBi         = arr_malloc ( "fidxBi   h5w_bnd", pUns->pFam,  mBcAct+1,sizeof( *idxBi    ));
  pBe = pBiEl    = arr_malloc ( "pBiEl    h5w_bnd", pUns->pFam,  mB,    sizeof( *pBiEl    ));
  pBk = pBikFc   = arr_malloc ( "pBikFc   h5w_bnd", pUns->pFam,  mB,    sizeof( *pBikFc   ));
  pBv = pBi2vx   = arr_malloc ( "pBi2vx   h5w_bnd", pUns->pFam,2*mB,    sizeof( *pBi2vx   ));

  ulong_t  *idxTri, *fidxTri,   *pTv, *pTri2vx,   *pTe, *pTriEl,  *pTk, *pTrikFc ;
  idxTri         = arr_malloc ( "idxTri   h5w_bnd", pUns->pFam,  mBcAct,sizeof( idxTri    ));
  fidxTri        = arr_malloc ( "fidxTri  h5w_bnd", pUns->pFam,  mBcAct+1,sizeof( idxTri    ));
  pTe = pTriEl   = arr_malloc ( "pTriEl   h5w_bnd", pUns->pFam,  mT,    sizeof( *pTriEl   ));
  pTk = pTrikFc  = arr_malloc ( "pTrikFc  h5w_bnd", pUns->pFam,  mT,    sizeof( *pTrikFc  ));
  pTv = pTri2vx  = arr_malloc ( "pTri2vx  h5w_bnd", pUns->pFam,3*mT,    sizeof( *pTri2vx  ));

  ulong_t  *idxQuad, *fidxQuad, *pQv, *pQuad2vx,  *pQe, *pQuadEl, *pQk, *pQuadkFc ;
  idxQuad        = arr_malloc ( "idxQuad  h5w_bnd", pUns->pFam,  mBcAct,sizeof( *idxQuad  ));
  fidxQuad       = arr_malloc ( "fidxQuad h5w_bnd", pUns->pFam,  mBcAct+1,sizeof( *idxQuad  ));
  pQe = pQuadEl  = arr_malloc ( "pQuadEl  h5w_bnd", pUns->pFam,  mQ,    sizeof( *pQuadEl  ));
  pQk = pQuadkFc = arr_malloc ( "pQuadkFc h5w_bnd", pUns->pFam,  mQ,    sizeof( *pQuadkFc ));
  pQv = pQuad2vx = arr_malloc ( "pQuad2vx h5w_bnd", pUns->pFam,4*mQ,    sizeof( *pQuad2vx ));




  /* List the element and face number. */
  int nBc, kBc = -1 ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  vrtx_struct **ppVx,  *pVxBeg, *pVxEnd, *pVx ;
  const faceOfElem_struct *pFoE ;
  ulong_t mVerts ;
  const int *kVxFc ;
  for ( nBc = 0 ; pBndPatch = NULL, nBc < pUns->mBc ; nBc++ ) {
    if ( isMatch_geoType( pUns->ppBc[nBc]->geoType, geoType ) ) {
      kBc++ ;
      idxBi[kBc]=0;
      idxTri[kBc]=0;
      idxQuad[kBc]=0;

      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pElem = pBndFc->Pelem ;
          if ( pElem && pElem->number && pElem->number > mElWritten &&
               pBndFc->nFace ) {
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mVerts = pFoE->mVertsFace ;
            kVxFc = pFoE->kVxFace ;
            ppVx = pElem->PPvrtx ;

            if ( mVerts == 2 ) {
              if ( pBe > pBiEl+mB-1 ) {
                sprintf ( hip_msg,
                          "miscount of bi face connectivities in h5w_bnd_faces.\n" ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
              *pBe++ = pElem->number ;
              *pBk++ = pBndFc->nFace ;

              *pBv++ = ppVx[ kVxFc[0] ]->number ;
              *pBv++ = ppVx[ kVxFc[1] ]->number ;
              idxBi[kBc]++ ;
            }
            else if ( mVerts == 3 ) {
              if ( pTe > pTriEl+mT-1 ) {
                sprintf ( hip_msg,
                          "miscount of tri face connectivities in h5w_bnd_faces.\n" ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
              *pTe++ = pElem->number ;
              *pTk++ = pBndFc->nFace ;

              *pTv++ = ppVx[ kVxFc[0] ]->number ;
              *pTv++ = ppVx[ kVxFc[1] ]->number ;
              *pTv++ = ppVx[ kVxFc[2] ]->number ;
              idxTri[kBc]++ ;
            }
            else {
              if ( pQe > pQuadEl+mQ-1 ) {
                sprintf ( hip_msg,
                          "miscount of quad face connectivities in h5w_bnd_faces.\n" ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
              *pQe++ = pElem->number ;
              *pQk++ = pBndFc->nFace ;

              *pQv++ = ppVx[ kVxFc[0] ]->number ;
              *pQv++ = ppVx[ kVxFc[1] ]->number ;
              *pQv++ = ppVx[ kVxFc[2] ]->number ;
              *pQv++ = ppVx[ kVxFc[3] ]->number ;
              idxQuad[kBc]++ ;
            }
          }
        }
    }
  }

  if ( kBc != mBcAct-1 ) {
    sprintf ( hip_msg, "Expected %d, found %d active boundaries in h5w_bnd_faces.", mBcAct, kBc );
    hip_err ( fatal, 0, hip_msg ) ;
  }

  if ( pBe - pBiEl != mB || pTe - pTriEl != mT || pQe - pQuadEl != mQ ) {
    sprintf ( hip_msg,
              "miscount of face connectivities in h5w_bnd_faces.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Offset the indices to write a lidx - last pointees. */
  ulong_t mBf, mTf, mQf ;
  for ( mBf=mTf=mQf=0, kBc = 0 ; kBc < mBcAct ; kBc++ ) {
    mBf += idxBi[kBc] ;
    mTf += idxTri[kBc] ;
    mQf += idxQuad[kBc] ;

    idxBi[kBc]   = mBf ;
    idxTri[kBc]  = mTf ;
    idxQuad[kBc] = mQf ;
  }


  /* Write the list to hdf. */
  if ( mB ) {
    h5_write_ulg ( grp_id, 0, "bnd_bi_lidx",    mBcAct,    idxBi ) ;
    ulidx2fidx ( idxBi, ( ulong_t ) mBcAct, fidxBi ) ;
    h5_write_ulg ( grp_id, 0, "bnd_bi_fidx",    mBcAct+1,   fidxBi ) ;
    h5_write_ulg ( grp_id, 0, "bnd_bi->elem",   mB,   pBiEl ) ;
    h5_write_ulg ( grp_id, 0, "bnd_bi->face",   mB,   pBikFc ) ;
    h5_write_ulg ( grp_id, 0, "bnd_bi->node", 2*mB,   pBi2vx ) ;
  }

  if ( mT ) {
    h5_write_ulg ( grp_id, 0, "bnd_tri_lidx",   mBcAct,   idxTri ) ;
    ulidx2fidx ( idxTri, ( ulong_t  ) mBcAct, fidxTri ) ;
    h5_write_ulg ( grp_id, 0, "bnd_tri_fidx",  mBcAct+1, fidxTri ) ;
    h5_write_ulg ( grp_id, 0, "bnd_tri->elem",   mT,  pTriEl ) ;
    h5_write_ulg ( grp_id, 0, "bnd_tri->face",   mT,  pTrikFc ) ;
    h5_write_ulg ( grp_id, 0, "bnd_tri->node", 3*mT,  pTri2vx ) ;
  }

  if ( mQ ) {
    h5_write_ulg ( grp_id, 0, "bnd_qua_lidx",   mBcAct,  idxQuad ) ;
    ulidx2fidx ( idxQuad, ( ulong_t ) mBcAct, fidxQuad ) ;
    h5_write_ulg ( grp_id, 0, "bnd_qua_fidx",  mBcAct+1, fidxQuad ) ;
    h5_write_ulg ( grp_id, 0, "bnd_qua->elem",   mQ, pQuadEl ) ;
    h5_write_ulg ( grp_id, 0, "bnd_qua->face",   mQ, pQuadkFc ) ;
    h5_write_ulg ( grp_id, 0, "bnd_qua->node", 4*mQ, pQuad2vx ) ;
  }




  /* hdf layoout of Boundary faces to xmf.
     Test for geoType should not be needed, to be removed. */
  if ( Fxmf ) {
    h5w_bnd_faces_xmf ( pUns, Fxmf,
                        geoType, mBcAct, bcLabels,
                        mB, idxBi, mT, idxTri, mQ, idxQuad,
                        meshFileNmH5, solFileNmH5 ) ;
  }

  if ( Fxmf ) {
    /* Wrap up xmf record.  */
    fprintf ( Fxmf, "  </Domain>\n");
    fprintf ( Fxmf, "</Xdmf>\n");
  }


  arr_free ( idxBi    ) ;
  arr_free ( fidxBi    ) ;
  arr_free ( pBiEl    ) ;
  arr_free ( pBikFc   ) ;
  arr_free ( pBi2vx   ) ;

  arr_free ( idxTri   ) ;
  arr_free ( fidxTri   ) ;
  arr_free ( pTriEl   ) ;
  arr_free ( pTrikFc  ) ;
  arr_free ( pTri2vx  ) ;

  arr_free ( idxQuad  ) ;
  arr_free ( fidxQuad  ) ;
  arr_free ( pQuadEl  ) ;
  arr_free ( pQuadkFc ) ;
  arr_free ( pQuad2vx ) ;

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_bnd_nodes_patchAreas:
*/
/*! write boundary nodes to hdf.
 *
 */

/*

  Last update:
  ------------
  5May20; write fidx.
  15jul19; use isMatch_geoType.
  4Jul17: extracted from h5w_bnd.


  Input:
  ------
  pUns: grid
  grp_id: hdf id of group to write under.
  mBcAct: number of active bcs.
  geoType: geo type of boundary, e.g. bnd or inter

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_bnd_nodes_patchAreas ( uns_s *pUns, hid_t grp_id,
                               int mBcAct, bcGeoType_e geoType ) {

  ulong_t *idx  =  arr_malloc ( "idx in h5w_bnode", pUns->pFam,
                                mBcAct+1, sizeof( *idx ) ) ;
  bndVxWt_s *pBWt = arr_malloc ( "pBWt in h5w_bnode", pUns->pFam,
                                 mBcAct, sizeof( *pBWt ) ) ;

  /* List all bnd vx that are marked with pVx->mark. The flag
     is set by mark_uns_vertBc depending on the value of singleNormal. */
  ulong_t  mVxAllBc ;
  make_bndVxWts ( pUns, geoType, pBWt, &mVxAllBc, 0, 1 ) ;

  /* Create an index. */
  ulong_t mVertIdx ;
  int nBc ;
  int kBc = -1 ;
  for ( mVertIdx = nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( isMatch_geoType( pUns->ppBc[nBc]->geoType, geoType ) ) {
      kBc++ ;
      mVertIdx += pUns->pmVertBc[nBc] ;
      idx[kBc] = mVertIdx ;
    }
  h5_write_ulg ( grp_id, 0, "bnode_lidx", mBcAct, idx ) ;
  ulidx2fidx( idx, ( ulong_t ) mBcAct, idx ) ;
  h5_write_ulg ( grp_id, 0, "bnode_fidx", mBcAct+1, idx ) ;


  h5_write_ulg ( grp_id, 0, "bnode->node", mVxAllBc, pBWt->pnVx ) ;

  h5_write_dbl ( grp_id, 0, "bnode->normal", pUns->mDim*mVxAllBc, pBWt->pWt ) ;

  arr_free ( idx ) ;






  /* Boundary patch surface */
  double *pBndPatchArea = arr_malloc ( "pBndPatchArea in h5w_bnd", pUns->pFam,
                                       mBcAct, sizeof ( *pBndPatchArea ) ) ;
  bndPatch_area ( mBcAct, pUns->mDim, pBWt, pBndPatchArea ) ;
  h5_write_dbl ( grp_id, 0, "Patch->area", mBcAct, pBndPatchArea ) ;

  arr_free ( pBWt->pnVx ) ;
  arr_free ( pBWt->pVx ) ;
  arr_free ( pBWt->pWt ) ;
  arr_free ( pBWt ) ;
  arr_free ( pBndPatchArea ) ;



  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_special_vx:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  27Jul20; work on geoType==bndAndInter
  5May20; write fidx.
  4jul17: extracted from h5w_bnd.


  Input:
  ------
  pUns: grid
  grp_id: hdf handle for Boundary group.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_special_vx ( uns_s *pUns, bcGeoType_e geoType, hid_t grp_id ) {



  ulong_t mVxAxi ;
  ulong_t *pN ;
  ulong_t *pnVxAxi ;
  mp_bndVx_s mpVx ;
  chunk_struct *pChunk ;
  ulong_t mVxMP, mEntries ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  if ( geoType == bndAndInter ) {
    /* Compute axis nodes and multi-periodicity nodes only for std bnd. */

    /* Vertices on the axis. */
    if ( pUns->specialTopo == axiX ||
         ( pUns->specialTopo >= axiX && pUns->specialTopo <= axiZ ) ) {
      /* How many are there? */
      mVxAxi = axis_verts ( pUns, pUns->specialTopo ) ;

      pN = pnVxAxi = arr_malloc ( "pnVxAxi in h5w_bnd", pUns->pFam,
                                  mVxAxi, sizeof( *pN ) ) ;

      /* Loop over all vertices, list all the ones on the axis. */
      pChunk = NULL ;
      while ( loop_verts( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
        for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
          if ( pVrtx->singular ) {
            *pN++ = pVrtx->number ;
          }

      h5_write_ulg ( grp_id, 0, "axis_node->node", mVxAxi, pnVxAxi ) ;
      arr_free ( pnVxAxi ) ;
    }




    /* Generate the list of multipatch boundary vertices. */
    make_mp_bndVx ( pUns, &mpVx ) ;
    mVxMP =  mpVx.mVxMP ;

    if ( mVxMP ) {
      if ( verbosity > 2 ) {
        sprintf ( hip_msg,
                  "Found multipatch boundary vertices ... writing data.\n" ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }

      /* List of the mp vx. */
      h5_write_ulg ( grp_id, 0, "mp_bnode->node", mVxMP, mpVx.nVxMP ) ;

      /* Index of patch entries. */
      h5_write_ulg ( grp_id, 0, "mp_bnode_fidx", mVxMP+1, mpVx.ndxVxMP ) ;
      ufidx2lidx ( mpVx.ndxVxMP, mVxMP, mpVx.ndxVxMP ) ;
      h5_write_ulg ( grp_id, 0, "mp_bnode_lidx", mVxMP, mpVx.ndxVxMP ) ;

      /* List of patches. */
      mEntries = mpVx.ndxVxMP[ mVxMP ]-1 ;
      h5_write_int ( grp_id, 0, "mp_bnode->patch", mEntries, mpVx.lsVxMP ) ;
    }

    arr_free ( mpVx.nVxMP ) ;
    arr_free ( mpVx.ndxVxMP ) ;
    arr_free ( mpVx.lsVxMP ) ;
  }


  return ( 0 ) ;
}


/******************************************************************************

  h5w_bnd:
  Write boundary info to hdf5 mesh file.

  Last update:
  ------------
  27jul20; call h5_special for geoType=bndAndInter
  9Jul19; write PatchLabels also to group Patch, if boundary conn is to be written.
          write a single list of bnd and interfaces.
  4Apr19; support writing grids without boundaries.
  4Aug17; fix bug with writing internal group without internal faces.
  14jul17; remove unused rootFile arg.
  4Jul17; major refactoring.
  12May17; intro geoType arg to write internal interfaces.
  21Nov16; Add surface area computation per patch
  22Apr16; right-pad patch labels with blanks to 240.
  13Jul15; fix bug with writing bound quads. (GS)
  15Dec13; replace x_axis_verts with axis_verts.
  15Dec13; remove topo identifier annCascade, intro axiY, axiZ.
  6Apr13: promote possibly large unsigned ints to ulong_t
  14Dec08; write x_axis_verts also with topo = annCasc.
  11Jul08; fix bug with initialisation of idx. Merci Gabriel.
  10Oct07; move mp_bnode from write_hdf5_per to here.
  10Mar07; incorporate write_hdf5_bnode,  write_hdf5_axis,
  17Dec06: derived from avbp_hdf5_sol.

  Input:
  ------
  pUns:
  file_id: id of opened hdf database.


  Returns:
  --------
  0 on failure, 1 on success.
  s
*/

int h5w_bnd ( uns_s *pUns, hid_t file_id, bcGeoType_e geoType,
              FILE* Fxmf, const char *meshFileNmH5, const char *solFileNmH5 ) {


  /* Open the Boundary group. */
  hid_t grp_id = grp_id = H5Gcreate(file_id, "Boundary", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
  if ( !grp_id ) {
    hip_err ( fatal, 0, "could not create `Boundary' grp in h5w_bnd.") ;
  }




  /* List of boundary labels. */
  string240 *bcLabels = arr_malloc ( "bcLabels in h5w_bnd",
                                     pUns->pFam, pUns->mBc, sizeof(string240) ) ;
  char *bcLblFtn =  arr_malloc ( "bcLblFtn in h5w_bnd",
                                 pUns->pFam, pUns->mBc, sizeof(string240) ) ;
  char *bcGeoType =  arr_malloc ( "bcGeoType in h5w_bnd",
                                  pUns->pFam, pUns->mBc, sizeof(char) ) ;
  char *bcF = bcLblFtn, *bcT = bcGeoType ;
  char geoTypeStr[LINE_LEN] ;
  int mBcAct = 0 ;
  ulong_t nBc ;
  const int compress = 0 ; // Dpm't compress coordinates, not effective?
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    strncpy ( bcLabels[mBcAct], pUns->ppBc[nBc]->text, 80 ) ;
    mBcAct++ ;
    ftnString0 ( bcF, sizeof(string240), pUns->ppBc[nBc]->text ) ;
    bcF += sizeof(string240) ;

    /* bnd, match, inter, duplicateInter, cut, noBcGeoType */
    geoType2Char ( pUns->ppBc[nBc]->geoType, geoTypeStr ) ;
    bcT[0] = geoTypeStr[0] ;
    bcT++ ;
  }


  /* Add to group 'Boundary'. */
  h5_write_fxStr ( grp_id, "PatchLabels", mBcAct, fxStr240, bcLblFtn ) ;
  h5_write_char ( grp_id, compress, "PatchGeoType", mBcAct, bcGeoType ) ;

  /* Do not add to Patch group since it leads to a regression issue on AVBP.
   * The number of datasets in /Patch is considered to be the number of Patchs
   * in the mesh. To be changed for the next release in AVBP but for now we can
   * not change this. GS
   */

  //  hid_t patch_grp_id = 0 ;
  //  if ( h5w_flag_bnd ) {
  //    /* Does grp `Patch' exist? If not add. */
  //    if ( !h5_grp_exists ( file_id, "Patch" ) ) {
  //      patch_grp_id = H5Gcreate( file_id, "Patch", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
  //    }
  //    else
  //      patch_grp_id = h5_open_group ( file_id, "Patch" ) ;
  //
  //    /* Add to surface grid under group Patch. */
  //    h5_write_fxStr ( patch_grp_id, "PatchLabels", mBcAct, fxStr240, bcLblFtn ) ;
  //    h5_write_char ( grp_id, compress, "PatchGeoType", mBcAct, bcGeoType ) ;
  //  }

  arr_free ( bcLblFtn ) ;
  arr_free ( bcGeoType ) ;


  /* Boundary faces to hdf and xmf. */
  const ulong_t mElWritten = {0}, mFcWritten[5] = {0} ;
  h5w_bnd_faces ( pUns, grp_id, geoType, mBcAct,
                  Fxmf, bcLabels, meshFileNmH5, solFileNmH5,
                  mElWritten, mFcWritten ) ;

  /* Done with the boundary faces. */
  arr_free ( bcLabels ) ;



  /* Boundary nodes. */
  h5w_bnd_nodes_patchAreas ( pUns, grp_id, mBcAct, geoType ) ;

  /* Special vertices, e.g. axes, multiple periodicity. */
  if ( geoType == bndAndInter ) {
    h5w_special_vx ( pUns, geoType, grp_id ) ;
  }


  /* Close boundary. */
  herr_t status = H5Gclose(grp_id);


  return ( 1 ) ;
}

/******************************************************************************

  h5w_per:
  Write periodicity to hdf5 mesh file.

  Last update:
  ------------
  21Jul21; fix bug with missing factor 2 in alloc for fc_lidx.
  24Jul20; fix c&p error in alloc of pBcShift.
  9Jun20; support translation.
  5May20; write fidx.
  28Apr20 ; add  pnVxPerBc_lidx
  28Feb18: rename rotAngle to rotAngleRad to clarify meaning.
  20Jul13; fix bug with listing bc->nr, rather than the order of writing.
  11Apr07; change to periodic_patch for patches.
           fix bug with writing 2*mVx, thanks Pablo.
  10Oct07; write entry only if period. patches exist,
           move mp_bnode to write_hdf5_bound.
  10Mar07; incorporate write_hdf5_mpvx.
  17Dec06: derived from avbp_hdf5_sol.

  Input:
  ------

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_perDir ( const transf_e tr_op ) {
  int iTrOp ;

  switch ( tr_op ) {
  case rot_x :
    iTrOp = 1 ;
    break ;
  case rot_y :
    iTrOp = 2 ;
    break ;
  case rot_z :
    iTrOp = 3 ;
    break ;
  case trans :
    iTrOp = 4 ;
    break ;
  default :
    iTrOp = -1 ;
  }
  return ( iTrOp ) ;
}

int h5w_per ( uns_s *pUns, hid_t file_id ) {

  const ulong_t mPBcP = pUns->mPerBcPairs ;
  if ( mPBcP == 0 )
    /* No peridocity. */
    return ( 1 ) ;


  /* Sort the periodic vertex pairs by bc. */
  const ulong_t mPVP = pUns->mPerVxPairs ;
  qsort ( pUns->pPerVxPair, mPVP,
          sizeof ( *pUns->pPerVxPair ), cmp_perVxPair_bc ) ;

  /* Make a lidx for the list of periodic vertices. */
  ulong_t *pnVxPerBc_lidx = arr_malloc ( "pnVxPerBc_lidx in h5w_per", pUns->pFam,
                                         2*mPBcP+1, sizeof( *pnVxPerBc_lidx ) ) ;
  ulong_t *pnBcPairs = arr_malloc ( "pnBcPairs in h5w_per", pUns->pFam,
                                    2*mPBcP, sizeof( *pnBcPairs ) ) ;
  ulong_t *pnVxPer = arr_malloc ( "pnVxPer in h5w_per", pUns->pFam,
                                  2*mPVP, sizeof( *pnVxPer ) ) ;
  ulong_t *pnV0 = pnVxPer, *pnV1 = pnVxPer+2*mPVP-1 ;

  ulong_t iVx ;
  perVxPair_s *pPV = pUns->pPerVxPair ;
  perVxPair_s *pPVLast =  pUns->pPerVxPair+pUns->mPerVxPairs-1 ;
  int iThisBc = pPV->pPerBc->pBc[ pPV->revDir]->order, iNxtBc ;
  ulong_t *plidx = pnVxPerBc_lidx ;
  int kBcP = 0 ;
  for ( pPV = pUns->pPerVxPair ; pPV <= pPVLast ; pPV++ ) {
    *pnV0++ = pPV->In->number ;
    /* The matching parter is listed backwards. */
    *pnV1-- = pPV->Out->number ;


    if ( pPV == pPVLast )
      iNxtBc = -1 ;
    else
      iNxtBc = pPV[1].pPerBc->pBc[ pPV[1].revDir ]->order ;

    if ( iThisBc != iNxtBc ) {
      /* New/final bc section. */
      *plidx++ = pPV-pUns->pPerVxPair ;
      // The Bc of the completed section.
      pnBcPairs[kBcP] = iThisBc ;
      // The periodic pair bc on the other side.
      pnBcPairs[kBcP+1] =  pPV->pPerBc->pBc[ 1-pPV->revDir ]->order ;

      // Next section.
      iThisBc = iNxtBc ;
      kBcP++ ;
    }
  } // for ( pPV = pUns->pPerVxPair
  if ( kBcP > mPBcP ) {
    hip_err ( fatal, 0, "too many periodic pairs in h5w_per." ) ;
  }
  if ( (pnV0 - pnVxPer) != mPVP ) {
    hip_err ( fatal, 0, "too many periodic nodes in h5w_per." ) ;
  }



  /* Complete the lidx symmetrically. */
  int nBc ;
  ulong_t mVxPBc ;
  for ( kBcP = 0 ; kBcP < mPBcP ; kBcP++ ) {
    mVxPBc = ( kBcP < mPBcP-1 ?
               pnVxPerBc_lidx[ mPBcP-kBcP-1 ]- pnVxPerBc_lidx[ mPBcP-kBcP-2 ] :
               pnVxPerBc_lidx[ 0 ] ) ;
    pnVxPerBc_lidx[ mPBcP+kBcP ] = pnVxPerBc_lidx[ mPBcP+kBcP-1 ] + mVxPBc ;
  }



  /* Matching patch info. */
  double *pBcRotAngle = arr_malloc ( "pBcRotAngle in h5w_per", pUns->pFam,
                                     mPBcP, sizeof( *pBcRotAngle ) ) ;
  int *piBcRotAxis = arr_malloc ( "pBcRotAxis in h5w_per", pUns->pFam,
                                  mPBcP, sizeof( *piBcRotAxis ) ) ;
  double *pBcShift = arr_malloc ( "pBcShift in h5w_per", pUns->pFam,
                                  mPBcP*pUns->mDim, sizeof( *pBcShift ) ) ;


  int foundRot=0 ;
  for ( kBcP = 0 ; kBcP < mPBcP ; kBcP++ ) {
    /* Find out as which number this bc is listed. */
    nBc = find_nBc ( pUns, pUns->pPerBc[kBcP].pBc[0] ) ; //lower
    pnBcPairs[2*kBcP] = nBc+1 ;

    nBc = find_nBc ( pUns, pUns->pPerBc[kBcP].pBc[1] ) ; // upper
    pnBcPairs[2*kBcP+1] = nBc+1 ;

    if ( pUns->pPerBc[kBcP].tr_op >= rot_x &&
         pUns->pPerBc[kBcP].tr_op <= rot_z ) {
      foundRot = 1 ;
      pBcRotAngle[kBcP] = pUns->pPerBc[kBcP].rotAngleRad/PI*180 ;

      piBcRotAxis[kBcP] = h5w_perDir ( pUns->pPerBc[kBcP].tr_op ) ;
      // if ( pUns->pPerBc[kBcP].tr_op >= rot_y &&
      //      pUns->pPerBc[kBcP].tr_op <= rot_z ) {
      //   sprintf ( hip_msg,  "periodic rotation is not around x-axis,\n"
      //             "        the hdf format does record the axis, but your solver\n"
      //             "        may tacitly assume x.\n" ) ;
      //   hip_err ( warning, 1, hip_msg ) ;
      // }
    }
    else if (  pUns->pPerBc[kBcP].tr_op >= trans ) {
      pBcRotAngle[kBcP] = 0. ;
      piBcRotAxis[kBcP] = h5w_perDir ( pUns->pPerBc[kBcP].tr_op ) ;
      vec_copy_dbl ( pUns->pPerBc[kBcP].shftIn2out, pUns->mDim,
                     pBcShift+kBcP*pUns->mDim ) ;
    }
    else
      hip_err ( fatal, 0, "unknown periodic transformation type in hw_per." ) ;
  } //     for ( kBcP = 0 ; kBcP < mPBcP ; kBcP++ ) {


  


  /* Make a list of periodic matching faces. They can only be one to one pairs, no multiplicity
     such as e.g. for corner nodes with multiple periodicity. */
  match_per_faces ( pUns ) ;

  ulong_t *pnFcPerBc_lidx = arr_malloc ( "pnVxPerBc_lidx in h5w_per", pUns->pFam,
                                         2*mPBcP+1, sizeof( *pnVxPerBc_lidx ) ) ;
  ulong_t mPerFcPairs = 0 ;
  pnFcPerBc_lidx[0] = 1 ;
  for ( kBcP = 0 ; kBcP < mPBcP ; kBcP++ ) {
    mPerFcPairs += pUns->pPerBc[kBcP].mPerFcPairs ;
    pnFcPerBc_lidx[kBcP] = mPerFcPairs ;
  }

  ulong_t *pnElPer = arr_malloc ( "pnElPer in h5w_per", pUns->pFam,
                                  2*mPerFcPairs, sizeof( *pnElPer ) ) ;
  int *pnFcPer = arr_malloc ( "pnFcPer in h5w_per", pUns->pFam,
                              2*mPerFcPairs, sizeof( *pnFcPer ) ) ;

  ulong_t *pnE0 = pnElPer, *pnE1 = pnElPer+2*mPerFcPairs-1 ;
  int *pnF0 = pnFcPer, *pnF1 = pnFcPer+2*mPerFcPairs-1 ;
  matchFc_struct *pPFc ;
  int kFc ;
  for ( kBcP = 0 ; kBcP < mPBcP ; kBcP++ ) {
    pPFc = pUns->pPerBc[kBcP].pPerFc ;
    for ( kFc = 0 ; kFc < pUns->pPerBc[kBcP].mPerFcPairs ; kFc++ ) {
      *pnE0++ = pPFc->pElem0->number ;
      *pnF0++ = pPFc->nFace0 ;
      *pnE1-- = pPFc->pElem1->number ;
      *pnF1-- = pPFc->nFace1 ;
      pPFc++ ;
    }

    if ( pUns->pPerBc[kBcP].mPerFcPairs != pPFc - pUns->pPerBc[kBcP].pPerFc ) {
      sprintf ( hip_msg,  "for periodic bc %s, expected %d faces, found %"FMT_ULG" in h5w_per.",
                pUns->pPerBc[kBcP].pBc[0]->text,
                pUns->pPerBc[kBcP].mPerFcPairs, pPFc - pUns->pPerBc[kBcP].pPerFc ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }



  /* Complete the lidx symmetrically. */
  ulong_t mFcPBc ;
  for ( kBcP = 0 ; kBcP < mPBcP ; kBcP++ ) {
    mFcPBc = ( kBcP < mPBcP-1 ?
               pnFcPerBc_lidx[ mPBcP-kBcP-1 ]- pnFcPerBc_lidx[ mPBcP-kBcP-2 ] :
               pnFcPerBc_lidx[ 0 ] ) ;
    pnFcPerBc_lidx[ mPBcP+kBcP ] = pnFcPerBc_lidx[ mPBcP+kBcP-1 ] + mFcPBc ;
  }





    
  hid_t       grp_id;   /* identifier */
  herr_t      status;
  grp_id = H5Gcreate(file_id, "Periodicity", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

  /* Periodic patch pairs. */
  h5_write_ulg ( grp_id, 0, "periodic_patch", 2*mPBcP, pnBcPairs ) ;

  if ( foundRot ) {
    h5_write_dbl ( grp_id, 0, "periodic_angle", mPBcP, pBcRotAngle ) ;
    h5_write_int ( grp_id, 0, "periodic_axis", mPBcP, piBcRotAxis ) ;
  }
  else {
    /* Must be trans. */
    h5_write_dbl ( grp_id, 0, "periodic_shift", mPBcP*pUns->mDim, pBcShift ) ;
  }


  h5_write_ulg ( grp_id, 0, "periodic_node", 2*mPVP, pnVxPer ) ;
  h5_write_ulg ( grp_id, 0, "periodic_node_lidx", 2*mPBcP, pnVxPerBc_lidx ) ;
  ulong_t umPBcP = mPBcP ;
  ulidx2fidx( pnVxPerBc_lidx, 2*umPBcP, pnVxPerBc_lidx ) ;
  h5_write_ulg ( grp_id, 0, "periodic_node_fidx", 2*mPBcP+1, pnVxPerBc_lidx ) ;



  
  
  h5_write_ulg ( grp_id, 0, "periodic_face_element", 2*mPerFcPairs, pnElPer ) ;
  h5_write_int ( grp_id, 0, "periodic_face_nface", 2*mPerFcPairs, pnFcPer ) ;
  h5_write_ulg ( grp_id, 0, "periodic_face_lidx", 2*mPBcP, pnFcPerBc_lidx ) ;
  ulidx2fidx( pnFcPerBc_lidx, 2*umPBcP, pnFcPerBc_lidx ) ;
  h5_write_ulg ( grp_id, 0, "periodic_face_fidx", 2*mPBcP+1, pnFcPerBc_lidx ) ;
  


  /* Close periodicity. */
  status = H5Gclose(grp_id);


  arr_free ( pnVxPerBc_lidx ) ;
  arr_free ( pnBcPairs ) ;
  arr_free ( pnVxPer ) ;
  arr_free ( pBcRotAngle ) ;
  arr_free ( piBcRotAxis ) ;

  arr_free ( pnFcPerBc_lidx ) ;
  arr_free ( pnElPer ) ;
  arr_free ( pnFcPer ) ;


  return ( 1 ) ;
}


/******************************************************************************

  h5w_elGraph:
  Add a Metis elGraph to the mesh file.

  Last update:
  ------------
  3Oct19; issue warning for i32 but continue to write elGraph.
  22Dec14; elGraph fails with 64 bit int (USE_ULONG), bypass.
  6Apr13: promote possibly large unsigned ints to ulong_t
  18May08; fix stride to list the second element of each graph edge.
  18Apr08; fix bugs with missing final element in xAdj and stride in adjcny.
  28Jun07: conceived.

  Input:
  ------
  pUns
  file_id

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5w_elGraph ( uns_s *pUns, hid_t file_id ) {

  hid_t       grp_id;   /* identifier */
  herr_t      status;

  ulong_t mXAdj, *pXAdj, mAdjncy, *pAdjncy, i ;

#ifdef HIP_USE_ULONG
  // fixed now.
  //hip_err ( warning, 1, "Use a 32bit integer version to write the elGraph." );
  //return ( 1 ) ;
#endif


  make_elGraph ( pUns, &mXAdj, &pXAdj, &mAdjncy, &pAdjncy ) ;


  grp_id = H5Gcreate(file_id, "ElGraph", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
  h5_write_ulg ( grp_id, 0, "xAdj", mXAdj+1, pXAdj ) ;

  /* pAdjncy lists the two elements for each graph edge in lexicographic fashion,
     as we know the first element from the index pXAdj, we only want to write the
     second one.
     This code does not seem to work.
     h5_write_int_stride ( grp_id, 0, "adjcny", mAdjncy, 2, pAdjncy+1 ) ;
     Hence repack by hand to list only every second entry and
     write as a contiguous block. */
  for ( i = 0 ; i < mAdjncy ; i++ )
    pAdjncy[i] = pAdjncy[2*i+1] ;
  h5_write_ulg ( grp_id, 0, "adjncy", mAdjncy, pAdjncy ) ;



  status = H5Gclose(grp_id);

  arr_free ( pAdjncy ) ;
  arr_free ( pXAdj ) ;

  return ( 1 ) ;
}

/******************************************************************************
  h5w_vx2el:   */

/*! build, write and destroy vx to elem connectivity.
 */

/*

  Last update:
  ------------
  5May20; write out also a fidx, correct alloc size of lidx. Dealloc arrays.
  30Jun14: conceived.


  Input:
  ------
  pUns = grid
  file_id = opened hdf database file

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_vx2El ( uns_s *pUns, hid_t file_id ) {

  llToElem_s *pllVxToElem = make_vxToElem ( pUns ) ;

  /* Make a list of vx to element pointers. */
  ulong_t mVx = pUns->mVertsAlloc ;
  ulong_t mVx2ElAlloc = ( pUns->mDim == 2 ? 2*mVx : 6*mVx ) ;
  ulong_t *vx2El = arr_malloc ( "vx2El in h5w_vx2el", pUns->pFam,
                                mVx2ElAlloc, sizeof( *vx2El ) ) ;

  /* Make an index. */
  ulong_t *lidx = arr_malloc ( "lidx in h5w_vx2el", pUns->pFam,
                               mVx+2, sizeof( *lidx ) ) ;
  /* have the lidx reflect the fact that the vertices are numbered from 1,
     the first slot is empty. The list written to hdf starts with the
     first vertex. */
  lidx[0] = 0 ;

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd, elNoInt ;
  ulong_t nr ;
  ulong_t nItem ;
  elem_struct *pElem ;
  ulong_t mVx2El=0 ;
  mVx = 0 ; // recount and compare.
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( ( nr = pVrtx->number ) ) {
        /* Valid Vx. List all elements it forms. */
        mVx++ ;
        lidx[nr] = lidx[nr-1] ;

        nItem = 0 ;
        while ( loop_toElem ( pllVxToElem, pVrtx->number, &nItem, &pElem ) ) {
          /* Add this element to this vertex. */

          /* Space left? */
          if ( lidx[nr] >= mVx2ElAlloc ) {
            mVx2ElAlloc = mVx2El*REALLOC_FACTOR+1 ;
            vx2El = arr_realloc( "vx2El in h5w_vx2el", pUns->pFam,
                                 vx2El, mVx2ElAlloc, sizeof( *vx2El ) ) ;
          }

          /* Add the element. */
          elNoInt = (int) pElem->number ;
          if ( elNoInt |=  pElem->number )
            hip_err ( fatal, 0, "integer size for elem no exceeded in h5w_vx2el");
          vx2El[ lidx[nr] ] = elNoInt ;

          /* Increment the lidx. */
          mVx2El = ++lidx[nr] ;
        }
      }


  if ( mVx != pUns->mVertsNumbered ) {
    sprintf ( hip_msg, "expected %"FMT_ULG" nodes, found %"FMT_ULG" in h5w_vx2El.",
              pUns->mVertsNumbered, mVx ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  hid_t grp_id = h5_open_group( file_id, "Connectivity" ) ;

  char nodeLbl[LINE_LEN] ;
  sprintf ( nodeLbl, "node->element" ) ;
  h5_write_ulg ( grp_id, 0, nodeLbl, mVx2El, vx2El ) ;

  /* Skip the zero/empty vertex position. */
  h5_write_ulg ( grp_id, 0,  "node->element_lidx", mVx, lidx+1 ) ;
  ulong_t umVx = mVx ;
  ulidx2fidx ( lidx+1, umVx, lidx+1 ) ;
  h5_write_ulg ( grp_id, 0, "node->element_fidx", mVx+1, lidx+1 ) ;


  arr_free ( vx2El ) ;
  arr_free ( lidx ) ;

  return ( 0 ) ;
}


/******************************************************************************
  h5w_faces:   */

/*! write a list of all element faces to hdf.
 */

/*

  Last update:
  ------------
  25Feb20; new interface to make_llfc
  12May17; change purpose line to distinguish from interface faces.
  19Sep16; use get_used_sizeof_llEnt.
  6Apr13: promote possibly large unsigned ints to ulong_t
  1Apr12: conceived.


  Input:
  ------
  pUns: grid to write
  file_id: opened hdf5 database.

*/

void h5w_faces ( uns_s *pUns, hid_t file_id ) {


  /* Make a list of faces, set doBndFc=0, only internal faces. */
  fc2el_s *pfc2el ;
  ulong_t mFcBecomeInt=0, mIntFcDupl=0, mBndFcDupl=0, mFcRemoved=0, mFcUnMtch=0 ;
  llVxEnt_s *pllVxFc = make_llFc ( pUns, bnd, &pfc2el,
                                   doWarn.bndFc, doRemove.bndFc, 0,
                                   &mFcBecomeInt, &mIntFcDupl, &mBndFcDupl ) ;



  /* Count per type. */
  ulong_t mFc = get_used_sizeof_llEnt ( pllVxFc ) ;
  elem_struct *pEl0, *pEl1 ;
  int nFc0, nFc1 ;
  const faceOfElem_struct *pFoE ;
  int mVxFc ;
  ulong_t mFcVx[5] = { 0,0,0,0,0 } ; /* Number of faces with that many nodes. */
  ulong_t nEnt ;
  for ( nEnt = 1 ; nEnt <= mFc ; nEnt++ ) {
    if ( !show_fc2el_elel ( pfc2el, nEnt, &pEl0, &nFc0, &pEl1, &nFc1 ) ) {
      /* Properly matched internal face */
      pFoE = elemType[ pEl0->elType ].faceOfElem + nFc0 ;
      mVxFc = pFoE->mVertsFace ;
      mFcVx[ mVxFc ]++ ;
    }
  }



  /* Alloc for the largest size, reuse for each type. */
  ulong_t mFcMax = 0 ;
  for ( mVxFc = 2 ; mVxFc < 5 ; mVxFc++ )
    mFcMax = MAX( mFcMax, mFcVx[ mVxFc ] ) ;

  ulong_t *pFc2El ;
  ulong_t *pEl2Fc ;
  pFc2El = arr_malloc ( "pFc2El in h5w_face", pUns->pFam,  2*mFcMax, sizeof( *pFc2El ) ) ;
  pEl2Fc = arr_malloc ( "pEl2Fc in h5w_face", pUns->pFam,  2*mFcMax, sizeof( *pEl2Fc ) ) ;


  hid_t grp_id ;
  grp_id = H5Gcreate(file_id, "Faces", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;


  ulong_t *pFe, *pEf ;
  ulong_t mFcWritten ;
  char faceLbl[][3] = { "0", "1", "bi", "tri", "qua" } ;
  char nodeLbl[LINE_LEN] ;
  for ( mVxFc = 2 ; mVxFc < 5 ; mVxFc++ ) {
    /* Pack faces into linear arrays. */
    pFe = pFc2El ;
    pEf = pEl2Fc ;
    mFcWritten = 0 ;

    for ( nEnt = 1 ; nEnt <= mFc ; nEnt++ ) {
      if ( !show_fc2el_elel ( pfc2el, nEnt, &pEl0, &nFc0, &pEl1, &nFc1 ) ) {
        /* Properly matched internal face. */
        pFoE = elemType[ pEl0->elType ].faceOfElem + nFc0 ;

        if ( mVxFc == pFoE->mVertsFace ) {
          /* Right number of forming ndoes, list this face. */
          mFcWritten++ ;

          /* Two adjacent elements. */
          *pFe++ = pEl0->number ;
          *pFe++ = pEl1->number ;
          /* And the canonical face number in these elements. */
          *pEf++ = nFc0 ;
          *pEf++ = nFc1 ;
        }
      }
    }

    if ( mFcWritten != mFcVx[mVxFc] ) {
      sprintf ( hip_msg, "in h5w_face, expected %"FMT_ULG" %d-noded faces,"
                " found %"FMT_ULG".\n",
                mFcVx[mVxFc], mVxFc, mFcWritten ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    /* Write per type. */
    if ( mFcWritten ) {
      /* Elements. */
      sprintf ( nodeLbl, "int_%s->elem", faceLbl[mVxFc] ) ;
      h5_write_ulg ( grp_id, 0, nodeLbl,  2*mFcVx[mVxFc],   pFc2El ) ;
      /* Faces. */
      sprintf ( nodeLbl, "int_%s->face", faceLbl[mVxFc] ) ;
      h5_write_ulg ( grp_id, 0, nodeLbl,  2*mFcVx[mVxFc],   pEl2Fc ) ;
    }
  }

  /* Free. */
  arr_free ( pFc2El ) ;
  arr_free ( pEl2Fc ) ;

  free_llEnt ( &pllVxFc ) ;
  arr_free ( pfc2el ) ;

  return ;
}

/******************************************************************************
  h5w_ascii_keys:   */

/*! Write a list of bc names/tags.
 */

/*

  Last update:
  ------------
  28Feb18: rename rotAngle to rotAngleRad to clarify meaning.
  9Jul15; ad trailing d0 to the rotAngle.
  Jan/Feb 14; minor changes in text/label formatting, GS.
  13Dec13: copied from write_avbp_asciiBound_4p7


  Input:
  ------
  pUns: grid
  bcFileName: name of file to write to.

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_ascii_key ( uns_s *pUns, char *pBcFile ) {

  const bc_struct *pBc ;

  FILE *boundFile ;
  int nBc ;

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "bc tags to %s", pBcFile ) ;
    hip_err ( info, 1, hip_msg ) ;
  }

  if ( ( boundFile = fopen ( pBcFile, "w" ) ) == NULL ) {
    sprintf ( hip_msg, "file: %s could not be opened.\n", pBcFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; }


  /* header. */
  fprintf ( boundFile, "$BOUNDARY-PATCHES\n"
            "!------------------------------------------------------\n" ) ;

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBc = pUns->ppBc[nBc] ;
    fprintf ( boundFile, "patch_name = %s\n", pBc->text ) ;

    switch ( pBc->type[0] ) {
    case 'l' :
      /* lower periodic */
      if ( pUns->ppBc[nBc]->pPerBc->rotAngleRad == 0. ){
        /* Translation. */
        fprintf ( boundFile, "boundary_condition = NO_BOUNDARY\n" ) ;
      }
      else {
        /* Rotation. */
        fprintf ( boundFile, "boundary_condition = PERIODIC_AXI\n" ) ;
        fprintf ( boundFile, "periodic_axi_sign = negative\n" ) ;
        fprintf ( boundFile, "periodic_axi_angle=%20.15fd0\n",
                  pUns->ppBc[nBc]->pPerBc->rotAngleRad/PI*180. ) ;
      }
      break ;
    case 'u' :
      /* lower periodic */
      if ( pUns->ppBc[nBc]->pPerBc->rotAngleRad == 0. ){
        /* Translation. */
        fprintf ( boundFile, "boundary_condition = NO_BOUNDARY\n" ) ;
      }
      else {
        /* Rotation. */
        fprintf ( boundFile, "boundary_condition = PERIODIC_AXI\n" ) ;
        fprintf ( boundFile, "periodic_axi_sign = positive\n" ) ;
        fprintf ( boundFile, "periodic_axi_angle=%20.15fd0\n",
                  pUns->ppBc[nBc]->pPerBc->rotAngleRad/PI*180. ) ;
      }
      break ;
    default  :
      fprintf ( boundFile, "boundary_condition = TO_BE_DEFINED\n" ) ;
    }
    fprintf ( boundFile,
              "!------------------------------------------------------\n" ) ;
  }

  /* Footer.  */
  fprintf ( boundFile, "$end_BOUNDARY-PATCHES\n" ) ;

  fclose ( boundFile ) ;
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_coarse_grid_conn:
*/
/*! write inter-grid connectivity to hdf.
 *
 */

/*

  Last update:
  ------------
  14jul17: conceived.


  Input:
  ------
  pUns
  file_id: file/group to write to.

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_coarse_grid_conn ( uns_s *pUns, hid_t file_id ) {

  hid_t grp_id = H5Gcreate(file_id, "MultiGrid", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

  ulong_t *vx2cEl, *uBuf ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  elem_struct **ppElC = pUns->ppElContain ;
  if ( pUns->ppElContain ) {
    uBuf = vx2cEl = arr_malloc ( "vx2cEl in h5w_coarse_grid_conn",
                                 pUns->pFam, pUns->mVertsNumbered,
                                 sizeof( *vx2cEl ) ) ;

    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( pVrtx->number ) {
          *uBuf++ = (*ppElC)->number ;
          ppElC++ ;
        }

    if ( ppElC - pUns->ppElContain != pUns->mVertsNumbered ) {
      sprintf ( hip_msg, "expected %"FMT_ULG", found %td fine grid nodes.",
                pUns->mVertsNumbered, ppElC - pUns->ppElContain ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }


    h5_write_ulg ( grp_id, 0, "node->coarse_elem",
                   pUns->mVertsNumbered, vx2cEl ) ;

    arr_free ( vx2cEl ) ;
  }



  double *dBuf, *vxWt ;
  if ( pUns->pElContainVxWt ) {
    h5_write_dbl ( grp_id, 0, "node->coarse_elem_vx_wt",
                   MAX_VX_ELEM*pUns->mVertsNumbered, pUns->pElContainVxWt ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_sliding_plane:
*/
/*! Write interpolation lines of a sliding plane to hdf.
 *
 */

/*

  Last update:
  ------------
  2Jun20; rename geoType to more uniquely spGeoType.
  5May20; write fidx.
  28Apr20; support different numbers of lines on either side.
  9Apr20; replace call to one_fxstr with fxStr and dim=1,
  reading one_fxStr fails for some reason.
  10Dec19: conceived.


  Input:
  ------
  pUns: grid
  file_id: hdf file

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s h5w_sliding_plane ( const uns_s *pUns, hid_t file_id ) {
  ret_s ret = ret_success () ;
  extern const char sp_geo_type_string[][MINITXT_LEN] ;

  if ( !pUns->mSlidingPlanePairs )
    // nothing to write )
    return ( ret ) ;

  hid_t status ;
  hid_t grp_sp_id = H5Gcreate(file_id, "SlidingPlane", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

  int kP ;
  char someChar[TEXT_LEN] ;
  slidingPlanePair_s *pSpP ;
  slidingPlaneSide_s *pSpS[2] ;
  for ( kP = 0 ; kP < pUns->mSlidingPlanePairs ; kP++ ) {
    pSpP = pUns->pSlidingPlanePair + kP ;
    pSpS[0] = pSpP->side[0] ;
    pSpS[1] = pSpP->side[1] ;

    if ( !pSpS[0]->mLines || !pSpS[1]->mLines ) {
      ret = hip_err ( warning, 1, "incomplete sliding plane setup, omitted." ) ;
      return ( ret ) ;
    }


    sprintf ( someChar, "%d", kP+1 ) ;
    hid_t grp_id = H5Gcreate(grp_sp_id, someChar, H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;


    /* Name. */
    h5_write_fxStr ( grp_id, "name", 1, fxStr240, pSpS[0]->name ) ;
    /* Patch numbers. */
    int kSide, nBc[2] ;
    for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
      nBc[kSide] = find_nBc ( pUns, pSpS[kSide]->pBc )+1 ; // hdf indexes from 1.
    }
    h5_write_int ( grp_id, 0, "nBc", 2, nBc ) ;

    /* Geometric type. */
    h5_write_fxStr ( grp_id, "geoType", 1, fxStr240,
                     (char *) sp_geo_type_string[ pSpS[0]->spGeoType ] ) ;

    /* Number of sliding/mixing lines. */
    int mLines[2] ;
    mLines[0] = pSpS[0]->mLines ;
    mLines[1] = pSpS[1]->mLines ;
    h5_write_int ( grp_id, 0, "mLines", 2, mLines ) ;


    /* heights, radii.*/
    int mLinesPair = mLines[0]+mLines[1] ;
    double *rh = arr_malloc ( "rh in h5w_slidingPlane", pUns->pFam, mLinesPair,
                              sizeof( *rh ) ) ;
    memcpy ( rh,           pSpS[0]->prh, mLines[0]*sizeof( *rh ) ) ;
    memcpy ( rh+mLines[0], pSpS[1]->prh, mLines[1]*sizeof( *rh ) ) ;
    h5_write_dbl ( grp_id, 0, "rh",  mLinesPair, rh ) ;
    arr_free ( rh ) ;



    /* Make a lidx. Pack both sides into one, hence 2*mLines. */
    int *lidx = arr_malloc ( "lidx in h5w_slidingPlane", pUns->pFam, mLinesPair+1,
                             sizeof( *lidx ) ) ;

    int kLine ;
    int mEgX = 0 ;
    int *pLidx = lidx ;
    for ( kSide = 0 ; kSide < 2 ; kSide ++ ) {
      for ( kLine = 0 ; kLine < mLines[kSide] ; kLine++ ) {
        mEgX += pSpS[kSide]->pspLine[kLine].mEgX ;
        *pLidx++ = mEgX ;
      }
    }

    h5_write_int ( grp_id, 0, "edge_lidx", mLinesPair, lidx ) ;
    ilidx2fidx ( lidx, mLinesPair, lidx ) ;
    h5_write_int ( grp_id, 0, "edge_fidx", mLinesPair+1, lidx ) ;



    /* List of edges and their data. */
    int *eg2node = arr_malloc ( "eg2node in h5w_sliding_plane", pUns->pFam, 2*mEgX,
                                sizeof( *eg2node ) ) ;
    double *eg2wt = arr_malloc ( "eg2wt in h5w_sliding_plane", pUns->pFam, 2*mEgX,
                                 sizeof( *eg2wt ) ) ;
    double *eg2arc = arr_malloc ( "eg2arc in h5w_sliding_plane", pUns->pFam, mEgX,
                                  sizeof( *eg2arc ) ) ;

    pLidx = lidx ;
    spLineX_s *pspL ;
    int *pEg2node = eg2node ;
    double *pEg2wt = eg2wt ;
    double *pEg2arc = eg2arc ;
    egX_s *pEgX ;
    for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
      for ( kLine = 0 ; kLine < mLines[kSide] ; kLine++ ) {
        pspL = pSpS[kSide]->pspLine + kLine ;
        for ( pEgX = pspL->egX ; pEgX < pspL->egX + pspL->mEgX ; pEgX++ ) {
          *pEg2node++ = pEgX->pVx[0]->number ;
          *pEg2node++ = pEgX->pVx[1]->number ;
          *pEg2wt++ = pEgX->wt[0] ;
          *pEg2wt++ = pEgX->wt[1] ;
          *pEg2arc++ = pEgX->t ;
        }
      }
    }

    /* Check counting. */
    if ( pEg2node - eg2node != 2*mEgX ||
         pEg2wt - eg2wt != 2*mEgX ||
         pEg2arc - eg2arc != mEgX ) {
      ret = hip_err ( fatal, 0, "miscount of intersection edges in h52_sliding_plane." ) ;
      return ( ret ) ;
    }

    h5_write_int ( grp_id, 0, "edge->node", 2*mEgX, eg2node ) ;
    h5_write_dbl ( grp_id, 0, "edge->weight", 2*mEgX, eg2wt ) ;
    h5_write_dbl ( grp_id, 0, "edge->arclen", mEgX, eg2arc ) ;


    arr_free ( lidx ) ;
    arr_free ( eg2node ) ;
    arr_free ( eg2wt ) ;
    arr_free ( eg2arc ) ;


    // Calculate Interpolation weights for vertices between lines.
    sp_calc_vx_weight_mixing_lines ( (uns_s *) pUns, pSpP ) ;
    /* Write a single record for both sides. */
    ulong_t mVxMPBoth = pSpP->side[0]->mVxMP +  pSpP->side[1]->mVxMP ;
    ulong_t *pnVxMP2nVx = arr_malloc ( "vxMP", pUns->pFam, mVxMPBoth, sizeof( *pnVxMP2nVx ) ) ;
    ulong_t *pnVxMP2lineLower = arr_malloc ( "vxMP", pUns->pFam, mVxMPBoth, sizeof( *pnVxMP2lineLower ) ) ;
    double *pwtnVxMPlineLower = arr_malloc ( "vxMP", pUns->pFam, mVxMPBoth, sizeof( *pwtnVxMPlineLower ) ) ;

    ulong_t mVxMP[2] = {0} ;
    int nVxWr = 0 ;
    for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
      mVxMP[kSide] = pSpP->side[kSide]->mVxMP ;
      memcpy ( pnVxMP2nVx + nVxWr, pSpP->side[kSide]->pnVxMP2nVx, mVxMP[kSide]*sizeof(*pnVxMP2nVx) ) ;
      memcpy ( pnVxMP2lineLower + nVxWr, pSpP->side[kSide]->pnVxMP2lineLower, mVxMP[kSide]*sizeof(*pnVxMP2lineLower) ) ;
      memcpy ( pwtnVxMPlineLower +nVxWr, pSpP->side[kSide]->pwtnVxMPlineLower, mVxMP[kSide]*sizeof(*pwtnVxMPlineLower) ) ;
      nVxWr += mVxMP[kSide] ;
    }

    // Write
    h5_write_ulg ( grp_id, 0, "mVxMP", 2, mVxMP ) ;
    h5_write_ulg ( grp_id, 0, "nVxMP->node", nVxWr, pnVxMP2nVx ) ;
    h5_write_ulg ( grp_id, 0, "nVxMP->line", nVxWr, pnVxMP2lineLower ) ;
    h5_write_dbl ( grp_id, 0, "nVxMP->weight", nVxWr, pwtnVxMPlineLower ) ;

    // dealloc
    arr_free ( pnVxMP2nVx ) ;
    arr_free ( pnVxMP2lineLower ) ;
    arr_free ( pwtnVxMPlineLower ) ;

    // remove from sliding plane datastructure.
    sp_free_vx_weight_mixing_lines ( pUns, pSpP ) ;

    status = H5Gclose(grp_id);
  }

  status = H5Gclose(grp_sp_id);

  return ( ret ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_grid_level:
*/
/*! Add a grid level to an hdf file.
 *
 */

/*

  Last update:
  ------------
  20aug19; move writing of bnd conn to write_hdf5_grid.
  1jul19; call h5w_bnd_faces for bndAndInter type.
  6Sep18; don't call special_verts in dump mode.
  14jul17: extracted from h5w_grid.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5w_grid_level ( uns_s *pUns, int nLvl,
                     hid_t file_id, FILE *Fxmf,
                     const char *meshFileNmH5, const char * solFileNmH5 ) {

  if ( nLvl != 0 )
    Fxmf = NULL ;


  /* Write volume mesh to xmf. */
  h5w_volGridXmf( pUns, Fxmf, meshFileNmH5, solFileNmH5 ) ;



  ulong_t mNegVols ;
  if ( !h5w_flag_noVol ) {
    /* Note: Renumbering now done in h5w_grid for all mg levels.
       Renumber elements by number of vertices (equivalent to type, as there
       is no adaptation with hanging nodes, yet, for hdf5.
       pUns->numberedType = invNum ;
       number_uns_elems_by_type ( pUns, leaf, tri, hex, 1 ) ; */

    if ( !h5w_flag_dump && !special_verts ( pUns ) ) {
      sprintf ( hip_msg, "failed to match periodic vertices in write_uns_hdf5.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
      return ( 0 ) ;
    }

    /* Update mesh sizes. */
    if ( !h5w_flag_dump )
      update_h_vol ( pUns, &mNegVols ) ;

    /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
       for the current numbering of elements. */
    count_uns_bndFaces ( pUns ) ;


    if ( verbosity > 3 ) {
      sprintf ( hip_msg, "        writing level %d to hdf5.", nLvl ) ;
      hip_err ( blank, 3, hip_msg ) ;
    }


    if ( nLvl == 0 )
      h5w_hdr  ( pUns, file_id ) ;

    const int compress = 0 ; // Dpm't compress coordinates, not effective?
    const ulong_t mVxWritten = 0 ;
    h5w_coor ( pUns, compress, file_id, mVxWritten ) ;

    if ( h5w_flag_vrtxVol )
      h5w_vxData ( pUns, compress, file_id) ;



    h5w_bnd  ( pUns, file_id, bndAndInter, Fxmf, meshFileNmH5, solFileNmH5 ) ;

    if ( Fxmf )
      fclose(Fxmf);

    const ulong_t mElOfTypeWritten[MAX_ELEM_TYPES] = {0} ;
    h5w_conn ( pUns, file_id, mElOfTypeWritten ) ;
    if ( h5w_flag_vx2El )
      h5w_vx2El ( pUns, file_id ) ;
    if ( h5w_flag_zones ) {
      int is_sol = 0 ;
      h5w_zone  ( pUns, file_id, is_sol ) ;
    }

    if ( !h5w_flag_dump )
      h5w_per  ( pUns, file_id ) ;
  }

  if ( h5w_flag_edge )
    h5w_elGraph ( pUns, file_id ) ;

  if ( h5w_flag_face )
    h5w_faces ( pUns, file_id ) ;

  if ( pUns->mSlidingPlanePairs )
    h5w_sliding_plane ( pUns, file_id ) ;

  if ( pUns->pUnsCoarse )
    h5w_coarse_grid_conn ( pUns, file_id ) ;

  return ( 0 ) ;
}


#ifdef ADAPT_HIERARCHIC

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_refType:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  : conceived.


  Input:
  ------
  pUns: grid
  file_id: opened group in hdf file
  mElTerm: number of terminal elements already written.

  Changes To:
  -----------

  Output:
  -------
  *pmChildEl: number of children elements added in this call.

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s h5w_refType ( uns_s *pUns, const hid_t hier_id, const ulong_t mElTerm,
                    ulong_t *pmChildEl ) {
#undef FUNLOC
#define FUNLOC "in h5w_refType"

  ret_s ret = ret_success () ;

  ulong_t mParentEl =  pUns->mElemsNumbered-mElTerm ;
  int *pnRefType = arr_malloc ( "nRefType "FUNLOC".",pUns->pFam,
                              mParentEl, sizeof ( *pnRefType ) ) ;
  int *pnRT = pnRefType ;


  chunk_struct *pChunk = NULL ;
  elem_struct *pElBeg, *pElem, *pElEnd ;
  const refType_struct *pRefT ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      if ( pElem->number && pElem->number > mElTerm ) {
        /* This is a valid non-terminal element, hence a parent. */
        pRefT = pElem->PrefType ;
        if ( !pRefT )
          hip_err ( fatal, 0, "this element should have been refined "FUNLOC"." ) ;
        else if ( pRefT->refOrBuf == buf ) {
          /* Buffered, ref type no is -mChildren. */
          *pnRT++ = -pRefT->mChildren ;
          *pmChildEl += pRefT->mChildren ;
        }
        else {
          *pnRT = pRefT->nr ;
          *pmChildEl += pRefT->mChildren ;
        }
      }
    } // for ( pElem = pElBeg ;

  if ( ( pnRT - pnRefType ) >= mParentEl ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", found %"FMT_ULG" parent elements "FUNLOC".",
              mParentEl, ( pnRT - pnRefType ) ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  h5_write_int ( hier_id, 0, "parentElem->refType", mParentEl, pnRefType ) ;

  arr_free ( pnRefType ) ;
  return ( ret ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_childElem:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  22jul20: conceived.


  Input:
  ------
  pUns: grid
  hier_id: opened hdf section to add to
  mElTerm: number of terminal/leaf elements already written
  mChildEl: number of parent to child pointers to write

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s h5w_childElem ( uns_s *pUns, const hid_t hier_id, const ulong_t mElTerm,
                      const ulong_t mChildEl ) {
#undef FUNLOC
#define FUNLOC "in h5w_childElem"

  ret_s ret = ret_success () ;

  ulong_t *pnChildEl =  arr_malloc ( "pnChildEl "FUNLOC".",pUns->pFam,
                                   mChildEl, sizeof ( *pnChildEl ) ) ;
  ulong_t *pnCh = pnChildEl ;

  ulong_t mElemParent = pUns->mElemsNumbered - mElTerm ;
  ulong_t *pidxChild =  arr_malloc ( "pidxChild "FUNLOC".",pUns->pFam,
                                  mElemParent+1, sizeof ( *pnChildEl ) ) ;
  ulong_t *piCh = pidxChild ;


  chunk_struct *pChunk = NULL ;
  elem_struct *pElBeg, *pElem, *pElEnd ;
  const refType_struct *pRefT ;
  int kCh ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      if ( pElem->number && pElem->number > mElTerm ) {
        /* This is a valid non-terminal element, hence a parent. */
        pRefT = pElem->PrefType ;
        if ( !pRefT )
          hip_err ( fatal, 0, "this element should have been refined "FUNLOC"." ) ;
        else {
          for ( kCh = 0 ; kCh <  pRefT->mChildren ; kCh++ ) {
            *pnCh++ = pElem->PPchild[kCh]->number ;
          }
          *piCh++ = pnCh - pnChildEl ; // index counts first elem as 1
        }
      }
    }
  }


  if ( ( pnCh - pnChildEl ) > mChildEl ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", found %"FMT_ULG" children elements "FUNLOC".",
              mChildEl, ( pnCh - pnChildEl ) ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
 if ( ( piCh - pidxChild ) > mElemParent ) {
    sprintf ( hip_msg, "found %"FMT_ULG", expected %"FMT_ULG" parent elements "FUNLOC".",
              ( piCh - pidxChild ), mElemParent ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  h5_write_ulg ( hier_id, 0, "parentElem->child", mChildEl, pnChildEl ) ;

  h5_write_ulg ( hier_id, 0, "parentElem->child_lidx", mElemParent, pidxChild ) ;
  //ulidx2fidx ( pidxChild, mElemParent, pidxChild ) ;
  //h5_write_ulg ( hier_id, 0, "parentElem->child_fidx", mElemParent+1, pidxChild ) ;

  arr_free ( pnChildEl ) ;
  arr_free ( pidxChild ) ;
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_adEdge:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  20jul20: derived from write_adEdge.


  Input:
  ------
  pUns: grid to write
  grp_id: opened hierarchical section.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s h5w_adEdge ( uns_s *pUns, const hid_t grp_id ) {
#undef FUNLOC
#define FUNLOC "in h5w_adEdge"

  ret_s ret = ret_success () ;


  /* How many are there. */
  const llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  ulong_t mEgUsed = 0, mEdges = 0 ;
  if ( pllAdEdge )
    mEgUsed = get_number_of_edges ( pllAdEdge, &mEdges ) ;
  else
    /* No edges to write. */
    return ( ret ) ;



  ulong_t *puBuf =  arr_malloc ( "puBuf "FUNLOC".",pUns->pFam,
                                   mEgUsed*3, sizeof ( *puBuf ) ) ;
  ulong_t *puB = puBuf ;



  /* List the three vertices for each edge. */
  int nEg ;
  vrtx_struct *pVx0, *pVx1, *pVxMid ;
  const adEdge_s *pAe, *pAdEdge = pUns->pAdEdge ;
  for ( nEg = 1 ; nEg <= mEdges ; nEg++ )
    if ( show_edge ( pllAdEdge, nEg, &pVx0, &pVx1 ) ) {
      /* Active edge. */
      *puB++ = pVx0->number ;
      *puB++ = pVx1->number ;

      /* Mid vertex. */
      pAe = pAdEdge + nEg ;
      if ( pAe->cpVxMid.nr ) {
        /* There is a mid vertex. */
        pVxMid = de_cptVx ( pUns, pAe->cpVxMid ) ;
        *puB++ = pVxMid->number ;
      }
      else
        /* Just an edge. Can we have unfilled adEdges? */
        *puB++ = 0 ;
    }


  if ( ( puB - puBuf ) > mEgUsed*3 ) {
    sprintf ( hip_msg, "found %"FMT_ULG", expected %"FMT_ULG" adapted edges "FUNLOC".",
              ( puB - puBuf ), mEgUsed ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  h5_write_ulg ( grp_id, 0, "adEdge", mEgUsed, puBuf ) ;


  arr_free ( puBuf ) ;

  return ( ret ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5w_grid_hierarchy:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  20jul20: conceived.


  Input:
  ------
  pUns: grid
  file_id: opened group in hdf file
  mElTerm: number of terminal elements already written.
  mChildEl: number of children elements to write.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s h5w_grid_hierarchy ( uns_s *pUns, const hid_t file_id ) {
#undef FUNLOC
#define FUNLOC "in h5w_grid_hierarchy"

  ret_s ret = ret_success () ;

  chunk_struct *pChunk = NULL ;
  ulong_t mElem2ChildPUsed = 0 ;
  while ( loop_chunks ( pUns, &pChunk ) )
    mElem2ChildPUsed += pChunk->mElem2ChildPUsed ;

  if ( !mElem2ChildPUsed )
    // no adaptation present, done.
    return ( ret ) ;


  /* Renumber for parents. h52_conn renumbers, but needs mElemsOfType. */
  const ulong_t mVxLeaf = pUns->mVertsNumbered ;
  const ulong_t mElemLeaf = pUns->mElemsNumbered ;
  ulong_t mElOfTypeLeaf[MAX_ELEM_TYPES] ;
  memcpy ( mElOfTypeLeaf, pUns->mElemsOfType,
           MAX_ELEM_TYPES*sizeof( *mElOfTypeLeaf) ) ;
  ulong_t mFcBndLeaf[MAX_VX_FACE+1] ;
  mFcBndLeaf[2] = pUns->mBiAllBc ;
  mFcBndLeaf[3] = pUns->mTriAllBc ;
  mFcBndLeaf[4] = pUns->mQuadAllBc ;
  ulong_t mFcInterLeaf[MAX_VX_FACE+1] ;
  mFcInterLeaf[2] = pUns->mBiAllInter ;
  mFcInterLeaf[3] = pUns->mTriAllInter ;
  mFcInterLeaf[4] = pUns->mQuadAllInter ;

  const int dontReset = 0 ;
  ulong_t mPrtElem = 0 ;
  elType_e elType ;
  for ( elType = tri ; elType <= hex ; elType++ ) {
    number_uns_elems_by_type ( pUns, parent, elType, elType, dontReset ) ;
    mPrtElem += ( pUns->mElemsOfType[elType] - mElOfTypeLeaf[elType] ) ;
  }

  count_uns_bndFaces ( pUns ) ;


  hid_t hier_id = H5Gcreate(file_id, "HierarchicRefinement",
                            H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
  if ( !hier_id )
    hip_err ( fatal, 0, "could not create section entry "FUNLOC"." ) ;

  /* Number of leaf elements written in the main section, including buffered el.*/
  h5_write_ulg ( hier_id, 0, "mLeafElem", 1, &mElemLeaf ) ;



  /* Parent elements/coarse grid. */
  const int compress = 0 ; // Dpm't compress coordinates, not effective?
  h5w_coor ( pUns, compress, hier_id, mVxLeaf ) ;

  /* Note that h5w_conn renumbers by type. */
  h5w_conn ( pUns, hier_id, mElOfTypeLeaf ) ;




  /* Open a parent Boundary group. */
  hid_t bnd_id = bnd_id = H5Gcreate( hier_id, "Boundary", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
  if ( !bnd_id ) {
    hip_err ( fatal, 0, "could not create `Boundary' grp in h5w_bnd.") ;
  }

  /* Boundary faces to hdf and xmf. */
  h5w_bnd_faces ( pUns, bnd_id, bndAndInter, pUns->mBc,
                  NULL, NULL, NULL, NULL,
                  mElemLeaf, mFcBndLeaf ) ;

  herr_t status = H5Gclose(bnd_id);



  /* Parent ref types. */
  ulong_t mChildEl = 0 ;
  h5w_refType ( pUns, hier_id, mElemLeaf, &mChildEl ) ;


  /* Parent to child conn. */
  h5w_childElem ( pUns, hier_id, mElemLeaf, mChildEl ) ;



  /* Adapted (hanging) edges. */
  h5w_adEdge ( pUns, hier_id ) ;


  status = H5Gclose(hier_id) ;

  return ( ret ) ;
}
#endif

/******************************************************************************

  write_hdf5_grid:
  Write the connectivity and bc info (all but bc and sol) to file.

  Last update:
  ------------
  20aug19; move writing of bnd conn from h5w_grid_level here.
  1oct17; rename to write_hdf_grid, make public.
  14jul17; extract h5w_grid_level.
  4Jyl17; refactor, open xmf file and build h5 filenames here.
  29Jun17; call refactored routine h5w_bnd_patch_conn.
  15Sep16, renamed to number_uns_elems_by_type
  10Sep16; new interface to number_uns_elemFromVerts
  30Jun14; fix bug with wrong numbering after -b: renumber.
           intro -v for writing vx2El connectivity.
  9Jul13; include bounding box in header file.
  6Apr13: promote possibly large unsigned ints to ulong_t
  15Dec12; intro h5w_bnd_patch_conn.
  14Apr08: extracted from write_hdf5.

  Input:
  ------
  rootFile = root of the output file name to which a level indicator will be added
  pUns     = grid

  Returns:
  --------
  0 on failure, 1 on success.

*/

int write_hdf5_grid ( char *rootFile, uns_s *pUns ) {

  hid_t       file_id ;   /* identifier */
  herr_t      status ;
  char outFile[TEXT_LEN], grpName[TEXT_LEN] ;

  /* Open files. */
  // prepend_path ( rootFile ) ;
  strcpy_prepend_path ( outFile, rootFile ) ;
  strcat ( outFile, ".mesh.h5" ) ;
  file_id = H5Fcreate( outFile, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

  char xmfile[TEXT_LEN] ;
  strcpy_prepend_path ( xmfile, rootFile ) ;
  strcat ( xmfile, ".mesh.xmf" ) ;

  FILE *Fxmf;
  if ( !( Fxmf = fopen(xmfile,"w") ) ) {
    sprintf ( hip_msg, "could not open xmf file %s\n", xmfile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "        writing grid in hdf5 to %s", outFile ) ;
    hip_err ( blank, 1, hip_msg ) ;
  }

  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "        writing xmf data to file: %s", xmfile ) ;
    hip_err ( blank, 3, hip_msg ) ;
  }




  /* File names are created only once, not again in various routines. */
  char meshFileNmH5[LINE_LEN] ;
  strcpy ( meshFileNmH5, rootFile ) ;
  strcat ( meshFileNmH5, ".mesh.h5" ) ;
  char solFileNmH5[LINE_LEN] ;
  strcpy ( solFileNmH5, rootFile ) ;
  strcat ( solFileNmH5, ".sol.h5" ) ;


  /* Renumber all grid levels, coarse grid element numbers are correct
     when writing the finer grid.
     Note: why not write the coarsest grid first? */
  int nLvl ;
  uns_s *pUnsLvl ;
  for ( nLvl = 0, pUnsLvl = pUns ; pUnsLvl ; pUnsLvl = pUnsLvl->pUnsCoarse, nLvl++ ) {
    if ( !h5w_flag_noVol ) {
      /* Renumber elements by number of vertices (equivalent to type, as there
         is no adaptation with hanging nodes, yet, for hdf5. */
      pUnsLvl->numberedType = invNum ;
      number_uns_elems_by_type ( pUnsLvl, leaf, tri, hex, 1 ) ;
    }
  }



  /* For backw compat: level 0 grid written at top level,
     all other levels using their own group underneath. */
  hid_t lvl_id = file_id ;
  for ( nLvl = 0, pUnsLvl = pUns ; pUnsLvl ; pUnsLvl = pUnsLvl->pUnsCoarse, nLvl++ ) {
    if ( nLvl ) {
      sprintf ( grpName, "LEVEL_%02d", nLvl ) ;
      lvl_id = H5Gcreate(file_id, grpName,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    }

    h5w_grid_level ( pUnsLvl, nLvl, lvl_id, Fxmf, meshFileNmH5, solFileNmH5 ) ;

    if ( nLvl )
      status = H5Gclose(lvl_id) ;
  }


#ifdef ADAPT_HIERARCHIC
  /* Parents of leaf elements, currently only for single/finest grid. */
  if ( h5w_flag_hierarchy )
    h5w_grid_hierarchy ( pUns, file_id ) ;
#endif



  // moved here from end of
  if ( h5w_flag_bnd) {
    h5w_bnd_patch_conn ( pUns, file_id ) ;

    /* Restate volume numbering. Mark all used vertices and renumber. */
    validate_elem_onPvx ( pUns ) ;
    /* Renumber continously into the attached chunks. */
    pUns->numberedType = invNum ;
    /* Renumber vertices. */
    number_uns_grid ( pUns ) ;
    /* Renumber the elements in order of element type. */
    number_uns_elem_leafs ( pUns ) ;
  }


  /* Terminate access to the grid file. */
  status = H5Fclose(file_id);

  /* Add signature to mesh file*/
  if ( h5w_flag_signature )
    signature(outFile);

  strcpy_prepend_path ( outFile, rootFile ) ;
  strcat ( outFile, ".asciiBound" ) ;
  if ( !h5w_flag_asciiBound )  {
  /* Use the old asciiBound. */
    write_avbp_asciiBound_4p7 ( pUns, outFile ) ;
  }
  else {
  /* Write the more recent hdf-style asciibound. */
    h5w_ascii_key ( pUns, outFile ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  write_hdf5_sol:
  Write a solution to hdf5.

  Last update:
  ------------
  9May22; restore prepend-path.
  9Apr20; replace call to one_fxstr with fxStr and dim=1,
          reading one_fxStr fails for some reason.
  7Sep15; write ielee parameter.
  17Dec13; comment out check_var_name, should be done at read time.
           remove else block in h5_write_all if-block. No longer executable.
  15Dec13; use renamed h5w_one_fx_str
  3Sep12; deal with if-else logic in the next change properly.
  2Sep12; don't insist on a complete GaseousPhase for write -all.
  3Jul10;  allow to write scalars and node-based vectors carried over
           from Parameters.
  18Sep09; use write_str80.
  25May09; recompute number of vars, rather than using avbp.restart.
  22May09; new interface for h5_write_solfield.
  7Apr09; allow hdfa without GaseousPhase.
  4Apr09; modifications by GS on writing rhol, mEqF, mEqS, nadd.
          JDM: replace varTypeS with varList.
  14Apr08; supply uniform species field for mono-species solutions.
  11Dec07; fix bug using neqfic (merci GS)
  10Sep07; add fictive species
  15Sep06: derived from avbp_write_sol.

  Input:
  ------
  pUns

  Changes To:
  -----------
  fileName

  Returns:
  --------
  0 on failure, 1 on success.

*/

int write_hdf5_sol ( uns_s *pUns, char *fileName ) {

  hid_t       file_id, grp_id;   /* identifier */
  herr_t      status;

  double *dBuf ;

  avbpFmt_e avbpFmt = v6_0 ;

  const int mDim = pUns->mDim, mEqu = pUns->varList.mUnknowns ;
  int iBuf[99], mVx = pUns->mVertsNumbered, nEqu=0,
    mEqF=0, mEqS=0, nreac=0, neqt=0, nadd=0, neq2pf=0, neqfic=0, nOther=0,
    k, nPb=0 ;
  varList_s *pVL = &pUns->varList ;
  var_s *pVar = pVL->var ;
  char versionString[TEXT_LEN], outFile[TEXT_LEN], grp[LEN_GRPNAME] ;

  int mP = pUns->restart.hdf.m5pList ;
  h5pList_s *pL, *pL0 = pUns->restart.hdf.h5pList ;

  if ( pUns->varList.varType == noVar ) {
    /* No solution. Don't write the file. */
    return ( 1 ) ;
  }


  strcpy ( outFile, fileName ) ;
  prepend_path ( outFile ) ;
  file_id = H5Fcreate( outFile, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);


  /* Count the variables. */
  /* Loop over all equ, count types and assume they are in order. */
  for ( k = 0 ; k < mEqu ; k++ )
    switch ( pVar[k].cat ) {
    case ns : mEqF++ ; break ;
    case species : mEqS++ ; break ;
    case rrates : nreac++ ; break ;
    case tpf : neq2pf++ ; break ;
    case rans : neqt++ ; break ;
    case add : nadd++ ; break ;
    case mean : nPb++ ; break ;
    case fictive : neqfic++ ; break ;
    case add_tpf : nPb++ ; break ;
    case other : nOther++ ; break ;
    case noCat : nPb++ ; break ;
    default : nPb++ ;
    }


  if ( mEqF != mDim+2  && nPb && !h5w_flag_all ) {
    sprintf ( hip_msg, "in write_hdf5_sol:\n"
              "         only %d flow variables in %d-D (ns).\n"
              "         found %d non-flow variables"
              " (mean, add_tpf, noCat).\n", mEqF, mDim, nPb ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "writing solution to %s\n", outFile ) ;
    hip_err ( info, 1, hip_msg ) ;
  }

  /* 1.28.2: allow writing primitive sol following n3s multi-species read.
     We want conservative variables.
  if ( mEqF ) conv_uns_var ( pUns, cons ) ;  */

  /* Make sure all vars are named.
  if ( !h5w_flag_all ) {
    check_var_name ( &pUns->varList, &pUns->restart, pUns->mDim ) ;
  } */
  h5_check_grp ( &pUns->varList ) ;




  /* All vars are written as vectors, so allocate one. */
  dBuf = arr_malloc ( "dBuf in write_hdf5_sol", pUns->pFam,pUns->mVertsNumbered,
                      sizeof( *dBuf ) ) ;


  /* Parameters. */
  grp_id = H5Gcreate(file_id, "Parameters", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

  /* Identification string. */
  sprintf ( versionString, " AVBP %s    ", avbpFmtStr[avbpFmt] ) ;
  h5_write_fxStr ( grp_id, "versionstring", 1, fxStr240, versionString ) ;

  /* GS: Parameters basics if not hdfa  */
  if ( mP == 0 ) {
    iBuf[0]=( pUns->restart.avbp.iniSrc ? pUns->restart.hdf.itno : 0.) ;
    h5_write_int ( grp_id, 0, "niter", 1, iBuf ) ;

    dBuf[0] = ( pUns->restart.avbp.iniSrc ? pUns->restart.hdf.dtsum : 0. ) ;
    h5_write_dbl ( grp_id, 0, "dtsum", 1, dBuf ) ;

    if ( pUns->restart.hdf.ielee )
      h5_write_char ( grp_id, 1, "ielee", 1, &pUns->restart.hdf.ielee ) ;
  }

  /* GS: only required for 2pf. */
  if ( neq2pf ) {
    dBuf[0] = ( pUns->restart.avbp.iniSrc ? pUns->restart.avbp.rhol : 0. ) ;
    h5_write_dbl ( grp_id, 0, "rhol", 1, dBuf ) ;
  }


  /* Dump hdfa scalar parameters. */
  for ( pL=pL0 ; pL<pL0+mP ; pL++ ) {
    if ( pL->i.type == h5_i )
      h5_write_int ( grp_id, 0, pL->i.label, 1, &pL->i.iVal ) ;
    else if ( pL->d.type == h5_d )
      h5_write_dbl ( grp_id, 0, pL->d.label, 1, &pL->d.dVal ) ;
    else if ( pL->s.type == h5_s )
      h5_write_fxStr ( grp_id, pL->d.label, 1, fxStr240, pL->s.str ) ;
  }

  /* Recomputed/overwritten. */
  h5_write_ulg ( grp_id, 0, "nnode", 1, &pUns->mVertsNumbered ) ;


  /* Close parameters. */
  status = H5Gclose(grp_id);



  /* This is now default, with no option to reverse.
     if ( h5w_flag_all ) { */
    /* Dump all groups that were read to file. Filter at read */

    for ( nEqu = 0 ; nEqu < mEqu ; nEqu++ ) {
      if ( pVar[nEqu].flag ) {
        /* Only write flagged variables. */

        strcpy ( grp, pVar[nEqu].grp ) ;
        /* Does the group exist or does it need creating?
           read_hdf5 has a routine h5_grp_exists, but we need to define
           the type hid_t to prototype it, too much hassle,
           so use the primitive. */
        if ( h5_obj_exists ( file_id, grp ) == H5I_GROUP )
          grp_id = H5Gopen( file_id, grp, H5P_DEFAULT ) ;
        else
          grp_id = H5Gcreate( file_id, grp,
                              H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;

        h5_write_solfield ( grp_id, grp, pVar[nEqu].cat,
                            pUns, nEqu, nEqu+1, mVx, dBuf ) ;

        /* Close Grp. */
        status = H5Gclose(grp_id);
      }
    }
//   }
//  else {
//    /* hdf keyword/ h5w_flag_all = 0:Only standard, predefined fields. */
//
//    /* GaseousPhase. */
//    grp_id = H5Gcreate( file_id, "GaseousPhase",
//                        H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
//
//    /*
//      for ( nEqu = 0 ; nEqu < mEqF ; nEqu++ ) {
//      pChunk = NULL ;
//      pdBuf = dBuf ;
//      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
//      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
//      if ( pVrtx->number )
//      *pdBuf++ = pVrtx->Punknown[nEqu] ;
//
//      h5_write_dbl ( grp_id, pVL->var[nEqu].name, mVx, dBuf ) ;
//      } */
//
//    h5_write_solfield ( grp_id, "GaseousPhase", ns, pUns, 0, mEqF, mVx, dBuf ) ;
//
//    /* Close GaseousPhase. */
//    status = H5Gclose(grp_id);
//
//
//    /* Species. */
//    if ( mEqS ) {
//      grp_id = H5Gcreate(file_id, "RhoSpecies",
//                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
//      h5_write_solfield ( grp_id, "RhoSpecies", species, pUns,
//                          mEqF, mEqu, mVx, dBuf ) ;
//
//      /* Close Species. */
//      status = H5Gclose(grp_id);
//    }
//    else {
//      /* Supply a uniform virtual species called Y_AIR. */
//      vec_ini_dbl ( 1., pUns->mVertsNumbered, dBuf ) ;
//
//      grp_id = H5Gcreate(file_id, "RhoSpecies",
//                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
//      h5_write_dbl ( grp_id, 0, "Y_AIR", pUns->mVertsNumbered, dBuf ) ;
//
//      /* Close Species. */
//      status = H5Gclose(grp_id);
//    }
//
//
//
//    /* LiquidPhase. */
//    if ( neq2pf ) {
//      grp_id = H5Gcreate(file_id, "LiquidPhase",
//                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
//
//      h5_write_solfield ( grp_id, varCatNames[tpf], tpf, pUns,
//                          mEqF, mEqu, mVx, dBuf ) ;
//
//      /* Close LiquidPhase. */
//      status = H5Gclose(grp_id);
//    }
//
//
//
//    /* FictiveSpecies. */
//    if (  neqfic ) {
//      grp_id = H5Gcreate(file_id, "FictiveSpecies",
//                         H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT) ;
//
//      h5_write_solfield ( grp_id, "FictiveSpecies", fictive, pUns,
//                          mEqF, mEqu, mVx, dBuf ) ;
//
//      /* Close LiquidPhase. */
//      status = H5Gclose(grp_id);
//    }
//  }

  arr_free ( dBuf ) ;


  /* Any solParams to write? */
  if ( h5w_flag_zones ) {
    int is_sol = 1 ;
    h5w_zone  ( pUns, file_id, is_sol ) ;
  }

  /* Terminate access to the file. */
  status = H5Fclose(file_id);

  return ( 1 ) ;

}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  write_mbMap_hdf5:
*/
/*! write a mapping from multi-block ijk vx to unstructured vx
 *
 *
 */

/*

  Last update:
  ------------
  14Mar18: conceived.


  Input:
  ------
  pUns: unstructured grid
  filename: hdf file to write.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int write_mbMap_hdf5 ( const char *fileName ) {

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;

  /* check that block info is recorded and matches. */
  int doCheckVol = 1 ;
  check_valid_uns ( pUns, doCheckVol) ;




  /* Check for non-corrupted block to chunk mapping. */
  int mBl = pUns->mChunks, iBl ;
  chunk_struct *pCh ;
  ulong_t mVxBl, sumVxBl =0, maxMVxBl = 0 ;
  for ( iBl = 0 ; iBl < mBl ; iBl++ ) {
    pCh = pUns->ppChunk[iBl] ;
    mVxBl = pCh->blockDim[0]*pCh->blockDim[1]*pCh->blockDim[2] ;
    sumVxBl += mVxBl ;
    maxMVxBl = MAX( maxMVxBl, mVxBl ) ;
    if ( mVxBl != pCh->mVerts ) {
      sprintf ( hip_msg,
                "block %d to chunk map not present or corrupted,\n"
                "          expected %"FMT_ULG", found %"FMT_ULG" verts.\n"
                "          make sure to use the 'map' keyword in `co 2uns map'\n",
                iBl+1, mVxBl, pCh->mVerts ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 1 ) ;
    }
  }



  /* Field of vx no big enough for the largest block. */
  int *pnVx = arr_malloc ( "pnVx in write_mbMap_hdf", pUns->pFam, maxMVxBl,
                           sizeof ( *pnVx ) ) ;

  char outFile[TEXT_LEN] ;
  strncpy ( outFile, fileName, TEXT_LEN-1 ) ;
  prepend_path ( outFile ) ;



  sprintf ( hip_msg, "    Writing mb to uns vertex map\n"
                     "      in hdf5 to %s", outFile ) ;
  hip_err ( blank, 2, hip_msg ) ;



  hid_t file_id = H5Fcreate( outFile, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);



  herr_t      status ;
  char grpName[TEXT_LEN] ;

  hid_t bl_id ;
  *pnVx = pUns->mVertsNumbered ;
  h5_write_int ( file_id, 0, "mVxUns", 1, pnVx ) ;


  hid_t bl2vx_id = H5Gcreate(file_id, "Block2Vx", H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);


  int *pn ;
  vrtx_struct *pVx ;
  for ( iBl = 0 ; iBl < mBl ; iBl++ ) {
    pCh = pUns->ppChunk[iBl] ;
    mVxBl = pCh->blockDim[0]*pCh->blockDim[1]*pCh->blockDim[2] ;

    bl_id = H5Gcreate( bl2vx_id, pCh->name,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    h5_write_int ( bl_id, 0, "size", 3, pCh->blockDim ) ;

    for ( pn = pnVx, pVx = pCh->Pvrtx+1 ; pVx <= pCh->Pvrtx+mVxBl ; pVx++ ) {
      /* This relies on vxCpt correctly set during merge. */
      *pn++ = pUns->ppChunk[pVx->vxCpt.nCh]->Pvrtx[pVx->vxCpt.nr].number ;
    }

    h5_write_int ( bl_id, 0, "vxNo", mVxBl, pnVx ) ;
    status = H5Gclose( bl_id );
  }

  status = H5Gclose( bl2vx_id ) ;

  status = H5Fclose( file_id ) ;


  arr_free ( pnVx ) ;
  return ( 0 ) ;
}


/******************************************************************************

  write_hdf5:
  Write the whole mesh and solution to a single file in hdf5.

  Last update:
  ------------
  18dec24; run conv_uns_var before writing xmf.
  24Apr20; check for uns grid type.
  21Mar18; force conversion to cons var, matching xmf, unless -p specified.
  14Mar18; existence of path dir not tested in set_menu.
  16Dec16; exclude surface meshes.
  29Jun14; add support for writing levels.
  4Apr21; use command-line arguments, allow edges, faces, compression.
  20Sep09; add info at the start.
  14Apr08; extract mesh writing into separate function, allow for sol only.
  17Dec06: derived from avbp_hdf5_sol.

  Input:
  ------
  rootFile = root of the output file name to which a level indicator will be added

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int write_hdf5 ( char *argLine ) {

  /* Check for valid unstructured grid. */
  if ( Grids.PcurrentGrid->uns.type != uns ) {
    hip_err ( warning, 1,
              "hdf formats are unstructured, your grid isn't. Nothing written.\n"
              "            Use copy 2uns to convert to unstructured.") ;
    return ( 0 ) ;
  }

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  char outFile[TEXT_LEN] ;
  char rootFile[TEXT_LEN] ;



  h5w_args ( argLine, rootFile ) ;
  // xmf needs the raw filename. prepend before each opening.
  // prepend_path ( rootFile ) ;

  int doCheckVol = 1 ;
  if ( !h5w_flag_dump )
    check_valid_uns ( pUns, doCheckVol) ;


  /* This needs to be a function call as this sets a static variable in
     h5_util, used both by write_hdf and read_hdf. */
  h5_set_zip ( h5w_zip_lvl ) ;

  if ( pUns->specialTopo == surf )
    sprintf ( hip_msg, "    Writing surface grid" ) ;
  else
    sprintf ( hip_msg, "    Writing grid level %d (0 being finest level)",
              h5w_nLevel ) ;
  hip_err ( blank, 1, hip_msg ) ;

  char rootFileWithPath[LINE_LEN] ;
  strcpy ( rootFileWithPath, rootFile ) ;
  prepend_path ( rootFileWithPath ) ;
  sprintf ( hip_msg, "     in hdf format to: %s", rootFileWithPath ) ;
  if  ( h5w_flag_bnd )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             including separate boundary grid (-b)" ) ;
  if  ( h5w_flag_dump )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             dump option, no checks (-d)" ) ;
  if ( h5w_flag_edge )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             with Metis element connectivity (-e)" ) ;
  if ( h5w_flag_face )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             with list of internal faces (-f)" ) ;
  if  ( h5w_flag_sol )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             solution only/no volume grid (-s)" ) ;
  if ( h5w_flag_all )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             solution with all variables (-a)" ) ;
  if ( h5w_flag_zip )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             with compression level %d (-c)", h5w_zip_lvl ) ;
  if ( h5w_flag_asciiBound )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             asciiBound in AVBP 7.X format\n");
  if ( !h5w_flag_asciiBound )
    sprintf ( strchr ( hip_msg, '\0' ),
              "\n             asciiBound in AVBP 6.X format\n");
  hip_err ( blank, 1, hip_msg ) ;




  /* Get the right level to write. */
  int n ;
  for ( n = 0 ; n < h5w_nLevel ; n++ ) {
    pUns = pUns->pUnsCoarse ;
    if ( !pUns ) {
      sprintf ( hip_msg, "coarse grid level %d does not exist.", h5w_nLevel ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 0 ) ;
    }
  }


  if ( !h5w_flag_noVol && pUns->varList.varType != cons && !h5w_flag_prim ) {
      // unless forced with -p to write primitive, convert to cons.
      conv_uns_var ( pUns, cons ) ;
  }


  /* Upon hdf5s, write only the solution but no grid. */
  if ( !h5w_flag_sol ) {
    write_hdf5_grid ( rootFile, pUns ) ;
  }


  if ( !h5w_flag_noVol ) {
    strcpy ( outFile, rootFile ) ;
    strcat ( outFile, ".sol.h5" ) ;

    write_hdf5_sol ( pUns, outFile ) ;
  }

  return ( 1 ) ;
}
