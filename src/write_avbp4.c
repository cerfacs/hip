/*
   write_avbp4.c
   Write an unstructured, multichunked grid as single block into an
   avbp 4.0 format. The routines writing the boundary files are in
   write_uns_avbp_bound.c.
 
   Last update:
   ------------
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   13Feb99; pack elMark.
   21Dec98; make _conn and _elMark static.
   22Nov96; cut out boundary routines.
   4May96; conceived.

   Contains:
   ---------
   write_avbp4:
   write_avbp4_conn:
   write_avbp4_elMark:
   
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"
#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const char version[] ;
extern char hip_msg[] ;
extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const int perBc_in_exBound ;



/****************************************************************

  write_avbp4_conn:
  Write the mesh connectivity of a chunked unstructured mesh to AVBP.
  The elements are renumbered in the sequence with a lexicographic
  ordering of element types.

  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  8May19; use elType rather than markdEdges in type selection.
  30Apr19; make work for ADAPT_REF
  21Oct16; guard if/else block around mVerts following compiler warning.
  1Jul14; remove MAX_DRV_ from loop syntax, as pgcc compiler complains about
          tautology.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  21Dec98; move mElems_w_mVerts into pUns.
  5May96: conceived..

  Input:
  ------
  pUns:
  PconnFile:
 
  Changes to:
  -----------
  *pUns->Pelem->number:
  *PconnFile:

*/

static int write_avbp4_conn ( uns_s *pUns, char *PconnFile ) {
  
  const ulong_t *mElems_w_mVerts = pUns->mElems_w_mVerts ;
  FILE *Fconn ;
  int buffer[IO_STREAM_LEN/4], mVerts, mElemsWritten = 0, nVert,
    mElemsWrittenThisType, mVxHg, mVertsWritten ;
#ifdef ADAPT_HIERARCHIC
  int kVxHg[MAX_ADDED_VERTS],  fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1] ;
#endif
  elType_e elType ;
  chunk_struct *Pchunk ;
  elem_struct *pElem ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;
  
  if ( verbosity > 2 )
    printf ( "      connectivity to %s\n", PconnFile ) ;

  /* Open input file. */
  if ( !( Fconn = fopen ( PconnFile, "w" ) ) ) {
    printf ( " FATAL: file: %s could not be opened.\n", PconnFile ) ;
    return ( 0 ) ; }
  
  /* Number of blocks, cells, layers of dummy cells (0). */
  buffer[0] = 3*sizeof( int ) ;
  buffer[1] = 1 ;
  buffer[2] = pUns->mElemsNumbered ;
  buffer[3] = 0 ;
  buffer[4] = buffer[0] ;
  FWRITE ( buffer, sizeof( int ), 5, Fconn ) ;

  /* k=1 loop, interior cells. */
  /* Number of dummy cells, number of different element types. */
  buffer[0] = 2*sizeof( int ) ;
  /* number of dummy cells. */
  buffer[1] = 0 ;
  /* number of different element types. As a check: set to zero in AVBP 4*/
  buffer[2] = 0 ;
  buffer[3] = buffer[0] ;
  FWRITE ( buffer, sizeof( int ), 4, Fconn ) ;

  /* Loop over all element types. */
  elType_e elTMax = MAX_DRV_ELEM_TYPES ; // Attempt to pacify the pgcc compiler.
  #ifdef ADAPT_HIERARCHIC
  int mBaseVerts ;
  #endif
  for ( elType = tri ; elType < elTMax ; elType++ ) {
    if ( elType <= hex )
      mVerts = elemType[elType].mVerts ;
    else
      mVerts = elType - MAX_ELEM_TYPES + 4 ;
      
    buffer[0] = 3*sizeof( int ) ;
    /* Number of vertices per element. */
    buffer[1] = mVerts ;
    buffer[2] = mElemsWritten + 1 ;
    buffer[3] = mElems_w_mVerts[elType] ;
    buffer[4] = buffer[0] ;
    FWRITE ( buffer, sizeof( int ), 5, Fconn ) ;
    
    if ( mElems_w_mVerts[elType] ) {
      /* Start the next record. */
      buffer[0] = mVerts*mElems_w_mVerts[elType]*sizeof( int ) ;
      FWRITE ( buffer, sizeof( int ), 1, Fconn ) ;
	
      mElemsWrittenThisType = 0 ;
      for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
	for ( pElem = Pchunk->Pelem+1 ;
	      pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
          /* Why is markdEdges abused here? Use elType directly, as it appears
             to hae all vx encoded, also for derived types?
             if ( pElem->markdEdges == elType && pElem->number ) { */
	  if ( pElem->elType == elType && pElem->number ) {
            /* This element is marked and of the type sought. */
	    ++mElemsWrittenThisType ;
            pElem->number = ++mElemsWritten ;

#ifdef ADAPT_HIERARCHIC 
	    if ( elType > hex ) {
              /* Derived Element. */
	      get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, PhgVx,
			       fixDiag, diagDir ) ;
              mBaseVerts = elemType[pElem->elType].mVerts ;
	      for ( nVert = 0 ; nVert < mBaseVerts ; nVert++ )
		buffer[nVert] = pElem->PPvrtx[nVert]->number ;
	      for ( nVert = 0 ; nVert < mVxHg ; nVert++ )
		buffer[mBaseVerts+nVert] = PhgVx[nVert]->number ;
	      mVertsWritten = elemType[pElem->elType].mVerts + mVxHg ;
	    }
	    else {
#endif
              /* Base element. */
	      for ( nVert = 0 ; nVert < mVerts ; nVert++ )
		buffer[nVert] = pElem->PPvrtx[nVert]->number ;
	      mVertsWritten = mVerts ;
#ifdef ADAPT_HIERARCHIC 
	    }
#endif
            FWRITE ( buffer, sizeof( int ), mVerts, Fconn ) ;

	    if ( mVertsWritten != mVerts ) {
	      sprintf ( hip_msg, "mismatch in vertex numbers, %d expected, %d found in"
                        " elem %"FMT_ULG" in write_avbp4_conn.\n",
                        mVerts, mVertsWritten, pElem->number ) ;
              hip_err ( fatal, 0, hip_msg ) ;
            }
	  }

      if ( mElemsWrittenThisType != mElems_w_mVerts[elType] ) {
	sprintf ( hip_msg, "for elem type %d, %"FMT_ULG" total elements expected, %d found.\n",
                  elType, mElems_w_mVerts[elType], mElemsWrittenThisType ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      /* Trailing length of the record. */
      buffer[0] = mVerts*mElems_w_mVerts[elType]*sizeof( int ) ;
      FWRITE ( buffer, sizeof( int ), 1, Fconn ) ;
    }
  }
  
  if ( mElemsWritten != pUns->mElemsNumbered ) {
    sprintf ( hip_msg, "%"FMT_ULG" total elements expected, %d found.\n",
	      pUns->mElemsNumbered, mElemsWritten ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
    
  /* k=2 loop, dummy cells. */
  /* Number of dummy cells, number of different element types = 0. */
  buffer[0] = 3*sizeof( int ) ;
  buffer[1] = 0 ;
  buffer[2] = 0 ;
  buffer[3] = 0 ;
  buffer[4] = buffer[0] ;
  FWRITE ( buffer, sizeof( int ), 5, Fconn ) ;

  elTMax = MAX_DRV_ELEM_TYPES ; // Attempt to pacify the pgcc compiler.
  for ( elType = tri ; elType <= elTMax ; elType++ ) {
    if ( elType <= hex ) {
      mVerts = elemType[elType].mVerts ;}
    else {
      mVerts = elType - MAX_ELEM_TYPES + 4 ;}
      
    buffer[0] = 3*sizeof( int ) ;
    /* Number of vertices per element. */
    buffer[1] = mVerts ;
    buffer[2] = 0 ;
    buffer[3] = 0 ;
    buffer[4] = buffer[0] ;
    FWRITE ( buffer, sizeof( int ), 5, Fconn ) ;
  }

  fclose ( Fconn ) ;
  return ( 1 ) ;
}

#ifdef ADAPT_HIERARCHIC
/******************************************************************************

  write_avbp4_elMark:
  Write an elmark file for all derived elements.
  
  Last update:
  ------------
  8May19; use elType rather than markdEdges in type selection.
  13Feb99 ; pack elMark.
  21Dec98; move mElems_w_mVerts into pUns.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_avbp4_elMark ( const uns_s *pUns, char *elMarkFile ) {

  const int *mElems_w_mVerts = ( const int *) pUns->mElems_w_mVerts ;
  FILE *FelMark  ;
  int buffer[3], mElemsWritten, mDrvElems = 0, mVxHg, mVxHgExp ;
  unsigned int elInt[2] ;
  elType_e elType ;
  const chunk_struct *Pchunk ;
  elem_struct *pElem ;
  elMark_s elMark ;

  if ( verbosity > 2 )
    printf ( "      element marker to %s\n", elMarkFile ) ;

  if ( sizeof( elMark_s ) != 2*sizeof( int ) ) {
    printf ( " FATAL: sizeof( elMark_s ) != 2*sizeof( int ) in write_avbp4_elMark.\n" ) ;
    return ( 0 ) ; }


  /* Open input file. */
  if ( !( FelMark = fopen ( elMarkFile, "w" ) ) ) {
    printf ( " FATAL: file: %s could not be opened.\n", elMarkFile ) ;
    return ( 0 ) ; }

  /* Count all derived element types. */
  for ( elType = ( elType_e ) ( hex+1 ) ;
	elType < ( elType_e ) MAX_DRV_ELEM_TYPES ; elType++ )
    mDrvElems += mElems_w_mVerts[elType] ;

  /* One record with the total number of derived elements. */
  buffer[0] = buffer[2] = sizeof( int ) ;
  buffer[1] = mDrvElems ;
  FWRITE ( buffer, sizeof( int ), 3, FelMark ) ;
  
  /* Write one looooooooong record. */
  buffer[0] = mDrvElems*sizeof( elMark_s ) ;
  FWRITE ( buffer, sizeof( int ), 1, FelMark ) ;
  
  /* Loop over all element types.*/
  mElemsWritten = 0 ;
  for ( elType = ( elType_e ) ( hex+1 ) ;
	elType < ( elType_e ) MAX_DRV_ELEM_TYPES ; elType++ )
    if ( mElems_w_mVerts[elType] )
      for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
	for ( pElem = Pchunk->Pelem+1 ;
	      pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
          /* Why is markdEdges abused here? Use elType directly, as it appears
             to hae all vx encoded, also for derived types?
             if ( pElem->markdEdges == elType && pElem->number ) { */
	  if ( pElem->elType == elType && pElem->number ) {
	    /* This element is marked and of the type sought. */
	    ++mElemsWritten ;
	    elMark = get_elMark_aE ( pUns, pElem, &mVxHg ) ;

	    mVxHgExp = pElem->markdEdges - elemType[ pElem->elType ].mVerts -
		       MAX_ELEM_TYPES + 4 ;
	    if ( mVxHg != mVxHgExp )
	      printf ( " FATAL: expected %d hanging verts, found %d"
		       " in write_avbp4_elMark.\n", mVxHg, mVxHgExp ) ;

            elMark2int ( elMark, elInt ) ;
            FWRITE ( elInt, sizeof(int), 2, FelMark ) ;
	  }
  /* Trailing length of the record. */
  buffer[0] = mDrvElems*sizeof( elMark_s ) ;
  FWRITE ( buffer, sizeof( int ), 1, FelMark ) ;


  if ( mElemsWritten != mDrvElems )
    printf ( " FATAL: %d total elements expected, %d found.\n",
	     mDrvElems, mElemsWritten ) ;

  fclose ( FelMark ) ;
  return ( 1 ) ;
}

#endif


/******************************************************************************

  write_uns_avbp4:
  Write the un-chunked consistent mesh to avbp files. In addition to
  the standard files for the finest level, write files for the parents.
  Format: AVBP 4.0+.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  30Apr19; reivve ADAPT_REF
  15Sep16; renamed to   number_uns_elems_by_type 
  10Sep16; new interface to number_uns_elemsFromVerts
  4May96: conceived.
  
  Input:
  ------
  pUns:
  mDim:         
  
  Returns:
  --------
  1
  
*/

int write_avbp4 ( uns_s *pUns, char *ProotFile ) {
  
  char outFile[MAX_BC_CHAR+15] ;
#ifdef ADAPT_HIERARCHIC
  char prtFile[MAX_BC_CHAR+4] ;
  int mPrtElems ;
#endif
  int mEqu ;
  FILE *adpFile = NULL, *visFile ;

  if ( !pUns->validGrid ) {
    printf ( " FATAL: you were told that this grid is invalid, weren't you?.\n" ) ;
    return ( 0 ) ; }
  else if ( (check_bnd_setup ( pUns )).status != success ) {
    printf ( " FATAL: cannot write grid without proper boundary setup.\n" ) ;
    return ( 0 ) ; }

  prepend_path ( ProotFile ) ;
  if ( verbosity > 0 )
    printf ( "   finest grid to AVBP 4- as %s\n", ProotFile ) ;
  
  /* mEqu = pUns->mUnknowns ; */
  if ( pUns->mDim == 3 )
    mEqu = 5 ;
  else if ( pUns->mDim == 2 ) {
    mEqu = 4 ;
    /* In 2-D, write a 'master-file' for dplot. */
    strcpy ( outFile, ProotFile ) ;
    strcat ( outFile, ".adp" ) ;
    if ( !( adpFile = fopen ( outFile, "w" ) ) ) {
      printf ( " FATAL: file: %s could not be opened.\n", outFile ) ;
      return ( 0 ) ; }
    else
      fprintf ( adpFile, "avbp 4.0\n" ) ;
  }

  /* In any-D, write a 'master-file' for visual. */
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".visual" ) ;
  if ( !( visFile = fopen ( outFile, "w" ) ) ) {
    printf ( " FATAL: file: %s could not be opened.\n", outFile ) ;
    return ( 0 ) ; }
  else
    fprintf ( visFile,
	      "' Masterfile for visual3/AVBP 4- by hip version %s.'\n", version ) ;

  
  /* Number all leafs by number of vertices, including hanging ones,
     count them. */
  number_uns_elems_by_type ( pUns, leaf, tri, hex, 1 ) ;
  /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
     for the current numbering of elements. */
  count_uns_bndFaces ( pUns ) ;
  /* Remove all periodic bc, if they are not supposed to be listed. */
  if ( !perBc_in_exBound ) rm_perBc ( pUns ) ;

  /* Make pairs of periodic nodes and faces. */
  if ( ! match_per_faces ( pUns ) ) {
    printf ( " FATAL: failed to establish periodicity in write_avbp3.\n" ) ;
    return ( 0 ) ; }
  if ( !special_verts ( pUns ) ) {
    printf ( " FATAL: failed to match periodic vertices in write_avbp3_inBound.\n" ) ;
    return ( 0 ) ; }



  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".sol" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_sol ( pUns, outFile, v4_7 ) ;
    
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".coor" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_coor ( pUns, outFile ) ;

  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".conn" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp4_conn ( pUns, outFile ) ;
  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".exBound" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_exBound ( pUns, outFile ) ;
  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".inBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_inBound ( pUns, outFile ) ;
  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".asciiBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_asciiBound_4p7 ( pUns, outFile ) ;

#ifdef ADAPT_HIERARCHIC
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".elMark" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  write_avbp4_elMark ( pUns, outFile ) ;

  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".adEdge" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;

  write_adEdge ( pUns, outFile ) ;

  if ( adpFile ) fclose ( adpFile ) ;

  /* Write the parents. */
  strcpy ( prtFile, ProotFile ) ;
  strcat ( prtFile, ".prt" ) ;

  /* Number parents and children. If there are parents, write them to file. */
  write_avbp3_prt ( pUns, prtFile, &mPrtElems ) ;

  if ( mPrtElems ) {
    if ( verbosity > 0 )
      printf ( "   Writing coarser grids to AVBP 4.0+ as %s\n", prtFile ) ;
    fprintf ( visFile, "'%s'\n", prtFile ) ;

    /* Number and count the parent elements only. */
    number_uns_elemFromVerts_adapt ( pUns, parent ) ;
    count_uns_bndFaces ( pUns ) ;
    if ( !perBc_in_exBound ) rm_perBc ( pUns ) ;

    /* Make pairs of periodic nodes and faces. */
    if ( ! match_per_faces ( pUns ) ) {
      printf ( " FATAL: failed to establish periodicity in write_avbp3.\n" ) ;
      return ( 0 ) ; }
    if ( !special_verts ( pUns ) ) {
      printf ( " FATAL: failed to match periodic vertices in write_avbp3_inBound.\n" ) ;
      return ( 0 ) ; }

    
    strcpy ( outFile, prtFile ) ;
    strcat ( outFile, ".exBound" ) ;
    fprintf ( visFile, "'%s'\n", outFile ) ;
    write_avbp_exBound ( pUns, outFile ) ;
    
    strcpy ( outFile, prtFile ) ;
    strcat ( outFile, ".inBound" ) ;
    fprintf ( visFile, "'%s'\n", outFile ) ;
    write_avbp_inBound ( pUns, outFile ) ;
  }
#endif
  
  fclose ( visFile ) ;

  /* Turn periodic boundaries back on. */
  number_uns_elem_leafs ( pUns ) ;
  count_uns_bndFaces ( pUns ) ;
  return ( 1 ) ;
}

