/*
   uns_loop.c:
   All kinds of loop wrappers over entities in a structured grid.

   Last update:
   ------------
   27Jan98; conceived


   This file contains:
   -------------------
   loop_chunks
   loop_elems
   loop_verts
   loop_bndPatches
   loop_bndFaces
   loop_bndPatches_bc
   loop_bndFaces_bc

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

#include "fnmatch.h"


extern const int verbosity ;


/******************************************************************************

  loop_chunks:
  Loop over all chunks of a grid.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk

  Changes To:
  -----------
  ppChunk
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_chunks ( const uns_s *pUns, chunk_struct **ppChunk ) {
  
  if ( *ppChunk ) {
    /* Next chunk in loop. */
    if ( ( *ppChunk = (*ppChunk)->PnxtChunk ) )
      return ( 1 ) ;
    else
      /* End of linked list. */
      return ( 0 ) ;
  }
  else {
    /* Initialize the loop. */
    if ( ( *ppChunk = pUns->pRootChunk ) )
      return ( 1 ) ;
    else
      /* No chunks in this grid. */
      return ( 0 ) ;
  }
}


/******************************************************************************

  loop_elems:
  Loop over all elements of a grid.
  
  Last update:
  ------------
  4Apr13; drop nBeg, nEnd from interface.
  27Jan98: conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppElemFirst
  ppElemLast
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_elems ( const uns_s *pUns, chunk_struct **ppChunk,
		 elem_struct **ppElemFirst, elem_struct **ppElemLast ) {
  
  if ( !loop_chunks ( pUns, ppChunk ) ) return ( 0 ) ;

  *ppElemFirst = (*ppChunk)->Pelem + 1 ;
  *ppElemLast = (*ppChunk)->Pelem + (*ppChunk)->mElems ;
  
  return ( 1 ) ;
}


/******************************************************************************

  loop_elems_type:
  Loop over all elements of a grid of a given type.
  
  Last update:
  ------------
  3Mar98; derived from loop_elems.
  
  Input:
  ------
  pUns:    the unstructured grid
  elType:  the sought element type
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppChunk
  ppElem
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_elems_type ( const uns_s *pUns, const elType_e elType,
		      chunk_struct **ppChunk, elem_struct **ppElem ) {
  
  if ( *ppChunk )
    /* There is a chunk. Find the next element of elType in that chunk. */
    for ( ++(*ppElem) ; *ppElem <= (*ppChunk)->Pelem + (*ppChunk)->mElems ; (*ppElem)++ )
      if ( (*ppElem)->elType == elType )
	return ( 1 ) ;

  /* Loop over the next chunks. */
  while ( loop_chunks ( pUns, ppChunk ) )
    for ( *ppElem = (*ppChunk)->Pelem + 1 ;
	  *ppElem <= (*ppChunk)->Pelem + (*ppChunk)->mElems ; (*ppElem)++ )
      if ( (*ppElem)->elType == elType )
	return ( 1 ) ;

  /* Nothing found. */
  return ( 0 ) ;
}

/******************************************************************************

  loop_verts:
  Loop over all vertices of a grid.
  
  Last update:
  ------------
  30Jun16; odd that *pnVrtxFirst/Last remain ints. They really should be
           ulong_t types, but probably I found this too cumbersome to
           propagate through, as they are never used.
  27Jan98: conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppChunk
  ppVrtxFirst
  pnVrtxFirst
  ppVrtxLast
  pnVrtxLast
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_verts ( const uns_s *pUns, chunk_struct **ppChunk,
		 vrtx_struct **ppVrtxFirst, int *pnVrtxFirst,
		 vrtx_struct **ppVrtxLast, int *pnVrtxLast ) {
  
  if ( !loop_chunks ( pUns, ppChunk ) ) return ( 0 ) ;

  *pnVrtxFirst = 1 ;
  *ppVrtxFirst = (*ppChunk)->Pvrtx + 1 ;

  *pnVrtxLast = (*ppChunk)->mVerts ;
  *ppVrtxLast = (*ppChunk)->Pvrtx + *pnVrtxLast ;
  
  return ( 1 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  loop_uns_cont:
*/
/*! Continue looping over vertices from a given pointer.
 *
 */

/*
  
  Last update:
  ------------
  4May20: derived from loop_verts.
  

  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  pVxPrev: most previously visited. 
  

  Changes To:
  -----------
  ppChunk
  ppVrtxFirst
  ppVrtxLast
  
  Returns:
  --------
  0 if list is exhausted, 1 if there are further chunks to loop over.
  
  
*/

int loop_verts_cont ( const uns_s *pUns, chunk_struct **ppChunk,
                      vrtx_struct **ppVrtxFirst, vrtx_struct *pVxPrev,
                      vrtx_struct **ppVrtxLast ) {

  if ( *ppChunk ) {
    *ppVrtxLast = (*ppChunk)->Pvrtx + (*ppChunk)->mVerts ;
    if  ( !pVxPrev ||
          pVxPrev <  *ppVrtxLast ) {
      /* More vertices in this chunk. */
      *ppVrtxFirst = pVxPrev+1 ;
      return ( 1 ) ;
    }
  }

  /* Look at the first/next chunk. */
  if ( !loop_chunks ( pUns, ppChunk ) )
    return ( 0 ) ;

  *ppVrtxFirst = (*ppChunk)->Pvrtx + 1 ;
  *ppVrtxLast = (*ppChunk)->Pvrtx + (*ppChunk)->mVerts ;
  
  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  loop_verts_bound:
*/
/*! loop over all boundary vertices.
 *
 * This loops over all bnd faces, and filters out with a nodal flag
 * those that have been visited already.
 */

/*
  
  Last update:
  ------------
  24Aug24 conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s loop_verts_bound (  ) {
#undef FUNLOC
#define FUNLOC "in loop_verts_bound"
  
  ret_s ret = ret_success () ;

  // Reserve flag

  // loop over all bnd faces

  // loop over all nodes of that face

  // return if there is an un-visited one

  return ( ret ) ;
}


/******************************************************************************

  loop_bndPatches:
  Loop over all boundary patches of a grid.
  
  Last update:
  ------------
  27Jan98: conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppChunk
  ppBndPatchFirst
  pnBndPatchFirst
  ppBndPatchLast
  pnBndPatchLast
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_bndPatches ( const uns_s *pUns, chunk_struct **ppChunk,
		      bndPatch_struct **ppBndPatchFirst,
		      bndPatch_struct **ppBndPatchLast ) {
  
  if ( !loop_chunks ( pUns, ppChunk ) ) return ( 0 ) ;

  *ppBndPatchFirst = (*ppChunk)->PbndPatch + 1 ;
  *ppBndPatchLast = (*ppChunk)->PbndPatch + (*ppChunk)->mBndPatches ;
  
  return ( 1 ) ;
}

/******************************************************************************

  loop_bndFaces:
  Loop over all boundary patches of a grid.
  
  Last update:
  ------------
  21Dec18; allow empty patches, as e.g. arising in mmg_adapt_per.
  10Jan15; correct interface description.
  27Jan98: conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppChunk
  ppBndPatch
  ppBndFcLast
  pnBndFcLast
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_bndFaces ( const uns_s *pUns, chunk_struct **ppChunk,
		    bndPatch_struct **ppBndPatch,
		    bndFc_struct **ppBndFcFirst, bndFc_struct **ppBndFcLast) {
  int mBndFc = 0 ;

  while ( !mBndFc ) {
    if ( *ppChunk && *ppBndPatch - (*ppChunk)->PbndPatch < (*ppChunk)->mBndPatches )
      /* There is one more patch in this chunk. */
      (*ppBndPatch)++ ;
    else if ( !loop_chunks ( pUns, ppChunk ) )
      return ( 0 ) ;
    else {
      /* New chunk. */
      if ( !(*ppChunk)->mBndPatches ) {
        /* This chunk has no faces. */
        *ppBndPatch = NULL ;
        *ppBndFcLast = *ppBndFcFirst - 1 ;
        return ( 1 ) ;
      }
      else
        *ppBndPatch = (*ppChunk)->PbndPatch + 1 ;
    }
    mBndFc = (*ppBndPatch)->mBndFc ;
  }

  *ppBndFcFirst = (*ppBndPatch)->PbndFc ;
  *ppBndFcLast = *ppBndFcFirst + mBndFc - 1 ;
  return ( 1 ) ;
}


/******************************************************************************

  loop_bndPatches_bc:
  Loop over all boundary patches of a grid with a specific bc.
  
  Last update:
  ------------
  16Jul99; allow graceful abort if there is no patch list.
  27Jan98: conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppChunk
  ppBndPatchBc
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_bndPatches_bc ( const uns_s *pUns, const int nBc,
			 bndPatch_struct **ppBndPatchBc )
{
  if ( !(*ppBndPatchBc) ) {
    /* Initialize. */
    if ( !pUns->ppRootPatchBc )
      /* No patch list, yet. */
      return ( 0 ) ;
    else if ( ( *ppBndPatchBc = pUns->ppRootPatchBc[nBc] ) )
      return ( 1 ) ;
    else
      /* No patch with this bc. */
      return ( 0 ) ;
  }
  else if ( ( *ppBndPatchBc = (*ppBndPatchBc)->PnxtBcPatch ) )
    /* There is another patch with this bc. */
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

/******************************************************************************

  loop_bndFaces_bc:
  Loop over all boundary faces of a given bc in a grid.
  
  Last update:
  ------------
  27Jan98: conceived.
  
  Input:
  ------
  pUns:    the unstructured grid
  ppChunk: the chunk
  

  Changes To:
  -----------
  ppChunk
  ppBndPatchFirst
  pnBndPatchFirst
  ppBndPatchLast
  pnBndPatchLast
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_bndFaces_bc ( const uns_s *pUns, const int nBc,
		       bndPatch_struct **ppBndPatchBc,
		       bndFc_struct **ppBndFcFirst, bndFc_struct **ppBndFcLast )
{
  if ( !loop_bndPatches_bc ( pUns, nBc, ppBndPatchBc ) ) return ( 0 ) ;

  *ppBndFcFirst = (*ppBndPatchBc)->PbndFc ;
  *ppBndFcLast = (*ppBndPatchBc)->PbndFc + (*ppBndPatchBc)->mBndFc - 1 ;
  
  return ( 1 ) ;
}

/******************************************************************************
  loop_bc_expr:   */

/*! Loop over all bcs matching a numerical or text expression.
 */

/*
  
  Last update:
  ------------
  9Jul13: conceived.
  

  Input:
  ------
  expr: pointer to the expression to match.

  Changes To:
  -----------
  **ppBc: the previous pBc visited.
    
  Returns:
  --------
  1 if another bc was found, 0 on termination.
  
*/

int loop_bc_expr ( bc_struct **ppBc, const char *expr ) {

  int isText = expr_is_text ( expr ) ;

  if ( !*ppBc ) 
    /* Init. */
    *ppBc = find_bc ( "", 0 ) ;
  else
    /* Next one. */
    *ppBc = (*ppBc)->PnxtBc ;

  while ( *ppBc ) {

    if ( ( isText && !fnmatch ( expr, (*ppBc)->text, 0 ) ) ||
         ( !isText && num_match( (*ppBc)->nr, expr ) ) ) {
      /* match .*/
      return (1) ;
    }

    /* Try the next. */
    *ppBc = (*ppBc)->PnxtBc ;
  }

  /* None left. */
  return ( 0 ) ;
}


/******************************************************************************
  loop_bc_expr:   */

/*! Loop over all bcs matching a numerical or text expression in an uns grid.
 */

/*
  
  Last update:
  ------------
  7aug20; fix bug with top index for mBc.
  9Jul13: conceived.
  

  Input:
  ------
  pUns: grid
  expr: pointer to the expression to match.

  Changes To:
  -----------
  piBc: position of the last bc in pUns->ppBc that was tested 
    
  Returns:
  --------
  pBc if match was found, Null otherwise.
  
*/

bc_struct *loop_bc_uns_expr ( uns_s *pUns, int *piBc, const char *expr ) {

  int isText = expr_is_text ( expr ) ;

  bc_struct *pBc ;
  if ( *piBc < -1 ) *piBc = -1 ; 
  while ( *piBc < pUns->mBc-1 ) {
    (*piBc)++ ;
    pBc = pUns->ppBc[*piBc] ;
    
    if ( ( isText && !fnmatch ( expr, pBc->text, 0 ) ) ||
         ( !isText && num_match( pBc->nr, expr ) ) ) {
      /* match .*/
      return ( pBc ) ;
    }
  }

  /* None left. */
  return ( NULL ) ;
}



/******************************************************************************
  loop_edges_face:   */

/*! Loop over all edges of a face, return the end nodes.
 *
 */

/*
  
  Last update:
  ------------
  6Jan15; change input arg type for ppVxFace to match get_uns_face.
  5Jan15; move to uns_loop from para_adapt's ref_utils.
  29Jun14; document interface, fix bug referencing *pkFace.
  13Jan13: conceived.
  

  Input:
  ------
  mVxFace: number of vx on the face
  ppVxFace: ordered list of pointers to the element vertices around the face 

  Changes To:
  -----------
  pkEg: pointer to the edge, incremented by one from input. 
         numbering runs 0...mEg-1

  Output:
  -------
  pnVx0/1: the two end-nodes of the edge
    
  Returns:
  --------
  0 if list of edges exhausted, 1 on success
  
*/

int loop_edges_face ( const int mVxFace, vrtx_struct **ppVxFace[], int *pkEg,
                      int *pnVx0, int *pnVx1 ) {


  *pkEg = MAX(-1,*pkEg ) ;
  (*pkEg)++ ;

  if ( mVxFace == 2 ) {
    // 2D. Only one edge=face.
    if ( *pkEg >= 1 )
      // No more faces left. 
      return ( 0 ) ;

    else {
      *pnVx0 = (*ppVxFace[0])->number ;
      *pnVx1 = (*ppVxFace[1])->number ;
      return ( 1 ) ;
    }
  }

  else {
    // 3D. 
    if ( *pkEg >=  mVxFace )
      // No more faces left. 
      return ( 0 ) ;

    else if ( *pkEg == mVxFace-1 ) {
      // Last edge. Perform cyclic rotation
      *pnVx0 = (*ppVxFace[*pkEg])->number ;
      *pnVx1 = (*ppVxFace[0])->number ;
      return ( 1 ) ;
    }
    else {
      *pnVx0 = (*ppVxFace[*pkEg])->number ;
      *pnVx1 = (*ppVxFace[*pkEg+1])->number ;
      return ( 1 ) ;
    }
  }
}
