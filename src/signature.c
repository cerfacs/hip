#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mdx.h"
#include "md5.h"

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

#include "hdf5.h"
#include "proto_hdf.h"

#ifdef __cplusplus
}
#endif

#define READ_CHUNK_SIZES    (16*1024*1024)


static  MD5_CTX md5_ctx;
static char digest_str[240];

void signature_start()
{
    MD5Init(&md5_ctx);
}

static char* hex_digits="0123456789abcdef";


void signature_stop()
{
    unsigned char digest[16];
    int i;
    
    MD5Final(digest,&md5_ctx);
    int p=0;
    
    /*Computes the actual hexadecimal string */
    for (i=0;i<16;i++)
    {
        unsigned char v=digest[i];
        
        digest_str[p++]=hex_digits[(v>>4) & 0xf];
        digest_str[p++]=hex_digits[v & 0xf];
        
    }
    
    digest_str[p++]=0;
}

void signature_addbuf(void* data,size_t data_len)
{
    #define MAX_CHUNK 512
    
    size_t chunk_size;
    unsigned char* buf=(unsigned char*)data;
    
    
    while(data_len>0)
    {
        chunk_size=data_len;
        
        if(chunk_size>MAX_CHUNK) chunk_size=MAX_CHUNK;
        
        MD5Update(&md5_ctx,buf,chunk_size);
        
        data_len-=chunk_size;
        buf+=chunk_size;
    }
    
}

void signature_add_int_vector(hid_t parent,const char* name)
{
    //get size of vector
//    size_t vec_tot_size=hdf_get_dset_size(parent,name);
    size_t vec_tot_size=h5_get_dset_size(parent,name);
    
    size_t data_left=vec_tot_size;
    size_t offset=0;
    size_t chunk_size;
    
    //alocate temp
    int* temp=(int*)malloc(READ_CHUNK_SIZES*sizeof(int));
    
    while(data_left>0)
    {
        chunk_size=data_left>READ_CHUNK_SIZES?READ_CHUNK_SIZES:data_left;
        
        //read data
        h5_load_int_hyperslab(parent,name,temp,offset,chunk_size);
        
        //update md5
        signature_addbuf(temp,sizeof(int)*chunk_size);
        
        offset+=chunk_size;
        data_left-=chunk_size;
    }
    
    //free temp
    free(temp);
    
}

void signature_add_dbl_vector(hid_t parent,const char* name)
{
    //get size of vector
    size_t vec_tot_size=h5_get_dset_size(parent,name);
    
    size_t data_left=vec_tot_size;
    size_t offset=0;
    size_t chunk_size;
    
    //alocate temp
    double* temp=(double*)malloc(READ_CHUNK_SIZES*sizeof(double));
    
    while(data_left>0)
    {
        chunk_size=data_left>READ_CHUNK_SIZES?READ_CHUNK_SIZES:data_left;
        
        //read data
        h5_load_double_hyperslab(parent,name,temp,offset,chunk_size);
        
        //update md5
        signature_addbuf(temp,sizeof(double)*chunk_size);
        
        offset+=chunk_size;
        data_left-=chunk_size;
    }
    
    //free temp
    free(temp);
    
}

#define VEC_TYPE_SCALAR   1
#define VEC_TYPE_INTEGER  0

typedef struct t_obj_for_signature
{
    char* parent;
    const char* name;
    int type;

}t_obj_for_signature;

t_obj_for_signature objects_for_signature[]={
{"Connectivity","tri->node",VEC_TYPE_INTEGER},
{"Connectivity","qua->node",VEC_TYPE_INTEGER},
{"Connectivity","tet->node",VEC_TYPE_INTEGER},
{"Connectivity","pyr->node",VEC_TYPE_INTEGER},
{"Connectivity","pri->node",VEC_TYPE_INTEGER},
{"Connectivity","hex->node",VEC_TYPE_INTEGER},
{"Coordinates","x",VEC_TYPE_SCALAR},
{"Coordinates","y",VEC_TYPE_SCALAR},
{"Coordinates","z",VEC_TYPE_SCALAR},
{NULL,NULL,0}
};

void signature_add_object(hid_t hfile,t_obj_for_signature* obj)
{
    
    if(!H5Lexists(hfile,obj->parent,H5P_DEFAULT))
    {
        return;
    }
    
//    hid_t parent=hdf_group_open(hfile,obj->parent);
//    hid_t parent = h5_open_group ( hfile, obj->parent ); 
    hid_t parent  = H5Gopen(hfile, obj->parent, H5P_DEFAULT ) ; 
    
    
    if(!H5Lexists(parent,obj->name,H5P_DEFAULT))
    {
        H5Gclose(parent);
        return;
    }
    
    switch(obj->type)
    {
        case VEC_TYPE_INTEGER:
            signature_add_int_vector(parent,obj->name);
            break;
        case VEC_TYPE_SCALAR:
            signature_add_dbl_vector(parent,obj->name);
            break;
    }
    
    H5Gclose(parent);
}

int signature(char *rootFile ) {

    //open mesh file with r/w access
    hid_t hfile=H5Fopen( rootFile, H5F_ACC_RDWR, H5P_DEFAULT);
//    hid_t hfile=hdf_open(rootFile,1);

    
    if(!H5Lexists(hfile,"Parameters",H5P_DEFAULT))
    {
        printf("hdf file has no 'Parameters' group, exit\n");
        H5Fclose(hfile);

        return 1;
    }

//    hid_t params_grp=hdf_group_open(hfile,"Parameters");
//    hid_t params_grp = h5_open_group ( hfile, "Parameters" ); 
    hid_t params_grp = H5Gopen(hfile, "Parameters", H5P_DEFAULT ) ; 

    if(H5Lexists(hfile,"md5_signature",H5P_DEFAULT))
    {
        printf("md5 signature already present\n");
        H5Fclose(hfile);
        return 0;
    }

    signature_start();


    int i;
    for(i=0;objects_for_signature[i].name!=NULL;i++)
    {
        signature_add_object(hfile,&objects_for_signature[i]);
    }

    signature_stop();

    h5_write_fxStr ( params_grp, "md5_signature", 1, fxStr240, digest_str ) ;

    H5Gclose(params_grp);
    
    H5Fclose(hfile);

    return 0;
}


