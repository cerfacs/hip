/*
  uns_specialTopo.c:
  Functions dealing with special topologies, such as axisymmetry. 
  
  Last update:
  ------------
  24Aug24; intro set_nxtSpecialTopo, 
           add pGrid to args of set_specialTopo
           make Grids writeable
  18Jun99; intro get/set_specialTopo.
  
  
  This file contains:
  -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern Grids_struct Grids ;
extern specialTopo_e specialTopo ;
extern const char topoString[][MINITXT_LEN] ;

specialTopo_e a2topo ( const char *topoString ) {

  char tstr[LINE_LEN] ;
  strncpy ( tstr, topoString, LINE_LEN ) ;
  tolowerstr ( tstr ) ;

  if ( !strncmp ( tstr, "notopo", LINE_LEN ) )
    return ( noTopo ) ;
  else if ( !strncmp ( tstr, "axi-x", LINE_LEN ) || !strncmp ( tstr, "axix", LINE_LEN )  )
    return ( axiX ) ;
  else if ( !strncmp ( tstr, "axi-y", LINE_LEN ) || !strncmp ( tstr, "axiy", LINE_LEN )  )
    return ( axiY ) ;
  else if ( !strncmp ( tstr, "axi-z", LINE_LEN ) || !strncmp ( tstr, "axiz", LINE_LEN )  )
    return ( axiZ ) ;
  else if ( !strncmp ( tstr, "nobc", LINE_LEN ) )
    return ( noBc ) ;
  else if ( !strncmp ( tstr, "surf", LINE_LEN ) )
    return ( surf ) ;
  else
    return ( unDef ) ;
}

void set_nxtSpecialTopo ( const char* topoString ) {

  specialTopo_e specialTopo = a2topo ( topoString ) ;
    
  if ( specialTopo >= noTopo && specialTopo <= surf )
    Grids.nxtSpecialTopo = specialTopo ;
  else {
    sprintf ( hip_msg, "unrecognised topo %d, resetting to noTopo",
              specialTopo ) ;
    hip_err ( warning, 0, hip_msg ) ;
    Grids.nxtSpecialTopo = noTopo ;
  }
  return ;
}

int set_specialTopo ( uns_s *pUns, const char *topoString ) {

  specialTopo_e specialTopo = a2topo ( topoString ) ;

  if ( pUns == NULL ) {
    if ( Grids.PcurrentGrid->uns.type != uns ) {
      /* Special topo only possible for unstructured grids. */
      return ( 1 ) ;
      // Use the current grid.
    }
    else
      pUns = Grids.PcurrentGrid->uns.pUns ;
  }
    
  
  else if ( specialTopo == unDef ) {
    // invalid topo
    sprintf ( hip_msg, "unrecognised topo %d, resetting", specialTopo ) ;
    hip_err ( warning, 0, hip_msg ) ;
    return (1) ;
  }

  pUns->specialTopo = specialTopo ;
  return ( 0 ) ;
}

specialTopo_e get_specialTopo ( ) {

  if ( !Grids.PcurrentGrid )
    return ( noTopo ) ;
  else if ( Grids.PcurrentGrid->uns.type != uns )
    /* Special topo only possible for unstructured grids. */
    return ( noTopo ) ;

  return ( Grids.PcurrentGrid->uns.pUns->specialTopo ) ;
}

const char *specialTopoString ( const uns_s *pUns ) {

  return ( topoString[ pUns->specialTopo ] ) ;
}


/******************************************************************************

  axis_verts:
  Make a list of vertices on a rotation axis.
  
  Last update:
  ------------
  5Sep15; only list axis verts for periodic bcs. In this way axis
          verts are not written e.g. when extruding to 360deg.
  5Jul11; deal with 2D.
  12Apr00; find axis verts also for annular cascades.
  : conceived.
  
  Input:
  ------
  pUns = grid

  Changes To:
  -----------
  pUns->Pchunk->pVrtx->singular

  Returns:
  --------
  number of singular vertices.
  
*/

int axis_verts ( uns_s *pUns, specialTopo_e axis ) {

  const int mDim = pUns->mDim ;
  const double zero[2] = {0.,0.} ;
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd, mVxAxis = 0 ;

  /* Reset all singularity flags. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      pVx->singular = 0 ;

  /* Mark all vertices "on" the x-Axis as singular. On means within epsOverlap
     of the x-Axis. */
  double distSq ;
  pChunk = NULL ;
  if ( pUns->mPerBcPairs && axis >= axiX && axis <= axiZ ) {
    /* Any rotation. */
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number ) {
          distSq = sq_distance_axis ( pVx->Pcoor, axis, mDim ) ;
          if ( distSq < Grids.epsOverlapSq ) {
            pVx->singular = 1 ;
            mVxAxis++ ;
          }
        }
  }

  return ( mVxAxis ) ;
}
