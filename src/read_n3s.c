/*
 read_uns_n3s.c:
 Read mesh connectivity, vertex coordinates, boundary information, solution.

  Last header update:
  ------------
  7Apr14; support variable size TAB, fix order of variable names, 
  6Apr13; move trim to meth.c
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  8Nov10; move def of av2n3s here.
  27Aug10: remove all those useless static declarations.
  25May07; fix guessing of varCat.
  8Dec07; intro fixed reference index in n3s sol file k_rho, k_u ...


 contains:
 ---------
 findIdeasTag
 read_ideas_grid
 read_ensight_master
 read_uns_n3s
 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[];
extern int check_lvl ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;
extern const double Gamma ;


/* A conversion list from n3s to AVBP numbering. 
   Also used in write_n3s. */
const int av2n3s[MAX_ELEM_TYPES][MAX_VX_ELEM] = { 
  {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,1,3,2},       /* tet */
  {0,1,2,3,4},      /* pyramids. */
  /*  {0,3,4,1,5,2},     prism */
  {0,3,4,1,5,2},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;



/* Order of relevant variables in n3s solution file:
               u,    v,    w,    p,    T,    sp,    rho,    cp,    gam 
in Fortran:    1,    2,    3,    4,    5,     8,     22,    28,     29 
  */
const int  k_u   =  0 ;
const int  k_v   =  1 ;
const int  k_w   =  2 ;
const int  k_p   =  3 ;
const int  k_T   =  4 ;
const int  k_sp  =  7 ;
const int  k_rho = 21 ;
const int  k_cp  = 27 ;
const int  k_gam = 28 ;



/* Advance the file pointer to the beginning of the next line in an ascii file. */
void nextline ( FILE *file ) {
  fscanf ( file, "%*[^\n]" ) ;
  fscanf ( file, "%*[\n]" ) ;
  return ;
}

/* Read a number of characters off a full binary line. */
int bReadNChar ( char *str, int n, FILE *Fle ) {

  int mBytes, nB=0, retVal=0 ;

  retVal = FREAD ( &mBytes, sizeof( int ), 1, Fle ) ;
  if ( retVal != 1 )
    return ( -1 ) ;

  if ( n > 0 ) {
    nB = MIN ( n*sizeof(char), mBytes ) ;
    retVal = FREAD ( str, sizeof(char), nB, Fle ) ;
  }

  /* How many bytes left on this line? */
  n = sizeof( int ) + MAX( 0, mBytes-nB*(int)sizeof( char) ) ;
  fseek ( Fle, n, SEEK_CUR ) ;  

  return ( retVal ) ;
}


/* Read a number of characters off a full binary line. */
int bReadNInt ( int* snt, int n, FILE *Fle ) {

  int mBytes, nB=0, retVal=0 ;

  FREAD ( &mBytes, sizeof( int ), 1, Fle ) ;  

  if ( n > 0 ) {
    n = MIN ( n, mBytes/sizeof( int ) ) ;
    retVal = FREAD ( snt, sizeof( int ), n, Fle ) ;
  }

  /* How many bytes left on this line? */
  nB = sizeof( int ) + MAX( 0, mBytes-n*(int)sizeof( int ) ) ;
  fseek ( Fle, nB, SEEK_CUR ) ;  

  return ( retVal ) ;
}


/* Read a number of characters off a full binary line. */
int bReadNDbl ( double* sdb, int n, FILE *Fle ) {

  int mBytes, nB=0, retVal=0 ;

  FREAD ( &mBytes, sizeof( int ), 1, Fle ) ;  

  if ( n > 0 ) {
    n = MIN ( n, mBytes/sizeof( double ) ) ;
    retVal = FREAD ( sdb, sizeof( double ), n, Fle ) ;
  }

  /* How many bytes left on this line? */
  nB = sizeof( int ) + MAX( 0, mBytes-n*(int)sizeof( double ) ) ;
  fseek ( Fle, nB, SEEK_CUR ) ;  

  return ( retVal ) ;
}


int bReadCol ( FILE *Fle, int *pnBc, char *pbcText, int *piEnt, int *pmFcBc ) {

  int mBytes, reqSize = 12*sizeof(char)+3*sizeof(int) ;

  FREAD ( &mBytes, sizeof( int ), 1, Fle ) ;  

  if ( mBytes < reqSize )
    return ( 0 ) ;

  FREAD ( pnBc, sizeof( int ), 1, Fle ) ;
  FREAD ( pbcText, sizeof( char ), 12, Fle ) ;
  FREAD ( piEnt, sizeof( int ), 1, Fle ) ;
  FREAD ( pmFcBc, sizeof( int ), 1, Fle ) ;


  /* How many bytes left on this line? */
  fseek ( Fle, 4+mBytes-reqSize, SEEK_CUR ) ;  

  return ( 4 ) ;
}


/* Locate a 'CRUBRIQUE=XXX' header where XXX is a given tag. */ 
int find_rubrique ( FILE* Fgrd, const char *tag, const int binFile ) {

  char t[LINE_LEN], ctag[LINE_LEN] ;
  int st, endStr = 0 ;
  int rewound = 0, mLine ;

  sprintf ( ctag, "CRUBRIQUE=%s", tag ) ;
  /* Add blanks to 12 characters to tag. n3s writes strings as 'A12'
     and uses embedded blanks. ;-( */
  for ( st = sizeof ( "CRUBRIQUE=" ) ; st < sizeof ( "CRUBRIQUE=" )+12 ; st++ ) {
    if ( ctag[st] == '\0' ) 
      endStr = 1 ;
    if ( endStr ) 
      ctag[st] = ' ' ;
  }
  ctag[st] = '\0' ;
  st-- ;

  for ( mLine = 1 ; 1 ; mLine++ ) {
    if ( binFile ) {
      if ( bReadNChar ( t, LINE_LEN, Fgrd ) < 0 ) {
        sprintf ( hip_msg, "header %s not found or eof in find_rubrique.", tag ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    } else {
      fgets ( t, 23, Fgrd ) ;
      if ( strlen( t ) == 22 ) nextline( Fgrd ) ;
    }

    if ( feof ( Fgrd ) ) {
      if ( rewound ) {
        /* Tag not in file. */
        sprintf ( hip_msg, "header %s not found in find_rubrique.", tag ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else {
        rewound = 1 ;
        rewind ( Fgrd ) ;
      }
    }

    if ( !strncmp ( ctag, t, st ) ) {
      /* Match. Skip two lines in an ascii file. */
      if ( !binFile ) {
        nextline( Fgrd ) ;
        nextline( Fgrd ) ;
      }
      return ( 1 ) ;
    }
  }
}


/* Read a number of int off an ascii line. */
int readNInt ( int *snt, const int n, FILE *Fl ) {

  int k ;

  /* Note that there is no provision to stop if a newline
     occurs before n items are read. */
  for ( k = 0 ; k < n ; k++ ) 
    fscanf ( Fl, "%d", snt+k ) ; 
  
  nextline( Fl ) ;

  return ( n ) ;
}

/* Read a number of doubles allowing exponential formats using a D. */
int readNdbl ( double *dbl, const int n, FILE *file ) {

  int i, trailing ;
  char str[TEXT_LEN], *pStr, c ;

  for ( i = 0 ; i < n ; i++ ) {
    /* Read up to the next space character or comma. */
    for ( c = ' ', pStr = str, trailing = 0 ; c ;  ) {
      c = fgetc ( file ) ;
      if ( isspace ( c ) || c == ',' || c == '\0' || c == '\n' ) {
        if ( trailing ) {
          /* Return a newline to the stream. */
          ungetc ( c, file ) ;
          c = '\0' ;
        }
      }       
      else if ( isdigit ( c ) || c == '.' || tolower ( c ) == 'e' )
        /* There will always be a digit to toggle to trailing whitespace. */
        trailing = 1 ;
      else if ( c == '+' || tolower ( c ) == '-' )
        ;
      else if ( tolower ( c ) == 'd' )
        /* Convert the d to an e. */
        c = 'e' ;


      if ( pStr-str >= TEXT_LEN )
        return ( i ) ;
      else 
        *pStr++ = c ;
    }

    if ( !sscanf ( str, "%lf", dbl+i ) )
      break ;
  }
  
  return ( i ) ;
}


/* Given a list of vertices on a face, find the matching face in 
   an element. */
int matchFcEl ( const elem_struct *pElem, int nFrmVx[], const int mVx ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;

  const vrtx_struct **ppVx = ( const vrtx_struct ** )pElem->PPvrtx ;
  
  int mFc = pElT->mSides, kFc, kd, k, mVxFc, nF2Vx[MAX_VX_FACE] ;

  /* n3s gives some faces in lexicographic, some in circumferential order.
     Order them lexicographically. */
  qsort ( nFrmVx, mVx, sizeof( int ), cmp_int ) ;
  

  for ( kFc= 1 ; kFc <= mFc ; kFc++ ) {
    pFoE = pElT->faceOfElem + kFc ;
    
    mVxFc = pFoE->mVertsFace ;
    kVxFace = pFoE->kVxFace ;

    if ( mVxFc == mVx ) {
      /* Compare vertices. Match first nFrmVx[0], then follow around. */
      for ( kd = 0 ; kd < mVx ; kd++ ) 
        if ( ppVx[ kVxFace[kd] ]->number == nFrmVx[0] ) {
          /* Now match the rest of the sequence. */
          for ( k = 1 ; k < mVx ; k++ )
            nF2Vx[k] = ppVx[ kVxFace[ (kd+k)%mVx ] ]->number ;

          qsort ( nF2Vx+1, mVx-1, sizeof( int ), cmp_int ) ;
          
          for ( k = 1 ; k < mVx ; k++ )
            if ( nF2Vx[k] != nFrmVx[k] )
              break ;

          if ( k == mVx )
            /* Match. */
            return ( kFc ) ;

          break ;
        }
    }
    
  }


  sprintf ( hip_msg, "in matchFcEl: no face of element %"FMT_ULG":\n formed by:", 
           pElem->number ) ;

  for ( k = 0 ; k < pElT->mVerts ; k++ )
    sprintf ( hip_msg, " %"FMT_ULG",", ppVx[k]->number ) ;
  sprintf ( hip_msg, "\n          matches the face formed by:\n" ) ; 
  for ( k = 0 ; k < mVx ; k++ )
    sprintf ( hip_msg, " %d,", nFrmVx[k] ) ;
  hip_err ( warning, 0, hip_msg ) ;

  return ( 0 ) ;
}

/******************************************************************
 
 read_n3s_grid:
 Read mesh connectivity, vertex coordinates, boundary information.

 Last update:
 ------------
  1Jul16; new interface to check_uns.
 9Jul11; call init_elem ; 
 28Jun07; extract make_uns_bndPatch.
 06Jan05; conceived.

 Input:
 ------
 Fgrd: opened grid file
 pUns: initialised grid container.

 Changes to:
 -----------
 pUns

 Returns:
 --------
 
*/

int read_n3s_grid ( FILE *File, uns_s *pUns ) {
  
  fpos_t fp0 ;

  grid_struct *pGrid ;
  chunk_struct *pChunk ;

  int someInt[14], mVx=0, mEl=0, mEl2Vx=0, mBndFc=0, mFc=0, mBc=0, m, iElType, mVxEl, 
      binFile, k, mDim, nEl, nVx, kVx, nFrmVx[MAX_VX_ELEM], mVxFc, kBc, nBc, iEnt, 
      mFcBc, mBytes, kFc, nFc, mCol ;
  vrtx_struct *pVx, **ppVx ;
  elem_struct *pEl ; 
  double *pCo ;

  char bcText[TEXT_LEN], someLine[TEXT_LEN] ;
  bndFc_struct *pBf ;
  bc_struct *pBc ;

 
  /* If the first int in the file is '0', this file is ASCII,
     otherwise it's binary. */
  fread ( someLine, sizeof( char ), 1, File ) ;  
  binFile = ( someLine[0] == '0' ? 0 : 1 ) ;
  if ( binFile ) rewind ( File ) ;
  
  /* This will be the next grid. */
  if ( verbosity > 2 && binFile )
    sprintf ( hip_msg, "  Reading binary n3s grid file." ) ;
  else if ( verbosity > 2 )
    sprintf ( hip_msg, "  Reading ascii n3s grid file." ) ;  
  hip_err ( blank, 2, hip_msg ) ;



  /* Header: */
  if ( !find_rubrique ( File, "EN-TETE", binFile ) ) return ( 0 ) ;
  for ( k = 0 ; k < 13 ; k++ ) 
    if ( binFile )
      bReadNInt ( someInt+k, 1, File ) ;
    else
      readNInt ( someInt+k, 1, File ) ;





  mDim   = someInt[0] ;
  mVx    = someInt[2] ;
  mEl    = someInt[3] ;
  mBndFc = someInt[4] ;
  mFc    = someInt[9] ;
  mCol   = someInt[11] ;


  /* Allocate the fields of which the sizes are known by now. */
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "   Dimension:                    %-d", mDim ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "   Number of elems:              %-d", mEl ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "   Number of vertices:           %-d", mVx ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "   Number of boundary faces:     %-d", mBndFc ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "   Number of surface elements:   %-d", mFc ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "   Number of colors:             %-d", mCol ) ; }
    hip_err ( blank, 2, hip_msg ) ;



  pUns->mDim = mDim ;
  /* Not sure whether there is a solution. */
  pUns->varList.mUnknowns = pUns->varList.mUnknFlow = 0 ;
  pUns->varList.varType = noVar ;



  
  /* Read element connectivity to find out how many element to node pointers
     are needed.  */
  fgetpos ( File, &fp0 ) ;

  if ( !find_rubrique ( File, "ELEMENT V", binFile ) ) return ( 0 ) ;

  mEl2Vx = 0 ;
  for ( nEl = 1 ; nEl <= mEl ; nEl++ ) {
    if ( binFile ) {
      bReadNInt ( &iElType, 1, File ) ;
    }
    else {
      fscanf ( File, "%*d%d", &iElType ) ;
      nextline ( File ) ;
    }
    mVxEl = iElType/100 ;
    mEl2Vx += mVxEl ;
  }

  fsetpos ( File, &fp0 ) ;



  /* Number of boundary faces is not known so far. */
/*  pChunk = append_chunk( pUns, pUns->mDim, mEl, mEl2Vx, 0, mVx, mBndFc, 0 ) ;*/
  pChunk = append_chunk( pUns, pUns->mDim, mEl, mEl2Vx, 0, mVx, mFc, 0 ) ;











  
  /* Read vertex coordinates. */
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "       Reading %d  nodes.", mVx ) ;
    hip_err ( blank, 3, hip_msg ) ;
  }
  if ( !find_rubrique ( File, "NOEUDS", binFile ) ) return ( 0 ) ;

  /* Note: Pcoor starts with index 1, ie. the first set of nDim coordinates skipped. */	
  for ( pCo = pChunk->Pcoor+mDim, pVx = pChunk->Pvrtx+1, nVx = 1 ;
	nVx <= mVx ; nVx++, pVx++, pCo += mDim ) {
    pVx->Pcoor = pCo ;
    pVx->Punknown = NULL ;
    pVx->number = nVx ;
    
    if ( binFile ) {
      m = bReadNDbl ( pCo, mDim, File ) ;
    }
    else {
      /* ICEM writes in an exponential format like 1.23D+00 which fscanf can't parse. */
      fscanf ( File, "%*d" ) ;  
      m = readNdbl ( pCo, mDim, File ) ;
      nextline ( File ) ;
    }

    if ( m != mDim ) {
      sprintf ( hip_msg, "could not read coordinates for node %d"
                " in read_n3s_grid.", 
               nVx ) ;
      hip_err ( fatal, 0, hip_msg ) ; 
    }
  }




  
  /* Read element connectivity.  */
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "       Reading %d  elements.", mEl ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }

  if ( !find_rubrique ( File, "ELEMENT V", binFile ) ) return ( 0 ) ;

  pEl = pChunk->Pelem ;
  ppVx = pChunk->PPvrtx ;
  pVx = pChunk->Pvrtx ;

  for ( nEl = 1 ; nEl <= mEl ; nEl++ ) {
    if ( binFile ) {
      m = bReadNInt ( someInt, 1+MAX_VX_ELEM, File ) ;
      iElType = someInt[0] ;
      mVxEl = iElType/100 ;
      for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) 
        nFrmVx[kVx] = someInt[1+kVx] ;
    }
    else {
      fscanf ( File, "%*d%d", &iElType ) ;
      mVxEl = iElType/100 ;
      for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) 
        fscanf ( File, "%d", nFrmVx+kVx ) ;
      nextline ( File ) ;
    }
    
#   ifdef CHECK_BOUNDS
      if ( ppVx + mVxEl - 1 > pChunk->PPvrtx + mEl2Vx - 1 ) {
        sprintf ( hip_msg, "more connectivity pointers found than counted." ) ;
        exit ( EXIT_FAILURE ) ; }
#   endif

    pEl++ ;
    elType_e elType = ( mDim == 2 ?  (elType_e) ( tri + mVxEl-3 ) : 
                                ( mVxEl == 8 ? hex : (elType_e) (tet + mVxEl-4) ) ) ;

    init_elem ( pEl, elType, nEl, ppVx ) ;
    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) 
      *(ppVx++) = pVx + nFrmVx[ av2n3s[ pEl->elType ][ kVx ] ] ;
  } 



  


  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "       Reading %d boundary faces.", mBndFc ) ;
    hip_err( blank, 2, hip_msg ) ;
  }

  if ( !find_rubrique ( File, "ELEMENT B", binFile ) ) return ( 0 ) ;

  pChunk = pUns->ppChunk[0] ;


  for ( pBf = pChunk->PbndFc+1 ; pBf <= pChunk->PbndFc + mBndFc ; pBf++ ) {
    if ( binFile ) {
      m = bReadNInt ( someInt, 2+MAX_VX_FACE, File ) ;
      nEl = someInt[0] ;
      mVxFc = someInt[1]/100 ;
      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) 
        nFrmVx[kVx] = someInt[2+kVx] ;
    }
    else {
      fscanf ( File, "%*d%d%d", &nEl, &iElType ) ;
      mVxFc = iElType/100 ;
      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) 
        fscanf ( File, "%d", nFrmVx+kVx ) ;
      nextline ( File ) ;
    }

    pBf->Pelem = pChunk->Pelem + nEl ;
    pBf->nFace = matchFcEl ( pBf->Pelem, nFrmVx, mVxFc ) ;
  }











  if ( verbosity > 3 )
    hip_err ( blank, 3, "       Reading boundary conditions." ) ;
  if ( !find_rubrique ( File, "COULEUR", binFile ) ) return ( 0 ) ;
    
  for ( kBc = 0 ; kBc < mCol ; kBc++ ) {
    if ( binFile )
      m = bReadCol ( File, &nBc, bcText, &iEnt, &mFcBc ) ;
    else {
      fgets ( someLine, TEXT_LEN, File ) ;
      m = sscanf ( someLine, "%d %12c %d %d", &nBc, bcText, &iEnt, &mFcBc ) ;
    }
    
    if ( m != 4 ) {
      sprintf ( hip_msg, "could not read all params for bc %d"
                " in read_n3s_grid.", kBc+1 ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }


    if ( nBc != 0 && iEnt == 3 ) {
      /* Faces. */ 
      bcText[12] = '\0' ;
      if ( !( pBc = find_bc ( bcText, 1 ) ) ) {
        sprintf ( hip_msg, " could not add boundary %s"
                  " in read_n3s_grid.", bcText ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      mBc++ ;
      
      /*Read the list of faces. */
      if ( binFile ) {
        FREAD ( &mBytes, sizeof( int ), 1, File ) ;
        if ( mBytes < mFcBc*sizeof( int ) ) {
          sprintf ( hip_msg, " only %d faces listed, but %d faces expected for"
                    " boundary %d", mBytes/( (int) sizeof(int)), mFcBc, nBc ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }
      }

      for ( kFc = 0 ; kFc < mFcBc ; kFc++ ) {
        if ( binFile ) 
          FREAD ( &nFc, sizeof(int), 1, File ) ;
        else
          fscanf ( File, "%d", &nFc ) ;
      
        pChunk->PbndFc[nFc].Pbc = pBc ;
      }
      if ( binFile )
        fseek ( File, sizeof ( int ) + mBytes-mFcBc*sizeof(int), SEEK_CUR ) ;
      else
        nextline ( File ) ;

    }
    else {
      /* Skip. */
      if ( binFile ) {
        FREAD ( &mBytes, sizeof( int ), 1, File ) ;
        fseek ( File, mBytes+sizeof(int), SEEK_CUR ) ;
      }
      else {
        for ( kFc = 0 ; kFc < mFcBc ; kFc++ )
          fscanf ( File, "%d", &nFc ) ;
        nextline ( File ) ;
      }
    }
  }




  pChunk->mBndFaces = mBndFc ;
  pChunk->mBndPatches = mBc ;
  pUns->mBc = mBc ;

  make_uns_bndPatch ( pUns ) ;

  number_uns_grid ( pUns ) ;

  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;

  
  /* Make a new grid. */
  if ( !( pGrid = make_grid () ) )
    free_chunk ( pUns, &pChunk ) ;
  else {
    /* Put the chunk into Grids. */
    pGrid->uns.type = uns ;
    pGrid->uns.pUns = pUns ;
    pGrid->uns.mDim = mDim ;
    pGrid->uns.pVarList = &(pUns->varList) ;
    pUns->nr = pGrid->uns.nr ;
  }
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  
  return ( 1 ) ;
}



/******************************************************************************

  primTCpG2Cons:
  convert u,v,[w],p,T,cp,Gamma (both variable) to cons.
  
  Last update:
  ------------
  14Apr08: derived from primT2Cons.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void primTCpG2cons( double primVar[], double consVar[], const int mDim,
                    int k_cp, int k_gam ) {
  /* Convert primitiveT (uvwpT) to conservative. */
  double u,v,w,p,T,rho,q, cp, gam_loc, gamM1_loc, R_loc ;

  u = primVar[0] ;
  v = primVar[1] ;

  if ( mDim == 3 ) {
    w = primVar[2] ;
    p = primVar[3] ;
    T = primVar[4] ;
    q = u*u + v*v + w*w ;
  }
  else {
    p = primVar[2] ;
    T = primVar[3] ;
    q = u*u + v*v ;
  }
  
  cp = primVar[k_cp] ;
  gam_loc = primVar[k_gam]-1. ;   
  gamM1_loc = primVar[k_gam]-1. ;   
  
  R_loc = cp*(1.-1./gam_loc) ; 
  rho = p/R_loc/T ;


  consVar[0] = rho ;
  consVar[1] = u*rho ;
  consVar[2] = v*rho ;

  if ( mDim == 3 ) {
    consVar[3] = w*rho ;
    consVar[4] = p/gamM1_loc + q ;
  }
  else {
    consVar[3] = p/gamM1_loc + q ;
  }
  
  return ;
}

/******************************************************************************

  read_n3s_sol:
  Read an n3s solution. These files are all binary.
  
  Last update:
  ------------
  7Apr14; fix too short string length in strncpy
  7Apr14; assume there is one variable name for each scalar var. record.
  5Apr14; test on the length of the second record. If short, assume variable size TAB.
  3Apr14; N3S v4. some change to how variables are written, needs reverse engineering
          as the format documentation is unbelievably crap.
          changes to k_rho, k_cp, k_gamma, the numers in the file are not what's in the doc.
  April10: GS: read header to distinguish beteween format variants, read new format
  20Mar10; write primitive vars for multi-species cases, conversion is bugged.
  25May09; pass keyword in for n3sa,
           read all solution fields under n3sa.
  24May09; check for matching start/end recordlenghts.
  4Apr08; replace varTypeS with varList.
  14Apr08; convert to cons using variable cp and gamma within read_n3s_sol.
  12Apr08; read post-proc variables if mUnknPost != 0 
  8Dec07; fix bug in counting number of unknowns for single-species case.
          intro fixed reference index in n3s sol file k_rho, k_u ...
          reorganise variable repacking.
  14 Sep 06: conceived.
  
  Input:
  ------
  Fsol: solution filename
  pUns: 

  Changes To:
  -----------
  pUns
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_n3s_sol ( FILE *File, uns_s *pUns, const char *keyword ) {


  /* Record 1: header. 
   -------------------*/
  int rlen ;
  FREAD ( &rlen, sizeof(int), 1, File ) ;
  /* Require at least the records up to iBidon. */
  int szBidon = 100 ;
  int slen = 80 + 3*sizeof(double) + 8*sizeof(int) + szBidon*(sizeof(int)+sizeof(double)) ;
  if ( rlen < slen ) {
    sprintf ( hip_msg, "header record too short in read_n3s_sol:" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* These are not used. */
  char someChar[80] ;
  double tstart, crank, cfl ;
  int isol, idefor, nstart ;
  FREAD ( someChar, sizeof (char), 80, File ) ;
  FREAD ( &tstart, sizeof(double), 1, File ) ;
  FREAD ( &crank, sizeof(double), 1, File ) ;
  FREAD ( &cfl, sizeof(double), 1, File ) ;
  FREAD ( &isol, sizeof(int), 1, File ) ;
  FREAD ( &idefor, sizeof(int), 1, File ) ;
  FREAD ( &nstart, sizeof(int), 1, File ) ;

  /* These are relevant. */  
  int mVerts, mElems, mDim, mUnknFile, mUnknPost ;
  FREAD ( &mVerts,    sizeof(int), 1, File ) ;
  FREAD ( &mElems,    sizeof(int), 1, File ) ;
  FREAD ( &mDim,      sizeof(int), 1, File ) ;
  FREAD ( &mUnknFile, sizeof(int), 1, File ) ;
  FREAD ( &mUnknPost, sizeof(int), 1, File ) ;

  /* N3s doc v4: provides a variable length of the 'bidon' arrays.
     But this is not present in the provided files. 
  if ( rlen >= slen+sizeof(int) ) {
    slen += sizeof(int) ;
    FREAD ( &szBidon, sizeof(int), 1, File ) ;
  } */

  /* Need to read iBidon to get VERSION of file, Skip RBION 100*double*/ 
  int iBidon[100];
  double rBidon[100];
  FREAD ( &rBidon, sizeof(double), szBidon, File ) ;
  FREAD ( &iBidon, sizeof(int),    szBidon, File ) ;

  /* Skip the character array and any rest. */
  fseek ( File, rlen-slen, SEEK_CUR ) ;

  int rlen_end ;
  FREAD ( &rlen_end, sizeof(int), 1, File ) ;
  if ( rlen_end != rlen ) {
    sprintf ( hip_msg, "corrupted binary file?\n"
              "record length mismatch in header in read_n3s_sol:\n"
              "      start: %d, end %d", rlen, rlen_end ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  sprintf ( hip_msg, "Found  %d Variables and %d Post procs Vars",
            mUnknFile, mUnknPost );
  hip_err ( info, 0, hip_msg ) ;

  if ( mVerts != pUns->mVertsNumbered ||
       mDim   != pUns->mDim ) {
    sprintf ( hip_msg, "dimension mismatch between grid and solution:\n"
              "   Dimension:                    %-d\n"
              "   Number of vertices:           %-d", mDim, mVerts ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Get version from file to set format */ 
  int version = iBidon[0];

  /* Check the record length of the 2nd line. If short, then this
     should be two ints giving the dimension of TAB. */
  FREAD ( &rlen, sizeof(int), 1, File ) ;
  int tabTypeVariable=-1, nV, nD ;
  if ( rlen >= 2800 ) {
    tabTypeVariable = 0 ;
    nV = 100 ;
    nD = 4 ; 
    pUns->restart.any.iniSrc =  n3s_v2 ;

    sprintf ( hip_msg, 
              "Reading solution file version %d, assuming fixed size TAB(100,4)",
              version );
    hip_err ( info, 2, hip_msg ) ;
  }  
  else {
    tabTypeVariable = 1 ;
    pUns->restart.any.iniSrc =  version ;
    /* Record 2: Data size and number of var info if version is right */
    FREAD ( &nV, sizeof(int), 1, File ) ; /* Nb of VARS */
    FREAD ( &nD, sizeof(int), 1, File ) ; /* Nb of DIMS + 2 ?*/
    /* Skip the remaining ones. */
    fseek ( File, rlen-2*sizeof(int), SEEK_CUR ) ;
    FREAD ( &rlen_end, sizeof(int), 1, File ) ;

    if ( rlen_end != rlen ) {
      sprintf ( hip_msg, "corrupted binary file?\n"
                "record length mismatch in table size in read_n3s_sol:\n"
                "      start: %d, end %d", rlen, rlen_end ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    sprintf ( hip_msg, 
              "Reading solution file version %d, assuming variable size TAB(100,4)",
              version );
    hip_err ( info, 2, hip_msg ) ;

    /* Read rlen for next record, for sync with files who don't have this line. */
    FREAD ( &rlen, sizeof(int), 1, File ) ;
  }
  



  /* Record 3: Table of variable types.
   -------------------*/
  // No need to consider any variables in TAB beyond mKV.
#define N3S_MKV (29) 
  const int mKV=N3S_MKV ;
  slen = nV*nD*sizeof(int)+N3S_MKV*12 ;
  if ( rlen < slen ) {
    sprintf ( hip_msg, "variable table too short in read_n3s_sol:" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( nD < 3 ) {
    sprintf ( hip_msg, "expected at least 3 elements in variable table in read_n3s_sol, found %d",
              nD ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  typedef struct {
    int keepVar; int iPresence; int iType; int iVarDim; char varName[13];
  } varTab_s ;
  varTab_s *pVarTab, *pVT ;
  pVarTab = arr_malloc ( "pVarTab in read_n3s_sol", pUns->pFam,
                         nV*nD, sizeof( varTab_s ) ) ;
  int *iData, *iD ;
  if ( tabTypeVariable == 0 ) {
    // Old version, fixed to 4*100 or similar, with first index varying first.
    iData = arr_malloc ( "iData in read_n3s_sol", pUns->pFam, nV, sizeof( int ) ) ;

    FREAD ( iData, sizeof(int), nV, File ) ;
    for ( pVT = pVarTab, iD = iData ; pVT < pVarTab+nV ; pVT++, iD++ )
      pVT->iPresence = *iD ; 

    FREAD ( iData, sizeof(int), nV, File ) ;
    for ( pVT = pVarTab, iD = iData ; pVT < pVarTab+nV ; pVT++, iD++ )
      pVT->iType =  *iD ;

    FREAD ( iData, sizeof(int), nV, File ) ;
    for ( pVT = pVarTab, iD = iData ; pVT < pVarTab+nV ; pVT++, iD++ )
      pVT->iVarDim =  *iD ;


    /* Skip the remaining ones. */
    fseek ( File, nV*(nD-3)*sizeof(int), SEEK_CUR ) ;

    arr_free ( iData ) ;
  }
  else if ( tabTypeVariable == 1 ) {
    // Written with second index of TAB varying first.
    iData = arr_malloc ( "iData in read_n3s_sol", pUns->pFam, nD, sizeof( int ) ) ;

    for ( pVT = pVarTab ; pVT < pVarTab+nV ; pVT++ ) {
      FREAD ( iData, sizeof(int), nD, File ) ;
      pVT->iPresence=iData[0] ; 
      pVT->iType    =iData[1] ; 
      pVT->iVarDim  =iData[2] ; 
    }

    arr_free ( iData ) ;
  }
  else {
    sprintf (hip_msg,"unknown n3s version number %d in read_n3s_sol",version) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Variable names seem to be written for each variable present, 
     no interest in variables beyond MKV. */
  char *n3sVarNames = arr_malloc ("n3sVarNames in read_n3s_sol", 
                                   pUns->pFam, N3S_MKV*12+1, sizeof( char )  ) ;
  fread ( n3sVarNames, 12, N3S_MKV, File ) ; 
  

  /* Skip the rest of the solution variables. */
  fseek ( File, rlen-slen, SEEK_CUR ) ;

  FREAD ( &slen, sizeof(int), 1, File ) ;
  if ( slen != rlen ) {
    sprintf ( hip_msg, "corrupted binary file?\n"
              "      record length mismatch in variable desc. (rec 2) in read_n3s_sol:\n"
              "      start: %d, end %d", rlen, slen ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  /* Record 4/5: Post proc record.
     -------------------*/
  if ( mUnknPost != 0 ) {
    /* Skip the post-processing record. */
    if ( tabTypeVariable == 1 ) {
      FREAD ( &rlen, sizeof(int), 1, File ) ;
      fseek ( File, rlen, SEEK_CUR ) ;
      FREAD ( &rlen, sizeof(int), 1, File ) ;
    }
    FREAD ( &rlen, sizeof(int), 1, File ) ;
    fseek ( File, rlen, SEEK_CUR ) ;
    FREAD ( &slen, sizeof(int), 1, File ) ;
    if ( slen != rlen ) {
      sprintf ( hip_msg, "corrupted binary file in read_n3s_sol?\n"
                "         record length mismatch in post-proc var descr (rec 3)\n"
                "         start: %d, end %d", rlen, slen ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }







  /* Read variables. */
  int is_multi_species ;
  if ( !strncmp( keyword, "n3sa", 4 ) ) {
    /* Read all variables. */
    for ( pVT = pVarTab ; pVT < pVarTab+nV ; pVT++ )
      pVT->keepVar = pVT->iPresence ;

    is_multi_species = pVarTab[k_sp].iPresence ;
  }
  else {
    /* Initialise the variables to read. */
    for ( pVT = pVarTab ; pVT < pVarTab+nV ; pVT++ )
      pVT->keepVar = 0 ;

    pVarTab[k_u]  .keepVar = 1 ; /* u velocity */
    pVarTab[k_v]  .keepVar = 1 ; /* v velocity */
    pVarTab[k_w]  .keepVar = ( mDim == 2 ? 0 : 1 ) ; /* w velocity */
    pVarTab[k_p]  .keepVar = 1 ; /* pressure */
    pVarTab[k_T]  .keepVar = 1 ; /* temperature */
    pVarTab[k_rho].keepVar  = 1 ; /* Density. */


    /* Is this a multi-species case? */
    if ( pVarTab[k_sp].iPresence ) {
      /* This is a multi-species file.  */
      is_multi_species = 1 ;
      pVarTab[k_sp].keepVar  = 1 ; /* species. */
      //     keepVar[k_cp]  = 1 ; /* cp */
      //     keepVar[k_gam] = 1 ; /* Gamma */
    }
    else {
      /* Mono-species. */
      is_multi_species = 0 ;
    }
  }


  /* Select and count the variables to extract from the file. */
  int k ;
  int mUn = 0 ;
  for ( pVT = pVarTab, k=0 ; pVT < pVarTab+nV ; pVT++, k++ )
    if ( pVT->keepVar && !pVT->iPresence ) {
      pVT->keepVar = 0 ;

      sprintf ( hip_msg, "variable %d: '%s', is not present"
                "         in read_n3s_sol.", k+1, pVT->varName ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
    else if ( pVT->keepVar ) {
      mUn = mUn + pVT->iVarDim ;
    }



      

  /* Malloc for the unknowns. */
  chunk_struct *pChunk = pUns->pRootChunk ;
  pChunk->Punknown = arr_malloc ( "Pchunk->Punknown in read_n3s_sol", pUns->pFam,
                                  ( mVerts + 1 )*(mUn), sizeof( double ) ) ;

  /* Set the vrtx->unknown pointer. */ 
  vrtx_struct *pVx ;
  double *pUnk ;
  for ( pVx = pChunk->Pvrtx+1, pUnk = pChunk->Punknown + mUn ;
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; 
        pVx++, pUnk += mUn )
    pVx->Punknown = pUnk ;


  varList_s *pVL = &pUns->varList ;
  int mUnAlloc = pVL->mUnknowns = mUn ;




  /* Records 3-N: solution variables.
   -------------------*/

  /* Copy all unknowns in the order on the file, then repack. */
  int kTab ; // fixed position of var in TAB.
  int kVF = -1 ; //  position of var in file (not in TAB).
  int kVL = -1 ; // position of var in varList. 
  int iDim ; // component of vector var.
  char tmpname[LEN_VARNAME] ;
  var_s *pVar ;
  for ( kTab = 0 ; kTab < mKV ; kTab++ ) {
    pVT = pVarTab + kTab ;
    if ( pVT->iPresence ) {
      /* Variable listed in the file, either read or skip this one. */
      for ( iDim = 0 ; iDim < pVT->iVarDim ; iDim++ ) {
        kVF++ ;

        /* Each vector component is apparently given on a separate line. */
        FREAD ( &rlen, sizeof(int), 1, File ) ;
        slen = mVerts*sizeof(double) ;
        if ( rlen < slen ) {
          sprintf ( hip_msg, "variable record %d too short"
                    " in read_n3s_sol:", k+1 ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }
      
        if ( !pVT->keepVar ) {
          /* Skip. */
          fseek ( File, rlen+sizeof(int), SEEK_CUR ) ;
        }
        else {
          /* Extract variable name. */
          kVL++ ;
          pVar = pVL->var+kVL ;

          strncpy ( tmpname, n3sVarNames+kVF*12, 12 ) ;
          tmpname[13] ='\0' ;
          trim(tmpname) ;
          strncpy ( pVar->name, tmpname, 12 ) ;
          
          if ( verbosity > 2 ) {
            sprintf ( hip_msg, "extracting variable %12s", pVar->name ) ;
            hip_err ( info, 4, hip_msg ) ;
          }

          /* Read and copy into unknowns. */
          for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx + pChunk->mVerts ; 
                pVx++ )
            FREAD ( pVx->Punknown+kVL, sizeof(double), 1, File ) ;
  
          /* Skip the rest. */
          fseek ( File, rlen-slen+sizeof(int), SEEK_CUR ) ;
        }
      }
    }
  }

  arr_free ( n3sVarNames ) ;

  pVL->mUnknFlow = mDim+2 ;
  pVL->varType = prim ;
  /* DEACTIVATED PRIM2CONS by GABRIEL STAFFELBACH */
  
  /* Convert to conservative variables. */
  
  //if ( is_multi_species ) {
  //  /* 1.28.2: Keep primitive variables, as the primtTcpG2cons does not work. */
  //  pVL->varType = prim ;
  //  /* multi-sp. Use supplied cp and gamma. This routine should end up in state_conv.
  //  pVL->varType = cons ;
  //  for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ )
  //    primTCpG2cons( pVx->Punknown, pVx->Punknown, mDim, k_cp, k_gam ) ;*/
  //}
  //else {
  //  /* Mono-species, use fixed gamma and R. */
  //  pVL->varType = cons ;
  //  for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ )
  //    primT2cons( pVx->Punknown, pVx->Punknown, mDim ) ;
  //  
  //  strncpy ( pVar[mDim+1].name, "rhoE", LEN_VARNAME ) ;
  //  strncpy ( pVar[mDim].name,   "rhow", LEN_VARNAME ) ;
  //  strncpy ( pVar[2].name,      "rhov", LEN_VARNAME ) ;
  //  strncpy ( pVar[1].name,      "rhou", LEN_VARNAME ) ;
  //  strncpy ( pVar[0].name,      "rho",  LEN_VARNAME ) ;
  //}
  

  /* Variable categories. */

/* Remove all other groups than add
  for ( k = 0 ; k <  mDim+2 ; k++ )
    pVar[k].cat = ns ;
*/
  /* The rest is additional. */
/*  for ( ; k < mUn ; k++ )*/
  for ( kVL=0 ; kVL < pVL->mUnknowns ; kVL++ ) {
    pVL->var[kVL].cat = add ;
    strcpy ( pVL->var[kVL].grp,"add");
    }

  /* Unless: */
  
/*  if ( is_multi_species ) {
    for ( k= mDim+4 ; k < mDim+4 + iVarDim[k_sp] ; k++ )
      pVar[k].cat = species ;
  }
*/  
  

  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 

  return ( 1 ) ;
}




/******************************************************************
 
 read_uns_n3s.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution in n3s format.

 Last update:
 ------------
 19Dec17; new interface to make_uns.
 25May09; pass keyword through for n3sa
          use full char length of LINE_LEN in r1_fopen.
 12Sep06; add solution.
 06Jan05; conceived

 Input:
 ------
 gridFile:  grid file
 solFile:   solution file
 keyword: 

 Returns:
 --------
 1
*/

int read_uns_n3s ( char *gridFile, char *solFile, const char* keyword ) {
  
  FILE *Fgrd, *Fsol=NULL ;
  uns_s *pUns ;

  /* Allocate an unstructured grid. */
  if ( !( pUns = make_uns ( NULL ) ) ) {
    sprintf ( hip_msg, " failed to alloc an unstructured grid in read_uns_n3s." ) ;
    hip_err ( fatal, 0, hip_msg ) ; }


  
  prepend_path ( gridFile ) ;
  if ( solFile[0] ) prepend_path ( solFile ) ;

  /* Is it a binary file? */
  if ( !( Fgrd = r1_fopen ( gridFile, LINE_LEN, "r" ) ) ) {
    sprintf ( hip_msg, " could not open %s", gridFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; }


  if ( !read_n3s_grid ( Fgrd, pUns ) ) {
    sprintf ( hip_msg, " failed to read grid in read_uns_n3s." ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  

  /* Is it a binary file? */
  if ( solFile[0] && !( Fsol = r1_fopen ( solFile, LINE_LEN, "r" ) ) ) {
    sprintf ( hip_msg, " could not open %s", solFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; }


  if ( Fsol && !read_n3s_sol ( Fsol, pUns, keyword ) ) {
    sprintf ( hip_msg, " failed to read sol in read_uns_n3s." ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  
  fclose ( Fgrd ) ;
  if ( Fsol ) fclose ( Fsol ) ;
  

  return ( 1 ) ;
}


