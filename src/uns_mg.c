/*
  uns_mg.c:
  Multigrid coarsening for unstructured meshes.
  
  Last update:
  ------------
  21Apr17: rename elem_isnt_collapsed to mgelem_isnt_collapsed, to 
           clarify that this function does collapsed node tracing.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  18Dec10; new pBc->type
  12Feb04; find containing coarse grid cell in pUns->pElCollapseTo in make_coarser_level.
  5Mar00; fix bug with 2D coarsening in 3D and mVxColl test. 
  22Nov99; remove all bits used to check edge-lengths.
  25Oct99; intro simplify_one_elem.
  7Jul99; reorder the graph of edges, properly track interior and periodic edges.
  
  
  This file contains:
  -------------------
  add_vxStack
  coarsen_one_level
  fileEg
  find_face
  loop_edge_coll_vx
  make_coarser_level
  make_mgFace
  mgVrtx
  mg_allConvex
  tryToCollapse_edge
  uns_coarsen

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"


#define MAX_MG_LEVELS (16)

#define STACK_SIZE 500
#define MAX_PARA_EDGES 4
#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern const Grids_struct Grids ;
extern const int bitEdge[] ;


/* Multigrid parameters. */
extern const double mgLen ;
extern const double mgLrgstAngle ;
extern const double mgTwistMin ;
extern const double mgVolAspect ;
extern const double mgArCutoff ;
extern const double mgRamp ;

/* Sharp corners. */
extern const double normAnglCut ;
extern const double intPolRim ;
extern const double intFcTol ;
extern const double intFullTol ;

/* Minnorm coeffs. */
extern const double minnorm_tol ;

/* Edgelength and two flags for short and long stretched edges. */
struct  _edgeLen_s {
  unsigned int shortEg:1 ;
  unsigned int longEg:1 ;
  unsigned int nonCollapsible:1 ;
  unsigned int mBnd:2 ;
  unsigned int mark:1 ;
  
  unsigned int perEdge:26 ;
  /* float length ;*/
} ;

/* A criterion for the heap sort of elemens according to smallest  volume. */
typedef struct {
  const elem_struct *pElem ;
  double vol ;
} elCrit_s ;

/* A boundary face as given by its nodes. */
typedef struct {
  int mVxFc ;
  ulong_t nVxFc[MAX_VX_FACE] ;
  bc_struct *pBc ;
} mgFace_s ;


typedef struct {
  ulong_t nEg ;                        /* The edge to collapse. */

  ulong_t nVx[2] ;                     /* The numbers of the terminal vertices. */
  vrtx_struct *pVx[2] ;            /* The two terminal vertex pointers. */
  double oldCoor[2][MAX_DIM] ;     /* The original coordinates. */
  double *pCoor[2] ;               /* The pointer to the coor to collapse/restore.*/
  int mVxColl[2] ;                 /* The number of previous collapses for restore. */

  ulong_t nxtEg[2] ;                   /* The continuation of this edge. */
} edgeToColl_s ;

typedef struct { /* Various reasons for failure of an element collapse. */
  ulong_t nonCollEg:1 ;
  ulong_t intBndEg:1 ;
  ulong_t egLen:1 ;
  ulong_t mColl:1 ;
  ulong_t angle:1 ;
  ulong_t twist:1 ;
  ulong_t volume:1 ;
  ulong_t nonConvex:1 ;
} elCollFail_s ;


typedef struct {
  bndFc_struct *pBndFc[2] ;
} eg2el_s ;


/* Statically allocated virtual element. */
//extern double vrtCoor[MAX_VX_ELEM*MAX_DIM] ;
//extern vrtx_struct vrtVrtx[MAX_VX_ELEM], *pVrtVrtx[MAX_VX_ELEM] ;
extern elem_struct vrtElem ;

double vrtCoor2[MAX_VX_ELEM*MAX_DIM] ;
vrtx_struct vrtVrtx2[MAX_VX_ELEM], *pVrtVrtx2[MAX_VX_ELEM] ;
elem_struct vrtElem2 ;



/******************************************************************************

  mgVrtx:
  Find the root-vertex of a set of collapsed vertices. The root is found when
  the vertex number and coordinate position coincide.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mgVrtx ( const vrtx_struct *pMgVrtx, const double *pMgCoor,
		    const int mDim, int nVx ){
  
  int nnVx ;

  if ( pMgVrtx && pMgCoor )
    while ( ( nnVx = ( pMgVrtx[nVx].Pcoor - pMgCoor )/mDim ) - nVx )
      nVx = nnVx ;

  return ( nVx ) ;
}




/******************************************************************************

  make_mgElem:
  Make a collapsed element following all coordinates to the root.
  
  Last update:
  ------------
  22Mar11; static allocation within uns_mg of elem
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

elem_struct *make_mgElem ( const elem_struct *pElem,
                           const vrtx_struct *pMgVrtx, const double *pMgCoor ) {

  const elemType_struct *pElT ;
  int kVx, mDim, nVx ;

  if ( pElem->invalid ) {
    printf ( " FATAL: invalid element in make_mgElem.\n" ) ;
    return ( 0 ) ; }
  
  pElT = elemType + pElem->elType ;
  mDim = pElT->mDim ;

  /* Make a temporary element with the collapsed coordinates to
     calculate the volume. */
  vrtElem.elType = pElem->elType ;
  
  /* Place vertex coordinates. */
  vrtElem.number = pElem->number ;
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    nVx = mgVrtx( pMgVrtx, pMgCoor, mDim, pElem->PPvrtx[kVx]->number ) ;
    vrtElem.PPvrtx[kVx]->number = nVx ;
    vrtElem.PPvrtx[kVx]->Pcoor = ( double * ) pMgCoor + mDim*nVx ;
  }

  return ( &vrtElem ) ;
}



/******************************************************************************

  :
  .
  
  Last update:
  ------------
  12jul20; compare distance against epsOverlap of no linked list of collapses given. 
  4Apr13; make all large counters ulong_t.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------
  pmVxElem: the number of distinct vertices on the element in 2D, (since degenerate
            quads get written as tris), in 3D it is the number of base vertices.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mgelem_isnt_collapsed ( const elem_struct *pEl, const vrtx_struct *pMgVrtx,
                          const double *pMgCoor, int *pmVxElem, ulong_t nVxLeft[] ) {
  
  const elemType_struct *pElT = elemType + pEl->elType ;
  const faceOfElem_struct *pFoE ;
  const int mDim = pElT->mDim ;
  const ulong_t mVx = pElT->mVerts ;
  const int  *kVxFace ;
  vrtx_struct **ppVx = pEl->PPvrtx ;
  ulong_t nVxColl[MAX_VX_ELEM], nVxFc[MAX_FACES_ELEM][MAX_VX_FACE] ;
  int mVxFc[MAX_FACES_ELEM], mFcLeft, kFc, kVx, kkVx ;
  ulong_t nVx ;
  int mVxLeft ;
  double distSq ;

  /* Count the number of distinct vertices on the element. */
  for ( mVxLeft = kVx = 0 ; kVx < mVx ; kVx++ ) {
      nVx = ppVx[kVx]->number ;
    if ( pMgVrtx && pMgCoor ) {
      /* Single list of vx and coor given, linked list can be traced. */
      nVxColl[kVx] = nVx = mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
      for ( kkVx = 0 ; kkVx < mVxLeft ; kkVx++ ) 
        if ( nVx == nVxLeft[kkVx] )
          break ;
    }
    else{
      /* Fall back on epsoverlap. */
      for ( kkVx = 0 ; kkVx < mVxLeft ; kkVx++ ) {
        distSq = sq_distance_dbl ( ppVx[kVx]->Pcoor, ppVx[kkVx]->Pcoor, mDim ) ; 
        if ( distSq <= Grids.epsOverlapSq )
          ; //break ;
      }
    }
    if ( kkVx == mVxLeft )
      /* nVx is not yet in the list. Add.*/
      nVxLeft[ (mVxLeft)++ ] = nVx ;
  }

  /* In 2D, hip converts the partly collapsed quad to a tri. List the actual
     number of vertices. */
  *pmVxElem = ( mDim == 2 ? mVxLeft : mVx ) ;
  
  if ( mVxLeft <= mDim )
    /* Not even a simplex left. */
    return ( 0 ) ;

  else if ( mDim == 2 )
    return ( 1 ) ;

  else if ( mVxLeft > 4 )
    /* Can there be a collapsed element with more than 4 nodes in 3D? */
    return ( 1 ) ;

  else {
    /* Left is the case of a 4 noded 3D element. This element might consist of only
       two quad faces that match each other. Count the faces that are left. */
    for ( mFcLeft = 0, kFc = 1 ; kFc <= pElT->mFaces ; kFc++ ) {
      pFoE = pElT->faceOfElem + kFc ;
      kVxFace = pFoE->kVxFace ;
      mVxFc[mFcLeft] = 0 ;

      /* Make a list of all the non-collapsed vertices on this face. */
      for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
        nVx = nVxColl[ kVxFace[kVx] ] ;
        for ( kkVx = 0 ; kkVx < mVxFc[mFcLeft] ; kkVx++ )
          if ( nVx == nVxFc[mFcLeft][kkVx] )
            break ;
        if ( kkVx == mVxFc[mFcLeft] )
          /* nVx is not yet in the list. */
          nVxFc[mFcLeft][ mVxFc[mFcLeft]++ ] = nVx ;
      }
      
      if ( mVxFc[mFcLeft] >= mDim )
        /* Enough of a face left. */
        mFcLeft++ ;
    }

    if ( mFcLeft == 2 )
      /* Only two faces left. */
      return ( 0 ) ;

    else if ( mFcLeft == 3 )
      /* Must be a sliver like flat element with a twisted quad face and two tris. */
      return ( 1 ) ;

    else {
      /* Four tri faces. Do they match? Try to compare the first face left with
         the others. If one match is found, the others have to match as well. */
      
      for ( kFc = 1 ; kFc < mFcLeft ; kFc++ ) {
        for ( kVx = 0 ; kVx < mVxFc[0] ; kVx++ ) {
          for ( kkVx = 0 ; kkVx < mVxFc[kFc] ; kkVx++ )
            if ( nVxFc[0][kVx] == nVxFc[kFc][kkVx] )
              break ;
          if ( kkVx == mVxFc[kFc] )
            /* No match. */
            break ;
        }
      
        if ( kVx == mVxFc[0] && mVxFc[0] == mVxFc[kFc] )
          /* Match. There must be two matching sets of tris, thus a collapsed element. */
          return ( 0 ) ;
      }

      /* No match. */
      return ( 1 ) ;
    }
  }
}



#ifdef DEBUG

/******************************************************************************

  make_mgElem0:
  Make a collapsed element following all coordinates to the root.
  
  Last update:
  ------------
  22Mar11; make vrtx, pVrtx also static.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

elem_struct *make_mgElem0 ( const elem_struct *pElem,
                            const vrtx_struct *pMgVrtx, const double *pMgCoor ) {

  const elemType_struct *pElT ;
  static elem_struct elem ;
  static vrtx_struct vrtx[MAX_VX_ELEM], *pVrtx[MAX_VX_ELEM] ;
  int kVx, mDim, nVx ;

  if ( pElem->invalid ) {
    printf ( " FATAL: invalid element in make_mgElem.\n" ) ;
    return ( 0 ) ; }
  
  pElT = elemType + pElem->elType ;
  mDim = pElT->mDim ;

  /* Make a generic element to be filled with the collapsed geometry. Could
     we make this static? */
  elem.PPvrtx = pVrtx ;
  for ( kVx = 0 ; kVx < MAX_VX_ELEM ; kVx++ )
    pVrtx[kVx] = vrtx+kVx ;

  /* Make a temporary element with the collapsed coordinates to
     calculate the volume. */
  elem.elType = pElem->elType ;
  
  /* Place vertex coordinates. */
  elem.number = pElem->number ;
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    nVx = pElem->PPvrtx[kVx]->number ;
    nVx = vrtx[kVx].number = mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
    vrtx[kVx].Pcoor = ( double * ) pMgCoor + mDim*mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
  }

  return ( &elem ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

elem_struct *make_mgElem2 ( const elem_struct *pElem,
                           const vrtx_struct *pMgVrtx, const double *pMgCoor ) {


  if ( pElem->invalid ) {
    printf ( " FATAL: invalid element in make_mgElem.\n" ) ;
    return ( 0 ) ; }


  init_elem ( &vrtElem2, noEl, 0, pVrtVrtx2 ) ;

  /* Make a generic element to be filled with the collapsed geometry. */
  vrtx_struct **ppVx = vrtElem2.PPvrtx ;
  int kVx ;
  for ( kVx = 0 ; kVx < MAX_VX_ELEM ; kVx++ ) {
    ppVx[kVx] = vrtVrtx2 + kVx ;
    ppVx[kVx]->number = kVx ;
    ppVx[kVx]->Pcoor = vrtCoor2 + kVx*MAX_DIM ;
  }
  
  const elemType_struct *pElT ;
  pElT = elemType + pElem->elType ;
  int mDim, nVx ;
  mDim = pElT->mDim ;

  /* Make a temporary element with the collapsed coordinates to
     calculate the volume. */
  vrtElem2.elType = pElem->elType ;
  
  /* Place vertex coordinates. */
  vrtElem2.number = pElem->number ;
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    nVx = mgVrtx( pMgVrtx, pMgCoor, mDim, pElem->PPvrtx[kVx]->number ) ;
    vrtElem2.PPvrtx[kVx]->number = nVx ;
    vrtElem2.PPvrtx[kVx]->Pcoor = ( double * ) pMgCoor + mDim*nVx ;
  }

  return ( &vrtElem2 ) ;
}





/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int viz_cutElems ( const uns_s *pUns,
                   const vrtx_struct *pMgVrtx, const double *pMgCoor,
                   char axis, double xCtr[3], double radius ) {
  
#define MAX_ELS_VIZ 1000
  const elem_struct *pElViz[MAX_ELS_VIZ] ;
  const elemType_struct *pElT ;
  const int *kVxEg ;
  const double *pCoEg[2] ;

  FILE *glutFile ;
  ulong_t mEls = 0 ;
  int cd, kEg ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pEl, *pElBeg, *pElEnd ;
  double rad2 = radius*radius, d0, d1, r0, r1, xX ;

  if      ( axis == 'z' ) cd = 2 ;
  else if ( axis == 'y' ) cd = 1 ;
  else                    cd = 0 ;

  /* Add all elements to the list that intersect the disc. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number ) {

        if ( pMgVrtx && pMgCoor )
          pElem = make_mgElem ( pEl, pMgVrtx, pMgCoor ) ;
        else
          pElem = pEl ;

        /* Only consider an element if the first coor is not, say, 50
           times too far away. */
        if ( sq_distance_dbl ( pElem->PPvrtx[0]->Pcoor, xCtr, 3 ) < 50.*rad2 ) {
          pElT = elemType + pElem->elType ;

          /* Loop over all edges. Do they intersect with the disc? */
          for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
            kVxEg = pElT->edgeOfElem[kEg].kVxEdge ;
            pCoEg[0] = pElem->PPvrtx[ kVxEg[0] ]->Pcoor ;
            pCoEg[1] = pElem->PPvrtx[ kVxEg[1] ]->Pcoor ;
            d0 = pCoEg[0][cd] - xCtr[cd] ;
            d1 = pCoEg[1][cd] - xCtr[cd] ;

            if ( d0*d1 < 0 ) {
              /* This edge crosses the plane. Does it cross the disc? */
              d0 = ABS( d0 ) ;
              d1 = ABS( d1 ) ;
              r0 = d0/(d0+d1) ;
              r1 = d1/(d0+d1) ;
              xX = r0*pCoEg[0][cd] + r1*pCoEg[1][cd] ;

              if ( xX < radius ) {
                /* This one is in. */
                if ( mEls <= MAX_ELS_VIZ-1 )
                  pElViz[mEls++] = pEl ;
                break ;
              }
            }
          }
        }
      }

  if ( mEls >= MAX_ELS_VIZ ) {
    printf ( " WARNING: too many elements in disc in viz_cut_elems.\n" ) ;
    return ( 0 ) ; }
  else if ( !mEls ) {
    printf ( " No elements found in disc.\n" ) ;
    return ( 0 ) ; }
  else
    return ( viz_mgElems ( pUns, pMgVrtx, pMgCoor, mEls, pElViz ) ) ;
}

#endif

int viz_mgElems ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  int mEls, const elem_struct **ppElem ) {

  int nEl ;
  const elem_struct *pEl, *pElem ;
  char flnm[LINE_LEN] ;
  
  
  for ( nEl = 0 ; nEl < mEls ; nEl++ ) {
    pEl = ppElem[nEl] ;
    if ( pEl ) {
      if ( pMgVrtx && pMgCoor )
        pElem = make_mgElem ( pEl, pMgVrtx, pMgCoor ) ;
      else
        pElem = pEl ;

      sprintf ( flnm, "mgEl%d.vtk", (int) pElem->number ) ;
      viz_one_elem_vtk( flnm, pElem, NULL ) ;
    }
  }

  return ( 1 ) ;
}


static int find_co_list ( const int mElemVx, const double *pCoList[],
                          const double *pCoor ) {
  int k ;
  for ( k = 0 ; k < mElemVx ; k++ )
    if ( pCoList[k] == pCoor )
      return ( k ) ;
  return ( -1 ) ;
}

static void fprintf_co ( FILE* fvtk, const int mDim, const double *pCo ) {
  fprintf ( fvtk, "%15.11g ", pCo[0] ) ;
  fprintf ( fvtk, "%15.11g ", pCo[1] ) ;
  fprintf ( fvtk, "%15.11g\n", ( mDim == 3 ?  pCo[2] : 0. ) ) ; 

}

void viz_mgElems_vtk ( char *fileName, int mEl, const elem_struct **ppElem,
                       const vrtx_struct *pMgVrtx, const double *pMgCoor,
                       const double *pCoor0, const double *pCoor1 ) {

  /* Open file. */
  char flnm[LINE_LEN] ;
  if ( fileName )
    strcpy ( flnm, fileName ) ;
  else 
    sprintf ( flnm, "oneElem.vtk" ) ;
  prepend_path ( flnm ) ;
  FILE *fvtk = fopen ( flnm, "w" ) ;
  if ( !fvtk ) {
    hip_err ( warning, 0, "could not open file, call ignored." ) ;
    return ;
  }

  
  /* Header. */
  fprintf ( fvtk, 
            "# vtk DataFile Version 3.1\n"
            "multiple element extract using hip::viz_one_elem_vtk.\n"
            "ASCII\n"
            "DATASET UNSTRUCTURED_GRID\n\n" ) ; 


  /* Count coors, make a subset conn table. */  
  int kEl, mElemVx = 0 ;
#define MAX_EL_VTK (1000)
#define MAX_VX_VTK (6*MAX_EL_VTK)
  const double *pCoList[MAX_VX_VTK] ;
  ulong_t vxnoList[MAX_VX_VTK] ;
  int kConn[MAX_VX_ELEM][MAX_EL_VTK] ; 
  int mVxList=0, mVx, mConn=0, kVx ;
  const elem_struct *pEl, *pElem ;
  const vrtx_struct *pVx ;
  int kVxList ;
  for ( kEl = 0 ; kEl < mEl ; kEl++ ) {
    pEl = ppElem[kEl] ;
    if ( pEl ) {
      if ( pMgVrtx && pMgCoor )
        pElem = make_mgElem ( pEl, pMgVrtx, pMgCoor ) ;
      else
        pElem = pEl ;

      mVx = elemType[ pElem->elType ].mVerts ;
      mConn += mVx ;
      for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
        pVx = pElem->PPvrtx[ kVx ] ;
        kVxList = find_co_list ( mVxList, pCoList, pVx->Pcoor ) ;
        if ( kVxList == -1 ) {
          // Add to the list.
          pCoList[mVxList] = pVx->Pcoor ;
          vxnoList[mVxList] = pVx->number ;
          kVxList = mVxList++ ;
        }
        kConn[kVx][kEl] = kVxList ;
      }
    }
  }


  
  /* Point header. */
  int mVxAdded = ( pCoor0 ? 1 : 0 ) + ( pCoor1 ? 1 : 0 ) ;
  fprintf ( fvtk,
            "POINTS %d FLOAT\n", mVxList + mVxAdded ) ;
  
  /* Write coor. */
  const int mDim =  elemType[ ppElem[0]->elType ].mDim ;
  for ( kVx = 0 ; kVx < mVxList ; kVx++ )
    fprintf_co ( fvtk, mDim, pCoList[kVx] ) ;

  /* Extra point coor? */
  if ( pCoor0 )
    fprintf_co ( fvtk, mDim, pCoor0 ) ;
  if ( pCoor1 )
    fprintf_co ( fvtk, mDim, pCoor1 ) ;
  fprintf ( fvtk, "\n" ) ;
  

  
  /* Element connectivity. */
  // no of cells, total size of list, including conn counters.
  fprintf ( fvtk,
            "CELLS %d %d\n", mEl, mEl+mConn ) ;

  const int *kVxMap, kVxPri[]    = {0,5,3,1,4,2}, kVxOth[]={0,1,2,3,4,5,6,7} ;
  for ( kEl = 0 ; kEl < mEl ; kEl++ ) {
    pElem = ppElem[kEl] ;
    if ( pElem ) {
      mVx = elemType[ pElem->elType ].mVerts ;
      fprintf ( fvtk, "%d ", mVx ) ;
      
      kVxMap = ( pElem->elType == pri ? kVxPri : kVxOth ) ;
      // Note: use 0 for 1st node.
      for ( kVx = 0 ; kVx < mVx ; kVx++ )
        fprintf ( fvtk, "%d ",
                  kConn[ kVxMap[kVx] ][kEl] ) ;
      fprintf ( fvtk, "\n" ) ;
    }
  }


  /* Element type */
  fprintf ( fvtk,
            "CELL_TYPES %d\n", mEl ) ;
  for ( kEl = 0 ; kEl < mEl ; kEl++ ) {
    pEl = ppElem[kEl] ;
    fprintf ( fvtk, "%d\n", elType2vtk( pEl->elType ) ) ;
  }
  fprintf ( fvtk, "\n" ) ;
 
  
  

  /* data. Use a value of 0 for the vertices, 1 for the extra point if any. */
  fprintf ( fvtk,
            "POINT_DATA %d\n", mVxList + mVxAdded ) ;

  fprintf ( fvtk,
            "SCALARS outside_elem FLOAT\n"
            "LOOKUP_TABLE default\n" ) ;
  for ( kVx = 0 ; kVx < mVxList ; kVx++ )
    fprintf ( fvtk, "0\n" ) ;
  if ( pCoor0 )
    fprintf ( fvtk, "1\n" ) ;
  if ( pCoor1 )
    fprintf ( fvtk, "1\n" ) ;

  fprintf ( fvtk,
            "SCALARS node_number FLOAT\n"
            "LOOKUP_TABLE default\n" ) ;
  for ( kVx = 0 ; kVx < mVxList ; kVx++ )
    fprintf ( fvtk, "%"FMT_ULG"\n", vxnoList[kVx] ) ;
  if ( pCoor0 )
    fprintf ( fvtk, "0\n" ) ;
  if ( pCoor1 )
    fprintf ( fvtk, "0\n" ) ;

  fclose ( fvtk ) ;
  return ;
}



#ifdef DEBUG


/******************************************************************************

  fileEg:
  Dump the graph of edges into a .dpl format.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns
  file
  
*/

void fileEg ( const uns_s *pUns, const llEdge_s *pllEdge,
              const vrtx_struct *pMgVrtx, const double *pMgCoor,
              const char *file )
{
  const int mDim = 2 ;
  FILE *dplFile ;
  ulong_t mEdges, mNewEdges, nEg ;
  int kVx ;
  ulong_t nVx[2] ;
  vrtx_struct *pVxEg[2] ;
  const double *pCo ;


  dplFile = fopen ( file, "w" ) ;
  
  fprintf ( dplFile, "unstr\n" ) ;
  fprintf ( dplFile, "%d\n", 0 ) ;
  fprintf ( dplFile, "%"FMT_ULG"\n", pUns->mVertsNumbered ) ;
  fprintf ( dplFile, "1. .7 .4. 86.\n" ) ;

  /* List all coordinates. */
  for ( kVx = 1 ; kVx <= pUns->mVertsNumbered ; kVx++ ) {
    pCo = pMgCoor + mDim*mgVrtx( pMgVrtx, pMgCoor, mDim, kVx ) ;
    fprintf ( dplFile, "%+6f %+6f %d .7 .4. 86.\n", pCo[0], pCo[1], kVx ) ;
  }

  /* Number of edges in the non-collapsed graph. */
  get_number_of_edges ( pllEdge, &mEdges ) ;

  /* Count the new edges. */
  for ( mNewEdges = 0, nEg = 1 ; nEg <= mEdges ; nEg++ )
    if ( show_edge ( pllEdge, nEg, pVxEg, pVxEg+1 ) ) {
      nVx[0] = mgVrtx( pMgVrtx, pMgCoor, mDim, pVxEg[0]->number ) ;
      nVx[1] = mgVrtx( pMgVrtx, pMgCoor, mDim, pVxEg[1]->number ) ;
      if ( nVx[0] != nVx[1] )
	++mNewEdges ;
    }

  fprintf ( dplFile, "%d\n", 1 ) ;
  fprintf ( dplFile, "%"FMT_ULG"\n", mNewEdges ) ;
  for ( nEg = 1 ; nEg <= mEdges ; nEg++ )
    if ( show_edge ( pllEdge, nEg, pVxEg, pVxEg+1 ) ) {
      nVx[0] = mgVrtx( pMgVrtx, pMgCoor, mDim, pVxEg[0]->number ) ;
      nVx[1] = mgVrtx( pMgVrtx, pMgCoor, mDim, pVxEg[1]->number ) ;
      if ( nVx[0] != nVx[1] )
	fprintf ( dplFile, " %"FMT_ULG" %"FMT_ULG"\n", pVxEg[0]->number, pVxEg[1]->number ) ;
    }
  
  fprintf ( dplFile, "%d\n", 0  ) ;
  
  fclose ( dplFile ) ;

  return ;
}


/******************************************************************************

  :
  Make a copy of pUns but use the collapsed coordinates.
  
  Last update:
  ------------
  1oct17; rewrite for hdf
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mg_write_hdf ( uns_s *pUns,
                   const vrtx_struct *pMgVrtx, const double *pMgCoor,
                   char fileName[] ) {


  /* Loop over all chunks, copy elem, node and coor numbers as 
     hdf will renumber. Keep track of the old arrays and restore
     before exiting. */
  int **ppElNr = arr_malloc ( "ppElNr in mg_write_hdf", pUns->pFam,
                              pUns->mChunks, sizeof ( *ppElNr ) ) ; 
  int **ppVxNr = arr_malloc ( "ppVxNr in mg_write_hdf", pUns->pFam,
                              pUns->mChunks, sizeof ( *ppVxNr ) ) ; 
  
  double **ppCoor = arr_malloc ( "ppCoor in mg_write_avbp3", pUns->pFam,
                                 pUns->mChunks, sizeof ( *ppCoor ) ) ;

  /* Change all coordinates to collapsed ones, while maintaining the
     complete list of vertices. */
  chunk_struct *pChunk ;
  elem_struct *pEl ;
  int *pElNr, *pVxNr ;                                
  double *pCo ;
  vrtx_struct *pVx ;
  int nCh ;
  const int mDim = pUns->mDim ;
  int nVxColl ;
  const double *pCoColl ;
  for ( nCh = 0 ; nCh < pUns->mChunks ; nCh++ ) {
    pChunk = pUns->ppChunk[nCh] ;

    /* Have all these copy arrays start from zero. */
    pElNr = ppElNr[nCh] = arr_malloc ( "ppElNr[nCh] in mg_write_avbp3", pUns->pFam,
                                       pChunk->mElems, sizeof ( *(ppElNr[nCh]) ) ) ;
    pVxNr = ppVxNr[nCh] = arr_malloc ( "ppVxNr[nCh] in mg_write_avbp3", pUns->pFam,
                                       pChunk->mVerts, sizeof ( *(ppVxNr[nCh]) ) ) ;
    pCo = ppCoor[nCh] = arr_malloc ( "ppCoor[nCh] in mg_write_avbp3", pUns->pFam,
                                     mDim*( pChunk->mVerts ), sizeof ( *(ppCoor[nCh]) ) ) ;

    for ( pEl = pChunk->Pelem+1 ; pEl <= pChunk->Pelem + pChunk->mElems ; pEl++ ) {
      *pElNr++ = pEl->number ;
    }
    
    for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ ) {
      *pVxNr++ = pVx->number ;
      vec_copy_dbl ( pVx->Pcoor, mDim, pCo ) ;
      pCo += mDim ;
      
      if ( pVx->number ) {
        /* Copy the collapsed coor into the vertex. */
        nVxColl = mgVrtx ( pMgVrtx, pMgCoor, mDim, pVx->number ) ;
        pCoColl = pMgCoor + mDim*nVxColl ;
        vec_copy_dbl ( pCoColl, mDim, pVx->Pcoor ) ;
      }
    }
  } 
    
  //write_avbp ( pUns, fileName, noFmt ) ;
  char flNm[LINE_LEN] ;
  strcpy ( flNm, fileName ) ;
  prepend_path ( flNm ) ;
  write_hdf5_grid ( flNm, pUns ) ;



  /* Copy back. */
  for ( nCh = 0 ; nCh < pUns->mChunks ; nCh++ ) {
    pChunk = pUns->ppChunk[nCh] ;

    /* Have all these copy arrays start from zero. */
    pElNr = ppElNr[nCh] ;
    pVxNr = ppVxNr[nCh] ;
    pCo = ppCoor[nCh] ;

    for ( pEl = pChunk->Pelem+1 ; pEl <= pChunk->Pelem + pChunk->mElems ; pEl++ ) {
      pEl->number = *pElNr++ ;
    }
    
    for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ ) {
      pVx->number = *pVxNr++ ;
      vec_copy_dbl ( pCo, mDim, pVx->Pcoor ) ;
      pCo += mDim ;
    }

    arr_free ( ppElNr[nCh] ) ;
    arr_free ( ppVxNr[nCh] ) ;
    arr_free ( ppCoor[nCh] ) ;
  } 

  arr_free ( ppElNr ) ;
  arr_free ( ppVxNr ) ;
  arr_free ( ppCoor ) ;
  return ( 1 ) ;
}



/******************************************************************************

  :
  .
  
  Last update:
  ------------
  19Mar17; replace viz_one_elem_glut with _vtk.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int viz_mgElem ( const elem_struct *pElem,
                 const vrtx_struct *pMgVrtx, const double *pMgCoor ) {

  const elem_struct *pMgEl ;
  int elemIsNotCollapsed ;
  double elemVol, lrgstAngle, dihAngle, fcAngle, elVol, hMin ;
  FILE *glutFile ;

  if ( pElem->invalid ) {
    printf ( " FATAL: invalid element.\n" ) ;
    return ( 0 ) ; }

  /* Make an element with the collapsed vertices. */
  if ( pMgVrtx && pMgCoor )
    pMgEl = make_mgElem ( pElem, pMgVrtx, pMgCoor ) ;
  else
    pMgEl = pElem ;

  if ( verbosity > 4 ) {
    /* See bug fix 9Jul13 in maxAngle, possibly a compiler bug. To fix,
       the const qualifier in maxAngle's arg list had to be removed, so
       cast the arg here to non-const. */
    lrgstAngle = maxAngle ( (elem_struct *) pMgEl, &elVol, &elemIsNotCollapsed, 
                            &hMin, &dihAngle, &fcAngle ) ;
    elemVol = get_elem_vol ( pMgEl ) ;
    
    printel ( pMgEl ) ;
    printf ( "   vol: %g, lrgst angle: %g.\n",
             elemVol, lrgstAngle ) ;
  }
  
  viz_one_elem_vtk ( "mgElem.vtk", pMgEl, NULL ) ;
  
  return ( 1 ) ;
}


int viz_exElems ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  viz_e vizWhat, int mEls, int minEl, int list ) {

  chunk_struct *pChunk ;
  elem_struct *pEl, *pElBeg, *pElEnd, *pElem, **ppElem ;
  double val = 0., *pVal, vol, hMin, dihAngle, fcAngle, elVol ;
  int kAngle ;
  ulong_t nEl ;
  int kEgMin, elemIsNotCollapsed ;
  FILE *glutFile ;

  minEl = MIN( minEl, mEls ) ;
  
  hip_err ( warning, 1, "conversion to _vtk only plots a single elem.");
  ppElem = arr_malloc ( "ppElem in viz_exElems", pUns->pFam,
                        mEls, sizeof( *ppElem ) ) ;
  pVal = arr_malloc ( "pVal in viz_exElems", pUns->pFam, mEls,sizeof( *pVal ) ) ;

  if ( !ppElem || !pVal ) {
    printf ( " FATAL: failed to alloc for %d hits.\n", mEls ) ;
    return ( 0 ) ; }
  if ( !( vizWhat == smlVol || vizWhat == lrgAngle ||
          vizWhat == twist ||vizWhat == lrgVol ) ) {
    printf ( " FATAL: no such viz_e %d\n", vizWhat ) ;
    return ( 0 ) ; }

  for ( nEl = 0 ; nEl < mEls ; nEl++ ) {
    ppElem[nEl] = NULL ;
    pVal[nEl] = TOO_MUCH ;
  }
  
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number ) {

        if ( pMgVrtx && pMgCoor )
          pElem = make_mgElem ( pEl, pMgVrtx, pMgCoor ) ;
        else
          pElem = pEl ;

        if ( vizWhat == smlVol ) {
          val = get_elem_vol ( pElem ) ;
          val = ( val < 1.e-25 ? 1.e25 : val ) ;
        }
        else if ( vizWhat == lrgVol ) {
          val = get_elem_vol ( pElem ) ;
          val = ( val < 1.e-25 ? 0. : 1./val ) ;
        }
        else if ( vizWhat == lrgAngle )
          val = maxAngle ( pElem, &elVol, &elemIsNotCollapsed, &hMin, &dihAngle, &fcAngle ) ;
        else if ( vizWhat == twist )
          val = get_lrgstFaceTwist ( pElem, &kAngle ) ;


        if ( val < pVal[mEls-1] ) {
          /* This one is in the top hits. Find its proper position and push
             all remaining entries up. */
          for ( nEl = mEls-1 ; nEl > 0 ; nEl-- ) {
            if ( val > pVal[nEl-1] ) {
              pVal[nEl] = val ;
              ppElem[nEl] = pEl ;
              break ;
            }
            else {
              pVal[nEl]   = pVal[nEl-1] ;
              ppElem[nEl] = ppElem[nEl-1] ;
            }
          }
          if ( nEl == 0 ) {
            pVal[0] = val ;
            ppElem[0] = pEl ;
          }
        }
      }

  for ( nEl = minEl-1 ; nEl < mEls ; nEl++ ) {
    pEl = ppElem[nEl] ;
    if ( pEl ) {
      if ( pMgVrtx && pMgCoor )
        pElem = make_mgElem ( pEl, pMgVrtx, pMgCoor ) ;
      else
        pElem = pEl ;

      if ( list ) 
        printf ( " elem: %"FMT_ULG", %g\n", pElem->number, pVal[nEl] ) ;
      viz_one_elem_vtk( "mgElem.vtk", pElem, NULL ) ;
    }
  }

  arr_free ( ppElem ) ;
  arr_free ( pVal ) ;

  return ( 1 ) ;
}



void findelmgvx ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  const int nVx, const int viz ) {
  const chunk_struct *Pchunk ;
  const elem_struct *Pelem, **ppElViz = NULL ;
  int kVx ;
  ulong_t mElViz = 0 ;

  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ; Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->PPvrtx )
	for ( kVx = 0 ; kVx < elemType[ Pelem->elType ].mVerts ; kVx++ )
	  if ( mgVrtx( pMgVrtx, pMgCoor, pUns->mDim,
                       Pelem->PPvrtx[kVx]->number ) == nVx ) {
            printf ( " p/c: %d/%d,", (int)(Pelem - Pchunk->Pelem), Pchunk->nr ) ;
	    printelal ( Pelem ) ;
            //add_viz_el ( Pelem, &ppElViz, &mElViz ) ;
	  }

  if ( viz )
    viz_mgElems ( pUns, pMgVrtx, pMgCoor, mElViz, ppElViz ) ;
  
  arr_free ( ppElViz ) ;
  
  return ;
}

void viz_mgEl_r ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  const vrtx_struct *pVx, const int nVx, float r, const int vizColl ) {

  const int mDim = pUns->mDim ;
  const double *pCo, *pCo2 ;
  double dist, r2 ;
  const chunk_struct *Pchunk ;
  const elem_struct *pElem, **ppElViz = NULL ;
  int kVx ;
  ulong_t mElViz = 0, nVxElem[MAX_VX_ELEM] ;
  int mVxEl ;

  r2 = r*r ;

  if ( pMgCoor )
    pCo = pMgCoor + mDim*mgVrtx(pMgVrtx,pMgCoor,pUns->mDim,nVx) ;
  else if ( pVx )
    pCo = pVx->Pcoor ;
  else
    pCo = findpvx( pUns, nVx )->Pcoor ;

  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( pElem = Pchunk->Pelem+1 ; pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
      if ( pElem->PPvrtx )
	for ( kVx = 0 ; kVx < elemType[ pElem->elType ].mVerts ; kVx++ ) {

          if ( pMgCoor )
            pCo2 =  pMgCoor + mDim*mgVrtx( pMgVrtx, pMgCoor, pUns->mDim,
                                           pElem->PPvrtx[kVx]->number ) ;
          else
            pCo2 = pElem->PPvrtx[kVx]->Pcoor ;

          dist = sq_distance_dbl ( pCo, pCo2, mDim ) ;
          if ( dist <= r2 ) {
            /* printf ( " p/c: %ld/%d (%g):,",
               pElem - Pchunk->Pelem, Pchunk->nr, dist ) ;
               printelal ( make_mgElem( pElem, pMgVrtx, pMgCoor ) ) ;*/
            if ( vizColl || mgelem_isnt_collapsed( pElem, pMgVrtx, pMgCoor, &mVxEl, nVxElem ) )
              //add_viz_el ( pElem, &ppElViz, &mElViz ) ;
            break ;
	  }
        }

  viz_mgElems ( pUns, pMgVrtx, pMgCoor, mElViz, ppElViz ) ;
  arr_free ( ppElViz ) ;
  
  return ;
}


int viz_mgBnd ( const uns_s *pUns,
                const vrtx_struct *pMgVrtx, const double *pMgCoor,
                unsigned int bndMask ) {

  const faceOfElem_struct *pFoE ;
  
  elem_struct *pElem, fcElem ;
  vrtx_struct *ppVx[4] ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  int nBc, kVx, nVx ;
  vrtx_struct *pVx ;

  fcElem.PPvrtx = ppVx ;

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBndPatch = NULL ;

    if ( bitEdge[nBc] & bndMask )
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
            pElem = pBndFc->Pelem ;
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;

            /* Make a virtual element for this face. */
            fcElem.elType = ( pFoE->mVertsFace == 3 ? tri : qua ) ;
            fcElem.number = pElem->number ;
            for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
              pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              nVx = mgVrtx ( pMgVrtx, pMgCoor, pUns->mDim, pVx->number ) ;
              fcElem.PPvrtx[kVx] = pUns->ppChunk[0]->Pvrtx + nVx ;
            }

            viz_one_elem_vtk( "mgElem.vtk", &fcElem, NULL ) ;
          }
  }

  return ( 1 ) ;
}


#endif



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  umg_args:
*/
/*! parse argument list for uns_mg.
 *
 *
 */

/*
  
  Last update:
  ------------
  10jul20; use 'c' as default.
  2Mar18; intro t arg
  10Jul17: derived from h5w_args.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  0 on failure, integer task code ottherwise.
  
*/

int umg_args ( char argLine[], char *mg_op,
               double *pminVol, int *pmLevels, char gridList[][LINE_LEN] ) {

  /* Reset args. */
  mg_op[0] = '\0' ;
  *pminVol = TOO_MUCH ;
  *pmLevels = 1 ;
  

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;
    

  /* Parse line of unix-style optional args. */
  char c ;
  /* colon behind arg means argument to be read,
     double colon means optional argument, but needs to be joined: -a0, not -a 0. 
     elem collapse coarsening: -m mLevels
     intergrid ops for sequence: -s n0 n1 ... nN 
     volmin in-situ coarsening: -v volMin
  */
  optind = 0 ;
  while ((c = getopt_long ( mArgs, ppArgs, "c:stv:",NULL,NULL)) != -1) {
    switch (c)  {
    case 'c':
      strcpy ( mg_op, "coarsen" ) ;
      *pmLevels = atoi( optarg ) ;
      break;
    case 's':
      strcpy ( mg_op, "sequence" ) ;
      break;
    case 't':
      strcpy ( mg_op, "test" ) ;
      break ;
    case 'v':
      strcpy ( mg_op, "volmin" ) ;
      *pminVol = atof( optarg ) ;
      break;
    case '?':
      if ( optopt == 'l' )
        fprintf (stderr, "Option -%c requires an argument.\n", optopt );
      else if (isprint (optopt)) {
        sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
      else {
        sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }


  char keyword[LINE_LEN] ;
  // First non opt arg, is it text? Determine mg operation.
  if ( mg_op[0] == '\0' ) {
    if ( optind < mArgs ) {
      strcpy ( keyword, ppArgs[optind++] ) ;
    
      if ( !strncmp( keyword, "sequence", 2 ) ) {
        strcpy ( mg_op, "sequence" ) ;
      }
      else if ( !strncmp( keyword, "volmin", 2 ) ) {
        strcpy ( mg_op, "volmin" ) ;
      }
      else if ( is_int ( keyword ) ) {
        // int is next, so mLevels for bkwd compat.
        strcpy ( mg_op, "coarsen" ) ;
        // Read the numeric value of the arg again below.
        optind-- ;
      }
      else {
        hip_err ( warning, 1, "no valid mg operation specified, ignored." ) ;
      }
    }
    else {
      // only mg arg: means coarsen one level.
      strcpy ( mg_op, "coarsen" ) ;
    }
  }
  



  
  /* Further numeric args? Note that mg_op could have been specified
     by Unix style -m/v/s option*/
  /* Read the remaining list of number args, mg_op will inform
     what they mean. */
  if ( optind < mArgs ) {
    switch ( mg_op[0] ) {
    case 'c':
      /* elem collapse coarsen, look for mLevels. */
      *pmLevels = atoi ( ppArgs[optind++] ) ;
      break ;
    case 's' :
    case 't' :
      /* sequence. look for a list of nGrid. */
      /* test. look for first and last grid in test sequence. */
      for ( ; optind < mArgs ; optind++ ) {
        strncpy( gridList[(*pmLevels)++], ppArgs[optind], LINE_LEN ) ;
      }
      break ;
    case 'v' :
      /* volmin in-situ collapse, look for double volmin. */
      *pminVol = atof ( ppArgs[optind++] ) ;
      break ;
    }
  }


    
      
  /* Check args. */
  if ( mg_op[0] == 'c' ) {
    if ( *pmLevels <= 0 ) {
      sprintf ( hip_msg,
                "cowardly ignoring non-positive number of levels %d, using 1.",
                *pmLevels ) ;
      hip_err ( warning, 1, hip_msg ) ;
      *pmLevels = 1 ;
    }
  }
  else if ( mg_op[0] == 'v' ) {

    if ( *pminVol <= 0 ) {
      sprintf ( hip_msg, "cowardly ignoring non-positive value for minimum volume %g.",
                *pminVol ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return (1) ;
    }
    else if ( Grids.PcurrentGrid->uns.type != uns ) {
      hip_err ( warning, 1, "can only apply volume coarsening to unstructured grid.");
      return ( 2 ) ;
    }
  }
  else if ( mg_op[0] == 's' ) {
    if ( *pmLevels < 2 ) {
      hip_err ( warning, 1, "need at least 2 grids to build a mg sequence." ) ;
      return ( 3 );
    }
  }
  else if ( mg_op[0] == 't' ) {
    if ( *pmLevels < 2 ) {
      if ( *pmLevels < 1 || gridList[0][0] == '\0' ) {
        hip_err ( blank, 1, "testing restriction for all grids in sequence." ) ;
        sprintf ( gridList[0], "%d", 0 ) ;
        sprintf ( gridList[1], "%d", 9999 ) ; // Find out later how many levels there are.
        return ( 3 );
      }
      else {
        hip_err ( blank, 1, "testing restriction for all grids in sequence." ) ;
        sprintf ( gridList[0], "%d", 9999 ) ; // Find out later how many levels there are.
        sprintf ( gridList[1], "%d", 0 ) ;
        return ( 3 );
      }
    }
  }

  return ( 0 ) ;
}


/******************************************************************************

  match_per_in_all_edges:
  Match periodic edges. Here I chose not to match the mid-edge locations, since
  there is no guarantee of spacial distance of at least epsOverlap between them.
  This is only guaranteed for the vertices.
  Instead, we match both end vertices.
  
  Last update:
  ------------
  30Nov15; fixe bug with indexing 2*nBc+1, not -1 in free.
  4Apr13; make all large counters ulong_t.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int match_per_in_all_edges ( uns_s *pUns, llEdge_s *pllEdge, edgeLen_s egLen[] ) {

  const elem_struct *pElem ;
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVx0, *pVx1,  *pVxP0, *pVxP1 ;
  const int *kVxEdge ;
  
  ulong_t mEdges, nEdge, nEdgeP ;
  int mPerVxBc[MAX_PER_PATCH_PAIRS], iEg, sw, nBc ;
  perVx_s *pPerVxBc[MAX_PER_PATCH_PAIRS];
  ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS] ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  
  /* Find a set of matching vertices. */
  if ( !make_perVxPairs ( pUns, pPerVxBc, ndxPerVxBc, mPerVxBc ) ) {
    printf ( " FATAL: could not establish periodic vertex pairs"
             " in match_per_in_all_edges.\n" );
    return ( 0 ) ; }

  /* Is there multiple periodicity. */
  else if ( !mult_per_vert ( pUns, mPerVxBc, pPerVxBc, ndxPerVxBc, 1 )
            || pUns->multPer ) {
    printf ( " WARNING: multiple peridoicity was found.\n"
             "          Adaption and coarsening work only for simple periodicity.\n" ) ;
    return ( 0 ) ;
  }

  /* Scan up to the last used edge. */
  get_number_of_edges ( pllEdge, &mEdges ) ;

  /* Reset. */
  for ( nEdge = 1 ; nEdge <= mEdges ; nEdge++ )
    egLen[nEdge].perEdge = egLen[nEdge].mBnd = 0 ;



  /* Tag all edges on periodic surfaces. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( !strncmp( pUns->ppBc[nBc]->text, "hip_per_", 8 ) ||
         pUns->ppBc[nBc]->type[0] == 'l' || pUns->ppBc[nBc]->type[0] == 'u' ) {
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          
          if ( pBndFc->Pelem && pBndFc->Pelem->number ) {
            pElem = pBndFc->Pelem ;
            pElT = elemType + pElem->elType ;
            pFoE = pElT->faceOfElem + pBndFc->nFace ;
            for ( iEg = 0 ; iEg < pFoE->mFcEdges ; iEg++ ) {
              kVxEdge = pElT->edgeOfElem[ pFoE->kFcEdge[iEg] ].kVxEdge ;
              pVx0 = pElem->PPvrtx[ kVxEdge[0] ] ;
              pVx1 = pElem->PPvrtx[ kVxEdge[1] ] ;
              if ( pVx0 != pVx1 ) {
                /* A non-collapsed edge. Find it. */
                if ( !( nEdge = get_edge_vrtx ( pllEdge, &pVx0, &pVx1, &sw ) ) ) {
                  sprintf ( hip_msg, "could not find edge %d in element %"FMT_ULG", %"FMT_ULG"-%"FMT_ULG","
                           " in match_per_in_all_edges.\n", iEg, pElem->number,
                           pVx0->number, pVx1->number ) ;
                  hip_err ( fatal, 0, hip_msg ) ;
                }
                else
                  egLen[nEdge].mBnd = 1 ;
              }
            }
          }
    }
  
  


  /* Loop over all periodic edges. */
  for ( nEdge = 1 ; nEdge <= mEdges ; nEdge++ )
    if ( egLen[nEdge].mBnd ) {
      show_edge ( pllEdge, nEdge, ( vrtx_struct ** ) &pVx0, ( vrtx_struct ** ) &pVx1 ) ;

      pVxP0 = find_perVxPartner ( pUns, pVx0, mPerVxBc, pPerVxBc, ndxPerVxBc ) ;
      pVxP1 = find_perVxPartner ( pUns, pVx1, mPerVxBc, pPerVxBc, ndxPerVxBc ) ;

      if ( !pVxP0 || !pVxP1 ) {
        sprintf ( hip_msg, "no match for vertices %"FMT_ULG", %"FMT_ULG" of edge %"FMT_ULG""
                  " in match_per_in_all_edges.\n", pVx0->number, pVx1->number, nEdge ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else {
        /* Find the edge that goes with these two vertices. */
        if ( !( nEdgeP = get_edge_vrtx ( pllEdge, &pVxP0, &pVxP1, &sw ) ) ) {
          printf ( "no match for edge %"FMT_ULG" with vertices %"FMT_ULG", %"FMT_ULG""
                   " in match_per_in_all_edges.\n", nEdge, pVx0->number, pVx1->number ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }

        else {
          /* Properly matched periodic pair of edges. */
          egLen[nEdge ].perEdge = nEdgeP ;
          egLen[nEdgeP].perEdge = nEdge ;
          egLen[nEdge ].mBnd = egLen[nEdgeP].mBnd = 0 ;
        }
      }
    }


  /* Free the auxiliary storage. */
  for ( nBc = 0 ; nBc < pUns->mPerBcPairs ; nBc++ ) {
    arr_free ( pPerVxBc[nBc] ) ;
    arr_free ( ndxPerVxBc[2*nBc  ] ) ;
    arr_free ( ndxPerVxBc[2*nBc+1] ) ;
  }
  
  
  return ( 1 ) ;
}


/******************************************************************************

  stretched_elem:
  Calculate the aspect ratio of an element in a specific direction. The aspect
  ratio will be the average length of an edge kMinEg and its parallel siblings
  compared to the average length of the other edges.
  Note that the original element is considered, not the current partly collapsed
  state.
  
  Last update:
  ------------
  4Apr13; make all large counters ulong_t.
  16Mar98: derived from ar_elem. Should replace that one. fix this.
  
  Input:
  ------
  pElem
  kMinEg
  
  Returns:
  --------
  the aspect ratio.
  
*/

int stretched_elem ( const elem_struct *pElem, const double arCutOff2,
                     int *pkMinEg, int *pmShortEg, int kShortEg[],
                     int *pkMaxEg, int *pmLongEg, int kLongEg[] ) {

  const elemType_struct *pElT ;
  double longestEg = -TOO_MUCH, shortestEg = TOO_MUCH, egLen ;
  int kEg ;
  const int *kVxEdge ;

  pElT = elemType + pElem->elType ;
  longestEg = -TOO_MUCH, shortestEg = TOO_MUCH ;

  /* Find longest and shortest edge-lengths. */
  for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
    kVxEdge = pElT->edgeOfElem[kEg].kVxEdge ;
    egLen = sq_distance_dbl( pElem->PPvrtx[ kVxEdge[0] ]->Pcoor,
                             pElem->PPvrtx[ kVxEdge[1] ]->Pcoor, pElT->mDim ) ;
    if ( egLen < shortestEg ) {
      *pkMinEg = kEg ;
      shortestEg = egLen ;
    }
    if ( egLen > longestEg ) {
      *pkMaxEg = kEg ;
      longestEg = egLen ;
    }
  }
  


  /* Find long and short edges. */
  *pmShortEg = *pmLongEg = 0 ;
  for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
    kVxEdge = pElT->edgeOfElem[kEg].kVxEdge ;
    egLen = sq_distance_dbl( pElem->PPvrtx[ kVxEdge[0] ]->Pcoor,
                             pElem->PPvrtx[ kVxEdge[1] ]->Pcoor, pElT->mDim ) ;

    if ( egLen < longestEg/arCutOff2 )
      kShortEg[ (*pmShortEg)++ ] = kEg ;

    if ( egLen > shortestEg*arCutOff2 )
      kLongEg[ (*pmLongEg)++ ] = kEg ;
  }

  
  if ( *pmShortEg )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}



/******************************************************************************

  vx_properties:
  
  Find the priority of vertices and edges on the boundary according to
  the number of boundary patches it is included in. Vertices and edges on
  corners also receive an increase.

  On coarser grids, inherit the priority from the finest mesh. On the finest
  grid calculate it.
  
  Last update:
  ------------
  2Feb18; fix up calculation of ridegs and corners with temp use of
          .color, move bnd reduction to umg_maxCollEg.
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  13Apr00; pVxColor->maxColl is now 1,2,3 for 1D, 2D or 3D collapse. Keep
           the lengths in try_toCollapse.
  3Mar00, fix bug with coarsening for 2D situations. Look at mLongEg, change
          coarsen1d.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int vx_properties ( uns_s *pUns,
                           const int mEdges, llEdge_s *pllEdge, edgeLen_s *pEgLen,
                           const double arCutOff ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  uns_s *pUnsF = pUns->pUnsFine ;
  const int mDim = pUns->mDim ;
  const vrtx_struct *pVxEg[2] ;
  const double arCutOff2 = arCutOff*arCutOff ;

  /* How many long edges may there be for a stretched element element of any type 
     to do 1d coarsening? E.g. flat hex has 8 long edges. */
  const int coarsen1d[] = {1,2,4,6,6,8} ;
  /* How many short edges to apply 2d coarsening for this elment type, if stretched. 
     e.g. 'pencil' hex has 8 short edges. */
  const int coarsen2d[] = {3,4,2,2,6,8} ; 

  chunk_struct *pChunk ;
  ulong_t mVx = pUns->mVertsNumbered, nVx, mVxFixed, mVxF, iVx, iEg, mEgFixed ;
  int kEg, sw ;
  ulong_t nEg ;
  int m0, m1, maxColl, minColl, kMinEg, kMaxEg, 
    kShortEg[MAX_EDGES_ELEM], kLongEg[MAX_EDGES_ELEM], mShortEg, mLongEg, kVx ;
  ulong_t nEgF, nVxEgF[2], nVxEgC[2], mEgFine ;
  color_s *pVxColor ;
  eg2el_s *pEg2El, *pEE ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBfBeg, *pBfEnd, *pBf, *pBf0, *pBf1 ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  edgeLen_s *pEL, *pEgLenFine ;
  double fcNorm0[MAX_DIM], fcNorm1[MAX_DIM], scProd ;
  llEdge_s *pllEdgeFine ;
  

  if ( pUnsF && pUnsF->pVxColor && pUnsF->pnVxCollapseTo ) {
    /* Inherit the boundary counters from the finest mesh. */

    pVxColor = arr_malloc ( "pVxColor in vx_properties", pUns->pFam,
                            mVx+1, sizeof( *pVxColor ) ) ;

    /* Reset. */
    for ( nVx = 1 ; nVx <= mVx ; nVx++ ) {
      pVxColor[nVx].mVxColl = 1 ;
      pVxColor[nVx].mBnd = pVxColor[nVx].mark = 0 ;
      pVxColor[nVx].mark = 0 ;
      pVxColor[nVx].maxColl = 1 ;
    }
     
    mVxF = pUnsF->mVertsNumbered ;
    for ( iVx = 1 ; iVx <= mVxF ; iVx++ ) {
      nVx = pUnsF->pnVxCollapseTo[iVx] ;
      pVxColor[nVx].mBnd = MAX( pVxColor[nVx].mBnd, pUnsF->pVxColor[iVx].mBnd ) ;
      pVxColor[nVx].maxColl = MAX( pVxColor[nVx].maxColl, 
                                   pUnsF->pVxColor[iVx].maxColl );
    }
    

    /* Inherit the boundary counters from the edges in the fine mesh. */
    pllEdgeFine = pUnsF->pllEdge ;
    pEgLenFine  = pUnsF->pEgLen ;
    get_number_of_edges ( pllEdgeFine, &mEgFine ) ;
    for ( nEgF = 1 ; nEgF <= mEgFine ; nEgF++)
      if ( show_edge( pllEdgeFine, nEgF,
                      ( vrtx_struct ** ) pVxEg, ( vrtx_struct ** ) pVxEg+1 ) ) {
        nVxEgF[0] = pVxEg[0]->number ;
        nVxEgF[1] = pVxEg[1]->number ;
        nVxEgC[0] = pUns->pUnsFine->pnVxCollapseTo[ nVxEgF[0] ] ;
        nVxEgC[1] = pUns->pUnsFine->pnVxCollapseTo[ nVxEgF[1] ] ;
        
        
        if ( nVxEgC[0] != nVxEgC[1] ) {
          /* This edge isn't collapsed. Find the coarse grid remains. */
          /* Nasty. But for the time hardwire a single chunk. Fix this. */
          pVxEg[0] = pUns->ppChunk[0]->Pvrtx + nVxEgC[0] ;
          pVxEg[1] = pUns->ppChunk[0]->Pvrtx + nVxEgC[1] ;
          
          if ( ( nEg = get_edge_vrtx ( pllEdge, pVxEg, pVxEg+1, &sw ) ) ) {
            /* The edge exists on the coarse mesh.
               By default, the edge is fully coarsenable. */
            pEgLen[nEg].mBnd = pEgLenFine[nEgF].mBnd ;
          }
        }
      }

    
    pUns->pVxColor = pVxColor ;
    arr_free ( pUnsF->pVxColor ) ;
    pUnsF->pVxColor = NULL ;
  }
  else {
    /* Make a new list from the current grid. */
    /* Count the boundaries for each vertex. */
    pVxColor = color_vx ( pUns, NULL ) ;

    maxColl = ( mDim == 2 ? 2 : 3 ) ;
    /* Reset the flag for each vertex. */
    for ( nVx = 1 ; nVx <= mVx ; nVx++ ) {
      pVxColor[nVx].mVxColl = 1 ;
      pVxColor[nVx].maxColl = 0 ;
      pVxColor[nVx].mark = 0 ;
      pVxColor[nVx].color = 0 ;
    }




    /* Loop over all elements and find out how much a vertex is to be
       coarsenend.  1-D, 2-D or 3-D coarsenening. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number ) {
          /* How many short edges are there? */
          if ( stretched_elem ( pElem, arCutOff2,
                                &kMinEg, &mShortEg, kShortEg,
                                &kMaxEg, &mLongEg, kLongEg ) ) {
            if ( mLongEg >= coarsen1d[ pElem->elType ] )
              /* mLongEg are all the edges longer than arCutOff times the shortest
                 edge. */
              maxColl = 1 ;
            else if ( mShortEg <= coarsen2d[ pElem->elType ] || mDim == 2 )
              /* mShortEg are all the edges shorter than arCutOff times the longest
                 edge. */
              maxColl = 2 ;
            else
              maxColl = 3 ;
          }
          else if ( mDim == 2 )
            maxColl = 2 ;
          else
            maxColl = 3 ;
          
          /* Scatter this collapse dimension to the vertices. 
             Use a MAX, this eliminates single stray short edges. */
          for ( kVx = 0 ; kVx < elemType[ pElem->elType ].mVerts ; kVx++ ) {
            nVx = pElem->PPvrtx[kVx]->number ;
            pVxColor[nVx].maxColl = MAX( pVxColor[nVx].maxColl, maxColl );
          }
        }



  
    /* Edge to boundary face pointers. Use them to find dihedral angles and
       increase the priority for vertices at discontinuities. */
    pEg2El = arr_malloc ( "pEg2El in vx_properties", pUns->pFam,
                          mEdges+1, sizeof ( *pEg2El ) ) ;
    for ( nEg = 1 ; nEg <= mEdges ; nEg++ )
      pEg2El[nEg].pBndFc[0] = pEg2El[nEg].pBndFc[1] = NULL ;
    
    /* Loop over all boundary faces and flag all boundary edges. */
    pChunk = NULL ;
    while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBfBeg, &pBfEnd ) )
      for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ )
        if ( pBf->Pelem && pBf->nFace && !pBf->Pelem->invalid ) {
          /* This is an active boundary face. Flag its edges. */
          pElem = pBf->Pelem ;
          pElT = elemType + pElem->elType ;
          pFoE = pElT->faceOfElem + pBf->nFace ;
          
          for ( iEg = 0 ; iEg < pFoE->mFcEdges ; iEg++ ) {
            kEg = pFoE->kFcEdge[iEg] ;
            nEg = get_elem_edge ( pllEdge, pElem, kEg, pVxEg, pVxEg+1, &sw ) ;

            if ( nEg == -1 )
              /* Collapsed edge, nothing to be done. */
              ;            
            else if ( nEg == 0 ) {
              /* Not found.  Hmm*/
              sprintf ( hip_msg, "edge from %"FMT_ULG" to %"FMT_ULG" not listed"
                        " in vx_properties.", 
                        pVxEg[0]->number, pVxEg[1]->number ) ;
              hip_err ( fatal, 0, hip_msg ) ;
            }
            else if ( !pEg2El[nEg].pBndFc[0] )
              pEg2El[nEg].pBndFc[0] = pBf ;
            else if ( !pEg2El[nEg].pBndFc[1] )
              pEg2El[nEg].pBndFc[1] = pBf ;
            else {
              sprintf ( hip_msg, "triply referenced boundary edge %"FMT_ULG""
                       " in vx_properties,", nEg ) ;
              hip_err ( warning, 1, hip_msg ) ;
            }
          }
        }
    
    
    for ( mEgFixed = 0, nEg = 1 ; nEg <= mEdges ; nEg++ ) {
      pEE = pEg2El + nEg ;
      pEL = pEgLen + nEg ;
      
      if ( !pEE->pBndFc[0] )
        /* Internal edge. */
        pEL->mBnd = 0 ;
      else if ( !pEE->pBndFc[1] )
        /* Just one boundary face adjoining. 2D. 
           JDM: 1Feb18: hence nothing implemented for 2D. May need to be done. */
        pEL->mBnd = 1 ;
      else {
        /* Two boundary faces. Are they separate? */
        pBf0 = pEE->pBndFc[0] ;
        pBf1 = pEE->pBndFc[1] ;

        if ( pBf0->Pbc == pBf1->Pbc )
          /* Same bc. */
          pEL->mBnd = 1 ;
        else 
          pEL->mBnd = 2 ;

        /* Check the angularity. */
        uns_face_normal ( pBf0->Pelem, pBf0->nFace, fcNorm0, &m0 ) ;
        vec_norm_dbl ( fcNorm0, mDim ) ;
        uns_face_normal ( pBf1->Pelem, pBf1->nFace, fcNorm1, &m1 ) ;
        vec_norm_dbl ( fcNorm1, mDim ) ;
        scProd = scal_prod_dbl ( fcNorm0, fcNorm1, mDim ) ;

        if ( scProd < normAnglCut ) {
          /* This is a corner. Augment the priority counters. Count the number of
             'ridge' edges impinging on each node, stor in .color. */
          mEgFixed++ ;
          pEL->mBnd++ ;
          
          show_edge ( pllEdge, nEg, (VX**)pVxEg, (VX**)pVxEg+1 ) ;
          pVxColor[ pVxEg[0]->number ].color++ ;
          pVxColor[ pVxEg[1]->number ].color++ ;
        }
      }
    }

    arr_free ( pEg2El ) ;


    /* Increment the priority of all vertices on sharp edges. */
    for ( mVxFixed = 0, nVx = 1 ; nVx <= mVx ; nVx++ ) {
      if ( pVxColor[nVx].mBnd ) {
        /* This node is on a boundary surface. All nodes on boundary
           faces only get part of the collapse. */
 
        if ( pVxColor[nVx].color > 2 ) {
          /* Corner. */
          mVxFixed++ ;
          /* Same as 3 bnd impinging. */
          pVxColor[nVx].mBnd = MAX( pVxColor[nVx].mBnd,3) ;
        }
        else if ( pVxColor[nVx].color > 1 ) {
          /* Ridge. */
          mVxFixed++ ;
          /* Same as 2 bnd impinging. */
          pVxColor[nVx].mBnd = MAX( pVxColor[nVx].mBnd,2) ;
        }
 
        /* Reduce collapse count to half at boundaries. 
           JDM, Feb18: that seems a daft way of doing it. Rather than giving a fraction
           of the collapse count, this reduces the dimensionality of the collapse. Not
           the same thing. 
           Consider revising application of maxcoll, e.g. dividing in half if mBnd is
           set. 
           Now done in umg_maxCollEg, applied to count, not dim.
        pVxColor[nVx].maxColl = MAX( 1, pVxColor[nVx].maxColl/2 ) ; 
*/
      }
    }


    /* Finished using .color in vx_properties. What does .color mean elsewhere? */
    for ( nVx = 1 ; nVx <= mVx ; nVx++ ) {
      pVxColor[nVx].color = 0 ;
    }

    if ( verbosity > 3 ) {
      sprintf ( hip_msg, "increased the priority for %"FMT_ULG" vertices, "
               "%"FMT_ULG" edges on sharp corners.\n", mVxFixed, mEgFixed ) ;
      hip_err ( info, 2, hip_msg ) ;
    }
  }




  for ( nVx = 1 ; nVx <= pUns->mVertsNumbered ; nVx++ ) {
    if ( pVxColor[nVx].maxColl < 1 || pVxColor[nVx].maxColl > 3 ) {
      sprintf ( hip_msg, "vx %"FMT_ULG": maxColl %d\n", nVx, pVxColor[nVx].maxColl ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  
  return ( 1 ) ;
}


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int move_edge  ( uns_s *pUns, const vrtx_struct *pMgVrtx, const double *pMgCoor,
                        llEdge_s *pllEdge, edgeLen_s **ppEdgeLen, int nOldEg ) {

  int nNewEg, nVx[2], sw, isNew ;
  const vrtx_struct *pVxEg[2] ;
  edgeLen_s *pOldEgl, *pNewEgl ;
  
  if ( show_edge( pllEdge, nOldEg, (VX**) pVxEg, (VX**) pVxEg+1 ) ) {
    nVx[0] = mgVrtx ( pMgVrtx, pMgCoor, pUns->mDim, pVxEg[0]->number ) ;
    nVx[1] = mgVrtx ( pMgVrtx, pMgCoor, pUns->mDim, pVxEg[1]->number ) ;

    if ( nVx[0] == nVx[1] ) {
      /* This is a collapsed edge, delete the edge. */
      del_edge ( pllEdge, nOldEg ) ;
      return ( 0 ) ;
    }
          
    else if ( nVx[0] != pVxEg[0]->number ||
              nVx[1] != pVxEg[1]->number ) {
      /* This edge has changed endpoints. Get the new final edge, update
         the parameters. */
      pVxEg[0] = pUns->ppChunk[0]->Pvrtx + nVx[0] ;
      pVxEg[1] = pUns->ppChunk[0]->Pvrtx + nVx[1] ;

      if ( !( nNewEg = get_edge_vrtx( pllEdge, pVxEg, pVxEg+1, &sw ) ) ) {
        /* Make a new edge and reset the properties. */
        if ( !( nNewEg = add_edge_vrtx( pllEdge, ( void **) ppEdgeLen,
                                        pVxEg, pVxEg+1, &sw, &isNew ) ) ) {
          printf ( " FATAL: could not add edge in tryToCollapse_elem.\n" ) ;
          return ( 0 ) ; }
        
        /* Initialize the new edge. */
        pNewEgl = (*ppEdgeLen) + nNewEg ;
        /*pNewEgl->length = 0. ;*/
        pNewEgl->shortEg = pNewEgl->longEg = pNewEgl->mBnd = 0 ;
        pNewEgl->perEdge = 0 ;
      }
    
      /* Collapse edge properties. */
      pOldEgl = (*ppEdgeLen) + nOldEg ;
      pNewEgl = (*ppEdgeLen) + nNewEg ;
      pNewEgl->shortEg        |= pOldEgl->shortEg         ;
      pNewEgl->longEg         |= pOldEgl->longEg          ;
      pNewEgl->nonCollapsible |= pOldEgl->nonCollapsible  ;

      pNewEgl->mBnd  = MAX( pNewEgl->mBnd,  pOldEgl->mBnd ) ;
      /* pNewEgl->length = MAX( pNewEgl->length, pOldEgl->length ) ;*/
      
      del_edge ( pllEdge, nOldEg ) ;
      return ( nNewEg ) ;
    }
    
    else
      /* Nothing to be done. */
      return ( nOldEg ) ;
  }

  else 
    /* Non-existent edge. */
    return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  umg_maxCollEg:
*/
/*! given pVxColor data for two nodes of an edge, determine collapse count
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  1Feb18: conceived.
  

  Input:
  ------
  pVxCol0, pVxCol1: pointers to pVxColor of the forming vertices
  maxCollDim: array of max collapse count per dimension

  Returns:
  --------
  maximal collapse count for the edge.
  
*/

int umg_maxCollEg ( const int maxCollDim[MAX_DIM+1],
                    const color_s *pVxCol0, const color_s *pVxCol1 ) {

  /* An n-dimensional collapse results in mgLen^n collapses into a node. */
  int collDim = MIN ( pVxCol0->maxColl, pVxCol1->maxColl ) ;

  /* Old version. */
  //if ( pVxCol0->mBnd || pVxCol1->mBnd ) 
  //collDim = MAX( 1, collDim/2 ) ;

  int maxCollEg = maxCollDim[ collDim ] ;
  
  if ( pVxCol0->mBnd || pVxCol1->mBnd )
    /* Half it for boundary nodes. */
    maxCollEg = MAX(maxCollDim[1],1./2.*maxCollEg) ;
  
  return ( maxCollEg ) ;
}


/******************************************************************************

  add_stack:
  Add an int to a simple linear list, provided that it is not in the list
  already or the size of the list exhausted.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_vxStack ( const vrtx_struct *pVxStack[], int *pmVxStack,
			 const int maxVxStack, const vrtx_struct *pVx )
{
  int kS ;
  
  /* Find out whether nNew is already listed in nVxStack. */
  for ( kS = 0 ; kS < *pmVxStack ; kS++ )
    if ( pVxStack[kS] == pVx )
      break ;
		
  if ( kS == *pmVxStack ) {
    /* Not yet present, add it. */
    if ( *pmVxStack >= maxVxStack-1 ) {
      printf ( " FATAL: VxStack size %d exceeded in add_vxStack.\n", maxVxStack ) ;
      return ( 0 ) ;
    }
    pVxStack[(*pmVxStack)++] = pVx ;
  }

  return ( 1 ) ;
}

int add_elStack ( const elem_struct *pElStack[], int *pmElStack,
			 const int maxElStack, const elem_struct *pEl )
{
  int kS ;
  
  /* Find out whether nNew is already listed in nElStack. */
  for ( kS = 0 ; kS < *pmElStack ; kS++ )
    if ( pElStack[kS] == pEl )
      break ;
		
  if ( kS == *pmElStack ) {
    /* Not yet present, add it. */
    if ( *pmElStack >= maxElStack-1 ) {
      printf ( " FATAL: ElStack size %d exceeded in add_elStack.\n", maxElStack ) ;
      return ( 0 ) ;
    }
    pElStack[(*pmElStack)++] = pEl ;
  }

  return ( 1 ) ;
}

int add_egStack ( int egStack[], int *pmEgStack,
			 const int maxEgStack, const int nEg )
{
  int kS ;
  
  /* Find out whether nNew is already listed in nEgStack. */
  for ( kS = 0 ; kS < *pmEgStack ; kS++ )
    if ( egStack[kS] == nEg )
      break ;
		
  if ( kS == *pmEgStack ) {
    /* Not yet present, add it. */
    if ( *pmEgStack >= maxEgStack-1 ) {
      printf ( " FATAL: EgStack size %d exceeded in add_EgStack.\n", maxEgStack ) ;
      return ( 0 ) ;
    }
    egStack[(*pmEgStack)++] = nEg ;
  }

  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  elem_isnt_lowAngle:
*/
/*! check whether face and dihedral angles, face twist are acceptable.
 *
 */

/*
  
  Last update:
  ------------
  23Mar17: extracted from mg_allConvex
  

  Input:
  ------
  pElMg: pointer to element with collapsed nodes.
  pElemVol: its volume
  mDim: dimension
  pElOrig: pointer to element before lastest collapse

  Changes To:
  -----------

  Output:
  -------
  pIsNotColl: 
    
  Returns:
  --------
  1 if angularity is acceptable/convex, 0 on success
  
*/

int elem_is_lowAngle ( elem_struct *pElMg, 
                       double *pElemVol,
                       const int mDim,
                       const elem_struct *pElOrig,
                       int *pIsNotColl,
                       const double lvlLrgstAngle,
                       const int doAllTests, elCollFail_s *pFail ) {

  *pIsNotColl = 1 ;
  double hMin, hMinOrig ;
  double dihAngle, fcAngle, lrgstAngle, origLrgstAngle ;
  double oldVol ;
  int kFcTwistMin ;
  int failure = 0 ;
  double twistMin ;

  lrgstAngle = maxAngle ( pElMg, pElemVol, pIsNotColl, &hMin, &dihAngle, &fcAngle ) ;
  if ( *pIsNotColl && lrgstAngle <= lvlLrgstAngle ) {
    /* If the angle is extreme, compare it to the original one. */
    /* See bug notice in maxAngle, 9Jul13. A possible compiler bug
       forces removing the const there, so cast it here to suppress
       compiler warning. */
    origLrgstAngle = maxAngle ( 
                               //pEl, 
                               (elem_struct *) pElOrig, 
                               &oldVol, pIsNotColl, &hMinOrig, &dihAngle, &fcAngle ) ;
        
    if ( lrgstAngle < origLrgstAngle ) {
      /* Collapse increases the largest angle. Reject. */
#           ifdef DEBUG
      if ( verbosity > 7 ) {
        printf ( "                         " ), printel ( pElMg ) ;}
#           endif
      if ( doAllTests )
        failure = pFail->angle = 1 ;
      else
        return ( 0 ) ;
    }
  }

  if ( *pIsNotColl ) {
    /* Element is not collapsed. Keep checking. Does it give a proper volume? */
    if ( mDim == 3 ) hMin *= sqrt( hMin ) ;
    if ( *pElemVol < mgVolAspect*hMin ) {
      if ( doAllTests )
        failure = pFail->volume = 1 ;
      else
        return ( 0 ) ;
    }
      

    /* Element is not collapsed. Keep checking. */
    if ( ( twistMin = get_lrgstFaceTwist ( pElMg, &kFcTwistMin ) ) < mgTwistMin ) {
#         ifdef DEBUG
      if ( verbosity > 7 ) {
        printf ( "        twist %6f at face %d ", twistMin, kFcTwistMin ) ;
        printfc ( pElMg, kFcTwistMin ) ;
        printf ( "                         " ), printel ( pElMg ) ;}
#         endif
      if ( doAllTests )
        failure = pFail->twist = 1 ;
      else
        return ( 0 ) ;
    }
        

  }
  // A fail can get us here when doAllTests is active.
  return ( 1-failure ) ;
}


/******************************************************************************

  mg_allConvex:
  Check whether the grid attached to a number of nodes is composed of elements
  with positive volume and a maximum angle below the tolerance. The coordinates
  taken into consideration are the collapsed ones.
  
  Last update:
  ------------
  13Feb19; promote possibly large int to ulong_t.
  2Feb18; add the forgotton angularity check post convexity check.
          move toward umg_ nomeclature.
  16Feb17; consider convexity using subvolumes, call to elem_is_convex.
  17Dec10; remove code referencing undefined kEgMaxAngle exposed by -DDEBUG
  8Dec99; reintro the volAspect criterion.
  14Jul99; return a 0. volume for collapsed elements.
  : conceived.
  
  Input:
  ------
  pUns:
  pElColl:    the collapsed element. It will not be tested.
  pMgVrtx:  the list of collapsed vertices
  pMgCoor:  and their coordinates.
  pVxCheck: the list of vertex pointers to check
  mVx:      the number of vertices to check.
  
  Returns:
  --------
  0 on failure, 1 on proper geometry.
  
*/

int umg_allConvex ( const uns_s *pUns, 
                   const vrtx_struct *pMgVrtx, double *pMgCoor,
                   const double lvlLrgstAngle,
                   const vrtx_struct *pVxStack[], int *pmVxStack,
                   const elem_struct *pElStack[], double elemVol[],
                   int *pmElStack,
                   const int doAllTests, elCollFail_s *pFail ) {
  
  const elemType_struct *pElT ;
  const elem_struct *pEl ;
  const int *kVxEdge ;

  elem_struct elem, *pElem ;
  vrtx_struct vrtx[MAX_VX_ELEM], *pVrtx[MAX_VX_ELEM], *pVx ;
  ulong_t iVx, nItem, nEl , nrVx, n0, n1 ;
  int kVx, nVx, mDim, kFcTwistMin, elemIsNotCollapsed, kEg, failure = 0 ;
  color_s *pVxColor ;

  pElem = &elem ;
  mDim  = pUns->mDim ;
  pVxColor = pUns->pVxColor ;

  /* Make a generic element to be filled with the collapsed geometry. Could
     we make this static? */
  elem.PPvrtx = pVrtx ;
  for ( kVx = 0 ; kVx < MAX_VX_ELEM ; kVx++ )
    pVrtx[kVx] = vrtx+kVx ;

  
  /* Make a list of all elements to check. */
  for ( iVx = 0 ; iVx < *pmVxStack ; iVx++ ) {
    nItem = 0 ;
    while ( loop_toElem ( pUns->pllVxToElem, pVxStack[iVx]->number, &nItem,
                          ( elem_struct ** ) &pEl ) )
      if ( !pEl->invalid ) {
	/* If this element is not collapsed, add it to the stack. */
        if ( !pEl->mark ) 
          add_elStack ( pElStack, pmElStack, STACK_SIZE, pEl ) ;

        /* Loop over all vertices of the element. If the terminal vertex to
           it is one of the collapsed ones, add it to the list. */
        for ( kVx = 0 ; kVx < elemType[ pEl->elType ].mVerts ; kVx++ ) {
          pVx = pEl->PPvrtx[kVx] ;
          nrVx = mgVrtx( pMgVrtx, pMgCoor, mDim, pVx->number ) ;
        
          if ( pVxColor[nrVx].mark ) {
            /* This is a collapsed vertex. Add it to the stack, if not yet present. */
            add_vxStack ( pVxStack, pmVxStack, STACK_SIZE, pVx ) ;
          }
        }
      }
  }



  /* Check. */
  int isCollapsible ;
  const elem_struct *pMgEl ;
  double lrgstAngle, hMinSq, dihAngle, fcAngle ;
  double elemVolOrig, hMinSqOrig, lrgstAngleOrig, dihAngleOrig, fcAngleOrig ;
  int isNotCollOrig ;
  for ( nEl = 0 ; nEl < *pmElStack ; nEl++ ) {
    pEl = pElStack[nEl] ;
    
    if ( !pEl->mark ) {
      /* This is not yet a collapsed element. */
      pElT = elemType + pEl->elType ;
      elem.elType = pEl->elType ;

      /* Establish collapsed vertex coordinates. */
      elem.number = pEl->number ;
      for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
        nVx = pEl->PPvrtx[kVx]->number ;
        nVx = vrtx[kVx].number = mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
        vrtx[kVx].Pcoor = pMgCoor + mDim*mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
      }

      if ( mDim == 2 ) {
        isCollapsible = elem_is_lowAngle ( pElem, elemVol+nEl, mDim, pEl,
                                      &elemIsNotCollapsed, lvlLrgstAngle,
                                      doAllTests, pFail ) ;
        if ( !isCollapsible ) 
          return ( 0 ) ;
      }
      else {
        /* 3D case. Check angularity first. */
        pMgEl = make_mgElem( pEl, pMgVrtx, pMgCoor ) ;
        lrgstAngle = maxAngle ( pMgEl, elemVol+nEl, &elemIsNotCollapsed,
                                &hMinSq, &dihAngle, &fcAngle ) ;
        
        if ( elemIsNotCollapsed ) {
          /* Element is already collapsed. */
          
          if ( lrgstAngle < lvlLrgstAngle ) {
            /* Angle is larger than level tolerance. Check whether this is due to 
               collapse, or pre-existing. If pre-existing, allow. */
            /* If the angle is extreme, compare it to the original one. */
            lrgstAngleOrig = maxAngle ( pEl, &elemVolOrig, &isNotCollOrig,
                                        &hMinSqOrig, &dihAngleOrig, &fcAngleOrig ) ;
        
            if ( lrgstAngle < lrgstAngleOrig ) {
              /* Collapse increases the largest angle. Reject. */
#           ifdef DEBUG
              if ( verbosity > 7 ) {
                printf ( "                         " ), printel ( pElem ) ;}
#           endif
              failure = pFail->angle = 1 ; 
              if ( !doAllTests )
                return ( 0 ) ;
            }
          }

          
          /* Now check for volume. Note: this is 3D. */
          if ( elemVol[nEl] < mgVolAspect*hMinSq*sqrt( hMinSq )/6 ) {
              failure = pFail->volume = 1 ;
            if ( !doAllTests )
              return ( 0 ) ;
          }

          
          /* Finally, check for convexity, replacing the former test on
             twist. */        
          const double minVolSubTet = 0. ;
          isCollapsible = elem_is_convex ( pElem, minVolSubTet, elemVol+nEl,
                                           &elemIsNotCollapsed ) ;
          if ( !isCollapsible ) {
#           ifdef DEBUG
            if ( verbosity > 7 ) {
              printf ( "                         " ), printel ( pElem ) ;}
#           endif
            failure = pFail->nonConvex = 1 ;
            if ( !doAllTests )
              return ( 0 ) ;
          }
        }  // elemIsNotCollapsed
      } // 3D

    

      /* Admissible collapse. 
         Does this non-collapsed element share any collapsed edges? */
      for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
        kVxEdge = pElT->edgeOfElem[kEg].kVxEdge ;
        n0 = vrtx[ kVxEdge[0] ].number ;
        n1 = vrtx[ kVxEdge[1] ].number ;
        if ( pVxColor[n0].color && pVxColor[n0].color == pVxColor[n1].color ) {
          /* This edge is shared. Tag the element with a zero volume to be
             heaped at the top if collapse is accepted. */
          elemVol[nEl] = 0. ;
          break ;
        }
      }
      
    } //pEl->mark, not yet marked as collapsed.
  } // for ( nEl = 0 ; nEl < *pmElStack ; nEl++ ) {

  
  return ( 1-failure ) ;
}
	
/******************************************************************************

  get_elem_edge_mg:
  Scan the ordered/linked list of edges for a match given two vertices.
  Trace each vertex to the origin of the linked list.
  
  Last update:
  ------------
  6Apr13; initialise n1,n2.
  7Nov99; don't insist on all edges being present. 
  
  Input:
  ------
  pllEdge:  the list of edges.
  pElem:    the element
  kEdge:    and the edge sought.
  
  Changes To:
  -----------
  pVrtx1/2:     the two vertices ordered for node numbers.
  pSwitch:      if the two vertices have been switched for proper ordering.
  
  Returns:
  --------
  0 on no match, the number of the matching edge nEdge on match.
  
*/

int get_elem_edge_mg ( uns_s *pUns, vrtx_struct *pMgVrtx, double *pMgCoor,
                       const int mDim, const llEdge_s *pllEdge,
                       const elem_struct *pElem, int kEdge,
                       const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
                       int *pSwitch ) {
  
  const int *kVxEdge ;
  // avoid compiler warning. But shouldn't the n1==n2 test be inside the pMGVrtx block?
  int n1=0, n2=0 ; 

  kVxEdge = elemType[ pElem->elType].edgeOfElem[kEdge].kVxEdge ;
	  
  /* The two forming nodes of the edge. */
  *ppVrtx1 = pElem->PPvrtx[ kVxEdge[0] ] ;
  *ppVrtx2 = pElem->PPvrtx[ kVxEdge[1] ] ;

  if ( pMgVrtx ) {
    /* Trace the linked list of collapsed vertices. */
    n1 = mgVrtx ( pMgVrtx, pMgCoor, mDim, (*ppVrtx1)->number ) ;
    n2 = mgVrtx ( pMgVrtx, pMgCoor, mDim, (*ppVrtx2)->number ) ;
    
    *ppVrtx1 = pUns->ppChunk[0]->Pvrtx + n1 ;
    *ppVrtx2 = pUns->ppChunk[0]->Pvrtx + n2 ;
  }

  if ( n1 == n2 )
    /* Collapsed edge. */
    return ( 0 ) ;
  else 
    return ( get_edge_vrtx ( pllEdge, ppVrtx1, ppVrtx2, pSwitch ) ) ;

}

/******************************************************************************

  edge_betweenBnd:
  Various collapses should not be allowed:
  a any interior edge connecting two boundary vertices of any priority,

  JDM Feb 18: Why?
  b any edge of a lower priority connecting two bnd vertices of some
    priority. (Bnd vertices of the same priority get averaged, thus the
    midedge position they average onto must be on the appropriate subdimensional
    manifold.) This also prevents collapsing of boundary edges between corners,
    ridges or boundary intersections.
  
  Last update:
  ------------
  5Feb18; Jusification for case b added. 
  7Nov99; treat non-exisiting edges as interior.
  9Aug99; intro priorities for edges as well.
  : conceived.
  
  Input:
  ------
  egLen:   edge properties
  nEg:     the number of the edge.
  vxColor: vertex properties
  nVx0/1:  the two forming vertices of the edge.

  Returns:
  --------
  0 if the edge is of the same priority as the least prioritary of its vertices,
  1 if its priority is lesser.
  
*/

int edge_betweenBnd ( const edgeLen_s egLen[], const int nEg,
                      const color_s vxColor[], const int nVx0, const int nVx1 ) {

  if ( !nEg ) {
    if ( vxColor[nVx1].mBnd > 0 && vxColor[nVx0].mBnd > 0 )
      /* Boundary vertices, but no such edge, assume interior. */
      return ( 1 ) ;
    else
      /* All interior. */
      return ( 0 ) ;
  }
  
  if ( MIN( vxColor[nVx0].mBnd, vxColor[nVx1].mBnd ) > egLen[nEg].mBnd )
    /* The edge has a lower priority than the lowest priority of its
       end vertices.  e.g. an edge internal to a patch (mBnd=1) that
       connects two nodes of mBnd=2 in a corner of a
       boundary. Allowing the collapse may pinch off that corner. */
    return ( 1 ) ;
  else
    /* Same priority. */
    return ( 0 ) ;
}





/******************************************************************************

  tryToCollapse_elem:
  Given an elem, collapse it if that is possible.
  
  Last update:
  ------------
  2Feb18; use umg_maxCollEg, revise how collapse count is reduced at boundaries.
  1oct17; state pFail->mNonColl when first returning w/o collapsible edges.
  5Mar00; fix problem with exceeding mVxColl when several edges in one elem
          collapse onto the same vertex.
  7Nov99; require edges only for boundaries.
  27Oct99; remove iside.
  10May99; use parallel edges.
  : conceived.
  
  Input:
  ------
  pllEdge: The list of edges
  nEg:     and the number of the edge to be collapsed.
  mDim:    Spatial dimension.

  Changes To:
  -----------
  pllEdge
  
  Returns:
  --------
  0 on no collapse, 1 on collapse.
  
*/

int tryToCollapse_elem ( elem_struct *pElem,
			 uns_s *pUns, llEdge_s *pllEdge,
			 vrtx_struct *pMgVrtx, double *pMgCoor,
			 edgeLen_s **ppEdgeLen,
			 const vrtx_struct *pVxStack[STACK_SIZE], int *pmVxStack,
                         elem_struct *pElStack[], double elemVol[],
                         int *pmElStack,
			 ulong_t *pmEgCollTried, ulong_t *pmEgColl,
                         const int maxColl[MAX_DIM+1],
                         const double lvlArCutOff, const double lvlLrgstAngle,
                         const int doAllTests, elCollFail_s *pFail ) {
  
  const elemType_struct *pElT ;
  const edgeOfElem_struct *pEoE ;
  const faceOfElem_struct *pFoE ;
  const int *kVxEg ;
  const double *pCoVx[MAX_VX_ELEM] ;

  double len, minLen, maxLen, arCutOff2, *pCo ;
  int mDim, nVx, n0, n1, ns0 = 0, ns1 = 0, nNghEg, side, failure, kVx,
    kMinEg = 0, kEg, mEg, mEgToColl, sw, iEg, kFc, mBnd0, mBnd1,
    nNewEg, nPerEg, nLstKeptEg, nLstSide, maxVxColl, mQuadFc, kQuadFc ;  
  edgeToColl_s etc[2*MAX_PARA_EDGES], *pEtc, *pEtc2 ;
  color_s *pVxColor ;

  pElT      = elemType + pElem->elType ;
  mDim	    = pUns->mDim ;
  pVxColor  = pUns->pVxColor ;
  arCutOff2 = lvlArCutOff*lvlArCutOff ;
  failure   = 0 ;

  /* In order to find the shortest edge, we need to get the current coordinates. */
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    nVx = mgVrtx( pMgVrtx, pMgCoor, mDim, pElem->PPvrtx[kVx]->number ) ;
    pCoVx[kVx] = pMgCoor + mDim*nVx ;
  }

  
  /* Find out whether this is an element collapsed into a pyr. Check whether there
     are zero, one or more quad faces. */
  for ( kFc = 1, mQuadFc = 0 ; kFc <= pElT->mFaces && mQuadFc < 2 ; kFc++ ) {
    pFoE = elemType[pElem->elType].faceOfElem + kFc ;
    if ( pFoE->mVertsFace == 4 &&
         pCoVx[ pFoE->kVxFace[0] ] != pCoVx[ pFoE->kVxFace[1] ] &&
         pCoVx[ pFoE->kVxFace[1] ] != pCoVx[ pFoE->kVxFace[2] ] &&
         pCoVx[ pFoE->kVxFace[2] ] != pCoVx[ pFoE->kVxFace[3] ] &&
         pCoVx[ pFoE->kVxFace[3] ] != pCoVx[ pFoE->kVxFace[0] ] ) {
      /* Fully quad face. */
      mQuadFc++ ;
      kQuadFc = kFc ;
    }
  }
        
    

  
  /* Find the shortest collapsible edge. */ 
  if ( mQuadFc == 1 ) {
    /* Only edges around the quad face are collapsible on a pyramid.
       Find the quad face. */
    pFoE = elemType[pElem->elType].faceOfElem + kQuadFc ;

    /* Find the shortest edge around the quad face. */
    for ( minLen = TOO_MUCH, iEg = 0 ; iEg < pFoE->mFcEdges ; iEg++ ) {
      kEg = pFoE->kFcEdge[iEg] ;
      kVxEg = pElT->edgeOfElem[kEg].kVxEdge ;
      len = sq_distance_dbl ( pCoVx[kVxEg[0]], pCoVx[kVxEg[1]], mDim ) ;
      if ( minLen > len ) {
        minLen = len ;
        kMinEg = kEg ;
      }
    }
  }
  else {
    /* Find the shortest edge of the element. */
    for ( minLen = TOO_MUCH, maxLen = -TOO_MUCH, kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
      kVxEg = pElT->edgeOfElem[kEg].kVxEdge ;
      len = sq_distance_dbl ( pCoVx[kVxEg[0]], pCoVx[kVxEg[1]], mDim ) ;
      if ( minLen > len ) {
        minLen = len ;
        kMinEg = kEg ;
      }
      maxLen = MAX( maxLen, len ) ;
    }
  }


  
  /* Make a list of all edges that should be collapsed. The shortest edge first. */
  etc[0].nEg = get_elem_edge_mg ( pUns, pMgVrtx, pMgCoor, mDim, pllEdge,
                                  pElem, kMinEg,
                                  (CV**) etc[0].pVx, (CV**) etc[0].pVx+1, &sw ) ;
  
  /* And all its parallel siblings. */
  pEoE = pElT->edgeOfElem + kMinEg ;
  mEg = pEoE->mParaEg+1 ;
  for ( kEg = 1 ; kEg < mEg ; kEg++ ) {
    pEtc = etc + kEg ;
    pEtc->nEg = get_elem_edge_mg ( pUns, pMgVrtx, pMgCoor, mDim, pllEdge,
                                   pElem, pEoE->kParaEg[kEg-1],
                                   (CV**) pEtc->pVx, (CV**) pEtc->pVx+1, &sw ) ;
  }
  
  /* And all of the periodic siblings. */
  (*pmEgCollTried ) += mEg ;
  for ( pEtc = etc ; pEtc < etc + mEg ; pEtc++ ) {
    pEtc2 = pEtc + mEg ;
    pEtc2->pVx[0] = NULL ;
    if ( pEtc->nEg && ( pEtc2->nEg = (*ppEdgeLen)[ pEtc->nEg ].perEdge ) ) {
      /* This edge has a periodic equivalent. */
      show_edge ( pllEdge, pEtc2->nEg, pEtc2->pVx, pEtc2->pVx+1 ) ;
      (*pmEgCollTried )++ ;
    }
  }
      



  
  /* Make a list of the old coordinates, etc. of each edge for a restore on failure.  */
  for ( pEtc = etc ; pEtc < etc + 2*mEg ; pEtc++ )
    if ( pEtc->pVx[0] ) {
      for ( side = 0 ; side < 2 ; side++ ) {
        /* Find the terminal vertex in the linked list. */
        nVx = pEtc->pVx[side]->number ;
        nVx = pEtc->nVx[side] = mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
        
        /* Memorize the coordinates and the original pointer. */
        pCo = pEtc->pCoor[side] = pMgCoor + mDim*nVx ;
        vec_copy_dbl ( pCo, mDim, pEtc->oldCoor[side] ) ;

        /* Memorize collapse count. */
        pEtc->mVxColl[side] = pVxColor[nVx].mVxColl ;
      }
    }
  



  /* Reset the flag. */
  pFail->nonCollEg = pFail->intBndEg = 0 ;
  pFail->egLen = pFail->mColl = pFail->angle = pFail->twist = 0 ;
  pFail->volume = pFail->nonConvex = 0 ;
  if ( doAllTests ) {

    /* Check for failure due to edge properties. */
    for ( pEtc = etc ; pEtc < etc + 2*mEg ; pEtc++ )
      if ( pEtc->pVx[0] && pEtc->nVx[0] != pEtc->nVx[1] ) {
        n0 = pEtc->nVx[0] ;
        n1 = pEtc->nVx[1] ;

        if ( (*ppEdgeLen)[ pEtc->nEg ].nonCollapsible )
          pFail->nonCollEg = 1 ;
        if ( edge_betweenBnd ( *ppEdgeLen, pEtc->nEg, pVxColor, n0, n1 ) )
          pFail->intBndEg = 1 ;

        /* A MIN here is more restrictive, but most importantly preserves
           short edges that otherwise might get eaten up by longer neighboring
           ones in layers with high aspect ratio. */
        maxVxColl = umg_maxCollEg ( maxColl, pVxColor+n0, pVxColor+n1 ) ;
        if (pEtc->mVxColl[0] + pEtc->mVxColl[1] > maxVxColl )
          pFail->mColl = 1 ;
      }
  }


  

  /* Now eliminate all edges from the list that are either already collapsed,
     tagged to be not collapsible, or are internal edges connecting  between
     boundaries.
     Collapsing all mEg edges would collapse the element. If any of the edges
     is non-collapsible, the element will not be fully collapsed. */
  int failMaxColl = 0, failEgBnd = 0 ;
  for ( pEtc = etc ; pEtc < etc + mEg ; pEtc++ ) {

    pEtc2 = pEtc + mEg ;
    n0 = pEtc->nVx[0] ;
    n1 = pEtc->nVx[1] ;
    maxVxColl = umg_maxCollEg ( maxColl, pVxColor+n0, pVxColor+n1 ) ;

    if ( !pEtc->pVx[0] || n0 == n1 )
      /* Edge is already collapsed. */
      pEtc->pVx[0] = pEtc2->pVx[0] = 0 ;
    else if ( pVxColor[n0].mVxColl + pVxColor[n1].mVxColl > maxVxColl ) {
      pEtc->pVx[0] = pEtc2->pVx[0] = 0 ;
      failMaxColl = 1 ;
    }
    else if ( ( pEtc->nEg && (*ppEdgeLen)[ pEtc->nEg ].nonCollapsible ) ||
              edge_betweenBnd ( *ppEdgeLen, pEtc->nEg, pVxColor, n0, n1) ) {
      /* Remove this pair of edges from the list. */
      pEtc->pVx[0] = pEtc2->pVx[0] = 0 ;
      failEgBnd = 1 ;
    }
    else if ( pEtc2->pVx[0] ) {
      /* There is a sibling edge. */
      ns0 = pEtc2->nVx[0] ;
      ns1 = pEtc2->nVx[1] ;
      maxVxColl = umg_maxCollEg ( maxColl, pVxColor+ns0, pVxColor+ns1 ) ;
      
      if ( pVxColor[ns0].mVxColl + pVxColor[ns1].mVxColl > maxVxColl ) {
        pEtc->pVx[0] = pEtc2->pVx[0] = 0 ;
        failMaxColl = 1 ;
      }
      else if ( ( pEtc2->nEg && (*ppEdgeLen)[ pEtc2->nEg ].nonCollapsible ) ||
            edge_betweenBnd ( *ppEdgeLen, pEtc2->nEg, pVxColor, ns0, ns1 ) ) {
        /* The edge or its periodic sibling is not collapsible. Remove the pair. */
        pEtc->pVx[0] = pEtc2->pVx[0] = 0 ;
        failEgBnd = 1 ;
      }
    }

    /* Set the collapse count now. The edges further down in the list can then
       take the reached count of collapses into account. */
    if ( pEtc->pVx[0] ) {
      pVxColor[n0].mVxColl += pVxColor[n1].mVxColl ;
      pVxColor[n1].mVxColl  = pVxColor[n0].mVxColl ;

      if ( pEtc2->pVx[0] ) {
        pVxColor[ns0].mVxColl += pVxColor[ns1].mVxColl ;
        pVxColor[ns1].mVxColl  = pVxColor[ns0].mVxColl ;
      }
    }
  }

    

  /* Count and repack the list. Note that mEg now includes the periodic edges.*/
  mEg *= 2 ;  
  for ( mEgToColl = 0, pEtc = etc ; pEtc < etc + mEg ; pEtc++ ) {
    if ( pEtc->pVx[0] )
      mEgToColl++ ;
    else {
      /* Find the next used edge. */
      for ( pEtc2 = pEtc+1 ; pEtc2 < etc + mEg ; pEtc2++ )
        if ( pEtc2->pVx[0] ) {
          /* Copy it down. */
          pEtc[0] = pEtc2[0] ;
          /* Void the copied slot. */
          pEtc2->pVx[0] = NULL ;
          mEgToColl++ ;
          break ;
        }
      if ( pEtc2 == etc + 2*MAX_PARA_EDGES )
        /* No more used edges. */
        break ;
    }
  }
  
  if ( !mEgToColl ) {
    /* Nothing collapsible. */
    if ( failMaxColl )
      pFail->mColl = 1 ;
    else if ( failEgBnd )
      pFail->nonCollEg = 1 ;
    
    return ( 0 ) ;
  }

#ifdef DEBUG  
  if ( maxLen/minLen > 8 )
    viz_mgElem( pElem, pMgVrtx, pMgCoor ) ;
#endif







  
  /* Collapse the selected edges. */
  for ( kEg = 0 ; kEg < mEgToColl ; kEg++ ) {
    pEtc = etc + kEg ;
    
    n0 = pEtc->nVx[0] ;
    n1 = pEtc->nVx[1] ;
    mBnd0 = pVxColor[n0].mBnd ;
    mBnd1 = pVxColor[n1].mBnd ;
    pVxColor[n0].mark  = pVxColor[n1].mark  = 1 ;
    pVxColor[n0].color = pVxColor[n1].color = kEg+1 ;

    /* Initialise the stack for mg_allConvex. */
    pVxStack[2*kEg  ] = pEtc->pVx[0] ;
    pVxStack[2*kEg+1] = pEtc->pVx[1] ;

    if ( mBnd0 > mBnd1 ) {
      /* Collapse onto 0. */
      pEtc->pCoor[1][0] = TOO_MUCH ; 
      pMgVrtx[n1].Pcoor = pEtc->pCoor[0] ;
      pVxColor[n1].mVxColl = 0 ;
    }
    else if ( mBnd0 < mBnd1 ) {
      /* Collapse onto 1. */
      pEtc->pCoor[0][0] = TOO_MUCH ; 
      pMgVrtx[n0].Pcoor = pEtc->pCoor[1] ;
      pVxColor[n0].mVxColl = 0 ;
    }
    else {
      /* Average, using the coordinate slot of 0. */
      pEtc->pCoor[1][0] = TOO_MUCH ; 
      pMgVrtx[n1].Pcoor = pEtc->pCoor[0] ;
      vec_avg_dbl ( pEtc->oldCoor[0], pEtc->oldCoor[1], mDim, pEtc->pCoor[0] ) ;
      pVxColor[n1].mVxColl = 0 ;
    }
  }
  *pmVxStack = 2*mEgToColl ;



  
  /* Check the geometry of all elements that are formed with the affected
     vertices. */
  *pmElStack = 0 ;    
  if ( !umg_allConvex ( pUns, pMgVrtx, pMgCoor, lvlLrgstAngle,
                       pVxStack, pmVxStack, ( const elem_struct ** ) pElStack,
                       elemVol, pmElStack, doAllTests, pFail ) ) {
    /* The collapsed geometry is inacceptable, either due to negative volumes
       or due to too large angles. */
    failure = 1 ;
  }




  
  /* Reset the marks that flagged vertices affected by collapses. */
  for ( pEtc = etc ; pEtc < etc + mEgToColl ; pEtc++ ) {
    n0 = pEtc->nVx[0] ;
    n1 = pEtc->nVx[1] ;
    pVxColor[n0].mark =  pVxColor[n1].mark  = 0 ;
    pVxColor[n0].color = pVxColor[n1].color = 0 ;
  }




  
  
  if ( failure || doAllTests ) {
    /* Restore the original coordinates. */
    for ( side = 0 ; side < 2 ; side++ )
      for ( pEtc = etc ; pEtc < etc + mEgToColl ; pEtc++ ) {
        pMgVrtx[ pEtc->nVx[side] ].Pcoor = pEtc->pCoor[side] ;
        vec_copy_dbl ( pEtc->oldCoor[side], mDim, pEtc->pCoor[side] ) ;
        pVxColor[  pEtc->nVx[side] ].mVxColl = pEtc->mVxColl[side] ;
      }
    
    return ( 1-failure ) ;
  }

  else { /* Viable collapse. */
    (*pmEgColl) += mEgToColl ;


  
    /* Reorder the edge list. */
    for ( kEg = 0 ; kEg < mEgToColl ; kEg++ ) {
      pEtc = etc + kEg ;
      pVxStack[0] = pEtc->pVx[0] ;
      pVxStack[1] = pEtc->pVx[1] ;
      *pmVxStack = 2 ;

      for ( nVx = 0 ; nVx < 2 ; nVx++ ) {

        /* Keep note of the last edge that remained. Continue to scan the
           edges with the given vertex from that edge after deletion. */
        nNghEg = nLstKeptEg = nLstSide = 0 ;
        while ( loop_edge_vx ( pllEdge, pVxStack[nVx], &n1, &nNghEg, &side ) ) {
          nPerEg = (*ppEdgeLen)[nNghEg].perEdge ;
      
          nNewEg = move_edge ( pUns, pMgVrtx, pMgCoor, pllEdge, ppEdgeLen, nNghEg ) ;
        
          if ( nNewEg == nNghEg ) {
            /* Nothing to do, except to update the last kept edge. */
            nLstKeptEg = nNghEg ;
            nLstSide   = side ;
          }
          else {
            /* Collapsed or moved edge, nNghEg has been deleted.
               Continue to scan after the last valid edge. */
            nNghEg = nLstKeptEg ;
            side = nLstSide ;

            if ( nNewEg && nPerEg ) {
              /* Moved periodic edge. */
              if ( !( nPerEg = move_edge ( pUns, pMgVrtx, pMgCoor,
                                           pllEdge, ppEdgeLen, nPerEg ) ) ) {
                hip_err ( fatal, 0, "could not make periodic collapsed edge"
                          " in tryToCollapse_elem.\n" ) ; }
            
              (*ppEdgeLen)[nNewEg].perEdge = nPerEg ;
              (*ppEdgeLen)[nPerEg].perEdge = nNewEg ;
            }
          }
        }
      }
    }
    /* What is this doing here? should be one brace further out.    
       return ( 1 ) ;*/
  }
    return ( 1 ) ;
}


/******************************************************************************

  cmp_elCrit:
  Comarison of elements and volumes for heap.c.
  
  Last update:
  ------------ 
  4Apr13; remove static storage class.
  : conceived.
  
  Input:
  ------
  pEC0/1: the two edge structs to compare
  
  Returns:
  --------
  -1 if pEC0 < pEC1, etc.
  
*/

int cmp_elCrit ( const void *pEC0, const void *pEC1 ) {
  
  static double vol0, vol1;
  vol0 = ( (elCrit_s*) pEC0 )->vol ;
  vol1 = ( (elCrit_s*) pEC1 )->vol ;

  if ( vol0 < vol1 )
    return ( -1 ) ;
  else if  ( vol0 > vol1 )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}



/******************************************************************************

  loop_edge_coll_vx:
  Loop over all edges attached to a vertex with taking all other vertices into
  account that have collapsed onto this one.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_edge_coll_vx ( const llEdge_s *pllEdge, 
                        const vrtx_struct *pMgVrtx, const double *pMgCoor,
                        const int mDim,
                        const vrtx_struct *pVxStack[],
                        int *pmVxStack, int *pnVxStack,
                        int *pnEg )
{
  int nVxNgh[2], n1, side ;
  vrtx_struct *pVxNghEg[2] ;

  if ( !*pmVxStack )
    /* Empty stack. */
    return ( 0 ) ;

  if ( *pnVxStack < 0 ) {
    /* Initialize. */
    *pnVxStack = 0 ;
    *pnEg = 0 ;
  }
  
  /* There might be another edge with this vertex. */
  for (  ; *pnVxStack < *pmVxStack ; (*pnVxStack)++, *pnEg = 0 )
    while ( loop_edge_vx ( pllEdge, pVxStack[*pnVxStack], &n1, pnEg, &side ) ) {
      
      if ( show_edge( pllEdge, *pnEg, pVxNghEg, pVxNghEg+1 ) ) {
	nVxNgh[0] = mgVrtx ( pMgVrtx, pMgCoor, mDim, pVxNghEg[0]->number ) ;
	nVxNgh[1] = mgVrtx ( pMgVrtx, pMgCoor, mDim, pVxNghEg[1]->number ) ;
	
	if ( nVxNgh[0] == nVxNgh[1] )
	  /* This is a collapsed edge. Add the other vertex to the stack,
	     if not yet present. */
	  add_vxStack ( pVxStack, pmVxStack, STACK_SIZE, pVxNghEg[1-side] ) ;
	else
	  /* Physical edge. */
	  return ( 1 ) ;
      }
    }

  /* No edge left. */
  return ( 0 ) ;
}
/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int find_coarse_vx ( uns_s *pUns, vrtx_struct *pMgVrtx, double *pMgCoor,
                     llEdge_s *pllEdge, int nVxF ) {

  int nVxC = mgVrtx ( pMgVrtx, pMgCoor, pUns->mDim, nVxF ), nEg, n1, side, nVxNgh ;
  vrtx_struct *pVx = pUns->ppChunk[0]->Pvrtx + nVxC, *pVxEg[2] ;

  nEg = 0 ;
  while ( loop_edge_vx ( pllEdge, pVx, &n1, &nEg, &side ) )
    if ( show_edge( pllEdge, nEg, pVxEg, pVxEg+1 ) ) {
      nVxNgh = mgVrtx ( pMgVrtx, pMgCoor, pUns->mDim, pVxEg[1-side]->number ) ;

      if ( pMgVrtx[nVxNgh].mark )
        return ( pMgVrtx[nVxNgh].number ) ;
    }

  printf ( " FATAL: failed to find a connected coarse grid neighbor for %d"
           " in find_coarse_vx\n", nVxC ) ;
  return ( 0 ) ;
}


/******************************************************************************

  make_mgFace:
  Make a list of faces by vertex numbers. This requires all vertices to
  be properly numbered with the number of the vertex they collapse into.
  Exclude faces with unnumbered vertices.
  
  Last update:
  ------------
  4Apr13; make all large counters ulong_t.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_mgFace ( const uns_s *pUns, const vrtx_struct *pMgVrtx,
		  mgFace_s **ppMgFace, ulong_t *pmMgFaces, int *pmMgBnd ) {
  
  const faceOfElem_struct *pFoE ;
  const double *pCo ;
  const int *kVxFace ;
  
  int mFaces, mMgBnd, nBc, mNewFc, mFcAlloc, kVx, kkVx, nVx, mDim, mVx, discFc,
    nVxF[MAX_VX_FACE], nVxColl[MAX_VX_FACE], found ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBF, *pBndFcBeg, *pBndFcEnd ;
  mgFace_s *pMgFace, *pFc ;
  vrtx_struct **ppVx ;
  double dist, minDist ;

  mDim = pUns->mDim ;

  pMgFace = NULL ;
  mMgBnd = mFaces = 0 ;
  
  /* Loop over all boundaries. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    mNewFc = 0 ;
    pBP = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBP, &pBndFcBeg, &pBndFcEnd ) ) {

      /* Realloc for boundary faces. */
      mFcAlloc = mFaces + mNewFc + ( pBndFcEnd - pBndFcBeg + 1 ) ;
      pMgFace = arr_realloc ( "pMgFace in make_bFace", pUns->pFam, pMgFace,
                              mFcAlloc, sizeof ( *pMgFace ) ) ;
      pFc = pMgFace + mFaces + mNewFc ;
      
      for ( pBF = pBndFcBeg ; pBF <= pBndFcEnd ; pBF++ )
	if ( pBF->Pelem && !pBF->Pelem->invalid && pBF->nFace ) {
	  /* Find the vertex numbers in the collapsed mesh. */
	  pFoE = elemType[ pBF->Pelem->elType ].faceOfElem + pBF->nFace ;
	  mVx = pFoE->mVertsFace ;
	  ppVx = pBF->Pelem->PPvrtx ;
          kVxFace = pFoE->kVxFace ;

	  /* Make a list of all the non-collapsed vertices on this face. */
	  for ( discFc = pFc->mVxFc = kVx = 0 ; kVx < mVx ; kVx++ ) {
            nVxF[kVx] = ppVx[ kVxFace[kVx] ]->number ;
            /* The number of the coarse grid vertex. */
	    nVxColl[kVx] = nVx = pMgVrtx[ pUns->pnVxCollapseTo[ nVxF[kVx] ] ].number ;
            discFc = ( nVx ? 0 : 1 ) ;
            
            /* Is the node already listed? */
	    for ( kkVx = 0 ; kkVx < pFc->mVxFc ; kkVx++ )
	      if ( nVx == pFc->nVxFc[kkVx] )
		break ;
            
	    if ( kkVx == pFc->mVxFc )
	      /* nVx is not yet in the list. */
	      pFc->nVxFc[ pFc->mVxFc++ ] = nVx ;
	  }


          
          if ( discFc ) {
            /* This face has a disconnected node, must be collapsed onto another one.
               Fix the fine to coarse grid pointer. */
            minDist = TOO_MUCH ;
	    for ( kVx = 0 ; kVx < pFc->mVxFc ; kVx++ )
              if ( !nVxColl[kVx] ) {
                /* This is the disconnected one. Find the closest sibling on the face
                   that is connected. */
                pCo = ppVx[ kVxFace[kVx] ]->Pcoor ;

                for ( found = kkVx = 0 ; kkVx < pFc->mVxFc ; kkVx++ )
                  if  ( ( nVx = nVxColl[kkVx] ) ) {
                    /* Connected sibling on the same face. */
                    dist = sq_distance_dbl ( pCo, pMgVrtx[nVx].Pcoor, mDim ) ;
                    if ( dist < minDist ) {
                      found = 1 ;
                      minDist = dist ;
                      /* List not the siblings number, but its position in the
                         list of vertices. */
                      nVx = pUns->pnVxCollapseTo[ nVxF[kkVx] ] ;
                      pUns->pnVxCollapseTo[ ppVx[ kVxFace[kVx] ]->number ] = nVx ;
                    }
                  }

                if ( !found ) {
                  sprintf ( hip_msg, 
                            "could not find a parent for %"FMT_ULG" (%d) in make_mgFace.\n",
                            ppVx[ kVxFace[kVx] ]->number, nVxColl[kVx] ) ;
                  hip_err ( fatal, 0, hip_msg ) ;
                }
              }
          }
          
	  else if ( pFc->mVxFc >= mDim ) {
	    /* Enough of a face left. List it to find it in some element. */
	    mNewFc++ ;
	    pFc->pBc = pBF->Pbc ;
	    pFc++ ;
	  }
	}
    }
    
    if ( mNewFc ) {
      /* There were faces left with this boundary condition. */
      mMgBnd++ ;
      mFaces += mNewFc ;
    }
  }

  *ppMgFace = pMgFace ;
  *pmMgFaces = mFaces ;
  *pmMgBnd = mMgBnd ;

  return ( 1 ) ;
}
      
/******************************************************************************

  find_face:
  Find a face in the coarser grid, given a list of vertex numbers.
  
  Last update:
  ------------
  13Feb19; promote possibly large int to ulong_t.
  : conceived.
  
  Input:
  ------
  nVxFace:        The list of vertex numbers to match
  mVxFace:        and the number of vertices in the list.
  pMgVrtx:        The base of the list of vertices.
  pllMgVxToElem:  The vertex to element list.

  Changes To:
  -----------
  ppNewEl:        The matching element.
  ppNewFc:        And the matching face in that element.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int find_face ( const ulong_t nVxFace[MAX_VX_FACE], const int mVxFace,
                const vrtx_struct *pMgVrtx, llToElem_s *pllMgVxToElem,
                elem_struct **ppNewEl, int *pnNewFc ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;

  ulong_t nItem, nVx, iVx ;
  int kFace, kVx, matched[MAX_VX_FACE] ;
  elem_struct *pMgElem ;
  vrtx_struct **ppVx ;
  
  /* Loop over all elements connected to the first vertex. */
  nItem = 0 ;
  while ( loop_toElem ( pllMgVxToElem, nVxFace[0], &nItem, &pMgElem ) ) {

    pElT = elemType + pMgElem->elType ;
    /* Loop over all faces/edges of this element. */
    for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
      pFoE = pElT->faceOfElem + kFace ;
      kVxFace = pFoE->kVxFace ;
      ppVx = pMgElem->PPvrtx ;

      /* Reset the list of flags whether each of nVxFace has been matched. */
      for ( kVx = 0 ; kVx < MAX_VX_FACE ; kVx++ )
        matched[kVx] = 0 ;

      /* Try to find each of the vertices of this face in nVxFace. */
      for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
	nVx = ppVx[ kVxFace[kVx] ]->number ;
	for ( iVx = 0 ; iVx < mVxFace ; iVx++ )
	  if ( nVx == nVxFace[iVx] ) {
	    matched[iVx] = 1 ;
	    break ;
	  }
	if ( iVx == mVxFace )
	  /* Failure, nVx is not in nVxFace. */
	  break ;
      }
      
      if ( kVx == pFoE->mVertsFace ) {
	/* All of the vertices of pFoE are in nVxFace. Now check whether all of
	   nVxFace have been matched in pFoE. */
	
	for ( iVx = 0 ; iVx < mVxFace ; iVx++ )
	  if ( !matched[iVx] )
	    break ;
	if ( iVx == mVxFace ) {
	  /* Match. */
	  *ppNewEl = pMgElem ;
	  *pnNewFc = kFace ;
	  return ( 1 ) ;
	}
      }
    }
  }

  /* No match. Not necessarily a failure, a corner element might have disappeared,
     leaving two boundary faces collapsed onto each other.
     printf ( " FATAL: could not match boundary face in find_face.\n" ) ; */
  *ppNewEl = NULL ;
  *pnNewFc = 0 ;
  
  return ( 1 ) ;
}


/******************************************************************************

  make_coarser_level:
  .
  
  Last update:
  ------------
  2Mar20; new name for  find_bndFc_pVxList. 
  6Sep18; rename reset_vx_mark.
  3Oct17; set pUns->pCoarse here, rather than in umg_coarsen_one_level_el.
  15jul17; store coarse grid containing element and its interpolation coeffs.
  13Jul17; rename pElCollapseTo to pElContain.
  18Dec16; number by mem order, use number_uns_grid_leafs.
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  27Jul11; fix bug with writing mConnEntries to mElem2ChildP.
  9Jul11; use init_elem.
  6May11; fix bug in debug-enabled code when fine grid node is outside the 
    coarse grid domain.
  12Feb04; find containing coarse grid cell in pUns->pElCollapseTo.
  25Oct99; intro simplify_one_elem.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_coarser_level ( uns_s *pUns, uns_s *pMgUns,
                         vrtx_struct *pMgVrtx, double *pMgCoor,
                         llEdge_s *pllEdge, const int nLevel ) {
  
  const elemType_struct *pElT ;
  const int mDim = pUns->mDim ;
  vrtx_struct * const pVxBase = NULL ;

  chunk_struct *pChunk, *pMgChunk ;
  int nVx, nDim ;
  ulong_t mMgVerts, nVxEg[2], mMgElems, mConnEntries, mVx ;
  int  kVx ;
  ulong_t mMgVx, mElemsFound, nrVx ;
  int mMgBnd ;
  ulong_t mMgFaces, mNewFaces ;
  int nBndPatch ;
  ulong_t  *pnVxCollapseTo ;
  int mVxElem ;
  ulong_t nVxC, mElemsSimplified, nVxF, nVxEl[MAX_VX_ELEM] ;
  int  kFc ;
  double *pCo, *pCoDown ;
  elem_struct *pEl, *pElBeg, *pElEnd, *pMgElem, *pMgEl ;
  vrtx_struct **ppMgVrtx, **ppMgVx, *pVxBeg, *pVxEnd, *pVx, *pVxC ;
  bndPatch_struct *pMgBndPatch ;
  bndFc_struct *pMgBF, *pMgBndFc ;
  llToElem_s *pllMgVxToElem ;
  mgFace_s *pMgFace, *pFc ;
  bc_struct *pBc ;

  pUns->pUnsCoarse     = pMgUns ;

  pMgChunk = make_chunk ( pMgUns ) ;
  pMgUns->pRootChunk   = pMgChunk ;


  
#ifdef DEBUG  
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) 
      if ( pEl->invalid )
	pEl->mark = 0 ;
      else if ( !pEl->invalid &&
                mgelem_isnt_collapsed ( pEl, pMgVrtx, pMgCoor, &mVxElem, nVxEl ) ) {
        int elemIsNotCollapsed ;
        double elemVol, hMin, dihAngle, fcAngle ;
        double angle = maxAngle ( make_mgElem( pEl, pMgVrtx, pMgCoor ),
                                  &elemVol, &elemIsNotCollapsed, &hMin, &dihAngle, &fcAngle ) ;
        if ( elemVol < 0. ) {
          printf ( " Negative volume: %"FMT_ULG", %g\n", pEl->number, elemVol ) ;
        }
        if ( angle < -1. ) {
          printf ( " Excessive angle: %"FMT_ULG", %g\n", pEl->number, angle ) ;
        }
      }
#endif
        


  
  /* Reset vertex usage marks. */
  for ( nVx = 1 ; nVx <= pUns->mVertsNumbered ; nVx++ ) {
    pMgVrtx[nVx].mark = 0 ;
    
    /* Number the vertices according to coordinate use. This will give each vertex
       the number of the root vertex. The root vertex uses its own coordinate slot,
       vertices collapsed onto the root point to the root's coordinate slot. */
    if ( pMgVrtx[nVx].number )
      pMgVrtx[nVx].number = mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
  }
  
  /* Count the new elements and the number of connectivity entries. */
  mMgElems = mConnEntries = 0 ;
  pChunk = NULL ;
  /* In order to recover boundary faces, we need to change the elements' number. */
  pUns->numberedType = noNum ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {

      if ( pEl->invalid )
	pEl->mark = 0 ;
      else if ( mgelem_isnt_collapsed ( pEl, pMgVrtx, pMgCoor, &mVxElem, nVxEl ) ) {
        /* At least a simplex left. */
        pEl->number = ++mMgElems ;
        mConnEntries += mVxElem ;

        /* Mark all vertices referenced by this element. */
        for ( kVx = 0 ; kVx < elemType[ pEl->elType ].mVerts ; kVx++ )
          pMgVrtx[ pMgVrtx[ pEl->PPvrtx[kVx]->number].number ].mark = 1 ;
      }
      else
        pEl->number = 0 ;
    }




  /* Renumber all used root vertices. */
  for ( mMgVerts = 0, nVx = 1 ; nVx <= pUns->mVertsNumbered ; nVx++ )
    if ( pMgVrtx[nVx].number && ( pMgVrtx[nVx].Pcoor - pMgCoor )/mDim == nVx ) {
      if ( pMgVrtx[nVx].mark )
        pMgVrtx[nVx].number = ++mMgVerts ;
      else
        /* Disconnected vertex. */
        pMgVrtx[nVx].number = 0 ;
    }


  /* Renumber all vertices pointing to their root. */
  for ( nVx = 1 ; nVx <= pUns->mVertsNumbered ; nVx++ )
    if ( pMgVrtx[nVx].number && ( pMgVrtx[nVx].Pcoor - pMgCoor )/mDim != nVx )
      pMgVrtx[nVx].number = pMgVrtx[ pMgVrtx[nVx].number ].number ;




  
  /* Make a list of coarse grid vertices that each fine grid vertex collapses into. */
  mVx = pUns->mVxCollapseTo = pUns->mVertsNumbered ;
  pnVxCollapseTo = arr_malloc ( "pnVxCollapseTo in make_coarser_level", pUns->pFam,
                                mVx+1, sizeof( *pnVxCollapseTo ) ) ;
  pUns->pnVxCollapseTo = pnVxCollapseTo ;

  for ( nVx = 1 ; nVx <= mVx ; nVx++ ) {
    nVxC =  mgVrtx( pMgVrtx, pMgCoor, mDim, nVx ) ;
    if ( !pMgVrtx[nVxC].number )
      /* Coarse grid vertex is pinched off. Find a used one in the vicinity. */
      nVxC = find_coarse_vx ( pUns, pMgVrtx, pMgCoor, pllEdge, nVx ) ;

    pnVxCollapseTo[nVx] = pMgVrtx[nVxC].number ;
  }
  



    
  /* Alloc for the elements. */
  pMgElem = arr_malloc ( "pMgElem in make_coarser_level", pMgUns->pFam,
                         mMgElems+1, sizeof( *pMgElem ) ) ;
  ppMgVrtx = arr_malloc ( "ppMgVrtx in make_coarser_level", pMgUns->pFam,
                          mConnEntries, sizeof( *ppMgVrtx ) ) ;
  pMgChunk->Pelem	   = pMgElem ;
  pMgChunk->mElems = pMgUns->mElemsAlloc = mMgElems ;
  pMgChunk->PPvrtx	   = ppMgVrtx ;
  pMgChunk->mElem2VertP = mConnEntries ;



  /* Make the elements. Note that pMgElem[1] is preincremented, ppMgVrtx[0] is
     post-incremented. */
  pMgEl = pMgElem ; ppMgVx = ppMgVrtx ;
  mElemsFound = mElemsSimplified = 0 ;
  pChunk = NULL ; // JDM 4 Jul 17: oops, that shouldn't have been missing.
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number ) {
        pEl->number = ++mElemsFound ;
	pElT = elemType + pEl->elType ;
	mVx = pElT->mVerts ;
	++pMgEl ;
        init_elem ( pMgEl, pEl->elType, mElemsFound, ppMgVx ) ; 

        if ( mDim == 2 ) {
          /* Either a quad or a triangle. */
          /* Loop over all edges, but in counterclockwise traversal. */
          for ( mMgVx = kVx = 0 ; kVx < mVx ; kVx++ ) {
            nVxEg[0] = pEl->PPvrtx[   kVx         ]->number ;
            nVxEg[1] = pEl->PPvrtx[ ( kVx+1 )%mVx ]->number ;
            if ( pMgVrtx[ nVxEg[0] ].number != pMgVrtx[ nVxEg[1] ].number )
              /* Non-collapsed Edge. Since we need to realloc pMgVrtx, use some base. */
              pMgEl->PPvrtx[mMgVx++] = pVxBase + pMgVrtx[ nVxEg[0] ].number ;
          }
          pMgEl->elType = ( mMgVx == 3 ? tri : qua ) ;
          ppMgVx += mMgVx ;
        }
        else {
          for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
            nVxEg[0] = pEl->PPvrtx[kVx]->number ;
            pMgEl->PPvrtx[kVx] = pVxBase + pMgVrtx[ nVxEg[0] ].number ;
          }
          /* Can this element be simplified to another primitive? */
          mElemsSimplified += simplify_one_elem ( pMgEl, pMgEl ) ;
          ppMgVx += elemType[ pMgEl->elType ].mVerts ;
        }
      }
  }
  if ( mElemsFound - mMgElems ) {
    sprintf ( hip_msg,
              "expected %"FMT_ULG" coarsened elements, found %"FMT_ULG" in make_coarser_level.",
	      mMgElems, mElemsFound ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  ulong_t ptrDiffMgVx = (ulong_t) (ppMgVx-ppMgVrtx+1) ;
  if ( ppMgVx > ppMgVrtx + mConnEntries ) {
    sprintf ( hip_msg, "expected %"FMT_ULG" coarsened conn., found %"FMT_ULG" in make_coarser_level.",
	     mConnEntries, ptrDiffMgVx ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "simplified %"FMT_ULG" elements to lower primitive.",
             mElemsSimplified ) ;
    hip_err ( info, 2, hip_msg ) ; }


      
  /* Pack the vertices. They already carry a preliminary number. There still can be
     disconnected vertices in the coarse mesh that have been pinched off in the
     collapse. */
  for ( nVx = 1 ; nVx <= pUns->mVertsNumbered ; nVx++ )
    if ( ( pMgVrtx[nVx].Pcoor - pMgCoor )/mDim == nVx && pMgVrtx[nVx].number ) {
      /* This is the main entry since vertex number and coordinate position
	 correspond. Copy nVx into nrVx. */
      nrVx = pMgVrtx[nVx].number ;
      pMgVrtx[nrVx].number = nrVx ;
      pMgVrtx[nrVx].Pcoor = pCoDown = pMgCoor + mDim*nrVx ;
      pCo = pMgCoor + mDim*nVx ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        pCoDown[nDim] = pCo[nDim] ;
    }
  
  /* Final realloc of the vertices. */
  pMgVrtx = arr_realloc ( "pMgVrtx in make_coarser_level", pMgUns->pFam, pMgVrtx,
                          mMgVerts+1, sizeof( *pMgVrtx ) );
  pMgCoor = arr_realloc ( "pMgCoor in make_coarser_level", pMgUns->pFam, pMgCoor,
                          ( mMgVerts+1 )*mDim, sizeof( *pMgCoor ) ) ;
  pMgChunk->Pvrtx  = pMgVrtx ;
  pMgChunk->Pcoor  = pMgCoor ;
  pMgChunk->mVerts = pMgUns->mVertsAlloc = mMgVerts ;


  /* Set the element to vertex pointers properly. */
  for ( pMgEl = pMgElem+1 ; pMgEl <= pMgElem+mMgElems ; pMgEl++ ) {
    pElT = elemType + pMgEl->elType ;
    mVx = pElT->mVerts ;
    ppMgVx = pMgEl->PPvrtx ;

    /* Loop over all vertices. */
    for ( kVx = 0 ; kVx < mVx ; kVx++ )
      ppMgVx[kVx] = pMgVrtx + ( ppMgVx[kVx] - pVxBase ) ;
  }
  

  /* Final renumber of the vertices. Disconnected vertices will remain unnumberd. */
  pUns->numberedType = invNum ;
  number_uns_grid_leafs ( pMgUns ) ;



  /* Nasty fix. Number disconnected coarse grid vertices back in. */
  for ( nVx = 1 ; nVx <= mMgVerts ; nVx++ )
    pMgVrtx[nVx].number = nVx ;



  /* Make a list of boundary faces with the renumbered vertices. This list only stores
     valid faces with all numbered vertices. */
  if ( !make_mgFace ( pUns, pMgVrtx, &pMgFace, &mMgFaces, &mMgBnd ) ) {
    hip_err ( fatal, 0, "Could not make a list of faces in make_coarser_level.\n" ) ;
    return ( 0 ) ; }
  
  

  
  
  /* Make a list of vertex to element pointers. Note that we only need elements
     with numbered vertices, not coordinates. */
  if ( !( pMgUns->pllVxToElem = pllMgVxToElem = make_vxToElem ( pMgUns ) ) ) {
    hip_err ( fatal, 0, "Could not create vertex to elem list in uns_coarsen.\n" ) ;
    return ( 0 ) ; }



  
  /* Make the boundaries.*/
  pMgBndPatch = arr_malloc ( "pMgBndPatch in make_coarser_level", pMgUns->pFam, 
                             mMgBnd+1, sizeof ( *pMgBndPatch ) ) ;
  pMgBndFc = arr_malloc ( "pMgBndFc in make_coarser_level", pMgUns->pFam, 
                          mMgFaces+1, sizeof ( *pMgBndFc ) ) ;
  pMgChunk->mBndPatches   = mMgBnd ;
  pMgChunk->PbndPatch	  = pMgBndPatch ;
  pMgChunk->mBndFaces	  = mMgFaces ;
  pMgChunk->PbndFc	  = pMgBndFc ;


  /* Reset mark2, use it to flag boundary vertices. */
  for ( nVx = 1 ; nVx <= mMgVerts ; nVx++ )
    pMgVrtx[nVx].mark2 = 0 ;

  
  /* Loop over all faces and find an element and side that match it. */
  for ( pBc = NULL, nBndPatch = mNewFaces = 0, pFc = pMgFace, pMgBF = pMgBndFc ;
	pFc < pMgFace + mMgFaces ; pFc++ ) {

    if ( pFc->pBc != pBc ) {
      /* New boundary patch. Count the faces with the previous one. */
      pMgBndPatch[nBndPatch].mBndFc = mNewFaces ;
      mNewFaces = 0 ;
      pMgBndPatch[++nBndPatch].PbndFc = pMgBF+1 ;
      pMgBndPatch[nBndPatch].Pbc = pBc = pFc->pBc ;
    }
    
    /* Find the face in a collapsed element. */
    mNewFaces++ ;
    pMgBF++ ;
    if ( !find_face ( pFc->nVxFc, pFc->mVxFc, pMgVrtx, pllMgVxToElem, 
		      &(pMgBF->Pelem), &(pMgBF->nFace) ) ) {
      hip_err ( fatal, 0, "no matching boundary face in make_coarser_level.\n" ) ;
    }

    for ( kVx = 0 ; kVx < pFc->mVxFc ; kVx++ ) 
      pMgVrtx[ pFc->nVxFc[ kVx ] ].mark2 = 1 ;

    pMgBF->Pbc = pFc->pBc ;
  }
  pMgBndPatch[mMgBnd].mBndFc = mNewFaces ;


  arr_free ( pMgFace ) ;

  /* Inherit fine grid periodicity. */
  pMgUns->mPerBcPairs = pUns->mPerBcPairs ;
  pMgUns->pPerBc = pUns->pPerBc ;
  
     


  /* Mark all fine-grid bondary vertices. */
  mark2_bndVx ( pUns ) ;

  /* First find containing coarse grid cells using a walk on the 
     wild side, aeh, coarse grid. */
  int nB, nE, mCVxOutside = 0, kSide ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxFc[MAX_VX_FACE] ;
  const elem_struct *pElem ;
  const bndFc_struct *pBndFc ;
  double faceGC[MAX_DIM], vx2face[MAX_DIM], fcNorm[MAX_DIM], sc ;
  int mVxFc, mTimesNormal ;
  vrtx_struct *ppVxC[MAX_VX_ELEM] ;
  double tol = -1.e-12 ; // Need to make set-able param.
  int fixNeg = 0 ;

  /* Unmark all coarse grid vx. */
  reset_vx_mark ( pMgUns ) ;

  /* Reset the virtual element used in face projection. make_mg_elem
     will have changed coordinate pointers to point to an actual element. */
  init_uns_vrtElem () ;

  /* Store the element interpolation coeffs. */
  elem_struct **ppElC ;
  ppElC = pUns->ppElContain =
    arr_malloc ( "pnElContain in make_coarser_level" ,pUns->pFam,
                 pUns->mVertsNumbered+1, sizeof ( *pUns->ppElContain ) ) ;
  double *pWt ;
  pWt = pUns->pElContainVxWt =
    arr_malloc ( "pnElContainVxWt in make_coarser_level" ,pUns->pFam,
                 MAX_VX_ELEM*pUns->mVertsNumbered,
                 sizeof ( *pUns->pElContainVxWt ) ) ;
  

  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nB, &pVxEnd, &nE ) ) 
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ ) 
      if ( pVx->number ) {
        nVxF = pVx->number ;
        nVxC = pUns->pnVxCollapseTo[nVxF] ;
        pVxC = pMgVrtx + nVxC ;

        pEl = ppElC[nVxF] = find_el_walk ( pMgUns, pVx->Pcoor, pVxC, &kFc ) ;

        /* Find interpolation coeffs on the coarse grid. */
        mVx = elemType[ pEl->elType ].mVerts ;
        minNormEl( pEl, mDim, mVx, pVx->Pcoor, minnorm_tol, fixNeg, pWt ) ;

        /* Tag all used coarse grid vertices. */
        for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
          if ( ABS( pWt[kVx] ) > tol )
            ppElC[nVxF]->PPvrtx[kVx]->mark = 1 ;
        }
        for ( ; kVx < MAX_VX_ELEM ; kVx++ )
          pWt[kVx] = 0. ;
        pWt += MAX_VX_ELEM ;

        
        kSide = elem_contains_co ( pEl, pVx->Pcoor, 0 ) ;
        if ( kSide ) {
          /* This one's outside its element. Find a matching boundary face. */
          pElem = ppElC[nVxF] ;
          pElT = elemType + pElem->elType ;

          ppElC[nVxF] = find_el_walk ( pMgUns, pVx->Pcoor, pVxC, &kFc ) ;

          face_grav_ctr ( pElem, kSide, faceGC, &pFoE, &mVxFc, pVxFc ) ;
          // JDM, March 2020: are we really using this inefficient search?
          pBndFc = find_bndFc_pVxList ( pMgUns, pVxFc, mVxFc ) ;

          if ( !pBndFc || ( pBndFc->Pelem != pElem && pBndFc->nFace != kSide ) ) {
            vec_diff_dbl ( faceGC, pVx->Pcoor, pElT->mDim, vx2face ) ;
            /* Outward face normal. */
            uns_face_normal ( pElem, kSide, fcNorm, &mTimesNormal ) ;
            sc = scal_prod_dbl ( vx2face, fcNorm, pElT->mDim );

            /* Consider the distance of the fine grid point from the face using
               some average h along the face: sc*h, relative to that h, hence sc. */
            if ( sc < -1.e-4 ) {
              sprintf ( hip_msg, "vx %"FMT_ULG" outside elem %"FMT_ULG", side %d by %g w/o bndFc.\n", 
                        pVx->number, pElem->number, kSide, sc ) ;
              hip_err ( warning, 5, hip_msg ) ;
              mCVxOutside++ ;
            }
          }
        }
      }

  /* Check that all coarse grid vx have a contributing fine grid vx. */
  pChunk = NULL ;
  while ( loop_verts ( pMgUns, &pChunk, &pVxBeg, &nB, &pVxEnd, &nE ) ) 
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ ) 
      if ( pVx->number && !pVx->mark ) {

        double minDist = TOO_MUCH ;
        const elem_struct *pElMin = NULL ;
        int kFcMin ;
        min_dist_face_elems ( pUns, pVx->Pcoor, &minDist, &pElMin, &kFcMin ) ;

        sprintf ( hip_msg, "coarse grid vx %"FMT_ULG" not connected to the fine grid.", 
                  pVx->number );
        hip_err ( warning, 1, hip_msg ) ;
      }
  

  sprintf ( hip_msg, "found %d vertices located outside the donor coarse grid cell.", 
            mCVxOutside ) ;
  hip_err ( info, 1, hip_msg ) ;


  free_toElem ( &pMgUns->pllVxToElem ) ;
  number_uns_grid ( pUns ) ;

  return ( 1 ) ;
}

/******************************************************************************

  coarsen_one_level_el:
  Coarsen one level using a heap of elements. Collapse one element at a time.
  
  Last update:
  ------------ 
  3Feb18; remove test for zero fail indicators, this could happen if the 
    element is already collapsed, but hasn't been tagged as such.
  31Jan18; fix logic bug with bad braces from past beauty edit.
  19Dec17; new interface to make_uns.
  4oct17; don't reduce maxColl on edges or faces.
  3oct17; set pUns->pUnsCoarse in make_coarser_level rather than here.
  1oct17; track non-convexity as reason for not collapsing an element.
  1Jul16; new interface to check_uns.
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  14Jul99; mark collapsed cells to avoid testing.
  10May99; do an extra pass of tryToCollapse to get statistics on failures.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

uns_s *umg_coarsen_one_level_el ( uns_s *pUns, const int nLevel,
                              const double lvlMgLen,
                              const double lvlArCutOff,
                              const double lvlLrgstAngle ) {

  const vrtx_struct *pVxStack[STACK_SIZE] ;
  const int mDim = pUns->mDim, mVerts = pUns->mVertsNumbered ;
  
  llEdge_s *pllEdge ;
  edgeLen_s *pEgLen ;
  double *pMgCoor, *pMgCo, oldVol, elemVol[STACK_SIZE] ;
  int nVxBeg, nVxEnd, nDim ;
  ulong_t mEdges ;
  int mVxStack, mElStack, nEl, mVxElem ;
  ulong_t mEgCollTried = 0, mEgColl = 0, mElCollTried = 0, mElColl = 0, 
    mEl, mFailColl = 0, mCollEl, 
    sFnonCollEg, sFintBndEg, sFegLen, sFmColl, sFangle, sFtwist,sFvolume, sFnonConvex ;
  int maxColl[MAX_DIM+1] ;
  ulong_t nVxElem[MAX_VX_ELEM] ;
  vrtx_struct *pMgVrtx, *pMgVx, *pVx, *pVxBeg, *pVxEnd ;
  elem_struct *pElem, *pElBeg, *pElEnd, *pElStack[STACK_SIZE] ;

  chunk_struct *pChunk ;
  heap_s *pHeap ;
  elCrit_s elCrit ;
  uns_s *pMgUns ;
  elCollFail_s fail ;

  /* Limits for 1d (edge), 2d (face), and 3d (volume) collapse. */
  maxColl[1] = lvlMgLen ;
  maxColl[2] = lvlMgLen*lvlMgLen ;
  maxColl[3] = lvlMgLen*lvlMgLen*lvlMgLen ;

  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "Collapsing for level %d,\n"
             "        maxColl: %d/%d/%d, mgAr: %g, mgAngle %g.",
             nLevel, maxColl[1], maxColl[2], maxColl[3],
             lvlArCutOff, acos(lvlLrgstAngle)/PI*180. ) ;
    hip_err ( info, 4, hip_msg ) ;
  }
  else if ( verbosity > 2 ) {
    sprintf ( hip_msg, "Collapsing for level %d.", nLevel ) ;
    hip_err ( info, 3, hip_msg ) ;
  }
  
  /* Make an edge list. Remake it, since we're not sure that counters
     have remained after renumbering. */
  pllEdge = make_llEdge_bnd ( pUns, &mEdges, sizeof( edgeLen_s ),
                              (void**) &(pUns->pEgLen) ) ;
  pEgLen = pUns->pEgLen ;

  if ( !pllEdge )
    hip_err ( fatal, 0, "could not create edge list in uns_coarsen." ) ;
  else 
    pUns->pllEdge = pllEdge ;

  if ( !( pUns->pllVxToElem = make_vxToElem ( pUns ) ) )
    hip_err ( fatal, 0, "could not create vertex to elem list in uns_coarsen.\n" ) ;


  
  /* Make a new grid. Make it here rather than in make_coarser_level so we
     can associate the vertex and coor storage with it. */
  pMgUns = make_uns ( NULL ) ;
  pMgUns->pUnsFine     = pUns ;
  pMgUns->pUnsFinest   = pUns->pUnsFinest ;
  pMgUns->mDim         = mDim ;

  pMgVrtx   = arr_malloc ( "pMgVrtx in uns_coarsen",  pMgUns->pFam,
                           mVerts+1, sizeof( *pMgVrtx ) ) ;
  pMgCoor   = arr_malloc ( "pMgCoor in uns_coarsen",  pMgUns->pFam,
                           mDim*( mVerts+1 ), sizeof( *pMgCoor ) );

  /* Match periodic edges. Mark boundary edges. */
  if ( pUns->mPerBcPairs )
    match_per_in_all_edges ( pUns, pllEdge, pEgLen ) ;
    
  /* Count the number of boundaries of a node for collapsing priorities. */
  vx_properties ( pUns, mEdges, pllEdge, pEgLen, lvlArCutOff ) ;


  
  /* Copy vertices and their coordinates. */
  pChunk = NULL ;
  pMgVx = pMgVrtx ; pMgCo = pMgCoor ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
    for ( pVx  = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
	pMgVx++ ;
	pMgCo += mDim ;
	if ( pMgVx - pMgVrtx > pUns->mVertsNumbered )
	  hip_err ( fatal, 0, "exceeded vertex space in uns_coarsen.\n" ) ;

	pMgVx->number = pMgVx - pMgVrtx ;
	pMgVx->Pcoor  = pMgCo ;
	for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  pMgCo[nDim] = pVx->Pcoor[nDim] ;
      }


  /* Make a heap list to sort elements for smallest volume. */
  if ( !( pHeap = make_heap ( pUns->mElemsNumbered, 1, 
                              sizeof( elCrit_s ), pUns->pFam, cmp_elCrit ) ) )
    hip_err ( fatal, 0, "failed to alloc a heap list in uns_coarsen.\n" ) ;


  /* Add all elements to the heap. Count them while we are at it for the
     diagnostic loop at the end. */
  pChunk = NULL ;
  mEl = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        mEl++ ;
        elCrit.pElem = pElem ;
        elCrit.vol = get_elem_vol ( pElem ) ;
        add_heap ( pHeap, &elCrit ) ;
        /* Set a mark for non-collapsed elements. */
        pElem->mark = 0 ;
      }


  if ( verbosity > 3 ) {
    /* Initialise the output string. */
    printf ( "          tried/succeeded to collapse %8d/%8d elements", 0, 0 ) ;
    fflush ( stdout ) ;
  }

  
  /* Loop over all edges while there are collapsible ones. */
#ifdef DEBUG
  int doShowMesh = 0 ;
#endif
  pElem = NULL ;
  while ( get_heap ( pHeap, &elCrit, 1 ) ) {

    /* An element will be heaped a lot of times. Try it only once inbetween
       collapses. */
    if ( elCrit.pElem != pElem ) {
      /* Compare the actual volume of the element with the heaped volume. */
      pElem = ( elem_struct * ) elCrit.pElem ;
      oldVol = elCrit.vol ;
      elCrit.vol = get_elem_vol ( make_mgElem( pElem, pMgVrtx, pMgCoor ) ) ;
      
      if ( oldVol != 0. && ABS( oldVol - elCrit.vol ) > 1.e-10 && !pElem->mark )
        /* The vol of the elem has changed since it was introduced. Reheap.
           Zero volume indicates that this is a neighboring element with priority. */
        add_heap ( pHeap, &elCrit ) ;
      
      else if ( !pElem->mark )
        /* Properly heaped elem, try to collapse it. Note that the collapsed grid's
           coordinates are in pMgVrtx. */
        if ( mgelem_isnt_collapsed ( pElem, pMgVrtx, pMgCoor, &mVxElem, nVxElem ) ) {
          mElCollTried++ ;


          /*{
            elem_struct *pelx = make_mgElem2(pElem,pMgVrtx,pMgCoor) ;
            int mVxx = elemType[pelx->elType].mVerts ;
            int k3 ;
            vrtx_struct **ppVx = pelx->PPvrtx ;
            for ( k3 = 0 ; k3 < mVxx ; k3++ ) {
            if ( ppVx[k3] == pMgVrtx+1 )
            printf ( "blah\n" ) ;
            }*/
        

          
          if ( tryToCollapse_elem ( pElem, pUns, pllEdge, pMgVrtx, pMgCoor, &pEgLen,
                                    pVxStack, &mVxStack, pElStack, elemVol, &mElStack,
                                    &mEgCollTried, &mEgColl,
                                    maxColl, lvlArCutOff, lvlLrgstAngle, 0, &fail ) ) {
            /* Successful collapse. */
            mElColl++ ;
#ifdef DEBUG_MG
            FILE *Fhist = fopen ( "elCollHist.dat", "a" ) ;
            int nEl = pElem->number, mEl = mElCollTried ;
            fprintf ( Fhist, "%d: %d +\n", mEl, nEl ) ;
            fclose ( Fhist ) ;

            if ( doShowMesh ) {
              viz_mgElem ( pElem, pMgVrtx, pMgCoor ) ;
              mg_write_hdf ( pUns, pMgVrtx, pMgCoor, "intermediate_mg_mesh" ) ;
            }
#endif

            
            /* Reheap all elements with the changed geometry. */
            for ( nEl = 0 ; nEl < mElStack ; nEl++ )
              /* If the volume is < 0., flag it for being collapsed. 
                 Note: vol = 0 is used to flag partially collapsed elements,
                 Fully collapsed vol is -99.
              */
              if ( elemVol[nEl] < 0. ) {
                /* Collapsed neighbor. */
                if ( !pElStack[nEl]->mark ) {
                  /* Newly collapsed. Mark and count it. */
                  pElStack[nEl]->mark = 1 ;
                }
              }
              else {
                /* Neighboring element affected but not collapsed. Reheap. */
                elCrit.pElem = pElStack[nEl] ;
                elCrit.vol = elemVol[nEl] ;
                add_heap ( pHeap, &elCrit ) ;
              }
          }
          else { //if ( !fail.nonCollEg ) { // Not yet collapsed, but attempt failed.
            mFailColl++ ;
#ifdef DEBUG_MG
            FILE *Fhist = fopen ( "elCollHist.dat", "a" ) ;
            int nEl = pElem->number, mEl = mElCollTried ;
            fprintf ( Fhist, "%d: %d -\n", mEl, nEl ) ;
            fclose ( Fhist ) ;
            if ( mgelem_isnt_collapsed ( pElem, pMgVrtx, pMgCoor, &mVxElem, nVxElem ) )
              if ( doShowMesh ) {
                viz_mgElem ( pElem, pMgVrtx, pMgCoor ) ;
              }
#endif
          }

          if ( verbosity > 3 && mElCollTried%5000 == 0 ) {
            printf ( "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
                     "%8"FMT_ULG"/%8"FMT_ULG" elements", mElCollTried, mElColl ) ;
            fflush ( stdout ) ;
          }
        }
    }
  }

  if ( verbosity > 3 ) {
    printf ( "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
             "%8"FMT_ULG"/%8"FMT_ULG" elements\n", mElCollTried, mElColl ) ;
    fflush ( stdout ) ;
  }



  double hMin, lrgstAngle, elVol, hMinSq, fcAngle, dihAngle ;
  int isNotColl ;
  ulong_t mElColl2 ;
  if ( verbosity < 5 && verbosity > 0 ) {
    /* Look for max angles. */
    const int doPrint = 1 ;
    check_angles ( pUns, doPrint ) ;
  } 
  else {
    /* Find out why the remaining edges could not be collapsed. */
    sFnonCollEg =  sFintBndEg = 0 ;
    sFegLen = sFmColl = sFangle = sFtwist = sFvolume = sFnonConvex = 0 ;
    mCollEl = mElColl2 = 0 ;
    
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && !pElem->mark ) {

          /* Check if element is collapsed, but not marked. 
             JDM: if there are, review tracking of collapsed elements. */
          lrgstAngle = maxAngle( pElem, &elVol, &isNotColl, &hMinSq, &dihAngle, &fcAngle );
          if ( !isNotColl )
            pElem->mark = 1 ;
          else if ( tryToCollapse_elem ( pElem, pUns, pllEdge, pMgVrtx, pMgCoor, &pEgLen,
                                    pVxStack, &mVxStack, pElStack, elemVol, &mElStack,
                                    &mEgCollTried, &mEgColl,
                                    maxColl, lvlArCutOff, lvlLrgstAngle, 1, &fail ) )
            mElColl2++ ;
          else {
            mCollEl++ ;
            sFnonCollEg += fail.nonCollEg ;
            sFintBndEg  += fail.intBndEg ; 
            sFegLen     += fail.egLen ;    
            sFmColl     += fail.mColl ;    
            sFangle     += fail.angle ;    
            sFtwist     += fail.twist ;    
            sFvolume    += fail.volume ;
            sFnonConvex += fail.nonConvex ;
          }
        }
  


    if ( verbosity > 1 ) {
      sprintf ( hip_msg, "Of %8"FMT_ULG" total, %8"FMT_ULG" non-collapsed elements,\n"
                "             %8"FMT_ULG" were collapsible,\n"
                "             %8"FMT_ULG" failed due to non-collapsible edges,\n"
                "             %8"FMT_ULG" failed due to internal edges between boundaries,\n"
                "             %8"FMT_ULG" failed due to excessively lengthened edges,\n"
                "             %8"FMT_ULG" failed due to too many fine grid verts in a coarse one,\n"
                "             %8"FMT_ULG" failed due to excessive dihedral angles,\n"
                "             %8"FMT_ULG" failed due to excessive twist,\n"
                "             %8"FMT_ULG" failed due to negative volume,\n"
                "             %8"FMT_ULG" failed due to nonconvex shape,\n",
                mEl, mElColl2+mCollEl, mElColl2, sFnonCollEg, sFintBndEg, sFegLen, sFmColl,
                sFangle, sFtwist, sFvolume, sFnonConvex ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
  }


  /* Redo the vertex to element list for the coarser level in make_coarser_level. */
  free_toElem ( &pUns->pllVxToElem ) ;
  free_heap ( &pHeap ) ;

#ifdef DEBUG_COLLAPSE
  {
    double id, root_id ;
    int err ;
    char solFile[LINE_LEN] ;
    
    if ( pUns->pVxColor && pUns->pRootChunk->Punknown && nLevel == 1 ) {
      
      pMgVx = pMgVrtx ; pMgCo = pMgCoor ;

      pChunk = NULL ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
        for ( pVx  = pVxBeg ; pVx <= pVxEnd ; pVx++ )
          if ( pVx->number ) {
            pVx->Punknown[0] = 1. ;
            pVx->Punknown[1] = 1.*pUns->pVxColor[ pVx->number ].maxColl ;
            pVx->Punknown[2] = 1.*pUns->pVxColor[ pVx->number ].mVxColl ;

            if ( pUns->pVxColor[ pVx->number ].mVxColl > maxColl[3] ) {
              printf ( " vx: %d, %d coll.\n",
                       pVx->number, pUns->pVxColor[ pVx->number ].mVxColl ) ;
            }
          }
    }
    /*
      strcpy ( solFile, "ubend-test" ) ;
      prepend_path ( solFile ) ;
      strcat ( solFile, ".sol.adf" ) ;


      ADF_Database_Open( solFile, "UNKNOWN", "IEEE_BIG", &root_id, &err ) ;
      if ( err != -1 ) {
      printf ( " FATAL: file: %s could not be opened: error %d.\n", solFile, err ) ;
      return ( 0 ) ; }


      write_test_sol ( pUns, root_id ) ;

      

      ADF_Database_Close ( root_id, &err ) ;*/
  }
#endif      


  /* Make a complete grid level, renumber. */
  if ( !make_coarser_level ( pUns, pMgUns, pMgVrtx, pMgCoor, pllEdge, nLevel ) ) {
    hip_err ( fatal, 0, "Failed to create coarser level in uns_coarsen." ) ;
   }

  /* Why do some boundary faces fold? */
  check_uns ( pMgUns, check_lvl ) ;

  pUns->pEgLen = pEgLen ;

  
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "coarsened for level %2d from %"FMT_ULG" to %"FMT_ULG" elements (%1.3g),\n"
             "                                        %"FMT_ULG" to %"FMT_ULG" vertices (%1.3g).\n", 
             nLevel,
	     pUns->mElemsNumbered, pMgUns->mElemsNumbered,
	     pUns->mElemsNumbered/1./pMgUns->mElemsNumbered,
	     pUns->mVertsNumbered, pMgUns->mVertsNumbered,
	     pUns->mVertsNumbered/1./pMgUns->mVertsNumbered ) ;
    hip_err ( info, 3, hip_msg ) ;
  }
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "tried to %"FMT_ULG", succeeded to collapse %"FMT_ULG" edges, \n"
              "            tried to %"FMT_ULG", succeeded to collapse %"FMT_ULG" elements. \n",
              mEgCollTried, mEgColl, mElCollTried, mElColl ) ;
    hip_err ( info, 3, hip_msg ) ;
  }
    
  
  return ( pMgUns ) ;
}


/******************************************************************************

  face_isnt_collapsed:
  Given a face of an element, find out whether it is collapsed. This
  relies on vertex numbers carrying the number of the node they are
  collapsed into.
  
  Last update:
  ------------
  4Feb06: derived from make_coarser_level.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int face_isnt_collapsed ( const bndFc_struct *pBndFc, ulong_t nVxFc[], int *pmVxFc ) {

  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  vrtx_struct * const *ppVx ; 
  int mVx, kVx, kkVx, nVx ;

  pFoE = elemType[ pBndFc->Pelem->elType ].faceOfElem + pBndFc->nFace ;
  mVx = pFoE->mVertsFace ;
  ppVx = pBndFc->Pelem->PPvrtx ;
  kVxFace = pFoE->kVxFace ;

  /* Make a list of all the non-collapsed vertices on this face. */
  for ( *pmVxFc = kVx = 0 ; kVx < mVx ; kVx++ ) {
    nVx = ppVx[ kVxFace[kVx] ]->number ;
            
    /* Is the node already listed? */
    for ( kkVx = 0 ; kkVx < *pmVxFc ; kkVx++ )
      if ( nVx == nVxFc[kkVx] )
        break ;
            
    if ( kkVx == *pmVxFc )
      /* nVx is not yet in the list. */
      nVxFc[ (*pmVxFc)++ ] = nVx ;
  }

  if ( *pmVxFc <  elemType[ pBndFc->Pelem->elType ].mDim )
    /* Element is collapsed. */
    return ( 0 ) ; 
  else
    return ( 1 ) ;
}



/******************************************************************************

  clean_coll_grid:
  .
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
          fix bug with using == instead of = for setting pEl->number.
          initialise mElemsSimplified.
  4Feb06: derived from make_coarser_level.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int clean_coll_grid ( uns_s *pUns ) {
  
  const int mDim = pUns->mDim ;

  chunk_struct *pChunk;
  int kVx ;
  ulong_t mElemsFound ;
  int mVxElem, nVxBeg, nVxEnd ;
  ulong_t nVxElem[MAX_VX_ELEM], mElemsSimplified=0, nVxFc[MAX_VX_FACE] ;
  int mVxFc ;
  ulong_t mBndFc ;
  double *pCoor ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx, *pVrtx ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcBeg, *pBndFcEnd, *pBndFc ;
  bndFcVx_s *pBv ;

  
  pVrtx = pUns->ppChunk[0]->Pvrtx ;
  pCoor = pUns->ppChunk[0]->Pcoor ;


  
  /* Reset vertex usage marks. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
    for ( pVx  = pVxBeg ; pVx <= pVxEnd ; pVx++ ) {
      pVx->mark = 0 ;
      if ( pVx->number )
        /* Number the vertices according to coordinate use. This will give each vertex
           the number of the root vertex. The root vertex uses its own coordinate slot,
           vertices collapsed onto the root point to the roots coordinate slot. */
        pVx->number = mgVrtx( pVrtx, pCoor, mDim, pVx->number ) ;
    }



  /* Allocate a temp bndFc array based on vertices. */
  pUns->mBndFcVx = mBndFc = pUns->mFaceAllBc ;
  pBv = pUns->pBndFcVx = arr_malloc ( "pUns->pBndFcVx in read_adf_bnd", pUns->pFam,
                                      mBndFc, sizeof( *pBv ) ) ;

  
  /* Record the forming vertices of all non-collapsed boundary faces. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->nFace ) {

        if ( face_isnt_collapsed ( pBndFc, nVxFc, &mVxFc ) ) {
          /* Find the supporting element using the new vertex numbers. 
             Naughty: reuse the storage for pBc. */
          pBv->pBc = pBndFc->Pbc ;
          pBv->mVx = mVxFc ;
          for ( kVx = 0; kVx < mVxFc ; kVx++ )
            pBv->ppVx[kVx] = pVrtx + nVxFc[kVx] ;

          pBndFc->Pbc = ( bc_struct * ) pBv ;
          pBv++ ;
        }
        else {
          /* Collapsed face, discard. */
          pBndFc->Pelem = NULL ;
          pBndFc->nFace = 0 ;
        }
      }





  /* Mark non-collapsed elements and their vertices. */
  pChunk = NULL ;
  pUns->numberedType = noNum ;
  mElemsFound = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {

      if ( pEl->invalid )
	pEl->mark = 0 ;
      else if ( mgelem_isnt_collapsed ( pEl, pVrtx, pCoor, &mVxElem, nVxElem ) ) {
        /* At least a simplex left. Mark all used vertices. */
        pEl->number = ++mElemsFound ;

        /* Mark all used vertices and reset el2vx pointers to the root copy. */
        for ( kVx = 0 ; kVx < mVxElem ; kVx++ ) {
          pVrtx[ nVxElem[kVx] ].mark = 1 ;
          pEl->PPvrtx[kVx] = pVrtx + pEl->PPvrtx[kVx]->number ;
        }

        /* Can this element be simplified to another primitive? */
        mElemsSimplified += simplify_one_elem ( pEl, pEl ) ;
      }
      else {
        pEl->number = 0 ;
        pEl->invalid = 1 ;
	pEl->mark = 0 ;
	pEl->term = 0 ;
      }
    }



  /* Redo the vx to elem pointers after simplification. */
  if ( !( pUns->pllVxToElem = make_vxToElem ( pUns ) ) ) {
    printf ( " FATAL: could not create vertex to elem list in clean_coll_grid.\n" ) ;
    return ( 0 ) ; }

  


  /* Loop over all boundary faces and if they are not collapsed,
     rematch them to an element */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->nFace ) {

        pBv = ( bndFcVx_s * ) pBndFc->Pbc ;
        mVxFc = pBv->mVx ;
        for ( kVx = 0; kVx < mVxFc ; kVx++ )
          nVxFc[kVx] = pBv->ppVx[kVx]->number ;
          
        if ( !find_face ( nVxFc, mVxFc, pVrtx, pUns->pllVxToElem, 
                          &(pBndFc->Pelem), &(pBndFc->nFace) ) ) {
          printf ( " FATAL: no matching boundary face in clean_coll_grid.\n" ) ;
          return ( 0 ) ; }          
      }


  arr_free ( pUns->pBndFcVx ) ; 
  pUns->pBndFcVx = NULL ;
  pUns->mBndFcVx= 0 ;

  /* Redo the vertex to element list for the coarser level in make_coarser_level. */
  free_toElem ( &pUns->pllVxToElem ) ;



  /* Final renumber of the vertices. Disconnected vertices will remain unnumberd. */
  pUns->numberedType = invNum ;
  number_uns_grid ( pUns ) ;

  return ( 1 ) ;
}




/******************************************************************************

  coll_insitu
  Collapse one element at a time in situ.
  
  Last update:
  ------------
  14jul20; do set quality limits for angles, otherwise there are flat cells.
  1Jul16; new interface to check_uns.
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  4Feb06; derived from coarsen_one_level_el
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

uns_s *coll_insitu ( uns_s *pUns, const double minVol ) {

  const vrtx_struct *pVxStack[STACK_SIZE] ;
  
  llEdge_s *pllEdge ;
  edgeLen_s *pEgLen ;
  double *pCoor, oldVol, elemVol[STACK_SIZE] ;
  int mVxStack, mElStack, nEl, mVxElem ;
  ulong_t mEgCollTried = 0, mEgColl = 0, mElCollTried = 0, 
    mEl, mFailColl = 0, mCollEl, sFnonCollEg, sFintBndEg, sFegLen, 
    sFmColl, sFangle, sFtwist, sFvolume ;
  int maxColl[MAX_DIM+1] =  { 0, 99, 99, 99 } ;
  ulong_t nVxElem[MAX_VX_ELEM] ;
  vrtx_struct *pVrtx ;
  elem_struct *pElem, *pElBeg, *pElEnd, *pElStack[STACK_SIZE] ;

  chunk_struct *pChunk ;
  heap_s *pHeap ;
  elCrit_s elCrit ;
  elCollFail_s fail ;

  double elVol ;
  /* Don't apply mesh quality criteria for these collapses.
  const double lvlArCutOff=1.e99, lvlLrgstAngle=-1.e99 ; */
  const double lvlArCutOff = mgArCutoff, lvlLrgstAngle = mgLrgstAngle ;

  sprintf ( hip_msg, "collapsing in situ for min vol >= %g.", minVol ) ;
  hip_err ( info, 3, hip_msg ) ;


  
  /* Make an edge list. Remake it, since we're not sure that counters
     have remained after renumbering. */
  ulong_t mEdges ;
  pllEdge = make_llEdge_bnd ( pUns, &mEdges, sizeof( edgeLen_s ),
                              (void**) &(pUns->pEgLen) ) ;
  pEgLen = pUns->pEgLen ;

  if ( !pllEdge )
    hip_err ( fatal, 0, "could not create edge list in coll_insitu." ) ;
  else 
    pUns->pllEdge = pllEdge ;

  if ( !( pUns->pllVxToElem = make_vxToElem ( pUns ) ) )
    hip_err ( fatal, 0, "could not create vertex to elem list in coll_insitu." ) ;

  /* contiguous vertex and coordinate storage. */
  pVrtx = pUns->ppChunk[0]->Pvrtx ;
  pCoor = pUns->ppChunk[0]->Pcoor ;


  /* Match periodic edges. Mark boundary edges. */
  if ( pUns->mPerBcPairs )
    match_per_in_all_edges ( pUns, pllEdge, pEgLen ) ;
    
  /* Count the number of boundaries of a node for collapsing priorities. */
  vx_properties ( pUns, mEdges, pllEdge, pEgLen, lvlArCutOff ) ;




  /* Make a heap list to sort elements for smallest volume. */
  if ( !( pHeap = make_heap ( pUns->mElemsNumbered, 1, 
                              sizeof( elCrit_s ), pUns->pFam, cmp_elCrit ) ) ) {
    hip_err ( fatal, 0, "failed to alloc a heap list in coll_insitu." ) ;
  }

  ulong_t mElColl=1 ;
  int nSweep ;
  const elem_struct *pMgEl ;
  /* JDM: tried a few sweeps, but the propagation through re-heaping works.
  for ( nSweep = 1 ; mElColl && nSweep <= 1 ; nSweep++ ) {
    mElColl = 0 ;
  */
    /* Add all elements below the volume threshold to the heap. */
    pChunk = NULL ;
    mEl = 0 ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number ) {
          pMgEl = make_mgElem( pElem, pVrtx, pCoor ) ;
          elVol = get_elem_vol( pMgEl
                                //pElem
                                ) ;
          if ( elVol < minVol &&
               mgelem_isnt_collapsed ( pMgEl,
                                       // NULL, NULL, 
                                       pUns->pRootChunk->Pvrtx, pUns->pRootChunk->Pcoor,
                                       &mVxElem, nVxElem ) ) {
            mEl++ ;
            elCrit.pElem = pElem ;
            elCrit.vol = elVol ;
            add_heap ( pHeap, &elCrit ) ;
            /* Set a mark for non-collapsed elements. */
            pElem->mark = 0 ;

            if ( verbosity > 4 ) {
              sprintf ( hip_msg, "found element smaller than minVol with vol=%g:\n", elVol) ;
              hip_err ( info, 5, hip_msg ) ;
              printelco (pElem) ;
            }
          }
        }

    if ( verbosity > 2 ) {
      /* Initialise the output string. */
      sprintf ( hip_msg, "found %"FMT_ULG" elements smaller than minVol\n", mEl ) ;
      hip_err ( info, 2, hip_msg ) ;
    }

  
    /* Loop over all elements while there are collapsible ones. */
    pElem = NULL ;
    while ( get_heap ( pHeap, &elCrit, 1 ) ) {

      /* An element may be heaped more than once. Try it only once inbetween
         collapses. */
      if ( elCrit.pElem != pElem && !elCrit.pElem->invalid ) {
        /* Compare the actual volume of the element with the heaped volume. */
        pElem = ( elem_struct * ) elCrit.pElem ;
        oldVol = elCrit.vol ;
        elCrit.vol = get_elem_vol ( make_mgElem( pElem, pVrtx, pCoor ) ) ;
      
        if ( oldVol != 0. && 
             ABS( oldVol - elCrit.vol ) > 1.e-14 && 
             !pElem->mark )
          /* The vol of the elem has changed since it was introduced. Reheap.
             Zero volume indicates that this is a neighboring element with priority. */
          add_heap ( pHeap, &elCrit ) ;
      
        else if ( !pElem->mark && elCrit.vol < minVol )
          /* Properly heaped elem which is too small, try to collapse
             it. */
          if ( mgelem_isnt_collapsed ( pElem,
                                       //NULL, NULL, 
                                       pUns->pRootChunk->Pvrtx, pUns->pRootChunk->Pcoor,
                                       &mVxElem, nVxElem ) ) {
            mElCollTried++ ;
          
            if ( tryToCollapse_elem ( pElem, pUns, pllEdge, pVrtx, pCoor, &pEgLen,
                                      pVxStack, &mVxStack, pElStack, elemVol, &mElStack,
                                      &mEgCollTried, &mEgColl,
                                      maxColl, lvlArCutOff, lvlLrgstAngle, 0, &fail ) ) {
              /* Successful collapse. */
              mElColl++ ;
              pElem->invalid = 1 ; pElem->number = 0 ;
            
              /* Reheap all elements with the changed geometry. */
              for ( nEl = 0 ; nEl < mElStack ; nEl++ )
                /* If the volume is 0., flag it for being collapsed. */
                if ( elemVol[nEl] < 0. ) {
                  /* Collapsed neighbour. */
                  if ( !pElStack[nEl]->mark ) {
                    /* Newly collapsed. Mark and count it. */
                    pElStack[nEl]->mark = 1 ;
                  }
                }
                else {
                  /* Neighbouring element affected but not collapsed. Reheap. */
                  elCrit.pElem = pElStack[nEl] ;
                  elCrit.vol = elemVol[nEl] ;
                  add_heap ( pHeap, &elCrit ) ;
                }
            }
            else
              mFailColl++ ;
          }
      }
    } //  while ( get_heap ( pHeap, &elCrit, 1 ) ) {


    if ( verbosity > 2 ) {
      sprintf ( hip_msg, //"sweep %d," 
                "tried %"FMT_ULG", collapsed %"FMT_ULG" elements\n", 
                //nSweep,
                mElCollTried, mElColl ) ;
      hip_err ( info, 2, hip_msg ) ;
      fflush ( stdout ) ;
    }

    /*}*/ // nSweeps

  
  if ( verbosity > 2 ) {
    /* Find out why the remaining edges could not be collapsed. */
    sFnonCollEg =  sFintBndEg = 0 ;
    sFegLen = sFmColl = sFangle = sFtwist = sFvolume = 0 ;
    mCollEl = 0 ;
    
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && !pElem->invalid && !pElem->mark ) {
          elVol = get_elem_vol( make_mgElem( pElem, pVrtx, pCoor ) ) ;
          if ( elVol < minVol &&
              mgelem_isnt_collapsed ( pElem,
                                      // NULL, NULL, 
                                      pUns->pRootChunk->Pvrtx, pUns->pRootChunk->Pcoor,
                                      &mVxElem, nVxElem ) ) {
            if ( tryToCollapse_elem ( pElem, pUns, pllEdge, pVrtx, pCoor, &pEgLen,
                                      pVxStack, &mVxStack, pElStack, elemVol, &mElStack,
                                      &mEgCollTried, &mEgColl,
                                      maxColl, lvlArCutOff, lvlLrgstAngle, 1, &fail ) )
              mElColl++ ;
            else {
              mCollEl++ ;
              sFnonCollEg += fail.nonCollEg ;
              sFintBndEg  += fail.intBndEg ; 
              sFegLen     += fail.egLen ;    
              sFmColl     += fail.mColl ;    
              sFangle     += fail.angle ;    
              sFtwist     += fail.twist ;    
              sFvolume    += fail.volume ;

              if ( !fail.nonCollEg  && !fail.intBndEg  && !fail.egLen  &&
                   !fail.mColl  && !fail.angle  && !fail.twist  && !fail.volume ) {
                sprintf ( hip_msg, "OOOPS in coll_insitu: %"FMT_ULG"\n", 
                          pElem->number ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
            } // if ( tryToCollapse_elem ( pElem, p
          } // if ( elVol < minVol &&
        } //  if ( pElem->number && !pElem->mark ) {

    sprintf ( hip_msg, " of %8"FMT_ULG" elements smaller than vol=%g,\n"
              "             %8"FMT_ULG" were collapsed,\n"
              "             %8"FMT_ULG" failed due to non collapsible edges,\n"
              "             %8"FMT_ULG" failed due to internal edges between boundaries,\n"
              "             %8"FMT_ULG" failed due to excessively lengthened edges,\n"
              "             %8"FMT_ULG" failed due to too many fine grid verts in a coarse one,\n"
              "             %8"FMT_ULG" failed due to excessive dihedral angles,\n"
              "             %8"FMT_ULG" failed due to excessive twist,\n"
              "             %8"FMT_ULG" failed due to negative volume,\n",
              mEl, minVol, mElColl, sFnonCollEg, sFintBndEg, sFegLen, sFmColl,
              sFangle, sFtwist,sFvolume ) ;
    hip_err ( info,3,hip_msg ) ;
  } // if ( verbosity > 2 ) {

  

  /* Done with the heap. */
  free_heap ( &pHeap ) ;


  /* Free the edged storage with the penultimate coarse mesh. */
  free_llEdge ( &pUns->pllEdge ) ;


  /* Redo the vertex to element list for the coarser level in make_coarser_level. */
  free_toElem ( &pUns->pllVxToElem ) ;



  /* Make a complete grid level, renumber. */
  if ( !clean_coll_grid ( pUns ) )
    hip_err ( fatal, 0, "failed to clean up collapsed grid in coll_insitu." ) ;


  /* Why do some boundary faces fold? */
  check_uns ( pUns, check_lvl ) ;

  return ( pUns ) ;
}

/******************************************************************************

  uns_coarsen:
  Coarsen one level.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int umg_coarsen ( uns_s *pUns, const int mLevels ) {

  uns_s *pUnsToCoarsen = pUns ;
  int nLevel ;
  double lvlMgLen = mgLen, lvlArCutOff = mgArCutoff, lvlLrgstAngle = mgLrgstAngle ;

  /* Renumber on leafs. The new levels are properly numbered after generation. */
  pUns->numberedType = invNum ;
  number_uns_grid ( pUns ) ;
  special_verts ( pUns ) ;

  
  if ( mLevels < 1 ) {
    hip_err ( warning, 1, "mLevels < 1: nothing to be done." ) ;
    return ( 1 ) ; }

  /* Remove the solution.
     delete_uns_sol ( pUns ) ; */

  for ( nLevel = 1 ; nLevel <= mLevels ; nLevel++ ) {

    if ( !( pUnsToCoarsen =
            umg_coarsen_one_level_el ( pUnsToCoarsen, nLevel,
                                   lvlMgLen, lvlArCutOff, lvlLrgstAngle ))) {
      sprintf ( hip_msg, "Could not coarsen to level %d in uns_coarsen.", nLevel ) ;
      hip_err ( fatal, 0, hip_msg ) ;
      return ( 0 ) ;
    }

    /* Ramp the aspect ratio cutoff with level depth. */
    lvlMgLen      *= mgRamp ; 
    lvlArCutOff   *= mgRamp ;
    lvlLrgstAngle = MAX( lvlLrgstAngle*mgRamp, -.99 ) ;
  }
    
  arr_free ( pUnsToCoarsen->pUnsFine->pEgLen ) ;
  pUnsToCoarsen->pUnsFine->pEgLen  = NULL ;

  /* Free the edged storage with the penultimate coarse mesh. */
  free_llEdge ( &pUnsToCoarsen->pUnsFine->pllEdge ) ;

  arr_free ( pUnsToCoarsen->pUnsFine->pVxColor ) ;
  pUnsToCoarsen->pUnsFine->pVxColor  = NULL ;

  return ( 1 ) ;
}


/******************************************************************************

  uns_coll_insitu:
  Coarsen a small number of elements in situ, ie. the coarsened grid is
  overwritten, no mg. levels.
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  4Feb06: derived from uns_coarsen 
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int umg_collapse_insitu ( uns_s *pUns, const double minVol ) {

  uns_s *pUnsToCoarsen = pUns ;

  /* Renumber on leafs. The new levels are properly numbered after generation. */
  pUns->numberedType = invNum ;
  number_uns_grid ( pUns ) ;
  special_verts ( pUns ) ;


  /* To avoid allocating duplicate and contiguous vertex and coordinate
     arrays, insist on the mesh being a single chunk.
  if ( pUns->mChunks > 1 ) {
    hip_err ( fatal, 0, "uns_coll_insitu needs the mesh in a single chunk.\n"
             "        Write your grid to file and reread.\n" ) ;
    return ( 0 ) ;
  } */
  /* Combine all coor into a single list to enable the linked list of collapses of mgVrtx. */
  make_single_pVrtx ( pUns ) ;

  /* Issue a warning about grids with non-simplex elements. */
  if ( pUns->mElemsNumbered - pUns->mElemsOfType[tri] - pUns->mElemsOfType[tet] )
    hip_err ( warning, 1, " using element collapse on non-simplex meshes\n" 
             "          can produce degenerate elements. Make sure your\n" 
             "          solver can deal with these. See the manual for details.\n" ) ;



  if ( !coll_insitu ( pUnsToCoarsen, minVol )) {
    hip_err ( fatal, 0, "Could not collapse elems in uns_coll_insitu.\n" ) ;
    return ( 0 ) ;
  }
    
  arr_free ( pUns->pEgLen ) ;
  pUns->pEgLen  = NULL ;

  /* Free the edged storage with the penultimate coarse mesh. */
  free_llEdge ( &pUns->pllEdge ) ;

  arr_free ( pUns->pVxColor ) ;
  pUns->pVxColor  = NULL ;

  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mg_compute_transfer_coeff:
*/
/*! given a sequence of coarser grids, compute transfer coeffs.
 *
 */

/*
  
  Last update:
  ------------
  11Nov17; use new interface to find_elem_tree_walk, increment intFullTol by 2.
  29Oct17; fix bug with ordering of ppElContain: starts from one.
  29jun17: conceived.
  

  Input:
  ------
  

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int umg_sequence ( const int mLevels, const char gridList[][LINE_LEN] ) {

  uns_s *ppUns[MAX_MG_LEVELS] ;
  /* Check the sequence. */

  int kGr ;
  for ( kGr = 0 ; kGr < mLevels ; kGr++ ) {
    if ( !( ppUns[kGr] = find_uns_expr ( gridList[kGr] ) ) ) {
      sprintf ( hip_msg, "grid matching `%s' at level %d is not an unstructured grid.",
                gridList[kGr], kGr ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }


  // make finest the current grid.
  set_current_grid_expr ( gridList[0] ) ;


  // Loop over all levels.
  uns_s *pUnsF, *pUnsTo, *pUnsC, *pUnsFrom ;
  kdroot_struct *pTree ;
  elem_struct **ppElC ;  
  double *pWt ;
  const elem_struct *pEl ;
  const elemType_struct *pElT ;
  int fixNeg = 0 ;
  chunk_struct *pChunk  ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVxTo ;
  elem_struct *pElC ;
  int nBeg, nEnd ;
  int mOut = 0, mReallyOut = 0 ;
  int domainOverlap = 1 ;
  for ( kGr = 0 ; kGr < mLevels-1 ; kGr++ ) {
    sprintf ( hip_msg, " generating inter-grid connectivity from %s to %s",
              gridList[kGr], gridList[kGr+1] ) ;
    hip_err ( info, 2, hip_msg ) ;

    
    pUnsF = ppUns[kGr] ;
    pUnsC = ppUns[kGr+1] ;
    

    // Link sequence.
    pUnsF->pUnsCoarse = pUnsC ;
    pUnsC->pUnsFine = pUnsF ;
    pUnsC->pUnsFinest = ppUns[0] ;

    /* Equivalence with interpolation. */
    pUnsFrom = pUnsC ;
    pUnsTo = pUnsF ;

    /* Tree structure to search for nearest vx. */
    sprintf ( hip_msg, "        adding vertices of coarse grid matching `%s' to the data-tree",
              gridList[kGr+1] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    pTree = kd_intp_tree ( pUnsFrom, pUnsTo, 0 ) ;

    /* Generate vertex to element pointers, needed for reco_el only. */
    sprintf ( hip_msg, "        generating vertex to element pointers for coarse grid matching `%s' ",
              gridList[kGr+1] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    pUnsFrom->pllVxToElem = make_vxToElem ( pUnsFrom ) ;




    
    
    // Alloc list of fine to coarse pointers.
    ppElC = pUnsF->ppElContain =
      arr_malloc ( "pnElContain in umg_sequence" ,pUnsF->pFam,
                   pUnsF->mVertsNumbered+1, sizeof ( *pUnsF->ppElContain ) ) ;
    pWt = pUnsF->pElContainVxWt =
      arr_malloc ( "pnElContainVxWt in umg_sequence" ,pUnsF->pFam,
                   MAX_VX_ELEM*(pUnsF->mVertsNumbered+1),
                   sizeof ( *pUnsF->pElContainVxWt ) ) ;
    


    
    // Loop over fine grid vx, find in coarse.
    int kVx ;
    pChunk = NULL ;
    while ( loop_verts ( pUnsTo, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
      for ( pVxTo = pVxBeg ; pVxTo <= pVxEnd ; pVxTo++ )
        if ( pVxTo->number ) {
          /* Find the closest vertex of pUnsFrom. 
             Note: thickness of the 'global' search layer is incremented by 2,
             since the search should be done, but we want to keep the intFullTol default at 0.*/
          pElC = find_el_tree_walk ( pVxTo, pUnsFrom, pTree,
                                     intPolRim, intFcTol, 2.+intFullTol,
                                     &mOut, &mReallyOut, &domainOverlap ) ;
          ppElC++ ;
          *ppElC = pElC ;

          // interpolate_elem ( pEl, pVxTo, pUnInt, &pUnsFrom->varList, reco ) ;
          pElT = elemType + pElC->elType ;
          minNormEl( pElC, pElT->mDim, pElT->mVerts, pVxTo->Pcoor, minnorm_tol, 0, pWt ) ;
          for ( kVx = pElT->mVerts ; kVx < MAX_VX_ELEM ; kVx++ )
            pWt[kVx] = 0. ; // Zero remaining elements.
          pWt += MAX_VX_ELEM ;
        }
    }


    /* Done for this mesh. */
    kd_del_tree ( &pTree ) ;
    free_toElem ( &pUnsFrom->pllVxToElem ) ;
    
  }

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  umg_restrict:
*/
/*! restrict a solution to the coarse grid.
 *
 */

/*
  
  Last update:
  ------------
  3Mar18: conceived.
  

  Input:
  ------
  pUnsF: fine grid
  isAvg: non-zero of restricting an averaged quanty such a u,v
         zero for an integral quanity such as res

  Returns:
  --------
  1 on failure, 0 on success
  
*/

int umg_restrict ( uns_s *pUnsF, int isAvg ) {

  uns_s *pUnsC = pUnsF->pUnsCoarse ;
  const int mDim = pUnsF->mDim ;
  
  double *pSumWtC = NULL ;
  if ( isAvg ) {
    pSumWtC = arr_calloc ( "pSumWtC in umg_restrict", pUnsF->pFam,
                           pUnsC->mVertsAlloc+1, sizeof ( *pSumWtC) ) ;
  }

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  int nVx ;
  elem_struct **ppElC = pUnsF->ppElContain, *pElC ;
  const double *pVxCWt = pUnsF->pElContainVxWt ;  ;
  int mVx, kVx ;
  vrtx_struct *pVxC ;
  double *pUnF, *pUnC, wt ;
  int kUn ;
  while ( loop_verts ( pUnsF, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        nVx = pVx->number ;
        pUnF = pVx->Punknown ;

        pElC = *(++ppElC) ;
        mVx = elemType[pElC->elType].mVerts ;

        for ( kVx = 0 ; kVx < mVx ; kVx ++ ) {
          pVxC = pElC->PPvrtx[kVx] ;
          pUnC = pVxC->Punknown ;
          wt = *pVxCWt++ ;
          pSumWtC[pVxC->number] += wt ;
          for ( kUn = 0 ; kUn < mDim+2 ; kUn++ ) {
            pUnC[kUn] += wt*pUnF[kUn] ;
          }
        }
        /* pVxCWt always writes MAX_VX_ELEM entries. */
        for ( ; kVx < MAX_VX_ELEM ; kVx++ ) {
          pVxCWt++ ;
        }
      }


  /* Scale coarse grid. */
  pChunk = NULL ;
  if ( isAvg ) {
    while ( loop_verts ( pUnsC, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVxC = pVxBeg ; pVxC <= pVxEnd ; pVxC++ )
        if ( pVxC->number ) {
          pUnC = pVxC->Punknown ;
          wt = pSumWtC[pVxC->number] ;
          for ( kUn = 0 ; kUn < mDim+2 ; kUn++ ) {
            pUnC[kUn] /= wt ;
          }
        }
  }
  
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  umg_prolong:
*/
/*! prolong a solution to the coarse grid.
 *
 */

/*
  
  Last update:
  ------------
  3Mar18: conceived.
  

  Input:
  ------
  pUnsF: fine grid

  Returns:
  --------
  1 on failure, 0 on success
  
*/

int umg_prolong ( uns_s *pUnsC ) {

  uns_s *pUnsF = pUnsC->pUnsFine ;
  const int mDim = pUnsC->mDim ;
  

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  int nVx ;
  elem_struct **ppElC = pUnsF->ppElContain, *pElC ;
  const double *pVxCWt = pUnsF->pElContainVxWt ;  ;
  int mVx, kVx ;
  vrtx_struct *pVxC ;
  double *pUnF, *pUnC, wt ;
  int kUn ;
  while ( loop_verts ( pUnsF, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        nVx = pVx->number ;
        pUnF = pVx->Punknown ;

        pElC = *ppElC++ ;
        mVx = elemType[pElC->elType].mVerts ;
        for ( kVx = 0 ; kVx < mVx ; kVx ++ ) {
          pVxC = pElC->PPvrtx[kVx] ;
          pUnC = pVxC->Punknown ;
          wt = *pVxCWt++ ;
          for ( kUn = 0 ; kUn < mDim+2 ; kUn++ ) {
            pUnF[kUn] += wt*pUnC[kUn] ;
          }
        }
        for ( ; kVx < MAX_VX_ELEM ; kVx++ ) {
          pVxCWt++ ;
        }
      }
  
  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  umg_test_restrict_prolong:
*/
/*! perform restriction and prolongation with const, linear and quad testfun.
 *
 *
 */

/*
  
  Last update:
  ------------
  3Apr20; use gridList.
  2Mar18: conceived.
  

  Input:
  ------
  pUns: grid
  gridLit: array of expressions matching starting grid in 0, final grid in 1 pos.

    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int umg_test_restrict_prolong ( uns_s *pUns, char gridList[2][LINE_LEN] ) {

  /* Translate the expressions in gridList into a numeric list. */
  int nGrid[2] ;
  uns_s *pUnsIni = find_uns_expr ( gridList[0] ) ;
  nGrid[0] = pUnsIni->nr ;
  uns_s *pUnsEnd = find_uns_expr ( gridList[1] ) ;
  nGrid[1] = pUnsEnd->nr ;

  
  uns_s *pUnsF, *pUnsC ;
  int iGrid ;
  /* Find the starting grid, last for prolong, first for restrict. */
  if ( nGrid[0] == nGrid[1] ) {
    hip_err ( warning, 1,
              "impossible sequence in umg_test_restrict_prolong." ) ;
    return ( 1 ) ;
  }
  else if ( nGrid[0] < nGrid[1] ) {
    /* Restriction. */
    iGrid = 0 ;
    for ( pUnsF = pUns ; pUnsF && iGrid != nGrid[0] ;
          pUnsF = pUnsF->pUnsCoarse )
      iGrid++ ;
    if ( !pUnsF || !(pUnsC = pUnsF->pUnsCoarse) ) {
      hip_err ( warning, 1,
                "no matching fine/coarse grid in umg_test_restrict_prolong." ) ;
      return ( 1 ) ;
    }
    pUnsIni = pUnsF ;
  }
  else {
    /* Prolongation. */
    iGrid = 0 ;
    for ( pUnsC = pUns ; pUnsC && iGrid != nGrid[1] ;
          pUnsC = pUnsC->pUnsCoarse )
      iGrid++ ;
    if ( !pUnsC || !(pUnsF = pUnsC->pUnsFine) ) {
      hip_err ( warning, 1,
                "no matching coarse/fine grid in umg_test_restrict_prolong." ) ;
      return ( 1 ) ;
    }
    pUnsIni = pUnsC ;
 }
  

  /* Apply prim var test solution: 
     rho = 1, 
     u,v,w = x,y,z, 
     p = 1 + xy + yz + zx */
  double someDbl = 0. ;
  init_uns_var ( pUnsIni, "x", someDbl ) ;


  
  /* Restrict or prolong. */
  const int isAvg = 1 ;
  if ( nGrid[0] < nGrid[1] ) {
    /* Restrict. */
    for ( iGrid = nGrid[0] ; iGrid < nGrid[1] ; iGrid++ ) {
      /* Allocate and initialise the solution on the coarse grid. */
      init_uns_var ( pUnsF->pUnsCoarse, "0", someDbl ) ;
      umg_restrict ( pUnsF, isAvg ) ;

      /* Next level */
      pUnsF = pUnsF->pUnsCoarse ;
    }
  }
  else {
    /* Prolong. */
    for ( iGrid = nGrid[1] ; iGrid > nGrid[1] ; iGrid-- ) {
      /* Allocate and initialise the solution on the fine grid. */
      init_uns_var ( pUnsC->pUnsFine, "0", someDbl ) ;
      umg_prolong ( pUnsC ) ;
      
      /* Next level */
      pUnsC = pUnsC->pUnsFine ;
    }
  }
  
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  uns_mg_ops:
*/
/*! Wrapper for all mg ops, find out which opt, branch.
 *
 */

/*
  
  Last update:
  ------------
  10jul17: conceived.
  

  Input:
  ------
  argLine:

  Returns:
  --------
  0
  
*/

int uns_mg ( char *argLine ) {

  char mg_op[LINE_LEN] ;
  double minVol ;
  int mLevels ;
  char gridList[MAX_MG_LEVELS][LINE_LEN] ;

  
  umg_args ( argLine, mg_op, &minVol, &mLevels, gridList ) ;

  
  switch ( mg_op[0] ) {
  case 'v':
    umg_collapse_insitu ( Grids.PcurrentGrid->uns.pUns, minVol ) ;
    break ;
    
  case 's':
    umg_sequence ( mLevels, gridList ) ;
    break ;
    
  case 'c':
    umg_coarsen ( Grids.PcurrentGrid->uns.pUns, mLevels ) ;
    break ;

  case 't':
    umg_test_restrict_prolong (  Grids.PcurrentGrid->uns.pUns, gridList ) ;
    break ;
    
  default:
    hip_err ( warning, 1, "unrecognised mg operation, nothing done." ) ;
  }

  return ( 0 ) ;
}
