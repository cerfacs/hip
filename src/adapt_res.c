/*

  adapt_res.c:
  Use the residual and adjoint weights to adapt.
  
  Last update:
  ------------
   18Dec10; new pBc->type
  14Apr01; new smoothing, residual at nodes, use stdd_distr, intro maxLevels.
  22Sep00; written.
  

  This file contains:
  -------------------

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"


extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern const double Gamma, GammaM1 ;


/******************************************************************************
  thresholds:
  Calculate the refinement thresholds using fixed fractions.
  *******************************************************************************/




int thresholds ( double val[], int mVals,
                 double fLo, double *pValLo, double fHi, double *pValHi ) {

  int m, mAdEl ;
  double *vcp = arr_malloc ( "vcp", NULL, mVals, sizeof( double ) ) ;

  /* Copy the list and sort the copy. */
  memcpy ( vcp, val, mVals*sizeof( double ) ) ;
  qsort ( vcp, mVals, sizeof( double ), cmp_double ) ;

  /* Remove the top end containing non-adaptable elements with TOO_MUCH. */
  for ( m = mVals-1 ; m >= 0 ; m-- )
    if ( val[m] < TOO_MUCH )
      break ;
  mAdEl = m ;


  m = fLo*mVals ;
  m = MAX( 0, MIN( m, mVals-1 ) ) ;
  *pValLo = vcp[m] ;
  if ( m == 0 )
    /* No derefinement to be done. */
    *pValLo -= 1. ;

  m = (1.-fHi)*mVals ;
  m = MAX( 0, MIN( m, mVals-1 ) ) ;
  *pValHi = vcp[m] ;

  arr_free ( vcp ) ;

  return ( mAdEl ) ;
}

static void count_refs ( double vf[], int mEls,
                         double valDeref, int *pmDeref,
                         double valRef,   int *pmRef ) {

  int nEl ;

  *pmDeref = *pmRef = 0 ;

  for ( nEl = 0 ; nEl < mEls ; nEl++ )
    if ( vf[nEl] < valDeref )
      (*pmDeref)++ ;
    else if ( vf[nEl] > valRef )
      (*pmRef)++ ;

  return ;
}


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  30Apr19; use new interface to loop_elems. 
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mark_ref_deref ( uns_s *pUns, const double *vf, double deref, double ref ) {

  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd, *pCh ;
  int nr, mRef, mDeref, nCh, mCh ;

  
  /* Reset all element markers for de/refinement. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      pElem->markdEdges = pElem->derefElem = 0 ;
  
  /* Loop over all elems. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        nr = pElem->number ;
        
        if ( vf[nr] < deref )
          /* Derefine. */
          pElem->derefElem = 1 ;

        else if ( vf[nr] > ref && vf[nr] < TOO_MUCH )
          /* Refine iso. */
          pElem->markdEdges = elemType[ pElem->elType ].allEdges ;
      }


  /* Correct for marked buffer elements. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->term && pElem->PrefType && pElem->PrefType->refOrBuf == buf ) {
        /* This is a buffered terminal element. Refine the parent if one of
           the children is to be refined. Derefine the parent if all children
           are to be derefined. */
        mRef = mDeref = 0 ;
        mCh = pElem->PrefType->mChildren ;
        
        /* Loop over all children. */
        for ( nCh = 0 ; nCh < mCh ; nCh++ ) {
          pCh = pElem->PPchild[nCh] ;

          mDeref += pCh->derefElem ;
          mRef   += ( pCh->markdEdges ? 1 : 0 ) ;
        }

        if ( mRef ) {
          /* Refine the parent. */
          pElem->markdEdges = elemType[ pElem->elType ].allEdges ;
          debuffer_elem ( pElem ) ;
        }

        else if ( mDeref == mCh ) {
          /* Derefine the parent. */
          pElem->derefElem = 1 ;
          debuffer_elem ( pElem ) ;
        }
      }
        
  return ( 1 ) ;
}

/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int ini_adj ( double *pAdj, int mVx, char var ) {

  double ini_r[5] = { 1., 0.,0.,0., 0. } ; 
  double ini_q[5] = { 0., 1.,1.,1., 0. } ; 
  double ini_p[5] = { 0., 0.,0.,0., 1. } ;
  double ini_a[5] = { 1., 1.,1.,1., 1. } ; 
  double *ini, *pA ;
  int nVx ;
 
  if ( var == 'r' )
    ini = ini_r ;
  else if ( var == 'q' )
    ini = ini_q ;
  else if ( var == 'p' )
    ini = ini_p ;
  else if ( var == 'a' )
    ini = ini_a ;
  else {
    printf ( " FATAL: no such residual '%c' in ini_adj.\n", var ) ;
    return ( 0 ) ;
  }

  for ( nVx = 0 ; nVx < mVx ; nVx++ ) {
    pA = pAdj + 5*nVx ;

    pA[0] = ini[0] ;
    pA[1] = ini[1] ;
    pA[2] = ini[2] ;
    pA[3] = ini[3] ;
    pA[4] = ini[4] ;
  }
  
  
  return ( 1 ) ;
}

/******************************************************************************

  :
  .
  
  Last update:
  ------------
  9jul19; replace ADAPT_REF with ADAPT_HIERARCHIC
  30Apr19; revive ADAPT_REF
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int plot_ele_dpl ( uns_s *pUns, double *vf, int mEq, int iB, int iE, int plotLog,
                   char *plotFile ) {

  FILE *dpl ;
  char dplName[LINE_LEN] ;
  ulong_t mVx = 0 ;
  int mVxEl, nVx, nBeg, nEnd, nr, i ;
  vrtx_struct *pVx ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  double val, *pV ;

  if ( pUns->mDim != 2 ) {
    printf ( " cannot write flat shaded dpl file in %d-d.\n", pUns->mDim ) ;
    return ( 0 ) ; }


  printf ( " writing flat shaded file to %s\n", plotFile ) ;
  strcpy ( dplName, plotFile ) ;
  prepend_path ( dplName ) ;
  dpl = fopen ( dplName, "w" ) ;

  /*
    flo = -14.52 ;
    fhi = -3.12 ;
    printf ( " using limits: %g < f < %g\n", flo, fhi ) ; */

  fprintf ( dpl, "unstr\n" ) ;
  fprintf ( dpl, "%"FMT_ULG"\n", pUns->mElemsOfType[tri] + pUns->mElemsOfType[qua] ) ;

  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        nr = pElem->number ;

        if ( pElem->elType == tri ) {
          fprintf ( dpl, " 3 %"FMT_ULG" %"FMT_ULG" %"FMT_ULG"  %16.5e %d\n", mVx+1, mVx+2, mVx+3, vf[nr],nr ) ;
          mVx += 3 ;
        }
        else {
          fprintf ( dpl, " 3 %"FMT_ULG" %"FMT_ULG" %"FMT_ULG" %"FMT_ULG"   %d\n", mVx+1, mVx+2, mVx+3, mVx+4, nr ) ;
          mVx += 4 ;
        }
      }


  fprintf ( dpl, "%"FMT_ULG"\n", 3*pUns->mElemsOfType[tri] + 4*pUns->mElemsOfType[qua] ) ;
  fprintf ( dpl, "1. 7. 0. 84.\n" ) ;

  pChunk = NULL ;
  mVx = 0 ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        nr = pElem->number ;
        mVxEl = elemType[ pElem->elType ].mVerts ;

        pV = vf + nr*mEq ;
        for ( val = 0., i = iE ; i <= iE ; i++ )
          val += pV[i] ;
        val = ( plotLog ? log( ABS( val ) ) : val ) ;
        /*
        val = MAX( val, flo ) ;
        val = MIN( val, fhi ) ;
        if ( pElem == pElEnd-1 ) val = flo ;
        if ( pElem == pElEnd ) val = fhi ;
        */

        for ( nVx = 0 ; nVx < mVxEl ; nVx++ ) {
          pVx = pElem->PPvrtx[nVx] ;
          mVx++ ;
          fprintf ( dpl, "%15.7e %15.7e %15.7e 1. 0. 84.  %"FMT_ULG" %"FMT_ULG"\n",
                    pVx->Pcoor[0], pVx->Pcoor[1], val, pVx->number, mVx ) ;
        }
      }

  fprintf ( dpl, "0\n0\n" ) ;
  fclose ( dpl ) ;

  return ( 1 ) ;
}

/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int plot_vx_dpl ( uns_s *pUns, double *vf, int mEq, int iB, int iE, int plotLog,
                  char *plotFile ) {

  FILE *dpl ;
  char dplName[LINE_LEN] ;
  int nBeg, nEnd, mVx = 0, nr, i ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  double val, *pV ;

  if ( pUns->mDim != 2 ) {
    printf ( " cannot write dpl file in %d-d.\n", pUns->mDim ) ;
    return ( 0 ) ; }

  iB = MAX( 0, iB ) ;
  iE = MIN( mEq, iE ) ;
  

  printf ( " writing gouraud shaded average from %d-%d to %s.\n", iB, iE, plotFile ) ;
  strcpy ( dplName, plotFile ) ;
  prepend_path ( dplName ) ;
  dpl = fopen ( dplName, "w" ) ;

  fprintf ( dpl, "unstr\n" ) ;
  fprintf ( dpl, "%"FMT_ULG"\n", pUns->mElemsOfType[tri] + pUns->mElemsOfType[qua] ) ;

  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        nr = pElem->number ;

        if ( pElem->elType == tri ) {
          fprintf ( dpl, " 3 %"FMT_ULG" %"FMT_ULG" %"FMT_ULG"  %"FMT_ULG"\n",
                    pElem->PPvrtx[0]->number,
                    pElem->PPvrtx[1]->number,
                    pElem->PPvrtx[2]->number, pElem->number ) ;
          mVx += 3 ;
        }
        else {
          fprintf ( dpl, " 3 %"FMT_ULG" %"FMT_ULG" %"FMT_ULG" %"FMT_ULG"   %"FMT_ULG"\n", 
                    pElem->PPvrtx[0]->number,
                    pElem->PPvrtx[1]->number,
                    pElem->PPvrtx[2]->number,
                    pElem->PPvrtx[3]->number, pElem->number ) ;
          mVx += 4 ;
        }
      }


  fprintf ( dpl, "%"FMT_ULG"\n", pUns->mVertsNumbered ) ;
  fprintf ( dpl, "1. 7. 0. 84.\n" ) ;

  pChunk = NULL ;
  while ( loop_verts( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg; pVrtx <= pVxEnd ; pVrtx++ )
      if ( ( nr = pVrtx->number ) ) {
        pV = vf + nr*mEq ;
        for ( val = 0., i = iE ; i <= iE ; i++ )
          val += pV[i] ;
        
        val = ( plotLog ? log( ABS( val ) ) : val ) ;
            
        fprintf ( dpl, "%15.7e %15.7e %15.7e 1. 0. 84.  %d\n",
                  pVrtx->Pcoor[0], pVrtx->Pcoor[1], val, nr ) ;
      }
  
  fprintf ( dpl, "0\n0\n" ) ;
  fclose ( dpl ) ;

  return ( 1 ) ;
}

/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void get_unknown_vx ( vrtx_struct *pVx, int addW, double unknVx[5] ) {

  const double *pUn ;
  
  /* Copy nodal unknowns. Shift in a 0. w-component in 2D.*/
  pUn = pVx->Punknown ;

  if ( addW ) {
    /* Slip in a w component. */
    unknVx[0] = pUn[0] ;
    unknVx[1] = pUn[1] ;
    unknVx[2] = pUn[2] ;
    unknVx[3] = 0. ;
    unknVx[4] = pUn[3] ;
  }      
  else { 
    unknVx[0] = pUn[0] ;
    unknVx[1] = pUn[1] ;
    unknVx[2] = pUn[2] ;
    unknVx[3] = pUn[3] ;
    unknVx[4] = pUn[4] ;
  }

  return ;
}

/******************************************************************************

  :
  Euler fluxes from primitive variables.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void euler_flux ( const double primVar[5], double f[5], double g[5], double h[5] ) {

  double r, u, v, w, p, qq, gpq ;
  
  r = primVar[0] ;
  u = primVar[1] ;
  v = primVar[2] ;
  w = primVar[3] ;
  p = primVar[4] ;
          
  qq = u*u + v*v + w*w ;
  gpq = Gamma/GammaM1*p + .5*r*qq ;
          
  f[0] = r*u ;
  f[1] = r*u*u + p ;
  f[2] = r*u*v ;
  f[3] = r*u*w ;
  f[4] = gpq*u ;
          
  g[0] = r*v ;
  g[1] = r*v*u ;
  g[2] = r*v*v + p ;
  g[3] = r*v*w ;
  g[4] = gpq*v ;
        
  h[0] = r*w ;
  h[1] = r*w*u ;
  h[2] = r*w*v ;
  h[3] = r*w*w + p ;
  h[4] = gpq*w ;

  return ;
}

static void fc_avg ( const elem_struct *pElem, const int nFc, const int mEq,
                     double unknVx[MAX_VX_ELEM][MAX_UNKNOWNS],
                     double unknFc[MAX_UNKNOWNS] ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mVxFace = pElT->faceOfElem[nFc].mVertsFace ;
  const int *kVxFace = pElT->faceOfElem[nFc].kVxFace ;
  int nE, iVx, kVx ;

  for ( nE = 0 ; nE < MAX_UNKNOWNS ; nE++ )
    unknFc[nE] = 0. ;
  
  for ( iVx = 0 ; iVx < mVxFace ; iVx++ ) {
    kVx = kVxFace[iVx] ;
    for ( nE = 0 ; nE < mEq ; nE++ ) {
      unknFc[nE] += unknVx[kVx][nE] ;
    }
  }

  for ( nE = 0 ; nE < MAX_UNKNOWNS ; nE++ )
    unknFc[nE] /= mVxFace ;

  return ;
}
  
static void add_face_flux ( const double fcNorm[3],
                            double f[], double g[], double h[], int mEq,
                            double elRes[] ) {
          
  int nEq ;
  
  for ( nEq = 0 ; nEq < mEq ; nEq++ )
    elRes[nEq] += f[nEq]*fcNorm[0] + g[nEq]*fcNorm[1] + h[nEq]*fcNorm[2] ;


  return ;
}


  
/******************************************************************************

  :
  .
  
  Last update:
  ------------
  30Apr19; revive ADAPT_REF
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void res_Euler ( uns_s *pUns, double elRes[], int mEq ) {

  const elemType_struct *pElT ;
  const int mDim = pUns->mDim,
    addW = ( pUns->varList.mUnknFlow == mDim+ 2 ? 1 : 0 ), *kVxFace ;

  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBf, *pBfBeg, *pBfEnd ;
  ulong_t mVx ;
  int kVx, nFc, mFc, mTimes, mVxFace, nE, nVx[MAX_VX_ELEM] ;
  double fcNorm[MAX_DIM], unknVx[MAX_VX_ELEM][MAX_UNKNOWNS],
    f[5], g[5], h[5], nx, ny, nz, d, *pR, unknFc[MAX_UNKNOWNS] ;

  
  /* Loop over all elements. Calculate an Euler flux residual via Green-Gauss. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        pElT = elemType + pElem->elType ;
        mVx = pElT->mVerts ;
        pR = elRes + pElem->number*mEq ;

        /* Calculate the unknowns, introduce an empty w if needed. */
        for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
          nVx[kVx] = pElem->PPvrtx[kVx]->number ;
          get_unknown_vx ( pElem->PPvrtx[kVx], addW, unknVx[kVx] ) ;
        }

        for ( nE = 0 ; nE < mEq ; nE++ )
          pR[nE] = 0. ;
        
        /* Get the face normals and average flow quantities. */
        mFc = pElT->mSides ;
        for ( nFc = 1 ; nFc <= mFc ; nFc++ ) {

          /* Normal. */
          uns_face_normal ( pElem, nFc, fcNorm, &mTimes ) ;
          fcNorm[0] /= mTimes ;
          fcNorm[1] /= mTimes ;
          fcNorm[2] /= mTimes ;

          /* Average unknowns, flux. */
          fc_avg ( pElem, nFc, mEq, unknVx, unknFc ) ;
          euler_flux ( unknFc, f, g, h ) ;
          add_face_flux ( fcNorm, f, g, h, mEq, pR ) ;
        }
      }


  
  /* Loop over all boundary faces. */
  pChunk = NULL ;
  pBP = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBP, &pBfBeg, &pBfEnd ) )
    for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ )
      if ( pBf->Pelem && pBf->Pelem->number && pBf->Pbc->type[0] == 'i' ) {
        /* This an inviscid wall face. */
        pElem = pBf->Pelem ;
        pElT = elemType + pElem->elType ;
        mVx = pElT->mVerts ;
        pR = elRes + pElem->number*mEq ;

        for ( nE = 0 ; nE < mEq ; nE++ )
          pR[nE] = 0. ;
        
        /* Calculate the unknowns, introduce an empty w if needed. */
        for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
          nVx[kVx] = pElem->PPvrtx[kVx]->number ;
          get_unknown_vx ( pElem->PPvrtx[kVx], addW, unknVx[kVx] ) ;
        }
        
        /* Unit normal. Note that we need the properly scaled normal in
           add_face_flux. */
        nFc = pBf->nFace ;
        uns_face_normal ( pElem, nFc, fcNorm, &mTimes ) ;
        fcNorm[0] /= mTimes ;
        fcNorm[1] /= mTimes ;
        fcNorm[2] /= mTimes ;
        d  = sqrt( fcNorm[0]*fcNorm[0] +
                   fcNorm[1]*fcNorm[1] +
                   fcNorm[2]*fcNorm[2] ) ;
        nx = fcNorm[0]/d ;
        ny = fcNorm[1]/d ;
        nz = fcNorm[2]/d ;
        
        /* Forming vertices of the face. */
        mVxFace = pElT->faceOfElem[nFc].mVertsFace ;
        kVxFace = pElT->faceOfElem[nFc].kVxFace ;

        /* Average unknowns. */
        fc_avg ( pElem, nFc, mEq, unknVx, unknFc ) ;

        /* Flip the normal to subtract the flux. */
        fcNorm[0] *= -1 ;
        fcNorm[1] *= -1 ;
        fcNorm[2] *= -1 ;
        
        euler_flux ( unknFc, f, g, h ) ;
        add_face_flux ( fcNorm, f, g, h, mEq, pR ) ;

        /* Add pressure flux. */
        for ( nE = 0 ; nE < mEq ; nE++ )
          f[nE] = g[nE] = h[nE] = 0. ;
        f[1] = g[2] = h[3] = -unknFc[4] ;
        
        add_face_flux ( fcNorm, f, g, h, mEq, pR ) ;
      }

  return ;
}

/******************************************************************************

  calc_vols:
  Calculate finite volumes.
  
  Last update:
  ------------
  30Apr19; use new interface to loop_elems.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void calc_vols ( uns_s *pUns, int vxMrk[], double *elVol, double *vxVol ) {

  const elemType_struct *pElT ;
  
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  double vol ;
  int nBeg, nEnd, kVx, mVx, nVx, mVxMrk ;

  /* Reset nodal volumes. */
  for ( nVx = 0 ; nVx < pUns->mVertsNumbered ; nVx++ )
    vxVol[nVx] = 0. ;
  
  
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        pElT = elemType + pElem->elType ;
        mVx = pElT->mVerts ;

        /* How many vertices are marked? */
        for ( kVx = mVxMrk = 0 ; kVx < mVx ; kVx++ ) {
          nVx = pElem->PPvrtx[kVx]->number ;
          if ( vxMrk[nVx] )
            mVxMrk++ ;
        }

        if ( mVxMrk ) {
          vol = get_elem_vol ( pElem ) ;
          vol /= mVxMrk ;

          /* Store the volume divided by the number of nodes for each element. */
          elVol[ pElem->number ] = vol ;

          for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
            nVx = pElem->PPvrtx[kVx]->number ;
            if ( vxMrk[nVx] )
              vxVol[nVx] += vol ;
          }
        }
        else
          elVol[ pElem->number ] = 0. ;
      }
  
  return ;
}

/* Set the elemental values to TOO_MUCH if the element is non-adaptable. */

static void rm_nonad_el ( uns_s *pUns, double elVal[], int mEq ) {

  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  int nr, i, adaptable ;
  double *pV ;

  
  /* Loop over all elements. Calculate the inner product v.f using central
     averages at each element. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( ( nr = pElem->number ) ) {

        if ( Grids.adapt.maxLevels >= 999 )
          adaptable = 1 ;
        else if ( adaptLvl_elem ( pElem ) < Grids.adapt.maxLevels )
          adaptable = 1 ;
        else
          adaptable = 0 ;

        if ( !adaptable ) {
          pV = elVal + mEq*nr ;
          for ( i = 0 ; i < mEq ; i++ )
            pV[i] = TOO_MUCH ;
        }

      }
  
  return ;
}

/******************************************************************************

  :
  Take a continous array defined over elements and scatter to the elem.
  Reweight by number of contributions, i.e. number of nodes of the elem.
  
  Last update:
  ------------
  7may24; rename _nr to distinguish from _area.
  30Apr19; use new interface to loop_elems.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void scatter_vx2ele_nr ( uns_s *pUns, int mEq, int vxMrk[],
                             double vxRes[], double vxVol[],
                             double elRes[], double elVol[] ) {

  const elemType_struct *pElT ;
  const int mEl = pUns->mElemsNumbered ;

  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  int nVx, mVxEl, kVx, nEl, nE ;
  double  *eR, *vR, fac ;


  /* Reset elRes for elements with marked vertices only. */
  for ( nEl = 0 ; nEl <= mEl ; nEl++ )
    if ( !elVol || elVol[nEl] > 0. ) {
      eR = elRes + mEq*nEl ;
      for ( nE = 0 ; nE < mEq ; nE++ )
        eR[nE] = 0. ;
    }
  
  
  /* Loop over all elements. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( ( nEl = pElem->number ) ) {
        pElT = elemType + pElem->elType ;
        eR = elRes + mEq*nEl ;

        mVxEl = pElT->mVerts ;
        for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
          nVx = pElem->PPvrtx[kVx]->number ;
          if ( vxMrk[nVx] ) {
            vR = vxRes + mEq*nVx ;

            /* If no elVol is given, use centroid interpolation. */
            fac = ( elVol ? elVol[nEl]/vxVol[nVx] : 1./mVxEl ) ;
            for ( nE = 0 ; nE < mEq ; nE++ )
              eR[nE] += fac*vR[nE] ;
          }
        }
      }
    
  return ;
}
/******************************************************************************

  :
  Take a continous array defined over elements and scatter to the nodes.
  Reweight by number of contributions, i.e. cells formed by a node.
  
  Last update:
  ------------
  7may24; rename _nr to distinguish from _area.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void scatter_ele2vx_nr ( uns_s *pUns, int mEq, int vxMrk[],
                             double elRes[], double vxRes[] ) {

  const elemType_struct *pElT ;
  const int mVx = pUns->mVertsNumbered ;

  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  int nBeg, nEnd, nVx, mVxEl, kVx, nEl, nE, mVxMrk ;
  double  *eR, *vR, fac ;


  /* Reset vxRes for marked vertices only. */
  for ( nVx = 0 ; nVx <= mVx ; nVx++ )
    if ( vxMrk[nVx] ) {
      vR = vxRes + mEq*nVx ;
      for ( nE = 0 ; nE < mEq ; nE++ )
        vR[nE] = 0. ;
    }

  
  /* Loop over all elements. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( ( nEl = pElem->number ) ) {

        pElT = elemType + pElem->elType ;
        eR = elRes + mEq*nEl ;

        /* Count the participating vertices. */
        mVxEl = pElT->mVerts ;
        for ( kVx = mVxMrk = 0 ; kVx < mVxEl ; kVx++ ) {
          nVx = pElem->PPvrtx[kVx]->number ;
          mVxMrk += vxMrk[nVx] ;
        }


        /* Scatter to marked vertices only. */
        for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
          nVx = pElem->PPvrtx[kVx]->number ;
          if ( vxMrk[nVx] ) {
            fac = 1./mVxMrk ;
            vR = vxRes + mEq*nVx ;
            for ( nE = 0 ; nE < mEq ; nE++ )
              vR[nE] += fac*eR[nE] ;
          }
        }
      }
    
  return ;
}


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  30Apr19; use new interface to loop_verts.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void flag_shock_nodes ( uns_s *pUns, int mVx, int vxMrk[], int nghLvl ) {
  
  /* Find a shock. */
  const elemType_struct *pElT ;
  const edgeOfElem_struct *pEoE ;
  const int mDim = pUns->mDim ;
    
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd, *pVx0, *pVx1 ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  int nBeg, nEnd, nVx, nVx0, nVx1, nEl, nEg, mVxMrk, *vxMa, n ;
  double *pU, *pU0, *pU1, rho, u,v,w, p, q, c, Ma,
    U[3], p0, p1, *pCo0, *pCo1, xe[3], scProd ;

  vxMa = arr_calloc ( "vxMa", pUns->pFam, mVx+1, sizeof( int ) ) ;


  for ( nVx = 0 ; nVx <= mVx ; nVx++ )
    vxMrk[nVx] = 0 ;
  
  /* Mark all supersonic points. */
  pChunk = NULL ;
  while ( loop_verts( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg; pVrtx <= pVxEnd ; pVrtx++ )
      if ( ( nVx = pVrtx->number ) ) {
        pU  = pVrtx->Punknown ;
        rho = pU[0] ;
        u   = pU[1] ;
        v   = pU[2] ;
        w   = pU[3] ;
        p   = pU[4] ;

        q   = sqrt( u*u + v*v + w*w ) ;
        c   = sqrt( Gamma*p/rho ) ;
        Ma  = q/c ;
        vxMa[nVx] = ( Ma > 1. ? 1 : 0 ) ;
      }


  
  /* Mark all endnodes of transonic edges if there is compression in
       flow direction. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( ( nEl = pElem->number ) ) {
          
        pElT = elemType + pElem->elType ;

        for ( nEg = 0 ; nEg < pElT->mEdges ; nEg++ ) {
          pEoE = pElT->edgeOfElem + nEg ;
          pVx0 = pElem->PPvrtx[ pEoE->kVxEdge[0] ] ;
          pVx1 = pElem->PPvrtx[ pEoE->kVxEdge[1] ] ;
          nVx0 = pVx0->number ;
          nVx1 = pVx1->number ;

          if ( vxMa[nVx0] - vxMa[nVx1] ) {
            /* Different sonic states along the edge. */

            pU0  = pVx0->Punknown ;
            pU1  = pVx1->Punknown ;
            /* Average speed along the edge. */
            U[0] = .5*( pU0[1] + pU1[1] ) ;
            U[1] = .5*( pU0[2] + pU1[2] ) ;
            U[2] = .5*( pU0[3] + pU1[3] ) ;
            q = sqrt ( U[0]*U[0] + U[1]*U[1] + U[2]*U[2] ) ;
              
            /* Pressures at the end. */
            p0   = pU0[4] ;
            p1   = pU1[4] ;

            pCo0 = pVx0->Pcoor ;
            pCo1 = pVx1->Pcoor ;
              
            vec_diff_dbl ( pCo1, pCo0, mDim, xe ) ;
            vec_norm_dbl ( xe, mDim ) ;
            scProd = scal_prod_dbl ( xe, U, mDim ) ;
              
            if ( ( scProd/q >  .6 && p1 - p0 > 0. ) ||
                 ( scProd/q < -.6 && p1 - p0 < 0. ) ) {
              /* Transonic shocked edge. */
              vxMrk[nVx0] = vxMrk[nVx1] = 1 ;
            }
          }
        }
      }

  /* Propagate the mark to the nth neighbour. */
  for ( n = 0 ; n < nghLvl ; n++ ) {

    for ( nVx = 0 ; nVx <= mVx ; nVx++ )
      vxMa[nVx] = 0. ;

    pChunk = NULL ;
    while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
        if ( ( nEl = pElem->number ) ) {
          
          pElT = elemType + pElem->elType ;
          
          for ( nEg = 0 ; nEg < pElT->mEdges ; nEg++ ) {
            pEoE = pElT->edgeOfElem + nEg ;
            pVx0 = pElem->PPvrtx[ pEoE->kVxEdge[0] ] ;
            pVx1 = pElem->PPvrtx[ pEoE->kVxEdge[1] ] ;
            nVx0 = pVx0->number ;
            nVx1 = pVx1->number ;

            if ( vxMrk[nVx0]|| vxMrk[nVx1] )
              vxMa[nVx0] = vxMa[nVx1] = 1. ;
          }
        }
    
    for ( nVx = 0 ; nVx <= mVx ; nVx++ )
      vxMrk[nVx] = ( vxMa[nVx] == 1. ? 1 : 0 ) ;
  }
    

  arr_free ( vxMa ) ;
    

  if ( verbosity > 3 ) {
    /* Count. */
    for ( nVx = 1, mVxMrk = 0 ; nVx <= mVx ; nVx++ )
      mVxMrk += vxMrk[nVx] ;
    printf ( "    INFO: found %d vertices around shocks.\n", mVxMrk ) ;
  }

  return ;
}


/******************************************************************************

  :
  Take a continous array defined over elements and scatter to the nodes.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/



static double v_f ( double elRes[], double elAdj[], const int mEl, const int mEq,
                    double vf[], int abs ) {

  int nr, nE ;
  double vfna, vfa, vfSum = 0., *pA, *pR ;


  
  /* Loop over all elements. Calculate the inner product v.f using central
     averages at each element. */
  for ( nr = 1 ; nr <= mEl ; nr++ ) {
    pR = elRes + nr*mEq ;
    pA = elAdj + nr*mEq ;
          
    vf[nr] = vfa = vfna = 0. ;

    if ( pA[0] < TOO_MUCH ) {
      for ( nE = 0 ; nE < mEq ; nE++ ) {
        vfa  += ABS( pR[nE]*pA[nE] ) ;
        vfna +=      pR[nE]*pA[nE] ;
      }
      vf[nr] = ( abs ? vfa : vfna ) ;
      vfSum += vfna ;
    }
    else
      vf[nr] = TOO_MUCH ;
  }

  return ( vfSum ) ;
}

/******************************************************************************

  :
  .
  
  Last update:
  ------------
  7may24; use renamed scatter*_nr
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void smooth_res ( uns_s *pUns, double elRes[], double vf[],
                         double vxAdj[], double elAdj[], int mEq,
                         int mSweeps, int mSweepsShock ) {

  int mEl = pUns->mElemsNumbered, mVx = pUns->mVertsNumbered, n, *vxMrk ;
  double *elVol = NULL, *vxVol = NULL, *vxRes = NULL ; 

  elVol = arr_malloc ( "elVol", pUns->pFam,      mEl+1,  sizeof( double ) ) ;
  vxVol = arr_calloc ( "vxVol", pUns->pFam,      mVx+1,  sizeof( double ) ) ;
  vxRes = arr_calloc ( "vxRes", pUns->pFam, mEq*(mVx+1), sizeof( double ) ) ;
  vxMrk = arr_malloc ( "vxMrk", pUns->pFam,     (mVx+1), sizeof( double ) ) ;

  /* Calculate finite volumes around  nodes. Use all nodes for initial smooting. */
  for ( n = 0 ; n < mVx+1 ; n++ )
    vxMrk[n] = 1 ;
  calc_vols ( pUns, vxMrk, elVol, vxVol ) ;


  /* Scatter the nodal adjoint to the elements. Note that the
     adjoint is not an quantity integrated over the dual volume like the residual
     but a point value. Simple average. */
  scatter_vx2ele_nr( pUns, mEq, vxMrk, vxAdj, vxVol, elAdj, NULL ) ;

  /* And mark non-adaptable elements. */
  rm_nonad_el ( pUns, elAdj, mEq ) ;
  
  /* Smoothen the residual. */
  if ( verbosity > 4 )
    printf ( "    INFO: smothing nodal residual %d times.\n", mSweeps ) ;


  if ( verbosity > 5 && pUns->mDim == 2 ) {
    plot_ele_dpl ( pUns, elRes, mEq, 0, 4, 0, "elRes.pre.0_4.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 0, 0, 0, "elRes.pre.0.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 1, 1, 0, "elRes.pre.1.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 2, 2, 0, "elRes.pre.2.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 4, 4, 0, "elRes.pre.4.dpl" ) ;
  }


  /* JDM
  for ( n = 0 ; n < (mEl+1)*mEq ; n++ )
    elRes[n] = 0. ;
  elRes[15*mEq] = elRes[22*mEq] = .5 ; */
    

#ifdef DEBUG
  if ( verbosity > 4 )
    printf ( " iter: %d, sumVf: %17.7e\n", -1,
             v_f( elRes, elAdj, mEl, mEq, vf, 0 ) ) ;
#endif
    
  

  for ( n = 0 ; n < mSweeps ; n++ ) {
    scatter_ele2vx_nr( pUns, mEq, vxMrk, elRes, vxRes ) ;
    scatter_vx2ele_nr( pUns, mEq, vxMrk, vxRes, vxVol, elRes, elVol ) ;

#ifdef DEBUG
    if ( verbosity > 4 )
      printf ( " iter: %d, sumVf: %17.7e\n", n,
               v_f( elRes, elAdj, mEl, mEq, vf, 0 ) ) ;
#endif
  }



  /* Now do smooting in the shock only. */
  flag_shock_nodes ( pUns, mVx, vxMrk, 1 ) ;

  /* JDM.
  for ( n = 0 ; n <= mVx ; n++ )
    vxMrk[n] = 0 ;
  vxMrk[19] = 1 ; */
  
  calc_vols ( pUns, vxMrk, elVol, vxVol ) ;


  for ( n = 0 ; n < mSweepsShock ; n++ ) {
    scatter_ele2vx_nr( pUns, mEq, vxMrk, elRes, vxRes ) ;
    scatter_vx2ele_nr( pUns, mEq, vxMrk, vxRes, vxVol, elRes, elVol ) ;

#ifdef DEBUG
    if ( verbosity > 4 )
      printf ( " iter: %d, sumVf: %17.7e\n", n,
               v_f( elRes, elAdj, mEl, mEq, vf, 0 ) ) ;
#endif
  }

  
  

  if ( verbosity > 5 && pUns->mDim == 2 ) {
    plot_vx_dpl ( pUns, vxRes, mEq, 0, 4, 0, "vxRes.1_5.dpl" ) ;
    plot_vx_dpl ( pUns, vxRes, mEq, 0, 0, 0, "vxRes.0.dpl" ) ;
    plot_vx_dpl ( pUns, vxRes, mEq, 1, 1, 0, "vxRes.1.dpl" ) ;
    plot_vx_dpl ( pUns, vxRes, mEq, 2, 2, 0, "vxRes.2.dpl" ) ;
    plot_vx_dpl ( pUns, vxRes, mEq, 4, 4, 0, "vxRes.4.dpl" ) ;

    for ( n = 0 ; n <= mVx ; n++ )
      vxRes[mEq*n] = vxMrk[n] ;
    vxRes[mEq*1] = -.1 ;
    vxRes[mEq*mVx] = 1.1 ;
    plot_vx_dpl ( pUns, vxRes, mEq, 0, 0, 0, "vxMrk.dpl" ) ;
    

    plot_ele_dpl ( pUns, elRes, mEq, 0, 4, 0, "elRes.post.0_4.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 0, 0, 0, "elRes.post.0.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 1, 1, 0, "elRes.post.1.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 2, 2, 0, "elRes.post.2.dpl" ) ;
    plot_ele_dpl ( pUns, elRes, mEq, 4, 4, 0, "elRes.post.4.dpl" ) ;
  }

    

  arr_free ( elVol ) ;
  arr_free ( vxVol ) ;
  arr_free ( vxRes ) ;

  return ;
}


static void get_avg_ele ( elem_struct *pElem, const double val[], int mEq,
                          double avg[]) {

  const int mVx = elemType[ pElem->elType ].mVerts ;
  const double *pV ;
  int nVx, kVx, nE ;

  for ( nE = 0 ; nE < mEq ; nE++ ) 
    avg[nE] = 0. ;
    
  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    nVx = pElem->PPvrtx[kVx]->number ;
    pV  = val + mEq*nVx ;
  
    /* Find the average for the element. */
    for ( nE = 0 ; nE < mEq ; nE++ ) 
      avg[nE] += pV[nE] ;
  }

  
  for ( nE = 0 ; nE < mEq ; nE++ ) 
    avg[nE] /= mVx ;

  
  return ;
}

/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int mark_vf ( uns_s *pUns,
                     char* adjFile, double fDeref, double fRef,
                     int iso, int mSmooth, int mSmoothShock, int abs ) {

  double *vxAdj = NULL, *elAdj = NULL, *vf = NULL,  *elRes = NULL,
    valDeref, valRef, avg, dev, sumVf ;
  int mAdj, mVx = pUns->mVertsNumbered, mEl = pUns->mElemsNumbered, nEl,
    mDeref, mRef, mAdEl ;

  if ( adjFile[1] != '\0' ) {
    sprintf ( hip_msg, " this format %s needs implementing in mark_vf\n", adjFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else {
    sprintf ( hip_msg, " this format %s needs implementing in mark_vf\n", adjFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  return ( 0 ) ;

    mAdj = 5 ;
    

  vf    = arr_calloc ( "vf",    pUns->pFam, mEl+1,        sizeof( double ) ) ;
  elRes = arr_calloc ( "elRes", pUns->pFam, mAdj*(mEl+1), sizeof( double ) ) ;
  vxAdj = arr_malloc ( "vxAdj", pUns->pFam, mAdj*(mVx+1), sizeof( double ) ) ;
  elAdj = arr_malloc ( "elAdj", pUns->pFam, mAdj*(mEl+1), sizeof( double ) ) ;


  
  if ( adjFile[1] != '\0' ) {
    /* Read the adjoint solution from file. Remember, node numbers start with 1. */
    sprintf ( hip_msg, " this format %s needs implementing in mark_vf\n", adjFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else{
    sprintf ( hip_msg, " this format %s needs implementing in mark_vf\n", adjFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Initialise the adjoint to unity. The first letter of the adjoint file
     dictates which components to keep. */
  ini_adj ( vxAdj+5, mVx, adjFile[0] ) ;

  

  
  /* Calculate the smoothened nodal residuals. */
  res_Euler ( pUns, elRes, mAdj ) ;

  /* Smoothen the residual. */
  smooth_res ( pUns, elRes, vf, vxAdj, elAdj, mAdj, mSmooth, mSmoothShock ) ;
  
  
  


  
  /* Calculate the dot product. */
  sumVf = v_f ( elRes, elAdj, mEl, mAdj, vf, abs ) ;
  if ( verbosity > 2 )
    printf ( "    INFO: sum v.f: %17.7e\n", sumVf ) ;

  if ( verbosity > 4 && pUns->mDim == 2 ) {
    plot_ele_dpl ( pUns, vf, 1, 0, 0, 1, "vf.log.dpl" ) ;
    plot_ele_dpl ( pUns, vf, 1, 0, 0, 0, "vf.dpl" ) ;
  }



  
  /* Use the absolute value for adaptation. Set the values for
     non-adaptable elements to TOO_MUCH. */
  for ( nEl = 1 ; nEl <= mEl ; nEl++ )
    vf[nEl] = ABS( vf[nEl] ) ;

  

  std_distr ( vf+1, mEl, &avg, &dev ) ;
  mAdEl = thresholds ( vf+1, mEl, fDeref, &valDeref, fRef, &valRef ) ;


  if ( fRef >= 0. ) {
    /* Refine using fixed fractions. */
    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "refining above %g = avg+%g*dev.\n",
                valRef, ( valRef - avg )/dev ) ;
      hip_err ( info, 1, hip_msg ) ;
      sprintf ( hip_msg, "derefining below %g = avg-%g*dev.\n",
                valDeref, ( avg-valDeref )/dev ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
  }
  else {
    /* Refine using mean deviation. */
    valDeref = avg - ABS(fDeref)*dev ;
    valRef   = avg + ABS(fRef)*dev ;
    count_refs ( vf+1, mEl, valDeref, &mDeref, valRef, &mRef ) ;
    
    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "refining %d elements above %g = %3.2f%%.\n",
               mRef, valRef, 1.*mRef/mEl ) ;
      hip_err ( info, 1, hip_msg ) ;
      sprintf ( hip_msg, "derefining %d elements below %g = %3.2f%%.\n",
               mDeref, valDeref, 1.*mDeref/mEl ) ;
      hip_err ( info, 1, hip_msg ) ;
  }
  }


  /* Mark the elems above/below the thresholds. */
  mark_ref_deref ( pUns, vf, valDeref, valRef ) ;
  
  arr_free ( vf ) ;
  arr_free ( elRes ) ;
  arr_free ( elAdj ) ;
  arr_free ( vxAdj ) ;

  return ( 1 ) ;
}


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int adapt_vf ( char *adjFile, double deref, double ref,
               int iso, int mSmooth, int mSmoothShock, int abs ) {

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;

  
  /* Easier to work with primitve variables. */
  conv_uns_var ( pUns, prim ) ;


  mark_vf ( pUns, adjFile, deref, ref, iso, mSmooth, mSmoothShock, abs ) ;
  
  /* If the ratio is given negative, do all adaption and propagation isotropically. */
  return ( adapt_uns_hierarchical( pUns, iso ) ) ;
}




