/*
   uns_listNprint.c
   Unstructured grid methods. Listing entities.
 
   Last update:
   ------------
   6Sep19; move list_grids here from hip.c.
   10Nov17; fix bug in printfcco for valid face number.
   16Aug17; add hMin and hMax to list_grid_info and list_grid_json
   14Dec16; intro list_grid_info
   8Apr13; use dereferenced pointer in sizeof when allocating.
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   22Dec97; cut out of uns_meth.c

   Contains:
   ---------
   list_grid_info:
   list_uns_bc:

   printel:
   printelco:
   printvxco:
   printfc:
   printfcco:
   printeg:
   printegco:

   findpvx:
   findelvx:
   findel2vx:
   findel3vx:
   findel4vx:

   findelbndfc:
   findvxbndfc:
*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern Grids_struct Grids ;
extern const char gridTypeNames[3][7] ;
extern const char varTypeNames[][LEN_VAR_C] ;
extern const char varCatNames[][LEN_VAR_C] ;

extern const elemType_struct elemType[] ;
extern const char topoString[][MINITXT_LEN] ;

/******************************************************************************
  list_grid_info:   */

/*! Print dim, mEle, mVx, mBc, etc to screen.
 *
 *
 */

/*
  
  Last update:
  ------------
  24Aug24; support axiY, axiZ.
  20Dec16; treat case of Patchlabel and/or patchBndArea being absent from hdf.
  14Dec16: conceived.
  

  Input:
  ------
  ...
  topo: not yet written out.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int list_grid_info ( int mDim, ulong_t mEl, ulong_t mConn, 
                     ulong_t mVx, ulong_t mBndFc, int mBc,
                     double volGrid, double volMin, 
                     double hMin, double hMax, 
                     char *bcLabel, size_t lbl_len,
                     double bndPatchArea[], 
                     double llBox[], double urBox[],
                     double llBoxCyl[], double urBoxCyl[], int isPer,
                     specialTopo_e specialTopo ) {

  sprintf ( hip_msg, "\n   Mesh has value/number of" ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     volume: %15.9e", volGrid ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     Element min volume: %15.9e", volMin ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     Element min eglen: %15.9e",hMin ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     Element max eglen: %15.9e",hMax ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     dim: %d", mDim ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     elements: %"FMT_ULG",", mEl ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     nodes: %"FMT_ULG",", mVx ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     connectivity entries: %"FMT_ULG",", mConn ) ; hip_err ( blank, 1, hip_msg ) ;

  sprintf ( hip_msg, "\n   Boundaries" ) ; hip_err ( blank, 1, hip_msg ) ;
  if ( isPer )
    hip_err ( blank,1, "     mesh has periodicity." ) ;
  else
    hip_err ( blank,1, "     mesh does not have periodicity." ) ;
  sprintf ( hip_msg, "     bnd faces: %"FMT_ULG",", mBndFc ) ; hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "     patches: %d", mBc ) ; hip_err ( blank, 1, hip_msg ) ;
  int nBc, lenLbl ;
  char label[30] ;
  if ( bcLabel && bndPatchArea ) {
    for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
      label[0] = '"' ;
      strncpy ( label+1, bcLabel + nBc*lbl_len, 25 ) ;
      label[25] = '\0' ;
      trim ( label ) ;
      lenLbl = strlen(label) ;
      label[ lenLbl   ] = '"' ;
      label[ lenLbl+1 ] = ',' ;
      label[ lenLbl+2 ] = '\0' ;
      sprintf( hip_msg, "      %d: %-25s surface: %15.9e",
               nBc+1, label, bndPatchArea[nBc] ) ;
      hip_err ( blank, 1, hip_msg ) ;
    }
  }
  else
    hip_err ( blank, 1, "     no labels or patch areas given in the hdf file." ) ;


  
  sprintf ( hip_msg, "\n   Domain" ) ; hip_err ( blank, 1, hip_msg ) ;
  char axDir=' ' ;
  double axMin, axMax ;
  if ( mDim == 3 ) {
    sprintf ( hip_msg, "     min x,y,z: %15.9e, %15.9e, %15.9e", llBox[0], llBox[1], llBox[2] ) ; hip_err ( blank, 1, hip_msg ) ;
    sprintf ( hip_msg, "     max x,y,z: %15.9e, %15.9e, %15.9e", urBox[0], urBox[1], urBox[2] ) ; hip_err ( blank, 1, hip_msg ) ;

    if ( specialTopo == axiX ) {
      axDir = 'x' ;
      axMin = llBox[0] ;
      axMax = urBox[0] ;
    }
    else if ( specialTopo == axiY ) {
      axDir = 'y' ;
      axMin = llBox[1] ;
      axMax = urBox[1] ;
    }
    else if ( specialTopo == axiY ) {
      axDir = 'z' ;
      axMin = llBox[2] ;
      axMax = urBox[2] ;
    }
    if ( specialTopo >= axiX || specialTopo <= axiZ ) {
      sprintf ( hip_msg, "     min r,th,%c:  %15.9e, %15.9e, %15.9e",
                axDir, llBoxCyl[0], llBoxCyl[1], axMin ) ; hip_err ( blank, 1, hip_msg ) ;
      sprintf ( hip_msg, "     max r,th,%c:  %15.9e, %15.9e, %15.9e",
                axDir, urBoxCyl[0], urBoxCyl[1], axMax ) ; hip_err ( blank, 1, hip_msg ) ;
    }
  }
  else {
    sprintf ( hip_msg, "     min x,y: %15.9e, %15.9e", llBox[0], llBox[1] ) ; hip_err ( blank, 1, hip_msg ) ;
    sprintf ( hip_msg, "     max x,y: %15.9e, %15.9e", urBox[0], urBox[1] ) ; hip_err ( blank, 1, hip_msg ) ;
    sprintf ( hip_msg, "     min r:   %15.9e", llBoxCyl[0] ) ; hip_err ( blank, 1, hip_msg ) ;
    sprintf ( hip_msg, "     max r:   %15.9e", urBoxCyl[0] ) ; hip_err ( blank, 1, hip_msg ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************
  list_grid_json:   */

/*! write a .json file  with the mesh info parameters 
 *
 *
 */

/*
  
  Last update:
  ------------
  21Fev17: conceived

  Returns:
  --------
  1 on failure, 0 on success
  
*/

int list_grid_json ( char *gridFile,int mDim, ulong_t mEl, ulong_t mConn, 
                     ulong_t mVx, ulong_t mBndFc, int mBc,
                     double volGrid, double volMin, 
                     double hMin, double hMax, 
                     char *bcLabel, size_t lbl_len,
                     double bndPatchArea[], 
                     double llBox[], double urBox[],
                     double llBoxCyl[], double urBoxCyl[], int isPer) {

  FILE *jsonfileid ;
  char jsonfile[LINE_LEN];
  int length=strlen(gridFile);
  strncpy ( jsonfile, gridFile,length-3) ; 
  jsonfile[length-3]='\0' ;
  strcat(jsonfile,".json");

  if ( ( jsonfileid = fopen ( jsonfile, "w" ) ) == NULL ) {
    sprintf ( hip_msg, "file: %s could not be opened.\n",jsonfile ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  fprintf(jsonfileid,"{\n");
  fprintf(jsonfileid,"\"mesh\":\"%s\",\n",gridFile);
  fprintf(jsonfileid,"\"volume\":%15.9e,\n", volGrid);
  fprintf(jsonfileid,"\"minvol\":%15.9e,\n", volMin);
  fprintf(jsonfileid,"\"hmin\":%15.9e,\n", hMin);
  fprintf(jsonfileid,"\"hmax\":%15.9e,\n", hMax);
  fprintf(jsonfileid,"\"dim\":%d,\n", mDim);
  fprintf(jsonfileid,"\"elements\":%"FMT_ULG",\n", mEl );
  fprintf(jsonfileid,"\"nodes\":%"FMT_ULG",\n", mVx);

  if ( isPer )  
    fprintf(jsonfileid,"\"periodic\":true,\n");
  else 
    fprintf(jsonfileid,"\"periodic\":false,\n");

  /* Domain extents info */
  fprintf (jsonfileid,"\"xmin\":%15.9e,\n\"xmax\":%15.9e,\n",llBox[0],urBox[0]);
  fprintf (jsonfileid,"\"ymin\":%15.9e,\n\"ymax\":%15.9e,\n",llBox[1],urBox[1]);
  fprintf (jsonfileid,"\"rmin\":%15.9e,\n\"rmax\":%15.9e,\n", llBoxCyl[0], urBox[0]) ; 
  if ( mDim == 3) {
     fprintf (jsonfileid,"\"zmin\":%15.9e,\n\"zmax\":%15.9e,\n",llBox[2],urBox[2]);
     fprintf (jsonfileid,"\"thmin\":%15.9e,\n\"thmax\":%15.9e,\n", llBoxCyl[1],urBox[1] ) ;
  }

  /* Boundary info */
  fprintf(jsonfileid,"\"boundaries\":{\n");
  int nBc ;
  char label[lbl_len] ;

  if ( bcLabel && bndPatchArea ) {
    for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
      strncpy ( label, bcLabel + nBc*lbl_len, lbl_len ) ;
      trim ( label ) ;
      fprintf(jsonfileid,"              \"%s\":{\"surface\":%15.9e}",label,bndPatchArea[nBc]);
      if ( nBc < mBc-1 ) 
        fprintf(jsonfileid,",\n");
      else 
        fprintf(jsonfileid,"\n");
    }
  }
  fprintf(jsonfileid,"             }\n");

  fprintf(jsonfileid,"}\n");
  fclose (jsonfileid) ;
  hprintf ( "\n   Generated json file: %s\n \n",jsonfile);
  
  return ( 0 ) ;
}



/******************************************************************************

  list_uns_bc:
  List all active boundary conditions of an unstructured grid.
  
  Last update:
  ------------
  6Swp19; use hprintf.
  : conceived.
  
  Input:
  ------
  Pgrid:
  
*/

void list_uns_bc ( const grid_struct *Pgrid, char *keyword ){
  
  uns_s *pUns = Pgrid->uns.pUns ;

  bndVxWt_s *pBWt = NULL ;
  double *pBndPatchArea = NULL ;
  ulong_t  mVxAllBc ;
  if ( !strncmp ( keyword, "area", 2 ) ) {
    /* List all bnd vx. */
    
    pBWt = arr_malloc ( "pBWt in h5w_bnode", pUns->pFam,
                        pUns->mBc, sizeof( *pBWt ) ) ;
    make_bndVxWts ( pUns, noBcGeoType, pBWt, &mVxAllBc, 0, 1 ) ;
    
    /* Use those to compute the boundary patch surface */
    pBndPatchArea = arr_malloc ( "pBndPatchArea in list_menu", pUns->pFam,
                                 pUns->mBc, sizeof ( *pBndPatchArea ) ) ;
    bndPatch_area ( pUns->mBc, pUns->mDim, pBWt, pBndPatchArea ) ;
  }

  
  /* Print the header. */
  print_bc ( NULL, pBndPatchArea ) ;

  int nBc ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pBndPatchArea )
      print_bc ( pUns->ppBc[nBc], pBndPatchArea+nBc ) ;
    else 
      print_bc ( pUns->ppBc[nBc], NULL ) ;

  hprintf ( "\n" ) ;

  arr_free ( pBWt ) ;
  arr_free ( pBndPatchArea ) ;

  return ;
}

#ifndef ADAPT_METH
/* non-adaptive version, full version with adaptive support in adapt_meth.c  */

void printelal ( const elem_struct *Pelem )
{ 
  int kVx ;

  if ( !Pelem ) {
    printf ( " No such elem.\n" ) ;
    return ; }
  
  printf ( " el: %"FMT_ULG", %s, inv: %d, zn: %d, vx:",
	   Pelem->number, elemType[Pelem->elType].name,
	   Pelem->invalid, Pelem->iZone ) ;
  if ( Pelem->PPvrtx )
    for ( kVx = 0 ; kVx < elemType[Pelem->elType].mVerts ; kVx++ )
      if ( Pelem->PPvrtx[kVx] )
	printf ( " %"FMT_ULG"", Pelem->PPvrtx[kVx]->number ) ;
      else
	printf ( " inv." ) ;
  else
    printf ( " inv PPvrtx." ) ;

  printf ( "\n" ) ;
}

#endif




/* Some obvious utility functions, mainly for debugging. */

void printel ( const elem_struct *Pelem )
{ 
  int kVx ;

  if ( !Pelem ) {
    printf ( " Empty elem.\n" ) ;
    return ; }
  
  printf ( " el: %"FMT_ULG", type %s, ", Pelem->number, elemType[Pelem->elType].name ) ;
  if ( Pelem->PPvrtx )
    for ( kVx = 0 ; kVx < elemType[Pelem->elType].mVerts ; kVx++ )
      if ( Pelem->PPvrtx[kVx] )
	printf ( " %"FMT_ULG"", Pelem->PPvrtx[kVx]->number ) ;
      else
	printf ( " inv." ) ;
  else
    printf ( " invalid PPvrtx." ) ;
  printf ( "\n" ) ;
}

void printelco ( const elem_struct *Pelem )
{ 
  int kVx, nDim ;

  if ( !Pelem ) {
    printf ( " Empty elem.\n" ) ;
    return ; }
  
  printf ( "          nr: %"FMT_ULG", type: %s\n",
           Pelem->number, elemType[Pelem->elType].name ) ;

  for ( kVx = 0 ; kVx < elemType[Pelem->elType].mVerts ; kVx++ ) {
    printf ( "           %d: nr %7"FMT_ULG", ", kVx, Pelem->PPvrtx[kVx]->number ) ;
    if ( Pelem->PPvrtx[kVx]->Pcoor )
      for ( nDim = 0 ; nDim < 3 ; nDim++ )
	printf ( " %15.9e", Pelem->PPvrtx[kVx]->Pcoor[nDim] ) ;
    else
      printf ( ", no coordinates" ) ;
    printf ( ".\n" ) ;
  }
}

void printvxco ( const vrtx_struct *Pvx, const int mDim ) {
  int nDim ;

  if ( !Pvx ) {
    printf ( " Empty vx.\n" ) ;
    return ; }
  
  printf ( " vx: %"FMT_ULG", ", Pvx->number ) ;
  if ( Pvx->Pcoor )
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      printf ( " %f", Pvx->Pcoor[nDim] ) ;
  else
    printf ( " no coordinate." ) ;
  printf ( "\n" ) ;
}

void printco ( double *Pcoor, const int mDim ) {
  int nDim ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    printf ( " %f", Pcoor[nDim] ) ;
  printf ( "\n" ) ;
}

void printfc ( const elem_struct *pElem, const int nFace ) {
  
  int fcType, kVx, nDim ;
  vrtx_struct **PPvxFc[MAX_VX_FACE] ;

  if ( !pElem ) {
    printf ( " Empty elem.\n" ) ;
    return ; }
  
  const elemType_struct *pElT= elemType + pElem->elType ;
  if ( pElT->mFaces < nFace ) {
    printf ( " No such face in this elem type.\n" ) ;
    return ;
  }
  
  const faceOfElem_struct *pFoE = pElT->faceOfElem + nFace ;
  const int *kVxFace = pFoE->kVxFace ;
  const int mDim = elemType[pElem->elType].mDim ;
  vrtx_struct **ppVx = pElem->PPvrtx ;
  const int mVxFc = pFoE->mVertsFace ;
  const vrtx_struct *pVx ;

  printf ( " elem %"FMT_ULG", face %d, ", pElem->number, nFace ) ;
  for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
    pVx = ppVx[ kVxFace[kVx] ] ;
    printf ( "  %"FMT_ULG", ", pVx->number ) ;
  }
  printf ( ".\n" ) ;

  return ;
}

void printfcco ( const elem_struct *pElem, const int nFace ) {
  
  int fcType, kVx, nDim ;
  vrtx_struct **PPvxFc[MAX_VX_FACE] ;

  if ( !pElem ) {
    printf ( " Empty elem.\n" ) ;
    return ; }
  
  const elemType_struct *pElT= elemType + pElem->elType ;
  if ( nFace < 1 || pElT->mFaces < nFace ) {
    printf ( " No such face in this elem type.\n" ) ;
    return ;
  }
  
  const faceOfElem_struct *pFoE = pElT->faceOfElem + nFace ;
  const int *kVxFace = pFoE->kVxFace ;
  const int mDim = elemType[pElem->elType].mDim ;
  vrtx_struct **ppVx = pElem->PPvrtx ;
  const int mVxFc = pFoE->mVertsFace ;
  const vrtx_struct *pVx ;

  printf ( " elem %"FMT_ULG", face %d\n", pElem->number, nFace ) ;
  for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
    pVx = ppVx[ kVxFace[kVx] ] ;
    printf ( "   %d: nr %"FMT_ULG", ", kVx, pVx->number ) ;
    if ( pVx->Pcoor )
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	printf ( " %17.11e", pVx->Pcoor[nDim] ) ;
    else
      printf ( ", no coordinates" ) ;
    printf ( ".\n" ) ;
  }
  return ;
}

void printeg ( const elem_struct *Pelem, const int kEdge )
{
  const elemType_struct *PelT ;
  int kVx ;
  const int *PkVxEg ;

  if ( !Pelem ) {
    printf ( " Empty elem.\n" ) ;
    return ; }
  
  PelT = elemType + Pelem->elType ;
  PkVxEg = PelT->edgeOfElem[kEdge].kVxEdge ;

  if ( kEdge >= PelT->mEdges ) {
    printf ( " no such edge %d for a %s.\n", kEdge, PelT->name ) ;
    return ; }

  printf ( " el: %"FMT_ULG", edge %d: ", Pelem->number, kEdge ) ;
  for ( kVx = 0 ; kVx < 2 ; kVx++ )
    printf ( " %"FMT_ULG"", Pelem->PPvrtx[ PkVxEg[kVx] ]->number ) ;
  printf ( ".\n" ) ;

  return ;
}


void printegco ( const elem_struct *Pelem, const int kEdge )
{
  const elemType_struct *PelT ;
  int kVx, nDim ;
  int mDim ;
  const int *PkVxEg ;
  const vrtx_struct *Pvrtx ;

  if ( !Pelem ) {
    printf ( " Empty elem.\n" ) ;
    return ; }
  
  PelT = elemType + Pelem->elType ;
  mDim = PelT->mDim ;
  PkVxEg = PelT->edgeOfElem[kEdge].kVxEdge ;

  if ( kEdge >= PelT->mEdges ) {
    printf ( " no such edge %d for a %s.\n", kEdge, PelT->name ) ;
    return ; }

  printf ( " el: %"FMT_ULG", edge %d:\n", Pelem->number, kEdge ) ;
  for ( kVx = 0 ; kVx < 2 ; kVx++ ) {
    Pvrtx = Pelem->PPvrtx[ PkVxEg[kVx] ] ;
    printf ( "   %d: nr %"FMT_ULG", ", kVx, Pvrtx->number ) ;
    if ( Pvrtx->Pcoor )
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	printf ( " %f", Pvrtx->Pcoor[nDim] ) ;
    else
      printf ( ", no coordinates" ) ;
    printf ( ".\n" ) ;
  }

  return ;
}


vrtx_struct * findpvxco ( const uns_s *pUns, const int mDim, 
                          const double x, const double y, const double z ) {
  const chunk_struct *Pchunk ;
  vrtx_struct *Pvrtx, *pVxMin = NULL ;
  double dist, distMin = TOO_MUCH, coMin[MAX_DIM] ;

  coMin[0] = x ;
  coMin[1] = y ;
  coMin[2] = z ;
  
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pvrtx = Pchunk->Pvrtx+1 ; Pvrtx <= Pchunk->Pvrtx+Pchunk->mVerts ; Pvrtx++ )
      if ( !Pvrtx->invalid ) {
        dist = sq_distance_dbl( Pvrtx->Pcoor, coMin, mDim ) ;

        if ( dist < distMin ) {
          distMin = dist ;
          pVxMin = Pvrtx ;
        }
      }

  printvxco( pVxMin, mDim ) ;
  return ( pVxMin ) ;
}
  
vrtx_struct * findpvx ( const uns_s *pUns, const int nVx )
{
  const chunk_struct *Pchunk ;
  vrtx_struct *Pvrtx ;
  
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pvrtx = Pchunk->Pvrtx+1 ; Pvrtx <= Pchunk->Pvrtx+Pchunk->mVerts ; Pvrtx++ )
      if ( Pvrtx->number == nVx ) {
        printf ( " c/p %d/%d\n", Pchunk->nr, (int)(Pvrtx - Pchunk->Pvrtx) ) ;
	return ( Pvrtx ) ;
      }
  return ( NULL ) ;
}
  
void findelvxViz ( const uns_s *pUns, const int nVx, char *fileName, const int iZone ) {
  const chunk_struct *Pchunk ;
  const elem_struct *Pelem ;
  const elem_struct **ppElViz = NULL ;
  int kVx ;
  ulong_t mElViz = 0 ;

  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
    for ( Pelem = Pchunk->Pelem+1 ; Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->PPvrtx )
	for ( kVx = 0 ; kVx < elemType[ Pelem->elType ].mVerts ; kVx++ ) {
	  if ( Pelem->PPvrtx[kVx]->number == nVx ) {
            printf ( " p/c: %d/%d,", (int)(Pelem - Pchunk->Pelem), Pchunk->nr ) ;
	    printelal ( Pelem ) ;
            
            if ( fileName != NULL && ( iZone < 0 || Pelem->iZone == iZone ) )
              add_viz_el ( Pelem, &ppElViz, &mElViz ) ;
	  }
      }
  }
          

  if ( mElViz ) {
    viz_elems_vtk ( fileName, mElViz, ppElViz, NULL ) ;
    arr_free ( ppElViz ) ;
  }

  return ;
}
            

void findelvx ( const uns_s *pUns, const int nVx ) {
  findelvxViz ( pUns, nVx, NULL, -1 ) ;
  return ;
}

void findel2vx ( const uns_s *pUns, const int nVx1, const int nVx2 )
{
  const chunk_struct *Pchunk ;
  const elem_struct *Pelem ;
  int kVx, kkVx ;

  for ( Pchunk                   = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem                  = Pchunk->Pelem+1 ; Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->PPvrtx )
	for ( kVx                = 0 ; kVx < elemType[ Pelem->elType ].mVerts ; kVx++ )
	  if ( Pelem->PPvrtx[kVx]->number == nVx1 )
	    for ( kkVx           = 0 ; kkVx < elemType[ Pelem->elType ].mVerts ; kkVx++ )
	      if ( Pelem->PPvrtx[kkVx]->number == nVx2 ) 
                { printf ( " p/c: %d/%d,", (int)(Pelem - Pchunk->Pelem), Pchunk->nr ) ;
		printelal ( Pelem ) ;
	      }
  return ;
}

void findel3vx ( const uns_s *pUns,
		 const int nVx1, const int nVx2, const int nVx3 )
{
  const chunk_struct *Pchunk ;
  const elem_struct *Pelem ;
  int kVx, kkVx, kkkVx ;

  for ( Pchunk                   = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem                  = Pchunk->Pelem+1 ; Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->PPvrtx )
	for ( kVx                = 0 ; kVx < elemType[ Pelem->elType ].mVerts ; kVx++ )
	  if ( Pelem->PPvrtx[kVx]->number == nVx1 )
	    for ( kkVx           = 0 ; kkVx < elemType[ Pelem->elType ].mVerts ; kkVx++ )
	      if ( Pelem->PPvrtx[kkVx]->number == nVx2 )
		for ( kkkVx      = 0 ; kkkVx < elemType[ Pelem->elType ].mVerts ; kkkVx++ )
		  if ( Pelem->PPvrtx[kkkVx]->number == nVx3 ) {
		    printf ( " p/c: %d/%d,", (int)(Pelem - Pchunk->Pelem), Pchunk->nr ) ;
		    printelal ( Pelem ) ; }
  return ;
}

void findel4vx ( const uns_s *pUns,
		 const int nVx1, const int nVx2, 
                 const int nVx3, const int nVx4 ){
  const chunk_struct *Pchunk ;
  const elem_struct *Pelem ;
  int kVx, kkVx, kkkVx, kkkkVx ;

  for ( Pchunk                   = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem                  = Pchunk->Pelem+1 ; Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->PPvrtx )
	for ( kVx                = 0 ; kVx < elemType[ Pelem->elType ].mVerts ; kVx++ )
	  if ( Pelem->PPvrtx[kVx]->number == nVx1 )
	    for ( kkVx           = 0 ; kkVx < elemType[ Pelem->elType ].mVerts ; kkVx++ )
	      if ( Pelem->PPvrtx[kkVx]->number == nVx2 )
		for ( kkkVx      = 0 ; kkkVx < elemType[ Pelem->elType ].mVerts ; kkkVx++ )
		  if ( Pelem->PPvrtx[kkkVx]->number == nVx3 )
		    for ( kkkkVx = 0 ; kkkkVx < elemType[ Pelem->elType ].mVerts ;
			  kkkkVx++ )
		      if ( Pelem->PPvrtx[kkkkVx]->number == nVx4 ) {
			printf ( " p/c: %d/%d,", (int)(Pelem - Pchunk->Pelem), Pchunk->nr ) ;
			printelal ( Pelem ) ; }
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_bndFc_*:
*/
/*! Various debugging utility functions to find a boundary face 
 *  given either an element, or a single vx pointer, or a list of vx pointers
 *  or a set of individual node numbers.
 *
 */

/*
  
  Last update:
  ------------
  : 2Mar20; rationalised the naming, intro _nVx.
  
*/


void find_bndFc_el ( const uns_s *pUns, const elem_struct *Pelem ) {

  const chunk_struct *Pchunk ;
  const bndPatch_struct *PbndPatch ;
  const bndFc_struct *PbndFc ;
  
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( PbndPatch = Pchunk->PbndPatch+1 ;
	  PbndPatch <= Pchunk->PbndPatch+Pchunk->mBndPatches ; PbndPatch++ )
      for ( PbndFc = PbndPatch->PbndFc ;
	    PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ )
	/*if ( PbndFc > Pchunk->PbndFc && PbndFc <= Pchunk->PbndFc + Pchunk->mBndFaces )*/
	  if ( PbndFc && PbndFc->Pelem == Pelem ) {
            printf ( " chunk %d, patch %d, face %d in patch, %d in fclist, elem %d, face %d\n",
                     Pchunk->nr, 
                     (int)(PbndPatch - Pchunk->PbndPatch),
		     (int)(PbndFc - PbndPatch->PbndFc), (int)(PbndFc - Pchunk->PbndFc),
		     (int)(PbndFc->Pelem - Pchunk->Pelem), PbndFc->nFace ) ;
	  }
  return ;
}

bndFc_struct *find_bndFc_pVx ( uns_s *pUns, const vrtx_struct *Pvrtx, int nr,
                            int doPrint ) {
  
  const chunk_struct *Pchunk ;
  const bndPatch_struct *PbndPatch ;

  bndFc_struct  *PbndFc, *pBndFc = NULL ;
  int fcType, kVx ;
  vrtx_struct **PPvxFc[MAX_VX_FACE] ;

  if ( !nr && !Pvrtx )
    return ( NULL ) ;
  else if ( !nr )
    nr                           = Pvrtx->number ;
  
  for ( Pchunk                   = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( PbndPatch              = Pchunk->PbndPatch+1 ;
	  PbndPatch <= Pchunk->PbndPatch+Pchunk->mBndPatches ; PbndPatch++ )
      for ( PbndFc               = PbndPatch->PbndFc ;
	    PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ )
	/* Make sure PbndFc points to something valid.
	if ( PbndFc >= Pchunk->PbndFc && PbndFc < Pchunk->PbndFc + Pchunk->mBndFaces ) */
          if ( PbndFc->Pelem && PbndFc->nFace )
            if ( get_uns_face ( PbndFc->Pelem, PbndFc->nFace, PPvxFc, &fcType ) )
              for ( kVx          = 0 ; kVx < fcType ; kVx++ )
                if ( (*PPvxFc[kVx])->number == nr ) {
                  pBndFc         = PbndFc ;
                  if ( doPrint ) {
                    printf ( " chunk %d, patch %d, face %d/%d, elem %d, face %d\n",
                             Pchunk->nr, 
                             (int)(PbndPatch - Pchunk->PbndPatch),
                             (int)(PbndFc - PbndPatch->PbndFc), 
                             (int)(PbndFc - Pchunk->PbndFc),
                             (int)(PbndFc->Pelem - Pchunk->Pelem), 
                             (int)(PbndFc->nFace) ) ;
                    printfcco(PbndFc->Pelem,PbndFc->nFace ) ;
                  }
                }
  return ( pBndFc ) ;
}


/* Find a boundary face given a list of vertex pointers. */
bndFc_struct *find_bndFc_pVxList ( uns_s *pUns, const vrtx_struct *ppVx[], int mVx ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  vrtx_struct **ppVrtx ;
  
  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBfBeg, *pBfEnd, *pBf ;
  elem_struct *pElem ;

  int kVx, k, mVxFace ;

  pChunk                         = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBfBeg, &pBfEnd ) )
    for ( pBf                    = pBfBeg ; pBf <= pBfEnd ; pBf++ )
      if ( pBf->Pelem && pBf->nFace && !pBf->Pelem->invalid ) {

        pElem                    = pBf->Pelem ;
        ppVrtx                   = pElem->PPvrtx ;
        pElT                     = elemType + pElem->elType ;
        pFoE                     = pElT->faceOfElem + pBf->nFace ;
        kVxFace                  = pFoE->kVxFace ;
        mVxFace                  = pFoE->mVertsFace ;

        /* Loop over all vertices  to match. */
        for ( k                  = 0 ; k < mVx ; k++ ) {
          for ( kVx              = 0 ; kVx < mVxFace ; kVx++ )
            if ( ppVrtx[ kVxFace[kVx] ] == ppVx[k] )
              break ;
          if ( kVx == mVxFace ) {
            /* Couldn't match this one. */
            k                    = mVx+1 ;
            break ;
          }
        }
           
        if ( k == mVx )      
          return ( pBf ) ;
      }

  return ( NULL ) ;
}


/* Find a boundary face given a list of vertices. */
bndFc_struct *find_bndFc_nVx ( uns_s *pUns,
                               const int nVx0,
                               const int nVx1,
                               const int nVx2,
                               const int nVx3 ) {

  int nVx[4] ;
  nVx[0] = nVx0 ;
  nVx[1] = nVx1 ;
  nVx[2] = nVx2 ;
  nVx[3] = nVx3 ;
  
  
  
  int kVx ;
  int mVx = 0 ;
  for ( kVx = 0 ; kVx < 4 ; kVx ++ ) {
    if ( nVx[kVx] )
      mVx++ ;
    else
      break ;
  }
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBfBeg, *pBfEnd, *pBf ;
  const elem_struct *pElem ;
  vrtx_struct **ppVrtx ;
  int k, mVxFace ;

  chunk_struct *pChunk                         = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBfBeg, &pBfEnd ) )
    for ( pBf                    = pBfBeg ; pBf <= pBfEnd ; pBf++ )
      if ( pBf->Pelem && pBf->nFace && !pBf->Pelem->invalid ) {

        pElem                    = pBf->Pelem ;
        ppVrtx                   = pElem->PPvrtx ;
        pElT                     = elemType + pElem->elType ;
        pFoE                     = pElT->faceOfElem + pBf->nFace ;
        kVxFace                  = pFoE->kVxFace ;
        mVxFace                  = pFoE->mVertsFace ;

        /* Loop over all vertices  to match. */
        for ( k                  = 0 ; k < mVx ; k++ ) {
          for ( kVx              = 0 ; kVx < mVxFace ; kVx++ )
            if ( ppVrtx[ kVxFace[kVx] ]->number  == nVx[k] )
              break ;
          if ( kVx == mVxFace ) {
            /* Couldn't match this one. */
            k                    = mVx+1 ;
            break ;
          }
        }
           
        if ( k == mVx )      
          return ( pBf ) ;
      }

  return ( NULL ) ;
}

/******************************************************************************

*/

int add_viz_el ( const elem_struct *pElem, const elem_struct ***pppElViz, ulong_t *pmVizEl ) {

  *pppElViz                      = arr_realloc ( "pppElViz in add_viz_el", NULL, *pppElViz,
                            ++(*pmVizEl), sizeof( **pppElViz ) ) ;

  (*pppElViz)[*pmVizEl-1]        = pElem ;

  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  elType2vtk:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int elType2vtk ( const elType_e elT ) {
#undef FUNLOC
#define FUNLOC "in elType2vtk"
  
  int vtk_type=0 ;

  switch ( elT ) {
  case noEl :
    break ;
  case bi :
    break ;
  case tri :
    vtk_type                     = 5 ;
    break ;
  case qua :
    vtk_type                     = 7 ;
    break ;
  case tet :
    vtk_type                     = 10 ;
    break ;
  case pyr :
    vtk_type                     = 14 ;
    break ;
  case pri :
    vtk_type                     = 13 ;
    break ;
  case hex :
    vtk_type                     = 12 ;
  }

  return ( vtk_type ) ;
}


/******************************************************************************

  viz_one_elem_vtk:
  write a single element to vtk.
  http://dunne.uni-hd.de/VisuSimple/documents/vtkfileformat.html
  
  Last update:
  ------------
  15Feb16: conceived.
  
  Input:
  ------
  pElem
  
*/

void viz_one_elem_vtk ( char *fileName, const elem_struct *pElem, const double *pCoor ) {

  const elemType_struct *pElT    = elemType + pElem->elType ;
  const int mVx                  = pElT->mVerts ;
  int kVx ;
  char flnm[LINE_LEN] ;
  if ( fileName )
    strcpy ( flnm, fileName ) ;
  else 
    sprintf ( flnm, "oneElem.vtk" ) ;
  prepend_path ( flnm ) ;

  FILE *fvtk                     = fopen ( flnm, "w" ) ;
  if ( !fvtk ) return ;

  /* Header. */
  fprintf ( fvtk, 
            "# vtk DataFile Version 3.1\n"
            "single element extract using hip::viz_one_elem_vtk.\n"
            "ASCII\n"
            "DATASET UNSTRUCTURED_GRID\n\n" ) ; 


  /* Coors. */
  int mVxAdd                     = ( pCoor ? 1 : 0 ) ;
  fprintf ( fvtk,
            "POINTS %d FLOAT\n", mVx+mVxAdd ) ;
  vrtx_struct **ppVx             = pElem->PPvrtx ;
  const vrtx_struct *pVx ;
  const int mDim                 = pElT->mDim ;

  for ( kVx                      = 0 ; kVx < mVx ; kVx++ ) {
    pVx                          = ppVx[ kVx ] ;
    fprintf ( fvtk, "%15.11g ", pVx->Pcoor[0] ) ;
    fprintf ( fvtk, "%15.11g ", pVx->Pcoor[1] ) ;
    fprintf ( fvtk, "%15.11g\n", ( mDim == 3 ?  pVx->Pcoor[2] : 0. ) ) ; 
  }


  if ( pCoor ) {
    fprintf ( fvtk, "%15.11g ", pCoor[0] ) ;
    fprintf ( fvtk, "%15.11g ", pCoor[1] ) ;
    fprintf ( fvtk, "%15.11g\n", ( mDim == 3 ?  pCoor[2] : 0. ) ) ; 
  }
  fprintf ( fvtk, "\n" ) ;


  /* Elem. */
  fprintf ( fvtk,
            "CELLS %d %d\n", 1, mVx+1 ) ;
  fprintf ( fvtk, "%d ", mVx ) ;
  const int *kVxMap, kVxPri[]    = {0,5,3,1,4,2}, kVxOth[]={0,1,2,3,4,5,6,7} ;
  kVxMap                         = ( pElem->elType == pri ? kVxPri : kVxOth ) ;
  for ( kVx                      = 0 ; kVx < mVx ; kVx++ )
    fprintf ( fvtk, "%d ", kVxMap[kVx] ) ;
  fprintf ( fvtk, "\n\n" ) ;

  fprintf ( fvtk,
            "CELL_TYPES 1\n" ) ;
  int vtk_type = elType2vtk ( pElem->elType ) ;
  fprintf ( fvtk, "%d\n", vtk_type ) ;
  fprintf ( fvtk, "\n" ) ;


  /* data. Use a value of 0 for the vertics, 1 for the extra point if any. */
  fprintf ( fvtk,
            "POINT_DATA %d\n", mVx+mVxAdd ) ;

  fprintf ( fvtk,
            "SCALARS outside_elem FLOAT\n"
            "LOOKUP_TABLE default\n" ) ;
  for ( kVx                      = 0 ; kVx < mVx ; kVx++ )
    fprintf ( fvtk, "0\n" ) ;
  if ( pCoor )
    fprintf ( fvtk, "1\n" ) ;

  fprintf ( fvtk,
            "SCALARS node_number FLOAT\n"
            "LOOKUP_TABLE default\n" ) ;
  for ( kVx                      = 0 ; kVx < mVx ; kVx++ )
    fprintf ( fvtk, "%"FMT_ULG"\n", ppVx[kVx]->number ) ;
  if ( pCoor )
    fprintf ( fvtk, "0\n" ) ;

  fclose ( fvtk ) ;
  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  viz_elems_vtk:
*/
/*! write a vtk file for a list of elements.
 *
 *
 */

/*
  
  Last update:
  ------------
  3Sep19: derived from viz_one_elem_vtk
  

  Input:
  ------
  fileName: name of vtk file
  mEls: number of elements
  ppElem: list of elements
  pCoor: one reference point for a glyph

  
*/
void viz_elems_vtk ( char *fileName, 
                    int mEls, const elem_struct **ppElem, const double *pCoor ) {
  viz_mgElems_vtk ( fileName, mEls, ppElem, NULL, NULL, pCoor, 0 ) ; 
}
void viz_elems_vtk0 ( char *fileName, 
                    int mEls, const elem_struct **ppElem, const double *pCoor ) {
  
  /* Open file. */
  char flnm[LINE_LEN] ;  
  if ( fileName )
    strcpy ( flnm, fileName ) ;
  else 
    sprintf ( flnm, "someElems.vtk" ) ;
  prepend_path ( flnm ) ;

  FILE *fvtk                     = fopen ( flnm, "w" ) ;
  if ( !fvtk ) return ;

  /* Header. */
  fprintf ( fvtk, 
            "# vtk DataFile Version 3.1\n"
            "multiple element extract using hip::viz_one_elem_vtk.\n"
            "ASCII\n"
            "DATASET UNSTRUCTURED_GRID\n\n" ) ; 


  /* Coors. */
  const elemType_struct *pElT = NULL ;
  int mVx=0 ;
  int nEl ;
  for ( nEl = 0 ; nEl < mEls ; nEl++ ) { 
    pElT = elemType + ppElem[nEl]->elType ;
    mVx += pElT->mVerts ;
  }
  int mVxAdd = ( pCoor ? 1 : 0 ) ;
  fprintf ( fvtk,
            "POINTS %d FLOAT\n", mVx+mVxAdd ) ;
  vrtx_struct **ppVx ;
  const vrtx_struct *pVx ;
  const int mDim = pElT->mDim ;
  const elem_struct *pEl ;
  int kVx ;
  for ( nEl = 0 ; nEl < mEls ; nEl++ ) {
    pEl = ppElem[nEl] ;
    pElT = elemType + pEl->elType ;
    ppVx = pEl->PPvrtx ;
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
      pVx = ppVx[ kVx ] ;
      fprintf ( fvtk, "%15.11g ", pVx->Pcoor[0] ) ;
      fprintf ( fvtk, "%15.11g ", pVx->Pcoor[1] ) ;
      fprintf ( fvtk, "%15.11g\n", ( mDim == 3 ?  pVx->Pcoor[2] : 0. ) ) ; 
    }
  }


  if ( pCoor ) {
    fprintf ( fvtk, "%15.11g ", pCoor[0] ) ;
    fprintf ( fvtk, "%15.11g ", pCoor[1] ) ;
    fprintf ( fvtk, "%15.11g\n", ( mDim == 3 ?  pCoor[2] : 0. ) ) ; 
  }
  fprintf ( fvtk, "\n" ) ;


  /* Elems. */
  fprintf ( fvtk,
            "CELLS %d %d\n", mEls, mVx+mEls ) ;// no of cells, total no of conn entries
  const int *kVxMap, kVxPri[] = {0,5,3,1,4,2}, kVxOth[]={0,1,2,3,4,5,6,7} ;
  int mVxWritten = 0 ;
  for ( nEl = 0 ; nEl < mEls ; nEl++ ) {
    pEl = ppElem[nEl] ;
    pElT = elemType + pEl->elType ;
    fprintf ( fvtk, "%d ", pElT->mVerts ) ;
    kVxMap = ( pEl->elType == pri ? kVxPri : kVxOth ) ;
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
      fprintf ( fvtk, "%d ", mVxWritten+kVxMap[kVx] ) ;
    }
    mVxWritten += pElT->mVerts ;
    fprintf ( fvtk, "\n" ) ;
  }
  fprintf ( fvtk, "\n" ) ;

  fprintf ( fvtk,
            "CELL_TYPES %d\n", mEls ) ;
  int vtk_type ;
  for ( nEl = 0 ; nEl < mEls ; nEl++ ) {
    switch ( ppElem[nEl]->elType ) {
    case noEl :
      break ;
    case bi :
      break ;
    case tri :
      vtk_type                     = 5 ;
      break ;
    case qua :
      vtk_type                     = 7 ;
      break ;
    case tet :
      vtk_type                     = 10 ;
      break ;
    case pyr :
      vtk_type                     = 14 ;
      break ;
    case pri :
      vtk_type                     = 13 ;
      break ;
    case hex :
      vtk_type                     = 12 ;
    }
    fprintf ( fvtk, "%d\n", vtk_type ) ;
  }
  fprintf ( fvtk, "\n" ) ;


  /* data. Use a value of 0 for the vertices, 1 for the extra point if any. */
  fprintf ( fvtk,
            "POINT_DATA %d\n", mVx+mVxAdd ) ;

  fprintf ( fvtk,
            "SCALARS outside_elem FLOAT\n"
            "LOOKUP_TABLE default\n" ) ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    fprintf ( fvtk, "0\n" ) ;
  if ( pCoor )
    fprintf ( fvtk, "1\n" ) ;

  fprintf ( fvtk,
            "SCALARS node_number FLOAT\n"
            "LOOKUP_TABLE default\n" ) ;
  for ( nEl = 0 ; nEl < mEls ; nEl++ ) {
    pEl = ppElem[nEl] ;
    pElT = elemType + pEl->elType ;
    kVxMap = ( pEl->elType == pri ? kVxPri : kVxOth ) ;
    ppVx = pEl->PPvrtx ;
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
      fprintf ( fvtk, "%"FMT_ULG"\n", ppVx[kVx]->number ) ;
    }
  }
  if ( pCoor )
    fprintf ( fvtk, "0\n" ) ;

  fclose ( fvtk ) ;
  return ;

}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  vis_elems:
*/
/*! currently: plot the elements with edges < h to a vtk file.
 *
 */

/*
  
  Last update:
  ------------
  2Sep21: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s vis_args ( char argLine[], 
                 ep_type *pepType, int *pIneqSign, double *pthrVal,
                 char *fileName ) {
#undef FUNLOC
#define FUNLOC "in vis_args"

  ret_s ret = ret_success () ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  

  /* Parse line of unix-style optional args. */
  char c ;
  double mVal = 0., tVal = 0. ;
  strcpy (fileName,"elems_prop.vtk") ;
  char ftype = 'm' ;
  char propName[LINE_LEN] = "volMin" ;
  while ((c = getopt_long ( mArgs, ppArgs, "p:t:m:f:",NULL,NULL)) != -1) {
    switch (c)  {
    case 'm':
      if ( optarg ) {
        mVal = atof( optarg ) ;
        ftype = 'm' ;
      }
      else
        hip_err ( warning, 1, "-m argument needs a value, using 1.1." ) ;
      break ;
      
    case 'p':
      if ( optarg ) {
        strcpy ( propName, optarg ) ;
        tolowerstr( propName ) ;
      }
      else
        hip_err ( warning, 1, "-p argument needs an argument, using volMin." ) ;
      break ;
      
    case 't':
      if ( optarg ) {
        tVal = atof( optarg ) ;
        ftype = 't' ;
      }
      else
        hip_err ( warning, 1, "-t argument needs a value, ignored." ) ;
      break ;
      
    case 'f':
      if ( optarg )
        strcpy ( fileName, optarg ) ;
      else
        hip_err ( warning, 1, "-f argument needs a value, using elems_prop.vtk." ) ;
      break ;
    }
  }

  
  /* What type of property to select on? */
  if ( !strncmp( propName, "hmin", 3 ) ) {
    *pepType = ep_hMin ;
    *pIneqSign = 1 ;
  }
  else if ( !strncmp( propName, "volmin", 5 ) ) {
    *pepType = ep_volMin ;
    *pIneqSign = 1 ;
  }
  else if ( !strncmp( propName, "maxangle", 5 ) ) {
    *pepType = ep_angMax ;
    *pIneqSign = -1 ;
  }
  else {
    hip_err ( warning, 1, "unknown element property "FUNLOC". Ignored.\n" ) ;
    ret.status = warning ;
    return (ret) ;
  }


  /* Threshold? */
  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  if ( ftype == 't' ) {
    /* tVal is the threshold. */
    switch ( *pepType ) {
    case ep_angMax :
      *pthrVal = 1./tVal ; // max
      break ;
    default:
      *pthrVal = tVal ;
    }
  }
  
  else if ( ftype == 'm' ) {
    /* mVal multiplies a reference value of the property. */
    switch ( *pepType ) {
    case ep_none:
      hip_err ( fatal, 0, "ep_none shouldn't have happened in "FUNLOC"." ) ;
      break ;
    case ep_hMin:
      *pthrVal = mVal*pUns->hMin ;
      break ;
    case ep_volMin :
      *pthrVal = mVal*pUns->volElemMin ;
      break ;
    case ep_angMax :
      *pthrVal = 1./mVal*check_angles( pUns, 0 ) ;
    }
  }
  else {
    hip_err ( warning, 1, "unknown filter operation "FUNLOC". Ignored.\n" ) ;
    ret.status = warning ;
  }

  return (ret) ;
}




ret_s vis_elems ( char *argLine ) {
#undef FUNLOC
#define FUNLOC "in vis_elems"
  ret_s ret = ret_success () ;

  ep_type epType = ep_none ;
  int ineqSign = 1 ; // positive for min, negative for max
  double thrVal = 0. ;
  char fileName[LINE_LEN] = "\0" ;
  ret = vis_args ( argLine, &epType, &ineqSign, &thrVal, fileName ) ;
  
  if ( ret.status == fatal )
    return ( ret ) ;
  
  ulong_t mElViz = 0 ;
  const elem_struct **ppElViz = NULL ;

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pElBeg,*pElEnd, *pElem ;
  double hMinSq, hMaxSq, dist ;
  const int fix_degenEdges = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        if ( ineqSign*calc_elem_property ( pElem, epType ) < ineqSign*thrVal ) {
          /* Add this element. */
          add_viz_el ( pElem, &ppElViz, &mElViz ) ;
        }
      }



  if ( mElViz ) {
    viz_elems_vtk ( fileName, mElViz, ppElViz, NULL ) ;
    arr_free ( ppElViz ) ;
  }
  
  return ( ret ) ;
}

