/*
   uns_meth.c
   Unstructured grid methods.
 
   Last update:
   ------------
   21Apr20; move mark functions to uns_mark.c
   30Apr19; include proto_adapt.h.
   8Sep17; intro number_uns_grid_zones, number_uns_elem_in_zones.
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   18Dec10; new pBc->type
   20Jul06; intro find_nBc
   9Apr3; intro reset_chk_vx_per.
   12Jan01; intro reset_elem_mark.
   11Jun99; allow numbering on parents in number_uns_elemFromVerts.
   11Feb98; add periodic siblings to non-periodic patches in mark_uns_vertBc.
   22Dec97; cut out of uns_meth.c

   Contains:
   ---------
   number_uns_grid:
   validate_elem_onPvx:
   increment_uns_elem_number:
   number_uns_elem_leafs:
   increment_uns_vert_number:
   mark_uns_vertFromElem:
   mark_uns_vertBc:
   list_vert_bc ;
   count_uns_bndFc_chk:
   reset_chk_vx_mark
   count_uns_bndFaces
   rm_perBc
   
*/
#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

//#ifdef ADAPT_HIERARCHIC
#include "cpre_adapt.h"
#include "proto_adapt.h"
//#endif

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern const int singleBndVxNormal ;

/******************************************************************************

  Copyright Jens-Dominik Mueller and CERFACS, 
  see the CECILL copyright notice at the top of the file.
  Author: Jens-Dominik Mueller
  contributor(s) : Gabriel Staffelbach

  "Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
  "Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fill_vx_nr2:
*/
/*! Make a list of global node numbers before renumbering locally.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  20Dec16: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void fill_chunk_vrtxNr2 ( uns_s *pUns ) {

  /* Alloc a number field for each chunk. Fill that field with the current
     node numbering. Check cptVx pointers are correct for each vx. */
  chunk_struct *pChunk = NULL ;
  ulong_t *pNr2 ;
  vrtx_struct *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  vrtx_struct *pVx ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
    pNr2 = pChunk->pVrtxNr2 = arr_malloc ( "pNr2 in fill_vx_nr2", pUns->pFam, 
                                           pChunk->mVerts+1, sizeof(*(pChunk->pVrtxNr2) ) ) ;

    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ ) {
      if ( pVx->vxCpt.nCh != pChunk->nr || pChunk->Pvrtx + pVx->vxCpt.nr != pVx )
        hip_err ( fatal, 0, "erroneous cptVx in fill_vx_nr2." ) ;

      *pNr2++ = pVx->number ;
    }
  }
  
  return ;
}

/******************************************************************************

  Copyright Jens-Dominik Mueller and CERFACS, 
  see the CECILL copyright notice at the top of the file.
  Author: Jens-Dominik Mueller
  contributor(s) : Gabriel Staffelbach

  "Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
  "Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  free_vx_nr2:
*/
/*! Deallocate pNr2 number field with every chunk.
 */

/*
  
  Last update:
  ------------
  20Dec176: conceived.
  

  Input:
  ------
  pUns

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void free_chunk_vrtxNr2 ( uns_s *pUns ) {
  
  chunk_struct *pChunk = NULL ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    if ( pChunk->pVrtxNr2 ) {
      arr_free ( pChunk->pVrtxNr2 ) ;
      pChunk->pVrtxNr2 = NULL ;
    }
  }

  return ;
}



/******************************************************************************

  number_uns_grid_types:
  Number all valid elements, either by type, or in memory order, 
  mark all vertices used by these elements, number all
  elements, number all vertices, count and link the boundary faces.
  
  Last update:
  ------------
  17Apr20; test for adapt hier looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  19Dec16; track pUns->numberedType
  18Dec16; re-introduce ordering by memory, so that loops travers elements in order.
  21Sep16; new interface to increment_uns_vert_number
  10Sep16; extracted from number_uns_grid
  30Nov99; don't call special_verts.
  8Mar97: cut out of check_uns.
  
  Input:
  ------
  pUns
  elTBeg, elTEnd: element types to include in the renumbering. If either is 'noEl',
                  then ordering by memory location (chronolgical) is done.
  useNumber: base vertex use on element number, rather than validty flag.
  doReset: if non-zero, restart vertex, numbering, otherwise continue from existing.
  doBound: count bnd faces and rebuild bnd face data structures.

  Changes To:
  -----------
  pUns
  
*/

void number_uns_grid_types ( uns_s *pUns, elType_e elTBeg, elType_e elTEnd,
                             const int useNumber, const int doReset, const int doBound ) {
  
  /* Number all leafs. */
  /* Check for suitable range, translate noEl to correct bound. */
  if ( elTBeg == noEl || elTEnd == noEl ) {
    number_uns_elem_leafs ( pUns ) ;
    pUns->numberedType = leaf ;
  }
  else {
    number_uns_elems_by_type ( pUns, leaf, elTBeg, elTEnd, doReset ) ;
    pUns->numberedType = elTypes ;
  }
  
  count_uns_elems_of_type ( pUns ) ;
  
  /* Mark all used vertices and renumber. */
  validate_uns_vertFromElem ( pUns, useNumber ) ;
  #ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge )
    mark_uns_vertFromAdEdge ( pUns ) ;
  #endif
  increment_uns_vert_number ( pUns, doReset ) ;

  if ( doBound ) {
    /* Count all boundary faces, link all boundary patches. */
    count_uns_bndFc_chk ( pUns ) ; // Only used in uns2D_uns3D. Needs correct PbndPatch structure.
    make_uns_ppChunk ( pUns ) ;
    make_uns_ppBc ( pUns ) ;
    count_uns_bndFaces ( pUns ) ;
  }
  
  return ;
}




/******************************************************************************

  number_uns_grid_elem_region:
  Renumber a grid chrono, but only over selected zones.
  
  Last update:
  ------------
  25Dec21; rename to elem_region.
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  8Sep17; derived from number_uns_grid_zones.
  
  Input:
  ------
  pUns: grid
  mZones: number of zones, sign to include inclusion or exclusion, if zero include all.
  iZone: list of zones to be included (positive mZones, or excluded
  useNumber: base vertex use on element number, rather than validty flag.
  doReset: if non-zero, restart vertex, numbering, otherwise continue from existing.
  doBound: count bnd faces and rebuild bnd face data structures.

  Changes To:
  -----------
  pUns
  
*/

void number_uns_grid_elem_regions
( uns_s *pUns, const int mReg, const int iReg[], 
  const int useNumber, const int doReset, const int doBound,
  const int useMark ) {

#undef FUNLOC
#define FUNLOC "in number_uns_grid_elem_region"


  number_uns_elems_in_regions ( pUns, leaf, mReg, iReg, doReset, useMark ) ;
  pUns->numberedType = elTypes ;

  
  count_uns_elems_of_type ( pUns ) ;
  
  /* 'Validate' all used vertices (uses validate flag, 
     rather than marks or vx no) and renumber. */
  ulong_t mVxVal = validate_uns_vertFromElem ( pUns, useNumber ) ;
#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge )
    mark_uns_vertFromAdEdge ( pUns ) ;
#endif
  increment_uns_vert_number ( pUns, doReset ) ;

  if ( doBound ) {
    /* Count all boundary faces, link all boundary patches. */
    count_uns_bndFc_chk ( pUns ) ;
    make_uns_ppChunk ( pUns ) ;
    make_uns_ppBc ( pUns ) ;
    count_uns_bndFaces ( pUns ) ;
  }
  
  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  number_uns_grid_regions_zones:
*/
/*! renumber grid in particular regions and zones.
 *
 *
 */

/*
  
  Last update:
  ------------
  1Mar22: conceived.
  

  Input:
  ------
  mReg, iReg: number of and list of regions to include. If 0, include all.
  mZones, iZone: number of and list of zones to include. If 0, include all.
  useNumber: base vertex use on element number, rather than validty flag.
  doReset: if non-zero, restart vertex, numbering, otherwise continue from existing.
  doBound: count bnd faces and rebuild bnd face data structures.

  Changes To:
  -----------
  pUns: grid: renumbered

  Output:
  -------
  pmConn
    
  Returns:
  --------
  ret
  
*/

ulong_t number_uns_grid_regions_zones
( uns_s *pUns,
  const int mReg, const int iReg[],
  const int mZones, const int iZone[],
  const int useNumber, const int doReset, const int doBound,
  ulong_t *pmConn) {
  
#undef FUNLOC
#define FUNLOC "in number_uns_grid_regions_zones"
  
  ret_s ret = ret_success () ;
  match_s match ;
  init_match( &match ) ;
  int i ;
  if ( mReg ) {
    match.matchMarks = mReg ;
    for ( i = 0 ; i < mReg ; i++ )
      match.kMark2Match = match.kMark2Match | 1 << iReg[i] ;
    match.kMark2NotMatch = ~match.kMark2Match ;
  }

  if ( mZones ) {
    match.matchZone = 1 ;
    match.mZones = mZones ;
    for ( i = 0 ; i < mZones ; i++ )
      match.iZone[i] = iZone[i] ;
  }
  
  ulong_t mElems = number_uns_grid_match ( pUns, &match, useNumber,doReset, doBound, pmConn ) ;

  return ( mElems ) ;
}



/******************************************************************************

  number_uns_grid_match:
  Renumber a grid including only elements that match a pattern.
  
  Last update:
  ------------
  6May20; derived from number_uns_grid_zones.
  
  Input:
  ------
  pUns: grid
  pMatch: match specification
  useNumber: base vertex use on element number, rather than validty flag.
  doReset: if non-zero, restart vertex, numbering, otherwise continue from existing.
  doBound: count bnd faces and rebuild bnd face data structures.

  Changes To:
  -----------
  pmConn: number of connetivity entries of numbered elements.

  Returns:
  --------
  number of numbered elements.
  
*/

ulong_t number_uns_grid_match
( uns_s *pUns, const match_s *pMatch,
  const int useNumber, const int doReset, const int doBound,
  ulong_t *pmConn ) {

  ulong_t mConn ;
  ulong_t mElemsNumbered = number_uns_elems_match ( pUns, pMatch, doReset, &mConn ) ;
  pUns->numberedType = elTypes ;

  
  count_uns_elems_of_type ( pUns ) ;
  
  /* 'Validate' all used vertices (uses validate flag, 
     rather than marks or vx no) and renumber. */
  validate_uns_vertFromElem ( pUns, useNumber ) ;
#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge )
    mark_uns_vertFromAdEdge ( pUns ) ;
#endif
  increment_uns_vert_number ( pUns, doReset ) ;

  if ( doBound ) {
    /* Count all boundary faces, link all boundary patches. */
    count_uns_bndFc_chk ( pUns ) ;
    make_uns_ppChunk ( pUns ) ;
    make_uns_ppBc ( pUns ) ;
    count_uns_bndFaces ( pUns ) ;
  }
  
  return ( mElemsNumbered ) ;
}




/******************************************************************************

  number_uns_grid:
  Mark all used elements, mark all vertices used by these elements, number all
  elements by type, number all vertices, count and link the boundary faces.
  
  Last update:
  ------------
  10Sep16; move content to number_uns_grid_types, call that for all types.
  30Nov99; don't call special_verts.
  8Mar97: cut out of check_uns.
  
  Changes To:
  -----------
  pUns
  
*/

void number_uns_grid ( uns_s *pUns ) {
  const int dontUseNumber = 0 ;
  const int doReset = 1 ;
  const int doBound = 1 ;
  number_uns_grid_types ( pUns, tri, hex, dontUseNumber, doReset, doBound ) ;
}

/******************************************************************************

  number_uns_grid_leafs:
  Mark all used elements, mark all vertices used by these elements, number all leaf
  elements in memory order, number all vertices, count and link the boundary faces.
  
  Last update:
  ------------
  10Sep16; move content to number_uns_grid_types, call that for all types.
  30Nov99; don't call special_verts.
  8Mar97: cut out of check_uns.
  
  Changes To:
  -----------
  pUns
  
*/

void number_uns_grid_leafs ( uns_s *pUns ) {
  const int dontUseNumber = 0 ;
  const int doReset = 1 ;
  const int doBound = 1 ;
  number_uns_grid_types ( pUns, noEl, noEl, dontUseNumber, doReset, doBound ) ;
}


/******************************************************************************

  mark_uns_elem_onPvx:
  Loop over all elements and mark all that have a valid first vertex pointer.
  
  Last update:
  ------------
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  6Jan15; fix bug with missing bracket wrapping around ifdef for ->leaf=0.
  9Jul11; use ADAPT_REF.
  4May96: conceived.
  
  Input:
  ------
  pUns
  
  Changes to:
  ----------k
  pChunk->Pvrtx->nr
  
*/

void validate_elem_onPvx ( uns_s *pUns ) {
  
  elem_struct *pElem ;
  chunk_struct *pChunk ;

  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) 
    /* Loop over all cells. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) {
      if ( pElem->invalid )
	;
      else if ( !pElem->PPvrtx )
	/* Not even a vertex pointer. Phew! */
	pElem->invalid = 1 ;
      else if ( !pElem->PPvrtx[0] )
	/* Unphysical cells have at least their first vertex pointer voided. */
	pElem->invalid = 1 ;
      else
	pElem->invalid = 0 ;

      if ( pElem->invalid ) {
	pElem->term = 0 ;
        #ifdef ADAPT_HIERARCHIC
        if ( pUns->pllAdEdge )
          pElem->root = pElem->leaf = 0 ;
        #endif
      }
    }

  return ;

}
 
/******************************************************************************

  number_uns_elem_leafs:
  Loop over all elements and number all leafs. Reset PmElemTotal
  
  Last update:
  ------------
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  4May96: conceived.
  
  Input:
  ------
  pUns
  
  Changes to:
  ----------
  pChunk->Pvrtx->nr
  
*/

void number_uns_elem_leafs ( uns_s *pUns ) {
  
  elem_struct *pElem ;
  chunk_struct *pChunk ;
  int mElemsBeforeThisChunk ;

  pUns->mElemsNumbered = 0 ;

  //#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge )
    pUns->numberedType = leaf ;
  //#endif
  
  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    pChunk->mElemsNumbered = 0 ;
    mElemsBeforeThisChunk = pUns->mElemsNumbered ;
    /* Loop over all cells. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if (
#ifdef ADAPT_HIERARCHIC
          ( pUns->pllAdEdge ? pElem->leaf : !pElem->invalid )
#else
          !pElem->invalid
#endif
          ) 
        pElem->number = ++pUns->mElemsNumbered ;
      else
        pElem->number = 0 ;

    pChunk->mElemsNumbered += pUns->mElemsNumbered - mElemsBeforeThisChunk ;
  }
  return ;
}

/******************************************************************************
  number_uns_elems_by_type:   */

/*! Number base elements by number of vertices, first all tris, quas ..
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  21Jul20; disable call of number_uns_elemFromVerts_adapt, use elem_matches.
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  8May19; remove setting of markdEdges.
  18Dec16; remove noEl check, this now indicates non-type, but memory ordering 
  22Oct16; align with number_uns_elem_leafs, 
           use pChunk->elemsNumbered, rather elemsMarked.
  12Sep16; renamed from number_uns_elemsFromVerts
  10Sep16; introduce elTypeBeg, elTypeEnd.
  15Dec11; fix bad usage, the original version did not actually renumber in sequence.
  9Jul11: cut out of adapt_meth.
  

  Input:
  ------
  pUns
  nrType: leaf, parent, child ..., only relevant in adapted grids with hanging nodes.
  elTypeBeg, elTypeEnd: if non-zero, loop only over those types.
  doReset: if true reset numbering and counters even with partial range of elems.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int number_uns_elems_by_type ( uns_s *pUns, numberedType_e nrType,
                               elType_e elTypeBeg, elType_e elTypeEnd,
                               const int doReset ) {

   int iType ;
  chunk_struct *Pchunk ;
  elem_struct *pElem ;
  if ( doReset || ( elTypeBeg == tri && elTypeBeg == hex ) ) {
    /* Full range of elments, Reset all counters. */
    pUns->mElemsNumbered = 0 ;
    for ( iType = 0 ; iType < MAX_ELEM_TYPES ; iType++ )
      pUns->mElemsOfType[iType] = 0 ;

    for ( iType = 0 ; iType < MAX_DRV_ELEM_TYPES ; iType++ )
      pUns->mElems_w_mVerts[iType] = 0 ;

    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
      Pchunk->mElemsNumbered = 0 ;
      for ( pElem = Pchunk->Pelem+1 ; 
            pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ ) {
        pElem->number = 0 ;
      }
    }
  }



    /* Currently no support to  write grids with hanging edges, 
       but buffered grids can, and they still have hanging edges.
#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge )
    return (  number_uns_elemFromVerts_adapt ( pUns, nrType ) ) ;
#endif
    */



  

  elType_e elType ;
  match_s match ;
  init_match ( &match ) ;
  match.matchElType = 1 ;

  /* Just base elements. */
  for ( elType = elTypeBeg ; elType <= elTypeEnd ; elType++ ) {
    match.elTypeBeg = match.elTypeEnd = elType ;
    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
      /* Loop over all cells. */
      for ( pElem = Pchunk->Pelem+1 ;
            pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
        if ( pElem->elType == elType &&
             !pElem->invalid &&
             !pElem->number &&
             elem_matches ( pElem, &match )
             ) {
          /* JDM, May 19; why alter markdEdges here?
             pElem->markdEdges = pElem->elType ; */
          pElem->number = ++pUns->mElemsNumbered ;
          ++pUns->mElemsOfType[ pElem->elType ] ;
          // First slots of _w_mVerts are base elements.
          ++pUns->mElems_w_mVerts[ pElem->elType ] ; 
          ++Pchunk->mElemsNumbered ;
        }
    }
  }


  return ( 0 ) ;
}




/******************************************************************************
  number_uns_elems_in_regions:   */

/*! Number base elements by zoning or elem marks
 *
 *
 */

/*
  
  Last update:
  ------------
  25Dec21; convert to region
  17Apr20; test for adapt hierarchic by checking pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  7May19; remove setting of markdEdges.
  9Jul18; use renamed zone_match_list.
  8Sep17; derived from number_uns_elems_by_type  

  Input:
  ------
  pUns
  nrType: leaf, parent, child ..., only relevant in adapted grids with hanging nodes.
  mReg: number of regions, sign to include inclusion or exclusion, if zero include all.
  iReg: list of zones to be included (positive mZones, or excluded
  doReset: if true reset numbering and counters even with partial range of elems. useMark: if nonzero, regions are given by element marks, otherwise by zones.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


int number_uns_elems_in_regions ( uns_s *pUns, numberedType_e nrType,
                                  const int mReg, const int iReg[],
                                  const int doReset,
                                  const int useMark ) {

#undef FUNLOC
#define FUNLOC "in number_uns_elems_in_regions"

  int kR ;
  if ( useMark ) {
    for ( kR = 0 ; kR < mReg ; kR++ ) {
      /* Check whether mark numbers are in range. */
      if ( iReg[kR] < 0 || iReg[kR] > SZ_ELEM_MARK-1 )
        hip_err ( fatal, 0, "kMark out of range in"FUNLOC"." ) ;
    }
  }


#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge ) {
    hip_err ( fatal, 0, "fix up for hierachic adaptation "FUNLOC".") ;
    return (  number_uns_elemFromVerts_adapt ( pUns, nrType ) ) ;
  }
#endif

  chunk_struct *Pchunk ;
  elem_struct *pElem ;
  int iType ;
  if ( doReset ) {
    /* Reset all counters. */
    pUns->mElemsNumbered = 0 ;
    for ( iType = 0 ; iType < MAX_ELEM_TYPES ; iType++ )
      pUns->mElemsOfType[iType] = 0 ;

    for ( iType = 0 ; iType < MAX_DRV_ELEM_TYPES ; iType++ )
      pUns->mElems_w_mVerts[iType] = 0 ;

    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
      Pchunk->mElemsNumbered = 0 ;
      for ( pElem = Pchunk->Pelem+1 ; 
            pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ ) {
        pElem->number = 0 ;
      }
    }
  }

  /* Just base elements. */
  int isMatch ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
    /* Loop over all cells. */
    for ( pElem = Pchunk->Pelem+1 ;
          pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ ) {
      if ( !pElem->invalid ) {
        
        if ( useMark )
          isMatch = elem_has_marks ( pElem, mReg, iReg ) ;
        else
          isMatch = zone_match_list ( mReg, iReg, pElem->iZone ) ;
        
        if ( isMatch
#ifdef ADAPT_HIERARCHIC
             &&
             ( pUns->pllAdEdge ? pElem->leaf : 1 )
#endif
             ) {
          /* JDM, May 19; why alter markdEdges here?
             pElem->markdEdges = pElem->elType ; */
          pElem->number = ++pUns->mElemsNumbered ;
          ++pUns->mElemsOfType[ pElem->elType ] ;
          ++pUns->mElems_w_mVerts[ pElem->elType ] ;
          ++Pchunk->mElemsNumbered ;
         
        }
      }
    }
  }
 

  return ( 0 ) ;
}


/******************************************************************************
  number_uns_elems_match:   */

/*! Number base elements if they match conditions.
 */

/*
  
  Last update:
  ------------
  6May20; derived from number_uns_elems_zones

  Input:
  ------
  pUns
  pMatch: definition of required matches.
  doReset: if true reset numbering and counters even with partial range of elems.

  Changes To:
  -----------

  Output:
  -------
  pmConn: number of conn entries of numbered elements.
    
  Returns:
  --------
  number of numbered elements.
  
*/

ulong_t number_uns_elems_match ( uns_s *pUns, const match_s *pMatch,
                                 const int doReset, ulong_t *pmConn ) {

#undef FUNLOC
#define FUNLOC "in number_uns_elems_match"

#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge ) {
    hip_err ( fatal, 0, "implementation of renumbering for adapted grids"
              " is incomplete "FUNLOC"." ) ;
    const numberedType_e nrType = leaf ;
    return (  number_uns_elemFromVerts_adapt ( pUns, nrType ) ) ;
  }
#endif

  chunk_struct *Pchunk ;
  elem_struct *pElem ;
  int iType ;
  if ( doReset ) {
    /* Reset all counters. */
    pUns->mElemsNumbered = 0 ;
    for ( iType = 0 ; iType < MAX_ELEM_TYPES ; iType++ )
      pUns->mElemsOfType[iType] = 0 ;

    for ( iType = 0 ; iType < MAX_DRV_ELEM_TYPES ; iType++ )
      pUns->mElems_w_mVerts[iType] = 0 ;

    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
      Pchunk->mElemsNumbered = 0 ;
      for ( pElem = Pchunk->Pelem+1 ; 
            pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ ) {
        pElem->number = 0 ;
      }
    }
  }

  /* Just base elements. */
  *pmConn = 0 ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
    /* Loop over all cells. */
    for ( pElem = Pchunk->Pelem+1 ;
          pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
      if ( elem_matches ( pElem, pMatch ) ) {
        pElem->number = ++pUns->mElemsNumbered ;
        ++pUns->mElemsOfType[ pElem->elType ] ;
        ++pUns->mElems_w_mVerts[ pElem->elType ] ;
        ++Pchunk->mElemsNumbered ;
        *pmConn += elemType[ pElem->elType].mVerts ;
      }
  }



  return ( pUns->mElemsNumbered ) ;
}






/******************************************************************************

  count_uns_elems_of_type:
  Count the number of different element types in a grid.
  
  Last update:
  ------------
  4Apr19; rename to _of_type to distinguish from new count_*elem*zone.
  : conceived.
  
  Input:
  ------
  pUns
  mElemsOfType[]: a field sized at least MAX_ELEM_TYPES

  Changes To:
  -----------
  mElemsOfType[]: the number of numbered elements in the grid for each type.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void count_uns_elems_of_type ( uns_s *pUns ) {
  
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElemBeg, *pElemEnd ;
  int elType ;

  /* Reset the counters. */
  for ( elType = 0 ; elType < MAX_ELEM_TYPES ; elType++ )
    pUns->mElemsOfType[elType] = 0 ;

  /* Loop over all elements. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number )
        ++pUns->mElemsOfType[ pElem->elType ] ;

  /* Sum all types. */
  pUns->mElemsNumbered = 0 ;
  for ( elType = 0 ; elType < MAX_ELEM_TYPES ; elType++ )
    pUns->mElemsNumbered += pUns->mElemsOfType[elType] ;

  return ;
}



/******************************************************************************

  count_uns_elems_region:
  Count the number elements and conn entries in a region given by 
  elem marks or zone.
  
  Last update:
  ------------
  26Dec21; convert to region fgrom _zone.
  17Apr19; include test for iZone.
  4Apr19: derived from count_uns_elems_of_type.
  
  Input:
  ------
  pUns
  iZone: elem mark or zone to match
  useMark: if nonzero: use marks, otherwise use zones.

  Output
  -----------
  *pmConnZone: number of connectivity entries in the zone.
  
  Returns:
  --------
  number of elements in the zone.
  
*/

ulong_t count_uns_elems_region ( const uns_s *pUns, const int iReg,
                                 ulong_t *pmConnZone, const int useMark ) {
  
  elem_struct *pElem, *pElemBeg, *pElemEnd ;
  int elType ;

  ulong_t mElemsZone = 0 ;
  *pmConnZone = 0 ;


  /* Loop over all elements. */
  int isMatch ;
  chunk_struct *pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number ) {
        if ( useMark ) 
          isMatch = elem_has_mark(pElem,iReg) ;
        else
          isMatch = pElem->iZone == iReg ;
        
        if ( isMatch ) {
          ++mElemsZone ;
          *pmConnZone += elemType[pElem->elType].mVerts ;
        }
      }

  return ( mElemsZone ) ;
}

/******************************************************************************

  number_uns_vert_bc:
  Loop over the vertices of a particular bc and increment a counter.
  
  Last update:
  ------------
  6Apr13: promote possibly large unsigned ints to ulong_t
  3Apr11; modify interface to all marking for more than one bc.
  17Sep09; init pBndPatch before loop_bndFc_bc.
           renumber vertices in order, after marking.
  27May09; count the number of faces per type.
  4May96: conceived.
  
  Input:
  ------
  pUns
  numAll: number all bc
  mNumBc: number this many bc listed in
  nNumBc: list of bcs to be numbered
  
  Changes to:
  ----------
  pChunk->Pvrtx->nr

  Output:
  ----------
  mFc[] = the number of faces in this bc: 2=bi, 3=tri, 4=quad.

  Returns:
  --------
  number of vx on these bc.
  
*/

ulong_t number_uns_vert_bc ( uns_s *pUns, 
                             int numAll, int mNumBc, const int nNumBc[], 
                             ulong_t mFc[MAX_VX_FACE+1] ) {

  ulong_t mVxNum, mVxNum2 ;
  int iBc ;
  int nBc ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcBeg, *pBndFcEnd, *pBndFc ;
  elem_struct *pElem ;
  const faceOfElem_struct *pFoE ;
  ulong_t mVerts ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int k ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;

  /* Reset the node numbering. */
  pUns->numberedType = vxBc ;
  reset_vx_number ( pUns ) ;

  mVxNum = 0 ;
  mFc[0] = mFc[1] = mFc[2] = mFc[3] = mFc[4] = 0 ;

  /* Loop over all boundaries (case numAll) or those in the list. */
  iBc = 0 ;
  if ( numAll ) mNumBc = pUns->mBc ;
  for ( iBc = 0 ; iBc < mNumBc ; iBc++ ) {
    nBc = ( numAll ? iBc : nNumBc[iBc] ) ;
    
    /* Mark and number all nodes in this boundary. */
    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pElem = pBndFc->Pelem ;
        if ( pElem && pElem->number && pBndFc->nFace ) {
          pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
          mVerts = pFoE->mVertsFace ;
          kVxFc = pFoE->kVxFace ;
          ppVx = pElem->PPvrtx ;

          /* Count the face. */
          mFc[ mVerts ]++ ;

          /* Number the vertices, if not yet numbered. */
          for ( k = 0 ; k < mVerts ; k++ ) 
            if ( !ppVx[ kVxFc[k] ]->number )
              ppVx[ kVxFc[k] ]->number = ++mVxNum ;
        }
      }
  }

    
  /* Renumber the marked ones in chronological order. */
  pChunk = NULL ;
  mVxNum2 = 0 ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number )
        pVrtx->number = ++mVxNum2 ;
        
  if ( mVxNum != mVxNum2 ) {
    sprintf ( hip_msg, " in :number_uns_vert_bc\n"
              "        two differing counts of numbered vertices.\n" ) ; 
    hip_err ( fatal, 0, hip_msg ) ;
  }

  pUns->mVertsNumbered = mVxNum ;

  return ( mVxNum ) ;
}
  
/******************************************************************************

  count_vx_mark:
  Count marked vertices
  
  Last update:
  ------------
  15Sep16; return ulong_t type
  30Jun16; derived from increment_uns_vert_number
  
  Input:
  ------
  pUns
  mark: 1 if != -1, then pVrtx->mark has to match mark,
  mark2: 1 if != -1, pVrtx->mark2 has to match mark2,
  mark3: 1 if != -1, pVrtx->mark3 has to match mark3,
  
  Returns:
  ----------
  number of vertices matching the bitmap
  
*/

ulong_t count_vx_mark ( uns_s *pUns, 
                        const int mark, const int mark2, const int mark3 ) {
  
  vrtx_struct *pVrtx ;
  chunk_struct *pChunk ;

  if ( mark == -1 && mark2 == -1 && mark3 == -1 )
    hip_err ( warning, 2, 
              "settings for all marks are optimal, anything matches." ) ;

  ulong_t mMatch = 0 ;
  
  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    pChunk->mVertsNumbered = 0 ;

    for ( pVrtx = pChunk->Pvrtx+1 ;
          pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
      if ( !pVrtx->invalid && pVrtx->number ) {
        if ( ( mark  == -1 || pVrtx->mark  == mark ) &&
             ( mark2 == -1 || pVrtx->mark2 == mark2 ) &&
             ( mark3 == -1 || pVrtx->mark3 == mark3 ) )
          /* This one is marked. */
          mMatch++ ;
      }
  }

  return ( mMatch ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  number_uns_vx_markN:
*/
/*! renumber vertices if markN is set.
 *
 */

/*
  
  Last update:
  ------------
  29Apr20: conceived.
  

  Input:
  ------
  pUns
  kMark

  Returns:
  --------
  number of marked vertices.
  
*/

ulong_t number_uns_vx_markN ( uns_s *pUns, const int kMark ) {
  ulong_t mVxMarked = 0 ;
  

  /* Set mark[0] for all nodes in the list. Assume the nodes are 
     numbered chrono across patches. */
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( vx_has_markN ( pVrtx, kMark ) )
        pVrtx->number = ++mVxMarked ;
      else
        pVrtx->number = 0 ;

  return ( mVxMarked ) ;
}


/******************************************************************************

  increment_uns_vert_number:
  Loop over the vertices and increment a counter.
  
  Last update:
  ------------
  13Jul20; set mVertsUsed to be highest number of valid pVrtx.
  21Sep16; intro DoReset arg
  30Jun12; Use ->invalid field instead of ->number to indicate valid element.
  4May96: conceived.
  
  Input:
  ------
  pUns
  doReset: if non-zero, restart vertex, numbering, otherwise continue from existing.
  
  Changes to:
  ----------
  pChunk->Pvrtx->nr
  
*/

void increment_uns_vert_number ( uns_s *pUns, const int doReset ) {
  
  vrtx_struct *pVrtx ;
  chunk_struct *pChunk ;

  if ( doReset )
    pUns->mVertsNumbered = 0 ;
  
  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    if ( doReset )
      pChunk->mVertsNumbered = 0 ;

    for ( pVrtx = pChunk->Pvrtx+1 ;
          pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
      if ( !pVrtx->invalid ) {
        if ( doReset || !pVrtx->number ) {
          /* This one is marked. */
          pVrtx->number = ++pUns->mVertsNumbered ;
          ++pChunk->mVertsNumbered ;
          pChunk->mVertsUsed = pVrtx - pChunk->Pvrtx ;
        }
      }
      else if ( doReset )
        pVrtx->number = 0 ;
  }

  return ;
}


/******************************************************************************

  increment_vx_number_bc:
  Loop over the vertices and increment a counter, number bc verts first.
  
  Last update:
  ------------
  20Dec17; fix issues with ULG format mismatch.
  19Dec16; number int vx after bnd vx.
  1jul16; derived from increment_uns_vertex_number.
  
  Input:
  ------
  pUns
  
  Changes to:
  ----------
  pChunk->Pvrtx->nr
  
*/

ulong_t increment_vx_number_bc ( uns_s *pUns ) {

  pUns->numberedType = vxBndFirst ;

  /* Mark bnd vx and count them. */
  mark2_bndVx ( pUns ) ;  
  ulong_t mVxBnd = count_vx_mark ( pUns, -1, 1, -1 ) ;
  
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx ;
  pUns->mVertsNumbered = 0 ;
  ulong_t mVxBnd2=0, nVxInt=mVxBnd ;
  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    pChunk->mVertsNumbered = 0 ;

    for ( pVx = pChunk->Pvrtx+1 ;
          pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ )
      if ( !pVx->invalid ) {
        if ( pVx->mark2 )
          pVx->number = ++mVxBnd2 ;
        else 
          pVx->number = ++nVxInt ;
	++pChunk->mVertsNumbered ;
      }
      else
        pVx->number = 0 ;
  }

  if ( mVxBnd != mVxBnd2 ) {
    sprintf ( hip_msg, "mismatch in bnd vx number in increment_vx_number_bc."
              " Expected %"FMT_ULG", found %"FMT_ULG".", mVxBnd, mVxBnd2 ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( mVxBnd ) ;
}

  
/******************************************************************************

  validate_uns_vertFromElem:
  Loop over all elements and mark all used vertices by setting number to 1. 
  
  Last update:
  ------------
  10Sep16; intro useNumber.
  30June12; use the new pVx->invalid field instead of the numbering.
            base validity of vx on validity of the element it is referenced by.
  4May96: conceived.
  
  Input:
  ------
  pUns
  useNumber: base vertex use on element number, rather than validty flag.
  
  Changes to:
  ----------
  pChunk->Pvrtx->nr

  Returns:
  ---------
  number of validated vx.
*/

ulong_t validate_uns_vertFromElem ( uns_s *pUns, const int useNumber ) {
  
  vrtx_struct *pVrtx, **ppVrtx ;
  elem_struct *pElem ;
  chunk_struct *pChunk ;

  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    /* Reset the marks. */
    for ( pVrtx = pChunk->Pvrtx+1 ;
	  pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
      pVrtx->invalid = 1 ;
  }
  

  /* Loop over all chunks. */
  ulong_t mVxValidated = 0 ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
  
    /* Loop over all cells. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( ( useNumber && pElem->number ) ||
           ( !useNumber && !pElem->invalid ) )
        /* if ( pElem->number ) */
	/* Loop over all vertices forming that cell. */
	for ( ppVrtx = pElem->PPvrtx ;
	      ppVrtx < pElem->PPvrtx + elemType[pElem->elType].mVerts ; ppVrtx++ ) {
          if ( (*ppVrtx)->invalid == 1) {
            // validate this vx, count.
            (*ppVrtx)->invalid = 0 ;
            mVxValidated++ ;
          }
        }
  }
  return (mVxValidated) ;
}


/******************************************************************************

  find_nBc:
  Given a bc pointer pBc, find under which number it appears with a given
  uns grid pUns->ppBc and pUns->ppRootPatchBc. Note that both are indexed 
  from 0.
  
  Last update:
  ------------
  22Apr16; test for valid ppBc.
  20Jun06: conceived.
  
  Input:
  ------
  pBc
  
  Returns:
  --------
  -1 on failure, nBc on success.
  
*/

int find_nBc ( const uns_s *pUns, const bc_struct *pBc ) {

  if ( !pUns->ppBc || !pUns->ppBc[0] )
    hip_err ( fatal, 0, "no list of bcs with this uns grid." ) ;

  int nBc ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->ppBc[nBc] == pBc )
      return ( nBc ) ;

  return ( -1 ) ;
}
/******************************************************************************

  list_vert_bc:
  List all vertex numbers with a given boundary patch. If a patch adjacent
  to a periodic one, but not periodic itself, add the periodic siblings of
  the other patch to the list. 
  
  Last update:
  ------------
  6Apr13; initialise dontAxis.
  18Feb13; convert int to ulong_t where so defined.
  16Dec08; new interface to mark_uns_vertBc.
  4Apr06; new interface to mark_uns_vertBc with dontSglNrm.
  14Jan01; use dontPer. list_vert_bc is only called in write_uns_adf to
           prepare a list of periodic nodes. 
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int list_vert_bc ( uns_s *pUns, const int nBc, const int mBndVx, int *pnBndVx ) {

  const int dontPer = 0, dontAxis=0 ;
  ulong_t mBi, mTri, mQuad ;
  int nBeg, nEnd ;
  int *pnVx = pnBndVx ;
  ulong_t mVxBc ;
  int foundPer ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;

  /* This takes care of adding periodic siblings. */
  mark_uns_vertBc ( pUns, nBc, dontPer, dontAxis, singleBndVxNormal,
                    &foundPer, &mVxBc, &mBi, &mTri, &mQuad ) ;

  mBi = mTri = mQuad = 0 ;

  /* Alloc, if no list given. */
  if ( !pnBndVx )
    pnVx = pnBndVx = arr_malloc ( "pnBndVx in list_vert_bc", pUns->pFam, 
                                  mBndVx, sizeof( int ) ) ;


  /* Find all boundary vertices with this bc. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->mark )
        /* This one is marked for this bc, add it. */
        *pnVx++ = pVrtx->number ;

  
  if ( pnVx - pnBndVx != mBndVx ) {
    printf ( " FATAL: miscount of boundary vertices for boundary %d:\n"
             "        %d expected, but %d found in list_vert_bc.\n",
             nBc, mBndVx, (int)(pnVx - pnBndVx) ) ;
    return ( 0 ) ; }
  else
    return ( 1 ) ;
}




/******************************************************************************

  count_uns_bndFc_chk:
  Count the active boundary faces within each chunk, update the counter
  in each chunk.

  Where do we actually make use of this? Replace this with count_uns_bndFaces,
  Fix this.
  May19: only used in uns2D_uns3D. 
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns:

  Changes To:
  -----------
  *pUns
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int count_uns_bndFc_chk ( uns_s *pUns ) {
  
  chunk_struct *pChunk ;
  bndPatch_struct *PbndPatch ;
  bndFc_struct *PbndFc ;
  
  for ( pChunk = pUns->pRootChunk, pChunk->mBndFacesMarked = 0 ;
        pChunk ; pChunk = pChunk->PnxtChunk ) {
    for ( PbndPatch = pChunk->PbndPatch+1 ;
	  PbndPatch <= pChunk->PbndPatch + pChunk->mBndPatches ; PbndPatch++ ) {
      PbndPatch->mBndFcMarked = 0 ;
      for ( PbndFc = PbndPatch->PbndFc ;
	    PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ )
	if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace )
	  PbndPatch->mBndFcMarked++ ;
      pChunk->mBndFacesMarked += PbndPatch->mBndFcMarked ;
    }
  }

  return ( 1 ) ;
}

/******************************************************************************

  reset_vx_number:
  Reset all vertex numbers.
  
  Last update:
  ------------
  26May09: derived from reset_chk_vx_mark.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns-> ->pVrtx->number
  
*/

void reset_vx_number ( uns_s *pUns ) {
  
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx ;
  
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ){
    for ( pVrtx = pChunk->Pvrtx + 1 ;
	  pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
      pVrtx->number = 0 ;
  }

  return ;
}



/******************************************************************************

  count_uns_bndFaces:
  Count all boundary faces active in a grid.
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  12may17; support interfaces.
  16Dec08; new interface to mark_uns_vertBc.
  9Apr3; intro singleBndVxNormal
  : conceived.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns:     update all the face, vert and elementBc counters.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int count_uns_bndFaces ( uns_s *pUns ) {

  const int dontPer = 0, dontAxis = 0 ;
  int nBc, foundPer ;

  pUns->mVertAllBc = pUns->mBiAllBc = pUns->mTriAllBc = pUns->mQuadAllBc = 0 ;
  pUns->mVertAllInter = pUns->mBiAllInter = pUns->mTriAllInter = pUns->mQuadAllInter = 0 ;

  

  reset_vx_mark2 ( pUns ) ;

  /* Loop over all the bc, mark, count and unmark faces and verts
     for that bc. dontPer does not include periodic siblings in the marking. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    mark_uns_vertBc ( pUns, nBc, dontPer, dontAxis, singleBndVxNormal, &foundPer, 
                      pUns->pmVertBc+nBc,
		      pUns->pmBiBc+nBc, pUns->pmTriBc+nBc, pUns->pmQuadBc+nBc ) ;

    /* Increment counters. */
    pUns->pmFaceBc[nBc] = pUns->pmBiBc[nBc] + pUns->pmTriBc[nBc] + pUns->pmQuadBc[nBc] ;

    if ( pUns->ppBc[nBc]->geoType == bnd ) {
      pUns->mVertAllBc += pUns->pmVertBc[nBc] ;
      pUns->mBiAllBc   += pUns->pmBiBc[nBc] ;
      pUns->mTriAllBc  += pUns->pmTriBc[nBc] ;
      pUns->mQuadAllBc += pUns->pmQuadBc[nBc] ;
    }
    else if ( pUns->ppBc[nBc]->geoType == inter ) {
      pUns->mVertAllInter += pUns->pmVertBc[nBc] ;
      pUns->mBiAllInter   += pUns->pmBiBc[nBc] ;
      pUns->mTriAllInter  += pUns->pmTriBc[nBc] ;
      pUns->mQuadAllInter += pUns->pmQuadBc[nBc] ;
    }
  }

  /* Total count. */
  pUns->mFaceAllBc = pUns->mBiAllBc + pUns->mTriAllBc + pUns->mQuadAllBc ;
  pUns->mFaceAllInter = pUns->mBiAllInter + pUns->mTriAllInter + pUns->mQuadAllInter ;

  return ( 1 ) ;
}


/******************************************************************************

  rm_perBc:
  Remove all periodic boundary conditions from the counters of verts and faces.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------
  pUns->
  
*/

void rm_perBc ( uns_s *pUns ) {

  int nBc ;
  
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( !strncmp(  pUns->ppBc[nBc]->text, "hip_per_", 8 ) ||
         pUns->ppBc[nBc]->type[0] == 'l' || pUns->ppBc[nBc]->type[0] == 'u' ) {
      /* Remove this one. */
      pUns->mVertAllBc    -= pUns->pmVertBc[nBc] ;
      pUns->mBiAllBc      -= pUns->pmBiBc[nBc] ;
      pUns->mTriAllBc     -= pUns->pmTriBc[nBc] ;
      pUns->mQuadAllBc    -= pUns->pmQuadBc[nBc] ;
      pUns->mFaceAllBc    -= pUns->pmQuadBc[nBc]+pUns->pmTriBc[nBc]+pUns->pmBiBc[nBc] ;
      pUns->pmVertBc[nBc]  = 0 ;
      pUns->pmBiBc[nBc]    = pUns->pmTriBc[nBc] = pUns->pmQuadBc[nBc] = 0 ;
      pUns->pmFaceBc[nBc]  = 0 ;
    }

  return ;
}

/******************************************************************************

  match_face_vxnr:
  Given a set of vertices in an element, determine the matching face number.
  
  Last update:
  ------------
  21Jan18; add doc.
  : conceived.
  
  Input:
  ------
  pElem
  nrVxFc: numbers of the vertices on the face
  mVxFc: nuber of vx on the face.

  Returns:
  --------
  face number for match, 0 on failure
  
*/

int match_face_vxnr ( const elem_struct *pEl, const ulong_t nrVxFc[], int mVxFc ) {

  const elemType_struct *pElT = elemType + pEl->elType ;
  const faceOfElem_struct *pFoE ;
  int kFace, match ;
  ulong_t nVx ;
  int kVx ;
  
  for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
    pFoE = pElT->faceOfElem + kFace ;

    if ( mVxFc == pFoE->mVertsFace ) {
      /* Number of vertices match. Loop over all vertices in this face. */
      for ( match = 1, nVx = 0 ; nVx < mVxFc ; nVx++ ) {
        for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
          if ( pEl->PPvrtx[ pFoE->kVxFace[kVx] ]->number == nrVxFc[nVx] )
            break ;
        if ( kVx >= mVxFc ) {
          /* Vertex nVx is not on this face. Try the next face. */
          match = 0 ;
          break ; }
      }

      if ( match ) {
        return ( kFace ) ;
      }
    }
  }

  /* Nothing matches. */
  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  match_int_list:
*/
/*! given a target int and a list of integers, find a match for target in list. 
 *k *
 */

/*
  
  Last update:
  ------------
  29Apr20; add comment
  18Dec18: conceived.
  

  Input:
  ------
  mI: number of keys
  mI2match
    
  Returns:
  --------
  pos for match (counting from 1) if there is a match, 0 otherwise.
  
*/

int match_int_list ( const int mI, const int mI2match[], const int i ) {

  int k ;
  for ( k = 0 ; k < mI ; k++ )
    if ( mI2match[k] == i )
      return ( k+1 ) ;
  
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_nVx:
*/
/*! Given a vx number, assuming chrono numbering of vertices, find the matching vx.
 *  Note: can't allow non-numbered vertices in the list. Use bsearch_void for that.
 */

/*
  
  Last update:
  ------------
  29Apr20: conceived.
  

  Input:
  ------
  pUns
  nVx
    
  Returns:
  --------
  the matching vx pointer on success, NULL on failure.
  
*/

vrtx_struct *find_nVx ( const uns_s *pUns, const int nVx ) {

  chunk_struct *pCh = pUns->pRootChunk ;
  ulong_t mVxNoPrev = 0 ;
  while ( pCh && pCh->mVertsNumbered < nVx )
    pCh = pCh->PnxtChunk ;

  vrtx_struct vxTrgt, *pVxT = &vxTrgt, *pVx ;
  vxTrgt.number = nVx ;
  if ( pCh ) {
    /* This chunk should contain nVx.  Note that pch-Pvrtx numbers from 1.*/
    pVx = bsearch ( &pVxT, pCh->Pvrtx+1, pCh->mVerts, sizeof ( pVxT ), cmp_vx ) ;

    if ( pVx->number == nVx )
      return ( pVx ) ;
  }  
  return ( NULL ) ;
}


vrtx_struct *find_pVx ( const uns_s *pUns, const vrtx_struct *pVxTrgt ) {

  chunk_struct *pCh = pUns->pRootChunk ;
  ulong_t mVxNoPrev = 0 ;
  while ( pCh && pCh->Pvrtx < pVxTrgt && pCh->Pvrtx + pCh->mVertsNumbered >= pVxTrgt )
    /* pVx can't be in this chunk. */
    pCh = pCh->PnxtChunk ;

  vrtx_struct *pVx ;
  if ( pCh ) {
    /* This chunk should contain nVx. Note that pch-Pvrtx numbers from 1. */
    pVx = bsearch ( &pVxTrgt, pCh->Pvrtx+1, pCh->mVerts, sizeof ( *pVx ), cmp_pvx ) ;

    if ( pVx )
      return ( pVx ) ;
  }  
  return ( NULL ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 if found, 0 otherwise.
  
*/

vrtx_struct **find_pVx_list ( vrtx_struct **ppVx,
                             vrtx_struct **ppVxList, const int mVxList ) {

  return ( bsearch ( ppVx, ppVxList, mVxList, sizeof (*ppVx), cmp_pvx ) ) ;

}

int find_npVx_list  ( vrtx_struct **ppVx, const int mVx,
                      vrtx_struct **ppVxList, const int mVxList,
                      vrtx_struct **ppVxL ) {
  int k ;
  vrtx_struct **ppVxMatch ;
  for ( k = 0 ; k < mVx ; k++ ) {
    ppVxMatch = find_pVx_list ( ppVx+k, ppVxList, mVxList ) ;
    if ( ppVxMatch )
      ppVxL[k] = *ppVxMatch ;
    else 
      /* No match, not all vx in pVx are in this list. */
      return ( 0 ) ;
  }

  return ( 1 ) ;
}
