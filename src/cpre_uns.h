/*
   cpre_uns.h:
   Unstructured stuff. Note, enums, defines and typedefs are needed from cpre.h.

  Last update:
  ------------
  7May24; intro pElemVol in chunk_struct
  25Apr23; no extra int for vx->flag1, make it flag:1, part of the bitfield.
  31May22: intro elem_data_s
  21Jul21: add vxMP2* to slidingPlaneSide_s.
  14May20; intro HyVol to uns_s ;
  21Apr20; convert elem->mark, mark2, mark3, ... into bitfield mark:SZ_ELEM_MARK
  9Jul19; rename to ADAPT_HIERARCHIC
  22May19; intro elem_s.isBuffered.
  15May19; add mVxTot to _elem_s.
  28Feb19; rename perBc->rotAngle to rotAngleRad, to clarify meaning.
  15Dec18; track use of elemMarks.
  4Sep18; add comment on numbering start of pChunk->PbndFc.
  18Apr18; use ulong_t in fluent types.
  12Mar18; intro ppIJK2vx in _uns_s. 
  21Jan18; add Pbc pointer also to intFc and matchFc.
  20Jan18; add mark2 and mark3 to elem_struct, to parallel vx->mark.
  4Sep17; track also perodic axis/tr_op with each pair and the uns_s mesh. 
   5Jul17; intro kOppSide in faceOfElem_s, 
           intro stackFc_s to support stacks.
  12May17; support interfaces, add interface face counters to uns_s.
  20Dec16; intro pChunk->pNr2, ->pVrtxVol ;
  19Dec16; intro numberedType elTypes, vxBc, vxBndFirst.
   1Jul16; introduce boundary edges for GUI interaction/query.
  12Dec13; intro pSolParam in zone_s.
  12Jul13; make pZones run from index 1, not 0.
  18Feb13; remove maxConn and piConn. They are required of type cgsize_t
           (which is int in 3.1.3-4), cgnstypes.h is only known to 
           read_uns_cgns.h. 
  17Feb13; switch counters for large fields/array sizes from int to ulong_t 
  30Jun12; track vx flag use by string.
           intro pVrtx->invalid to persistently track invalid nodes.
  19Mar12; track usage of vx marks.
  17Dec11; add vxMarkActivity.
  16Dec11; rename hrbMark to the more descriptive general boxMark.
  9Jul11; intro zones into elem_struct and uns_s, use ADAPT_REF. 
  7Jul11; use INT_MAX out of limits.h instead of MAX_INT. (Where was that defined?)
  3Jun11; consistently define sizes for cpt_s under IPTR64.
  4Apr11; add elType_e bi at the end of the list. 
          intro iConn, mConn.
  8Nov10; move def of av2n3s into read_n3s.
  14Sep10; intro MAX_UNS_CP.
  3Jul10; track volElemMax.
  1Jul10; move HH_ dataytype defs to cpre.h
  29Nov09; make pEl->number and pVx->number unsigned ints for large meshes.
  15Nov09; use ulong_t for counters of pointers, etc.
  19Sep09; track volEleMin in pUns.
  5Apr09; more restart to cpre.h
  4Apr09; change varType to varList.
  14Dec08; change the split of an int (32bit) in cpt_s to 4/28, i.e.
           a max of 16 blocks, but 268M nodes.
  18May08; add explanation to mp_bndVx.
  10Dec07; redefine n3s prisms.
  27Jun07; intro flFc_s.
  13May07; rename elem_type_enum to elType_e.
  18Jun06; intro epsOverlap to pUns.
  15May06; intro mp_bndVx_s, change vx->number to unsigned int.
  5Apr06; intro AVSP 3.0 format, intro SZ_mEqK.
  14Mar06; change def of n3s prism.
  30Mar04; store pointers to pBc rather than bc numbers in perBc_s.
  2Feb04; add 3 to restart_u for AVBP meanval.
  20Dec01; move typedef of perBc_s to cpre.h
  10Mar01; intro pVxCpt for mg-adapt.
  7Sep00;  intro restart_u.
  30Mar00; new pPerVXPair that carries pPerBc.
  25Oct99; remove mAttEdges from faceOfElem_struct, intro edgeAttAt.
  22Oct99; add an element pointer to bndFcVx.
  15Jul99; intro bndFcVx.
  24Jan99; intro elemType[].edgeOfElem[].kParaEg.
  21Dec98; dump the triamond.
  29Oct98; intro edgeOfElem.kEgInFc ;
  19Mar98; move all unstructured typedefs from cpre.h here.
  8Feb98; move towards compound pointers, intro nChunk and nVx in vrtx_struct. 
  20Aug96; intro attFaces.
  6May96: cut from cpre.h

*/


/* max no of grids to make up one set. Used in uns_copy. */
#define MAX_UNS_CP 128

/* number of equation fields in read_avbp.c, write_avbp.c */
# define SZ_mEqK 99

/* Consider only volumetric elements here. */
#define MAX_ELEM_TYPES 6
/* Including 2D boundary faces, type bi. */
#define MAX_ALL_ELEM_TYPES 7

/* Although I would like to keep 0=noEle as a sign of
   non-initialization, we can't waste the space with refPattern[0], and many
   other fixed-size arrays. */
typedef enum { tri, qua, tet, pyr, pri, hex, bi, noEl } elType_e  ;

#define MAX_VX_ELEM 8
#define MAX_FACES_ELEM 6
/* Maximum number of vertices per face. For the time being,
   only simplices are considered. */
#define MAX_VX_FACE 4
#define MAX_EDGES_ELEM 12
#define MAX_EDGES_FACE 4

#define SZ_ELEM_MARK (6) // How many components in the elem mark bitfield?
#define MASK_ELEM_MARK (63) //1+2+4+8+16+32 -1 = 63
#define BOX_MARK (SZ_ELEM_MARK-1) // use the last mark as boxMark.
#define CUT_MARK (0) // use the first mark as cutxsMark.

/* Worst case a fully refined hex, 27 verts - 8 of the parent. */
#define MAX_ADDED_VERTS 19
#define MAX_REFINED_VERTS ( MAX_VX_ELEM + MAX_ADDED_VERTS )

/* Maximum number of vertices per face. For the time being,
   only simplices are considered. */
#define MAX_CHILDS_ELEM 10
#define MAX_CHILDS_FACE 8

/* Maximum number of pairs of periodic patches. */
#define MAX_PER_PATCH_PAIRS 10
#define MAX_PER_VX 8

/* Number of possible derived element types.
   The first 0 <= iPos < MAX_ELEM_TYPES are the base elements.
   the following MAX_ELEM_TYPES <= iPos < MAX_DRV_ELEM_TYPES are derived elements
   with mVerts = iPos - MAX_ELEM_TYPES + 4 vertices, i.e. the derived element with
   the least number of vertices has four, a tri with a hanging node. */
#define MAX_DRV_ELEM_TYPES 29

/* How many characters in an adf surface group name without trailing \0. */
# define ADF_SURFLBL_LEN 20
# define ADF_SURFLBL_WRITE_LEN 80


/* Unstructured typedefs. */

typedef enum { noNum, root, leaf, term, parent, leafNprt, elTypes,
               vxBc, vxBndFirst, invNum } numberedType_e ;

typedef struct _elemType_struct  elemType_struct ;
typedef struct _edgeOfElem_struct edgeOfElem_struct ;
typedef struct _faceOfElem_struct faceOfElem_struct ;
typedef struct _edgeOfFace_struct edgeOfFace_struct ;

typedef struct _cpt_s cpt_s ;
typedef struct _chunk_struct chunk_struct ;

typedef struct _color_s color_s ;
typedef struct _llEg2Elem_s llEg2Elem_s ;

typedef struct _vrtx_struct vrtx_struct ;
typedef struct _elem_struct elem_struct ;          
typedef struct _elem_data_s elem_data_s ;          

typedef struct _bndPatch_struct bndPatch_struct ;  
typedef struct _bndFc_struct    bndFc_struct ;        
typedef struct _bndFcVx_s       bndFcVx_s ;        
typedef struct _intFc_struct    intFc_struct ;        
typedef struct _intFc_struct    degenFc_struct ;        
typedef struct _matchFc_struct  matchFc_struct ;

typedef struct _childSpc_s childSpc_s ;

typedef struct _perVxPair_s perVxPair_s ;

/* cpre_adapt.h: */
typedef struct _refType_struct refType_struct ;
typedef struct _adEdge_s adEdge_s ;

/* edge_manage.c: */
typedef struct _llEdge_s llEdge_s ;

/* uns_fcOfVx.c: */
typedef struct _surfTri_s surfTri_s ;

/* uns_llfc.c: */
typedef struct _fc2el_s fc2el_s ;

/* uns_mg.c */
typedef struct _edgeLen_s edgeLen_s ;

/* uns_per.c */
typedef struct _perVx_s perVx_s ;
typedef struct _ndxPerVx_s ndxPerVx_s ;

/* uns_toElem.c: */
typedef struct _llToElem_s llToElem_s ;

/* uns_vxEnt.c: */
typedef struct _llVxEnt_s llVxEnt_s ;

/* fix_uns_elem.c: */
typedef enum { maxAng, ll2ur, ul2lr, _0_2, _1_3 } splitType_e ;

/* To debug a mesh: */
typedef enum { smlVol, lrgVol, lrgAngle, twist } viz_e ; 

/* flag activity. */
typedef enum { flagNotAct, flagUser } flagAct_e ;



/* Integration/interpolation lines. */
typedef struct {
  /* Arclength along the line. */
  double t ;
  /* Intersection coordinate. */
  double xInt[MAX_DIM] ;

  /* The vertices on the edge/face. */
  vrtx_struct *pVx[MAX_VX_FACE] ;
  /* And their interpolation weights. */
  double wt[MAX_VX_FACE] ;
} fcInt_s ;

typedef struct {
  /* The line. */
  double xyzBeg[MAX_DIM] ;
  double xyzEnd[MAX_DIM] ;

  /* For a straight line: unit vector and len from beg to end. */
  double l1[MAX_DIM] ;
  double len ;
  /* Direction with the largest component. */
  int kMax ;

  int mFcInt ;
  int mFcAlloc ;
  fcInt_s *fcInt ;
} lineX_s ;





/* Sliding or mixing planes:
   Create pairs of matching lines on boundaries described by
   a list of intersections of mesh edges with the line.
*/

typedef struct {
  /* Arclength along the line. */
  double t ;
  /* Intersection coordinate. */
  double xX[MAX_DIM] ;

  /* The vertices on the edge/face. */
  vrtx_struct *pVx[2] ;
  /* And their interpolation weights. */
  double wt[2] ;
} egX_s ;

typedef struct {
  /* The line. */
  int mEgX ;
  int mEgXAlloc ;
  egX_s *egX ;
} spLineX_s ;

/* Type of sliding/mixing plane:
   radial machine: r=const; z, theta variable.  
   axial machine: x=const; r, theta variable 
*/
typedef enum { sp_no_const,
               sp_const_r,sp_const_rx,  sp_const_ry, sp_const_rz,
               sp_const_x, sp_const_y, sp_const_z} sp_geo_type_e ;

typedef struct _slidingPlaneSide_s slidingPlaneSide_s ;
struct _slidingPlaneSide_s {
  /* number and name that identifies the pair of sides. */
  int nr ; // global counter initialised/incremented in make_slidingPlaneSide.
  char name[LINE_LEN] ;

  
  /* The grid this sliding plane belongs to. */
  uns_s *pUns ;

  /* nonzero if this is the master side which defines r,h */
  int isMaster ;

  /* sliding plane on the other side. */
  slidingPlaneSide_s *pOtherSide ;

  
  /* The patch on this side of the plane. */
  bc_struct *pBc ;

  /* coordinate transformation. */
  sp_geo_type_e spGeoType ;

  int mLines ;
  /* For sliding/mixing plane: 
     sp_const_x: radius (axial machine), 
     sp_const_r: height (x) (radial machine)*/
  double *prh ;
  /* Set in sp_set_arc_ref_3d.
     For sliding/mixing plane: 
     sp_const_x: (axial machine) x=[0,0,1]
     sp_const_r: (radial machine) x = [1,0,0]*/
  double xArcRef[MAX_DIM] ;
  spLineX_s *pspLine ;

  /* Interpolation weights between bracketing mixing lines
     for all vertices on this interface side. */
  ulong_t mVxMP ;
  ulong_t *pnVxMP2nVx ;
  ulong_t *pnVxMP2lineLower ;
  double *pwtnVxMPlineLower ;
} ;

typedef struct {
  slidingPlaneSide_s *side[2] ;
} slidingPlanePair_s ;

/*******************************************************************************/
/* Definition of faces in 3D, edges in 2D. 
   Up to four vertex positions for each face of each element. */
struct _faceOfElem_struct {
  int mVertsFace ;
  int kVxFace[MAX_VX_FACE] ;

  /* Number of the opposite face. This is only non-zero for
     quads, prisms and hexes. */
  int nOppSide ;

  /* List of edges for each face. Calculated from edgeOfElem and
     faceOfElem in init_uns. edgeDir is 1 if the edge has the surface
     of the face to the left, 0 otherwise.
     The edges are ordered for each face such that edge 0 runs from the forming vx 0
     of that face to vx 1, etc. */
  int mFcEdges ;
  int kFcEdge[MAX_EDGES_FACE] ;  /* The edge number in edgeOfElem. */
  int edgeDir[MAX_EDGES_FACE] ;
  int bitEdgeFace ;              /* A mask for all the edges of a face. */

  int kAttEdge[MAX_VX_FACE] ;
  int edgeAttAt[MAX_VX_FACE] ;   /* A list of edges "attached" to a face, ie. 
                                    all edges connected with exactly one vertex
                                    to this face. 
                                    All entries are organised corresponding to the
                                    kVxFace list. Exception being the triangular
                                    faces of a pyramid, where the apex node has
                                    two attached edges. In this case only one of
                                    them is listed.
                                    edgeAttAt indicates which vertex of the edge
                                    is on the face. */
} ;



/* Definition of edges. Note that the sequence of 2-D edges
   is not identical to the sequence of 2-D face, although the
   entities are the same. The edges are sorted lexicographically
   by vertex positions in the element and, sigh, start at 0. */
struct _edgeOfElem_struct {
  int kVxEdge[2] ;
  int kFcEdge[2] ; /* The face[s] formed with this edge. 1 in 2D, 2 in 3D.
                      In 3D the face 0 is left of the edge, the edge 1 is right
                      of the edge when seen from the outside of the element. */
  int kEgInFc[2] ; /* In which position is this edge listed with the face.
                      Viewed from outside the, the face to the left of the edge is 0.*/
  int mParaEg ;    /* Number of edges that are parallel to this one. */
  int kParaEg[4] ; /* Index of the parallel edge. There can be at most four, hex. */
} ;



/* The 'technical data' of the elements. Filled in init_uns. */
struct _elemType_struct { 
  elType_e elType ;
  char name[4] ;
  int mDim ;
  int mVerts ;
  int mEdges ;
  int mSides ; /* The number of mDim-1 entities. Edges in 2D, faces in 3D. */
  int mFaces ; /* The number of 3D faces, ie. 0 in 2D. */

  /* The description of the mDim1Faces. In 3D both types of faces are identical. */
  faceOfElem_struct faceOfElem[MAX_FACES_ELEM+1] ;
  /* The edges. */
  edgeOfElem_struct edgeOfElem[MAX_EDGES_ELEM] ;
  
  /* The refinement patterns. */
  int mRefTypes ;
  refType_struct *PrefType ;

  /* A mask for all edges of this element. */
  int allEdges ;
} ;


/*******************************************************************************
  compound pointer. */
#define MAX_CPT_EXP_CHUNKS 5 
/*#define MAX_CPT_EXP_CPTVXNR (32-MAX_CPT_EXP_CHUNKS)*/
/* Can you do exponentials in cpp? */

#ifdef IPTR64
struct _cpt_s {
#define MAX_CPT_CHUNKS INT_MAX
#define MAX_CPT_VXNR   INT_MAX
  unsigned int nCh ;  /* Chunk nr as listed in pUns->ppChunk. */
  ulong_t nr ;  /* The seq. vx nr within that chunk. */
} ;
#else
struct _cpt_s {
#define MAX_CPT_CHUNKS (32-1) /* 2^MAX_EXP_CHUNKS */
#define MAX_CPT_VXNR (134217728-1) /* 2^(32-MAX_EXP_CHUNKS) */
  unsigned int nCh:MAX_CPT_EXP_CHUNKS ;  /* Chunk nr as listed in pUns->ppChunk. */
  unsigned int nr:(32-MAX_CPT_EXP_CHUNKS) ;  /* The seq. vx nr within that chunk. */
} ;
#endif

static const cpt_s CP_NULL = {0,0} ;
static const cpt_s CP_MAX = {  MAX_CPT_CHUNKS, MAX_CPT_VXNR } ;


/* 8 bits gives 256 colors. */
#define MAX_COLORS 255
struct _color_s {
  unsigned int mBnd:7 ;
  unsigned int color:8 ;
  unsigned int mark:1 ;
  unsigned int mVxColl:8 ;
  unsigned int maxColl:8 ;
} ;


/* A zone that elements/faces/edges/nodes can be associated with. */
#define SZ_ELEM_ZONE 8 
#define MAX_ZONES 255  /* 2^SZ_ELEM_ZONE-1 */
// JDM May 2020, not used? #define MAX_PAR 4
typedef struct {
  int iZone ;   /* Index of this zone in pUns->pZones, 1 .. mZones . */
  int number ;  /* Reference number when writing to file. */
  char name[MAX_BC_CHAR] ;

  /* Is this zone referenced? If so, by how many elements. 
     a negative value indicates invalid count. */
  int mElemsZone ;

  param_s *pParam ; // Fixed parameters written with the mesh file
  param_s *pSolParam ; // Variable parameters written with the sol file
} zone_s ;


/*******************************************************************************
 Grid-based info: vertices, elements, faces, etc. 
*/

/* Vertices. */				       
struct _vrtx_struct {					       
  ulong_t number ;

  // Packed bitfield.
  unsigned int invalid:1 ; /* invalid vertex? Persistent, even when 
                              numbering changes. */
  unsigned int mark:1 ;  /* internal marks used during counting/numbering. */
  unsigned int mark2:1 ;
  unsigned int mark3:1 ;
  unsigned int singular:1 ; /* On an axis .... */
  unsigned int per:1 ;   /* periodic. */
  unsigned int flag1:1 ;   /* user-set flag, e.g. for zoning. */

  cpt_s vxCpt ;  /* internal pointer, chunk + number in chunk. full 32 bit. */

  double *Pcoor ;
  double *Punknown ;
} ;


typedef enum { ep_none,
  ep_hMin, ep_volMin, ep_angMax,
} ep_type ;

/* Elements. */
struct _elem_struct {
  ulong_t number ;            /* The number of the element. Used for numbering
				      during i/o, but also historically for indicating
				      leaf elements. This will change. */

 
  /* Packed bitfields. Currently <= 32 bit. 
     But when put in a struct with 64bit types such as ulong_t or dbl ptr,
     it is packed out to 64bit as well. */

  elType_e elType:4 ;        /* The type of element with all the stats. */

  unsigned int term:1 ;            /* Is it a terminal, ie leaf in an unbuffered grid? */
  unsigned int mark:SZ_ELEM_MARK ;
  //unsigned int mark2:1 ;
  //unsigned int mark3:1 ;
  //unsigned int cutMark:1 ;           /* Cut for large angles. */
  //unsigned int boxMark:1 ;           /* Is the element within a box/zone? */
  unsigned int invalid:1 ;         /* Is it a discarded, invalid element? */

  unsigned int iZone:SZ_ELEM_ZONE ; /* pointer to zone the elem belongs to. */


  unsigned int markdEdges:MAX_EDGES_ELEM ;/* Bit pattern, edges already used when computing edge weights, edges marked for refinement. */

  // Sum: 32 bits.

  
 vrtx_struct **PPvrtx ;           /* A pointer to the pack of vertices. see below. */




#ifdef ADAPT_HIERARCHIC
  unsigned int root:1 ;            /* Is it a root element of the coarsest grid? */
  unsigned int leaf:1 ;            /* Is it a leaf element of the finest grid? */

  unsigned int refdEdges:MAX_EDGES_ELEM ; /* Bit pattern for the refined edges. */
  unsigned int derefElem:1 ;       /* Set to one if the cell is to be derefined. */

  /* Max number of corner (normal) and hanging nodes 
     that can be expressed is (citing from above):
       MAX_DRV_ELEM_TYPES- MAX_ELEM_TYPES + 4 vertices, i.e. the derived element
      with the least number of vertices has four, a tri with a hanging node.
      With MAX_DRV_ELEM_TYPES=29, max hg vx = 29-6+4 = 27.
      That fits into 2^5. */
#define SZ_VX_TOTAL (5)
  unsigned int mVxTot:SZ_VX_TOTAL ;
  // Do we need to manually set that?
  // unsigned int blank2:(32-MAX_EDGES_ELEM-3-SZ_VX_TOTAL) ;

  const refType_struct *PrefType ; /* Refinement type. */
  elem_struct *Pparent ;           /* Parent cell of this cell. */
  elem_struct **PPchild ;          /* List of pointers to the children. */
#endif
} ;

/* List of pointers to the vertices.
   Vertex numbers and face numbers over the elements as given by avbp.
   OK, let's do some ascii art here:

   2: 1----2, what else?

   3:    3      4: 4-----3     or 4:     3__    
        / \        |     |              / \.\4  
       /   \       1-----2             /.. \ /  
      1-----2                         1-----2

   Faces:   1: 2,3       1: 1,2             1: 2,4,3 
            2: 3,1       2: 2,3             2: 1,3,4
            3: 1,2       3: 3,4             3: 1,4,2
                         4: 4,1             4: 1,2,3

   5:    _5_    6:  6_______5     
        / | \       |\      |\    
       4-/-\-3      | \4____|_\3     
       |/   \|      | /     | /       
       1-----2      |/______|/    
                    1       2

   Faces:   1: 1,4,3,2    1: 1,2,5,6
            2: 1,2,5      2: 3,4,6,5
            3: 2,3,5      3: 1,4,3,2
            4: 3,4,5      4: 4,1,6
            5: 4,1,5      5: 2,3,5
                                                            
   8:    3----7                                             
        /|   /|                                             
       / |  / |                                             
      4----8  |                                             
      |  2-|--6                                             
      | /  | /                                              
      |/   |/                                               
      1----5

   Faces:   1: 1,2,6,5
            2: 6,2,3,7
            3: 8,7,3,4
            4: 1,5,8,4
            5: 1,4,3,2
            6: 5,6,7,8

*/

struct _elem_data_s {
  elem_struct *pElem ;
  double dblData ;
} ;


/* structure to identify a sequence of possible matches. */
typedef struct {

  /* Match if elem is numbered. */
  int hasNumber ;

  /* Multiple zones. */
  // JDM Feb 22: why do we need matchZone? Use mZones > 0 ?
  int matchZone ;
  int mZones ;
  int iZone[MAX_ZONES] ;

  /* Elem marks */
  int matchMarks ;
  unsigned int kMark2Match ;
  unsigned int kMark2NotMatch ;

  int matchElType ; // elem only
  elType_e elTypeBeg ;
  elType_e elTypeEnd ;

#ifdef ADAPT_HIERARCHIC
  int matchAdaptType ;
  numberedType_e adaptType ;
#endif

  int matchVxPer ; // vx only
  int isPer ; // if non-zero, a matching vx has to be periodic.
 
} match_s ;


/* Boundary faces. */		         
struct _bndFc_struct {				         
  elem_struct *Pelem ;          /* Pointer to the interior element. */
  int nFace ;                   /* Number of the face in the int. elem. */
  bc_struct *Pbc ;              /* Pointer to the boundary condition. */
  unsigned int invalid:1 ;        /* removed bnd face? */
  unsigned int markdEdges:4 ;   /* Flag for each of the (at most) 4 edges of the face. */
  bcGeoType_e geoType ;           /* to establish whether a boundary has become internal
                                   or not, we need to check all faces. */
} ;

/* Interfaces. These are the cut faces exposed by clipping. */
/* Degenerate faces. These are faces that collapse to a line or a point. Aliased
   via the typdef in cpre.h. */
struct _intFc_struct {
  elem_struct *Pelem ;                    /* Pointer to the interior element. */
  int nFace ;                             /* Number of the face in the int. elem. */
  bc_struct *pBc ;              /* Pointer to the boundary condition. */
} ;

/* Matching faces. These are faces between matching blocks. */
struct _matchFc_struct {
  elem_struct *pElem0 ;                   /* Pointer to the interior element. */
  int nFace0 ;                            /* Number of the face in the int. elem. */

  elem_struct *pElem1 ;                   /* Pointer to the interior element. */
  int nFace1 ;                            /* Number of the face in the int. elem. */
  
  bc_struct *pBc ;              /* Pointer to the boundary condition. */
} ;

typedef struct {
  vrtx_struct *pVx[2] ;
}  egVx_s ;


/* Various data collectors for utilities. */

/* A face out of the file, with forming nodes, formed cells, and a doubly linked
   list for each formed element. Used in fluent and cedre formats. */
typedef struct _flFc_s flFc_s ;
struct _flFc_s {
  int mVxFc ; ulong_t nVx[MAX_VX_FACE] ;
  ulong_t nEl[2] ;
  flFc_s *pNxtFc[2] ;
} ;



/* A list of boundary vertices for each face. */
struct _bndFcVx_s {
  ulong_t mVx ;
  vrtx_struct *ppVx[MAX_VX_FACE] ;
  bc_struct *pBc ;
} ;


/* Boundary normals and weights for output. */
typedef struct {
  ulong_t mBndVx ;
  vrtx_struct **pVx ;
  ulong_t *pnVx ;
  double *pNrm ;
  double *pWt ;
}  bndVxWt_s ;

/* A boundary vertex normal. */
typedef struct {
  const vrtx_struct *pVx ;
  double bndNorm[MAX_DIM] ;
} bndNorm_s ;


  /* List of bnd vx with multiple boundaries. */
typedef struct {
  ulong_t mVxMP ;     /* number of multi-patch vx */
  ulong_t *nVxMP ;    /* list of node numbers of mp vx */ 
  ulong_t *ndxVxMP ;  /* index of first bc for each mp vx in lsVxMP */
  int *lsVxMP ;   /* list of bc patches assoc. with each mp vx */
} mp_bndVx_s ;


struct _perBc_s {
  bc_struct *pBc[2] ;        /* The two pairs of bc, _inlet, then _outlet. */

  transf_e tr_op ;           /* relevant here: trans, or rot_x, rot_y, rot_z */
  double rotAngleRad ;       /* In rad. This all could be simpler, allowing only
                                rotation around x. Next time. */
  
  double xIn[MAX_DIM*MAX_DIM] ;    /* The coordinates of mDim points on the _inlet. */
  double vecIn[MAX_DIM*MAX_DIM] ;  /* The vectors in the "plane" of the _inlet. */
  double rotIn2out[MAX_DIM*MAX_DIM] ;  /* From the _inlet to the _outlet basis. */
  double shftIn2out[MAX_DIM] ;  /* From the _inlet to the _outlet basis. */
  double xOut[MAX_DIM*MAX_DIM] ;   /* The coordinates of the matching ones on _outlet. */
  double vecOut[MAX_DIM*MAX_DIM] ; /* The vectors in the "plane" of the _outlet. */
  double rotOut2in[MAX_DIM*MAX_DIM] ; /* From the _outlet to the _inlet basis. */
  double shftOut2in[MAX_DIM] ; /* From the _outlet to the _inlet basis. */

  int mPerFcPairs ;
  matchFc_struct *pPerFc ;
} ;

struct _perVxPair_s {
  vrtx_struct *In ;    /* The two vertices. */
  vrtx_struct *Out ;
  perBc_s *pPerBc ;    /* The boundary pair. */
  int revDir ;         /* 0 if vertex In is on boundary In of the pPerBc. */
} ;

/*******************************************************************************/
/* A compact struct collecting various pointers to add children elements in
   a new chunk. */
struct _childSpc_s {
  /* The base addresses of the fields, for debug.
     elem_struct *Pelem ;
     vrtx_struct **PPvrtx ;
     elem_struct **PPchild ;
     vrtx_struct *Pvrtx ;
     double *Pcoor ;
     double *Punknown ; */
  uns_s *pUns ;
  chunk_struct *PnewChunk ;

  
  elem_struct *PlstElem ;     /* Last element written. */
  vrtx_struct **PPlstVx ;     /* Last element to vertex pointer written. */
  elem_struct **PPlstChild ;  /* Last child written. */   
  vrtx_struct *PlstVrtx ;     /* Last vertex written. */
  double *PlstCoor ;          /* Last coordinate written. */
  double *PlstUnknown ;       /* Last unknown written. */
  
  elem_struct *Pelem_max ;    /* Last element that can be written. */ 
  vrtx_struct **PPvrtx_max ;  /* Last element to vertex pointer that can be written. */  
  elem_struct **PPchild_max ; /* Last child that can be written. */   
  vrtx_struct *Pvrtx_max ;    /* Last vertex that can be written. */
  double *Pcoor_max ;         /* Last coordinate that can be written. */
  double *Punknown_max ;      /* Last coordinate that can be written. */
} ;

/* AVBP. */
typedef enum { noFmt, v4_2, v4_7, v5_1, v5_3, t5_1, t2_0, s3_0, v6_0, h1_0 } avbpFmt_e ;
# define DEFAULT_avbpFmt (v5_3)






/*********************************************************************
 Groups: boundary patches, chunks, unstructured grids.
*/

/* Boundary patches. */
struct _bndPatch_struct {
  chunk_struct *Pchunk ;                  /* The chunk with this patch. */
  				         
  bndPatch_struct *PnxtBcPatch ;          /* of the same boundary condition. */

  bc_struct *Pbc ;                        /* Boundary condition of this patch. */
  bndFc_struct *PbndFc ;                  /* Pointer to the first boundary face. 
                                             Numberings starts from 1.*/
  ulong_t mBndFc ;                            /* Number of elements in this patch. */
  ulong_t mBndFcMarked ;                      /* Number of active elements in this patch. */

  ulong_t mBndEg ; /* Number of edges listed with this patch. */
  egVx_s *pBndEg ;   /* List of edges, could be perimeter but also feature. */

  double llBox[MAX_DIM] ;                 /* Bounding box of this patch. */
  double urBox[MAX_DIM] ;
  double llBoxCyl[2] ;       /* Cylindrical coordinates Bounding box of all grids. */
  double urBoxCyl[2] ;
} ;				         

/* One chunk of an unstructured grid. */
struct _chunk_struct {

  uns_s *pUns ;
  
  int chunkMark ;                   /* free marker for this chunk. */
  				 
  int nr ;                          /* Number. */
  char name[LINE_LEN] ;

  double llBox[MAX_DIM] ;	    /* Bounding box of the chunk. */
  double urBox[MAX_DIM] ;	    
  //double hMin ;                     /* min/max edge length in the chunk. */
  //double hMax ;			 
  				 
  chunk_struct *PprvChunk ;	    /* Linked list of chunks. */
  chunk_struct *PnxtChunk ;	 

  ulong_t nLstVxPrvChk ;            /* Global numbering went up to here in
				       the previous chunk. */
  ulong_t mVerts ;			    /* Vertices. */
  ulong_t mVertsNumbered ;    
  ulong_t mVertsMarked ; 
  ulong_t mVertsUsed ; /* Number of vx used prior to realloc. */
  vrtx_struct *Pvrtx ;		    /* Pvrtx starts with index 1. */
  double *Pcoor ;		    /* Coordinates, start at mDim*1.*/
				 
  double *Punknown ;		    /* Unknowns at the vertices, double. */

  ulong_t sizeDblMark ;                 /* List of double markers, it's size. */
  double *PdblMark ;		 


  cpt_s *pVxCpt ;                   /* Temporary fix for mg-hierarchy w/ adaption.
                                       A pointer to a parent for each added vertex. */

  ulong_t *pVrtxNr2 ; /* Auxiliary number field e.g. to stash a global number
                     when pVx->number is locally, temporarily renumbered. */
  double *pVrtxVol ; /* Store a vertex volume (or some other vertex-length double), if needed. */


  
  ulong_t mElems ;			    /* Cells. */
  ulong_t mElemsNumbered ;		    
  ulong_t mElemsMarked ; 
  ulong_t mElemsUsed ; /* Number of elems used prior to realloc. */
  elem_struct *Pelem ;		    /* Pelem starts with index 1. */
  ulong_t mElem2VertP ;		 
  ulong_t mElem2VertPUsed ;/* Number of ptr used prior to realloc. */ 
  vrtx_struct **PPvrtx ;	    /* 1-D list of vertex pointers, starts at 0. */
  double *pElemVol ; /* Store an elementvolume (or some other element-length double), if needed. */
  double *PCellunknown ;		    /* Unknowns at cell centres, double. */


#ifdef ADAPT_HIERARCHIC
  ulong_t mElem2ChildP ;  /* Number of ptr allocated. */
  ulong_t mElem2ChildPUsed ; /* Number of ptr used prior to realloc. */ 
  elem_struct **PPchild ;           /* 1-D list of children of elems, starts at 0. */
#endif  
  int *PelemMark ;		 



  ulong_t mBndPatches ;	            /* Number of boundary patches. */
  ulong_t mBndPatchesUsed ; /* Number of boundary patches prior to realloc. */
  bndPatch_struct *PbndPatch ;	    /* List of boundary patches. [1...]*/
  ulong_t mBndFaces ;		    /* Total number of boundary faces. */
  ulong_t mBndFacesMarked ;             /* Number of active boundary faces. */
  ulong_t mBndFacesUsed ;      /* Number of boundary faces prior to realloc. */
  bndFc_struct *PbndFc ;	    /* List of boundary faces. [1...]*/
				    
  ulong_t mIntFaces ;		    /* Internal, ie. cut faces. */
  intFc_struct *PintFc ;	    
  double llIntFcBox[MAX_DIM] ;	    /* Bounding box of cut faces. */
  double urIntFcBox[MAX_DIM] ;	    
				    
  ulong_t mMatchFaces ;		    /* Matching faces, between former blocks. */
  matchFc_struct *PmatchFc ;	    
  double llMatchFcBox[MAX_DIM] ;    /* Bounding box. */
  double urMatchFcBox[MAX_DIM] ; 
  				 
  ulong_t mDegenFaces ;		    /* Degenerate faces. */
  degenFc_struct *PdegenFc ;	    
  double llDegenFcBox[MAX_DIM] ;    /* Bounding box. */
  double urDegenFcBox[MAX_DIM] ;




  /* If a map from mb to uns is to be written, record the vx mapping. */
  int nBlock ;             /* block number, counting from 1. */
  int blockDim[MAX_DIM] ;  /* block dims. */
  vrtx_struct **ppIJK2Vx ;  /* uns vx pointer for each mb vx. */

  
  
} ;






/* A list of faces and elements that form a stack, e.g. a stack
   of prisms normal to a boundary. The 'root' face is on the boundary,
   if the stack is attached to the boundary. */
typedef struct _stackFc_s stackFc_s ;
struct _stackFc_s {
  stackFc_s *pPrvStackFc ; // Linked list.
  stackFc_s *pNxtStackFc ;
  
  elem_struct *pElem ; // Element
  int nFc ; // and the face to the pPrvStackFc or if root,  pBndFc
} ;

typedef struct {
  stackFc_s *pStackFc ;
  // if this stack starts on the bnd, pointer to that face, nil otherwise.
  bndFc_struct *pBndFc ;
  // the non qua/pri/hex elem at the end, nil otherwise.
  // Note, pElem is just a flag here, the face is stored with pStackFc.       
  elem_struct *pCapElem ;
  
} stackFcTerm_s ;


/*******************************************************************************/

/* root. */
struct _uns_s {
  int nr ;                          /* Duplication of the number in Grids. */
  //char name[TEXT_LEN] ; // use the name in pGrid.
  arrFam_s *pFam ;
  grid_struct *pGrid ;              /* Grid container. */

  int validGrid ;                   /* No write operations on invalid grids. */
  int adapted ;
  specialTopo_e specialTopo ;
  
  int mDim ;			    /* Number of spatial dimensions. */
  double llBox[MAX_DIM] ;	    /* Bounding box of the grid. */
  double urBox[MAX_DIM] ;	    
  double llBoxCyl[2] ;	    /* Cylindrical coordinates bounding box */
  double urBoxCyl[2] ;	    
  double hMin ;                     /* min/max edge length in the grid. */
  double hMax ;
  double volElemMin ;               /* min cell volume. */
  const elem_struct *pMinElem ;           /* pointer to smallest element. */
  double volElemMax ;               /* min cell volume. */ 
  const elem_struct *pMaxElem;            /* pointer to largest element. */
  double volDomain ;                /* volume of the domain */

  /* Min. difference required between two vertices, value local to this grid. */
  double epsOverlap ;
  double epsOverlapSq ;


  int mChunks ;
  chunk_struct **ppChunk ;          /* All chunks in an array. */
  chunk_struct *pRootChunk ;        /* The root of the linked list of chunks. */
  ulong_t mElemsAlloc ;                 /* Total number of alloced elems/verts in all */
  ulong_t mVertsAlloc ;                 /* chunks, ie. maximal possible number. */
  
  numberedType_e numberedType ;     /* One of root, leaf, invNum. */
  ulong_t mElemsNumbered ;              /* Number of active (ie. numbered) element. */
  ulong_t mElemsOfType[MAX_ELEM_TYPES] ;/* The number of numbered elements of each type. */
  ulong_t mElems_w_mVerts[MAX_DRV_ELEM_TYPES] ; /* Number of elems per nr of vertices.
                                               The first slots are for the base 
                                               elements.*/
  ulong_t mVertsNumbered ;
  ulong_t nHighestElemNumber ;	 
  ulong_t nHighestVertNumber ;	    /* Highest vertex number used. */
  color_s *pVxColor ;               /* A color for each vertex. */


    /* Marks. */
  int useElemMark[SZ_ELEM_MARK] ;               /* 1 if mark in pVrtx is used. */
  char useElemMarkBy[SZ_ELEM_MARK][LINE_LEN]  ; /* debugging indicater which routine is using it. */
  //  int useElemMark1 ;              /* 1 if mark in pVrtx is used. */
  //char useElemMark1By[LINE_LEN] ; /* debugging indicater which routine is using it. */
  //int useElemMark2 ;              /* 1 if mark in pVrtx is used. */
  //char useElemMark2By[LINE_LEN] ; /* debugging indicater which routine is using it. */


  /* Flag activity. */
  int vxFlag1Active ;              /* 0 = flag is off. */
  char vxFlag1UsedBy[LINE_LEN] ;   /* Who is using this flag at the moment? For debug. */

  /* Marks. */  
  int useVxMark ;               /* 1 if mark in pVrtx is used. */
  char useVxMarkBy[LINE_LEN]  ; /* debugging indicater which routine is using it. */
  int useVxMark2 ;              /* 1 if mark in pVrtx is used. */
  char useVxMark2By[LINE_LEN] ; /* debugging indicater which routine is using it. */
  int useVxMark3 ;              /* 1 if mark in pVrtx is used. */
  char useVxMark3By[LINE_LEN] ; /* debugging indicater which routine is using it. */


  varList_s varList ;              /* # of unknowns, type ...  list of vars. */

  int mBc ;                         /* Number of all patches. */
  int mBcBnd ;                      /* Number of bnd patches with bnd geoType. */
  bc_struct **ppBc ;                /* List of bcs[0]. */
  bndPatch_struct **ppRootPatchBc ; /* List of root boundary patches[0]. */


  ulong_t mBndFcVx ;
  bndFcVx_s *pBndFcVx ;             /* If boundary faces are given by vertices, find
                                       the element in check_conn. List it here
                                       temporarily. */
  

  /* Temp storage for boundary node to node pointers and their index, 
     if bc patches are given as a list of nodes. */
  int mBvx2Vx ;
  /* This will be read e.g. from hdf as int. */
  int *pnBvx2Vx ;
  int *pnBvx2Vx_fidx ; // points to sized as mBc+1.




  ulong_t *pmVertBc ;                   /* For each bc: number of bnd vertices. */
  ulong_t *pmBiBc ;                     /*                        linear bnd faces. */
  ulong_t *pmTriBc ;                    /*                        triangular bnd faces. */
  ulong_t *pmQuadBc ;                   /*                        quad bnd faces. */
  ulong_t *pmFaceBc ;                   /*                        all bnd faces. */

  ulong_t mVertAllBc ;                  /* Total counters over all bc. */
  ulong_t mBiAllBc ;
  ulong_t mTriAllBc ;
  ulong_t mQuadAllBc ;
  ulong_t mFaceAllBc ;                  /* All types of faces. */
				 
  ulong_t mVertAllInter ;                  /* Total counters over all interfaces. */
  ulong_t mBiAllInter ;
  ulong_t mTriAllInter ;
  ulong_t mQuadAllInter ;
  ulong_t mFaceAllInter ;                  /* All types of faces on interfaces. */



  /* Element zones, and associated parameters. */
  int mZones ;
  zone_s *pZones[MAX_ZONES+1] ; /* pZone[0] will remain empty, runs from 1. */

  
  /* Adaptation. */
  llEdge_s *pllAdEdge ;             /* A linked list of adapted edges. */
  adEdge_s *pAdEdge ;               /* And how they are adapted. */
  int isBuffered ;                  /* 0 if unbuffered, i.e. hanging nodes. */
                   
  llEdge_s *pllEdge ;               /* A list of edges for edge-weights. */
       
  llToElem_s *pllVxToElem ;         /* A list of elements formed by each vertex. */
       
  uns_s *pUnsFine ;                 /* The next finer mesh. */
  uns_s *pUnsCoarse ;               /* The next coarser mesh. */
  uns_s *pUnsFinest ;               /* The next finer mesh. */

  edgeLen_s *pEgLen ;               /* permissible edge-lengths on it. */
  
  ulong_t mVxCollapseTo ;               /* Size of pnVxCollapseTo[1]. */
  ulong_t *pnVxCollapseTo ;             /* For each vertex in the current mesh, the vertex 
                                       number in the next coarser mesh that it collapses
                                       into. */
  elem_struct **ppElContain ;     /* For each vertex in the current mesh, the element 
                                       in the next coarser mesh that contains
                                       it. Numbered from 1. */
  double *pElContainVxWt ;           /* Interpolation weights for the vertices of the
                                         coarse grid element that contains a fine grid 
                                         vx. Numbered from zero. */


  /* Periodicity */
  int mPerBcPairs ;                 /* Number of periodic pairs of bcs. */
  perBc_s *pPerBc ;                 /* List of periodic operations. */

  ulong_t mPerVxPairs ;                 /* How many 'pairs' of periodic vertices?*/
  perVxPair_s *pPerVxPair ;
  int multPer ;                     /* 1, if there is multiple periodicity. */
  // JDM: Sep 2017: we can have multiple periodic angles, e.g. opposite signs.
  // remove ref from write_cgns.
  //transf_e per_tr_op ;                  /* relevant here: trans, or rot_x, rot_y, rot_z */
  //double perAngleRad ;              /* Rotation angle in radian for single periodic cases. */


  ulong_t mSymmVx ;                     /* A list of symmetric vertices. */
  vrtx_struct **ppSymmVx ;
  

  ulong_t mCutElems ;                   /* Count elements to be cut up to reduce angles.
                                       To be removed once we can realloc elems. */
  ulong_t mCutBndFc ;                   /* The number of boundary faces with these elems. */

  restart_u restart ;  /* Some bollocks to initialise solution files for restarts. */ 


  /* Sliding and Mixing planes. */
  int mSlidingPlaneSides ;
  /* A list of pointers, so we can alloc for one side and move it to another
     grid without affecting pointers to the side. */
  slidingPlaneSide_s **ppSlidingPlaneSide ;

  int mSlidingPlanePairs ;
  slidingPlanePair_s *pSlidingPlanePair ;


  /* Geometry definition for one hyperplane/volume.
     For now only one. We could have a linked list of them, similar to Grids.hrb,
     but this needs many interface functions. Add when needed. */
#define MAX_HYVOL (1)
  int mHyVol ;
  geo_s pHyVol[MAX_HYVOL] ;


  
  /* Stacks, e.g. prisms normal to a boundary. 
     Not used, yet. */
  ulong_t mStackFcOfType[MAX_ELEM_TYPES] ;
  
  arr_s *pArrStackFc ; // array wrapper for pStackFc.
  stackFc_s *pStackFc ; // Storage for the multipe inked Lists


  arr_s *pArrStackFcBeg ; // Array wrapper
  stackFcTerm_s *pStackFcBeg ; // Root for each list on the bnd.
  arr_s *pArrStackFcEnd ; // Array wrapper
  stackFcTerm_s *pStackFcEnd ; // End for each list on the hybrid interface
} ;
