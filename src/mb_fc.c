/*
   mb_fc.c:
   Create unstructured grid faces from a structured multiblock grid. 
 
   Last update:
   ------------
   10Sep97; allow Pbl->PelemMark = NULL ;
   15Nov96; conceived, cut out of mb_2uns.c.

   Contains:
   ---------
   get_mbBndFc:
   get_mbIntFc:
   get_mbDegenFc:
   
*/

#include "cpre.h"
#include "cpre_mb.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_mb_uns.h"
#include "proto_mb.h"
#include "proto_uns.h"

/*************************************************************************
  
 get_mbBndFc:
 get all boundary faces that are included in a cut from a block.
 Note that interior faces do not count as boundary faces.

 Last update:
 ------------
 10Sep97; allow for Pbl->PelemMark = NULL, consider all marked in that case.
 5May96: intro bndPatch.
 
 Input:
 ------
 PBL:       structured grid block,
 mDim:      dimensions,
 Pchunk:    current unstructured chunk,

 
 Changes to:
 -----------
 Pchunk:

 Returns:
 --------
 0: on failure,
 1: on success.

*/

int get_mbBndFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk )
{
  int mBndFaces, nSubFace, nCell, *elemMrk, mBndPatches, mPatchFaces ;
  subFace_struct *PSF ;
  int ll[MAX_DIM], ur[MAX_DIM] ;
  int index1, multVert1, multCell1, index2, multVert2, multCell2,
    offsetVert, offsetCell, indexStatic, dll, dlr, dur, dul, n1, n2, nBc ;
  bndFc_struct *PbndFc ;
  bndPatch_struct *PbndPatch ;

  /* Aliases for the block. */
  elemMrk = PBL->PelemMark ;

  /* Count all external boundary faces. */
  mBndFaces = mBndPatches = 0 ;
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace )
  { PSF = PBL->PPsubFaces[nSubFace] ;
    if ( PSF->Pbc )
    { /* This subface does have a boundary condition. */
      mBndPatches++ ;
      mPatchFaces = 0 ;
      
      /* Get the 2-D surface parameters for this blockside. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      /* Loop over the subface. */
      n2 = ll[index2] ;
      n1 = ll[index1] - 1 ; 
      while ( nCell = cell_loop_subfc ( ll, ur, mDim,
				        &n1, index1, multCell1, &n2, index2, multCell2,
				        offsetCell ) )
	if ( !elemMrk || elemMrk[nCell] ) 
	{ mBndFaces++ ;
	  mPatchFaces++ ;
	}
      
      if ( !mPatchFaces )
        /* There are no faces of this boundary condition left. Remove it. */
	mBndPatches-- ;
      else {
	/* Add this bc to the list with the chunk. */
	for ( nBc = 0 ; nBc < Pchunk->mBc ; nBc++ )
	  if ( Pchunk->PPbc[nBc] == PSF->Pbc )
	    break ;

	if ( nBc >= Pchunk->mBc ) {
	  /* No such bc yet with this chunk. Add it. */
	  Pchunk->PPbc =
            arr_realloc ( "Pchunk->PPbc in get_mbBndFc",  Pchunk->pUns->pFam, Pchunk->PPbc,
                          ++Pchunk->mBc, sizeof( bc_struct * ) ) ;
	  Pchunk->PPbc[ Pchunk->mBc-1 ] = PSF->Pbc ; }
      }
	    
    }
  }
  Pchunk->mBndPatches = mBndPatches ;

  /* Alloc for boundary faces. */
  if ( mBndFaces ) {
    Pchunk->PbndFc = arr_malloc ( "Pchunk->PbndFc in get_mbBndFc", Pchunk->pUns->pFam,
                                  mBndFaces+1, sizeof( bndFc_struct ) ) ;
    Pchunk->PbndPatch = arr_malloc ( "Pchunk->PbndPatch in get_mbBndFc", Pchunk->pUns->pFam,
                                     mBndPatches+1, sizeof( bndPatch_struct ) ) ;
  }
  else {
    Pchunk->mBndFaces = mBndFaces ;
    return ( 1 ) ; }
  
  PbndFc = Pchunk->PbndFc ;
  PbndPatch = Pchunk->PbndPatch ;
  /* Write to the allocated space starting with PbndPatch[1]. */
  PbndPatch->Pchunk = NULL ;
  /* PbndPatch->PprvBndPatch = NULL ;
     PbndPatch->PnxtBndPatch = PbndPatch + 1 ;
  */
  PbndPatch->PprvBcPatch = NULL ;
  PbndPatch->PnxtBcPatch = NULL ;
  PbndPatch->Pbc = NULL ;
  PbndPatch->PbndFc = PbndFc + 1 ;
  PbndPatch->mBndFc = 0 ;
  
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace ) {
    PSF = PBL->PPsubFaces[nSubFace] ;
    if ( PSF->Pbc ) {
      /* This subface does have a boundary condition. */
      PbndPatch++ ;
      mPatchFaces = 0 ;
      
      /* Get the 2-D surface parameters for this blockside. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      /* Loop over the subface. */
      n2 = ll[index2] ;
      n1 = ll[index1] - 1 ; 
      while ( nCell = cell_loop_subfc ( ll, ur, mDim,
				        &n1, index1, multCell1, &n2, index2, multCell2,
				        offsetCell ) )
	if ( !elemMrk || elemMrk[nCell] ) {
	  /* Add this face to this patch. */
	  mPatchFaces++ ;
	  PbndFc++ ;
	  
#         ifdef CHECK_BOUNDS
	  if ( PbndFc - Pchunk->PbndFc > mBndFaces ) {
	      printf ( " FATAL: %d is beyond PbndFc' bounds of %d in get_mbVerts\n",
		       PbndFc - Pchunk->PbndFc, mBndFaces ) ;
	      return ( 0 ) ; }
#         endif
	  
	  PbndFc->Pelem = Pchunk->Pelem + ( elemMrk ? elemMrk[nCell] : nCell ) ;
	  PbndFc->nFace = get_faceNr_ijk ( indexStatic, ll[indexStatic], mDim ) ;
	  PbndFc->Pbc = PSF->Pbc ;
	}

      /* Count the number of faces in this patch. */
      if ( mPatchFaces ) {
	/* There are faces in this patch. */

#       ifdef CHECK_BOUNDS
          if ( PbndPatch - Pchunk->PbndPatch > mBndPatches )
	  { printf ( " FATAL: %d is beyond PbndPatch's bounds of %d in get_mbBndFc\n",
		     PbndPatch - Pchunk->PbndPatch, mBndPatches ) ;
	    return ( 0 ) ;
	  }
#       endif

	PbndPatch->Pchunk = Pchunk ;
	
	/* PbndPatch->PprvBndPatch = PbndPatch - 1 ;
	   PbndPatch->PnxtBndPatch = NULL ;
	   PbndPatch->PprvBndPatch->PnxtBndPatch = PbndPatch ;
	*/
	
	PbndPatch->Pbc = PSF->Pbc ;
	PbndPatch->PbndFc = PbndFc - mPatchFaces + 1 ;
      
	PbndPatch->mBndFc = mPatchFaces ;
      }
      else
        /* Remove this patch. */
	PbndPatch-- ;
    }
  }

  Pchunk->mBndFaces = mBndFaces ;
  return ( 1 ) ;
}
  

/**********************************************************************

  get_mbIntFc:
  get internal interfaces, be it from a block interface or from a cut
  through a block. Note that there are no interfaces listed between
  adjacent blocks if the grid is continuous. Only where a structured
  grid cell has been cut, an interface is created.
  Imagine the interfaces as a surface grid for the whole created by
  the cut.

  Last update:
  ------------
  10Sep97; allow for Pbl->PelemMark to be NULL, then all is marked.
  
  Input:
  ------
  PBL:        structured grid block,
  mDim:       dimensions,
  Pchunk:     current unstructured chunk,
  
  Changes to:
  -----------
  Pchunk:

  Returns:
  --------
  0: on failure,
  1: on success.

*/

int get_mbIntFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk )
{
  int mIntFaces, nSubFace, mVert[MAX_DIM], *elemMrk ;
  subFace_struct *PSF ;
  int ll[MAX_DIM], ur[MAX_DIM] ;
  int index1, multVert1, multCell1, index2, multVert2, multCell2,
      offsetVert, offsetCell, indexStatic,
      dll, dlr, dur, dul, n1, n2 ;
  int nCell, nDim ;
  intFc_struct *PintFc ;
  block_struct *PBLO ;
  int ijk[MAX_DIM], ijkO[MAX_DIM], nCellO, dCell ;

  if ( !PBL->PelemMark ) {
    /* No element marker, thus no interfaces. */
  /* Alloc for interior interfaces. */
    Pchunk->PintFc = NULL ;
    Pchunk->mIntFaces = 0 ;
    return ( 1 ) ; }


  /* Aliases for the block quantities. */
  elemMrk = PBL->PelemMark ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    mVert[nDim] = PBL->mVert[nDim] ;
  
  /* Count all internal boundary faces between blocks. Note that there
     is such an interface only if there is a marked cell on one side of
     the interface and an unmarked on the other. If both cells are marked,
     there is no interface, the grid is complete. If neither of the cells
     is marked, there is obviously no interface. */
  mIntFaces = 0 ;
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace )
  { PSF = PBL->PPsubFaces[nSubFace] ;
    if ( PSF->PrBlock )
    { /* This subface is internal. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      if ( PSF->PlBlock == PBL )
	/* The other block is on the right. */
	PBLO = PSF->PrBlock ;
      else
	/* The other block is on the left. */
	PBLO = PSF->PlBlock ;
	    
      /* Loop over the subface. */
      if ( ll[indexStatic] == 1 )
	/* -ijk face. */
	ijk[indexStatic] = ll[indexStatic] ;
      else
	/* -ijk face. Cells run one index less. */
	ijk[indexStatic] = ll[indexStatic]-1 ;
      
      /* Loop over the subface. */
      n2 = ll[index2] ;
      n1 = ll[index1] - 1 ; 
      while ( nCell = cell_loop_subfc ( ll, ur, mDim,
				        &n1, index1, multCell1, &n2, index2, multCell2,
				        offsetCell ) )
	if ( elemMrk[nCell] )
	{ /* This cell is marked. Find out about the status of
	     the cell on the other side of the interface. For that,
	     calculate the indices nI,nJ,nK of the llf node of the cell,
	     translate them to the other block and calculate
	     the linear cell number on the other side. */
	  ijk[index1] = n1 ;
	  ijk[index2] = n2 ;
	  
	  if ( PSF->PlBlock == PBL )
	    /* The other block is on the right. */
	    trans_l2r ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	  else
	    /* The other block is on the left. */
	    trans_r2l ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	  nCellO = get_nElem_ijk ( mDim, ijkO, PBLO->mVert ) ;
	  
	  if ( !PBLO->PelemMark[nCellO] )
	    /* The cell on the other side of the subface is not marked.
	       This is an internal interface. */
	    mIntFaces++ ;
	}
    }
  }
  
  /* Add the number of internal faces created by cutting the block.
     Loop over the interior faces in every direction, dCell being the
     difference in cell number across the interior face. */
  dCell = 1 ; 
  nDim = 0, index1 = 1, index2 = 2 ;
  ijk[0] = ijk[1] = ijk[2] = 1 ;
  while ( nCell =  face_loop_block ( mDim, mVert,
				     ijk, &index1, &index2, &nDim, &dCell ) )
  { nCellO = nCell + dCell ;
    if ( !elemMrk[nCell] && elemMrk[nCellO] )
      /* One of the cells is not marked, this is an interface. */
      mIntFaces++ ;
    else if ( elemMrk[nCell] && !elemMrk[nCellO] )
      /* One of the cells is not marked, this is an interface. */
      mIntFaces++ ;
  }

  /* Alloc for interior interfaces. */
  if ( mIntFaces )
    Pchunk->PintFc = arr_malloc ( " Pchunk->PintFc in get_mbIntFc", Pchunk->pUns->pFam,
                                  mIntFaces+1, sizeof( intFc_struct ) ) ;
  else {
    Pchunk->mIntFaces = mIntFaces ;
    return ( 1 ) ;
  }

  /* List the interior interfaces into PintFc, starting for aesthetics
     with PintFc[1]. */
  PintFc = Pchunk->PintFc ;
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace )
  { PSF = PBL->PPsubFaces[nSubFace] ;
    if ( PSF->PrBlock )
    { /* This subface is internal. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      if ( PSF->PlBlock == PBL )
	/* The other block is on the right. */
	PBLO = PSF->PrBlock ;
      else
	/* The other block is on the left. */
	PBLO = PSF->PlBlock ;
	    
	    
      /* Loop over the subface. */
      if ( ll[indexStatic] == 1 )
	/* -ijk face. */
	ijk[indexStatic] = ll[indexStatic] ;
      else
	/* -ijk face. Cells run one index less. */
	ijk[indexStatic] = ll[indexStatic]-1 ;

      /* Loop over the subface. */
      n2 = ll[index2] ;
      n1 = ll[index1] - 1 ; 
      while ( nCell = cell_loop_subfc ( ll, ur, mDim,
				        &n1, index1, multCell1, &n2, index2, multCell2,
				        offsetCell ) )
	if ( elemMrk[nCell] )
	{ /* This cell is marked. Find out about the status of
	     the cell on the other side of the interface. For that,
	     calculate the indices nI,nJ,nK of the llf node of the cell,
	     translate them to the other block and calculate
	     the linear cell number on the other side. */
	  ijk[index1] = n1 ;
	  ijk[index2] = n2 ;
	  
	  if ( PSF->PlBlock == PBL )
	    /* The other block is on the right. */
	    trans_l2r ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	  else
	    /* The other block is on the left. */
	    trans_r2l ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	  nCellO = get_nElem_ijk ( mDim, ijkO, PBLO->mVert ) ;
	  
	  if ( !PBLO->PelemMark[nCellO] )
	  { /* The cell on the other side of the subface is not marked.
	       This is an internal interface. */
	    PintFc++ ;
	    
#           ifdef CHECK_BOUNDS
	      if ( PintFc - Pchunk->PintFc > mIntFaces )
	      { printf ( " FATAL: %d is beyond PintFc' bounds of %d in get_mbIntFc\n",
			 PintFc - Pchunk->PintFc, mIntFaces ) ;
		return ( 0 ) ;
	      }
#           endif
	      
	    PintFc->Pelem = Pchunk->Pelem + elemMrk[nCell] ;
	    PintFc->nFace =
	      get_faceNr_ijk ( indexStatic, ll[indexStatic], mDim ) ;
	  }
	}
    }
  }

  /* List the interior interfaces from cut blocks. Note that PintFc has been
     accumulated in the loop of block interfaces. */
  dCell = 1 ; 
  nDim = 0, index1 = 1, index2 = 2 ;
  ijk[0] = ijk[1] = ijk[2] = 1 ;
  while ( nCell =  face_loop_block ( mDim, mVert,
				     ijk, &index1, &index2, &nDim, &dCell ) )
  { nCellO = nCell + dCell ;

    if ( elemMrk[nCell] && !elemMrk[nCellO] )
    { /* Only one of the cells is not marked, this is an interface. */
      PintFc++ ;
      
#     ifdef CHECK_BOUNDS
        if ( PintFc - Pchunk->PintFc > mIntFaces )
	{ printf ( " FATAL: %d is beyond PintFc' bounds of %d in get_mbIntFc\n",
		   PintFc - Pchunk->PintFc, mIntFaces ) ;
	  return ( 0 ) ;
	}
#     endif

      PintFc->Pelem = Pchunk->Pelem + elemMrk[nCell] ;
      /* Recall that get_faceNr_ijk assumes the -ijk interface
	 for index=1. The interface between nCell and nCellO is
	 +nDim for nCell. */
      PintFc->nFace = get_faceNr_ijk ( nDim, 99, mDim ) ;
    }
    else if ( !elemMrk[nCell] && elemMrk[nCellO] )
    { /* Only one of the cells is not marked, this is an interface. */
      PintFc++ ;
      
#     ifdef CHECK_BOUNDS
        if ( PintFc - Pchunk->PintFc > mIntFaces )
	{ printf ( " FATAL: %d is beyond PintFc' bounds of %d in get_mbIntFc\n",
		   PintFc - Pchunk->PintFc, mIntFaces ) ;
	  return ( 0 ) ;
	}
#     endif

      PintFc->Pelem = Pchunk->Pelem + elemMrk[nCellO] ;
      /* -nDim interface for nCellO. */
      PintFc->nFace = get_faceNr_ijk ( nDim, 1, mDim ) ;
    }
  }
  Pchunk->mIntFaces = mIntFaces ;
  return ( 1 ) ;
}
/**********************************************************************

  get_mbDegenFc:
  Get degenerate subfaces.

  Last update:
  14Oct96: derived from get_mbMatchFc.
  
  Input:
  ------
  PBL:        structured grid block,
  mDim:       dimensions,
  Pchunk:     current unstructured chunk,
  
  Changes to:
  -----------
  Pchunk:

  Returns:
  --------
  0: on failure,
  1: on success.

*/

int get_mbDegenFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk )
{
  int mDegenFaces=0, nSubFace ;
  subFace_struct *PSF ;
  degenFc_struct *PdegenFc ;
  int ll[MAX_DIM], ur[MAX_DIM] ;
  int index1, multVert1, multCell1, index2, multVert2, multCell2,
      offsetVert, offsetCell, indexStatic,
      dll, dlr, dur, dul, n1, n2 ;
  int ijk[MAX_DIM], nCell ;

  /* Count all degenerate faces included in the cut. */
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace )
  { PSF = PBL->PPsubFaces[nSubFace] ;
    if ( !PSF->PrBlock && !PSF->Pbc )
    { /* This subface is degenerate. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
	  
      /* Find the proper static bound. */
      if ( ll[indexStatic] == 1 )
	/* -ijk face. */
	ijk[indexStatic] = ll[indexStatic] ;
      else
	/* +ijk face. */
	ijk[indexStatic] = ll[indexStatic]-1 ;
      
      /* Loop over the subface. */
      n2 = ll[index2] ;
      n1 = ll[index1] - 1 ; 
      while ( nCell = cell_loop_subfc ( ll, ur, mDim,
				        &n1, index1, multCell1, &n2, index2, multCell2,
				        offsetCell ) )
	if ( PBL->PelemMark[nCell] )
	  mDegenFaces++ ;
    }
  }

  if ( !mDegenFaces )
  { /* There were no or there are no degenerate faces left. */
    Pchunk->mDegenFaces = 0 ;
    Pchunk->PdegenFc = NULL ;
    return ( 1 ) ;
  }
  
  /* Alloc for matching interfaces. */
  PdegenFc = Pchunk->PdegenFc =
    arr_malloc ( "Pchunk->PdegenFc in get_mbDegenFc", Pchunk->pUns->pFam,
                 mDegenFaces+1, sizeof( degenFc_struct ) ) ;

  /* List the interior interfaces into PdegenFc, starting for aesthetics
     with PdegenFc[1]. */
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace )
  { PSF = PBL->PPsubFaces[nSubFace] ;
    if ( !PSF->PrBlock && !PSF->Pbc )
    { /* This subface is degenerate. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
	  
      /* Find the proper static bound. */
      if ( ll[indexStatic] == 1 )
	/* -ijk face. */
	ijk[indexStatic] = ll[indexStatic] ;
      else
	/* +ijk face. */
	ijk[indexStatic] = ll[indexStatic]-1 ;
      
      /* Loop over the subface. */
      n2 = ll[index2] ;
      n1 = ll[index1] - 1 ; 
      while ( nCell = cell_loop_subfc ( ll, ur, mDim,
				        &n1, index1, multCell1, &n2, index2, multCell2,
				        offsetCell ) )
	if ( PBL->PelemMark[nCell] )
	{ PdegenFc++ ;

#         ifdef CHECK_BOUNDS
	    if ( PdegenFc > Pchunk->PdegenFc + mDegenFaces )
	    { printf ( " FATAL: beyond bounds in get_mbDegenFc.\n" ) ;
	      return ( 0 ) ;
	    }
#         endif
	  
	  PdegenFc->Pelem = Pchunk->Pelem + PBL->PelemMark[nCell] ;
	  PdegenFc->nFace =
	    get_faceNr_ijk ( indexStatic, ll[indexStatic], mDim ) ;
	}
    }
  }

  Pchunk->mDegenFaces = mDegenFaces ;
  return ( 1 ) ;
}

