/*
  fix_uns_elem.c:
  Fix degenerate elements in an unstructured grid.
 
  Last update:
  ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  11Apr00; cleanup, change simplify_all_elems to use match_bndFc intro quad22tris.
  16Oct96; cut out of check_uns.c.

  Contains:
  ---------
  fix_elem:

  hex22prisms:
  hex2prism:
  elem2tetsNpyrs:

  qua22tris
  simplify_one_elem:
  simplify_all_elems:

  make_tet:
  make_pyr:

  check_elem_space:
  check_vrtx_space:

  fix_boundFace:
  fix_intFace:
  fix_matchFace:
  update_face:
  simplify_one_elem ;
*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;
extern const elemType_struct elemType[] ;


/******************************************************************************

  fix_elem:
  Take a degenerate elem and try to convert it to a simplex with fewer
  edges. Currently supported:
  Hex -> prism.
  
  Last update:
  ------------
  8Jul96: conceived.
  
  Input:
  ------
  Pelem: pointer to the degenerate element.
  mDim:  

  Changes To:
  -----------
  *Pelem:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fix_elem ( elem_struct *Pelem, int mDegenEdges, 
	       chunk_struct *Pchunk, uns_s *pUns, chunk_struct *PlstChunk, 
	       elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert,
	       vrtx_struct **PPlstVrtx, double **PPlstCoor ) {
  
  int mVerts = elemType[Pelem->elType].mVerts ;
    
  if ( mVerts == 8 ) {
    /* This is a hex. */
    if ( mDegenEdges == 2 ) {
      /* with two collapsed edges. Find out how they are related.
         The case that we're trying to deal with here is the one
	 of two opposing edges on the same face, collapsing the
	 hex to a prism. */
      if ( !hex2prism ( Pelem, Pchunk, pUns ) )
	/* Failed. Cut up into tets and pyramids. */
	elem2tetsNpyrs ( Pelem, Pchunk, pUns, mDegenEdges,
		         PlstChunk, PPlstElem, PPPlstElem2Vert, PPlstVrtx, PPlstCoor ) ;
    }
    else
      /* Cut up. */
      elem2tetsNpyrs ( Pelem, Pchunk, pUns, mDegenEdges,
		       PlstChunk, PPlstElem, PPPlstElem2Vert, PPlstVrtx, PPlstCoor ) ;
  }
  else
    /* Can't deal with this type of degenerate element. */
    return ( 0 ) ;

  return ( 1 ) ;
}

/******************************************************************************

  hex2prism:
  Convert a hex with two degenerate edges to a prism.
  
  Last update:
  ------------
  16Oct96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hex2prism ( elem_struct *Pelem, chunk_struct *Pchunk, uns_s *pUns ) {
  
  vrtx_struct **PPvxFc[2*MAX_VX_FACE], **PPvxNewFc[MAX_VX_FACE+1],
              *PPvxNewEl[MAX_VX_ELEM] ;
  int kFace, mDgEdgeFace, fcType, kEdge, kDgEdge[4], kVrtx,
      kFcNewElemFc[MAX_FACES_ELEM+1], kFc,
      kNewFace, found=0, kNewVx, newFcType, kOldVx ;
  /* This is fixed. */
  const int mDim = 3 ;
  elem_struct newElem, *PnewElemFc[MAX_FACES_ELEM+1] ;
  
  /* Loop over all six faces of this hex. */
  for ( mDgEdgeFace = 0, kFace = 1 ; kFace <= 6 && mDgEdgeFace != 2 ; kFace++ )
  { get_uns_face ( Pelem, kFace, PPvxFc, &fcType ) ;
    /* Append the first vertex of the face for cyclic rotation.
       Note: PPvxFc starts at 0 ;-(.*/
    PPvxFc[fcType] = PPvxFc[0] ;
      
    /* Count the number of degenerate edges on this face. Have kDgEdge
       point to the end of the degenerate edge, ie. the second occurence
       of the duplicate vertex pointer.*/
    for ( mDgEdgeFace = 0, kEdge = 0 ; kEdge < fcType ; kEdge++ )
      if ( *PPvxFc[kEdge] == *PPvxFc[kEdge+1] )
      { mDgEdgeFace++ ;
	kDgEdge[mDgEdgeFace] = kEdge+1 ;
      }
  }
  
  if ( mDgEdgeFace == 2 )
  { /* This is the face. */
	
    if ( kDgEdge[2] - kDgEdge[1] == 2 )
    { /* The collapsed edges are opposed on this face. Make
	 the prism edge 1-2 out of the face. */
      PPvxNewEl[0] = *PPvxFc[ kDgEdge[1] ] ;
      PPvxNewEl[1] = *PPvxFc[ kDgEdge[2] ] ;
      
      /* Find the triangular faces with one degenerate edge. */
      /* Loop over all six faces of this hex. */
      for ( mDgEdgeFace = 0, kFace = 1 ; kFace <= 6 ; kFace++ )
      { get_uns_face ( Pelem, kFace, PPvxFc, &fcType ) ;
	/* Append the first vertex of the face for cyclic rotation.
	   Note: PPvxFc starts at 0 ;-(.*/
	PPvxFc[fcType] = PPvxFc[0] ;
	
	/* Count the number of degenerate edges on this face. */
	for ( mDgEdgeFace = 0, kEdge = 0 ; kEdge < fcType ; kEdge++ )
	  if ( *PPvxFc[kEdge] == *PPvxFc[kEdge+1] )
	  { mDgEdgeFace++ ;
	    kDgEdge[mDgEdgeFace] = kEdge+1 ;
	  }
	
	if ( mDgEdgeFace == 1 )
	{ /* Complete the cyclic succession. */
	  PPvxFc[fcType+1] = PPvxFc[1] ;
	  PPvxFc[fcType+2] = PPvxFc[2] ;
	  PPvxFc[fcType+3] = PPvxFc[3] ;

	  if ( *PPvxFc[ kDgEdge[1] ] == PPvxNewEl[0] )
	  { /* This face is connected to vertex 1, thus we have the
	       triangular face 4-1-6. */
	    PPvxNewEl[5] = *PPvxFc[ kDgEdge[1] + 1 ] ;
	    PPvxNewEl[3] = *PPvxFc[ kDgEdge[1] + 2 ] ;
	  }
	  else if ( *PPvxFc[ kDgEdge[1] ] == PPvxNewEl[1] )
	  { /* This face is connected to vertex 2, thus we have the
	       triangular face 2-3-5. */
	    PPvxNewEl[2] = *PPvxFc[ kDgEdge[1] + 1 ] ;
	    PPvxNewEl[4] = *PPvxFc[ kDgEdge[1] + 2 ] ;
	  }
	  else
	  { printf  ( "FATAL: this shouldn\'t have happened in fix_elem.\n" ) ;
	    exit ( EXIT_FAILURE ) ;
	  }
	}
      }

      /* Form an element with the prism. */
      newElem.PPvrtx = PPvxNewEl ;
      newElem.elType = pri ;
	
      /* Establish a list of equivalencies between the faces of the
	 old hex and the new prism. */
      for ( kFc = 1 ; kFc <= 6 ; kFc++ )
      { get_uns_face ( Pelem, kFc, PPvxFc, &fcType ) ;
	PPvxFc[fcType] = PPvxFc[0] ;
	
	/* Check for degeneracies along the edges of this face. */
	for ( mDgEdgeFace = 0, kEdge = 0 ; kEdge < fcType ; kEdge++ )
	  if ( *PPvxFc[kEdge] == *PPvxFc[kEdge+1] )
	    mDgEdgeFace++ ;
	
	if ( fcType - mDgEdgeFace < 3 )
	{ /* This face has shrunk to an edge or a point. Slate it for removal. */
	  kFcNewElemFc[kFc] = 0 ;
	  PnewElemFc[kFc] = NULL ;
	}
	else 
	{ /* The element that the face is pointing to has changed, find back
	     the reduced face in the new element. */
	  
	  /* Loop over all the faces of the new element. */
	  for ( kNewFace = 1 ;
	        get_uns_face ( &newElem, kNewFace, PPvxNewFc, &newFcType ) ;
	        kNewFace ++ )
	  {
	    /* Check whether the face of the reduced element coincides with
	       the degenerate face. Do this by looping over all vertices of the
	       old face and finding them in the new face. */
	    for ( found = 1, kOldVx = 0 ; kOldVx < fcType && found ; kOldVx++ )
	      for ( found = 0, kNewVx = 0 ; kNewVx < newFcType ; kNewVx++ )
		if ( *PPvxFc[kOldVx] == *PPvxNewFc[kNewVx] )
		  found = 1 ;
	    if ( found )
	    { /* All vertices are matched, this is it. */
	      kFcNewElemFc[kFc] = kNewFace ;
	      PnewElemFc[kFc] = Pelem ;
	      break ;
	    }
	  }
	  
	  /* No match found. */
	  if ( !found )
	  { printf ( " WARNING: no matching faces found in update_face.\n" ) ;
	    return ( 0 ) ;
	  }
	}
      }

      /* Fix the boundary faces of this element. */
      fix_boundFace ( Pchunk, Pelem, kFcNewElemFc, PnewElemFc ) ;
      fix_intFace ( Pchunk, Pelem, kFcNewElemFc, PnewElemFc ) ;
      fix_matchFace ( pUns, mDim, Pelem, kFcNewElemFc, PnewElemFc ) ;
      
      /* Replace the hex with the prism. Lose the two vertex slots.*/
      Pelem->elType = pri ;
      for ( kVrtx = 0 ; kVrtx < 6 ; kVrtx++ )
	Pelem->PPvrtx[kVrtx] = PPvxNewEl[kVrtx] ;
    }
    else
      /* Can't deal with consecutive collapsed edges */
      return ( 0 ) ;
  }
  else
    /* The two edges must be on opposing faces. */
    return ( 0 ) ;

  return ( 1 ) ;
}

/******************************************************************************

  elem2tetsNpyrs:
  Cut up a hex into tets and pyramids by inserting a central vertex.
  All the faces remain unchanged, thus the topology outside the hex
  does not change.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  21Oct16; guard if block as per compiler warning.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  9Jul11; filter source using ADAPT_RES
  16Oct96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int elem2tetsNpyrs ( elem_struct *Pelem, chunk_struct *Pchunk, uns_s *pUns,
		     const int mDegenEdges,
		     chunk_struct *PlstChunk, elem_struct **PPlstElem,
		     vrtx_struct ***PPPlstElem2Vert,
		     vrtx_struct **PPlstVrtx, double **PPlstCoor ) {
  
  int mDgEdgeFace, kFace, fcType, kDgEdge[4], kEdge, nDim,
      kFcNewElemFc[MAX_FACES_ELEM+1], kVrtx ;
  vrtx_struct *Pvrtx, **PPvxFc[MAX_VX_FACE+1], *Pvrtx1, *Pvrtx2 ;
  double *Pcoor ;
  const int mDim = pUns->mDim ;
  elem_struct *PnewElemFc[MAX_FACES_ELEM+1], *PnewElem ;
  const elemType_struct *PelT = elemType + Pelem->elType ;
  const edgeOfElem_struct *PEoE = PelT->edgeOfElem ;

  if ( elemType[Pelem->elType].mDim != 3 ) {
    printf ( " FATAL: elem2tetsNpyrs only does 3-D elements.\n" ) ;
    return ( 0 ) ; }

  /* Make a new vertex. */
  if ( !check_vrtx_space ( PlstChunk, PPlstVrtx, PPlstCoor, mDim ) ) return ( 0 ) ;
  Pvrtx = ++(*PPlstVrtx ) ;
  PlstChunk->mVertsNumbered++ ;
  Pvrtx->number = 1 ;
  *(PPlstCoor) += mDim ;
  Pcoor = Pvrtx->Pcoor = *(PPlstCoor) ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Pcoor[nDim] = 0. ;

  /* Loop over all physical edges and average the coordinates. This should
     bias the average such as to improve angularity. */
  for ( kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ )
  { Pvrtx1 = Pelem->PPvrtx[ PEoE[kEdge].kVxEdge[0] ] ;
    Pvrtx2 = Pelem->PPvrtx[ PEoE[kEdge].kVxEdge[1] ] ;
    if ( Pvrtx1 != Pvrtx2 ) {
      /* This is a physical edge. */
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	Pcoor[nDim] += Pvrtx1->Pcoor[nDim] + Pvrtx2->Pcoor[nDim] ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	Pcoor[nDim] /= 2*( PelT->mEdges - mDegenEdges ) ;
    }
  }

  /* Loop over all faces and connect physical ones with the
     center vertex to form a new element. */
  for ( kFace = 1 ; kFace <= 6 ; kFace++ )
  { get_uns_face ( Pelem, kFace, PPvxFc, &fcType ) ;
    /* Append the first vertex of the face for cyclic rotation.
       Note: PPvxFc starts at 0 ;-(.*/
    PPvxFc[fcType] = PPvxFc[0] ;
      
    /* Count the number of degenerate edges on this face. Have kDgEdge
       point to the end of the degenerate edge, ie. the second occurence
       of the duplicate vertex pointer.*/
    for ( mDgEdgeFace = 0, kEdge = 0 ; kEdge < fcType ; kEdge++ )
      if ( *PPvxFc[kEdge] == *PPvxFc[(kEdge+1)%fcType] )
	kDgEdge[mDgEdgeFace++] = (kEdge+1)%fcType ;

#   ifdef CHECK_CONVEX
    { double xFwd[MAX_DIM], xBkw[MAX_DIM], xCr[MAX_DIM], xVx[MAX_DIM], xAvg[MAX_DIM]  ;
      int kCorner, mAvg=0 ;
      
      if ( mDgEdgeFace < 2 ) {
        /* Check whether the new vertex is inside the polygon of physical faces.
	   For that, calculate the crossproduct of two physical edges. */
	kCorner = (kDgEdge[0]+1)%fcType ;
	vec_diff_dbl ( (*PPvxFc[kDgEdge[0]])->Pcoor,
		       (*PPvxFc[kCorner])->Pcoor, mDim, xFwd ) ;
	vec_diff_dbl ( (*PPvxFc[(kCorner+1)%fcType])->Pcoor,
		       (*PPvxFc[kCorner])->Pcoor, mDim, xBkw ) ;
	cross_prod_dbl ( xFwd, xBkw, mDim, xCr ) ;

	/* Calculate an average position on the face. */
	for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  xAvg[nDim] = 0. ;
	for ( kEdge = 0 ; kEdge < fcType ; kEdge++ )
	  if ( *PPvxFc[kEdge] != *PPvxFc[(kEdge+1)%fcType] )
	  { for ( nDim = 0 ; nDim < mDim ; nDim++ )
	      xAvg[nDim] += (*PPvxFc[kEdge])->Pcoor[nDim] ;
	    mAvg++ ;
	  }
	for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  xAvg[nDim] /= mAvg ;
	
	/* Compare the vector from the average face location to the new
	   vertex with the cross product to allow for twisted quad faces. */
	vec_diff_dbl ( Pcoor, xAvg, mDim, xVx ) ;
	if ( scal_prod_dbl ( xCr, xVx, mDim ) <= 0. ) {
          sprintf ( hip_msg, "non-convex split of elem %"FMT_ULG" chunk %d face %d"
		   " in elem2TetsNpyrs.\n", Pelem->number, Pchunk->nr, kFace ) ;
          hip_err ( warning, 1, hip_msg ) ;
	}
      }
    }
#   endif
    
    if ( mDgEdgeFace > 1 )
    { /* This face is collapsed. No cell. */
      kFcNewElemFc[kFace] = 0 ;
      PnewElemFc[kFace] = NULL ;
    }
    else if ( mDgEdgeFace == 1 )
    { /* Form a tet. The tet will be built on the triangular face as 1-2-4,
	 ie. face 4. */
      if ( !( PnewElem = make_tet ( Pelem, PPvxFc, fcType, kDgEdge,
				    Pvrtx, PlstChunk, PPlstElem, PPPlstElem2Vert ) ) )
	return ( 0 ) ;
      else
      { kFcNewElemFc[kFace] = 4 ;
	PnewElemFc[kFace] = PnewElem ;
      }
    }
    else
    { /* Form a pyramid. The pyr will be built on the quad face as 1-2-3-4,
	 ie. face 1. */
      if ( !( PnewElem = make_pyr ( Pelem, PPvxFc, fcType, 
				    Pvrtx, PlstChunk, PPlstElem, PPPlstElem2Vert ) ) )
	return ( 0 ) ;
      else
      { kFcNewElemFc[kFace] = 1 ;
	PnewElemFc[kFace] = PnewElem ;
      }
    }
  }
      
  /* Fix the boundary faces of this element. */
  fix_boundFace ( Pchunk, Pelem, kFcNewElemFc, PnewElemFc ) ;
  fix_intFace ( Pchunk, Pelem, kFcNewElemFc, PnewElemFc ) ;
  fix_matchFace ( pUns, mDim, Pelem, kFcNewElemFc, PnewElemFc ) ;
  
  /* Remove the hex. Lose the vertex slots. Note that on setting the first
     vertex pointer to zero, mark_uns_elem will set Pelem->number = 0. */
  for ( kVrtx = 0 ; kVrtx < 6 ; kVrtx++ )
    Pelem->PPvrtx[kVrtx] = NULL ;
  Pelem->invalid = 1 ;
  Pelem->term = 0 ;
#ifdef ADAPT_HIERARCHIC
  Pelem->root = Pelem->leaf = 0 ;
#endif
  
  return ( 1 ) ;
}

/******************************************************************************

  simplify_elem:
  Take a possibly degenerate element and convert it to a simpler type.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int simplify_one_elem ( elem_struct *pElDeg, elem_struct *pElSim ) {

  const faceOfElem_struct *pFoE ;
  const int *kFcEg, *kVxEg0, *kVxEg1, *kVxEgAtt ;
  int kFc, kSide, returnVal = 0, kVx, kEg, attAt, mMgVx ;
  vrtx_struct **ppVx, *ppVx2[MAX_VX_ELEM] ;

  /* Make a copy of the element. */
  *pElSim = *pElDeg ;
  ppVx = pElSim->PPvrtx ;


  if ( pElSim->elType == tri || pElSim->elType == tet )
    /* Can't simplify a simplex. */
    return ( 0 ) ;

  else if ( pElSim->elType == qua ) {

    /* Either a quad or a triangle. Store all nodes before overwriting. */
    for ( kVx = 0 ; kVx < 4 ; kVx ++ ) 
      ppVx2[kVx] = pElSim->PPvrtx[kVx] ;

    /* Loop ccw over all edges. */
    for ( mMgVx = kVx = 0 ; kVx < 4 ; kVx++ ) {
      if ( ppVx2[kVx] != ppVx2[ (kVx+1)%4 ] )
        /* Non-collapsed Edge. */
        pElSim->PPvrtx[mMgVx++] = ppVx2[kVx] ;
    }
    pElSim->elType = ( mMgVx == 3 ? tri : qua ) ;
  }

  
  if ( pElSim->elType == hex ) {
    /* A hex can be turned into a prism if two opposite edges on a face
       collapse. */
    for ( kFc = 1 ; kFc < 6 && pElSim->elType == hex ; kFc++ ) {
      pFoE = elemType[hex].faceOfElem + kFc ;
      kFcEg = pFoE->kFcEdge ;
      for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
        kVxEg0 = elemType[hex].edgeOfElem[ kFcEg[kSide]   ].kVxEdge ;
        kVxEg1 = elemType[hex].edgeOfElem[ kFcEg[kSide+2] ].kVxEdge ;

        if ( ppVx[ kVxEg0[0] ] == ppVx[ kVxEg0[1] ] && 
             ppVx[ kVxEg1[0] ] == ppVx[ kVxEg1[1] ] ) {
          /* This is such a one. Repack into a prism. Make the degenerate face
             collapse into 1-2. */
          ppVx2[0] = ppVx[ kVxEg0[0] ] ;
          ppVx2[1] = ppVx[ kVxEg1[0] ] ;

          /* Find the opposite vertices by looking at attached edges. */
          for ( kVx = 0 ; kVx < 4 ; kVx++ ) {
            kEg = pFoE->kAttEdge[kVx] ;
            attAt = pFoE->edgeAttAt[kVx] ;
            kVxEgAtt = elemType[hex].edgeOfElem[kEg].kVxEdge ;
            ppVx2[kVx+2] =  ppVx[ kVxEgAtt[1-attAt] ] ;
          }

          /* Repack into the original array and permutate the opposite face
             properly. Note that this repack is not invariant to changes of
             the definition of the canonical elements. */
          ppVx[0] = ppVx2[0] ;
          ppVx[1] = ppVx2[1] ;
          if ( kSide ) {
            ppVx[2] = ppVx2[5] ;
            ppVx[3] = ppVx2[4] ;
            ppVx[4] = ppVx2[2] ;
            ppVx[5] = ppVx2[3] ;
          }
          else {
            ppVx[2] = ppVx2[4] ;
            ppVx[3] = ppVx2[3] ;
            ppVx[4] = ppVx2[5] ;
            ppVx[5] = ppVx2[2] ;
          }

          pElSim->elType = pri ;
          returnVal = 1 ;
          break ;
        }
      }
    }
  }


  
  if ( pElSim->elType == pri ) {
    /* A prism can turn into a tet if a complete tri face collapses. */
  }

  if ( pElSim->elType == pri ) {
    /* Better approach: look at the edges attached (normal to) tri faces and
       check for collapse. */
    /* A prism can also be turned into a pyramid if one of the edges between quads
       is collapsed. Loop over the three quad faces and compare the vertices
       opposite. The vertices are not well ordered in the current AVBP nomenclature,
       the first edge can be either shared by a quad or tri. */
    for ( kFc = 1 ; kFc < 5 ; kFc++ ) {
      pFoE = elemType[pri].faceOfElem + kFc ;
      if ( pFoE->mVertsFace == 4 ) {
        for ( kVx = 0 ; kVx < 3 ; kVx++ ) {
          kEg = pFoE->kAttEdge[kVx] ;
          attAt = pFoE->edgeAttAt[kVx] ;
          kVxEgAtt = elemType[pri].edgeOfElem[kEg].kVxEdge ;
          ppVx2[kVx] = ppVx[ kVxEgAtt[ 1-attAt ] ] ;
        }
          
        if ( ppVx2[0] == ppVx2[2] && ppVx2[0] == ppVx2[2] ) { /* Why this duplication?*/
          /* This is such a one. Make this vertex the apex. */
          ppVx2[4] = ppVx2[0] ;
          for ( kVx = 0 ; kVx < 4 ; kVx++ )
            ppVx2[kVx] = ppVx[ pFoE->kVxFace[kVx] ] ;


          /* Repack. */
          ppVx[0] = ppVx2[0] ;
          ppVx[1] = ppVx2[3] ;
          ppVx[2] = ppVx2[2] ;
          ppVx[3] = ppVx2[1] ;
          ppVx[4] = ppVx2[4] ;
          
          pElSim->elType = pyr ;
          returnVal = 1 ;
          break ;
        }
      }
    }
  }



  
  if ( pElSim->elType == pyr ) {
    /* A pyramid can be turned into a tet if any edge of the quad face is collapsed. */
    for ( kFc = 1 ; kFc < 5 && pElSim->elType == pyr ; kFc++ ) {
      pFoE = elemType[pyr].faceOfElem + kFc ;
      if ( pFoE->mVertsFace == 4 ) {
        for ( kSide = 0 ; kSide < 4 ; kSide++ ) {
          if (  ppVx[ pFoE->kVxFace[kSide] ] == ppVx[ pFoE->kVxFace[ (kSide+1)%4 ] ] ) {
            /* This is such a one. Repack. Again, this repack is not invariant
               to changes of the canonical element types. */

            for ( kVx = 0 ; kVx < kSide ; kVx++ )
              ppVx2[kVx] =  ppVx[ pFoE->kVxFace[kVx] ] ;
            for ( kVx++ ; kVx < 4 ; kVx++ )
              ppVx2[kVx-1] =  ppVx[ pFoE->kVxFace[kVx] ] ;

            /* Use any edge attached to the quad face to find the apex. */
            kEg = pFoE->kAttEdge[0] ;
            attAt = pFoE->edgeAttAt[0] ;
            kVxEgAtt = elemType[pyr].edgeOfElem[kEg].kVxEdge ;
            ppVx2[3] = ppVx[ kVxEgAtt[ 1-attAt ] ] ;

            /* Repack. */
            ppVx[0] = ppVx2[0] ;
            ppVx[1] = ppVx2[1] ;
            ppVx[2] = ppVx2[2] ;
            ppVx[3] = ppVx2[3] ;
            
            pElSim->elType = tet ;
            returnVal = 1 ;
            break ;
          }
        }
      }
    }
  }
  
  return ( returnVal ) ;
}

/******************************************************************************

  quad22tris:
  Convert a quad into 2 tris.
  
  Last update:
  ------------
  11Apr00: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int qua22tris ( elem_struct *pElem, elem_struct **ppEl2, vrtx_struct ***pppVrtx, 
                        splitType_e splitType ) {

  vrtx_struct *pVx[4] ;
  double *pCo[4], egVec[4][2], r[4], scProd[4], crProd[4] ;
  int kMax ;
  elem_struct *pEl2 ;

  pVx[0] = pElem->PPvrtx[0] ;
  pVx[1] = pElem->PPvrtx[1] ;
  pVx[2] = pElem->PPvrtx[2] ;
  pVx[3] = pElem->PPvrtx[3] ;

  pCo[0] = pVx[0]->Pcoor ;
  pCo[1] = pVx[1]->Pcoor ;
  pCo[2] = pVx[2]->Pcoor ;
  pCo[3] = pVx[3]->Pcoor ;

  if ( splitType == maxAng ) {
    /* Find the maximum angle. */
    egVec[0][0] = pCo[1][0] - pCo[0][0] ;
    egVec[1][0] = pCo[2][0] - pCo[1][0] ;
    egVec[2][0] = pCo[3][0] - pCo[2][0] ;
    egVec[3][0] = pCo[0][0] - pCo[3][0] ;
    egVec[0][1] = pCo[1][1] - pCo[0][1] ;
    egVec[1][1] = pCo[2][1] - pCo[1][1] ;
    egVec[2][1] = pCo[3][1] - pCo[2][1] ;
    egVec[3][1] = pCo[0][1] - pCo[3][1] ;

    r[0] = sqrt( egVec[0][0]*egVec[0][0] + egVec[0][1]*egVec[0][1] ) ;
    r[1] = sqrt( egVec[1][0]*egVec[1][0] + egVec[1][1]*egVec[1][1] ) ;
    r[2] = sqrt( egVec[2][0]*egVec[2][0] + egVec[2][1]*egVec[2][1] ) ;
    r[3] = sqrt( egVec[3][0]*egVec[3][0] + egVec[3][1]*egVec[3][1] ) ;

    egVec[0][0] /= r[0], egVec[0][1] /= r[0] ;
    egVec[1][0] /= r[1], egVec[1][1] /= r[1] ;
    egVec[2][0] /= r[2], egVec[2][1] /= r[2] ;
    egVec[3][0] /= r[3], egVec[3][1] /= r[3] ;

    /* The edge vectors are circular. Thus, the smaller the angle,
       the smaller the scalar product. The index of the scalar product
       is related to the vertex at the corner. */
    scProd[0] = egVec[3][0]*egVec[0][0] + egVec[3][1]*egVec[0][1] ;
    scProd[1] = egVec[0][0]*egVec[1][0] + egVec[0][1]*egVec[1][1] ;
    scProd[2] = egVec[1][0]*egVec[2][0] + egVec[1][1]*egVec[2][1] ;
    scProd[3] = egVec[2][0]*egVec[3][0] + egVec[2][1]*egVec[3][1] ;
      
    crProd[0] = egVec[0][0]*egVec[1][1] - egVec[0][1]*egVec[1][0] ;
    crProd[1] = egVec[1][0]*egVec[2][1] - egVec[1][1]*egVec[2][0] ;
    crProd[2] = egVec[2][0]*egVec[3][1] - egVec[2][1]*egVec[3][0] ;
    crProd[3] = egVec[3][0]*egVec[0][1] - egVec[3][1]*egVec[0][0] ;
      
    if ( crProd[0] < 0. ) scProd[0] = 2.-scProd[0] ;
    if ( crProd[1] < 0. ) scProd[1] = 2.-scProd[1] ;
    if ( crProd[2] < 0. ) scProd[2] = 2.-scProd[2] ;
    if ( crProd[3] < 0. ) scProd[3] = 2.-scProd[3] ;

    kMax = 0 ;
    if ( scProd[1] > scProd[kMax] ) kMax = 1 ;
    if ( scProd[2] > scProd[kMax] ) kMax = 2 ;
    if ( scProd[3] > scProd[kMax] ) kMax = 3 ;

    if ( kMax == 0 || kMax == 2 ) splitType = _0_2 ;
    else                          splitType = _1_3 ;
  }
  
  else if ( splitType == ll2ur || splitType == ul2lr ) {
    /* Find the diagonal to best match the chosen direction. */
    egVec[0][0] = pCo[2][0] - pCo[0][0] ;
    egVec[1][0] = pCo[3][0] - pCo[1][0] ;
    egVec[0][1] = pCo[2][1] - pCo[0][1] ;
    egVec[1][1] = pCo[3][1] - pCo[1][1] ;

    r[0] = sqrt( egVec[0][0]*egVec[0][0] + egVec[0][1]*egVec[0][1] ) ;
    r[1] = sqrt( egVec[1][0]*egVec[1][0] + egVec[1][1]*egVec[1][1] ) ;

    egVec[0][0] /= r[0], egVec[0][1] /= r[0] ;
    egVec[1][0] /= r[1], egVec[1][1] /= r[1] ;

    if ( splitType == ll2ur )
      egVec[2][1] =  egVec[2][0] = .7071 ;
    else
      egVec[2][1] = -.7071, egVec[2][0] = .7071 ;
        

    scProd[0] = egVec[0][0]*egVec[2][0] + egVec[0][1]*egVec[2][1] ;
    scProd[1] = egVec[1][0]*egVec[2][0] + egVec[1][1]*egVec[2][1] ;

    if ( ABS( scProd[0] ) > ABS( scProd[1] ) ) splitType = _0_2 ;
    else                                       splitType = _1_3 ;
  }
    

  /* Make the new triangles. */
  (*ppEl2)++ ;
  (*pppVrtx) += 3 ;

  pEl2 = *ppEl2 ;
  pEl2->number = pElem->number ;
  pEl2->PPvrtx = *pppVrtx ;
  pEl2->elType = tri ;
  pElem->elType = tri ;
  
  if ( splitType == _0_2 ) {
    pElem->PPvrtx[0] = pVx[0] ;
    pElem->PPvrtx[1] = pVx[1] ;
    pElem->PPvrtx[2] = pVx[2] ;
    pEl2-> PPvrtx[0] = pVx[0] ;
    pEl2-> PPvrtx[1] = pVx[2] ;
    pEl2-> PPvrtx[2] = pVx[3] ;
  }
  else {
    pElem->PPvrtx[0] = pVx[0] ;
    pElem->PPvrtx[1] = pVx[1] ;
    pElem->PPvrtx[2] = pVx[3] ;
    pEl2-> PPvrtx[0] = pVx[1] ;
    pEl2-> PPvrtx[1] = pVx[2] ;
    pEl2-> PPvrtx[2] = pVx[3] ;
  }

  return ( 1 ) ;
}



/******************************************************************************

  simplify_all_elems:
  Loop over all elements, reduce. In order to recover boundary faces, place them
  all in a llFc list and recover them from the modified list of elements.
  
  Last update:
  ------------
  1Jul16; new interface to check_uns.
  4Apr13; modified interface to loop_elems.
  11Apr00; make it work for quad22tris, as well. Use match_bndFcVx for the
           recovery of boundary faces.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int simplify_all_elems ( uns_s *pUns, int quad22tris, splitType_e splitType ) {

  const faceOfElem_struct *pFoE ;

  int mFc = pUns->mFaceAllBc, kVx, mQuads ;
  bndFcVx_s *pBv ;
  chunk_struct *pChunk, *pChunk2 ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem, *pElBeg, *pElEnd, *pEl2 ;
  vrtx_struct **ppVx ;

  /* Make a list of all boundary faces by vertices. */
  pUns->mBndFcVx = mFc ;
  pBv = pUns->pBndFcVx = arr_malloc ( "pUns->pBndFcVx in read_adf_bnd", pUns->pFam,
                                      mFc, sizeof( *pUns->pBndFcVx ) ) ;

  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc= pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->nFace ) {
        /* Actual face, list its vertices and element. */
        pElem = pBndFc->Pelem ;
        pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
        /* Store the face pointer in the bc slot. */
        pBv->pBc = pBndFc->Pbc ;
        pBv->mVx = pFoE->mVertsFace ;
        for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ )
          pBv->ppVx[kVx] = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
        pBv++ ;
      }
  mFc = pBv - pUns->pBndFcVx ;


  /* match_bndFcVx expects the boundary spaces to be attached to pRootChunk. */
  if ( pUns->mChunks > 1 ) {

    /* All the boundary face info is stored in pBndFcVx. Delete the storage. */
    pChunk = NULL ;
    while ( loop_chunks ( pUns, &pChunk ) ) {
      arr_free ( pChunk->PbndPatch ) ;
      arr_free ( pChunk->PbndFc ) ;
      pChunk->mBndPatches = 0 ;
      pChunk->PbndPatch   = NULL ;
      pChunk->mBndFaces   = 0 ;
      pChunk->PbndFc      = NULL ;
    }
    
    pChunk = pUns->pRootChunk ;
    pChunk->PbndFc  = arr_malloc ( "pChunk->PbndFc in read_adf_bnd", pUns->pFam,
                                         mFc+1, sizeof( *pChunk->PbndFc ) ) ;
    pChunk->PbndFc  = arr_malloc ( "pChunk->PbndPatch in read_adf_bnd", pUns->pFam,
                                         pUns->mBc+1, sizeof( *pChunk->PbndFc ) ) ;
    pChunk->mBndPatches = pUns->mBc ;
    pChunk->mBndFaces = mFc ;
  }
  

  if ( quad22tris ) {
    /* Count the quads and alloc an extra element space. */
    count_uns_elems_of_type ( pUns ) ;
    mQuads = pUns->mElemsOfType[qua] ;

    pChunk2 = append_chunk ( pUns, pUns->mDim, mQuads, 4*mQuads, 0, 0, 0, 0 ) ;
    pEl2 = pChunk2->Pelem ;
    ppVx = pChunk2->PPvrtx ;

    reset_elems ( pChunk2->Pelem+1, mQuads ) ;

    /* Split all quads. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && pElem->elType == qua )
          qua22tris ( pElem, &pEl2, &ppVx, splitType ) ;

    if ( pEl2-pChunk2->Pelem != mQuads ) {
      printf ( " FATAL: too many triangles in simplify_all_elems (%d vs. %d).\n",
               (int)(pEl2-pChunk2->Pelem), mQuads ) ;
      return ( 0 ) ; }
  }

  
  else {
    /* Simplify all simplifiable elements. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number )
          simplify_one_elem ( pElem, pElem ) ;
  }



  /* Recover the face number in each element. */
  if ( !match_bndFcVx ( pUns ) ) {
    hip_err ( fatal, 0, "could not match boundary faces in simplify_all_elems.\n" ) ;
    return ( 0 ) ; }

  arr_free ( pUns->pBndFcVx ) ;
  pUns->pBndFcVx = NULL ;

  check_uns ( pUns, check_lvl ) ;
  
  return ( 1 ) ;
}

/******************************************************************************

  make_tet:
  Build a tet from a given triangular face and another vertex.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  : conceived.
  
  Input:
  ------
  Pelem:   The element,
  PPvxFc:  the list of vertices forming the face
  fcType:  the number of vertices around this face, 3=tri, 4=quad.
  kDgEdge: Numbers of the degenerate edges, numbers of the vertex at the end of
           the degenerate edge.
  Pvrtx:   new center vertex of the face is to be connected to.
  PlstChunk: last chunk with new elements.
  PPlstElem: last element in that chunk.
  PPPlstElem2Vert: last element to vertex pointer in that chunk.

  Changes To:
  -----------
  *Pelem
  PPlstElem
  PPPlstElem2Vert

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

elem_struct *make_tet ( elem_struct *Pelem, vrtx_struct **PPvxFc[], int fcType,
		        int kDgEdge[],
		        vrtx_struct *Pvrtx, chunk_struct *PlstChunk, 
		        elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert ) {
  
  int kVertFc ;
  
  /* Check for sufficient space. */
  if ( !check_elem_space ( 4, PlstChunk, PPlstElem, PPPlstElem2Vert ) )
  { printf ( " FATAL: out of space in make_tet.\n" ) ;
    return ( 0 ) ;
  }

  /* Increment the element pointer. */
  Pelem = ++(*PPlstElem) ;
  PlstChunk->mElemsNumbered++ ;
  Pelem->PPvrtx = *PPPlstElem2Vert + 1 ;
  /* List the three forming nodes of the face as 0-1-2. */
  for ( kVertFc = 0 ; kVertFc < fcType ; kVertFc++ )
    if ( kVertFc != kDgEdge[0] )
      /* This is not the second appearance of the degenerate vertex. */
      *(++*PPPlstElem2Vert) =  *PPvxFc[kVertFc] ;

  *(++*PPPlstElem2Vert) = Pvrtx ;
  Pelem->elType = tet ;
  /* OK, FIXIT! this is not clean. We here assume that the new tet is a root
     and a leaf element on a single grid. */
  Pelem->invalid = 0 ;
  Pelem->number = 1 ;
  Pelem->term = 1 ;
#ifdef ADAPT_HIERARCHIC
  Pelem->root = Pelem->leaf = 1 ;
  Pelem->Pparent = NULL ;
#endif
  
  return ( Pelem ) ;
}

/******************************************************************************

  make_pyr:
  Build a pyramid from a given triangular face and another vertex.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  : conceived.
  
  Input:
  ------
  Pelem:   The element,
  PPvxFc:  the list of vertices forming the face
  fcType:  the number of vertices around this face, 3=tri, 4=quad.
  Pvrtx:   new center vertex of the face is to be connected to.
  PlstChunk: last chunk with new elements.
  PPlstElem: last element in that chunk.
  PPPlstElem2Vert: last element to vertex pointer in that chunk.

  Changes To:
  -----------
  *Pelem
  PPlstElem
  PPPlstElem2Vert
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

elem_struct *make_pyr ( elem_struct *Pelem, vrtx_struct **PPvxFc[], int fcType,
		        vrtx_struct *Pvrtx, chunk_struct *PlstChunk, 
		        elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert ) {
  
  int kVert ;
  
  /* Check for sufficient space. */
  if ( !check_elem_space ( 5, PlstChunk, PPlstElem, PPPlstElem2Vert ) )
  { printf ( " FATAL: out of space in make_pyr.\n" ) ;
    return ( 0 ) ;
  }

  /* Increment the element pointer. */
  Pelem = ++(*PPlstElem) ;
  PlstChunk->mElemsNumbered++ ;
  Pelem->PPvrtx = *PPPlstElem2Vert + 1 ;
  /* List the three forming nodes of the face as 3-2-1-0. */
  for ( kVert = 0 ; kVert < fcType ; kVert++ )
    *(++*PPPlstElem2Vert) = *PPvxFc[fcType-kVert-1] ;

  *(++*PPPlstElem2Vert) = Pvrtx ;
  Pelem->elType = pyr ;
  /* OK, Fix this.! this is not clean. We here assume that the new tet is a root
     and a leaf element on a single grid. */
  Pelem->invalid = 0 ;
  Pelem->number = 1 ;
  Pelem->term = 1 ;
#ifdef ADAPT_HIERARCHIC
  Pelem->root = Pelem->leaf = 1 ;
  Pelem->Pparent = NULL ;
#endif
  
  return ( Pelem ) ;
}

/******************************************************************************

  check_elem_space:
  Check whether there is space for one more element.
  
  Last update:
  ------------
  20Dec14; tidy up.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int check_elem_space ( int mElem2Vert, chunk_struct *Pchunk, 
		       elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert ) {
  
  int mElems, mElem2Verts, offset ;
  elem_struct *PnewElem, *Pelem ;
  vrtx_struct **PPnewVrtx ;

  if ( !Pchunk )
    return ( 0 ) ;

  /* Elements. */
  if ( *PPlstElem >= Pchunk->Pelem + Pchunk->mElems ) {
    /* Realloc the element space. Increase by at least one elem. */
    mElems = 1.1*Pchunk->mElems + 1 ;
    PnewElem = arr_realloc ( "PnewElem in check_elem_space", Pchunk->pUns->pFam,
                             Pchunk->Pelem, mElems+1, sizeof ( *PnewElem ) ) ;     
    if ( verbosity > 3 ) {
      sprintf ( hip_msg, 
                "     reallocated element space to %d in check_elem_space.\n",
                mElems ) ;
      hip_err ( info, 5, hip_msg ) ;
    }

    if ( ( offset = PnewElem - Pchunk->Pelem ) ) {
      /* The element field has moved. 
	 Note that no updates for boundary faces pointing to these elements
	 are made. */
      Pchunk->Pelem = PnewElem ;
      Pchunk->mElems = mElems ;
      *PPlstElem += offset ;
    }
  }

  /* Element to vertex pointers. Note that these pointers start at 0.*/
  if ( *PPPlstElem2Vert + mElem2Vert >= Pchunk->PPvrtx + Pchunk->mElem2VertP ) {
    /* Realloc the pointer space. Increase by at least one elem. */
    mElem2Verts = 1.1*Pchunk->mElem2VertP + mElem2Vert ;
    PPnewVrtx = arr_realloc ( "PPnewVrtx in check_elem_space",  Pchunk->pUns->pFam,
                              Pchunk->PPvrtx, mElem2Verts, sizeof ( *PPnewVrtx )) ;

    if ( verbosity > 5 ) {
      sprintf ( hip_msg, 
                "realloced element pointer space to %d in check_elem_space.\n",
                mElem2Verts ) ;
      hip_err ( warning, 6, hip_msg ) ;
    }

    if ( ( offset = PPnewVrtx - Pchunk->PPvrtx ) ) {
      /* The element to vertex pointer field has moved. Offset the element
	 entries. */
      for ( Pelem = Pchunk->Pelem+1 ; Pelem <= *PPlstElem ; Pelem++ )
	if ( Pelem->PPvrtx[0] )
	  /* This is a valid element. */
	  Pelem->PPvrtx += offset ;
      
      Pchunk->PPvrtx = PPnewVrtx ;
      Pchunk->mElem2VertP = mElem2Verts ;
      *PPPlstElem2Vert += offset ;
    } 
  }

  return ( 1 ) ;
}

/******************************************************************************

  check_vert_space:
  Check whether there is space for one more vertex.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int check_vrtx_space ( chunk_struct *Pchunk, vrtx_struct **PPlstVrtx,
		       double **PPlstCoor, const int mDim ) {
  
  int mVerts, offset, mCoors ;
  vrtx_struct *PnewVrtx, **PPvrtx, *Pvx ;
  double *PnewCoor ;
  elem_struct *PlastElem ;
  
  if ( *PPlstVrtx >= Pchunk->Pvrtx + Pchunk->mVerts ) {
    /* Realloc the vertex space. Increase by at least one elem. */
    mVerts = 1.1*Pchunk->mVerts + 1 ;
    mCoors = ( mVerts+1)*mDim ;
    PnewVrtx = arr_realloc ( "PnewVrtx in check_vrtx_space", Pchunk->pUns->pFam,
                             Pchunk->Pvrtx, mVerts+1, sizeof ( *PnewVrtx ) ) ;
    PnewCoor = arr_realloc ( "PnewCoor in check_vrtx_space", Pchunk->pUns->pFam,
                             Pchunk->Pcoor, mCoors, sizeof ( *PnewCoor ) ) ;

    if ( verbosity > 2 )
      printf ( " INFO: realloced vertex space to %d.\n", mVerts ) ;
    
    if ( ( offset = PnewVrtx - Pchunk->Pvrtx ) )
    { /* The new vertex field has moved. Apply the offset to the elements. */
      PlastElem = Pchunk->Pelem + Pchunk->mElems ;
      for ( PPvrtx = Pchunk->Pelem[1].PPvrtx ;
	    PPvrtx <= PlastElem->PPvrtx + elemType[PlastElem->elType].mVerts ; PPvrtx++ )
	if ( *PPvrtx )
	  /* This is a valid vertex pointer. */
	  *PPvrtx += offset ;
      
      Pchunk->Pvrtx = PnewVrtx ;
      Pchunk->mVerts = mVerts ;
      *PPlstVrtx += offset ;
    }

    if ( ( offset = PnewCoor - Pchunk->Pcoor ) )
    { /* The new coor field has moved. Apply the offset to the vertices. */
      for ( Pvx = Pchunk->Pvrtx + 1 ; Pvx <= *PPlstVrtx ; Pvx++ )
	if ( Pvx->Pcoor )
	  Pvx->Pcoor += offset ;
      
      Pchunk->Pcoor = PnewCoor ;
      *PPlstCoor += offset ;
    }
  }

    return ( 1 ) ;
}

/******************************************************************************

  fix_boundFace:
  Scan over all boundaries, find the boundary face with degenerate edges of the
  old element, fix the face type and update the position pointer to the new element.
  
  Last update:
  ------------
  10Jul96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fix_boundFace ( const chunk_struct *Pchunk, elem_struct *Pelem,
		    int kFcNewElemFc[], elem_struct *PnewElemFc[] ) {
  
  bndPatch_struct *PbndPatch ;
  bndFc_struct *PbndFc ;
  
  /* Loop over all boundary patches of this chunk. */
  for ( PbndPatch = Pchunk->PbndPatch+1 ;
        PbndPatch <= Pchunk->PbndPatch + Pchunk->mBndPatches ;
        PbndPatch++ )
    /* Loop over all boundary faces in this patch. */
    for ( PbndFc = PbndPatch->PbndFc ;
	  PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ )
      if ( PbndFc->Pelem == Pelem && PbndFc->nFace )
        /* This is the element we're after. */
	update_face ( &(PbndFc->Pelem), &(PbndFc->nFace), 
		      kFcNewElemFc, PnewElemFc ) ;
  
  return ( 1 ) ;
}



/******************************************************************************

  fix_intFace:
  Scan over all interfaces, find the internal/cut face with degenerate edges of the
  old element, fix the face type and update the position pointer to the new element.
  
  Last update:
  ------------
  10Jul96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fix_intFace ( const chunk_struct *Pchunk, elem_struct *Pelem,
		  int kFcNewElemFc[], elem_struct *PnewElemFc[] ) {
  
  intFc_struct *PintFc ;

  /* Loop over all internal/cut faces in this patch. */
  for ( PintFc = Pchunk->PintFc + 1 ;
        PintFc <= Pchunk->PintFc + Pchunk->mIntFaces ; PintFc++ )
    if ( PintFc->Pelem == Pelem && PintFc->nFace )
      /* This is the element we're after. */
      update_face ( &(PintFc->Pelem), &(PintFc->nFace), 
		    kFcNewElemFc, PnewElemFc ) ;
  
  return ( 1 ) ;
}




/******************************************************************************

  fix_matchFace:
  Scan over all matching faces, find the matching face with degenerate edges of the
  old element, fix the face type and update the position pointer to the new element.
  Note that the bounding box is calculated with the old element. Thus, it must not
  be voided or overwritten before the matching faces have been fixed.
  
  Last update:
  ------------
  21Jan18; tidy up nomenclature in cpre_uns: matchFc_s ->pElem0, pElem1 instead of 1,2.
  10Jul96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fix_matchFace ( const uns_s *pUns, const int mDim, elem_struct *Pelem,
		    int kFcNewElemFc[], elem_struct *PnewElemFc[] ) {

  int kVrtx, nDim, mVerts = elemType[Pelem->elType].mVerts ;
  matchFc_struct *PmatchFc ;
  double elBBll[MAX_DIM], elBBur[MAX_DIM] ;
  const chunk_struct *Pchunk ;

  /* Find a bounding box for this element. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
  { elBBll[nDim] = TOO_MUCH ;
    elBBur[nDim] = -TOO_MUCH ;
 
    for ( kVrtx = 0 ; kVrtx < mVerts ; kVrtx++ )
    { elBBll[nDim] = MIN( elBBll[nDim], Pelem->PPvrtx[kVrtx]->Pcoor[nDim] ) ;
      elBBur[nDim] = MAX( elBBur[nDim], Pelem->PPvrtx[kVrtx]->Pcoor[nDim] ) ;
    }
  }

  /* Loop over all chunks. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    if ( overlap_dbl( elBBll, elBBur, Pchunk->llBox, Pchunk->urBox, mDim ) ) {
      /* There is overlap. This chunk might contain matching faces with this
	 element. Loop over all matching faces of this chunk. */
      for ( PmatchFc = Pchunk->PmatchFc + 1 ;
	    PmatchFc <= Pchunk->PmatchFc + Pchunk->mMatchFaces ; PmatchFc++ )
	
	if ( PmatchFc->pElem0 == Pelem && PmatchFc->nFace0 )
	  /* This is the element we're after. */
	  update_face ( &(PmatchFc->pElem0), &(PmatchFc->nFace0), 
		        kFcNewElemFc, PnewElemFc ) ;
	else if ( PmatchFc->pElem1 == Pelem && PmatchFc->nFace1 )
	  /* This is the element we're after. */
	  update_face ( &(PmatchFc->pElem1), &(PmatchFc->nFace1), 
		        kFcNewElemFc, PnewElemFc  ) ;
    }
  
  return ( 1 ) ;
}



/******************************************************************************

  update_face:
  Update an interior element pointer in a boundary or so face.
  
  Last update:
  ------------
  10Jul96: conceived.
  
  Input:
  ------
  Pelem: pointer to the old degenerate element that is still in the structure.
  PnFace:   pointer to the face index in the face data structure.

  Changes To:
  -----------
  PnFace:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void update_face ( elem_struct **PPelem, int *PnFace, 
		   int kFcNewElemFc[], elem_struct *PnewElemFc[] ) {
  
  *PPelem = PnewElemFc[*PnFace] ;
  *PnFace = kFcNewElemFc[*PnFace] ;
}

