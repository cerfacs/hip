/*
  meth.c:
  Generic methods for all types of grids.
  
  Last update:
  ------------
  7Sep17; intro hip_check_count.
  19Mar17; intro vec_avg_4dbl_3d.
  18Dec16; intro pack_int_list.
  9Jul13; intro hip_err_header.
          intro expr_is_text
  16Dec11; move add_hrb from here to adapt_meth.c
  27May09; intro ftnString.
  4Apr09; drop the duplicating check_varName.
  07Jan05; move heap_cmp comparison routines from heap to cmp_ here.
  20Oct98; add vec_add_dbl
  28Apr98; add rot_coor, etc.
  10Oct96: move selections functions to mb_or_uns.c.
  15Jul96; cut out of cpre.c.
*/

#include <string.h>
#include <ctype.h>
#include <fnmatch.h> // char expression matching.

#include "cpre.h"
#include "proto.h"

extern Grids_struct Grids ;
extern arrFam_s *pArrFamUnsInit ;
extern double Gamma, GammaM1 ;
extern const char varCatNames[][LEN_VAR_C] ;

extern const int verbosity ;
extern const hip_output_e hip_output ;
int hip_fat_header_written ;
int hip_warn_header_written ;
extern char hip_msg[] ;
extern char hip_out_buf[MAX_MSG_LINES*LINE_LEN] ;
extern char *pHip_out_buf_wrt ;

extern const char version[] ;

extern const char fatal_log_file[] ;
extern const char warning_log_file[] ;



/******************************************************************************
  hip_err_header:   */

/*! write a header for an error log file.
 */

/*
  
  Last update:
  ------------
  89Jul13: conceived.
  
  Input:
  ------
  logFl: 
 
*/

void hip_err_header ( FILE* logFl, hip_stat_e status ) {

  if ( status == warning ) {
    hip_warn_header_written = 1 ;
    fprintf ( logFl, "Warning log for hip Version %s\n", version ) ;
  }
  else {
    hip_fat_header_written = 1 ;
    fprintf ( logFl, "Fatal error log for hip Version %s\n", version ) ;
  }

  fprintf ( logFl, "  compiled with" ) ;
#if MACH_DEP == Linux
  fprintf ( logFl, " MACH_DEP=Linux" ) ;
#endif
#ifdef DEBUG
  fprintf ( logFl, " -DDEBUG" ) ;
#endif
#ifdef LITTLE_ENDIAN
  fprintf ( logFl, " -DLITTLE_ENDIAN" ) ;
#endif
#ifdef IPTR64
  fprintf ( logFl, " -DIPTR64" ) ;
#endif
#ifdef HIP_USE_ULONG
  fprintf ( logFl, " -DHIP_USE_ULONG." ) ;
#endif
  fprintf ( logFl, ".\n\n" ) ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  ret_success:
*/
/*! init a ret_s struct for success 
 *
 */

/*
  
  Last update:
  ------------
  5Sep19: conceived.
    
  Returns:
  --------
  ret: ret_s with 'success.
  
*/

ret_s ret_success ( ) {
  ret_s ret = { NULL, NULL, success, hip_out_buf } ;

  return ( ret ) ;
}

ret_s ret_failure ( const char *pFailMsg ) {
  strncpy ( hip_out_buf, pFailMsg, sizeof( hip_out_buf ) ) ;
  ret_s ret = { NULL, NULL, fatal, hip_out_buf } ;

  return ( ret ) ;
}

int ret_is_success ( const ret_s ret ) {
  if ( ret.status == success )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

int ret_is_failure ( const ret_s ret ) {
  if ( ret.status == fatal )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}


/******************************************************************************

  hip_err:
  hip's error print/log, which considers the verbosity level and 
  aborts if appropriate.

  Devel comment toward hprintfx, JDM 3/9/12
  modify the interface to avoid the typical preceding printf to hip_msg. 
  Use vprintf, e.g. look here: http://www.eskimo.com/~scs/cclass/int/sx11c.html
  
  Last update:
  ------------
  8Mar18; make str const.
  8Jul13; intro hip_err_header.
  9May11; change errCat indentation.
  11Oct07: conceived.
  
  Input:
  ------
  str: error message

  Changes To:
  -----------

  
  Returns:
  --------
  ret: ret_s strucure with return values.
  
*/

ret_s hip_err ( hip_stat_e status, int printLvl, const char *str ) {
  ret_s ret  = ret_success () ;

  FILE *fatFl, *warnFl ;
  const char errCat[][20] = { "ERROR:", "FATAL:", "   WARNING:", "       INFO:", ""} ;
  ret.status = status ;
  strncpy ( pHip_out_buf_wrt, str, LINE_LEN ) ;

  if ( verbosity >= printLvl ) {
    if ( status == fatal || status == warning )
      hprintf ( "\n" ) ;
    hprintf ( "%s %s\n", errCat[status], str ) ;
    if ( status == fatal || status == warning )
      hprintf ( "\n" ) ;
  }


  if ( status == fatal || ( status == warning && verbosity == 0 ) ) {

    /* Dump an error log. Treat warnings as fatal under verb=0. */
    fatFl  = fopen ( fatal_log_file, "w" ) ;

    if ( !hip_fat_header_written ) hip_err_header ( fatFl, status ) ;

    fprintf ( fatFl, "%s %s\n", errCat[status], str ) ;
    fprintf ( fatFl, "Exiting via hip_err. Sorry.\n" ) ;
    fclose ( fatFl ) ;

    /* Terminate */
    if ( verbosity >= printLvl ) {
      hprintf ( "Exiting via hip_err, see error log in %s. Sorry.\n",
               fatal_log_file ) ;
    }
    #ifdef DEBUG
    printf ( "DEBUG mode: override exit ( EXIT_FAILURE )\n" ) ;
    #else
    exit ( EXIT_FAILURE ) ;
    #endif
  }

  else if ( status == warning ) {

    /* Dump an warning log. */
    warnFl  = fopen ( warning_log_file, "a" ) ;

    if ( !hip_warn_header_written ) hip_err_header ( warnFl, status ) ;

    fprintf ( warnFl, "%s %s\n", errCat[status], str ) ;
    fclose ( warnFl ) ;
  }


  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void hprintf( char *fmt, ... ) {

  char *pStr ;
  size_t n ;
  va_list argptr;
  va_start(argptr,fmt);

  switch ( hip_output ) {
  case string :
    /* append to hip_msg, as long as there is space. */
    n = pHip_out_buf_wrt-hip_out_buf + MAX_MSG_LINES*LINE_LEN - 1 ;
    vsnprintf( pHip_out_buf_wrt, n, fmt, argptr ) ;
    pHip_out_buf_wrt = strchr( pHip_out_buf_wrt, '\0' ) ;
    break ;
   
  default:
    /* screen. */
    vprintf( fmt, argptr );
  }

  va_end(argptr);

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  flush_hip_out_buf:
*/
/*! reset the write ptr into hip_out_buf
 *
 */

/*
  
  Last update:
  ------------
  6Sep19: conceived.
  

    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

/* hip_cython */
void flush_hip_out_buf () {
  pHip_out_buf_wrt = hip_out_buf ;
  pHip_out_buf_wrt[0] = '\0' ;

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  hip_err_miscount:
*/
/*! issue a fatal on miscount/overfill.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  7Sep17: conceived.
  

  Input:
  ------
  iExpected: expected number,
  iFound: actually found/written number
  dataName: name of the array/data item
  funName: name of the fun where the miscount occurred.
  
*/

void hip_check_count ( const int iExpected, const int iFound,
                        char *dataName, char *funName ) {
  if ( iExpected - iFound ) {
    sprintf ( hip_msg, "expected %d, found %d %s in %s.",
              iExpected, iFound, dataName, funName ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  return ;
}

/*
Copyright Jens-Dominik Mueller and CERFACS, see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>
*/
/******************************************************************************
  pack_ulg_list:   
*/  

/*! Given a list of ulong_t, e.g. node or elem numbers, sort ascending and remove duplicates.
 *
 * This is meant for short lists, e.g. 4 face node numbers.
 *
 */

/*
  
  Last update:
  ------------
  13Feb19: derived from pack_int_list.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int pack_ulg_list ( ulong_t *nEnt, int mEnt ) {

  qsort ( nEnt, mEnt, sizeof ( *nEnt ), cmp_ulong_t ) ;

  /* Condense the list by eliminating collapsed edges, i.e. duplicate vx. */
  int k, kk ;
  for ( k = 0 ; k < mEnt-1 ; k++ ) {
    if ( nEnt[k] == nEnt[k+1] ) {
      /* Copy the remaining list down. */
      for ( kk = k+1 ; kk < mEnt ; kk++ ) {
        nEnt[kk] = nEnt[kk+1] ;
      }
      mEnt-- ;
      k-- ; /* And try with that one again. */
    }
  }
  /* Compare first and last. */
  if ( mEnt > 2 && nEnt[0] == nEnt[mEnt-1] )
    mEnt-- ;
  
  return ( mEnt ) ;
}

/******************************************************************************
  pack_int_list:   
*/  

/*! Given a list of integers, e.g. node or elem numbers, sort ascending and remove duplicates.
 *
 * This is meant for short lists, e.g. 4 face node numbers.
 *
 */

/*
  
  Last update:
  ------------
  16Dec16: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int pack_int_list ( int *nEnt, int mEnt ) {

  qsort ( nEnt, mEnt, sizeof ( *nEnt ), cmp_int ) ;

  /* Condense the list by eliminating collapsed edges, i.e. duplicate vx. */
  int k, kk ;
  for ( k = 0 ; k < mEnt-1 ; k++ ) {
    if ( nEnt[k] == nEnt[k+1] ) {
      /* Copy the remaining list down. */
      for ( kk = k+1 ; kk < mEnt ; kk++ ) {
        nEnt[kk] = nEnt[kk+1] ;
      }
      mEnt-- ;
      k-- ; /* And try with that one again. */
    }
  }
  /* Compare first and last. */
  if ( mEnt > 2 && nEnt[0] == nEnt[mEnt-1] )
    mEnt-- ;
  
  return ( mEnt ) ;
}



/******************************************************************************

  aidx2lidx: Translate an AVBP-style index of first elements cum
  number of pointees to an index of last elements, possibly in situ.
  
  Last update:
  ------------
  1May20; use void return type.
  7Apr13; promote possibly large ints to ulong_t.
  19Dec06: conceived.
  
  Input:
  ------
  aidx: index listing first pointees for mKey keys.
  mKey: number of keys.

  Changes To:
  -----------
  lidx: index listing last pointees.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void aidx2lidx ( ulong_t *aidx, const ulong_t mKeys, ulong_t *lidx ) {

  ulong_t i ;

  for ( i = 0 ; i < mKeys ; i++ ) {
    lidx[i] = aidx[2*i] + aidx[2*i+1] ;
  }

  return ;
}



/******************************************************************************

  fidx2lidx:
  lidx2fidx:
  Translate an index of first/last elements to an index of last/first elements,
  possibly in situ.
  
  Last update:
  ------------
  1May20; intro lidx2fidx, use void return type.
  7Apr13; promote possibly large ints to ulong_t.
  18May08; fixed bugs, dropped ptees argument.
  19Dec06: conceived.
  
  Input:
  ------
  fidx/lidx: index listing first/last pointees for mKey keys, ie mKey+1 values.
  mKey: number of keys.

  Output:
  -----------
  lidx/fidx: index listing last/first pointees. Needs to have size mBc+1 for lidx2fidx.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void ufidx2lidx ( ulong_t *fidx, const ulong_t mKeys, ulong_t *lidx ) {

  ulong_t i ;

  for ( i = 0 ; i < mKeys ; i++ ) {
    lidx[i] = fidx[i+1]-1 ;
  }

  return ;
}


void ifidx2lidx ( int *fidx, const int mKeys, int *lidx ) {

  int i ;

  for ( i = 0 ; i < mKeys ; i++ ) {
    lidx[i] = fidx[i+1]-1 ;
  }

  return ;
}


// Should work in-situ,
// make sure fidx is sized to mKeys+1
void ulidx2fidx ( ulong_t *lidx, const ulong_t mKeys, ulong_t *fidx ) {

  ulong_t i ;

  for ( i = mKeys ; i > 0 ; i-- ) {
    fidx[i] = lidx[i-1]+1 ;
  }
  fidx[0] = 1 ;

  return ;
}



// Should work in-situ,
// make sure fidx is sized to mKeys+1
// Number from zero.
void ilidx2fidx ( int *lidx, const int mKeys, int *fidx ) {

  int i ;

  for ( i = mKeys ; i > 0 ; i-- ) {
    fidx[i] = lidx[i-1]+1 ;
  }
  fidx[0] = 1 ;

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_grid_numbered:
*/
/*! return a pointer to the grid with the given number
 *
 *
 */

/*
  
  Last update:
  ------------
  30jun17: extracted from set.c
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


int grid_match_expr( const grid_struct *pGrid, const char expr[] ) {

  if ( !pGrid )
    /* No such grid. */ 
    return ( 0 ) ;
 
  if ( expr[0] == '-'  ) {
    /* Epression starts with a -, such as a neg. number, matches most recent. */
    if ( !pGrid->uns.PnxtGrid )
      return ( 1 ) ;
  }
  else if ( !fnmatch( expr, pGrid->uns.name, 0 ) )
    /* text match. */
    return ( 1 ) ;
  else if ( num_match( pGrid->uns.nr, expr ) )
    /* number match. */
    return ( 1 ) ;

  /* No match. */
  return ( 0 ) ;
}



grid_struct *find_grid ( const char *expr, grid_type_enum type ) {
  
  grid_struct *pGrid = NULL ;

  for ( pGrid = Grids.PfirstGrid ; pGrid ; pGrid = pGrid->uns.PnxtGrid )
    if (  grid_match_expr( pGrid, expr ) &&
         ( type == noGr || pGrid->uns.type == type ) ) {
      /* This is the one. */
      break ;
    }

  return ( pGrid ) ;
}

grid_struct *find_grid_numbered ( int nr, grid_type_enum type ) {
  
  grid_struct *pGrid = NULL ;

  for ( pGrid = Grids.PfirstGrid ; pGrid ; pGrid = pGrid->uns.PnxtGrid )
    if ( pGrid->uns.nr == nr &&
         ( type == noGr || pGrid->uns.type == type ) ) {
      /* This is the one. */
      break ;
    }

  return ( pGrid ) ;
}



/************************************************************************

  make_grid:
  Allocate and initialize a grid for the linked list of grids.

  Last update:
  ------------
  12Jan01; intro Grids.adapt
  22Jul96; cut out of read_dpl.

  Returns:
  --------
  NULL:   on failure,
  PnewGrid: the allocated grid,

*/

grid_struct *make_grid () {
  
  extern Grids_struct Grids ;
  grid_struct *Pgrid, *PnewGrid ;
  
  /* Make a new grid. */
  PnewGrid = arr_malloc ( "PnewGrid in make_grid", pArrFamUnsInit, 1,
                          sizeof ( grid_struct ) ) ;
  PnewGrid->uns.nr = ++Grids.mGrids ;
  PnewGrid->uns.PnxtGrid = NULL ;
  sprintf ( PnewGrid->uns.name, "grid_%d", Grids.mGrids ) ;
  PnewGrid->uns.pVarList = NULL ;
  
  /* Travel to the end of the linked list, alloc
     and append. */
  if ( ( Pgrid = Grids.PlastGrid ) ) {
    /* There are grids already. */
    Pgrid->uns.PnxtGrid = PnewGrid ;
    PnewGrid->uns.PprvGrid = Pgrid ;
  }
  else {
    /* First grid. */
    PnewGrid->uns.PprvGrid = NULL ;
    Grids.PfirstGrid = PnewGrid ;
  }
  Grids.PlastGrid = PnewGrid ;

  return ( PnewGrid ) ;
  
}
/************************************************************************

   update_bc_e:
   Set the summary boundary type bc_e according to type or text.


   Last update:
   17May06; conceived
   
   Input:
   ------
   pBc: bc in question

   Changes to:
   -----------
   pBc->bc_e
*/

spec_bc_e set_bc_e ( bc_struct *pBc ) {



  if ( !strncmp( pBc->text, "hip_per_inlet", 13 ) ||
       !strncmp( pBc->text, "hip_per_outlet", 14 ) ) {
    pBc->bc_e = perBc ;
  }

  else {
  /* ex doc:
   e:   & entry           &    o:   & outlet          & u:   & upper periodic  \\
   f:   & far field       &    p:   & any periodic    & v:   & viscous wall    \\
   i:   & inviscid wall   &    n:   & none            & w:   & any wall        \\
   l:   & lower periodic  &    s:   & symmetry plane  \\
  */


    switch ( pBc->type[0] ) {
    case 'i' :
      pBc->bc_e = wallBc ;
      break ;
    case 'v' :
      pBc->bc_e = wallBc ;
      break ;
    case 'w' :
      pBc->bc_e = wallBc ;
      break ;

    case 'l' :
      pBc->bc_e = perBc ;
      break ;
    case 'u' :
      pBc->bc_e = perBc ;
      break ;
    case 'p' :
      pBc->bc_e = perBc ;
      break ;

    case 'e' :
      pBc->bc_e = otherBc ;
      break ;
    case 'f' :
      pBc->bc_e = otherBc ;
      break ;
    case 'n' :
      pBc->bc_e = otherBc ;
      break ;
    case 's' :
      pBc->bc_e = otherBc ;
      break ;

    default :
      pBc->bc_e = unspecBc ;
      break ;
    }
  }
    
  return ( pBc->bc_e ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  is_int:
*/
/*! is the string an integer number?
 *
 */

/*
  
  Last update:
  ------------
  11Jul17: conceived.
  

  Input:
  ------
  pStr: string
    
  Returns:
  --------
  1 if integer, 0 otherwise
  
*/

int is_int ( const char *pStr ) {

  const char *pC ;
  for ( pC = pStr ; *pC != '\0' ; pC++ )
    if ( !( isdigit(*pC) || *pC == '-' ) )
      return ( 0 ) ;
  
  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  is_float:
*/
/*! is the string an integer number?
 *
 */

/*
  
  Last update:
  ------------
  11Jul17: conceived.
  

  Input:
  ------
  pStr: string
    
  Returns:
  --------
  1 if float, 0 otherwise
  
*/

int is_float ( const char *pStr ) {

  const char *pC ;
  int hasDec = 0 ;
  int hasE = 0 ;
  for ( pC = pStr ; *pC != '\0' ; pC++ )
    if ( !( isdigit(*pC) ||
            *pC=='-' ||
            *pC=='.' ||
            *pC=='e' || *pC=='E' )           
            )
      return ( 0 ) ;
    else if ( *pC=='.' ) {
      // only one decimal point. No dec pt in exponent.
      if ( hasDec || hasE )
        return ( 0 ) ;
      else
        hasDec = 1 ;
    }
    else if ( *pC=='e'  || *pC=='E') {
      // only one exp,
      if ( hasE )
        return ( 0 ) ;
      else
        hasE = 1 ;
    }
  
  return ( 1 ) ;
}



/******************************************************************************
  expr_is_text:   */

/*! Given an expression, determine whether is is text or numerical.
 */

/*
  
  Last update:
  ------------
  9Jul13: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int expr_is_text ( const char *expr ) {

  const char *pC ;
  int matchText = 0 ;

  /* If we find any alphabetic characters or wildcards, the expression
     refers to the bc text. */
  for ( pC = expr ; pC-expr < strlen( expr ) ; pC++ )
    if ( isalpha ( *pC ) || *pC == '?' || *pC == '*' )
      /* numExpr is not a bc number but a bc text. */
      matchText = 1 ;

  return ( matchText ) ;
}

/************************************************************************

   find_bc:
   Find a match for the given bc string in the list of bcs, if
   not, create a new entry.


   Last update:
   ------------
   11May17; replace matchBc with geoType in bc init.
   13Jan15; replace also other special characters with underscores.
   22Dec14; Replace any blanks with an underscore.
   30May96; moved from read_doc_as.c to mb_meth.c.
   20Apr96: conceived.
   
   Input:
   ------
   string: string with the new boundary condition.
   mode:-1: decrement the ref-count and delete the entry
            if 0 for a found string.
         0: return the root bc.
         1: increment the ref-counter.
	 2: do not increment the ref-counter, return NULL when
	    no bc with the string is found.
	 3: return a partial match.

   Changes to:
   -----------

*/

bc_struct *find_bc ( const char *inString, int mode ) {
  
  static bc_struct rootBc, *ProotBc = &rootBc ;
  static int mBcs = 0 ;
  int iPos ;
  bc_struct *Pbc, *PnewBc, *PlastBc ;
  char string[LINE_LEN+1] ;

  if ( mode == 0 ) {
    if ( ProotBc->PnxtBc )
      return ( ProotBc->PnxtBc ) ;
    else
      return ( NULL ) ;
  }
  strncpy ( string, inString, LINE_LEN ) ;
  /* Erase all trailing blanks from string. First scan to the
     end. */
  for ( iPos=0 ; string[iPos] != '\0' ; iPos++ )
    ;
  /* Now scan backward as long as there are blanks. */
  while ( string[--iPos] == ' ' )
    ;
  /* Mark the end. */
  string[++iPos] = '\0' ;

  /* Replace any blanks or other special char with an underscore. */
  specchar2underscore ( string ) ;



  /* Loop over the existing entries in bcS. Note that the root is
     an empty bc to avoid having it deleted on losing all its references. */
  for ( Pbc = ProotBc->PnxtBc, PlastBc = ProotBc ;
        Pbc ; PlastBc = Pbc, Pbc = Pbc->PnxtBc )
    if ( mode == 3 ) {
      if ( strstr( Pbc->text, string ) )
	/* Pbc->text contains the string. */
	return ( Pbc ) ; }
    else if ( !strcmp( string, Pbc->text ) ) {
      /* This bc type already exists. */
      if ( mode == 1 ) {
        ++Pbc->refCount ;
	return ( Pbc ) ; }
      
      else if ( mode == -1 ) {
	/* Decrement the refcount. */
	--Pbc->refCount ;
	
	if ( Pbc->refCount == 0 ) {
	  /* Remove this entry. */
	  Pbc->PprvBc->PnxtBc = Pbc->PnxtBc ;
	  Pbc->PnxtBc->PprvBc = Pbc->PprvBc ;
	  arr_free ( Pbc ) ;
	  return ( NULL ) ; } 
	else
	  return ( Pbc ) ;
      }
      else
	/* Just return the pointer. */
	return ( Pbc ) ;
    }
  
  /* No matching string has been found. */
  if ( mode == -1 )
    /* Deletion of the void. */
    return ( NULL ) ;
  else if ( mode == 1 ) {
    /* Make a new one. */
    PnewBc = arr_malloc ( "PnewBc in find_bc", pArrFamUnsInit, 1,  
                          sizeof( bc_struct ) ) ;
    if ( !PnewBc ) {
      sprintf ( hip_msg, "could not allocate space for a new boundary "
               "condition in find_bc." ) ;
      hip_err ( fatal, 0, hip_msg ) ; 
    }

    /* Append to the linked list and fill. */
    PlastBc->PnxtBc = PnewBc ;
    PnewBc->PprvBc = PlastBc ;
    PnewBc->PnxtBc = NULL ;
    PnewBc->geoType = bnd ;
    PnewBc->refCount = 1 ;
    PnewBc->mark = 0 ;
    PnewBc->type[0] = 'n' ; PnewBc->type[1] = '\0' ; 
    PnewBc->pPerBc = NULL ;
    PnewBc->nr = PnewBc->order = ++mBcs ;
    strcpy ( PnewBc->text, string ) ;

    return ( PnewBc ) ;
  }
  else
    /* No bc of this string exists to be referenced. */
    return ( NULL ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  geoType2Char:
*/
/*! Encode the geometric type of boundary or patch or face list
 *  into a character.
 *
 */

/*
  
  Last update:
  ------------
  15jul19; derived from geoType2Char.
  

  Input:
  ------
  geoType1, 2: enums of bnd, match, inter, cut that need to match/include.

  Returns:
  --------
  1 on match, zero on mismatch
  
*/

int isMatch_geoType ( bcGeoType_e geoType1, bcGeoType_e geoType2 ) {


  if ( geoType1 > noBcGeoType ) {
    /* swap the two args to have the specific type in geoType1. */
    bcGeoType_e gt = geoType2 ;
    geoType2 = geoType1 ;
    geoType1 = gt ;
  }
   
  if ( geoType1 > noBcGeoType ) {
    /* i.e. geoType2 must also be a super type. */
    hip_err ( warning, 4,
              "comparing two superset bc geo types makes no sense in isMatch_geoType" ) ;
    return ( 0 ) ;
  }
  
  else if ( geoType1 == geoType2 )
    /* Let's include a none == none as a match. */
    return ( 1 ) ;

  else if ( geoType2 > noBcGeoType ) {
    /* Match to an extended type. */
    if ( geoType2 == any ) {
      if ( geoType1 < noBcGeoType ) return ( 1 ) ;
      else return ( 0 ) ; 
    }
    else if ( geoType2 == bndAndInter ) {
      if ( geoType1 == bnd )       return ( 1 ) ;
      else if ( geoType1 == inter )        return ( 1 ) ;
      else if ( geoType1 ==  duplicateInter )        return ( 1 ) ;
      else return ( 0 ) ;
    }
  }
  
  return ( 0 )  ;
}






/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  geoType2Char:
*/
/*! Encode the geometric type of boundary or patch or face list
 *  into a character.
 *
 */

/*
  
  Last update:
  ------------
  10jul19; move return out of case block.
  9Jul19; rename interDuplicate to duplicateInter, to have unique first chars.
  11May17: conceived.
  

  Input:
  ------
  geoType: enum of bnd, match, inter, cut
    
  Returns:
  --------
  character corresponding to type
  
*/

void geoType2Char ( bcGeoType_e geoType, char *pStr ) {

  switch ( geoType ) {
  case bnd :
    strcpy ( pStr, "bnd" ) ;
    break ;
  case match :
    strcpy ( pStr, "match" ) ;
    break ;
  case inter :
    strcpy ( pStr, "inter" ) ;
    break ;
  case duplicateInter :
    strcpy ( pStr, "dpInt" ) ;
    break ;
  case cut :
    strcpy ( pStr, "cut" ) ;
    break ;
  default:
    strcpy ( pStr, "undef" ) ;
  }
  
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  geoType2Char:
*/
/*! Encode the geometric type of boundary or patch or face list
 *  into a character.
 *
 */

/*
  
  Last update:
  ------------
  10Jul19; deried from geoType2char.
  

  Input:
  ------
  character corresponding to type
    
  Returns:
  --------
  geoType: enum of bnd, match, inter, cut
  
*/

bcGeoType_e char2geoType ( char gChar ) {
  bcGeoType_e geoType ;
  switch ( gChar ) {
  case 'b' :
    geoType = bnd ;
    break ;
  case 'm' :
    geoType = match ;
    break ;
  case 'i' :
    geoType = inter ;
    break ;
  case 'd' :
    geoType = duplicateInter ;
    break ;
  case 'c' :
    geoType = cut ;
    break ;
  default:
    geoType = noBcGeoType ;
  }
  
  return ( geoType ) ;
}




/******************************************************************************

  print_bc:
  Print out a bc.
  
  Last update:
  ------------
  6Sep19; use hprintf.
  16Apr18: write out surface areas.
  : conceived.
  
  Input:
  ------
  Pbc: the pointer to the bc.
  pBndPatchArea: if non-null, a pointer to the patch area.
*/

void print_bc ( const bc_struct *Pbc, double *pBndPatchArea ) {

  char string[LINE_LEN] ;
  
  if ( !Pbc ) {
    /* Print the header. */
    hprintf (   "   Nr: Mrk(#), geoType, bcType, order," ) ;
    if ( pBndPatchArea )
      hprintf ( "   area,       " ) ;
    hprintf (   " text\n" ) ;
  }
  else { 
    geoType2Char ( Pbc->geoType, string ) ;
    hprintf ( "   %2d: %1d (%2d), %7s,   %4s,  %3d,",
	     Pbc->nr, Pbc->mark, Pbc->refCount, string, Pbc->type, Pbc->order ) ;

    if ( pBndPatchArea )
      hprintf ( " %14.5e,", *pBndPatchArea ) ; 

    hprintf ( " %-40s\n", Pbc->text ) ;
  }
  return ;
}

/******************************************************************************

  ipow:
  Integer power p = d^n that returns 1. if n==0..
  
  Last update:
  ------------
  18.12.2: copied here from x2_cholsl
  
  Input:
  ------
  d: 
  n:
  
  Returns:
  --------
  p
  
*/

double ipow ( double d, int n ) {
  /* Integer power that returns 1. if n==0. */
  int i ;
  double p = d ;

  if ( n < 0 ) {
    p = 1./d ;
    n = -n ;
  }
  else
    p = d ;

  if ( n ) {
    for ( i = 1 ; i < n ; i++ )
      p *= d ;
    return ( p ) ;
  }
  else
    return ( 1. ) ;
}
  
/******************************************************************************

  daxb:
  Calculate a matrix vector product Ax=b:
  
  Last update:
  ------------
  18.12.2: copied here from x2_cholsl.
  
  Input:
  ------
  A:
  x:

  Changes To:
  -----------
  b:
  
*/

void daxb ( double **A, int m, int n, double x[], double b[] ) {
  /* calculate a matrix vector product Ax=b. */
  int i,j ;
  
  for ( j=1; j<=m; j++ ) {
    b[j] = 0 ;

    for ( i=1; i<=n; i++ ) 
      b[j] += A[j][i]*x[i] ;
  }
}

/******************************************************************************

  datxb:
  Calculate a matrix vector product A^Tx=b:
  
  Last update:
  ------------
  4Feb2; derived from daxb.
  
  Input:
  ------
  A:
  x:

  Changes To:
  -----------
  b:
  
*/

void datxb ( double **A, int m, int n, double x[], double b[] ) {
  /* calculate a matrix vector product Ax=b. */
  int i,j ;
  
  for ( j=1; j<=m; j++ ) {
    b[j] = 0 ;

    for ( i=1; i<=n; i++ ) 
      b[j] += A[i][j]*x[i] ;
  }
}



/******************************************************************************

  overlap_dbl:
  check two double boxes for overlap.
  
  Last update:
  ------------
  10Jul96: derived from overlap in tree.
  
  Input:
  ------
  Pll/ur1/2: lowerleftfront... and upperrightrear... corners of the two
             bounding boxes to compare.

  Returns:
  --------
  1 on overlap, 1 otherwise.
  
*/

int overlap_dbl ( const double *Pll1, const double *Pur1,
		  const double *Pll2, const double *Pur2,
                  const int mDim )
	
{ /* Check whether the two ranges have an overlap. */
  int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
  { /* If the ur1 is smaller than the ll2, there can be
       no overlap. */
    if ( Pur1[nDim] < Pll2[nDim] )
      return ( 0 ) ;
    else if ( Pur2[nDim] < Pll1[nDim] )
      return ( 0 ) ;
  }

  /* All dimensions overlap. */
  return ( 1 ) ;
}


/******************************************************************************

  sq_distance_dbl:
  Calculate the squared distance between two doubles.
  
  Last update:
  ------------
  
  Input:
  ------
  Pcoor1/2: pointers to the first elements of the two double vectors.
  mDim: dimensions
  
  Returns:
  --------
  the squared distance.
  
*/

double sq_distance_dbl ( const double *Pcoor1, const double *Pcoor2, const int mDim ) {

  int nDim ;
  double diff, distance ;

  /* Loop over all dimensions. */
  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ ) {
    diff = Pcoor1[nDim] - Pcoor2[nDim] ;
    distance += diff*diff ;
  }
  return ( distance ) ;
}

/******************************************************************************

  sq_distance_axis:
  Calculate the squared distance from a point to a coordinate axis.
  
  Last update:
  ------------
  15Dec13; derived from sq_distance_dbl
  
  Input:
  ------
  pCoor: pointer to the first elements of the coordinate
  axis: x,y, or z axis.
  mDim: dimensions
  
  Returns:
  --------
  the squared distance.
  
*/

double sq_distance_axis ( const double *pCoor, 
                          const specialTopo_e axis, const int mDim ) {

  int nDim ;
  double co[MAX_DIM] ;
  double distance = 0. ;

  memcpy ( co, pCoor, mDim*sizeof( double ) ) ;

  /* Only x,y or z axes are possible, so simply set that coor to zero. */
  switch ( axis ) {
  case axiX:
    co[0] = 0. ;
    break ;
  case axiY:
    co[1] = 0. ;
    break ;
  case axiZ:
    co[2] = 0. ;
    break ;
  default:
    sprintf ( hip_msg, "in sq_distance_axis: called with non-axi topo %d\n", axis ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
    
  /* Loop over all dimensions. */
  double *pCo ; 
  for ( pCo = co ; pCo < co+mDim ; pCo++ ) {
    distance += *pCo * (*pCo) ;
  }
  return ( distance ) ;
}


/******************************************************************************

  vec_len_dbl_sq:
  vec_len_dbl:
  Calculate a vector's length.
  
  Last update:
  ------------
  11Nov17; intro vec_len_dbl_sq.
  
  Input:
  ------
  Pcoor1/2: pointers to the first elements of the two double vectors.
  mDim: dimensions
  
  Returns:
  --------
  the squared distance.
  
*/

double vec_len_dbl_sq ( double *pCoor, const int mDim ) { 
  int nDim ;
  double distance ;

  /* Loop over all dimensions. */
  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ )
    distance +=  pCoor[nDim]*pCoor[nDim] ;
  return ( distance ) ;
}

double vec_len_dbl ( double *pCoor, const int mDim ) { 
  return ( sqrt( vec_len_dbl_sq ( pCoor, mDim ) ) ) ;
}

/******************************************************************************

  vec_norm_dbl:
  Normalize a vector to unit length.
  
  Last update:
  ------------
  
  Input:
  ------
  Pcoor1/2: pointers to the first elements of the two double vectors.
  mDim: dimensions
  
  Returns:
  --------
  the vector length.
  
*/

double vec_norm_dbl ( double *pCoor, const int mDim ) {

  static int nDim ;
  static double distance ;

  /* Loop over all dimensions. */
  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ )
    distance +=  pCoor[nDim]*pCoor[nDim] ;

  if ( distance < MIN_POSITIVE_FLOAT )
    return ( 0. ) ;

  distance = sqrt( distance ) ;
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    pCoor[nDim]/= distance ;

  return ( distance ) ;
}

/******************************************************************************

  vec_diff:
  Substract two vectors. z = x - y
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void vec_diff_dbl ( const double *Px, const double *Py, const int mDim, double *Pz )
{
 int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Pz[nDim] = Px[nDim] - Py[nDim] ;
 
 return ;
}

/******************************************************************************

  vec_add_dbl:
  Add two vectors. z = x + y
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void vec_add_dbl ( const double *Px, const double *Py, const int mDim, double *Pz )
{
 int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Pz[nDim] = Px[nDim] + Py[nDim] ;
 
 return ;
}

/******************************************************************************

  vec_add_dbl:
  Add two vectors. z = x + a*y
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void vec_add_mult_dbl ( const double *Px, const double a, const double *Py,
                        const int mDim, double *Pz ) {
 int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Pz[nDim] = Px[nDim] + a*Py[nDim] ;
 
 return ;
}

/******************************************************************************

  cross_prod_dbl:
  Vector product x \cross y = z

  Why do I return the 2-D cross-product in the x-component? Fix this.
  
  Last update:
  ------------
  4Dec19; remove redundant copy in 3d also, 
          fix bug with non-assignment of output in 2d.
  22Sep19; remove redundant 2-d self-assignment.
  20Jun06; allow overwrite. 
  : conceived.
  
  Input:
  ------
  pX, pY: vecs to cross 
  mDim: number of dims

  Output:
  -------
  pZ: cross product
  
*/

void cross_prod_dbl ( const double *pX, const double *pY, const int mDim, double *pZ ) {
  
  if ( mDim == 2 ) {
    pZ[0] = pX[0]*pY[1] - pX[1]*pY[0] ;
    pZ[1] = 0. ;
  }
  else if ( mDim == 3 ) {
    pZ[0] = pX[1]*pY[2] - pX[2]*pY[1] ;
    pZ[1] = pX[2]*pY[0] - pX[0]*pY[2] ;
    pZ[2] = pX[0]*pY[1] - pX[1]*pY[0] ;
  }
  else {
    sprintf ( hip_msg, "can't do cross_prod_dbl in %d-D.", mDim ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
 
 return ;
}

/******************************************************************************

  scal_prod_dbl:
  Scalar product x . y = z.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double scal_prod_dbl ( const double *Px, const double *Py, const int mDim ) {
  
 int nDim ;
 double sProd = 0. ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    sProd += Px[nDim]*Py[nDim] ;
 
 return ( sProd ) ;
}


/******************************************************************************
  dih_angle:   */

/*! given two outward face normals, their length, and an edge vector, compute the dihedral angle.
 *
 * more detailed desc for doxygen
 *
 */

/*
Copyright Jens-Dominik Mueller and CERFACS, see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>
*/


/*
  
  Last update:
  ------------
  16Dec16: conceived.
  

  Input:
  ------
  n1, len1 = first vector and length
  n2, len2 = 2nd vector and length, cross product of n1 x n2 should point into dir of eg if convex.
  eg = vector along the edge between the two faces/normals.
    
  Returns:
  --------
  scalar product (cos al) of angle between the two normals if convex. If concave, returns -2-cos(al).
  
*/

double dih_angle ( double *n1, double len1, double *n2, double len2, double *eg ) {
  
  double scProd = scal_prod_dbl ( n1, n2, 3 )/len1/len2 ;        

  double crPrd[3] ;
  cross_prod_dbl ( n1, n2, 3, crPrd ) ;

  double scProd2 = scal_prod_dbl ( eg, crPrd, 3 ) ;

  if ( scProd2 < 0 )
    scProd = -2.-scProd ; 
  
  return ( scProd2 ) ;
}


/******************************************************************************

  vec_mult_dbl:
  Take the average of two vectors.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void vec_mult_dbl ( double *Px, const double mult, const int mDim ) {
  
  int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Px[nDim] *= mult ;
 
 return ;
}



/******************************************************************************

  vec_avg_dbl:
  Take the average of two vectors.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void vec_avg_dbl ( const double *Px, const double *Py, const int mDim, double *Pz ) {
  int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Pz[nDim] = .5*( Px[nDim] + Py[nDim] ) ;
 
 return ;
}

void vec_avg_4dbl_3d ( const double *P0, const double *P1,
                       const double *P2, const double *P3,
                       double *Pavg ) {

  /* Loop over all dimensions. */
  Pavg[0] = .25*( P0[0] + P1[0] + P2[0] + P3[0] ) ;
  Pavg[1] = .25*( P0[1] + P1[1] + P2[1] + P3[1] ) ;
  Pavg[2] = .25*( P0[2] + P1[2] + P2[2] + P3[2] ) ;
 
 return ;
}

/* Same but update both Px and Py with the average. */
void vec_avg2_dbl ( double *Px, double *Py, const int mDim ) {
  int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Py[nDim] = Px[nDim] = .5*( Px[nDim] + Py[nDim] ) ;
 
 return ;
}


/******************************************************************************

  vec_copy_dbl:
  Copy a vector.
  
  Last update:
  ------------
  6Apr09; use EXIT_FAILURE on exit.
  7Sep00; intro a buffer tmp to allow in situ copying.
  : conceived.
  
  Input:
  ------
  Px: source vector
  mDim: dize

  Output:
  -----------
  Pz: destination vector.
  
  
*/

void vec_copy_dbl ( const double *Px, const int mDim, double *Pz ) {
  
  ulong_t sz = mDim*sizeof( double ) ;

  /* Use a temporary buffer to allow in situ copying. */
  static double tmp[99] ;
#ifdef DEBUG
  if ( mDim > 99 ) {
    printf ( " FATAL: too large a vector (>99) in vec_copy_dbl\n" ) ;
    exit ( EXIT_FAILURE ) ;
  }
#endif

  memcpy ( tmp, Px, sz ) ;
  memcpy ( Pz, tmp, sz ) ;
 
  return ;
}

/******************************************************************************

  vec_init_dbl:
  Initialize a vector with some constant.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void vec_ini_dbl ( const double iniDbl, const int mDim, double *Pz ) {
  
  int nDim ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    *Pz++ = iniDbl ;
  
  return ;
}


/******************************************************************************

  rot_coor_dbl:
  Project a vector onto a new orthogonal normal basis given in that coordinate system.
  Note that pCoor and pNewCoor may NOT overlap.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pCoor:     The coordinate/vector to transform.
  pBase:     The new basis, given as xyxyxy in 2D or xyzxyzxyz in 3D, normalized,
             orthogonal, right-hand system, all usual disclaimers.
  mDim:      The number of dimensions.

  Changes To:
  -----------
  pNewCoor: The vector in the new base.
  
*/

void rot_coor_dbl ( const double *pCoor, const double *pBase, const int mDim,
		    double *pNewCoor ) {
  
  int nDim ;
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    pNewCoor[nDim] = scal_prod_dbl ( pCoor, pBase+nDim*mDim, mDim ) ;

  return ;
}


/******************************************************************************

  max_diff_vec_dbl:
  Find the max difference of all vector components.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pVecA/B: the two candidates for the max.
  mDim:    dimensions.

  Changes To:
  -----------
  pVecMax: the componentwise maximum.
  
*/

void max_diff_vec_dbl ( const double *pVecA, const double *pVecB, const int mDim,
		   double *pVecMax ) {
  
  int nDim ;
  double diff ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    diff = pVecA[nDim] - pVecB[nDim] ;
    diff = ABS( diff ) ;
    *pVecMax = MAX( *pVecMax, diff ) ;
  }

  return ;
}

/******************************************************************************

  max_vec_dbl:
  Find the componentwise maximum of a vector. Note that the increment may NOT
  be used in the MAX macro.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pVecA/B: the two candidates for the max.
  mDim:    dimensions.

  Changes To:
  -----------
  pVecMax: the componentwise maximum.
  
*/

void vec_max_dbl ( const double *pVecA, const double *pVecB, const int mDim,
		   double *pVecMax ) {
  
  int nDim ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    pVecMax[nDim] = MAX( pVecA[nDim], pVecB[nDim] ) ;

  return ;
}

/******************************************************************************

  vec_min_dbl:
  Find the componentwise minimum of a vector. 
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pVecA/B: the two candidates for the min.
  mDim:    dimensions.

  Changes To:
  -----------
  pVecMin: the componentwise minimum.
  
*/

void vec_min_dbl ( const double *pVecA, const double *pVecB, const int mDim,
		   double *pVecMin ) {
  
  int nDim ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    pVecMin[nDim] = MIN( pVecA[nDim], pVecB[nDim] ) ;

  return ;
}



/******************************************************************************

  transpose_dbl:
  Transpose a square matrix of mDim.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pMat
  mDim

  Changes To:
  -----------
  pMat
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int transpose_dbl ( const double *pMat, const int mDim, double *pMatT ){

  double a ;

  vec_copy_dbl ( pMat, mDim*mDim, pMatT ) ;
  
  if ( mDim == 2 ) {
    a = pMatT[1] ; pMatT[1] = pMatT[2] ; pMatT[2] = a ;
    return ( 1 ) ;
  }
  else if ( mDim == 3 ) {
    a = pMatT[1] ; pMatT[1] = pMatT[3] ; pMatT[3] = a ;
    a = pMatT[2] ; pMatT[2] = pMatT[6] ; pMatT[6] = a ;
    a = pMatT[5] ; pMatT[5] = pMatT[7] ; pMatT[7] = a ;
    return ( 1 ) ;
  }
  else {
    printf ( " FATAL: illegal dimension %d in transpose_dbl.\n", mDim ) ;
    return ( 1 ) ;
  }
}

/******************************************************************************
  mat_vec_dbl:   */

/*! matrix vector multiply
 *
 */

/*
  
  Last update:
  ------------
  16Feb16: conceived.
  

  Input:
  ------
  pA: A in Ax=y in row major linear storage (a11, a12, a13, a21 ... )
  m,n: number of rows, columns of A
  px: x

  Output:
  -------
  y
  
*/

void mat_vec_dbl ( const double *pA, const int m, const int n, 
                   const double *px, double *py ) {

  int i ;
  for ( i = 0 ; i < m ; i++ )
    py[i] = scal_prod_dbl ( pA+i*n, px, n ) ;
 
  return ;
}


/******************************************************************************

  matmult_dbl:
  Multiply two square matrices. Matrices are stored rowwise.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void matmult_dbl ( const double *pA, const double *pB, const int mDim, double *pAB ) {

  int i, j ;
  double *pRowAB, bT[MAX_DIM*MAX_DIM] ;

  for ( i = 0 ; i < mDim*mDim ; i++ )
    pAB[i] = 0. ;

  transpose_dbl ( pB, mDim, bT ) ;

  for ( i = 0 ; i < mDim ; i++ ) {
    pRowAB = pAB + i*mDim ;
    
    for ( j = 0 ; j < mDim ; j++ )
      pRowAB[j] = scal_prod_dbl ( pA+i*mDim, bT+j*mDim, mDim ) ;
  }
    
  return ;
}


/******************************************************************************
  fnscanf_line:   */

/*! Advance read ptr up to & including line feed.
 */

/*
  
  Last update:
  ------------
  23Feb14: conceived.
  

  Input:
  ------
  File: positioned file to read
  sz: # of characters, including \0

  Changes To:
  -----------
  *str
    
  Returns:
  --------
  0 if end of file, 1 otherwise.
  
*/

int fscanf_end_line ( FILE *File ) {

  // skip to end of line.
  fscanf ( File, "%*[^\n]" ) ;
  fscanf ( File, "%*[\n]" ) ;

  if ( !feof( File ) ) 
    return ( 1 ) ;
  else
    return ( 0 ) ;
}



/******************************************************************************
  fscanf_str_line:   */

/*! Read chars up to n chars or line feed including blanks, then skip to line feed. 
 */

/*
  
  Last update:
  ------------
  23Feb14: conceived.
  

  Input:
  ------
  File: positioned file to read
  sz: # of characters, including \0

  Changes To:
  -----------
  *str
    
  Returns:
  --------
  # of characters read.
  
*/

int fscanf_str_line ( FILE *File, int sz, char *str ) {

  fgets ( str, sz, File ) ;
  
  // replace \n with \0
  int len = strlen(str);
  if (len > 0 && str[len-1] == '\n')
    str[len-1] = '\0';

  return ( len ) ;
}


/******************************************************************************

  ftnchar:
  fill a string with blanks to a specific length, as would be done for a fortran 
  output. Can be in situ. No trailing \0 is added.
  
  Last update:
  ------------
  17Jun09; switch args to avoid confusion, align with strncmp order, destination first.
  26May09: conceived.
  
  Input:
  ------
  inString: ptr to char to write
  len: length to pad to
  outString: ptr to char field of length len
  
  Changes To:
  -----------
  outString: ptr to padded char.

*/
void ftnString ( char* outString, const int len, const char* inString ) {

  const char *pi = inString ;
  char *po = outString ;

  while ( *pi != '\0' && pi - inString < len )
    *po++ = *pi++ ;

  while ( po - outString < len-1 )
    *po++ = ' ' ;

  *po++ = '\0'; 

  return ;
} 

/******************************************************************************

  ftnString0:
  fill a string with blanks to a specific length, as would be done for a fortran 
  output. Trailing \0 is added as the final character.
  
  Last update:
  ------------
  27Apr16; derived from ftnString by GS.
  
  Input:
  ------
  inString: ptr to char to write
  len: length to pad to
  outString: ptr to char field of length len
  
  Changes To:
  -----------
  outString: ptr to padded char.

*/
void ftnString0 ( char* outString, const int len, const char* inString ) {

  const char *pi = inString ;
  char *po = outString ;

  while ( *pi != '\0' && pi - inString < len )
    *po++ = *pi++ ;

  while ( po - outString < len-1 )
    *po++ = ' ' ;

  *po++ = '\0'; 

  return ;
} 



/******************************************************************************
  specchar2underscore:   */

/*! Replace blanks in a string with underscores.
 *
 */

/*
  
  Last update:
  ------------
  13Jan15: derived from blank2underscore.
  

  Changes To:
  -----------
  string
  
*/

void specchar2underscore ( char *string  ) {

  int len = strlen( string ) ;
  char *pCh ;

  for ( pCh = string ; pCh < string+len ; pCh++ ) {
    switch ( *pCh ) {
    case ' ': *pCh = '_' ; break ;
    case '=': *pCh = '_' ; break ;
    case '(': *pCh = '_' ; break ;
    case ')': *pCh = '_' ; break ;
    case '[': *pCh = '_' ; break ;
    case ']': *pCh = '_' ; break ;
    case '{': *pCh = '_' ; break ;
    case '}': *pCh = '_' ; break ;
    }
  }

  return ;
}


/******************************************************************************
  blank2underscore:   */

/*! Replace blanks in a string with underscores.
 *
 */

/*
  
  Last update:
  ------------
  22Dec14: conceived.
  

  Changes To:
  -----------
  string
  
*/

void blank2underscore ( char *string  ) {

  int len = strlen( string ) ;
  char *pCh ;

  for ( pCh = string ; pCh < string+len ; pCh++ )
    if ( *pCh == ' ' )
      *pCh = '_' ;

  return ;
}


/******************************************************************************
  trim:   */

/*! Trim spaces and tabs from beginning and end of a string.
 */

/*
  
  Last update:
  ------------
  7Apr14; tidy up, in chase of a an O2 bug, remvove possible address beyond mem
  6Apr13: moved here from read_n3s.
  
  Changes To:
  -----------
  s: string
  
*/


void trim(char *s) {

  // Trim spaces and tabs from beginning:
  int i=0,j;
  while((s[i]==' ')||(s[i]=='\t')) {
    i++;
  }
  int leni = strlen(s)-i ; 
  if( i>0 ) {
    for( j=0 ; j<leni ; j++ ) {
      s[j]=s[j+i];
    }
    s[j]='\0';
  }

  // Trim spaces and tabs from end:
  i=leni-1;
  while((s[i]==' ')||(s[i]=='\t')) {
    s[i]='\0';
    i--;
  }
  return ;
}


/******************************************************************************

  prepend_path:
  Take a filename argument and prepend the path, if the given filename is
  relative.
  
  Last update:
  ------------
  18Dec14; allow zero filenames to wash through untreated.
  13Jul04; text field now of TEXT_LEN
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

char *prepend_path ( char *fileName ) {

  char oldFileName[TEXT_LEN] ;

  r1_stripquote ( fileName, TEXT_LEN ) ;

  if ( fileName[0] != '\0' && fileName[0] != '/' ) {
    /* There is a file with a relative path, prepend. */

    strncpy ( oldFileName, fileName, TEXT_LEN-1 ) ;
    strncpy ( fileName, Grids.path, TEXT_LEN-1 ) ;
    strncat ( fileName, oldFileName, TEXT_LEN-1 ) ;

    if ( strlen ( Grids.path ) + strlen ( oldFileName ) >= TEXT_LEN )
      printf ( " SORRY: complete path too long in prepend_path.\n" ) ;
  }

  return ( fileName ) ;
}


/******************************************************************************

  strcpy_prepend_path:
  Take a filename argument and prepend the path, if the given filename is
  relative, copy to new string.
  
  Last update:
  ------------
  17Dec19; derived from prepend_path.
  
  Input:
  ------
  fileName

  Output:
  -----------
  fileNameWithPath
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void strcpy_prepend_path ( char *fileNameWithPath, const char *fileNameNoPath ) {

  /* Copy fileName, to allow a const char fileNameNoPath input arg. */
  char fileName[TEXT_LEN] ;
  strncpy ( fileName, fileNameNoPath, TEXT_LEN ) ;
  r1_stripquote ( fileName, TEXT_LEN ) ;

  if ( fileName[0] != '/' ) {
    /* There is a file with a relative path, prepend. */

    if ( strlen ( Grids.path ) + strlen ( fileName ) >= TEXT_LEN )
      hip_err ( warning, 1, "complete path too long"
                " in strcpy_prepend_path, ignored." ) ;
    else {
      strncpy ( fileNameWithPath, Grids.path, TEXT_LEN-1 ) ;
      strncat ( fileNameWithPath, fileName, TEXT_LEN-1 ) ;
    }
  }

  return ;
}


/******************************************************************************
  isNumRange:   */

/*! does a string represent a numerical range such as 1-4,5,9-12
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  18Jul11: conceived.
  

  Input:
  ------
  rg: char string representing a range
    
  Returns:
  --------
  1 if it is a numerical range, 0 otherwise.
  
*/

int isNumRange ( const char rg[] ) {

  const char *pr = rg ;
  
  while ( *pr !='\n' && *pr != '\0' ) {
    if ( isdigit ( *pr ) )
      ;
    else if ( *pr == ',' )
      ;
    else if ( *pr == '-' )
      ;
    else
      return ( 0 ) ;
    pr++ ;
  }

  return ( 1 ) ;
}

/* Convert string to all lowercase letters. */
void tolowerstr ( char *p ) {
  for ( ; *p; ++p)
    *p = tolower(*p);
  return;
}

/**************************************************************************************
  Some standard comparison functions.

 17Dec10; rename nan to my_nan to avoid conflict with intrinsic.
*/

int cmp_char ( const void *pChar0, const void *pChar1 ) {
  return ( ( *( char* )pChar0 ) - ( *( char* )pChar1 ) ) ;
}

int cmp_int ( const void *pInt0, const void *pInt1 ) {
  return ( ( *( int* )pInt0 ) - ( *( int* )pInt1 ) ) ;
}

int cmp_ulong_t ( const void *pSzt0, const void *pSzt1 ) {
#ifdef DEBUG
  ulong_t* upSzt0 =  ( ulong_t* ) pSzt0 ;
  ulong_t* upSzt1 =  ( ulong_t* ) pSzt1 ;
  return ( ( *upSzt0 ) - ( *upSzt1 ) ) ;
#else
  return ( ( *( ulong_t* )pSzt0 ) - ( *( ulong_t* )pSzt1 ) ) ;
#endif
}

int cmp_float ( const void *pFloat0, const void *pFloat1 ) {
  float diff = ( ( *( float* )pFloat0 ) - ( *( float* )pFloat1 ) ) ;
  if ( diff > 0. )
    return ( 1 ) ;
  else if ( diff < 0. )
    return ( -1 ) ;
  else
    return ( 0 ) ;
}

int cmp_double ( const void *pDouble0, const void *pDouble1 ) {
  double diff = ( *( double* )pDouble0 ) - ( *( double* )pDouble1 ) ;
  if ( diff > 0. )
    return ( 1 ) ;
  else if ( diff < 0. )
    return ( -1 ) ;
  else
    return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  i32_pack4i:
*/
/*! Pack 4 8bit unsigned integers into one 32 bit unsigned int
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  20Apr20: conceived.
  

  Input:
  ------
  int3-0, integer packed value for each field
  m: number of fields
  pI: array with one int per field.
    
  Returns:
  --------
  the packed 32bit integer.
  
*/

unsigned int i32_pack4i
( const int i3, const int i2, const int i1, const int i0 ) {

  unsigned int i32 = 0 ;

  i32 += ( i3 & 1 ), i32 <<= 1 ;
  i32 += ( i2 & 1 ), i32 <<= 1 ;
  i32 += ( i1 & 1 ), i32 <<= 1 ;
  i32 += ( i0 & 1 ) ;

  return ( i32 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  i32_packNi:
*/
/*! Pack N<=32 8bit unsigned integers into one N-bit unsigned int
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  25Apr23; fix bug with MAX->MIN, add doc.
  20Apr20: derived from i32_4bit
  

  Input:
  ------
  m: number of fields
  pI: array with one int per field.
    
  Returns:
  --------
  the packed integer with the lowest N bits set.
  
*/

unsigned int i32_packNi ( int m, const int *pi ) {
#undef FUNLOC
#define FUNLOC "in fun_name"
  
  unsigned int i32 = 0 ;

  m = MIN( m, 32 ) ;
  int k ;
  for ( k = m-1 ; k >0 ; k-- ) {
    i32 += ( pi[k] & 1 ) ;
    i32 <<= 1 ;
  }
  i32 += ( pi[0] & 1 ) ;

  return ( i32 ) ;
}

// Published for C++, Need to check if those also work in c.
// https://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit

void i32_set_bit_n ( int *pi32, const int n ) {
#undef FUNLOC
#define FUNLOC "i32_set_bit_n"
  if ( n >= 32 )
    hip_err ( fatal, 0, "only 32 bits possible "FUNLOC"." ) ;
  
  *pi32 |= 1UL << n;
  return ;
}

void i32_set_bit_n_value ( int *pi32, const int n, int iVal ) {
#undef FUNLOC
#define FUNLOC "i32_set_bit_n_value"
  if ( n >= 32 )
    hip_err ( fatal, 0, "only 32 bits possible "FUNLOC"." ) ;

  // make sure iVal is only 0 or 1
  iVal = !!iVal ;
  *pi32 ^= (-iVal ^ *pi32) & (1UL << n);
  return ;
}

void i32_reset_bit_n ( int *pi32, const int n ) {
#undef FUNLOC
#define FUNLOC "i32_reset_bit_n"
  if ( n >= 32 )
    hip_err ( fatal, 0, "only 32 bits possible "FUNLOC"." ) ;

  *pi32 &= ~(1UL << n) ;
  return ;
}

void i32_toggle_bit_n ( int *pi32, const int n ) {
#undef FUNLOC
#define FUNLOC "i32_toggle_bit_n"
  if ( n >= 32 )
    hip_err ( fatal, 0, "only 32 bits possible "FUNLOC"." ) ;

  *pi32  ^= 1UL << n;
  return ;
}

unsigned int i32_check_bit_n ( int *pi32, const int n ) {
#undef FUNLOC
#define FUNLOC "i32_check_bit_n"
  if ( n >= 32 )
    hip_err ( fatal, 0, "only 32 bits possible "FUNLOC"." ) ;
  int i = *pi32 ;

  return ( (i >> n) & 1U );
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  i32_unpack4int:
*/
/*! Pack 4 8bit unsigned integers into one 32 bit unsigned int
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  20Apr20: conceived.
  

  Input:
  ------
  int3-0, integers with at most 8 bits.
    
  Returns:
  --------
  the packed 32bit integer.
  
*/

void i32_unpack4i ( int i32, int *pi3, int *pi2, int *pi1, int *pi0 ) {

  *pi0 = ( i32 & 1 ), i32 >>= 1 ;
  *pi1 = ( i32 & 1 ), i32 >>= 1 ;
  *pi2 = ( i32 & 1 ), i32 >>= 1 ;
  *pi3 = ( i32 & 1 ) ;

  return ;
}





/******************************************************************************
  ceil_bsearch:   */

/*! Find the next larger element in an ordered list using binary search.
 *
 *
 */

/*
  
  Last update:
  ------------
  8Jan15; treat hitting top end of stack (also empty stack) properly.
  5Jan15: derived from bsearch.
  

  Input:
  ------
  key: key to match
  base: base pointer
  nmemb: number of entries
  size: their size
  compar: comparison function.
  
  Returns:
  --------
  the closest match from below.
  
*/

void *ceil_bsearch ( const void *key, const void *base0,
                      size_t nmemb, size_t size,
                      int (*compar)(const void *, const void *)) {

  const char *base = base0;
  size_t lim;
  int cmp;
  const void *p;

  if ( nmemb == 0 )
    // Empty list.
    return ( NULL ) ;

  p = base+(nmemb-1)*size ;
  cmp = (*compar)(key, p);
  if ( cmp > 0 )
    // beyond the last item
    return ( NULL ) ;

  for (lim = nmemb; lim != 0; lim >>= 1) {
    p = base + (lim >> 1) * size;
    cmp = (*compar)(key, p);
    if (cmp == 0) {
      // return ((void *)p);
      hip_err ( fatal, 0, "matching key %d found in floor_bsearch." ) ;
    }
    if ( lim == 1 ) {
      // bracketed.
      return ( (void*) p+1 ) ;
    }
    if (cmp > 0) {	/* key > p: move right */
      base = (char *)p + size;
      lim--;
    }		/* else move left */
  }
  return (NULL);
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  det3:
*/
/*! compute a 3x3 determinant
 *
 *
 */

/*
  
  Last update:
  ------------
  16Feb17: conceived.
  

  Input:
  ------
  mat = matrix
    
  Returns:
  --------
  det = determinant
  
*/

double det3 ( double mat[3][3] ) {

  // downwards, to lower right.
  double d0 = mat[0][0]*mat[1][1]*mat[2][2] ;
  double d1 = mat[0][1]*mat[1][2]*mat[2][0] ;
  double d2 = mat[0][2]*mat[1][0]*mat[2][1] ;

  // upwards, to upper right.
  double u0 = mat[2][0]*mat[1][1]*mat[0][2] ;
  double u1 = mat[2][1]*mat[1][2]*mat[0][0] ;
  double u2 = mat[2][2]*mat[1][0]*mat[0][1] ;

  double det = d0+d1+d2-u0-u1-u2 ;
  return ( det ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  bsearch_void:
*/
/*! perform a bsearch on an ordered list with embedded empty elements.
 *
 */

/*
  
  Last update:
  ------------
  4May20: dervied from Unix bsearch. Needs validation.
  

  Input:
  ------
 * key: pointer to item being searched for
 * base: pointer to first element to search
 * num: number of elements
 * size: size of each element
 * cmpFun: pointer to comparison function

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  NULL on failure, pointer to match on success
  
*/

void *bsearch_void(const void *key, const void *base, size_t num, size_t size,
                   int (*cmpFunVoid)( const void *, const void *, int *) ) {
  
  const char *pivot, *pivLo, *pivHi;
  int result, resLo=0, resHi=0 ; // to pacify compiler warning.
  int isInvalid, isInvLo, isInvHi;

  const char *pivBase ;
  const char *pivLast ;


  while (num > 0) {
    pivot = ( (char*) base ) + (num >> 1) * size ;
    result = cmpFunVoid( key, pivot, &isInvalid ) ;

    if ( isInvalid ) {
      // pivot is a void/empty element. Search up and down to find a valid
      // one. Search both dir, to ensure that the interval is decreased.
      pivBase = base ;
      for ( pivLo = pivot-size ; isInvalid && pivLo >= (char*) base ; pivLo -= size )
        resLo = cmpFunVoid( key, pivLo, &isInvalid ) ;

      if ( resLo == 0 )
        // Found.
        return (void *) pivLo;


      if ( resLo < 0 )
        // Next valid element down is still too large. 
        result = resLo ;
      
      else {
        /* Search for a valid element in the upper half of the interval. */
        isInvalid = 1 ;
        pivLast = base + (num-1) * size ;
        for ( pivHi = pivot+size ; isInvalid && pivHi <= pivLast ; pivHi += size )
          resHi = cmpFunVoid( key, pivHi, &isInvalid ) ;

        if  ( resHi == 0 )
          // Found.
          return (void *) pivHi;

        if ( resLo < 0 && resHi > 0 )
          // Bracketed interval, but no match.
          return ( NULL ) ;

        result = resHi ;
      }
    } //  if ( isInValid ) {

    if (result == 0)
      // Found.
      return (void *)pivot;

    if (result > 0) {
      // Key is larger
      base = pivot + size;
      num--;
    }
    else
      // Key is smaller
      num >>= 1;
  }

  return NULL;
}



#ifdef DEBUG
void pr_distDbl ( const double *pCo0, const double *pCo1, const int mDim ) {

  static int nDim ;
  static double diff, distance ;

  /* Loop over all dimensions. */
  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ ) {
    diff = pCo0[nDim] - pCo1[nDim] ;
    distance += diff*diff ;
  }
  if ( mDim == 2 )
    printf ( " (%g, %g) - (%g, %g): %g\n",
             pCo0[0], pCo0[1], pCo1[0], pCo1[1], distance ) ;
  else 
    printf ( " (%g, %g, %g) - (%g, %g, %g): %g\n",
             pCo0[0], pCo0[1], pCo0[2], pCo1[0], pCo1[1], pCo1[2], distance ) ;
}


int my_nan ( double a ) {

  if ( a-a != 0. )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

  
int nan_check ( double *pV, int mI, int mJ ) {

  int i, j ;

  for ( j = 0 ; j < mJ ; j++ )
    for ( i = 0 ; i < mI ; i++ )
      if ( my_nan ( pV[j*mI+i] ) ) {

        printf ( " FATAL: nan in i,j %d,%d\n", i, j ) ;
        return ( 0 ) ;
      }
  return ( 1 ) ;
}

#endif

