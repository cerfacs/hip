/*
   read_avbp3_prt.c:
   Read a set of files in AVBP 4.2 format. Adaptive hierarchy.


   Last update:
   ------------


   This file contains:
   -------------------
   read_avbp3_prt
   
*/

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern const refType_struct bufferType[MAX_FACES_ELEM*MAX_CHILDS_FACE+1] ;

/******************************************************************************

  read_avbp3_prt:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp3_prt ( FILE *Fparent, uns_s *pUns, chunk_struct **ppChunk )
{
  int mPrtElems, mLeafElems, mPrtElem2VertP, mPrtElem2ChildP,
      mPrtBndPatches, mPrtBndFaces, mChildren, readOk = 1, nRoot, nParent, nRefType,
      kVx, kCh, number [5] ;
  chunk_struct *Pchunk, *PfineChunk = pUns->pRootChunk ;
  elem_struct *Pelem, **PPlstCh    ;
  vrtx_struct **PPlstVx ;
  elType_e elType ;

  *ppChunk = NULL ;

  if ( verbosity > 2 )
    printf ( "      Reading parents and element hierarchy.\n" ) ;

  readOk = 0 ;
  if ( FREAD ( &mPrtElems, sizeof( int ), 1, Fparent ) == 1 )
    if ( FREAD ( &mLeafElems, sizeof( int ), 1, Fparent ) == 1 )
      if ( FREAD ( &mPrtElem2VertP, sizeof( int ), 1, Fparent ) == 1 )
	if ( FREAD ( &mPrtElem2ChildP, sizeof( int ), 1, Fparent ) == 1 )
	  if ( FREAD ( &mPrtBndPatches, sizeof( int ), 1, Fparent ) == 1 )
	    if ( FREAD ( &mPrtBndFaces, sizeof( int ), 1, Fparent ) == 1 )
	      readOk = 1 ;
  if ( !readOk ) {
    printf ( " FATAL: could not read mPrts, mLeafs in read_avbp3_prt.\n" ) ;
    return ( 0 ) ; }

  if ( !mPrtElems ) {
    if ( verbosity > 0 )
      printf ( "    INFO: no parent elements found in read_avbp3_prt.\n" ) ;
    return ( 1 ) ;
  }

  if ( !( Pchunk = append_chunk ( pUns, pUns->mDim, 
                                  mPrtElems, mPrtElem2VertP, mPrtElem2ChildP,
				  0, mPrtBndFaces, mPrtBndPatches ) ) ) {
    printf ( " FATAL: could not allocate parent spaces in read_avbp3_prt.\n" ) ;
    return ( 0 ) ; }
  else {
    /* Set the pointers to the last used space. */
    PPlstVx = Pchunk->PPvrtx - 1 ;
    PPlstCh = Pchunk->PPchild -1 ;
  }

  /* Loop over all parents. */
  for ( readOk = 1, Pelem = Pchunk->Pelem+1 ;
        readOk && Pelem <= Pchunk->Pelem + mPrtElems ; Pelem++ ) {
    readOk = 0 ;
    /* The element type. */
    if ( FREAD ( &elType, sizeof( int ), 1, Fparent ) == 1 )
      /* The element's root status, if there is one. */
      if ( FREAD ( &nRoot, sizeof ( int ), 1, Fparent ) == 1 )
	/* The parent, if there is one. */
	if ( FREAD ( &nParent, sizeof ( int ), 1, Fparent ) == 1 )
	  /* The refinement type, -1 for buffering. */
	  if ( FREAD ( &nRefType, sizeof( int ), 1, Fparent ) == 1 )
	    /* Number of children. */
	    if ( FREAD ( &mChildren, sizeof( int ), 1, Fparent ) == 1 )
	      readOk = 1 ;
    if ( readOk ) {
      Pelem->elType = elType ;
      /* All parents are in this chunk. */
      if ( nParent )
	Pelem->Pparent = Pchunk->Pelem + nParent ;
      else
	Pelem->Pparent = NULL ;
	
      if ( nRefType == -1 ) {
	/* Buffered. */
	Pelem->PrefType = bufferType+mChildren ;
	Pelem->term = 1 ;
      }
      else {
	/* Refined. */
	Pelem->PrefType = elemType[elType].PrefType + nRefType ;
	Pelem->term = 0 ;
      }
      Pelem->PPchild = PPlstCh + 1 ;
      Pelem->PPvrtx = PPlstVx + 1;

      /* Initialize the status. */
      Pelem->root = nRoot ;
      Pelem->leaf = Pelem->invalid = 0 ;
      
    }

#   ifdef CHECK_BOUNDS
      if ( PPlstVx + elemType[elType].mVerts >= Pchunk->PPvrtx + mPrtElem2VertP ||
	   PPlstCh + mChildren >= Pchunk->PPchild + mPrtElem2ChildP ) {
      printf ( " FATAL: beyond bounds in read_avbp3_prt.\n" ) ;
	return ( 0 ) ; }
#   endif
    
    /* The forming vertices. All vertices are stored with the fine chunk. */
    for ( kVx = 0 ; kVx < elemType[Pelem->elType].mVerts && readOk ; kVx++ )
      if ( FREAD ( number, sizeof( int ), 1, Fparent ) != 1 )
	readOk = 0 ;
      else
	*(++PPlstVx) = PfineChunk->Pvrtx + number[0] ;

    /* List the children. */
    for ( kCh = 0 ; kCh < mChildren && readOk ; kCh++ )
      if ( FREAD ( number, sizeof ( int ), 2, Fparent ) != 2 )
	readOk = 0 ;
      else if ( number[1] ) {
	/* This child is a leaf. */
	*(++PPlstCh) = PfineChunk->Pelem + number[0] ;
	(*PPlstCh)->Pparent = Pelem ;
 	(*PPlstCh)->root = 0 ;

	if ( Pelem->term )
	  /* Unterminalize all children. */
	  (*PPlstCh)->term = 0 ;
     }
      else {
	/* This child is another parent. */
	*(++PPlstCh) = Pchunk->Pelem + number[0] ;
	(*PPlstCh)->Pparent = Pelem ;
      }
  }

  if ( !readOk )
    printf ( " FATAL: error while reading parents in read_avbp3_parent.\n" ) ;

  if ( verbosity > 3 )
    printf ( "         Found %d parent elements.\n", mPrtElems ) ;

  *ppChunk = Pchunk ;
  return ( 1 ) ;
}


