/*
  proto_adapt.h:
  All headers for unstructured adaptive methods.

  Last update:
  ------------
  11Sep19; additional args to buffer_3D_elem.
  9Jul11; rename  number_uns_elemFromVerts_adapt.
  15May01; intro calc_abs.
  3Apr00; intro get_fcvx_aE.
  22Oct97; properly list the adEdge family of files.

*/

/* adapt_meth.c */
int std_distr ( double val[], int mVals, double *pAvg, double *pDev ) ;
void mark_all_edges ( uns_s *pUns ) ;
void mark_edges ( elem_struct *pElem,
                  int (*x_scalar) ( const elem_struct*, const int,
                                    double* , double* , double* ),
                  const int kVar,
                  const double valDeref, const double valRef ) ;

int calc_M_R_D ( const elem_struct *Pelem, const int kVar,
	         double *PMRD,
		 double *PAvgMRD,
		 double EdgeMRD[]) ;
int calc_abs ( const elem_struct *Pelem, const int kVar,
               double *PabsMax, double *PabsAvg, double abs[] ) ;
int calc_diff ( const elem_struct *Pelem, const int kVar,
	        double *PdiffMax, double *PdiffAvg, double diff[] ) ;


void number_uns_elem_leafNprt ( uns_s *pUns, int *PmLeafElems, 
			        int *PmPrtElems, int mPrtElemsPerType[],
                                int *PmPrtElem2VertP,
			        int *PmPrtElem2ChildP,
			        int *PmPrtBndPatches, int *PmPrtBndFaces ) ;

int adaptLvl_elem ( const elem_struct *Pelem ) ;
/* adapt_reset is public and prototyped in proto_uns.h */
/* adapt_nr is public and prototyped in proto_uns.h */
void printelal ( const elem_struct *Pelem ) ;

/* adEdge.c */
int get_child_aE ( const uns_s *pUns, const llEdge_s *pllAdEdge, adEdge_s *pAdEdge,
                   int nAe,
                   int nChAe[2], const vrtx_struct *pVx[3] ) ;
int get_grchild_aE ( const uns_s *pUns, const llEdge_s *pllAdEdge, adEdge_s *pAdEdge,
                     int nAe,
                     int nChAe[2], int nGrChAe[4], const vrtx_struct *pVx[5] ) ;

int get_face_aE ( const uns_s *pUns,
                  const elem_struct *pElem, const int kFace, int *pmVxBase,
                  int *pmVxFc, const vrtx_struct *pVxFc[2*MAX_VX_FACE+1],
                  int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                  int *pnFixAe, int *pfixDiagDir ) ;
int get_fcvx_aE ( const uns_s *pUns,
                  vrtx_struct *pVxCrnr[MAX_VX_FACE], int mVxBase,
                  int *pmVxFc, const vrtx_struct *pVxFc[2*MAX_VX_FACE+1],
                  int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                  int *pnFixAe, int *pfixDiagDir ) ;
int get_face_half_aE ( const uns_s *pUns,
                       int mVxBase, const vrtx_struct *pVxFc[2*MAX_VX_FACE+1],
                       int nHFcAe[2*MAX_VX_FACE],
                       const vrtx_struct *pVxHFc[2*MAX_VX_FACE] ) ;

void rm_adEdgeVx_elem ( uns_s *pUns, const elem_struct *pElem, 
                        int nAe[MAX_EDGES_ELEM],
                        int nCrossAe[MAX_FACES_ELEM+1][MAX_VX_FACE] ) ;
 
int add_adEdge_vrtx ( uns_s *pUns, vrtx_struct *pVrtx0, vrtx_struct *pVrtx1,
                      int *pnPerEg, int *pNewEg ) ;
int add_adEdge_elem ( uns_s *pUns, const elem_struct *pElem, const int kEdge,
                      int *pnPerEg, int *pNewEg ) ;

int add_elem_aE_vx ( uns_s *pUns, elem_struct *pElem,
                     const int doFaces, const int doBuf,
                     vrtx_struct *ppVrtx[MAX_VX_ELEM+MAX_ADDED_VERTS ],
                     chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                     double **ppLstCoor, double **ppLstUnknown,
                     int nAe[MAX_EDGES_ELEM],
                     int nCrossAe[MAX_FACES_ELEM+1][MAX_VX_FACE] ) ;
int add_quadFc_aE ( uns_s *pUns, vrtx_struct *pVxCrnr[MAX_VX_FACE],
                    const int doBuf,
                    vrtx_struct **ppVxCtr,
                    // gcc 11.3 doesn't like this.
                    // int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                    int *nFcAe, int *nCrossAe,
                    int *pnFixAe, int *pfixDiagDir,
                    chunk_struct *pRefChunk, vrtx_struct **ppLstVx, double **ppLstCoor,
                    double **ppLstUnknown ) ;

int write_adEdge ( const uns_s *pUns, const char *adFile ) ;
int read_adEdge ( uns_s *pUns, FILE *adFile ) ;

int match_per_aE ( uns_s *pUns ) ;
int clean_uns_adEdge ( uns_s *pUns, llEdge_s *pllAdEdge, adEdge_s *pAdEdge ) ;
int count_newVx_llAe ( const llEdge_s *pllAdEdge, const adEdge_s *pAdEdge ) ;

/* void list_adEdge ( const uns_s *pUns, void const * const pData ) ;*/
void list_adEdge ( const uns_s *pUns, void const * const pData ) ;

/* buffer_uns.c: */
void init_childSpc ( childSpc_s *PchildSpc, uns_s *pUns, chunk_struct *PbufChunk ) ;

int buffer_elems ( uns_s *pUns, chunk_struct *PbufChunk ) ;

void buf2ref_vol_ctr ( uns_s *pUns,
                       const int doRefNegVol, const int doRefCtrVx,
                       int *pChange ) ;

elem_struct * debuffer_elem ( elem_struct *Pelem ) ;

/* buffer_uns_2D.c: */
void count_buf_elems_2D ( const uns_s *pUns,
                          int *PmNewElems, int *PmNewElem2VertP ) ;

int buffer_2D_elem ( elem_struct *Pelem, const uns_s *pUns,
		     childSpc_s *PchildSpc ) ;

int add_child_2D ( childSpc_s *PchildSpc,
		   elem_struct *Pelem, elType_e chType, vrtx_struct *PvxPrt[],
		   int kVxPrt0, int kVxPrt1, int kVxPrt2, int kVxPrt3 ) ;

/* buffer_uns_3D.c */
void estim_buf_elems_3D ( const uns_s *pUns,
			  int *PmNewElems, int *PmNewElem2VertP, int *PmNewVerts,
			  int *PmNewBndFc, int *PmNewBndPatch ) ;

int buffer_3D_elem ( uns_s *pUns, elem_struct *Pelem, const int mVxHg, const int kVxHg[],
		     vrtx_struct *PvxElem[], const surfTri_s *PsurfTri,
		     childSpc_s *PchildSpc, const int doOnlyCheckVol,
                     int *pmMakesNegVol, int *pDoesAddCtrVx ) ;

int add_child_3D ( elType_e chType, childSpc_s *PchildSpc,
		   elem_struct *Pelem, vrtx_struct *PvxPrt[],
		   surfTri_s *PsurfTri, const int kVx,
                    const int doCheckVol, int *pmNegVol ) ;
int add_center_3D ( uns_s *pUns, elem_struct *Pelem, vrtx_struct *PvxElem[],
		    const surfTri_s *PsurfTri, childSpc_s *PchildSpc,
                    const int doCheckVol, int *pmNegVol ) ;
elem_struct *add_child_3D_kVx ( elType_e chType, childSpc_s *PchildSpc,
                                elem_struct *Pelem, vrtx_struct *PvxPrt[],
                                const int kVxCh[], const int doCheckVol, int *pmNegVol) ;

/* buffer_hex.c */
int buffer_hex( uns_s *pUns, elem_struct *Pelem,
	        const int mVxHg, const int kVxHg[], vrtx_struct *PvxPrt[],
	        const surfTri_s *PsurfTri, childSpc_s *PchildSpc,
                const int doOnlyCheckVol,
                int *pmMakesNegVol, int *pDoesAddCtrVx ) ;
     

/* read_avbp3_adapt.c: */
int read_avbp3_prt ( FILE *Fparent, uns_s *pUns, chunk_struct **ppCchunk ) ;

/* read_avbp4.c: read_avbp4 is defined in proto_uns.h.*/
int read_avbp4_conn ( FILE *Fconn, FILE *FelMark, uns_s *pUns, vrtx_struct *PbaseVx ) ;


/* refbuf_bound.c */
int make_refbuf_bndfc ( uns_s *pUns, chunk_struct *pNewChunk ) ;

/* adapt_uns_hierarhical.c: */
int add_elem_crossFc ( uns_s *pUns, const elem_struct *pElem,
                       chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                       double **ppLstCoor, double **ppLstUnknown,
                       const int doBuf ) ;

int adapt_uns_hierarchical ( uns_s *pUns, const int iso ) ;
/* only called from adapt_uns_hierarchical. no need for prototype. */
// int deref_uns_hierarchical ( uns_s *pUns, int *PmDerefdElems, int *PmDerefdVerts ) ;
vrtx_struct *place_vx_edge ( const elem_struct *pElem, const int kEdge,
                             const uns_s *pUns, chunk_struct *pChunk,
                             vrtx_struct **PPlstVx, double **PPlstCoor,
                             double **PPlstUnknown ) ;
/* not called.
vrtx_struct *place_vx_face ( const elem_struct *pElem, const int kFace,
                             const uns_s *pUns, chunk_struct *pChunk,
                             vrtx_struct **PPlstVx, double **PPlstCoor,
                             double **PPlstUnknown ) ;
*/
vrtx_struct *adapt_uh_place_vx_elem ( const elem_struct *Pelem, 
			     const uns_s *pUns, chunk_struct *Pchunk,
			     vrtx_struct **PPlstVx, double **PPlstCoor,
			     double **PPlstUnknown ) ;
vrtx_struct *adapt_uh_place_vx ( const uns_s *pUns, chunk_struct *pChunk,
                        vrtx_struct **ppLstVx, double **ppLstCoor,
                        double **ppLstUnknown,
                        vrtx_struct *pVrtx[], const int mVxToAvg ) ;
int adapt_uh_match_elem_eg ( uns_s *pUns, elem_struct *pElem, int iso,
                    int *pChange ) ;
int match_all_refs ( uns_s *pUns, int iso, int *pmSweeps ) ;

/* uns_drvElem.c: */
int get_surfTri_drvElem ( const elem_struct *Pelem, const int mVxHg,
			  const int kVxHg[MAX_ADDED_VERTS],
			  const int fixDiag[MAX_FACES_ELEM+1],
			  const int diagDir[MAX_FACES_ELEM+1],
			  int mFacets[MAX_FACES_ELEM+1],
			  int mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
			  int kFacetVx[][MAX_CHILDS_FACE][MAX_VX_FACE] ) ;
int get_surfVx_drvElem ( const elem_struct *pElem, vrtx_struct *pHgVx[],
                         int mFacets[MAX_FACES_ELEM+1],
                         int mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
                         int kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
                         vrtx_struct
                         *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ) ;

elMark_s get_elMark_aE ( const uns_s *pUns, const elem_struct *Pelem,
			 int *PmVxHg ) ;
void elMark2int ( const elMark_s elMark, unsigned int elInt[2] ) ;
elMark_s int2elMark ( unsigned int elInt[2] ) ;

int number_uns_elemFromVerts_adapt ( uns_s *pUns, numberedType_e nrType ) ;


/* uns_refType.c: */
int add_uns_refType ( const refType_struct *PsampleRefType, const elem_struct *Pelem ) ;


/* uns_surfTri.c: */
surfTri_s *make_surfTri ( const uns_s *pUns, const elem_struct *Pelem,
			  int *PmVxHg, int kVxHg[], vrtx_struct *PvxPrt[] ) ;

int surfTri_mFacets ( const surfTri_s *PsurfTri ) ;
int surfTri_mVerts ( const surfTri_s *PsurfTri ) ;
int surfTri_n_edgedVx ( const surfTri_s *PsurfTri, int *PiVx, const int mFacets ) ;
int surfTri_diffVx ( const surfTri_s *PsurfTri, const int kVx,
		     const int iFacet, const int nDiff ) ;
int surfTri_ngh_facet ( surfTri_s *PsurfTri, int kFacet ) ;
int surfTri_nxt_facet ( const surfTri_s *PsurfTri, int *PkFacet,
		        int *PkFace, int *PmVertsFacet, int kVxFacet[] ) ;

void printSt ( const surfTri_s *PsurfTri ) ;
void printStvx ( const surfTri_s *PsurfTri, const vrtx_struct *PprtVx[] ) ;

/* write_avbp3_adapt.c: */
int write_avbp3_adapt ( uns_s *pUns, char *ProotFile ) ;
int write_avbp3_prt ( uns_s *pUns, char *PparentFile, int *pmPrtElems ) ;


/* write_avbp4.c: */
int write_avbp4 ( uns_s *pUns, char *ProotFile ) ;
