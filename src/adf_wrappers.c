/*
   adf_wrappers.c:
   Collect all adf utilities.

   Last update:
   ------------
   24Apr02; intro adf_delete.
   22Sep00, extracted from read/write_adf.

   This file contains:
   -------------------
   adf_query1
   adf_read1
   
   adf_query2
   adf_read2

   adf_listChildren

   adf_create
   adf_write1
   adf_write2

*/

extern const int verbosity ;
#include "cpre.h"
#include "proto.h"
#include "ADF.h"

int adf_delete ( const double pid, const char tag[] ) {
  
  double id ;
  int err ;

  ADF_Get_Node_ID ( pid, tag, &id, &err ) ;

  if ( err == -1 ) {
    /* Exists. */
    ADF_Delete ( pid, id, &err ) ;
    return ( 1 ) ;
  }
  else
    return ( 0 ) ;
}

double adf_query1 ( const double pid, const char tag[], int *pmData ) {

  double id ;
  int err, mDim, dimVals[2] ;
  
  
  ADF_Get_Node_ID ( pid, tag, &id, &err ) ;

  if ( err == 29 ) {
    /* node named tag does not exist. */
    *pmData = 0 ;
    return ( 0. ) ;
  }
  else if ( err != -1 ) {
    printf ( " FATAL: error %d seeking %s adf_query1.\n", err, tag ) ;
    return ( 0. ) ;
  }

  ADF_Get_Number_of_Dimensions ( id, &mDim, &err ) ;
  if ( mDim == 1 ) {
    ADF_Get_Dimension_Values ( id, pmData, &err ) ;
    if ( err != -1 ) {
      printf ( " FATAL: error quering data for %s in adf_query1.\n", tag ) ;
      return ( 0. ) ;
    }
  }
  else if ( mDim == 2 ) {
    /* hydra writes everything as 2D. */
    ADF_Get_Dimension_Values ( id, dimVals, &err ) ;
    if ( err != -1 ) {
      printf ( " FATAL: error quering data for %s in adf_query1.\n", tag ) ;
      return ( 0. ) ;
    }
    *pmData = dimVals[0]*dimVals[1] ;
    return ( id ) ;
  }
  else {
    /* Some error. */
    printf ( " FATAL: wrong dimension %d in adf_query1.\n", mDim ) ;
    return ( 0. ) ;
  }

  return ( id ) ;
}

int adf_read1 ( const double pid, const char tag[], const int mData, char *pData ) {
  
  double id ;
  int err, mDataBase, mDim, dimVals[2] ;
  const int one[2] = {1, 1} ;

  id = adf_query1 ( pid, tag, &mDataBase ) ;
  if ( id == 0.0 ) {
    printf ( " FATAL: no such child: %s in adf_read1.\n", tag ) ;

    if ( verbosity > 3 )
      adf_listChildren ( pid ) ;
    
    return ( 0 ) ;
  }
  ADF_Get_Number_of_Dimensions ( id, &mDim, &err ) ;
  mDataBase = MIN( mDataBase, mData ) ; 

  if ( mDim == 1 ) {
    ADF_Read_Data ( id,            one, &mDataBase, one,
                    1, &mDataBase, one, &mDataBase, one, pData, &err ) ;
    if ( err != -1 ) {
      printf ( " FATAL: error %d reading data for %s in adf_read1.\n", err, tag ) ;
      return ( 0 ) ;
    }
  }
  else if ( mDim == 2 ) {
    /* Read OPlus 2x2 fields. */
    ADF_Get_Dimension_Values ( id, dimVals, &err ) ;
    ADF_Read_Data ( id,            one, dimVals,    one,
                    1, &mDataBase, one, &mDataBase, one, pData, &err ) ;
    if ( err != -1 ) {
      printf ( " FATAL: error %d reading 2x2 data for %s in adf_read1.\n", err, tag ) ;
      return ( 0 ) ;
    }
  }
  else {
    /* Some error. */
    printf ( " FATAL: wrong dimension %d in adf_read1.\n", mDim ) ;
    return ( 0. ) ;
  }

  return ( 1 ) ;
}



double adf_query2 ( const double pid, const char tag[], int *pDim0, int *pDim1 ) {

  double id ;
  int err, mDim, nDim[2] ;
  
  ADF_Get_Node_ID ( pid, tag, &id, &err ) ;
  if ( err == 29 ) {
    /* node named tag does not exist. */
    *pDim0 = *pDim1 = 0 ;
    return ( 0. ) ;
  }
  else if ( err != -1 ) {
    printf ( " FATAL: error %d seeking %s adf_query2.\n", err, tag ) ;
    return ( 0. ) ;
  }

  ADF_Get_Number_of_Dimensions ( id, &mDim, &err ) ;
  if ( mDim != 2 ) {
    printf ( " FATAL: adf_query2 reads only 2-D data.\n" ) ;
    return ( 0. ) ;
  }

  ADF_Get_Dimension_Values ( id, nDim, &err ) ;
  *pDim0 = nDim[0] ;
  *pDim1 = nDim[1] ;

  return ( id ) ;
}


 
int adf_read2 ( const double pid, const char tag[],
                const int dim0, const int dim1, char *pData ) {
  
  double id ;
  int err, dimB0, dimB1 ;

  id = adf_query2 ( pid, tag, &dimB0, &dimB1 ) ;
  if ( dimB0 - dim0 || dimB1 - dim1 ) {
    printf ( " FATAL: dimension mismatch for: %s: %d,%d expected, %d,%d found "
             " in adf_read2.\n", tag, dim0, dim1, dimB0, dimB1 ) ;
    return ( 0 ) ;
  }

  ADF_Read_All_Data ( id, pData, &err ) ;

  if ( err != -1 ) {
    printf ( " FATAL: error %d reading data for: %s in adf_read2.\n", err, tag ) ;
    return ( 0 ) ;
  }
  else 
    return ( 1 ) ;
}

void adf_listChildren ( const double pid ) {

  int mCh, err, nCh, len, dims[99], mDim, nDim, mNm ;
  char tag[LINE_LEN], string[LINE_LEN] ;
  double cid ;
  
  
  if ( pid == 0. ) {
    printf ( "   invalid parent.\n" ) ;
    return ;
  }

  ADF_Get_Name ( pid, tag, &err ) ;
  ADF_Number_of_Children ( pid, &mCh, &err ) ;
  if ( err != -1 ) {
    printf ( "    invalid parent, err: %d\n", err ) ;
    return ;
  }
  else
    printf ( "  ---- node %32s (%23.16e), %d children:\n", tag, pid, mCh ) ;

  
  for ( nCh = 1 ; nCh <= mCh ; nCh++ ) {
    ADF_Children_Names ( pid, nCh, 1, LINE_LEN, &mNm, tag, &err ) ; 
    ADF_Get_Node_ID ( pid, tag, &cid, &err ) ;
    printf ( "          %d: %32s, ", nCh, tag ) ;

    ADF_Is_Link ( cid, &len, &err ) ;
    if ( len ) {
      ADF_Get_Link_Path( cid, string, tag, &err ) ;
      printf ( " linked to %s\n", string ) ;
      printf ( "          %d: %32s, ", nCh, tag ) ;
    }

    ADF_Get_Data_Type( cid, string, &err ) ;
    printf ( " %s, ", string ) ;
    
    ADF_Get_Number_of_Dimensions ( cid, &mDim, &err ) ;

    if ( err == 52 )
      printf ( " dangling link\n" ) ;
    else {
      ADF_Get_Dimension_Values ( cid, dims, &err ) ;
      
      for ( nDim = 0 ; nDim < MIN( 4, mDim ) ; nDim++ )
        printf ( "[%d]", dims[nDim] ) ;
      
      printf ( "\n" ) ;
    }
  }
  
  return ;
}



/******************************************************************************

  adf_create:
  Wrappers to a node in adf.
  
  Last update:
  ------------
  4Apr00; intro isNew.
  : conceived.
  
  Input:
  ------
  pid: the parent id
  label: the adf label

  Changes To:
  -----------
  pIsNew: 0 if old, 1 if the node had to be created.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double adf_create ( const double pid, const char label[], int *pIsNew ) {

  double id ;
  int err ;
  
  ADF_Get_Node_ID ( pid, label, &id, &err ) ;

  if ( err == 29 ) {
    /* node named tag does not exist. Make it. */
    ADF_Create ( pid, label, &id, &err ) ;
    *pIsNew = 1 ;
  }
  else
    *pIsNew = 0 ;

  if ( err != -1 ) {
    printf ( " FATAL: ADF error %d in adf_create.\n", err ) ;
    return ( 0. ) ; }
  else 
    return ( id ) ;
}

double adf_write1 ( const double pid, const char label[], const char dataType[],
                    const int mCols, const char *pData, const int overWrite,
                    int *pWritten ) {

  double id ;
  int err = 0, isNew, mDim, nDim[2] ;

  if ( mCols == 0 ) {
    /* Nothing to write. */
    *pWritten = 0 ;
    return ( 0. ) ;
  }

  id = adf_create ( pid, label, &isNew ) ;

  if ( !isNew && !overWrite ) {
    /* Old node. Is the datasize the same? */
    ADF_Get_Number_of_Dimensions ( id, &mDim, &err ) ;
    ADF_Get_Dimension_Values ( id, nDim, &err ) ;

    if ( ( mDim == 1 && nDim[0] == mCols ) ||
         ( mDim == 2 && nDim[0] == 1 && nDim[1] == mCols ) ) {
      /* Datasize matches. Don't write. */
      *pWritten = 0 ;
      return ( id ) ;
    }
    else if ( verbosity > 3 )
      printf ( "    WARNING: overwriting node %s, expected %d, found %d data.\n",
               label, nDim[0], mCols ) ;
  }


    
  if ( id )
    ADF_Put_Dimension_Information ( id, dataType, 1, &mCols, &err ) ;
  if ( id && err == -1 )
    ADF_Write_All_Data( id, pData, &err ) ;


#ifdef ADF_DEBUG_WRITE
  {
    int n ;

    if ( adf_debug_write ) {
      printf ( " adf_write1 to %s, %d of %s:\n    ", label, mCols, dataType ) ;
      if ( !strncmp( dataType, "I4", 2 ) )
        for ( n = 0 ; n < mCols ; n++ )
          printf ( " %d", (( int * ) pData)[n] ) ;
      else if ( !strncmp( dataType, "R8", 2 ) )
        for ( n = 0 ; n < mCols ; n++ )
          printf ( " %g", (( double * ) pData)[n] ) ;
      else if ( !strncmp( dataType, "C1", 2 ) )
        for ( n = 0 ; n < mCols ; n++ )
          printf ( "%c", pData[n] ) ;
      printf ( "\n" ) ;
    }
  }
#endif

  if ( err != -1 ) {
    printf ( " FATAL: ADF error %d in adf_write1.\n", err ) ;
    return ( 0. ) ; }
  else {
    *pWritten = 1 ;
    return ( id ) ;
  }
}

double adf_write2 ( const double pid, const char label[], const char dataType[],
                    const int mCols, const int mRows, const char *pData,
                    const int overWrite, int *pWritten ) {

  double id ;
  int err = 0, arrDim[2], isNew, mDim, nDim[2] ;
  arrDim[0] = mCols ;
  arrDim[1] = mRows ;

  if ( mRows == 0 || mCols == 0 )
    /* Nothing to write. */
    return ( 0. ) ;

#ifdef ADF_DEBUG_WRITE
  /* Check all integer pointers. */
  if ( !strncmp( dataType, "I4", 2 ) ) {
    int n,r, *pD = ( int * ) pData ;
    for ( r = 0 ; r < mRows ; r++ )
      for ( n = 0 ; n < mCols ; n++ )
        if ( pD[r*mCols+n] <= 0 )
          printf ( " Fatal, found pointer to %d in %d, %d of %s\n",
                   pD[r*mCols+n], r,n, label ) ;
  }
#endif

  id = adf_create ( pid, label, &isNew ) ;



  if ( !isNew && !overWrite ) {
    /* Old node. Is the datasize the same? */
    ADF_Get_Number_of_Dimensions ( id, &mDim, &err ) ;
    ADF_Get_Dimension_Values ( id, nDim, &err ) ;

    if ( mDim != 2 ) {
      printf ( "    WARNING: overwriting node %s, expected %d dims, found %d.\n",
               label, 2, mDim ) ;
    }
    else if ( nDim[1] == mRows && nDim[0] == mCols ) {
      /* Datasize matches. Don't write. */
      *pWritten = 0 ;
      return ( id ) ;
    }
    else if ( !strncmp( dataType, "R8", 2 ) &&
              nDim[1] == mRows && nDim[0] == 3 && mCols == 2 ) {
      /* 2D-3D difference between hydgrd and hip. Don't write. */
      *pWritten = 0 ;
      return ( id ) ;
    }
    else if ( verbosity > 3 )
      printf ( "    WARNING: overwriting node %s, expected %d*%d, found %d*%d data.\n",
               label, nDim[0], nDim[1], mRows, mCols ) ;
  }


  
  if ( id )
    ADF_Put_Dimension_Information ( id, dataType, 2, arrDim, &err ) ;
  if ( id && err == -1 )
    ADF_Write_All_Data ( id, pData, &err ) ;

#ifdef ADF_DEBUG_WRITE
  {
    int n,r ;

    if ( adf_debug_write ) {
      printf ( " adf_write2 to %s, %d*%d of %s:\n", label, mRows, mCols, dataType ) ;
      if ( !strncmp( dataType, "I4", 2 ) )
        for ( r = 0 ; r < mRows ; r++ ) {
          printf ( "   " ) ;
          for ( n = 0 ; n < mCols ; n++ )
            printf ( " %d", (( int * ) pData)[r*mCols+n] ) ;
          printf ( "\n" ) ;
        }
      else if ( !strncmp( dataType, "R8", 2 ) )
        for ( r = 0 ; r < mRows ; r++ ) {
          printf ( "   " ) ;
          for ( n = 0 ; n < mCols ; n++ )
            printf ( " %g", (( double * ) pData)[r*mCols+n] ) ;
          printf ( "\n" ) ;
        }
      else if ( !strncmp( dataType, "C1", 2 ) )
        for ( r = 0 ; r < mRows ; r++ ) {
          printf ( "   " ) ;
          for ( n = 0 ; n < mCols ; n++ )
            printf ( "%c", pData[r*mCols+n] ) ;
          printf ( "\n" ) ;
        }
    }
  }
#endif
  
  if ( err != -1 ) {
    printf ( " FATAL: ADF error %d in adf_write2.\n", err ) ;
    return ( 0. ) ; }
  else {
    *pWritten = 1 ;
    return ( id ) ;
  }
}

