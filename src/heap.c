/*
   heap.c:
   Heap list. Generic. Put the smallest value at the top.

   Last update:
   ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
   7Jan05; move heap_cmp comparison routines to cmp_ in uns_meth.
   12Sep2; intro realloc =[0,1], intro empty_heap.


   This file contains:
   -------------------

*/

extern const int verbosity ;

#include "cpre.h"
#include "proto.h"

const int minSize = 100 * sizeof( int ) ; 

struct _heap_s {
  int mData ;    /* How large is the heap currently. Note that we will be using
		    <= mData for data. Note that 0 is unused, since we work with 
		    integer divide from child to parent, the 0 slot is used for
		    exchange. */
  int realloc ;  /* If set to zero, no realloc occurs, smallest items fall off
                    the edge. */
  ulong_t dataSize ; /* How many ints is one data item. */
  char *pData ;  /* For simplicity, address the data as chars. */

  int nLast ;    /* Where is the end of the current heap. */

  int (*cmpFun) ( const void *pData0, const void *pData1 ) ;
  /* A pointer to a comparison function, returning <0 if 0<1, 0 if 0=1, >0 if 0>1. */
} ;

#define P_DATA(n) ( pHeap->pData + pHeap->dataSize*n )

/******************************************************************************

  make_heap:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  mData:      initial allocation size.
  sizeofData: size of each data item in bytes. 
  cmpFun:     A Comparison function, returning <0 if 0<1, 0 if 0=1, >0 if 0>1.
  
  Returns:
  --------
  NULL on failure, a pointer to the heap root on success.
  
*/

heap_s *make_heap ( int mData, int realloc, const ulong_t dataSize, arrFam_s *pFam,
		    int (* cmpFun) ( const void *pData0, const void *pData1 ) ) {
  
  heap_s *pHeap ;
  void *pData ;
  mData  = ( mData > 0 ? mData : minSize ) ;

  if ( !( pHeap = arr_malloc ( "pHeap in make_heap", pFam,  1, sizeof ( *pHeap ) ) ) ||
       !( pData = arr_malloc ( "pData in make_heap", pFam,  mData+1, dataSize ) ) ) {
    printf ( " FATAL: failed to alloc heap in make_heap.\n" ) ;
    return ( NULL ) ; }

  pHeap->mData = mData ;
  pHeap->realloc = ( realloc ? 1 : 0 ) ; 
  pHeap->dataSize = dataSize ;
  pHeap->pData = pData ;
  pHeap->nLast = 0 ;
  pHeap->cmpFun = cmpFun ;
  
  return ( pHeap ) ;
}

/******************************************************************************

  free_heap:
  Remove a haap.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppHeap: a heap root.

  Changes to:
  -----------
  ppHeap:
  
*/

void free_heap ( heap_s **ppHeap ) {
  
  if ( !*ppHeap )
    return ;

  arr_free ( (*ppHeap)->pData ) ;
  arr_free ( *ppHeap ) ;
  *ppHeap = NULL ;

  return ;
}
/******************************************************************************

  empty_heap:
  Reset the counters.
  
  Last update:
  ------------
  13Sep2: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int empty_heap ( heap_s *pHeap ) {

  if ( !pHeap )
    /* No such thing. */
    return ( 0 ) ;

  pHeap->nLast = 0 ;
  return ( 1 ) ;
}



/******************************************************************************

  add_heap:
  Add an entry to the heap. Realloc if necessary.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pHeap:     a pointer to the root of the heap.
  pUserData: a pointer to the data to be stored and compared.

  Changes To:
  -----------
  pHeap:     reallocation if needed.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_heap ( heap_s *pHeap, const void *pUserData ) {
  
  static int nNew, nPrnt ;
  static void *pSwitch ;
  static ulong_t dataSize ;

  dataSize = pHeap->dataSize ;

  if ( pHeap->mData <= pHeap->nLast ) {
    /* Heap exhausted. */
    if ( pHeap->realloc ) {
      /* realloc. */
      pHeap->mData = REALLOC_FACTOR*pHeap->mData + 1 ;
      pHeap->pData =
        arr_realloc ( "pHeap->pData in add_heap", NULL, pHeap->pData,
                      pHeap->mData+1, pHeap->dataSize ) ;
    }
    else if (  pHeap->cmpFun ( pUserData, P_DATA( pHeap->mData ) ) < 0 )
      /* new value is smaller, drop off the last item. */
      pHeap->nLast-- ;
    else
      /* new value is larger, disregard. */
      return ( 1 ) ;
  }

  /* Intro the new data at the tail. */
  pSwitch = pHeap->pData ;
  nNew = ++pHeap->nLast ;
  nPrnt = nNew/2 ;
  memcpy ( P_DATA( nNew ), pUserData, dataSize ) ;

  while ( nNew > 1 && pHeap->cmpFun ( P_DATA( nNew ), P_DATA( nPrnt ) ) < 0 ) {
    /* Switch nNew up. */
    memcpy ( pSwitch,         P_DATA( nPrnt ), dataSize ) ;
    memcpy ( P_DATA( nPrnt ), P_DATA( nNew  ), dataSize ) ;
    memcpy ( P_DATA( nNew  ), pSwitch,         dataSize ) ;
    nNew = nPrnt ;
    nPrnt = nNew/2 ;
  }

  return ( 1 ) ;
}

/******************************************************************************

  query_heap:
  Query the top value.
  
  Last update:
  ------------
  13Sep2; negate the empty condition. What a bug!
  : conceived.
  
  Input:
  ------
  pHeap:     A pointer to the heap root.
  pUserData: A pointer to a data location.


  Changes To:
  -----------
  pUserData: will contain the top data.
  
  Returns:
  --------
  0 if the heap is empty, 1 otherwise.
  
*/

int query_heap ( const heap_s *pHeap, void *pUserData ) {
  
  if ( !pHeap->nLast )
    /* Nothing there. */
    return ( 0 ) ;

  memcpy ( pUserData, P_DATA( 1 ), pHeap->dataSize ) ;
  
  return ( 1 ) ;
}


/******************************************************************************

  get_heap:
  Remove the top item from the heap.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pHeap:     A pointer to the heap root.
  pUserData: A pointer to a data location.


  Changes To:
  -----------
  pHeap:     may be reallocated if the used size has become too small.
  pUserData: will contain the top data.
  
  Returns:
  --------
  0 if the heap is empty or allocation fails, 1 otherwise.
  
*/
int get_heap ( heap_s *pHeap, void *pUserData, const int realloc ) {

  static int nLast, nNew, nCh0, nCh1 ;
  static void *pSwitch ;
  static ulong_t dataSize ;

  dataSize = pHeap->dataSize ;
  nLast = pHeap->nLast ;

  if ( !nLast )
    /* Nothing there. */
    return ( 0 ) ;

  /* Return the top item. */
  memcpy ( pUserData,   P_DATA( 1 ),              dataSize ) ;
  /* Copy the bottom item up. */
  memcpy ( P_DATA( 1 ), P_DATA( pHeap->nLast-- ), dataSize ) ;
  
  /* Intro the new data at the top. */
  pSwitch = pHeap->pData ;
  nNew = 1 ;
  nCh0 = 2*nNew ;
  nCh1 = nCh0 + 1 ;

  while ( ( nCh0 <= nLast && pHeap->cmpFun ( P_DATA( nNew ), P_DATA( nCh0 ) ) > 0 ) ||
	  ( nCh1 <= nLast && pHeap->cmpFun ( P_DATA( nNew ), P_DATA( nCh1 ) ) > 0 ) ) {
    /* Larger than some child. Switch nNew down. */
    if ( nCh1 > nLast || pHeap->cmpFun ( P_DATA( nCh0 ), P_DATA( nCh1 ) ) < 0 ) {
      /* nCh0 is smaller or the only of the two present, switch it up. */
      memcpy ( pSwitch,        P_DATA( nCh0 ), dataSize ) ;
      memcpy ( P_DATA( nCh0 ), P_DATA( nNew ), dataSize ) ;
      memcpy ( P_DATA( nNew ), pSwitch,         dataSize ) ;
      nNew = nCh0 ;
    }
    else {
      /* nCh1 is smaller, switch it up. */
      memcpy ( pSwitch,        P_DATA( nCh1 ), dataSize ) ;
      memcpy ( P_DATA( nCh1 ), P_DATA( nNew ), dataSize ) ;
      memcpy ( P_DATA( nNew ), pSwitch,         dataSize ) ;
      nNew = nCh1 ;
    }

    nCh0 = 2*nNew ;
    nCh1 = nCh0 + 1 ;
  }

  if ( realloc && ( pHeap->mData-1 )/REALLOC_FACTOR < MAX( pHeap->nLast, minSize ) ) {
    /* Heap too large, realloc. */
    pHeap->mData = pHeap->nLast + 1 ;
    pHeap->pData =
      arr_realloc ( "pHeap->pData in get_heap", NULL, pHeap->pData,
                    pHeap->mData+2, pHeap->dataSize ) ;
    if ( !pHeap->pData ) {
      printf ( " FAILED to reallocate heap list in add_heap.\n" ) ;
      return ( 0 ) ; }
  }
  return ( 1 ) ;
}

