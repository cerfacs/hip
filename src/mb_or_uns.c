/*
  mb_or_uns.c:
  Since we don't have polymorphism, yet, these functions select between
  mb and uns.

  Last update:
  ------------
  10Oct96: cut out of meth.c.

  Contains:
  ---------
  write_av:
  write_pts:
  write_cut:
  write_screen:
  read_dpl:
  write_dpl:
  check_grid:
  interpolate:
  split_quads:
*/

#include <string.h>
#include <ctype.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_mb.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_mb.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern Grids_struct Grids ;


/******************************************************************************

  write_av:
  Write a grid in avbp format.
  
  Last update:
  ------------
  28Jul05; intro level
  20Dec01; add switch for 4.6/4.7 style .asciiBounds.
  22Jul96: conceived.
  
  Input:
  ------
  rootFile: root part of the family of files.
  keyword:  what version?
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_av ( char *pRootFile, const char keyword[], int level ) {

  int n ;
  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  
  if ( Grids.PcurrentGrid->mb.type == mb ) {
    sprintf ( hip_msg, "cannot write multi block grids to avbp." ) ;
      hip_err ( warning, 1, hip_msg ) ;
    return ( 0 ) ;
  }

  for ( n = 0 ; n < level ; n++ ) {
    pUns = pUns->pUnsCoarse ;
    if ( !pUns ) {
      sprintf ( hip_msg, "coarse grid level %d does not exist.", level ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 0 ) ;
    }
  }



  
  prepend_path ( pRootFile ) ;
  if ( verbosity > 0 ) {
    if ( level == 0 ) {
      sprintf ( hip_msg, "   Writing finest grid" ) ;
      hip_err ( blank, 1, hip_msg ) ;
    }
    else {
      sprintf ( hip_msg, "   Writing grid level %d", level ) ;
      hip_err ( blank, 1, hip_msg ) ;
    }
  }

  return ( write_avbp ( pUns, pRootFile, keyword ) ) ;

}

/******************************************************************************

  write_pts:
  Write the cut of a current grid to pts. This works in 2-D only.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  PptsFile

  Changes To:
  -----------
  PptsFile
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_pts ( char *PptsFile, const char *Pkeyword ) {
  
  FILE *FptsOut ;
  int returnVal = 1, writeKeepNotCut ;

  if ( Grids.PcurrentGrid->uns.type == uns ) {
    sprintf ( hip_msg, "cannot write unstructured cut to pts." ) ;
    hip_err ( warning, 1, hip_msg ) ;
    return ( 0 ) ; 
  }
  else if ( Grids.PcurrentGrid->uns.mDim != 2 ) {
    sprintf (  hip_msg, "write_pts cannot deal with %d-dimensional grid.",
               Grids.PcurrentGrid->uns.mDim ) ;
    hip_err ( warning, 1, hip_msg ) ;
    return ( 0 ) ; 
  }
  else if ( !( FptsOut = fopen ( prepend_path ( PptsFile ), "w" ) ) ) {
    sprintf (  hip_msg, "file named %s could not be opened.", PptsFile ) ;
    hip_err ( warning, 1, hip_msg ) ;
    return ( 0 ) ; 
  }
  
  else if ( Grids.PcurrentGrid->mb.type == mb ) {
    writeKeepNotCut = ( strncmp ( Pkeyword, "cu", 2 ) ? 1 : 0 ) ;

    if ( !( returnVal = write_mb_pts ( Grids.PcurrentGrid->mb.Pmb, FptsOut,
				       writeKeepNotCut ) ) ) {
      sprintf ( hip_msg, "could not write structured cut to pts." ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  
  fclose ( FptsOut ) ;
  return ( returnVal ) ;
}



/******************************************************************************

  write_cut:
  Write a cut of a grid.
  
  Last update:
  ------------
  22Jul96: conceived.
  
  Input:
  ------
  rootFile: root part of the family of files.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_cut ( char *ProotFile, const char *Pkeyword ){

  char fileName[TEXT_LEN] ;
    
  if ( Grids.PcurrentGrid->uns.type == uns )
    return ( write_uns_cut ( Grids.PcurrentGrid->uns.pUns, ProotFile ) ) ;

  else if ( Grids.PcurrentGrid->mb.type == mb ) {
    sprintf ( fileName, "%s\n", ProotFile ) ;
    return ( write_pts ( fileName, Pkeyword ) ) ;
  }
  else
    return ( 0 ) ;
}


/******************************************************************************

  write_screen:
  Write a grid to the screen.
  
  Last update:
  ------------
  22Jul96: conceived.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_screen ( ) {

  if ( Grids.PcurrentGrid->uns.type == uns )
    return ( write_uns_ascii ( Grids.PcurrentGrid->uns.pUns,
			       Grids.PcurrentGrid->uns.mDim ) ) ;
  
  else {
    hip_err ( warning, 1, "can\'t dump this type of grid to the screen." ) ;
    return ( 0 ) ;
  }
}

/******************************************************************
 
 read_dpl:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from a generic .dpl file for cpre.

 Last update:
 12Jul96: derived from readsolmg.

 Input:
 ------
 Pchunk:
 partNumber: numeric identifier for this grid.

 Changes to:
 -----------


 Returns:
 --------
 
*/

int read_dpl( char *PdplFile, char *PsolFile ) {
  
  char fileHeader[6], *pfg ;
  FILE *FdplIn=NULL, *FsolIn=NULL ;
  int kPos, returnVal ;

  prepend_path ( PdplFile ) ;
  if ( !( FdplIn = fopen ( PdplFile, "r" ) ) ) {
    sprintf ( hip_msg, "file named:%s not found.", PdplFile ) ;
    hip_err ( warning, 1, hip_msg ) ;
    return ( 0 ) ; 
  }
  pfg = fgets( fileHeader, 6, FdplIn ) ;
  for ( kPos = 0 ; kPos < 5 ; kPos++ )
    fileHeader[kPos] = tolower( fileHeader[kPos] ) ;


  if ( PsolFile[0] != '\0' ) {
    prepend_path ( PsolFile ) ;
    if ( !( FsolIn = fopen ( PsolFile, "r" ) ) ) {
      sprintf ( hip_msg, "file named:%s not found.", PsolFile ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 0 ) ; 
    }
  }
    
  

  if ( !( strncmp ( fileHeader, "unstr", 5 ) ) ) {
    if ( !( returnVal = read_uns_dpl ( FdplIn ) ) ) {
      sprintf (  hip_msg, "error reading unstructured .dpl file." ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  else if ( !( strncmp ( fileHeader, "uns3d", 5 ) ) ) {
    if ( !( returnVal = read_uns_dpl3d ( FdplIn, FsolIn ) ) ) {
      sprintf ( hip_msg, "error reading unstructured .dpl file." ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  else if ( !( strncmp ( fileHeader, "str", 3 ) ) ) {
    if ( !( returnVal = read_str_dpl ( FdplIn ) ) ) {
      sprintf (  hip_msg, "error reading unstructured .dpl file." ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  else {
    sprintf ( hip_msg, "unrecognized file format %s in read_dpl.",
              fileHeader ) ;
    hip_err ( warning, 1, hip_msg ) ;
    returnVal = 0 ; 
  }
  fclose ( FdplIn ) ;
  return ( returnVal ) ;

}  

/******************************************************************************

  write_dpl:
  Write a grid in dpl format.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  27Jul96: derived from write_avbp.
  
  Input:
  ------
  rootFile: filename.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_dpl ( char *ProotFile, const char keyword[] ) {

  if ( Grids.PcurrentGrid->uns.type == uns )
    if ( !strncmp ( keyword, "dplad", 5 ) ) {
#ifdef ADAPT_HIERARCHIC
	return ( write_uns_dpl_adapt ( Grids.PcurrentGrid->uns.pUns, ProotFile ) ) ;
#else
        hip_err ( warning, 0, 
                  "adaptation not compiled in this version, specify -DADAPT_HIERARCHIC\n" ) ;
        return ( 0 ) ;
#endif
    }
    else 
	return ( write_uns_dpl ( Grids.PcurrentGrid->uns.pUns, ProotFile ) ) ;
  else if ( Grids.PcurrentGrid->mb.type == mb ) {
    hip_err ( warning, 0, "writing of structured files to dpl is not yet implemented.\n" ) ;
    return ( 0 ) ; }
  else
    return ( 0 ) ;
}


/******************************************************************************

  check_grid:
  Check the connectivity of a grid.
  
  Last update:
  ------------
  1Jul16; new interface to check_uns.
  : conceived.

  Input:
  ------
  Pgrid:
  
  Returns:
  --------
  0 on invalid, 1 on valid grid.
  
*/

int check_grid ( grid_struct *Pgrid ) {
  
  if ( !Pgrid ) {
    hip_err ( warning, 0, "there is no grid to check." ) ;
    return ( 0 ) ; }
  if ( Pgrid->mb.type == mb ) {
    hip_err ( warning, 0,
              "checking of multiblocked grids is not implemented, yet." ) ;
    return ( 0 ) ; }
  else if ( Pgrid->uns.type == uns ) {
    if ( !check_uns ( Pgrid->uns.pUns, check_lvl ) ) {
      sprintf ( hip_msg, "Grid %d is invalid.", Pgrid->uns.nr ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 0 ) ; }
    else
      return ( 1 ) ;
  }
  return ( 0 ) ;
}

/******************************************************************************

  interpolate:
  Interpolate the solution between two unstructured grids.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int interpolate ( int gridNr, char charAxi ) {

  grid_struct *pGridFrom ;
  uns_s *pUnsFrom, *pUnsTo ;

  if ( Grids.PcurrentGrid->uns.type != uns ) {
    printf ( " FATAL: grid to interpolate to must be unstructured.\n" ) ;
    return ( 0 ) ;
  }
  pUnsTo = Grids.PcurrentGrid->uns.pUns ;
  
  
  for ( pGridFrom = Grids.PfirstGrid ; pGridFrom ; 
        pGridFrom = pGridFrom->uns.PnxtGrid )
    if ( pGridFrom->uns.nr == gridNr )
      break ;
  if ( !pGridFrom ) {
    sprintf ( hip_msg, "no grid %d found.", gridNr ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }

  if ( pGridFrom->uns.type != uns ) {
    hip_err ( warning, 1, "grid to interpolate from must be unstructured.\n" ) ;
    return ( 0 ) ;
  }
  pUnsFrom = pGridFrom->uns.pUns ;


  return ( uns_interpolate ( pUnsFrom, pUnsTo, charAxi ) ) ;
}

/******************************************************************************

  split_quads:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int split_quads ( splitType_e splitType ) {

  uns_s *pUns ;

  if ( Grids.PcurrentGrid->uns.type != uns ||
       Grids.PcurrentGrid->uns.pUns->mDim != 2 ) {
    hip_err ( warning, 1, "grid to split to must be 2D unstructured.\n" ) ;
    return ( 0 ) ;
  }
  pUns = Grids.PcurrentGrid->uns.pUns ;
  

  return ( simplify_all_elems ( pUns, 1, splitType ) ) ;
}

