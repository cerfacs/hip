/*
   write_uns_adf.c:
   Unstructured grid to an adf database.

   Last update:
   ------------
   15May06; drop all refs to doHydra
   12Jul04; intro node-->mg_verts
   10Jan01; write enough output for hydgrd in non-hydra mode.
   22Sep00; extract adf wrappers into separate file.
   18Apr99; intro chgSet_s.
   29Oct99; change the solution file to a horizontal structure, 'flow' keyword.


   This file contains:
   -------------------

*/
#include <strings.h>
#include <time.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"
#include "ADF.h"

/**/
#define ADF_DEBUG_WRITE
int adf_debug_write = 0 ;

#define CV const vrtx_struct

extern const char version[], lastUpdate[], varTypeNames[][LEN_VARTYPENAME] ;

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const Grids_struct Grids ;

extern const int perBc_in_exBound ;
extern const double normAnglCut ;
extern const int symmCoor ;

extern const int mVarC ;
extern const char varCat[8][8] ;

typedef struct {
  int all ;
  int verts ;
  int edges ;
  int perPairs ;
  int bndVx ;
  int bndFc ;
  int elems ;
  int parents ;
} chgSets_s ;


static int write_adf_header ( uns_s *pUns, const double root_id,
                              chgSets_s *pChgSets, const int overWrite );
static int write_adf_hdr_avbp ( uns_s *pUns, const double root_id );

static int write_adf_level ( uns_s *pUns, const double lvl_id, const int nLevel,
                             chgSets_s *pChgSets );
static int write_adf_coor  ( uns_s *pUns, const double lvl_id, 
                             int *pReturnValue, chgSets_s *pChgSets );
static int write_adf_conn  ( uns_s *pUns, const double lvl_id,
                             int mElemsNumbered, 
                             int *pFoundDrv, int *pReturnValue,
                             chgSets_s *pChgSets );
static int write_adf_bnd ( uns_s *pUns, const double lvl_id,
                           int writeNormals, int *pReturnValue,
                           chgSets_s *pChgSets, bndVxWt_s *pBWt, int *pmAllBndVx );
static int write_adf_per   ( uns_s *pUns, const double lvl_id, int *pReturnValue,
                             chgSets_s *pChgSets );
static int write_adf_edges ( uns_s *pUns, const double lvl_id,
                             bndVxWt_s *pBWt, int mAllVxBnd,
                             int *pReturnValue, chgSets_s *pChgSets );

static int write_adf_prt ( uns_s *pUns, const double grid_id, int *pReturnValue,
                           chgSets_s *pChgSets );

static int write_adf_sol ( uns_s *pUns, const double lvl_id );
static int write_adf_sol_hydra ( uns_s *pUns, const double lvl_id,
                                 chgSets_s *pChgSets );

static char* mikeg_encode ( const char groupName[], const char groupType );


/******************************************************************************

  write_uns_adf:
  Write all levels of a grid to adf.
  
  Last update:
  ------------
  16Dec98: derived from write_oxd2.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_uns_adf ( uns_s *pUns, char *pRootName, int overWrite ) {

  const char *pVt ;
  
  uns_s *pUnsLvl ;
  char solFile[TEXT_LEN], gridFile[TEXT_LEN], string[LINE_LEN] ;
  int nLevel, err, returnValue = 1, isNew ;
  double root_id, sol_id, lvl_id, grid_id, id ;

  /* Reset the changed sets. */
  chgSets_s chgSets = { 0, 0,0,0,0,0,0,0} ;
  chgSets.all = overWrite ;


  if ( !pUns->validGrid ) {
    printf ( " FATAL: you were told that this grid is invalid, weren't you?.\n" ) ;
    return ( 0 ) ; }
  else if ( !check_bnd_setup ( pUns ) ) {
    printf ( " FATAL: cannot write a grid without proper boundary setup.\n" ) ;
    return ( 0 ) ; }

  /* Number elements on all levels. */ 
  for ( pUnsLvl = pUns, nLevel = 0 ; pUnsLvl ;
        pUnsLvl = pUnsLvl->pUnsCoarse, nLevel++ )
    number_uns_elemFromVerts ( pUnsLvl, leaf ) ;



    /* Write out the mesh for all grid levels, one file per level. */
  for ( pUnsLvl = pUns, nLevel = 0 ; pUnsLvl ;
        pUnsLvl = pUnsLvl->pUnsCoarse, nLevel++ ) {

    if ( pUnsLvl->pUnsCoarse || nLevel )
      /* Append a number. */
      sprintf ( gridFile, "%s.grid.%d.adf", pRootName, nLevel+1 ) ;
    else
      sprintf ( gridFile, "%s.grid.adf", pRootName ) ;
    prepend_path ( gridFile ) ;

    if ( overWrite ) {
      sprintf ( string, "/bin/rm -f %s", gridFile ) ;
      system ( string ) ;
    }
      
    ADF_Database_Open( gridFile, "OLD", "IEEE_BIG", &lvl_id, &err ) ;
    if ( err != -1 ) {
      /* File doesn't exist. Write everything. */
      overWrite = 1 ;
      ADF_Database_Open( gridFile, "UNKNOWN", "IEEE_BIG", &lvl_id, &err ) ;
      if ( err != -1 ) {
        printf ( " FATAL: file: %s could not be opened: error %d.\n", gridFile, err ) ;
        return ( 0 ) ; }
    }
    
    if ( verbosity > 0 )
      printf ( "   Writing level %d in adfh to: %s.\n", nLevel, gridFile ) ;
    
    /* Global variables. */
    write_adf_header ( pUns, lvl_id, &chgSets, overWrite ) ;

    if ( !write_adf_level ( pUnsLvl, lvl_id, nLevel, &chgSets ) ) {
      printf ( " FATAL: could not write level %d in write_uns_adf.\n", nLevel ) ;
      return ( 0 ) ; }


    if ( nLevel == 0 && pUns->adapted )
      /* Write the parent connectivity with the finest level.  For the
         parent to child pointers, we need both parents and children
         numbered independently. This is done locally in write_adf_prt. */
      write_adf_prt ( pUns, lvl_id, &returnValue, &chgSets ) ;

  
    ADF_Database_Close ( lvl_id, &err ) ;
  }




  
  /* Is there a solution? */
  if ( pUns->varTypeS.varType != noVar ) {


    strcpy ( solFile, pRootName ) ;
    prepend_path ( solFile ) ;
    strcat ( solFile, ".sol.adf" ) ;


    if ( overWrite ) {
      sprintf ( string, "/bin/rm -f %s", solFile ) ;
      system ( string ) ;
    }
      

    ADF_Database_Open( solFile, "UNKNOWN", "IEEE_BIG", &root_id, &err ) ;
    if ( err != -1 ) {
      printf ( " FATAL: file: %s could not be opened: error %d.\n", solFile, err ) ;
      return ( 0 ) ; }


    /* Create a link to the grid. Do put an absolute path-name.
    sprintf ( gridFile, "%s.grid.adf", pRootName ) ;
    prepend_path ( gridFile ) ;
    ADF_Link( root_id, "grid", gridFile, "/grid", &id, &err ) ; */

    /* Variable type.
       Not useful any longer.
    pVt = varTypeNames[0] + LEN_VARTYPENAME*pUns->varTypeS.varType ;
    adf_write1 ( root_id, "variable_type", "C1", sizeof( pVt ), pVt,
                 chgSets.verts, &isNew ) ; */

    write_adf_sol ( pUns, root_id ) ;

      

    ADF_Database_Close ( root_id, &err ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_header ( uns_s *pUns, const double root_id,
                              chgSets_s *pChgSets, const int overWrite ) {

  const uns_s *pUnsLvl ;
  char *pSurfLbl, *psl, string[LINE_LEN] ;
  double hip_id, log_id ;
  int err, nBc, mMgLevels, isNew ;
  time_t tp ;
  bc_struct *pBc ;
            
  /* Hip processing log. */
  log_id = adf_create ( root_id, "logfile", &isNew ) ;
  hip_id = adf_create ( log_id,  "hip", &isNew ) ;
    
  /* version. */
  sprintf ( string, "hip_version: %s of %s.", version, lastUpdate ) ;
  adf_write1 ( hip_id, "version", "C1", strlen( string ), string, 1, &isNew ) ;
  /* Get the current date and time from the system */
  tp = time( NULL );
  strftime( string, 32, "%a %b %d %H:%M:%S %Y", localtime(&tp));
  adf_write1 ( hip_id, "written_on", "C1", strlen( string ), string, 1, &isNew ) ;
    
    
  
  /* Spatial dimensions. */
  adf_write1 ( root_id, "dimension", "I4", 1, ( char * ) &pUns->mDim, pChgSets->all,
               &isNew ) ;
  if ( isNew && !overWrite ) {
    /* The dimension changes everything. */
    printf ( " WARNING: found different dimensions in adf. Overwriting everything.\n" );
    pChgSets->all = 1 ;
  }

  /* Periodic angle. */  
  if ( pUns->rotAngleRad != 0. || pUns->specialTopo == annCasc )
    adf_write1 ( root_id, "periodic_angle", "R8", 1, ( char * ) &pUns->rotAngleRad,
                 pChgSets->all, &isNew ) ;
    
    
  /* Surface groups. */
  if ( !( psl = pSurfLbl = arr_malloc ( "pSurfLbl in write_uns_adf", pUns->pFam,
                                        pUns->mBc, ADF_SURFLBL_WRITE_LEN ) ) ) {
    printf ( " FATAL: could not allocate surface labels in write_uns_adf.\n" ) ;
    return ( 0 ) ; }
    
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBc = pUns->ppBc[nBc] ;
    strncpy ( psl, mikeg_encode ( pBc->text, pBc->type ), ADF_SURFLBL_LEN ) ;
    psl += ADF_SURFLBL_WRITE_LEN ;
  }

  adf_write2 ( root_id, "surface_groups", "C1",
               ADF_SURFLBL_WRITE_LEN, pUns->mBc, pSurfLbl, pChgSets->all, &isNew ) ;
  arr_free ( pSurfLbl ) ;

  return ( 1 ) ;
}

/******************************************************************************

  write_adf_hdr_avbp:
  Specific headers for avbp, mostly timestep records.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_hdr_avbp ( uns_s *pUns, const double root_id ) {

  double id, zero=0.0 ;
  int isNew ;
  
  if ( pUns->restart.avbp.initialised ) {
    adf_write1 ( root_id, "niterd", "R8", 1, ( char * ) &pUns->restart.avbp.itno,
                 1, &isNew ) ;
    adf_write1 ( root_id, "dtsum", "R8", 1, ( char * ) &pUns->restart.avbp.dtsum,
                 1, &isNew ) ;
    adf_write1 ( root_id, "dt_av", "R8", 1, ( char * ) &pUns->restart.avbp.dt_av,
                 1, &isNew ) ;
  }
  else {
    adf_write1 ( root_id, "niterd", "R8", 1, ( char * ) &zero,
                 1, &isNew ) ;
    adf_write1 ( root_id, "dtsum", "R8", 1, ( char * ) &zero,
                 1, &isNew ) ;
    adf_write1 ( root_id, "dt_av", "R8", 1, ( char * ) &zero,
                 1, &isNew ) ;
  }
  return (1) ;
}

/******************************************************************************

  write_adf_level:
  Write one level to the database under node lvl_id.
  
  Last update:
  ------------
  8Nov99; don't write periodic boundary nodes for hydra.
  16Dec98: derived from avbp4.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_level ( uns_s *pUns, const double lvl_id, const int nLevel,
                             chgSets_s *pChgSets ) {

  const int doWriteNormals = 1 ;
  int returnValue = 1, foundDrv = 0, nBc, mAllBndVx ;
  bndVxWt_s *pBWt ;

  if ( verbosity > 2 )
    printf ( "   level %d to adf.\n", nLevel ) ;


  
  /* Number the leafs with the number of vertices.
     MG connectivity requires coarse grid elements to have their
     final number. Numbering is now done outside the loop over levels.
  number_uns_elemFromVerts ( pUns, leaf ) ; */
  
  /* Make pairs of periodic nodes. Faces are listed non-periodically. */
  if ( !special_verts ( pUns ) ) {
    printf ( " FATAL: failed to match periodic vertices in write_adf_level.\n" ) ;
    return ( 0 ) ; }
  else if ( pUns->multPer && verbosity > 2 ) {
    printf ( "    INFO: found multiple periodicity.\n" ) ;
    return ( 0 ) ; }

  /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
     for the current numbering of elements. */
  count_uns_bndFaces ( pUns ) ;


  /* Boundary weights and normals. */
  pBWt = arr_malloc ( "pBWt in write_adf_level", pUns->pFam,
                      pUns->mBc, sizeof( bndVxWt_s ) ) ;


  
  /* Finest grid. Start numbering elements above 0. */
  write_adf_coor  ( pUns, lvl_id,  &returnValue, pChgSets ) ;
  write_adf_conn  ( pUns, lvl_id, 0,  &foundDrv, &returnValue, pChgSets ) ;
  write_adf_bnd   ( pUns, lvl_id, doWriteNormals,  &returnValue, pChgSets,
                    pBWt, &mAllBndVx ) ;
  write_adf_per   ( pUns, lvl_id, &returnValue, pChgSets ) ;

  /* edge list
  write_adf_edges ( pUns, lvl_id, pBWt, mAllBndVx, &returnValue, pChgSets ) ;
  */


  arr_free ( pBWt->pWt ) ;
  arr_free ( pBWt->pnVx ) ;
  arr_free ( pBWt->pVx ) ;
  arr_free ( pBWt ) ;


  
  if ( !returnValue ) {
    printf ( " FATAL: could not write the finest grid to .adf.\n" ) ;
    return ( 0 ) ; }
  else
    return ( 1 ) ;
}



/******************************************************************************

  write_adf_coor:
  Write the coordinates.
  
  Last update:
  ------------
  13Jul04; intro doHydra switch to list triplets in 2D.
  12Jul04; intro node-->mg_verts
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_coor ( uns_s *pUns, const double lvl_id, 
                            int *pReturnValue, chgSets_s *pChgSets ) {

  const int mDim = pUns->mDim ;
  chunk_struct *pChunk ;
  int nBeg, nEnd, isNew, *nElC, nVx, mVx, kVx, *nVxC, *pVC, *pUse, 
    mDimW = mDim ;
  elem_struct *pElC ;
  vrtx_struct *pVx, *pVxEnd, *pVxBeg, **ppVx ;
  double *pCoor, *pCo ;

  if ( !*pReturnValue )
    return ( 0 ) ;

  pCo = pCoor = arr_malloc ( "pCoor in write_adf_coor", pUns->pFam,
                             pUns->mVertsNumbered*mDimW, sizeof( double ) ) ;

  
  /* Loop over all vertices and list numbered coordinates. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ))
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        vec_copy_dbl ( pVx->Pcoor, mDim, pCo ) ;
        pCo += mDim ;
      }

  if ( pCo-pCoor != mDimW*pUns->mVertsNumbered ) {
    printf ( " FATAL: %d coordinates expected, but %d found in write_adf_coor.\n",
             mDimW*pUns->mVertsNumbered, (int)(pCo-pCoor) ) ;
    *pReturnValue = 0 ;
    return ( *pReturnValue ) ; }


  /* Coordinates. Triplets. */
  adf_write2 ( lvl_id, "node_coordinates","R8", mDimW, pUns->mVertsNumbered,
               (char*) pCoor, pChgSets->verts, &pChgSets->verts ) ;
  arr_free ( pCoor ) ;


  if ( pUns->pUnsCoarse ) {
    /* Multigrid vertex connectivity. */
    if ( pUns->mVertsNumbered != pUns->mVxCollapseTo ) {
      printf ( " FATAL: expected %d collapse pointers, found %d in write_adf_coor.\n",
               pUns->mVertsNumbered, pUns->mVxCollapseTo ) ;
      *pReturnValue = 0 ; return ( 0 ) ; }

#ifdef ADF_DEBUG_WRITE
    {
      mVx = pUns->pUnsCoarse->mVertsNumbered ;
      pUse = arr_malloc ( "write_adf_coor:pUse", pUns->pFam,
                          mVx+1, sizeof( double ) ) ;

      if ( verbosity > 2 )
        printf ( "      INFO: Checking mg pointers.\n" ) ;

      for ( nVx = 1 ; nVx <= mVx ; nVx++ )
        pUse[nVx] = 0 ;

      for ( nVx = 1 ; nVx <= pUns->mVertsNumbered; nVx++ )
        if ( pUns->pnVxCollapseTo[nVx] <= 0 || pUns->pnVxCollapseTo[nVx] > mVx )
          printf ( " FATAL: found vx %d pointing to %d\n",
                   nVx, pUns->pnVxCollapseTo[nVx] ) ;
        else
          pUse[ pUns->pnVxCollapseTo[nVx] ] = 1 ;
        
      for ( nVx = 1 ; nVx <= mVx ; nVx++ )
        if ( !pUse[nVx] )
          printf ( " FATAL: found unreferenced coarse grid vx %d\n", nVx ) ;

      arr_free ( pUse ) ;
    }
#endif

    adf_write2 ( lvl_id, "node-->mg_node", "I4", 1, pUns->mVertsNumbered,
                 (char*) ( pUns->pnVxCollapseTo+1 ), pChgSets->verts, &isNew );


    /* Multigrid connectivity: fine grid vertex to containing coarse
       grid element. */
    nElC = arr_malloc ( "write_adf_coor:nElC", pUns->pFam, pUns->mVertsNumbered+1, 
                        sizeof( int ) ) ;
    nVxC = arr_calloc ( "write_adf_coor:nVxC", pUns->pFam, 
                        MAX_VX_ELEM*(pUns->mVertsNumbered+1), sizeof( int ) ) ;



    /* Loop over all vertices and find the number of the coarse cell. */
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ))
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( ( nVx = pVx->number ) ) {
          pElC = pUns->pElCollapseTo[ nVx ] ;
          nElC[ nVx ] = pElC->number ;

          /* List the forming vertices of that coarse grid element. */
          pVC = nVxC + nVx*MAX_VX_ELEM ;
          mVx = elemType[pElC->elType].mVerts ;
          ppVx = pElC->PPvrtx ;
          for ( kVx = 0 ; kVx < mVx ; kVx++ )
            pVC[kVx] = ppVx[kVx]->number ;
        }

    adf_write2 ( lvl_id, "node-->mg_elem", "I4", 1, pUns->mVertsNumbered,
                 (char*) ( nElC+1 ), pChgSets->verts, &isNew ) ;  
    adf_write2 ( lvl_id, "node-->mg_verts", "I4", MAX_VX_ELEM, pUns->mVertsNumbered,
                 (char*) ( nVxC+MAX_VX_ELEM ), pChgSets->verts, &isNew ) ;  

    arr_free ( nElC ) ;
    arr_free ( nVxC ) ;



  }
  
  return ( *pReturnValue ) ;
}
/******************************************************************************

  write_adf_conn:
  Write the connectivity. Renumber all elements in the proper element type order.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_conn ( uns_s *pUns, const double lvl_id,
                            int mElemsNumbered, 
                            int *pFoundDrv, int *pReturnValue, chgSets_s *pChgSets ) {

  vrtx_struct **ppVx, *pHgVx[MAX_ADDED_VERTS] ;
  chunk_struct *pChunk ;
  int nBeg, nEnd, mVerts, mVxHg, *pEl2Vx, *pEV, mElems, mEN = mElemsNumbered, found, nV,
    kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1], isNew ;
  unsigned int *pnElMark, *pEM ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  char string[LINE_LEN] ;
  elem_type_enum elType ;
  elMark_s elMark ;

  if ( !*pReturnValue ) return ( 0 ) ;

  /* If the vertices change, we need to rewrite the element connectivity. */
  pChgSets->elems |= pChgSets->verts ;

  /* Element connectivities. First all base elements. */
  for ( elType = tri ; elType <= hex ; elType++ ) {
    mVerts = elemType[elType].mVerts ;
    /* We want clean base elements only. These are counted in the first tri-hex
       positions of mElems_w_mVerts. */
    mElems = pUns->mElems_w_mVerts[elType] ;
    pEl2Vx = NULL ;

    if ( mElems ) {      
      pEV = pEl2Vx = arr_malloc ( "pEl2Vx in write_adf_conn", pUns->pFam,
                                  mVerts*mElems, sizeof( int ) ) ;
      
      /* Make a consecutive list of pointers. */
      pChunk = NULL ;
      while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
        for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
          if ( pEl->number && pEl->markdEdges == elType ) {
            pEl->number = ++mEN ;
            for ( ppVx = pEl->PPvrtx ; ppVx < pEl->PPvrtx+mVerts ; ppVx++ )
              *pEV++ = (*ppVx)->number ;
          }
      
      if ( pEV-pEl2Vx != mVerts*mElems ) {
        printf ( " FATAL: %d conn. entries expected, but %d found in write_adf_conn.\n",
                 mVerts*mElems, (int)(pEV-pEl2Vx) ) ; *pReturnValue = 0 ; return ( 0 ) ;}
    }

    /* Create the adf entry, possibly empty. */
    sprintf ( string, "%s-->node", elemType[elType].name ) ;
    adf_write2 ( lvl_id, string, "I4", mVerts, mElems, (char*) pEl2Vx,
                 pChgSets->elems, &isNew ) ;
    pChgSets->elems |= isNew ;
    arr_free ( pEl2Vx ) ;
  }
  
  




  
  /* List the connectivity of the derived elements. */
  for ( found = 0, elType = (elem_type_enum ) (hex+1) ;
        elType < MAX_DRV_ELEM_TYPES ; elType++ )
    if ( ( mElems = pUns->mElems_w_mVerts[elType] ) ) {
      found += mElems ;
      mVerts = elType - MAX_ELEM_TYPES + 4 ;

      
      pEM = pnElMark = arr_malloc ( "pnElMark in write_adf_conn", pUns->pFam,
                                    mElems, sizeof( elMark_s ) ) ;
      pEV = pEl2Vx = arr_malloc ( "pEl2Vx in write_adf_conn", pUns->pFam,
                                  mVerts*mElems, sizeof( int ) ) ;

      pChunk = NULL ;
      while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
        for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
          if ( pEl->number && pEl->markdEdges == elType ) {
            pEl->number = ++mEN ;

            /* Base vertices. */
            for ( ppVx = pEl->PPvrtx ;
                  ppVx < pEl->PPvrtx + elemType[ pEl->elType ].mVerts ; ppVx++ )
              *pEV++ = (*ppVx)->number ;

            /* Hanging vertices. */
            get_drvElem_aE ( pUns, pEl, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
            for ( nV = 0 ; nV < mVxHg ; nV++ )
              *pEV++ = pHgVx[nV]->number ;

            /* Integer formatted elMark. */
            elMark = get_elMark_aE ( pUns, pEl, &mVxHg ) ;
            elMark2int ( elMark, pEM ) ;
            pEM += 2 ;
          }
        
      if ( pEV-pEl2Vx != mVerts*mElems ) {
        printf ( " FATAL: %d conn. entries expected, but %d found  in write_adf_conn.\n",
                 mVerts*mElems, (int)(pEV-pEl2Vx) ) ; *pReturnValue = 0 ; return (0 ) ; }

      
      /* Create the adf entry for the connectivity. */
      sprintf ( string, "drv_elem%dnode-->node", mVerts ) ;
      adf_write2 ( lvl_id, string, "I4", mVerts, mElems, (char*) pEl2Vx,
                 pChgSets->elems, &isNew ) ;
      pChgSets->elems |= isNew ;
      arr_free ( pEl2Vx ) ;
      
      /* Create the adf entry for the elMark. */
      sprintf ( string, "drv_elem%dnode_elmark", mVerts ) ;
      adf_write1 ( lvl_id, string, "I4", 2*mElems, (char*) pnElMark,
                 pChgSets->elems, &isNew ) ;
      arr_free ( pnElMark ) ;
    }

  if ( found ) {
    /* There were derived elements. Make an entry with the elMark version. */
    *pFoundDrv = found ;
    sprintf ( string, "%s", ELMARK_VERSION ) ;
    adf_write1 ( lvl_id, "elmark_version", "C1", strlen( string ), string,
                 pChgSets->elems, &isNew ) ;
  }

  if ( mEN - mElemsNumbered != pUns->mElemsNumbered ) {
    printf ( " FATAL: expected %d, found %d numbered elements in write_adf_conn.\n",
             pUns->mElemsNumbered, mEN ) ;
    *pReturnValue = 0 ; }
  
  return ( *pReturnValue ) ;
}

/******************************************************************************

  write_adf_bnd:
  Boundary faces.
  
  Last update:
  ------------
  24Apr2; write face normals for AVBP.
  : conceived.
  
  Input:
  ------
  pUns
  lvl_id
  writeNormals: write boundary nodes and normals, face to node connectivity.
  doHydra

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_bnd ( uns_s *pUns, const double lvl_id,
                           int writeNormals, int *pReturnValue,
                           chgSets_s *pChgSets, bndVxWt_s *pBWt, int *pmAllBndVx ) {

  const int mDim = pUns->mDim, doPer_and_edge0 = 1 ;
  const faceOfElem_struct *pFoE ;

  vrtx_struct **ppVx ;
  const int *kVxFc ;
  int nBc, mBc = pUns->mBc, mB, mT, mQ, mVerts, *pnBndVx,
    *pnV, *pMaskBndVx, *pBv, *pBi2vx, *pBm, *pBiMask, *pBe, *pBiEl,
    *pBk, *pBn, *pBikFc, *pBiNorm, *pTv, *pTm, *pTe, *pTk, *pTn, *pQv,
    *pQm, *pQe, *pQk, *pQn, *pTri2vx, *pTriMask, *pTriEl, *pTriNorm,
    *pQuad2vx, *pQuadMask, *pQuadEl, *pQuadNorm,
    *pTrikFc, *pQuadkFc, *listNormals, mAV, isNew ;
  bndPatch_struct *pBndPatch ;
  elem_struct *pElem ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  double *pN, *pNorm, *pW, *pWeight, *pWallDist ;

  if ( !*pReturnValue ) return ( 0 ) ;

  /* If the vertices change, so do the boundary vertices. */
  pChgSets->bndVx |= pChgSets->verts ;
  pChgSets->bndFc |= pChgSets->verts ;
  pChgSets->bndFc |= pChgSets->elems ;

  make_bndVxWts ( pUns, pBWt, pmAllBndVx ) ;
  mAV = *pmAllBndVx ;
      
  /* Dump. */
  adf_write2 ( lvl_id, "bnd_node-->node",   "I4", 1, mAV, (char*)(pBWt->pnVx),
               pChgSets->bndVx, &pChgSets->bndVx ) ;
  adf_write1 ( lvl_id, "m_bnd_node_group", "I4", mBc, (char*) pUns->pmVertBc,
               pChgSets->bndVx, &isNew ) ;
  adf_write2 ( lvl_id, "bnd_node-->group",  "I4", 1, mAV, (char*)(pBWt->pnBc),
               pChgSets->bndVx, &isNew ) ;
  arr_free ( pBWt->pnBc ) ;
  

  /* Calculate wall-distance. */
  if ( ( pWallDist = calc_wall_dist ( pUns, pBWt, *pmAllBndVx ) ) ) {
    /* There were viscous wall vertices. */
    adf_write2 ( lvl_id, "node_wall_distance","R8", 1, pUns->mVertsNumbered,
                 (char*) pWallDist, pChgSets->verts, &isNew ) ;
    arr_free ( pWallDist ) ;
  }
  
  arr_free ( pBWt->pNrm ) ;



  
  /* Vertices on the axis. */
  if ( pUns->specialTopo != axiX ) {
    /* No special verts, remove old records. */
    adf_delete ( lvl_id, "axi_node-->node" ) ;
  }
  else {
    /* How many are there? */
    int mVxAxi = x_axis_verts ( pUns ), *pXv, *pVxAxi, nBeg, nEnd ;
    chunk_struct *pChunk ;
    vrtx_struct *pVxBeg, *pVxEnd, *pVx ;

    pXv = pVxAxi = arr_malloc ( "pVxAxi in write_adf_bnd", pUns->pFam,
                                mVxAxi, sizeof( int ) ) ;
    
    pChunk = NULL ;
    while ( loop_verts( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->singular )
          *pXv++ = pVx->number ;

    adf_write2 ( lvl_id, "axi_node-->node", "I4", 1, mVxAxi, (char*) pVxAxi,
                 pChgSets->bndFc, &isNew ) ;

    arr_free ( pVxAxi ) ;
  }

  
  
  /* Boundary faces. */
  mB = pUns->mBiAllBc ;
  mT = pUns->mTriAllBc ;
  mQ = pUns->mQuadAllBc ;

  if ( writeNormals ) {
    pBv = pBi2vx    = arr_malloc ( "pBi2vx    in write_adf_bnd", pUns->pFam,
                                   mB*2, sizeof( int ) ) ;
    pBm = pBiMask   = arr_malloc ( "pBiMask   in write_adf_bnd", pUns->pFam,
                                   mB, sizeof( int ) ) ;
    pBn = pBiNorm   = arr_malloc ( "pBiMask   in write_adf_bnd", pUns->pFam,
                                   2*mB, sizeof( double ) ) ;
    
    pTv = pTri2vx   = arr_malloc ( "pTri2vx   in write_adf_bnd", pUns->pFam,
                                   mT*3, sizeof( int ) ) ;
    pTm = pTriMask  = arr_malloc ( "pTriMask  in write_adf_bnd", pUns->pFam,
                                   mT, sizeof( int ) ) ;
    pTn = pTriNorm   = arr_malloc ( "pTriNorm   in write_adf_bnd", pUns->pFam,
                                   3*mT, sizeof( double ) ) ;
    
    pQv = pQuad2vx  = arr_malloc ( "pQuad2vx  in write_adf_bnd", pUns->pFam,
                                   mQ*4, sizeof( int ) ) ;
    pQm = pQuadMask = arr_malloc ( "pQuadMask in write_adf_bnd", pUns->pFam,
                                   mQ, sizeof( int ) ) ;
    pQn = pQuadNorm = arr_malloc ( "pQuadNorm   in write_adf_bnd", pUns->pFam,
                                   3*mQ, sizeof( double ) ) ;
  }
  pBe = pBiEl     = arr_malloc ( "pBiEl    in write_adf_bnd",
                                 pUns->pFam, mB, sizeof( int ) ) ;
  pBk = pBikFc    = arr_malloc ( "pBikFc   in write_adf_bnd",
                                 pUns->pFam, mB, sizeof( int ) ) ;
  pTe = pTriEl    = arr_malloc ( "pTriEl   in write_adf_bnd",
                                 pUns->pFam, mT, sizeof( int ) ) ;
  pTk = pTrikFc   = arr_malloc ( "pTrikFc  in write_adf_bnd",
                                 pUns->pFam, mT, sizeof( int ) ) ;
  pQe = pQuadEl   = arr_malloc ( "pQuadEl  in write_adf_bnd",
                                 pUns->pFam, mQ, sizeof( int ) ) ;
  pQk = pQuadkFc  = arr_malloc ( "pQuadkFc in write_adf_bnd",
                                 pUns->pFam, mQ, sizeof( int ) ) ;   

  
  /* List the element and face number. */
  for ( nBc = 0 ; pBndPatch = NULL, nBc < mBc ; nBc++ ) {

    double fcNorm[MAX_DIM] ;
    int mTimes ;

    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pElem = pBndFc->Pelem ;
        if ( pElem && pElem->number && pBndFc->nFace ) {
          pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
          mVerts = pFoE->mVertsFace ;
          kVxFc = pFoE->kVxFace ;
          ppVx = pElem->PPvrtx ;

          uns_face_normal ( pElem, pBndFc->nFace, fcNorm, &mTimes ) ;
          vec_mult_dbl ( fcNorm, 1./mTimes/mVerts, mDim ) ;

          if ( mVerts == 2 ) {
            *pBe++ = pElem->number ;
            *pBk++ = pBndFc->nFace ;
            if ( writeNormals ) {
              *pBv++ = ppVx[ kVxFc[0] ]->number ; 
              *pBv++ = ppVx[ kVxFc[1] ]->number ;
              *pBm++ = nBc+1 ;
              
              *pBn++ = fcNorm[0] ;
              *pBn++ = fcNorm[1] ;
            }
          }
          else if ( mVerts == 3 ) {
            *pTe++ = pElem->number ;
            *pTk++ = pBndFc->nFace ;
            if ( writeNormals ) {
              *pTv++ = ppVx[ kVxFc[0] ]->number ; 
              *pTv++ = ppVx[ kVxFc[1] ]->number ;
              *pTv++ = ppVx[ kVxFc[2] ]->number ;
              *pTm++ = nBc+1 ;
              
              *pTn++ = fcNorm[0] ;
              *pTn++ = fcNorm[1] ;
              *pTn++ = fcNorm[2] ;
            }
          }
          else {
            *pQe++ = pElem->number ;
            *pQk++ = pBndFc->nFace ;
            if ( writeNormals ) {
              *pQv++ = ppVx[ kVxFc[0] ]->number ; 
              *pQv++ = ppVx[ kVxFc[1] ]->number ;
              *pQv++ = ppVx[ kVxFc[2] ]->number ;
              *pQv++ = ppVx[ kVxFc[3] ]->number ;
              *pQm++ = nBc+1 ;

              *pQn++ = fcNorm[0] ;
              *pQn++ = fcNorm[1] ;
              *pQn++ = fcNorm[2] ;
            }
          }
        }
      }
  }

  if ( pBe - pBiEl != mB || pTe - pTriEl != mT || pQe - pQuadEl != mQ ) {
    printf ( " FATAL: miscount of face connectivities in write_adf_bnd.\n" ) ;
    return ( 0 ) ; }


                                                 
  adf_write2 ( lvl_id, "bnd_line-->elem",         "I4",  1,  mB, (char*)   pBiEl,
               pChgSets->bndFc, &isNew ) ;
  pChgSets->bndFc |= isNew ;
  adf_write2 ( lvl_id, "bnd_tri-->elem",          "I4",  1,  mT, (char*)  pTriEl,
               pChgSets->bndFc, &isNew ) ;
  pChgSets->bndFc |= isNew ;
  adf_write2 ( lvl_id, "bnd_qua-->elem",          "I4",  1,  mQ, (char*) pQuadEl,
               pChgSets->bndFc, &isNew ) ;
  pChgSets->bndFc |= isNew ;

  adf_write2 ( lvl_id, "bnd_line-->face_in_elem", "I4",  1,  mB, (char*)   pBikFc,
               pChgSets->bndFc, &isNew ) ;
  adf_write2 ( lvl_id, "bnd_tri-->face_in_elem",  "I4",  1,  mT, (char*)  pTrikFc,
               pChgSets->bndFc, &isNew ) ;
  adf_write2 ( lvl_id, "bnd_qua-->face_in_elem",  "I4",  1,  mQ, (char*) pQuadkFc,
               pChgSets->bndFc, &isNew ) ;

  /* Indices. */
  adf_write1 ( lvl_id, "m_bnd_line_group", "I4", mBc, (char*) pUns->pmBiBc, 
               pChgSets->bndFc, &isNew ) ;
  adf_write1 ( lvl_id, "m_bnd_tri_group", "I4", mBc, (char*) pUns->pmTriBc, 
               pChgSets->bndFc, &isNew ) ;
  adf_write1 ( lvl_id, "m_bnd_qua_group", "I4", mBc, (char*) pUns->pmQuadBc,
               pChgSets->bndFc, &isNew ) ;
  

  if ( writeNormals ) {
    adf_write2 ( lvl_id, "bnd_line-->node",         "I4", 2, mB, (char*)   pBi2vx,
                 pChgSets->bndFc, &isNew  ) ;
    adf_write2 ( lvl_id, "bnd_tri-->node",          "I4", 3, mT, (char*)  pTri2vx,
                 pChgSets->bndFc, &isNew  ) ;
    adf_write2 ( lvl_id, "bnd_qua-->node",          "I4", 4, mQ, (char*) pQuad2vx, 
                 pChgSets->bndFc, &isNew ) ;
    
    adf_write2 ( lvl_id, "bnd_line-->group",        "I4", 1, mB, (char*)   pBiMask,
                 pChgSets->bndFc, &isNew );
    adf_write2 ( lvl_id, "bnd_tri-->group",         "I4", 1, mT, (char*)  pTriMask,
                 pChgSets->bndFc, &isNew );
    adf_write2 ( lvl_id, "bnd_qua-->group",         "I4", 1, mQ, (char*) pQuadMask,
                 pChgSets->bndFc, &isNew );

    adf_write2 ( lvl_id, "bnd_line_norm",           "R8", 2, mB, (char*)   pBiNorm,
                 pChgSets->bndFc, &isNew );
    adf_write2 ( lvl_id, "bnd_tri_norm",            "R8", 3, mB, (char*)   pTriNorm,
                 pChgSets->bndFc, &isNew );
    adf_write2 ( lvl_id, "bnd_qua_norm",            "R8", 3, mB, (char*)   pQuadNorm,
                 pChgSets->bndFc, &isNew );
  }




  if ( writeNormals ) {
    arr_free (   pBi2vx  ) ;  
    arr_free (  pTri2vx  ) ;  
    arr_free ( pQuad2vx  ) ;  
    arr_free (   pBiMask ) ;  
    arr_free (  pTriMask ) ;  
    arr_free ( pQuadMask ) ;
    arr_free (   pBiNorm ) ;  
    arr_free (  pTriNorm ) ;  
    arr_free ( pQuadNorm ) ;
  }
  arr_free (   pBiEl   ) ;  
  arr_free (  pTriEl   ) ;  
  arr_free ( pQuadEl   ) ;  
  arr_free (   pBikFc  ) ;  
  arr_free (  pTrikFc  ) ;  
  arr_free ( pQuadkFc  ) ;

  return ( 1 ) ;
}


/******************************************************************************

  write_adf_per:
  Write a list of periodic vertex pairs. When mulitple periodicity occurs,
  all combinations are listed once. Symmetry vertices are listed at the
  beginning of the list as periodic with themselves. The behaviour of both
  symmetry and periodicity is not specified.
  
  Last update:
  ------------
  24Apr02; list mSymmVx only for hydra.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_per ( uns_s *pUns, const double lvl_id, int *pReturnValue,
                           chgSets_s *pChgSets ) {

  int nBc, iVx, mVx, *pnVxSymmPer, *pnV, *pnV2, mSymmVx = 0 ;

  if ( !*pReturnValue ) return ( 0 ) ;

  
  mVx = mSymmVx + pUns->mPerVxPairs ;
    
  if ( !mVx )
    /* Nothing to write. */
    return ( 1 ) ;

  pChgSets->perPairs |= pChgSets->bndVx ;


  pnV = pnVxSymmPer = arr_malloc ( "pnVxSymmPer in write_adf_per", pUns->pFam,
                                   2*mVx, sizeof( int ) ) ;
  
  /* Symmetry vertices. Loop over all patches and list the boundary nodes. */
  if ( mSymmVx ) {
    adf_write1 ( lvl_id, "m_symm_edge", "I4", 1, (char*) &mSymmVx,
                 pChgSets->perPairs, &pChgSets->perPairs ) ;

    for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
      if ( pUns->ppBc[nBc]->type == 's' ) {
        list_vert_bc ( pUns, nBc, pUns->pmVertBc[nBc], pnV ) ;
        pnV += pUns->pmVertBc[nBc] ;
      }
    
    /* Duplicate all the entries to make symmetric edges. */
    pnV2 = pnVxSymmPer + 2*mSymmVx-1 ;
    for ( pnV = pnVxSymmPer + mSymmVx-1 ; pnV >= pnVxSymmPer ; pnV-- ) {
      *pnV2-- = *pnV ;
      *pnV2-- = *pnV ;
    }
  }
  
  
  /* List the periodic vertex pairs. */
  pnV = pnVxSymmPer + 2*mSymmVx ;
  for ( iVx = 0 ; iVx < pUns->mPerVxPairs ; iVx++ ) {
    *pnV++ = pUns->pPerVxPair[iVx].In->number ;
    *pnV++ = pUns->pPerVxPair[iVx].Out->number ;
  }

  if ( pnV - pnVxSymmPer != 2*mVx ) {
    printf ( " FATAL: %d periodic pairs expected, %d found in write_adf_per.\n",
              pUns->mPerVxPairs, (int)(pnV - pnVxSymmPer)/2 - mSymmVx ) ;
    return ( 0 ) ; }

  adf_write2 ( lvl_id, "per_edge-->node", "I4", 2, mVx, (char*)  pnVxSymmPer,
               pChgSets->perPairs, &pChgSets->perPairs ) ;
  arr_free ( pnVxSymmPer ) ;

  return ( 1 ) ;
}


/******************************************************************************

  write_adf_edges:
  Write edges, symmetric and not, and element to edge connectivity.
  
  Last update:
  ------------
  10Nov00; use pBWt instead of invoking make_bndVxNormals.
  9Feb99; list quad-->edge in circular order, the first edge is between the
          first and second vertex.
  1Oct99; intro egWtCutOff ;
  3Aug99; list all elemental edges as anti-symmetric, even if they don't carry
          any weight.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_edges ( uns_s *pUns, const double lvl_id,
                             bndVxWt_s *pBWt, int mAllBndVx,
                             int *pReturnValue, chgSets_s *pChgSets ) {

  const int mDim = pUns->mDim ;
  int nEdge, mEdges, mListEdges, mSymmEg, mAntiEg, *pVe, * pnVx_Eg, nDim, isNew, 
    *pEe, *pE0, *pEl2Eg, mElems, kEg, sw, nBeg, nEnd, *pEgNr, mAe, lastElemEdge,
    mElemEg ;
  double *pNe, *pEN, *pNrm_Eg, *pEdgeNormals = NULL ;
  vrtx_struct *pVx1, *pVx2 ;
  elem_type_enum elType ;
  chunk_struct *pChunk ;
  elem_struct *pElBeg, *pElEnd, *pEl ;
  char string[TEXT_LEN] ;

  if ( !*pReturnValue ) return ( 0 ) ;

  pChgSets->edges |= pChgSets->verts ;
  /* We also list edge to element connectivity. */ 
  pChgSets->edges |= pChgSets->elems ;


  /* Make a list of edges. */
  if ( verbosity > 3 )
    printf ( "      INFO: preparing list of edges.\n" ) ;
  
  if ( !make_edge_normals ( pUns, &pEdgeNormals,
                            &mListEdges, &mSymmEg, &mAntiEg, &mElemEg ) ) {
    printf ( " FATAL: failed to get a list of periodic edges in write_adf_edges.\n" ) ;
    return ( 0 ) ; }
  /* We want all elemental edges listed. */
  mAntiEg = MAX(mAntiEg,mElemEg) ;




  /* List forming vertices and weights of anti-symmetric edges. */
  pVe = pnVx_Eg  = arr_malloc ( "pnVx_Eg in write_adf_edges", pUns->pFam, 
                                mAntiEg*2, sizeof( int ) ) ;
  pNe = pNrm_Eg  = arr_malloc ( "pNrm_Eg in write_adf_edges", pUns->pFam,
                                mAntiEg*mDim, sizeof( double ) ) ;
        pEgNr    = arr_malloc ( "pEgNr in write_adf_edges", pUns->pFam,
                                mListEdges+1, sizeof( int ) ) ;
  
  /* Anti-symmetric edge list. List all edges that carry an anti-symmetric weight. */

  /* Reset the numbers. */
  for ( mAe = 0, nEdge = 1 ; nEdge <= mListEdges ; nEdge++ )
    pEgNr[nEdge] = 0 ;
  
  /* Loop over all edges, find unmarked anti-symmetric ones. */ 
  for ( mAe = 0, nEdge = 1 ; nEdge <= mListEdges ; nEdge++ )
    if ( show_edge ( pUns->pllEdge, nEdge, &pVx1, &pVx2 ) ) {

      /* Find out whether the edge carries an anti-symmetric component.
         All elemental edges must be listed, they are the first mElemEg in the list. */
      pEN = pEdgeNormals + 2*mDim*nEdge + mDim ;
      for ( nDim = 0 ; nDim < mDim && nEdge > mElemEg ; nDim++ )
        if ( ABS( pEN[nDim] ) > Grids.egWtCutOff )
          break ;

      if ( nDim < mDim ) {
        /* There was an anti-symmetric component. */
        *pVe++ = pVx1->number ;
        *pVe++ = pVx2->number ;

        if ( pVx1->number == 56 && pVx2->number == 375338 )
          printf ( " the one.\n" ) ;
        
        vec_copy_dbl ( pEN, mDim, pNe ) ;
        pNe += mDim ;
        /* Number all anti-symmetric edges for element->edge pointers. */
        pEgNr[nEdge] = ++mAe ;

        if ( pVx1->number == pVx2->number )
          printf ( " FATAL: found collapsed edge %d-%d in write_adf_edges.\n",
                   pVx1->number, pVx2->number ) ;
        
      }
    }
  
  if ( pVe-pnVx_Eg != 2*mAntiEg )
    printf ( " FATAL: expecting %d antim. edge conn., found %d in write_adf_edges.\n"
	     " File will be invalid.\n", 2*mAntiEg, (int)(pVe-pnVx_Eg) ) ;



  
    /* Check the weights for summing to zero. */
  {
    int nBc, nVx, mDim = pUns->mDim, nEg, nVx0, nVx1, kVx, kVxMax ;
    bndNorm_s *pBnr, *pBndNorm ;
    double *pVxWt, *pEgWt, norm, maxErr = -TOO_MUCH, *wt0, *wt1, wt[2][MAX_DIM],
      *pWt = arr_malloc ( "pWt", pUns->pFam, pUns->mVertsNumbered+1,
                          mDim*sizeof( double ) ) ;

    for ( pVxWt = pWt ; pVxWt < pWt + ( pUns->mVertsNumbered+1 )*mDim ; pVxWt++ )
      *pVxWt = 0. ;


    /* Edge weights. */
    for ( nEg = 0 ; nEg < mAntiEg ; nEg++ ) {
      pEgWt  = pNrm_Eg + mDim*nEg ;

      pVxWt = pWt     + mDim*pnVx_Eg[2*nEg] ;
      vec_add_dbl ( pVxWt, pEgWt, mDim, pVxWt ) ;
      pVxWt = pWt     + mDim*pnVx_Eg[2*nEg+1] ;
      vec_diff_dbl ( pVxWt, pEgWt, mDim, pVxWt ) ;
    }
      

    /* Boundary terms. */
    for ( kVx = 0 ; kVx < mAllBndVx ; kVx++ ) {
      nVx = pBWt->pnVx[kVx] ;
      pVxWt = pWt + mDim*nVx ;
          
      vec_add_dbl ( pVxWt, pBWt->pWt+kVx*mDim, mDim, pVxWt ) ;
    }


    /* Parallel exchange. */
    for ( kVx = 0 ; kVx < pUns->mPerVxPairs ; kVx++ ) {

      nVx0 = pUns->pPerVxPair[kVx].In->number ;
      nVx1 = pUns->pPerVxPair[kVx].Out->number ;

      wt0 = pWt + mDim*nVx0 ;
      wt1 = pWt + mDim*nVx1 ;

      rot_coor_dbl ( wt1, pUns->pPerBc->rotOut2in, mDim, wt[0] ) ;
      rot_coor_dbl ( wt0, pUns->pPerBc->rotIn2out, mDim, wt[1] ) ;

      vec_add_dbl ( wt[0], wt0, mDim, wt0 ) ;
      vec_add_dbl ( wt[1], wt1, mDim, wt1 ) ;
    }


    
    /* Symmetry vertices. Loop over all patches and set the component normal to
       the symmetry plane to zero. A bit ugly, since we needn't use elems and
       boundary faces so far. */
    for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
      if ( pUns->ppBc[nBc]->type == 's' ) {
        const elemType_struct *pElT ;
        const faceOfElem_struct *pFoE ;
        const int *kVxFace ;
        bndPatch_struct *pBndPatch = NULL ;
        bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
        elem_struct *pElem ;
        int kVx, mV, nVx ;
        
        while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
          for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) 
            if ( ( pElem = pBndFc->Pelem ) && pElem->number && pBndFc->nFace ) {
              pElT = elemType + pElem->elType ;
              pFoE = pElT->faceOfElem + pBndFc->nFace ;
              kVxFace = pFoE->kVxFace ;
              mV = pFoE->mVertsFace ;

              for ( kVx = 0; kVx < mV ; kVx++ ) {
                nVx = pElem->PPvrtx[ kVxFace[kVx] ]->number ;
                pVxWt = pWt + mDim*nVx ;
                pVxWt[symmCoor] = 0. ;
              }
            }
      }

    

    /* Find the maximum errors. */
    for ( nVx = kVxMax = 1 ; nVx <= pUns->mVertsNumbered ; nVx++ ) {
      norm = vec_len_dbl ( pWt+mDim*nVx, mDim ) ;
      if ( norm > maxErr ) {
        kVxMax = nVx ;
        maxErr = norm ;
      }
      
      if ( norm > 1.e-9 && verbosity > 5 ) {
        vrtx_struct *pVx = pUns->ppChunk[0]->Pvrtx+nVx ;
        printf ( " FATAL: weight of %d (%10.5g,%10.5g,%10.5g)\n"
                 "      sums to %10.5g (%10.5g,%10.5g,%10.5g).\n",
                 nVx, pVx->Pcoor[0], pVx->Pcoor[1], pVx->Pcoor[2], norm,
                 pWt[mDim*nVx], pWt[mDim*nVx+1], pWt[mDim*nVx+2] ) ;
        pVx = NULL ;
      }
    }

    arr_free ( pWt ) ;

    if ( verbosity > 2 )
      printf ( "      INFO: maximum error in sums of edge weights: %g at vertex %d\n",
               maxErr, kVxMax ) ;
  }
    

  adf_write2 ( lvl_id, "edge-->node",   "I4",    2, mAntiEg, (char*) pnVx_Eg,
               pChgSets->edges, &pChgSets->edges );
  adf_write2 ( lvl_id, "edge_weights", "R8", mDim, mAntiEg, (char*) pNrm_Eg,
               pChgSets->edges, &pChgSets->edges );
  arr_free ( pnVx_Eg ) ;
  arr_free ( pNrm_Eg ) ;




  

  if ( mSymmEg ) {
    /* List forming vertices and weights of symmetric edges. */
    pVe = pnVx_Eg  = arr_malloc ( "pnVx_Eg in write_adf_edges", pUns->pFam,
                                  mSymmEg*2, sizeof( int ) ) ;
    pNe = pNrm_Eg = arr_malloc ( "pNrm_Eg in write_adf_edges", pUns->pFam,
                                 mSymmEg*mDim, sizeof( double ) ) ;
  
    /* write(1) (( ewt(idum,ie),idum=1,2),ie=1,nedge ) ! symmetric edge norms */
    for ( nEdge = 1 ; nEdge <= mListEdges ; nEdge++ )
      if ( show_edge ( pUns->pllEdge, nEdge, &pVx1, &pVx2 ) ) {

        /* Find out whether the edge carries an symmetric component. */
        pEN = pEdgeNormals + 2*mDim*nEdge ;
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
          if ( ABS( pEN[nDim] ) > Grids.egWtCutOff )
            break ;

        if ( nDim < mDim ) {
          /* There was a non-zero component. */
          *pVe++ = pVx1->number ;
          *pVe++ = pVx2->number ;
          vec_copy_dbl ( pEN, mDim, pNe ) ;
          pNe += mDim ;
        }
      }
    
    if ( pVe-pnVx_Eg != 2*mSymmEg )
      printf ( " FATAL: expecting %d symm. edge conn., found %d in write_adf_edges.\n"
               " File will be invalid.\n", 2*mSymmEg, (int)(pVe-pnVx_Eg) ) ;


    adf_write2 ( lvl_id, "symm_edge-->node", "I4",      2, mSymmEg, (char*) pnVx_Eg,
                 pChgSets->edges, &pChgSets->edges ) ;
    adf_write2 ( lvl_id, "symm_edge_weights", "R8", mDim, mSymmEg, (char*) pNrm_Eg,
                 pChgSets->edges, &isNew ) ;
    arr_free ( pnVx_Eg ) ;
    arr_free ( pNrm_Eg ) ;
  }


  

  /* List element to edge connectivity. */
  if ( mDim == 2 ) {
    /* Create the tri+qua = quad record directly.*/
    mElems = pUns->mElemsOfType[tri] + pUns->mElemsOfType[qua] ;

    pEe = pEl2Eg =
      arr_malloc ( "pEl2Eg in write_adf_edges", pUns->pFam, 4*mElems, sizeof(int)) ;
    
    for ( elType = tri ; elType <= qua ; elType++ )
      if ( ( pUns->mElems_w_mVerts[elType] ) ) {
        mEdges = elemType[elType].mEdges ;
        
        /* Make a consecutive list of pointers. */
        pChunk = NULL ;
        while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
          for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
            if ( pEl->number && pEl->elType == elType ) {
              pE0 = pEe ;
              /* List quad-->edge in circular order. */
              for ( kEg = 0 ; kEg < mEdges ; kEg++ ) {
                pVx1 = pEl->PPvrtx[ kEg ] ;
                pVx2 = pEl->PPvrtx[ (kEg+1)%mEdges ] ;
                nEdge = pEgNr[ get_edge_vrtx ( pUns->pllEdge, 
                                               (CV**) &pVx1, (CV**) &pVx2, &sw ) ] ;
                if ( pVx1 == pVx2 )
                  /* Degenerate edge. */
                  *pEe++ = 0 ;
                else if ( !nEdge ) {
                  printf ( " FATAL: elemental edge %d-%d not anti-symmetric"
                           " in write_adf_edges\n", pVx1->number, pVx2->number ) ;
                  *pEe++ = 0 ;
                }
                else
                  *pEe++ = lastElemEdge = nEdge ;
              }

              for ( kEg = 0 ; kEg < mEdges ; kEg++ )
                if ( pE0[kEg] == 0 )
                  /* Degenerate edge, make it point to the last edge of this element. */
                  pE0[kEg] = lastElemEdge ;
              
              if ( elType == tri )
                /* Bogus fourth entry, duplicate the first one. */
                *pEe++ = pEe[-3] ;
            }
      }
    
    if ( pEe-pEl2Eg != 4*mElems ) {
      printf ( " FATAL: %d con. entries expected, but %d found in write_adf_edge.\n",
               4*mElems, (int)(pEe-pEl2Eg) ) ; *pReturnValue = 0 ; return ( 0 ) ; }
    
      adf_write2 ( lvl_id, "quad-->edge",          "I4", 4, mElems, (char*) pEl2Eg,
                   pChgSets->edges, &isNew ) ;
      arr_free ( pEl2Eg ) ;
  }

  else {
    /* Create a separate record for all element types in 3D. */
    for ( elType = tet ; elType <= hex ; elType++ ) {
      mElems = pUns->mElemsOfType[elType] ;
      mEdges = elemType[elType].mEdges ;
      pEl2Eg = NULL ;
        
      if ( mElems ) {
        pEe = pEl2Eg = arr_malloc ( "pEl2Eg in write_adf_edges", pUns->pFam,
                                            mEdges, mElems*sizeof( int ) ) ;
      
        /* Make a consecutive list of pointers. */
        pChunk = NULL ;
        while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
          for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
            if ( pEl->number && pEl->elType == elType ) {
              pE0 = pEe ;
              for ( kEg = 0 ; kEg < mEdges ; kEg++ ) {
                nEdge = pEgNr[ get_elem_edge ( pUns->pllEdge, pEl, kEg,
                                         (CV**) &pVx1, (CV**) &pVx2, &sw ) ] ; 
                if ( pVx1 == pVx2 )
                  /* Degenerate edge. */
                  *pEe++ = 0 ;
                else if ( !nEdge ) {
                  printf ( " FATAL: elemental edge %d-%d not anti-symmetric"
                           " in write_adf_edges\n", pVx1->number, pVx2->number ) ;
                  return ( 0 ) ; }
                else
                  *pEe++ = lastElemEdge = nEdge ;
              }
              
              for ( kEg = 0 ; kEg < mEdges ; kEg++ )
                if ( pE0[kEg] == 0 )
                  /* Degenerate edge, make it point to the last edge of this element. */
                  pE0[kEg] = lastElemEdge ;
            }
        
        if ( pEe-pEl2Eg != mEdges*mElems ) {
          printf ( " FATAL: %d conn. entries expected, but %d found"
                   " in write_adf_conn.\n", mEdges*mElems, (int)(pEe-pEl2Eg) ) ;
          *pReturnValue = 0 ; return ( 0 ) ; }

      }
      
      
      /* Create the adf entry. */
      sprintf ( string, "%s-->edge", elemType[elType].name ) ;
      adf_write2 ( lvl_id, string, "I4", mEdges, mElems, (char*) pEl2Eg,
                   pChgSets->edges, &isNew ) ;
      arr_free ( pEl2Eg ) ;
    }
  }


  free_llEdge ( &pUns->pllEdge ) ;

  if ( verbosity > 3 )
    printf ( "      INFO: found %d anti-symmetric, %d symmetric edges.\n",
             mAntiEg, mSymmEg ) ;

  return ( 1 ) ;
}

/******************************************************************************

  write_adf_prt:
  Write the list of parents for derefinement, etc. Always updated.
  
  Last update:
  ------------
  9Mar01; write mg-intergrid connectivity pointers node-->mg_node.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_prt ( uns_s *pUns, const double root_id, int *pReturnValue,
                           chgSets_s *pChgSets ) {

  const int dontWriteNormals = 0, dontHydra = 0 ;
  const refType_struct *pRefT ;
  chunk_struct *pChunk ;
  elem_struct **ppCh, *pElem, *pElBeg, *pElEnd ;
  double prt_id ;
  int mLeafElems, mPrtElems, mPrtElem2VertP, mPrtElem2ChildP, mPrtBndPatches,
    mPrtBndFaces, mPrtElemsPerType[MAX_ELEM_TYPES], *pmC, *pmCh_prtEl,
    *pnR, *pnRefT_prtEl, *pnC, *pnCh_prtEl, nBeg, nEnd, nRefType, err, mChildren, nCh,
    foundDrv, mNr, mVerts, mVxHg, kVxHg[MAX_ADDED_VERTS], isNew, 
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1], mAllBndVx, nr, *nVxPrt ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS], *pVx, *pVxB, *pVxE ;
  char string[LINE_LEN] ;
  elem_type_enum elType ;
  bndVxWt_s *pBWt ;

  chgSets_s chgSetsPrt = *pChgSets ;
  chgSetsPrt.all = 1 ;
  

  if ( !*pReturnValue ) return ( 0 ) ;



  
  /* Number parents and children independently. Count. */
  number_uns_elem_leafNprt ( pUns, &mLeafElems, &mPrtElems, mPrtElemsPerType,
			     &mPrtElem2VertP, &mPrtElem2ChildP,
			     &mPrtBndPatches, &mPrtBndFaces ) ;

  /* Continue numbering parent elements from here. */
  mNr = pUns->mElemsNumbered ;

  
  if ( !mPrtElems )
    /* There are no parents to write. */
    return ( 1 ) ;




  /* Write a hydra style mg-adapt hierarchy for the vertices. */
  nVxPrt = arr_calloc ( "nVxPrt in write_adf_prt", pUns->pFam,
                        pUns->mVertsNumbered+1, sizeof( int ) ) ;

  /* Start all vertices off with pointing to themselves. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxB, &nBeg, &pVxE, &nEnd ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
      if ( ( nr = pVx->number ) )
        nVxPrt[nr] = nr ;

    /* Only the vertices after the first chunk are added and require a parent. */
  pChunk = pUns->ppChunk[0] ;
  while ( loop_verts ( pUns, &pChunk, &pVxB, &nBeg, &pVxE, &nEnd ) )
    for ( pVx = pChunk->Pvrtx+1 ; pVx <= pVxE ; pVx++ )
      if ( pVx->number ) {
        nr = pVx - pChunk->Pvrtx ;
        nVxPrt[pVx->number] = ( de_cptVx ( pUns, pChunk->pVxCpt[nr] ) )->number ;
      }

  adf_write1 ( root_id,"node-->mg_node","I4",pUns->mVertsNumbered, (char*)(nVxPrt+1),
               1, &isNew ) ;

  arr_free ( nVxPrt ) ;



  
  
  /* Make a node for the parents. */
  prt_id = adf_create ( root_id, "parent", &isNew ) ;
  
  /* Reftype version. */
  sprintf ( string, "%s", REFTYPE_VERSION ) ;
  adf_write1 ( prt_id, "reftype_version", "C1", strlen( string ), string,
               1, &isNew ) ;

  /* List the children for each parent. */
  pmC = pmCh_prtEl   = arr_malloc ( "pmCh in write_adf_prt", pUns->pFam,
                                    mPrtElems, sizeof( int ) ) ;
  pnR = pnRefT_prtEl = arr_malloc ( "pnRefT_prtEl in write_adf_prt", pUns->pFam,
                                    mPrtElems, sizeof( int ) ) ;
  pnC = pnCh_prtEl   = arr_malloc ( "pnCh_prtEl in write_adf_prt", pUns->pFam,
                                    mPrtElem2ChildP, sizeof ( int ) ) ;


  /* The element counters mElemsOfType are set for the finest mesh. Loop
     over all types and renumber the parents. Buffered elements are listed
     as derived elements to recover hanging edges. Number them in sequence
     such that parent-->child pointers are ok in the second pass. */
  for ( elType = tri ; elType <= hex ; elType++ ) {
    mVerts = elemType[elType].mVerts ;
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && pElem->elType == elType &&
             ( pRefT = pElem->PrefType ) ) {
          if ( pRefT->refOrBuf == ref ) {
            pElem->number = ++mNr ;
          }
          else {
            /* Buffered element. Count the number of vertices for proper ordering. */
            get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
	    pElem->markdEdges = mVerts + mVxHg + MAX_ELEM_TYPES - 4 ;
          }
        }
  }

  /* Buffered/derived elements. */
  for ( elType = (elem_type_enum)(hex+1) ; elType < MAX_DRV_ELEM_TYPES ; elType++ ) {
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && pElem->markdEdges == elType &&
             ( pRefT = pElem->PrefType ) ) {
          if ( pRefT->refOrBuf == buf ) {
            pElem->number = ++mNr ;
          }
        }
  }



  
  /* List them in sequence. */
  for ( elType = tri ; elType <= hex ; elType++ ) {
    mVerts = elemType[elType].mVerts ;
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && pElem->elType == elType &&
             ( pRefT = pElem->PrefType ) ) {
          if ( pRefT->refOrBuf == ref ) {
            ppCh = pElem->PPchild ;
            mChildren = pRefT->mChildren ;
            if ( pRefT->refOrBuf == ref )
              nRefType = pRefT->nr ;
            else if ( pRefT->refOrBuf == buf )
              nRefType = -1 ;
            
            *pnR++ = nRefType ;
            *pmC++ = mChildren ;
            for ( nCh = 0 ; nCh < mChildren ; nCh++ )
              *pnC++ = ppCh[nCh]->number ;
          }
          else {
            /* Buffered element. Count the number of vertices for proper ordering. */
            get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
	    pElem->markdEdges = mVerts + mVxHg + MAX_ELEM_TYPES - 4 ;
          }
        }
  }

  /* Buffered/derived elements. */
  for ( elType = (elem_type_enum)(hex+1) ; elType < MAX_DRV_ELEM_TYPES ; elType++ ) {
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && pElem->markdEdges == elType &&
             ( pRefT = pElem->PrefType ) ) {
          if ( pRefT->refOrBuf == buf ) {
            ppCh = pElem->PPchild ;
            mChildren = pRefT->mChildren ;
            if ( pRefT->refOrBuf == ref )
              nRefType = pRefT->nr ;
            else if ( pRefT->refOrBuf == buf )
              nRefType = -1 ;
            
            *pnR++ = nRefType ;
            *pmC++ = mChildren ;
            for ( nCh = 0 ; nCh < mChildren ; nCh++ )
              *pnC++ = ppCh[nCh]->number ;
          }
        }
  }

  
  if ( pmC - pmCh_prtEl != mPrtElems || pnC - pnCh_prtEl != mPrtElem2ChildP ) {
    printf ( " FATAL: miscount in number of parents or number of children"
             " in write_adf_prt.\n" ) ;
    return ( 0 ) ; }


  
  adf_write1 ( prt_id,"m_children_elem",     "I4",mPrtElems,      (char*)pmCh_prtEl,
               1, &isNew ) ;
  adf_write1 ( prt_id,"elem-->reftype",      "I4",mPrtElems,      (char*)pnRefT_prtEl,
               1, &isNew );
  adf_write1 ( prt_id,"parent-->child_elem", "I4",mPrtElem2ChildP,(char*)pnCh_prtEl,
               1, &isNew ) ;

  arr_free ( pmCh_prtEl   ) ;
  arr_free ( pnRefT_prtEl ) ;
  arr_free ( pnCh_prtEl   ) ;


  /* Continue numbering parent elements from here. */
  mNr = pUns->mElemsNumbered ;

  /* Use the generic routines to write the parents after numbering parents only. 
     Number and count the parent elements only. Redo the elem_w_mVerts counters. */
  number_uns_elemFromVerts ( pUns, parent ) ;
  count_uns_bndFaces ( pUns ) ;


  /* Boundary weights and normals. */
  pBWt = arr_malloc ( "pBWt in write_adf_bnd", pUns->pFam,
                      pUns->mBc, sizeof( bndVxWt_s ) ) ;
  pBWt->pWt = NULL ;
  
  
  /* Note that adf_conn renumbers by type, as needed by adf_bnd. Although this
     is already done in the loop above. */
  write_adf_conn ( pUns, prt_id, mNr, dontHydra, &foundDrv, pReturnValue, &chgSetsPrt ) ;
  write_adf_bnd  ( pUns, prt_id, dontWriteNormals, dontHydra, pReturnValue, &chgSetsPrt,
                   pBWt, &mAllBndVx );

  arr_free ( pBWt->pWt ) ;
  arr_free ( pBWt->pnVx ) ;
  arr_free ( pBWt->pVx ) ;
  arr_free ( pBWt ) ;

    
  /* Restore leaf numbering. */
  number_uns_elemFromVerts ( pUns, leaf ) ;
  count_uns_bndFaces ( pUns ) ;


 return ( 1 ) ;
}


/******************************************************************************

  write_adf_sol:
  .
  
  Last update:
  ------------
  25Apr02; derived from write_adf_sol_hydra.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_sol ( uns_s *pUns, const double lvl_id ) {

  const int mVerts = pUns->mVertsNumbered, ovwrt = 1 ;
  const char *pVarC ;
  double *pSv, *pSolVar, nm_id, fi_id ;
  int nBeg, nEnd, isNew, mUnknowns, maxUnkn = 0, mBeg = 0, sumUnkn = 0 ;
  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  varType_s *pVar ;

  /* .AVBP -> adf conversion. Create the linked list of variable categories. */


  if ( ( pUns->restart.avbp.neqs || pUns->restart.avbp.nreac ||
         pUns->restart.avbp.neqt || pUns->restart.avbp.nadd ) &&
       !pUns->varTypeS.pNxtVar )
    varType_avbp2adf ( pUns ) ;
  
  /* Maximum field size and allocate the max size needed for any field. */
  for ( pVar = &pUns->varTypeS ; pVar ; pVar = pVar->pNxtVar ) {
    maxUnkn = MAX( maxUnkn, pVar->mUnknFlow ) ;
    sumUnkn += pVar->mUnknFlow ;
  }

  pSolVar = arr_malloc ( "pSolVar in write_adf_sol", pUns->pFam, 
                         mVerts*maxUnkn, sizeof( double ) ) ;

  /* Create headers. */
  nm_id = adf_create ( lvl_id, "solnames", &isNew ) ;
  fi_id = adf_create ( lvl_id, "fields",   &isNew ) ;

  for ( pVar = &pUns->varTypeS ; pVar ; pVar = pVar->pNxtVar ) {
    mUnknowns = pVar->mUnknFlow ;
    pVarC = varCat[pVar->category] ;

    /* Variable names. Botch! Can't be bothered to fix this. After
       changing the def of pVarName to an array, there should be code here
       to concatenate the varnames. Now only the first gets written. */
    if ( pVar->pVarName[0][0] )
      adf_write2 ( nm_id, pVarC, "C1", LEN_VARNAME, mUnknowns,
                   (char*) pVar->pVarName[0], ovwrt, &isNew ) ;
    
    
    /* Pack the solution variables. */
    pChunk = NULL ;
    pSv = pSolVar ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number ) {
          vec_copy_dbl ( pVx->Punknown+mBeg, mUnknowns, pSv ) ;
          pSv += mUnknowns ;
        }
  
    if ( mVerts * mUnknowns != pSv-pSolVar ) {
      printf ( " FATAL: for %s expected %d (=%d*%d) unknowns,"
               " found %d in write_adf_sol.\n",
               pVarC, mVerts*mUnknowns, mVerts, mUnknowns,
               (int)(pSv-pSolVar) ) ; return ( 0 ) ; }
    
  
    adf_write2 ( fi_id, pVarC, "R8", mUnknowns, mVerts, (char*) pSolVar,
                 ovwrt, &isNew ) ;
    mBeg += mUnknowns ;
  }
  
  if ( mBeg != sumUnkn ) {
    printf ( " FATAL: expected %d unknowns per node, found %d in write_adf_sol.\n",
             sumUnkn, mBeg ) ;
             return ( 0 ) ; }
  
  arr_free ( pSolVar ) ;
  
  return ( 1 ) ;
}


/******************************************************************************

  write_adf_sol_hydra:
  .
  
  Last update:
  ------------
  29Oct99; change the solution file to a horizontal structure, 'flow' keyword.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_adf_sol_hydra ( uns_s *pUns, const double lvl_id,
                           chgSets_s *pChgSets ) {

  const int mUnknowns = pUns->varTypeS.mUnknowns, mVerts = pUns->mVertsNumbered ;
  double *pSv, *pSolVar ;
  int nBeg, nEnd, isNew ;
  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;

  
  pSv = pSolVar = arr_malloc ( "pSolVar in write_adf_sol", pUns->pFam, 
                               mVerts*mUnknowns, sizeof( double ) ) ;
  
  /* Pack the solution variables. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        vec_copy_dbl ( pVx->Punknown, mUnknowns, pSv ) ;
        pSv += mUnknowns ;
      }

  
  if ( mVerts * mUnknowns != pSv-pSolVar ) {
    printf ( " FATAL: expected %d (=%d*%d) unknowns, found %d in write_adf_sol.\n",
             mUnknowns, mVerts, pUns->mVertsNumbered * mUnknowns,
             (int)(pSv-pSolVar) ) ; return ( 0 ) ; }

  
  adf_write2 ( lvl_id, "flow", "R8", mUnknowns, mVerts, (char*) pSolVar,
               pChgSets->verts, &isNew ) ;
  arr_free ( pSolVar ) ;
  
  return ( 1 ) ;
}


/******************************************************************************

  _:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static char* mikeg_encode ( const char groupName[], const char groupType ) {

  static char mNm[ADF_SURFLBL_LEN+1] ;
  char *pCh ;
  int len ;

  strncpy ( mNm, groupName, ADF_SURFLBL_LEN-1) ;
  len = strlen( groupName ) ;

  /* Pad the string with blanks. */
  for ( pCh = mNm+len ; pCh < mNm+ADF_SURFLBL_LEN-1 ; pCh++ )
    *pCh = ' ' ;

  mNm[ADF_SURFLBL_LEN-1] = groupType ;
  mNm[ADF_SURFLBL_LEN] = '\0' ;

  return ( mNm ) ;
}
 
