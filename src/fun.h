/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : 

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>


  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s fun_name (  ) {
#undef FUNLOC
#define FUNLOC "in fun_name"
  
  ret_s ret = ret_success () ;

  return ( ret ) ;
}
