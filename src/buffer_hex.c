/*
   buffer_hex.c:
   Buffer a hex. This is a visual routine, converted to c via f2c and post-editing.

   Last update:
   ------------


   This file contains:
   -------------------
   buffer_hex
   rota
   spin

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern const int bitEdge[MAX_EDGES_ELEM] ;
extern const refType_struct bufferType[MAX_FACES_ELEM*MAX_CHILDS_FACE+1] ;


/***********************************************************************
 Spin the cell by 90 degrees around the y axis:
*/  
void spin ( int kVxRot[] ) {
  static int isave;

  /* Parameter adjustments */
  --kVxRot;

  /* Function Body */
  isave = kVxRot[4] ;
  kVxRot[4] = kVxRot[3] ;
  kVxRot[3] = kVxRot[2] ;
  kVxRot[2] = kVxRot[1] ;
  kVxRot[1] = isave ;
  isave = kVxRot[8] ;
  kVxRot[8] = kVxRot[7] ;
  kVxRot[7] = kVxRot[6] ;
  kVxRot[6] = kVxRot[5] ;
  kVxRot[5] = isave ;
  isave = kVxRot[12] ;
  kVxRot[12] = kVxRot[11] ;
  kVxRot[11] = kVxRot[10] ;
  kVxRot[10] = kVxRot[14] ;
  kVxRot[14] = isave ;
  isave = kVxRot[18] ;
  kVxRot[18] = kVxRot[17] ;
  kVxRot[17] = kVxRot[16] ;
  kVxRot[16] = kVxRot[15] ;
  kVxRot[15] = isave ;
  isave = kVxRot[22] ;
  kVxRot[22] = kVxRot[19] ;
  kVxRot[19] = kVxRot[21] ;
  kVxRot[21] = kVxRot[25] ;
  kVxRot[25] = isave ;
  isave = kVxRot[24] ;
  kVxRot[24] = kVxRot[23] ;
  kVxRot[23] = kVxRot[20] ;
  kVxRot[20] = kVxRot[26] ;
  kVxRot[26] = isave;

  return ;
}


/**********************************************************************
 Rotate the kVxRot by 90 degrees around the x axis:
*/
void rota ( int kVxRot[] ) {
  static int isave;

  /* Parameter adjustments */
  --kVxRot;

  /* Function Body */
  isave = kVxRot[3] ;
  kVxRot[3] = kVxRot[7] ;
  kVxRot[7] = kVxRot[6] ;
  kVxRot[6] = kVxRot[2] ;
  kVxRot[2] = isave ;
  isave = kVxRot[4] ;
  kVxRot[4] = kVxRot[8] ;
  kVxRot[8] = kVxRot[5] ;
  kVxRot[5] = kVxRot[1] ;
  kVxRot[1] = isave;
  isave = kVxRot[9] ;
  kVxRot[9] = kVxRot[11] ;
  kVxRot[11] = kVxRot[13] ;
  kVxRot[13] = kVxRot[14] ;
  kVxRot[14] = isave ;
  isave = kVxRot[17] ;
  kVxRot[17] = kVxRot[23] ;
  kVxRot[23] = kVxRot[26] ;
  kVxRot[26] = kVxRot[15] ;
  kVxRot[15] = isave;
  isave = kVxRot[16] ;
  kVxRot[16] = kVxRot[19] ;
  kVxRot[19] = kVxRot[20] ;
  kVxRot[20] = kVxRot[21] ;
  kVxRot[21] = isave ;
  isave = kVxRot[18] ;
  kVxRot[18] = kVxRot[22] ;
  kVxRot[22] = kVxRot[24] ;
  kVxRot[24] = kVxRot[25] ;
  kVxRot[25] = isave ;

  return ;
}



/******************************************************************************

  buffer_hex:
  Buffer a hex.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns: grid
  pElem: element to buffer
  mVxHg # of hanging vertices on the element
  kVxHag: position of these hanging nodes
  PvxElem: 
  PsurfTri: surface triangulation for this element.
  doCheckVol: if non-zero, check children for positive volumes

  Changes To:
  -----------
  PchildSpc: pointers to elem, vx, storage for the children elements


  Output:
  -----------
  *pmNegVol: non-zero if buffering produces neg. vols, zero otherw.
  *pDoesAddCtrVx: non-zero if buffering adds a new central vx.

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int buffer_hex( uns_s *pUns, elem_struct *Pelem,
	        const int mVxHg, const int kVxHg[], vrtx_struct *PvxPrt[],
	        const surfTri_s *PsurfTri, childSpc_s *PchildSpc,
                const int doCheckVol,
                int *pmNegVol, int *pDoesAddCtrVx ) {
  
  /* Corner and hanging node numbers for each face, omitting 0. */
  const int index[7][9] = { { 0,  0,  0,  0,  0,  0,  0,  0,  0 },
			    { 1,  2,  3,  4, 15, 16, 17, 18,  9 },
			    { 3,  2,  6,  7, 16, 21, 20, 19, 10 },
			    { 4,  3,  7,  8, 17, 19, 23, 22, 11 },
			    { 4,  8,  5,  1, 22, 24, 25, 18, 12 },
			    { 5,  8,  7,  6, 24, 23, 20, 26, 13 },
			    { 2,  1,  5,  6, 15, 25, 26, 21, 14 }
  } ;
  /* Translate the edge number [0..mE-1] of each hanging vertex into its pos in cell. */
  const int edgeInCell[12] = { 15, 18, 25, 16, 21, 17,
			       19, 22, 26, 24, 20, 23 } ;
  /* Translate the face number [1..mF] idem. */
  const int faceInCell[7] = { 0, 14, 10, 11, 12, 9, 13 } ;

  /* Translate avbp face numbers to visual. */
  const int a2vFace[7] = { 0, 6, 2, 3, 4, 1, 5 } ;

  static int kVx, kFace, iVx, mHgVertsFace, isave,
    faceType[7], mFacesType1, mFacesType6,
    mFaceTypesTotal, if1, if2, fcVx[9], kVxRot[MAX_REFINED_VERTS],
    kVxCh[MAX_VX_ELEM], visFace ;
  static const elemType_struct *PelT ;
  static int mV, mE, mF ;
  PelT = elemType+Pelem->elType ;
  mV = PelT->mVerts, mE = PelT->mEdges, mF = PelT->mFaces ;

  *pmNegVol = *pDoesAddCtrVx = 0 ;

  /********************************************************************** 
    Routine to create hybrid grid within a buffer cell for a
    hexahedral adapted grid.
    
    Node numbers for hexahedral cells:
    

                            ^ y
                            |
                            |  Spin ->
                            |

 		       8------23-------7
 		      /:      /       /|
 		    24------13------20 |
 		    /: :    / :     /| |
 		   5------26-------6 | |
 		   | : :   |  :    | | |            ^
 		   | : 22..|.11....|.|19     Rotate |
 		   | : /   |  :    | |/|
 		   | 12:   |  :    |10 |   ----------->  x
 		   | / :   |  :    |/| |
 		  25------14------21 | |
 		   | : :   |  :    | | |
 		   | : 4...|.17....|.|.3
 		   | :/    |  /    | |/
 		   |18-----|-9-----|16
 		   |/      |/      |/
 		   1------15-------2



     where:	cell  -	node index for the above nodes
 			(0 indicates no node for 9-26)

     Copyright 1991, Massachusetts Institute of Technology.
     Improved & debugged M. Rudgyard, CERFACS December 1993
                                              January  1996
     Translated into c Jens-Dominik Mueller, CERFACS, June 1997.
  *********************************************************************/

  if ( !mVxHg )
    return ( 1 ) ;

  /* Increment the number of children in add_child_3D_kVx, */
  Pelem->PrefType = bufferType ;
  
  /* Fill a complete table of all vertices on the element. Store the position+1
     of the vertex in PvxPrt in kVxRot. */
  for ( kVx = 0 ; kVx < mV ; kVx++ )
    kVxRot[kVx] = kVx+1 ;
  for ( kVx = mV ; kVx < MAX_REFINED_VERTS ; kVx++ )
    kVxRot[kVx] = 0 ;
  for ( iVx = 0 ; iVx < mVxHg ; iVx++ ) {
    kVx = kVxHg[iVx] ;
    if ( kVx < mV ) {
      sprintf ( hip_msg, "hanging vertex %d in corner %d in buffer_hex.", iVx, kVx ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( kVx < mV + mE )
      /* edgeInCell and faceInCell are taken from the graph, need a decrement -1,
	 since kVxRot starts at 0. */
      kVxRot[ edgeInCell[ kVx-mV ]-1 ] = mV+iVx+1 ;
    else if ( kVx <= mV + mE + mF )
      kVxRot[ faceInCell[ kVx-mV-mE ]-1 ] = mV+iVx+1 ;
    else {
      printf ( hip_msg, "hanging vertex %d in impossible position %d in buffer_hex.",
	       iVx, kVx ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  
  /* Define face types: */
  mFaceTypesTotal =  mFacesType1 = mFacesType6 = 0 ;
  elem_struct *pElCp ;
  for ( kFace = 1 ; kFace <= 6 ; ++kFace )  {
    /* Label faces with following generic types:

     +---#---+    +---#---+   +---#---+
     |       |    |       |   |       |
     |       |    |       |   |       #
     |       |    |       |   |       |
     +-------+    +---#---+   +-------+
    				     
      Type  1      Type  2     Type  3 
    				     
     +---#---+    +---#---+   +---#---+
     |       |    |       |   |       |
     |       #    #       #   #   #   #
     |       |    |       |   |       |
     +---#---+    +---#---+   +---#---+
    				     
      Type  4      Type  5     Type  6
    */

    /* The face number in visual's numbering. */
    visFace = a2vFace[kFace] ;
    
    for ( iVx = 0 ; iVx < 9 ; ++iVx )
      /* Index is based on the array starting at 1. Decrement. */
      fcVx[iVx] = kVxRot[ index[visFace][iVx]-1 ] ;
    for ( mHgVertsFace = 0, iVx = 4 ; iVx < 9; ++iVx )
      if ( fcVx[iVx] )
	++mHgVertsFace ;

    faceType[visFace] = mHgVertsFace ;
    if ( mHgVertsFace > 2 )
      faceType[visFace] = mHgVertsFace + 1 ;

    if ( mHgVertsFace == 2 ) {
      if ( fcVx[4] && fcVx[6] )
	faceType[visFace] = 2 ;
      else if ( fcVx[5] && fcVx[7] )
	faceType[visFace] = 2 ;
      else
	faceType[visFace] = 3 ;
    }
    if ( faceType[visFace] == 1 )
      ++mFacesType1 ;
    if (faceType[visFace] == 6)
      ++mFacesType6 ;
    mFaceTypesTotal += faceType[visFace] ;
  }

  /* The case of no buffering has already been handled. */
  double elVol, elVolBuf = 0 ;
  elem_struct *pChElem ;
  if ( doCheckVol ) elVol = get_elem_vol ( Pelem ) ;
  if ( mFaceTypesTotal == 2 )  {
    /* Node on one edge only: */

    /* Rotate and spin until mid-edge node is in position 2 (front top edge refined). */
    if ( kVxRot[20] || kVxRot[18] || kVxRot[21] || kVxRot[24] ) 
      rota( kVxRot ) ;
    if ( kVxRot[15] || kVxRot[19] || kVxRot[23] || kVxRot[17] ) 
      spin( kVxRot ) ;
    while( kVxRot[25] == 0 )
      rota( kVxRot ) ;

    /* Define 4 new pyramids: */
    kVxCh[0] = kVxRot[0]-1 ;
    kVxCh[1] = kVxRot[1]-1 ;
    kVxCh[2] = kVxRot[2]-1 ;
    kVxCh[3] = kVxRot[3]-1 ;
    kVxCh[4] = kVxRot[25]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[1]-1 ;
    kVxCh[1] = kVxRot[5]-1 ;
    kVxCh[2] = kVxRot[6]-1 ;
    kVxCh[3] = kVxRot[2]-1 ;
    kVxCh[4] = kVxRot[25]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[3]-1 ;
    kVxCh[1] = kVxRot[2]-1 ;
    kVxCh[2] = kVxRot[6]-1 ;
    kVxCh[3] = kVxRot[7]-1 ;
    kVxCh[4] = kVxRot[25]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[0]-1 ;
    kVxCh[1] = kVxRot[3]-1 ;
    kVxCh[2] = kVxRot[7]-1 ;
    kVxCh[3] = kVxRot[4]-1 ;
    kVxCh[4] = kVxRot[25]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
  }

  
  else if ( mFaceTypesTotal == 10 && mFacesType6 == 1 ) {
    /* One homogeneously refined face: */
    /* Rotate and spin until mid-face node is in position 14 */
    /* (refined face at front): */
    if ( kVxRot[9] || kVxRot[11] )
      spin( kVxRot ) ;
    while( !kVxRot[13] )
      rota( kVxRot ) ;

    /* Define 4 new tetrahedra */
    kVxCh[0] = kVxRot[3]-1 ;
    kVxCh[1] = kVxRot[2]-1 ;
    kVxCh[2] = kVxRot[14]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[2]-1 ;
    kVxCh[1] = kVxRot[6]-1 ;
    kVxCh[2] = kVxRot[20]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[6]-1 ;
    kVxCh[1] = kVxRot[7]-1 ;
    kVxCh[2] = kVxRot[25]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[3]-1 ;
    kVxCh[1] = kVxRot[24]-1 ;
    kVxCh[2] = kVxRot[7]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    /* and 5 new pyramids: */
    kVxCh[0] = kVxRot[0]-1 ;
    kVxCh[1] = kVxRot[24]-1 ;
    kVxCh[2] = kVxRot[13]-1 ;
    kVxCh[3] = kVxRot[14]-1 ;
    kVxCh[4] = kVxRot[3]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[1]-1 ;
    kVxCh[1] = kVxRot[14]-1 ;
    kVxCh[2] = kVxRot[13]-1 ;
    kVxCh[3] = kVxRot[20]-1 ;
    kVxCh[4] = kVxRot[2]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[13]-1 ;
    kVxCh[1] = kVxRot[25]-1 ;
    kVxCh[2] = kVxRot[5]-1 ;
    kVxCh[3] = kVxRot[20]-1 ;
    kVxCh[4] = kVxRot[6]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[24]-1 ;
    kVxCh[1] = kVxRot[4]-1 ;
    kVxCh[2] = kVxRot[25]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    kVxCh[4] = kVxRot[7]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[3]-1 ;
    kVxCh[1] = kVxRot[2]-1 ;
    kVxCh[2] = kVxRot[6]-1 ;
    kVxCh[3] = kVxRot[7]-1 ;
    kVxCh[4] = kVxRot[13]-1 ;
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
  }

  
  else if ( mFaceTypesTotal == 20 && mFacesType6 == 2 && mFacesType1 == 0 )
  { /* Two homogeneously refined opposite faces: */
    /* Rotate and spin until one of mid-face nodes is in position 14 
       (one of refined face at front): */
    if ( kVxRot[9] || kVxRot[11] )
      spin( kVxRot ) ;
    while( !kVxRot[13] )
      rota( kVxRot ) ;

    /* Define 4 new hexahedra. */
    kVxCh[0] = kVxRot[0]-1 ;
    kVxCh[1] = kVxRot[14]-1 ;
    kVxCh[2] = kVxRot[16]-1 ;
    kVxCh[3] = kVxRot[3]-1 ;
    kVxCh[4] = kVxRot[24]-1 ;
    kVxCh[5] = kVxRot[13]-1 ;
    kVxCh[6] = kVxRot[10]-1 ;
    kVxCh[7] = kVxRot[21]-1 ;
    pChElem = add_child_3D_kVx ( hex, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[14]-1 ;
    kVxCh[1] = kVxRot[1]-1 ;
    kVxCh[2] = kVxRot[2]-1 ;
    kVxCh[3] = kVxRot[16]-1 ;
    kVxCh[4] = kVxRot[13]-1 ;
    kVxCh[5] = kVxRot[20]-1 ;
    kVxCh[6] = kVxRot[18]-1 ;
    kVxCh[7] = kVxRot[10]-1 ;
    pChElem = add_child_3D_kVx ( hex, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[13]-1 ;
    kVxCh[1] = kVxRot[20]-1 ;
    kVxCh[2] = kVxRot[18]-1 ;
    kVxCh[3] = kVxRot[10]-1 ;
    kVxCh[4] = kVxRot[25]-1 ;
    kVxCh[5] = kVxRot[5]-1 ;
    kVxCh[6] = kVxRot[6]-1 ;
    kVxCh[7] = kVxRot[22]-1 ;
    pChElem = add_child_3D_kVx ( hex, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[24]-1 ;
    kVxCh[1] = kVxRot[13]-1 ;
    kVxCh[2] = kVxRot[10]-1 ;
    kVxCh[3] = kVxRot[21]-1 ;
    kVxCh[4] = kVxRot[4]-1 ;
    kVxCh[5] = kVxRot[25]-1 ;
    kVxCh[6] = kVxRot[22]-1 ;
    kVxCh[7] = kVxRot[7]-1 ;
    pChElem = add_child_3D_kVx ( hex, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
  }


  else if ( mFaceTypesTotal == 20 && mFacesType6 == 2 && mFacesType1 == 2 )
  { /* Two homogeneously refined adjacent faces: */
    /* Define the two refined faces (if1 & if2): */

    if1 = 0;
    if2 = 0;
    for ( visFace = 1 ; visFace <= 6 ; ++visFace )
      if ( faceType[visFace] == 6 ) {
	if ( if1 )
	  if2 = visFace ;
	else 
	  if1 = visFace ;
      }

    /* Mark the mid--edge node common to both refined faces: */
    if (if1 == 1) {
      iVx = if2 + 14;
      if (if2 == 6)
	iVx = 15 ; }
    else if (if1 == 2) {
      iVx = if2 + 15;
      if (if2 == 3)
	iVx = 19 ; }
    else if (if1 == 3)
      iVx = if2 + 18;
    else if (if1 == 4)
      iVx = if2 + 19;
    else
      iVx = 26;
    isave = kVxRot[iVx-1] ;
    kVxRot[iVx-1] = -9999999;

    /* Rotate and spin until the common node is in position 26 */
    /* --- the refined faces are then at the front and on top: */
    if ( kVxRot[20] == -9999999 || kVxRot[18] == -9999999 ||
	 kVxRot[21] == -9999999 || kVxRot[24] == -9999999 )
      rota( kVxRot ) ;
    if ( kVxRot[15] == -9999999 || kVxRot[19] == -9999999 ||
	 kVxRot[23] == -9999999 || kVxRot[17] == -9999999 )
      spin( kVxRot ) ;
    while( kVxRot[25] != -9999999 )
      rota( kVxRot ) ;
    kVxRot[25] = isave ;

    /* Define 3 new tetrahedra, */
    kVxCh[0] = kVxRot[3]-1 ; 
    kVxCh[1] = kVxRot[2]-1 ; 
    kVxCh[2] = kVxRot[14]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[3]-1 ; 
    kVxCh[1] = kVxRot[2]-1 ; 
    kVxCh[2] = kVxRot[13]-1 ;
    kVxCh[3] = kVxRot[12]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[2]-1 ; 
    kVxCh[1] = kVxRot[3]-1 ; 
    kVxCh[2] = kVxRot[22]-1 ;
    kVxCh[3] = kVxRot[12]-1 ;
    pChElem = add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    /* 6 pyramids */
    kVxCh[0] = kVxRot[0]-1 ; 
    kVxCh[1] = kVxRot[24]-1 ;
    kVxCh[2] = kVxRot[13]-1 ;
    kVxCh[3] = kVxRot[14]-1 ;
    kVxCh[4] = kVxRot[3]-1 ; 
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[14]-1 ;
    kVxCh[1] = kVxRot[13]-1 ;
    kVxCh[2] = kVxRot[20]-1 ;
    kVxCh[3] = kVxRot[1]-1 ; 
    kVxCh[4] = kVxRot[2]-1 ; 
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[24]-1 ;
    kVxCh[1] = kVxRot[23]-1 ;
    kVxCh[2] = kVxRot[12]-1 ;
    kVxCh[3] = kVxRot[13]-1 ;
    kVxCh[4] = kVxRot[3]-1 ; 
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[13]-1 ;
    kVxCh[1] = kVxRot[12]-1 ;
    kVxCh[2] = kVxRot[19]-1 ;
    kVxCh[3] = kVxRot[20]-1 ;
    kVxCh[4] = kVxRot[2]-1 ; 
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[23]-1 ; 
    kVxCh[1] = kVxRot[7]-1 ;  
    kVxCh[2] = kVxRot[22]-1 ; 
    kVxCh[3] = kVxRot[12]-1 ; 
    kVxCh[4] = kVxRot[3]-1 ;  
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    kVxCh[0] = kVxRot[12]-1 ;
    kVxCh[1] = kVxRot[22]-1 ;
    kVxCh[2] = kVxRot[6]-1 ; 
    kVxCh[3] = kVxRot[19]-1 ;
    kVxCh[4] = kVxRot[2]-1 ; 
    pChElem = add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
    
    /* and 2 prisms : */
    kVxCh[0] = kVxRot[24]-1 ;
    kVxCh[1] = kVxRot[13]-1 ;
    kVxCh[2] = kVxRot[12]-1 ;
    kVxCh[3] = kVxRot[23]-1 ;
    kVxCh[4] = kVxRot[25]-1 ;
    kVxCh[5] = kVxRot[4]-1 ; 
    pChElem = add_child_3D_kVx ( pri, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;

    kVxCh[0] = kVxRot[13]-1 ;
    kVxCh[1] = kVxRot[20]-1 ;
    kVxCh[2] = kVxRot[19]-1 ;
    kVxCh[3] = kVxRot[12]-1 ;
    kVxCh[4] = kVxRot[5]-1 ; 
    kVxCh[5] = kVxRot[25]-1 ;
    pChElem = add_child_3D_kVx ( pri, PchildSpc, Pelem, PvxPrt, kVxCh, doCheckVol, pmNegVol ) ;
    if ( doCheckVol && pChElem ) elVolBuf += get_elem_vol ( pChElem ) ;
  }
  else  {
    /* In the remaining cases, add a center vertex and connect all
       exterior faces of the element. */
    *pDoesAddCtrVx = 1 ;
    if ( !add_center_3D ( pUns, Pelem, PvxPrt, PsurfTri, PchildSpc, doCheckVol, pmNegVol ) )    {
      sprintf ( hip_msg,"could not center-buffer element in buffer_hex." ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  return ( 1 ) ;
}
