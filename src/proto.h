/*
  proto.h:
  General functions.

  Last update:
  ------------
  26Aug24, intro ret_failure, ret_is_*
  24Aug24, move uns_specialTopo functions to proto_uns.
  5Sep19; rename init to hip_init.
          add check_valid, license.
          intro ret_s for _menu calls.
  15jul19; intro isMatch_geoType.
  11Jul19; rename find_nVar to find_kVar, to clarify meaning of return value.
  2Apr19; intro find_nVar.
  20mar18; move hcg_util to its own file with knowledge of cgns types.
  11Nov17; intro vec_len_dbl_sq.
  11Nov17; intro set_current_grid
  30jun17; intro find_grid_numbered.
  19Mar17; intro vec_avg_4dbl_3d.
  18Dec16; intro pack_int_list.
  16Feb16; intro mat_vec_dbl.
  5Jan15; remove bcOrderCompare (already in proto_uns.h), 
          intro ceil_bsearch.
  27Jun14; new version of next_vec_var.
  23Feb14; intro fscanf_str_line.
  28Aug11; intro next_vec_var.
  18Jul11; intro isNumRange
  3Jul10; remove adf protos, 
  17Sep09; intro set_var_flag_range.
  27May09; intro find_flow_vector.
  4Apr09; rename check_varType into the more appropriate check_varNames ;
  13Dec08; drop nan def, collides with some intrinsics fns.
           intro specialTopoString.
  18May08; change interface to fidx2lidx.
  17May06; intro set_bc_e.
  4Feb4; intro datxb.
  1Sep3; intro check_varType ;
  18Dec2; intro ipow, daxb.
  12Sep2; new interface to make_heap.
  25Oct00; intro get_var.
  22Sep00; intro adf_wrappers.
  
*/

/* bye.c */
ret_s bye () ;

/* check_valid.c */
ret_s license(void) ;
ret_s check_valid_license ( void  ) ;

/* heap.c */
heap_s *make_heap ( int mData, int realloc, const ulong_t sizeofData, arrFam_s *pFam,
		    int (* cmpFun) ( const void *pData0, const void *pData1 ) ) ;
void free_heap ( heap_s **ppHeap ) ;
int empty_heap ( heap_s *pHeap ) ;
int add_heap ( heap_s *pHeap, const void *pUserData ) ;
int query_heap ( const heap_s *pHeap, void *pData ) ;
int get_heap ( heap_s *pHeap, void *pUserData, const int realloc ) ;

/* help.c */
ret_s help_menu ();

/* hip.c */
ret_s version_banner () ;
//ret_s read_transf_op ( transf_e *pTr_op, double dval[MAX_DIM] ) ;
ret_s read_transf_op ( transf_e *pTr_op, double *dval ) ;

/* listNprint.c */
ret_s list_grids () ;
ret_s list_surfaces ( char *keyword ) ;



/* meth.c */
ret_s ret_success () ;
ret_s ret_failure ( const char *pFailMsg ) ;
int ret_is_success ( const ret_s ret ) ;
int ret_is_failure ( const ret_s ret ) ;


ret_s hip_err ( hip_stat_e status, int printLvl, const char *str ) ;
void hprintf( char *fmt, ...)  ;
void flush_hip_out_buf () ;

void hip_check_count ( const int iExpected, const int iFound,
                        char *dataName, char *funName ) ;

int pack_ulg_list ( ulong_t *nEnt, int mEnt ) ;
int pack_int_list ( int *nEnt, int mEnt ) ;


void aidx2lidx ( ulong_t *aidx, const ulong_t mKeys, ulong_t *lidx ) ;
void ufidx2lidx ( ulong_t *fidx, const ulong_t mKeys, ulong_t *lidx ) ;
void ulidx2fidx ( ulong_t *lidx, const ulong_t mKeys, ulong_t *fidx ) ;
void ifidx2lidx ( int *fidx, const int mKeys, int *lidx ) ;
void ilidx2fidx ( int *lidx, const int mKeys, int *fidx ) ;


grid_struct *find_grid ( const char *nameOrNr, grid_type_enum type ) ;
grid_struct *find_grid_numbered ( int nr, grid_type_enum type ) ;
grid_struct *make_grid () ;

int add_hrb ( hrbs_s *pHrbs, const double ll[MAX_DIM], const double ur[MAX_DIM],
              const double radius, const int mDim ) ;

spec_bc_e set_bc_e ( bc_struct *pBc ) ;
int is_int ( const char *pStr ) ;
int is_float ( const char *pStr ) ;
int expr_is_text ( const char *expr ) ;
bc_struct *find_bc( const char *string, int mode ) ;

int isMatch_geoType ( bcGeoType_e geoType1, bcGeoType_e geoType2 ) ;
void geoType2Char ( bcGeoType_e geoType, char *pStr ) ;
bcGeoType_e char2geoType ( char gChar ) ;

void print_bc ( const bc_struct *Pbc, double *pBndPatchArea ) ;

double ipow ( double d, int n ) ;
void daxb ( double **A, int m, int n, double x[], double b[] ) ;
void datxb ( double **A, int m, int n, double x[], double b[] ) ;


int overlap_dbl ( const double *Pll1, const double *Pur1,
		  const double *Pll2, const double *Pur2,
		  const int mDim ) ;
double sq_distance_dbl ( const double *Pcoor1, const double *Pcoor2, const int mDim ) ;
double sq_distance_axis ( const double *pCoor, 
                          const specialTopo_e axis, const int mDim ) ;
double vec_len_dbl_sq ( double *pCoor, const int mDim ) ;
double vec_len_dbl ( double *pCoor, const int mDim ) ;
double vec_norm_dbl ( double *pCoor, const int mDim ) ;
void vec_add_dbl ( const double *Px, const double *Py, const int mDim, double *Pz ) ;
void vec_add_mult_dbl ( const double *Px, const double a, const double *Py,
                        const int mDim, double *Pz ) ;
void vec_diff_dbl ( const double *Px, const double *Py, const int mDim, double *Pz ) ;

void cross_prod_dbl ( const double *Px, const double *Py, const int mDim, double *Pz ) ;
double scal_prod_dbl ( const double *Px, const double *Py, const int mDim ) ;
double dih_angle ( double *n1, double len1, double *n2, double len2, double *eg ) ;

void vec_mult_dbl ( double *Px, const double mult, const int mDim ) ;
void vec_avg_dbl ( const double *Px, const double *Py, const int mDim, double *Pz ) ;
void vec_avg_4dbl_3d ( const double *P0, const double *P1,
                       const double *P2, const double *P3,
                       double *Pavg ) ;
void vec_avg2_dbl ( double *Px, double *Py, const int mDim ) ;
void vec_copy_dbl ( const double *Px, const int mDim, double *Pz ) ;
void vec_ini_dbl ( const double iniDbl, const int mDim, double *Pz ) ;
void rot_coor_dbl ( const double *pCoor, const double *pBase, const int mDim,
		    double *pNewCoor ) ;
void max_diff_vec_dbl ( const double *pVecA, const double *pVecB, const int mDim,
                        double *pVecMax ) ;
void vec_max_dbl ( const double *pVecA, const double *pVecB, const int mDim,
		   double *pVecMax ) ;
void vec_min_dbl ( const double *pVecA, const double *pVecB, const int mDim,
		   double *pVecMin ) ;
void mat_vec_dbl ( const double *pA, const int m, const int n, 
                   const double *px, double *py ) ;
int transpose_dbl ( const double *pMat, const int mDim, double *pMatT ) ;
void matmult_dbl ( const double *pA, const double *pB, const int mDim, double *pAB ) ;

int fscanf_end_line ( FILE *File ) ;
int fscanf_str_line ( FILE *File, int sz, char *str ) ;
void ftnString ( char* outString, const int len, const char* inString ) ;
void ftnString0 ( char* outString, const int len, const char* inString ) ;
void specchar2underscore ( char *string  ) ;
void blank2underscore ( char *string  ) ;
void trim(char *s) ;

char *prepend_path ( char *fileName ) ;
void strcpy_prepend_path ( char *fileNameWithPath, const char *fileName ) ;
int isNumRange ( const char rg[] ) ;

void tolowerstr ( char *p );

int cmp_char ( const void *pChar0, const void *pChar1 ) ;
int cmp_int ( const void *pInt0, const void *pInt1 ) ;
int cmp_ulong_t ( const void *pSzt0, const void *pSzt1 ) ;
int cmp_float ( const void *pFloat0, const void *pFloat1 ) ;
int cmp_double ( const void *pDouble0, const void *pDouble1 ) ;

unsigned int i32_pack4i ( int int3, int int2, int int1, int int0 ) ;
unsigned int i32_packNi ( int m, const int *pi ) ;
void i32_unpack4i ( int int32, int *pInt3, int *pInt2, int *pInt1, int *pInt0 ) ;

void *ceil_bsearch ( const void *key, const void *base,
                      size_t nmemb, size_t size,
                     int (*compar)(const void *, const void *)) ;

double det3 ( double mat[3][3] ) ;

void *bsearch_void(const void *key, const void *base, size_t num, size_t size,
                   int (*cmpFunVoid)( const void *, const void *, int *) ) ;

int nan_check ( double *pV, int mI, int mJ ) ;
/*int nan ( double a ) ;*/


/* set.c */

void hip_init () ;

// Extracted python-callable utility functions:
ret_s set_bc_text ( char *expr, char *bc_text ) ;
ret_s set_bc_type ( char *expr, char *bc_type_str ) ;

void set_prompt ( const grid_struct *pGrid ) ;
void set_current_pGrid ( grid_struct *pGrid ) ;
void set_current_grid_expr ( const char *expr ) ;

ret_s set_menu () ;
ret_s var_menu () ;

ret_s set_bc_text_arg (  ) ;
ret_s set_bc_type_arg (  ) ;
ret_s set_bc_mark_arg (  ) ;
ret_s set_bc_order_arg (  ) ;

ret_s set_hyvol (  ) ;


/* var.c: */
varCat_e get_varCat_from_grpName ( const char *grpName ) ;
int is_vec_from_cat_name ( const char thisCatName[], const char thisVarName[] ) ;
int next_vec_var ( const varList_s *pVL, const int mDim, varCat_e thisCat,
                   int *pkVar, int kVecVar[MAX_DIM] ) ;
void find_flow_vector ( const varList_s *pVL, int kFlo[MAX_DIM] ) ;
int check_var_name ( varList_s *pVarList, restart_u *pRestart, int mDim ) ;
void set_var_vec (varList_s *pVL, int kV, int isVec ) ;
int set_var_flag_range ( varList_s *pVL, int iFlag, char *range ) ;
void set_one_var_cat_name ( var_s *pVar, const int kEq,
                            const char *varName, const char *grpName ) ;
int set_var_name_cat_range ( varList_s *pVarList, char *keyword, char *range, char *name ) ;
void prim2cons( double primVar[], double consVar[], const int mDim ) ;
void cons2prim( double consVar[], double primVar[], const int mDim ) ;
void prim2para ( double primVar[], double paraVar[], const int mDim ) ;
void cons2para ( double consVar[], double paraVar[], const int mDim ) ;

int var2var ( const varType_e oldVarType, varType_e newVarType,
	      void (**convFun) ( double*, double*, const int ) ) ;

double get_mach_freestream ( const double freeStreamVar[], const int mDim ) ;
int get_freestream_mach ( double freeStreamVar[], const int mDim,
			  double Ma, double alpha, double theta ) ;

double get_var ( varList_s *pVarList, double *pUn, const char *var ) ;

int find_kVar ( const varList_s *pVL, int kVarPrev, const char *varNameExpr ) ;
