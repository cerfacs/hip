/*
 state_conv.c:
 Various conversion functions.
 
 Last update:
 ------------
 28Aug11; renamed var.c, intro next_vec_var.
 11Dec07; intro primT.
 2Sep03; intro set_var_name_cat_range.
 18Jun03; fix bug with including w in q in prim2cons.
 Oct00; intro get_var.
 May95; intro residual conversions.
 
*/
#include "cpre.h"

#include "proto.h"
#include "fnmatch.h"


extern const double Gamma ;
extern const double GammaM1 ;
extern const double R ;
extern const char varCatNames[][LEN_VAR_C] ;
extern char hip_msg[] ;

extern const char varCatNames[][LEN_VAR_C] ;
extern const char h5GrpNames[][LEN_GRPNAME] ;



/******************************************************************************

  h5r_varCat:
  Deduce a variable category from the h5 group name.
  
  Last update:
  ------------
  5Apr09: conceived.
  
  Input:
  ------
  grpName:

  
  Returns:
  --------
  the matching varCat, 'other' if there is no match.
  
*/

varCat_e get_varCat_from_grpName ( const char *grpName ) {

  varCat_e k ;

  for ( k=1 ; k<other ; k++ )
    if ( !strcmp( grpName, varCatNames[k] ) )
      return ( k ) ;

  return ( other ) ;
}


/******************************************************************************
  h5r_isVec:   */

/*! Given a variable category and name, return its vector component number.
 */

/*
  
  Last update:
  ------------
  29Aug11: conceived.
  

  Input:
  ------
  catName: variable cateogry name
  varName: variable name
    
  Returns:
  --------
  0 if scalar, 1..3 if vector component x..z
  
*/

int is_vec_from_cat_name ( const char thisCatName[], const char thisVarName[] ) {

#define mVCVec (2)
  const char catName[mVCVec][13] = { "GaseousPhase", "LiquidPhase" } ;
  const char varName[mVCVec][MAX_DIM][13] = {
    { "rhou", "rhov", "rhow" },
    { "alphalrholul", "alphalrholvl", "alphalrholwl" } } ;



  int kC ;
  for ( kC = 0 ; kC < mVCVec ; kC++ ) {
    if ( !strcmp( thisCatName, catName[kC] ) )
      break ;
  }
  if ( kC == mVCVec )
    /* Can't have vector components in this category. */
    return (0) ;



  int kV ;
  for ( kV = 0 ; kV < MAX_DIM ; kV++ )
    if ( !strcmp( thisVarName, varName[kC][kV] ) )
      /* This is a matching vector component. */
      return (kV+1) ;


  /* No match. */
  return ( 0 ) ;
}



/******************************************************************************
  next_vec_var:   */

/*! find the index (running from 0 ) of the next vector variable in varList.
 */

/*
  
  Last update:
  ------------
  27Jun14; rewrite using the vector numbers.
  23Mar14; allow vec vars in 'other'.
  29Aug11: conceived.
  

  Input:
  ------
  pVL =  the varList
  mDim = dims
  thisCat = if a cat is given, look only in this cat. 
  *pkVar = position of the x-comp of the previous vector variable.

  Changes To:
  -----------
  pKVar = on output the index of the x-comp of the current vector variable, -1 if there isn't any.
  kVecVar = positions of x,y,z vector comp corresponding to pkVar.
    
  Returns:
  --------
  1 if there is a vec var, 0 if there isn't.
  
*/

int next_vec_var ( const varList_s *pVL, const int mDim, varCat_e thisCat,
                   int *pkVar, int kVecVar[MAX_DIM] ) {

  /* Reset the comp. */
  int k ;
  for ( k = 0 ; k < mDim ; k++ )
    kVecVar[k] = -1 ;

  /* Find the next vector var if any. */
  int kV ;
  const var_s *pV ;
  int iVy=0, iVz=0 ;
  for ( kV = *pkVar+1 ; kV < pVL->mUnknowns ; kV++ ) {
      pV = pVL->var+kV ;

      if ( thisCat == noCat || pV->cat == thisCat ) {
        /* matching cat. */
        if ( pV->isVec && (pV->isVec)%mDim == 1 ) {
          /* Next vector vec. */
          kVecVar[0] = *pkVar = kV ;
          thisCat = pV->cat ;
          /* The isVec entries for the y and z components matching this x. */
          iVy = pV->isVec+1 ;
          iVz = pV->isVec+2 ;
          break ;
        }
      }
  }

  if ( kVecVar[0] == -1 ) 
    /* No match found. */
    return ( 0 ) ;
 
  else {
    /* Find the remaining components. No particular ordering is required, but
       they have to be in the same cat, and have the same modulo of mDim, i.e
       if x=1, then y=2, z=3. If x=4, y,z=5,6. */
    for ( kV = 1 ; kV < pVL->mUnknowns ; kV++ ) {
      pV = pVL->var+kV ;

      if ( pV->cat == thisCat ) {
        /* matching cat. */
        if ( pV->isVec == iVy )       kVecVar[1] = kV ;
        else if ( pV->isVec == iVz )  kVecVar[2] = kV ;
      }
    }
  

    /* Check whether all comp have been found. */
    for ( k = 0 ; k < mDim ; k++ )
      if ( kVecVar[k] == -1 ) {
        char vecChar[][2] = { "x", "y", "z" } ;
        char catChar[][4] = { "ns", "tpf" } ;
        sprintf ( hip_msg, "missing vector component %s for variable category"
                  " %s in next_vec_var.", vecChar[k], varCatNames[thisCat] ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

    return (1) ;
  }
}


/******************************************************************************

  find_flow_vector:
  Find the position of the flow vector (u,v,w) in the list of variables.
  
  Last update:
  ------------
  16Sep09; initialise kFlo[0:2] correctly.
  May09: conceived.
  
  Input:
  ------
  pVL: list of variables

  Output:
  -------
  kFlo: position of the u,v [and w] variables.
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

void find_flow_vector ( const varList_s *pVL, int kFlo[MAX_DIM] ) {

  int nD, nVa ;
  const var_s *pVar ;
  const char varNmCons[][5] = { "rhou", "rhov", "rhow" } ;
  const char varNmPrim[][2] = { "u", "v", "w" } ;
  /*   const char **pVNm ;  */
  const char *varNm[3] ;


  /* -1 indicates not found. */
  kFlo[0] = kFlo[1] = kFlo[2] = -1 ;

  /* Are we looking for conservative of primitive variable names? 
     Why can't we have a char ** ptr to a 2-dim char array? 
     pVNm = ( pVL->varType == cons ? varNmCons : varNmPrim ) ;
  */
  if ( pVL->varType == cons ) {
    varNm[0] = varNmCons[0] ;
    varNm[1] = varNmCons[1] ;
    varNm[2] = varNmCons[2] ;
  }
  else {
    varNm[0] = varNmPrim[0] ;
    varNm[1] = varNmPrim[1] ;
    varNm[2] = varNmPrim[2] ;
  }
    

  for ( nVa = 0 ; nVa < pVL->mUnknowns ; nVa++ ) {
    pVar = pVL->var + nVa ;
    if ( pVar->cat == ns )
      for ( nD = 0 ; nD < 3 ; nD++ )
        if ( !strcmp( pVar->name, varNm[nD] ) ) {
          if ( kFlo[nD] == -1 ){
            /* Found this var, first find. */
            kFlo[nD] = nVa ;
            break ;
          }
          else {
            sprintf ( hip_msg, " in find_flow_vector:\n"
                      "        duplication of flow vector variables %s and %s.\n",
                      pVL->var[kFlo[nD]].name, pVL->var[nVa].name ) ; 
            hip_err ( fatal, 0, hip_msg ) ;
          }
        }
  }


  for ( nD = 0 ; nD < 3 ; nD++ )
    if ( kFlo[nD] == -1 ) {
      /* Flow vector not identified. */
      sprintf ( hip_msg, " in find_flow_vector:\n"
                "        could not find flow vector for dim %d.\n",
                nD+1 ) ; 
      hip_err ( fatal, 0, hip_msg ) ;
    }


  return ;
}


/******************************************************************************

  check_var_name:
  Check whether all variables are named, if not, issue default varnames. 
  Also check whether vector variables are contiguous.
  
  Last update:
  ------------
  23Mar14; allow reading of solutions without flow variables.
           replace all blanks in var names with underscores.
  14Dec13; set var flag to 1 after reading.
  19Oct03; intro check on contiguous vector variables.
  18Sep09; when building a var name, append the number at the end.
           treat mEqu=0 correctly. 
  5Apr09; count variables by type.
  4Apr09; use new def of varList.
  4Oct06; intro kVarCat
  28Sep06: conceived.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns->varTypeS.varType
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int check_var_name ( varList_s *pVL, restart_u *pRestart, int mDim ) {

  const int mEqu = pVL->mUnknowns ;

  int mEqF=0, mEqS=0, nreac=0, neqfic=0, neqt=0, nadd=0, neq2pf=0, nPb=0, 
    nMean=0, nAddTpf=0, nOther=0, k ;
  char *name ;
  var_s *pVar ;

  if ( mEqu == 0 ) {
    /* There are no vars. That's fine for now. */
    return ( 1 ) ;
  }

  /* Replace blanks in all variable names with an underscore. */
  int len ;
  char *pCh ;
  for ( k = 0 ; k < mEqu ; k++ ) {
    pVar = pVL->var + k ;
    name = pVar->name ;
    
    len = strlen( name ) ;
    for ( pCh = name ; pCh < name+len ; pCh++ )
      if ( *pCh == ' ' )
        *pCh = '_' ;
  }


  if ( mEqu < mDim+2 ) {
    sprintf ( hip_msg, "in check_var_name:\n"
             "        looking for at least %d flow vars, found %d.\n", 
             mDim+2, mEqF ) ;
    //hip_err ( fatal, 0, hip_msg ) ; 
    hip_err ( warning, 1, hip_msg ) ;

    for ( k = 0 ; k < mEqu ; k++ ) {
      pVar = pVL->var + k ;
      pVar->cat = other ;
    }

    return ( 1 ) ; 
  }
    

  /* Loop over all equ, count types and assume they are in order. */
  for ( k = 0 ; k < mEqu ; k++ ) {
    pVar = pVL->var + k ;
    pVar->flag = 1 ;
    name = pVar->name ;

    switch ( pVar->cat ) {
    case ns :      ++mEqF ; break ;
    case species : ++mEqS ; break ;
    case rrates :  ++nreac ; break ;
    case tpf :     ++neq2pf ; break ;
    case rans :    ++neqt ; break ;
    case add :     ++nadd ; break ;
    case mean :    ++nMean ; break ;
    case fictive : ++neqfic ; break ;
    case add_tpf : ++nAddTpf ; break ;
    case other :   ++nOther ; break ;
    default :      ++nPb ; break ;
    }

    if ( name[0] == '\0' ) {
      /* No var name so far. Below we assume that flow vars come first. */

      if ( pVL->varType == cons && k < mEqF ) {
        /* conservative flow variables. */
        switch ( k ) {
        case 0 :      sprintf ( name, "rho"  ) ; break ;
        case 1 :      sprintf ( name, "rhou" ) ; break ;
        case 2 :      sprintf ( name, "rhov" ) ; break ;
        case 3 :      sprintf ( name, "rhoE" ) ; break ;
        }
        if ( mDim == 3 && k == 3 ) sprintf ( name, "rhow" ) ;
        if ( mDim == 3 && k == 4 ) sprintf ( name, "rhoE" ) ;
      }
      else if (  pVL->varType == prim && k < mEqF ) {
        /* primitive flow variables. */
        switch ( k ) {
        case 0 :      sprintf ( name, "rho" ) ; break ;
        case 1 :      sprintf ( name, "u"   ) ; break ;
        case 2 :      sprintf ( name, "v"   ) ; break ;
        case 3 :      sprintf ( name, "p"   ) ; break ;
        }
        if ( mDim == 3 && k == 3 ) sprintf ( name, "w" ) ;
        if ( mDim == 3 && k == 4 ) sprintf ( name, "p" ) ;
      }

      else if ( pVar->grp[0] ) {
        /* Build a name out of the group and number. */
        snprintf ( name, LEN_VARNAME, "%s_%d", pVar->grp, k+1 ) ;
      }

      else {
        /* No groupname, build a name out of the category and number. */
        snprintf ( name, LEN_VARNAME, "%s_%d", 
                   varCatNames[ (int) pVar->cat ], k+1 ) ;
      }
    }
  }


  int nDim ;
  /* Check that vector variables are contiguous. */
  for ( k = 0 ; k < mEqu ; k++ ) {
    pVar = pVL->var + k ;

    if ( pVar->isVec == 1 ) {
      /* This is a vector, make sure the next two are the other components. */
      for ( nDim = 1 ; nDim < mDim ; nDim++ ) {
        if (pVar[nDim].isVec != nDim+1 )
          break ;
      }
      if ( nDim != mDim ) {
        sprintf ( hip_msg, "components of vector variable with first component %s"
                  " are not contiguous\n", pVar->name ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  }


  /* Store the solution parameters for a restart. */
  pRestart->any.neqf   = mEqF ;
  pRestart->any.neqs   = mEqS ;
  pRestart->any.neqfic = neqfic ;
  pRestart->any.nreac  = nreac ;
  pRestart->any.neqt   = neqt  ;
  pRestart->any.nadd   = nadd  ;
  pRestart->any.neq2pf = neq2pf  ;
  pRestart->any.nadd_tpf = nAddTpf  ;
  pRestart->any.nother = nOther  ;


  return ( 1 ) ;
}

/******************************************************************************
  set_var_vec:   */

/*! Set the vector component to a variable.
 */

/*
  
  Last update:
  ------------
  3Sep12; fix bug with range test.
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void set_var_vec (varList_s *pVL, int kV, int isVec ) {

  int mUn = pVL->mUnknowns ;

  /* kV runs from 0. */
  kV-- ;

  if ( kV < 0 || kV >= mUn )
    hip_err ( warning, 0, "variable number out of range." ) ;

  var_s *pV = pVL->var + kV ;

  pV->isVec = isVec ;

  return ;
}



/******************************************************************************

  set_var_flag_range: 
  Modify variable flag
  
  Last update:
  ------------
  18Dec19; rename to _range to align with set_var_name_vec_range.
  4Apr09; use new def of varList.
  15Dec06; tidy up, drop repeat name block.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int set_var_flag_range ( varList_s *pVL, int iFlag, char *range ) {

  int mV = 1, mUn, kV ;

  /* Find the appropriate boundaries. */
  mUn = pVL->mUnknowns ;

  for ( kV = 0 ; kV < mUn ; kV++ ) 
    /* Note that mV contains the increment by one for a range of 1 ... mUnknowns. */
    if ( range[0] == '\0' || num_match( mV+kV, range ) ) {
        pVL->var[kV].flag = iFlag ;
    }

  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_one_var_cat_name:
*/
/*! set name, cat, group for one variable.
 *
 *
 */

/*
  
  Last update:
  ------------
  18Dec19: extracted from read_uns_hdf5.
  

  Input:
  ------
  kEq: # of eq to set
  varName: name of var
  grpName: one of the recognised group headings h5varCatNames. 

  Changes to:
  -----------
  pVar: var list

  
*/

void set_one_var_cat_name ( var_s *pVar, const int kEq,
                            const char *varName, const char *grpName ) {

  pVar[kEq].cat = get_varCat_from_grpName( grpName ) ;
  pVar[kEq].isVec = is_vec_from_cat_name ( grpName, varName ) ;
  pVar[kEq].flag = 1 ;
  strncpy ( pVar[kEq].name, varName, LEN_VARNAME ) ;
  strncpy ( pVar[kEq].grp, grpName, LEN_VARNAME ) ;

  return ;
}


/******************************************************************************

  set_var_name_cat_range: 
  Modify variable names and categories.
  
  Last update:
  ------------
  18Dec19; rename to update. Use set_ for initial def.
  22Feb19; intro 'hip' cat for internal/utility vars.
  3Sep12; fix bug with setting var names: move cat test inside if block.
  4Apr09; use new def of varList.
  15Dec06; tidy up, drop repeat name block.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int set_var_name_cat_range ( varList_s *pVL, char *keyword, char *range, char *name ) {

  int mV = 1, mUn, kV ;
  varCat_e vCat = noCat ;

  if ( keyword[0] == 'c' ) {
    /* Match the category to a pre-defined variable category. */
    if ( !strncmp ( name, "ns", 2 ) )
      vCat = ns;
    else if ( !strncmp ( name, "species", 2 ) )
      vCat = species ;
    else if ( !strncmp ( name, "rrates", 2 ) )
      vCat = rrates ;
    else if ( !strncmp ( name, "tpf", 2 ) )
      vCat = tpf ;
    else if ( !strncmp ( name, "rans", 2 ) )
      vCat = rans ;
    else if ( !strncmp ( name, "add", 2 ) )
      vCat = add ;
    else if ( !strncmp ( name, "mean", 2 ) )
      vCat = mean ;
    else if ( !strncmp ( name, "hip", 2 ) )
      vCat = mean ;
    else {
      vCat = noCat ;
      sprintf ( hip_msg, "%s is not a recognised var. category.\n", name ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }
  }

  /* Find the appropriate boundaries. */
  mUn = pVL->mUnknowns ;

  for ( kV = 0 ; kV < mUn ; kV++ ) 
    /* Note that mV contains the increment by one for a range of 1 ... mUnknowns. */
    if ( num_match( mV+kV, range ) ) {

      if ( keyword[0] == 'n' )
        /* Name change. */
        strncpy ( pVL->var[kV].name, name, LEN_VARNAME ) ;
      else
        /* Category change. */
        pVL->var[kV].cat = vCat ;
    }


  return ( 1 ) ;
}


/* Primitive variables: rho, u, v, p.
   Conservative variables: rho, rho*u, rho*v, rho*e. */

void prim2cons( double primVar[], double consVar[], const int mDim ) {
  /* Convert primitive to conservative. Do energy first, so
     primVar==consVar is allowed. */
  double q = primVar[1]*primVar[1] + primVar[2]*primVar[2] ;

  if ( mDim == 3 ) {
    q += primVar[3]*primVar[3] ;
    q *= .5*primVar[0] ;
    consVar[4] = primVar[4]/GammaM1 + q ;
    consVar[3] = primVar[0] * primVar[3] ;
  }
  else {
    q *= .5*primVar[0] ;
    consVar[3] = primVar[3]/GammaM1 + q ;
  }

  consVar[0] = primVar[0] ;
  consVar[1] = primVar[0] * primVar[1] ;
  consVar[2] = primVar[0] * primVar[2] ;
  
  return ;
}

void primT2cons( double primVar[], double consVar[], const int mDim ) {
  /* Convert primitiveT (uvwpT) to conservative. */
  double u,v,w,p,T,rho,q ;

  u = primVar[0] ;
  v = primVar[1] ;

  if ( mDim == 3 ) {
    w = primVar[2] ;
    p = primVar[3] ;
    T = primVar[4] ;
    q = u*u + v*v + w*w ;
  }
  else {
    p = primVar[2] ;
    T = primVar[3] ;
    q = u*u + v*v ;
  }
  rho = p/R/T ;


  consVar[0] = rho ;
  consVar[1] = u*rho ;
  consVar[2] = v*rho ;

  if ( mDim == 3 ) {
    consVar[3] = w*rho ;
    consVar[4] = p/GammaM1 + q ;
  }
  else {
    consVar[3] = p/GammaM1 + q ;
  }
  
  return ;
}

void primT2prim( double primTVar[], double primVar[], const int mDim ) {
  /* Convert primitiveT (uvwpT) to primitive (rho,u,v,w,p). */
  double u,v,w,p,T,rho ;

  u = primTVar[0] ;
  v = primTVar[1] ;

  if ( mDim == 3 ) {
    w = primTVar[2] ;
    p = primTVar[3] ;
    T = primTVar[4] ;
  }
  else {
    p = primTVar[2] ;
    T = primTVar[3] ;
  }
  rho = p/R/T ;


  primVar[0] = rho ;
  primVar[1] = u ;
  primVar[2] = v ;

  if ( mDim == 3 ) {
    primVar[3] = w ;
    primVar[4] = p ;
  }
  else {
    primVar[3] = p ;
  }
  
  return ;
}

void prim2primT ( double primVar[], double primTVar[], const int mDim ) {
  /* Convert primitiveT (uvwpT) to primitive (rho,u,v,w,p). */

  double u,v,w,p,T,rho ;

  rho = primVar[0] ;
  u = primVar[1]/rho ;
  v = primVar[2]/rho ;

  if ( mDim == 3 ) {
    w = primVar[3]/rho ;
    p = primVar[4] ;
  } else {
    p = primTVar[3] ;
  }
  T = p/rho/R ;


  primTVar[0] = u ;
  primTVar[1] = v ;
  if ( mDim == 3 ) {
    primTVar[2] = w ;
    primTVar[3] = p ;
    primTVar[4] = T ;
  } else {
    primTVar[2] = p ;
    primTVar[3] = T ;
  }

  return ;
}


void cons2prim( double consVar[], double primVar[], const int mDim ) {
  /* Convert conservative to primitive. Do energy first, so
     primVar==consVar is allowed. */
  double rho, u, v, w, rhoE, q, p ;

  rho = consVar[0] ;
  u = consVar[1]/rho ;
  v = consVar[2]/rho ;

  if ( mDim == 3 ) {
    w = consVar[3]/rho ;
    q = .5*rho*( u*u + v*v + w*w ) ;
    rhoE = consVar[4] ;
    p = GammaM1*( rhoE - q ) ;
    primVar[3] = w ;
    primVar[4] = p ;
  }
  else {
    q = .5*rho*( u*u + v*v ) ;
    rhoE = consVar[3] ;
    p = GammaM1*( rhoE - q ) ;
    primVar[3] = p ;
  }

  primVar[0] = rho ;
  primVar[1] = u ;
  primVar[2] = v ;

  return ;
}

void cons2primT ( double consVar[], double primVar[], const int mDim ) {
  /* Convert conservative to primitiveT = u,v,w,p,T */
  double rho, u, v, w, rhoE, q, p, T ;

  rho = consVar[0] ;
  u = consVar[1]/rho ;
  v = consVar[2]/rho ;

  if ( mDim == 3 ) {
    w = consVar[3]/rho ;
    q = .5*rho*( u*u + v*v + w*w ) ;
    rhoE = consVar[4] ;
    p = GammaM1*( rhoE - q ) ;
  }
  else {
    q = .5*rho*( u*u + v*v ) ;
    rhoE = consVar[3] ;
    p = GammaM1*( rhoE - q ) ;
  }

  T = p/rho/R ;

  primVar[0] = u ;
  primVar[1] = v ;
  if  ( mDim == 3 ) {
    primVar[2] = w ;
    primVar[3] = p ;
    primVar[4] = T ;
  } else {
    primVar[2] = p ;
    primVar[3] = T ;
  }

  return ;
}

void prim2para ( double primVar[], double paraVar[], const int mDim ) {
  /* Primitive variables rho,u,v,p to parameter vector. */

  paraVar[0] = sqrt(primVar[0]) ;
  paraVar[1] = paraVar[0]*primVar[1] ;
  paraVar[2] = paraVar[0]*primVar[2] ;

  if ( mDim == 3 ) {
    paraVar[3] = paraVar[0]*primVar[3] ;
    paraVar[4] = Gamma/GammaM1 * primVar[4]/paraVar[0] +
                 .5*paraVar[0]*( primVar[1]*primVar[1] +
				 primVar[2]*primVar[2] +
				 primVar[3]*primVar[3] ) ;
  }
  else
    paraVar[3] = Gamma/GammaM1 * primVar[3]/paraVar[0] +
                 .5*paraVar[0]*( primVar[1]*primVar[1] +
				 primVar[2]*primVar[2] ) ;

  return ;
  
}


void cons2para ( double consVar[], double paraVar[], const int mDim )
{ /* Conservative variables rho,u,v,p to parameter vector.
     Fix this to being less lazy when we get around to.
  double primVar[MAX_DIM+2] ;

  cons2prim ( consVar, primVar, mDim ) ;
  prim2para ( primVar, paraVar, mDim ) ;
  return ; */

  double OneOverRootRho = 1./sqrt( consVar[0] ) ;

  paraVar[0] = OneOverRootRho ;
  paraVar[1] = OneOverRootRho*consVar[1] ;
  paraVar[2] = OneOverRootRho*consVar[2] ;
    
  if ( mDim == 2 )
  { paraVar[3] = OneOverRootRho*( Gamma*consVar[3] -
				  .5*GammaM1*( paraVar[1]*paraVar[1] +
					       paraVar[2]*paraVar[2] ) ) ;
  }
  else
  { paraVar[3] = OneOverRootRho*consVar[3] ;
    paraVar[3] = OneOverRootRho*( Gamma*consVar[3] -
				  .5*GammaM1*( paraVar[1]*paraVar[1] +
					       paraVar[2]*paraVar[2] +
					       paraVar[3]*paraVar[3] ) ) ;
  }
  return ;
}


/******************************************************************************

  var2var:
  Find the matching state variable conversion routinge.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int var2var ( const varType_e oldVarType, const varType_e newVarType,
	      void (**PconvFun) ( double*, double*, const int ) ) {
  
  *PconvFun = NULL ;

  if ( oldVarType == noType )
    return ( 0 ) ;

  else if ( oldVarType == newVarType )
    /* Nothing to convert. */
    return ( 1 ) ;
  
  else if ( oldVarType == cons ) {
    if ( newVarType == prim )
      *PconvFun = cons2prim ;
    else if ( newVarType == primT )
      *PconvFun = cons2primT ;
    else if ( newVarType == para )
      *PconvFun = cons2para ;
  }
  
  else if ( oldVarType == prim ) {
    if ( newVarType == cons )
      *PconvFun = prim2cons ;
    else if ( newVarType == primT )
      *PconvFun = prim2primT ;
    else if ( newVarType == para )
      *PconvFun = prim2para ;
  }

  else if ( oldVarType == primT ) {
    if ( newVarType == cons )
      *PconvFun = prim2cons ;
    else if ( newVarType == prim )
      *PconvFun = primT2prim ;
    /* else if ( newVarType == para )
     *PconvFun = prim2para ; */
  }

  if ( *PconvFun )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

/******************************************************************************

  get_mach_freestream:
  Calculate the freestream Mach number.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double get_mach_freestream ( const double freeStreamVar[], const int mDim )
{
  double Einf, Ma ;

  if ( mDim == 2 )
    Einf = freeStreamVar[3]/freeStreamVar[0] ;
  else if ( mDim == 3 )
    Einf = freeStreamVar[4]/freeStreamVar[0] ;
  else
  { printf ( " SORRY, cannot calculate a Mach number in %d dimensions.\n", mDim ) ;
    return ( 0. ) ;
  }

  Ma = 1./sqrt( Gamma*GammaM1*( Einf-.5 ) ) ;

  return ( Ma ) ;
}

/******************************************************************************

  get_freestream_mach:
  Calculate the freestream from the Mach number and two angles in radian,
  theta working in the z-plane.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_freestream_mach ( double freeStreamVar[], const int mDim,
			   double Ma, double alpha, double theta )
{
  freeStreamVar[0] = 1 ;
  freeStreamVar[1] = cos( alpha ) * cos( theta ) ;
  freeStreamVar[2] = sin( alpha ) * cos( theta ) ;
  
  if ( mDim == 2 )
    freeStreamVar[3] = 1./(Ma*Ma*Gamma*GammaM1)+0.5 ;
  else if ( mDim == 3 ) {
    freeStreamVar[3] = sin( theta ) ;
    freeStreamVar[4] = 1./(Ma*Ma*Gamma*GammaM1)+0.5 ;
  }
  else {
    printf ( " SORRY, cannot calculate a Mach number in %d dimensions.\n", mDim ) ;
    return ( 0 ) ; }

  return ( 1 ) ;
}


/******************************************************************************

  get_var:
  Extract a scalar variable from the vector.
  
  Last update:
  ------------
  4Apr09; use new def of varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double get_var ( varList_s *pVL, double *pUn, const char *var ) {

  int kVar = atoi ( var ), kp = pVL->mUnknFlow-1 ;
 
  if ( kVar > 0 && kVar <= pVL->mUnknowns )
    return ( pUn[kVar-1] ) ;

  else if ( var[0] == 'r' )
    return ( pUn[0] ) ;
  
  else if ( var[0] == 'u' )
    return ( pUn[1] ) ;
  
  else if ( var[0] == 'v' )
    return ( pUn[2] ) ;
  
  else if ( var[0] == 'w' )
    return ( pUn[3] ) ;
  
  else if ( var[0] == 'p' )
    return ( pUn[kp] ) ;

  else if  ( var[0] == 't' )
    return ( pUn[kp]/pUn[0]/R ) ;

  else if  ( var[0] == 'q' )
    return ( sqrt( pUn[1]*pUn[1] + pUn[2]*pUn[2] + pUn[3]*pUn[3] ) ) ;

  else
    return ( 1. ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_nVar:
*/
/*! Given a snipped of a variable name or number, return the next matching var number.
 *
 *
 */

/*
  
  Last update:
  ------------
  11Jul19; correct bug with -1 offset for nV. rename nVar to kVar for clarity.
  2Apr19: conceived.
  

  Input:
  ------
  pVL: varlist
  kVarPrev: last kVar searched
  varNameExpr: alphanum expression that matches the variable.
  

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  kVar with 0<=kVar<mVar on success, -1 on failure. 
  
*/

int find_kVar ( const varList_s *pVL, int kVarPrev, const char *varNameExpr ) {


  
  int nV ;
  int mV = pVL->mUnknowns ;

  if ( is_int ( varNameExpr ) ) {
    /* Number given, ranges from 1 .. mUnk, try that. */
    nV = atoi ( varNameExpr ) ;
    if ( nV > 0 && nV <= mV )
      return ( nV-1 ) ;
    else
      /* Not in range. */
      return ( -1 ) ;
  }

  /* Text match. */
  const char *vName ;
  /* kV ranges 0 .. mV-1. */
  int kV ;
  for ( kV = kVarPrev+1 ; kV < mV ; kV++ )  {
    vName = pVL->var[kV].name ;
    
    if ( !fnmatch( varNameExpr, vName, 0 ) )
      return ( kV ) ;
  }
  
  
  return ( -1 ) ;
}
