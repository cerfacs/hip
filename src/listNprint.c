/*
  listNprint.c:
*/

/*! listing/query routines for aspects that may be non-uns.
 */


/* 
  Last update:
  ------------
  6Sep19; conceived

  
  
  This file contains:
  -------------------
  list_grids
 
*/
#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"
#include "proto_mb_uns.h"
#include "proto_mb.h"


#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern Grids_struct Grids ;
extern const char gridTypeNames[3][7] ;
extern const char varTypeNames[][LEN_VAR_C] ;
extern const char varCatNames[][LEN_VAR_C] ;
extern const elemType_struct elemType[] ;




/***************************************************************************

  PRIVATE

**************************************************************************/


/***************************************************************************

  PUBLIC

**************************************************************************/

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  list_grids:
*/
/*! list all grids.
 *
 */

/*
  
  Last update:
  ------------
  6Sep19: extracted from hip.c

    
  Returns:
  --------
  ret_s with 1 on failure, 0 on success
  
*/

/* hip_cython */
ret_s list_grids () {
  ret_s ret = ret_success () ;
  
  const grid_struct *Pgrid ;
  const varList_s *pVarList ;

  hprintf ( "        Nr Type Dim    Topo    Cells    Nodes  BndFc       Vars  Name:\n" ) ;
  for ( Pgrid = Grids.PfirstGrid ; Pgrid ; Pgrid = Pgrid->uns.PnxtGrid ) {
    pVarList = Pgrid->uns.pVarList ;
      
    /* Mark the current grid. */
    if ( Pgrid == Grids.PcurrentGrid )
      hprintf ( "  Curr: " ) ;
    else
      hprintf ( "        " ) ;

    if ( Pgrid->uns.type == uns ) {
      uns_s *pUns = Pgrid->uns.pUns ;
      hprintf ( "%2d %4s %3d %7s %8"FMT_ULG" %8"FMT_ULG" %6"FMT_ULG" %2d*%6s  %s\n",
                Pgrid->uns.nr, gridTypeNames[Pgrid->uns.type],
                Pgrid->uns.mDim, specialTopoString( pUns ),
                pUns->mElemsNumbered, pUns->mVertsNumbered, 
                pUns->mFaceAllBc, pUns->varList.mUnknowns,
                varTypeNames[pUns->varList.varType], Pgrid->uns.name ) ;
    }
    else {
      hprintf ( "%2d %4s %3d                           %2d*%6s  %s\n",
                Pgrid->uns.nr, gridTypeNames[Pgrid->uns.type],
                Pgrid->uns.mDim, Pgrid->uns.pVarList->mUnknowns,
                varTypeNames[Pgrid->uns.pVarList->varType], Pgrid->uns.name ) ;
    }
  }
  hprintf ( "\n" ) ;

  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  list_surfaces:
*/
/*! list all boundary surfaces/patches
 *
 *
 */

/*
  
  Last update:
  ------------
  6Sep19: extracted from hip.c
    
  Returns:
  --------
  ret: 1 on failure, 0 on success
  
*/

/* hip_cython */
ret_s list_surfaces ( char *keyword ) {
  ret_s ret = ret_success () ;

  bc_struct *Pbc ;
  if ( !strncmp ( keyword, "all", 2 ) ) {
    /* List all surfaces. */
    print_bc ( NULL, NULL ) ;
    for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc )
      print_bc ( Pbc, NULL ) ;
    hprintf (   "\n" ) ;
  }
  else
    /* List only the surfaces present in the current grid. */
    if ( Grids.PcurrentGrid->uns.type == uns )
      /* Unstructured. */
      list_uns_bc ( Grids.PcurrentGrid, keyword ) ;
    else
      /* Multiblock. */
      list_mb_bc ( Grids.PcurrentGrid ) ;

  return ( ret ) ;
}
