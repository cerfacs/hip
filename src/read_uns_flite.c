/*
 read_uns_flite.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from a BAe flite file. Tets only.

 contains:
 read_uns_cfdrc
 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern int check_lvl ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;

typedef struct {
  bc_struct *pBc ;
} surf_s ;

/******************************************************************
 
 read_uns_flite.c:

 Last update:
 ------------
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
 9Jul11; call init_elem
 4Apr09; replace varTypeS with varList.
 15Jul99; derived from read_uns_cfdrc.

 Input:
 ------


 Changes to:
 -----------


 Returns:
 --------
 
*/

int read_uns_flite ( char *cFroFile, char *cGriFile, char *cBcoFile ) {

  const int mDim = 3 ;
  
  FILE *froFile, *griFile, *bcoFile ;
  char text[MAX_BC_CHAR] ;
  int mVerts, mBndVx, nSurf, nElem, nVrtx, mBndFc, nBc, nrVx[4], mSurfs, mLines,
    mElems, someInt[9] ;
  float someFloat[3] ;
  double *pCo ;
  chunk_struct *pChunk ;
  elem_struct *pElem ;
  bndFcVx_s *pBndFcVx, *pBf ;
  vrtx_struct *pVrtx, **ppVx ;
  uns_s *pUns ;
  surf_s *pSurf, *pS ;

  prepend_path ( cFroFile ) ;
  prepend_path ( cGriFile ) ;
  prepend_path ( cBcoFile ) ;

  if ( ( froFile = fopen ( cFroFile, "r" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", cFroFile ) ;
    return ( 0 ) ; }
  if ( ( griFile = fopen ( cGriFile, "r" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", cGriFile ) ;
    return ( 0 ) ; }
  
  if ( ( bcoFile = fopen ( cBcoFile, "r" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", cBcoFile ) ;
    return ( 0 ) ; }

  
  /* This will be the next grid. */
  if ( verbosity > 2 )
    printf ( "    Reading unstructured flite.\n" ) ;


  FREAD ( &mBndFc, sizeof( int ), 1, froFile ) ;
  FREAD ( &mBndVx, sizeof( int ), 1, froFile ) ;
  FREAD ( someInt, sizeof( int ), 4, froFile ) ;
  FREAD ( &mElems, sizeof( int ), 1, griFile ) ;
  FREAD ( &mVerts, sizeof( int ), 1, griFile ) ;
  FREAD ( someInt, sizeof( int ), 1, griFile ) ;

  fscanf ( bcoFile, "%*[^\n]\n%d %d", &mSurfs, &mLines ) ;
  fscanf ( bcoFile, "\n" ) ;


  
  /* Allocate the fields of which the sizes are known by now. */
  if ( verbosity > 2 )
    printf ( "     Boundary faces:  %d\n"
             "     Tetrahedra:      %d\n"
             "     Vertices:        %d\n"
             "     Surfaces:        %d\n"
             "     Lines:           %d\n",
             mBndFc, mElems, mVerts, mSurfs, mLines ) ;



    
  /* Allocate a chunk. */
  if ( !( pUns = make_uns ( NULL ) ) ) {
    hip_err ( fatal, 0, "failed to alloc a new unstructured grid"
              " in read_uns_cfdrc.\n" ) ;}
  else {
    pUns->mDim = mDim ;
  }

  /* Alloc a temporary list of boundary faces to be sorted and a list of
     boundary conditions using the upper bound given in the file header. */
  pUns->pBndFcVx = pBndFcVx =
    arr_malloc ( "pUns->pBndFcVx in read_uns_flite", pUns->pFam,
                 mBndFc, sizeof ( bndFcVx_s ) );
  pSurf  = arr_malloc ( "pSurf in read_uns_flite", pUns->pFam,
                        mSurfs, sizeof ( surf_s ) ) ;
  pUns->mBndFcVx = mBndFc ;


  
  /* Read the boundary surface groups. Skip a tag line. */
  if ( verbosity > 3 )
    printf ( "       Reading %d boundary surfaces.\n", mSurfs ) ;
  fscanf ( bcoFile, "%*[^\n]" ) ; fscanf ( bcoFile, "\n" ) ;
  for ( pUns->mBc = 0, pS = pSurf ; pS < pSurf+mSurfs ; pS++ ) {
    fscanf ( bcoFile, "%*d %d", &nSurf ) ;
    fscanf ( bcoFile, "\n" ) ;

    /* Make a label for this surface. */
    if ( nSurf == 1 )
      sprintf ( text, "solid wall" ) ;
    else if ( nSurf == 2 ) 
      sprintf ( text, "symmetry" ) ;
    else if ( nSurf == 3 ) 
      sprintf ( text, "inflow/outflow" ) ;
    else if ( nSurf == 10 ) 
      sprintf ( text, "trailing edge" ) ;
    else 
      sprintf ( text, "flite surface type %d", nSurf ) ;
    
    if ( !( pS->pBc = find_bc ( text, 2 ) ) ) {
      /* New bc. */
      pUns->mBc ++ ;
      pS->pBc = find_bc ( text, 1 ) ;
    }
  }
  fclose ( bcoFile ) ;

  

   
  /* Allocate a chunk. We don't know the number of boundary faces, yet. */
  if ( !( pChunk = append_chunk ( pUns, pUns->mDim, mElems, 4*mElems,
                                  0, mVerts, mBndFc, pUns->mBc ) ) ) {
    printf ( " FATAL: could not allocate a chunk in read_uns_flite.\n" ) ;
    return ( 0 ) ; }



  
  /* Loop over all element types. */
  if ( verbosity > 3 )
    printf ( "       Reading %d elements.\n", mElems ) ;

  pElem = pChunk->Pelem ;
  ppVx = pChunk->PPvrtx ;
  pVrtx = pChunk->Pvrtx ;
  nElem = 0 ;
  for ( nElem = 1 ; nElem <= mElems ; nElem++) {

    pElem++ ;
    ppVx += 4 ;
    
    if ( FREAD ( nrVx, sizeof( int ), 4, griFile ) != 4 ) {
      printf ( " FATAL: failed to read connectivity for element %d in read_uns_flite.\n",
               nElem ) ; return ( 0 ) ; }
    
    init_elem ( pElem, tet, nElem, ppVx ) ;

    ppVx[0] = pVrtx + nrVx[0] ;
    ppVx[1] = pVrtx + nrVx[1] ;
    ppVx[2] = pVrtx + nrVx[3] ;
    ppVx[3] = pVrtx + nrVx[2] ;
  }



  
  /* Vertices. */
  if ( verbosity > 3 )
    printf ( "       Reading %d vertices.\n", mVerts ) ;
  
  pVrtx = pChunk->Pvrtx ;
  pCo = pChunk->Pcoor ;
  for ( nVrtx = 1 ; nVrtx <= mVerts ; nVrtx++) {
    if ( FREAD ( someFloat, sizeof( float ), 3, griFile ) != 3 ) {
      printf ( " FATAL: reading vertices failed.\n" ) ;
      exit (  EXIT_FAILURE ) ; }

    ++pVrtx ;
    pCo += mDim ;
    pCo[0] = someFloat[0] ;
    pCo[1] = someFloat[1] ;
    pCo[2] = someFloat[2] ;
    pVrtx->Pcoor = pCo ;
    pVrtx->Punknown = NULL ;
    pVrtx->number = nVrtx ;
  }





  
  if ( verbosity > 3 )
    printf ( "        Skipping %d boundary vertices.\n", mBndVx ) ;
  if ( fseek ( froFile, mBndVx*(sizeof( int )+3*sizeof(float)), SEEK_CUR ) ) {
    printf ( " FATAL: skipping failed.\n" ) ;
    exit ( EXIT_FAILURE ) ; }
  


  if ( verbosity > 3 )
    printf ( "       Reading %d boundary faces.\n", mBndFc ) ;
  for ( pVrtx = pChunk->Pvrtx, pBf = pBndFcVx ; pBf < pBndFcVx+mBndFc ; pBf++) {

    /* Forming vertices. Read the face number as a first vertex. */
    if ( FREAD ( nrVx, sizeof( int ), 4, froFile ) != 4 ) {
      printf ( " FATAL: reading boundary faces failed.\n" ) ;
      exit ( EXIT_FAILURE ) ; }
    pBf->ppVx[0] = pVrtx + nrVx[1] ;
    pBf->ppVx[1] = pVrtx + nrVx[2] ;
    pBf->ppVx[2] = pVrtx + nrVx[3] ;
    pBf->mVx = 3 ;

    /* Surface group. Note that pSurf is numbered starting from 0. */
    if ( FREAD ( &nBc, sizeof( int ), 1, froFile ) != 1 ) {
      printf ( " FATAL: reading boundary face group failed.\n" ) ;
      exit ( EXIT_FAILURE ) ; }
    pBf->pBc = pSurf[nBc-1].pBc ;
  }
  arr_free ( pSurf ) ;

  fclose ( froFile ) ;
  fclose ( griFile ) ;



  /* Match boundary faces. */
  if ( !match_bndFcVx ( pUns ) ) {
    printf ( " FATAL: could not match boundary faces in read_uns_flite.\n" ) ;
    return ( 0 ) ;
  }
 



  
  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;
  
  /* Make a new grid. */
  grid_struct *pGrid ;
  if ( !( pGrid = make_grid () ) ) {
    printf ( " WARNING: malloc for the linked list of grids"
	     " failed in read_uns_cfdrc.\n" ) ;
    free_chunk ( pUns, &pChunk ) ;
    return ( 0 ) ; }
  else {
    /* Put the chunk into Grids. */
    pGrid->uns.type = uns ;
    pGrid->uns.pUns = pUns ;
    pGrid->uns.mDim = mDim ;
    pGrid->uns.pVarList = &(pUns->varList) ;
    pUns->nr = pGrid->uns.nr ;
    pUns->pGrid = pGrid ;
  }
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  
  return ( 1 ) ;
}
