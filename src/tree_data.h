/* At most an octree, for now. */
#define MAX_BOX 8
#define MAX_DIM 3

/* Specify the data type and value type for tree.c. */
#define DATA_TYPE void
#define VALU_TYPE double

#define TOO_MUCH ( 1.e25 )

/* cut_mb.c */
VALU_TYPE *coor2coor( const DATA_TYPE *Pcoor ) ;

/* merge_uns.c */
VALU_TYPE *vrtx2coor( const DATA_TYPE *Pdata ) ;

/* uns_per.c */
VALU_TYPE *gravFc2coor ( const DATA_TYPE *pGravFc ) ;
VALU_TYPE *perVx2coor ( const DATA_TYPE *pPerVx ) ;

/* Why is this repeated from array.h here? array.c
void *arr_ini_nonArr ( void *pFam, char *name, const void *pData,
                       size_t (*sizeFun) ( const void *pData,
                                           size_t *pOvh, ulong_t *pmData, ulong_t *pDataSize ) ) ;
void *arr_del_nonArr ( const void *pData ) ; */
