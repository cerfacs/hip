/* read_str_dpl.c: Reading a structured multiblock grid via a as.doc file.

 Contains:
 ---------
 read_str_dpl:

 Last update:
 --------------
 6Jan15; rename pFamMb to more descriptive pArrFamMb
 18Dec10; new pBc->type

 
*/
#include <string.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

#define MDIM 2
#define INT_TOO_MUCH 999999999

extern arrFam_s *pArrFamMb;

/******************************************************************************

  read_str_dpl:

  
  Last update:
  ------------
  26Nov98: fix bug with calculating the bounding box.
  16jul96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_str_dpl ( FILE *FdplIn )
{
  extern Grids_struct Grids ;
  extern int verbosity ;
  fpos_t filePosFaces ;

  char c5[5], text[MAX_BC_CHAR], bcText[MAX_BC_CHAR] ;
  grid_struct *Pgrid ;
  mb_struct *Pmb ;
  block_struct *PblockS, *Pbl ;
  double *Pcoor ;
  int mI, mJ, mVerts, nVert, mBounds, nBound, nFace, mFaces, mOutFaces, partNumber,
      vxFc[2], kVx, nVx, nI, nJ, minI, maxI, minJ, maxJ, mElems, mBndFaces,
      nameBnd, mScan ;
  subFace_struct *PsubFaces, **PPsubFaces, *Psf ;

  /* Get the partNumber. It will be one more than the number of existing grids. */
  partNumber = Grids.mGrids + 1 ;
  if ( verbosity > 2 )
    printf ( "  Reading structured dpl as part %d.\n", partNumber ) ;

  /* Make sure it's an structured .dpl. */
  rewind ( FdplIn ) ;
  if ( fscanf ( FdplIn, "%[sS]%[tT]%[rR]%[uU]%[cC]%*[^\n]",
                &c5[0], &c5[1], &c5[2], &c5[3], &c5[4] ) != 5 )
  { printf ( " FATAL: file is not of dpl type.\n" ) ;
    return ( 0 ) ;
  }
  fscanf ( FdplIn, "%*[\n]" ) ;

  /* Dimensions. */
  if ( fscanf ( FdplIn, "%d %d%*[^\n]", &mI, &mJ ) != 2 )
  /* if ( fscanf ( FdplIn, "%d %d", &mI, &mJ ) != 2 ) */
  { printf ( " FATAL: could not read grid dimensions in read_str_dpl.\n" ) ;
    fclose ( FdplIn ) ;
    return ( 0 );
  }
  mVerts = mI*mJ ;
  mElems = ( mI-1 )*( mJ-1 ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;
  
  /* Freestream. */
  fscanf ( FdplIn, "%*[^\n]%*[\n]" ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;
  
  /* Alloc. Note that PblockS is indexed starting from 1.*/
  PblockS = arr_malloc ( "PblockS in read_str_dpl", pArrFamMb,  2, sizeof( block_struct ) ) ;
  Pmb = arr_malloc ( "Pmb in read_str_dpl",  pArrFamMb, 1, sizeof( mb_struct ) ) ;
  Pcoor = arr_malloc ( "Pcoor in read_str_dpl", pArrFamMb,
                       ( mVerts+1 )*MDIM, sizeof( double ) ) ;

  Pmb->PblockS = PblockS ;
  Pmb->mBlocks = 1 ;
  Pmb->mDim = 2 ;
  Pmb->mUnknowns = 0 ;
  Pmb->mVerts = mVerts ;
  Pmb->mElems = mElems ;

  Pbl = PblockS + 1 ;
  Pbl->nr = 1 ;
  Pbl->skip = 1 ;
  Pbl->mVertFile[0] = Pbl->mVert[0] = mI ;
  Pbl->mVertFile[1] = Pbl->mVert[1] = mJ ;
  Pbl->mVertFile[2] = Pbl->mVert[2] = 1 ;
  Pbl->mVertsBlock = mVerts ;
  Pbl->mElemsBlock = mElems ;
  Pbl->Pcoor = Pcoor ;
  Pbl->PmbRoot = Pmb ;

  /* Void the marker fields. */
  Pbl->PdblMark = Pbl->Punknown = NULL ;
  Pbl->PintMark = Pbl->PelemMark = NULL ;
  
  /* Initialize block bounding box. */
  Pbl->llBox[0] = TOO_MUCH ;
  Pbl->urBox[0] = - TOO_MUCH ;
  Pbl->llBox[1] = TOO_MUCH ;
  Pbl->urBox[1] = - TOO_MUCH ;

  /* Read coordinates. */
  for ( nVert = 1 ; nVert <= mVerts ; nVert++ )
  {
    /* Calculate a permuted vertex position. Note that hip et al
       store vertices with i running fastest. */
    nJ = ( nVert-1 )%mJ + 1 ;
    nI = ( nVert-nJ )/mJ + 1 ;
    nVx = nI + mI*( nJ-1 ) ;

    /* Location of the y-coordinate. */
    Pcoor = Pbl->Pcoor + 2*nVx + 1 ;
    
#   ifdef CHECK_BOUNDS
      if ( Pcoor > Pbl->Pcoor + ( mVerts+1 )*MDIM - 1 )
	printf ( " WARNING: %td exceeds the bounds of Pcoor: %d"
		 " in read_str_dpl.\n", Pcoor - Pbl->Pcoor, ( mVerts+1 )*MDIM ) ;
#   endif

    fscanf ( FdplIn, "%lf %lf%*[^\n]", Pcoor-1, Pcoor ) ;
    fscanf ( FdplIn, "*\n" ) ;

    /* Calculate the bounding box. */
    Pbl->llBox[0] = MIN( Pbl->llBox[0], *(Pcoor-1) ) ;
    Pbl->urBox[0] = MAX( Pbl->urBox[0], *(Pcoor-1) ) ;
    Pbl->llBox[1] = MIN( Pbl->llBox[1], *Pcoor ) ;
    Pbl->urBox[1] = MAX( Pbl->urBox[1], *Pcoor ) ;
  }

  /* Info. */
  printf  ( "   Read %d * %d = %d vertices.\n", mI, mJ, mVerts ) ;

  /* Number of boundaries */
  fscanf ( FdplIn, "%d%*[^\n]%*[\n]", &mBounds ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;

  /* Get a file pointer to reread the boundary faces after
     the memory has been allocated. */
  if ( fgetpos ( FdplIn, &filePosFaces ) != 0 )
  { printf ( " FATAL: failure while getting file" ) ;
    printf ( " pointer to faces.\n" ) ;
    exit ( EXIT_FAILURE ) ;
  }

  /* Scan the .dpl for the number of boundary faces. */
  for ( mBndFaces = 0, nBound = 1 ; nBound <= mBounds ; nBound++ )
  { fscanf ( FdplIn, "%d%*[^\n]\n", &mFaces ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
    mBndFaces += mFaces ;
    for ( nFace = 1 ; nFace <= mFaces ; nFace++ )
    { fscanf ( FdplIn, "%*[^\n]" ) ;
      fscanf ( FdplIn, "%*[\n]" ) ;
    }
  }
  
  /* Scan the .dpl for the number of outer boundary faces. */
  fscanf ( FdplIn, "%d%*[^\n]\n", &mOutFaces ) ;
  if ( mOutFaces )
    /* Make this one more boundary. */
    mBounds++ ;

  /* Allocate for boundaries. */
  PsubFaces = arr_malloc ( "PsubFaces in read_str_dpl", pArrFamMb,
                           mBounds+1, sizeof( subFace_struct ) ) ;
  PPsubFaces = arr_malloc ( "PPsubFaces in read_str_dpl", pArrFamMb,
                            mBounds, sizeof( subFace_struct * ) ) ;
  if ( !PsubFaces )
  { printf ( " FATAL: allocation for the boundary structures failed in read_str_dpl.\n" ) ;
    free_mb ( &Pmb ) ;
    return ( 0 ) ;
  }
  else
  { Pmb->mSubFaces = mBounds ;
    Pmb->subFaceS = PsubFaces ;
    Pbl->mSubFaces = mBounds ;
    Pbl->PPsubFaces = PPsubFaces ;
  }

  /* Reposition the .dpl file. */
  if ( fsetpos ( FdplIn, &filePosFaces ) != 0 )
  { printf ( " FATAL: failure while repositioning" ) ;
    printf ( " file pointer to faces.\n" ) ;
    free_mb ( &Pmb ) ;
    return ( 0 ) ;
  }

  /* Loop over all boundaries. */
  for ( nBound = 1 ; nBound <= mBounds ; nBound++ )
  {
    /* Number of faces on this boundary. */
    nameBnd = 0 ;
    fscanf ( FdplIn, "%d", &mFaces ) ;
    fgets ( text, MAX_BC_CHAR, FdplIn ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
    mScan = sscanf ( text, "%d%s", &nameBnd, bcText ) ;
    if ( mScan  < 1 )
      nameBnd = nBound ;

    /* Make a new bc entry. */
    if ( mScan == 2 )
    { /* There is a given text. */
      strncpy ( bcText, text, MAX_BC_CHAR ) ;
      r1_beginstring ( bcText, MAX_BC_CHAR ) ;
    }
    else
    { if ( nBound == mBounds && mOutFaces )
	/* This is an outer boundary. */
	sprintf ( bcText, "%d: Grid part nr. %d, outer boundary",
		  nBound, partNumber ) ;
      else
	/* This is a normal boundary. */
	sprintf ( bcText, "%d: Grid part nr. %d, segment %d",
		  nameBnd, partNumber, nameBnd ) ;
    }

    /* Initialize the max-min for this boundary. */
    minJ = minI = INT_TOO_MUCH ;
    maxJ = maxI = -INT_TOO_MUCH ;
    
    /* Loop over all faces. */
    for ( nFace = 1 ; nFace <= mFaces ; nFace++ ) {
      /* Read the two forming vertices. */
      fscanf ( FdplIn, "%d %d%*[^\n]", vxFc, vxFc+1 ) ;
      fscanf ( FdplIn, "%*[\n]" ) ;

      /* Dplot organizes the vertex numbers with mJ running fastest.
	 Find the min and max of the running index. */
      for ( kVx = 0 ; kVx < 2 ; kVx++ )
      { nJ = ( vxFc[kVx]-1 )%mJ + 1 ;
	nI = ( vxFc[kVx]-nJ )/mJ + 1 ;

	minJ = MIN( minJ, nJ ) ;
	maxJ = MAX( maxJ, nJ ) ;
	minI = MIN( minI, nI ) ;
	maxI = MAX( maxI, nI ) ;
      }
    }

    /* Plausibility check: there have to be one less faces than the
       difference in the running index. There has to be a static index. */
    if ( minI == maxI ) {
      /* J runs. */
      if ( maxJ - minJ != mFaces )
      { printf ( " FATAL: Boundary Nr. %d tries to span %d vertices"
		 " with %d faces.\n", nBound, maxJ - minJ, mFaces ) ;
	free_mb ( &Pmb ) ;
	fclose ( FdplIn ) ;
	return ( 0 ) ;
      }
      else if ( verbosity > 3 )
        printf  ( "   Read boundary %d, (%d,%d) to (%d,%d), named %s\n",
		  nBound, minI, minJ, maxI, maxJ, bcText ) ;
    }
    else if ( minJ == maxJ )
    { /* I runs. */
      if ( maxI - minI != mFaces )
      { printf ( " FATAL: Boundary Nr. %d tries to span %d vertices"
		 " with %d faces.\n", nBound, maxI - minI, mFaces ) ;
	free_mb ( &Pmb ) ;
	fclose ( FdplIn ) ;
	return ( 0 ) ;
      }
      else if ( verbosity > 3 )
        printf  ( "   Read boundary %d, (%d,%d) to (%d,%d), named %s\n",
		  nBound, minI, minJ, maxI, maxJ, bcText ) ;
    }
    else
    { printf ( " FATAL: boundary %d, (%d,%d) to (%d,%d), named %s\n"
	       "        has no static index.\n",
	       nBound, minI, minJ, maxI, maxJ, bcText ) ;
      free_mb ( &Pmb ) ;
      fclose ( FdplIn ) ;
      return ( 0 ) ;
    }

    /* Fill the subface structure.*/
    Psf = PsubFaces + nBound ;
    Psf->PlBlock = Pbl ;
    Psf->llLBlockFile[0] = Psf->llLBlock[0] = minI ;
    Psf->llLBlockFile[1] = Psf->llLBlock[1] = minJ ;
    Psf->urLBlockFile[0] = Psf->urLBlock[0] = maxI ;
    Psf->urLBlockFile[1] = Psf->urLBlock[1] = maxJ ;
    
    Psf->PrBlock = NULL ;
    Psf->Pbc = find_bc( bcText, 1 ) ;

    /* Alas, Pbl-PPsubFaces starts with 0.*/
    Pbl->PPsubFaces[ nBound-1 ] = Psf ;
  }
  
  
  /* Make a new grid. */
  if ( !( Pgrid = make_grid () ) ) {
    printf ( " WARNING: malloc for the linked list of grids"
	     " failed in read_uns_dpl.\n" ) ;
    free_mb ( &Pmb ) ;
    fclose( FdplIn ) ;
    return ( 0 ) ;
  }
  else {
    /* Put the chunk into Grids. */
    Pgrid->mb.type = mb ;
    Pgrid->mb.Pmb = Pmb ;
    Pgrid->mb.mDim = 2 ;
    
    /* Make this grid the current one. */
    Grids.PcurrentGrid = Pgrid ;

    /* Count the number of elements and vertices in the entire block. */
    mb_count ( Pmb ) ;
    mb_degen_subfc ( Pmb ) ;
    mb_size ( Pmb ) ;

    /* Link bcs, get the bc bounding boxes. */
    mb_bcSubFc ( Pmb->PblockS, Pmb->mBlocks ) ;
    mb_bcBox ( Pmb->mDim ) ;
    
    return ( 1 ) ;
  }
}
