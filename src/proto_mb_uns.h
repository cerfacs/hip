/*
  proto_mb_uns.h:
  Functions for translating from mb to uns.


  Latest update:
  --------------
  12Mar18; intro doMapl
  7Apr13; add protos from mb_ele_vert.c

*/

/* mb_2uns.c. */
int cp_mb2uns ( const int doMap ) ;

/* mb_ele_vert.c */
int get_mbElems ( block_struct *PBL, const int mDim, vrtx_struct * const Pbase,
		  chunk_struct *Pchunk ) ;
int get_mbVerts ( block_struct *PBL, const int mDim, const int mUnknown,
		  vrtx_struct * const Pbase, chunk_struct *Pchunk,
                  const int doMap ) ;

/* mb_fc.c: */
int get_mbBndFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk ) ;
int get_mbIntFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk ) ;
int get_mbDegenFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk ) ;

/* Moved here to allow calling this from read_uns_cgns. */
/* read_mb_cgns.c */
int read_mb_cgns ( char *gridFile, char *solFile, const int skip, const int kBlock ) ;


/* write_mb_cut.c */
int write_mb_pts ( const mb_struct *Pmb, FILE *FptsOut, const int writeKeepNotCut ) ;
VALU_TYPE *cutEg2coor ( const DATA_TYPE *Pdata ) ;


/* mb_or_uns.c */
int write_av ( char *ProotFile, const char keyword[], int level ) ;
int write_cut ( char *ProotFile, const char *Pkeyword ) ;
int write_screen ( ) ;
int read_dpl( char *PdplFile, char *PsolFile ) ;
int write_dpl ( char *ProotFile, const char keyword[] ) ;
int write_pts ( char *PptsFile, const char *Pkeyword ) ;
	
int check_grid ( grid_struct *Pgrid ) ;
int interpolate ( int gridNr, char axi );

int split_quads ( splitType_e splitType ) ;
