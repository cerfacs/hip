/*
  buffer_uns:
  Refine elements in order to buffer hanging nodes. No new nodes on the surface
  of an element may be created at this stage.
  
  Last update:
  20Nov98; cleanup.
  
  This file contains:
  ---------
  estime_buf_elems_3D:
  buffer_elems_3D:
  
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;
extern const int bitEdge[MAX_EDGES_ELEM] ;
extern const refType_struct bufferType[MAX_FACES_ELEM*MAX_CHILDS_FACE+1] ;

/******************************************************************************

  estim_buf_elems_3D:
  Estimate worst case bounds for the elements, element2vertex pointers and vertices
  needed to buffer a grid in 3D.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void estim_buf_elems_3D ( const uns_s *pUns,
			  int *PmNewElems, int *PmNewElem2VertP, int *PmNewVerts,
			  int *PmNewBndFc, int *PmNewBndPatch ) {
  
  const chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  const elemType_struct *PelT ;
  int refdElem, mRefdEdges, kFace, mVxFc,
    nFcAe[MAX_VX_FACE], nCrossAe[MAX_VX_FACE-1], nFixAe, fixDiagDir, mBaseVx ;
  vrtx_struct const *PvxFace[2*MAX_VX_FACE+1] ;
  
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) 
    /* Loop over all cells. Find the refined faces and assume the worst
       case of adding an interior vertex to all of them. */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf ) {
        PelT = elemType + Pelem->elType ;
	refdElem = 0 ;
	
	/* Find all faces of this element. */
	for ( kFace = 1 ; kFace <= PelT->mFaces ; kFace++ ) {
          get_face_aE ( pUns, Pelem, kFace, &mBaseVx,
                        &mVxFc, PvxFace, nFcAe, nCrossAe, &nFixAe, &fixDiagDir ) ;
	  if ( PvxFace[8] ) {
	    /* This is a hanging face from the coarse side. */
	    refdElem = 1 ;
	    *PmNewElems += 4 ;
	    *PmNewElem2VertP += 4*5 ;
	  }
	  else if ( ( mRefdEdges = mVxFc - mBaseVx ) ) {
	    /* There are hanging edges. */
	    refdElem = 1 ;
            *PmNewElems += mRefdEdges+2 ;
            *PmNewElem2VertP += ( mRefdEdges+2 )*5 ;
          }
        }
	
	if ( refdElem )
	  (*PmNewVerts)++ ;
      }

  /* Baeh. Fix this later. */
  *PmNewVerts *= 1.2 ;
  *PmNewElems *= 1.2 ;
  *PmNewElem2VertP *= 1.2 ;
  return ;
}


/******************************************************************************

  buffer_elems_3D:
  Given the exterior surface polygonization, find the dissection of the interior
  into simplices. If none can be found, add a vertex at the interior.
  
  Last update:
  ------------
  11Sep19; intro args for testing for neg vols.
  16May19; use hip_err.
  : conceived.
  
  Input:
  ------
  pUns: grid
  pElem: element to buffer
  mVxHg # of hanging vertices on the element
  kVxHag: position of these hanging nodes, see get_drvElem_aE 
  PvxElem: 
  PsurfTri: surface triangulation for this element.
  doCheckVol: check children  for positive volumes

  Changes To:
  -----------
  PchildSpc: pointers to elem, vx, storage for the children elements


  Output:
  -----------
  *pmNegVol: non-zero if buffering produces neg. vols, zero otherw.
  *pDoesAddCtrVx: non-zero if buffering adds a new central vx.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int buffer_3D_elem ( uns_s *pUns, elem_struct *Pelem,
                     const int mVxHg, const int kVxHg[],
		     vrtx_struct *PvxElem[], const surfTri_s *PsurfTri,
		     childSpc_s *PchildSpc, const int doCheckVol,
                     int *pmNegVol, int *pDoesAddCtrVx ) {

  *pmNegVol = *pDoesAddCtrVx = 0 ;

  /* This element needs buffering. In case there is an old invalid
     PPchild, remove it. */
  Pelem->PPchild = NULL ;
  
  if ( Pelem->elType == hex ) {
    if ( !buffer_hex ( pUns, Pelem, mVxHg, kVxHg, PvxElem, PsurfTri, PchildSpc,
                       doCheckVol, pmNegVol, pDoesAddCtrVx) ) {
      sprintf ( hip_msg, "could not buffer elem %"FMT_ULG" in buffer_3D_elem.\n",
                Pelem->number ) ;
      hip_err ( fatal,0,hip_msg ) ;
    }
  }
  else {
    /* Flag should be set non-zero, but as we don't yet have 
       a buffering without added vx for non-hex, this would lead to 
       global refinement.
       *pmDoesAddCtrVx = 1 ;  */
    //if ( doCheckVol )
    //  hip_err ( warning, 1, "neg vol check from buffering not yet implem. for non-hex.");
    if ( !add_center_3D ( pUns, Pelem, PvxElem, PsurfTri, PchildSpc, doCheckVol, pmNegVol ) ) {
      sprintf ( hip_msg,
                "could not add center to buffer elem %"FMT_ULG" in"
                " buffer_3D_elem.\n",
                Pelem->number ) ;
      hip_err ( fatal,0,hip_msg ) ;
    }
  }

  Pelem->leaf = 0 ;
  Pelem->number = 0 ;
  return ( 1 ) ;
}



  /******************************************************************************

  add_child_3D_kVx:
  Add a child to a parent element given a list of vertices.
  Write the child and the pointer to the child at the next incremented location
  of the given pointers.
  
  Last update:
  ------------
  15may19; use hip_err.
  : conceived.
  
  Input:
  ------
  chType:      The type of the child.
  PchildSpc:   Pointers to last used storage and array sizes.

  Pelem:       The parent element.
  PvxPrt:      The forming and hanging vertices of the parent.
  kVxPrt[0..4]:The position of the forming vertices of the child in the parent.

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
  */

elem_struct *add_child_3D_kVx ( elType_e chType, childSpc_s *PchildSpc,
                                elem_struct *Pelem, vrtx_struct *PvxPrt[],
                                const int kVxCh[], const int doCheckVol, int *pmNegVol ) {
  
  elem_struct *Pchild = PchildSpc->PlstElem +1 ;
  const elemType_struct *PelT = elemType + chType ;
  int iVx ;
  double vol ;

# ifdef CHECK_BOUNDS
  if ( Pchild > PchildSpc->Pelem_max ||
       PchildSpc->PPlstChild > PchildSpc->PPchild_max ||
       PchildSpc->PPlstVx + elemType[chType].mVerts > PchildSpc->PPvrtx_max ) {
    hip_err ( fatal, 0, "space for children exhausted in add_child_2D.\n" ) ;
  }
# endif

  /* One more child. */
  Pelem->PrefType++ ;
  
  Pchild->elType = chType ;
  Pchild->PPvrtx = (PchildSpc->PPlstVx)+1 ;
  for ( iVx = 0 ; iVx < PelT->mVerts ; iVx++ )
    Pchild->PPvrtx[iVx] = PvxPrt[ kVxCh[iVx] ] ;

  /* Validate. Update counters. */
  PchildSpc->PlstElem++ ;
  PchildSpc->PPlstVx += elemType[chType].mVerts ;
  *(++(PchildSpc->PPlstChild)) = Pchild ;

  if ( !Pelem->PPchild )
    /* This is the first child for Pelem. */
    Pelem->PPchild = PchildSpc->PPlstChild ;

  /* Validate the child. */
  Pchild->number = Pchild-PchildSpc->PnewChunk->Pelem +
    PchildSpc->pUns->nHighestElemNumber ;
  Pchild->PPchild = NULL ;
  Pchild->leaf = 1 ;
  Pchild->term = 0 ;
  Pchild->Pparent = Pelem ;
  Pchild->root = Pchild->invalid = 0 ;

  double elVolCh ;
  if ( doCheckVol ) {
    elVolCh = get_elem_vol ( Pchild ) ;
    if( elVolCh < 0. ) {
      /* Invalid element. Can't cut this way. */
      (*pmNegVol)++ ;

      if ( verbosity > 6 ) { 
        sprintf ( hip_msg,
                  "non-positive volume %g in child element in add_child_3D_kVx.",
                  elVolCh ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
    }
  }
  return ( Pchild ) ;
}



/******************************************************************************

  add_center_3D:
  Buffer a 3D element by adding a center vertex. Average over corner vertices
  only.
  
  Last update:
  ------------
  15may19; use hip_err.
  14May97: cut out of elem2tetsNpyrs.
  
  Input:
  ------


  Changes To:
  -----------
  PvxElem[]: 
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_center_3D ( uns_s *pUns, elem_struct *Pelem, vrtx_struct *PvxElem[],
		    const surfTri_s *PsurfTri, childSpc_s *PchildSpc,
                    const int doCheckVol, int *pmNegVol ) {
  
  static int kFacet, kFace, kVxFacet[MAX_VX_FACE+1], kVxChild[MAX_VX_FACE+1],
    kVxCenter, mVxFacet ;

  /* Increment the number of children in add_child_3D_kVx. */
  Pelem->PrefType = bufferType ;

    
  /* Make a new vertex at the center of the element. */
  kVxCenter = surfTri_mVerts ( PsurfTri ) ;
  PvxElem[kVxCenter] = 
    adapt_uh_place_vx_elem ( Pelem, pUns, PchildSpc->PnewChunk, &(PchildSpc->PlstVrtx),
		    &(PchildSpc->PlstCoor), &(PchildSpc->PlstUnknown ) ) ;
  if ( !PvxElem[kVxCenter] ) {
    hip_err ( fatal, 0, "could not place center vertex in add_center_3D." ) ;
  }

  /* Loop over all faces and connect physical ones with the
     center vertex to form a new element. */
  kFacet = 0 ;
  while ( surfTri_nxt_facet ( PsurfTri, &kFacet, &kFace, &mVxFacet, kVxFacet ) )
    if ( mVxFacet == 3 ) {
      /* Form a tet. The tet will be built on the triangular face as 0-1-2,
	 ie. face 4. */
      kVxChild[0] = kVxFacet[0] ;
      kVxChild[1] = kVxFacet[1] ;
      kVxChild[2] = kVxFacet[2] ;
      kVxChild[3] = kVxCenter ;
      add_child_3D_kVx ( tet, PchildSpc, Pelem, PvxElem, kVxChild, doCheckVol, pmNegVol ) ;
    }
    else {
      /* Form a pyramid. The pyr will be built on the quad face as 0-3-2-1,
	 ie. face 1. */
      kVxChild[0] = kVxFacet[0] ;
      kVxChild[1] = kVxFacet[3] ;
      kVxChild[2] = kVxFacet[2] ;
      kVxChild[3] = kVxFacet[1] ;
      kVxChild[4] = kVxCenter ;
      add_child_3D_kVx ( pyr, PchildSpc, Pelem, PvxElem, kVxChild, doCheckVol, pmNegVol ) ;
    }
 

  double prtVol, chVol, totalVol = 0 ;
  int kCh, mCh ;
#define EPS_VOL 1.e-3
  *pmNegVol = 0 ;
  if ( doCheckVol ) {
    /* Calculate the parent volume. */
    prtVol = drvElem_volume ( PchildSpc->pUns, Pelem ) ;

    /* Loop over all children and accumulate the volume. */
    mCh = surfTri_mFacets ( PsurfTri ) ;
    for ( kCh = 0 ; kCh < mCh ; kCh++ ) {
      chVol = get_elem_vol ( Pelem->PPchild[kCh] ) ;
      if ( chVol < 0 ) {
        (*pmNegVol)++ ;
      }
      totalVol += chVol ;
    }
    if ( ABS( prtVol - totalVol )/prtVol > EPS_VOL ) {
      sprintf ( hip_msg,
                "incorrect volume of all children, %g vs %g in add_center_3D.\n",
                prtVol, totalVol ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  }
  
  return ( 1 ) ;
}



  
  
