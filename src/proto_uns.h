/*
  proto_uns.h:
  All headers of unstructured methods for prototyping.

  Latest update:
  ---------------
  26Aug24; intro write_stl.
  24Aug24, expanded specialTopo functions.
  6Sep19; use hprintf
  5Sep19; intro ret_s for _menu calls.
  3Sep19; add viz_elems_vtk
  9Jul19; rename to ADAPT_HIERARCHIC
  3Apr19; intro zone_elem_mod_perBcLayer.
  16Dec18; proto write_gmsh_lvl for debugging/testing.
  15Dec18; intro zone_elem_del
           new ucopy names for public uns_copy functions.
           add reserve/release_vx/elemMark routines.
  21Sep18; add doCheckBnd arg to write_gmsh.
  8Sep18; new arg to transform():  doCheck 
  6Sep18; intro reset_chk_vx_*_1chunk
          new arg doCheck for merge_uns.
  4Sep18; new interface to merge_uns, copy_per_part.
          intro face_all_mark3_vx
  9Jul18; rename zone_list_match to zone_match_list for consistency.
  8Jun18; add filename arg to viz_one_elem_vtk,
          new interface to llFc_add_facet, make_llFc.
  10Nov17; new arg to find_el_tree_walk.
           new interface to x_lin.
  2oct17; intro mg_write_hdf.
  1ct17; intro zone_elem_mod_type
  7Sep17: intro face_all_numbered_vx.
  16Aug17; add hmin/hmax to list_grid_info and list_grid_json output
  13July17; extract find_el_tree_walk from uns_interpolate.
  12Jul17; intro wrapper call uns_mg to all mg operations.
   5Jul17: rename llfc to llFc, which now uses only numbered elems. 
  17May17; additional arg in check_conn.
  12May17; rename rm_match_fc to rm_special_faces.
           intro geoType arg to make_llfc and make_llhybfc.
  21Apr17; update to interface of elem_is_convex.
  20Feb16; intro renameDuplPerBc to cp_oneUns.
  14Dec16; intro bndPatch_area.
  21Sep16; new arg to increment_vx_number
  19Sep16; intro get_used_sizeof_llEnt.
  18Sep16; intro merge_vx_face.
           pass pGrid rather than pUns to mmg_adapt.
  15Sep16; make count_vx_mark public.
  12Sep16; rename number_uns_elemFromVerts into number_uns_elems_by_type
  10Sep16; intro make_llHybTrifc
  1Jul16; intro incement_vx_number_bc.
          new interface to check_uns, check_elems.
  30Jun16; intro decimate_mmg.
  14Avr16; intro adapt_mmg_2d, renaming of adapt_mmg into adapt_mmg_3d.
  6Mar16; intro adapt_mmg.
  17Feb16; intro init_uns_vrtElem.
  15Feb16; minNormEl also used in uns_mg, make public.
           intro viz_one_elem_vtk
  12Feb16; add args to maxAngle.
  7Feb16; remove pUns from arg list for unset_per.
  9Jan15; intro extend_chunk
  22Sep14; promote args of zone_elem_mod_range to ulong_t.
  27Jun14; rename zone_elem_add_all to zone_elem_mod_all, mofified interface
           intro zone_list, zone_list_all.
  23Feb14; drop const from mVarFl in read_gmsh.
  15Dec13; replace x_axis_verts with axis_verts.
  9Jul13; intro loop_bc_expr.
  7Jul13; change interface to make_uns_grid,
          add zone_elem_add_range. 
  6Apr13; add missing prototypes thrown up by using petty compiler.
  1Apr12; intro show_fc2el_elel.
  17Dec11; add zone_elem_add_all.
  17Jul11; intro zone.c calls.
  9Jul11; intro uns_face_normal_co, 
          move base-element wrapper for number_uns_elemFromVerts into uns_number.
  3Apr11; modify interface to number_uns_vert_bc
  22Dec10; intro bcPatch_nrm_gc
  18Dec10; intro bc_is_l/u/per
           intro fix_per_setup.
           intro realloc_unknowns.
  14Sep10 ; add mUnknown to interface of make_uns_grid.
  25May09; pass keyword into read_n3s.
  3April08; new interface for set_per_corners.
            pass keyword into read_hdf5*.
  16Dec08; intro doAxis into mark_uns_vertBc.
  13May07; intro make_uns_grid, read_hdf5.

*/



/* adapt.c: */
int adapt_funk ( char keyword[],
		 const double derefFct,
		 const double ref,
		 const double edgeRatio ) ;
int adapt_hierarchical_sensor ( char keyword[], const double deref, const double ref, const int iso ) ;

/* adapt_meth.c */
void number_uns_elem_leafs ( uns_s *pUns ) ;
void printelal ( const elem_struct *Pelem ) ;

/* adapt_mmg.c */
void adapt_mmg (grid_struct *pGrid, char* argLine ) ;

/* adapt_res.c */
int adapt_vf ( char *adjFile, double deref, double ref,
               int iso, int mSmooth, int mSmoothShock, int abs ) ;


/* adEdge.c: */
void mark_uns_vertFromAdEdge ( uns_s *pUns ) ;


/* buffer_uns.c: */
int buffer_uns ( uns_s *pUns ) ;
void debuffer_uns ( uns_s *pUns ) ;

/* check_lic.c: */
int  check_lic() ;


/* check_uns.c: */
ret_s calc_elem_qual_stats ( char *argLine ) ;

int check_uns ( uns_s *pUns, const int check_lvl ) ;
double check_angles ( uns_s *pUns, const int doPrint ) ;
int check_elems ( uns_s *pUns, int *Pchange, const int check_lvl ) ;
int check_vol ( uns_s *pUns ) ;
int check_valid_uns ( uns_s *pUns, const int checkVol ) ;
ret_s check_hmin ( uns_s *pUns, char filter[], const double dVal ) ;



int update_vol ( uns_s *pUns ) ;

int get_degenEdges ( const elem_struct *pElem, const chunk_struct *Pchunk,
                     double *PhMin, double *PhMax, double *pDist,
                     const int check_pVx, const double epsOverlapSq );
int set_degenVx ( uns_s *pUns ) ;
void update_h_vol ( uns_s *pUns, ulong_t *pmNegVols ) ;

/* cp_uns2D_uns3D.c: */
int cp_uns2D_uns3D ( double zBeg, double zEnd, const int mSlice, char axis ) ;
int rot_3d ( char rot2[] ) ;

/* edge_manage.c */
llEdge_s *make_llEdge ( uns_s *pUns, cpt_s cptVxMax, ulong_t mEdges, ulong_t dataSize,
                        llEdge_s *pllEdge, void **ppEdgeData ) ;
void free_llEdge ( llEdge_s ** ppllEdge ) ;
llEdge_s *make_llEdge_grid ( uns_s *pUns, ulong_t *pmEdges,
                             ulong_t dataSize, void **ppEdgeData ) ;
llEdge_s *make_llEdge_bnd ( uns_s *pUns, ulong_t *pmEdges,
                            ulong_t dataSize, void **ppEdgeData ) ;

void del_edge ( llEdge_s *pllEdge, ulong_t nDelEdge ) ;

int get_edge_vrtx ( const llEdge_s *pllEdge,
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch ) ;
int get_elem_edge ( const llEdge_s *pllEdge,
                    const elem_struct *pElem, int kEdge,
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch ) ;
int add_edge_vrtx ( llEdge_s *pllEdge, void **ppEdgeData, 
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch, int *pNew ) ;
int add_elem_edge ( llEdge_s *pllEdge, void **ppEdgeData,
		    const elem_struct *pElem, const int kEdge,
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch, int *pNew ) ;

int loop_edge_vx ( const llEdge_s *pllEdge, const vrtx_struct *pVx,
		   int *pn1stEdge, int *pnEdge, int *pSide ) ;

ulong_t get_number_of_edges ( const llEdge_s *pllEdge, ulong_t *pnLstEdge );
int get_sizeof_llEdge ( const llEdge_s *pllEdge ) ;
int show_edge ( const llEdge_s *pllEdge, const int nEdge,
		vrtx_struct **ppVrtx1, vrtx_struct **ppVrtx2 ) ;


void listEdge ( const uns_s *pUns, llEdge_s const * const pllEdge,
                void (*printData) ( const uns_s*, void const * const ) ) ;
void listEdgeVx ( const uns_s *pUns, llEdge_s const * const pllEdge,
                  const vrtx_struct *pVrtx,
                  void (*printData) ( const uns_s*, void const*const ) ) ;
void printEdge ( const uns_s *pUns, llEdge_s const * const pllEdge, int nEdge,
                 void (*printData) ( const uns_s*, void const * const ) ) ;

/* decimate_mmg.c: */
int decimate_mmgs_3d ( uns_s *pUns, char* argLine ) ;


/* fix_uns_elem.c: */
int fix_elem ( elem_struct *Pelem, int mDegenEdges,
	       chunk_struct *Pchunk, uns_s *pUns, chunk_struct *PlstChunk, 
	       elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert,
	       vrtx_struct **PPlstVrtx, double **PPlstCoor ) ;

int hex2prism ( elem_struct *Pelem, chunk_struct *Pchunk, uns_s *pUns ) ;
int elem2tetsNpyrs ( elem_struct *Pelem, chunk_struct *Pchunk, uns_s *pUns,
		     const int mDegenEdges,
		     chunk_struct *PlstChunk, elem_struct **PPlstElem,
		     vrtx_struct ***PPPlstElem2Vert,
		     vrtx_struct **PPlstVrtx, double **PPlstCoor ) ;
elem_struct *make_tet ( elem_struct *Pelem, vrtx_struct **PPvxFc[], int fcType,
		        int kDgEdge[],
		        vrtx_struct *Pvrtx, chunk_struct *PlstChunk, 
		        elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert ) ;
elem_struct *make_pyr ( elem_struct *Pelem, vrtx_struct **PPvxFc[], int fcType,
		        vrtx_struct *Pvrtx, chunk_struct *PlstChunk, 
		        elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert ) ;

int check_elem_space ( int eleType, chunk_struct *PChunk, 
		       elem_struct **PPlstElem, vrtx_struct ***PPPlstElem2Vert ) ;
int check_vrtx_space ( chunk_struct *Pchunk, vrtx_struct **PPlstVrtx,
		       double **PPlstCoor, const int mDim ) ;

int fix_boundFace ( const chunk_struct *Pchunk, elem_struct *Pelem,
		    int kFcNewElemFc[], elem_struct *PnewElemFc[] ) ;
int fix_intFace ( const chunk_struct *Pchunk, elem_struct *Pelem,
		  int kFcNewElemFc[], elem_struct *PnewElemFc[] ) ;
int fix_matchFace ( const uns_s *pUns, const int mDim, elem_struct *Pelem,
		    int kFcNewElemFc[], elem_struct *PnewElemFc[] ) ;
void update_face ( elem_struct **PPelem, int *PnFace, 
		   int kFcNewElemFc[], elem_struct *PnewElemFc[] ) ;

int simplify_all_elems ( uns_s *pUns, int quad22tris, splitType_e splitType ) ;
int simplify_one_elem ( elem_struct *pElDeg, elem_struct *pElSim ) ;

/* get_edge_weights.c: */
int make_edge_normals ( uns_s *pUns,
			double **ppEdgeNorm, ulong_t *pmEdges,
			ulong_t *pmSymmEg, ulong_t *pmAntimEg, ulong_t *pmElemEg ) ;
void printEgwt ( uns_s *pUns, const double *pEdgeNorm, const ulong_t nVx ) ;
void checkEgwt ( uns_s *pUns, const double *pEdgeNorm, const ulong_t nVx ) ;


/* init_uns.c: */
void init_uns_vrtElem () ;
void init_elem ( elem_struct *pEl, elType_e elType, 
                 ulong_t nEl, vrtx_struct **ppVx ) ;
void init_bndFc ( bndFc_struct *pBf ) ;

void init_slidingPlaneSide ( slidingPlaneSide_s *pSlidingPlaneSide ) ;
void init_uns_core_data () ;
void init_uns ( uns_s *pUns, grid_struct *pGrid  ) ;
void init_match ( match_s *pMatch ) ;

/* merge_uns.c: */
ret_s add_uns_grid ( uns_s *pUnsTo, uns_s *pUnsAdded ) ;
ret_s add_uns_grids ( char *line ) ;

int merge_vrtx_chunk ( chunk_struct *pChunk, int mVxInt, int mVrtx ) ;
int merge_uns ( uns_s *pUns, const int doUseVxMark3, const int doCheck ) ;
int merge_regions ( uns_s *pUns, const int mReg, const int iReg[], const int doReset ) ;
VALU_TYPE *vrtx2coor( const DATA_TYPE *Pdata ) ;
/* Attempt to optimise the code? But how can a function that is only
   supplied at runtime be inlined? Anyways some MAC compiler issues a warning.
 inline VALU_TYPE *vrtx2coor( const DATA_TYPE *Pdata ) __attribute__((always_inline));
 */
int merge_vx_face ( const int mDim, 
                   elem_struct *pEl0, int kFc0,
                    elem_struct *pEl1, int kFc1 ) ;


/* read_gmsh: */
ret_s read_gmsh ( char* argLine ) ;



/* read_uns_avbp.c: */
int read_uns_avbp ( char masterFile[TEXT_LEN], int adfSol ) ;
int read_avbp_sol ( FILE *Fsol, uns_s *pUns, chunk_struct *Pchunk ) ;
int read_avbp_coor ( FILE *Fcoor, uns_s *pUns, chunk_struct *Pchunk ) ;
/*
int read_avbp_conn ( FILE *Fconn, uns_s *pUns, chunk_struct *Pchunk,
vrtx_struct *PbaseVx ) ;*/

int read_uns_avbp4 ( char masterFile[TEXT_LEN] ) ;

/* read_uns_avbp_bound.c: */
int read_avbp_asciiBound_4p7 ( FILE *fAscii, uns_s *pUns ) ;
int read_avbp_asciiBound ( FILE *Fascii, uns_s *pUns ) ;
int read_avbp_exBound ( FILE *FexBound, uns_s *pUns, chunk_struct *pChunk ) ;
int read_avbp_inBound ( FILE *FinBound, uns_s *pUns, chunk_struct *pChunk ) ;

/* read_uns_cedre.c */
int read_uns_cedre ( char *flName ) ;

/* read_uns_centaur.c */
int read_uns_centaur ( char* fileName ) ;

/* read_uns_cfdrc.c: */
int read_uns_cfdrc ( char* fileName, const int flip_negative_quads ) ;


/* read_uns_dpl.c: */
int read_uns_dpl ( FILE *FdplIn ) ;
int read_uns_dpl3d ( FILE *FdplIn, FILE *FsolIn ) ;


/* read_uns_flite.c: */
int read_uns_flite ( char *cFroFile, char *cGriFile, char *cBcoFile ) ;

/* read_ensight.c */
int read_ensight ( char *caseFile ) ;

/* read_fluent.c */
int read_fluent ( const char *keyword, char *flCase, char* flDat ) ;

/* read_hdf5.c */
int read_hdf5_sol ( uns_s *pUns, char *fileName ) ;
int read_hdf5 ( char* argLine ) ;


/* read_hyd.c */
int read_hyd_sol ( uns_s *pUns, char *fileName ) ;
int read_hyd ( char* argLine ) ;


/* read_uns_cgns.c */
ret_s read_uns_cgns ( char* argLine ) ;


/* read_uns_n3s.c */
int read_uns_n3s ( char *gridFile, char *solFile, const char* keyword ) ;


int read_uns_saturne ( char *gridFile, char *masterFile, int saveNr ) ;

/* uns_2tet.c */
int uns_2tet () ;


/* uns_color.c: */
color_s *color_vx ( uns_s *pUns, llEdge_s *pllEdge ) ;

/* uns_copy.c */
uns_s *cp_uns_zone ( uns_s *pUns2, uns_s *pUns0,
                     const int iZone2, const int iZone0, 
                     const int renameDuplPerBc,
                     const int doUnknown ) ;
uns_s *ucopy_oneUns ( grid_struct *pGr0, int nCp, 
                   grid_struct **ppGrCp, int *pmVxNumbered,
                   const int renameDuplPerBc ) ;
uns_s *ucopy_uns2uns ( int mZ, const transf_e tr_op, const double dval[] ) ;
uns_s *ucopy_per_part ( uns_s *pUns0,  const int iZone0, 
                        uns_s *pUns2, const int iZone2,
                        perBc_s *pPerBc, const int perDir,
                        const int doUnknowns, const int useMark ) ;
uns_s *ucopy_elem_region ( uns_s *pUns0,  const int iReg0, 
                      uns_s *pUns2, const int iRebg2,
                      const int renameDuplPerBc,
                      const int doUnknown,
                      const int useMark ) ;


/* uns_drvElem.c: */
double drvSide_volume ( const uns_s *pUns,
		        const elem_struct *Pelem, const int nSide,
		        double volVec[] ) ;
int get_drvSide_aE ( const uns_s *pUns,
		     const elem_struct *Pelem, const int nSide,
		     int *PmVxSide, vrtx_struct *PvxSide[],
		     int *pFixDiag, int *pDiagDir ) ;
int drvSide_normls ( const elem_struct *pElem, int mSides, const int kSide[],
                     // this upsets gcc 11.3
		     //const int mVxHg, const int kVxHg[MAX_ADDED_VERTS],
		     const int mVxHg, const int *kVxHg,
		     vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		     // int fixDiag[MAX_FACES_ELEM+1], int diagDir[MAX_FACES_ELEM+1],
		     int *fixDiag, int *diagDir,
		     double nodeNorm[][MAX_DIM] ) ;

double drvElem_volume ( const uns_s *pUns, const elem_struct *Pelem ) ;
int get_drvElem_aE ( const uns_s *pUns, const elem_struct *Pelem,
		     int *PmVxHg,
		     //int kVxHg[], vrtx_struct *PhgVx[],
		     //int fixDiag[], int diagDir[]
		     int *kVxHg, vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		     int *fixDiag, int *diagDir
                     ) ;

int drvElem_normls ( const elem_struct *Pelem, const int mVxHg,
		     // const int kVxHg[],
		     const int *kVxHg,
		     vrtx_struct *PhgVx[],
		     // int fixDiag[], int diagDir[],
		     int *fixDiag, int *diagDir,
		     double nodeNorm[][MAX_DIM] ) ;

void printDrvFc ( const uns_s *pUns, const elem_struct *pElem, int kFace ) ;

/* uns_generate.c */
ret_s uns_generate ( double ll[2], double ur[2], int mI, int mJ ) ;

/* uns_geom.c */
void attach_chunk_vrtxVol ( uns_s *pUns ) ;
double compute_vrtxVol ( uns_s *pUns, int nVar ) ;
int edgeLen_from_vol ( uns_s *pUns, const int nVar, const int mDim ) ;
ret_s calc_edgeLen ( uns_s *pUns, const int kVar, const char operation[] ) ;
void free_chunk_vrtxVol ( uns_s *pUns ) ;

int x_lin ( const double *pEg0, const double *pEg1,  double *pt,
            const double *pLn0, const double *pLn1,  double *ps ) ;
int x_line3d ( double a0[], double a1[], double b0[], double b1[], 
               double *ps, double *pt ) ;
int x_tri ( const double *pCo[3], const double *pBeg, const double *pEnd, 
            double xInt[3], double al[3] ) ;

int line_cross_face ( const int mDim, const int mVxFc, vrtx_struct *pVxFc[],
                      double *pBeg, double *pEnd, double *pX ) ;

double point_dist_face ( const elem_struct *pEl, const int kFc, 
                         const double *pVxCo, double *phEdge ) ;
int min_dist_face_el ( const double *pCo, const elem_struct *pElem, 
                      double *pMinDist, const elem_struct **ppElMin, int *pkFc ) ;
void min_dist_face_bnd ( const uns_s *pUns, const double *pCo, 
                    double *pMinDist, const elem_struct **ppElMin, int *pkFc ) ;
void min_dist_face_elems ( uns_s *pUns, const double *pCo, 
                      double *pMinDist, 
                      const elem_struct **ppElMin, int *pkFc ) ;


void get_uns_box ( uns_s *pUns ) ;
void get_chunkBox ( chunk_struct *Pchunk, const int mDim ) ;
void get_elem_facets( elem_struct *pElem, int mFacets[], 
                      int mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE], 
                      vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ) ;
int get_uns_face ( const elem_struct *Pelem, const int nFace,
		    vrtx_struct **PPvxFc[], int *PfcType ) ;

void bndPatch_area ( const int mBc, const int mDim, const bndVxWt_s *bWt,
                     double bndPatchArea[] ) ;
int  bcPatch_nrm_gc ( uns_s *pUns, const int nBc, 
                      double bcNrm[MAX_DIM], double bcGC[MAX_DIM],
                      double *pbcArea ) ;

void uns_face_normal_co ( const int mDim, const int mVertsFace, 
                          const double *pCoVx[MAX_VX_FACE], 
                          double fcNorm[MAX_DIM], int *PmTimesNormal ) ;
int uns_elem_normls ( const elem_struct *Pelem, double nodeNorm[][MAX_DIM] ) ;
int uns_face_normal ( const elem_struct *Pelem, const int kSide,
		      double fcNorm[MAX_DIM], int *PmTimesNormal ) ;
int uns_face_normal_list ( const elem_struct *Pelem, const int kSide, 
                           int *pmVxFc, const double *pCoVx[MAX_VX_FACE], 
                           double fcNorm[MAX_DIM], int *PmTimesNormal ) ;

double get_lrgstFaceTwist ( const elem_struct *pElem, int *pkFcTwistMin ) ;
double get_face_dist ( elem_struct *pElem, int kSide, const double *pCo,
       double *phEdge ) ;
double get_faceTwist ( const elem_struct *pElem, const int kFc ) ;
double get_edge_len ( const elem_struct *pElem, const int kEg ) ;
double get_elem_vol ( const elem_struct *Pelem ) ;
ret_s calc_minmax_elem_vol_with_vx
( const uns_s *pUns, double **ppminVolElemWithVx, double **ppmaxVolElemWithVx ) ;

double calc_elem_property ( elem_struct *pElem, const ep_type epType ) ;

ulong_t flip_negative_volumes ( uns_s *pUns ) ;
void elem_flip_vol  ( elem_struct *pElem ) ;

int elem_is_convex ( const elem_struct *pElem, double volMin, double *pVol,
                     int *pElemIsNotCollapsed ) ;

double maxAngle ( const elem_struct *pElem, double *pElemVol, int *pIsCollapsed,
                  double *phMin, double *pMaxDihAngle, double *pMaxFcAngle );
double maxMinAngle ( const elem_struct *pElem, double *pElemVol, int *pIsNotColl,
                     double *phMinSq, double *phAvg,
                     double *pMaxDihAngle, double *pMaxFcAngle,
                     double *pMinAngle ) ;
double get_face_lrgstAngle ( const elem_struct *pElem, int kFace, int *pkVxFc ) ;


void unflag_vx ( uns_s *pUns ) ;
void use_vx_flag ( uns_s *pUns, char *usedBy ) ;
int el_has_flag1_vx ( elem_struct *pElem ) ;
int is_vx_flagged ( const vrtx_struct *pVx ) ;
void free_vx_flag ( uns_s *pUns ) ;
int uns_flag_vx_vol ( uns_s *pUns, char *usedBy ) ;
int is_in_geo ( double *pCo, const int mDim, const geo_s *pGeo ) ;

ret_s cart2cyl ( const double xyz[], int kDim, const int mDim,
                 double *pR, double *pThRad  ) ;

void uns_flag_vx_geo ( uns_s *pUns, const geo_s *pGeo, char *usedBy ) ;
int uns_flag_vx_bnd ( uns_s *pUns, const int nBc, ulong_t mFc[5], char *usedBy ) ;

int is_elem_in_hrb ( const elem_struct *pElem, const hrbs_s *pBoxes ) ;
void mark_elems_in_hrb ( uns_s *pUns, hrbs_s *pHrbs ) ;


void elem_grav_ctr ( const elem_struct *pElem,
                     double elemGC[], const elemType_struct **ppElT,
                     int *pmVxEl, const vrtx_struct *pVxEl[] ) ;
void face_grav_ctr ( const elem_struct *pElem, const int kFace, 
		     double faceGC[], const faceOfElem_struct **ppFoE,
                     int *pmVxFc, const vrtx_struct *pVxFc[] ) ;
void edge_grav_ctr ( const elem_struct *pElem, const int kEdge,
		     double edgeGC[MAX_DIM] ) ;
void med_normal_edge_2D ( const elem_struct *pElem, double elemGC[MAX_DIM],
                          const int kEdge, double medNorm[MAX_DIM] ) ;

int face_in_elem ( const elem_struct *pElem, const int mVxFace, const ulong_t nVxFc[] ) ;

int make_mp_bndVx ( uns_s *pUns, mp_bndVx_s mpVx[] ) ;
int make_bndVxWts ( uns_s *pUns, bcGeoType_e geoType,
                    bndVxWt_s bWt[], ulong_t *pmAllBndVx, 
                    const int doFV, const int doWts ) ;
double *calc_wall_dist ( uns_s *pUns, bndVxWt_s bWt[], int mAllBndVx ) ;

void transform ( grid_struct *pGrid, const transf_e tr_op,
                 const double dval[],
                 const int useMark, const int doCheck ) ;





/* uns_int_line.c: */
double uns_int_line ( double xyzBeg[3], double xyzEnd[3], char *fileName, char *var ) ;
int integrate_rectangle ( double swf[], double sef[], double nwf[], double swr[], 
                          int mE, int mN, char *fileName, char *var) ;

/* uns_int_plane.c: */
int uns_int_plane ( double xP[3][3] ) ;
int uns_int_bnd ( const double z ) ;

/* uns_interpolate.c */
int minNormEl( const elem_struct *pElem, const int mDim, const int mVx,
               const double xp[MAX_DIM], const double tol, const int fixNeg, 
               double alpha[MAX_VX_ELEM] ) ;
kdroot_struct *kd_intp_tree ( uns_s *pUnsFrom, uns_s *pUnsTo, const int axi ) ;
int uns_interpolate ( uns_s *pUnsFrom, uns_s *pUnsTo, char cAxi ) ;




/* uns_listNprint.c */
int list_grid_info ( int mDim, ulong_t mEl, ulong_t mConn, 
                     ulong_t mVx, ulong_t mBndFc, int mBc,
                     double volGrid, double volMin, double hMin, double hMax,
                     char *bcLabel, 
                     size_t lbl_len, double bndPatchArea[], 
                     double llBox[], double urBox[],
                     double llBoxCyl[], double urBoxCyl[], int isPer,
                     specialTopo_e topo ) ;
int list_grid_json ( char gridFile[], int mDim, ulong_t mEl, ulong_t mConn,
                     ulong_t mVx, ulong_t mBndFc, int mBc,
                     double volGrid, double volMin, double hMin, double hMax,
                     char *bcLabel, 
                     size_t lbl_len, double bndPatchArea[],
                     double llBox[], double urBox[],
                     double llBoxCyl[], double urBoxCyl[], int isPer );


void list_uns_bc ( const grid_struct *Pgrid, char *keyword ) ;

void printel ( const elem_struct *Pelem ) ;
void printelco ( const elem_struct *Pelem ) ;
void printvxco ( const vrtx_struct *Pvx, const int mDim ) ;
void printco ( double *Pcoor, const int mDim ) ;
void printfc ( const elem_struct *Pelem, const int nFace ) ;
void printfcco ( const elem_struct *Pelem, const int nFace ) ;
void printeg ( const elem_struct *Pelem, const int kEdge ) ;
void printegco ( const elem_struct *Pelem, const int kEdge ) ;

vrtx_struct * findpvxco ( const uns_s *pUns, const int mDim, 
                          const double x, const double y, const double z ) ;
vrtx_struct *findpvx ( const uns_s *pUns, const int nVx ) ;
void findelvx ( const uns_s *pUns, const int nVx ) ;
void findelvxViz ( const uns_s *pUns, const int nVx, char *fileName, const int iZone ) ;
void findel2vx ( const uns_s *pUns, const int nVx1, const int nVx2 ) ;
void findel3vx ( const uns_s *pUns,
		 const int nVx1, const int nVx2, const int nVx3 ) ;
void findel4vx ( const uns_s *pUns,
		 const int nVx1, const int nVx2, const int nVx3, const int nVx4 ) ;

void find_bndFc_el ( const uns_s *pUns, const elem_struct *Pelem ) ;
bndFc_struct *find_bndFc_pVx ( uns_s *pUns, const vrtx_struct *Pvrtx, int nr,
                            int doPrint ) ;

bndFc_struct *find_bndFc_pVx ( uns_s *pUns, const vrtx_struct *Pvrtx, int nr,
                               int doPrint ) ;

  bndFc_struct *find_bndFc_pVxList ( uns_s *pUns, const vrtx_struct *ppVx[], int mVx ) ;
  
bndFc_struct *find_bndFc_nVx ( uns_s *pUns,
                               const int nVx0,
                               const int nVx1,
                               const int nVx2,
                               const int nVx3 ) ;

int add_viz_el ( const elem_struct *pElem, const elem_struct ***pppElViz, ulong_t *pmVizEl );

ret_s vis_elems ( char argline[] );

//int viz_one_elem_glut ( FILE *glutFile, const elem_struct *pElem );
int elType2vtk ( const elType_e elT ) ;
void viz_one_elem_vtk ( char *fileName, const elem_struct *pElem, const double *pCoor ) ;
void viz_elems_vtk ( char *fileName, 
                    int mEls, const elem_struct **ppElem, const double *pCoor ) ;
void viz_elems_vtk0 ( char *fileName, 
                      int mEls, const elem_struct **ppElem, const double *pCoor );

/* uns_llFc.c: */
int show_fc2el_elel ( const fc2el_s *pfc2el, const int nFc,
                      elem_struct **ppElem0, int *pkFc0, 
                      elem_struct **ppElem1, int *pkFc1 ) ;
int elem2vx_from_fc ( int mFlFc, flFc_s *pFace, int mElems, elem_struct *pElem, 
                      vrtx_struct *pVrtx ) ;

llVxEnt_s *make_llFc ( uns_s *pUns, bcGeoType_e geoType,
                       fc2el_s **ppfc2el,
                       int doWarn, int doRemove, int doListBnd,
                       ulong_t *pmBndFcBecomeInt, ulong_t *pmIntFcDupl, ulong_t *pmBndFcDupl ) ;

llVxEnt_s *make_llHybTriFc ( uns_s *pUns,
                             fc2el_s **ppfc2el,
                             int doWarn, int doRemove, int doListBnd,
                             ulong_t *pmFcBecomeInt, ulong_t *pmFcDupl, 
                             ulong_t *pmFcRemoved, ulong_t *pmFcUnMatched ) ;
llVxEnt_s * make_llInterFc_vxMark ( uns_s *pUns, 
                                    fc2el_s **ppfc2el,
                                    const int kMark ) ;

int create_matchFc_llVx ( uns_s *pUns, const int useMark3,
                          const char bcLbl_12[MAX_BC_CHAR],
                          const char bcLbl_13[MAX_BC_CHAR],
                          const char bcLbl_23[MAX_BC_CHAR]
                          ) ;

int match_bndFcVx ( uns_s *pUns ) ;
int match_bvx2vx ( uns_s *pUns ) ;

int rm_special_faces ( uns_s *pUns ) ;
int check_conn ( uns_s *pUns, int *pBcGeoType_change )  ;
int make_elGraph ( uns_s *pUns, ulong_t *pmXAdj, ulong_t **ppXAdj, 
                   ulong_t *pmAdjncy, ulong_t **ppAdjncy ) ;

/* uns_loop.c: */
int loop_chunks ( const uns_s *pUns, chunk_struct **ppChunk ) ;
int loop_elems ( const uns_s *pUns, chunk_struct **ppChunk,
		 elem_struct **ppElemFirst, elem_struct **ppElemLast ) ;
//		 elem_struct **ppElemFirst, int *pnElemFirst,
//		 elem_struct **ppElemLast, int *pnElemLast ) ;
int loop_elems_type ( const uns_s *pUns, const elType_e elType,
		      chunk_struct **ppChunk, elem_struct **ppElem ) ;
int loop_verts ( const uns_s *pUns, chunk_struct **ppChunk,
		 vrtx_struct **ppVrtxFirst, int *pnVrtxFirst,
		 vrtx_struct **ppVrtxLast, int *pnVrtxLast ) ;
int loop_verts_cont ( const uns_s *pUns, chunk_struct **ppChunk,
                      vrtx_struct **ppVrtxFirst, vrtx_struct *pVxPrev,
                      vrtx_struct **ppVrtxLast ) ;

int loop_bndPatches ( const uns_s *pUns, chunk_struct **ppChunk,
		      bndPatch_struct **ppBndPatchFirst,
		      bndPatch_struct **ppBndPatchLast ) ;
int loop_bndFaces ( const uns_s *pUns, chunk_struct **ppChunk,
		    bndPatch_struct **ppBndPatch,
		    bndFc_struct **ppBndFcFirst, bndFc_struct **ppBndFcLast) ;
int loop_bndPatches_bc ( const uns_s *pUns, const int nBc,
			 bndPatch_struct **ppBndPatchBc ) ;
int loop_bndFaces_bc ( const uns_s *pUns, const int nBc, bndPatch_struct **ppBndPatchBc,
		       bndFc_struct **ppBndFcFirst, bndFc_struct **ppBndFcLast ) ;
int loop_bc_expr ( bc_struct **ppBc, const char *expr ) ;
bc_struct *loop_bc_uns_expr ( uns_s *pUns, int *piBc, const char *expr ) ;

int loop_edges_face ( const int mVxFace, vrtx_struct **pVxFace[], int *pkEg,
                      int *pnVx0, int *pnVx1 ) ;



/* uns_mark.c */

int reserve_vx_markN ( uns_s *pUns, const int markNo, const char* useBy ) ;
int release_vx_markN ( uns_s *pUns, const int markNo ) ;
int reserve_elem_markN ( uns_s *pUns, const int markNo, const char* useBy ) ;
int release_elem_markN ( uns_s *pUns, const int markNo ) ;;

void reset_vx_markN ( uns_s *pUns, const int markNo ) ;
void reset_vx_mark_1chunk ( chunk_struct *pChunk ) ;
void reset_vx_mark2_1chunk ( chunk_struct *pChunk ) ;
void reset_vx_mark3_1chunk ( chunk_struct *pChunk ) ;
void reset_vx_mark_all_1chunk ( chunk_struct *pChunk ) ;
void reset_vx_per_1chunk ( chunk_struct *pChunk ) ;
void check_vx_mark_1chunk ( chunk_struct *pChunk ) ;

void reset_vx_mark ( uns_s *pUns ) ;
void reset_vx_mark2 ( uns_s *pUns ) ;
void reset_vx_mark3 ( uns_s *pUns ) ;
void reset_vx_mark_all ( uns_s *pUns ) ;
void reset_vx_per ( uns_s *pUns ) ;
void check_vx_mark ( uns_s *pUns ) ;

int vx_set_markN ( vrtx_struct *pVx, const int markNo ) ;
int vx_has_markN ( const vrtx_struct *pVx, const int markNo ) ;


void reserve_elem_mark ( uns_s *pUns, const int kMark, const char* useBy ) ;
int reserve_next_elem_mark ( uns_s *pUns, const char* useBy ) ;
int release_elem_mark ( uns_s *pUns, const int kMark ) ;
void reset_all_elem_mark_rane ( uns_s *pUns, const int kMarkBeg, const int kMarkEnd ) ;
void reset_all_elem_all_mark ( uns_s *pUns ) ;
void reset_all_elem_mark ( uns_s *pUns, const int kMark ) ;

void reset_elem_mark_1chunk ( chunk_struct *pChunk, const int kMark ) ;
void reset_elem_all_mark_1chunk ( chunk_struct *pChunk ) ;


void reset_elem_mark ( elem_struct *pElem, const int kMark ) ;
void reset_elem_mark_range ( elem_struct *pElem, const int kMarkBeg, const int kMarkEnd ) ;
void reset_elem_all_mark ( elem_struct *pElem ) ;

void set_elem_mark_val ( elem_struct *pElem, const int kMark, const int val ) ;
int elem_has_marks ( const elem_struct *pElem, const int mMarks, const int iMark[] ) ;
int elem_has_mark ( const elem_struct *pElem, const int kMark ) ;
int elem_mark2int ( const elem_struct *pElem ) ;
void elem_int2mark ( elem_struct *pElem, int intMrk ) ;
void set_elem_mark ( elem_struct *pElem, const int kMark ) ;

int mark_elem_type ( uns_s *pUns, const int kMark, 
                     const elType_e elTBeg, const elType_e elTEnd ) ;
ulong_t mark_elem_perBcLayer ( uns_s *pUns, int kElMark[2],
                               const int mLayer,
                               const elType_e elTypeLo, const elType_e elTypeHi,
                               int *pmBcPer, int nrBcPer[] ) ;
int mark_elem_remaining ( uns_s *pUns, const int kMarkTrue, const int kMarkFalse, 
                          const elType_e elTBeg, const elType_e elTEnd,
                          const int kMark2set ) ;
int mark_vx_elem_match_per ( uns_s *pUns0, const match_s *pMultMatch,
                             int kVxMark2Set, int kVxMarkWork[2],
                             const int doReset, const int doPer ) ;
ulong_t elem_invalidate_mark ( uns_s *pUns, const int iMark ) ;


/* uns_meth.c: */
int bfBcNrCompare ( const void *pBf1, const void *pBf2 ) ;
int bcOrderCompare ( const void *vppBc0, const void *vppBc1 ) ;

uns_s *find_uns_expr ( const char *expr ) ;
uns_s *find_uns_numbered ( int nr ) ;

slidingPlaneSide_s * make_slidingPlaneSide ( uns_s *pUns,
                                             const int isMaster,
                                             const char ifcName[LINE_LEN] ) ;
int move_slidingPlaneSides ( uns_s *pUnsTo, uns_s* pUnsFrom ) ;

uns_s *make_uns ( grid_struct *pGrid ) ;
grid_struct *make_uns_grid ( uns_s **ppUns, int mDim,
                             ulong_t mEl, ulong_t mConn, ulong_t mEl2ChP,
                             ulong_t mVx, ulong_t mUnknown,
                             ulong_t mBndFc, int mBc ) ;
chunk_struct* make_chunk ( uns_s *pUns ) ;
void realloc_unknowns ( uns_s *pUns, int mUn0, int mUn2 ) ;


// Note: also directly called from read_ensight.
void append_elem ( chunk_struct *pChunk, ulong_t mElems, 
                   ulong_t mElem2VertP, ulong_t mElem2ChildP ) ;
void append_vrtx ( chunk_struct *pChunk, ulong_t mVerts, int mDim, int mEq ) ;
void append_bndFc ( chunk_struct *pChunk, ulong_t mBndPatches, ulong_t mBndFaces ) ;
//void append_vrtx ( chunk_struct *pChunk, ulong_t mVerts, int mDim, int mEq ) ;
//void append_elem ( chunk_struct *pChunk, ulong_t mElems, ulong_t mElem2VertP ) ;
chunk_struct *extend_chunk ( uns_s *pUns, int mDim,
			     ulong_t mElems,
			     ulong_t mElem2VertP, ulong_t mElem2ChildP,
			     ulong_t mVerts,
			     ulong_t mBndFaces, ulong_t mBndPatches ) ;
chunk_struct *append_chunk ( uns_s *pUns, int mDim,
			     ulong_t mElems,
			     ulong_t mElem2VertP, ulong_t mElem2ChildP,
			     ulong_t mVerts,
			     ulong_t mBndFaces, ulong_t mBndPatches ) ;
ret_s make_single_pVrtx ( uns_s *pUns ) ;
void free_uns ( uns_s **ppUns ) ;
void free_chunk ( uns_s *pUns, chunk_struct **ppChunk ) ;

void reset_elems ( elem_struct *pElem, ulong_t mElems ) ;
void reset_verts ( vrtx_struct *pVrtx, ulong_t mVerts ) ;
void reset_bndFcVx ( bndFcVx_s *pBndFcVx, ulong_t mBndFcVx ) ;


int conv_uns_var ( uns_s *pUns, varType_e newVarType ) ;
int init_uns_var ( uns_s *pUns, const char keyword[], const double value ) ;
void set_uns_freestream ( uns_s *pUns, double freeStreamVar[] ) ;
void ini_default_freestream ( uns_s *pUns ) ;
int make_uns_sol ( uns_s *pUns, int mUn, const char *solType ) ;
void delete_uns_sol ( uns_s *pUns ) ;
void traceMinMax ( const double *pUn, ulong_t n,
                   double *pvalMin, ulong_t *pnMin, double *pvalMax, ulong_t *pnMax ) ;
void min_max_var ( uns_s *pUns ) ;


int make_uns_ppChunk ( uns_s *pUns ) ;
int make_uns_ppBc ( uns_s *pUns ) ;
int make_uns_bndPatch ( uns_s *pUns ) ;
void link_uns_bcpatch ( uns_s *pUns ) ;

void uns_compress_bc ( uns_s *pUns ) ;
ret_s split_uns_bc_patch ( uns_s *pUns, const char *exprBc0, const geo_s *pGeo,
                           char *bcSplitAppend ) ;
ret_s split_uns_bcPatch_arg () ;

bc_struct *create_interFc_zones (  uns_s *pUns,
                                   const int mZonesL, const int iZoneL[mZonesL],
                                   const int mZonesC, const int iZoneC[mZonesL],
                                   int *mBndFcZn, int *mBcZn,
                                   char bcLbl[MAX_BC_CHAR] ) ;


llToElem_s *make_vxToElem ( uns_s *pUns ) ;

elem_struct *find_el_walk ( uns_s *pUns, const double *pCo, const vrtx_struct *pVx,
                            int *pkFc ) ;
elem_struct* find_el_tree_walk ( const vrtx_struct *pVxTo,
                                 uns_s *pUnsFrom, kdroot_struct *pTree,
                                 const double intPolRim, const double intFcTol,
                                 const double intFullTol,
                                 int *pmOut, int *pmReallyOut, int *pdomainOverlap ) ;

elem_struct *find_el_face ( uns_s *pUns, const int mDim, 
                           const elem_struct *pElem, const int kFc ) ;

int find_face_elem ( int mVx, const vrtx_struct *ppVxFc[], const elem_struct *pElem ) ;
int face_all_numbered_vx ( const elem_struct *pElem, const elemType_struct *pElT,
                           const int kFace, const int doMark, int *pmVxMarked ) ;
int face_all_mark3_vx ( const elem_struct *pElem, const elemType_struct *pElT,
                        const int kFace, const int doMark, int *pmVxMarked ) ;

int elem_contains_co ( const elem_struct *pElem, const double *pCo, const int kSide0 ) ;

void varType_avbp2adf ( uns_s *pUns ) ;


vrtx_struct *de_cptVx ( const uns_s *pUns, const cpt_s cpVx ) ;
cpt_s min_cpt ( cpt_s cpt0, cpt_s cpt1 ) ;
cpt_s max_cpt ( cpt_s cpt0, cpt_s cpt1 ) ;
int cmp_cpt ( cpt_s cpt0, cpt_s cpt1 ) ;

int cmp_perVxPair ( const void* pPerVx0, const void* pPerVx1 ) ;
int cmp_perVxPair_bc ( const void* pPerVx0, const void* pPerVx1 ) ;

int cmp_vx ( const void* pVx0, const void* pVx1 ) ;
int cmp_pvx ( const void* vpVx0, const void* vpVx1 ) ;

int cmp_elem_data ( const void* vpED0, const void* vpED1 ) ;
void add_elem_list ( elem_data_s *pListElemData, const int mList,
                     elem_struct *pElem, const double elData ) ;

void printelal ( const elem_struct *Pelem ) ;


// solution variables:

var_s *find_var_name ( varList_s *pVL, var_s *pVar, const char *pVarName ) ;

 void scatter_vx2ele_area ( uns_s *pUns, int mEq, 
                                  double vxQ[], double vxVol[],
                                  double elQ[], double elVol[] ) ;
void scatter_ele2vx_area ( uns_s *pUns ) ;
void unslice_scalar_var ( chunk_struct *pChunk, const ulong_t mVx,
                          const int kEq, const double *dBuf,
                          double *pValMin, ulong_t *pnMin,
                          double *pValMax, ulong_t *pnMax ) ;

void mult_uns_var_var ( uns_s *pUns,
                        const int kVar0, const int kVar1, const int kVar2,
                        double *pValMin, double *pValMax ) ;
void mult_uns_var_scal ( uns_s *pUns,
                         const int kVar0, double const scal, const int kVar2,
                         double *pValMin, double *pValMax ) ;

void set_uns_var ( uns_s *pUns, const int kVar, double const val ) ;


int count_elem_matches ( const match_s *pMatch ) ;
void make_elem_match_k ( const match_s *pAllMatch,
                         const int kMatch, const int doReset,
                         match_s *pMatch ) ;
int elem_matches ( const elem_struct *pElem, const match_s *pElMatch ) ;
int vx_matches ( const vrtx_struct *pVx, const match_s *pMatch ) ;


/* uns_mg.c: */
int uns_mg ( char *argLine ) ;


int mg_write_hdf ( uns_s *pUns,
                   const vrtx_struct *pMgVrtx, const double *pMgCoor,
                   char fileName[] ) ;


void viz_mgElems_vtk ( char *fileName, int mEl, const elem_struct **ppElem,
                       const vrtx_struct *pMgVrtx, const double *pMgCoor,
                       const double *pCoor0, const double *pCoor1 ) ;
int viz_mgElem ( const elem_struct *pElem,
                 const vrtx_struct *pMgVrtx, const double *pMgCoor ) ;
int viz_mgElems ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  int mEls, const elem_struct **ppElem ) ;

int viz_exElems ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  viz_e vizWhat, int mEls, int minEl, int list ) ;
int viz_exElem ( const uns_s *pUns,
                 const vrtx_struct *pMgVrtx, const double *pMgCoor,
                 viz_e vizWhat ) ;

void viz_mgel_r ( const uns_s *pUns,
                  const vrtx_struct *pMgVrtx, const double *pMgCoor,
                  const int nVx, double r ) ;
int viz_mgBnd ( const uns_s *pUns,
                const vrtx_struct *pMgVrtx, const double *pMgCoor,
                unsigned int bndMask );

/* uns_sliding_plane. */
void sp_free_vx_weight_mixing_lines ( const uns_s *pUns, slidingPlanePair_s *pSpP ) ;
ret_s sp_calc_vx_weight_mixing_lines ( uns_s *pUns, slidingPlanePair_s *pSpP ) ;

ret_s pair_slidingPlaneSides ( uns_s *pUns ) ;
ret_s uns_int_slidingplane ( uns_s *pUns, const int nBc,
                            const sp_geo_type_e mpGeoType,
                            const int doComputeR,
                            int *pmLines, double rh[] ) ;
ret_s uns_interface_sliding_plane (char line[LINE_LEN] ) ;



/* uns_number.c */
void fill_chunk_vrtxNr2 ( uns_s *pUns ) ;
void free_chunk_vrtxNr2 ( uns_s *pUns ) ;
void number_uns_grid_types ( uns_s *pUns, elType_e elTBeg, elType_e elTEnd,
                             const int useNumber, const int doReset, const int doBound ) ;

ulong_t number_uns_grid_regions_zones
( uns_s *pUns,
  const int mReg, const int iReg[],
  const int mZones, const int iZone[],
  const int useNumber, const int doReset, const int doBound,
  ulong_t *pmConn) ;
ulong_t number_uns_grid_match
( uns_s *pUns, const match_s *pMatch,
  const int useNumber, const int doReset, const int doBound,
  ulong_t *pmConn ) ;

void number_uns_grid ( uns_s *pUns ) ;
void number_uns_grid_zones ( uns_s *pUns, const int mZones, const int iZone[], 
                             const int useNumber, const int doReset, const int doBound ) ;
void number_uns_grid_elem_regions ( uns_s *pUns, const int mReg, const int iReg[], 
                                    const int useNumber, const int doReset, const int doBound,
                                    const int useMark ) ;
void number_uns_grid_leafs ( uns_s *pUns ) ;
void validate_elem_onPvx ( uns_s *pUns ) ;
int number_uns_elems_by_type ( uns_s *pUns, numberedType_e nrType,
                               elType_e elTypeBeg, elType_e elTypeEnd, const int doReset ) ;
int number_uns_elems_in_regions ( uns_s *pUns, numberedType_e nrType,
                               const int mZones, const int iZone[],
                               const int doReset,
                                 const int useMark ) ;
ulong_t number_uns_elems_match ( uns_s *pUns, const match_s *pMatch,
                                 const int doReset, ulong_t *pmConn ) ;

ulong_t count_vx_mark ( uns_s *pUns, 
                    const int mark, const int mark2, const int mark3 ) ;
ulong_t number_uns_vx_markN ( uns_s *pUns, const int kMark ) ;


void count_uns_elems_of_type ( uns_s *pUns ) ;
ulong_t count_uns_elems_region ( const uns_s *pUns, const int iReg,
                                 ulong_t *pmConnZone, const int useMark ) ;


ulong_t number_uns_vert_bc ( uns_s *pUns, 
                         int numAll, int mNumBc, const int nNumBc[], 
                         ulong_t mFc[5] )  ;
void increment_uns_vert_number ( uns_s *pUns, const int doReset ) ;
ulong_t increment_vx_number_bc ( uns_s *pUns ) ;


/* uns_mark.c */
void set_vrtx_mark_k ( vrtx_struct *pVx, const int kMark ) ;
void reset_vrtx_mark_k ( vrtx_struct *pVx, const int kMark ) ;
int check_vrtx_mark_k ( vrtx_struct *pVx, const int kMark ) ;

int set_vx_mark_face_k ( const elem_struct *pElem, const int nFace,
                         const int kMark ) ;

ulong_t set_vx_mark_k_nbc ( uns_s *pUns, const int nBc,
                            const int kMark, const int doReset ) ;
ulong_t set_vx_mark_k_pbc ( uns_s *pUns, const bc_struct *pBc,
                            const int kMark, const int doReset ) ;

void reset_1chunk_vx_mark_k ( chunk_struct *pChunk, const int kMark ) ;
void reset_all_vx_mark_k ( uns_s *pUns, const int kMark ) ;

void mark2_bndVx ( uns_s *pUns ) ;
int mark_vx_elem ( elem_struct *pElem ) ;
int mark2_vx_elem ( elem_struct *pElem ) ;
int mark3_vx_elem ( elem_struct *pElem ) ;
int markN_vx_elem ( elem_struct *pElem, const int kMark ) ;

int mark_vx_elem_regions ( uns_s *pUns,
                           const int mRegL, const int iRegL[mRegL],
                           const int mRegU, const int iRegU[mRegU],
                           const int useMark,
                           ulong_t *pmElemsZone, ulong_t *pmConnZone,
                           ulong_t *pmVertsZone,
                           ulong_t *pmBndFcZone ) ;
int mark_vx_elem_zones (  uns_s *pUns0,
                          const int mZonesL, const int iZoneL[mZonesL],
                          const int mZonesU, const int iZoneU[mZonesU],
                          ulong_t *pmElemsZone, ulong_t *pmConnZone,
                          ulong_t *pmVertsZone,
                          ulong_t *pmBndFcZone ) ;
int mark3_vx_zones_per ( uns_s *pUns0, int mZones, int iZone[],
                         const int doReset, const int doPer ) ;
int mark3_vx_elem_mark_per ( uns_s *pUns0, int mMarks, int iMark[],
                             const int doReset, const int doPer ) ;
int mark3_vx_elem_zones ( uns_s *pUns0,
                          const int iZoneF,
                          const int iZoneL,
                          const int iZoneC,
                          const int iZoneU,
                          ulong_t *pmElZoneF, ulong_t *pmConnZoneF, ulong_t *pmVxZoneF,
                          ulong_t *pmElZoneL, ulong_t *pmConnZoneL, ulong_t *pmVxZoneL,
                          ulong_t *pmElZoneC, ulong_t *pmConnZoneC, ulong_t *pmVxZoneC,
                          ulong_t *pmElZoneU, ulong_t *pmConnZoneU, ulong_t *pmVxZoneU ) ;

int mark_vx_per ( uns_s *pUns, int markN[2], int *pmBcPer, int nBcPer[],
                  const int doReset ) ;

void mark_uns_vertBc ( uns_s *pUns, const int nBc, 
                       const int doPer, const int doAxis, const int sglNrm, 
                       int *pFoundPer, 
		       ulong_t *PmVxBc, ulong_t *PmBiFc, ulong_t *PmTriFc, ulong_t *PmQuadFc ) ;
ulong_t validate_uns_vertFromElem ( uns_s *pUns, const int useNumber ) ;

int find_nBc ( const uns_s *pUns, const bc_struct *pBc ) ;
int list_vert_bc ( uns_s *pUns, const int nBc, const int mBndVx, int *pnBndVx ) ;

int count_uns_bndFc_chk ( uns_s *pUns ) ;
void reset_vx_number ( uns_s *pUns ) ;



int count_uns_bndFaces ( uns_s *pUns ) ;
void rm_perBc ( uns_s *pUns ) ;

int match_face_vxnr ( const elem_struct *pEl, const ulong_t nrVxFc[], int mVxFc ) ;

int match_int_list ( const int mI, const int mI2match[], const int i ) ;

vrtx_struct *find_nVx ( const uns_s *pUns, const int nVx ) ;
vrtx_struct *find_pVx ( const uns_s *pUns, const vrtx_struct *pVx ) ;
vrtx_struct **find_pVx_list ( vrtx_struct **ppVx,
                             vrtx_struct **ppVxList, const int mVxList ) ;

int find_npVx_list  ( vrtx_struct **pVx, const int mVx,
                      vrtx_struct **ppVxList, const int mVxList,
                      vrtx_struct **ppVxL ) ;

/* uns_per.c */
int bc_is_per ( const bc_struct *pBc ) ;
int bc_is_l ( const bc_struct *pBc ) ;
int bc_is_u ( const bc_struct *pBc ) ;
perBc_s *find_perBcPair ( const uns_s *pUns, const bc_struct *pBc, int *pIsL ) ;

void unset_per ( bc_struct *pBc ) ;
void unset_all_perBc ( uns_s *pUns ) ;

int set_per_rotation ( uns_s *pUns, const char per_pair[], const char axis[],
                       const double rotAngle ) ;
int set_per_corners ( uns_s *pUns, const char per_pair[], 
                      const double coor[6*MAX_DIM],
                      int reverse_rot ) ;
int match_per_faces ( uns_s *pUns ) ;
int match_per_verts ( uns_s *pUns ) ;
bc_struct *match_perPair_lu ( bc_struct *pBcRef, const int mPerBc, perBc_s *pPerBc, const int znIsL ) ;
int special_verts ( uns_s *pUns ) ;
void make_solution_per ( uns_s *pUns ) ;
int make_perVxPairs ( uns_s *pUns, 
                       perVx_s *pPerVxBc[MAX_PER_PATCH_PAIRS],
                       ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS],
                       int mPerVxBc[MAX_PER_PATCH_PAIRS] ) ;
int mult_per_vert ( uns_s *pUns, const int mPerVxBc[], perVx_s *pPerVxBc[],
                    ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS],
                    const int onlyIn ) ;
vrtx_struct *find_perVxPartner ( uns_s *pUns,
                                 const vrtx_struct *pVx, const int mPerVxBc[],
                                 perVx_s *pPerVxBc[], ndxPerVx_s *ndxPerVxBc[] ) ;

int list_per_pairs ( const grid_struct *pGrid ) ;
int match_only_per_edges ( uns_s *pUns ) ;
int find_per_edge ( const llEdge_s *pllAdEdge, const adEdge_s *pAdEdge,
                    const vrtx_struct **ppVx0, const vrtx_struct **ppVx1,
                    const vrtx_struct **ppPerVx0, const vrtx_struct **ppPerVx1 ) ;

int fix_per_setup ( uns_s *pUns ) ;
ret_s check_per_setup ( uns_s *pUns ) ;
ret_s check_bnd_setup ( uns_s *pUns ) ;

/* uns_refType.c */
int add_uns_refType ( const refType_struct *PsampleRefType, const elem_struct *Pelem ) ;


/* uns_specialTopo.c: */
int axis_verts ( uns_s *pUns, specialTopo_e axis ) ;

specialTopo_e a2topo ( const char *topoString ) ;
void set_nxtSpecialTopo ( const char* topoString ) ;
int set_specialTopo ( uns_s *pUns, const char * topoString ) ;
specialTopo_e get_specialTopo ( ) ;
const char *specialTopoString ( const uns_s *pUns ) ;

/* uns_toElem.c: */
llToElem_s *make_toElem ( llToElem_s **ppllToElem, arrFam_s *pFam, ulong_t mEnt ) ;
void free_toElem ( llToElem_s **ppllToElem ) ;

int add_toElem ( llToElem_s **ppllToElem, const ulong_t nEntry, elem_struct *pElem ) ;
int get_toElem ( const llToElem_s *pllToElem, const ulong_t nEntry, elem_struct *pElem,
		 ulong_t *pnLastTE ) ;
int loop_toElem ( const llToElem_s *pllToElem, const ulong_t nEntry,
		  ulong_t *pnItem, elem_struct **ppElem ) ;

void listToElem ( const llToElem_s *pllToElem ) ;
void printToElem ( const llToElem_s *pllToElem, const ulong_t n ) ;

/* uns_vxEnt. */
llVxEnt_s *make_llEnt ( llVxEnt_s *pllEnt, uns_s *pUns, char **ppEntData,
                        cpt_s cptVxMax, ulong_t mEnts, int mVxEnt,
                        ulong_t dataSize ) ;
void free_llEnt ( llVxEnt_s ** ppllEnt ) ;
void del_ent ( llVxEnt_s *pllEnt, ulong_t nDelEnt ) ;

int get_ent_vrtx ( const llVxEnt_s *pllEnt, int mVx, const vrtx_struct *pVrtx[],
                   int *pkVxMin ) ;
int add_ent_vrtx ( llVxEnt_s *pllEnt,
                   int mVx, const vrtx_struct *pVrtx[], int *pkVxMin ) ;
int loop_ent_vx ( const llVxEnt_s *pllEnt, const vrtx_struct *pVx, ulong_t *pnEnt ) ;

int get_sizeof_llEnt ( const llVxEnt_s *pllEnt ) ;
int get_used_sizeof_llEnt ( const llVxEnt_s *pllEnt ) ;
int show_ent ( const llVxEnt_s *pllEnt, const int nEnt, vrtx_struct *pVrtx[] ) ;

void listEnt ( llVxEnt_s const * const pllEnt,
               void (*printData) ( const uns_s*, void const * const ) ) ;
void listEntVx ( llVxEnt_s const * const pllEnt,
                 const vrtx_struct *pVrtx,
                 void (*printData) ( const uns_s*, void const * const ) ) ;
void printEnt ( llVxEnt_s const * const pllEnt, ulong_t nEnt,
                void (*printData) ( const uns_s*, void const * const ) ) ;

/* write_gmsh.c: */
int write_gmsh_lvl ( char *fileName, const int nLvl, uns_s *pUns ) ;
int write_gmsh_uns ( uns_s *pUns, const char *rootFlNm0, const int doCheckBnd ) ;
int write_gmsh ( const char *fileName, const int doCheckBnd ) ;


/* write_hdf.c */
int write_hdf5_grid ( char *rootFile, uns_s *pUns ) ;
int write_hdf5_sol ( uns_s *pUns, char *fileName ) ;
int write_hdf5 ( char* argLine ) ;
int write_mbMap_hdf5 ( const char *fileName ) ;


/* write_uns.c: */
int write_uns_ascii ( uns_s *pUns, const int mDim ) ;

/* write_uns_cgns.c: */
int write_uns_cgns ( char *pRootName ) ;


/* write_uns_dpl.c: */
int write_uns_dpl ( uns_s *pUns, char *PdplFile ) ;
int write_uns_dpl_adapt ( uns_s *pUns, char *PdplFile ) ;

/* write_avbp.c: */
int write_avbp ( uns_s *pUns, char *ProotFile, const char keyword[] ) ;
int write_avbp_conn ( uns_s *pUns, const char *PconnFile ) ;
int write_avbp_coor ( uns_s *pUns, const char *PcoorFile ) ;
int write_avbp_sol ( uns_s *pUns, const char *PsolFile, avbpFmt_e avbpFmt ) ;

/* write_avbp_bound.c: */
int write_avbp_exBound ( uns_s *pUns, char *PboundFile ) ;
int write_avbp_inBound ( uns_s *pUns, char *PboundFile ) ;
int write_avbp_asciiBound_4p7 ( uns_s *pUns, char *PboundFile ) ;
int write_avbp_asciiBound_4p2 ( uns_s *pUns, char *PboundFile ) ;

/* write_avbp4.c: */
int write_avbp4 ( uns_s *pUns, char *ProotFile ) ;

/* ensight: */
int write_ensight ( char *rootFile ) ;

/* write_fieldview.c */
int write_fieldview ( char* fileName ) ;

/* write_n3s.c */
int write_n3s ( char *pFlGeom, char *pFlSol ) ;

/* write_uns_stl.c */
ret_s write_stl ( grid_struct *pGrid, const char* stlNamePrefix ) ;

/* write_uns_cut.c: */
int write_uns_cut ( uns_s *pUns, char *rootFile ) ;


/* zone.c */
int zone_match_list ( const int mZones, const int iZone[], int zone ) ;
ulong_t zone_list_nodes ( uns_s *pUns, zone_s *pZ, ulong_t **ppZoneVx ) ;
ret_s zone_list ( uns_s *pUns, char expr[] ) ;
ret_s zone_list_all ( ) ;
ret_s zone_menu () ;
int zone_add ( uns_s *pUns, const char zn_name[], int iZone, const int doWarnDupl )  ;
zone_s *zone_copy ( uns_s *pUns2, const zone_s *pZ0 ) ;
void zone_copy_all  ( uns_s *pUns0, uns_s *pUns2 ) ;
int zone_get_active_number ( const uns_s *pUns, const int nZone ) ;
int zone_loop ( uns_s *pUns, zone_s **ppZone  ) ;
int zone_elem_mod_remaining ( uns_s *pUns, const int iZone ) ;
int zone_elem_mod_all ( uns_s *pUns, int nZoneSrc, zone_s *pZone ) ;
int zone_elem_mod_range ( uns_s *pUns, const int nZone, 
                          const ulong_t nElBeg, const ulong_t nElEnd ) ;
int zone_elem_mod_type ( uns_s *pUns, const int iZone, 
                         const elType_e elTBeg, const elType_e elTEnd ) ;
int zone_elem_invalidate ( uns_s *pUns, const int iZone ) ;
ulong_t zone_elem_mod_bclayer ( uns_s *pUns, int iZone,
                            const int nBc, const int mLayer,
                                const int iZoneClash, ulong_t *pElemClash ) ;
ulong_t zone_elem_mod_perBcLayer ( uns_s *pUns, int iZone[2],
                                   const int mLayer,
                                   const elType_e elTypeLo, const elType_e elTypeHi,
                                   int *pmBcPer, int nBcPer[] ) ;
int zone_add_param ( uns_s *pUns, char expr[], 
                     parType_e parType, const int isVec, 
                     const char par_name[], int dim, void *pv ) ;
int zone_add_solParam ( uns_s *pUns, char expr[], 
                     parType_e parType, const int isVec, 
                        const char par_name[], int dim, void *pv ) ;
void zone_del  ( uns_s *pUns, char *range ) ;
int zone_name_sequence  ( uns_s *pUns ) ;
int zone_merge ( uns_s *pUns, const int mZ, const int iZones[], const int doReset ) ;
int zone_region_merge ( uns_s *pUns,
                        const int mReg, const int iReg[],
                        const int mZones, const int iZone[],
                        const int doReset ) ;


#ifdef ADAPT_HIERARCHIC

/* Public adaptation calls. */
int adapt_reset ( uns_s *pUns ) ;
int adapt_uns_hierarchical_nr ( uns_s *pUns, char *fileType, char *fileName ) ;

int adapt_div_rot ( const double deref, const double ref ) ;
int adapt_entropy ( const double deref, const double ref ) ;
int adapt_grad ( const char var[], const double deref, const double ref ) ;
int adapt_dfmax ( const char var[], const double deref, const double ref,
		  const double edgeRatio ) ;
int adapt_Max_div_rot ( const double derefFct, const double ref,
		        const double edgeRatio ) ;

#endif
