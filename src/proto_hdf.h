/*  
  proto_hdf.h:
  HDF5 functions.

  Last update:
  ------------
  21Feb17; intro h5_list_unread_dset
  17Dec14; rename fx_str to fxStr, intro h5_read_fxStr.
  16Dec13; intro compress flag for h5_write_vec and callers.
  15Dec13; intro fx_str routines, replacing str80.
  5Apr13; switch int to ulong_t for array lengths.
          intro h5_read/write_szt.
  27Jul11; intro h5_read_dat.
  3Jul10; made public from read/write_hdf.
*/
/* h5_util.c */
parType_e h5_dclass2parType ( hid_t dclass_id ) ;

int h5_nxt_dset ( hid_t grp_id, int *idx, char *dset_name ) ;
int h5_list_unread_dset ( hid_t grp_id, char grpName[],
                          int mIgnoreDset, char ignoreNm[][LINE_LEN] ) ;

int h5_nxt_grp ( hid_t loc_id, int *idx, char *grp_name ) ;
int h5_obj_exists ( hid_t grp_id, const char* name ) ;
int h5_dset_exists ( hid_t grp_id, const char *dset_name ) ;
int h5_grp_exists ( hid_t loc_id, const char *grp_name ) ;
hid_t h5_open_group ( hid_t file_id, char *grpName ) ;


ulong_t h5_read_dat ( hid_t grp_id, char *set_nm, hid_t *pmtyp_id, size_t len, void *pDat ) ;
ulong_t h5_read_dbl ( hid_t grp_id, char *set_nm, size_t len, double *dBuf ) ;
ulong_t h5_read_int ( hid_t grp_id, char *set_nm, size_t len, int *iBuf ) ;
ulong_t h5_read_ulg ( hid_t grp_id, char *set_nm, size_t len, ulong_t *ulgBuf ) ;
ulong_t h5_read_char ( hid_t grp_id, char *set_nm, size_t len, char *string ) ;

int h5_write_dbl_stride ( hid_t grp_id, const int compress, char *set_nm, 
                          int dim, int stride, double *dBuf ) ;
int h5_write_int_stride ( hid_t grp_id, const int compress, char *set_nm, 
                          int dim, int stride, int *iBuf ) ;

ulong_t h5_read_arr ( hid_t grp_id, hid_t class_id, hid_t *pmtyp_id, 
                      const char *set_nm, size_t mRows, size_t mCols, 
                      void *data ) ;
ulong_t h5_read_darr ( hid_t grp_id, char *set_nm, size_t mRows, size_t mCols, double *dBuf ) ;
  ulong_t h5_read_iarr ( hid_t grp_id, char *set_nm, size_t mRows, size_t mCols, int *iBuf ) ;
ulong_t h5_read_uarr ( hid_t grp_id, char *set_nm, size_t mRows, size_t mCols, ulong_t *ulgBuf ) ;

int h5_write_vec ( hid_t grp_id, const int compress, hid_t mtyp_id,
                  const char *dset_name, size_t dim, const void *data ) ;
int h5_write_dbl ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const double *dBuf ) ;
int h5_write_int ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const int *iBuf ) ;
int h5_write_ulg ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const ulong_t *ulgBuf ) ;
int h5_write_char ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const char *string ) ;

int h5_write_fxStr ( hid_t grp_id, const char *dset_name, 
                      size_t dim, fxStr_e fxStr, void *string ) ;
int h5_write_one_fxStr ( hid_t grp_id, const char *dset_name, 
                          fxStr_e fxStr, void *string ) ;

int h5_read_fxStr_len ( hid_t grp_id, const char *dset_name ) ;
int h5_read_fxStr ( hid_t grp_id, const char *dset_name, 
                    size_t dim, fxStr_e fxStr, char *string ) ;
int h5_read_one_fxStr ( hid_t grp_id, const char *dset_name, 
                         fxStr_e fxStr, char *string ) ;

void h5_set_zip ( int h5w_zip ) ;

int signature(char* rootFile ) ; 
size_t h5_get_dset_size(hid_t parent,const char* name) ; 
void h5_load_double_hyperslab(hid_t parent,const char* name,double* buffer,size_t offset,size_t count) ; 
void h5_load_int_hyperslab(hid_t parent,const char* name,int* buffer,size_t offset,size_t count) ; 


