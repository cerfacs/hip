/*
   mb_ele_vert.c:
   Create unstructured grid elements and vertices from a structured multiblock grid. 
 
   Last update:
   ------------
   25Feb18; use hip_err, remove redundant prototype decl.
   18Dec10; new pBc->type
   28Jul97; add mDim vertex unknowns.
   15Nov96; conceived, cut out of mb_2uns.c.

   Contains:
   ---------
   get_mbElems:
   get_mbVerts:
   
*/

#include "cpre.h"
#include "cpre_mb.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_mb_uns.h"
#include "proto_mb.h"
#include "proto_uns.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern char hip_msg[] ;

/*************************************************************************

  get_mbElems:
  get all cells that are included in a cut of a block.
 
  last udpated:
  -------------
  12Dec13; fix bug with actually calling init_elem, not init_uns.
  9Jul11; use init_elem.
 
  Input:
  ------
  PBL:        pointer to the block.
  Pchunk:     pointer to the current chunk of unstructured grid,
  Pbase:      base pointer to calculate an offset for the vertex pointers.

 
  Changes to:
  -----------
  PBL:
  
  Returns:
  --------
  0: on failure,
  1: on success.
 
*/

int get_mbElems ( block_struct *PBL, const int mDim, vrtx_struct * const Pbase,
		  chunk_struct *Pchunk )
{
  int mElems, nK, nJ, nI, nllf, nulf, nlrf, nurf, nElem,
      mVert[MAX_DIM], *elemMrk, mVertsPerElem, mElemsMarked ;
  vrtx_struct **PPvrtx ;
  elem_struct *Pelem ;

  /* Aliases for the block. */
  elemMrk = PBL->PelemMark ;

  if ( mDim == 2 )
  { /* Default 2-D value. */
    mVert[0] = PBL->mVert[0] ;
    mVert[1] = PBL->mVert[1] ;
    mVert[2] = 1 ;
    mVertsPerElem = 4 ;
  }
  else if ( mDim == 3 )
  { /* Default 2-D value. */
    mVert[0] = PBL->mVert[0] ;
    mVert[1] = PBL->mVert[1] ;
    mVert[2] = PBL->mVert[2] ;
    mVertsPerElem = 8 ;
  }
  else
  { sprintf ( hip_msg, " get_mbElems can\'t deal with %d dimensions.\n",
	     mDim ) ;
    return ( 0 ) ;
  }

  /* Alloc elem related spaces. */
  if ( ( mElems = PBL->mElemsMarked ) ) {
    Pchunk->Pelem = arr_malloc ( "Pchunk->Pelem in get_mbElems", Pchunk->pUns->pFam,
                                 mElems+1, sizeof( elem_struct ) ) ;
    Pchunk->PPvrtx = arr_malloc ( "Pchunk->PPvrtx in get_mbElems", Pchunk->pUns->pFam,
                                  mVertsPerElem*mElems, sizeof( vrtx_struct * ) ) ;
  }
  else {
    Pchunk->mElems = 0 ;
    return ( 1 ) ;
  }

  /* Get the elems. */
  PPvrtx = Pchunk->PPvrtx ;
  Pelem = Pchunk->Pelem ;
  mElemsMarked = 0 ;
  /* Trip the outer loop at least once for 2-D grids. */
  for ( nK = 1 ; nK <= ( mDim == 2 ? 1 : mVert[2]-1 ) ; nK++ )
    for ( nJ = 1 ; nJ < mVert[1] ; nJ++ )
      for ( nI = 1 ; nI < mVert[0] ; nI++ )
      {
	/* Linear elem number. */
	nElem = ( ( nK-1 )*( mVert[1]-1 ) + ( nJ-1) )*( mVert[0]-1 ) + nI ;
	
	if ( elemMrk[nElem] )
	{ /* Get this one. Count the elements into elemMrk.*/
	  elemMrk[nElem] = ++mElemsMarked ;
	  
#         ifdef CHECK_BOUNDS
	    /* OK, this looks messy. The idea is to check whether we are
	       attempting to write out of bounds before incrementing the
	       pointer. On the lhs you will find the value of the pointer
	       after the increment, on the rhs the dimension of the field-1.
	       Since c indexes from 0, the maximum allowable pointer difference
	       on the lhs is one less than the field size. */
	    if ( Pelem+1 - Pchunk->Pelem > mElems ) {
              sprintf ( hip_msg, " %d is beyond Pelem's bounds of %d in get_mbElems\n",
		       (int)(Pelem+1 - Pchunk->Pelem), mElems ) ;
	      hip_err ( fatal, 0, hip_msg ) ; }
	    if ( PPvrtx+mVertsPerElem-1 >  Pchunk->PPvrtx + mVertsPerElem*mElems-1 ) {
              sprintf ( hip_msg, " %d is beyond PPvrtx' bounds of %d in get_mbElems\n",
		       (int)(PPvrtx+mVertsPerElem - Pchunk->PPvrtx),  mVertsPerElem*mElems ) ;
	      hip_err ( fatal, 0, hip_msg ) ; }
#         endif

	  /* Next element. */
	  Pelem++ ;
          init_elem ( Pelem, noEl, nElem, PPvrtx ) ;

	  if ( mDim == 2 && mVertsPerElem == 4 )
	    Pelem->elType = qua ;
	  else if ( mDim == 3 && mVertsPerElem == 8 )
	    Pelem->elType = hex ;
	  else {
            sprintf ( hip_msg, " no %d-noded structured element in %d dimensions"
		     " in get_mbElems.\n", mVertsPerElem, mDim ) ;
	    hip_err ( fatal, 0, hip_msg ) ; }

	  if ( mDim == 2 )
	  { /* Corner vertices of the=is marked elem: */
	    nllf =  ( nJ-1)*mVert[0] + nI ;
	    nulf = nllf + mVert[0] ;
	    nlrf = nllf + 1 ;
	    nurf = nulf + 1 ;
	    
	    /* Note that, as usual, the long element spaces that occur
	       only once per chunk start with 1. However, all cyclical
	       indices, like the vertices, start at 0. */
	    *(PPvrtx++) = Pbase + nllf ;
	    *(PPvrtx++) = Pbase + nlrf ;
	    *(PPvrtx++) = Pbase + nurf ;
	    *(PPvrtx++) = Pbase + nulf ;
	  }
	  else if ( mDim == 3 )
	  { /* Corner vertices of the=is marked elem: */
	    nllf =  ( ( nK-1 )*mVert[1] + ( nJ-1) )*mVert[0] + nI ;
	    nulf = nllf + mVert[0] ;
	    nlrf = nllf + mVert[0]*mVert[1] ;
	    nurf = nlrf + mVert[0] ;
	    
	    /* Note that, as usual, the long element spaces that occur
	       only once per chunk start with 1. However, all cyclical
	       indices, like the vertices, start at 0. */
	    *(PPvrtx++) = Pbase + nllf ;
	    *(PPvrtx++) = Pbase + nllf+1 ;
	    *(PPvrtx++) = Pbase + nulf+1 ;
	    *(PPvrtx++) = Pbase + nulf ;
	    *(PPvrtx++) = Pbase + nlrf ;
	    *(PPvrtx++) = Pbase + nlrf+1 ;
	    *(PPvrtx++) = Pbase + nurf+1 ;
	    *(PPvrtx++) = Pbase + nurf ;
	  }
	}
      }
  Pchunk->mElems = mElems ;
  Pchunk->mElem2VertP = mElems*mVertsPerElem ;
  return ( 1 ) ;
}
  

/*************************************************************************

  get_mbVerts:
  get all elem to vertex pointers of the elems that are
  included in a cut of a block.

  Last update:
  ------------
  12Mar18; intro doMap.
  
  Input:
  ------
  pBl:        structured grid block,
  mDim:       spatial dimensions,
  mUnknown:   number of unknowns,
  Pbase:      base for the vertex pointer offsets,
  Pchunk:     current unstructured grid chunk,
  doMap:      if nonzero, allocate a mb vx to uns vx field.
  
  Changes to:
  -----------
  Pchunk:

  Returns:
  --------
  0: on failure,
  1: on success.

*/

int get_mbVerts ( block_struct *pBl, const int mDim, const int mUnknown,
		  vrtx_struct * const Pbase, chunk_struct *Pchunk,
                  const int doMap ) {
  
  int nVert, nElem, mVerts, nDim, *vxMrk, nUnknown,
      mVertsElem = ( mDim == 2 ? 4 : 8 ) ;
  vrtx_struct **PPvrtx, *Pvrtx ;
  double *Pcoor, *PblPcoor, *Punknown, *PblPunknown ;
  elem_struct *Pelem ;

  /* Aliases for the block. */
  vxMrk = pBl->PintMark ;

  /* Reset the marks of all vertices. */
  for ( nVert = 1 ; nVert <= pBl->mVertsBlock ; nVert++ )
    vxMrk[nVert] = 0 ;

  /* Loop over all elems and mark any vertex that is connected. */
  for ( nElem = 1 ; nElem <= pBl->mElemsMarked ; nElem++ ) {
    Pelem = Pchunk->Pelem+nElem ;
    
    for ( PPvrtx = Pelem->PPvrtx ; PPvrtx < Pelem->PPvrtx + mVertsElem ; PPvrtx++ ) {
      vxMrk[ *PPvrtx - Pbase ] = 1 ;

#     ifdef CHECK_BOUNDS
        if ( *PPvrtx-Pbase > pBl->mVertsBlock )	{
          sprintf ( hip_msg, " PPVrtx base is beyond pBl->Pvrtx's bounds\n"
                   " of %d in get_mbVerts\n",
		   pBl->mVertsBlock ) ;
	  hip_err ( fatal, 0, hip_msg ) ; }
#     endif
    }
  }

  /* Count the number of connected vertices and set the marker to the
     incremental position in the list of vertices. */
  mVerts = 0 ;
  for ( nVert = 1 ; nVert <= pBl->mVertsBlock ; nVert++ )
    if ( vxMrk[nVert] ) {
      mVerts++ ;
      vxMrk[nVert] = mVerts ; }
      
  /* Alloc vertex related spaces. */
  Pchunk->Pvrtx = arr_malloc ( "Pchunk->Pvrtx in get_mbVerts", Pchunk->pUns->pFam,
                               mVerts+1, sizeof( vrtx_struct ) ) ;
  Pchunk->Pcoor = arr_malloc ( "Pchunk->Pcoor in get_mbVerts", Pchunk->pUns->pFam,
                               mDim*( mVerts+1 ), sizeof( double ) ) ;
  /* Add an extra mDim slots for a gradient value per vertex. */
  Pchunk->Punknown = arr_malloc ( "Pchunk->Punknown in get_mbVerts", Pchunk->pUns->pFam,
                                  (mUnknown+mDim+1)*( mVerts+1 ), sizeof( double ) ) ;

  if ( doMap ) {
    if ( mVerts != pBl->mVertsBlock ) {
      sprintf ( hip_msg, " block had %d nodes, only %d copied.\n"
                "          Mapping struct to uns will be invalid\n",
                pBl->mVertsBlock, mVerts ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }

    Pchunk->nBlock = pBl->nr ;
    Pchunk->blockDim[0] = pBl->mVert[0] ;
    Pchunk->blockDim[1] = pBl->mVert[1] ;
    Pchunk->blockDim[2] = pBl->mVert[2] ;
    /* Actually not yet needed, if vxCpt are tracked.
      Pchunk->ppIJK2Vx = arr_malloc ( "Pchunk->pIJK2Vx in get_mbVerts", Pchunk->pUns->pFam,
                                   pBl->mVertsBlock+1, sizeof( *Pchunk->ppIJK2Vx ) ) ;
    */
  }


  /* Get the marked vertices. Note that vrtx and coor fields
     start with the index 1. */
  Pvrtx = Pchunk->Pvrtx ;
  Pcoor = Pchunk->Pcoor + mDim ;
  Punknown = Pchunk->Punknown + mUnknown ;
  /* Note: Pvrtx points in the chunk, nVert runs in the block. */
  for ( nVert = 1 ; nVert <= pBl->mVertsBlock ; nVert++ )
    if ( vxMrk[nVert] ) {
      /* Get this one. */
	  
#     ifdef CHECK_BOUNDS
        /* OK, this looks messy. The idea is to check whether we are
	   attempting to write out of bounds before incrementing the
	   pointer. On the lhs you will find the value of the pointer
	   after the increment, on the rhs the dimension of the field - 1.
	   Since c indexes from 0, the maximum allowable pointer difference
	   is one less than the field size. */
	if ( Pvrtx+1 - Pchunk->Pvrtx > mVerts )	{
	  sprintf ( hip_msg, " %d is beyond Pvrtx' bounds of %d in get_mbVerts\n",
                   (int)(Pvrtx+1 - Pchunk->Pvrtx), mVerts ) ;
	  hip_err ( fatal, 0 , hip_msg ) ; }
	if ( Pcoor+(mDim-1) - Pchunk->Pcoor > mDim*( mVerts+1 )-1 ) {
	  printf ( hip_msg, " %d is beyond Pcoor's bounds of %d in get_mbVerts\n",
                   (int)(Pcoor+(mDim-1) - Pchunk->Pcoor), mDim*( mVerts+1 ) ) ;
	  hip_err ( fatal, 0 , hip_msg ) ; }
	if ( Punknown+(mUnknown-1) - Pchunk->Punknown >
	     (mUnknown+mDim+1)*( mVerts+1 )-1 )	{
	  printf ( hip_msg, " %d is beyond Punknown's bounds of %d in get_mbVerts\n",
                   (int)(Punknown+(mUnknown-1) - Pchunk->Punknown), 
                   mUnknown*( mVerts+1 ) ) ;
	  hip_err ( fatal, 0 , hip_msg ) ; }
#     endif

      /* Next vertex. */
      Pvrtx++ ;

      /* For debugging, list the vertex number in the block
	 with the chunk. */
      Pvrtx->number = nVert ;
      Pvrtx->vxCpt.nCh = Pchunk->nr ;
      Pvrtx->vxCpt.nr = nVert ;
      

      Pvrtx->Pcoor = Pcoor ;
      PblPcoor = pBl->Pcoor + mDim*nVert ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	*(Pcoor++) = *(PblPcoor++) ;

      Pvrtx->Punknown = Punknown ;
      PblPunknown = pBl->PdblMark + mUnknown*nVert ;
      for ( nUnknown = 0 ; nUnknown < mUnknown ; nUnknown++ )
	*(Punknown++) = *(PblPunknown++) ;
      /* Skip the extra storage for gradients, etc. */
      Punknown += mDim+1 ;
    }
      
  /* Correct the element to vertex pointers. Note that there is only one contiguous
     list of element to vertex pointers. */
  for ( PPvrtx = Pchunk->Pelem[1].PPvrtx ;
        PPvrtx < Pchunk->Pelem[1].PPvrtx + mVertsElem*pBl->mElemsMarked ; PPvrtx++ )
    /* The number of this vertex in the full list over all blocks is
       *PPvrtx - Pbase. The number in the current block in the list of all
       retained vertices is incemented in vxMrk. */
    *PPvrtx = Pchunk->Pvrtx + vxMrk[ *PPvrtx - Pbase ] ;

  Pchunk->mVerts = mVerts ;
  return ( 1 ) ;
}
