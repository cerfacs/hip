/*
 Public access to tree.c. Prototypes.

 Last update:
 21Jun11; derived from tree_public.h
*/

/* Typedefs. No need to tell more in public. */
typedef struct _kdroot_struct kdroot_struct ;

/* Prototypes. */
kdroot_struct *kd_ini_tree ( void *pFam, char *treeName,
                        const int mDim, const double minDist, 
                        const double *Pll, const double *Pur,
			double *(*data2valu) ( const DATA_TYPE *) ) ;
void kd_del_tree ( kdroot_struct **PPRoot ) ;

void kd_add_data ( kdroot_struct *Proot, const DATA_TYPE *Pdata ) ; 

int kd_m_nearest_datas ( const kdroot_struct *pRoot, const void *Pdata, int mData,
                         const void *pNearestData[], const ulong_t dataSize,
                         double nearestDistSq[], int *pmRgTot ) ;
void *kd_nearest_data ( const kdroot_struct *PRoot, const DATA_TYPE *Pdata,
			  double *PnearestDist ) ;

/* User supplied function to extract the pointer to contiguous mDim data of double
   from the pointer to void. */
double *data2valu ( const void *Pdata ) ;
