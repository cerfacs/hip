/*
   uns_interpolate.c:


   Last update:
   ------------
   10Nov17; pass intPol_dist_inside to find_elem_tree_walk.
   5Sep15; introduce use of CLAPACK.
   9Jan01; copy restart info over in uns_interpolate. Fix bug with circumf. veloc.
   27Oct00; introduce limting in interpolate_elem.
   8Apr00; modify to allow axi interpolation.

   This file contains:
   -------------------
   elem_contains_vx:
   interpolate_elem_axi:
   interpolate_elem:

   vrtx2rad:
   uns_interpolate:
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
/* #include "nrutil.h" */
#include "nr.h"
/* now done in cpre.h #include "lapack.h" */

#ifdef LAPACK
/* Fortran lapack extract. */
#include "lapack.h"
#endif

#ifdef CLAPACK
/* Clapack. */
#include "f2c.h"
#include "clapack.h"
#endif

/* Timing stuff. Do we really need types.h? */
#include <sys/types.h>
#include <sys/times.h>
struct tms timings_int ;
static clock_t nowTime, iniTime ;
static int clk_ticks ;

/* #define TESTFUNC 1 */

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern const Grids_struct Grids ;

typedef struct {
  const vrtx_struct *pVx ;
  double dist ;
} vxDist_s ;

/* Statically allocated virtual element. */
//extern elem_struct vrtElem ;


extern const reco_enum reco ;
extern const double mVxRecoFactor ;
extern const double intPolRim ;
extern const double intPol_dist_inside ;
extern const double intFcTol ;
extern const double intFullTol ;

/* Minnorm coeffs. */
extern const double minnorm_tol ;


/* Weighting function. */
extern const double choldc_tol ;
extern const double ls_lambda ;

/* A table of number of basis functions [mDim][poly], hence mDim=0,1 are empty. */
const int mBsFunc[4][3] = { {0,0,0}, {0,0,0},
                            {1,3,6}, {1,4,10} } ;
/* A list of basis function composition. cBasis[mDim][3], where the the third
   index runs over the 3 coordinate directions. */
const int cBsFunc[4][MAX_LS_BASES][3] = { {{0}},
                                       {{0}},
                                      /* 2D */
                                       {{0,0,0},
                                        {1,0,0},
                                        {0,1,0},
                                        {2,0,0},
                                        {1,1,0},
                                        {0,2,0}},
                                       /* 3D */
                                       {{0,0,0},
                                        {1,0,0},
                                        {0,1,0},
                                        {0,0,1},
                                        {2,0,0},
                                        {1,1,0},
                                        {1,0,1},
                                        {0,2,0},
                                        {0,1,1},
                                        {0,0,2}} } ;

/* Global arrays allocated using nr_util routines. */
double **a, **ata,**at,*pa, **v, *p, *c, *f,*d, *wt, *w, *b ;
int m, n ;

#ifdef TESTFUNC

static double testfunc ( int fnr, const double co0[], int mDim ) {
  /* Polynomial coefficients for the functions to be fitted. */
  double c000 = 1. ;
  double c100 = 0.01 ;
  double c010 = 0.01 ;
  double c001 = 0.01 ;
  double c200 = 3. ;
  double c020 = 3. ;
  double c002 = 1. ;
  double c110 = .5 ;
  double c101 = 4. ;
  double c011 = 3. ;
  double f, co[MAX_DIM] ;

  /* Offset. */
  co[0] = co0[0] - .5 ;
  co[1] = co0[1] - .5 ;
  co[2] = co0[2] - .5 ;

  if ( fnr >= 0 ) {
    /* The function is a polynomial. */
    f = c000 ;
    if ( fnr == 0 ) return ( f ) ;

    f += c100*co[0] + c010*co[1] ;
    if ( mDim == 3 ) f += c001*co[2] ;
    if ( fnr == 1 ) return ( f ) ;

    f += c200*co[0]*co[0] + c110*co[0]*co[1] + c020*co[1]*co[1] ;
    if ( mDim == 3 ) f += c101*co[0]*co[2] + c011*co[1]*co[2] + c002*co[2]*co[2] ;
    if ( fnr == 2 ) return ( f ) ;

    f *= co[0]+co[1] ;
    if ( fnr == 3 ) return ( f ) ;

    sprintf ( hip_msg, "polynomials of %dth degree are not implemented.\n", fnr ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( fnr == -1 ){
    int b = (int) co[0]/2/PI ;
    double x = co[0]-b*(2*PI) ;
    f = sin( x ) ;
    return ( f ) ;
  }
  else {
    sprintf ( hip_msg, "function nr %d is not implemented.\n", fnr ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Just to avoid a compiler moan. */
  return ( 0. ) ;
}


/******************************************************************************
  intp_testfunc_set:  */

/*! Set unknowns of a testfunction on donor grid
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  10May11: extracted from uns_interpolate.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void intp_testfunc_set ( uns_s *pUnsFrom ) {

  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double *pCo, *pUn ;
  const int mDim = pUnsFrom->mDim ;

  /* Test function on pUnsFrom. */
  pChunk = NULL ;
  while ( loop_verts ( pUnsFrom, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        pCo = pVx->Pcoor ;
        pUn = pVx->Punknown ;

        if ( TESTFUNC == 1 ) {
          pUn[0] = testfunc ( 0, pCo, mDim ) ;
          pUn[1] = testfunc ( 1, pCo, mDim ) ;
          pUn[2] = testfunc ( 2, pCo, mDim ) ;
          pUn[3] = testfunc ( 3, pCo, mDim ) ;
        } else {
          pUn[0] = testfunc (-1, pCo, mDim ) ;
          pUn[1] = testfunc (-1, pCo, mDim ) ;
          pUn[2] = testfunc (-1, pCo, mDim ) ;
          pUn[3] = testfunc ( 2, pCo, mDim ) ;
        }
      }

  return ;
}


/******************************************************************************
  intp_testfunc_check:   */

/*! check interpolated test function on recipient grid.
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  10May11: extracted from uns_interpolate.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void intp_testfunc_check ( uns_s *pUnsTo ) {


  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double *pCo, *pUn ;
  const int mDim = pUnsTo->mDim ;


   /* Compare to test function on pUnsTo. */
  double l2[4] = {0.,0.,0.,0.}, err[4], errMax[4] = {0.,0.,0.,0.},
    valMin[4] = {TOO_MUCH, TOO_MUCH, TOO_MUCH, TOO_MUCH },
    valMax[4] = {-TOO_MUCH, -TOO_MUCH, -TOO_MUCH, -TOO_MUCH } ;

  if ( verbosity < 3 )
    return ;


  pChunk = NULL ;
  while ( loop_verts ( pUnsTo, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        pCo = pVx->Pcoor ;
        pUn = pVx->Punknown ;

        if ( TESTFUNC == 1 ) {
          err[0] = pUn[0] - testfunc ( 0, pCo, mDim ) ;
          err[1] = pUn[1] - testfunc ( 1, pCo, mDim ) ;
          err[2] = pUn[2] - testfunc ( 2, pCo, mDim ) ;
          err[3] = pUn[3] - testfunc ( 3, pCo, mDim ) ;
        } else {
          err[0] = pUn[0] - testfunc (-1, pCo, mDim ) ;
          err[1] = pUn[1] - testfunc (-1, pCo, mDim ) ;
          err[2] = pUn[2] - testfunc (-1, pCo, mDim ) ;
          err[3] = pUn[3] - testfunc ( 2, pCo, mDim ) ;
        }

        l2[0] += err[0]*err[0] ;
        l2[1] += err[1]*err[1] ;
        l2[2] += err[2]*err[2] ;
        l2[3] += err[3]*err[3] ;

        errMax[0] = MAX( errMax[0], ABS( err[0] ) ) ;
        errMax[1] = MAX( errMax[1], ABS( err[1] ) ) ;
        errMax[2] = MAX( errMax[2], ABS( err[2] ) ) ;
        errMax[3] = MAX( errMax[3], ABS( err[3] ) ) ;

        valMax[0] = MAX( valMax[0], pUn[0] ) ;
        valMax[1] = MAX( valMax[1], pUn[1] ) ;
        valMax[2] = MAX( valMax[2], pUn[2] ) ;
        valMax[3] = MAX( valMax[3], pUn[3] ) ;

        valMin[0] = MIN( valMin[0], pUn[0] ) ;
        valMin[1] = MIN( valMin[1], pUn[1] ) ;
        valMin[2] = MIN( valMin[2], pUn[2] ) ;
        valMin[3] = MIN( valMin[3], pUn[3] ) ;

        if ( verbosity > 4 &&
             ( ABS( err[0] ) > 1.e-7 ||
               ABS( err[1] ) > 1.e-7 ||
               ABS( err[2] ) > 1.e-7 ||
               ABS( err[3] ) > 1.e-7 ) )
          printf ( " vx: %3d, % 7.3e, % 7.3e, % 7.3e, % 7.3e\n",
                   pVx->number, err[0], err[1], err[2], err[3] ) ;
      }

    l2[0] = sqrt(l2[0])/mVxTo ;
    l2[1] = sqrt(l2[1])/mVxTo ;
    l2[2] = sqrt(l2[2])/mVxTo ;
    l2[3] = sqrt(l2[3])/mVxTo ;

    printf ( "\n    Interpolation error: x^0:  l2: %7.3e,  linf: %7.3e\n"
             "                         x^1:  l2: %7.3e,  linf: %7.3e\n"
             "                         x^2:  l2: %7.3e,  linf: %7.3e\n"
             "                         x^3:  l2: %7.3e,  linf: %7.3e\n\n",
             l2[0], errMax[0], l2[1], errMax[1], l2[2], errMax[2], l2[3], errMax[3] ) ;
    printf ( "    Minimum values: %-7.3e, %-7.3e, %-7.3e, %-7.3e\n",
             valMin[0],  valMin[1],  valMin[2],  valMin[3] ) ;
    printf ( "    Maximum values: %-7.3e, %-7.3e, %-7.3e, %-7.3e\n",
             valMax[0],  valMax[1],  valMax[2],  valMax[3] ) ;

  return ;
}

#endif

/******************************************************************************

  cmp_elCrit:
  Comarison of two vertices with proximity to an interpolation point.

  Last update:
  ------------
  : conceived.

  Input:
  ------
  pEC0/1: the two vxDist structs to compare

  Returns:
  --------
  -1 if pEC0 < pEC1, etc.

*/

static int cmp_vxDist ( const void *pVxD0, const void *pVxD1 ) {

  static double d0, d1;
  d0 = ( (vxDist_s*) pVxD0 )->dist ;
  d1 = ( (vxDist_s*) pVxD1 )->dist ;

  if ( d0 < d1 )
    return ( -1 ) ;
  else if  ( d0 > d1 )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}



/******************************************************************************

  lsfit_svd:
  Calculate a least squares fit using the SVD.

  Last update:
  ------------
  22Jun11; write solution to separate pUnknown to align with interpolate_elem.
  4Feb2: derived from lsfit_chol

  Input:
  ------
  pVxTo:     vertex to fit for.
  pUnknown:  interpolated solution.
  pVx[]:     list of vertices to fit from.
  m:         number of basis functions to fit.
  n:         number of elements in the RHS.
  mU:        number of unknowns to fit.

  Changes To:
  -----------
  pVxTo->Punknown

  Returns:
  --------
  the number of singular values.

*/
#define LSFIT lsfit_svd
static int lsfit_svd ( const vrtx_struct *pVxTo, double pUnknown[],
                       const vrtx_struct *pVx[],
                       const int mDim, int m, int n, int mU ) {

  const double *x0 = pVxTo->Pcoor ;
  const int *pBs ;
  const double *pCo ;
  int i,j,k, nU, mSing ;
  double x[3], l2 = 0., dx, tol=1.e-10 ;


  /* Weighting functions. Calculate an "averaged" distance, use L2. */
  if ( ls_lambda < 1.e25 ) {
    l2 = 0. ;
    for ( j=1; j<=n; j++ ) {
      pCo = pVx[j-1]->Pcoor ;
      x[0] = pCo[0] - x0[0] ;
      x[1] = pCo[1] - x0[1] ;
      x[2] = ( mDim == 3 ? pCo[2] - x0[2] : 0. ) ;
      l2 += x[0]*x[0] + x[1]*x[1] + x[2]*x[2] ;
    }
    l2 = l2/n ;
    /* tol = l2*1.e-5 ; */
  }




  /* Compute the weighting wt[] and fill each row in A. */
  for ( j=1; j<=n; j++ ) {
    pCo = pVx[j-1]->Pcoor ;
    x[0] = pCo[0] - x0[0] ;
    x[1] = pCo[1] - x0[1] ;
    x[2] = ( mDim == 3 ? pCo[2] - x0[2] : 0. ) ;
    if ( ls_lambda < 1.e25 ) {
      dx = x[0]*x[0] + x[1]*x[1] + x[2]*x[2] ;
      wt[j] = exp( -dx/l2/ls_lambda ) ;
    }
    else
      wt[j] = 1. ;

    for ( i = 1 ; i <= m ; i++ ) {
      pBs = cBsFunc[mDim][i-1] ;
      pa = a[j]+i ;
      *pa = wt[j] ;
      for ( k = 0 ; k < mDim ; k++ )
        *pa *= ipow( x[k], pBs[k] ) ;

    }
  }

  /* SVD. */
  svdcmp ( a, n, m, w, v ) ;


  /* Count singular values. */
  for ( mSing = 0, i = 1 ; i <= m ; i++ )
    if ( w[i] < tol )
      mSing++ ;

  /* Fit each unknown. */
  for ( nU = 0 ; nU < mU ; nU++ ) {

    /* RHS. Note that f, wt run [1..n] */
    for ( j = 1; j <= n; j++ )
      f[j] = wt[j]*pVx[j-1]->Punknown[nU] ;

    /* calculate Ut*b. */
    datxb ( a, m, n, f, d ) ;

    /* Compute the first coefficient. */
    c[1] = 0. ;
    for ( i = 1 ; i <= m ; i++ )
      c[1] += v[1][i]*d[i]/w[i] ;


    pUnknown[nU] = c[1] ;
  }

  return ( mSing ) ;
}


/******************************************************************************
  printMat:   */

/*! Print elements of a matrix.
 */

/*

  Last update:
  ------------
  13Jul15: conceived.


  Input:
  ------
  label:  tag to identify which matrix is printed
  m:      rows
  n:      columns of
  A:      matrix to print in Fortran style, ie. column-major
  lda:    leading dimension of A

*/

void printMat ( char *label, int m, int n, double *A, int lda ) {

  printf ( "%s is %d x %d\n", label, m, n ) ;

  // Build a list of pointers to the top of the column. Note that
  // At is unconstrained skinny (m>n), and can be at most square m=n.
  int k ;
  double *pA[MAX_VX_ELEM] ;
  for ( k = 0 ; k < n ; k++ )
    pA[k] = A + k*lda ;

  int i ;
  for ( i = 0 ; i < m ; i++ ) {
    for ( k = 0 ; k < n ; k++ )
      printf ( " %6.2f", pA[k][i] ) ;

    printf ( "\n" ) ;
  }
  return ;
}


/******************************************************************************
  fillAtb:   */

/*! Fill the system matrix for linear interpolation on an element.
 *
 */

/*

  Last update:
  ------------
  13Jul15: conceived.


  Input:
  ------
  mDim:  spatial dimension
  pElem: element to interpolate on
  m:     number of distinct vertices in pElem, i.e. rows of At
  lda:   leading dimension of Fortran (column-major) matrix At.
  xp:    point to interpolate for.

  Changes To:
  -----------

  Output:
  -------
  *pn:   number of columns of At
  At:    transpose of system matrix A
  b:     RHS of system.

  Returns:
  --------
  1 on failure, 0 on success

*/

void fillAtb ( const int mDim, double coor[MAX_VX_ELEM][MAX_DIM],
               int m, int *pn, double *At, const int lda,
               const double xp[], double *b ) {

  // Fill x, unit size square, numbered ccw.;
  *pn = mDim+1 ;
  //double x[MAX_VX_ELEM*2] = { 1., 2. , 3. , 4. , 5. , 6. , 7. , 8. } ;
  // double x[MAX_VX_ELEM*MAX_DIM] = {
  //   0., 0., 0.,
  //   1., 0., 0.,
  //   1., 1., 0.,
  //   0., 1., 0.,
  //   0., 0., 1.,
  //   1., 0., 1.,
  //   1., 1., 1.,
  //   0., 1., 1.
  // } ;


  // Fill At in Fortran, column-major order, i.e. all x, then y ,,
  // in a column each.
  // Build a list of pointers to the top of the column. Note that
  // At is unconstrained skinny (m>n), and can be at most square m=n.
  double *pA[MAX_DIM+1] ;
  int kD ;
  for ( kD = 0 ; kD < mDim+1 ; kD++ )
    pA[kD] = At + kD*lda ;

  // Fill At and b.
  double *pAt = At ;
  int kVx ;
  for ( kD = 0 ; kD < mDim ; kD++ ) {
    for ( kVx = 0 ; kVx < m ; kVx ++ )
      //pA[kD][kVx] = x[MAX_DIM*kVx+kD] ;
      pA[kD][kVx] = coor[kVx][kD] ;
    b[kD] = xp[kD] ;
  }
  kD = mDim ;
  for ( kVx = 0 ; kVx < m ; kVx ++ )
     pA[kD][kVx] = 1. ;
  b[kD] = 1 ;

  return ;
}

/******************************************************************************
  addAtbNeg:   */

/*! Given a position of a negative interpolation coeff, add a row to nullify.
 *
 */

/*

  Last update:
  ------------
  13Jul15: conceived.


  Input:
  ------
  kNeg:    position of negative coeff to nullify
  m:       number of rows of At
  lda:     leading dimension of At

  Changes To:
  -----------
  *pn:     number of columns of At
  At:      transpose of system matrix, Fortran style, column-major.
           with additional row for coeff to be nullified.
  b:       augmented RHS.

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int addAtbNeg ( const int kNeg, const int m, int *pn,
                double *At, const int lda,
                double *b ) {

  double *pA ;
  int kVx ;
  if ( kNeg != -1 ) {
    pA = At + (*pn)*lda ;
    for ( kVx = 0 ; kVx < m ; kVx++ )
      pA[kVx] = 0. ;
    pA[kNeg] = 1. ;

    b[*pn] = 0 ;

    (*pn)++ ;
  }

  return ( 0 ) ;
}

/******************************************************************************
  minnormsolve:   */

/*! Solve the possibly underdetermined system A*al=b with min norm for al.
 */

/*

  Last update:
  ------------
  5Sep15; switch to clapack, use integer = long int, instead of int.
  13Jul15: conceived.


  Input:
  ------
  m:     number of rows in At0
  n:     number of columns in At0
  At0:   Fortran style (column-major) transpose of system matrix
  AtWk:  workspace same size as At0
  ldat:  leading dimension of At0
  b:     RHS of system
  lb:    length of b


  Output:
  -------
  al:    min norm solution of the system.

  Returns:
  --------
  1 on failure, 0 on success

*/

int minNormSolve ( const int m, const int n,
                    double *At0, double *AtWk, const int ldat,
                    double *b, const int lb, double al[m] ) {

#ifdef CLAPACK
  #define CLAP_INT integer
#else
  // Lapack Fortran routines
  #define CLAP_INT int
#endif

  // Copy At and b as they will be overwritten by Lapack.
  memcpy ( AtWk, At0, ldat*n*sizeof(double) ) ;
  //printMat( "At", m, n, AtWk, ldat) ;

  memcpy ( al, b, MAX_VX_ELEM*sizeof(double) ) ;
  //printMat( "b", m, 1, al, 1) ;

  // Lapack work array.
  double work[MAX_VX_ELEM] ;
  CLAP_INT lw = m ;

  // QR decomp.
  double tau [MAX_VX_ELEM] ;
  CLAP_INT info ;
  CLAP_INT ml = m ; // type conf between int and f2c's integer = long int.
  CLAP_INT nl = n ;
  CLAP_INT ldatl = ldat ;
  CLAP_INT lwl = lw ;
  dgeqrf_( &ml, &nl, AtWk, &ldatl, tau, work, &lwl, &info ) ;
  if ( info ) {
    int info2 = info ;
    sprintf ( hip_msg, "dgeqrf failed with code %d in minNormSolve", info2 ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
    //printMat( "R", n, n, AtWk, ldat) ;

  // w = R^-T b
  char *cU = "U";
  char *cT = "T";
  char *cN = "N";
  CLAP_INT one = 1;
  CLAP_INT lbl = lb ;

  /* Check for singular A. */
  int k ;
  double diagTol = 1.e-10 ;
  for ( k = 0, info=0 ; k < n ; k++ )
    if ( ABS( AtWk[k*ldat +k] ) < diagTol ) {
      info = 8 ;
      break ;
    }
  if (info !=8 )
    dtrtrs_( cU, cT, cN, &nl, &one, AtWk, &ldatl, al, &lbl, &info ) ;
  if ( info == 8 ) {
    /* Singular matrix, collinear points. E.g. a 3-D elem reduced to a planar
       face with the target node not in the plane. */
    return ( 8 ) ;
  }
  else if ( info ) {
    int info2 = info ;
    sprintf ( hip_msg, "dtrtrs failed with code %d in minNormSolve.\n \n \t \t Set interpolation to 'se in-re el' instead \n \n", info2 ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  //printMat( "v", m, 1, al, 1) ;

  // Left mult with Q
  char *cL = "L";
  dormqr_( cL, cN, &ml, &one, &nl, AtWk, &ldatl, tau, al, &lbl,
           work, &lwl, &info ) ;
  if ( info ) {
    int info2 = info ;
    sprintf ( hip_msg, "dormqr failed with code %d in minNormSolve", info2 ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  //printMat( "al", m, 1, al, 1) ;

  return ( 0 ) ;
}

/******************************************************************************
  findNegCoeff:   */

/*! Scan for negative coefficients in alpha
 */

/*

  Last update:
  ------------
  13Jul15: conceived.


  Input:
  ------
  m:     number of rows
  n:     number of columns in transpose of system matrix At
  alpha: min norm solution to A alpha = b with possibly negative coeff.
  tol:   tolerance for negativity ( tol = -eps will accept slightly neg coeff)

  Changes To:
  -----------
  fNegAlpha: On entry 1 for any coeff nullified in A, 0 otherwise,
             On exit additional 1 for the new most negative coeff.

  Output:
  -------
  pkMin:  position of the most negative coefficient.

  Returns:
  --------
  1 if a new negative coefficient has been found, 0 otherwise.

*/

int findMostNegCoeff ( const int m, const int n, const double *alpha,
                       const double tol, int *fNegAlpha, int *pkMin ) {

  int fNewNegAlpha = 0 ;
  double alMin ;
  int k ;
  *pkMin = -1 ;
  // Further reduction in stencil possible. Find the most negative one.
  alMin = tol ;
  for ( k = 0 ; k < m ; k++ ) {
    if ( alpha[k] < alMin && fNegAlpha && fNegAlpha[k] == 0 ) {
      *pkMin = k ;
      alMin = alpha[k] ;
    }
  }

  if ( *pkMin != -1 ) {
    // New constraint for kMin.
    fNewNegAlpha = 1 ;
    fNegAlpha[*pkMin] = 1 ;
  }

  return ( fNewNegAlpha ) ;
}

/******************************************************************************
  zeroSmallCoeff:   */

/*! Round coefficients alpha to zero or one with tol. Renormalise, relimit.
 *
 */

/*

  Last update:
  ------------
  14jul17; rework from countNegCoeff to be zeroSmallCoeff.
  13Jul15: conceived.


  Input:
  ------
  m:      number of coefficients
  alpha:  list of coefficients
  tol:    tolerance, must be negative.


  Returns:
  --------
  number of coefficients rounded up or down.

*/

void zeroSmallCoeff ( const int m, double alpha[m], const double tol ) {

  int k ;
  double sumAl = 0. ;
  double akm1 ;
  double sumChg = 0 ;
  int canChange = 0 ;
  for ( k = 0 ; k < m ; k++ ) {
    if ( alpha[k] > tol && alpha[k] < -tol) {  // note: tol is negative.
      alpha[k] = 0. ;
    }
    else if ( (akm1 =  alpha[k]-1 ) > tol && akm1 < -tol ) { // note: tol is negative.
      alpha[k] = 1. ;
      sumAl += 1. ;
    }
    else {
      canChange = 1 ;
      sumAl += alpha[k] ;
      sumChg += alpha[k] ;
    }
  }

  double dAl ;
  if ( canChange ) {
    dAl =  -(sumAl-1.)/sumChg ;
    for ( k = 0 ; k < m ; k++ ) {
      if ( alpha[k] > 0. && alpha[k] < 1. )
        alpha[k] += dAl*alpha[k] ;

      if ( alpha[k] > 1 ) alpha[k] = 1. ;
      else if ( alpha[k] < 0. ) alpha[k] = 0. ;
    }
  }

  return ;
}

/******************************************************************************
  project_elem_planar:   */

/*! Project parts of an element, typically a face, into a plane.
 */

/*

  Last update:
  ------------
  9Sep16; treat 0-d case correctly.
  16Feb16: conceived.

  Input:
  ------
  mDim
  mVx:   number of non-duplicate vertices
  fNegAlpha: nonzero for each active vertex among the mVx

  Changes:
  --------
  coor:  coordinates, repacked to exclude duplicates
  xp: coordinate to project

  Returns:
  --------
  1 on failure, 0 on success

*/

int project_elem_planar ( const int mDim, const int mVx,
                          double coor[MAX_VX_ELEM][MAX_DIM],
                          const int fNegAlpha[MAX_VX_ELEM],
                          double xp[MAX_DIM] ) {

  double Rt[MAX_DIM*MAX_DIM] ; // transpose rotation matrix, or rot back to xyz.
  double *ps = Rt, *pt = Rt+mDim, *pn = Rt+(mDim-1)*mDim ;

  /* Make a list of the active vertices. */
  int kVx, kVxAct[MAX_VX_ELEM], m = 0 ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    if ( !fNegAlpha[kVx] )
      kVxAct[m++] = kVx ;

  double *pCo0 = coor[ kVxAct[0] ] ;

  double sc ;
  switch ( m ) {
  case ( 0 ) :
    hip_err ( fatal, 0, "no positive node left in project_elem_planar, this shouldn't have happened." ) ; break ;
  case ( 1 ) :
    // One vertex left, just take that.
    break ;
  default:

    // First tangent from 0 to 1
    vec_diff_dbl ( coor[ kVxAct[1] ], pCo0, mDim, ps ) ;
    vec_norm_dbl ( ps, mDim ) ;

    if ( mDim == 2 ) {
      pn[0] = -ps[1] ;
      pn[1] =  ps[0] ;
    }
    else {
      // Second tangent,  0 to m-1.
      vec_diff_dbl ( coor[ kVxAct[m-1] ], pCo0, mDim, pt ) ;
      /* Make it orthogonal to s. */
      sc = scal_prod_dbl ( ps, pt, mDim ) ;
      sc *= -1 ;
      vec_add_mult_dbl ( pt, sc, ps, mDim, pt ) ;
      vec_norm_dbl ( pt, mDim ) ;

      // Normal.
      cross_prod_dbl ( ps, pt, mDim, pn ) ;
    }

    /* Fill the coordinates. */
    double co[MAX_DIM] ;
    for ( kVx = 1 ; kVx < m ; kVx++ ) {
      vec_diff_dbl ( coor[ kVxAct[kVx] ], pCo0, mDim, co ) ;
      mat_vec_dbl ( Rt, mDim, mDim, co, coor[ kVxAct[kVx] ] ) ;
    }

    /* Rotate additional point. */
    vec_diff_dbl ( xp, pCo0, mDim, co ) ;
    mat_vec_dbl ( Rt, mDim, mDim, co, xp ) ;

    /* By construction, vx 0 is at the origin. */
    double zero[MAX_DIM] = {0.} ;
    memcpy ( pCo0, zero, MAX_DIM*sizeof(*zero) ) ;
  }

  return ( 0 ) ;
}


/******************************************************************************
  minNormCo:   */

/*! Compute non-negative linearly exact interpol. coefficients for coordinates.
 */

/*

  Last update:
  ------------
  14jul17; move countNegCoeff to minNormEl, change type to void.
  9Sep16; treat 0-d case correctly, handle planar reduction that does not result in a singular matrix.
  15Feb16; support collapsed elements.
  13Jul15: conceived.


  Input:
  ------
  mDim:    spatial dimension
  mVx:     number of distinct vertices
  coor:    nodal coordinates
  fNegAlpha: nonzero for any alpha=0.
  xp:      coordinates of point to interpolate for
  tol:     a weight < tol, e.g. 1.e-10, is considered negative.
  fixNeg:  if non-zero, remove any final non-zero weights by projection.

  Changes To:
  -----------
  coor: planar projection of active coors in case of singular matrix
  xp: planar projection of xp in case of singular matrix

  Output:
  -------
  alpha:  interpolation coefficiens for each node of pElem.

  Returns:
  --------
  Number of negative coefficients remaining.

*/

void minNormCo( const int mDim, const int mVx,
               double coor[MAX_VX_ELEM][MAX_DIM],
               int fNegAlpha[MAX_VX_ELEM],
               double xp[MAX_DIM],
               const double tol, const int fixNeg,
               double alpha[MAX_VX_ELEM] ) {

  int kVx ;
  int found = 0 ;
  if ( mDim == 0 ) {
    /* Projection on a corner point. There should be only one non-negative vertex left. */
    for ( kVx = 0 ; kVx < mVx ; kVx++ )
      if ( fNegAlpha[kVx] ) {
        alpha[kVx] = 0.0 ;
      }
      else {
        alpha[kVx] = 1.0 ;
        if ( found )
          hip_err ( fatal, 0, "two positive nodes for 0-d interpolation in minNormCo" ) ;
        else
          found = 1 ;
      }
  }
  else {
    // Build the transpose of the base system matrix.
    int m, n ;
    const int lda = MAX_VX_ELEM ;
    double At[MAX_VX_ELEM*MAX_VX_ELEM], AtWk[MAX_VX_ELEM*MAX_VX_ELEM] ;
    const int lb = MAX_VX_ELEM ;
    double b[MAX_VX_ELEM] ;
    fillAtb ( mDim, coor, mVx, &n, At, lda, xp, b ) ;

    /* Add zero constraints for all duplicated vertices. */
    for ( kVx = 0 ; kVx < mVx ; kVx++ )
      if ( fNegAlpha[kVx] )
        addAtbNeg ( kVx, mVx, &n, At, lda, b ) ;


    int fNewNegAlpha = 1 ;
    int kMin ;
    int k, info = 0 ;
    while ( fNewNegAlpha ) {
      fNewNegAlpha = 0 ;

      // Copy At into AtWk, b into alpha and solve.
      // At and b remain unchanged, ready for constraints to be added.
      //printMat( "At", mVx, n, At, lda) ;
      //printMat( "b", mVx, 1, b, 1) ;
      if ( info != 8 )
        info = minNormSolve ( mVx, n, At, AtWk, lda, b, lb, alpha ) ;
      // for ( k = 0 ; k < m ; k++ )
      //   printf ( "  %6.2f (%1d)\n", alpha[k], fNegAlpha[k] ) ;

      if ( info == 8 ) {
        /* Singular matrix, collinear points. E.g. a 3-D elem reduced to a planar
           face with the target node not in the plane.
           Project remaining coordinates into the plane and try with mDim-1. */
        project_elem_planar ( mDim, mVx, coor, fNegAlpha, xp ) ;
        minNormCo ( mDim-1, mVx, coor, fNegAlpha, xp, tol, fixNeg, alpha ) ;
      }
      else if ( info ) {
        hip_err ( fatal, 0, "unspecified min norm solve error in minNormEl.\n"
                  "Send an SOS to your friendly hip developer." ) ;
      }
      else {
        // Check for negative coeffs.
        //fNewNegAlpha = findMostNegCoeff ( mVx, n, alpha, 0., fNegAlpha, &kMin ) ;
        fNewNegAlpha = findMostNegCoeff ( mVx, n, alpha, tol, fNegAlpha, &kMin ) ;

        if ( fNewNegAlpha ) {
          if ( mVx > n ) {
            // Can eleminate more vertices, constrain the negative coeff at kMin.
            addAtbNeg ( kMin, mVx, &n, At, lda, b ) ;
          }
          else {
            // Already at minimal number of support vertices for this dimension, project
            // one dimension down.
            info = 8 ;
          }
        }
      }
    }
  }

  return ;
}



/******************************************************************************
  vxList_flagDup:   */

/*! Given an element, make a copy of the vertex list for minnorm
 * flagging duplicate vertices.
 *  Note that this compacted element as an unordered list of vertices,
 *  type is destroyed.
 */

/*

  Last update:
  ------------
  15Feb16: conceived.


  Input:
  ------
  pElem = original element with possibly collapsed edges

  Changes To:
  -----------

  Output:
  -------
  co = list of coor of pElem
  fDupVx = nonzero for each duplicated vertex.

  Returns:
  --------
  number of non-duplicate vertices.

*/

int vxList_flagDup ( const elem_struct *pElem, const int mDim, const int mVx,
                     double coor[MAX_VX_ELEM][MAX_DIM],
                     int fDupVx[MAX_VX_ELEM] ) {

  vrtx_struct **ppVrtx = pElem->PPvrtx ;
  int k, k2, mDup=0 ;

  for ( k = 0 ; k < mVx ; k++ ) {
    memcpy ( coor[k], ppVrtx[k]->Pcoor, mDim*sizeof( coor[0][0]) ) ;

    /* Is this one already listed? */
    for ( k2 = 0, fDupVx[k] = 0 ; k2 < k ; k2++ ) {
      if ( ppVrtx[k] == ppVrtx[k2] ) {
        fDupVx[k] = 1 ;
        mDup++ ;
        break ;
      }
    }
  }

  return ( mVx-mDup ) ;
}



/******************************************************************************
  minNormEl:   */

/*! Compute non-negative linearly exact interpol. coefficients on an element.
 */

/*

  Last update:
  ------------
  15Feb16; support collapsed elements.
  13Jul15: conceived.


  Input:
  ------
  mDim:    spatial dimension
  pElem:   element with collapsed vertex list
  mVx:     number of distinct vertices
  xp:      coordinates of point to interpolate for
  tol:     a weight < tol, e.g. -1.e-10, is considered negative.
  fixNeg:  if non-zero, remove any final non-zero weights by projection.

  Changes To:
  -----------

  Output:
  -------
  alpha:  interpolation coefficiens for each node of pElem.

  Returns:
  --------
  Number of negative coefficients remaining.

*/

int minNormEl( const elem_struct *pElem, const int mDim, const int mVx,
               const double xp[MAX_DIM], const double tol, const int fixNeg,
               double alpha[MAX_VX_ELEM] ) {

  double coor[MAX_VX_ELEM][MAX_DIM] ;
  int fNegAlpha[MAX_VX_ELEM] ;
  vxList_flagDup ( pElem, mDim, mVx, coor, fNegAlpha ) ;

  /* Make a copy of xp, so we can modify the value during a possible
     planar projection. */
  double xpMod[MAX_DIM] ;
  memcpy ( xpMod, xp, MAX_DIM*sizeof(*xp) ) ;

  minNormCo( mDim, mVx, coor, fNegAlpha, xpMod, tol, fixNeg, alpha ) ;

  /* set coeff below abs(tol) to exact zero.
  int kVx ;
  double abstol = ABS(tol) ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    if ( alpha[kVx] < abstol )
      alpha[kVx] = 0. ; */

  zeroSmallCoeff( mVx, alpha, tol ) ;

  return ( 0 ) ;
}



/******************************************************************************
  interpolate_elem_minnorm:   */

/*! Interpolate a value in an element using the minimum norm solution.

  Last update:
  ------------
  13Jul17; define minnorm_tol in hip.c
  13Jul15; conceived.

  Input:
  ------
  pElem
  pVx:      vertex to interpolate for.
  pUnInt:   variable field to store the interpolated solution
  pVarList:

  Changes To:
  -----------
  pVx->Punknown ;

  Returns:
  --------
  0 on failure, 1 on success.

*/

int interpolate_elem_minnorm ( const elem_struct *pElem, vrtx_struct *pVx,
  double *pUnInt, const varList_s *pVarList ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mVx = pElT->mVerts ;
  const int mDim = pElT->mDim ;

  double alpha[MAX_VX_ELEM] ;
  double tol = -1.e-12 ; // Need to make set-able param.
  int fixNeg = 0 ;
  int mNegCoeff = minNormEl( pElem, mDim, mVx, pVx->Pcoor,
                             minnorm_tol, fixNeg, alpha ) ;


  double  *pU ;
  int kVx, nU, mU = pVarList->mUnknowns, nDim, kVxMin ;
  vrtx_struct **ppVx = pElem->PPvrtx ;
  /* Extrapolate from the closest vertex. */
  for ( nU = 0 ; nU < mU ; nU++ ) {
    pUnInt[nU] = 0. ;

    /* Extrapolate. */
    for ( kVx = 0 ; kVx < mVx ; kVx++ )
      pUnInt[nU] += alpha[kVx]*ppVx[kVx]->Punknown[nU] ;
  }

  return ( 1 ) ;
}



/******************************************************************************

  interpolate_elem_grad:
  Interpolate a value in an element using gradient extrapolation from
  the element centre.

  Last update:
  ------------
  21Dec10; pass a separate pUnInt.
  4Apr09; replace varTypeS with varList.
  27Oct00: introduce limiting.

  Input:
  ------
  pElem
  pVx:      vertex to interpolate for.
  pUnInt:   variable field to store the interpolated solution
  pVarList:

  Changes To:
  -----------
  pVx->Punknown ;

  Returns:
  --------
  0 on failure, 1 on success.

*/

int interpolate_elem_grad ( const elem_struct *pElem, vrtx_struct *pVx,
                            double *pUnInt, const varList_s *pVarList,
                            reco_enum reco ) {

  const elemType_struct *pElT ;

  double nodeNorm[MAX_VX_ELEM][MAX_DIM], grad[MAX_UNKNOWNS][MAX_DIM] ;
  double dist, distMin ;
  double elemVol, min2vx[MAX_DIM], uMin[MAX_UNKNOWNS], uMax[MAX_UNKNOWNS], *pU ;
  int kVx, nU, mU = pVarList->mUnknowns, nDim, kVxMin ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  pElT = elemType + pElem->elType ;
  elemVol = 0. ;

  if ( reco == reco_el ) {
    /* Reset the gradients. */
    for ( nDim = 0 ; nDim < pElT->mDim ; nDim++ )
      for ( nU = 0 ; nU < mU ; nU++ )
        grad[nU][nDim] = 0. ;

    /* Get a list of nodal normals. */
    uns_elem_normls ( pElem, nodeNorm ) ;


    for ( nU = 0 ; nU < mU ; nU++ ) {
      uMin[nU] =  TOO_MUCH ;
      uMax[nU] = -TOO_MUCH ;
    }

    /* Calculate an average gradient. */
    for ( nDim = 0 ; nDim < pElT->mDim ; nDim++ )
      for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
        pU = ppVx[kVx]->Punknown ;

        /* Solution. */
        for ( nU = 0 ; nU < mU ; nU++ ) {
          grad[nU][nDim] += pU[nU]*nodeNorm[kVx][nDim] ;

          uMin[nU] = MIN( uMin[nU], pU[nU] ) ;
          uMax[nU] = MAX( uMax[nU], pU[nU] ) ;
        }

        /* And a volume. */
        elemVol += ppVx[kVx]->Pcoor[nDim]*nodeNorm[kVx][nDim] ;
      }
    elemVol /= pElT->mDim ;
  }




  /* Find the closest vertex in the element to pVx */
  for ( distMin = TOO_MUCH, kVxMin = kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    dist = sq_distance_dbl ( pVx->Pcoor, ppVx[kVx]->Pcoor, pElT->mDim ) ;
    if ( dist < distMin ) {
      kVxMin = kVx ;
      distMin = dist ;
    }
  }

  double dVal ;
  if ( reco == reco_el ) {
    /* Vector from the closest vertex to the location sought. */
    vec_diff_dbl ( pVx->Pcoor, ppVx[kVxMin]->Pcoor, pElT->mDim, min2vx ) ;


    /* Extrapolate from the closest vertex. */
    for ( nU = 0 ; nU < mU ; nU++ ) {
      pUnInt[nU] = ppVx[kVxMin]->Punknown[nU] ;

      /* Extrapolate. */
      for ( nDim = 0 ; nDim < pElT->mDim ; nDim++ )
        pUnInt[nU] += min2vx[nDim]*grad[nU][nDim]/elemVol ;

      /* Limit. */
      pUnInt[nU] = MIN( pUnInt[nU], uMax[nU] ) ;
      pUnInt[nU] = MAX( pUnInt[nU], uMin[nU] ) ;
    }
  }
  else if ( reco == reco_flag ) {
    /* Just pick the closest value, but overwrite the target only if
       the donor value is non-zero. */
    for ( nU = 0 ; nU < mU ; nU++ ) {
      dVal = ppVx[kVxMin]->Punknown[nU] ;
      if ( dVal > Grids.epsOverlap ) // yes, not really the right meaning for this use, but it's handy.
        pUnInt[nU] = dVal ;
      else
        pUnInt[nU] = 0. ;
    }
  }
  else {
    hip_err ( fatal, 0, "unknown reco type in interpolate_elem_grad. This shouldn't have happeneed." ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************
  interpolate_elem:   */

/*! wrapper for element-based interpolation.
 */

/*

  Last update:
  ------------
  10Jul15: conceived.


 Input:
  ------
  pElem     element to interpolate on.
  pVx:      vertex to interpolate for.
  pVarList: types of variables.

  Changes To:
  -----------
  pUnInt:   variable field to store the interpolated solution

  Returns:
  --------
  1 on failure, 0 on success.

*/

int interpolate_elem ( const elem_struct *pElem, vrtx_struct *pVx,
                       double *pUnInt,
                       const varList_s *pVarList, reco_enum reco ) {

  if ( reco == reco_el || reco == reco_flag )
    interpolate_elem_grad ( pElem, pVx, pUnInt, pVarList, reco ) ;
  else
    interpolate_elem_minnorm ( pElem, pVx, pUnInt, pVarList ) ;

  return ( 0 ) ;
}


/******************************************************************************

  interpolate_elem_axi:
  Given an axisymmetric 3D element, interpolate the solution axisymmetrically
  for a given location. Axisymmetry is assumed around the x-axis.
  We interpolate simply within the element, although that will interpolate
  along the secant and give errors when interpolating swirled flow fields. We
  presume here, that the angle between periodic planes is very small.
  Return the vector to the interpolation point, as well.


  Last update:
  ------------
  21Dec10; pass a separate pUnInt.
  4Apr09; replace varTypeS with varList.
  : conceived.

  Input:
  ------
  pElem
  pVx:      the vertex location to interpolate for.
  pUnInt:   variable field to store the interpolated solution
  pVarList: the variable type (and how many there are.)
  reco:     interpolation type.


  Changes To:
  -----------
  gc:       a normalised vector in direction of the gravity center of the element,
            projected onto a y,z plane.

  Returns:
  --------
  0 on failure, 1 on success.

*/

int interpolate_elem_axi ( const elem_struct *pElem, vrtx_struct *pVx,
                           double *pUnInt,
                           const varList_s *pVarList, double gc[MAX_DIM],
                           reco_enum reco ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const double *pCo ;

  int kVx ;
  vrtx_struct someVx ;
  double vxCoor[MAX_DIM], r ;

  gc[0] = 0. ;
  gc[1] = 0. ;
  gc[2] = 0. ;


  if ( pElT->mDim == 3 ) {
    /* Find the gravity center of the element. */
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
      pCo = pElem->PPvrtx[kVx]->Pcoor ;
      gc[1] += pCo[1] ;
      gc[2] += pCo[2] ;
    }
    gc[1] /= pElT->mVerts ;
    gc[2] /= pElT->mVerts ;
    vec_norm_dbl ( gc, 3 ) ;
  }
  else
    /* No y component. */
    gc[1] = 1. ;


  /* Place the query location along the vector to the gravity center in the y,z plane. */
  someVx.Pcoor    = vxCoor ;
  someVx.Punknown = pVx->Punknown ;

  pCo = pVx->Pcoor ;
  r = sqrt ( pCo[1]*pCo[1] + pCo[2]*pCo[2] ) ;
  vxCoor[0] = pCo[0] ;
  vxCoor[1] = r*gc[1] ;
  vxCoor[2] = r*gc[2] ;


  return ( interpolate_elem ( pElem, &someVx, pUnInt, pVarList, reco ) ) ;
}


/* ***************************************************** */

/* Extract cylindrical coordinates. */

VALU_TYPE *vrtx2rad( const DATA_TYPE *Pdata ) {

  static double radCoor[MAX_DIM] ;
  const double *pCo ;

  /* Suppress the compiler warning. */
  vrtx_struct *Pvrtx ;
  Pvrtx = ( vrtx_struct * ) Pdata ;
  pCo = Pvrtx->Pcoor ;

  radCoor[0] = pCo[0] ;
  radCoor[1] = sqrt ( pCo[1]*pCo[1] + pCo[2]*pCo[2] ) ;

  return ( radCoor ) ;
}

/******************************************************************************

  heap_vx_elem:
  Add all forming vertices of an element to the heap.

  Last update:
  ------------
  13Feb19; in stencil, promote nEl to ulong_t
  13Sep2: conceived.

  Input:
  ------
  pEl: the element
  pCoTo: reference point to calculate the distance to.

  Changes To:
  -----------
  pHeap:

*/

static void heap_vx_elem ( const elem_struct *pEl, const int mDim, const double *pCoTo,
                           heap_s *pHeap ) {

  const elemType_struct *pElT ;
  int mVxEl, kVx ;
  vrtx_struct *pVx, **ppVx ;
  double dx, dy, dz, *pCo ;
  vxDist_s vxDist ;

  pElT = elemType + pEl->elType ;
  mVxEl = pElT->mVerts ;
  ppVx = pEl->PPvrtx ;

  for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
    pVx = ppVx[kVx] ;
    if ( !pVx->mark ) {
      /* Add this one to the heap. */
      pVx->mark = 1 ;
      pCo = pVx->Pcoor ;
      dx =                   pCo[0] - pCoTo[0] ;
      dy =                   pCo[1] - pCoTo[1] ;
      dz = ( mDim == 2 ? 0 : pCo[2] - pCoTo[2] ) ;
      vxDist.pVx = pVx ;
      vxDist.dist = dx*dx + dy*dy + dz*dz ;
      add_heap ( pHeap, &vxDist ) ;
    }
  }

  return ;
}

static void stencil ( uns_s *pUnsFrom, const double *pCoTo, vrtx_struct *pVxNrst,
                      heap_s *pHeap ) {

  const elemType_struct *pElT ;
  elem_struct *pEl ;
  int mVxEl, kVx ;
  ulong_t nEl ;
  vrtx_struct *pVx, **ppVx ;

  /* Loop over all cells formed with the nearest node, add 1st O neighbours. */
  nEl = 0 ;
  while ( loop_toElem ( pUnsFrom->pllVxToElem, pVxNrst->number, &nEl,
                        ( elem_struct ** ) &pEl ) ) {
    /* Add all the nodes of this element. */
    heap_vx_elem ( pEl, pUnsFrom->mDim, pCoTo, pHeap ) ;

    pElT = elemType + pEl->elType ;
    mVxEl = pElT->mVerts ;
    ppVx = pEl->PPvrtx ;

    /* Loop over all forming nodes, add their neighbours. */
    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
      pVx = ppVx[kVx] ;
      nEl = 0 ;
      while ( loop_toElem ( pUnsFrom->pllVxToElem, pVx->number, &nEl,
                            ( elem_struct ** ) &pEl ) )
        heap_vx_elem ( pEl, pUnsFrom->mDim, pCoTo, pHeap ) ;
    }
  }

  return ;
}

/******************************************************************************
  make_intp_src_table:   */

/*! Make a correspondence table for variable interpolation,
 *
 * See Table 5 in doc in sec. on partial interpolation.
 *
 */

/*

  Last update:
  ------------
  2Jul12: conceived.


  Input:
  ------
  mUn1:      number of variables on the donor grid.
  nV2[]:     variable in pos k in donor is pos nV2[k] in rcpt.
  mUn2:      number of variables on the rcpt. grid.
  nV1[]:     variable in pos k in rcpt is pos nV1[k] in donor.
  mUnInterp: total number of variables to interpolate.

  Output:
  -------
  iVarSrc[ovlp][k]: index of source of variable k, with overlap 0 or 1:
                    0 = donor, 1 = rcpt, 2 = freestream of donor.
  kVarSrc[ovlp][k]: pos of source of variable k, with overlap 0 or 1 in
                    the source variable field.

*/

void make_intp_src_table ( const int mUn1, const int nV2[],
                           const int mUn2, const int nV1[],
                           int mUnIntp,
                           int iVarSrc[][MAX_UNKNOWNS], int kVarSrc[][MAX_UNKNOWNS] ) {

  int k, k1, k2 ;
  int mV = 0 ;

  /* First the source variables from the donor. */
  for ( k = 0 ; k < mUn1 ; k++ ) {
    /* Overlap, Donor present. */
    iVarSrc[1][mV] = 0 ;
    kVarSrc[1][mV] = k ;

    /* No overlap. */
    k2 = nV2[mV] ;
    if ( k2 != -1 ) {
      /* matching rcpt. variable, use it. */
      iVarSrc[0][mV] = 1 ;
      kVarSrc[0][mV] = k2 ;
    }
    else {
      /* No match in rcpt, use freestream. */
      iVarSrc[0][mV] = 2 ;
      kVarSrc[0][mV] = k ;
    }
    mV++ ;
  }

  /* Look at the remaining variables defined only on the recipient mesh. */
  for ( k = 0 ; k < mUn2 ; k++ ) {
    k1 = nV1[k] ;
    if ( k1 == -1 ) {
      /* This one is not present on the donor solution. Keep the recipient sol. */
      iVarSrc[1][mV] = 1 ;
      kVarSrc[1][mV] = k ;
      iVarSrc[0][mV] = 1 ;
      kVarSrc[0][mV] = k ;
      mV++ ;
    }
  }

  if ( mV - mUnIntp )
    hip_err ( fatal, 0, "panic in make_intp_src_table: mismatch in variable numbers" ) ;

  return ;
}

/******************************************************************************
  overwrite_unknown:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  2Jul12; use i/kVarSrc Tables.
  21Dec10: conceived.


  Input:
  ------
  pUnDon: unknowns of the donor grid
  pUnTo: unknowns at the recpient grid
  pUnDef: default variables
  mUnInpt: number of variables in the final solution
  iVarSrc[ovlp][]: for each var and ovlp=0,1: 0 for donor, 1 for rcp, 2 for default variable source.
  kVarSrc[ovlp][]: position of the variable to use in each source.
  overlap: 1 for overlap, 0 for none.

  Changes To:
  -----------
  pUnTo: unknowns to overwrite

*/

void overwrite_unknown  ( double *pUnDon, double *pUnTo, double *pUnDef,
                          int mUnIntp,
                          int iVarSrc[][MAX_UNKNOWNS], int kVarSrc[][MAX_UNKNOWNS],
                          int ovlp ) {

  /* Store the current variables on the recpient grid before overwriting. */
  double unTo[MAX_UNKNOWNS] ;
  memcpy ( unTo, pUnTo, mUnIntp*sizeof( double ) ) ;

  int k ;
  for ( k = 0 ; k < mUnIntp ; k++ ) {
    switch ( iVarSrc[ovlp][k] ) {
    case 0 : /* use the donor mesh variable. */
      pUnTo[k] = pUnDon[ kVarSrc[ovlp][k] ] ; break ;
    case 1 : /* retain the target grid variable. */
      pUnTo[k] =   unTo[ kVarSrc[ovlp][k] ] ; break ;
    case 2 : /* use the default/freestream variable. */
      pUnTo[k] = pUnDef[ kVarSrc[ovlp][k] ] ; break ;
    default :
      hip_err ( fatal, 0, " panic in overwrite_unknowns, unknown source identifier.\n");
    }
  }

  return ;
}

/******************************************************************************
  init_interp:   */

/*! Initialise/allocate for interpolation.
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  17Feb17: allocate SVD stuff only for reco_1/2, not for !reco_el.
  10May11, extracted from uns_interpolate.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void intp_init ( uns_s *pUnsFrom, vrtx_struct *pSomeVx, int *pmVxMax,
                 const vrtx_struct ***pppVxStencil, double **pDistStencil ) {

  double someCoor[MAX_DIM] ;
  const int mDim = pUnsFrom->mDim ;
  int poly ;

  if  ( reco == reco_el ) {
    /* Linear reco within the containing element only. */
    pSomeVx->Pcoor = someCoor ; pSomeVx->Pcoor[2] = 0. ; pSomeVx->number = -11 ;
  }
  else if ( reco == reco_1 || reco == reco_2 ){
    /* Least squares reco. */
    reset_vx_mark ( pUnsFrom ) ;

    /* How many coefficients to calculate? */
    poly = ( reco == reco_1 ? 1 : 2 ) ;
    m = mBsFunc[mDim][poly] ;
    /* How many reco points? */
    n = ceil( mVxRecoFactor*m ) ;
    /* At most this many in the matrices. */
    *pmVxMax = 3*n ;

    /* A list of support vertices. */
    *pppVxStencil = arr_malloc ( "pppVxStencil in init_interp", pUnsFrom->pFam, *pmVxMax,
      sizeof( vrtx_struct * )) ;
    /* And their distance to target. */
    *pDistStencil = arr_malloc ( "pDistStencil in init_interp", pUnsFrom->pFam, *pmVxMax,
      sizeof( double )) ;

    /* NR array alloc for SVD. */
    a   = matrix(1,*pmVxMax,1,*pmVxMax);
    v   = matrix(1,m,1,m);
    w   = vector(1,*pmVxMax);
    wt  = vector(1,*pmVxMax);
    f   = vector(1,*pmVxMax);
    b   = vector(1,MAX_DIM);
    d   = vector(1,m);
    c   = vector(1,m);
    w   = vector(1,m);
  }

  return ;
}

/******************************************************************************
  alloc_unknown:   */

/*! allocate fields for unknowns on the recipien grid.
 *
 * See table in doc on partial interpolation to determine size. It can never
 * be smaller than the current size of the recipient variable vector.
 */


/*

  Last update:
  ------------
  2Jul12; base counts on revised Table of variable sources, see doc.
  30Jun12; update header description.
  10May11, extracted from uns_interpolate.


  Input:
  ------
  pUnsFrom: donor mesh
  pUnsTo:   recipient mesh
  nV2[]:    variable in pos k in donor is pos nV2[k] in rcpt.
  pmUn:     number of unknowns on donor mesh.
  nV1[]:    variable in pos k in rcpt is pos nV1[k] in donor.
  pmUn2:    number of unknowns on recipient mesh

  Output:
  -------
  nV2[]:    index of the corresponding variable in the rcpt. mesh, -1 otherwise.

  Returns:
  --------
  1 on failure, 0 on success

*/

int intp_alloc_unknown ( uns_s *pUnsFrom, uns_s *pUnsTo, int nV2[],
                         int *pmUn, int nV1[], int *pmUn2 ) {

  int k1, k2 ;
  const var_s *pVar1, *pVar2 ;
  const char *name1, *name2 ;
  int mUnIntp = 0 ;
  varList_s newVarList ;

  /* Is there a solution with pUnsFrom? */
  if ( pUnsFrom->varList.varType == noVar ) {
    sprintf ( hip_msg, "mesh to interpolate from has no solution.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  *pmUn = pUnsFrom->varList.mUnknowns ;
  *pmUn2 = pUnsTo->varList.mUnknowns ;


  /* Is there a solution of the same dimension with pUnsTo? */
  if ( pUnsTo->varList.varType == noVar ) {
    mUnIntp = *pmUn ;
    /* No solution yet with pUnsTo, allocate a solution field. */
    realloc_unknowns ( pUnsTo, 0, mUnIntp ) ;

    pUnsTo->varList = pUnsFrom->varList ;

    /* Set flags and copy variables. */
    for ( k1 = 0 ; k1 < *pmUn ; k1++ )
      nV2[k1] = -1 ;
  }

  else {
    /* Target grid has variables. Find out which ones match. */
    sprintf ( hip_msg, "mesh to interpolate to has a solution.\n"
              "          This solution will be (partially) overwritten.\n" ) ;
    hip_err ( warning, 2, hip_msg ) ;

    newVarList = pUnsFrom->varList ;
    /* Which variables of pUnsFrom are already present in pUnsTo? */
    for ( k2 = 0 ; k2 < *pmUn2 ; k2++ )
      nV1[k2] = -1 ;
    for ( k1 = 0 ; k1 < *pmUn ; k1++ ) {
      pVar1 = pUnsFrom->varList.var + k1 ;
      newVarList.var[mUnIntp] = *pVar1 ;
      mUnIntp++ ;

      name1 = pVar1->name ;
      nV2[k1] = -1 ; /* -1 = not present. */
      for ( k2 = 0 ; k2 < *pmUn2 ; k2++ ) {
        pVar2 = pUnsTo->varList.var + k2 ;
        name2 = pVar2->name ;

        if ( !strcmp ( name1, name2 ) ) {
          /* Match. */
          nV2[k1] = k2 ;  /* variable in pos k1 in donor is pos k2 in rcpt.. */
          nV1[k2]= k1 ;   /* variable in pos k2 in rcpt. is pos k1 in donor. */
          break ;
        }
      }
    }

    /* Are there any variables in the rcpt. mesh that are not present in the donor? */
    for ( k2 = 0 ; k2 < *pmUn2 ; k2++ ) {
      if ( nV1[k2] == -1 ) {
        pVar2 = pUnsTo->varList.var + k2 ;
        newVarList.var[mUnIntp] = *pVar2 ;
        mUnIntp++ ;
      }
    }

    /* Enlarge pUnkown in in pUnsTo, if necessary. Shrinking is done at the
       end after interpolation, as we exchange variable positions on the fly. */
    newVarList.mUnknowns = mUnIntp ;
    pUnsTo->varList = newVarList ;
    if ( mUnIntp > *pmUn2 )
      realloc_unknowns ( pUnsTo, *pmUn2, mUnIntp ) ;
  }

  return ( mUnIntp ) ;
}

/******************************************************************************
  intp_tree:   */

/*! plant a kdtree over the grids for all donor nodes.
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  22Jun11; switch to kdtree.
  10May11, extracted from uns_interpolate.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

kdroot_struct *kd_intp_tree ( uns_s *pUnsFrom, uns_s *pUnsTo, const int axi ) {

  const int mDim = pUnsFrom->mDim ;
  int nDim ;
  double ll[MAX_DIM], ur[MAX_DIM] ;
  double dist ;
  double maxRatio = 0., dFrom, dTo, ratio ;

  /* Declare a tree. Use the maximum dimensions of either mesh. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    ll[nDim] = MIN( pUnsFrom->llBox[nDim], pUnsTo->llBox[nDim] ) ;
    ur[nDim] = MAX( pUnsFrom->urBox[nDim], pUnsTo->urBox[nDim] ) ;

    /* Enlarge the box a bit. */
    dist  = .1*( ur[nDim] - ll[nDim] ) ;
    ur[nDim] += dist ;
    ll[nDim] -= dist ;

    /* Compare coverage, issue a warning if sizes are out by more than
       1 Order of magnitude to avoid unit mismatches m vs mm. */
    dFrom =  pUnsFrom->urBox[nDim] - pUnsFrom->llBox[nDim] ;
    dTo   =  pUnsTo->urBox[nDim]   - pUnsTo->llBox[nDim] ;
    ratio = dFrom/dTo ;
    maxRatio = MAX( maxRatio, ratio) ;
    maxRatio = MAX( maxRatio, 1./ratio) ;
  }

  if ( maxRatio > 2 && verbosity > 1 ) {
    sprintf ( hip_msg,
              "very large discrepancy of %g in mesh dimensions"
              " in uns_interpolate\n", maxRatio ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }

  kdroot_struct *pTree ;

  if ( !axi )
    pTree = kd_ini_tree ( pUnsFrom->pFam, "intp_tree", mDim, Grids.epsOverlap,
                          ll, ur, vrtx2coor ) ;
  else if ( pUnsFrom->mDim == 2 ) {
    /* Make a 2D tree for x, y. In this case we use the standard coordinate
       extraction function vrtx2coor. The 2D vertices in the tree are fine, the
       3D vertices to be compared need to be preprocessed to offer x, r coordinates. */
    pTree = kd_ini_tree ( pUnsFrom->pFam, "intp_tree", 2, Grids.epsOverlap,
                          ll, ur, vrtx2coor ) ;
  }
  else {
    /* Make a 2D tree for x, r. Make sure the bounds contain it.
       In this case we use an extraction function that converts x,y,z into x,r.
       Since all coordinates are modified by vrtx2rad, we need no preprocessing. */
    ll[2] = MAX( ll[2], ll[2] ) ;
    ur[2] = MAX( ur[2], ur[2] ) ;
    pTree = kd_ini_tree ( pUnsFrom->pFam, "intp_tree", 2, Grids.epsOverlap,
                          ll, ur, vrtx2rad ) ;
  }

  if ( !pTree ) {
    sprintf ( hip_msg, "failed to plant a tree in intp_tree.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }



  /* Toss all vertices of pUnsFrom into a tree. */
  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;

  pChunk = NULL ;
  while ( loop_verts ( pUnsFrom, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number )
        kd_add_data ( pTree, pVx ) ;

  return ( pTree ) ;
}

/******************************************************************************

  uns_interpolate:
  Take a solution from one mesh and interpolate it onto another one.

  Last update:
  ------------
  14Dec19; use new fun name for kd_m_nearest_datas.
  11Nov17; ensure valid numbering.
  13July17; extract find_el_tree_walk from uns_interpolate.
  17Feb17: free SVD mem only for reco_1/2, not for !reco_el.
  18Dec15; change logic in searching for containing elements: drop any
           detailed searches if first search reveals to be outside interpolation
           rim.
  7Se15: introduce intFcTol set by in-fc to control when a vertex is
        considered outside an element for interpolation.
  6Apr13; promote possibly large ints to ulong_t
          commenting out block testing on uninitialised 'found'.
  3Sep12; fix bug with overlap always 0 for l-s interpolation.
  2Jul12; rework use of variables from donor/rcpt/freest according to table in doc.
  22Jun11; switch to kdtree.
  12May11; rewrite, split up into smaller funs, use nearest_datas.
  8May11; initialise nV2 for target grid without solution.
  3Apr11; issue a warning if the box sizes are too disparate to catch unit errors.
          issue status.
  21Dec10; treat incomplete/partially equivalent sets of unknowns
  4Apr09; replace varTypeS with varList.
  14Sep06; allow interpolation of partially covered grids.
  2Apr05; comment code left over from using normal equations.
  28Jun03; fix bug in calling ll[3] for x,r bounds.
  9Jan01; copy restart info over.
  8Apr00; intro axi.
  : conceived.

  Input:
  ------
  pUnsFrom:
  pUnsTo:
  axi:      if 1, the mesh to interpolate from is considered a thinly
            sliced axisymmetric mesh that is supposed to be interpolated
            onto a whole circumferential mesh.


  Changes To:
  -----------
  pUnsTo

  Returns:
  --------
  0 on failure, 1 on success.

*/

int uns_interpolate ( uns_s *pUnsFrom, uns_s *pUnsTo, char cAxi ) {

  const int mDim = pUnsFrom->mDim ;
  const vrtx_struct **pVxStencil ;
  double *distStencil ;
  chunk_struct *pChunk ;
  vrtx_struct *pVxBeg, *pVxEnd, someVx, *pSomeVx = &someVx, *pVxTo ;
  int nBeg, nEnd, mUn, mUn2, axi = 0, domainOverlap, mVxMaxStencil ;
  ulong_t mVxTo = pUnsTo->mVertsNumbered ;
  int mDontInt = 0 ;
  kdroot_struct *pTree ;
  ulong_t mVxDone = 0 ;
  ulong_t mVxIntp ;
  int iPercDone = 0 ;
  double *pUn, gc[MAX_DIM],
    v_r, v_th, *pCoTo, hEdge, pUnInt[MAX_UNKNOWNS] ;
  int nV2[MAX_UNKNOWNS], nV1[MAX_UNKNOWNS] ;
  int iVarSrc[2][MAX_UNKNOWNS], kVarSrc[2][MAX_UNKNOWNS] ;
  int mOutWarningNotGiven = 1 ;

  /* For non-elem reco. */
  int kVx, mSing ;
  ulong_t mRed=0, sumRed=0 ;


  /* Axi treatment is only done for a 3D mesh. */
  if ( pUnsTo->mDim == 2 ) axi = 0 ;
  else if ( tolower( cAxi ) == 'a' ) axi = 1 ;

  /* Do they have the same dimensions? */
  if ( pUnsTo->mDim != pUnsFrom->mDim && ( !axi || pUnsTo->mDim == 2 ) ) {
    sprintf ( hip_msg, "interpolation needs same dimensions, you have %d-D to %d-D.\n",
             pUnsFrom->mDim, pUnsTo->mDim ) ;
    hip_err ( fatal, 0, hip_msg ) ; }


  /* Ensure numbering. */
  if ( pUnsFrom->numberedType == invNum )
    number_uns_grid ( pUnsFrom ) ;
  if ( pUnsTo->numberedType == invNum )
    number_uns_grid ( pUnsTo ) ;

  /* Allocate for least squares, etc. */
  iniTime = times ( &timings_int ) ;
  intp_init ( pUnsFrom, &someVx, &mVxMaxStencil, &pVxStencil, &distStencil ) ;

  /* Allocate required uknown spaces. */
  int mUnIntp = intp_alloc_unknown ( pUnsFrom, pUnsTo, nV2, &mUn, nV1, &mUn2 ) ;

  /* Make a table of variable correspondence/use. */
  make_intp_src_table ( mUn, nV2, mUn2, nV1, mUnIntp, iVarSrc, kVarSrc ) ;


  hip_err( info, 3, "Interpolation status: adding vertices to the data-tree" ) ;
  pTree = kd_intp_tree ( pUnsFrom, pUnsTo, axi ) ;



  /* Generate vertex to element pointers, needed for reco_el only. */
  if ( reco == reco_el || reco == reco_min_norm  || reco == reco_flag ) {
    hip_err( info, 3, "Interpolation status: generating vertex to element pointers" ) ;
    pUnsFrom->pllVxToElem = make_vxToElem ( pUnsFrom ) ;
  }



#ifdef TESTFUNC
  intp_testfunc_set ( pUnsFrom ) ;
#endif





  /* Retain restart info. */
  pUnsTo->restart = pUnsFrom->restart ;



  hip_err( info, 3, "Interpolation status: interpolating for vertices" ) ;
  if ( verbosity > 2 ) {
    printf ( "                 completed   0%%" ) ;fflush ( stdout ) ;
  }


  /* Loop over all vertices of pUnsTo. */
  int mRgTot = 0, mOut = 0, mReallyOut = 0 ;
  int closeToElem ;
  const elem_struct *pEl ;
  pChunk = NULL ;
  while ( loop_verts ( pUnsTo, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
    for ( pVxTo = pVxBeg ; pVxTo <= pVxEnd ; pVxTo++ )
      if ( pVxTo->number ) {
        mVxDone++ ;
        pCoTo = pVxTo->Pcoor ;
        domainOverlap = 1 ;


        if ( pUnsFrom->mDim == 2 && axi )
          /* This implies that pUnsTo->mDim == 3 and the tree in x,y.
             Turn x,y,z into x,r, where r is compared to y in the 2D tree. */
          pSomeVx->Pcoor = vrtx2rad ( pVxTo ) ;
        else
          pSomeVx = pVxTo ;


        if ( reco == reco_el || reco == reco_min_norm || reco == reco_flag ) {
          /* Interpolate on one element. */
          /* Find the closest vertex of pUnsFrom. */
          pEl = find_el_tree_walk ( pSomeVx, pUnsFrom, pTree,
                                    intPolRim, intFcTol, intFullTol,
                                    &mOut, &mReallyOut, &domainOverlap ) ;

          if ( axi )
            /* Axi. */
            interpolate_elem_axi
              ( pEl, pVxTo, pUnInt, &pUnsFrom->varList, gc, reco ) ;
          else
            interpolate_elem ( pEl, pVxTo, pUnInt, &pUnsFrom->varList, reco ) ;
        }


        else {
          /* Least squares interpolation: reco_1/2. */
          /* Find the closest vertex of pUnsFrom.
             pVxNrst = nearest_data ( pTree, pSomeVx, &dist ) ; */
          mVxIntp = kd_m_nearest_datas
            ( pTree, pSomeVx, mVxMaxStencil,
              (const void **) pVxStencil, sizeof( vrtx_struct* ),
              distStencil, &mRgTot ) ;

          /* When using least-squares, base domainOverlap on distance. */
          if ( mVxIntp == 0 &&
               distStencil[0] > intPol_dist_inside*Grids.epsOverlap ) {
            /* Not covered by the overlayed grid, don't interpolate. */
            mDontInt++ ;
            domainOverlap = 0 ;
          }
          else {
            /* Reconstruct a linear or quadratic based on closest
               neighbouring nodes. In 2/3-D open a heap for each
               quadrant/octant and find a minimum number in each. */

            mSing = lsfit_svd ( pVxTo, pUnInt, pVxStencil,
                                mDim, m, mVxIntp, mUn ) ;

            if ( mSing ) {
              mRed++ ;
              sumRed += mSing ;
            }
          }
        }




        /* Axi cases need a rotation of the velocities. */
        if ( axi ) {
          pUn = pUnInt ;

          if ( pUnsFrom->mDim == 2 ) {
            /* Add a zero w component. */
            pUn[4] = pUn[3] ;
            pUn[3] = 0. ;
          }

          /* Rotate a swirl component around. Note that gc is normalised. */
          v_r  =  gc[1]*pUn[2] + gc[2]*pUn[3] ;
          v_th = -gc[2]*pUn[2] + gc[1]*pUn[3] ;

          /* Coordinates to interpolate for. */
          gc[1] = pCoTo[1] ;
          gc[2] = pCoTo[2] ;
          vec_norm_dbl ( gc, 3 ) ;
          pUn[2] = gc[1]*v_r - gc[2]*v_th ;
          pUn[3] = gc[2]*v_r + gc[1]*v_th ;
        }

        /* Write the interpolated solution over the existing one. */
        overwrite_unknown
          ( pUnInt, pVxTo->Punknown, pUnsFrom->varList.freeStreamVar,
            mUnIntp, iVarSrc, kVarSrc, domainOverlap ) ;


        if ( mOutWarningNotGiven && verbosity > 1 && mOut > 10000 && mOut > .1*mVxDone ) {
          mOutWarningNotGiven=0 ;
          printf ( "\n\n" ) ;
          hip_err ( warning, 1,
                    "More than 20%% of nodes are found to be outside the donor grid,\n"
                    "        which triggers a global search through all boundary cells.\n"
                    "        Consider increasing the tolerance 'in-fc'.\n" ) ;
          printf ( "\b\b\b\b%3d%%", iPercDone ) ; fflush ( stdout ) ;
        }


        if ( verbosity > 2 &&
             100*mVxDone/pUnsTo->mVertsNumbered > iPercDone ) {
          iPercDone = 100*mVxDone/pUnsTo->mVertsNumbered ;
          printf ( "\b\b\b\b%3d%%", iPercDone ) ; fflush ( stdout ) ;
        }
      }
  }

  if ( verbosity > 2 ) {
    printf ( "\b\b\b\b100%%\n" ) ; ; fflush ( stdout ) ;
  }

  if ( verbosity > 4 && mReallyOut ) {
    sprintf ( hip_msg, "%d nodes found to be too far away from the best containing cell.\n"
              " in uns_interpolate.\n"
              " This may happen on coarse grids, or adjust in-fc-tol to a larger value.", mReallyOut) ;
    hip_err ( warning, 2, hip_msg ) ;
  }

  if ( verbosity > 2 && reco == reco_1) {
    sprintf ( hip_msg,
              " Average no of vx in range: %g\n", (double)mRgTot/mVxDone ) ;
    hip_err ( info, 2, hip_msg ) ;
  }



#ifdef TESTFUNC
  intp_testfunc_check ( pUnsTo ) ;
#endif




  if ( reco == reco_1 || reco == reco_2 ) {

    /* NR array alloc for SVD. */
    free_matrix(a,1,mVxMaxStencil,1,m);
    free_matrix(v,1,m,1,m);
    free_vector(wt,1,mVxMaxStencil);
    free_vector(f,1,mVxMaxStencil);
    free_vector(d,1,m);
    free_vector(c,1,m);


  }



  if ( reco != reco_el && verbosity > 2 ) {
    sprintf ( hip_msg,
              " interpolated %"FMT_ULG" vertices,\n"
              "          %"FMT_ULG" (%g%%) using lower order reconstruction,"
              " avg. red. %g coeff.\n",
              mVxTo,
              mRed, 100.*mRed/mVxTo, ( mRed ? 1.*sumRed/mRed : 0. ) ) ;
    hip_err ( info, 2, hip_msg ) ;
  }

  if ( mDontInt && verbosity > 2 ) {
    sprintf ( hip_msg,
              " %d nodes were not interpolated as they fell outside\n"
              "          the rim around the providing grid.\n", mDontInt ) ;
    hip_err ( info, 2, hip_msg ) ;
  }


  /* Free the tree, the vertex to element connectivity. */
  kd_del_tree ( &pTree ) ;
  free_toElem ( &pUnsFrom->pllVxToElem ) ;

  if ( verbosity > 3 ) {
    nowTime = times ( &timings_int ) ;
    sprintf ( hip_msg,
              " %g sec elapsed time.\n", (nowTime-iniTime)*1./clk_ticks ) ;
    hip_err ( info, 2, hip_msg ) ;
  }


  return ( 1 ) ;
}


