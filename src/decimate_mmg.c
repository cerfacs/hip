/*
  decimate_surface.c:
*/

/*! Decimate a surface mesh
 *
 *   more details
 *
 */


/*
  Last update:
  ------------
  15Dec17; updgrade mmg to repo version, new file structure of build.
  ; conceived



  This file contains:
  -------------------

*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#include "mmg/libmmg.h"
// Previous to MMG7 all ints where 32.  Swith in makefile.h or cmake ( automatic )
#ifdef MMG_INT32
  #define MMG5_int int
#endif
#include <time.h>

#define CV const vrtx_struct
#define VX vrtx_struct

extern const elemType_struct elemType[] ;
extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

/* Store faces and normals either side of a surface edge. */
typedef struct {
  vrtx_struct *pVrtx[2] ;
  elem_struct *pElem[2] ;
  int nFace[2] ;

  double fcNrm[2][3] ;

  int flag[2] ;
  int feature ;
}  edgeFc_s ;

/***************************************************************************

  PRIVATE

**************************************************************************/

/******************************************************************************
  mmgs_args:   */

/*! Filter arguments from the commandline for mmgs
 *
 */

/*

  Last update:
  ------------
  22Nov16: add save msh option
  30Jun16: derived from mmg_args


  Input:
  ------
  argLine: command line string.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmgs_args ( char argLine[],
                double *phGrad, double *pIsoFactor,
                double *phMin, double *phMax,
                double *pHausdorff,
                double *pscProdMin,
                int *savemsh,
                const uns_s *pUns ) {


  /* Add a blank keyword, required to preced args. */
  char* blk = "blank ", argLine2[LINE_LEN] ;
  strcpy ( argLine2, blk ) ;
  strcat ( argLine2, argLine ) ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine2, &ppArgs ) ;

  /* Default values. */
  *pIsoFactor = 1.e2 ; // Note: larger default value compared to refinement.
  *phGrad = -1 ; // default: don't worry.
  *pscProdMin = 0.8 ;
  *savemsh = 0 ;
  *pHausdorff = 0.001 ; // Note: smaller default value compared to refinement.


  /* Parse line of unix-style optional args. */
  char c ;
  /* colon behind arg means argument to be read,
     double colon means optional argument, but needs to be joint: -a0, not -a 0. */
  int set_iso=0, set_hMin=0, set_hMax=0 ;
  double isoFactor, hMin, hMax, hGrad, hausdorff, scProdMin ;
  while ((c = getopt_long ( mArgs, ppArgs, "a:f:g:h:l:u:s",NULL,NULL)) != -1) {
    switch (c)  {
    case 'a': // angle as in scalar product of face normals
      scProdMin = atof( optarg ) ;
      if ( scProdMin < -1. || scProdMin > 1. ) {
        sprintf ( hip_msg, "requesting scProdMin of face angle"
                  " to be %g < 1 or <1, ignored.", scProdMin ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else {
        *pscProdMin = scProdMin ;
      }
      break;
    case 'f':
      isoFactor = atof( optarg ) ;
      if ( isoFactor < 0. ) {
        sprintf ( hip_msg, "requesting fixed factor decimation"
                  " with factor %g < 0, ignored.", isoFactor ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else {
        *pIsoFactor = isoFactor ;
        set_iso = 1 ;
      }
      break;
    case 'g':
      hGrad = atof( optarg ) ;
      if ( hGrad !=-1.0 && hGrad < 0. ) {
        sprintf ( hip_msg, "requesting spacing gradient hGrad"
                  " with factor %g < 0, ignored.", hGrad ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else
        *phGrad = hGrad ;
      break;
    case 'h':
      hausdorff = atof( optarg ) ;
      if ( hausdorff < 0. ) {
        sprintf ( hip_msg, "requesting negative Hausdorff distance"
                  " with value %g < 0, ignored.", hausdorff ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else
        *pHausdorff = hausdorff ;
      break;
    case 'l':
      hMin = atof( optarg ) ;
      if ( hMin < 0. ) {
        sprintf ( hip_msg, "requesting hMin"
                  " to be %g < 0, ignored.", hMin ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else {
        *phMin = hMin ;
        set_hMin = 1 ;
      }
      break;
    case 'u':
      hMax = atof( optarg ) ;
      if ( hMax < 0. ) {
        sprintf ( hip_msg, "requesting hMax"
                  " to be %g < 0, ignored.", hMax ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else {
        *phMax = hMax ;
        set_hMax = 1 ;
      }
      break;
    case 's':
      *savemsh = 1;
      break;

    case '?':
      if ( optopt == 'l' )
        fprintf (stderr, "Option -%c requires an argument.\n", optopt );
      else if (isprint (optopt)) {
        sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
      else {
        sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }

  if ( set_iso || ( !set_hMax && !set_hMin ) ) {
    // Either iso is set, or nothing is set, in which case use default iso.
    if ( !set_hMin )
      *phMin = pUns->hMin*(*pIsoFactor) ;
    if ( !set_hMax )
      *phMax = pUns->hMax*(*pIsoFactor) ;
  }
  else if ( set_hMax ) {
    *pIsoFactor = pUns->hMax*(*phMax) ;
    if ( !set_hMin )
      *phMin = pUns->hMin*(*pIsoFactor) ;
  }
  else if ( set_hMin ) {
    *pIsoFactor = pUns->hMin*(*phMin) ;
    if ( !set_hMax )
      *phMax = pUns->hMax*(*pIsoFactor) ;
  }


  return ( 1 ) ;
}



/******************************************************************************
  mmgs_put_mesh:   */

/*! Copy a surface mesh to mmg.
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  19Sep21; call mmgs_set_meshsize, rather than mmg2d_set_meshsize
  : conceived.


  Input:
  ------
  isofactor: isotropic coarsening factor (multiplies h, so > 1 is coarser)
  hMin: min mesh spacing
  hMax: max mesh spacing
  hGrad: max mesh gradation
  hausdorff: hausdorff distance measuring fidelity of surface. Larger = less accurate.
  pUns
  mVxBnd: number of boundary vertices.

  Changes To:
  -----------
  *ppMMesh: pointer to surface mesh
  *ppMMet: pointer to metric

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmgs_put_mesh_surf (MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet,
                        const double isofactor,
                        const double hMin, const double hMax,
                        const double hGrad, const double hausdorff,
                        uns_s *pUns, ulong_t mBndVx, ulong_t mMmgFc ) {


  *ppMMesh = NULL ;
  *ppMMet = NULL ;

  MMGS_Init_mesh( MMG5_ARG_start,
                   MMG5_ARG_ppMesh, ppMMesh, MMG5_ARG_ppMet, ppMMet,
                   MMG5_ARG_end );

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "Initialisation of MMGS");
    hip_err ( info, 1, hip_msg ) ;
  }


  // remesher options
  MMGS_Set_iparameter( *ppMMesh, *ppMMet, MMGS_IPARAM_verbose, 5);
  MMGS_Set_iparameter( *ppMMesh, *ppMMet, MMGS_IPARAM_noinsert, 0);
  MMGS_Set_iparameter( *ppMMesh, *ppMMet, MMGS_IPARAM_noswap, 0);
  MMGS_Set_iparameter( *ppMMesh, *ppMMet, MMGS_IPARAM_nomove, 0);
  MMGS_Set_dparameter( *ppMMesh, *ppMMet, MMGS_DPARAM_hmin, hMin);
  MMGS_Set_dparameter( *ppMMesh, *ppMMet, MMGS_DPARAM_hmax, hMax);    // augmenter la valeur pour décimer
  MMGS_Set_dparameter( *ppMMesh, *ppMMet, MMGS_DPARAM_hausd, hausdorff ) ;   // augmenter la valeur pour décimer
  MMGS_Set_dparameter( *ppMMesh, *ppMMet, MMGS_DPARAM_hgrad, hGrad );



  /* Set mmg mesh size. */
  int mBi = 0 ; // No edges here.
  MMGS_Set_meshSize( *ppMMesh, mBndVx, mMmgFc, mBi ) ;

  /* Version for develop branch
  MMG2D_Set_meshSize( *ppMMesh, mBndVx, mMmgFc, 0, mBi ) ;
  */


  /* Fill vertices. */
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  ulong_t mVxWritten = 0 ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number && pVx->mark2 ) {
        mVxWritten++ ;
        MMGS_Set_vertex( *ppMMesh,
                         pVx->Pcoor[0], pVx->Pcoor[1], pVx->Pcoor[2], 0,
                         mVxWritten ) ;
      }
  if ( mVxWritten != mBndVx ) {
    sprintf ( hip_msg,
              "mismatch in bnd vx in mmgs_put_mesh:"
              " expected %"FMT_ULG", found %"FMT_ULG".",
              pUns->mVertAllBc, mVxWritten ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Fill tris. */
  elem_struct *pEl ;
  ulong_t mTrWritten = 0 ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  int nBc ;
  int mTriWritten = 0 ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] ) {
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                                 &pBndFcBeg, &pBndFcEnd ) ) {
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pEl = pBndFc->Pelem ;
          if ( pEl->number ) {
              pFoE = elemType[pEl->elType].faceOfElem + pBndFc->nFace ;
              kVxFc = pFoE->kVxFace ;
            if ( pFoE->mVertsFace == 3 ) {
              pFoE = elemType[pEl->elType].faceOfElem + pBndFc->nFace ;
              kVxFc = pFoE->kVxFace ;
              mTriWritten++ ;
              MMGS_Set_triangle ( *ppMMesh,
                                  pEl->PPvrtx[kVxFc[0]]->number,
                                  pEl->PPvrtx[kVxFc[1]]->number,
                                  pEl->PPvrtx[kVxFc[2]]->number,
                                  nBc+1, mTriWritten);
            }
            else if ( pFoE->mVertsFace == 4 ) {
              pFoE = elemType[pEl->elType].faceOfElem + pBndFc->nFace ;
              kVxFc = pFoE->kVxFace ;
              /* Add two faces for each quad. */
              mTriWritten++ ;
              MMGS_Set_triangle ( *ppMMesh,
                                  pEl->PPvrtx[kVxFc[0]]->number,
                                  pEl->PPvrtx[kVxFc[1]]->number,
                                  pEl->PPvrtx[kVxFc[2]]->number,
                                  nBc+1, mTriWritten);
              mTriWritten++ ;
              MMGS_Set_triangle ( *ppMMesh,
                                  pEl->PPvrtx[kVxFc[0]]->number,
                                  pEl->PPvrtx[kVxFc[2]]->number,
                                  pEl->PPvrtx[kVxFc[3]]->number,
                                  nBc+1, mTriWritten);
            }
          }
        }
      }
    }
  if ( mTriWritten != mMmgFc ) {
    sprintf ( hip_msg,
              "mismatch in bnd faces in mmgs_put_mesh: expected %"FMT_ULG", found %d",
              mMmgFc, mTriWritten ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  /* If you need to fill edges :   MMGS_Set_edge ( *ppMMesh, edge_vert1, edge_vert2, boundary_condition, edge_number);*/


  /* Check the mesh validity */
  if ( !MMGS_Chk_meshData( *ppMMesh, *ppMMet ) )
    hip_err ( fatal, 0, "failed after MMGS_Chk_meshData in mmgs_put_mesh" ) ;


  return ( 0 ) ;
}

/******************************************************************************
  mmgs_get_sizes:   */

/*! Obtain sizes of an mmgs mesh.
 *
 */

/*

  Last update:
  ------------
  30jun16; derived from mmg_get_sizes.


  Input:
  ------
  mesh: mmgs 3d surface mesh

  Changes To:
  -----------

  Output:
  -------
  *pmDim: spatial dimension
  *pmEl: number of Elements
  *pmConn: number of connectivity entries
  *pmVx: number of vertices.
  *pmBc: number of bcs.

*/

void mmgs_get_sizes ( MMG5_pMesh mesh,
                 int *pmDim, ulong_t *pmEl, ulong_t *pmConn,
                 ulong_t *pmVx, ulong_t *pmBndFc, int *pmBc ) {

  ulong_t uBuf[6], i ;
  elType_e elType ;


  // Only 3D here.
  *pmDim = 3 ;


  // Coordinates
  *pmVx =  mesh->np ;

  // Connectivity, tri faces only, add virtual, flat prism elements
  // in the volume.
  *pmEl = mesh->nt ;
  *pmConn = 6*(*pmEl) ;


  /* Number of bnd faces. */
  *pmBndFc = mesh->nt ;

  /* Number of Boundaries. Take the highest index as number of bcs*/
  MMG5_pTria triMmg;
  int k;
  *pmBc = 0 ;
  for ( k = 1 ; k <= mesh->nt ; k++ ) {
    triMmg = &mesh->tria[k];
    *pmBc = MAX( *pmBc, triMmg->ref ) ;
  }

  return ;
}

/******************************************************************************
  mmgs_get_coor:   */

/*! extract coordinates from a mmgs 3d surface mesh.
 */

/*

  Last update:
  ------------
  19Apri16; Added 2D options
  5Mar16; tidied up.
  Dec15: conceived by GS.


  Input:
  ------
  mesh: mmgs 3d surface mesh

  Changes To:
  -----------
  pChunk:

  Output:
  -------

  Returns:
  --------
  number of coordinate vectors read.

*/

int mmgs_get_coor ( MMG5_pMesh mesh, chunk_struct *pChunk ) {

  ulong_t k ;
  MMG5_pPoint	Vertex;
  vrtx_struct *pVx ;

  /* Coordinates: mDim, mVx. */
  for ( k = 1 ; k <= mesh->np ; k++ ) {
    Vertex = &mesh->point[k];
    pVx = pChunk->Pvrtx + k ;
    pVx->number = k ;
    pVx->Pcoor[0] = Vertex->c[0];
    pVx->Pcoor[1] = Vertex->c[1];
    // mmgs is always in 3D.
    pVx->Pcoor[2] = Vertex->c[2];
  }

  return ( mesh->np ) ;
}


/******************************************************************************
  mmgs_get_conn:   */

/*! extract face and element connectivity from a mmgs 3d surface mesh
 *  mmgs lists only boundary faces, create a flat prism as
 *  a 'virtual' element to express the face in the datastructure.
 *
 */

/*

  Last update:
  ------------
  1Jul16; derived from mmg_get_bnd

  Input:
  ------
  mesh: mmgs 3d surface mesh

  Changes To:
  -----------
  pChunk:

  Returns:
  --------
  number of connectivity entries transferred.

*/

ulong_t mmgs_get_conn ( MMG5_pMesh mesh, chunk_struct *pChunk,
                        const int mBc, bc_struct **ppBc ) {

  int i ;
  int status ;
  int mFcRead = 0 ;
  bndFc_struct *pBf = pChunk->PbndFc+1 ;
  elem_struct *pElem = pChunk->Pelem ;
  vrtx_struct **ppVrtx = pChunk->PPvrtx ;
  vrtx_struct *pVrtx = pChunk->Pvrtx ;

  MMG5_pTria triMmg ;
  int nTet;

  /* Read all faces. A bc no of mBc+1 means an internal face created
     at submission to mmgs to fix faces with non-simplex elements.
     make_uns_bndPatch will order faces and create the list of bc
     in the new grid. */
  for( i=1 ; i <= mesh->nt ; i++ ) {
    triMmg = mesh->tria + i ;

    mFcRead++ ;

    //* Create a prism. *//
    pElem++ ;
    pElem->elType = pri ;
    pElem->number = i ;
    pElem->PPvrtx = ppVrtx ;


    // List forming nodes:
    // pri
    // 146 253
    // 035 142
    // tri
    // 012 012
    *ppVrtx++ = pVrtx+triMmg->v[0] ;
    *ppVrtx++ = pVrtx+triMmg->v[0] ;
    *ppVrtx++ = pVrtx+triMmg->v[1] ;
    *ppVrtx++ = pVrtx+triMmg->v[1] ;
    *ppVrtx++ = pVrtx+triMmg->v[2] ;
    *ppVrtx++ = pVrtx+triMmg->v[2] ;


    /* Create a boundary face. */
    pBf->Pelem = pElem ; // watch out, nTet is only int, not ulong!
    pBf->nFace = 4 ;
    pBf->Pbc   = ppBc[triMmg->ref-1] ; // faces were submited with nBc+1.
    pBf++ ;
  }

  return ( mesh->nt ) ;
}



/******************************************************************************
  mmgs_2hip:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  4Mar16; tidy up.
  Dec15: conceived. GS


  Input:
  ------
  mesh: mmgs 3d surface mesh.
  pUns: original mesh with bcs

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

uns_s * mmgs_2hip( MMG5_pMesh mesh, uns_s *pUns ) {


  int mDim ;
  ulong_t mEl, mConn, mVx, mBndFc ;
  int mBc ;

  /* Get sizes. */
  mmgs_get_sizes ( mesh, &mDim, &mEl, &mConn, &mVx, &mBndFc, &mBc ) ;

  if ( verbosity > 1 ) {
    sprintf( hip_msg, "MMGS-decimated grid has %"FMT_ULG" elements,"
             " %"FMT_ULG" nodes, %"FMT_ULG" bnd faces.",
             mEl, mVx, mBndFc ) ;
    hip_err ( info, 1, hip_msg ) ;
  }

  uns_s *pUnsMmgs = NULL ;
  if ( !make_uns_grid ( &pUnsMmgs, mDim, mEl, mConn, 0, mVx, 0, mBndFc, mBc ) ) {
    sprintf ( hip_msg, "failed to alloc for grid in mmgs_2hip.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  pUnsMmgs->specialTopo = surf ;


  /* Read. */
  chunk_struct *pChunk = pUnsMmgs->pRootChunk ;
  mmgs_get_coor ( mesh, pChunk ) ;
  mmgs_get_conn ( mesh, pChunk, pUns->mBc, pUns->ppBc ) ;

  make_uns_bndPatch ( pUnsMmgs ) ;

  /* Validate the grid. Temporarlily disable the conn check as the prisms
     are flat.  */
  check_uns ( pUnsMmgs, 3 ) ;


  return ( pUnsMmgs ) ;
}



/******************************************************************************
  mmgs_free_all:   */

/*! liberate mmg memory.
 */

/*

  Last update:
  ------------
  30Jun16: derived from mmg_free_all

  Changes To:
  -----------
  ppMMesh: pointer to pointer to mesh
  ppMMet: pointer to pointer to metric

*/

void mmgs_free_all (  MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet ) {

  /* Free mmgs memory */
  MMGS_Free_all( MMG5_ARG_start,
                 MMG5_ARG_ppMesh, ppMMesh,
                 MMG5_ARG_ppMet, ppMMet,
                 MMG5_ARG_end);


  return ;
}

/******************************************************************************
  perim_feat_edges_surf:   */

/*! Extract a list of perimeter and feature edges from a mesh surface
 */

/*

  Last update:
  ------------
  1Jul16: conceived.


  Input:
  ------
  pUns
  scProd = scalar product of max angular deviation of surface normals
           either side of an edge to make it a feature edge.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int bnd_feat_edges_surface ( uns_s *pUns, const double scProdMin ) {
  /* Make a list of edges on the boundaries. */
  edgeFc_s *pEgFc ;
  ulong_t mEdges ;
  llEdge_s *pllEdge = make_llEdge_bnd ( pUns, &mEdges, sizeof( edgeFc_s ),
                              (void**) &(pEgFc) ) ;



  /* Reset. */
  ulong_t nEdge ;
  for ( nEdge = 1 ; nEdge <= mEdges ; nEdge++ ) {
    pEgFc[nEdge].pVrtx[0] = pEgFc[nEdge].pVrtx[1] = NULL ;
    pEgFc[nEdge].pElem[0] = pEgFc[nEdge].pElem[1] = NULL ;
    pEgFc[nEdge].flag[0] = pEgFc[nEdge].flag[1] = pEgFc[nEdge].feature = 0 ;
  }


  /* Tag all edges with the bc's and normals either side. */
  double fcNrm[3] ; // This is only 3-D.
  int nBc ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  int mTimesNrm ;
  int iEg ;
  const int *kVxEdge ;
  vrtx_struct *pVx[2] ;
  int sw, egDir ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        if ( pBndFc->Pelem && pBndFc->Pelem->number ) {
          pElem = pBndFc->Pelem ;
          pElT = elemType + pElem->elType ;
          pFoE = pElT->faceOfElem + pBndFc->nFace ;

          // calc face normal
          uns_face_normal ( pElem, pBndFc->nFace, fcNrm, &mTimesNrm ) ;
          vec_norm_dbl ( fcNrm, 3 ) ;

          for ( iEg = 0 ; iEg < pFoE->mFcEdges ; iEg++ ) {
            kVxEdge = pElT->edgeOfElem[ pFoE->kFcEdge[iEg] ].kVxEdge ;
            pVx[0] = pElem->PPvrtx[ kVxEdge[0] ] ;
            pVx[1] = pElem->PPvrtx[ kVxEdge[1] ] ;
            egDir = pFoE->edgeDir[iEg] ;
            if ( pVx[0] != pVx[1] ) {
              /* A non-collapsed edge. Find it. */
              if ( !( nEdge = get_edge_vrtx ( pllEdge,
                                              (const vrtx_struct **) pVx+0,
                                              (const vrtx_struct **) pVx+1, &sw ) ) ) {
                sprintf ( hip_msg, "could not find edge %d in element"
                          " %"FMT_ULG", %"FMT_ULG"-%"FMT_ULG","
                          " in bnd_feat_edges_surface.\n", iEg, pElem->number,
                          pVx[0]->number, pVx[1]->number ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
              else {
                pEgFc[nEdge].pVrtx[0] = pVx[0] ;
                pEgFc[nEdge].pVrtx[1] = pVx[1] ;

                if ( ( sw || egDir ) && !(sw && egDir) ) { // what was xor again?
                  if (  pEgFc[nEdge].pElem[1] ) {
                    hip_err ( warning, 3, "duplicate element for edge right side [1]." ) ;
                    if ( verbosity > 4 ) {
                      printf ( " trying to add this face\n" ) ;
                      printfcco( pElem, pBndFc->nFace ) ;
                      printf ( "    to those two\n" ) ;
                      printfc(pEgFc[nEdge].pElem[0], pEgFc[nEdge].nFace[0]) ;
                      printfc(pEgFc[nEdge].pElem[1], pEgFc[nEdge].nFace[1]) ;
                    }
                  }
                  else{
                    pEgFc[nEdge].pElem[1] = pElem ;
                    pEgFc[nEdge].nFace[1] = pBndFc->nFace ;
                    memcpy ( pEgFc[nEdge].fcNrm[1], fcNrm, 3*sizeof(double) ) ;
                    pEgFc[nEdge].flag[1] = nBc ;
                  }
                }
                else {
                  if (  pEgFc[nEdge].pElem[0] ) {
                    hip_err ( warning, 3, "duplicate element for edge left side [0]." ) ;
                    if ( verbosity > 4 ) {
                      printf ( " trying to add this face\n" ) ;
                      printfcco( pElem, pBndFc->nFace ) ;
                      printf ( "    to those two\n" ) ;
                      printfc(pEgFc[nEdge].pElem[0], pEgFc[nEdge].nFace[0]) ;
                      printfc(pEgFc[nEdge].pElem[1], pEgFc[nEdge].nFace[1]) ;
                    }
                  }
                  else {
                    pEgFc[nEdge].pElem[0] = pElem ;
                    pEgFc[nEdge].nFace[0] = pBndFc->nFace ;
                    memcpy ( pEgFc[nEdge].fcNrm[0], fcNrm, 3*sizeof(double) ) ;
                    pEgFc[nEdge].flag[0] = nBc ;
                  }
                }
              }
            }
          }
        }
  }




  /* Loop over all boundary edges. */
  double scProd ;
  ulong_t mEgPerimFeat ;
  egVx_s *pBndEg ;
  bndPatch_struct *pBP ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {

    /* First pass: count. */
    for ( nEdge = 1, mEgPerimFeat=0 ; nEdge <= mEdges ; nEdge++ )
      if ( pEgFc[nEdge].flag[0] == nBc || pEgFc[nEdge].flag[1] == nBc )  {
        /* This edge is in or attached to nBc, consider it. */

        //check normals
        scProd = scal_prod_dbl ( pEgFc[nEdge].fcNrm[0],
                                 pEgFc[nEdge].fcNrm[1], 3 ) ;
        if ( scProd < scProdMin )
          // if diff by more than scProd, then flag as feature
          pEgFc[nEdge].feature = 1 ;

        if ( pEgFc[nEdge].flag[0] - pEgFc[nEdge].flag[1]
             // || pEgFc[nEdge].feature == 1
             ) {
          // if bcs differ or feature, then list
          mEgPerimFeat++ ;
        }
      }

    // store with the patch.
    pBP = pUns->ppRootPatchBc[nBc] ;
    pBP->mBndEg = mEgPerimFeat ;
    pBP->pBndEg = pBndEg =
      arr_malloc( "pBndEg in bnd_feat_edges_surface", pUns->pFam,
                  mEgPerimFeat, sizeof( *(pBP->pBndEg) ) ) ;

    /* List. */
    for ( nEdge = 1 ; nEdge <= mEdges ; nEdge++ )
      if ( pEgFc[nEdge].flag[0] == nBc || pEgFc[nEdge].flag[1] == nBc )  {
        if ( pEgFc[nEdge].flag[0] - pEgFc[nEdge].flag[1]
             // || pEgFc[nEdge].feature == 1
             ) {
          // bcs differ or feature, list
          show_edge ( pllEdge, nEdge,
                      ( vrtx_struct ** ) pBndEg->pVx+0,
                      ( vrtx_struct ** ) pBndEg->pVx+1 ) ;
          pBndEg++ ;
        }
      }

    if ( pBndEg - pBP->pBndEg != mEgPerimFeat ) {
      sprintf ( hip_msg, "Mismatch in bnd edges in bnd_feat_edges_surface. "
                "Expected %"FMT_ULG", found %zu edges.",
                mEgPerimFeat, (size_t) (pBndEg - pBP->pBndEg) ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }


  /* Free the auxiliary storage. */
  free_llEdge ( &pllEdge ) ;
  arr_free ( pEgFc ) ;

  return ( 0 ) ;
}

/******************************************************************************
Copyright Jens-Dominik Mueller and CERFACS, see the CECILL copyright notice at
the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>
*/
/*
  mmgs_calc_eglen_surf:   */

/*! Compute average mesh spacing on a 3D surface mesh and scale with given factor.
 *  Requires bnd vx to be numbered only/first.
 */

/*

  Last update:
  ------------
  19Dec16: conceived.


  Input:
  ------
  pUns
  mBndVx = number of boundary vertices, which must be numbered first.
  isoFactor = scale the existing spacing with this factor.

  Changes To:
  -----------
  pMMet = the mmgs metric.

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmgs_calc_eglen_surf ( uns_s *pUns, ulong_t mBndVx, double isoFactor, MMG5_pSol pMMet ) {

  /* Alloc a scalar spacing for all bnd vx. */
  double *phVx = arr_calloc ( "phVx in mmgs_calc_eglen_surf", pUns->pFam, mBndVx+1, sizeof (*phVx ) ) ;
  double *pmEgVx = arr_calloc ( "pmEgVx in mmgs_calc_eglen_surf", pUns->pFam, mBndVx+1, sizeof (*pmEgVx ) ) ;


  /* Loop over all bnd faces, compute spacings, scatter. */
  chunk_struct *pChunk = NULL ;
  bndPatch_struct *pBndPatch = NULL ;
  bndFc_struct *pBndFcBeg, *pBndFcEnd, *pBndFc ;
  elem_struct *pElem ;
  const faceOfElem_struct *pFoE ;
  ulong_t mVxFc ;
  const int *kVxFc ;
  int kVxFcExt[MAX_VX_FACE+1] ;
  vrtx_struct **ppVx ;
  int k ;
  double vecEg[MAX_DIM], hEg ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
      pElem = pBndFc->Pelem ;
      if ( pElem && pElem->number && pBndFc->nFace ) {
        pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
        mVxFc = pFoE->mVertsFace ;
        kVxFc = pFoE->kVxFace ;
        ppVx = pElem->PPvrtx ;

        /* Extend the list of vx to avoid using MOD. */
        memcpy ( kVxFcExt, kVxFc, mVxFc*sizeof(*kVxFc) ) ;
        kVxFcExt[mVxFc] = kVxFc[0] ;

        for ( k = 0 ; k < mVxFc ; k++ ) {
          vec_diff_dbl ( ppVx[ kVxFcExt[k+1] ]->Pcoor, ppVx[ kVxFcExt[k] ]->Pcoor, 3, vecEg ) ;
          hEg = vec_len_dbl ( vecEg, 3 ) ;
          phVx[ ppVx[ kVxFcExt[k+1] ]->number ] += hEg ;
          phVx[ ppVx[ kVxFcExt[k]   ]->number ] += hEg ;
          pmEgVx[ ppVx[ kVxFcExt[k+1] ]->number ]++ ;
          pmEgVx[ ppVx[ kVxFcExt[k]   ]->number ]++ ;
        }
      }
    }

  /* Normalise with number of contributions. */
  int n ;
  for ( n = 1 ; n <= mBndVx ; n++ ) {
    if ( pmEgVx[n] )
      phVx[n] *= isoFactor/pmEgVx[n] ;
    else {
      sprintf ( hip_msg, "zero edges incident on boundary node %d\n", n ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }



  /* Copy to mmgs. */
  for ( n = 1 ; n <= mBndVx ; n++ ) {
    MMGS_Set_scalarSol( pMMet, phVx[n], n) ;
  }

  arr_free ( phVx ) ;
  arr_free ( pmEgVx ) ;

  return ( 0 ) ;
}


/***************************************************************************

  PUBLIC

**************************************************************************/

/******************************************************************************
  decimate_mmg:   */

/*! Decimate a surface mesh using mmgs.
 *
 */

/*

  Last update:
  ------------
  12Oct20; remove periodicity, issue warning.
  22Nov16: add save msh option
  29Jun16: conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int decimate_mmgs_3d ( uns_s *pUns, char* argLine ) {

  /* Needs to be 3d. */
  if ( pUns->mDim != 3 )
    hip_err ( fatal, 0, "decimate_mmg works only on 3D meshes." ) ;


  double isoFactor, hGrad, hMin, hMax, hausdorff, scProdMin ;
  int savemsh ;
  if ( !mmgs_args ( argLine, &hGrad, &isoFactor, &hMin, &hMax, &hausdorff, &scProdMin,
                    &savemsh, pUns ) )
    return (1) ;


  /* unset periodicity. */
  if ( pUns->pPerBc ) {
    hip_err ( warning, 2, "decimate cannot handle periodicity, surface grids.\n"
              "            are generated and written without periodicity.\n" ) ;
    pUns->mPerBcPairs = 0 ;
    arr_free ( pUns->pPerBc ) ;
    pUns->pPerBc = NULL ;
  }


  /* Count boundary faces. */
  count_uns_bndFaces ( pUns ) ;
  ulong_t mMmgFc = pUns->mTriAllBc + 2*pUns->mQuadAllBc ;

  /* Number vertices on the surface first, and only once, then number internal vx,
     so we can use a simple array to compute mesh spacing. */
  ulong_t mBndVx = increment_vx_number_bc ( pUns ) ;



  /* Extract surface mesh and pass to mmg. */
  MMG5_pMesh pMMesh ;
  MMG5_pSol pMMet ;
  mmgs_put_mesh_surf ( &pMMesh, &pMMet,
                       isoFactor, hMin, hMax, hGrad, hausdorff,
                       pUns, mBndVx, mMmgFc ) ;



  // Initialise metric.
  if ( !MMGS_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after MGS_Set_solSize in adapt_mmg" ) ;
  if ( !MMGS_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after MMGS_Chk_meshData in adapt_mmg" ) ;

  /* Compute spacing of the initial mesh, scale with isoFactor and supply as metric to mmgs. */
  mmgs_calc_eglen_surf ( pUns, mBndVx, isoFactor, pMMet ) ;


  // if ( mmgMethod == isoFac ) {
  //   // constant factor
  //   mmg_metric_from_const ( pMMesh, pMMet, isoFactor ) ;
  // }




  /* save original in msh format */
  if ( savemsh == 1 ) {
     MMGS_saveMesh(pMMesh, "initMesh.mesh") ;
  }

  /* Decimate. */
  MMGS_mmgslib ( pMMesh, pMMet ) ;

  /* save mmgs output in msh format */
  if ( savemsh == 1 ) {
     MMGS_saveMesh(pMMesh, "decimatedMesh.mesh") ;
  }

  /*  Convert to hip structures */
  uns_s *pUnsMmg = mmgs_2hip( pMMesh, pUns );

  /* Copy from mmg to pUns. */
  mmgs_free_all ( &pMMesh, &pMMet ) ;

  /* GS Decimate is broken ? disable for now */
  bnd_feat_edges_surface ( pUnsMmg, scProdMin ) ;

  return ( 0 ) ;
}
