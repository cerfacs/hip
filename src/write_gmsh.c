/*
 write_gms.c:
 Write mesh connectivity, vertex coordinates, boundary information
 and solution to a gmsh .msh file.

  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.

 contains:

 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern const char varTypeNames[][LEN_VAR_C] ;
extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;


extern const int g2h[MAX_ELEM_TYPES][MAX_VX_ELEM] ;

/******************************************************************************

  gmsh_iElT:
  Translate hip's elemType to the numerical iElT for a gmsh file.
  
  Last update:
  ------------
  12Feb10: copied from gmsh_elT.
  
  Input:
  ------
  pElT: pointer to the corresponding elemType

  
  Returns:
  --------
  iElT: gmsh type integer
  
*/

int gmsh_iElT ( const elType_e elType ) {

  int iElT ;

  switch ( elType ) {
  case ( tri ) : iElT = 2 ; break ; /* tri. */ 
  case ( qua ) : iElT = 3 ; break ; /* qua. */ 
  case ( tet ) : iElT = 4 ; break ; /* tet. */ 
  case ( hex ) : iElT = 5 ; break ; /* hex. */ 
  case ( pri ) : iElT = 6 ; break ; /* pri. */ 
  case ( pyr ) : iElT = 7 ; break ; /* pyr. */ 
  default :      iElT = 0 ;
  }

  return ( iElT) ;
}



/******************************************************************************

  gmsh_write_header:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_header ( FILE *Fmsh ) {

  fprintf ( Fmsh, "%s", "$MeshFormat\n" ) ;
  fprintf ( Fmsh, "2 0 %d\n", (int) sizeof(double) ) ;
  fprintf ( Fmsh, "%s", "$EndMeshFormat\n" ) ;

  return (1) ;
}

/******************************************************************************

  gmsh_write_nodes:
  .
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  : conceived.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_nodes ( FILE *Fmsh, uns_s *pUns ) {

  chunk_struct *pChunk ;
  vrtx_struct *pVxFirst, *pVxLast, *pVx ;
  int nVxFirst, nVxLast ;
  const int mDim  = pUns->mDim ;

  fprintf ( Fmsh, "$Nodes\n" ) ;
  fprintf ( Fmsh, "%"FMT_ULG"\n", pUns->mVertsNumbered ) ;

  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxFirst, &nVxFirst, &pVxLast, &nVxLast ) ) 
    for ( pVx = pVxFirst ; pVx <= pVxLast ; pVx++ ) 
      if ( pVx->number ) {
        fprintf ( Fmsh, "%8"FMT_ULG" %23.15e %23.15e", pVx->number, pVx->Pcoor[0], pVx->Pcoor[1] ) ;
        if ( mDim == 2 )
          fprintf ( Fmsh, "  0.0 \n" ) ;
        else
          fprintf ( Fmsh, " %23.15e\n", pVx->Pcoor[2] ) ;
      }


  fprintf ( Fmsh, "$EndNodes\n" ) ;

  return ( 1 ) ;
}


/******************************************************************************

  gmsh_write_nodes:
  .
  
  Last update:
  ------------
  2Feb18; fix bug with incrementing pVxCWt for mVx < k < MAX_VX_ELEM
  20Dec17; fix issues with ULG format mismatch. Promote large ints to ulong_t.
  1Oct17; write out min-norm coeffs.
  17Jul17; switch pElCollapseTo to ppElContain
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  15Mar11: derived from gmsh_write_nodes.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_mg_conn ( FILE *Fmsh, uns_s *pUns ) {

  chunk_struct *pChunk ;
  vrtx_struct *pVxFirst, *pVxLast, *pVx ;
  ulong_t nVx ;
  int nVxFirst, nVxLast ;
  elem_struct **ppElC = pUns->ppElContain, *pElC ;
  ulong_t nVxC ;
  ulong_t mVx ;
  double *pVxCWt = pUns->pElContainVxWt ;
  int k ;

  fprintf ( Fmsh, "$MGconn " ) ;

  if ( pUns->pElContainVxWt ) {
    /* Inter-grid transfer weigths have been computed. Write to file. */
    
    fprintf ( Fmsh, " %% node number, nearest coarse grid node,"
              " containing coarse grid element,"
              " 8*forming nodes, 8 weights."
              ) ;
    fprintf ( Fmsh, "\n" ) ;
    fprintf ( Fmsh, "%"FMT_ULG"\n", pUns->mVertsNumbered ) ;

    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxFirst, &nVxFirst, &pVxLast, &nVxLast ) ) 
      for ( pVx = pVxFirst ; pVx <= pVxLast ; pVx++ ) 
        if ( (nVx = pVx->number) ) {

          /* Containing coarse grid elem. */
          ppElC++ ;
          pElC = *ppElC ;

          if ( pUns->pnVxCollapseTo )
            nVxC = pUns->pnVxCollapseTo[ nVx ] ;
          else
            /* e.g. a provided sequence, we only have the containing elem. */
            nVxC = pElC->PPvrtx[0]->number ;

          fprintf ( Fmsh, "%"FMT_ULG" %"FMT_ULG" %"FMT_ULG"", nVx,  
                    nVxC,
                    pElC->number ) ;


          mVx = elemType[pElC->elType].mVerts ;
          for ( k = 0 ; k < mVx ; k++ )
            fprintf ( Fmsh, " %"FMT_ULG, pElC->PPvrtx[k]->number ) ;
          for ( ; k < MAX_VX_ELEM ; k++ )
            fprintf ( Fmsh, " 0" ) ;
          
          for ( k = 0 ; k < mVx ; k++ )
            fprintf ( Fmsh, " %23.15e", *pVxCWt++ ) ;
          for ( ; k < MAX_VX_ELEM ; k++ ) {
            pVxCWt++ ;
            fprintf ( Fmsh, " 0.0" ) ;
          }

          fprintf ( Fmsh, "\n" ) ;

        }
  }
  
  else { // No weights computed.
    fprintf ( Fmsh, " %% node number, nearest coarse grid node,"
              " containing coarse grid element" ) ;
    fprintf ( Fmsh, "\n" ) ;
    fprintf ( Fmsh, "%"FMT_ULG"\n", pUns->mVertsNumbered ) ;

    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxFirst, &nVxFirst, &pVxLast, &nVxLast ) ) 
      for ( pVx = pVxFirst ; pVx <= pVxLast ; pVx++ ) 
        if ( (nVx = pVx->number) ) {
          fprintf ( Fmsh, "%"FMT_ULG" %"FMT_ULG" %"FMT_ULG"\n", nVx,  
                    pUns->pnVxCollapseTo[ nVx ],
                    pUns->ppElContain[ nVx ]->number ) ;
        }
  }

  fprintf ( Fmsh, "$EndMGconn\n" ) ;

  return ( 1 ) ;
}

/******************************************************************************

  gmsh_write_bFc:
  Write one boundary face, 2D or 3D to gmsh.
  
  Last update:
  ------------
  5Apr13; promote possibly large integers to ulong_t.
  12Nov10; change format to list parent element.
  12Feb10: conceived.
  
  Input:
  ------
  Fmsh: file to write to
  pmBF: counter how many bnd faces have been written.
  pBF: the bnd face to write.

  Changes To:
  -----------
  pmBF: incremented.

  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_bFc ( FILE *Fmsh, const ulong_t mEl, ulong_t *pmBF, 
                     bndFc_struct *pBF ) {

  const elem_struct *pElem = pBF->Pelem ;
  const ulong_t nElP = pElem->number ;
  const faceOfElem_struct *pFoE = elemType[ pElem->elType ].faceOfElem + pBF->nFace ;
  const int *kVxFc = pFoE->kVxFace ;
  int mVxFace = pFoE->mVertsFace ;
  vrtx_struct **ppVx = pElem->PPvrtx ;
  int k ;
  ulong_t nVx ;
  int bcNr = pBF->Pbc->nr ;

  (*pmBF)++ ;
  
  fprintf ( Fmsh, "%"FMT_ULG" %d", *pmBF+mEl, mVxFace-1 ) ; /* El nr., type */
  fprintf ( Fmsh, " 6 %d %d 0 0 %"FMT_ULG" %d", bcNr, bcNr, nElP, pBF->nFace ) ;  /* Tags. */ 

  for ( k = 0 ; k < mVxFace ; k++ ) {
    /* Copy this into an int, as number is not a full int. */
    nVx = ppVx[ kVxFc[k] ]->number ;
    fprintf ( Fmsh, " %"FMT_ULG"", nVx ) ;
  }
  fprintf ( Fmsh, "\n" ) ;

  return (1) ;
}

/******************************************************************************

  gmsh_write_1elem:
  .
  
  Last update:
  ------------
  12Nov10; change format for tags.
  : conceived.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_1elem ( FILE *Fmsh, const int mBF, int mBc, elem_struct *pElem ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  int mVerts = pElT->mVerts ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  const int iElT = gmsh_iElT ( pElem->elType ) ;
  const int nElP = pElem->number ; 
  const int nEl =  nElP + mBF ;
  int k, nVx[MAX_VX_ELEM] ;

  fprintf ( Fmsh, "%d %d", nEl, iElT ) ; /* El nr., type */
  fprintf ( Fmsh, " 3 %d %d 0", mBc+1, mBc+1 ) ;  /* Tags. */ 

  for ( k = 0 ; k < mVerts ; k++ ) {
    /* Copy this into an int, as number is not a full int. */
    nVx[ g2h[pElem->elType][k] ] = ppVx[k]->number ;
  }
  for ( k = 0 ; k < mVerts ; k++ ) {
    fprintf ( Fmsh, " %d", nVx[k] ) ;
  }
  fprintf ( Fmsh, "\n" ) ;
  

  return (1) ;
}

/******************************************************************************

  gmsh_write_elems:
  .
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  23Jun10; fix bug with bc no for internal elements.
  : conceived.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_elems ( FILE *Fmsh, uns_s *pUns ) {

  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcFirst, *pBndFcLast, *pBF ;
  elem_struct *pEl, *pElFirst, *pElLast ;
  ulong_t mBF = 0, mEl = 0 ;

  fprintf ( Fmsh, "$Elements\n" ) ;
  fprintf ( Fmsh, "%"FMT_ULG"\n", pUns->mFaceAllBc + pUns->mElemsNumbered ) ;
  

  /* List all elems. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElFirst, &pElLast ) )
    for ( pEl = pElFirst ; pEl <= pElLast ; pEl++ )
      if ( pEl->number ) {
        mEl++ ;
        gmsh_write_1elem ( Fmsh, mBF, pUns->mBc+1, pEl ) ;
      }

  /* List all bnd faces. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcFirst, &pBndFcLast ) )
    for ( pBF = pBndFcFirst ; pBF <= pBndFcLast ; pBF++ )
      if ( pBF->Pelem && pBF->Pelem->number )
        gmsh_write_bFc ( Fmsh, mEl, &mBF, pBF ) ;


  fprintf ( Fmsh, "$EndElements\n" ) ;

  return (1) ;
}
/******************************************************************************

  gmsh_sol:
  Write a solution component to gmsh.
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  24Feb10: conceived.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmsh_write_sol ( uns_s *pUns, const char *rootFlNm, 
                     const char *varName, int kVar, int mComp ) {

  char fileName[TEXT_LEN] ;
  FILE *Fvar ;
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx1, *pVx2, *pVx ;
  int n1, n2 ;
  int k=0, m=0 ;

  if ( mComp == 1 )
    /* Scalar. */
    m = 1 ;
  else if ( mComp == 3 )
    /* Vector, supplement zeros in 2D. */
    m = MIN( pUns->mDim, mComp ) ;
  else
    hip_err ( warning, 1, "gmsh_write_sol can only deal with scalars or 3-vectors.\n" ) ;


  sprintf ( fileName, "%s-%s.msh", rootFlNm, varName ) ;
  prepend_path ( fileName ) ;

  if ( ( Fvar = fopen( fileName, "w")) == NULL) {
     sprintf ( hip_msg, "failed to open solution file in gmsh_sol:"
               "         %s\n", fileName ) ;
     hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( verbosity > 2 ) { 
    sprintf ( hip_msg, "   Writing %s as gmsh to %s", varName, fileName ) ;
    hip_err ( blank, 1, hip_msg ) ;
  }

  gmsh_write_header ( Fvar ) ;


  fprintf ( Fvar, "$NodeData\n" ) ;

  /* String tags. */
  fprintf ( Fvar, "1\n%s\n", varName ) ;
  /* Real tags. */
  fprintf ( Fvar, "1\n0.0\n" ) ;
  /* Integer tags. */
  fprintf ( Fvar, "4\n0\n%d\n%"FMT_ULG"\n0\n", mComp, pUns->mVertsNumbered ) ;

  /* Loop over all valid nodes. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVx1, &n1, &pVx2, &n2 ) )
    for ( pVx = pVx1 ; pVx <= pVx2 ; pVx++ )
      if ( pVx->number ) {
        fprintf ( Fvar, "%8"FMT_ULG"", pVx->number ) ;
        for ( k = kVar ; k < kVar+m ; k++ )
          fprintf ( Fvar, " %17.9e", pVx->Punknown[k] ) ;
        /* Add zeros to promote 2D to 3D. */
        for ( k = m ; k < mComp ; k++ )
          fprintf ( Fvar, "   0.0" ) ;
        fprintf ( Fvar, "\n" ) ;
      }

  fprintf ( Fvar, "$EndNodeData\n" ) ;

  return (1) ;
}


/******************************************************************************

  write_gmsh_lvl:
  Write grid and solution to gmsh.
  
  Last update:
  ------------
  15Mar11; extracted from write_gmsh, add m g connectivity.
  
  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int write_gmsh_lvl ( char *fileName, const int nLvl, uns_s *pUns ) {

  FILE *Fmsh ;

  if ( ( Fmsh = fopen( fileName, "w")) == NULL) {
     sprintf ( hip_msg, "failed to open mesh file in write_gmsh:"
               "         %s\n", fileName ) ;
     hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( verbosity > 0 ) {
    sprintf ( hip_msg, "   Writing grid for level %d as gmsh to %s", nLvl, fileName ) ;
    hip_err ( blank, 1, hip_msg ) ;
  }

  gmsh_write_header ( Fmsh ) ;
  
  gmsh_write_nodes ( Fmsh, pUns ) ;

  gmsh_write_elems ( Fmsh, pUns ) ;

  if ( pUns->pnVxCollapseTo || pUns->ppElContain )
    /* There is multi-grid intra-grid connectivity. */
    gmsh_write_mg_conn ( Fmsh, pUns ) ;

  fclose ( Fmsh ) ;

  return ( 1 ) ;
}



/******************************************************************************

  write_gmsh:
  Write grid and solution to gmsh.
  
  Last update:
  ------------
  15Dec18; extracted from write_gmsh to allow writing of non-current pUns.
  
  Input:
  ------
  pUns: grid
  rootFlNm0: root of the filename
  doCheckBnd: if nonzero, perform bnd check for periodicity, etc.
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int write_gmsh_uns ( uns_s *pUns, const char *rootFlNm0, const int doCheckBnd ) {

  uns_s *pUnsLvl ;
  char rootFlNm[TEXT_LEN], fileName[TEXT_LEN] ;
  int mEqu, nLvl=0 ;

  strcpy ( rootFlNm, rootFlNm0 ) ;
  prepend_path ( rootFlNm ) ;


  if ( !pUns->validGrid ) {
    sprintf ( hip_msg, " you were told that this grid is invalid, weren't you?.\n" ) ;
    hip_err ( warning, 1, hip_msg ) ; }
  else if ( doCheckBnd && (check_bnd_setup ( pUns )).status != success ) {
    sprintf ( hip_msg, " writing grid without proper boundary setup.\n" ) ;
    hip_err ( warning, 1, hip_msg ) ; }


  if ( !pUns->pUnsCoarse ) {
    /* Only one finest level. Just write a .msh, no numbering. */
    snprintf ( fileName, LINE_LEN, "%s.msh", rootFlNm ) ;
    write_gmsh_lvl ( fileName, nLvl, pUns )  ;
    strcpy ( fileName, rootFlNm ) ;
  }
  else {
    /* Write the sequence backward, coarsest first, to ensure correct
       numbering for intergrid pointers. */
    for ( pUnsLvl = pUns ; pUnsLvl->pUnsCoarse ; pUnsLvl = pUnsLvl->pUnsCoarse )
      nLvl ++ ;

    for ( ; pUnsLvl ; pUnsLvl = pUnsLvl->pUnsFine, nLvl-- ) {
      /* Write a MG sequence. */
      sprintf ( hip_msg, "\nWriting level %d to gmsh:", nLvl ) ;
      hip_err ( blank, 1, hip_msg ) ;
      snprintf ( fileName, LINE_LEN, "%s.%d.msh", rootFlNm, nLvl ) ;
      prepend_path ( fileName ) ;
      write_gmsh_lvl ( fileName, nLvl, pUnsLvl )  ;


      /* Solution. */
      mEqu = pUnsLvl->varList.mUnknowns ;
      /* Solution, if any, but only write it for the finest grid. */
      if ( pUnsLvl->varList.varType != noVar && mEqu == pUnsLvl->mDim+2 ) {
        /* Write N-S equ to 3 files of primitive variables. */
        conv_uns_var ( pUnsLvl, prim ) ;
    
        gmsh_write_sol ( pUnsLvl, fileName, "density",  0,      1 ) ;
        gmsh_write_sol ( pUnsLvl, fileName, "velocity", 1,      3 ) ;
        gmsh_write_sol ( pUnsLvl, fileName, "pressure", mEqu-1, 1 ) ;
      }
      else if ( mEqu ) {
        sprintf ( hip_msg, "implement writing %d variables in %d-D in write_gmsh.\n"
                  "          writing grid without solution\n",
                  mEqu, pUnsLvl->mDim ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }

    }
  }

  return ( 1 ) ;
}




/******************************************************************************

  write_gmsh:
  Write grid and solution to gmsh.
  
  Last update:
  ------------
  16Dec18; body extracted into separate public write_gmsh_uns.
  2Mar18; ensure conversion to primitive vars when writing solution.
  2Feb18; write mg sequence backward from coarsest for consistent numbering.
  22Mar11; move write_sol back into gmsh after extracting out write_gmsh_lvl
  10Feb10: conceived.
  15Mar11; add mg connectivity.
  
  Input:
  ------
  rootFlNm0: root of the filename
  doCheckBnd: if nonzero, perform bnd check for periodicity, etc.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int write_gmsh ( const char *rootFlNm0, const int doCheckBnd ) {

  if ( Grids.PcurrentGrid->uns.type != uns ) {
    sprintf ( hip_msg, "you can only write unstructured grids to gmsh.\n" ) ;
    hip_err ( warning, 1, hip_msg ) ; }

  return ( write_gmsh_uns ( Grids.PcurrentGrid->uns.pUns, rootFlNm0, doCheckBnd ) ) ;
}

