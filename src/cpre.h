/*
   cpre.h:
   Global include file, contains defines needed in cpre_mb.h and
   cpre_uns.h. Includes all typdefs. Includes also all enums, since
   enum 'strings' are global, i.e. must be unique.
   
   Last update:
   ------------
   24Aug24; intro nxtSpecialTopo into Grids_s.
   6Dec20; add sector to geo_s.
   14May20; intro cyl to geo_s.
   3Apr20; rename grid.text to grid.name.
   6Sep19; include stdarg for var arg fprint wrappers.
   5Aug19; intro Grids.adapt.doPer.
   22Feb19; intro 'hip' varCat for internal use.
   21Mar18; intro doWarn.abort, doRemove_listUnMatchedFc.
   11May17; intro bcGeoType_e ;
            change order in doFc struct to match bcGeoType.
   17Feb17; combine surf2d and surf3d into a single surf specialTopo_e.
   16Dec16; intro topo types surf2d, surf3d.
   5Sep15; move lapack.h back into uns_interpolate, to allow 
           switching on -DClAPACK.
   18Dec13; make HH_ULG = H5T_NATIVE_ULONG only for USE_ULONG,
            otherwise HH_ULG = H5T_NATIVE_UINT
   15Dec13; remove annCasc from specialTopo_e, intro axiY, axiZ.
   14Dec13; intro typedef for str80.
   2Apr12; add blank status to hip_stat_e.
   31Mar12; include unistd.h for use of getopt() 
   9Jul11; intro *Par_s as generic parameter containers.
   7Jul11; read limits.h for INT_MAX
   16Sep10; intro transf_e.
   1Jul10; up filename len TEXT_LEN to 1024
           intro h5pList_s, modif of restart_u ;
   5Apr09; more restart_u from cpre_uns.h here, add neqf, nother.
   4Apr09; intro 'other' to varCat_e.
           change varType_s to varList_s, intro var_s.
   14Dec08; intro MINITXT_LEN
   11Dec07; intro variable type 'primT' for n3s.
   8Dec07; move DEFAULT_epso here from hip.c.
   11Oct07; support error/warning logging: intro hip_status_e, MAX_MSG_LINES.
   14Sep06; remove global variable category in varType_S.
   17May06; intro spec_bc_e.
   3Apr06; drop pNxtVar from varType_s.
   30Mar05; fix bug in the definition of ABS. Ouch!
   14Sep04; switch def of ABS to flip sign only on neg. numbers, avoid switching 0.
   13Jul04; all filenams now of TEXT_LEN.
   1Sep3; change def of varType->pVarName to an array.
   18Dec2; remove no_reco from reco_enum, intro MAX_LS_BASES.
   25Apr02; add a name array to varType.
   20Dec01; move declaration of perBc_s from cpre.h to cpre_uns.h, 
            add *pPerBc to bc_struct.
   12Jan01; intro Grids.adapt.
   7Sep00; intro mUnknFlow to varTypeS.
   19Mar98; move all unstructured typedefs to cpre_uns.h.
   7Feb98; introduce the compound pointer cpt_s.
   28Jan98; create uns_struct.
   6May96: cpre_mb.h and cpre_uns.h cut off, add patch pointer to bc_struct.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <getopt.h>

#define MAX_DIM 3
#define MAX_UNKNOWNS 256
#define MAX_GROUPS 20 /* number of groups in an hdf file. */


#define MAX_ROT_STRUCT 24
#define MAX_BC_CHAR 81
#define NO_DBL_MARK ( -999. )
#define NO_INT_MARK ( -999 )
#define TOO_MUCH ( 1.e25 )
#define MIN_POSITIVE_FLOAT ( 1.e-25 )
#define TEXT_LEN 1024
#define MINITXT_LEN 10

#define IO_STREAM_LEN 1024

#define REALLOC_FACTOR 1.33

/* Default precision for nodal coordinates. */
#define DEFAULT_epsOverlap (1.e-20)


/* Check the match for internal faces on multiblock grids. Good to have it.*/
#define CHECK_MATCH
/* Check convexity. Applies to adapted grids when buffering and reading back in. */
#define CHECK_CONVEX
/* Check boundary vertices against the list of faces.
#define CHECK_BOUND_VERTS */

#ifdef DEBUG
/* Check array bounds. Should only be necessary during initial debug. */
#  define CHECK_BOUNDS
#endif

/* The Makefile should set this properly using your HOSTTYPE environment.
   The definition is now in read1.h. Make this one here obsolete, fix this. */
#ifdef LITTLE_ENDIAN
#  define FREAD fread_linux
#  define FWRITE fwrite_linux
#else
#  define FREAD fread
#  define FWRITE fwrite
#endif



/* Large unsigned integer types and corresponding format specifiers. 
   For very large meshes use ulong_t for element, node, face and 
   patch numbers. Numbers of boundaries or zones should never be
   big enough to exceed an int. 

   From /usr/include/limits.h:
   int                  INT_MAX	   2147483647
   unsigned int:        UINT_MAX   4294967295U
   64bit unsigned long: ULONG_MAX  18446744073709551615UL
*/
#ifdef HIP_USE_ULONG
#  define ulong_t size_t
#  define FMT_ULG "zu"
/* ulong_t for hdf: unsigned long for large grids. */
#define HH_ULG H5T_NATIVE_ULONG
#else
#  define ulong_t unsigned int
#  define FMT_ULG "u"
/* ulong_t for hdf: unsigned int. */
#define HH_ULG H5T_NATIVE_UINT 
#endif


/* Data (memory) type shorthands for HDF. */

#define HH_DBL H5T_NATIVE_DOUBLE
#define HH_INT H5T_NATIVE_INT
#define HH_STR H5T_C_S1 





/* To all dilettants: don't edit anything past here. */

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#define SGN(a) ( (a) < 0 ? (-1) : (1) )
#define ABS(a) ( (a) < 0 ? (-(a)) : (a) )

#ifndef PI
#  define PI 3.14159265358979323846
#endif



/* cpre.h: */

/* Simplifies allocation of arrays of strings. */
typedef enum { noFixStr, fxStr80, fxStr240 } fxStr_e ;
typedef char string80[80]; 
typedef char string240[240]; 


typedef enum { success, fatal, warning, info, blank } hip_stat_e ;


typedef struct _Grids_struct Grids_struct ;
typedef union  _grid_struct  grid_struct ;
typedef struct _bc_struct    bc_struct ;

// topoString is def'd in hip.c
// unDef is only a spare return arg for failure
typedef enum { noTopo, axiX, axiY, axiZ, noBc, surf, unDef } specialTopo_e ;

/* cpre_mb.h: */
typedef struct _mb_struct       mb_struct ;
/* Some private mb functions appearing in proto_mb.h require this. Fix this. */
typedef struct _block_struct    block_struct ;
/* subFace_struct still appears in a linked list with each bc. Fix this. */
typedef struct _subFace_struct  subFace_struct ;
typedef struct _rotation_struct rotation_struct ;

/* cpre_uns.h */
typedef struct _uns_s uns_s ;
typedef struct _perBc_s perBc_s ;

/* heap.c */
typedef struct _heap_s heap_s ;

/* Grid types recognized so far. */
typedef enum { noGr, mb, uns } grid_type_enum ;

/* transformation options, hip.c, uns_copy.c */
typedef enum { noTr, 
               trans, 
               rot_x, rot_y, rot_z,
               scale, 
               ref_x, ref_y, ref_z } transf_e ;

/* Types of aspect ratios. */
typedef enum { noIso, minmax_all, minmax_dir,
	       avg_all, avg_dir } isotype_enum ;

/* Types of function reconstruction. */
/* At most a quadratic in 3D. */
#define MAX_LS_BASES (10)
typedef enum { reco_el, reco_1, reco_2, reco_min_norm, reco_flag } reco_enum ; 

/* Warning and removal directives used in check_uns. Needs to be known by hip.c */
typedef struct _doFc_struct  doFc_struct ;
struct _doFc_struct {
  int bndFc ;
  int matchFc ;
  int intFc ;
  int abortFc ;
  int listUnMatchedFc ;
} ;



/*********************************************************************
        Variable definitions. */


/* Types of state variables. */
/*#define LEN_VARTYPENAME 6 */
#define LEN_GRPNAME 30
#define LEN_VARNAME 30
#define LEN_VAR_C 8
typedef enum { noVar, // no variables present, mEq=0. 
               cons, prim, primT, para, 
               noType // unknown type.
            } varType_e ;
typedef enum { noCat, ns, species, rrates, tpf, rans, add, mean, fictive, 
               add_tpf, param, other, hip } varCat_e ;
/* Definition in hip.c
const char varCatNames[][LEN_VAR_C] =  { "noCat", "ns", "species", "rrates",
                                         "tpf", "rans", "add", "mean", "fictive",
                                         "add_tpf", "param", "other" } ;
*/

typedef struct {
  varCat_e cat ;          /* used for grouping in avbp formats. */
  char grp[LEN_GRPNAME] ; /* used for grouping in hdf formats. */
  char name[LEN_VARNAME] ;
  int isVec ;             /* 0 for scalar, 1 ... 3 for x,y,z comp of a vector. */
  
  int flag ;
} var_s ;



typedef struct _varList_s varList_s ;
struct _varList_s {
  int mUnknowns ; 
  int mUnknFlow ; /* Number of flow uknowns, used for AVBP formats. */
  varType_e varType ; /* cons or other? */

  var_s var[MAX_UNKNOWNS] ; /* List of variables. */
  double freeStreamVar[MAX_UNKNOWNS] ;
} ;






/* ****************************************************************************
   Generic containers for parameters to be carried along with the 
   solution in/out of hip. */

typedef enum { noPar, parInt, parDbl, parVec } parType_e ;
typedef struct _param_s param_s ;
struct _param_s {
  char name[LEN_VARNAME] ;
  parType_e parType ;
  int dim ;
  void *pv ;
  
  param_s *pPrvPar ;
  param_s *pNxtPar ;
  unsigned int flag ;
} ;




/* Log scalar parameters in h5 files for regurgitation, 
   trace only int, double and string=char[TEXT_LEN] */
#define MAX_H5_PARAMS 100
typedef enum { h5_i, h5_d, h5_s } h5_var_e ;
typedef union {
  struct {
    char label[TEXT_LEN] ;
    h5_var_e type ;
    int iVal ;
  } i ;
  struct {
    char label[TEXT_LEN] ;
    h5_var_e type ;
    double dVal ;
  } d ;
  struct {
    char label[TEXT_LEN] ;
    h5_var_e type ;
    char str[TEXT_LEN] ;
  } s ;
} h5pList_s ;


/*
1: .visual files. 2: from ADF, 
3: AVBP meanval., 4: tpf v5.1, 5: tpf v2.0, 6: avsp s3.0, 
10: hdf, 11: hdfa 
50: n3s pre 2.52, n3s post 2.52
*/
typedef enum { 
  visual, adf, avbp_mean, avbp_tpf_v5, avbp_tpf_v2, avsp_s3,
  hdf, hdfa, n3s_v2, n3s_v2p52 
} iniSrc_e ;

typedef union {
  struct {
    iniSrc_e iniSrc ;
    int itno, neqf, neqs, neqfic, nreac, neqt, nadd, neq2pf, nadd_tpf, nother ;
    double dtsum, rhol ;
  } any ;
  struct {
    iniSrc_e iniSrc ;
    int itno, neqf, neqs, neqfic, nreac, neqt, nadd, neq2pf, nadd_tpf, nother ;
    double dtsum, rhol ;
    /* tpf 2.0, only for avbp write */
    int ithick, iles, ichem, iavisc, ipenalty, iwfles, istoreadd, istoreadd_tpf, 
        iavisc_tpf, ipenalty_tpf, nvar_evap, nvar_sigma, nvar_qb ;
    double dt_av, ma_cp, gmm1 ;
  } avbp ;
  struct {
    iniSrc_e iniSrc ;
    int itno, neqf, neqs, neqfic, nreac, neqt, nadd, neq2pf, nadd_tpf, nother ;
    double dtsum, rhol ;
    /* List of hdfa parameters. */
    int m5pList ;
    h5pList_s h5pList[MAX_H5_PARAMS] ;
    char ielee ;
  } hdf ;
} restart_u ;





/* A list of boxes, Haribos, for inclusions. */
typedef struct {
  /* The two endpoints of a line and a squared radius. */
  double llEnd[MAX_DIM] ;
  double urEnd[MAX_DIM] ;
  double radiusSq ;
  /* The normalized vector along the line. */
  double vec1[MAX_DIM] ;

  /* A rectangular bounding box for fast exclusion. */
  double llBox[MAX_DIM] ;
  double urBox[MAX_DIM] ;
  double rllBox[2] ;       /* Cylindrical coordinates Bounding box of all grids. */
  double rurBox[2] ;    
} hrb_s ;

typedef struct {
  int mHrbs ;
  hrb_s *pHrb ;
} hrbs_s ;


typedef enum { noGeo, box, plane, cyl, sphere, hrb, sector,
               allGeo, remaining, nodeFlagged, elementFlagged } geo_e ;

typedef union {
  struct {
    geo_e type ;
  } noGeo ;
  struct {
    geo_e type ;
    hrbs_s hrbList ;
  } hrb ;
  struct {
    geo_e type ;
    double ll[MAX_DIM] ;
    double ur[MAX_DIM] ;
  } box ;
  struct {
    geo_e type ;
    double loc[MAX_DIM] ;
    double norm[MAX_DIM] ;
  } plane ;
  struct {
    geo_e type ;
    double loc[MAX_DIM] ;
    double axis[MAX_DIM] ;
    double rad ;
  } cyl ;
  struct {
    geo_e type ;
    double loc[MAX_DIM] ;
    double rad ;
  } sph ;
  struct {
    geo_e type ;
     // index of fixed axis dir, the the two in the r-th plane,
    // 012 = xyz, only Cartesian axes.
    int kDim[MAX_DIM] ;
    double llf[MAX_DIM] ; // in Cartesian coor
    double urr[MAX_DIM] ;
    double r[2] ; // rad[0] <= r <= rad[1]
    // theta[0] <= theta <= theta[1] with
    // theta = 0 on the positive kDim+1 axis, branch cut in the negative kDim+1 axis.
    double th[2] ;
  } sec ;
 } geo_s ;





typedef struct {
  /* How many levels deep at most? */
  int maxLevels ;
  /* Upgrade if upRef edges of the next pattern exist. */
  double upRef ;
  /* Make adaptation periodic. 0 by default, as mmg_adapt for per
     has bugs. */
  int doPer ;
} adapt_s ;

/* Grids. The root to everything and the universe. */
struct _Grids_struct {
  int mGrids ;                  /* Total number of visible grids. */
  int mUnsGrids ;               /* Number of unstructured grids, counting also 
                                   coarsened grids. */
  grid_struct *PfirstGrid ;     /* Linked list of grids. */
  grid_struct *PlastGrid ;
  grid_struct *PcurrentGrid ;   /* This is the one we're working on. */
  
  double llBox[MAX_DIM] ;       /* Bounding box of all grids. */
  double urBox[MAX_DIM] ;
  double rllBox[2] ;       /* Cylindrical coordinates Bounding box of all grids. */
  double rurBox[2] ;

  /* Minimum difference required between two vertices. */
  double epsOverlap ;
  double epsOverlapSq ;

  double lp_tolerance ;         /* When is a gradient sufficiently lp? */
  int lp_sweeps ;               /* How many sweeps? */
  double egWtCutOff ;           /* Consider edge weights sgnificant down to this norm. */

  int fixPerVx ;                /* Fix or leave periodic vertex mismatch. */
  
  char path[TEXT_LEN] ;      /* Path to be prepended. */

  /* Boxes for adaption, etc. */
  hrbs_s hrbs ;

  /* Adaption parameters. */
  adapt_s adapt ;

  /* Params to be applied to the next grid read. */
  specialTopo_e nxtSpecialTopo ;
} ;

/* One instance of a grid. */
/* JDM to do: limit the union to pMb, pUns. */
union _grid_struct {
  struct {
    grid_struct *PnxtGrid ;
    grid_struct *PprvGrid ;
    int nr ;
    int mDim ;
    char name[TEXT_LEN] ;

    varList_s *pVarList ;
    
    grid_type_enum type ;
    mb_struct *Pmb ;
  } mb ;

  struct {
    grid_struct *PnxtGrid ;
    grid_struct *PprvGrid ;
    int nr ;
    int mDim ;
    char name[TEXT_LEN] ;

    varList_s *pVarList ;

    grid_type_enum type ;
    uns_s *pUns ;
  } uns ;
} ;



/* summary type for all wall and all per bcs, respectively. */
typedef enum { unspecBc, perBc, wallBc, otherBc } spec_bc_e ;



/* Geometric type of the bc. 
   bnd = normal boundary face to apply bc
   match = matching bc, to be removed in final mesh
   inter = interface bc, matching but to be retained, for application 
           of jump conditions
   cut = faces that cut across the mesh, no match with grid elements.
*/
typedef enum { bnd, match, inter, duplicateInter, cut, noBcGeoType, bndAndInter, any } bcGeoType_e ; 


/* Boundary conditions, used in structured and unstructured
   context. */
struct _bc_struct {
  char text[MAX_BC_CHAR] ;      /* The text of the bc. */

  spec_bc_e bc_e ;
  char type[MAX_BC_CHAR] ;
  /* ex doc:
   e:   & entry           &    o:   & outlet          & u:   & upper periodic  \\
   f:   & far field       &    p:   & any periodic    & v:   & viscous wall    \\
   i:   & inviscid wall   &    n:   & none            & w:   & any wall        \\
   l:   & lower periodic  &    s:   & symmetry plane  \\
  */
  perBc_s *pPerBc ;             /* Is it a periodic one? */  

  int nr ;                      /* The number assigned to this bc. */
  int order ;                   /* In which order to list them in output. */
  int refCount ;                /* Number of times this bc is referenced. */
  int mark ;

  bc_struct *PprvBc ;           /* Linked list of boundary conditions. */
  bc_struct *PnxtBc ;

  bcGeoType_e geoType ;         /* see def of bcGeoType_e above. */
   /* int matchBc ;                 True if this is a matching bc for composite meshes. */

  
  double llBox[MAX_DIM] ;       /* Bounding box of this bc. */
  double urBox[MAX_DIM] ;
  double rllBox[2] ;       /* Cylindrical coordinates Bounding box of all grids. */
  double rurBox[2] ;
  
  subFace_struct *ProotSubFc ;  /* Root of the linked list of
				   multiblock-structured subfaces. */
} ;

/* the array library. */
#define MBYTES_DEBUG 8
#include "./../lib_do/array/array.h"


/* Tree stuff. */
#include "tree_data.h"
#include "do2kdtree.h"
#include "./../lib_do/tree/tree_public.h"

/* the line-oriented I/O. */
#define LINE_LEN TEXT_LEN
#include "./../lib_do/read1/read1.h"

/* At most error/warning msgs of 10 lines. */
#define MAX_MSG_LINES 10



typedef struct {
  grid_struct *pGrid ;
  uns_s *pUns ;
  hip_stat_e status ;
  char *msg ;
} ret_s ;

typedef enum { screen, string, file } hip_output_e ;
