/*
  refine_uns.c:
  
  Last update:
  ------------
  13Jan01; switch to iso. Why wasn't aniso propagated?
  21Jul00; intro half face test to make aniso adapt work.
  Aug98; support adaption with fixed diagonals.
  
  
  This file contains:
  -------------------
  auh_find_ref
  auh_deref_elem
  auh_place_vx_edge
  auh_place_vx
  auh_naca0012_y

  auh_find_midVx:
  auh_fix_crossFc:
  auh_place_vx_elem
  auh_match_elem_eg:
  auh_match_elem_fc:
  
  deref_uns:
  auh_fill_uns_ref:
  auh_count_uns_ref_elems:
  auh_match_all_refs:

  refine_uns:
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const int bitEdge[MAX_EDGES_ELEM] ;
extern const elemType_struct elemType[] ;
extern const double chord ;

extern Grids_struct Grids ;

extern int check_lvl ;


/******************************************************************************

  auh_deref_elem:
  Derefine an element.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------
  pUns:
  pElem:       The parent element to be derefined.
  pllAdEdge:

  Changes To:
  -----------
  pElem:
  *PmDerefdElems: the number of removed elements and vertices.
  *PmDerefdVerts:
  doFaces: on 1, fix cross refinement on quad faces. This is relevant for
           derefinement. First create all the new edges, then create all
           cross edges for faces that have a fully refined perimeter.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_deref_elem ( uns_s *pUns, elem_struct *pElem, llEdge_s *pllAdEdge,
                     int *PmDerefdElems, int *PmDerefdVerts,
                     const int doFaces ) {
  
  const refType_struct *pRefT = pElem->PrefType ;
  const elemType_struct *pElT = elemType + pElem->elType ;
  
  int kChild, kEdge, nAe, mVxBase, kFace, mVxFc, nChAe[2],
    nFcAe[MAX_VX_FACE], nCrossAe[MAX_VX_FACE-1], nFixAe, fixDir, dir, kCross ;
  elem_struct *pChild ;
  chunk_struct *pRefChunk = NULL ;
  vrtx_struct *pLstVx ;
  const vrtx_struct *pVx[MAX_VX_ELEM+MAX_ADDED_VERTS] ;
  double *pLstCoor, *pLstUnknown ;
  
  /* Loop over all the children and find out whether they are leaves and marked
     for derefinement. */
  for ( kChild = 0 ; kChild < pRefT->mChildren ; kChild++ ) {
    pChild = pElem->PPchild[kChild] ;
    if ( !( pChild->leaf && !pChild->PPchild && pChild->derefElem ) )
      /* Either no leaf or no derefinement. */
      return ( 0 ) ;
  }

  /* Loop over all edges. Children prohibit derefinement. */
  for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
    nAe = get_elem_edge ( pllAdEdge, pElem, kEdge, pVx, pVx+1, &dir ) ;
    if ( nAe &&
         get_child_aE ( pUns, pllAdEdge, pUns->pAdEdge, nAe, nChAe, pVx ) )
      /* There are grandchildren. */
      return ( 0 ) ;
  }

  /* Loop over all faces. */
  for ( kFace = 0 ; kFace < pElT->mFaces ; kFace++ ) {
    get_face_aE ( pUns, pElem, kFace,
                  &mVxBase, &mVxFc, pVx, nFcAe, nCrossAe, &nFixAe, &fixDir ) ;
    for ( kCross = 0 ; kCross < MAX_VX_FACE-1 ; kCross++ )
      if ( nCrossAe[kCross] &&
           get_child_aE ( pUns, pllAdEdge, pUns->pAdEdge, nCrossAe[kCross],
                          nChAe, pVx ) )
        /* There are children to this cross edge. */
        return ( 0 ) ;
  }
  
  /* Derefinement possible. Loop over all the parent's refined edges and create
     them. Since all the nodes exist, we can supply a bogus chunk. */
  int dontBuf = 0 ;
  int nAeList[MAX_EDGES_ELEM], nCrossAeList[MAX_FACES_ELEM+1][MAX_VX_FACE] ;
  add_elem_aE_vx ( pUns, pElem,
                   doFaces, dontBuf,
                    ( vrtx_struct ** ) pVx, 
                   pRefChunk,
                &pLstVx, &pLstCoor, &pLstUnknown, 
                nAeList, nCrossAeList ) ;


  if ( doFaces ) {
    /* Remove the children. Set the parent's number to the first child's number. */
    pElem->number = pElem->PPchild[0]->number ;
    /* Void all the children. */
    for ( kChild = 0 ; kChild < pRefT->mChildren ; kChild++ ) {
      pChild = pElem->PPchild[kChild] ;
      pChild->invalid = 1 ;
      pChild->leaf = pChild->term = pChild->number = 0 ;
      pChild->PPvrtx = NULL ;
    }
    /* Make the parent a leaf. Lose the storage associated with the children. */
    pElem->leaf = pElem->term = 1 ;
    /* pElem->PpChild = NULL ; */
    
    /* Count the derefinement. */
    *PmDerefdElems += pRefT->mChildren ;
    /* Was there a center vertex? */
    if ( pRefT->kElemVert )
      (*PmDerefdVerts)++ ;
    
    /* Make the parent a leaf. */
    pElem->PrefType = NULL ;
  }
  
  return ( 1 ) ;
}


/******************************************************************************

  deref_uns:
  Derefine all parent elements that have all children marked for derefinement.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------
  *pUns,
  *PmDerefdElems,
  *PmDerefdVerts
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_deref_uns_hierarchical ( uns_s *pUns, int *PmDerefdElems, int *PmDerefdVerts ) {
  
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  chunk_struct *pChunk ;
  elem_struct *pElem ;
  const int doFaces = 1, dontFaces = 0 ;

  /* In a first pass create all the elemental hanging edges that are exposed
     by derefinement. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    for ( pElem = pChunk->Pelem + 1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( !pElem->leaf && pElem->PrefType )
	if ( pElem->PPchild[0]->leaf && pElem->PrefType->refOrBuf == ref )
	  /* This is a parent with a first child as a leaf. Try to derefine. */
	  auh_deref_elem ( pUns, pElem, pllAdEdge, PmDerefdElems, PmDerefdVerts,
                       dontFaces ) ;

  
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    for ( pElem = pChunk->Pelem + 1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( !pElem->leaf && pElem->PrefType )
	if ( pElem->PPchild[0]->leaf && pElem->PrefType->refOrBuf == ref )
	  /* This is a parent with a first child as a leaf. Try to derefine. */
	  auh_deref_elem ( pUns, pElem, pllAdEdge, PmDerefdElems, PmDerefdVerts,
                       doFaces ) ;

  return ( 1 ) ;
}


/******************************************************************************

  Look for missing midvertices. Existing face vertices have been found in
  fix_crossfc. Now loop over the edges of refined elements. If the edge exists,
  compare the children vx to the midedge one.
  .
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_find_midVx ( uns_s *pUns ) {

  const refType_struct *pRefT ;
  const edgeVert_s *pREV ;
  const faceVert_s *pRFV ;
  const vrtx_struct *pVx[2] ;
  const elemType_struct *pElT ;
  const int *kFcEdge ;
  
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  adEdge_s *pAdEdge = pUns->pAdEdge ;
  int iEg, kEg, nAe, dir, iFc, kFc ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pChild ;
  vrtx_struct *pVxEdge[MAX_EDGES_ELEM], *pVxFace ;
#ifdef DEBUG
  int mVxFound = 0 ;
#endif
  

  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) 
    /* Loop over all cells that can define interfaces. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( !pElem->invalid && ( pRefT = pElem->PrefType ) ) {
        /* This element is refined. Look for descendant vertices. */
        pElT = elemType + pElem->elType ;

        /* Reset the added edge vertices. */
        for ( kEg = 0 ; kEg < MAX_EDGES_ELEM ; kEg++ )
          pVxEdge[kEg] = NULL ;
          
        
        /* First loop over all edges that carry an extra vertex. */
        for ( iEg = 0 ; iEg < pRefT->mEdgeVerts ; iEg++ ) {
          pREV = pRefT->edgeVert+iEg ;
          kEg = pREV->kEdge ;

          if ( ( nAe = get_elem_edge ( pllAdEdge, pElem, kEg, pVx, pVx+1, &dir ) ) &&
               !pAdEdge[nAe].cpVxMid.nr ) {
            /* This one is missing. */
            pChild = pElem->PPchild[ pREV->kChild ] ;
            pVxEdge[kEg] = pChild->PPvrtx[ pREV->kChildVx ] ;
            pAdEdge[nAe].cpVxMid = pVxEdge[kEg]->vxCpt ;
#ifdef DEBUG
            mVxFound++ ;
#endif
          }
        }

        /* Secondly, loop over all faces. Note that we are only looking for
           hanging vertices on quad faces. Since all the faces attached to the
           center vertex of a fully refined hex are perfectly matched, we never
           need to look for this one. */
        for ( iFc = 0 ; iFc < pRefT->mFaceVerts ; iFc++ ) {
          pRFV = pRefT->faceVert+iFc ;
          kFc = pRFV->kFace ;
          kFcEdge = pElT->faceOfElem[kFc].kFcEdge ;

          pChild = pElem->PPchild[ pRFV->kChild ] ;
          pVxFace = pChild->PPvrtx[ pRFV->kChildVx ] ;

          /* Look for both cross edges. */
          pVx[0] = pVxEdge[ kFcEdge[0] ] ;
          pVx[1] = pVxEdge[ kFcEdge[2] ] ;
          if ( ( nAe = get_edge_vrtx ( pllAdEdge, pVx, pVx+1, &dir )) &&
               !pAdEdge[nAe].cpVxMid.nr )
            pAdEdge[nAe].cpVxMid = pVxFace->vxCpt ;
          
          pVx[0] = pVxEdge[ kFcEdge[1] ] ;
          pVx[1] = pVxEdge[ kFcEdge[3] ] ;
          if ( ( nAe = get_edge_vrtx ( pllAdEdge, pVx, pVx+1, &dir )) &&
               !pAdEdge[nAe].cpVxMid.nr )
            pAdEdge[nAe].cpVxMid = pVxFace->vxCpt ;
        }
      }
  
  return ( 1 ) ;
}

/******************************************************************************

  auh_add_cross_aE:
  Given eight vertices in order around a fully refined quad face, add the
  two cross edges. 
  
  Last update:
  ------------
  12Dec19; rename to auh_
  28Nov19; simplified interface ot add_quadFc_aE.
  3Mar00; cut out of auh_fix_crossFc.
  
  Input:
  ------
  pUns
  pVxFace:   the eight vertices in circumferential order.
  pRefChunk: the new vertex space if a vertex needs to be created.
  ppLstVx
  ppLstCoor
  ppLstUnknown:
  doBuf: if non-zero, don't consider periodic siblings.

  Changes To:
  -----------
  pRefChunk
  
  Returns:
  --------
  the number of added vertices.
  
*/

int auh_add_cross_aE ( uns_s *pUns, const vrtx_struct *pVxFace[2*MAX_VX_FACE+1],
                       chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                       double **ppLstCoor, double **ppLstUnknown,
                       const int doBuf ) {

  const vrtx_struct *pVrtx1, *pVrtx2 ;
  int iEg, nFcAe[MAX_VX_FACE], nCrossAe[2], newEg, mVxAdded = 0,
    nPerEg, nFixAe, fixDir ;
  cpt_s cp0, cp1 ;
  vrtx_struct *pVrtxCtr, *pVxCrnr[4] ;

  /* Add the cross edges. */
  for ( iEg = 0 ; iEg < 2 ; iEg++ )
    if ( ( pVrtx1 = pVxFace[2*iEg+1] ) && ( pVrtx2 = pVxFace[2*iEg+5] ) )
      if ( !( nCrossAe[iEg] =
              add_adEdge_vrtx ( pUns, (VX*) pVrtx1, (VX*) pVrtx2,
                                &nPerEg, &newEg ) ) ) {
        hip_err ( fatal, 0, "could not add adapted edge in auh_fill_uns_ref.");
        return ( 0 ) ; }

      
  /* Find a vertex for the cross edges. Three possibilities exist:
               - the vertex exists on the edges.
               - the vertex exists with a child
               - the vertex has to be created. */
  cp0 = pUns->pAdEdge[ nCrossAe[0] ].cpVxMid ;
  cp1 = pUns->pAdEdge[ nCrossAe[1] ].cpVxMid ;

  if ( cp0.nr && cp1.nr ) {
    if ( cp1.nr != cp0.nr || cp1.nCh != cp0.nCh ) {
      hip_err ( fatal, 0, "mismatch of face vertex in auh_fill_uns_ref." ) ;
      return ( 0 ) ;
    }
  }
  else if ( cp1.nr ) {
    pUns->pAdEdge[ nCrossAe[0] ].cpVxMid = cp0 = cp1 ;
  }
  else if ( cp0.nr ) {
    pUns->pAdEdge[ nCrossAe[1] ].cpVxMid = cp1 = cp0 ;
  }

  if ( !cp0.nr && pRefChunk ) {
    /* Make a new one. Repack the corner vertices. */
    mVxAdded++ ;
    pVxCrnr[0] = (VX*) pVxFace[0] ;
    pVxCrnr[1] = (VX*) pVxFace[2] ;
    pVxCrnr[2] = (VX*) pVxFace[4] ;
    pVxCrnr[3] = (VX*) pVxFace[6] ;

    add_quadFc_aE ( pUns, (VX**) pVxCrnr, doBuf, 
                    &pVrtxCtr, nFcAe, nCrossAe, &nFixAe, &fixDir,
                    pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown ) ;
  }

  return ( mVxAdded ) ;
}
        
/******************************************************************************

  auh_fix_crossFc:
  Given a face, place two cross edges with a center vertex if all edges around
  the face are refined.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------
  pUns
  pRefChunk     Where to put the newly created vertices.
  ppLstVx
  ppLstCoor
  ppLstUnknown


  Changes To:
  -----------
  pUns
  pRefChunk
  ppLstVx
  ppLstCoor
  ppLstUnknown

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_fix_crossFc ( uns_s *pUns,
                      chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                      double **ppLstCoor, double **ppLstUnknown,
                      const int doBuf ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxFace[2*MAX_VX_FACE+1] ;
  const elem_struct *pElem ;

  chunk_struct *pChunk ;
  int mVxAdded = 0 ;

  
  /* Make sure all quad faces are compatible. Loop also over faces of non-terminal
     elements, since the cross-refinement can have existed already in the mesh
     to be refined. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    /* Loop over all cells that can define interfaces. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) {
      if ( !pElem->invalid ) {
        /* This element is to be refined. */
        /* If a face is has crossing half-refinement, add a vertex on the face. */
        mVxAdded += add_elem_crossFc ( pUns, pElem,
                                       pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown,
                                       doBuf ) ;

      }
    }
  }
  
  return ( mVxAdded ) ;
}

/******************************************************************************

  auh_naca0012_y:
  For a given x, find the y of a NACA 0012 w/ 1.0089299679 chord.	    
  .
  
  Last update:
  ------------
  12Dec19; rename to auh_
  5Mar3; correct camber formula from
         http://www.aerospaceweb.org/question/airfoils/q0041.shtml
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------

  
*/

double auh_naca0012_y ( double x, int upperSurface ) {
  
  double CMax, CLoc, Thck, yChord, yThick;

  CMax = CLoc = .0, Thck = .12 ;
  
  if ( x < 0. ) {
    sprintf ( hip_msg, "invalid x %g in auh_naca0012_y.\n", x ) ;
    hip_err ( fatal, 0, hip_msg ) ;
    return ( 0 ) ;
  }
  /* Renormalize.; */
  x /= chord ;
  
  if ( x < CLoc )
    yChord = CMax/CLoc*( 2. - x/CLoc )*x ;
  else
    yChord = CMax/(1.-CLoc)*(1.-CLoc)*( (1.-2.*CLoc) + ( 2.*CLoc - x )*x ) ;

  /*       This is the original NACA coefficient: -.1015* x **4) */
  /* TE at x=1 with a modified NACA coefficient: -.1036* x **4) */
  yThick = 5.*Thck*( .2969*sqrt(x) +
		     x*( -.1260 + x*( -.3516 + x*( .2843 + x*( -.1036)))) ) ;

  if ( upperSurface )
    return ( yChord + yThick ) ;
  else
    return ( yChord - yThick ) ;
}

/******************************************************************************

  auh_naca0012_y:
  Place all boundary nodes on the analytic curve.    
  .
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------

  
*/

void auh_match_naca0012 ( uns_s *pUns ) {

  const int *kVxEg ;

  bndPatch_struct *pBp ;
  bndFc_struct *pBf, *pBfBeg, *pBfEnd ;
  elem_struct *pElem ;
  double *pCo, y, dy, rms = 0., dyMax = -TOO_MUCH ;
  int nBc, upper, kEg ;
  
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( !strncmp( pUns->ppBc[nBc]->text, "naca0012", 8 ) ) {

      pBp = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBp, &pBfBeg, &pBfEnd ) )
        for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ ) {
          pElem = pBf->Pelem ;
          if ( pElem && pElem->number ) {
            kEg = elemType[ pElem->elType ].faceOfElem[ pBf->nFace ].kFcEdge[0] ;
            kVxEg = elemType[ pElem->elType ].edgeOfElem[ kEg ].kVxEdge ;
            /* Contour is closed, it suffices to do one end. */
            pCo = pElem->PPvrtx[ kVxEg[0] ]->Pcoor ;
            
            upper = ( pCo[1] > 0. ? 1 : 0 ) ;
            y = auh_naca0012_y ( pCo[0], upper ) ;

            dy = pCo[1]-y ;
            rms += dy*dy ;
            dyMax = MAX( dy, dyMax ) ;

            pCo[1] = y ;
          }
        }
    }

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "enforced naca0012 boundary, L2: %g, eMax: %g\n", rms, dyMax ) ;
    hip_err ( info, 2, hip_msg ) ;
  }
  return ;
}


/******************************************************************************

  auh_place_vx_edge:
  Place a vertex at the center of a face of an element.
  Seems unused 12/12/19?
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

vrtx_struct *auh_place_vx_edge ( const elem_struct *pElem, const int kEdge,
                             const uns_s *pUns, chunk_struct *pChunk,
                             vrtx_struct **ppLstVx, double **ppLstCoor,
                             double **ppLstUnknown ) {
  
  const int *kVxEdge = elemType[ pElem->elType ].edgeOfElem[kEdge].kVxEdge ;
  vrtx_struct *pVrtx[2] ;

  pVrtx[0] = pElem->PPvrtx[ kVxEdge[0] ] ;
  pVrtx[1] = pElem->PPvrtx[ kVxEdge[1] ] ;

  return ( adapt_uh_place_vx ( pUns, pChunk, ppLstVx, ppLstCoor, ppLstUnknown, pVrtx, 2 ) ) ;
}
/******************************************************************************

  auh_fill_uns_ref:
  Create the refined elements and vertices.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  10Sep16; use reset_elem_mark.
  10Jan15; allow pRefChunk to be an extended rootChunk, rather than a
           separately allocated one.
  16Dec11; rename hrbMark to boxMark.
  Aug98; support adaption with fixed diagonals.
  : conceived.
  
  Input:
  ------
  pUns: grid
  pRefChunk: chunk to add the refined elements to. Could be an extended
             rootChunk or a newly allocated one (deprecated option.)

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_fill_uns_ref ( uns_s *pUns, chunk_struct *pRefChunk ) {
  
  const refType_struct *pRefT ;
  const edgeVert_s *pEV ;
  const int doFaces = 1, *kVxEg ;

  elem_struct *pLstEl, *pElem, **ppLstChild ;
  /* Note: the elem2Vert space index starts at 0. */
  vrtx_struct *pLstVx, **ppLstVx, *ppVrtx[ MAX_VX_ELEM + MAX_ADDED_VERTS ],
    *pVx0, *pVx1, *pVxMid ;
  chunk_struct *pChunk ;
  int iVert, kChild, kCtrVx, nBc, kEg, kEdge, iEdge, upper ;
  double *pLstCoor, *pLstUnknown ;
  bndPatch_struct *pBp ;
  bndFc_struct *pBfBeg, *pBfEnd, *pBf ;

  pUns->adapted = 1 ;


  // Pointers to the last used entities. 
  pLstEl = pRefChunk->Pelem + pRefChunk->mElemsUsed ;
  ppLstVx = pRefChunk->PPvrtx + pRefChunk->mElem2VertPUsed - 1 ;
  ppLstChild = pRefChunk->PPchild + pRefChunk->mElem2ChildPUsed -1 ;
  int mVU = pRefChunk->mVertsUsed ;
  pLstVx = pRefChunk->Pvrtx + mVU  ;
  pLstCoor = pRefChunk->Pcoor+ mVU*pUns->mDim  ;
  pLstUnknown = pRefChunk->Punknown + mVU*pUns->varList.mUnknowns   ; 


  const int dontBuf = 0 ;
  int nAeList[MAX_EDGES_ELEM], nCrossAeList[MAX_FACES_ELEM+1][MAX_VX_FACE] ;
  for ( pChunk = pUns->pRootChunk ; 
        pChunk && pChunk != pRefChunk ; pChunk = pChunk->PnxtChunk ) 
    /* Loop over all cells that can define interfaces. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) {
      /* Initially no elements have been refined at this level. Set this mark
	 for each refined element and read it for the creation of boundary
	 faces. */
      pElem->mark = 0 ;
      
      if ( pElem->term && ( pRefT = pElem->PrefType ) && pRefChunk) {
        /* This element is to be refined. Add all new vertices on 
           edges and faces. */
        add_elem_aE_vx ( pUns, pElem, doFaces, dontBuf,
                          ppVrtx, pRefChunk,
                     &pLstVx, &pLstCoor, &pLstUnknown, 
                     nAeList, nCrossAeList ) ;
        
        /* Add the centernode. */
        if ( ( kCtrVx = pRefT->kElemVert ) )
          ppVrtx[kCtrVx] = adapt_uh_place_vx_elem ( pElem, pUns, pRefChunk,
                                           &pLstVx, &pLstCoor, &pLstUnknown ) ;
        
        /* Make the children. */
        pElem->PPchild = ppLstChild + 1 ;
        for ( kChild = 0 ; kChild < pRefT->mChildren ; kChild++ ) {
          pLstEl++ ;
          
#         ifdef CHECK_BOUNDS
            if ( pLstEl > pRefChunk->Pelem + pRefChunk->mElems ) {
              sprintf ( hip_msg, "beyond bounds of pRefChunk->Pelem (%d,%d)"
                        " in auh_fill_uns_ref.\n",
                       (int)(pLstEl - pRefChunk->Pelem), pRefChunk->mElems ) ;
              hip_err ( fatal, 0, hip_msg ) ; }
#         endif
	    
          pLstEl->elType = pRefT->child[kChild].elType ;
          pLstEl->Pparent = pElem ;
          pLstEl->root = pLstEl->invalid = 0 ;
          pLstEl->PPchild = NULL ;
          pLstEl->leaf = pLstEl->term = 1 ;
          pLstEl->derefElem = pLstEl->mark = 0 ;
          reset_elem_mark ( pLstEl, CUT_MARK ) ;
          //pLstEl->cutMark = 0 ;
          reset_elem_mark ( pLstEl, BOX_MARK ) ;
          //pLstEl->boxMark = pElem->boxMark ;
          pLstEl->PrefType = NULL ;
          pLstEl->markdEdges = pLstEl->refdEdges = 0 ;
          pLstEl->number = pLstEl-pRefChunk->Pelem + pUns->nHighestElemNumber ;
          pLstEl->PPvrtx = ppLstVx+1 ;
          
          //#         ifdef CHECK_BOUNDS
            if ( ppLstVx + elemType[pLstEl->elType].mVerts >=
                 pRefChunk->PPvrtx + pRefChunk->mElem2VertP ) {
              sprintf ( hip_msg, "beyond bounds of pRefChunk->PPvrtx (%"FMT_ULG",%"FMT_ULG")"
                       " in auh_fill_uns_ref.\n",
                       (ppLstVx-pRefChunk->PPvrtx + 
                         elemType[pLstEl->elType].mVerts),
                       (pRefChunk->mElem2VertP) ) ;
              hip_err ( fatal, 0, hip_msg ) ; }
            //#         endif
	    
          for ( iVert = 0 ; iVert < elemType[pLstEl->elType].mVerts ; iVert++ )
            *(++ppLstVx) = ppVrtx[ pRefT->child[kChild].kParentVert[iVert] ] ;
          *(++ppLstChild) = pLstEl ;
        }
        
        /* Mark the element inactive. */
        pElem->leaf = pElem->term = 0 ;
        pElem->number = 0 ;
      }
    }

  /* update counters with refchunk. */
  pRefChunk->mElemsUsed  = (pLstEl - pRefChunk->Pelem) ;
  pRefChunk->mElem2VertPUsed  = ( ppLstVx - pRefChunk->PPvrtx ) ; 
  pRefChunk->mElem2ChildPUsed  = (ppLstChild - pRefChunk->PPchild)+1 ; 

  

  /* Do a first pass, create all cross edges that are needed. */
  auh_fix_crossFc ( pUns, NULL, &pLstVx, &pLstCoor, &pLstUnknown, dontBuf ) ;

  /* Loop over all elements and try to find existing midvertices. */
  auh_find_midVx ( pUns ) ;

  /* Do a second pass, create all vertices that are still missing. Take
     care of propagation here. */
  while ( auh_fix_crossFc ( pUns, pRefChunk, &pLstVx, &pLstCoor, &pLstUnknown,
                            dontBuf ) )
    ;

#ifdef DEBUG
  {
    int mEg, nEg ;
    vrtx_struct *pVx[2] ;
    get_number_of_edges ( pUns->pllAdEdge, &mEg ) ;

    for ( nEg = 1 ; nEg <= mEg ; nEg++ )
      if ( show_edge ( pUns->pllAdEdge, nEg, pVx, pVx+1 ) &&
           !pUns->pAdEdge[nEg].cpVxMid.nr ) {
        sprintf ( hip_msg, "missing midvertex on edge %d in auh_fill_uns_ref.", 
                  nEg ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
  }
#endif
  
  

  /* Note that make_refbuf_bndfc relies on pElem->mark being set for refined
     elements. */
  if ( pRefChunk && !make_refbuf_bndfc ( pUns, pRefChunk ) )
    hip_err ( fatal, 0, "could not find boundary faces in buffer_elems.\n" ) ;

  
    
  /* Reset all element marks. 
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) 
    for ( pElem = pChunk->Pelem+1 ; 
          pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      pElem->mark = 0 ; */
  reset_all_elem_mark ( pUns, 0 ) ;


  /* Fix surface nodes of known geometries. */
  double x0, x1, y0, y1, y0p, y1p, xAvg, yAvg, xMid, yMid,
    dx, dy, yAvgp, egLen, d0, d1 ;
  int n ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( !strncmp( pUns->ppBc[nBc]->text, "naca0012", 8 ) ) {
 
      pBp = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBp, &pBfBeg, &pBfEnd ) )
        for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ ) {
          pElem = pBf->Pelem ;
          if ( pElem && !pElem->number && ( pRefT = pElem->PrefType ) &&
               pElem->PrefType->refOrBuf == ref ) {
            /* This is a refined element with a boundary face. */
            kEg = elemType[pElem->elType].faceOfElem[ pBf->nFace ].kFcEdge[0] ;
            kVxEg = elemType[ pElem->elType ].edgeOfElem[ kEg ].kVxEdge ;
            pVx0 = pElem->PPvrtx[ kVxEg[0] ] ;
            pVx1 = pElem->PPvrtx[ kVxEg[1] ] ;


            /* Loop over all the refined edges to find kEg. */
            for ( pVxMid = NULL, iEdge = 0 ; 
                  iEdge < pRefT->mEdgeVerts ; iEdge++ ) {
              kEdge = pRefT->edgeVert[iEdge].kEdge ;
              if ( kEdge == kEg ) {
                pEV = pRefT->edgeVert + iEdge ;
                pVxMid = pElem->PPchild[pEV->kChild]->PPvrtx[ pEV->kChildVx ] ;
                break ;
              }
            }

            /* Is this vertex a newly created one? Then it has to be in the
               last chunk. */
            if ( pVxMid && pVxMid->vxCpt.nCh == pUns->mChunks-1 ) {
              /* Actual coordinates of the parent vertices. */
              x0 = pVx0->Pcoor[0] ;
              x1 = pVx1->Pcoor[0] ;
              y0 = pVx0->Pcoor[1] ;
              y1 = pVx1->Pcoor[1] ;
              xAvg = xMid = .5*(x0+x1) ;
              yAvg =        .5*(y0+y1) ;
              
              /* Upper or lower surface? */
              upper = ( yAvg > 0. ? 1 : 0 ) ;
              y0p  = auh_naca0012_y ( x0,   upper ) ;
              y1p  = auh_naca0012_y ( x1,   upper ) ;
              yAvgp = .5*( y0p+y1p ) ;

              /* Normal to the secant out of the domain. */
              dy = y1p - y0p ;
              dx = x0  - x1 ;
              egLen = sqrt( dx*dx + dy*dy ) ;

              for ( n = 0 ;  n < 55 ; n++ ) {
                yMid = auh_naca0012_y ( xMid, upper ) ;
              
                /* Squared distances to either end vertex. */
                dx = ( xMid - x0 ) ;
                dy = ( yMid - y0p ) ;
                d0 = dx*dx + dy*dy ;
                
                dx = ( xMid - x1 ) ;
                dy = ( yMid - y1p ) ;
                d1 = dx*dx + dy*dy ;

                if ( ABS(1.-d0/d1) < .01 ) {
                  /* Sufficiently equidistant. */
                  dx = xMid - xAvg ;
                  dy = yMid - yAvgp ;
                  break ;
                }
                else if ( d0 > d1 )
                  /* Do a bisection. */
                    x1 = xMid ;
                else
                    x0 = xMid ;
                  
                /*printf ( "%4d: %2d-%2d, %9.6f %9.6f ( %9.6f %9.6f )\n",
                  n, pVx0->number, pVx1->number,
                  xMid, yMid, d0, d1 ) ;*/
                  xMid = .5*(x0+x1) ;
              }
              
              /* Use the actual point. Requires that all initial points are
                 on the surface, as well. */
              pVxMid->Pcoor[0] = xMid ;
              pVxMid->Pcoor[1] = yMid ;


              /*printf ( "\n" ) ;*/
            }
          }
        }
    }
  
  return ( 1 ) ;
}


/******************************************************************************

  count_refs:
  Get the number of new elements and the number of new vertices at the
  cell centers (quads! hexes!) of an unstructured grid marked for refinement.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------
  pUns:
  PmNewVerts:
  PmNewElems:
  PmNewElem2Verts:

  Changes To:
  -----------
  PmNewVerts:      incremented number of new vertices at the element centers,
  PmNewElems:      incremented number of new elements,
  PmNewElem2Verts: incremented number of new element to vertex pointers.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_count_uns_ref_elems ( uns_s *pUns,
				 int *PmNewVerts, int *PmNewElems,
				 int *PmNewElem2VertP, int *PmNewElem2ChildP ) {
  
  llEdge_s *pllAdEdge ;
  int mNewVx, mNewEl, mNewppVx ;
  chunk_struct *pChunk ;
  const refType_struct *pRefT ;
  elem_struct *pElem ;
  
  pllAdEdge = pUns->pllAdEdge ;
  mNewppVx = mNewEl = 0 ;
  
  /* Count the new vertices on edges and faces. */
  mNewVx = count_newVx_llAe ( pllAdEdge, pUns->pAdEdge ) ;

  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( pElem->term && pElem->PrefType ) {
        /* This is a refined element. */
	pRefT = pElem->PrefType ;

        /* Add all faces. This means adding each face vertex twice, once from
           each side. */
        mNewVx += pRefT->mFaceVerts ;
        /* A half-refined face can add a vertex if the refinement is crossed. */
        mNewVx += pRefT->mHalfRefQuadFc ;
        
	if ( pElem->PrefType->kElemVert )
	  /* Make a center vertex for this element. */
	  mNewVx++ ;

	/* Number of children. */
	mNewEl += pRefT->mChildren ;
        mNewppVx += pRefT->mChElem2Vert ;
      }
  
  *PmNewVerts = mNewVx ;
  *PmNewElems = *PmNewElem2ChildP = mNewEl ;
  *PmNewElem2VertP = mNewppVx ;
  return ( 1 ) ;
}



/******************************************************************************

  auh_find_ref:
  Given a set of marked edges on an element, find the refinement pattern that
  comprises them all.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

const refType_struct *auh_find_ref ( const elem_struct *pElem,
                                     unsigned int markdEdges, const int isotropy ) {
  
  const refType_struct *pRefT, *pRefZero ;
  const elemType_struct *pElT ;

  if ( !pElem )
    return ( NULL ) ;
  else if ( !markdEdges )
    return ( NULL ) ;

  pElT = elemType + pElem->elType ;
  pRefZero = pElT->PrefType ;

  if ( isotropy && markdEdges )
    /* Adapt isotropically. */
    markdEdges |= pElT->allEdges ; 
  
  for ( pRefT = pRefZero ;
        pRefT < pRefZero + pElT->mRefTypes ; pRefT++ )
    if ( ( markdEdges | pRefT->refEdges ) == pRefT->refEdges )
      /* This is the first type that contains all desired edges.
         Note that pElem->markdEdges only marks the desired edges, not 
         the imposed ones and the ones that the upgrade pRefT adds. */
      break ;

  return ( pRefT ) ;
}


/******************************************************************************

  auh_match_elem_fc:
  Add cross edges on quad faces if all the perimeter edges exist and the two
  midvertices exist that the cross edge spans.
  
  Last update:
  ------------
  12Dec19; rename to auh_
  21Jul00; intro half face fix to make aniso adapt work.
  : conceived.
  
  Input:
  ------
  pUns
  pElem

  Changes To:
  -----------
  *pChange: 1 if an edge has been added, unchanged otherwise.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int auh_match_elem_fc ( uns_s *pUns, elem_struct *pElem, int iso,
                    int *pChange ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxFc[2*MAX_VX_FACE+1], *pVxHFc[2*MAX_VX_FACE] ;
  int kFace, mVxFc, mVxBase, nFcAe[MAX_VX_FACE], nHFcAe[2*MAX_VX_FACE],
    nCrossAe[MAX_VX_FACE-1], nFixAe, fixDiagDir, nPerEg, newEg[2], kVx ;
  vrtx_struct *pVxCtr[2] ;
  

  for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
    pFoE = pElT->faceOfElem + kFace ;

    if ( pFoE->mVertsFace == 4 ) {
      get_face_aE ( pUns, pElem, kFace, &mVxBase, &mVxFc, pVxFc,
                    nFcAe, nCrossAe, &nFixAe, &fixDiagDir ) ;


      
      if ( nFcAe[0] && nFcAe[1] && nFcAe[2] && nFcAe[3] ) {
        /* This is a fully hanging quad face. Place the cross edges, if we can. */
        newEg[0] = newEg[1] = 0 ;
        if ( !nCrossAe[0] && pVxFc[1] && pVxFc[5] )
          nCrossAe[0] = add_adEdge_vrtx ( pUns, (VX*)pVxFc[1], (VX*)pVxFc[5],
                                          &nPerEg, newEg ) ;
        if ( !nCrossAe[1] && pVxFc[3] && pVxFc[7] )
          nCrossAe[1] = add_adEdge_vrtx ( pUns, (VX*)pVxFc[3], (VX*)pVxFc[7],
                                          &nPerEg, newEg+1 ) ;

        if ( ( !nCrossAe[0] && pVxFc[1] && pVxFc[5] ) ||
             ( !nCrossAe[1] && pVxFc[3] && pVxFc[7] ) ) {
          hip_err ( fatal, 0, "failed to add cross edges in auh_match_elem_fc." ) ;
          return ( 0 ) ;
        }
        else if ( newEg[0] || newEg[1] )
          *pChange = 1 ;

        if ( nCrossAe[0] && nCrossAe[1] ) {
          /* Are there already vertices? */
          pVxCtr[0] = de_cptVx( pUns, pUns->pAdEdge[ nCrossAe[0] ].cpVxMid ) ;
          pVxCtr[1] = de_cptVx( pUns, pUns->pAdEdge[ nCrossAe[1] ].cpVxMid ) ;
          
          if ( pVxCtr[0] && pVxCtr[1] && pVxCtr[0] != pVxCtr[1] ) {
            hip_err ( fatal, 0, "found inconsistent cross edges in auh_match_elem_fc.\n" ) ;
            return ( 0 ) ;
          }
          else if ( pVxCtr[0] )
            pUns->pAdEdge[ nCrossAe[1] ].cpVxMid = pVxCtr[0]->vxCpt ;
          else if ( pVxCtr[1] )
            pUns->pAdEdge[ nCrossAe[0] ].cpVxMid = pVxCtr[1]->vxCpt ;
        }
      }


      
      if ( pVxFc[8] && 0 ) {
        /* There is a center vertex. Check for fully hanging half faces. */
        get_face_half_aE ( pUns, 4, pVxFc, nHFcAe, pVxHFc ) ;

        for ( kVx = 0 ; kVx < 4 ; kVx++ )
          if ( pVxFc[ (2*kVx+1)%8 ] &&
               pVxHFc[ (2*kVx+2)%8 ] && pVxHFc[ (2*kVx+7)%8 ] ) {
            /* Add the cross edges. */
            nCrossAe[0] = add_adEdge_vrtx ( pUns,
                                            (VX*)pVxFc[ (2*kVx+1)%8 ], (VX*)pVxFc[8],
                                            &nPerEg, newEg ) ;
            nCrossAe[1] = add_adEdge_vrtx ( pUns,
                                            (VX*)pVxHFc[ (2*kVx+2)%8 ],
                                            (VX*)pVxHFc[ (2*kVx+7)%8 ],
                                            &nPerEg, newEg ) ;

            if ( nCrossAe[0] && nCrossAe[1] ) {
              /* Are there already vertices? */
              pVxCtr[0] = de_cptVx( pUns, pUns->pAdEdge[ nCrossAe[0] ].cpVxMid ) ;
              pVxCtr[1] = de_cptVx( pUns, pUns->pAdEdge[ nCrossAe[1] ].cpVxMid ) ;
              
              if ( pVxCtr[0] && pVxCtr[1] && pVxCtr[0] != pVxCtr[1] ) {
                hip_err ( fatal, 0, "found inconsistent cross edges in auh_match_elem_fc.\n" ) ;
                return ( 0 ) ;
              }
              else if ( pVxCtr[0] )
                pUns->pAdEdge[ nCrossAe[1] ].cpVxMid = pVxCtr[0]->vxCpt ;
              else if ( pVxCtr[1] )
                pUns->pAdEdge[ nCrossAe[0] ].cpVxMid = pVxCtr[1]->vxCpt ;
            }
          }
      }

    }
  }
  

  return ( 1 ) ;
}







/******************************************************************************

  auh_match_all_refs:
  Loop over all elements, compare the current pattern of marked edges to the
  possible combinations and upgrade to the next pattern that has all of
  the edges currently marked. 
  
  Last update:
  ------------
  12Dec19; rename to auh_
  11Sep19; add check for neg vols/ctr vx resulting from buffering.
  : conceived.
  
  Input:
  ------
  pUns:
  PmSweeps:
  iso:       1 if matching is to
              lead to isotropic refinement of the neighbor.

  Changes To:
  -----------
  ProotChunk->Pelem[].markdEdges: The bitmap of required edges to be refined.
              pElem[].PrefType: The matching refinement pattern.
  
  Returns:
  --------
  0 on no changes to propagate, 1 on changes.
  
*/

int auh_match_all_refs ( uns_s *pUns, int iso, int *pmSweeps ) {

  chunk_struct *pChunk ;
  elem_struct *pElem ;
  int change = 0 ;

  
  if ( !(*pmSweeps) )
    /* This is the first sweep. Reset the pattern of already refined edges. */
    for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) 
      /* Loop over all leaves. */
      for ( pElem = pChunk->Pelem+1 ;
	    pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
	if ( pElem->term ) {
	  /* Mask the edges that don't exist in this element. */
	  pElem->refdEdges = 
            pElem->markdEdges &= elemType[pElem->elType].allEdges ;
	}
  
  (*pmSweeps)++ ;
  if ( verbosity > 5 && *pmSweeps > 1 ) {
    sprintf ( hip_msg, "%d sweep of auh_match_all_refs.\n", *pmSweeps ) ;
    hip_err ( info, 5, hip_msg ) ;
  }


  
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) 
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) {
      
      if ( pElem->term )
        /* Add all edges needed by the current refinement pattern. */
	adapt_uh_match_elem_eg ( pUns, pElem, iso, &change ) ;

      // JDM, 11Sep19: this is odd, why not also on the ->term condition?
      if ( !pElem->invalid )
        /* Add all cross edges needed by the current edge constellation. */
	auh_match_elem_fc ( pUns, pElem, iso, &change ) ;
    }

  /* Upgrade negative volumes/added central vx to refinement. */
  const int doRefNegVol = 1, doRefCtrVx = 0 ;
  buf2ref_vol_ctr ( pUns, doRefNegVol, doRefCtrVx, &change) ;


  return ( change ) ;
}


/* **********************************************************************

PUBLIC
 */



/******************************************************************************

  auh_place_vx:
  Place a vertex given a bunch of vertices to average.
  
  Last update:
  ------------
  23jul0; increment pointer in list of uknknowns only by mEq, not mEq+mDim+1.
  12Dec19; rename to adapt_uh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

vrtx_struct *adapt_uh_place_vx ( const uns_s *pUns, chunk_struct *pChunk,
                        vrtx_struct **ppLstVx, double **ppLstCoor,
                        double **ppLstUnknown,
                        vrtx_struct *pVrtx[], const int mVxToAvg ) {
  
  vrtx_struct *pLstVx, *pVxMax ;
  double *pLstCoor, *pLstUnknown ;
  int mDim, mUnknowns, nDim, nUnknown, iVx, nNew ;

  mDim = pUns->mDim, mUnknowns = pUns->varList.mUnknowns ;

  /* Point to the next open storage. */
  pLstVx = ++(*ppLstVx) ;
  pLstCoor = *ppLstCoor += mDim ;

  // Where would this be turned on?
  //# ifdef CHECK_BOUNDS
    if ( pLstVx > pChunk->Pvrtx + pChunk->mVerts ) {
      sprintf ( hip_msg, "beyond bounds of pChunk->Pvrtx (%"FMT_ULG",%"FMT_ULG")"
                " in auh_place_vx.\n",
	        (pLstVx - pChunk->Pvrtx), pChunk->mVerts ) ;
      hip_err ( fatal, 0, hip_msg ) ; 
    }
    if ( pLstCoor > pChunk->Pcoor + pChunk->mVerts*mDim ) {
      sprintf ( hip_msg, "beyond bounds of pChunk->Pcoor (%"FMT_ULG",%"FMT_ULG")"
                " in auh_place_vx.\n",
	        (pLstCoor - pChunk->Pcoor), mDim*pChunk->mVerts ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    //# endif

  /* Average the coordinates. */
  pLstVx->Pcoor = pLstCoor ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    pLstVx->Pcoor[nDim] = 0. ;
    for ( iVx = 0 ; iVx < mVxToAvg ; iVx++ )
      pLstVx->Pcoor[nDim] += pVrtx[iVx]->Pcoor[nDim] ;
    pLstVx->Pcoor[nDim] /= mVxToAvg ;
  }

  /* mg-adapt fix. Trace a parent node for hydra style node-->mg_node. */
  for ( pVxMax = NULL, iVx = 0 ; iVx < mVxToAvg ; iVx++ )
    /* Use only parents from the initial mesh. */
    if ( pVrtx[iVx]->vxCpt.nCh == 0 )
      pVxMax = MAX( pVxMax, pVrtx[iVx] ) ;

  nNew = *ppLstVx - pChunk->Pvrtx ; 
  if ( !pVxMax ) {
    if ( verbosity > 1 ) {
      sprintf ( hip_msg, "could not find a coarse grid parent for (%d,%d)"
               " in auh_place_vx.\n", nNew, pChunk->nr ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  else
    pChunk->pVxCpt[nNew] = pVxMax->vxCpt ; 
  

  if ( pChunk->Punknown ) {
    /* Average the unknowns. */
    pLstUnknown = *ppLstUnknown += mUnknowns ; // why? +mDim+1 ;
#   ifdef CHECK_BOUNDS
      if ( pLstUnknown > 
           pChunk->Punknown + (pChunk->mVerts+1)*(mUnknowns // why> +mDim+1
                                                  )) {
        sprintf ( hip_msg, "beyond bounds of pChunk->PUnknown (%d, %d*%d)"
		 " in auh_place_vx.\n",
		 (int)(pLstUnknown - pChunk->Punknown), 
                  pChunk->mVerts,mUnknowns //+mDim
                  ) ;
	hip_err ( fatal, 0, hip_msg ) ; }
#   endif

    pLstVx->Punknown = pLstUnknown ;
    for ( nUnknown = 0 ; nUnknown < mUnknowns ; nUnknown++ ) {
      pLstVx->Punknown[nUnknown] = 0. ;
      for ( iVx = 0 ; iVx < mVxToAvg ; iVx++ )
	pLstVx->Punknown[nUnknown] += pVrtx[iVx]->Punknown[nUnknown] ;
      pLstVx->Punknown[nUnknown] /= mVxToAvg ;
    }
  }

  pLstVx->number = pLstVx - pChunk->Pvrtx + pUns->nHighestVertNumber ;
  pLstVx->vxCpt.nr = pLstVx - pChunk->Pvrtx ;
  pLstVx->vxCpt.nCh = pChunk->nr ;
  pLstVx->mark = pLstVx->mark2 = pLstVx->per = pLstVx->singular = 0 ;
  
  return ( pLstVx ) ;
}

/******************************************************************************
public
*******************************************************************************/

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  add_elem_crossFc:
*/
/*! add cross faces for each of the faces of an elem that have 4 hanging nodes.
 *
 *
 */

/*
  
  Last update:
  ------------
  27Apr20; size nCrossAe to MAX_VX_FACE-1, as used in called functions.
  15Apr20: extracted from  auh_fix_crossFc

  Input:
  ------
  pUns: grid
  pElem: element to add for
  pRefChunk: chunk to add the refined elements to. Could be an extended
  rootChunk or a newly allocated one (deprecated option.)

  Changes To:
  -----------
  ppLstVx: last vertex slot used previously,
  ppLstCoor: last coor slot used previously,
  ppLstUnknown: last unknown slot used previously,
    
  Returns:
  --------
  number of added vx.
  
*/

int add_elem_crossFc ( uns_s *pUns, const elem_struct *pElem,
                       chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                       double **ppLstCoor, double **ppLstUnknown,
                       const int doBuf ) {
  ret_s ret = ret_success () ;
            
  const elemType_struct *pElT = elemType + pElem->elType ;
  const vrtx_struct *pVxFace[2*MAX_VX_FACE+1] ;
  const faceOfElem_struct *pFoE ;
  int mVxBase, nFcAe[MAX_VX_FACE], mVxFc, nCrossAe[MAX_VX_FACE-1], nFixAe, fixDir, 
    nFace, mVxAdded = 0 ;

  for ( nFace = 1 ; nFace <= pElT->mFaces ; nFace++ ) {
    pFoE = pElT->faceOfElem + nFace ;
          
    if ( pFoE->mVertsFace == 4 ) {
      /* Only quad faces. */
      nFixAe = 0 ;
 
      /* Find all edges on and around this face. */
      get_face_aE ( pUns, pElem, nFace,
                    &mVxBase, &mVxFc, pVxFace, nFcAe, nCrossAe,
                    &nFixAe, &fixDir ) ;

      if ( pVxFace[1] && pVxFace[3] && pVxFace[5] && pVxFace[7]  ) {
        /* All mid-vertices around the quad face exist. */
        mVxAdded += auh_add_cross_aE ( pUns, pVxFace,
                                       pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown,
                                       doBuf ) ;
      }
    }
  }

  return ( mVxAdded ) ;
}





/******************************************************************************

  adapt_uh_place_vx_elem:
  Place a vertex at the center of an element.
  
  Last update:
  ------------
  12Dec19; rename to adapt_uh_
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

vrtx_struct *adapt_uh_place_vx_elem ( const elem_struct *pElem, 
                             const uns_s *pUns, chunk_struct *pChunk,
                             vrtx_struct **ppLstVx, double **ppLstCoor,
                             double **ppLstUnknown ) {
  
  vrtx_struct *pVrtx[MAX_VX_ELEM] ;
  const int mVerts = elemType[pElem->elType].mVerts ;
  int kVx ;

  /* Make a consecutive list of vertices. */
  for ( kVx = 0 ; kVx < mVerts ; kVx++ )
    pVrtx[kVx] = pElem->PPvrtx[kVx] ;

  return ( adapt_uh_place_vx ( pUns, pChunk,
                               ppLstVx, ppLstCoor, ppLstUnknown, pVrtx, mVerts ) ) ;
}




/******************************************************************************

  adapt_uh_match_elem_eg:
  For a given element, select a refinement pattern that is compatible with
  the markings of the sensor and the imposed edges
  
  Last update:
  ------------
  12Dec19; rename to adapt_uh_
  17Mar00; convert to auh_match_elem_eg.
  Oct97: conceived.
  
  Input:
  ------
  pllAdEdge:  List of adapted edges/faces,
  pElem:      The element to find a refinement pattern for,
  iso:        if set to 1, all refinements are done isotropically.
  pChange:   

  Changes To:
  -----------
  pllAdEdge: New edges, changes.
  pElem:     New refinement pattern pElem->PrefType ;
  pChange:   set to 1 if new imposed edges have been created, which makes another
             sweep necessary.
  
  Returns:
  --------
  1.
  
*/

int adapt_uh_match_elem_eg ( uns_s *pUns, elem_struct *pElem, int iso,
                           int *pChange ) {
  
  const elemType_struct *pElT = elemType + pElem->elType ;
  const refType_struct *pRefT ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxEdge[5], *pVxFc[2*MAX_VX_FACE+1] ;
  const vrtx_struct *pVrtx1, *pVrtx2 ;
  unsigned int fixedFc, refdEdges ;
  int kFace, kEdge, dir, nAe[MAX_EDGES_ELEM], 
    mVxFc, nChAe[2], mVxBase, nAe0, nAe2, nPerEg, newEg = 0, 
    nFcAe[MAX_VX_FACE], nCrossAe[MAX_VX_FACE-1], nFixAe, fixDiagDir, newFixFc ;
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  

  /* Add the imposed edges. Imposed edges are edges that have children. */
  for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
    if ( ( nAe[kEdge] = get_elem_edge ( pllAdEdge, pElem, kEdge,
                                        &pVrtx1, &pVrtx2, &dir ) ) ) {
      /* The edge exists. */
      if ( get_child_aE ( pUns, pllAdEdge, pUns->pAdEdge,
                          nAe[kEdge], nChAe, pVxEdge ) )
        /* This edge is imposed. */
        pElem->refdEdges |= bitEdge[kEdge] ;
    }
    else if ( bitEdge[kEdge] & pElem->refdEdges ) {
      /* The edge is needed and not existent. Add it. */
      if ( !( nAe[kEdge] = add_adEdge_elem ( pUns, pElem, kEdge, &nPerEg, &newEg ) ) ) {
        hip_err ( fatal,0,"failed to add an edge in auh_match_elem_eg.\n" ) ;
      }
      *pChange = 1 ;
    }


  
  /* Loop over all faces to check for children on the face. Note that faces
     only exist in 3D. */
  for ( newFixFc = 0, kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
    get_face_aE ( pUns, pElem, kFace,
                  &mVxBase, &mVxFc, pVxFc,
                  nFcAe, nCrossAe, &nFixAe, &fixDiagDir ) ;
    pFoE = elemType[ pElem->elType ].faceOfElem + kFace ;

    if ( nFixAe )
      /* Enter the loop for fixed diagonals. */
      newFixFc = 1 ; 

    if ( mVxBase == 3 ) {
      /* Check whether the center facet formed by the three midvertices is refined.
         For the time being, triangular faces are refined isotropically, thus checking
         on one edge with children is enough. */
      if ( ( nAe0 = nAe[ pFoE->kFcEdge[0] ] ) &&
           ( nAe2 = nAe[ pFoE->kFcEdge[2] ] ) &&
           ( pVrtx1 = de_cptVx ( pUns, pUns->pAdEdge[nAe0].cpVxMid ) ) &&
           ( pVrtx2 = de_cptVx ( pUns, pUns->pAdEdge[nAe2].cpVxMid ) ) &&
           get_edge_vrtx ( pUns->pllAdEdge, &pVrtx1, &pVrtx2, &dir ) ) {
        /* There is an edge on the face. Force full refinement. */
        pElem->refdEdges |= pFoE->bitEdgeFace ;
      }
    }
  }
  
  /* We have to have all the edges marked for the fixed diagonal check. */
  if ( pElem->refdEdges ) {
    pRefT = auh_find_ref ( pElem, pElem->refdEdges, iso ) ;
    pElem->refdEdges |= pRefT->refEdges ;
  }
  
      
  /* Loop over all faces to check for fixed diagonals. The outer loop takes care of
     propagation around the faces in case there are several fixed ones. */
  for ( fixedFc = 0 ; newFixFc ; )
    for ( newFixFc = 0, kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
      get_face_aE ( pUns, pElem, kFace, &mVxBase, &mVxFc, pVxFc,
                    nFcAe, nCrossAe, &nFixAe, &fixDiagDir ) ;
      pFoE = elemType[ pElem->elType ].faceOfElem + kFace ;
      if ( nFixAe && ( pElem->refdEdges & pFoE->bitEdgeFace )
           && !( fixedFc & bitEdge[kFace] ) ) {
        /* On this face, there is at least one edge marked and a fixed diagonal.
           Mark full refinement of the face. */
        pElem->refdEdges |= pFoE->bitEdgeFace ;
        newFixFc = 1;
        fixedFc |= bitEdge[kFace] ;
        break ;
      }
    }


  
  /* Find a containing reftype. */
  pRefT = pElem->PrefType ;
  refdEdges = ( pRefT ? pRefT->refEdges : 0 ) ;
  
  if ( refdEdges != ( refdEdges | pElem->refdEdges ) ) {
    /* Not all of the imposed or marked edges are refined in this element.
       More refinement needed. Upgrade to the next containing pattern. */
    pRefT = pElem->PrefType = auh_find_ref ( pElem, pElem->refdEdges, iso ) ;
    pElem->refdEdges |= pRefT->refEdges ;
    
    /* Make all necessary adEdges. Loop over all edges. */
    for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
      if ( bitEdge[kEdge] & pRefT->refEdges ) {
        /* This edge is to be refined. Make sure it exists. */
        pElem->refdEdges |= bitEdge[kEdge] ;
        if ( ! nAe[kEdge]  &&
             !( nAe[kEdge] =
                add_adEdge_elem ( pUns, pElem, kEdge, &nPerEg, &newEg ) ) ) {
          hip_err ( fatal, 0, "failed to add an edge in auh_match_all_refs." ) ;
          return ( 0 ) ;
        }
        else if ( newEg )
          *pChange = 1 ;
      }
  }


  /* Upgrade to isotropy, if it is cheap enough. E.g., do a full
     refinement on a tri if two edges are already refined. */
  if ( Grids.adapt.upRef < 1. ) {
    int mExEg = 0 ;
    const refType_struct *pUpRefT = pElT->PrefType + pElT->mRefTypes-1 ;

    if ( pElem->PrefType != pUpRefT ) {
      /* How many edges of this pattern are present. */
      for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
        if ( pUpRefT->vxOnEdge[kEdge] && nAe[kEdge] )
          mExEg++ ;
      
      if ( ( 1.*mExEg )/pUpRefT->mEdgeVerts > Grids.adapt.upRef )
        /* Cheap upgrade. */
        pRefT = pUpRefT ;
      
      
      if ( pRefT ) {
        pElem->PrefType = pRefT ;
        pElem->refdEdges |= pRefT->refEdges ;
        
        /* Make all necessary adEdges. Loop over all edges. */
        for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
          if ( bitEdge[kEdge] & pRefT->refEdges ) {
            /* This edge is to be refined. Make sure it exists. */
            pElem->refdEdges |= bitEdge[kEdge] ;
            if ( ! nAe[kEdge]  &&
                 !( nAe[kEdge] =
                    add_adEdge_elem ( pUns, pElem, kEdge, &nPerEg, &newEg ) ) ) {
              hip_err ( fatal, 0, "failed to add an edge in auh_match_all_refs.\n" ) ;
              return ( 0 ) ;
            }
            else if ( newEg )
              *pChange = 1 ;
          }
      }
    }
  }
    
    
  return ( 1 ) ;
}



/******************************************************************************

  adapt_uns_hierarchical:
  Adapt an unstructured grid hierarchically by one level. Loop over 
  all cells and refine marked ones.
  
  Last update:
  ------------
  27Apr20; dealloc pPerVxBc, ndxPerVxBc 
  12Dec19; rename to adapt_uns_hierarchical
  9May19; revert to append_chunk, rather than extend_chunk.
  8Jan15; reset hMin after adaptation.
  : conceived.
  
  Input:
  ------
  pUns:
  iso:  if set to nonzero, adapt isotropically including all propagation.

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int adapt_uns_hierarchical ( uns_s *pUns, const int iso ) {
  
  int mNewVerts, mNewElems, mNewElem2VertP, mNewElem2ChildP, mSweeps = 0,
    mOldElems, mOldVerts, mDerefdElems, mDerefdVerts, 
    mPerVxBc[MAX_PER_PATCH_PAIRS] ;
  chunk_struct *PrefChunk = NULL, *pChunk ;
  perVx_s *pPerVxBc[MAX_PER_PATCH_PAIRS];
  ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS] ;




  
  /* Establish periodic vertex equivalencies. */
  check_bnd_setup ( pUns ) ;
  arr_free ( pUns->pPerVxPair ) ;

  if ( pUns->pPerBc ) {

    if ( !make_perVxPairs ( pUns, pPerVxBc, ndxPerVxBc, mPerVxBc ) )
      hip_err ( fatal, 0, "could not establish periodic vertex pairs"
               " in adapt_uns_hierarchical." ) ;

    /* Make a duplicate list of vertex entries with everything 
       appearing on In. */
    mult_per_vert ( pUns, mPerVxBc, pPerVxBc, ndxPerVxBc, 2 ) ;

    /* Make sure all existing edges have their periodic equivalent. */
    if ( !match_per_aE ( pUns ) )
      hip_err ( fatal, 0, "periodic failure in adapt_uns_hierarchical." ) ;
  }


  /* Make sure our NACA 0012 matches.
  auh_match_naca0012 ( pUns ) ; */

  
  mNewVerts = mNewElems = mNewElem2VertP = mNewElem2ChildP = mSweeps = 0 ;
  mOldElems = mOldVerts = mDerefdElems = mDerefdVerts = 0 ;

  
  /* Try to find a better way later. Debuffer completely to prevent 
     derefinement from creating hanging edges that are still buffered. 
     Fix this. */
  debuffer_uns ( pUns ) ;

  
  /* Count the elements and vertices in the current grid. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    mOldElems += pChunk->mElems ;
    mOldVerts += pChunk->mVerts ;
  }
  pUns->nHighestElemNumber = mOldVerts ;
  pUns->nHighestVertNumber = mOldVerts ;

  /* Initialize or resize the edge datastructure. */
  if ( !( *( ( llEdge_s ** ) &pUns->pllAdEdge ) =
          make_llEdge ( pUns, CP_NULL, 0, sizeof( adEdge_s ),
                        pUns->pllAdEdge, ( void ** ) &pUns->pAdEdge ) ) )
    hip_err ( fatal, 0, "could not create the edge list in refine_uns." ) ;

  
  
  /* Mark all cells with the lowest possible refinement pattern. */
  /* Upgrade the patterns until there is no more change. */
  while ( auh_match_all_refs ( pUns, iso, &mSweeps ) )
    ;

  
  /* Count the new elems and vertices. */
  auh_count_uns_ref_elems ( pUns, &mNewVerts, &mNewElems,
		        &mNewElem2VertP, &mNewElem2ChildP ) ;

  if ( mNewVerts || mNewElems ) {
    /* Append a new chunk. Note that auh_fill_uns_ref counts, allocates and
       fills the boundary faces. */
    // Replace append with extend for parallel_refine embedded lib,
    // but since parallel refine is not active/validated, return to append.
    // PrefChunk = extend_chunk ( pUns, pUns->mDim, 
     PrefChunk = append_chunk ( pUns, pUns->mDim, 
                               mNewElems, mNewElem2VertP, mNewElem2ChildP,
                               mNewVerts, 0, 0 ) ;
    PrefChunk->pVxCpt = arr_calloc ( "pVxCpt in refine_uns", pUns->pFam,
                                     mNewVerts+1, sizeof (cpt_s ) ) ;
  }



  
  /* Fill the elements and verts of the refined level. */
  if ( !auh_fill_uns_ref ( pUns, PrefChunk ) )
    hip_err ( fatal, 0, "refinement of unstructured grid failed"
              " in refine_uns." ) ;

  
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "refinement added %d elements, %d vertices, %d sweeps.",
	     mNewElems, mNewVerts, mSweeps-1 ) ;
    hip_err ( info, 3, hip_msg ) ;
  }


  
  /* Count and number. */
  pUns->numberedType = noNum ;
  number_uns_grid ( pUns ) ;

  /* The minimum distance can change to anything, just think about a highly
     stretched tet that is adapted isotropically. */
  pUns->epsOverlap /= 2 ;
  pUns->epsOverlapSq /= 4 ; 


  /* Redo the list of periodic pairs. Include all newly added periodic
     vertices for the edge cleaning. */
  if ( pUns->pPerBc ) {
    if ( !make_perVxPairs ( pUns, pPerVxBc, ndxPerVxBc, mPerVxBc ) )
      hip_err ( fatal, 0, "could not establish periodic vertex pairs"
               " in match_per_in_all_edges." );

    /* Make a duplicate list of vertex entries with everything appearing on In. */
    mult_per_vert ( pUns, mPerVxBc, pPerVxBc, ndxPerVxBc, 2 ) ;

    int nBc ;
    for ( nBc = 0 ; nBc < pUns->mPerBcPairs ; nBc++ ) {
      arr_free ( pPerVxBc[nBc] ) ;
      arr_free ( ndxPerVxBc[2*nBc  ] ) ;
      arr_free ( ndxPerVxBc[2*nBc+1] ) ;
    }
  }


  /* Remove all non-hanging edges.
  clean_uns_adEdge ( pUns, pUns->pllAdEdge, pUns->pAdEdge ) ; */
# ifdef DEBUG
  int bcGeoType_change
  check_conn ( pUns, &bcGeoType_change ) ;
# endif

  
  /* Derefine all elements which have all children marked for derefinement.
     Do this after cleaning out any obsolete edges which might block derefinement. */
  auh_deref_uns_hierarchical ( pUns, &mDerefdElems, &mDerefdVerts ) ;
  if ( mDerefdElems ) {
    sprintf ( hip_msg, "derefinement removed %d elements.", mDerefdElems ) ;
    hip_err ( info, 3, hip_msg ) ;
  }
  /* Remove all non-hanging edges. */
  clean_uns_adEdge ( pUns, pUns->pllAdEdge, pUns->pAdEdge ) ;
# ifdef DEBUG
  check_conn ( pUns, &bcGeoType_change ) ;
# endif

  /* Count and number. */
  arr_free ( pUns->pPerVxPair ) ;
  pUns->mPerVxPairs = 0 ; 
  pUns->pPerVxPair = NULL ; 
  pUns->numberedType = noNum ;
  number_uns_grid ( pUns ) ;

  // Recompute epsOverlap from scratch.
  pUns->epsOverlap = pUns->epsOverlapSq = 0.0 ;
  check_uns ( pUns, check_lvl ) ;

  Grids.epsOverlap = .9*pUns->hMin ;
  Grids.epsOverlapSq = Grids.epsOverlap*Grids.epsOverlap ;

  sprintf ( hip_msg, "adapted grid to %"FMT_ULG" elements,"
            " %"FMT_ULG" vertices.",
            pUns->mElemsNumbered, pUns->mVertsNumbered ) ;
  hip_err ( info, 2, hip_msg ) ;
            
  
  return ( 1 ) ;
}



