/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  bye.c:
*/
/*! poetry output
 *
 */

/*
  
  Last update:
  ------------
  10Sep19: extracted from hipc.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/
#include "cpre.h"
#include "proto.h"

ret_s bye (  ) {
  ret_s ret = ret_success () ;

  /* 20.07, Louise Gluck winning Nobel Lit */
  printf ( "\n"
           "    All Hallows\n"
           "\n"
           "    Even now this landscape is assembling. \n"
           "    The hills darken. The oxen \n"
           "    Sleep in their blue yoke, \n"
           "    The fields having been \n"
           "    Picked clean, the sheaves \n"
           "    Bound evenly and piled at the roadside \n"
           "    Among cinquefoil, as the toothed moon rises: \n"
           "     \n"
           "    This is the barrenness \n"
           "    Of harvest or pestilence \n"
           "    And the wife leaning out the window \n"
           "    With her hand extended, as in payment, \n"
           "    And the seeds \n"
           "    Distinct, gold, calling \n"
           "    Come here \n"
           "    Come here, little one \n"
           "     \n"
           "    And the soul creeps out of the tree.  \n"
           "\n"
           "    Louise Glück\n"
           "\n\n"
           ) ;

  return ( ret ) ;
}

  /* not used yet.
  printf ( "\n"
           "\n"
           "Excerpt from ``First Memory''\n"
           "\n"
           " From the beginning of time \n"
           " In childhood, I thought \n"
           " That pain meant \n"
           " I was not loved. \n"
           " It meant I loved. \n"
           "\n"
           "Louise Glück"
           "\n\n"
           ) ;


  */

/* 20.07, on pandemics
  printf ( "\n"
           " SONNET 65\n\n"
           " Since brass, nor stone, nor earth, nor boundless sea,\n"
           " But sad mortality o'er-sways their power,\n"
           " How with this rage shall beauty hold a plea,\n"
           " Whose action is no stronger than a flower?\n"
           " O, how shall summer's honey breath hold out\n"
           " Against the wreckful siege of battering days,\n"
           " When rocks impregnable are not so stout,\n"
           " Nor gates of steel so strong, but Time decays?\n"
           " O fearful meditation! where, alack,\n"
           " Shall Time's best jewel from Time's chest lie hid?\n"
           " Or what strong hand can hold his swift foot back?\n"
           " Or who his spoil of beauty can forbid?\n"
           "    O, none, unless this miracle have might,\n"
           "    That in black ink my love may still shine bright.\n"
           "\n"
           "   William Shakespeare\n"
           "\n\n"
           ) ;


  return ( ret ) ;
}
*/

/*
  20.04


  printf ( "\n"
           " Double, double toil and trouble  \n"
           "\n"
           " Double, double toil and trouble;\n"
           " Fire burn and caldron bubble.\n"
           " Fillet of a fenny snake,\n"
           " In the caldron boil and bake;\n"
           " Eye of newt and toe of frog,\n"
           " Wool of bat and tongue of dog,\n"
           " Adder's fork and blind-worm's sting,\n"
           " Lizard's leg and howlet's wing,\n"
           " For a charm of powerful trouble,\n"
           " Like a hell-broth boil and bubble.\n"
           "\n"
           " Double, double toil and trouble;\n"
           " Fire burn and caldron bubble.\n"
           " Cool it with a baboon's blood,\n"
           " Then the charm is firm and good.\n"
           "     \n"
           "   Song of the Witches from Macbeth IV.i 10-19; 35-38\n"
           "   William Shakespeare\n"
           "\n"
           ) ;



  19.12
  printf ( "\n"
           "   On Brexit\n"
           "\n"
           "   You do look, my son, in a moved sort,\n"
           "   As if you were dismay'd: be cheerful, sir.\n"
           "   Our revels now are ended. These our actors,\n"
           "   As I foretold you, were all spirits and\n"
           "   Are melted into air, into thin air:\n"
           "   And, like the baseless fabric of this vision,\n"
           "   The cloud-capp'd towers, the gorgeous palaces,\n"
           "   The solemn temples, the great globe itself,\n"
           "   Ye all which it inherit, shall dissolve\n"
           "   And, like this insubstantial pageant faded,\n"
           "   Leave not a rack behind. We are such stuff\n"
           "   As dreams are made on, and our little life\n"
           "   Is rounded with a sleep. Sir, I am vex'd;\n"
           "   Bear with my weakness; my, brain is troubled:\n"
           "   Be not disturb'd with my infirmity:\n"
           "   If you be pleased, retire into my cell\n"
           "   And there repose: a turn or two I'll walk,\n"
           "   To still my beating mind.  \n"
           "     \n"
           "   The Tempest, Act IV, Scene I\n"
           "   William Shakespeare\n"
           ) ;
*/

/* 19.09
  printf ( "\n"
           "Elm\n"
           "By Sylvia Plath\n"
           "\n"
           "For Ruth Fainlight\n"
           "\n\n"
           "I know the bottom, she says. I know it with my great tap root:   \n"
           "It is what you fear.\n"
           "I do not fear it: I have been there.\n"
           "\n"
           "Is it the sea you hear in me,   \n"
           "Its dissatisfactions?\n"
           "Or the voice of nothing, that was your madness?\n"
           "\n"
           "Love is a shadow.\n"
           "How you lie and cry after it\n"
           "Listen: these are its hooves: it has gone off, like a horse.\n"
           "\n"
           "All night I shall gallop thus, impetuously,\n"
           "Till your head is a stone, your pillow a little turf,   \n"
           "Echoing, echoing.\n"
           "\n"
           "Or shall I bring you the sound of poisons?   \n"
           "This is rain now, this big hush.\n"
           "And this is the fruit of it: tin-white, like arsenic.\n"
           "\n"
           "I have suffered the atrocity of sunsets.   \n"
           "Scorched to the root\n"
           "My red filaments burn and stand, a hand of wires.\n"
           "\n"
           "Now I break up in pieces that fly about like clubs.   \n"
           "A wind of such violence\n"
           "Will tolerate no bystanding: I must shriek.\n"
           "\n"
           "The moon, also, is merciless: she would drag me   \n"
           "Cruelly, being barren.\n"
           "Her radiance scathes me. Or perhaps I have caught her.\n"
           "\n"
           "I let her go. I let her go\n"
           "Diminished and flat, as after radical surgery.   \n"
           "How your bad dreams possess and endow me.\n"
           "\n"
           "I am inhabited by a cry.   \n"
           "Nightly it flaps out\n"
           "Looking, with its hooks, for something to love.\n"
           "\n"
           "I am terrified by this dark thing   \n"
           "That sleeps in me;\n"
           "All day I feel its soft, feathery turnings, its malignity.\n"
           "\n"
           "Clouds pass and disperse.\n"
           "Are those the faces of love, those pale irretrievables?   \n"
           "Is it for such I agitate my heart?\n"
           "\n"
           "I am incapable of more knowledge.   \n"
           "What is this, this face\n"
           "So murderous in its strangle of branches?--\n"
           "\n"
           "Its snaky acids hiss.\n"
           "It petrifies the will. These are the isolate, slow faults\n"
           "That kill, that kill, that kill.\n"
           "\n" ) ;

 19.07

               "   Tiefe Sehnsucht\n"
               "   \n"
               "   Maienkätzchen, erster Gruß,\n"
               "   ich breche euch und stecke euch\n" 
               "   an meinen alten Hut.\n"
               "   Maienkätzchen, erster Gruß,\n"
               "   einst brach ich euch und steckte euch\n"
               "   der Liebsten an den Hut.\n"
               "   \n"
               "   Detlev von Liliencron (1844-1909)\n"

 */


/* up to 18.03: 

      printf ( "\n"
               " Summer night by Kobayashi Issa\n"
               " Summer night--\n"
               " even the stars\n"
               " are whispering to each other.\n"
               "\n" ) ;
*/


/* 17.03, never deployed. 
              "\n"
               "                  Snowdrop\n"
               "        \n"
               "        Now is the globe shrunk tight\n"
               "        Round the mouse’s dulled wintering heart.\n"
               "        Weasel and crow, as if moulded in brass,\n"
               "        Move through an outer darkness\n"
               "        Not in their right minds,\n"
               "        With the other deaths. She, too, pursues her ends,\n"
               "        Brutal as the stars of this month,\n"
               "        Her pale head heavy as metal.\n"
               "        \n"
               "                                 Ted Hughes\n"
 
 */

  /* 17.01 Carpinus betulus.
               " Jeder weiß, was so ein Maikäfer\n"
                " für ein Vogel sei.\n"
                " In den Bäumen hin und her\n"
                " Fliegt und kriecht und krabbelt er.\n"
                " \n"
                " Max und Moritz, immer munter,\n"
                " Schütteln sie vom Baum herunter.\n"
                " In die Tüte von Papiere\n"
                " Sperren sie die Krabbeltiere.\n"
                " \n"
                " Fort damit und in die Ecke\n"
                " Unter Onkel Fritzens Decke!\n"
                " Bald zu Bett geht Onkel Fritze\n"
                " In der spitzen Zippelmütze;\n"
                " \n"
                " Seine Augen macht er zu,\n"
                " Hüllt sich ein und schläft in Ruh.\n"
                " Doch die Käfer, kritze, kratze!\n"
                " Kommen schnell aus der Matratze.\n"
                " \n"
                " Schon faßt einer, der voran,\n"
                " Onkel Fritzens Nase an.\n"
                " \"Bau!\" schreit er. \"Was ist das hier?\"\n"
                " Und erfaßt das Ungetier.\n"
                " \n"
                " Und den Onkel, voller Grausen,\n"
                " Sieht man aus dem Bette sausen.\n"
                " \"Autsch!\" - Schon wieder hat er einen\n"
                " Im Genicke, an den Beinen;\n"
                " \n"
                " Hin und her und rundherum\n"
                " Kriecht es, fliegt es mit Gebrumm.\n"
                " Onkel Fritz, in dieser Not,\n"
                " Haut und trampelt alles tot\n"
                " \n"
                " Guckste wohl, jetzt ist's vorbei\n"
                " Mit der Käferkrabbelei!\n"
                " Onkel Fritz hat wieder Ruh\n"
                " Und macht seine Augen zu.\n"
                "\n"
               "                 Wilhelm Busch, Max und Moritz\n"
  
   */


              /* 1.48. Deployed?
              "\n"
              "       January\n"
              "       \n"
              "       The days are short,\n"
              "         The sun a spark\n"
              "       Hung thin between\n"
              "         The dark and dark.\n"
              "       \n"
              "                     John Updike\n"
              "\n" ) ;
              */

              /* 1.46:
              "\n"
              "Rilke, Herbsttag\n"
              "\n"
              "Herr: es ist Zeit. Der Sommer war sehr groß.\n"
              "Leg deinen Schatten auf die Sonnenuhren,\n"
              "und auf den Fluren laß die Winde los.\n"
              "\n"
              "Befiel den letzten Früchten voll zu sein;\n"
              "gib ihnen noch zwei südlichere Tage,\n"
              "dränge sie zur Vollendung hin und jage\n"
              "die letzte Süße in den schweren Wein.\n"
              "\n"
              "Wer jetzt kein Haus hat, baut sich keines mehr.\n"
              "Wer jetzt allein ist, wird Es lange bleiben,\n"
              "wird wachen, lesen, lange Briefe schreiben\n"
              "und wird in den Alleen hin und her\n"
              "unruhig wandern, wenn die Blätter treiben. \n"
              "\n" ) ;
              */

      /* 1.45, never deployed.

              "  Mother, Summer, I \n"
              "              Philip Larkin\n"
*/

               /* 1.44

              "   \n\n"
              "  Quelque Parfum\n"
              "  \n"
              "  Malgré le ciel encore bas\n"
              "  et cet air qui chancelle,\n"
              "  quelque chose nouvelle\n"
              "  flotte vers l'odorat.\n"
              "  \n"
              "  Quelque parfum tout vert\n"
              "  discrètement se dégage.\n"
              "  Un plaisir déménage:\n"
              "  le printemps est ouvert.\n"
              "\n"
              "  - Rainer Maria Rilke -\n"

              "\n");
               */
               /* 1.42-43 
              "   \n\n"
               "    this is the garden:colours come and go,\n"
               "   \n"
               "    this is the garden:colours come and go,\n"
               "    frail azures fluttering from night's outer wing\n"
               "    strong silent greens silently lingering,\n"
               "    absolute lights like baths of golden snow.\n"
               "    This is the garden:pursed lips do blow\n"
               "    upon cool flutes within wide glooms,and sing\n"
               "    (of harps celestial to the quivering string)\n"
               "    invisible faces hauntingly and slow.\n"
               "   \n"
               "    This is the garden.   Time shall surely reap\n"
               "    and on Death's blade lie many a flower curled,\n"
               "    in other lands where other songs be sung;\n"
               "    yet stand They here enraptured,as among\n"
               "    the slow deep trees perpetual of sleep\n"
               "    some silver-fingered fountain steals the world.\n"
               "   \n"
               "    e.e. cummings\n"
 

1.41
                "   Daffodils \n"
                "    \n"
                "   I wandered lonely as a cloud \n"
                "   That floats on high o'er vales and hills, \n"
                "   When all at once I saw a crowd, \n"
                "   A host, of golden daffodils; \n"
                "   Beside the lake, beneath the trees, \n"
                "   Fluttering and dancing in the breeze. \n"
                "    \n"
                "   Continuous as the stars that shine \n"
                "   And twinkle on the milky way, \n"
                "   They stretched in never-ending line \n"
                "   Along the margin of a bay: \n"
                "   Ten thousand saw I at a glance, \n"
                "   Tossing their heads in sprightly dance. \n"
                "    \n"
                "   The waves beside them danced; but they \n"
                "   Out-did the sparkling waves in glee: \n"
                "   A poet could not but be gay, \n"
                "   In such a jocund company: \n"
                "   I gazed--and gazed--but little thought \n"
                "   What wealth the show to me had brought: \n"
                "    \n"
                "   For oft, when on my couch I lie \n"
                "   In vacant or in pensive mood, \n"
                "   They flash upon that inward eye \n"
                "   Which is the bliss of solitude; \n"
                "   And then my heart with pleasure fills, \n"
                "   And dances with the daffodils. \n"
                "   \n"
                "   William Wordsworth \n"
              ) ; 
                               */
      /* 1.40
              "     Hälfte des Lebens\n"
               "\n"
               "     Mit gelben Birnen hänget\n"
               "     Und voll mit wilden Rosen\n"
               "     Das Land in den See,\n"
               "     Ihr holden Schwäne,\n"
               "     Und trunken von Küssen\n"
               "     Tunkt ihr das Haupt\n"
               "     Ins heilignüchterne Wasser.\n"
               "\n"
               "     Weh mir, wo nehm ich, wenn\n"
               "     Es Winter ist, die Blumen, und wo\n"
               "     Den Sonnenschein,\n"
               "     Und Schatten der Erde?\n"
               "     Die Mauern stehn\n"
               "     Sprachlos und kalt, im Winde\n"
               "     Klirren die Fahnen.\n"
               "\n"
               "     Friedrich Hölderlin (1770–1843)\n\n"
               */
               /* 1.39
               "     You Knew Summer By Its Fragrance\n"
               "        \n"
               "     You knew summer by its fragrance,\n"
               "     the ancient silence\n"
               "     of the wall, the frenzy of the cicadas,\n"
               "     you invent the lightly bitter perpendicular\n"
               "     light, the brief shadow\n"
               "     in which a young urchin has dropped asleep,\n"
               "     the luster of his shoulder blades.\n"
               "     It is that that blinds you, the sunlight of the flesh.\n"
               "     \n"
               "                        Eugenio de Andrade\n"
               "                          (translation Alexis Levitin)\n\n"
               */
              /* 1.38, never released.
               "                  Snowdrop\n"
                "                                 Ted Hughes\n"
               "\n" */

              /* 1.36 Achillea
              "  \n"
              "   Flourish – Sept 5 \n"
              "   \n"
              "   With one last flourish \n"
              "   rainbows and roses announce \n"
              "   summer is over. \n\n" 
              "      by poetrydiary\n\n"  */

      /* 1.35 Ichtyosaurus 
              "\n   haïku d'été\n\n"
              "   Mon âme\n"
              "   plonge dans l'eau et ressort\n"
              "   avec le cormoran\n\n"
              "   Onitsura (traduction: Munier)\n" ) ;
       */
      /* 1.34 Pollywog
              "   Spring is like a perhaps hand\n"
              "   \n"
              "   Spring is like a perhaps hand\n"
              "   (which comes carefully\n"
              "   out of Nowhere)arranging\n"
              "   a window,into which people look(while\n"
              "   people stare\n"
              "   arranging and changing placing\n"
              "   carefully there a strange\n"
              "   thing and a known thing here)and\n"
              "   \n"
              "   changing everything carefully\n"
              "   \n"
              "   spring is like a perhaps\n"
              "   Hand in a window\n"
              "   (carefully to\n"
              "   and fro moving New and\n"
              "   Old things,while\n"
              "   people stare carefully\n"
              "   moving a perhaps\n"
              "   fraction of flower here placing\n"
              "   an inch of air there)and\n"
              "   \n"
              "   without breaking anything. \n"
              "                    E. E. Cummings\n" ) ; */

      /* 1.32 Daphnia
         printf ( "\n"
                  "   And much the most answering things that he knew\n"
                  "   Were geraniums (red) and delphiniums (blue).\n"
                  "                A.A. Milne\n" ) ;
 */ 
      /* 1.31. Magnolia.
      printf ("\n"
              "  Dear March - Come in -\n"
              "  How glad I am - \n"
              "  I hoped for you before -\n"
              "  Put down your Hat -\n"
              "  You must have walked -\n"
              "  How out of Breath you are -\n"
              "  Dear March, how are you, and the Rest - \n"
              "  Did you leave Nature well -\n"
              "  Oh March, Come right up stairs with me -\n"
              "  I have so much to tell -\n"
              "\n"
              "  I got your Letter, and the Birds -\n"
              "  The Maples never knew that you were coming - till I called\n"
              "  I declare - how Red their Faces grew -\n"
              "  But March, forgive me - and\n"
              "  All those Hills you left for me to Hue -\n"
              "  There was no Purple suitable - \n"
              "  You took it all with you - \n"
              "\n"
              "  Who knocks? That April.\n"
              "  Lock the Door -\n"
              "  I will not be pursued -\n"
              "  He stayed away a Year to call\n"
              "  When I am occupied - \n"
              "  But trifles look so trivial\n"
              "  As soon as you have come\n"
              "\n"
              "  That Blame is just as dear as Praise\n"
              "  And Praise as mere as Blame - \n"
              "                                           Emily Dickinson\n" );
 */
      /* 1.29 Meriones unguiculatus
      printf ( "     The Shakespeare Reduction: Haiku-Sonnet XVIII\n\n"
               "      eternal summer\n"
               "      too hot the eye of heaven\n"
               "      his gold complexion \n\n"
               "                    Gwilym Williams\n" ) ; */      
      /*
      1.28    
      printf ("    Das fehlende Glied zwischen Affen und Mensch sind wir selbst.\n"
              "     Konrad Lorenz.\n" ) ; 
      1.25 Dromaius novaehollandia
      printf ("     one person's hip is the end of another person's thigh bone.\n" ) ; 
      1.23, KAA
      printf (
       " Kaa, his head motionless on the ground, thought of all that he had\n"
       " seen and known since the day he came from the egg.\n"
       " \n"
       " The light seemed to go out of his eyes and leave them like stale\n"
       " opals, and now and again he made little stiff passes with his head,\n"
       " right and left, as though he were hunting in his sleep. Mowgli dozed\n"
       " quietly, for he knew that there is nothing like sleep before hunting,\n"
       " and he was trained to take it at any hour of the day or night.\n"
       " \n"
       " Then he felt Kaa's back grow bigger and broader below him as the huge\n"
       " python puffed himself out, hissing with the noise of a sword drawn\n"
       " from a steel scabbard;\n"
       " \n"
       " 'I have seen all the dead seasons,' Kaa said at last, 'and the great\n"
       " trees and the old elephants, and the rocks that were bare and\n"
       " sharp-pointed ere the moss grew. Art thou still alive, Manling?'\n\n" 
       ) ; */
      /*
      printf ("               hip: the greater trochanter.\n") ;
      printf ("               hip: makes your dreams go further.\n" ) ; 
      printf ("    thou shall not desire another man's hip. Domenicus 13:48.\n" ) ; 
      printf ("               Ob Sommerer oder Winter, hip steht immer zur Seite.\n" ) ; 
      printf ("               there's more to hip than you think.\n" ) ; 
      printf ("               hip: meshes with your life.\n" ) ; 
      printf ("               hip: total control.\n\n" ) ; */

















/*




    unto thee i

    unto thee i
    burn incense
    the bowl crackles
    upon the gloom arise purple pencils

    fluent spires of fragrance
    the bowl
    seethes
    a flutter of stars

    a turbulence of forms
    delightful with indefinable flowering,
    the air is
    deep with desirable flowers

    i think
    thou lovest incense
    for in the ambiguous faint aspirings
    the indolent frail ascensions,

    of thy smile rises the immaculate
    sorrow
    of thy low
    hair flutter the level litanies

    unto thee i burn
    incense, over the dim smoke
    straining my lips are vague with
    ecstasy my palpitating breasts inhale the

    slow
    supple
    flower
    of thy beauty,my heart discovers thee

    unto
    whom i
    burn
    olbanum

        e.e. cummings



Midsummer, Tobago by Derek Walcott
Broad sun-stoned beaches.

White heat.
A green river.

A bridge,
scorched yellow palms

from the summer-sleeping house
drowsing through August.

Days I have held,
days I have lost,

days that outgrow, like daughters,
my harbouring arms. 



Der Sturm

Im Windbrand steht die Welt. Die Städte knistern.
Halloh, der Sturm, der große Sturm ist da.
Ein kleines Mädchen fliegt von den Geschwistern.
Ein junges Auto flieht nach Ithaka.

Ein Weg hat seine Richtung ganz verloren.
Die Sterne sind dem Himmel ausgekratzt.
Ein Irrenhäusler wird zu früh geboren.
In San Franzisko ist der Mond geplatzt.

Alfred Lichtenstein
Aus der Sammlung Die Dämmerung


Der Sommerregen

Auf einmal ist aus allem Grün im Park
man weiß nicht was, ein Etwas fortgenommen;
man fühlt ihn näher an die Fenster kommen
und schweigsam sein. Inständig nur und stark

ertönt aus dem Gehölz der Regenpfeifer,
man denkt an einen Hieronymus:
so sehr steigt irgend Einsamkeit und Eifer
aus dieser einen Stimme, die der Guss

erhören wird. Des Saales Wände sind
mit ihren Bildern von uns fortgetreten,
als dürften sie nich hören was wir sagen.

Es spiegeln die verblichenen Tapeten
das ungewisse Licht von Nachmittagen,
in denen man sich fürchtet als Kind.

Rainer Maria Rilke, Juli 1906, Paris 

*/
     /* 1.45, never deployed.

              "  Mother, Summer, I \n"
              "\n"
              "  My mother, who hates thunder storms,\n"
              "  Holds up each summer day and shakes\n"
              "  It out suspiciously, lest swarms\n"
              "  Of grape-dark clouds are lurking there;\n"
              "  But when the August weather breaks\n"
              "  And rains begin, and brittle frost\n"
              "  Sharpens the bird-abandoned air,\n"
              "  Her worried summer look is lost,\n"
              "\n"
              "  And I her son, though summer-born\n"
              "  And summer-loving, none the less\n"
              "  Am easier when the leaves are gone\n"
              "  Too often summer days appear\n"
              "  Emblems of perfect happiness\n"
              "  I can''t confront: I must await\n"
              "  A time less bold, less rich, less clear:\n"
              "  An autumn more appropriate. \n"
              "\n"

              "              Philip Larkin\n"
*/


/* A Thunderstorm in Town

She wore a 'terra-cotta' dress,
And we stayed, because of the pelting storm,
Within the hansom's dry recess,
Though the horse had stopped; yea, motionless
We sat on, snug and warm.

Then the downpour ceased, to my sharp sad pain,
And the glass that had screened our forms before
Flew up, and out she sprang to her door:
I should have kissed her if the rain
Had lasted a minute more.

Thomas Hardy

*/

/*
January

 Again I reply to the triple winds
running chromatic fifths of derision
outside my window:
Play louder.
You will not succeed. I am
bound more to my sentences
the more you batter at me
to follow you.
And the wind,
as before, fingers perfectly
its derisive music.
William Carlos Williams
*/


/*

Along the Hard Crust...
By: Anna Akhmatova

The city is caught in the grip of ice--
Trees, walls, snow, are as under glass.
Over crystals, I and the patterned sleighs
Go our separate, unsteady ways.

Along the hard crust of deep snows,
To the secret, white house of yours,
So gentle and quiet – we both
Are walking, in silence half-lost.
And sweeter than all songs, sung ever,
Are this dream, becoming the truth,
Entwined twigs’ a-nodding with favor,
The light ring of your silver spurs...

*/
              /* 1.38, unreleased
               "\n" */


/* 
April

By Julia Travers

 

Stretching out

from dry wood,

a new leaf.


-----------------------------------

A small bird flying

By Margaret Kilmartin

 

A small bird flying

With every silent fast flap

A world of freedom

--------------------------

After the Rain

By Carolyn Gillespie

 

In the crystal balls

On the cow-parsley

The future is upside down.

-----------------------------


*/
