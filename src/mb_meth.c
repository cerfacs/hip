/*
   mb_meth.c
   Structured grid methods.
 
   Last update:
   ------------
   6Jan15; rename pFamMb to more descriptive pArrFamMb
   24Apr98; return proper dll/dur ... from get_uns_subfc.
   11Nov97; fix mb_size bug.
   18Oct97; fix face_loop_block
   30May96; move rotation stuff to mb_rot.c.
   20Apr96; conceived.

   Contains:
   ---------
   make_blocks:
   get_mb_subface:
   get_mb_boundVert:
   get_mb_boundCell:
   get_mb_boundFace:
   get_faceNr_ijk:
   get_ijk_nElem:
   get_ijk_nVert:
   get_nElem_ijk:
   get_nVert_ijk:
   check_nElem_ijk:
   check_nVert_ijk:
   free_mb:
   cell_loop_subfc:
   face_loop_block:
   put_mb_subFc:
   mb_count:
   mb_size:
   mb_bb:
   get_static_subface:
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

extern int verbosity ;
extern Grids_struct Grids ;
extern arrFam_s *pArrFamMb;
extern char hip_msg[] ;


/******************************************************************************

  make_blocks:
  Allocate a list of blocks and reset it.
  
  Last update:
  ------------
  6Nov96: conceived.
  
  Input:
  ------
  mBlocks:
  
  Returns:
  --------
  the pointer to the allocated space.
  
*/

block_struct *make_blocks ( int mBlocks  )
{
  block_struct *Pblocks, *Pbl ;
  
  Pblocks = arr_malloc ( "Pblocks in make_blocks", pArrFamMb, 
                         mBlocks+1, sizeof( block_struct ) ) ;
  if ( !Pblocks )
    return ( NULL ) ;
  else
  { for ( Pbl = Pblocks ; Pbl <= Pblocks+mBlocks ; Pbl++ )
    { Pbl->nr = 0 ;
      Pbl->Pcoor = Pbl->PdblMark = Pbl->Punknown = NULL ;
      Pbl->PintMark = Pbl->PelemMark = NULL ;
    }

    return ( Pblocks ) ;
  }
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mb_apply_skip:
*/
/*! Does a block or window index range divide by skip? If so, apply.
 *
 */

/*
  
  Last update:
  ------------
  10Mar18: extracted from read_mb_cgns.
  

  Input:
  ------
  fcName: name/label of the face, used in err msg
  fullRg: original/fullsize/non-skpped index vector
  kDimLo/Hi: inclusive index dim to check,
  skip: skip divisor.

  Output:
  -------
  skipRg: decimated index vector divided by skip.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mb_apply_skip ( const char *fcName,
                    const int fullRg[MAX_DIM],
                    const int kDimLo, const int kDimHi,
                    const int skip,
                    int skipRg[MAX_DIM] ) {

  int kDim ;
  for ( kDim = kDimHi ; kDim <= kDimHi ; kDim++ ) {
    /* Check whether the subface indices divide by skip. */
    if ( (fullRg[kDim]-1 )%skip ) {
      sprintf ( hip_msg, "left subface %s: index %d at dim %d doesn't"
                " divide by %d in mb_apply_skip.\n",
                fcName, fullRg[kDim], kDim, skip ) ;
      hip_err ( fatal, 0, hip_msg ) ;
      return ( 0 ) ;
    }
    skipRg[kDim] = ( fullRg[kDim]-1 )/skip + 1 ;
  }
  return ( 1 ) ;
}

/******************************************************************************

  get_mb_subface:
  Given a subFace on a block, find the index parameters of the subface
  and return loop parameters for a loop over the subface only, ie mDim-1.
  
  Last update:
  25Apr96: cleanup.
  20Apr96: conceived.
  
  Input:
  ------
  Pbl:              the block,
  Psf:              the subFace,
  mDim:             number of dimensions,
  
  Output:
  -------
  ll/ur:            the lower left/upper right indices of the subFace,
  Pindex1/2:        the first/second running index on the subface,
  PmultVert1/2:     the multipliers for the running indices for the
                    lower left vertex of each face on the subface,
  PmultCell1/2:     the multipliers for the running indices for the
                    cells that border on the subface.
  PoffsetVert/Cell: static offsets for the +ijk faces.
  PindexStatic      position of the static index,
  dll/lr/ur/ul:     The vertex number differences of the vertices of the face on the
                    subface, relative the the vertex with the same indices as the cell.

  Returns:
  --------
  0: on an inconsistent subface,
  1: otherwise.
  
*/

int get_mb_subface ( const block_struct *Pbl, const subFace_struct *Psf,
		     const int mDim, int ll[], int ur[],
		     int *Pindex1, int *PmultVert1, int *PmultCell1,
		     int *Pindex2, int *PmultVert2, int *PmultCell2,
		     int *PoffsetVert, int *PoffsetCell, int *PindexStatic,
		     int *Pdll, int *Pdlr, int *Pdur, int *Pdul ) {
  
  static int nDim, mVert[MAX_DIM] ;
  
  if ( Psf->PlBlock == Pbl ) {
    /* The block is to the left of the subface. */
    ll[2] = ur[2] = 1 ;
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      ll[nDim] = Psf->llLBlock[nDim] ;
      ur[nDim] = Psf->urLBlock[nDim] ;
      mVert[nDim] = Pbl->mVert[nDim] ;
    }
  }
  else if ( Psf->PrBlock == Pbl ) {
    /* The block is to the right of the subface. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      ll[nDim] = Psf->llRBlock[nDim] ;
      ur[nDim] = Psf->urRBlock[nDim] ;
      mVert[nDim] = Pbl->mVert[nDim] ;
    }
  }
  else
    return ( 0 ) ;

  if ( mDim == 2 ) {
    /* We can do 2D. */
    if ( ll[1] == ur[1] ) {
      /* +- j. */
      *PindexStatic = 1 ;
      *Pindex1 = 0 ;
      *Pindex2 = 2 ;
      
      *PmultVert1 = 1 ;
      *PmultVert2 = mVert[0] ;
      *PoffsetVert = 1 ;
      *PmultCell1 = 1 ;
      *PmultCell2 = mVert[0] - 1 ;
      
      if ( ll[1] == 1 ) {
        /* -j: On a 3x2 example, #[1][1] = 1: 1-2 */
	*Pdll = 0 ;
	*Pdlr = 1 ;
	*PoffsetCell = 1 ;
      }
      else {
        /* +j: On a 3x2 example: #[1][1] = 1: 5-4 */
	*Pdll = mVert[0]+1 ;
	*Pdlr = mVert[0] ;
	*PoffsetCell = ( mVert[0]-1 )*( mVert[1] - 2 ) + 1 ;
      }
    }
    else { /* +/- i. */
      *PindexStatic = 0 ;
      *Pindex1 = 1 ;
      *Pindex2 = 2 ;
      
      *PmultVert2 = 1 ;
      *PmultVert1 = mVert[0] ;
      *PoffsetVert = 1 ;
      *PmultCell2 = 1 ;
      *PmultCell1 = mVert[0] - 1 ;
      *Pdll = 0 ;
      *Pdlr = 1 ;

      if ( ll[0] == 1 ) {
        /* -i: On a 3x2 example: 4
	                         |
				 1 */
	*PoffsetCell = 1 ;
	*Pdll = mVert[0] ;
	*Pdlr = 0 ;
      }
      else {
        /* +i: On a 3x2 example: 5
	                         |
				 2 */
	*PoffsetCell = mVert[0] - 1 ;
	*Pdll = 1 ;
	*Pdlr = mVert[0]+1 ;
      }
    }
    return ( 1) ;
  }
  
  else if ( mDim == 3 )
  {
    /* Find out what block side we are dealing with. */
    if ( ll[2] == ur[2] )
    { *PindexStatic = 2 ;
    
      if ( ll[2] == 1 )
      {	/* -k, loop over i and j keeping k=1 */
	*Pindex1 = 0 ;
	*PmultVert1 = 1 ;
	*PmultCell1 = 1 ;
	*Pindex2 = 1 ;
	*PmultVert2 = mVert[0] ;
	*PmultCell2 = mVert[0] - 1 ;
	*PoffsetVert = 1 ;
	*PoffsetCell = 1 ;
      
	/* Sample face of nI=nJ=nK=1:    4---<---5
	   in a 3x3x3 block:             |       |
	                                 V       A
					 |       |
					 1--->---2
	   offsets to the vertex number of nI,nJ,nK are: */
	*Pdll = 0 ;
	*Pdlr = 1 ;
	*Pdur = mVert[0]+1 ;
	*Pdul = mVert[0] ;
      }
      else 
      {	/* +k, loop over i and j keeping k=ur[3] */
	*Pindex1 = 0 ;
	*PmultVert1 = 1 ;
	*PmultCell1 = 1 ;
	*Pindex2 = 1 ;
	*PmultVert2 = mVert[0] ;
	*PmultCell2 = mVert[0]-1 ;
	*PoffsetVert = 1 + ( ur[2]-1 )*mVert[0]*mVert[1] ;
	*PoffsetCell = 1 + ( ur[2]-2 )*( mVert[0]-1 )*( mVert[1]-1 ) ;
      
	/* Sample face of nI=nJ=nK=1:   23---<---22
	   in a 3x3x3 block:             |       |
	                                 V       A
					 |       |
				        20--->---19 
	   Offsets to the vertex number of nI,nJ,nK are: */
	*Pdll = 1 ;
	*Pdlr = 0 ;
	*Pdur = mVert[0] ;
	*Pdul = mVert[0]+1 ;
      }
    }
    else if ( ll[1] == ur[1] )
    { *PindexStatic = 1 ;

      if ( ll[1] == 1 )
      {	/* -j, loop over i and k keeping j=1 */
	*Pindex1 = 0 ;
	*PmultVert1 = 1 ;
	*PmultCell1 = 1 ;
	*Pindex2 = 2 ;
	*PmultVert2 = mVert[0]*mVert[1] ;
	*PmultCell2 = ( mVert[0]-1 )*( mVert[1]-1 ) ;
	*PoffsetVert = 1 ;
	*PoffsetCell = 1 ;

	/* Sample face of nI=nJ=nK=1:    2---<---11
	   in a 3x3x3 block:             |       |
	                                 V       A
					 |       |
					 1--->---10
	   Offsets to the vertex number of nI,nJ,nK are: */
	*Pdll = 0 ;
	*Pdlr = mVert[0]*mVert[1] ;
	*Pdur = *Pdlr+1 ;
	*Pdul = 1 ;
      }
      else 
      {	/* +j, loop over i and k keeping j=ur[1] */
	*Pindex1 = 0 ;
	*PmultVert1 = 1 ;
	*PmultCell1 = 1 ;
	*Pindex2 = 2 ;
	*PmultVert2 = mVert[0]*mVert[1] ;
	*PmultCell2 = ( mVert[0]-1 )*( mVert[1]-1 ) ;
	*PoffsetVert = 1 + ( ur[1]-1 )*mVert[0] ;
	*PoffsetCell = 1 + ( ur[1]-2 )*( mVert[0]-1 ) ;

	/* Sample face of nI=nJ=nK=1:   17---<---8
	   in a 3x3x3 block:             |       |
	                                 V       A
					 |       |
					16--->---7
	   Offsets to the vertex number of nI,nJ,nK are: */
	*Pdll = mVert[0]*mVert[1] ;
	*Pdlr = 0 ;
	*Pdur = 1 ;
	*Pdul = *Pdll+1 ;
      }
    }
    else
    { *PindexStatic = 0 ;

      if ( ll[0] == 1 )
      {	/* -i, loop over j and k keeping i=1 */
	*Pindex1 = 1 ;
	*PmultVert1 = mVert[0] ;
	*PmultCell1 = mVert[0]-1 ;
	*Pindex2 = 2 ;
	*PmultVert2 = *PmultVert1*mVert[1] ;
	*PmultCell2 = *PmultCell1*( mVert[1]-1 ) ;
	*PoffsetVert = 1 ;
	*PoffsetCell = 1 ;

	/* Sample face of nI=nJ=nK=1:   13---<---4
	   in a 3x3x3 block:             |       |
	                                 V       A
					 |       |
					10--->---1
	   Offsets to the vertex number of nI,nJ,nK are: */
	*Pdlr = 0 ;
	*Pdll = mVert[0]*mVert[1] ;
	*Pdur = mVert[0] ;
	*Pdul = *Pdll+mVert[0] ;
      }
      else 
      {	/* +i, loop over j and k keeping i=ur[0] */
	*Pindex1 = 1 ;
	*PmultVert1 = mVert[0] ;
	*PmultCell1 = mVert[0]-1 ;
	*Pindex2 = 2 ;
	*PmultVert2 = *PmultVert1*mVert[1] ;
	*PmultCell2 = *PmultCell1*( mVert[1]-1 ) ;
	*PoffsetVert = mVert[0] ;
	*PoffsetCell = *PoffsetVert - 1 ;

	/* Sample face of nI=nJ=nK=1:    6---<---15
	   in a 3x3x3 block:             |       |
	                                 V       A
					 |       |
					 3--->---12
	   Offsets to the vertex number of nI,nJ,nK are: */
	*Pdlr = mVert[0]*mVert[1] ;
	*Pdll = 0 ;
	*Pdur = *Pdlr+mVert[0] ;
	*Pdul = mVert[0] ;
      }
    }
    return ( 1 ) ;
  }
  else
  { printf ( " FATAL: get_mbSubface can\'t deal with %d dimensions.\n", mDim ) ;
    return ( 0 ) ;
  }
}


/*******************************************************************************

   get_mb_boundVert:
   Use the 2-D parameters of get_mb_subface and return the number of
   the boundary vertex.

   Last update:
   25Apr96: cleanup.
   20Apr96: conceived.
   
   Input:
   ------
   index1/2:    running indices,
   multVert1/2: multipliers,
   offsetVert:  offset to the face,
   
   Returns:
   -------
   nVert:       vertex number on the subface.
*/

int get_mb_boundVert ( int index1, int multVert1,
		       int index2, int multVert2, int offsetVert )
{
  static int nVert ;
  nVert = ( index1-1 )*multVert1 + ( index2-1 )*multVert2 + offsetVert ;
  return ( nVert ) ;
}


/*******************************************************************************

   get_mb_boundCell:
   Use the 2-D parameters of get_mb_subface and return the number of
   the boundary cell.

   Last update:
   25Apr96: cleanup.
   20Apr96: conceived.
   
   Input:
   ------
   index1/2:    running indices,
   multCell1/2: multipliers,
   offsetCell:  offset to the face,
   
   Returns:
   -------
   nCell:       vertex number on the subface.
*/

int get_mb_boundCell ( int index1, int multCell1,
		       int index2, int multCell2, int offsetCell )
{
  int nCell ;
  nCell = ( index1-1 )*multCell1 + ( index2-1 )*multCell2 + offsetCell ;
  return ( nCell ) ;
}

/*******************************************************************************

   get_mb_boundFace:
   Use the 2-D parameters of get_mb_subface and return the numbers of
   forming vertices of a specific subface to Pbuffer. Return the
   pointer Pbuffer to the last vertex number written.

   Last update:
   20Apr96: conceived.
   
   Input:
   ------
   n1/2:         indices on the subface,
   mult1/2:      multipliers for the vertex indices,
   dll/lr/ur/ul: offsets of the vertex numbers on this subface,
   
   Changes to:
   -----------
   *pnFcVert      the vertex numbers of the face, right hand rule outward, or
                  element to the left in 2D,

   Returns:
   --------
   nVx:          the number of the vertex with n1, n2, offset. 
*/

int get_mb_boundFace ( int n1, int mult1,
		       int n2, int mult2, int offset,
		       int dll, int dlr, int dur, int dul,
		       int *pnFcVert )
{
  static int nVx ;
  
  nVx = ( n1-1 )*mult1 + ( n2-1 )*mult2 + offset ;
  *pnFcVert = nVx + dll ;
  *(++pnFcVert) = nVx + dlr ;
  *(++pnFcVert) = nVx + dur ;
  *(++pnFcVert) = nVx + dul ;

  return ( nVx ) ;

}

/*******************************************************************************

  get_faceNr_ijk:
  Use the 2-D parameters of get_mb_subface and return the face number
  of the boundary face in avbp convention. (See cpre.h for that format.)
  
  
  Last update:
  20Apr96: conceived.
  
  Input:
  ------
  indexStatic: position of the static index of that subface,
  indexValue:  value of the static index,
  mDim:        number of spatial dimensions,
  
  Returns:
  --------
  ( int ):     number of the face.

*/

int get_faceNr_ijk ( int indexStatic, int indexValue, int mDim )
{
  if ( mDim == 2 )
  { if ( indexStatic == 0  )
    { if ( indexValue == 1 )
      { /* -i. */
	return ( 4 ) ;
      }
      else
      {/* +i. */
	return ( 2 ) ;
      }
    }
    else if ( indexStatic == 1  )
    { if ( indexValue == 1 )
      { /* -j. */
	return ( 1 ) ;
      }
      else
      {/* +j. */
	return ( 3 ) ;
      }
    }
  }
  else if ( mDim == 3 )
  { if ( indexStatic == 0  )
    { if ( indexValue == 1 )
      { /* -i. */
	return ( 4 ) ;
      }
      else
      {/* +i. */
	return ( 2 ) ;
      }
    }
    else if ( indexStatic == 1  )
    { if ( indexValue == 1 )
      { /* -j. */
	return ( 1 ) ;
      }
      else
      {/* +j. */
	return ( 3 ) ;
      }
    }
    else 
    { if ( indexValue == 1 )
      { /* -k. */
	return ( 5 ) ;
      }
      else
      {/* +k. */
	return ( 6 ) ;
      }
    }
  }

  printf ( " FATAL: get_faceNr_ijk cannot do %d dimensions.\n", mDim ) ;
  return ( 0 ) ;
}

/*******************************************************************************

   get_ijk_nElem:
   Get the indices i,j,k from the lexicographic number of a cell.

   Last update:
   20Apr96: conceived.
   
   Input:
   ------
   mVert[]:    the number of vertices in each direction,
   mCellsBlock: the total number of cells in the block,
   nCell:      the number of the cell for which ijk is sought.
   mDim:       the number of dimensions.
   
   Changes to:
   -----------
   ijk[]:      indices of nCell.
*/

void get_ijk_nElem ( const int mVert[], int mElemsBlock,
		     int nElem, const int mDim, int ijk[] ) {
  
  static int nDim ;

# ifdef CHECK_BOUNDS
    if ( nElem > mElemsBlock || nElem < 1 )
    { printf ( " WARNING: out of bounds in get_ijk_nElem:\n" ) ;
      printf ( "          nElem = %d > mElemsBlock = % d\n",
	       nElem, mElemsBlock ) ;
      exit ( EXIT_FAILURE ) ;
    }
# endif
  
  for ( nDim = mDim-1 ; nDim > 0 ; nDim-- )
  {
    mElemsBlock /= mVert[nDim]-1 ;
    ijk[nDim] = 1 + ( nElem-1 )/mElemsBlock ;
    nElem -= ( ijk[nDim]-1 )*mElemsBlock ;
  }
  mElemsBlock /= mVert[0]-1 ;
  ijk[0] = 1 + ( nElem-1 )/mElemsBlock ;
}


/*******************************************************************************

   get_ijk_nVert:
   Get the indices i,j,k from the lexicographic number of a vertex.

   Last update:
   20Apr96: conceived.
   
   Input:
   ------
   mVert[]:    the number of vertices in each direction,
   mVertBlock: the total number of vertices in the block,
   nVert:      the number of the vertex for which ijk is sought.
   mDim:       the number of dimensions.
   
   Changes to:
   -----------
   ijk[]:      indices of nVert.
*/

void get_ijk_nVert ( int mVert[], int mVertBlock, int nVert, int mDim, int ijk[] ) {
  
  int nDim ;

# ifdef CHECK_BOUNDS
    if ( nVert > mVertBlock || nVert < 1 )
    { printf ( " WARNING: out of bounds in get_ijk_nr:\n" ) ;
      printf ( "          nVert = %d > mVertBlock = % d\n",
	       nVert, mVertBlock ) ;
      exit ( EXIT_FAILURE ) ;
    }
# endif
  
  for ( nDim = mDim-1 ; nDim > 0 ; nDim-- )
  {
    mVertBlock /= mVert[nDim] ;
    ijk[nDim] = 1 + ( nVert-1 )/mVertBlock ;
    nVert -= ( ijk[nDim]-1 )*mVertBlock ;
  }
  mVertBlock /= mVert[0] ;
  ijk[0] = 1 + ( nVert-1 )/mVertBlock ;
}


/*******************************************************************************

   get_nElem_ijk:
   Get the lexicographic element number from the indices i,j,k.

   Last update:
   20Apr96: conceived.
   
   Input:
   ------
   mDim:  number of dimensions,
   ijk[]: indices of the element,
   IJK[]: upper range of indices,
   
   Returns:
   -------
   index: the lexicographic number of the element.
*/

int get_nElem_ijk ( const int mDim, const int ijk[], const int IJK[] ) {
  /* Return a linear index 1...max on a mDim-field from 1,1,1 to *IJK-1. */
  int index = 0, nDim ;
  
  for ( nDim = mDim-1 ; nDim > 0 ; nDim-- )
  {
#   ifdef CHECK_BOUNDS
      if ( ijk[nDim] > IJK[nDim]-1 || ijk[nDim] < 1 )
      { printf ( " FATAL: out of bounds in get_nElem_ijk.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
#   endif

    index += ijk[nDim] - 1 ;
    index *= IJK[nDim-1]-1 ;
  }
  
# ifdef CHECK_BOUNDS
    if ( ijk[0] > IJK[0]-1 || ijk[0] < 1 )
    { printf ( " FATAL: beyond bounds in get_nElem_ijk.\n" ) ;
      exit ( EXIT_FAILURE ) ;
    }
# endif

  index += ijk[0] ;
  return ( index ) ;
}
  

/*******************************************************************************

   get_nVert_ijk:
   Get the lexicographic vertex number from the indices i,j,k.

   Last update:
   20Apr96: conceived.
   
   Input:
   ------
   mDim:  number of dimensions,
   ijk[]: indices of the vertex,
   IJK[]: upper range of indices,
   
   Returns:
   -------
   index: the lexicographic number of the vertex.

*/

int get_nVert_ijk ( const int mDim, const int ijk[], const int IJK[] ) {
  /* Return a linear index 1...max on a mDim-field from 1,1,1 to *IJK. */
  int index = 0, nDim ;
  
  for ( nDim = mDim-1 ; nDim > 0 ; nDim-- )
  {
#   ifdef CHECK_BOUNDS
      if ( ijk[nDim] > IJK[nDim] || ijk[nDim] < 1 )
      { printf ( " FATAL: out of bounds in get_nVert_ijk.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
#   endif

    index += ijk[nDim] - 1 ;
    index *= IJK[nDim-1] ;
  }
  
# ifdef CHECK_BOUNDS
    if ( ijk[0] > IJK[0] || ijk[0] < 1 )
    { printf ( " FATAL: beyond bounds in get_nVert_ijk.\n" ) ;
      exit ( EXIT_FAILURE ) ;
    }
# endif

  index += ijk[0] ;
  return ( index ) ;
}

/*******************************************************************************

   check_nElem_ijk:
   Get the lexicographic element number from the indices i,j,k. As opposed to
   get_nElem_ijk, always check ijk for validity and return 0 if ijk is out of
   range.

   Last update:
   5Aug96; derived from get_nElem_ijk.
   
   Input:
   ------
   mDim:  number of dimensions,
   ijk[]: indices of the element,
   IJK[]: upper range of indices,
   
   Returns:
   -------
   index: the lexicographic number of the element, 0 if it's out of bounds.
*/

int check_nElem_ijk ( const int mDim, const int ijk[], const int IJK[] ) {
  /* Return a linear index 1...max on a mDim-field from 1,1,1 to *IJK-1. */
  int index = 0, nDim ;
  
  for ( nDim = mDim-1 ; nDim > 0 ; nDim-- )
  { if ( ijk[nDim] > IJK[nDim]-1 || ijk[nDim] < 1 )
      return ( 0 ) ;
    index += ijk[nDim] - 1 ;
    index *= IJK[nDim-1]-1 ;
  }
  
  if ( ijk[0] > IJK[0]-1 || ijk[0] < 1 )
    return ( 0 ) ;

  index += ijk[0] ;
  return ( index ) ;
}
  

/*******************************************************************************

   check_nVert_ijk:
   Get the lexicographic vertex number from the indices i,j,k. As opposed to
   get_nVert_ijk, always check ijk for validity and return 0 if ijk is out of
   range.

   Last update:
   5Aug96: derived from get_nVert_ijk.
   
   Input:
   ------
   mDim:  number of dimensions,
   ijk[]: indices of the vertex,
   IJK[]: upper range of indices,
   
   Returns:
   -------
   index: the lexicographic number of the vertex.

*/

int check_nVert_ijk ( const int mDim, const int ijk[], const int IJK[] )
{ /* Return a linear index 1...max on a mDim-field from 1,1,1 to *IJK. */
  int index = 0, nDim ;
  
  for ( nDim = mDim-1 ; nDim > 0 ; nDim-- )
  {
    if ( ijk[nDim] > IJK[nDim] || ijk[nDim] < 1 )
      return ( 0 ) ;
    index += ijk[nDim] - 1 ;
    index *= IJK[nDim-1] ;
  }
  
  if ( ijk[0] > IJK[0] || ijk[0] < 1 )
    return ( 0 ) ;

  index += ijk[0] ;
  return ( index ) ;
}


/******************************************************************************

  free_mb:
  Free all storage in an mb root.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  PPmb

  Changes To:
  -----------
  *PPmb
  
  
*/

void free_mb ( mb_struct **PPmb ) {
  
  block_struct *Pbl ;
  
  if ( (*PPmb)->PblockS )
    for ( Pbl = (*PPmb)->PblockS ;
	  Pbl <= (*PPmb)->PblockS + (*PPmb)->mBlocks ; Pbl++ )
      if ( Pbl ) {
	arr_free ( Pbl->Pcoor ) ;
	arr_free ( Pbl->PdblMark ) ;
	arr_free ( Pbl->PintMark ) ;
	arr_free ( Pbl->Punknown ) ;
	arr_free ( Pbl->PelemMark ) ;
	arr_free ( Pbl->PPsubFaces ) ;
	arr_free ( Pbl ) ;
      }
  
  arr_free ( (*PPmb)->subFaceS ) ;
  arr_free ( *PPmb ) ;
  *PPmb = NULL ;
  return ;
}

/******************************************************************************

  cell_loop_subfc:
  Loop over all cells of a subface in 2- and 3-D.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ll/ur: subface dimension.
  mDim: spatial dimension.
  n1/2: current subface indices.
  index1/2: running indices.
  multCell1/2: index multipliers out of get_mb_subface.
  offsetCell: cell offset out of get_mb_subface.

  Changes To:
  -----------
  Pn1, Pn2
  
  Returns:
  --------
  nCell during the loop, 0 on completion.
  
*/

int cell_loop_subfc ( const int ll[], const int ur[], const int mDim,
		      int *Pn1, const int index1, const int multCell1,
		      int *Pn2, const int index2, const int multCell2,
		      const int offsetCell ) {
  
  /* Increment the innermost index at the beginning of the loop and go from
     there. In this way, *Pn1 and *Pn2 coincide with nCell on exit. */
  ++(*Pn1 ) ;

  if ( mDim == 2 )
  { if ( *Pn1 == ur[index1] )
      return ( 0 ) ;
    
    return ( ( (*Pn1)-1 )*multCell1 + offsetCell ) ;
  }

  else
  { /* 3D */
    if ( *Pn1 >= ur[index1] )
    { /* Increment the outer loop. */
      (*Pn2)++ ;
      *Pn1 = ll[index1] ;
    }

    if ( *Pn2 >= ur[index2] )
      /* Outer loop finished. */
      return ( 0 ) ;

    return ( ( (*Pn1)-1 )*multCell1 + ( (*Pn2)-1 )*multCell2 + offsetCell ) ;
  }
}


/******************************************************************************

  face_loop_block:
  Loop over all faces interior to a block.
  
  Last update:
  ------------
  18Oct97; deal properrly with blocks that are only one layer thick.
  : conceived.
  
  Input:
  ------
  mDim:
  mVert:     Block dimension in nodes.
  ijk:       Current cell index.
  Pindex1/2: the two running indices.
  PnDim:     the dimension that the face is normal to, or, the direction of the
             neighboring cell that shares the face.
  PdCell:    Offset in the cell numbers of the neighbors.


  Changes To:
  -----------
  ijk
  Pindex1/2
  PnDim
  PdCell
  
  Returns:
  --------
  0 on done, cell number nCell during the loop.
  
*/

int face_loop_block ( const int mDim, const int mVert[],
		      int ijk[], int *Pindex1, int *Pindex2, int *PnDim, int *PdCell ) {
  
  int nCell, ndx1 = *Pindex1, ndx2 = *Pindex2, nDim = *PnDim, change ;
  
  if ( mDim == 2 ) {
    for ( change = 1 ; change ; ) {
      change = 0 ;
      
      if ( ijk[ndx1] >= mVert[ndx1] ) {
	/* Done with the inner loop. */
	ijk[nDim]++ ;
	ijk[ndx1] = 1 ;
	change = 1 ; }
      
      if ( ijk[nDim] >= mVert[nDim]-1 ) {
	/* Done with the third innermost loop. Increment the face direction. */
	*PdCell *= mVert[nDim]-1 ;
	nDim = ++(*PnDim) ;
	/* Difference to the neighboring cell in this dimension. */
	ndx1 = *Pindex1 = ( nDim+1 )%mDim ;
	
	ijk[nDim] = 1 ;
	ijk[ndx1] = 1 ;
	change = 1 ; }

      if ( nDim == 2 )
	/* Done. */
	return ( 0 ) ;
    }
      
    nCell = get_nElem_ijk ( mDim, ijk, mVert ) ;
    ijk[ndx1]++ ;
    return ( nCell ) ;
  }
  else {
    /* 3D. */
    for ( change = 1 ; change ; ) {
      change = 0 ;
      
      if ( ijk[ndx1] >= mVert[ndx1] ) {
	/* Done with the innermost loop. */
	ijk[ndx2]++ ;
	ijk[ndx1] = 1 ;
	change = 1 ; }
      
      if ( ijk[ndx2] >= mVert[ndx2] ) {
	/* Done with the second innermost loop. */
	ijk[nDim]++ ;
	ijk[ndx1] = 1 ;
	ijk[ndx2] = 1 ;
	change = 1 ; }
      
      if ( ijk[nDim] >= mVert[nDim]-1 ) {
	/* Done with the third innermost loop. Increment the face direction. */
	*PdCell *= mVert[nDim]-1 ;
	nDim = ++(*PnDim) ;
	/* Difference to the neighboring cell in this dimension. */
	ndx1 = *Pindex1 = ( nDim+1 )%mDim ;
	ndx2 = *Pindex2 = ( nDim+2 )%mDim ;
	
	ijk[nDim] = 1 ;
	ijk[ndx1] = 1 ;
	ijk[ndx2] = 1 ;
	change = 1 ; }
      
      if ( nDim == 3 )
	/* Done. */
	return ( 0 ) ;
    }
    
    nCell = get_nElem_ijk ( mDim, ijk, mVert ) ;
    ijk[ndx1]++ ;
    return ( nCell ) ;
  }
}

/******************************************************************************

  put_mb_subFc:
  Find the first free spot with a block to list the next subface.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pbl: block
  Psf: subface.

  Changes To:
  -----------
  Pbl:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int put_mb_subFc ( block_struct *Pbl, subFace_struct *Psf ) {
  
  static subFace_struct **PPSF ;
  
  /* Find the first open entry with Pbl. */
  for ( PPSF = Pbl->PPsubFaces ; *PPSF ; PPSF++ )
    ;
  if ( PPSF - Pbl->PPsubFaces >= Pbl->mSubFaces ) {
    sprintf ( hip_msg, "trying too many subfaces for block %d. in put_mb_subFc.\n",
              Pbl->nr ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else {
    *PPSF = Psf ;
    return ( 1 ) ;
  }
  
  return ( 0 ) ; // Just to shut up the compiler, as hip_err stops.
}

/******************************************************************************

  mb_count:
  Count elements and vertices in a multiblock grid.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pmb

  Changes To:
  -----------
  *Pmb
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mb_count ( mb_struct *Pmb ) {
  
  block_struct *Pbl ;
  int mVerts = 0, mElems = 0, mElemsBlock=0, mVertsBlock, nDim ;

  if ( !Pmb )
    return ( 0 ) ;
  else if ( !Pmb->PblockS )
    return ( 0 ) ;
  
  for ( Pbl = Pmb->PblockS+1 ; Pbl <= Pmb->PblockS+ Pmb->mBlocks ; Pbl++ )
  { Pbl->PmbRoot = Pmb ;
    
    for ( mVertsBlock = mElemsBlock = 1,
	 nDim = 0 ; nDim < Pmb->mDim ; nDim++ )
    { mVertsBlock *= Pbl->mVert[nDim] ;
      mElemsBlock *= Pbl->mVert[nDim]-1 ;
    }
    Pbl->mElemsBlock = mElemsBlock ;
    Pbl->mVertsBlock = mVertsBlock ;

    mVerts += mVertsBlock ;
    mElems += mElemsBlock ;
  }

  Pmb->mElems = mElemsBlock ;
  Pmb->mVerts = mVerts ;
  
  return ( 1 ) ;
}


/******************************************************************************

  mb_size:
  Calculate the minimum and maximum edge lengths, hMin and hMax for a mb-grid.
  Loop over all interior edges, and then add all boundary faces that are non-
  degenerate. Before calling mb_size, the degenerate faces have to be fixed.
  
  Last update:
  ------------
  11Nov97; fix stupid cut&paste bug in the j and k loops.
  : conceived.
  
  Input:
  ------
  Pmb: the multi block grid

  Changes To:
  -----------
  Pmb->Pblocks->hMin/Max.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mb_size ( mb_struct *Pmb ) {
  
  static block_struct *Pbl ;
  static int staticDim, staticDir, freeDir1, freeDir2, hiDegenFc[MAX_DIM],
             loDegenFc[MAX_DIM], loI, hiI, loJ, hiJ, loK, hiK, 
             nDim, vertDiff, nI, nJ, nK, nVert, nSubFace ;
  static int mDim ;
  static double h, *Pcoor, *PcoMin, *PcoMax, *PcoMinAll, *PcoMaxAll,
                hMinAllBlocks, hMaxAllBlocks ;
  static subFace_struct *Psf ;

  mDim = Pmb->mDim ;
  hMinAllBlocks = TOO_MUCH, hMaxAllBlocks = -TOO_MUCH ;
  
  if ( !Pmb )
    return ( 0 ) ;
  else if ( !Pmb->PblockS )
    return ( 0 ) ;
  
  for ( Pbl = Pmb->PblockS+1 ; Pbl <= Pmb->PblockS+ Pmb->mBlocks ; Pbl++ ) {
    Pbl->PmbRoot = Pmb ;

    /* Reset the markers for degenerate faces. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      hiDegenFc[nDim] = loDegenFc[nDim] - 0 ;
	 
    /* Loop over all the subfaces. */
    for ( nSubFace = 0 ; nSubFace < Pbl->mSubFaces ; nSubFace++ ) {
      Psf = Pbl->PPsubFaces[nSubFace] ;
      if ( !Psf->PrBlock && !Psf->Pbc ) {
	/* This is a degenerate subface. */
	get_static_subface ( Psf, mDim, 0,
			     &staticDim, &staticDir, &freeDir1, &freeDir2 ) ;
	if ( staticDir == -1 )
	  /* -ijk. */
	  hiDegenFc[staticDim] = 1 ;
	else
	  /* -ijk. */
	  loDegenFc[staticDim] = 1 ;
      }
    }

    /* Reset the limits. */
    Pbl->hMin = TOO_MUCH ;
    Pbl->hMax = -TOO_MUCH ;

    /* Edges in i. Don't loop over the edges in the blockfaces +/-jk if they
       are degenerate. In 2-D, make sure that the k-loop is tripped once.*/
    vertDiff = 1 ;
    loK = ( mDim == 2 ? 1 : loDegenFc[2] ? 2 : 1 ) ;
    hiK = ( mDim == 2 ? 1 : hiDegenFc[2] ? Pbl->mVert[2]-1 : Pbl->mVert[2] ) ;
    loJ = ( loDegenFc[1] ? 2 : 1 ) ;
    hiJ = ( hiDegenFc[1] ? Pbl->mVert[1]-1 : Pbl->mVert[1] ) ;
    loI = 1 ;
    hiI = Pbl->mVert[0]-1 ;
      
    for ( nK = loK ; nK <= hiK ; nK++ )
      for ( nJ = loJ ; nJ <= hiJ  ; nJ++ )
	for ( nI = loI ; nI <= hiI ; nI++ ) {
	  nVert = ((( nK-1 )*Pbl->mVert[1] ) + nJ-1 )*Pbl->mVert[0] + nI ;
	  Pcoor = Pbl->Pcoor + mDim*nVert ;
	  h = sq_distance_dbl( Pcoor, Pcoor + mDim*vertDiff, mDim ) ;
	  if ( h < Pbl->hMin ) { Pbl->hMin = h ; PcoMin = Pcoor ; }
	  if ( h > Pbl->hMax ) { Pbl->hMax = h ; PcoMax = Pcoor ; }
	}
	 
    /* Edges in j. */
    vertDiff = Pbl->mVert[0] ;
    loK = ( mDim == 2 ? 1 : loDegenFc[2] ? 2 : 1 ) ;
    hiK = ( mDim == 2 ? 1 : hiDegenFc[2] ? Pbl->mVert[2]-1 : Pbl->mVert[2] ) ;
    loJ = 1 ;
    hiJ = Pbl->mVert[1]-1 ;
    loI = ( loDegenFc[0] ? 2 : 1 ) ;
    hiI = ( hiDegenFc[0] ? Pbl->mVert[0]-1 : Pbl->mVert[0] ) ;

    for ( nK = loK ; nK <= hiK ; nK++ )
      for ( nJ = loJ ; nJ <= hiJ  ; nJ++ )
	for ( nI = loI ; nI <= hiI ; nI++ ) {
	  nVert = ((( nK-1 )*Pbl->mVert[1] ) + nJ-1 )*Pbl->mVert[0] + nI ;
	  Pcoor = Pbl->Pcoor + mDim*nVert ;
	  h = sq_distance_dbl( Pcoor, Pcoor + mDim*vertDiff, mDim ) ;
	  if ( h < Pbl->hMin ) { Pbl->hMin = h ; PcoMin = Pcoor ; }
	  if ( h > Pbl->hMax ) { Pbl->hMax = h ; PcoMax = Pcoor ; }
	}
	 
    /* Edges in k. */
    vertDiff = Pbl->mVert[0]*Pbl->mVert[1] ;
    loK = ( mDim == 2 ? 1 : 1 ) ;
    hiK = ( mDim == 2 ? 0 : Pbl->mVert[2]-1 ) ;
    loJ = ( loDegenFc[1] ? 2 : 1 ) ;
    hiJ = ( hiDegenFc[1] ? Pbl->mVert[1]-1 : Pbl->mVert[1] ) ;
    loI = ( loDegenFc[0] ? 2 : 1 ) ;
    hiI = ( hiDegenFc[0] ? Pbl->mVert[0]-1 : Pbl->mVert[0] ) ;

    for ( nK = loK ; nK <= hiK ; nK++ )
      for ( nJ = loJ ; nJ <= hiJ  ; nJ++ )
	for ( nI = loI ; nI <= hiI ; nI++ ) {
	  nVert = ((( nK-1 )*Pbl->mVert[1] ) + nJ-1 )*Pbl->mVert[0] + nI ;
	  Pcoor = Pbl->Pcoor + mDim*nVert ;
	  h = sq_distance_dbl( Pcoor, Pcoor + mDim*vertDiff, mDim ) ;
	  if ( h < Pbl->hMin ) { Pbl->hMin = h ; PcoMin = Pcoor ; }
	  if ( h > Pbl->hMax ) { Pbl->hMax = h ; PcoMax = Pcoor ; }
	}

    Pbl->hMin = sqrt( Pbl->hMin ) ; 
    Pbl->hMax = sqrt( Pbl->hMax ) ; 
    
    if ( verbosity > 4 )
      printf ( "       INFO: block %3d, hMin: %7g, hMax: %7g.\n",
	       Pbl->nr, Pbl->hMin, Pbl->hMax ) ;

    if ( hMinAllBlocks > Pbl->hMin ) { hMinAllBlocks = Pbl->hMin ; PcoMinAll = PcoMin ; }
    if ( hMaxAllBlocks < Pbl->hMax ) { hMaxAllBlocks = Pbl->hMax ; PcoMaxAll = PcoMax ; }
  }

  if ( verbosity > 4 ) {
    printf ( "   INFO: hMin: %9g at ( ", hMinAllBlocks ) ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	printf ( " %6g", PcoMinAll[nDim] ) ;
    printf ( " ).\n         hMax: %9g at ( ", hMaxAllBlocks ) ;
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      printf ( " %6g", PcoMaxAll[nDim] ) ;
    printf ( " ).\n" ) ;
  }
  else if ( verbosity > 2 )
    printf ( "   INFO: hMin: %9g, hMax: %9g.\n", hMinAllBlocks, hMaxAllBlocks ) ;
  if ( hMinAllBlocks <= Grids.epsOverlap )
    printf ( " WARNING: the current value of epsOverlap %g is less than the\n"
	     "          the smallest grid size %g.\n",
	     Grids.epsOverlap, hMinAllBlocks ) ;
  return ( 1 ) ;
}



/******************************************************************************

  mb_bb:
  Calculate a bounding box around a block
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pmb: the multi block grid

  Changes To:
  -----------
  Pmb->Pblocks->ll/urBox ;
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mb_bb ( mb_struct *Pmb ) {
  
  static block_struct *Pbl ;
  static int nDim, mDim ;
  static double *Pcoor ;
  mDim = Pmb->mDim ;
  
  if ( !Pmb )
    return ( 0 ) ;
  else if ( !Pmb->PblockS )
    return ( 0 ) ;
  
  for ( Pbl = Pmb->PblockS+1 ; Pbl <= Pmb->PblockS+ Pmb->mBlocks ; Pbl++ ) {
    /* Reset the box. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      Pbl->llBox[nDim] = TOO_MUCH ;
      Pbl->urBox[nDim] = -TOO_MUCH ; }

    /* OK, this is not nice. Looping over all exterior vertices would suffice.
       Fix this when you get around to. */
    for ( Pcoor = Pbl->Pcoor + mDim ;
	  Pcoor <= Pbl->Pcoor + mDim*Pbl->mVertsBlock ; Pcoor += mDim )
      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
	Pbl->llBox[nDim] = MIN ( Pbl->llBox[nDim], Pcoor[nDim] ) ;
	Pbl->urBox[nDim] = MAX ( Pbl->urBox[nDim], Pcoor[nDim] ) ;
      }
  }
      
  return ( 1 ) ;
}


/******************************************************************************

  get_static_subface:
  Get the static and running directions of a subface.
  
  Last update:
  ------------
  21Oct16; compiler found self comparision with Psf->llRBlock[nDim], must be ur
  : conceived.
  
  Input:
  ------
  Psf: subface.
  mDim: spatial dimensions.
  side: 0 for left, 1 for right.

  Changes To:
  -----------
  PstaticDim: the static dimension,
  PstaticDir: the static direction, -1 for -ijk, +1 for +ijk.
  PfreeDir1/2: the two running directions.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_static_subface ( const subFace_struct *Psf, const int mDim, const int side, 
			 int *PstaticDim, int *PstaticDir, int *PfreeDir1, int *PfreeDir2 ) {
  
  int nDim, foundStatic = 0, foundRunning1 = 0, returnVal = 1 ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    if ( side == 0 )
    { if ( Psf->llLBlock[nDim] == Psf->urLBlock[nDim] )
      { /* This is the static direction. */
	if ( foundStatic )
	{ printf ( " FATAL: found two static directions in get_static_subface.\n" ) ;
	  returnVal = 0 ;
	}
	
	foundStatic = 1 ;
	*PstaticDim = nDim ;
	if ( Psf->llLBlock[nDim] == 1 )
	  /* -ijk */
	  *PstaticDir = -1 ;
	else
	  *PstaticDir = 1 ;
      }
      else
      { /* This is a running direction. */
	if ( foundRunning1 )
	{ /* This is the second running direction. */
	  *PfreeDir2 = nDim ;
	}
	else
	{ /* This is the second running direction. */
	  *PfreeDir1 = nDim ;
	  foundRunning1 = 1 ;
	}
      }
    }

    else
    { if ( Psf->llRBlock[nDim] == Psf->urRBlock[nDim] )
      { /* This is the static direction. */
	if ( foundStatic )
	{ printf ( " FATAL: found two static directions in get_static_subface.\n" ) ;
	  returnVal = 0 ;
	}
	
	foundStatic = 1 ;
	*PstaticDim = nDim ;
	if ( Psf->llRBlock[nDim] == 1 )
	  /* -ijk */
	  *PstaticDir = -1 ;
	else
	  *PstaticDir = 1 ;
      }
      else
      { /* This is a running direction. */
	if ( foundRunning1 )
	{ /* This is the second running direction. */
	  *PfreeDir2 = nDim ;
	}
	else
	{ /* This is the second running direction. */
	  *PfreeDir1 = nDim ;
	  foundRunning1 = 1 ;
	}
      }
    }

  return ( returnVal ) ;
}

/******************************************************************************

  transpose_block:
  If a 2D block has all negative volumes, swap the coordinates.
  
  Last update:
  ------------
  6Jan15; rename pFamMb to more descriptive pArrFamMb
  2Apr05; fix allocation bug with VxVec.
  : conceived.
  
  Input:
  ------
  pBL: the block
  mDim: dimensions

  Changes To:
  -----------
  pBL->Pcoor
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int transpose_block ( block_struct *pBl, const int mDim ) {

  const int *mVert = pBl->mVert ;
  int nI, nJ, mV, nll, nul, nlr, nur, n, nT, nSubFace, ll[2] ;
  double *pCo[4], *pCoT, *pCoorT, *pCoN, vxVec[4][2], vol ;
  subFace_struct *pSf ;
  
  if ( mDim != 2 )
    return ( 1 ) ;

  mV = mVert[0]*mVert[1] ;

  for ( nJ = 1 ; nJ < mVert[1] ; nJ++ )
    for ( nI = 1 ; nI < mVert[0] ; nI++ ) {

      nll =  ( nJ-1)*mVert[0] + nI ;
      nul = nll + mVert[0] ;
      nlr = nll + 1 ;
      nur = nul + 1 ;
      
      /* Note that, as usual, the long element spaces that occur
         only once per chunk start with 1. However, all cyclical
         indices, like the vertices, start at 0. */
      pCo[0] = pBl->Pcoor + nll*mDim ;
      pCo[1] = pBl->Pcoor + nlr*mDim ;
      pCo[2] = pBl->Pcoor + nur*mDim ;
      pCo[3] = pBl->Pcoor + nul*mDim ;
  
      vxVec[1][0] = pCo[1][0] - pCo[0][0] ;
      vxVec[1][1] = pCo[1][1] - pCo[0][1] ;
      vxVec[2][0] = pCo[2][0] - pCo[0][0] ;
      vxVec[2][1] = pCo[2][1] - pCo[0][1] ;
      vxVec[3][0] = pCo[3][0] - pCo[0][0] ;
      vxVec[3][1] = pCo[3][1] - pCo[0][1] ;
      /* Split the quad along 0 - 2 to form a triangle 0 1 2 and 0 2 3. */
      vol  = vxVec[1][0]*vxVec[2][1] - vxVec[1][1]*vxVec[2][0] ;
      vol -= vxVec[3][0]*vxVec[2][1] - vxVec[3][1]*vxVec[2][0] ;

      if ( vol > 0. )
        /* Not all elements of this block are reversed. */
        return ( 1 ) ;
    }
  

  /* Transpose the nodes. In-situ transpose seems too complicated, just reallocate. */
  pCoorT = arr_malloc( "pCoor in swap_block", pArrFamMb, 
                       mDim*( mV+1 ), sizeof ( double )) ;
  
  for ( nJ = 1 ; nJ <= mVert[1] ; nJ++ )
    for ( nI = 1 ; nI <= mVert[0] ; nI++ ) {
      n = (nJ-1)*mVert[0] + nI ;
      nT = (nI-1)*mVert[1] + nJ ;

      pCoN = pBl->Pcoor  + mDim*n ;
      pCoT = pCoorT      + mDim*nT ;
      
      /* 2D! */
      pCoT[0] = pCoN[0] ;
      pCoT[1] = pCoN[1] ;
    }

  arr_free ( pBl->Pcoor ) ;
  pBl->Pcoor = pCoorT ;



  /* Transpose all subface entries relating to this block. */
  for ( nSubFace = 0 ; nSubFace < pBl->mSubFaces ; ++nSubFace ) {
    pSf = pBl->PPsubFaces[nSubFace] ;
    if ( pSf->PlBlock == pBl ) {
      ll[0]        = pSf->llLBlock[0] ;
      ll[1]        = pSf->llLBlock[1] ;
      pSf->llLBlock[0] = pSf->urLBlock[0] ;
      pSf->llLBlock[1] = pSf->urLBlock[1] ;
      pSf->urLBlock[0] = ll[0] ;
      pSf->urLBlock[1] = ll[1] ;
    }
    else if ( pSf->PrBlock == pBl ) {
      ll[0]        = pSf->llRBlock[0] ;
      ll[1]        = pSf->llRBlock[1] ;
      pSf->llRBlock[0] = pSf->urRBlock[0] ;
      pSf->llRBlock[1] = pSf->urRBlock[1] ;
      pSf->urRBlock[0] = ll[0] ;
      pSf->urRBlock[1] = ll[1] ;
    }
  }
    
  
  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  23Feb18: conceived.
  

  Input:
  ------
  pBl: block
  mDim: index dim
    
  Returns:
  --------
  the square of the minimum non-degenerate edge length.
  
*/

double get_block_hMinSq ( block_struct *pBl, const int mDim ) {

  /* Work out all interior edge lengths. */
  double hMinSq = TOO_MUCH, hSq ;
  int kk, offset ;
  int idx0, idx1, idx2, i0, i1, i2 ;
  int ijk[MAX_DIM] ;
  double *pCo0 ;
  for ( idx0 = 0 ; idx0 < mDim ; idx0++ ) {
    /* All edges in direction of idx0. Linear storage Offset 
       between coordinates in the iDim direction. */
    for ( kk = 0, offset= mDim ; kk < idx0 ; kk++ )
      offset *= pBl->mVert[kk] ;

    
    idx1 = (idx0+1)%mDim ;
    idx2 = (idx0+2)%mDim ;
    for ( i0 = 1 ; i0 < pBl->mVert[idx0] ; i0++ ) {
      ijk[idx0] = i0 ;
      for ( i1 = 2 ; i1 < pBl->mVert[idx1] ; i1++ ) {
        ijk[idx1] = i1 ;
        for ( i2 = 2 ; i2 < pBl->mVert[idx2] ; i2++ ) {
          ijk[idx2] = i2 ;
          pCo0 = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijk, pBl->mVert ) ;
          hSq = sq_distance_dbl ( pCo0, pCo0+offset, mDim ) ;
          if ( hSq < hMinSq ) {
            hMinSq = hSq ;
          }
        }
      }
    }
  }

  /* Edge lengths in the block faces, as long as not degenerate. */
  int nSubFace ;
  subFace_struct *pSf ;
  int ll[MAX_DIM], ur[MAX_DIM] ;
  int multVert1, multCell1 ;
  int multVert2, multCell2 ;
  int offsetVert, offsetCell, idxStatic ;
  int dll, dlr, dur, dul ;
  int ijk1[MAX_DIM] ;
  double *pCo1 ;
  for ( nSubFace = 0 ; nSubFace < pBl->mSubFaces ; ++nSubFace ) {
    pSf = pBl->PPsubFaces[nSubFace] ;
    if ( pSf->PrBlock || pSf->Pbc ) {
      /* This subface is not degenerate. */
      get_mb_subface ( pBl, pSf, mDim, ll, ur,
		       &idx1, &multVert1, &multCell1,
		       &idx2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &idxStatic,
		       &dll, &dlr, &dur, &dul ) ;

      /* Loop over all interior edges in this subface, i.e. in
         the idx1 and idx2 directions. */
      ijk[idxStatic] =  ijk1[idxStatic] = i0 = ll[idxStatic] ;
      idx1 = (idxStatic+1)%mDim ;
      idx2 = (idxStatic+2)%mDim ;
      for ( i1 = 1 ; i1 < pBl->mVert[idx1] ; i1++ ) {
        ijk[idx1] = ijk1[idx1] = i1 ;
        for ( i2 = 1 ; i2 < pBl->mVert[idx2] ; i2++ ) {
          ijk[idx2] = i2 ;
          ijk1[idx2] = i2+1 ;
          pCo0 = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijk, pBl->mVert ) ;
          pCo1 = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijk1, pBl->mVert ) ;
          hSq = sq_distance_dbl ( pCo0, pCo1, mDim ) ;
          if ( hSq < hMinSq ) {
            hMinSq = hSq ;
          }
        }
      }
    }
  }

  return ( hMinSq ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  get_mb_hMinSq:
*/
/*! Get mininmal non-degenerate edge length in a multi-block grid.
 *
 */

/*
  
  Last update:
  ------------
  25Feb18: conceived.
  

  Input:
  ------
  pMb: the multi-block grid

  Returns:
  --------
  hMinMbSq
  
*/

double get_mb_hMinSq  ( mb_struct *pMb ) {

  const int mDim = pMb->mDim ;
  block_struct *pBl = pMb->PblockS ;
  double hMinMbSq = TOO_MUCH ;
  int nBlock ;
  for ( nBlock = 1 ; nBlock <= pMb->mBlocks ; nBlock++ ) {
    hMinMbSq = MIN( hMinMbSq, get_block_hMinSq ( pBl+nBlock, mDim ) ) ;
  }

  return ( hMinMbSq ) ;
}


/******************************************************************************/

/* debug funs. */

#ifdef DEBUG

int printijkco ( const block_struct *Pbl, const int ijk[] )
{ /* Print the coordinates of node ijk in block Pbl. */
  int mDim = 3 ;
  int nVert = ((( ijk[2]-1 )*Pbl->mVert[1] ) + ijk[1]-1 )*Pbl->mVert[0] + ijk[0] ;
  double *Pcoor = Pbl->Pcoor + mDim*nVert ;

  printf ( "(%d,%d,%d) %d (%d): %g, %g %g\n",
	   ijk[0], ijk[1], ijk[2], nVert, Pbl->nr,
	   Pcoor[0], Pcoor[1], Pcoor[2] ) ;

  return (1) ;
}

int printijkT ( const block_struct *Pbl, const int ijk[], const subFace_struct *Psf )
{ /* Print the coordinates of the node incident to ijk in block Pbl on the
     other side of Psf. */
  int nDim, mDim = 3, ijkO[3] ;
  /* Which side is Pbl in Psf. */
  if ( Psf->PlBlock == Pbl )
  { /* Left side. */
  
    /* Check whether ijk is actually on Psf. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      if ( Psf->llLBlock[nDim] > ijk[nDim] || Psf->urLBlock[nDim] < ijk[nDim] )
	printf ( " FATAL: ijk not on the subface in dim %d\n", nDim ) ;

    printijkco ( Pbl, ijk ) ;

    if ( Psf->ProtL2R )
    { trans_l2r ( ijk, Psf->ProtL2R->rotMatrix, Psf->vertShift, ijkO ) ;
      printijkco ( Psf->PrBlock, ijkO ) ;
    }
    else
      printf ( " no rot on this face.\n" ) ;
  }
  else if ( Psf->PrBlock == Pbl )
  { /* Right side. */
  
    /* Check whether ijk is actually on Psf. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      if ( Psf->llRBlock[nDim] > ijk[nDim] || Psf->urRBlock[nDim] < ijk[nDim] )
	printf ( " FATAL: ijk not on the subface in dim %d\n", nDim ) ;

    printijkco ( Pbl, ijk ) ;

    if ( Psf->ProtL2R )
    { trans_r2l ( ijk, Psf->ProtL2R->rotMatrix, Psf->vertShift, ijkO ) ;
      printijkco ( Psf->PlBlock, ijkO ) ;
    }
    else
      printf ( " no rot on this face.\n" ) ;
  }
  else
    printf ( "FATAL: ijk not on Psf.\n" ) ;

  return ( 1 ) ;
}
	  
int printdco ( const block_struct *Pbl1, const int ijk1[],
	       const block_struct *Pbl2, const int ijk2[] )
{
  int mDim = 3 ;
  double diff ;
  int nVert1 = ((( ijk1[2]-1 )*Pbl1->mVert[1] ) + ijk1[1]-1 )*Pbl1->mVert[0] + ijk1[0] ;
  double *Pcoor1 = Pbl1->Pcoor + mDim*nVert1 ;
  int nVert2 = ((( ijk2[2]-1 )*Pbl2->mVert[1] ) + ijk2[1]-1 )*Pbl2->mVert[0] + ijk2[0] ;
  double *Pcoor2 = Pbl2->Pcoor + mDim*nVert2 ;

  printf ( "(%d,%d,%d) %d (%d): %g, %g %g\n",
	   ijk1[0], ijk1[1], ijk1[2], nVert1, Pbl1->nr,
	   Pcoor1[0], Pcoor1[1], Pcoor1[2] ) ;
  
  printf ( "(%d,%d,%d) %d (%d): %g, %g %g\n",
	   ijk2[0], ijk2[1], ijk2[2], nVert2, Pbl2->nr,
	   Pcoor2[0], Pcoor2[1], Pcoor2[2] ) ;

  diff = sqrt( sq_distance_dbl( Pcoor1, Pcoor2, mDim ) ) ;
  printf ( " diff: %g\n" , diff ) ;

  return ( 1 ) ;
}

#endif
