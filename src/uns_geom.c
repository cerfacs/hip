/*
   uns_meth.c
   Unstructured grid methods.
 
   Last update:
   ------------
   19Mar17; compute actual gravity centre in elem_is_convex.
  16Feb17, intro elem_is_convex
  8Apr13; use dereferenced pointer in sizeof when allocating.
   16Dec11; move add_hrb here from meth.c
   28Aug11; move transform to uns_trans_rot.c
   22Dec10; intro bcPatch_nrm_gc
   18Dec10; new pBc->type
   15Sep06; intro get_face_dist, x_tri, x_face.
   17May06; intro make_mp_bndVx
   6Jan05; complete connectivity reflection in transform.
   4Feb4; adjust ll/urBox in transform.
   9Apr3; intro singleBndVxNormal, transform.
   28Jun00; use two rotational copies for the wall distance, ensure periodicity.
   10Apr00; make calc_wall_distance periodic.
   27Sep99; intro uns_face_normal_vx.
   17Jun99; change make_bndVxNormals, intro make_bndFcNormals.
   19Jan98; intro get_faceTwist.
   19Oct97; copy PPbc in append_chunk from the root chunk.
   6May96: move make_chunk here from mb_2uns.
   2May96; conceived.

   Contains:
   ---------
   get_unsBox:
   get_chunkBox:
   get_uns_face:
   uns_elem_normls:
   bcPatch_nrm_gc
   uns_face_normal:
   uns_face_normal_vx:
   get_faceTwist:
   get_elem_vol:
   get_elem_lrgstAngle:

   add_hrb
   is_elem_in_hrb:
   mark_elems_in_hrb:
   
   elem_grav_ctr:
   face_grav_ctr:
   edge_grav_ctr
   med_normal_edge:

   face_in_elem:
   
   cmp_bndVx:
   make_bndVxNormals:

   bndNorm2coor:
   calc_wall_dist:
*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"
#include "math.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern const double normAnglCut ;
extern const int symmCoor ;
extern const int singleBndVxNormal ;
extern const double para_tol ;
extern const double x_fac ;
extern double  mgVolMinSub ;

extern const int negVol_flip ;

# define CRPRD3(Px,Py,Pz) { (Pz[0]) = (Px[1])*(Py[2]) - (Px[2])*(Py[1]) ;\
                            (Pz[1]) = (Px[2])*(Py[0]) - (Px[0])*(Py[2]) ;\
                            (Pz[2]) = (Px[0])*(Py[1]) - (Px[1])*(Py[0]) ;}


int cmp_bndNorm ( const void* pBn0, const void* pBn1 ) {
  return ( (( bndNorm_s * ) pBn0)->pVx->number - (( bndNorm_s * ) pBn1)->pVx->number ) ;
}

/******************************************************************************

  Copyright Jens-Dominik Mueller and CERFACS, 
  see the CECILL copyright notice at the top of the file.
  Author: Jens-Dominik Mueller
  contributor(s) : Gabriel Staffelbach

  "Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
  "Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fill_vx_nr2:
*/
/*! Make a list of global node numbers before renumbering locally.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  8may24: use realloc and separate zero, rather than calloc, 
          as we don't know whether the field is already allocated.
  20Dec16: derived from fill_chunk_vrtxNr2.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void attach_chunk_vrtxVol ( uns_s *pUns ) {

  /* Alloc a vertex volume field for each chunk. Fill that field with the current
     node numbering. Check cptVx pointers are correct for each vx. */
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVxBeg, *pVxEnd ;
  int nBeg, nEnd, nVol ;
  vrtx_struct *pVx ;
  double *pVV ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
    pChunk->pVrtxVol = arr_realloc ( "pNr2 in fill_vx_nr2", pUns->pFam, 
                                     pChunk->pVrtxVol, pChunk->mVerts+1,
                                     sizeof(*(pChunk->pVrtxVol) ) ) ;

    pVV = pChunk->pVrtxVol ;
    *pVV++ = 0. ;  // zero also first, unused entry.
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++, pVV++ ) {
      *pVV = 0. ;
      if ( pVx->vxCpt.nCh != pChunk->nr || pChunk->Pvrtx + pVx->vxCpt.nr != pVx )
        hip_err ( fatal, 0, "erroneous cptVx in attach_chunk_vrtxVol." ) ;
    }
  }
  
  return ;
}

/******************************************************************************

  Copyright Jens-Dominik Mueller and CERFACS, 
  see the CECILL copyright notice at the top of the file.
  Author: Jens-Dominik Mueller
  contributor(s) : Gabriel Staffelbach

  "Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
  "Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  compute_vx_volume:
*/
/*! Compute the volume at nodes by scattering the element volume equally to the vx.
 *
 */

/*
  
  Last update:
  ------------
  8May24; call attach_chunk_vrtxvol from w/in compute_vrtxvol.
  22Feb18; intro nVar.
  20Dec16: conceived.
  

  Input:
  ------
  pUns
  nVar: if non-zero, write as n-th variable pVar[nVar-1],
        otherwise write to arrays allocated with each chunk.

  Changes To:
  -----------
  pUns->ppChunk[]->pVrtxVol = the special allocated field of nodal volumes.

  Returns:
  --------
  the volume of the domain.
  
*/

double compute_vrtxVol ( uns_s *pUns, int kVar ) {

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  vrtx_struct *pVx ;
  if ( kVar < 0 ) {
    // Store with pChunk->PvrtxVol 
    attach_chunk_vrtxVol ( pUns ) ;
  }
  else {
    // Zero all vertex volume unknowns.
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        pVx->Punknown[kVar] = 0. ;
    }
  }


    
  /* Compute element vols and scatter. */  
  elem_struct *pElBeg, *pElEnd, *pEl ;
  double elVolVx ; // fraction of the element volume assigned to each vx.
  int k ;
  cpt_s cpVx ;
  double *pVxVol ; // nodal volume, sum of the element contributions.
  double domainVol = 0. ;

  pChunk = NULL ;
  static const elemType_struct *pElT ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {
      if ( !pEl->invalid || pEl->number ) {
        pElT = elemType + pEl->elType ;
        elVolVx = get_elem_vol ( pEl ) ;
        domainVol += elVolVx ;
        elVolVx /= pElT->mVerts ;

        for ( k = 0 ; k < pElT->mVerts ; k++ ) {
          pVx = pEl->PPvrtx[k] ;
          if ( kVar<0 ) {
            // write to chunk array indexed b cptVx.
            cpVx = pVx->vxCpt ;
            pVxVol = pUns->ppChunk[cpVx.nCh]->pVrtxVol + cpVx.nr ;
            *pVxVol += elVolVx ;
          }
          else {
            // Store as var kVar.            
            pVx->Punknown[kVar] += elVolVx ;
          }
        }
      }
    }
  }
  return ( domainVol ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  edgLen_from_vol:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  19Sep21; add correction to volFac due to difference in numbers of elems vs nodes
  22Feb19: conceived.
  

  Input:
  ------
  pUns
  kVar: var pos of edge length.
  mDim: if not one, current unknown is nodal volume in mDim dimensions.
  

  Changes To:
  -----------
  Punknown[kVar]: conversion in-situ of volume to avg iso length.

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int edgeLen_from_vol ( uns_s *pUns, const int kVar, const int mDim ) {

  if ( kVar >= pUns->varList.mUnknowns )
    hip_err ( fatal, 0, "not enough variables in edgeLen_from_vol" ) ;
  else if ( mDim == 1 )
    // Nothing needs doing.
    return ( 0 ) ;
  
  // Equilateral tet: V = a^3\sqrt(2)/12; tri:: A = a^2\sqrt(3)/4
  // Correct for the number of nodes vs elements.
  const double volFac = ( mDim == 3 ? 8.4853 : 2.3094)*pUns->mVertsNumbered/pUns->mElemsNumbered ;

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        pVx->Punknown[kVar] = pow(volFac*pVx->Punknown[kVar],1.0/mDim) ;
      }
    
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_avg_edgeLen:
*/
/*! Calculate min, avg or max edge lengths over all edges formed with each node.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  19Sep21: conceived.
  

  Input:
  ------
  pUns
  kVar: var pos of edge length.
  operation: one of min, avg, max

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s calc_edgeLen ( uns_s *pUns, const int kVar, const char operation[] ) {
#undef FUNLOC
#define FUNLOC "in calc_avg_edgeLen"
  
  ret_s ret = ret_success () ;

  char oper ;
  if ( !strncmp( "min", operation, 2 ) )
    oper = 'n' ;
  else if ( !strncmp( "max", operation, 2 ) )
    oper = 'x' ;
  else if ( !strncmp( "avg", operation, 2 ) )
    oper = 'a' ;
  else {
    ret.status = warning ;
    hip_err ( warning, 1, "unrecognised operation type in calc_edgeLen" ) ;
    return (ret) ;
  }
    

  // Allocate the number of edges formed with each node.
  int *mEgNode = arr_calloc ( "mEgNode "FUNLOC".", pUns->pFam,
                              pUns->mVertsNumbered+1, sizeof( *mEgNode ) ) ;


  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVxBeg, *pVx, *pVxEnd ;
  int nBeg, nEnd ;
  cpt_s cpVx ;
  double h, *pLen ;
  // Initialise to high value to retain the minimum.
  if ( oper == 'n' ) {
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number ) {
          if ( kVar<0 ) {
            cpVx = pVx->vxCpt ;
            pLen = pUns->ppChunk[cpVx.nCh]->pVrtxVol + cpVx.nr ;
          }
          else {
            pLen = pVx->Punknown + kVar ;
          }
          *pLen = TOO_MUCH ;
        }
    }
  }


   
  /* Compute element edge lengths and scatter. */  
  elem_struct *pElBeg, *pElEnd, *pEl ;
  const elemType_struct *pElT ;
  int kEg ;
  const edgeOfElem_struct *pEoE ;
  vrtx_struct *pVxEg[2] ;
  int k ;
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {
      if ( !pEl->invalid || pEl->number ) {
        pElT = elemType + pEl->elType ;

        for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
          pEoE = pElT->edgeOfElem ;
          pVxEg[0] = pEl->PPvrtx[ pEoE[kEg].kVxEdge[0] ] ;
          pVxEg[1] = pEl->PPvrtx[ pEoE[kEg].kVxEdge[1] ] ;
 
          if ( pVxEg[0] != pVxEg[1] ) {
            /* Non-collapsed edge. */
            h = sqrt(sq_distance_dbl ( pVxEg[0]->Pcoor, pVxEg[1]->Pcoor, pElT->mDim ) ) ;

            for( k = 0 ; k < 2 ; k++ ) {
              mEgNode[ pVxEg[k]->number ]++ ;

              if ( kVar<0 ) {
                // write to chunk array indexed b cptVx.
                cpVx = pVxEg[k]->vxCpt ;
                pLen = pUns->ppChunk[cpVx.nCh]->pVrtxVol + cpVx.nr ;
              }
              else
                pLen = pVxEg[k]->Punknown+kVar ;
              
              switch ( oper ) {
              case 'a' : // avg
                *pLen += h ;
                break ;
              case 'n' : // min
                *pLen = MIN( *pLen, h ) ;
                break ;
              case 'x' : // max
                *pLen = MAX( *pLen, h ) ;
                break ;
              }
            }
          }
        }
      }
    }
  }


  if ( oper == 'a' ) {
    /* Averaging, scale. */
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number ) {
          if ( kVar<0 ) {
            cpVx = pVx->vxCpt ;
            pLen = pUns->ppChunk[cpVx.nCh]->pVrtxVol + cpVx.nr ;
          }
          else {
            pLen = pVx->Punknown+kVar ;
          }
          *pLen /=  mEgNode[ pVx->number ] ;
        }
    }
  }


  arr_free ( mEgNode ) ;
  

  return ( ret ) ;
}


/******************************************************************************

  Copyright Jens-Dominik Mueller and CERFACS, 
  see the CECILL copyright notice at the top of the file.
  Author: Jens-Dominik Mueller
  contributor(s) : Gabriel Staffelbach

  "Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
  "Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  free_vrtxVol:
*/
/*! Deallocate vrtxVol double field with every chunk.
 */

/*
  
  Last update:
  ------------
  20Dec176: derived from free_chunk_vrtxNr2.
  

  Input:
  ------
  pUns

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void free_chunk_vrtxVol ( uns_s *pUns ) {
  
  chunk_struct *pChunk = NULL ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    if ( pChunk->pVrtxVol ) {
      arr_free ( pChunk->pVrtxVol ) ;
      pChunk->pVrtxVol = NULL ;
    }
  }

  return ;
}



/******************************************************************************

  x_lin:
  Calculate the intersection between two line segments.
  
  Last update:
  ------------
  10Nov17; new interface: returnvalues for cases and return *ps.
  : conceived.
  
  Input:
  ------
  pCo[2][]:    the coordinates of the vertices on the edge (first line) along t
  *pBeg, *pEnd: beginning and end coordinates of the second line along s.

  Changes To:
  -----------
  *pt:         the length along the edge/first line.
  
  Returns:
  --------
  0 if lines are parallel
  1 if intersecting, with 0 <= s,t <= 1
  2 if intersecting, with 0 <= t <= 1
  3 if intersecting, with 0 <= s, <= 1
  4 if intersecting
  
*/

int x_lin ( const double *pEg0, const double *pEg1,  double *pt,
            const double *pLn0, const double *pLn1,  double *ps ) {

  int retVal = 0 ;
  double x0,y0, dx1,dy1, dx2, dy2, xb, yb, det, s, t ;

  x0 = pEg0[0] ;
  y0 = pEg0[1] ;

  dx1 = pEg1[0] - x0 ;
  dy1 = pEg1[1] - y0 ;

  xb = pLn0[0] ;
  yb = pLn0[1] ;

  dx2 = pLn1[0] - xb ;
  dy2 = pLn1[1] - yb ;

  det = dy1*dx2 - dx1*dy2 ;

  if ( ABS( det ) < para_tol*Grids.epsOverlap )
    /* Line is parallel to face. */
    return ( 0 ) ;

  s = ( dy1*( x0-xb ) - dx1*( y0-yb ) )/det ;
  if ( ABS( dx1 ) > ABS( dy1 ) )
    t = ( s*dx2 - ( x0-xb) )/dx1 ;
  else
    t = ( s*dy2 - ( y0-yb) )/dy1 ;


  if ( 0. <= s && s <= 1. &&
       0. <= t && t <= 1. )
    /* Intersection on the edge and on the line. */
    retVal = 1 ;
  else if ( 0. <= t && t <= 1. )
    /* Intersection on the edge. */
    retVal = 2 ;
  else if ( 0. <= s && s <= 1. )
    /* Intersection on the line. */
    retVal = 3 ;
  else
    retVal = 4 ;
    
  *pt = t ;
  *ps = s ;
  return ( retVal ) ;
}

/******************************************************************************

  x_line3d:
  Intersect two lines in 3d.
  
  Last update:
  ------------
  7Jul4: conceived.
  
  Input:
  ------
  a0,a1 = beginning/end of 1st line
  b0,b1 = beginning/end of 2nd line

  Changes To:
  -----------
  *ps = weight of a1 in x = (1-*pt)*a0+*pt*a1.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int x_line3d ( double a0[], double a1[], double b0[], double b1[], 
               double *ps, double *pt ) {

  double a, b, c, d, det, e, f, s, t, xs, xt ;

  /* Write the system in matrix form as 
     ( dax dbx ) * ( s ) = ( b0x - a0x )
     ( day dby )   ( t ) = ( b0y - a0y )
     ( daz dbz )         = ( b0z - a0z )
     with 
     dax = a1[0] - a0[0] ... 

     Use the shorthand
     (   a   b ) * ( s ) = ( e )
     (   c   d )   ( t ) = ( f )
  */
    
  a = a1[0] - a0[0] ;
  b = b1[0] - b0[0] ;
  c = a1[1] - a0[1] ;
  d = b1[1] - b1[1] ;

  det = a*d - c*b ;

  if ( ABS( det ) < para_tol*Grids.epsOverlap )
    /* Lines are parallel, can't intersect. */
    return ( 0 ) ;

  e = b0[0] - a0[0] ;
  f = b0[1] - a0[1] ;
      
  s = ( d*e - b*f )/det ;

  if ( s < 0. || s > 1 )
    /* Intersection, but not within the finite length of the line a. */
    return ( 0 ) ; 

  t   = ( -c*e + a*f )/det ;
  if ( t < 0. || t > 1 )
    /* Intersection, but not within the finite length of the line b. */
    return ( 0 ) ; 

  /* Verify 3rd equ. */
  xs = a0[2] + s*( a1[2] - a0[2] ) ; 
  xt = b0[2] + t*( b1[2] - b0[2] ) ;

  if ( ABS( xs - xt ) < x_fac*Grids.epsOverlap ) {
    /* Acceptable match. */
    *ps = s ;
    *pt = t ;
    return ( 1 ) ;
  }
  else
    return ( 0 ) ;
}


/******************************************************************************

  x_tri:
  Calculate the intersection between a triangular face and a line.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pCo[3][]:    the coordinates of the vertices of the face
  pBeg,pEnd[3]: beginning and end of the line.

  output:
  -----------
  xInt:        the intersection with the triangular face, if any.
               If the line is in the plane of the face, xInt is taken as the centroid.
  
  Returns:
  --------
  1 if intersecting, 0 otherwise.
  
*/

int x_tri ( const double *pCo[3], const double *pBeg, const double *pEnd, 
            double xInt[3], double al[3] ) {

  const int mDim = 3 ;

  double x0, xb, dx1, dx2, dxe, y0, yb, dy1, dy2, dye, z0, zb, dz1, dz2, dze, det,
    al1, al2, al3, l1[3], len ;
  int retVal = 0 ;

  x0 = pCo[0][0] ;
  y0 = pCo[0][1] ;
  z0 = pCo[0][2] ;

  dx1 = pCo[1][0] - x0 ;
  dy1 = pCo[1][1] - y0 ;
  dz1 = pCo[1][2] - z0 ;

  dx2 = pCo[2][0] - x0 ;
  dy2 = pCo[2][1] - y0 ;
  dz2 = pCo[2][2] - z0 ;

  xb = pBeg[0] ;
  yb = pBeg[1] ;
  zb = pBeg[2] ;

  vec_diff_dbl ( pEnd, pBeg, mDim, l1 ) ;
  len = vec_norm_dbl ( l1, mDim ) ;

  dxe = l1[0] ;
  dye = l1[1] ;
  dze = l1[2] ;

  det = -dx1*dz2*dye - dx2*dy1*dze + dx2*dz1*dye + 
         dx1*dze*dy2 + dxe*dy1*dz2 - dxe*dz1*dy2 ;



  if ( ABS( det ) < para_tol*Grids.epsOverlapSq )
    /* Line is parallel to face. Can't intersect. */
    return ( 0 ) ;



  /* Intersection somewhere in space, using Cramer's rule. */
  al1 = ( dz2*dye - dze*dy2)*x0 + ( dx2*dze - dxe*dz2)*y0 + (-dx2*dye + dxe*dy2)*z0
      + (-dz2*dye + dze*dy2)*xb + ( dxe*dz2 - dx2*dze)*yb + ( dx2*dye - dxe*dy2)*zb ;
    
  al2 = (-dz1*dye + dy1*dze)*x0 + ( dxe*dz1 - dx1*dze)*y0 + (-dxe*dy1 + dx1*dye)*z0
      + (-dy1*dze + dz1*dye)*xb + ( dx1*dze - dxe*dz1)*yb + (-dx1*dye + dxe*dy1)*zb ;
    
  al3 = ( dy1*dz2 - dy2*dz1)*x0 + (-dx1*dz2 + dx2*dz1)*y0 + (-dx2*dy1 + dx1*dy2)*z0
      + (-dy1*dz2 + dy2*dz1)*xb + ( dx1*dz2 - dx2*dz1)*yb + ( dx2*dy1 - dx1*dy2)*zb ;

  al1 /= det ;
  al2 /= det ;
  al3 /= det ;
    
    
  if ( al1 + al2 <= 1. && al1 >= 0. && al2 >= 0. && al3 <= len && al3 >= 0. ) {
    /* Intersecton. */
    xInt[0] = x0 + al1*dx1 + al2*dx2 ;
    xInt[1] = y0 + al1*dy1 + al2*dy2 ;
    xInt[2] = z0 + al1*dz1 + al2*dz2 ;
      
    retVal = 1 ;
  }

  al[0] = 1. - al1 - al2 ;
  al[1] = al1 ;
  al[2] = al2 ;
  return ( retVal ) ;
}

/******************************************************************************

  lin_2:
  Compute a point given the end vertices and weights.
  
  Last update:
  ------------
  15Sep06: derived from add_2_face.
  
  Input:
  ------
  pCo: coordinates of the end points. 
  t:   weight of the second point.

  Changes To:
  -----------
  pX:  the linear combination.
*/


void lin_2 ( const double *pCo0, const double *pCo1, const int mDim, 
             const double t, double *pX ) {

  double wt[MAX_VX_FACE] ;
  int k ;


  wt[0] = 1.-t ;
  wt[1] = t ;
  for ( k = 0 ; k < mDim ; k++ )
    pX[k] = wt[0]*pCo0[k] + wt[1]*pCo1[k]  ;

  return ;
}

/******************************************************************************

  point_dist_tri: Given a the coor of a tri face, the coordinates of a
  point, and the coeff al of the two edge vectors to a point, compute
  the min dist to that point.

  See also:
  eberly99distance_point_triangle_3d.pdf
  jones95_3d_distance_point_triangle.pdf

  
  Last update:
  ------------
  19Feb18; fix c&p bug with wrong indices for al[] when computing xPlane.
  11Nov17; fix erroneous calculation for cases along edge.
  15Sep06: conceived.
  
  Input:
  ------
  pVxCo: point coor
  pFcCo: face coor
  al[]:  the point pVxCo is reached by taking pFcCo[0] + al[1]*de1 + al[2]*de2
         where de1/2 are the vectors along the edge from 0 to 1, 0 to 2 resp.
  
  Returns:
  --------
  the min distance to either the face, an edge of the face or a corner.
  
*/

double point_dist_tri ( const double *pVxCo, const double *pFcCo[3], 
                        double al[3] ) {
    
  double d ;
  double xPlane[3] ;
  int k, kp1, kp2 ;
  double vecEg[3], egLenSq ;
  double vecPoint[3] ;
  double sc ;
  double t ;
  double xEg[3] ;

  
  /* Then corners. If both other weights are negative, corner must be the closest. */
  if ( al[0] <= 0. && al[1] <= 0. ) 
    /* Vx2 is closest. */
    d = sqrt( sq_distance_dbl ( pVxCo, pFcCo[2], 3 ) ) ;
  else if ( al[1] <= 0. && al[2] <= 0. )
    /* Vx0 is closest. */
    d = sqrt( sq_distance_dbl ( pVxCo, pFcCo[0], 3 ) ) ;
  else if  ( al[2] <= 0. && al[0] <= 0. )
    /* Vx1 is closest. */
    d = sqrt( sq_distance_dbl ( pVxCo, pFcCo[1], 3 ) ) ;

  else if ( al[0] >= 0. && al[1] >= 0. && al[2] >= 0. ) {
    /* Compute the intersection point in the plane. */
    xPlane[0] = al[0]*pFcCo[0][0] + al[1]*pFcCo[1][0] + al[2]*pFcCo[2][0] ;
    xPlane[1] = al[0]*pFcCo[0][1] + al[1]*pFcCo[1][1] + al[2]*pFcCo[2][1] ;
    xPlane[2] = al[0]*pFcCo[0][2] + al[1]*pFcCo[1][2] + al[2]*pFcCo[2][2] ;

    d = sqrt( sq_distance_dbl ( pVxCo, xPlane, 3 ) ) ;
  }

  else {
    
    /* Edges. */
    if ( al[0] < 0. ) {
      /* Closest point is along edge 0. */
      k = 0;
      kp1 = 1 ;
      kp2 = 2 ;
    }
    else if ( al[1] < 0. ) {
      /* Closest point is along edge 1. */
      k = 1;
      kp1 = 2 ;
      kp2 = 0 ;
    }
    else { // if ( al[2] < 0. ) {
      /* Closest point is along edge 2. */
      k = 2;
      kp1 = 0 ;
      kp2 = 1 ;
    }


    /* Find the distance along edge k. */
    /* Edge vector. */
    vec_diff_dbl ( pFcCo[kp2], pFcCo[kp1], 3, vecEg ) ;
    egLenSq = vec_len_dbl_sq ( vecEg, 3 ) ;
    /* First corner to point. */
    vec_diff_dbl ( pVxCo, pFcCo[kp1], 3, vecPoint ) ;
    /* Angle. */
    sc  = scal_prod_dbl ( vecPoint, vecEg, 3 ) ;
    /* fraction of edge from first corner to footpoint on edge. */
    t = sc/egLenSq ;

    if ( t < 0. )
      /* First corner is closest. */
      d = sqrt( sq_distance_dbl ( pFcCo[kp1], pVxCo, 3 ) ) ;
    else if ( t > 1. )
      /* Second corner is closest. */
      d = sqrt( sq_distance_dbl ( pFcCo[kp2], pVxCo, 3 ) ) ;
    else {
      /* Closest point is on the edge. Construct it. */
      vec_add_mult_dbl ( pFcCo[kp1], t, vecEg, 3, xEg ) ;
      d = sqrt( sq_distance_dbl ( xEg, pVxCo, 3 ) ) ;
    }
  }
  
  return ( d ) ;
}
  
/******************************************************************************

  point_dist_face:
  Compute the distance from a point to a face.
  
  Last update:
  ------------
  31Aug12; fix bug with missing call to x_tri for tri face.
  15Sep06: derived from line_x_face.
  
  Input:
  ------
  pEl:
  kFc:         element and face
  pVxCo:       coordinates of the point

  Output:
  ------- 
  *phEdge:     length of the longest edge around the face.
  
  Returns:
  --------
  the distance to the point. 
  
*/

double point_dist_face ( const elem_struct *pEl, const int kFc, 
                         const double *pVxCo, double *phEdge ) {
  
  const elemType_struct *pElT = elemType + pEl->elType ;
  int mDim = pElT->mDim, mTimesNormal ;

  const double *pCoFc[MAX_VX_FACE+1] ;
  const double *pCo[MAX_VX_FACE] ;
  double  xInt[2][3],
    fcNorm[MAX_DIM], h, hMax, xEnd[MAX_DIM], sc, vec1[MAX_DIM], al[3], 
    d = TOO_MUCH, d1, d0, 
    x[MAX_DIM] ;
  int mVxFc, k ;

  /* Compute the face normal. */
  uns_face_normal_list ( pEl, kFc, &mVxFc, pCoFc, fcNorm, &mTimesNormal ) ;
  vec_norm_dbl ( fcNorm, mDim ) ;



  /* Compute the edge lengths. */
  if ( mDim == 2 )
    *phEdge = sqrt( sq_distance_dbl ( pCoFc[0], pCoFc[1], 2 ) ) ;
  else {
    /* Complete cyclic succession. */
    pCoFc[mVxFc] = pCoFc[0] ;
    hMax = 0. ;
    for ( k = 0 ; k < mVxFc ; k++ ) {
      h = sq_distance_dbl ( pCoFc[k], pCoFc[k+1], 3 ) ;
      hMax = MAX( h, hMax ) ;
    }
    *phEdge = sqrt( hMax ) ;
  }

  /* Generate a line through the point parallel to the normal
     that crosses the plane of the face. */
  //vec_diff_dbl ( pVxCo, pCoFc[0], mDim, vec1 ) ;
  // sc = scal_prod_dbl ( vec1, fcNorm, mDim ) ;
  vec_add_mult_dbl ( pVxCo, -2., fcNorm, mDim, xEnd ) ;
  
  double t, s ;
  int rVal ;
  if ( mVxFc == 2 ) {
    /* 2D edge/face. */
    rVal = x_lin ( pCoFc[0], pCoFc[1], &t, pVxCo, xEnd, &s ) ;
    if ( rVal > 0 && rVal < 3 ) {
      lin_2 ( pCoFc[0], pCoFc[1], 2, t, x ) ;
      d = sqrt( sq_distance_dbl ( pVxCo, x, 2 ) ) ;
    }
    else if ( t < 0. ) 
      /* Vx 0 is closest. */
      d = sqrt( sq_distance_dbl ( pVxCo, pCoFc[0], 2 ) ) ;
    else if ( t > 1. ) 
      /* Vx 1 is closest. */
      d = sqrt( sq_distance_dbl ( pVxCo, pCoFc[1], 2 ) ) ;
    else
      hip_err ( fatal, 0, "this shouldn't have happened in point_dist_face" ) ;
  }

  else if ( mVxFc == 3 ) {
    /* Triangular face. */
    pCo[0] = pCoFc[0] ;
    pCo[1] = pCoFc[1] ;
    pCo[2] = pCoFc[2] ;

    x_tri ( pCo, pVxCo, xEnd, xInt[0], al ) ;
    d = point_dist_tri ( pVxCo, pCo, al ) ;
  }

  else {
    /* Quad. Split into triangles. */
    pCo[0] = pCoFc[0] ;
    pCo[1] = pCoFc[1] ;
    pCo[2] = pCoFc[2] ;
    x_tri ( pCo, pVxCo, xEnd, xInt[0], al ) ;
    d0 = point_dist_tri ( pVxCo, pCo, al ) ;
      
    pCo[0] = pCoFc[2] ;
    pCo[1] = pCoFc[3] ;
    pCo[2] = pCoFc[0] ;
    x_tri ( pCo, pVxCo, xEnd, xInt[1], al ) ;
    d1 = point_dist_tri ( pVxCo, pCo, al ) ;

    d = MIN( d1, d0 ) ;
  }

  return ( d ) ;
}


/******************************************************************************
  min_dist_face:   */

/*! Compute the smallest distance to any face in an element if 
 *  it can be smaller than a given, previous, minimal distance.
 */

/*
  
  Last update:
  ------------
  21Feb16; fix bug with logic.
  18Dec15; fix bug with negative distances, place ABS.
  16Jul15; make bb search more effective, search for all outward faces.
  10Jul15: conceived.
  

  Input:
  ------
  pCo:     coordinate to search for
  pElem:   element to test for.    
  

  Changes To:
  -----------
  minDist: current minimal distance of pCo to an element.
           Unchanged if no closer one was found. Zero if the element contains pCo.
  pElMin:  element with minimal distance, or containing element.
           Unchanged if no closer one was found.
  kFc:     outward pointing face with minimal distance in pElMin
           0 if contained. Unchanged if no closer one was found.
    
  Returns:
  --------
  1 if a closer face was found, 0 otherwise.
  
*/

int
min_dist_face_el ( const double *pCo, const elem_struct *pElem, 
                   double *pMinDist, const elem_struct **ppElMin, int *pkFc ) {

  if ( *pMinDist == 0. )
    /* Either bad init of minDist, or previously a containing element was found. 
       Can't do better than that. */
    return (0) ;

  /* Bounding box of the element. */
  const int mVerts = elemType[pElem->elType].mVerts ;
  const int mDim = elemType[pElem->elType].mDim ;
  const vrtx_struct **ppVx = (const vrtx_struct ** ) pElem->PPvrtx ;
  int nDim, kVrtx ;
  double bbll, bbur ;
  double dist, minDistEl = TOO_MUCH ;
  int insideBB = 1 ;
  /* Find a bounding box for this element. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) { 
    bbur = bbll = pElem->PPvrtx[0]->Pcoor[nDim] ;
 
    for ( kVrtx = 1 ; kVrtx < mVerts ; kVrtx++ ) {
      bbll = MIN( bbll, ppVx[kVrtx]->Pcoor[nDim] ) ;
      bbur = MAX( bbur, ppVx[kVrtx]->Pcoor[nDim] ) ;
    }

    /* Distance to bb ll compared to minDist. */
    dist = ABS( bbll - pCo[nDim]) ;
    if ( dist < *pMinDist )
      minDistEl = MIN( minDistEl, dist ) ;
    else
      /* too far away from bb. */
      return (0) ;

    /* ur */
    dist = ABS( pCo[nDim] - bbur )  ;
    if ( dist < *pMinDist )
      minDistEl = MIN( minDistEl, dist ) ;
    else
      /* too far away. */
      return (0) ;
  }

  double hEdge ;
  int retVal = 0, contained = 1 ;
  /* This covers both the contained case, kFc=0, as well as the
     non-contained case. In that case, check all faces with an 
     outward normal. */
  int kFc = 0 ;
  while ( ( kFc = elem_contains_co ( pElem, pCo, kFc ) ) ) {
    contained = 0 ;
    /* pCo is outside, but inside the BB enlarged by minDist. 
       New outward pointing face. Compute face distances. */
    minDistEl = point_dist_face ( pElem, kFc, pCo, &hEdge ) ;
    if ( *pMinDist > minDistEl ) {
      /* found a better element. */
      retVal = 1 ;
      *pkFc = kFc ;
      *pMinDist = minDistEl ;
      *ppElMin = pElem ;
    }
  }

  if ( contained ) {
    /* Contained. */
    *pkFc = 0 ;
    *pMinDist = 0. ;
    *ppElMin = pElem ;
    return ( 1 ) ;
  }
  else 
    return ( retVal ) ;
}

/******************************************************************************
  min_dist_face_bnd:   */

/*! find minimum face distance over all bnd elements.
 */

/*
  
  Last update:
  ------------
  10Jul15: conceived.
  

  Input:
  ------
  pUns: grid
  pCo:  point to search for 

  minDist: current minimal distance of pCo to an element.
  pElMin:  element with minimal distance or containing element.
  kFc:     outward pointing face with minimal distance in pElMin
           0 if contained.
    
  
*/

void
min_dist_face_bnd ( const uns_s *pUns, const double *pCo, 
                    double *pMinDist, const elem_struct **ppElMin, int *pkFc ) {

  chunk_struct *pCh=NULL ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBFcF, *pBFcL, *pBF ;
  while ( loop_bndFaces ( pUns, &pCh, &pBP, &pBFcF, &pBFcL ) )
    for ( pBF = pBFcF ; pBF <= pBFcL ; pBF++ )
      if ( pBF->Pelem && pBF->Pelem->number ) {
        min_dist_face_el ( pCo, pBF->Pelem, pMinDist, ppElMin, pkFc ) ;
      }

  return ;
}


/******************************************************************************
  min_dist_face_elems:   */

/*! find minimum face distance over all elements.
 */

/*
  
  Last update:
  ------------
  18Dec15; initialise pCh to NULL.
  10Jul15: conceived.
  

  Input:
  ------
  pUns: grid
  pCo:  point to search for 

  minDist: current minimal distance of pCo to an element.
  pElMin:  element with minimal distance or containing element.
  kFc:     outward pointing face with minimal distance in pElMin
           0 if contained.
    
  
*/

void
min_dist_face_elems ( uns_s *pUns, const double *pCo, 
                      double *pMinDist, 
                      const elem_struct **ppElMin, int *pkFc ) {

  chunk_struct *pCh=NULL ;
  elem_struct *pElBeg, *pEl, *pElEnd ;
  while ( loop_elems ( pUns, &pCh, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number ) {
        min_dist_face_el ( pCo, pEl, pMinDist, ppElMin, pkFc ) ;
      }

  return ;
}



/******************************************************************************

  get_unsBox:
  Calculate bounding boxes for an unstructured grid. For simplicity, just
  loop over all numbered vertices.
  
  Last update:
  24Aug14; support axiY, axiZ.
           fix angle calc
  21Nov16: Added Cylindrical coordinates bounding box 
           (by default X is used as the axis as it is the AVBP standard)
  2May96: conceived.
  
  Input:
  ------
  Pchunk:
  
  Changes to:
  -------
  *PChunk:
  
*/

void get_uns_box ( uns_s *pUns ) {

  const int mDim = pUns->mDim ;
  chunk_struct *pChunk ;
  int nBeg, nEnd ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  double rbox[2] ; 
  
  vec_ini_dbl (  TOO_MUCH, mDim, pUns->llBox ) ; 
  vec_ini_dbl ( -TOO_MUCH, mDim, pUns->urBox ) ;
  vec_ini_dbl (  TOO_MUCH,    2, pUns->llBoxCyl ) ;
  vec_ini_dbl ( -TOO_MUCH,    2, pUns->urBoxCyl ) ; 

  pChunk = NULL ;
  double cylCoor[MAX_DIM] ;
  //GS:  Possible to loop only on Boundary nodes to improve efficiency ? 
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        if ( mDim == 3 ) {
          if ( pUns->specialTopo >= axiX &&
               pUns->specialTopo <= axiZ ) {
            if ( pUns->specialTopo == axiX ) {
              //cylCoor[0] = pVx->Pcoor[0] ;
              cylCoor[1] = pVx->Pcoor[1] ;
              cylCoor[2] = pVx->Pcoor[2] ;
            }
            if ( pUns->specialTopo == axiY ) {
              //cylCoor[0] = pVx->Pcoor[2] ;
              cylCoor[1] = pVx->Pcoor[2] ;
              cylCoor[2] = pVx->Pcoor[0] ;
            }
            if ( pUns->specialTopo == axiZ ) {
              //cylCoor[0] = pVx->Pcoor[1] ;
              cylCoor[1] = pVx->Pcoor[0] ;
              cylCoor[2] = pVx->Pcoor[1] ;
            }
            
            // Radius off the x-axis
            rbox[0] = vec_len_dbl ( cylCoor+1, 2 ) ;
            // axi-x: Angle in the y-z plane relative to:y axis.
            // cylCoor:
            // axi-0: Angle in the 1-2 plane relative to:0 axis.
            rbox[1] = atan2( cylCoor[2],cylCoor[1])/PI * 180 ;
            // if ( cylCoor[1] > 0) {
            //   rbox[1] = atan2( cylCoor[2],cylCoor[1])/PI * 180 ;
            // }
            // else if ( cylCoor[1] < 0 ) {
            //   rbox[1] = atan2( cylCoor[2],cylCoor[1])/PI * 180 + PI ;
            // }
            // else if ( cylCoor[2] > 0 ) {
            //   rbox[1] = PI/2 - atan2( cylCoor[1],cylCoor[2])/PI * 180 ;
            // }
            // else if ( cylCoor[2] < 0 ) {
            //   rbox[1] = -PI/2 - atan2( cylCoor[1],cylCoor[2])/PI * 180 ;
            // }
          }

        }
        else {
          rbox[0] = pVx->Pcoor[1] ; // use the y coor as radius.
          rbox[1] = 0. ; // zero angle.
        }
        vec_max_dbl ( pUns->urBox, pVx->Pcoor, mDim, pUns->urBox ) ;
        vec_min_dbl ( pUns->llBox, pVx->Pcoor, mDim, pUns->llBox ) ;
        vec_max_dbl ( pUns->urBoxCyl, rbox, 2, pUns->urBoxCyl ) ;
        vec_min_dbl ( pUns->llBoxCyl, rbox, 2, pUns->llBoxCyl ) ;
      }

  return ;
}
  
/******************************************************************************

  get_chunkBox:
  Calculate bounding boxes for an unstructured Chunk, including the
  various faces. 
  
  Last update:
  2May96: conceived.
  
  Input:
  ------
  Pchunk:
  
  Changes to:
  -------
  *PChunk:
  
*/

void get_chunkBox ( chunk_struct *Pchunk, const int mDim ) {
  
  bndPatch_struct *PbndPatch ;
  bndFc_struct *PbndFc ;
  intFc_struct *PintFc ;
  matchFc_struct *PmatchFc ;
  vrtx_struct **PPvxFc[MAX_VX_FACE], *Pvx ;
  int fcType, nDim, kVert ;
  double llBox[MAX_DIM], urBox[MAX_DIM] ;

  /* Reset the chunk box. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    Pchunk->llBox[nDim] = TOO_MUCH ;
    Pchunk->urBox[nDim] = -TOO_MUCH ;
  }
  
  /* Boundary faces. */
  for ( PbndPatch = Pchunk->PbndPatch + 1 ;
        PbndPatch <= Pchunk->PbndPatch + Pchunk->mBndPatches ; PbndPatch++ ) {
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      llBox[nDim] = TOO_MUCH ;
      urBox[nDim] = -TOO_MUCH ;
    }

    /* Note that the list of PbndFc starts with the first element
       for the chunk, however, PbndPatch->PbndFc points to the first
       element of the patch. */
    for ( PbndFc = PbndPatch->PbndFc ;
	  PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ ) {
      get_uns_face ( PbndFc->Pelem, PbndFc->nFace, PPvxFc, &fcType ) ;
      for ( kVert = 0 ; kVert < fcType ; kVert++ ) {
        Pvx = *PPvxFc[kVert] ;
	for ( nDim = 0 ; nDim < mDim ; nDim++ )	{
          llBox[nDim] = MIN( llBox[nDim], Pvx->Pcoor[nDim] ) ;
	  urBox[nDim] = MAX( urBox[nDim], Pvx->Pcoor[nDim] ) ;
	}
      }
    }
  
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      PbndPatch->llBox[nDim] = llBox[nDim] ;
      PbndPatch->urBox[nDim] = urBox[nDim] ;
      Pchunk->llBox[nDim] = MIN( Pchunk->llBox[nDim], llBox[nDim] ) ;
      Pchunk->urBox[nDim] = MAX( Pchunk->urBox[nDim], urBox[nDim] ) ;
    }
  }

  /* Internal faces. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    llBox[nDim] = TOO_MUCH ;
    urBox[nDim] = -TOO_MUCH ;
  }

  /* PintFc starts with 1. */
  for ( PintFc = Pchunk->PintFc + 1 ;
        PintFc <= Pchunk->PintFc +  Pchunk->mIntFaces ; PintFc++ ) {
    get_uns_face ( PintFc->Pelem, PintFc->nFace, PPvxFc, &fcType ) ;
    for ( kVert = 0 ; kVert < fcType ; kVert++ )
    { Pvx = *PPvxFc[kVert] ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
      { llBox[nDim] = MIN( llBox[nDim], Pvx->Pcoor[nDim] ) ;
	urBox[nDim] = MAX( urBox[nDim], Pvx->Pcoor[nDim] ) ;
      }
    }
  }
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    Pchunk->llIntFcBox[nDim] = llBox[nDim] ;
    Pchunk->urIntFcBox[nDim] = urBox[nDim] ;
    Pchunk->llBox[nDim] = MIN( Pchunk->llBox[nDim], llBox[nDim] ) ;
    Pchunk->urBox[nDim] = MAX( Pchunk->urBox[nDim], urBox[nDim] ) ;
  }


  /* Matching faces. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    llBox[nDim] = TOO_MUCH ;
    urBox[nDim] = -TOO_MUCH ;
  }

  /* Matching faces start with index 1. */
  for ( PmatchFc = Pchunk->PmatchFc + 1 ;
        PmatchFc <= Pchunk->PmatchFc +  Pchunk->mMatchFaces ; PmatchFc++ ) {
    /* Just use elem1, since they are [tested,assumed] to be the same. */
    get_uns_face ( PmatchFc->pElem0, PmatchFc->nFace0, PPvxFc, &fcType ) ;
    for ( kVert = 0 ; kVert < fcType ; kVert++ ) {
      Pvx = *PPvxFc[kVert] ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
        llBox[nDim] = MIN( llBox[nDim], Pvx->Pcoor[nDim] ) ;
	urBox[nDim] = MAX( urBox[nDim], Pvx->Pcoor[nDim] ) ;
      }
    }
  }
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    Pchunk->llMatchFcBox[nDim] = llBox[nDim] ;
    Pchunk->urMatchFcBox[nDim] = urBox[nDim] ;
    Pchunk->llBox[nDim] = MIN( Pchunk->llBox[nDim], llBox[nDim] ) ;
    Pchunk->urBox[nDim] = MAX( Pchunk->urBox[nDim], urBox[nDim] ) ;
  }

  return ;
}

/******************************************************************************
  get_elem_facets:   */

/*! return the vertex pointers of the faces of an element.
 *
 * non-adaptive replacement for get_srfVx_drvElem
 *
 */

/*
  
  Last update:
  ------------
  10Jul11: conceived.
  

  Input:
  ------
  pUns
  pElem

  Output:
  -------
  mFacets[MAX_FACES_ELEM+1]: 1 facet for each face.
  mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE]: mVxFace for each face/zeroth facet
  pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE]: vertices of each zeroth facet.
  
*/

void get_elem_facets( elem_struct *pElem, int mFacets[], 
                      int mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE], 
                      vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;
  int kFc, mVxFc, kVx ;
  vrtx_struct **ppVx = pElem->PPvrtx ;


  for ( kFc = 1 ; kFc <= pElT->mSides ; kFc++ ) {
    pFoE = pElT->faceOfElem + kFc ;

    mFacets[kFc] = 1 ;
    mFacetVerts[kFc][0] = mVxFc = pFoE->mVertsFace ;

    for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
      pFacetVx[kFc][0][kVx] = ppVx[ pFoE->kVxFace[kVx] ] ;
    }
  }

  return ;
}


/******************************************************************************

  get_uns_face:
  For a given face of an element, return the type of the face and a list of
  vertex pointers on that face. The nodes are listed counterclockwise as seen
  from the exterior of the element.
  A question that remains: why didn't I just use the list of vertices in
  faceOfElem? Fix this.
  
  Last update:
  11Apr97; use elemType.
  9Jul96: return 0 on wrong face index.
  2May96: conceived.
  
  Input:
  ------
  Pelem:    Pointer to the element.
  nFace:    Number of the face in the element. See cpre.h.
  
  Changes to:
  -------
  PPvxFc[]: Array of pointers to the location of the vertex pointers
            in the element. 
  PmFaceVerts: The number of vertices on the face.

  Returns:
  --------
  1/0: on success/failure.
  
*/

int get_uns_face ( const elem_struct *Pelem, const int nFace,
		   vrtx_struct **PPvxFc[], int *PmVertsFace ) {

  const elemType_struct *PelT = elemType + Pelem->elType ;
  const faceOfElem_struct *PFoE = PelT->faceOfElem + nFace ;
  const edgeOfElem_struct *PEoE = PelT->edgeOfElem ;
  int iEdge, kEdge ;

  if ( Pelem->elType < tri || Pelem->elType > hex ) { 
    sprintf ( hip_msg, "no such element type %d in get_uns_face.\n", Pelem->elType ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( nFace < 1 || nFace > PelT->mSides ) { 
    /* An invalid face number is a valid exit condition for get_uns_face. */
    return ( 0 ) ;
  }

  if ( PelT->mDim == 2 )
  { /* In 2-D, a face corresponds to an edge. */
    *PmVertsFace = 2 ;
    kEdge = PFoE->kFcEdge[0] ;
    if ( PFoE->edgeDir[0] == 1 )
    { /* The edge runs along the face. */
      PPvxFc[0] = Pelem->PPvrtx + PEoE[kEdge].kVxEdge[0] ;
      PPvxFc[1] = Pelem->PPvrtx + PEoE[kEdge].kVxEdge[1] ;
    }
    else 
    { /* The edge runs opposite to the face. */
      PPvxFc[0] = Pelem->PPvrtx + PEoE[kEdge].kVxEdge[1] ;
      PPvxFc[1] = Pelem->PPvrtx + PEoE[kEdge].kVxEdge[0] ;
    }
  }
  else
  { /* In 3-D a face is a closed loop of edges. */
    *PmVertsFace = PFoE->mVertsFace ;
    for ( iEdge = 0 ; iEdge < PFoE->mFcEdges ; iEdge++ )
    { kEdge = PFoE->kFcEdge[iEdge] ;
      if ( PFoE->edgeDir[iEdge] == 1 )
	/* The face is to the left of the edge. */
	PPvxFc[iEdge] = Pelem->PPvrtx + PEoE[kEdge].kVxEdge[1] ;
      else 
	PPvxFc[iEdge] = Pelem->PPvrtx + PEoE[kEdge].kVxEdge[0] ;
    }
  }

  return (1 ) ;
}

/******************************************************************************

  uns_elem_normls.c:
  Calculate the averaged node-normals for an element. Simplex elements only.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int uns_elem_normls ( const elem_struct *Pelem, double nodeNorm[][MAX_DIM] ) {
  
  static int kSide, iVert, nDim, mTimesNormal ;
  static const elemType_struct *PelT ;
  static const faceOfElem_struct *PFoE ;
  static double fcNorm[MAX_DIM] ;
  PelT = elemType + Pelem->elType ;

  /* Reset the normals. */
  for ( nDim = 0 ; nDim < PelT->mDim ; nDim++ )
    for ( iVert = 0 ; iVert < PelT->mVerts ; iVert++ )
      nodeNorm[iVert][nDim] = 0. ;
  
  for ( kSide = 1 ; kSide <= PelT->mSides ; kSide++ ) {
    PFoE = PelT->faceOfElem + kSide ;
    uns_face_normal ( Pelem, kSide, fcNorm, &mTimesNormal ) ;

    /* Scatter the face normal to the forming vertices. */
    for ( nDim  = 0 ; nDim < PelT->mDim ; nDim++ ) {
      fcNorm[nDim] /= PFoE->mVertsFace*mTimesNormal ; 
      for ( iVert = 0 ; iVert < PFoE->mVertsFace ; iVert++ )
	nodeNorm[ PFoE->kVxFace[iVert] ][nDim] += fcNorm[nDim] ;
    }
  }
  return ( 1 ) ;
}

/*
Copyright Jens-Dominik Mueller and CERFACS
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

Hip is a package for manipulating unstructured computational grids and their
associated datasets. https://inle.cerfacs.fr/projects/hip

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. The users must also reference the original authors on each
use/communicaiton/publication.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and, more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/******************************************************************************
  fun_name:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Dec16: adapted from first draft by GS.
  

  Input:
  ------
  mBc ; number of bc in bWt
  mDim ; spatial dimension
  bWt ; array of bndVxWt for each patch.

  Changes to:
  -------
  bndPatchArea ; summed area for each patch.
  
*/

void bndPatch_area ( const int mBc, const int mDim, const bndVxWt_s *bWt,
                     double bndPatchArea[] ) {
  const bndVxWt_s *pBWtP ;
  int nBc ;
  int nVx ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ ){
    bndPatchArea[nBc] = 0.0 ;
    pBWtP = bWt+nBc ;
    for ( nVx = 0 ; nVx < pBWtP->mBndVx ; nVx++ )
      bndPatchArea[nBc] += vec_len_dbl ( pBWtP->pWt + mDim*nVx, mDim ) ;
 
  }
  return ;
}

    
/******************************************************************************
  bcPatch_gcn_rm:   */

/*! Compute the gravity centre and the overall normal for a bc patch. 
 *
 *
 */

/*
  
  Last update:
  ------------
  26Aug11; fix bug with missing normalisation of bcGC.
  1Apr11; fix bug with switched gc/nrm args, wrong multiplication of area.
  22Dec10: conceived.
  

  Input:
  ------
  pUns: unstructured grid,
  nBc: the patch number to work on,
  bcGc: coordinates of the gravity centre
  bcNrm: normal over the patch.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int bcPatch_nrm_gc ( uns_s *pUns, const int nBc, 
                     double bcNrm[MAX_DIM], double bcGC[MAX_DIM],
                     double *pbcArea ) {
  
  const int mDim = pUns->mDim ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBFc0, *pBFc1, *pFc ;
  int mFc = 0 ;
  double faceGC[MAX_DIM] ;
  const faceOfElem_struct *pFoE ;
  int mVxFc ;
  const vrtx_struct *pVxFc[MAX_VX_FACE] ;
  double fcNrm[MAX_DIM] ;
  int mTms ;
  double mult ;
  double area ;
    
  vec_ini_dbl ( 0., mDim, bcGC ) ;
  vec_ini_dbl ( 0., mDim, bcNrm ) ;
  *pbcArea = 0. ;

  pBP = NULL ;
  while ( loop_bndFaces_bc ( pUns, nBc, &pBP, &pBFc0, &pBFc1 ) ) {
    for ( pFc = pBFc0 ; pFc <= pBFc1 ; pFc++ ) 
      if ( pFc->Pelem && pFc->Pelem->number ) {
        mFc++ ; 
        face_grav_ctr ( pFc->Pelem, pFc->nFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;
        uns_face_normal ( pFc->Pelem, pFc->nFace, fcNrm, &mTms ) ;

        mult = 1./mTms ;
        vec_add_mult_dbl ( bcNrm, mult, fcNrm, mDim, bcNrm ) ;
        area = mult*vec_norm_dbl ( fcNrm, mDim ) ;
        *pbcArea += area ;
        vec_add_mult_dbl ( bcGC, area, faceGC, mDim, bcGC ) ; 
      }
  }

  mult = 1./(*pbcArea ) ;
  vec_mult_dbl ( bcGC, mult, mDim ) ;
 
  return ( mFc ) ;
}

/******************************************************************************
  uns_face_normal_co:   */

/*! Compute an outward face normal given r.h.s. vertex coordinates.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  9Jul11: cut out of uns_face_normal_list
  

  Input:
  ------
  mDim
  mVertsFace
  pCoVx

  Output:
  -------
  fcNorm
  PmTimesNormal
    
  
*/

void uns_face_normal_co ( const int mDim, const int mVertsFace, 
                          const double *pCoVx[MAX_VX_FACE], 
                          double fcNorm[MAX_DIM], int *PmTimesNormal ) {
  
  int nDim ;
  double sfNorm[4][MAX_DIM], vecFwrd[MAX_DIM], vecBckw[MAX_DIM], vecDiag[MAX_DIM] ;

  if ( mDim == 2 ) {
    /* A 2D side is an edge. */
    fcNorm[0] = pCoVx[1][1] - pCoVx[0][1] ;
    fcNorm[1] = pCoVx[0][0] - pCoVx[1][0] ;
    *PmTimesNormal = 1 ;
  }
  else if ( mVertsFace == 3 ) {
    /* A triangular face. Calculate the cross produrct around kVx[0].*/
    for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
      vecFwrd[nDim] = pCoVx[1][nDim] - pCoVx[0][nDim] ;
      vecBckw[nDim] = pCoVx[2][nDim] - pCoVx[0][nDim] ;
    }
    cross_prod_dbl ( vecFwrd, vecBckw, 3, fcNorm ) ;
    *PmTimesNormal = 2 ;
  }
  else if ( mVertsFace == 4 ) {
    /* A quad face 0,1,2,3. Cut into two triangular faces and average
       both possible diagonals. */
    
    /*Diagonal 0,2. */
    for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
      vecFwrd[nDim] = pCoVx[1][nDim] - pCoVx[0][nDim] ;
      vecDiag[nDim] = pCoVx[2][nDim] - pCoVx[0][nDim] ;
      vecBckw[nDim] = pCoVx[3][nDim] - pCoVx[0][nDim] ;
    }
    cross_prod_dbl ( vecFwrd, vecDiag, 3, sfNorm[0] ) ;
    cross_prod_dbl ( vecDiag, vecBckw, 3, sfNorm[1] ) ;
    
    /*Diagonal 1,3. */
    for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
      vecFwrd[nDim] = pCoVx[2][nDim] - pCoVx[1][nDim] ;
      vecDiag[nDim] = pCoVx[3][nDim] - pCoVx[1][nDim] ;
      vecBckw[nDim] = pCoVx[0][nDim] - pCoVx[1][nDim] ;
    }
    cross_prod_dbl ( vecFwrd, vecDiag, 3, sfNorm[2] ) ;
    cross_prod_dbl ( vecDiag, vecBckw, 3, sfNorm[3] ) ;
    
    for ( nDim  = 0 ; nDim < mDim ; nDim++ )
      fcNorm[nDim] = sfNorm[0][nDim] + sfNorm[1][nDim] +
	             sfNorm[2][nDim] + sfNorm[3][nDim] ;
    *PmTimesNormal = 4 ;
  }
  else {
    sprintf ( hip_msg, "cannot deal with a %d-noded face in"
	     " uns_face_normal_co.\n", mVertsFace ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  return ;
}

/******************************************************************************

  uns_face_normal:
  Calculate the outward face normal for an element given a side. Simplex elements only.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pelem
  kSide:   running from 1 to mSides.

  Changes To:
  -----------
  fcNorm:        The outward face normal.
  *PmTimesNormal The multiplicity, ie. an integer scaling factor of that normal.
                 For efficiency, this multiplication is not done inside uns_face_normal.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int uns_face_normal ( const elem_struct *Pelem, const int kSide,
		      double fcNorm[MAX_DIM], int *PmTimesNormal ) {

  const double *pCoVx[MAX_VX_FACE] ;
  int mVxFc ;

  return ( uns_face_normal_list ( Pelem, kSide, 
                                  &mVxFc, pCoVx, fcNorm, PmTimesNormal ) ) ;

}

int uns_face_normal_list ( const elem_struct *Pelem, const int kSide, 
                           int *pmVxFc, const double *pCoVx[MAX_VX_FACE], 
                           double fcNorm[MAX_DIM], int *PmTimesNormal ) {

  
  const elemType_struct *pElT = elemType + Pelem->elType ;
  const faceOfElem_struct *pFoE ;
  int iVert, nDim, mVxFc ;


  /* Reset the normal. */
  for ( nDim = 0 ; nDim < MAX_DIM ; nDim++ )
    fcNorm[nDim] = 0. ;
  
  pFoE = pElT->faceOfElem + kSide ;
  *pmVxFc = mVxFc = pFoE->mVertsFace ; 
  /* Find the vertices forming that side. */
  for ( iVert = 0 ; iVert < mVxFc ; iVert++ )
    pCoVx[iVert] = Pelem->PPvrtx[ pFoE->kVxFace[iVert] ]->Pcoor ;


  uns_face_normal_co ( pElT->mDim, mVxFc, pCoVx, fcNorm, PmTimesNormal ) ;

  return ( 1 ) ;
}




/******************************************************************************

  uns_face_normal_vx:
  Calculate the outward face normal for the facet of the dual volume of
  an element given a side. Quad faces only, otherwise use uns_face_normal and
  scatter.
  
  Last update:
  ------------
  25Sep99, derived from uns_face_normal and get_ewts_bnd.
  
  Input:
  ------
  pElem
  kFace
  kVx:    The position of the vertex on the face.

  Changes To:
  -----------
  fcNorm: The outward face normal at vertex kVx.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int uns_face_normal_vx ( const elem_struct *pElem, const int kFace, const int kVx,
                         double fcNorm[MAX_DIM] ) {

  const int mDim = 3 ;
  int mVxFc ;
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxFc[MAX_VX_FACE] ;
  double faceGC[MAX_DIM], edgeFwd[MAX_DIM], edgeBkwd[MAX_DIM], fctNorm[MAX_DIM],
    vx2faceGC[MAX_DIM] ;

  
  pElT = elemType + pElem->elType ;
  pFoE = pElT->faceOfElem + kFace ;

  if ( pFoE->mVertsFace != 4 ) {
    sprintf ( hip_msg, "uns_face_normal_vx does only quad faces.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  vec_ini_dbl ( 0., mDim, fcNorm ) ;

  
  face_grav_ctr ( pElem, kFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;
  
  if ( mVxFc < 3 )
    /* Collapsed face, normal is zero. */
    return ( 1 ) ;


  if ( mVxFc < 4 ) {
    /* Redo the list of vertices. We need a full where collapsed vertices are
       listed without removing duplication as face_grav_ctr does. */
    pVxFc[0] = pElem->PPvrtx[ pFoE->kVxFace[0] ] ;
    pVxFc[1] = pElem->PPvrtx[ pFoE->kVxFace[1] ] ;
    pVxFc[2] = pElem->PPvrtx[ pFoE->kVxFace[2] ] ;
    pVxFc[3] = pElem->PPvrtx[ pFoE->kVxFace[3] ] ;
  }
  
  /* Normal quad face. Calculate the facet of the dual volume. */
  /* Vector from the vertex to the face centroid. */
  vec_diff_dbl ( faceGC, pVxFc[kVx]->Pcoor, 3, vx2faceGC ) ;
  /* Backward edge. */ 
  vec_diff_dbl ( pVxFc[(kVx+4-1)%4]->Pcoor, pVxFc[kVx]->Pcoor, 3, edgeBkwd ) ;
  /* Forward edge. */ 
  vec_diff_dbl ( pVxFc[(kVx  +1)%4]->Pcoor, pVxFc[kVx]->Pcoor, 3, edgeFwd ) ;

  
    /* The outward normal of the backward triangular facet. */
  cross_prod_dbl ( edgeBkwd, vx2faceGC, mDim, fctNorm ) ;
  /* Note that edgeVec is normalized to the entire edge length,
     our area is formed with half of it, and the cross product gives twice the
     triangular area. */
  vec_add_mult_dbl ( fcNorm, -.25, fctNorm, mDim, fcNorm ) ;
  
  /* The outward normal of the forward triangular facet. */
  cross_prod_dbl ( edgeFwd, vx2faceGC, mDim, fctNorm ) ;
  vec_add_mult_dbl ( fcNorm, .25, fctNorm, mDim, fcNorm ) ;

  return ( 1 ) ;
}


/******************************************************************************

  get_lrgstFaceTwist, get_faceTwist:
  Get face twist as a deviation of a quad face from a linear (triangular) face.
  Use the smaller of the two scalar products of the subtriangulations.
  
  Last update:
  ------------
  3Nov99: optimized, intro aspect ratio correction.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

#ifdef DEBUG
void print_lrgstfaceTwist ( const elem_struct *pElem ) {

  int kFc ;
  double fcTw = get_lrgstFaceTwist ( pElem, &kFc ) ;
  printf ( "%"FMT_ULG", face%d, twist: %g\n", pElem->number, kFc, fcTw ) ;
  return ;
}
#endif

double get_lrgstFaceTwist ( const elem_struct *pElem, int *pkFcTwistMin ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  double twistMin = 1., twist ;
  int kFc ;

  if ( pElem->elType == tri || pElem->elType == tet ) {
    /* Simplices have zero twist. */
    *pkFcTwistMin = 1 ;
    return ( 1. ) ;
  }
  
  for ( kFc = 1 ; kFc <= pElT->mFaces ; kFc++ )
    if ( twistMin > ( twist = get_faceTwist( pElem, kFc ) ) ) {
      twistMin = twist ;
      *pkFcTwistMin = kFc ;
    }
  
  return ( twistMin ) ;
}


double get_faceTwist ( const elem_struct *pElem, const int kFc ) {

  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static const double *pCoor[MAX_VX_FACE+1] ;
  static const int *kVxFace ;
  
  static int nDim, kVx ;
  static double sfNorm[4][MAX_DIM], vecFwrd[MAX_DIM], vecBckw[MAX_DIM],
    vecDiag[MAX_DIM], scProd[2], len[2] ;

  pElT = elemType + pElem->elType ;
  pFoE = pElT->faceOfElem + kFc ;

  if ( pFoE->mVertsFace != 4 )
    /* Only quad faces can have a twist. */
    return ( 1. ) ;

  kVxFace = pFoE->kVxFace ;

  /* Are they distinct? As soon as there is one collapsed edge, it cannot be quad. */
  for ( kVx = 0 ; kVx < 4 ; kVx++ )
    pCoor[kVx] = pElem->PPvrtx[ kVxFace[kVx] ]->Pcoor ;

  if ( pCoor[0] == pCoor[1] || pCoor[1] == pCoor[2] ||
       pCoor[2] == pCoor[3] || pCoor[3] == pCoor[0] )
      /* Collapsed edge. At most a linear face that cannot be twisted. Adios! */
      return ( 1. ) ;
  
  if ( pCoor[0] == pCoor[2] || pCoor[1] == pCoor[3] )
    /* Cross collapsed. Definite no no! */
    return ( -99. ) ;
      
  
  /* Diagonal 0,2. */
  for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
    vecFwrd[nDim] = pCoor[1][nDim] - pCoor[0][nDim] ;
    vecDiag[nDim] = pCoor[2][nDim] - pCoor[0][nDim] ;
    vecBckw[nDim] = pCoor[3][nDim] - pCoor[0][nDim] ;
  }
  
  CRPRD3 ( vecFwrd, vecDiag, sfNorm[0] ) ;
  len[0] = sqrt( sfNorm[0][0]*sfNorm[0][0] +
                 sfNorm[0][1]*sfNorm[0][1] +
                 sfNorm[0][2]*sfNorm[0][2] ) ;
  CRPRD3 ( vecDiag, vecBckw, sfNorm[1] ) ;
  len[1] = sqrt( sfNorm[1][0]*sfNorm[1][0] +
                 sfNorm[1][1]*sfNorm[1][1] +
                 sfNorm[1][2]*sfNorm[1][2] ) ;
  scProd[0] = ( sfNorm[0][0]*sfNorm[1][0] +
                sfNorm[0][1]*sfNorm[1][1] +
                sfNorm[0][2]*sfNorm[1][2] )/len[0]/len[1] ;

  
  /* Diagonal 1,3. */
  for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
    vecFwrd[nDim] = pCoor[2][nDim] - pCoor[1][nDim] ;
    vecDiag[nDim] = pCoor[3][nDim] - pCoor[1][nDim] ;
    vecBckw[nDim] = pCoor[0][nDim] - pCoor[1][nDim] ;
  }
  
  CRPRD3 ( vecFwrd, vecDiag, sfNorm[0] ) ;
  len[0] = sqrt( sfNorm[0][0]*sfNorm[0][0] +
                 sfNorm[0][1]*sfNorm[0][1] +
                 sfNorm[0][2]*sfNorm[0][2] ) ;
  CRPRD3 ( vecDiag, vecBckw, sfNorm[1] ) ;
  len[1] = sqrt( sfNorm[1][0]*sfNorm[1][0] +
                 sfNorm[1][1]*sfNorm[1][1] +
                 sfNorm[1][2]*sfNorm[1][2] ) ;
  scProd[1] = ( sfNorm[0][0]*sfNorm[1][0] +
                sfNorm[0][1]*sfNorm[1][1] +
                sfNorm[0][2]*sfNorm[1][2] )/len[0]/len[1] ;

  return ( MIN( scProd[0], scProd[1] ) ) ;
}


/******************************************************************************

  get_edge_len:
  Return the squared edge length.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double get_edge_len ( const elem_struct *pElem, const int kEg ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const int *kVxEdge = pElT->edgeOfElem[kEg].kVxEdge ;
  const vrtx_struct *pVx0, *pVx1 ;
  double len ;

  pVx0 = pElem->PPvrtx[ kVxEdge[0] ] ;
  pVx1 = pElem->PPvrtx[ kVxEdge[1] ] ;

  if ( pVx0->Pcoor == pVx1->Pcoor )
    /* Degenerate edge. */
    return ( 0. ) ;

  len = sq_distance_dbl ( pVx0->Pcoor, pVx1->Pcoor, pElT->mDim ) ;
  return ( len ) ;
}

/******************************************************************************

  get_elem_vol:
  Calculate the volume of a base element.
  
  Last update:
  ------------
  20Nov99; derived from the optimised maxAngle.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double get_elem_vol ( const elem_struct *pElem ) {
    
  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static const double *pCo[4] ;
  static const int *kVxFace ;
  static const vrtx_struct **ppVx ;

  static double fcNorm[MAX_DIM], 
                fcNorm1[MAX_DIM], fcNorm2[MAX_DIM], fcNorm3[MAX_DIM],
    fcAvg[MAX_DIM], vol, vxVec[4][MAX_DIM] ;
  static int kFc ;

  pElT = elemType + pElem->elType ;
  ppVx = ( const vrtx_struct ** ) pElem->PPvrtx ;
  vol = 0. ;

  if ( pElT->mDim == 3 ) {
    /* All outward face normals. Track degenerate edges and update the degenerate
       edge to face pointers. */

    for ( kFc = 1 ; kFc <= pElT->mFaces ; kFc++ ) {
      pFoE = pElT->faceOfElem + kFc ;
      kVxFace = pFoE->kVxFace ;

      pCo[0] = ppVx[ kVxFace[0] ]->Pcoor ;
      pCo[1] = ppVx[ kVxFace[1] ]->Pcoor ;
      pCo[2] = ppVx[ kVxFace[2] ]->Pcoor ;
      
      vxVec[0][0] = pCo[1][0] - pCo[0][0] ;
      vxVec[0][1] = pCo[1][1] - pCo[0][1] ;
      vxVec[0][2] = pCo[1][2] - pCo[0][2] ;
      vxVec[1][0] = pCo[2][0] - pCo[1][0] ;
      vxVec[1][1] = pCo[2][1] - pCo[1][1] ;
      vxVec[1][2] = pCo[2][2] - pCo[1][2] ;

      if ( pFoE->mVertsFace == 3 ) {
        /* Linear face, a cross product of two edges gives twice the area. */
        CRPRD3 ( vxVec[0], vxVec[1], fcNorm ) ;
        fcAvg[0] = pCo[0][0] + pCo[1][0] + pCo[2][0] ;
        fcAvg[1] = pCo[0][1] + pCo[1][1] + pCo[2][1] ;
        fcAvg[2] = pCo[0][2] + pCo[1][2] + pCo[2][2] ;
        /* Six times the volume. */
        vol += fcNorm[0]*fcAvg[0] + fcNorm[1]*fcAvg[1] + fcNorm[2]*fcAvg[2] ;
      }
      else {
        /* Quad face. Average both triangulations. */
        pCo[3] = pElem->PPvrtx[ kVxFace[3] ]->Pcoor ;
        /* Non-degenerate face. */
        vxVec[2][0] = pCo[3][0] - pCo[2][0] ;
        vxVec[2][1] = pCo[3][1] - pCo[2][1] ;
        vxVec[2][2] = pCo[3][2] - pCo[2][2] ;
        vxVec[3][0] = pCo[0][0] - pCo[3][0] ;
        vxVec[3][1] = pCo[0][1] - pCo[3][1] ;
        vxVec[3][2] = pCo[0][2] - pCo[3][2] ;
        CRPRD3 ( vxVec[0], vxVec[1], fcNorm ) ;
        CRPRD3 ( vxVec[1], vxVec[2], fcNorm1 ) ;
        CRPRD3 ( vxVec[2], vxVec[3], fcNorm2 ) ;
        CRPRD3 ( vxVec[3], vxVec[0], fcNorm3 ) ;
        fcNorm[0] += fcNorm1[0] + fcNorm2[0] + fcNorm3[0] ;
        fcNorm[1] += fcNorm1[1] + fcNorm2[1] + fcNorm3[1] ;
        fcNorm[2] += fcNorm1[2] + fcNorm2[2] + fcNorm3[2] ;
        /* Six times the volume. */
        fcAvg[0] = pCo[0][0] + pCo[1][0] + pCo[2][0] + pCo[3][0] ;
        fcAvg[1] = pCo[0][1] + pCo[1][1] + pCo[2][1] + pCo[3][1] ;
        fcAvg[2] = pCo[0][2] + pCo[1][2] + pCo[2][2] + pCo[3][2] ;
        /* We have four times the area, four times the average, need 6 times
             the normal. .25/4*6 = 3/8. */
        vol += 3./8*( fcNorm[0]*fcAvg[0] +
                      fcNorm[1]*fcAvg[1] +
                      fcNorm[2]*fcAvg[2] ) ;
      }
    }
    
    return ( vol/6/3 ) ;
  }
    
  else {
    /* 2D */
    pCo[0] = ppVx[0]->Pcoor ;
    pCo[1] = ppVx[1]->Pcoor ;
    pCo[2] = ppVx[2]->Pcoor ;

    vxVec[0][0] = pCo[1][0] - pCo[0][0] ;
    vxVec[0][1] = pCo[1][1] - pCo[0][1] ;

    if ( pElT->mVerts == 3 ) {
      vxVec[1][0] = pCo[2][0] - pCo[1][0] ;
      vxVec[1][1] = pCo[2][1] - pCo[1][1] ;
      vol = vxVec[0][0]*vxVec[1][1] - vxVec[0][1]*vxVec[1][0] ;
    }
    else {
      pCo[3] = ppVx[3]->Pcoor ;
      vxVec[2][0] = pCo[2][0] - pCo[0][0] ;
      vxVec[2][1] = pCo[2][1] - pCo[0][1] ;
      vxVec[3][0] = pCo[3][0] - pCo[0][0] ;
      vxVec[3][1] = pCo[3][1] - pCo[0][1] ;
      /* Split the quad along 0 - 2 to form a triangle 0 1 2 and 0 2 3. */
      vol  = vxVec[0][0]*vxVec[2][1] - vxVec[0][1]*vxVec[2][0] ;
      vol -= vxVec[3][0]*vxVec[2][1] - vxVec[3][1]*vxVec[2][0] ;
    }

    return ( .5*vol ) ;
  }
}

double printElVol( const elem_struct *Pelem ) {
  
  /* Bugger that. */
  static double nodeNorm[MAX_VX_ELEM][MAX_DIM], elemVol ;
  static const elemType_struct *pElT ;
  static int nDim, kVx ;

  pElT = elemType + Pelem->elType ;
  elemVol = 0. ;
  
  uns_elem_normls ( Pelem, nodeNorm ) ;

  for ( nDim = 0 ; nDim < pElT->mDim ; nDim++ )
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
      elemVol += Pelem->PPvrtx[kVx]->Pcoor[nDim]*nodeNorm[kVx][nDim] ;
  elemVol /= pElT->mDim ;

  printf ( "%g\n", elemVol ) ;

  return ( elemVol ) ;
  /**/
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_minmax_elem_vol_with_vx:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s calc_minmax_elem_vol_with_vx
( const uns_s *pUns, double **ppminVolElemWithVx, double **ppmaxVolElemWithVx ) {
#undef FUNLOC
#define FUNLOC "in calc_minmax_elem_vol_with_vx"
  ret_s ret = ret_success () ;

  // Make sure the vertex-sized arrays match the current numbering.
  if ( *ppminVolElemWithVx ) arr_free ( *ppminVolElemWithVx ) ;
  *ppminVolElemWithVx =
    arr_malloc ( "pminVolElemWithVx "FUNLOC".", pUns->pFam, pUns->mVertsNumbered+1,
                 sizeof( **ppminVolElemWithVx ) ) ;

  if ( *ppmaxVolElemWithVx ) arr_free ( *ppmaxVolElemWithVx ) ;
  *ppmaxVolElemWithVx =
    arr_malloc ( "pmaxVolElemWithVx "FUNLOC".", pUns->pFam, pUns->mVertsNumbered+1,
                 sizeof( **ppmaxVolElemWithVx ) ) ;

  // Init those arrays.
  ulong_t nVx ;
  for ( nVx = 0 ; nVx <= pUns->mVertsNumbered ; nVx++ ) {
    (*ppminVolElemWithVx)[nVx] = TOO_MUCH ;
    (*ppmaxVolElemWithVx)[nVx] = -TOO_MUCH ;
  }
  

                             
  const elemType_struct *pElT ;
  chunk_struct *pCh = NULL ;
  elem_struct *pElBeg, *pElem, *pElEnd ;
  double elVol ;
  int kVx ;
  while ( loop_elems ( pUns, &pCh, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      elVol = get_elem_vol( pElem ) ;
      pElT = elemType + pElem->elType ;
      for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
        nVx = pElem->PPvrtx[kVx]->number ;
        (*ppminVolElemWithVx)[nVx] = MIN( (*ppminVolElemWithVx)[nVx], elVol ) ;
        (*ppmaxVolElemWithVx)[nVx] = MAX( (*ppmaxVolElemWithVx)[nVx], elVol ) ;
      }
    }
  }

  return ( ret ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_elem_property:
*/
/*! calculate an element property such as hMin, volMin, etc.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  21Sep21: conceived.
  

  Input:
  ------
  pElem: element
  epType: element property to compute
    
  Returns:
  --------
  value of the element property.
  
*/

double calc_elem_property ( elem_struct *pElem, const ep_type epType ) {
#undef FUNLOC
#define FUNLOC "in calc_elem_property"
  
  double dProp = 0. ;
  double hMinSq = TOO_MUCH ;
  double hMaxSq = -1. ;
  chunk_struct *pChunk = NULL ;
  double dist ;
  int fix_degenElems = 0 ;
  double epsOverlapSq = 0. ;
  double *pElemVol=0 ;
  int *pIsNotColl=0 ;
  double *phMinSq=0, *pMaxDihAngle=0, *pMaxFcAngle=0 ;
  switch ( epType ) {
  case ep_none:
    hip_err ( fatal, 0, "ep_none shouldn't have happened in"FUNLOC"." ) ;
  case ep_hMin:
    /* Calc edge lengths */
    get_degenEdges ( pElem, pChunk, &hMinSq, &hMaxSq, &dist,
                     fix_degenElems, epsOverlapSq ) ;
    dProp = sqrt(hMinSq) ;
    break ;

  case ep_volMin:
    dProp = get_elem_vol ( pElem ) ;
    break ;
    
  case ep_angMax:
    dProp = maxAngle ( pElem, pElemVol, pIsNotColl,
                       phMinSq, pMaxDihAngle, pMaxFcAngle ) ;
    break ;
  }

  return ( dProp ) ;
}


/******************************************************************************
  elem_flip_vol:   */

/*! reverse the sense of an element to flip the sign of its volume.
 *  Currently: hex only.
 */

/*
  
  Last update:
  ------------
  9Mar16: conceived.
  
  Changes To:
  -----------
  pElem
  
*/

void elem_flip_vol  ( elem_struct *pElem ) {

  vrtx_struct *ppVx[MAX_VX_ELEM], **ppVrtx = pElem->PPvrtx ;
  const elemType_struct *pElT = elemType + pElem->elType ;
  int mVx = pElT->mVerts ;
  int k ;

  for ( k = 0 ; k < mVx ; k++ )
    ppVx[k] = ppVrtx[k] ;

  int kVxSwap[MAX_ELEM_TYPES][MAX_VX_ELEM] = {
    {0,2,1}, //tet
    {0,3,2,1}, //qua
    {0,2,1,3}, //tet
    {0,3,2,1,4}, //pyr
    {0,1,4,5,2,3}, //pri
    {0,3,2,1,4,7,6,5} //hex
  } ;

  for ( k = 0 ; k < mVx ; k++ )
    ppVrtx[k] = ppVx[ kVxSwap[pElem->elType][k] ] ;

  return ;
}

/******************************************************************************
  flip_negative_volumes:   */

/*! Flip elements with negative volumes if all elments of the type are negative.
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t flip_negative_volumes ( uns_s *pUns ) {


  // Check for reversed element types.
  elType_e elT ;
  chunk_struct *pChunk ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  ulong_t mElT[MAX_ELEM_TYPES] = {0} ;
  ulong_t mElTPos[MAX_ELEM_TYPES] = {0}, mElTNeg[MAX_ELEM_TYPES] = {0} ;
  double vol ;

  /* Count negative and positive volumes per element type. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {
      elT = pEl->elType ;
      mElT[elT]++ ;
      vol = get_elem_vol ( pEl ) ;
      if ( vol > 0. )
        mElTPos[elT]++ ;
      else
        mElTNeg[elT]++ ;
    }


  ulong_t mFlip = 0 ;
  for ( elT = tri ; elT <= hex ; elT++ ) {
    if ( mElT[elT] ) {
      /* There were elements of this type. */
      if ( mElTNeg[elT] && mElTPos[elT] ) {
        sprintf ( hip_msg, "found %"FMT_ULG" %s with negative,"
                  " %"FMT_ULG" with"
                  " positive volumes, can't swap all elements,"
                  " grid will retain negative volumes.", 
                  mElTNeg[elT], elemType[elT].name, mElTPos[elT] ) ;
        hip_err ( warning, 1, hip_msg ) ;
        mElTNeg[elT] = 0 ;
      }
      else if ( mElTNeg[elT] ) {
        /* Swap around all elements of this type. */
        sprintf ( hip_msg, "found %"FMT_ULG" %s with negative volumes," 
                  " will flip all elements.",
                  mElTNeg[elT], elemType[elT].name ) ;
        hip_err ( info, 1, hip_msg ) ;
        mFlip = 1 ;
      }
    }
  }


  pChunk = NULL ;
  if ( mFlip ) {
    mFlip = 0 ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( mElTNeg[pEl->elType] ) {
          elem_flip_vol ( pEl ) ;
          mFlip++ ;
        }
  }

  return ( mFlip ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  elem_is_convex:
*/
/*! check whether element is convex. 
 *
 * Subdivide an element into subelems of interpolation facets and centroid
 * evaluate whether all these subelements have positive volume.
 */

/*
  
  Last update:
  ------------
  16Feb17: conceived.
  21Apr17; intro *pElemIsCollapsed.
  

  Input:
  ------
  pElem
  volMin: elem is convex if all subtet have volume >= minVol

  Output:
  -------
  *pVol: total element volume if convex.
  *pElemIsNotCollapsed: nonzero if the element is not collapsed.
    
  Returns:
  --------
  1 if convex, 0 otherwise.
  
*/

int elem_is_convex ( const elem_struct *pElem, double volMin, double *pVol,
                     int *pElemIsNotCollapsed ) {

  const double volMin6 = volMin*6. ;
  double elemVol = 0. ;
  *pElemIsNotCollapsed = 1 ;

  const elemType_struct *pElT = elemType + pElem->elType ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  /* Extract a list of vertex coordinates and compute the element centroid. */
  const int mVx = pElT->mVerts ;
  int kVx ;
  double coG[MAX_DIM] = {0.} ; /* Coordinates of the gravity centre. */
  double coGFc[MAX_DIM] ; /* Vertex average of a face. */
  const int mDim = pElT->mDim ;
  
  /* Centroid */
  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    vec_add_dbl ( coG, ppVx[kVx]->Pcoor, mDim, coG ) ;
  }
  vec_mult_dbl ( coG, 1./mVx, mDim ) ;
 

  const int mFc = pElT->mFaces ;
  int kFc ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  int mVxFc, mVxFcAllFc[MAX_FACES_ELEM+1] ;
  double vol = 0. ;
  double *coCAllFc[MAX_FACES_ELEM+1][MAX_VX_FACE] ; // corner vertices of the face.
  double **coC ; // pointer to list of corner vx for one face.
  double coEAllFc[MAX_FACES_ELEM+1][MAX_VX_FACE][MAX_DIM] ; // mid edges of all faces.
  double (*coE)[MAX_DIM] ; // pointer to list of mid edges of a face.
  double coDAllFc[MAX_FACES_ELEM+1][5][MAX_DIM][MAX_DIM] ; // Coor differences of the sub-tet.
  double (*coD)[MAX_DIM] ; // pointer to list of coor diffs for one face.
  int k, kk ;
  double cgFct[MAX_DIM], cgEl[MAX_DIM] = {0.} ;
  vrtx_struct vxCoG ;
  vxCoG.number = 99 ;
  vxCoG.Pcoor = coG ;

  int mFcNonCollapsed = 0 ;
  if ( mDim == 3 ) {
  
    /* First pass, establish facets toopology, compute gravity centre. */
    for ( kFc = 1 ; kFc <= mFc ; kFc++ ) {
      pFoE = pElT->faceOfElem+kFc ;
      kVxFace = pFoE->kVxFace ;
      coC = coCAllFc[kFc] ;
      coE = coEAllFc[kFc] ;


      /* Extract a list of coor for the face. */
      coC[0] = ppVx[ kVxFace[0] ]->Pcoor ;
      coC[1] = ppVx[ kVxFace[1] ]->Pcoor ;
      coC[2] = ppVx[ kVxFace[2] ]->Pcoor ;
      mVxFc =  pFoE->mVertsFace ;
      if ( mVxFc == 4 )
        coC[3] = ppVx[ kVxFace[3] ]->Pcoor ;


      /* Condense the list of edges by removing collapses. */
      for ( kVx = 0 ; kVx < mVxFc-1 ; kVx++ ) {
        if ( coC[kVx] == coC[kVx+1] ) {
          /* Copy the remaining list down. */
          for ( kk = kVx+1 ; kk < mVxFc ; kk++ ) {
            coC[kk] = coC[kk+1] ;
          }
          mVxFc-- ;
          kVx-- ; /* And try with that one again. */
        }
      }
      /* Compare first and last. */
      if ( mVxFc > 2 && coC[0] == coC[mVxFc-1] )
        mVxFc-- ;

      mVxFcAllFc[kFc] = mVxFc ;
      switch ( mVxFc ) {
      case 1:
        /* Nothing left doing. */
        break ;
      case 2:
        /* Nothing left doing. */
        break ;
      case 3:
        /* Simple tri face: one tet with centroid. */
        mFcNonCollapsed++ ;
        coD = coDAllFc[kFc][0] ;
        vec_diff_dbl ( coG,    coC[0], mDim, coD[0] ) ;
        vec_diff_dbl ( coC[2], coC[0], mDim, coD[1] ) ;
        vec_diff_dbl ( coC[1], coC[0], mDim, coD[2] ) ;
        vol = det3( coD ) ;
        elemVol += vol ;

        /* Gravity centre. */
        vec_avg_4dbl_3d ( coC[0], coC[1], coC[2], coG, cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;
        break ;
      case 4:
        /* Quad face: split into four tri faces along each edge and the face grav ctr */
        mFcNonCollapsed++ ;

        /* Compute mid-edges. Let local edge 0 run from node 0 to 1.*/
        coGFc[0] = 0.25*( coC[0][0] + coC[1][0] + coC[2][0] + coC[3][0] ) ;
        coGFc[1] = 0.25*( coC[0][1] + coC[1][1] + coC[2][1] + coC[3][1] ) ;
        coGFc[2] = 0.25*( coC[0][2] + coC[1][2] + coC[2][2] + coC[3][2] ) ;
        
        /* Facet 0: Corner at 0: eg3, nd0, eg0. */
        coD = coDAllFc[kFc][0] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[0][k] ;
          coD[2][k] = coC[1][k] - coC[0][k] ;
          coD[1][k] = coGFc[k]  - coC[0][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coC[1], coGFc, coC[0], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 1: Corner at 1: eg0, nd1, eg1. */
        coD = coDAllFc[kFc][1] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[1][k] ;
          coD[2][k] = coC[2][k] - coC[1][k] ;
          coD[1][k] = coGFc[k]  - coC[1][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coC[2], coGFc, coC[1], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 2:Corner at 2: eg1, nd2, eg2. */
        coD = coDAllFc[kFc][2] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[2][k] ;
          coD[2][k] = coC[3][k] - coC[2][k] ;
          coD[1][k] = coGFc[k]  - coC[2][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coC[3], coGFc, coC[2], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 3: Corner at 3: eg2, nd3, eg3. */
        coD = coDAllFc[kFc][3] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[3][k] ;
          coD[2][k] = coC[0][k] - coC[3][k] ;
          coD[1][k] = coGFc[k]  - coC[3][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coC[0], coGFc, coC[3], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;
     
        break ;
      case 44: /* Disabled. */
        /* Quad face: split consistent with min-norm interpolation: four half corners,
           two triangles in the planar rhombus at the centre. Only one of those two need
           evaluating. */
        mFcNonCollapsed++ ;

        /* Compute mid-edges. Let local edge 0 run from node 0 to 1.*/
        vec_avg_dbl ( coC[0], coC[1], mDim, coE[0] ) ;
        vec_avg_dbl ( coC[1], coC[2], mDim, coE[1] ) ;
        vec_avg_dbl ( coC[2], coC[3], mDim, coE[2] ) ;
        vec_avg_dbl ( coC[3], coC[0], mDim, coE[3] ) ;

        
        /* Facet 0: Corner at 0: eg3, nd0, eg0. */
        coD = coDAllFc[kFc][0] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[0][k] ;
          coD[1][k] = coE[3][k] - coC[0][k] ;
          coD[2][k] = coE[0][k] - coC[0][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coE[3], coE[0], coC[0], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 1: Corner at 1: eg0, nd1, eg1. */
        coD = coDAllFc[kFc][1] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[1][k] ;
          coD[1][k] = coE[0][k] - coC[1][k] ;
          coD[2][k] = coE[1][k] - coC[1][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coE[0], coE[1], coC[1], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 2:Corner at 2: eg1, nd2, eg2. */
        coD = coDAllFc[kFc][2] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[2][k] ;
          coD[1][k] = coE[1][k] - coC[2][k] ;
          coD[2][k] = coE[2][k] - coC[2][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coE[1], coE[2], coC[2], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 3: Corner at 3: eg2, nd3, eg3. */
        coD = coDAllFc[kFc][3] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coC[3][k] ;
          coD[1][k] = coE[2][k] - coC[3][k] ;
          coD[2][k] = coE[3][k] - coC[3][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += vol ;
        /* Gravity centre. */
        vec_avg_4dbl_3d ( coG, coE[2], coE[3], coC[3], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;

        /* Facet 5, centre rhombus: eg0, eg1, eg2. 
           Same as eg123, so compute only once. */
        coD = coDAllFc[kFc][4] ;
        for ( k = 0 ; k < 3 ; k++ ) {
          coD[0][k] = coG[k]    - coE[0][k] ;
          coD[1][k] = coE[2][k] - coE[0][k] ;
          coD[2][k] = coE[1][k] - coE[0][k] ;
        }      
        vol = det3( coD ) ;
        elemVol += (vol+vol) ;
        
        /* Gravity centre. Needs to be done twice. */
        vec_avg_4dbl_3d ( coG, coE[2], coE[1], coE[0], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;
        vec_avg_4dbl_3d ( coG, coE[3], coE[2], coE[0], cgFct ) ;
        vec_add_mult_dbl ( cgEl, vol, cgFct, mDim, cgEl ) ;
     
        break ;
        
      default:
        hip_err ( fatal, 0, "unrecognised number of face"
                  " vertices in elem_is_convex" ) ;
      }
    }
    *pVol = elemVol/6 ;


    

    /* Check collapse status. */
    int kFcMatch, kkVx ;
    double **coCMatch ; // pointer to list of corner vx for face to match.
    int match ;
    switch (mFcNonCollapsed ) {
    case 0  :
    case 2 :
      /* Collapsed into a point or into two matching faces. * */
      *pElemIsNotCollapsed = 0 ;
      break ;
    case 1  :
      /* A single face, e.g. a quad face folded on itself. Reject via volume.
      hip_err ( warning, 1, "found an element with a single face in elem_is_convex,"
                " that shouldn't have happened." ) ; */
      break ;
    case 3  :
      /* can be a degenerate case of a single quad face matched by two tris,
         will be filtered out by the volume check. */
      break ;
    case 4 :
      /* Four faces. Can't see how two pairs of quad could be created, reject those.
         Do the tris match? Try to compare the first face left with
         the others. If one match is found, the others have to match as well. */

      /* Find the first non-collapsed face. */
      for ( kFc = 1, kFcMatch=0 ; kFc <= mFc ; kFc++ ) {
        mVxFc =  mVxFcAllFc[kFc] ;
        if ( mVxFc > 2 ) {
          if ( !kFcMatch ) kFcMatch = kFc ;
          else if ( mVxFcAllFc[kFc] > 2 && mVxFcAllFc[kFc] != mVxFc ) {
            /* Not all same faces. */
            mVxFc = 0 ;
          }
        }
      }
      if ( mVxFc == 3 ) {
        coCMatch = coCAllFc[kFcMatch] ;
        /* 4 tri faces. Find a matching tri face. */
        for ( kFc = kFcMatch+1, match=0 ; kFc <= mFc && !match ; kFc++ ) {
          
          for ( kVx = 0, match = 1 ; kVx < 3 && match ; kVx++ ) {
            for ( kkVx = 0 ; kkVx < 3 ; kkVx++ ) {
              if ( coCMatch[kVx] == coCAllFc[kFc][kkVx] )
                break ;
            }
            if ( kkVx == 3 )
              match = 0 ;
          }
        }

        if ( match ) {
          *pElemIsNotCollapsed = 0 ;
        }
      }
    }



    
    if ( *pElemIsNotCollapsed == 0 ) {
      /* Collapsed, fine. */
      return ( 1 ) ;
    }
    
    else if ( elemVol <= volMin6 ) {
      /* Element has insufficent volume: reject as non-convex. Note that zero volume
         elements are flagged as collapsed if the collapse operation is accepted. */
     return ( 0 ) ;
    }
    
    else {
      /* Second pass, compute visibility with actual gravity centre. */
      vec_mult_dbl ( cgEl, 1./elemVol, 3 ) ;
      memcpy ( coG, cgEl, 3*sizeof(double) ) ;
      for ( kFc = 1 ; kFc <= mFc ; kFc++ ) {
        // pFoE = pElT->faceOfElem+kFc ;
        // kVxFace = pFoE->kVxFace ;
        coC = coCAllFc[kFc] ;
        coE = coEAllFc[kFc] ;

        mVxFc = mVxFcAllFc[kFc] ;
        switch ( mVxFc ) {
        case 1:
          /* Nothing left doing. */
          break ;
        case 2:
          /* Nothing left doing. */
          break ;
        case 3:
          /* Simple tri face: one tet with centroid. */
          coD = coDAllFc[kFc][0] ;
          vec_diff_dbl ( coG,    coC[0], mDim, coD[0] ) ;
          //vec_diff_dbl ( coC[1], coC[0], mDim, coD[1] ) ;
          //vec_diff_dbl ( coC[2], coC[0], mDim, coD[2] ) ;
          vol = det3( coD ) ;
          //elemVol += vol ;
          break ;
        case 4:
          /* Quad face: split into four facets connecting each edge with the face avg. */
        
          /* Facet 0: Corner at 0: eg3, nd0, eg0. */
          coD = coDAllFc[kFc][0] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k] - coC[0][k] ;
          }      
          vol = det3( coD ) ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 1: Corner at 1: eg0, nd1, eg1. */
          coD = coDAllFc[kFc][1] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coC[1][k] ;
          }      
          vol = det3( coD ) ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 2: Corner at 2: eg1, nd2, eg2. */
          coD = coDAllFc[kFc][2] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coC[2][k] ;
          }      
          vol = det3( coD ) ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 3: Corner at 3: eg2, nd3, eg3. */
          coD = coDAllFc[kFc][3] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coC[3][k] ;
          }      
          vol = det3( coD ) ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 5, centre rhombus: eg0, eg1, eg2. 
             Same as eg123, so compute only once.
          coD = coDAllFc[kFc][4] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coE[0][k] ;
          }      
          vol = det3( coD ) ; */
          break ;
          
        case 44: /* Disabled. */
          /* Quad face: split consistent with min-norm interpolation: four half corners,
             two triangles in the planar rhombus at the centre. Only one of those two need
             evaluating. */
        
          /* Facet 0: Corner at 0: eg3, nd0, eg0. */
          coD = coDAllFc[kFc][0] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k] - coC[0][k] ;
            // coD[1][k] = coE[0][k] - coC[0][k] ;
            // coD[2][k] = coE[3][k] - coC[0][k] ;
          }      
          vol = det3( coD ) ;
          //elemVol += vol ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 1: Corner at 1: eg0, nd1, eg1. */
          coD = coDAllFc[kFc][1] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coC[1][k] ;
            // coD[1][k] = coE[1][k] - coC[1][k] ;
            // coD[2][k] = coE[0][k] - coC[1][k] ;
          }      
          vol = det3( coD ) ;
          //elemVol += vol ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 2: Corner at 2: eg1, nd2, eg2. */
          coD = coDAllFc[kFc][2] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coC[2][k] ;
            //coD[1][k] = coE[2][k] - coC[2][k] ;
            //coD[2][k] = coE[1][k] - coC[2][k] ;
          }      
          vol = det3( coD ) ;
          //elemVol += vol ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 2: Corner at 3: eg2, nd3, eg3. */
          coD = coDAllFc[kFc][3] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coC[3][k] ;
            //coD[1][k] = coE[3][k] - coC[3][k] ;
            //coD[2][k] = coE[2][k] - coC[3][k] ;
          }      
          vol = det3( coD ) ;
          // elemVol += vol ;
          if ( vol <= volMin6 )
            break ;

          /* Facet 5, centre rhombus: eg0, eg1, eg2. 
             Same as eg123, so compute only once. */
          coD = coDAllFc[kFc][4] ;
          for ( k = 0 ; k < 3 ; k++ ) {
            coD[0][k] = coG[k]    - coE[0][k] ;
            //coD[1][k] = coE[2][k] - coE[0][k] ;
            //coD[2][k] = coE[1][k] - coE[0][k] ;
          }      
          vol = det3( coD ) ;
          // elemVol += (vol+vol) ;
          break ;
        default:
          hip_err ( fatal, 0, "unrecognised number of faces vertices in elem_is_convex" ) ;
        }

        if ( vol <= volMin6 ) {
          /* non-convex.  */
          return (0) ;
        }
      }
    }
  }
    
  else { /* 2D */
  }

  /* Convex element. */
  return ( 1 ) ;
}




/******************************************************************************

  maxAngle:
  Compute the cosine of the
  max dihedral angle between faces and the 
  max in-face angle between the edges of all faces.
  In 2-D, dihedral and in-Face angles are the same.
  
  Last update:
  ------------
  31May22; intro maxMinAngle, calculate also min angle 
  4Feb18; treat case of less than three non-deg faces.
  3Feb18; rename hMin to hMinSq to clarify, add interface doc.
          make pElem a const *.
  15jul17; fix bug with kVC not allocated with extra elem for repeat.
  9Nov16; return max of face and dihedral angles.
  12Feb16; return both dihedral and in-Face angles as args, the max of both as value.
  9Jul13; odd behaviour: elem_struct *pElem from calling function 
          does not cast properly into const elem_struct *pElem here,
          Compiler bug? Fixed by removing the const qualifier.
  17May00; fix bug with sign of scProd in 2D.
  : conceived.
  
  Input:
  ------
  pElem:

  Output:
  --------
  pElemVol: element volume
  pIsNotColl: true if the element is not fully collapsed
  phMinSq: square of length of shortest edge
  phAvg: average edge length.
  pMaxDihAngle: maximal dihedral angle
  pMaxFcAngle: maximal angle in-face angle, i.e. angle between edges.
  pMinAngle: minimal face or dihedral angle.

  Returns:
  --------
  the max of maximal dihedral and in-face angles.

  0 on failure, 1 on success.
  
*/
double maxAngle ( const elem_struct *pElem, double *pElemVol, int *pIsNotColl,
                  double *phMinSq, double *pMaxDihAngle, double *pMaxFcAngle ) {
  double minAngle = 99. ;
  double hAvg ;
return maxMinAngle ( pElem, pElemVol, pIsNotColl,
                     phMinSq, &hAvg, pMaxDihAngle, pMaxFcAngle, &minAngle ) ;
}

double maxMinAngle ( const elem_struct *pElem, double *pElemVol, int *pIsNotColl,
                     double *phMinSq, double *phAvg,
                     double *pMaxDihAngle, double *pMaxFcAngle,
                     double *pMinAngle ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const edgeOfElem_struct *pEoE ;
  vrtx_struct **ppVx ;
  const double *pCo[MAX_VX_FACE+1], *pCoOther[2][2] ;
  const int *kVxEdge, *kVxFace ;

  double egVec[MAX_VX_FACE][MAX_DIM], *fc0, *fc1,
    scProd, scProd2, scMin, scMinDih, scMinFc, scMinDihQ, scMax,
    crProd[MAX_DIM], egLen[MAX_VX_FACE],
    fcNorm[MAX_FACES_ELEM+1][MAX_DIM], vxVec[MAX_VX_FACE][MAX_DIM],
    fcNorm1[MAX_DIM], fcNorm2[MAX_DIM], fcNorm3[MAX_DIM], fcNormLen[MAX_FACES_ELEM+1],
    fcAvg[MAX_DIM], dist ;
  double *fcNorm0, diag[2][MAX_DIM], len[MAX_VX_FACE] ;
  int kVx, kk, kFc, mVxFc, kEg, kDim, kDim1,
    nonDgEg[MAX_FACES_ELEM+1], 
    mEg, mOtherVx0, mOtherVx1, mFcMatch, fcNotMatch, mNonDgFc, kFcL, kFcR, kVx0, kVx1,
    kVxColl[MAX_VX_ELEM], // for each vx, the position of the vx it has collapsed into.
    kVC[MAX_VX_FACE+1], kEg1, fcIsMatched[MAX_FACES_ELEM+1] ;

# define CRPRD3(Px,Py,Pz) { (Pz[0]) = (Px[1])*(Py[2]) - (Px[2])*(Py[1]) ;\
                            (Pz[1]) = (Px[2])*(Py[0]) - (Px[0])*(Py[2]) ;\
                            (Pz[2]) = (Px[0])*(Py[1]) - (Px[1])*(Py[0]) ;}


  pElT = elemType + pElem->elType ;
  ppVx = pElem->PPvrtx ;
    
  *pIsNotColl = 1 ;
  *pElemVol = 0. ;
  *phMinSq = TOO_MUCH ;

  int kFcVx[MAX_VX_ELEM][MAX_VX_ELEM]; // face number for each edge between the two vx.
  double vlen[MAX_VX_FACE] ;
  int mEgNonColl = 0 ;
  *phAvg = 0 ;
  if ( pElT->mDim == 3 ) {

    /* List the lowest numbered vertex each forming vx is collapsed into. */
    kVxColl[0] = 0 ; // Can only be itself.
    for ( kVx = 1 ; kVx < pElT->mVerts ; kVx++ ) {
      kVxColl[kVx] = kVx ;
      pCo[0] = ppVx[ kVx ]->Pcoor ;
      for ( kk = 0 ; kk < kVx ; kk++ ) {
        pCo[1] = ppVx[ kk ]->Pcoor ;
        if ( pCo[0] == pCo[1] ) {
          kVxColl[ kVx ] = kk ;
          break ;
        }
      }
    }

    /* Make a list of collapsed vertices. */
    for ( pEoE = pElT->edgeOfElem, kEg = 0 ; kEg < pElT->mEdges ; kEg++, pEoE++ ) {
      kVxEdge = pEoE->kVxEdge ;
      kVx0 = kVxColl[ kVxEdge[0] ] ;
      kVx1 = kVxColl[ kVxEdge[1] ] ;
      if ( kVx0 == kVx1 ) {
        /* Degenerate edge. */
        kFcVx[kVx0][kVx1] = kFcVx[kVx1][kVx0] = -1 ;
      }
      else {
        /* This is not a degenerate edge. */
        kFcVx[kVx0][kVx1] = kFcVx[kVx1][kVx0] = -1 ;
        dist = sq_distance_dbl ( ppVx[ kVx1 ]->Pcoor, ppVx[ kVx0 ]->Pcoor, 3 ) ;
        *phMinSq = MIN( *phMinSq, dist ) ;
        mEgNonColl++ ;
        *phAvg += sqrt(dist) ;
        
      }
    }
    *phAvg /= mEgNonColl ;


    /* Compute face normals and in-face angles. */
    scMinFc = scMinDihQ = TOO_MUCH ;
    scMax = -TOO_MUCH ;
    for ( kFc = 1, mNonDgFc = 0 ; kFc <= pElT->mFaces ; kFc++ ) {
      pFoE = pElT->faceOfElem + kFc ;
      kVxFace = pFoE->kVxFace ;
      fcIsMatched[kFc] = 0 ;

      pCo[0] = ppVx[ kVxFace[0] ]->Pcoor ; // face vertex coordinates
      kVC[0] = kVxColl[ kVxFace[0] ] ; // face vertex position in element
      pCo[1] = ppVx[ kVxFace[1] ]->Pcoor ;
      kVC[1] = kVxColl[ kVxFace[1] ] ;
      pCo[2] = ppVx[ kVxFace[2] ]->Pcoor ;
      kVC[2] = kVxColl[ kVxFace[2] ] ;
      if  ( pFoE->mVertsFace == 4 ) {
        mVxFc = 4 ;
        pCo[3] = ppVx[ kVxFace[3] ]->Pcoor ;
        kVC[3] = kVxColl[ kVxFace[3] ] ;
      }
      else
        mVxFc = 3 ;

      
      /* Condense the list by eliminating collapsed edges, i.e. duplicate vx. */
      for ( kVx = 0 ; kVx < mVxFc-1 ; kVx++ ) {
        if ( pCo[kVx] == pCo[kVx+1] ) {
          /* Copy the remaining list down. */
          for ( kk = kVx+1 ; kk < mVxFc ; kk++ ) {
            pCo[kk] = pCo[kk+1] ;
            kVC[kk] = kVC[kk+1] ;
          }
          mVxFc-- ;
          kVx-- ; /* And try with that one again. */
        }
      }
      /* Compare first and last. */
      if ( mVxFc > 2 && pCo[0] == pCo[mVxFc-1] )
        mVxFc-- ;

      
      nonDgEg[kFc] = mVxFc ;
      if ( mVxFc <= 2 ) {
        /* Degenerate face, point or line. Angle = zero. */
        fcNorm[kFc][0] = 0. ;
        fcNorm[kFc][1] = 0. ;
        fcNorm[kFc][2] = 0. ;
        fcNormLen[kFc] = 1. ;
        scMinFc = 0. ;
      }
      if ( mVxFc == 3 ) {
        /* Single normal for the planar triangular face. */
        mNonDgFc++ ;

        vxVec[0][0] = pCo[1][0] - pCo[0][0] ;
        vxVec[0][1] = pCo[1][1] - pCo[0][1] ;
        vxVec[0][2] = pCo[1][2] - pCo[0][2] ;
        vxVec[1][0] = pCo[2][0] - pCo[1][0] ;
        vxVec[1][1] = pCo[2][1] - pCo[1][1] ;
        vxVec[1][2] = pCo[2][2] - pCo[1][2] ;
        vxVec[2][0] = pCo[0][0] - pCo[2][0] ;
        vxVec[2][1] = pCo[0][1] - pCo[2][1] ;
        vxVec[2][2] = pCo[0][2] - pCo[2][2] ;
 
        /* Face normal: linear face, a 
           cross product of two edges gives twice the area. */
        cross_prod_dbl ( vxVec[0], vxVec[1], 3, fcNorm[kFc] ) ;
        fcNormLen[kFc] = sqrt( fcNorm[kFc][0]*fcNorm[kFc][0] +
                               fcNorm[kFc][1]*fcNorm[kFc][1] +
                               fcNorm[kFc][2]*fcNorm[kFc][2] ) ;
        fcAvg[0] = pCo[0][0] + pCo[1][0] + pCo[2][0] ;
        fcAvg[1] = pCo[0][1] + pCo[1][1] + pCo[2][1] ;
        fcAvg[2] = pCo[0][2] + pCo[1][2] + pCo[2][2] ;
        /* Six times the volume. */
        *pElemVol += ( fcNorm[kFc][0]*fcAvg[0] +
                       fcNorm[kFc][1]*fcAvg[1] +
                       fcNorm[kFc][2]*fcAvg[2] ) ;

        /* List the face with each of its forming edges. */
        kFcVx[ kVC[0] ][ kVC[1] ] = kFc ;
        kFcVx[ kVC[1] ][ kVC[2] ] = kFc ;
        kFcVx[ kVC[2] ][ kVC[0] ] = kFc ;


        /* In-Face angles. */
        vlen[0] = vec_len_dbl ( vxVec[0], 3 ) ;
        vlen[1] = vec_len_dbl ( vxVec[1], 3 ) ;
        vlen[2] = vec_len_dbl ( vxVec[2], 3 ) ;
        for ( kDim = 0 ; kDim < 3 ; kDim++ ) {
          kDim1 = (kDim+1)%3 ;
          /* Edges are cyclical, scProd=1 == 180deg. */
          scProd = -scal_prod_dbl ( vxVec[kDim], vxVec[kDim1], 3 ) ;
          scProd /= vlen[kDim]*vlen[kDim1] ;        
          cross_prod_dbl ( vxVec[kDim], vxVec[kDim1], 3, fcNorm1 ) ;
          scProd2 = scal_prod_dbl ( fcNorm1, fcNorm[kFc], 3 ) ;
          if ( scProd2 < 0 ) scProd = -2.-scProd ; 
          if ( scProd < scMinFc ) scMinFc = scProd ;
          if ( scProd > scMax ) scMax = scProd ;
        }
      }

      else if ( mVxFc == 4 ) {
        if ( pCo[0] == pCo[2] || pCo[1] == pCo[3] ) {
          /* Face is "pinched" into two lines: two non-consec. vx are collapsed.*/
          fcNorm[kFc][0] = 0. ;
          fcNorm[kFc][1] = 0. ;
          fcNorm[kFc][2] = 0. ;
          fcNormLen[kFc] = 0 ;
          scMinFc = -2 ; // Assign worst face angle.
        }
        else {
          /* Non-degenerate quad face. Average both triangulations. */
          mNonDgFc++ ;
          fcNorm0 = fcNorm[kFc] ;
          vxVec[0][0] = pCo[1][0] - pCo[0][0] ;
          vxVec[0][1] = pCo[1][1] - pCo[0][1] ;
          vxVec[0][2] = pCo[1][2] - pCo[0][2] ;
          vxVec[1][0] = pCo[2][0] - pCo[1][0] ;
          vxVec[1][1] = pCo[2][1] - pCo[1][1] ;
          vxVec[1][2] = pCo[2][2] - pCo[1][2] ;
          vxVec[2][0] = pCo[3][0] - pCo[2][0] ;
          vxVec[2][1] = pCo[3][1] - pCo[2][1] ;
          vxVec[2][2] = pCo[3][2] - pCo[2][2] ;
          vxVec[3][0] = pCo[0][0] - pCo[3][0] ;
          vxVec[3][1] = pCo[0][1] - pCo[3][1] ;
          vxVec[3][2] = pCo[0][2] - pCo[3][2] ;

          // the facet normal 0 is for the tri facet formed around node 0, i.e. 3 0 1. 
          cross_prod_dbl ( vxVec[3], vxVec[0], 3, fcNorm0 ) ;
          cross_prod_dbl ( vxVec[0], vxVec[1], 3, fcNorm1 ) ;
          cross_prod_dbl ( vxVec[1], vxVec[2], 3, fcNorm2 ) ;
          cross_prod_dbl ( vxVec[2], vxVec[3], 3, fcNorm3 ) ;

          /* Check dihedral angles across both internal diagonals. diag 0 runs 02 ...*/
          diag[0][0] = pCo[2][0] - pCo[0][0] ;
          diag[0][1] = pCo[2][1] - pCo[0][1] ;
          diag[0][2] = pCo[2][2] - pCo[0][2] ;
          diag[1][0] = pCo[3][0] - pCo[1][0] ;
          diag[1][1] = pCo[3][1] - pCo[1][1] ;
          diag[1][2] = pCo[3][2] - pCo[1][2] ;
          
          len[0] = vec_len_dbl ( fcNorm0, 3 ) ;
          len[1] = vec_len_dbl ( fcNorm1, 3 ) ;
          len[2] = vec_len_dbl ( fcNorm2, 3 ) ;
          len[3] = vec_len_dbl ( fcNorm3, 3 ) ;

          scProd = dih_angle ( fcNorm3, len[3], fcNorm1, len[1], diag[0] ) ;          
          //if ( scProd < scMinDihQ ) scMinDihQ = scProd ;
          scProd = dih_angle ( fcNorm0, len[0], fcNorm2, len[2], diag[1] ) ;          
          //if ( scProd < scMinDihQ ) scMinDihQ = scProd ;

          

          fcNorm[kFc][0] += fcNorm1[0] + fcNorm2[0] + fcNorm3[0] ;
          fcNorm[kFc][1] += fcNorm1[1] + fcNorm2[1] + fcNorm3[1] ;
          fcNorm[kFc][2] += fcNorm1[2] + fcNorm2[2] + fcNorm3[2] ;
          fcNormLen[kFc] = sqrt( fcNorm[kFc][0]*fcNorm[kFc][0] +
                                 fcNorm[kFc][1]*fcNorm[kFc][1] +
                                 fcNorm[kFc][2]*fcNorm[kFc][2] ) ;
          /* Six times the volume. */
          fcAvg[0] = pCo[0][0] + pCo[1][0] + pCo[2][0] + pCo[3][0] ;
          fcAvg[1] = pCo[0][1] + pCo[1][1] + pCo[2][1] + pCo[3][1] ;
          fcAvg[2] = pCo[0][2] + pCo[1][2] + pCo[2][2] + pCo[3][2] ;
          /* We have four times the area, four times the average, need 6 times
             the normal. .25/4*6 = 3/8. */
          *pElemVol += 3./8*( fcNorm[kFc][0]*fcAvg[0] +
                              fcNorm[kFc][1]*fcAvg[1] +
                              fcNorm[kFc][2]*fcAvg[2] ) ;

          /* List the face with each of its forming edges. */
          kFcVx[ kVC[0] ][ kVC[1] ] = kFc ;
          kFcVx[ kVC[1] ][ kVC[2] ] = kFc ;
          kFcVx[ kVC[2] ][ kVC[3] ] = kFc ;
          kFcVx[ kVC[3] ][ kVC[0] ] = kFc ;

          /* In-Face angles. */
          vlen[0] = vec_len_dbl ( vxVec[0], 3 ) ;
          vlen[1] = vec_len_dbl ( vxVec[1], 3 ) ;
          vlen[2] = vec_len_dbl ( vxVec[2], 3 ) ;
          vlen[3] = vec_len_dbl ( vxVec[3], 3 ) ;
          for ( kDim = 0 ; kDim < 4 ; kDim++ ) {
            kDim1 = (kDim+1)%4 ;
            /* Edges are cyclical, scProd=1 == 180deg. */
            scProd = -scal_prod_dbl ( vxVec[kDim], vxVec[kDim1], 3 ) ;        
            scProd /= vlen[kDim]*vlen[kDim1] ;        
            CRPRD3 ( vxVec[kDim], vxVec[kDim1], fcNorm1 ) ;
            scProd2 = scal_prod_dbl ( fcNorm1, fcNorm[kFc], 3 ) ;
            if ( scProd2 < 0 ) scProd = -2.-scProd ; 
            if ( scProd < scMinFc ) scMinFc = scProd ;
            if ( scProd > scMax ) scMax = scProd ;
          }
        } // else non-degen quad face
      }
    }

    if ( mNonDgFc < 3 ) {
      /* A volumetric element would need at least three faces. This
         one has to be a collapsed element. */
        *pIsNotColl = 0 ;
        *pElemVol = -99. ;
        return ( 0. ) ;
    }


    /* Calculate the dihedral angles across edges. */
    scMinDih = TOO_MUCH ;
    for ( kEg = mFcMatch = 0 ; kEg < pElT->mEdges ; kEg++ ) {
      kVxEdge = pElT->edgeOfElem[kEg].kVxEdge ;
      kVx0 = kVxColl[ kVxEdge[0] ] ;
      kVx1 = kVxColl[ kVxEdge[1] ] ;
      kFcR = kFcVx[kVx0][kVx1] ;
      kFcL = kFcVx[kVx1][kVx0] ;

      if ( kVx0 != kVx1 && kFcR ) {
        /* This is a non-degenerate edge. Needs to be done only once, so set the
           faces around the edge to 0. */
        kFcVx[kVx0][kVx1] = kFcVx[kVx1][kVx0] = 0 ;
        pCo[0] = ppVx[ kVxEdge[0] ]->Pcoor ;
        pCo[1] = ppVx[ kVxEdge[1] ]->Pcoor ;
        fc0 = fcNorm[kFcR] ;
        fc1 = fcNorm[kFcL] ;
        scProd = -( fc0[0]*fc1[0] + fc0[1]*fc1[1] + fc0[2]*fc1[2] )/
          fcNormLen[kFcR]/fcNormLen[kFcL] ;
        
        fcNotMatch = 1 ;
        if ( scProd > .999999 ) {
          /* Are these two faces collapsed onto each other? Find the one or
             two vertices that are normally non-shared. */
          mOtherVx0 = mOtherVx1 = 0 ;
          pCoOther[0][0] = pCoOther[0][1] = pCoOther[1][0] = pCoOther[1][1] = NULL ;
          
          pFoE =  pElT->faceOfElem + kFcR ;
          kVxFace = pFoE->kVxFace ;
          for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
            pCo[2] = ppVx[ kVxFace[kVx] ]->Pcoor ;
            if ( pCo[2] != pCo[0] && pCo[2] != pCo[1] )
              pCoOther[0][mOtherVx0++] = pCo[2] ;
          }

          pFoE =  pElT->faceOfElem + kFcL ;
          kVxFace = pFoE->kVxFace ;
          for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
            pCo[2] = ppVx[ kVxFace[kVx] ]->Pcoor ;
            if ( pCo[2] != pCo[0] && pCo[2] != pCo[1] )
              pCoOther[1][mOtherVx1++] = pCo[2] ;
          }
          
          if ( ( pCoOther[0][0] == pCoOther[1][0] &&
                 pCoOther[0][1] == pCoOther[1][1] ) ||
               ( pCoOther[0][0] == pCoOther[1][1] &&
                 pCoOther[0][1] == pCoOther[1][0] ) ) {
            /* Two faces, collapsed onto each other. */
            fcNotMatch = 0 ;
            if ( !fcIsMatched[kFcR] && !fcIsMatched[kFcL] )
              mFcMatch++ ;
            fcIsMatched[kFcR] = fcIsMatched[kFcL] = 1 ;
          }
        }

        if ( fcNotMatch ) {
          /* Compare the cross product to the edge direction to define the sector. */
          cross_prod_dbl ( fc0, fc1, 3, crProd ) ;
          egVec[0][0] = pCo[1][0] - pCo[0][0] ;
          egVec[0][1] = pCo[1][1] - pCo[0][1] ;
          egVec[0][2] = pCo[1][2] - pCo[0][2] ;
          scProd2 = ( crProd[0]*egVec[0][0] +
                      crProd[1]*egVec[0][1] +
                      crProd[2]*egVec[0][2] ) ;
          
          if ( scProd2 < 0. )
            /* Angle larger than pi. */
            scProd = -2.-scProd ;
          
          if ( scProd < scMinDih ) scMinDih = scProd ;
          if ( scProd > scMax )    scMax = scProd ;
        }
      }
    }

    *pMaxDihAngle = scMinDih ;
    *pMaxFcAngle = scMinFc ;
    *pMinAngle = scMax ;
    if ( scMinDih == TOO_MUCH ) {
      /* No dihedral angle was calculated, all faces matched. Is this a permissible
         collapsed element? */
      if ( ( mFcMatch == 1 && mNonDgFc == 2 ) ||
           ( mFcMatch == 2 && mNonDgFc == 4 ) ) {
        /* Yup. */
        *pIsNotColl = 0 ;
        *pElemVol = -99. ;
        return ( 0. ) ;
      }
      else {
        /* Reject. */
        return ( -99. ) ;
      }
    }
    else {
      scMin = MIN( scMinDih, scMinFc ) ; //Fix face angles for degen elems. Do we need them?
      *pElemVol /= 6*3 ;
      return ( scMin ) ;
    }
  }
    
  else {
    /* 2D */
    mEg = 0 ;
    pCo[0] = ppVx[0]->Pcoor ;
    pCo[1] = ppVx[1]->Pcoor ;
    pCo[2] = ppVx[2]->Pcoor ;
    if ( pElT->mVerts == 3 ) {
      pCo[3] = pCo[0] ;
      if ( pCo[0] != pCo[1] ) mEg++ ;
      if ( pCo[1] != pCo[2] ) mEg++ ;
      if ( pCo[2] != pCo[0] ) mEg++ ;
    }
    else {
      pCo[3] = ppVx[3]->Pcoor ;
      pCo[4] = pCo[0] ;
      if ( pCo[0] != pCo[1] ) mEg++ ;
      if ( pCo[1] != pCo[2] ) mEg++ ;
      if ( pCo[2] != pCo[3] ) mEg++ ;
      if ( pCo[3] != pCo[0] ) mEg++ ;
    }

    if ( mEg < 3 ) {
      /* Collapsed element. No angles. */
      *pIsNotColl = 0 ;
      *pElemVol = -99. ;
      return ( 0. ) ;
    }


    
    /* Find the vectors. */
    for ( kEg = mEg = 0 ; kEg < pElT->mEdges ; kEg++ )
      if ( pCo[kEg] != pCo[kEg+1] ) {
        egVec[mEg][0] = pCo[kEg+1][0] - pCo[kEg][0] ;
        egVec[mEg][1] = pCo[kEg+1][1] - pCo[kEg][1] ;
        egLen[mEg] = egVec[mEg][0]*egVec[mEg][0] + egVec[mEg][1]*egVec[mEg][1] ;
        *phMinSq = MIN( *phMinSq, egLen[mEg] ) ;
        egLen[mEg] = sqrt ( egLen[mEg] ) ;
        mEg++ ;
        *phAvg += sqrt(egLen[mEg]) ;
      }
    *phAvg /= mEg ;

    for ( kEg = 0, scMinDih = 0. ; kEg < mEg ; kEg++ ) {
      kEg1 = ( kEg+1 )%mEg ;
      scProd = -( egVec[kEg][0]*egVec[kEg1][0] + egVec[kEg][1]*egVec[kEg1][1] )/
        egLen[kEg]/egLen[kEg1] ;

      crProd[2] = egVec[kEg][0]*egVec[kEg1][1] - egVec[kEg][1]*egVec[kEg1][0] ;
      *pElemVol += crProd[2] ;
      
      if ( crProd[2] < 0. )
        scProd = -2.-scProd ;

      if ( scProd < scMinDih )
        scMinDih = scProd ;
    }

    if ( mEg == 3 )
      /* Each cross product gives twice the volume, we have three of the same. */
      *pElemVol /= 2*3 ;
    else
      /* Each cross product gives twice the volume, we added both choices of diagonal. */
      *pElemVol /= 2*2 ;

    return ( scMinDih ) ;
  }
}




/******************************************************************************

  get_face_lrgstAngle:
  Get the largest face angle in an element. 
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem
  
  Returns:
  --------
  The cosine of the largest angle in the face, if the largest angle is <180deg,
  -cos-2 otherwise.
  
*/

double get_face_lrgstAngle ( const elem_struct *pElem, int kFace, int *pkVxFc )
{
  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static int mV, kVx, iVx, mDim, mTimes ;
  static double maxAngle, scProd, crProd[MAX_DIM], vecFwd[MAX_DIM], vecBkwd[MAX_DIM],
    *pFwdCo, *pBkwdCo, *pCo, *pCoVx[MAX_VX_FACE], fcNorm[MAX_DIM] ;

  pElT = elemType + pElem->elType ;
  pFoE = pElT->faceOfElem + kFace ;
  mDim = pElT->mDim ;
  mV = pFoE->mVertsFace ;
  maxAngle = TOO_MUCH ;
  
  /* Coordinates of the face vertices. */
  for ( iVx = 0 ; iVx < mV ; iVx++ ) {
    kVx = pFoE->kVxFace[iVx] ;
    pCoVx[iVx] = pElem->PPvrtx[kVx]->Pcoor ;
  }

    /* Calculate scalar products. */
  for ( iVx = 0 ; iVx < mV ; iVx++ ) {
    pCo = pCoVx[iVx] ;

    if ( ( pBkwdCo = pCoVx[(iVx+mV-1)%mV] ) ==  pCo ) {
      /* The backward edge is collapsed. */
      if ( mV == 3 )
	/* This face is collapsed. */
	return ( TOO_MUCH ) ;
      else if ( ( pBkwdCo = pCoVx[(iVx+mV-2)%mV] ) ==  pCo )
	/* The next edge is collapsed as well. Flat quad face. */
	return ( TOO_MUCH ) ;
    }
	
    if ( ( pFwdCo = pCoVx[(iVx+1)%mV] ) ==  pCo ) {
      /* The forward edge is collapsed. */
      if ( mV == 3 )
	/* This element is collapsed. */
	return ( TOO_MUCH ) ;
      else if ( ( pFwdCo = pCoVx[(iVx+2)%mV] ) ==  pCo )
	/* The next edge is collapsed as well. Flat quad. */
	return ( TOO_MUCH ) ;
    }
	
    vec_diff_dbl ( pFwdCo, pCo, mDim, vecFwd ) ;
    vec_diff_dbl ( pBkwdCo, pCo, mDim, vecBkwd ) ;
    vec_norm_dbl ( vecBkwd, mDim ) ;
    vec_norm_dbl ( vecFwd,  mDim ) ;
    scProd = scal_prod_dbl ( vecFwd, vecBkwd, mDim ) ;

    /* Check the quadrant with the cross-product. */
    cross_prod_dbl ( vecFwd, vecBkwd, mDim, crProd ) ;
    /* Compare that cross-product to the face-normal. */
    uns_face_normal ( pElem, kFace, fcNorm, &mTimes ) ;
    if ( scal_prod_dbl ( crProd, fcNorm, mDim ) < 0. )
      scProd  = -2.-scProd ;

    if ( scProd < maxAngle ) {
      *pkVxFc = iVx ;
      maxAngle = scProd ;
    }
  }

  return ( maxAngle ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  get_equiangle_skew:
*/
/*! Compute min/max angles in an element, use those to compute equi-angle skew.
 *
 *
 */

/*
  
  Last update:
  ------------
  20Dc17: formula copied into shell of function.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

double get_equiangle_skew ( const elem_struct *pElem ) {

  double easkew = 0 ;

  /*
  e.g.
https://www.sharcnet.ca/Software/Ansys/15.0.7/en-us/help/icm_help/iedit_equiangle.html

Equiangle Skewness

This quality parameter applies to tetra, hexa, quad, and tri elements.

Element equiangle skew = 1.0 – max ((Qmax – Qe) / (180 – Qe), (Qe – Qmin) / Qe),

where

Qmax = largest angle in the face or element

Qmin = smallest angle in the face or element

Qe = angle of an equiangular face or element (e.g., 60 degrees for a triangle, and 90 degrees for a square).

   */
  
  return ( easkew ) ;
}


/******************************************************************************
  flag_vx:   */

/*! Lock, set, unset, inquire and free the vertex flag.
 */

/*
  
  Last update:
  ------------
  30Jun12; add free_vx_flag, track use with character.
  16Dec11: conceived.
  

  Input:
  ------
  pUns: grid to flag
  geo:  geometric type and parameters.

    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void use_vx_flag ( uns_s *pUns, char *usedBy ) {

  if ( usedBy && pUns->vxFlag1Active ) {
    sprintf ( hip_msg, "vx flag 1 still locked by %s.", pUns->vxFlag1UsedBy ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  } 

  pUns->vxFlag1Active = 1 ;
  if ( usedBy ) strncpy ( pUns->vxFlag1UsedBy, usedBy, LINE_LEN ) ;

  return ;
}

void set_vx_flag ( vrtx_struct *pVx, int flag, 
                   ulong_t *pmFlAdd, ulong_t *pmFlRem ) {

  if ( pVx->flag1 && !flag ) (*pmFlRem)++ ;
  if ( !pVx->flag1 && flag ) (*pmFlAdd)++ ;
  pVx->flag1 = flag ;
  return ;
}

void unflag_vx ( uns_s *pUns ) {

  /* Reset flags at vertices. */
  ulong_t mFlAdd = 0, mFlRem = 0 ;
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      set_vx_flag ( pVx, 0, &mFlAdd, &mFlRem ) ;

  return ;
}

int is_vx_flagged ( const vrtx_struct *pVx ) {

  if ( pVx->flag1 )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

void free_vx_flag ( uns_s *pUns ) {

  pUns->vxFlag1Active = 0 ;
  pUns->vxFlag1UsedBy[0] = '\0' ;
  return ;
}


/******************************************************************************
  fun_name:   */

/*! Return 1 if an element contains flagged nodes.
 */

/*
  
  Last update:
  ------------
  16Dec11: conceived.
  

  Input:
  ------
  pElem: the element.

  Returns:
  --------
  1 if element has flagged nodes, 0 otherwise
  
*/

/* Flag an element if one if its forming vertices is flagged. */
int el_has_flag1_vx ( elem_struct *pElem ) {

  int kVx ;
  int mVx = elemType[ pElem->elType].mVerts ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    if ( ppVx[kVx]->flag1 )
      return ( 1 ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************
  uns_flag_vx_vol:   */

/*! flag vertices referenced by volumetric elements.
 */

/*
  
  Last update:
  ------------
  22Apr14; fix cut&paste in header comment, 
           make the loop run over elements to actually flag used vx.
  01Jul12; base decision on vertex validity, instead of number.
  31Jan12: derived from uns_flag_vx_geo.
  

  Input:
  ------
  pUns:
  usedBy: which routine is using the flag from now on? If NULL, continuing use.
  
*/

int uns_flag_vx_vol ( uns_s *pUns, char *usedBy ) {

  use_vx_flag ( pUns, usedBy ) ;
  unflag_vx ( pUns ) ;
  
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElemBeg, *pElemEnd ;
  const elemType_struct *pElT ;
  int kVx ;
  vrtx_struct *pVrtx ;
  ulong_t mVxFlag = 0 ;
  ulong_t mFlRem = 0 ;
  while ( loop_elems( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( !pElem->invalid  ) {
        pElT = elemType + pElem->elType ;
        for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
          pVrtx = pElem->PPvrtx[kVx] ;
          set_vx_flag ( pVrtx, ( pVrtx->invalid ? 0 : 1 ), &mVxFlag, &mFlRem ) ;
        }
      }

  return (mVxFlag) ;
}

/******************************************************************************
  uns_flag_vx_bnd:   */

/*! flag all valid vertices belonging to a particular boundary.
 */

/*
  
  Last update:
  ------------
  7Apr13; promote possibly large int to type ulong_t.
  30Jun12; use the ->invalid flag to track active vx, instead of the number.
  31Jan12: derived from uns_flag_vx_geo.
  

  Input:
  ------
  pUns:
  nBc: the bc to flag for. Flag for volume nodes if nBc=-1, reset if nBc<-1
  usedBy: which routine is using the flag from now on? If NULL, continuing use.
  
*/

int uns_flag_vx_bnd ( uns_s *pUns, const int nBc, ulong_t mFc[5], char *usedBy ) {


  /* Set flag status. */
  if ( nBc < -1 ) {
    unflag_vx ( pUns ) ;
    free_vx_flag ( pUns ) ;
  }
  else {
    use_vx_flag ( pUns, usedBy ) ;
  }


  ulong_t  mVxFlag = 0 ;
  ulong_t  mFlRem = 0 ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcBeg, *pBndFcEnd, *pBndFc ;
  elem_struct *pElem ;
  const faceOfElem_struct *pFoE ;
  int mVxFc ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  vrtx_struct *pVx ;
  int k ;
  if ( nBc >= 0 ) {

    mFc[0] = mFc[1] = mFc[2] = mFc[3] = mFc[4] = 0 ;
    
    /* Mark and number all nodes in this boundary. */
    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pElem = pBndFc->Pelem ;
        if ( pElem && pElem->number && pBndFc->nFace ) {
          pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
          mVxFc = pFoE->mVertsFace ;
          kVxFc = pFoE->kVxFace ;
          ppVx = pElem->PPvrtx ;

          /* Count the face. */
          mFc[ mVxFc ]++ ;

          /* Number the vertices, if not yet numbered. */
          for ( k = 0 ; k < mVxFc ; k++ ) {
            pVx = ppVx[ kVxFc[k] ] ;
            if ( !pVx->invalid ) {
              set_vx_flag ( pVx, 1, &mVxFlag, &mFlRem ) ;
            }
          }
        }
      }
  }
  else if ( nBc == -1 ) {
    hip_err ( fatal, 0, " in uns_flag_vx_bnd: use uns_flag_vx_vol instead." ) ; 
  } 

  return ( mVxFlag ) ;
}




/******************************************************************************
  uns_flag_vx_geo:   */

/*! flag vertices satisfying certain geometric conditions.
 */

/*
  
  Last update:
  ------------
  14May20; add cyl.
  31Jan12: derived from uns_flag_vx_geo.
  

  Input:
  ------
  pUns:
  geo:  geometric type and parameters.
  usedBy: which routine is using the flag from now on? If NULL, continuing use.
  
*/



int is_in_Box ( double *pCo, const int mDim, const geo_s *pGeo ) {
  int k, is_in = 1 ;
  for ( k = 0 ; k < mDim ; k++ ) {
    if ( pCo[k] < pGeo->box.ll[k] ) is_in = 0 ;
    if ( pCo[k] > pGeo->box.ur[k] ) is_in = 0 ;
  }
  return ( is_in ) ;
}

int is_below_plane ( double *pCo, const int mDim, const geo_s *pGeo ) {
  double vec[MAX_DIM] ;
  vec_diff_dbl ( pCo, pGeo->plane.loc, mDim, vec ) ;
  double scp = scal_prod_dbl( vec, pGeo->plane.norm, mDim ) ;
  if ( scp >= 0. ) 
    return ( 1 ) ;
  else        
    return ( 0 ) ;
}

int is_in_cyl (double *pCo, const int mDim, const geo_s *pGeo ) {
  double vec[MAX_DIM] ; 
  vec_diff_dbl ( pCo, pGeo->cyl.loc, mDim, vec ) ;
  
  double scp = scal_prod_dbl( vec, pGeo->cyl.axis, mDim ) ;
  double vec2axis[MAX_DIM] ;
  vec_add_mult_dbl ( vec, -scp, pGeo->cyl.axis, mDim, vec2axis ) ; 
  
  double rad = vec_norm_dbl( vec2axis, mDim ) ;
  if ( rad <= pGeo->cyl.rad ) 
    return ( 1 ) ;
  else        
    return ( 0 ) ;
}

int is_in_sec (double *pCo, const int mDim, const geo_s *pGeo ) {

  double rCo, thCo ;
  cart2cyl ( pCo, pGeo->sec.kDim[0], mDim, &rCo, &thCo ) ;

  if ( rCo < pGeo->sec.r[0] ) 
    return ( 0 ) ;
  if ( rCo > pGeo->sec.r[1] ) 
    return ( 0 ) ;
  if ( thCo < pGeo->sec.th[0] ) 
    return ( 0 ) ;
  if ( thCo > pGeo->sec.th[1] ) 
    return ( 0 ) ;
  else        
    return ( 1 ) ;
}


int is_in_sph (double *pCo, const int mDim, const geo_s *pGeo ) {
  double vec[MAX_DIM] ;
  vec_diff_dbl ( pCo, pGeo->sph.loc, mDim, vec ) ;
  double rad = vec_norm_dbl( vec, mDim ) ;
  if ( rad <= pGeo->sph.rad ) 
    return ( 1 ) ;
  else        
    return ( 0 ) ;
}


int is_in_geo ( double *pCo, const int mDim, const geo_s *pGeo ) {
  if ( pGeo->box.type == box   && is_in_Box      ( pCo, mDim, pGeo ) )
    return ( 1 ) ;

  if ( pGeo->box.type == plane && is_below_plane ( pCo, mDim, pGeo ) )
    return ( 1 ) ;

  if ( pGeo->box.type == sphere && is_in_sph   ( pCo, mDim, pGeo ) )
    return ( 1 ) ;

  if ( pGeo->box.type == cyl   && is_in_cyl      ( pCo, mDim, pGeo ) )
    return ( 1 ) ;

  if ( pGeo->box.type == sector && is_in_sec   ( pCo, mDim, pGeo ) )
    return ( 1 ) ;
  /* Nothing matches. */
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  cart2cyl:
*/
/*! Convert Cartesian coordinates to cylindrical.
 *
 */

/*
  
  Last update:
  ------------
  6Dec20; conceived.
  

  Input:
  ------
  xyz: Cartesian coordinate
  kDim: index of the cylindrical axis, 012 = xyz.

  Output:
  -------
  *pR: radius
  *pThRad: theta in radian
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s cart2cyl ( const double xyz[], int kDim, const int mDim,
                 double *pR, double *pThRad  ) {
#undef FUNLOC
#define FUNLOC "in cart2cyl"
  
  ret_s ret = ret_success () ;

  int k0 = 0, k1 = 1 ;
  if ( mDim == 2 ) {
    kDim = 2 ;
    k0 = 0 ;
    k1 = 1 ;
  }
  else if ( mDim == 3 ){
    k0  = (kDim+1)%3 ;
    k0  = (kDim+2)%3 ;
  }
  else
    hip_err ( fatal, 0, "not 2 or 3 dim "FUNLOC"." ) ;
    

  double x0[2] = {1,0} ; // coor of the theta=0 axis in the axis-normal plane
  double x[2] ; // coor of xyz in the normal-axis plane 
  x[0] = xyz[k0] ;
  x[1] = xyz[k1] ;

  *pR = vec_norm_dbl ( x, 2 ) ;

  double sp, cp[MAX_DIM] ;
  sp = scal_prod_dbl ( x0, x, 2 ) ;
  cross_prod_dbl ( x0, x, 2, cp ) ;
  double thRad = acos(sp) ;
  
  if ( cp[0] >= 0. && sp >= 0. ) {
    /* First quadrant. */
    *pThRad = thRad ;
  }
  else if ( cp[0] < 0. && sp > 0. ) {
    /* Second quadrant. */
    *pThRad = (PI-thRad) ;
  }
  if ( cp[0] < 0. && sp < 0. ) {
    /* Third quadrant. */
    *pThRad = (PI+thRad) ;
  }
  else {
    /* Fourth quadrant. */
    *pThRad = (-thRad ) ;
  }

  return ( ret ) ;
}


void uns_flag_vx_geo ( uns_s *pUns, const geo_s *pGeo, char *usedBy ) {

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;
  const int mDim = pUns->mDim ;


  /* Set flag status. */
  if ( pGeo->box.type == noGeo ) {
    free_vx_flag ( pUns ) ;
  }
  else if ( pGeo->box.type == allGeo || 
            pGeo->box.type == box || 
            pGeo->box.type == plane || 
            pGeo->box.type == cyl || 
            pGeo->box.type == sector || 
            pGeo->box.type == sphere ) {

    use_vx_flag ( pUns, usedBy ) ;
  }
  else
    hip_err ( fatal, 0, "panic in flag_uns_vx_geo: unknown internal geo_s type.\n" ) ;


  /* Set flags at vertices. */
  ulong_t mFlAct = 0, mFlAdd = 0, mFlRem = 0 ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ ) {

      if ( pGeo->box.type == noGeo ) /* Reset all to zero. */
        set_vx_flag ( pVx, 0, &mFlAdd, &mFlRem ) ;

      if ( pGeo->box.type == allGeo ) /* Set all to 1. */
        set_vx_flag ( pVx, 1, &mFlAdd, &mFlRem ) ;

      else if ( is_in_geo ( pVx->Pcoor, mDim, pGeo ) )
        /* Match, inclusion. */
        set_vx_flag ( pVx, 1, &mFlAdd, &mFlRem ) ;

      /* Final count. */
      if ( pVx->flag1 ) mFlAct++ ;
    }


  sprintf ( hip_msg, "added %"FMT_ULG" nodal flags, removed %"FMT_ULG","
            " currently %"FMT_ULG" nodes flagged.\n",
            mFlAdd, mFlRem, mFlAct) ;
  hip_err( info, 3, hip_msg ) ;

  return ;
}




/******************************************************************************

  add_hrb:
  Add a haribo to the list.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_hrb ( hrbs_s *pHrbs, const double ll[MAX_DIM], const double ur[MAX_DIM],
              const double radius, const int mDim ) {
  
  hrb_s *pHrb ;
  int kDim ;
  
  pHrbs->pHrb = arr_realloc ( "pHrbs->pHrb in add_hrb", NULL, pHrbs->pHrb,
                              pHrbs->mHrbs+1, sizeof( *pHrbs->pHrb ) ) ;

  /* Endpoints and radius of the haribo. */
  pHrb = pHrbs->pHrb + pHrbs->mHrbs ;
  vec_copy_dbl ( ll, mDim, pHrb->llEnd ) ;
  vec_copy_dbl ( ur, mDim, pHrb->urEnd ) ;
  pHrb->radiusSq = radius*radius ;

  /* Normalized vector from ll to ur. */
  vec_diff_dbl ( ur, ll, mDim, pHrb->vec1 ) ;
  vec_norm_dbl ( pHrb->vec1, mDim ) ;
  
  /* Find a bounding box for the line. Use a worst case to include the haribo. */
  vec_min_dbl ( ll, ur, mDim, pHrb->llBox ) ;
  vec_max_dbl ( ll, ur, mDim, pHrb->urBox ) ;
  for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
    pHrb->llBox[kDim] -= radius ;
    pHrb->urBox[kDim] += radius ;
  }

  pHrbs->mHrbs++ ;
  return ( 1 ) ;
}



/******************************************************************************

  is_elem_in_hrb:
  Given a list of 'boxes', ie. line segments with a radius, find out whether
  all vertices of an element are contained in the boxes.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int is_elem_in_hrb ( const elem_struct *pElem, const hrbs_s *pHrbs ) {
  
  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mDim = pElT->mDim ;
  const vrtx_struct *pVx ;
  const hrb_s *pHrb ;
  int kVx ;
  double diffBeg[MAX_DIM], diffEnd[MAX_DIM], foot[MAX_DIM], scalProd ;
  
  
  if ( !pHrbs->mHrbs )
    /* Empty list of hrbs, this is a global hrb. */
    return ( 1 ) ;

  /* Loop over all vertices of this element. */
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    pVx = pElem->PPvrtx[kVx] ;

    /* Check for inclusion in all hrbs. */
    for ( pHrb = pHrbs->pHrb ; pHrb < pHrbs->pHrb + pHrbs->mHrbs ; pHrb++ ) {
      if ( overlap_dbl ( pVx->Pcoor, pVx->Pcoor, pHrb->llBox, pHrb->urBox, mDim ) ) {
        /* This vertex lies in the bounding hrb. Check exactly. */
        vec_diff_dbl ( pVx->Pcoor, pHrb->urEnd, mDim, diffEnd ) ;
        vec_diff_dbl ( pVx->Pcoor, pHrb->llEnd, mDim, diffBeg ) ;

        if ( ( scalProd = scal_prod_dbl ( diffBeg, pHrb->vec1, mDim ) ) < 0. )
          /* Vertex is prior to the begining of the line. */
          vec_copy_dbl ( pHrb->llEnd, mDim, foot ) ;
        else if ( scal_prod_dbl ( diffEnd, pHrb->vec1, mDim ) > 0. )
          /* Vertex is past the end of the line. Note that pHrb->diff points from
             beginning to end. */
          vec_copy_dbl ( pHrb->urEnd, mDim, foot ) ;
        else {
          /* Calculate the foot of the normal to the line toward the vertex. */
          vec_copy_dbl ( pHrb->vec1, mDim, foot ) ;
          vec_mult_dbl ( foot, -scalProd, mDim ) ;
          vec_diff_dbl ( pHrb->llEnd, foot, mDim, foot ) ;
        }

        /* Calculate the distance. */
        if ( sq_distance_dbl ( foot, pVx->Pcoor, mDim ) > pHrb->radiusSq )
          /* Too far, reject. */
          return ( 0 ) ;
      }
      else
        /* One vertex is out, reject. */
        return ( 0 ) ;
    }
  }

  /* All vertices in. */
  return ( 1 ) ;
}

/******************************************************************************

  mark_hrb:
  Mark all elements with the hrbs.
  Note JDM 17/12/11: hrb's are currently not used. When used again, recode
  to use flags rather than mark.
  
  Last update:
  ------------
  21Apr20; use the last elem mark instead of boxMark.
  4Apr13; modified interface to loop_elems
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void mark_elems_in_hrb ( uns_s *pUns, hrbs_s *pHrbs ) {

  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;


  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number && is_elem_in_hrb ( pElem, pHrbs ) )
        // pElem->boxMark = 1 ;
        set_elem_mark ( pElem, BOX_MARK ) ;
      else
        //pElem->boxMark = 0 ;
        reset_elem_mark ( pElem, BOX_MARK ) ;
  
  return ;
}

/******************************************************************************

  elem_grav_ctr:
  Average the gravity center of an element out of all forming vertices.
  Consider duplicate vertices only once.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem


  Changes To:
  -----------
  elemGC: The gravity center.
  ppElT:  The element type.
  pmVxEl: The number of unique vertices on the element,
  pVxEl:  The vertices.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void elem_grav_ctr ( const elem_struct *pElem,
		     double elemGC[], const elemType_struct **ppElT,
                     int *pmVxEl, const vrtx_struct *pVxEl[] )
{
  static const elemType_struct *pElT ;
  static int mDim, nDim, kVert, kkV, mVx ;
  static const vrtx_struct *pVx ;

  *ppElT = pElT = elemType + pElem->elType ;
  mDim = pElT->mDim ;
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    elemGC[nDim] = 0. ;

  for ( kVert = mVx = 0 ; kVert < pElT->mVerts ; kVert++ ) {
    pVx = pElem->PPvrtx[kVert] ;
    
    /* Is this vertex a duplicate? */
    for ( kkV = 0 ; kkV < mVx ; kkV++ )
      if ( pVx ==  pElem->PPvrtx[kkV] )
        break ;

    if ( kkV == mVx ) {
      /* No duplicate found. */
      pVxEl[mVx++] = pVx ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        elemGC[nDim] += pVx->Pcoor[nDim] ;
    }
  }
  *pmVxEl = mVx ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    elemGC[nDim] /= mVx ;

  return ;
}

/******************************************************************************

  face_grav_ctr:
  Average the gravity center of the face of an element out of all forming vertices.
  
  Last update:
  ------------
  30Nov98; return a list of unique vertices.
  : conceived.
  
  Input:
  ------
  pElem
  kFace

  Changes To:
  -----------
  faceGC:  The gravity center of the face.
  ppFoE:   The faceOfElem.
  pmVxFc:  The number of non-duplicated vertices on the face.
  pVxFc:   The list of non-duplicated vertices.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void face_grav_ctr ( const elem_struct *pElem, const int kFace, 
		     double faceGC[], const faceOfElem_struct **ppFoE,
                     int *pmVxFc, const vrtx_struct *pVxFc[] )
{
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  int mDim, nDim, kVert, mVx, iVx, iiVx ;
  const vrtx_struct *pVx ;

  pElT = elemType + pElem->elType ;
  *ppFoE = pFoE = pElT->faceOfElem + kFace ;
  mDim = pElT->mDim ;
  mVx = 0 ;

  /* Reset the gravity center. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    faceGC[nDim] = 0. ;

  /* Make a list of vertices, omitting duplicates. Accumulate the gravity center. */
  for ( iVx = mVx = 0 ; iVx < pFoE->mVertsFace ; iVx++ ) {
    kVert = pFoE->kVxFace[iVx] ;
    pVx = pElem->PPvrtx[kVert] ;
    
    /* Is this vertex a duplicate? */
    for ( iiVx = 0 ; iiVx < mVx ; iiVx++ ) {
      if ( pVx == pVxFc[iiVx] )
        break ;
    }

    if ( iiVx == mVx ) {
      /* No duplicate found. */
      pVxFc[mVx++] = pVx ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        faceGC[nDim] += pVx->Pcoor[nDim] ;
    }
  }
  *pmVxFc = mVx ;

  /* Normalize. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    faceGC[nDim] /= mVx ;

  return ;
}


/******************************************************************************

  edge_grav_ctr:
  Average the gravity center of the face of an element out of all forming vertices.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void edge_grav_ctr ( const elem_struct *pElem, const int kEdge,
		     double edgeGC[MAX_DIM] )
{
  static const int *kVxEdge ;
  static int mDim, nDim ;

  mDim = elemType[ pElem->elType ].mDim ;
  kVxEdge = elemType[ pElem->elType ].edgeOfElem[kEdge].kVxEdge ;
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    edgeGC[nDim]  = .5*( pElem->PPvrtx[ kVxEdge[0] ]->Pcoor[nDim] +
			 pElem->PPvrtx[ kVxEdge[1] ]->Pcoor[nDim] ) ;

  return ;
}


/******************************************************************************

  med_normal:
  Calculate the normal of the median dual between an edge and the element's
  gravity center. The normal will be signed to point outward from the vertex
  iVx=[0,1] of that face. For the time being, 2-D only.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem : the element
  kFace:  and its side
  iVx:    which vertex of the side will be inward of the normal.

  Changes To:
  -----------
  medNorm:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void med_normal_edge_2D ( const elem_struct *pElem, double elemGC[MAX_DIM],
                          const int kEdge, double medNorm[MAX_DIM] )
{
  static double edgeGC[MAX_DIM], edge2elemGC[MAX_DIM] ;
  static const elemType_struct *pElT  ;
  static const int *kVxEdge ;
  pElT = elemType + pElem->elType ;

  edge_grav_ctr ( pElem, kEdge, edgeGC ) ;
  /* The vector from edge gravity center to element gravity center. */
  vec_diff_dbl ( elemGC, edgeGC, pElT->mDim, edge2elemGC ) ;
  medNorm[0] =  edge2elemGC[1] ;
  medNorm[1] = -edge2elemGC[0] ;

  /* Rotate the normal in the direction of the edge. Do this by calculating
     the scalar product with the normal. */
  kVxEdge = pElT->edgeOfElem[kEdge].kVxEdge ;
  vec_diff_dbl ( pElem->PPvrtx[kVxEdge[1]]->Pcoor, pElem->PPvrtx[kVxEdge[0]]->Pcoor,
                 pElT->mDim, edgeGC ) ;

  if ( scal_prod_dbl ( edgeGC, medNorm, pElT->mDim ) < 0. ) {
    /* Reverse the direction of the normal. */
    medNorm[0] =  -medNorm[0] ;
    medNorm[1] =  -medNorm[1] ;
  }
  return ;
}

/******************************************************************************

  face_in_elem:
  Given a list of node numbers, find the matching face in a given cell.
  
  Last update:
  ------------
  24Jun98: conceived.
  
  Input:
  ------
  pElem:   the element.
  mVxFace: the number of nodes on the face sought.
  nVxFc:   the node numbers on the face sought
  
  Returns:
  --------
  0 on failure, the face number on success.
  
*/

int face_in_elem ( const elem_struct *pElem, const int mVxFace, const ulong_t nVxFc[] ) {

  int kFace, fcType, nVx, iVx, match ;
  vrtx_struct **ppVxFc[MAX_VX_FACE] = {NULL} ; // silence -O3 compiler.
  
  /* Find the edge back in the element. */
  for ( kFace = 1 ; get_uns_face ( pElem, kFace, ppVxFc, &fcType ) ; kFace++ )
    if ( mVxFace == fcType ) {
      /* Number of vertices match. Loop over all vertices in this face. */
      for ( match = 1, nVx = 0 ; nVx < mVxFace ; nVx++ ) {
        for ( iVx = 0 ; iVx < mVxFace ; iVx++ )
          if ( (*ppVxFc[iVx])->number == nVxFc[nVx] )
            break ;
        if ( iVx >= mVxFace ) {
          /* Vertex nVx is not on this face. Try the next face. */
          match = 0 ;
          break ; }
      }
      
      if ( match ) {
        return ( kFace ) ;
      }
    }

  /* No match found. */
  return ( 0 ) ;
}

/******************************************************************************

  make_bndVxNrm:
  Calculate boundary normals starting from the calculated boundary
  weights in make_bndVxWts.
  Average normals for viscous and inviscid walls for all bcs.
  Finally, reloop and compare the normals to the particular face
  normals attached to each vertex.  Set the normal to zero if the
  angle difference is too large.
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  17Feb13; change print format specifiers to %"FMT_ULG" for ulong_t args. 
  10Aug01; extracted from make_bndVxWts.
  
  Input:
  ------
  pUns:

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_bndVxNrm ( uns_s *pUns, bndVxWt_s bWt[], int mAllBndVx ) {

  const int mDim = pUns->mDim ;
  const faceOfElem_struct *pFoE ;
  ulong_t nPerVx, nVx, mV, mWallVx ;
  int mTimes, kVx, nBc ;
  bndVxWt_s *pBW ;
  bndPatch_struct *pBndPatch ;
  elem_struct *pElem ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  double fcNorm[MAX_DIM], normOutInIn[MAX_DIM], normInInOut[MAX_DIM], *pWt,
    *pN, *pNI, *pNO, *pBN ;
  bndNorm_s *pBndNorm, *pBn, bn, *pBnI, *pBnO, bnI, bnO ;


  /* Normals. Average over all faces of wall type.  */
  /* Count all vertices of wall type. */
  reset_vx_mark ( pUns ) ;

  for ( nBc = mWallVx = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBW = bWt + nBc ;
    mV = pBW->mBndVx ;
      
    if ( pUns->ppBc[nBc]->type[0] == 'i' ||
         pUns->ppBc[nBc]->type[0] == 'v' ||
         pUns->ppBc[nBc]->type[0] == 'w' )
      for ( kVx = 0 ; kVx < mV ; kVx++ ) {
        pBW->pVx[kVx]->mark = 1 ;
        mWallVx++ ;
      }
  }
    
  pBn = pBndNorm = arr_calloc ( "pBndNorm in make_bndVxWts", pUns->pFam,
                                mWallVx*mDim, sizeof( *pBndNorm ) ) ;

  /* Make a list of wall vertices. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBW = bWt + nBc ;
    mV = pBW->mBndVx ;
    
    if ( pUns->ppBc[nBc]->type[0] == 'i' ||
         pUns->ppBc[nBc]->type[0] == 'v' ||
         pUns->ppBc[nBc]->type[0] == 'w' )
      for ( kVx = 0 ; kVx < mV ; kVx++ )
        if ( pBW->pVx[kVx]->mark ) {
          /* Add each vertex only once. */
          pBn->pVx = pBW->pVx[kVx] ;
          vec_copy_dbl ( pBW->pNrm + kVx*mDim, mDim, pBn->bndNorm ) ;
          pBW->pVx[kVx]->mark = 0 ;
          pBn++ ;
        }
  }

  mWallVx = pBn - pBndNorm ;
      
  /* Sort them. */
  qsort ( pBndNorm, mWallVx, sizeof ( bndNorm_s ), cmp_bndNorm ) ;
    


    
  /* Scatter the weights for wall bcs. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->ppBc[nBc]->type[0] == 'i' ||
         pUns->ppBc[nBc]->type[0] == 'v' ||
         pUns->ppBc[nBc]->type[0] == 'w' ) {
      pBW = bWt + nBc ;
      mV = pBW->mBndVx ;
      
      for ( kVx = 0 ; kVx < mV ; kVx++ ) {
        bn.pVx = pBW->pVx[kVx] ;
        pBn = bsearch ( &bn, pBndNorm, mWallVx,
                        sizeof ( bndNorm_s ), cmp_bndNorm ) ;
        if ( !pBn ) {
          sprintf ( hip_msg, "missing bnd vx (3) %"FMT_ULG" in"
                    " make_bndVxWts.\n", nVx);
          hip_err ( fatal, 0, hip_msg ) ;
        }
        pN = pBn->bndNorm ;
        
        nVx = pBW->pnVx[kVx] ;
        pWt = pBW->pWt + mDim*kVx ;
        
        vec_add_dbl ( pWt, pN, mDim, pN ) ;
      }
    }
  
  /* Fix the normals for all periodic nodes on non-periodic surfaces. */
  for ( nPerVx = 0 ; nPerVx < pUns->mPerVxPairs ; nPerVx++ ) {
    bnI.pVx = pUns->pPerVxPair[nPerVx].In ;
    pBnI = bsearch ( &bnI, pBndNorm, mWallVx, sizeof ( bndNorm_s ), cmp_bndNorm ) ;
    
    bnO.pVx = pUns->pPerVxPair[nPerVx].Out ;
    pBnO = bsearch ( &bnO, pBndNorm, mWallVx, sizeof ( bndNorm_s ), cmp_bndNorm ) ;
    
    if ( pBnI && pBnO ) {
      pNI = pBnI->bndNorm ;
      pNO = pBnO->bndNorm ;
      
      rot_coor_dbl ( pNO, pUns->pPerBc->rotOut2in, mDim, normOutInIn ) ;
      rot_coor_dbl ( pNI, pUns->pPerBc->rotIn2out, mDim, normInInOut ) ;
      
      vec_add_dbl  ( pNI, normOutInIn, mDim, pNI ) ;
      vec_add_dbl  ( pNO, normInInOut, mDim, pNO ) ;
    }
    else if ( pBnI || pBnO ) {
      sprintf ( hip_msg, "missing per bnd vx %"FMT_ULG"-%"FMT_ULG" in"
                " make_bndVxWts.\n",
               bnI.pVx->number, bnO.pVx->number ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  
  
  /* We want unit normals. */
  for ( pBn = pBndNorm ; pBn < pBndNorm + mWallVx ; pBn++ )
    vec_norm_dbl ( pBn->bndNorm, mDim ) ;

    



  /* Reloop over all wall faces, Compare the face normal to the
       gathered one, set to zero if it exceeds a certain angle. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    if ( pUns->ppBc[nBc]->type[0] == 'i' ||
         pUns->ppBc[nBc]->type[0] == 'v' ||
         pUns->ppBc[nBc]->type[0] == 'w' ) {
        
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->nFace && ( pElem = pBndFc->Pelem ) && pElem->number ) {
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mV = pFoE->mVertsFace ;

            /* Calculate an average normal and scatter. */
            uns_face_normal ( pElem, pBndFc->nFace, fcNorm, &mTimes ) ;
            vec_norm_dbl ( fcNorm, mDim ) ;
                
            /* Check whether any vertex on this face is a boundary vertex. */
            for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
              bn.pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              pBn = bsearch ( &bn, pBndNorm, mWallVx,
                              sizeof ( bndNorm_s ), cmp_bndNorm ) ;
              if ( !pBn ) {
                sprintf ( hip_msg, "missing bnd vx (6) %"FMT_ULG" in"
                          " make_bndVxWts.\n", nVx) ;
                hip_err ( warning, 1, hip_msg ) ;
              }
              pN = pBn->bndNorm ;

              if ( scal_prod_dbl ( fcNorm, pN, mDim ) < normAnglCut )
                /* Too large an angle. Zero. */
                vec_ini_dbl ( 0., mDim, pN ) ;
            }
          }
    }
  }

    
  /* Fix all periodic siblings of the ones that have been set to zero. */
  for ( nPerVx = 0 ; nPerVx < pUns->mPerVxPairs ; nPerVx++ ) {
    bnI.pVx = pUns->pPerVxPair[nPerVx].In ;
    pBnI = bsearch ( &bnI, pBndNorm, mWallVx, sizeof ( bndNorm_s ), cmp_bndNorm ) ;

    bnO.pVx = pUns->pPerVxPair[nPerVx].Out ;
    pBnO = bsearch ( &bnO, pBndNorm, mWallVx, sizeof ( bndNorm_s ), cmp_bndNorm ) ;

    if ( pBnI && pBnO ) {
      pNI = pBnI->bndNorm ;
      pNO = pBnO->bndNorm ;

      if ( pNI[0] == 0. && pNI[1] == 0. && pNI[mDim-1] == 0. )
        pNO[0] = pNO[1] = pNO[mDim-1] = 0. ;
      else if ( pNO[0] == 0. && pNO[1] == 0. && pNO[mDim-1] == 0. )
        pNI[0] = pNI[1] = pNI[mDim-1] = 0. ;
    }
  }

    

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->ppBc[nBc]->type[0] == 's' ) {

      chunk_struct *pChunk ;
      bndFc_struct *pBf, *pBfB, *pBfE ;
        
      /* Fix all nodes that touch the symmetry plane. They must have their
         normals in the symmetry plane. */
      pBW = bWt + nBc ;

      pChunk = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBfB, &pBfE ) )
        for ( pBf = pBfB ; pBf <= pBfE ; pBf++ )
          if ( pBf->nFace && ( pElem = pBf->Pelem ) && pElem->number ) {
            pFoE = elemType[ pElem->elType ].faceOfElem + pBf->nFace ;
            mV = pFoE->mVertsFace ;
        
            for ( kVx = 0 ; kVx < mV ; kVx++ ) {
              bn.pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              pBn = bsearch ( &bn, pBndNorm, mWallVx,
                              sizeof ( bndNorm_s ), cmp_bndNorm ) ;

              if ( pBn ) {
                /* Remove the component that is normal to the symmetry plane. */
                pN = pBn->bndNorm ;
                pN[ symmCoor ] = 0. ;
              }
            }
          }
    }
    

        
  /* We want unit normals. */
  for ( pBn = pBndNorm ; pBn < pBndNorm + mWallVx ; pBn++ )
    vec_norm_dbl ( pBn->bndNorm, mDim ) ;

    
  /* Repack the normals into the patch groups. */
  for ( kVx = 0 ; kVx < mAllBndVx ; kVx++ ) {
    nVx = bWt->pnVx[kVx] ;
    pBN = bWt->pNrm + kVx*mDim ;

    bn.pVx = bWt->pVx[kVx] ;
    pBn = bsearch ( &bn, pBndNorm, mWallVx, sizeof ( bndNorm_s ), cmp_bndNorm ) ;
    if ( pBn ) {
      pN = pBn->bndNorm ;
      vec_copy_dbl ( pN, mDim, pBN ) ;
    }
  }

  arr_free ( pBndNorm ) ;

  return ( 1 ) ;
}


/******************************************************************************

  make_mp_bndVx:
  Make a list of boundary vertices associated with more than one patch.
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  8Jul13; use cmp_ulong_t where appropriate, instead of cmp_int.
  17Feb13; make *pkVx, nVxMP of type ulong_t.
  31Oct10; revert to the version of 1.28.2, use type 'w', but not the
           other wall types 'i' or 'v'.
  4Jul10; don't use the wall flag, but base the patch test on string reco.
  21Jun06; add warning.
  17Jun06; check m3 flag to disregard already-listed mp-vx
  6Jun06; need to consider multiple occ in same patch via pVx->mark in 
          multiplicity counter.
  17May06; limit to wall bcs only.
  15May06; conceived
  
  Input:
  ------
  pUns:
  bWt:

  Changes To:
  -----------
  mpVx
  pVx->mark, pVx->mark2, pVx->mark3.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_mp_bndVx ( uns_s *pUns, mp_bndVx_s *mpVx ) {


  const faceOfElem_struct *pFoE ;

  vrtx_struct *pVx ;
  bndPatch_struct *pBndPatch ;
  elem_struct *pElem ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  int nBc, kVx, *lsVxMP, i, m ;
  ulong_t nVx, mV, iVx, *pkVx, *nVxMP, mVxMP, *ndxVxMP, mEntries, maxPatchVx=0 ;


  reset_vx_mark2 ( pUns ) ;
  reset_vx_mark3 ( pUns ) ;

  /* Find all m-p bnd vx. Use of vertex markers:
     pVx->mark:  vx seen this patch,
     pVx->mark2: vx seen with any patch,
     pVx->mark3: vx identified as m-p.
      */
  mVxMP = 0 ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    if ( pUns->ppBc[nBc]->type[0] == 'w' ) {

      reset_vx_mark ( pUns ) ;

      /* Mark m-p vx with mark3 and count. */
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
              
            pElem = pBndFc->Pelem ;
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mV = pFoE->mVertsFace ;
              
            for ( kVx = 0 ; kVx < mV ; kVx++ ) {
              pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              if ( pVx->number ) {

                if ( pVx->mark )
                  /* Seen this one before in this patch. Do nothing. */
                  ;
                else if ( pVx->mark2 && !pVx->mark3 ) {
                  /* Multiply referenced, but not yet listed as such. */
                  mVxMP++ ;
                  pVx->mark = pVx->mark3 = 1 ;
                }
                else 
                  /* First encounter. */
                  pVx->mark = pVx->mark2 = 1 ;
              }
            }
          }
    }
  }



  /* Allocate the list of counters. */
  mpVx->mVxMP = mVxMP ;
  nVxMP = mpVx->nVxMP   = arr_calloc ( "mpVx->nVxMP in make_mp_bndVx", pUns->pFam,
                                       mVxMP, sizeof( *mpVx->nVxMP ) ) ;
  ndxVxMP = mpVx->ndxVxMP   = arr_calloc ( "mpVx->ndxVxMP in make_mp_bndVx",
                                           pUns->pFam, mVxMP+1, sizeof( *mpVx->ndxVxMP ) ) ;


  /* Make a list of the m-p bnd vx. */
  mVxMP = 0 ;
  reset_vx_mark ( pUns ) ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    if ( pUns->ppBc[nBc]->type[0] == 'w' ) {
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
              
            pElem = pBndFc->Pelem ;
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mV = pFoE->mVertsFace ;
              
            for ( kVx = 0 ; kVx < mV ; kVx++ ) {
              pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              if ( pVx->number && pVx->mark3 && !pVx->mark ) {
                /* First encounter of a multiply referenced. Add. */
                nVx = pVx->number ;
                nVxMP[ mVxMP++ ] = nVx ;
                pVx->mark = 1 ;
              }
            }
          }
    }
  }


  /* Sort them. */
  qsort ( nVxMP, mVxMP, sizeof ( *nVxMP ), cmp_ulong_t ) ;


  /* Count the number of patches for each vx. */
  mEntries = 0 ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    reset_vx_mark ( pUns ) ;
    if ( pUns->ppBc[nBc]->type[0] == 'w' ) {

      /* Loop over all boundary faces of this patch and scatter the face normal. */
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
              
            pElem = pBndFc->Pelem ;
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mV = pFoE->mVertsFace ;
              
            for ( kVx = 0 ; kVx < mV ; kVx++ ) {
              pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              if ( pVx->number && pVx->mark3 && !pVx->mark ) {
                /* Multiply referenced, but not yet added for this patch. */
                pVx->mark = 1 ;
                nVx = pVx->number ;
                pkVx = bsearch ( &nVx, nVxMP, mVxMP, sizeof ( *nVxMP ), cmp_ulong_t ) ;

                if ( pkVx ) {
                  /* Already listed. */
                  iVx = pkVx - nVxMP ;
                  ndxVxMP[ iVx ]++ ;
                  mEntries++ ;
                }
                else {
                  sprintf ( hip_msg, 
                            "bnd vx %"FMT_ULG" not listed"
                            " in make_mp_bndVx.\n", nVx ) ;
                  hip_err ( warning, 1, hip_msg ) ;
                }
              }
            }
          }
    }
  }

 

  /* Alloc the list of patches. */
  lsVxMP = mpVx->lsVxMP   = arr_calloc ( "mpVx->lsVxMP in make_mp_bndVx",
                                         pUns->pFam, mEntries, sizeof( *mpVx->lsVxMP ) ) ;

  /* Offset the index. */
  mEntries = 1 ;
  maxPatchVx = 0 ;
  for ( i = 0 ; i < mVxMP ; i++ ) {
    m = ndxVxMP[i] ;
    maxPatchVx = MAX( maxPatchVx, m ) ;
    ndxVxMP[i] = mEntries ;
    mEntries += m ;
  }
  ndxVxMP[mVxMP] = mEntries ;

  if ( verbosity > 2 && maxPatchVx > 2 ) {
    sprintf ( hip_msg, "some bnd nodes belong to %"FMT_ULG" patches.", 
              maxPatchVx ) ;
    hip_err ( warning, 2, hip_msg );
  }



  /* Fill the list of patches. Note that the index runs from 1 for the 
     first element, similarly the first boundary with nBc=0 is listed as 1. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    reset_vx_mark ( pUns ) ;
    if ( pUns->ppBc[nBc]->type[0] == 'w' ) {

      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
              
            pElem = pBndFc->Pelem ;
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mV = pFoE->mVertsFace ;
              
            for ( kVx = 0 ; kVx < mV ; kVx++ ) {
              pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              if ( pVx->number && pVx->mark3 && !pVx->mark ) {
                /* Multiply referenced. Add. */
                pVx->mark = 1 ;
                nVx = pVx->number ;
                pkVx = bsearch ( &nVx, nVxMP, mVxMP, sizeof ( *nVxMP ), cmp_ulong_t ) ;

                iVx = pkVx - nVxMP ;
                /* Find the next empty slot. */
                for ( i = ndxVxMP[iVx]-1 ; lsVxMP[i] && i < ndxVxMP[iVx+1]-1 ; i++ ) { 
                  if ( lsVxMP[i]-1 == nBc )
                    break ;
                }
                if ( i == ndxVxMP[iVx+1]-1 ) {
                  sprintf ( hip_msg, 
                            "in make_mp_bndVx:"
                            " no slot to add patch for vx %"FMT_ULG"\n", nVx ) ;
                  hip_err ( fatal, 0, hip_msg ) ;
                }
                else 
                  /* Offset by 1, so 0 is the empty flag. */
                  lsVxMP[i] = nBc+1 ;
              }
            }
          }
    }
  }

  return ( 1 ) ;
}

/******************************************************************************
  pPVx2bWt:   */

/*! return the pointers to the boundary weights (patchwise weighted normals)
 *  for a periodic pair
 *
 */

/*
  
  Last update:
  ------------
  18Sep23; intro return status so incomplete setups can be handled gracefully.
  21Apr16: conceived.
  

  Input:
  ------
  pUns: 
  pPV: periodic vertex pair
  bWt: list of boundary weights/normals

  Returns:
  --------
  *ppWtIn/Out: pointers to the bnd weights for the pair.
  
*/

int pPVx2bWt ( const uns_s *pUns, const perVxPair_s *pPV, bndVxWt_s bWt[],
                double **ppWtIn, double **ppWtOut ) {

  ulong_t nVx ;
  int nBc ;
  ulong_t *pnVx, iVx ;
  
  // Inlet: number of the vx.
  nVx = pPV->In->number ; 
  // number of the bc in pUns that vx is on.
  nBc = find_nBc ( pUns, pPV->pPerBc->pBc[0] ) ;

  if ( nVx == 0 || nBc == -1 )
    // failure
    return ( 1 ) ;
  
  // index of this weight/normal in bWt.
  pnVx = bsearch ( &nVx, bWt[nBc].pnVx, bWt[nBc].mBndVx,
                   sizeof ( *bWt[nBc].pnVx ), cmp_ulong_t ) ;
  if ( !pnVx ) {
    sprintf ( hip_msg, 
              "missing inlet bnd normal vx %"FMT_ULG" in make_bndVxWts.", nVx);
    hip_err ( fatal, 0, hip_msg ) ;
  }
  // pointer to its weight ( = patchwise weighted normal. )
  iVx = pnVx - bWt[nBc].pnVx ;
  *ppWtIn = bWt[nBc].pWt+iVx*pUns->mDim ;


  // Outlet: number of the vx.
  nVx = pPV->Out->number ; 
  // number of the bc in pUns that vx is on.
  nBc = find_nBc ( pUns, pPV->pPerBc->pBc[1] ) ; 
  // index of this weight/normal in bWt.
  pnVx = bsearch ( &nVx, bWt[nBc].pnVx, bWt[nBc].mBndVx,
                   sizeof ( *bWt[nBc].pnVx ), cmp_ulong_t ) ;
  if ( !pnVx ) {
    sprintf ( hip_msg, 
              "missing outlet bnd normal vx %"FMT_ULG" in make_bndVxWts.", nVx);
    hip_err ( fatal, 0, hip_msg ) ;
  }
  // pointer to its weight ( = patchwise weighted normal. )
  iVx = pnVx - bWt[nBc].pnVx ;
  *ppWtOut = bWt[nBc].pWt+iVx*pUns->mDim ;

  return (0) ;
}


/******************************************************************************

  make_bndVxWts:
  Make a list of boundary normals and possibly boundary weights.  To
  begin with gather-scatter the weights.  Then average normals for
  viscous and inviscid walls for all bcs.  Finally, reloop and compare
  the normals to the particular face normals attached to each vertex.
  Set the normal to zero if the angle difference is too large.
  
  Last update:
  ------------
  15jul19 use isMatch_geoType.
  6Sep18; rename reset_vx_mark.
  14jul17; write periodic mismatch diagnostic only if period. exists.
  12May17; intro geoType arg to support interfaces.
  8Jul13; use cmp_ulong_t where appropriate, instead of cmp_int.
  18Feb13; convert int to ulong_t where so defined.
  16Dec08; new interface to mark_uns_vertBc.
  11Apr08; add documentation.
  18Dec06; intro doWts.
  15May06; drop doHydra, 
  4Apr06; new interface to mark_uns_vertBc with dontSglNrm.
  20Dec01; exclude vertices on singular boundaries from being listed with a normal.
  10Aug01; extract make_bndVxNrm.
  10Jan00; write enough output for hydgrd in non-hydra mode.
  30Dec00; fix bugs in non-hydra mode.
  11Nov00; rewrite from make_bndVxNormals.
  
  Input:
  ------
  pUns = the unstructured grid
  geoType = bnd for boundary, int for interface.
  doFV = treat quad faces in a consistent FV dual volume way, rather than simple average
  doWts = calculate not only the list of nodes, but also the weights

  Changes To:
  -----------
  bWt = the list of boundary weights
  pmAllBndVx = the number of boundary nodes in bWt.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_bndVxWts ( uns_s *pUns, bcGeoType_e geoType,
                    bndVxWt_s bWt[], ulong_t *pmAllBndVx, 
                    const int doFV, const int doWts ) {

  const int mDim = pUns->mDim, dontPer = 0, dontAxis = 0 ;
  const faceOfElem_struct *pFoE ;
  ulong_t mVertsThisBc, mBiThisBc, mTriThisBc, mQuadThisBc ;
  int nBeg, nEnd, mTimes ;
  ulong_t nVx ;
  int kVx ;
  ulong_t iVx ;
  ulong_t  mAV, mV, mBndVx ;
  int  nBc, foundPer ;
  ulong_t *pkVx ;
  bndVxWt_s *pBW ;
  chunk_struct *pChunk ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  bndPatch_struct *pBndPatch ;
  elem_struct *pElem ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  double fcNorm[MAX_DIM], *pWt ;



  /* Count the number of boundary vertices. */
  int kBc = -1 ;
  for ( nBc = mAV = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( isMatch_geoType( pUns->ppBc[nBc]->geoType, geoType )
         || geoType == noBcGeoType ) {
      kBc++ ;
      mAV += bWt[kBc].mBndVx = pUns->pmVertBc[nBc] ;
    }
  
  /* Store the sum and the base pointers in bWt[0]. */
  bWt->pVx   = arr_malloc ( "bWt->pVx in make_bndVxWts", pUns->pFam,
                            mAV, sizeof( *bWt->pVx ) ) ;
  bWt->pnVx  = arr_malloc ( "bWt->pnVx in make_bndVxWts", pUns->pFam,
                            mAV, sizeof( *bWt->pnVx ) ) ;
  if ( doWts ) {
    bWt->pWt = arr_calloc ( "bWt->pWt in make_bndVxWts", pUns->pFam,
                            mAV*mDim, sizeof( *bWt->pWt ) ) ; 
  } 
  else {
    bWt->pWt = NULL ;
  } 
  bWt->pNrm = NULL ;



  if ( singleBndVxNormal )
    /* Use the vx->mark2 flag to indicate whether this node is a boundary node. */
    reset_vx_mark2 ( pUns ) ;

    
  /* Loop over all patches and calculate a weight for each node just by
     averaging over the faces of that patch. */
  kBc = -1 ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( isMatch_geoType ( pUns->ppBc[nBc]->geoType, geoType )
         || geoType == noBcGeoType ) {
      kBc++ ;
      pBW = bWt + kBc ;
      /* How many were there in the previous patch? */

      if ( kBc ) {
        /* Make each bWt[] point to some section of the storage. */
        mV =        bWt[ kBc-1 ].mBndVx ;
        pBW->pVx  = bWt[ kBc-1 ].pVx  + mV ;
        pBW->pnVx = bWt[ kBc-1 ].pnVx + mV ;
        pBW->pWt  = ( doWts ? bWt[ kBc-1 ].pWt  + mV*mDim : NULL ) ;
      }
    
      if ( pBW->mBndVx ) {
        /* Mark all vertices of this boundary patch. */
        mark_uns_vertBc ( pUns, nBc, dontPer, dontAxis, singleBndVxNormal,
                          &foundPer,
                          &mVertsThisBc, &mBiThisBc, &mTriThisBc, &mQuadThisBc ) ;
        mBndVx = 0 ;
  
        /* List all vertices of this patch. */
        pChunk = NULL ;
        while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
          for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
            if ( pVx->mark )
              pBW->pVx[mBndVx++] = pVx ;

      
        /* Sort them. */
        qsort ( pBW->pVx, mBndVx, sizeof ( vrtx_struct * ), cmp_vx ) ;

        /* And make a list of their vertex numbers. */
        for ( kVx = 0 ; kVx < mBndVx ; kVx++ )
          pBW->pnVx[kVx] = pBW->pVx[kVx]->number ;



      
        /* Loop over all boundary faces of this patch and scatter the face normal. */
        pBndPatch = NULL ;
        while ( doWts && 
                loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
          for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
            if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
              
              pElem = pBndFc->Pelem ;
              pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
              mV = pFoE->mVertsFace ;
              
              
              if ( mV == 4 && doFV ) {
                /* Treat the quad surface in a FV way, ie. calculate each facet
                   of the dual volume. */
                for ( kVx = 0 ; kVx < mV ; kVx++ ) {
                  pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
                  if ( !pVx->singular ) {
                    nVx = pVx->number ;
                    pkVx = bsearch ( &nVx, pBW->pnVx, mBndVx,
                                     sizeof ( *pBW->pnVx ), cmp_ulong_t ) ;
                    if ( !pkVx )
                      sprintf ( hip_msg, 
                                "missing fv bnd vx %"FMT_ULG" in make_bndVxWts.", nVx);
                    iVx = pkVx - pBW->pnVx ;
                    pWt = pBW->pWt + iVx*mDim ;
                  
                    uns_face_normal_vx ( pElem, pBndFc->nFace, kVx, fcNorm ) ;
                    vec_add_dbl ( pWt, fcNorm, mDim, pWt ) ;
                  }
                }
              }
              else {
                /* Either a simplex or a non FV surface. Calculate an average normal
                   and scatter. */
                uns_face_normal ( pElem, pBndFc->nFace, fcNorm, &mTimes ) ;
                vec_mult_dbl ( fcNorm, 1./mTimes/mV, mDim ) ;
                for ( kVx = 0 ; kVx < mV ; kVx++ ) {
                  pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
                  if ( !pVx->singular && pVx->mark ) {
                    /* This vertex is on this patch. Scatter. */
                    nVx = pVx->number ;
                    pkVx = bsearch ( &nVx, pBW->pnVx, mBndVx,
                                     sizeof ( *pBW->pnVx ), cmp_ulong_t ) ;
                    if ( !pkVx ) {
                      sprintf ( hip_msg, 
                                "missing fe bnd vx %"FMT_ULG" in"
                                " make_bndVxWts.\n", nVx );
                      hip_err ( fatal, 0, hip_msg ) ;
                    }
                    iVx = pkVx - pBW->pnVx ;
                    pWt = pBW->pWt + iVx*mDim ;
                  
                    vec_add_dbl ( pWt, fcNorm, mDim, pWt ) ;
                  }
                }
              }
            }
      }
    }
  

  /* Periodicity. */
  int mVP = pUns->mPerVxPairs ; 
  int n ;
  const perVxPair_s *pPV = pUns->pPerVxPair ;
  //perBc_s *pPerBc ;
  double *pWtIn, *pWtOut, wtIn[MAX_DIM], wtOut[MAX_DIM] ;
  double dm = 0.0 ;
  if ( mVP ) {
    /* Single periodicity. Just average across each periodic edge, but rotate
       vectors before averaging. */
    for ( n = 0 ; n < mVP ; n++ ) {
      if ( pPVx2bWt ( pUns, pPV, bWt, &pWtIn, &pWtOut ) )
        // Incomplete setup. Fail gracefully
        break ;

      rot_coor_dbl ( pWtIn,  pPV->pPerBc->rotIn2out, mDim, wtIn ) ;
      vec_mult_dbl ( wtIn, -1., mDim ) ; // Normals are outward facing, opposite. 
      rot_coor_dbl ( pWtOut, pPV->pPerBc->rotOut2in, mDim, wtOut ) ;
      vec_mult_dbl ( wtOut, -1., mDim ) ; 

      if ( pWtIn == pWtOut ) {
        /* Node on an axis. */
        max_diff_vec_dbl ( wtOut, wtIn, mDim, &dm ) ;
        vec_avg_dbl      ( wtOut, wtIn, mDim, pWtOut ) ;
      }
      else {
        /* Two different vertces. */
        max_diff_vec_dbl ( pWtIn, wtOut, mDim, &dm ) ;
        max_diff_vec_dbl ( pWtOut, wtIn, mDim, &dm ) ;
        vec_avg_dbl ( pWtIn, wtOut, mDim, pWtIn ) ;
        vec_avg_dbl ( pWtOut, wtIn, mDim, pWtOut ) ;
      }
    }

    if ( n == mVP ) {
      sprintf ( hip_msg, "corrected a maximal periodic error in normals of %g.", dm ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
    else {
      sprintf ( hip_msg, "incomplete setup for per pair %d. Bnd node vols incomplete ", n ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }


  *pmAllBndVx = mAV ;

  return ( 1 ) ;
}

/******************************************************************************

  calc_wall_dist:
  .
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  10Nov00; use new make_bndVxWts.
  28Jun00; use two rotational copies for the wall distance, ensure periodicity.
  10Apr00; add periodic copies backward and forward for each viscous node. 
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
double *bndNorm2coor ( const DATA_TYPE *pBn ) {
  return ( (( bndNorm_s * ) pBn)->pVx->Pcoor ) ;
}


double *calc_wall_dist ( uns_s *pUns, bndVxWt_s bWt[], int mAllBndVx ) {

  const int mDim = pUns->mDim ;
  const vrtx_struct *pVx ;
  
  int nDim, nBc, nBeg, nEnd, mViscBndVx, mPerVxBc[MAX_PER_PATCH_PAIRS], found, kVx, mV ;
  double dist, llBox[MAX_DIM], urBox[MAX_DIM],
    *pWd, *pWallDist = NULL, vecVx[MAX_DIM], scProd, *pCoorPer = NULL, *pCoP,
    *pDistIn, *pDistOut ;
  root_struct *pTree ;
  bndVxWt_s *pBW ;
  bndNorm_s *pBndNorm, *pBn, outBn, *pBndNormPer = NULL, *pBnP, *pBnP1, *pBnP2 ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd, *pVrtxPer = NULL, *pVxP, *pVxP1, *pVxP2 ;
  perVx_s *pPerVxBc[MAX_PER_PATCH_PAIRS];
  perVxPair_s *pPerVxP ;
  ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS] ;
  perBc_s *pPerBc ;


  if ( verbosity > 3 )
    printf ( "      INFO: calculating wall distance.\n" ) ;
  
  if ( pUns->pPerBc ) {

    if ( !make_perVxPairs ( pUns, pPerVxBc, ndxPerVxBc, mPerVxBc ) ) {
      sprintf ( hip_msg, "could not establish periodic vertex pairs"
               " in match_per_in_all_edges.\n" );
      hip_err ( fatal, 0, hip_msg ) ; }

    /* Make a duplicate list of vertex entries with everything appearing on In. */
    mult_per_vert ( pUns, mPerVxBc, pPerVxBc, ndxPerVxBc, 1 ) ;
  }


  
  /* Get the size of pUns. */
  get_uns_box ( pUns ) ;

  /* Plant a tree. Get the size. */
  vec_copy_dbl ( pUns->llBox, mDim, llBox ) ;
  vec_copy_dbl ( pUns->urBox, mDim, urBox ) ;
    
  /* Enlarge the box a bit. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    dist = urBox[nDim] - llBox[nDim] ;

    if ( pUns->mPerBcPairs == 1  ) {
      /* Make the box large enough to accept forward and backward rotation/shift. */
      llBox[nDim] -= 2*dist ;
      urBox[nDim] += 2*dist ;
    }

    /* Add a tweak. */
    dist = MAX( .1*dist, Grids.epsOverlap ) ;
    llBox[nDim] -= dist ;
    urBox[nDim] += dist ;
  }
  pTree = ini_tree ( pUns->pFam, "calc_wall_dist", mDim, llBox, urBox, bndNorm2coor ) ;





  /* Make a list of pointers to bndNorm_s. Keep each pBndNorm around such that
     the tree can operate on int. */
  pBn = pBndNorm = arr_malloc ( "pBndNorm in calc_wall_dist", pUns->pFam,
                                mAllBndVx, sizeof( *pBndNorm ) ) ;
  
  /* Add all viscous wall nodes to the tree. */
  for ( mViscBndVx = nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->ppBc[nBc]->type[0] == 'v' ) {
      pBW = bWt + nBc ;
      mV = pBW->mBndVx ;

      for ( kVx = 0 ; kVx < mV ; kVx++ ) {
        pBn->pVx = pBW->pVx[kVx] ;
        vec_copy_dbl ( pBW->pNrm + kVx*mDim, mDim, pBn->bndNorm ) ;
        add_data ( pTree, pBn ) ;
        pBn++ ;
      }
    }
  mViscBndVx = pBn - pBndNorm ;
  

  
  if ( pUns->mPerBcPairs == 1 && mViscBndVx ) {
    /* Duplicate all viscous boundary nodes by rotating forward and backward. */
    pPerBc = pUns->pPerBc ;


    /* Alloc extra vertex and boundary normal spaces. */
    pBnP = pBndNormPer = arr_malloc ( "ppBndNormPer in calc_wall_dist", pUns->pFam,
                                      4*mViscBndVx, sizeof( *pBndNormPer ) ) ;
    pVxP = pVrtxPer    = arr_malloc ( "pVrtxPer in calc_wall_dist", pUns->pFam,
                                      4*mViscBndVx, sizeof( *pVrtxPer ) ) ;
    pCoP = pCoorPer    = arr_malloc ( "pCoorPer in calc_wall_dist", pUns->pFam,
                                      4*mViscBndVx*mDim, sizeof( *pCoorPer ) ) ;
    
    /* Loop over all boundary vertices found, rotate/translate them and their normals.
       Mark them to save us searching for their closest wall node. */
    reset_vx_mark ( pUns ) ;
    for ( pBn = pBndNorm ; pBn < pBndNorm + mViscBndVx ; pBn++ ) {
      pVx = pBn->pVx ;
      (( vrtx_struct * ) pVx)->mark = 1 ;

      /* Forward rotation. Coordinate. */
      pBnP1 = pBnP ;
      pVxP1 = pVxP ;
      pBnP1->pVx = pVxP1 ;
      pVxP1->Pcoor = pCoP ;
      pVxP1->number = pBnP1->pVx->number ;
      rot_coor_dbl ( pVx->Pcoor, pPerBc->rotIn2out, mDim, pVxP1->Pcoor ) ;
      vec_add_dbl ( pVxP1->Pcoor, pPerBc->shftIn2out, mDim, pVxP1->Pcoor ) ;
      /* Normal. */
      rot_coor_dbl ( pBn->bndNorm, pPerBc->rotIn2out, mDim, pBnP1->bndNorm ) ;

      /* Add the rotated/translated boundary vertex.
      pBnN = nearest_data ( pTree, pBnP1, &dist ) ;
      if ( dist > Grids.epsOverlapSq ) */
        add_data ( pTree, pBnP1 ) ;

      pBnP++ ;
      pVxP++ ;
      pCoP += mDim ;


      /* Second forward rotation. Coordinate. */
      pBnP2 = pBnP ;
      pVxP2 = pVxP ;
      pBnP2->pVx = pVxP2 ;
      pVxP2->Pcoor = pCoP ;
      pVxP2->number = pBnP2->pVx->number ;
      rot_coor_dbl ( pVxP1->Pcoor, pPerBc->rotIn2out, mDim, pVxP2->Pcoor ) ;
      vec_add_dbl ( pVxP2->Pcoor, pPerBc->shftIn2out, mDim, pVxP2->Pcoor ) ;
      /* Normal. */
      rot_coor_dbl ( pBnP1->bndNorm, pPerBc->rotIn2out, mDim, pBnP2->bndNorm ) ;

      /* Add the rotated/translated boundary vertex.
      pBnN = nearest_data ( pTree, pBnP2, &dist ) ;
      if ( dist > Grids.epsOverlapSq ) */
        add_data ( pTree, pBnP2 ) ;

      pBnP++ ;
      pVxP++ ;
      pCoP += mDim ;

          

        
      /* Backward rotation. Coordinate. */
      pBnP1 = pBnP ;
      pVxP1 = pVxP ;
      pBnP1->pVx = pVxP1 ;
      pVxP1->Pcoor = pCoP ;
      pVxP1->number = pBnP1->pVx->number ;
      rot_coor_dbl ( pVx->Pcoor, pPerBc->rotOut2in, mDim, pVxP1->Pcoor ) ;
      vec_add_dbl ( pVxP1->Pcoor, pPerBc->shftOut2in, mDim, pVxP1->Pcoor ) ;
      /* Normal. */
      rot_coor_dbl ( pBn->bndNorm, pPerBc->rotOut2in, mDim, pBnP1->bndNorm ) ;

      /* Add the rotated/translated boundary vertex.
      pBnN = nearest_data ( pTree, pBnP1, &dist ) ;
      if ( dist > Grids.epsOverlapSq ) */
        add_data ( pTree, pBnP1 ) ;

      pBnP++ ;
      pVxP++ ;
      pCoP += mDim ;

          
      /* Second backward rotation. Coordinate. */
      pBnP2 = pBnP ;
      pVxP2 = pVxP ;
      pBnP2->pVx = pVxP2 ;
      pVxP2->Pcoor = pCoP ;
      pVxP2->number = pBnP2->pVx->number ;
      rot_coor_dbl ( pVxP1->Pcoor, pPerBc->rotOut2in, mDim, pVxP2->Pcoor ) ;
      vec_add_dbl ( pVxP2->Pcoor, pPerBc->shftOut2in, mDim, pVxP2->Pcoor ) ;
      /* Normal. */
      rot_coor_dbl ( pBnP1->bndNorm, pPerBc->rotOut2in, mDim, pBnP2->bndNorm ) ;

      /* Add the rotated/translated boundary vertex.
      pBnN = nearest_data ( pTree, pBnP2, &dist ) ;
      if ( dist > Grids.epsOverlapSq ) */
        add_data ( pTree, pBnP2 ) ;

      pBnP++ ;
      pVxP++ ;
      pCoP += mDim ;
    }
  }

  if ( verbosity > 3 )
    printf ( "      INFO: %d viscous boundary vertices found.\n", mViscBndVx ) ;

  

  /* Malloc for all vertices. */
  pWd = pWallDist = arr_malloc ( "pWallDist n calc_wall_dist", pUns->pFam,
                                 pUns->mVertsNumbered, sizeof( *pWallDist ) ) ;
  
  if ( !mViscBndVx ) {
    /* No viscous boundaries found. Initialize to a large number. */
    for ( pWd = pWallDist ; pWd < pWallDist + pUns->mVertsNumbered ; pWd++ )
      *pWd = TOO_MUCH ;
  }

  else {
    /* Find the closest viscous wall vertex for each vertex. To save a few bucks,
       mark all viscous boundary wall vertices. They have a 0 walldistance by def.*/
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( pVrtx->number ) {
          
          if ( pVrtx->mark )
            *pWd++ = 0. ;
          else {
            /* Find a match in the tree. Note that dist has the root taken. */
            outBn.pVx = pVrtx ;
            pBn = nearest_data ( pTree, &outBn, &dist ) ;
            
            /* Dot the distance with the wall normal. */
            vec_diff_dbl ( pBn->pVx->Pcoor, pVrtx->Pcoor, mDim, vecVx ) ;
            vec_norm_dbl ( vecVx, mDim ) ;
            scProd = MAX( 0., scal_prod_dbl ( pBn->bndNorm, vecVx, mDim ) ) ;
            
            if ( scProd == 0. )
              /* There is no sensible scalar product, most likely because the
                 normal is set to 0. Use the full distance. */
              *pWd++ = dist ;
            else
              /* Dot it. */
              *pWd++ = dist*scProd ;
          }
        }
  }





  
  /* Periodic exchange. Necessary in cases where an offset of one blade is
     enough for one side, but the other side is closest to a blade two off. */
  for ( found = 0, pPerVxP = pUns->pPerVxPair ;
        pPerVxP < pUns->pPerVxPair+pUns->mPerVxPairs ; pPerVxP++ ) {
    pDistIn  = pWallDist + pPerVxP->In->number - 1 ;
    pDistOut = pWallDist + pPerVxP->Out->number - 1 ;
    if ( ABS( *pDistIn - *pDistOut ) > .001*Grids.epsOverlap ) {
      *pDistIn = *pDistOut = MIN( *pDistIn, *pDistOut ) ;

      if ( !found ) {
        printf ( " WARNING: found and fixed differing wall dist. at periodic nodes.\n"
                 "          this means, two rotations were insufficient and interior.\n"
                 "          wall distances will be slightly off.\n" ) ;
        found = 1 ;
      }
    }
  }

  /* dealloc_and_return: */
  
  arr_free ( pBndNorm ) ;
  del_tree ( &pTree ) ;

  if ( pUns->pPerBc && mViscBndVx ) {
    arr_free ( pBndNormPer ) ;
    arr_free ( pVrtxPer ) ;
    arr_free ( pCoorPer ) ;
  }


  if ( verbosity > 3 )
    printf ( "      INFO: done calculating wall distance.\n" ) ;

  return ( pWallDist ) ;
}

