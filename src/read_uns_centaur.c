/*
 read_uns_centaur.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from a generic .cfdrc file for cpre.

 Last update:
 ------------
 26Sep14; treat large meshes with ulong_t.
 18Dec10; new pBc->type
 11Dec07; fix c5_skip to to accept 0 mLen from non-type 5.
 28Jun07; intro c5 routines:
 20Apr06; treat -1 hybFileType for 2D.
 5Feb04; fix decrement error in pPan2Grp for 2D.
 5July1; conceived


 contains:
 read_uns_cfdrc
 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;


/* A conversion list from Centaur's format to avbp. */
const int c2a[6][8] =
{ {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,2,1,3},        /* tet */
  {0,1,2,3,4},      /* pyramid */
  {0,3,4,1,5,2},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;

void rfail ( char *msg ) {
  sprintf ( hip_msg, "in rfail from read_uns_centaur: %s.\n", msg ) ;
  hip_err ( fatal, 0, hip_msg ) ;
  return ;
}


int bread_char ( FILE *Fhyb, int sE, int mChar, char *pString, char *msg ) {

  int recLen, rLen = mChar*sizeof( char ) ;

  if ( sE ) {
    if ( !fread ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread ( pString, sizeof( char ), mChar, Fhyb ) != mChar )
      rfail ( msg ) ;
  }
  else  {
    if ( !fread_linux ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread_linux ( pString, sizeof( char ), mChar, Fhyb ) != mChar )
      rfail ( msg ) ;
  }

    fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;
  return ( 1 ) ;
}
/******************************************************************************
  bread_int_mt:   */

/*! read a binary record with integers, an empty record is equiv to zero value.
 */

/*
  
  Last update:
  ------------
  19Jul13; intro mt_rec_is_zero
  : conceived.
  

  Input:
  ------
  Fhyb: opened and positioned file
  sE: 1 if same-endian
  mInt: number of int to read
  msg: error message for rfail.

  Output:
  -------
  intVal: allocated data field
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


int bread_int_mt ( FILE *Fhyb, int sE, int mInt, int *intVal, char *msg ) {

  int recLen, rLen = mInt*sizeof( int ) ;
  
  if ( sE ) {
    if ( !fread ( &recLen, sizeof( int ), 1, Fhyb ) )
      rfail ( msg ) ;
    if ( mInt == 1 && recLen == 0 ) {
      /* take this as a zero. */
      *intVal = 0 ;
      return ( 1 ) ;
    }
    else if ( recLen < rLen ||
              fread ( intVal, sizeof( int ), mInt, Fhyb ) != mInt )
      rfail ( msg ) ;
  }
  else {
    if ( !fread_linux ( &recLen, sizeof( int ), 1, Fhyb ) )
      rfail ( msg ) ;
    if ( mInt == 1 && recLen == 0 ) {
      /* take this as a zero. */
      *intVal = 0 ;
      return ( 1 ) ;
    }
    else if ( recLen < rLen ||
              fread_linux ( intVal, sizeof( int ), mInt, Fhyb ) != mInt )
      rfail ( msg ) ;
  }
  
  fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;

  return ( 1 ) ;
}


int bread_int ( FILE *Fhyb, int sE, int mInt, int *intVal, char *msg ) {

  int recLen, rLen = mInt*sizeof( int ) ;
  
  if ( sE ) {
    if ( !fread ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread ( intVal, sizeof( int ), mInt, Fhyb ) != mInt )
      rfail ( msg ) ;
  }
  else {
    if ( !fread_linux ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread_linux ( intVal, sizeof( int ), mInt, Fhyb ) != mInt )
      rfail ( msg ) ;
  }
  
  fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;

  return ( 1 ) ;
}
int bread_uint ( FILE *Fhyb, int sE, int mInt, 
                 unsigned int *uintVal, char *msg ) {

  // cast this, which only works for little-endian machines.
  int retVal = bread_int ( Fhyb, sE, mInt, ( int * ) uintVal, msg ) ;
  return ( retVal ) ; 
}

int bread_1int ( FILE *Fhyb, int sE, int *intVal, char *msg ) {
  return bread_int ( Fhyb, sE, 1, intVal, msg ) ;
}

int bread_1int_mt ( FILE *Fhyb, int sE, int *intVal, char *msg ) {

  /* Variant of bread_1int to work around a poor format for mZones:
     an empty record is equivalent to zero. */
  return bread_int_mt ( Fhyb, sE, 1, intVal, msg ) ;
}

int bread_dbl ( FILE *Fhyb, int sE, int mDbl, double *dblVal, char *msg ) {

  int recLen, rLen = mDbl*sizeof( double ) ;

  if ( sE ) {  
    if ( !fread ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread ( dblVal, sizeof( double ), mDbl, Fhyb ) != mDbl )
      rfail ( msg ) ;
  }
  else { 
    if ( !fread_linux ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread_linux ( dblVal, sizeof( double ), mDbl, Fhyb ) != mDbl )
      rfail ( msg ) ;
  }

  fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;

  return ( 1 ) ;
}

int bread_flt ( FILE *Fhyb, int sE, int mFlt, float *fltVal, char *msg ) {

  int recLen, rLen = mFlt*sizeof( float ) ;
  
  if ( sE ) {
    if ( !fread ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread ( fltVal, sizeof( float ), mFlt, Fhyb ) != mFlt )
      rfail ( msg ) ;
  }
  else {
    if ( !fread_linux ( &recLen, sizeof( int ), 1, Fhyb ) ||
         recLen < rLen ||
         fread_linux ( fltVal, sizeof( float ), mFlt, Fhyb ) != mFlt )
      rfail ( msg ) ;
  }
  
  fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;

  return ( 1 ) ;
}

int bread_skip ( FILE *Fhyb, int sE ) {
  

  int recLen ;
  
  if ( sE ) {
    if ( !fread ( &recLen, sizeof( float ), 1, Fhyb ) )
      return ( 0 ) ;
  }
  else {
    if ( !fread_linux ( &recLen, sizeof( float ), 1, Fhyb ) )
      return ( 0 ) ;
  }

  fseek ( Fhyb, recLen + sizeof( int ), SEEK_CUR ) ;

  return ( 1 ) ;
}

/******************************************************************************
   c5read_uint:   */

/*! read a chunked set of unsigned integers in centaur v5 format.
 *
 */

/*
  
  Last update:
  ------------
  26Sep14; documented, promote mRead etc. to ulong_t
  : conceived.
  

  Input:
  ------
  Fyb: positioned file
  sE: if non-zero, read same-endian (no conversion.)
  mVal: number of values to read
  mLen: number of values per record.
  msg: text for error msg passed to rfail.

  Output:
  -------
  intVal: read unsigned integers
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int c5read_int ( FILE *Fhyb, int sE, ulong_t mVal, ulong_t mLen, 
                 int *intVal, char *msg ) {

  int retVal ;
  ulong_t mRead=0, mToRead ;
  ulong_t mLin ; // could there be an overflow as an intermediate result?
  int i ;
  int *pVal = intVal ;
  char msg2[LINE_LEN] ;
  
  if ( mLen ) {
    mLin = mVal/mLen ;
    if ( mLin*mLen != mVal )
      mLin++ ;
    mLin = MAX( 1, mLin ) ;
  }
  else
    mLin = 1 ;

  for ( i = 0 ; i < mLin ; i++ ) {
    mToRead = MIN( mLen, mVal-mRead ) ;
    sprintf ( msg2, "%s_rec%d", msg, i ) ;

    retVal = bread_int ( Fhyb, sE, mToRead, pVal, msg ) ;

    pVal += mToRead ;
    mRead += mToRead ;
  }
  
  return ( retVal ) ;
}

/******************************************************************************
   c5read_uint:   */

/*! read a chunked set of unsigned integers in centaur v5 format.
 *
 */

/*
  
  Last update:
  ------------
  26Sep14; documented, promote mRead etc. to ulong_t
  : conceived.
  

  Input:
  ------
  Fyb: positioned file
  sE: if non-zero, read same-endian (no conversion.)
  mVal: number of values to read
  mLen: number of values per record.
  msg: text for error msg passed to rfail.

  Output:
  -------
  intVal: read unsigned integers
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int c5read_uint ( FILE *Fhyb, int sE, ulong_t mVal, ulong_t mLen, 
                  unsigned int *intVal, char *msg ) {

  int retVal ;
  ulong_t mRead=0, mToRead ;
  ulong_t mLin ; // could there be an overflow as an intermediate result?
  int i ;
  unsigned int *pVal = intVal ;
  char msg2[LINE_LEN] ;
  
  if ( mLen ) {
    mLin = mVal/mLen ;
    if ( mLin*mLen != mVal )
      mLin++ ;
    mLin = MAX( 1, mLin ) ;
  }
  else
    mLin = 1 ;

  for ( i = 0 ; i < mLin ; i++ ) {
    mToRead = MIN( mLen, mVal-mRead ) ;
    sprintf ( msg2, "%s_rec%d", msg, i ) ;

    retVal = bread_uint ( Fhyb, sE, mToRead, pVal, msg2 ) ;

    pVal += mToRead ;
    mRead += mToRead ;
  }
  
  return ( retVal ) ;
}



/******************************************************************************

  c5read_flt/dbl:
  Read a chunked centaur v5 record for int, float or double.
  
  Last update:
  ------------
  22Sep14; promote mVals to ulong_t
  11Dec07; catch zero mVals in c5_skip for type 4 files.
  28Jun07: conceived.
  
  Input:
  ------
  Fhyb    = opened file
  sE      = 1 for same-endian, 0 for different endian require byte-swapping
  mInt    = number of vals to read
  mLen    = number of vals on one line.
  *intVal = pointer to space
  msg     = identifying text for error message.

  Changes To:
  -----------
  intVal
  
  Returns:
  --------
  the number of values read.
  
*/


int c5read_flt ( FILE *Fhyb, int sE, ulong_t mVal, int mLen, float *fltVal, char *msg ) {

  int retVal, mRead=0, mToRead, mLin, i ;
  float *pVal = fltVal ;
  
  mLin = mVal/mLen ;
  if ( mLin*mLen != mVal )
    mLin++ ;
  mLin = MAX( 1, mLin ) ;

  for ( i = 0 ; i < mLin ; i++ ) {
    mToRead = MIN( mLen, mVal-mRead ) ;

    retVal = bread_flt ( Fhyb, sE, mToRead, pVal, msg ) ;

    pVal += mToRead ;
    mRead += mToRead ;
  }
  
  return ( retVal ) ;
}

int c5read_dbl ( FILE *Fhyb, int sE, ulong_t mVal, ulong_t mLen, 
                 double *dblVal, char *msg ) {

  int retVal, mRead=0, mToRead, mLin, i ;
  double *pVal = dblVal ;
  
  mLin = mVal/mLen ;
  if ( mLin*mLen != mVal )
    mLin++ ;
  mLin = MAX( 1, mLin ) ;

  for ( i = 0 ; i < mLin ; i++ ) {
    mToRead = MIN( mLen, mVal-mRead ) ;

    retVal = bread_dbl ( Fhyb, sE, mToRead, pVal, msg ) ;

    pVal += mToRead ;
    mRead += mToRead ;
  }
  
  return ( retVal ) ;
}


int c5_skip ( FILE *Fhyb, int sE, ulong_t mVal, ulong_t mLen ) {
  /* Skip the next mVal ints. Under type 5 files, this might
     mean several lines. mLen = 0 means no line-length is
     defined, all is on a single line. */

  int retVal = 0, mLin, i ;


  if ( mVal && mLen == 0 ) {
    hip_err ( fatal, 0, " non-zero values to skip on zero line len in c5_skip\n" ) ;
  }
  else if ( mLen == 0 ) {
    /* Skip one line. */
    retVal = bread_skip ( Fhyb, sE ) ;
  }
  else {
    /* Skip mVal values, but at least one record. */
    mLin = mVal/mLen ;
    if ( mLin*mLen != mVal )
      mLin++ ;
    mLin = MAX( 1, mLin ) ;

    for ( i = 0 ; i < mLin ; i++ ) {
      retVal = bread_skip ( Fhyb, sE ) ;
    }
  } 
  
  return ( retVal ) ;
}

/******************************************************************************
  c5read_reclens:   */

/*! read length of data and chunking from Centaur header line.
 *
 *
 */

/*
  
  Last update:
  ------------
  22Sep14: renamed from c5read_2int.
  

  Input:
  ------
  Fhyb: positioned file
  v5: if nonzero, read v5 style of file
  sE: if nonzero, assume same-endianness (no byte swap.)
  msg: error msg to be passed on in case of failure.

  Changes To:
  -----------
  no side effects

  Output:
  -------
  i0: first unsigned int to read
  i1: second unsigned int to read
    
  Returns:
  --------
  number of values read
  
*/

int c5read_reclens ( FILE *Fhyb, int v5, int sE, 
                     ulong_t *u0, ulong_t *u1, char *msg ) {

  unsigned int uiBuf[2] ;
  int retVal ;

  if ( v5 ) 
    retVal = bread_uint ( Fhyb, sE, 2, uiBuf, msg ) ;
  else {
    /* Type -1 (2D) or type 4 file, no length given. Return the number
       of vals as length, they will be on a single line. */
    retVal = bread_uint ( Fhyb, sE, 1, uiBuf, msg ) ;
    uiBuf[1] = uiBuf[0] ;
  }
  *u0 = uiBuf[0] ;
  *u1 = uiBuf[1] ;

  return ( retVal ) ;
}



/******************************************************************************
  centBcType2hip:   */

/*! convert Centaur bc types to hip types.
 */

/*
  
  Last update:
  ------------
  5 Jul 13: conceived.
  

  Input:
  ------
  cBcType: centaur bc type
    
  Returns:
  --------
  hip bc type
  
*/

int centBcType2hip ( int cType ) {

  /* From Centaur example file:
    Each boundary group stores 2 pieces of information:
    bctype is the boundary type (integer) used by our local solver
    The integer classification is as follows:
    1   -1000  - Viscous Wall
    1001-2000  - Inviscid Wall
    2001-3000  - Symmetry
    3001-4000  - Inlet
    4001-5000  - Outlet
    5001-6000  - Farfield
    6001-7000  - Periodic
    7001-8000  - Shadow
    8001-8500  - Interface
    8501-9000  - Wake Surfaces
    9001-10000 - Moving Walls
    10001-      - Other
  */


  return ( 0 ) ;
}
/******************************************************************************
  cent_skip_per:   */

/*! skip records in centaur hybrid file with nodal periodicity.
 */

/*
  
  Last update:
  ------------
  7jul13: cut out of read_centaur.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_skip_per ( FILE* Fhyb, const int sE ) {

  
  /* Skip periodicity. */
  int i ;
  int mVxPer ;
  bread_1int ( Fhyb, sE, &mVxPer, "mPerVxPair" ) ;
  if ( mVxPer ) {
    sprintf ( hip_msg, " ignoring %d periodic vertices, \n"
            " lists are recalculated using patch setup.", mVxPer ) ;
    hip_err ( info, 4, hip_msg ) ;
    for ( i = 0 ; i < mVxPer ; i++ ) {
    /* Skip 4 records with periodicity info relating to nodes. */
      bread_skip ( Fhyb, sE ) ;
      bread_skip ( Fhyb, sE ) ;
      bread_skip ( Fhyb, sE ) ;
      bread_skip ( Fhyb, sE ) ;
    }
  }


  return ( 0 ) ;
}


/******************************************************************************
  cent_read_intfc:   */

/*! skip records in centaur hybrid file with internal interfaces.
 */

/*
  
  Last update:
  ------------
  7jul13: cut out of read_centaur.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_read_intfc ( FILE* Fhyb, const int sE, 
                      int mPanels, int *pPanIsInt ) {

  /* Interface bcs. */
  int mIntFcPanels ;
  bread_1int ( Fhyb, sE, &mIntFcPanels, "mIntFcPanels" ) ;

  int i ;
  for ( i = 0 ; i < mPanels ; i++ ) 
   pPanIsInt[i] = 0 ;

  if ( mIntFcPanels ) {
    int *lsPanIsInt = arr_malloc ( "lsPanIsInt in cent_read_intFc", 
                                   NULL, mIntFcPanels, sizeof(int) ) ;
    bread_int ( Fhyb, sE, mIntFcPanels, lsPanIsInt, "lsPanIsInt" ) ;

    for ( i = 0 ; i < mIntFcPanels ; i++ ) 
      pPanIsInt[ lsPanIsInt[ i ]-1 ] = 1 ;

    arr_free ( lsPanIsInt ) ;
  }


  return ( 0 ) ;
}


/******************************************************************************
  cent_skip_intfc:   */

/*! skip records in centaur hybrid file with internal interfaces.
 */

/*
  
  Last update:
  ------------
  7jul13: cut out of read_centaur.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_skip_intfc ( FILE* Fhyb, const int sE ) {

  /* Interface bcs. */
  int mIntFcPanels ;
  bread_1int ( Fhyb, sE, &mIntFcPanels, "mIntFcPanels" ) ;
  if ( mIntFcPanels ) {
    sprintf ( hip_msg, " ignoring %d interface panels.", mIntFcPanels ) ;
    hip_err ( info, 4, hip_msg ) ;

    bread_skip ( Fhyb, sE ) ;
  }

  return ( 0 ) ;
}



/******************************************************************************
  cent_alloc:   */

/*! read sizes off a centaur hyb file and allocate
 *
 */

/*
  
  Last update:
  ------------
  15jul19; support interfaces, set geoType for interfaces.
  19Dec17; new interface to make_uns.
  26Sep14; promote large int to ulong_t.
  18Oct13; initialise pPanIsInt to zero for 2D.
  Sep13; GS: fix pb with 2D not having internal panel records.
  19Jul13; use bread_1int_mt for mZones, as this is given in the file
           not as zero, but as an empty record.
  6jul13: cut out from read_centaur.
  

  Input:
  ------
  Fhyb: open mesh file

  Changes To:
  -----------

  Output:
  -------
  *pv5: is it a level 5 format file
  sE: 1 if same-endian
  mElems[]: number of elements for each type.
  *pmPanels: number of panels
  *ppPan2Grp: pointer from each panel to a bc group/patch
  *ppPanIsInt: 1 for each panel if the panel is internal.
    
  Returns:
  --------
  pUns with allocated fields.
  
*/

uns_s *cent_alloc ( FILE *Fhyb, int *pv5, int *psE, ulong_t mElems[], 
                    int *pmPanels, int **ppPan2Grp, int **ppPanIsInt ) {

  uns_s *pUns = make_uns ( NULL ) ;
  int rLen, recLen, hybFileType ;
  
  /* Make sure it's an unstructured CENTAUR.  */
  rLen = sizeof( float ) + sizeof ( int ) ;

  /* indicator for same endianness: 
     sE = 1: files are same endian as machine, no swap.
     sE = 0: files are other endian, needs swap. */
  int sE = -1 ;
  float centVersion ;

  if ( sE == -1 ) {
    rewind ( Fhyb ) ;
    fread ( &recLen, sizeof( float ), 1, Fhyb ) ;
    fread ( &centVersion, sizeof( float ), 1, Fhyb ) ;
    fread ( &hybFileType, sizeof( int ), 1, Fhyb ) ;
    if ( centVersion >= 1.0 &&  centVersion <= 40.0 &&
         hybFileType >= -1 &&  hybFileType <= 10 ) {
      sE = 1 ;
      fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;
      if ( verbosity > 2 ) {
        sprintf ( hip_msg, "found same-endian file, version %5.2f, type %i.", 
                  centVersion, hybFileType ) ;
        hip_err ( info, 2, hip_msg ) ;
      }
    }
  }

  if ( sE == -1 ) {
    rewind ( Fhyb ) ;
    fread_linux ( &recLen, sizeof( float ), 1, Fhyb ) ;
    fread_linux ( &centVersion, sizeof( float ), 1, Fhyb ) ;
    fread_linux ( &hybFileType, sizeof( int ), 1, Fhyb ) ;
    if ( centVersion >= 1.0 &&  centVersion <= 40.0 &&
         hybFileType >= -1 &&  hybFileType <= 10 ) {
      sE = 0 ;
      fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;
      if ( verbosity > 2 ) {
        sprintf ( hip_msg, "found other-endian file, version %5.2f, type %i.", 
                  centVersion, hybFileType ) ; 
        hip_err ( info, 2, hip_msg ) ;
      }
    }
  }

  if ( sE == -1 )
    rfail ( " failed to recognise centaur header" ) ;
  *psE = sE ;

  int mDim ;
  pUns->mDim = mDim = ( hybFileType < 0 ? 2 : 3 ) ;
  if ( mDim ==3 && hybFileType < 4 )
    rfail ( " centaur only implemented for type 4 or 5 files" ) ;
  else if ( hybFileType > 5 )
    rfail ( " centaur only implemented up to type 5 files" ) ;




  /* Version 5 can split all long records over several lines. */
  int v5 ;
  *pv5 = v5 = ( hybFileType == 5 ? 1 : 0 ) ; 

  /* Number of nodes, line length. */
  ulong_t mVx ;
  ulong_t mLen ;
  c5read_reclens ( Fhyb, v5, sE, &mVx, &mLen, "mVx,mLen" ) ;
  /* Skip the lines with the coordinates. */
  c5_skip ( Fhyb, sE, mVx, mLen ) ;



  /* Number of elems. */
  ulong_t mBNd ;
  ulong_t mBndFc ;
  if ( mDim == 2 ) {
    c5read_reclens ( Fhyb, v5, sE, mElems+tri, &mLen, "sz mTri" ) ;
    sprintf ( hip_msg, "            Triangular elements:          %-"FMT_ULG"", 
              mElems[tri]) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 3*mElems[tri], 3*mLen ) ;
    
    c5read_reclens ( Fhyb, v5, sE, mElems+qua, &mLen, "sz mQua" ) ;
    sprintf ( hip_msg, "            Quadrangular elements:        %-"FMT_ULG"", 
              mElems[qua] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 4*mElems[qua], 4*mLen ) ;
    
    /* Disregard boundary nodes. */
    c5read_reclens ( Fhyb, v5, sE, &mBNd, &mLen, "sz mBndNd" ) ;
    c5_skip ( Fhyb, sE, 2*mBNd, 2*mLen ) ;
    sprintf ( hip_msg, "            Boundary nodes:               %-"FMT_ULG"", 
              mBNd ) ;
    hip_err ( blank, 3, hip_msg ) ;
    
    c5read_reclens ( Fhyb, v5, sE, &mBndFc, &mLen, "sz mBndFc" ) ;
    sprintf ( hip_msg, "            Boundary Faces:               %-"FMT_ULG"\n", 
              mBndFc ) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 2*mBndFc, 2*mLen ) ;
    c5_skip ( Fhyb, sE, mBndFc, mLen ) ;
  }

  else { /* 3-D */
    c5read_reclens ( Fhyb, v5, sE, mElems+hex, &mLen, "sz mHex" ) ;
    sprintf ( hip_msg, "            Hexahedral elements:          %-"FMT_ULG"", 
              mElems[hex] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 8*mElems[hex], 8*mLen ) ;
    
    c5read_reclens ( Fhyb, v5, sE, mElems+pri, &mLen, "sz mPri" ) ;
    sprintf ( hip_msg, "            Wedge elements:               %-"FMT_ULG"", 
              mElems[pri] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 6*mElems[pri], 6*mLen ) ;
    
    c5read_reclens ( Fhyb, v5, sE, mElems+pyr, &mLen, "sz mPyr" ) ;
    sprintf ( hip_msg, "            Pyramidal elements:           %-"FMT_ULG"", 
              mElems[pyr] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 5*mElems[pyr], 5*mLen ) ;
    
    c5read_reclens ( Fhyb, v5, sE, mElems+tet, &mLen, "sz mTet" ) ;
    sprintf ( hip_msg, "            Tetrahedral elements:         %-"FMT_ULG"", 
              mElems[tet] ) ;
    hip_err ( blank, 3, hip_msg ) ;
    c5_skip ( Fhyb, sE, 4*mElems[tet], 4*mLen ) ;
    
    c5read_reclens ( Fhyb, v5, sE, &mBndFc, &mLen, "sz mBndFc" ) ;
    sprintf ( hip_msg, "            Boundary faces:               %-"FMT_ULG"\n", 
              mBndFc ) ;
    hip_err ( blank, 3, hip_msg ) ;
    /* Skip the bnd faces. */
    c5_skip ( Fhyb, sE, 8*mBndFc, 8*mLen ) ;
    c5_skip ( Fhyb, sE, mBndFc, mLen ) ;
  }



  


 
  /* A number of bc panels can make up a bc group. Read bc's now and
     fill pUns->ppBc as this is needed when reading the faces in 
     the second pass. They come bofore panels/groups in the file. */
  int mPanels ;
  bread_1int ( Fhyb, sE, &mPanels, "mPanels" ) ;
  *pmPanels = mPanels ;

  int *pPan2Grp = *ppPan2Grp 
                = arr_malloc ( "pPan2Grp", pUns->pFam, mPanels, sizeof(int) ) ;
  /* According to the doc, this record is not chunked. */
  bread_int ( Fhyb, sE, mPanels, pPan2Grp, "pan2Grp" ) ;

  /* Bc info. Make a bc for each group. Nothing chunked. */
  int mBc ;
  bread_1int ( Fhyb, sE, &mBc, "mBc" ) ;
  int *pType = arr_malloc ( "bcType", pUns->pFam, mBc, sizeof( int ) ) ;
  char *pText = arr_malloc ( "bcText", pUns->pFam, mBc, 80 ) ;
  bread_int  ( Fhyb, sE,    mBc, pType, "bcType" ) ;
  bread_char ( Fhyb, sE, 80*mBc, pText, "bcText" ) ;

  pUns->mBc = mBc ;
  pUns->ppBc = arr_malloc ( "pUns->ppBc in read_uns_centaur", pUns->pFam,
                            mBc, sizeof( bc_struct * ) ) ;

  /* Make a bc for each group. */
  int k ;
  char *pT ;
  char  bcText[MAX_BC_CHAR] ;
  for ( k = 0 ; k < mBc ; k++ ) {
    pT = pText + k*80 ;
    r1_endstring ( pT, 80 ) ;
    snprintf ( bcText, MAX_BC_CHAR, "%s", pT ) ;
    pUns->ppBc[k] = find_bc ( bcText, 1 ) ;
  }


  arr_free ( pType ) ;
  arr_free ( pText ) ;

  cent_skip_per   ( Fhyb, sE ) ;

  /* Array with int flag for each panel, 1 if it is internal. */
  *ppPanIsInt = arr_malloc ( "pPanIsInt", pUns->pFam, mPanels, sizeof(int) ) ;
  /* internal faces are not written by hyb in 2D */
  int *pInt ;
  int kBc ;
  if ( mDim == 3) {
    cent_read_intfc ( Fhyb, sE, mPanels, *ppPanIsInt ) ;
    for  ( k = 0 ; k < mPanels ; k++ ) {
      if ( (*ppPanIsInt)[k] ) {
        kBc = pPan2Grp[k]-1 ;
        pUns->ppBc[kBc]->geoType = inter ;
      }
    }
  }
  else {
    /* No internal panels given in 2D in the current format. Zero. */
    for ( pInt = *ppPanIsInt ; pInt < *ppPanIsInt+mPanels ; pInt++ )
      *pInt = 0 ;
  }

  int mZones = 0;
  /* Zones are only writtn in 3-D. */
  if ( mDim == 3 ) {
    bread_1int_mt ( Fhyb, sE, &mZones, "mZones" ) ;
    if ( mZones && centVersion < 5.0 ) {
      sprintf ( hip_msg, "found Centaur version %f, hip wants at least 5.0\n"
                "            you may encounter problems when reading zones.\n",
                centVersion ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  
  /* Total numbers. */
  ulong_t mElems2D = mElems[tri] || mElems[qua]  ;
  ulong_t mElems3D = mElems[tet] || mElems[pyr] || mElems[pri] || mElems[hex] ;
  ulong_t mElemsTotal=0 ;
  ulong_t mConnEntries=0 ;
  if ( mElems2D && !mElems3D ) {
    mDim = 2 ;
    mElemsTotal = mElems[tri] + mElems[qua] ;
    mConnEntries = 3*mElems[tri] + 4*mElems[qua] ;
  }
  else if ( !mElems2D && mElems3D ) {
    mDim = 3 ;
    mElemsTotal = mElems[tet] + mElems[pyr] + mElems[pri] + mElems[hex] ;
    mConnEntries = 4*mElems[tet] + 5*mElems[pyr] + 
                   6*mElems[pri] + 8*mElems[hex] ;
  }
  else {
    sprintf ( hip_msg, 
             "hip can't deal with mixed grid of %"FMT_ULG" 2D and %"FMT_ULG" 3D elements.\n", 
             mElems2D, mElems3D ) ;
    hip_err ( fatal, 0,hip_msg ) ;
  }


  /* Allocate the fields of which the sizes are known by now. */
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "            Total number of elements:     %"FMT_ULG"", 
              mElemsTotal ) ;
    hip_err ( blank, 3, hip_msg ) ;
    sprintf ( hip_msg, "            Number of vertices:           %"FMT_ULG"", 
              mVx ) ;
    hip_err ( blank, 3, hip_msg ) ;
    sprintf ( hip_msg, "            Number of boundary faces:     %"FMT_ULG"", 
              mBndFc ) ;
    sprintf ( hip_msg, "            Number of Zones:              %-d\n", 
              mZones ) ;
    hip_err ( blank, 3, hip_msg ) ;
  }

  make_uns_grid ( &pUns, pUns->mDim, mElemsTotal, mConnEntries, 0,
                  mVx, 0, mBndFc, mBc ) ;

  return ( pUns ) ;
}

/******************************************************************************
  cent_read_coor:   */

/*! read coordinates. 
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  6jul13: cut out of read_centaur.
  

  Input:
  ------
  Fhyb
  v5: 1 if it is a v5 file with chunked records
  sE: 1 if same-endian

  Changes To:
  -----------
  pUns[]

  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_read_coor ( FILE* Fhyb, const int v5, const int sE, 
                     uns_s *pUns ) {

  /* There should be a safety check here whether pChunk->pVrtx is big enough.
     but currently there is no tracking in cpre_uns for this size. 
     Consider implementing a function for this in array.c
  */

  /* Coordinates. */
  ulong_t mVx, mLen ;
  c5read_reclens ( Fhyb, v5, sE, &mVx, &mLen, "mVx,mLen" ) ;
  chunk_struct *pChunk = pUns->pRootChunk ;
  int mDim = pUns->mDim ;
  c5read_dbl ( Fhyb, sE, mVx*mDim, mLen*mDim, pChunk->Pcoor+mDim, "pCoor" ) ;

  ulong_t n ;
  vrtx_struct *pVx ;
  for ( n = 1 ; n <= mVx ; n++ ) {
    pVx = pChunk->Pvrtx + n ;
    pVx->number = n ;
    pVx->Pcoor = pChunk->Pcoor + n*mDim ;
    pVx->Punknown = NULL ;
  }
    
  return ( 0 ) ;
}

/******************************************************************************
  cent_read_elems:   */

/*! read elements from a centaur hybrid file.
 */

/*
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  26Sep14; promote large int to ulong_t.
  23Sep14; convert e2n to unsigned int. That should transparently
           work for reading int or unsigned int.
  6jul13: cut out of read_centaur.
  

  Input:
  ------
  Fhyb
  v5: 1 if it is a v5 file with chunked records
  sE: 1 if same-endian
  mElems[]: number of elements per type.

  Changes To:
  -----------
  pUns[]

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_read_elems  ( FILE* Fhyb, const int v5, const int sE, 
                       uns_s *pUns, ulong_t mElems[] ) {

  /* Allocate temporary integer element to node pointers. */
  elType_e nElT ;
  unsigned int  *e2n[hex+1] ; // what is the largest node number? is this
                     // written as int, unsigned int, or long?
  unsigned int mElemsTotal = 0 ;
  for ( nElT = tri ; nElT <= hex ; nElT++ )
    if ( mElems[nElT] ) {
      mElemsTotal += mElems[nElT] ;
      e2n[nElT] = arr_malloc ( "e2n", pUns->pFam, mElems[nElT],
                               elemType[nElT].mVerts*sizeof( *e2n[hex+1] ) ) ;
    }
    else
      e2n[nElT] = NULL ;



  
  /* Elements. */
  if ( verbosity > 4 ) {
    sprintf ( hip_msg, "     reading %u elements.", mElemsTotal ) ;
    hip_err ( blank, 5, hip_msg ) ;
  }

  int mDim = pUns->mDim ;
  ulong_t mLen ;
  if ( mDim == 2 ) {
    c5read_reclens ( Fhyb, v5, sE, mElems+tri, &mLen, "mTri" ) ;
    c5read_uint ( Fhyb, sE, 3*mElems[tri], 3*mLen, e2n[tri], "e2n tri" ) ;
    
    c5read_reclens ( Fhyb, v5, sE, mElems+qua, &mLen, "mQua" ) ;
    c5read_uint ( Fhyb, sE, 4*mElems[qua], 4*mLen, e2n[qua], "e2n qua" ) ;    
  }
  else {
    c5read_reclens ( Fhyb, v5, sE, mElems+hex, &mLen, "mHex" ) ;
    c5read_uint ( Fhyb, sE, 8*mElems[hex], 8*mLen, e2n[hex], "e2n hex" ) ;

    c5read_reclens ( Fhyb, v5, sE, mElems+pri, &mLen, "mPri" ) ;
    c5read_uint ( Fhyb, sE, 6*mElems[pri], 6*mLen, e2n[pri], "e2n pri" ) ;

    c5read_reclens ( Fhyb, v5, sE, mElems+pyr, &mLen, "mPyr" ) ;
    c5read_uint ( Fhyb, sE, 5*mElems[pyr], 5*mLen, e2n[pyr], "e2n pyr" ) ;

    c5read_reclens ( Fhyb, v5, sE, mElems+tet, &mLen, "mTet" ) ;
    c5read_uint ( Fhyb, sE, 4*mElems[tet], 4*mLen, e2n[tet], "e2n tet" ) ;
  }


  /* Loop over all element types. */
  chunk_struct *pChunk = pUns->pRootChunk ;
  elem_struct *pElem = pChunk->Pelem ;
  vrtx_struct **ppVrtx = pChunk->PPvrtx ;
  vrtx_struct *pVrtx = pChunk->Pvrtx ;
  ulong_t mEl = 0 ;
  int mVxEl ;
  unsigned int *nFrmVx ;
  ulong_t n ;
  int kVx ;
  for ( nElT = tri ; nElT <= hex ; nElT++ ) {
    /* Normally this is set upon numbering/recount, we need this already
       in cent_read_zones. */
    pUns->mElemsOfType[nElT] = mElems[nElT] ;
    
    mVxEl = elemType[nElT].mVerts ;
    nFrmVx = e2n[nElT] - mVxEl ;
    for ( n = 0 ; n < mElems[nElT] ; n++ ) {
      nFrmVx += mVxEl ;

      pElem++ ;
      pElem->number = ++mEl ;
      pElem->elType = nElT ;
      pElem->PPvrtx = ppVrtx ;
      for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) 
	*(ppVrtx++) = pVrtx + nFrmVx[ c2a[nElT][kVx] ] ;
    
#ifdef ADAPT_HIERARCHIC
      /* pElem is still unrefined. */
      pElem->refdEdges = pElem->markdEdges = 0 ;
      pElem->PrefType = NULL ;
      pElem->Pparent = NULL ;
      pElem->PPchild = NULL ;
      pElem->root = pElem->leaf = pElem->term = 1 ;
      pElem->invalid = 0 ;
#endif
    }
  }
  
  return ( 0 ) ;
}

/******************************************************************************
  cent_read_bnd:   */

/*! read boundary patches and faces off a hybrid centaur file.
 */

/*
  
  Last update:
  ------------
  9Jul15; cast pnGroup to int when testing for underflow.
  19Dec14; update pUns->pRootChunk->mBndFace with actually used faces.
  26Sep14; use ulong_t for large int.
  18Oct13; re-enable block that includes only valid external groups.
  : conceived.
  

  Input:
  ------
  Fhyb: file
  sE: 1 if same-endian

  Changes To:
  -----------
  *pUns ;

    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_read_bnd ( FILE* Fhyb, int v5, int sE, uns_s *pUns, 
                    int mPanels, int *pPan2Grp, int *pPanIsInt ) {

  /* Boundary faces. */
  ulong_t mBndFc = pUns->pRootChunk->mBndFaces ;
  if ( verbosity > 4 ) {
    sprintf ( hip_msg, "     reading %"FMT_ULG" boundary faces.", mBndFc ) ;
    hip_err( blank, 5, hip_msg ) ;
  }

  int mDim = pUns->mDim ;
  unsigned int *pnFrmVx, *pnGroup ;
  ulong_t mB ;
  ulong_t mLen ;
  bndFcVx_s *pBv ;
  chunk_struct *pChunk = pUns->pRootChunk ;
  unsigned int *pnF ;
  int mBc = pUns->mBc ;
  ulong_t mBndFcUsed = 0 ;
  ulong_t mVal ;
  ulong_t nB ;

  pUns->mBndFcVx = mBndFc ;
  pUns->pBndFcVx = arr_malloc ( "pUns->pBndFcVx in cent_read_bnd", pUns->pFam,
                                mBndFc, sizeof( bndFcVx_s ) ) ;
  pnGroup = arr_malloc ( "pnGroup in cent_read_bnd", pUns->pFam,
                         mBndFc, sizeof( *pnGroup ) ) ;

  if ( mDim == 2 ) {
    pnFrmVx = arr_malloc ( "pnFrmVx in cent_read_bnd", pUns->pFam,
                           mBndFc, 2*sizeof( *pnFrmVx ) ) ;
    /* Disregard boundary nodes. */
    c5read_reclens ( Fhyb, v5, sE, &mB, &mLen, "sz mBndNd" ) ;
    mVal = 2*mB ; // promote to correct type.
    c5_skip ( Fhyb, sE, mVal, 2*mLen ) ;


    c5read_reclens ( Fhyb, v5, sE, &mVal, &mLen, "mBndFc" ) ;
    if ( mVal != mBndFc ) {
      sprintf ( hip_msg, 
                "mismatch in 2D cent_read_bnd: "
                "expected %"FMT_ULG", found %"FMT_ULG" faces.\n", 
                mBndFc, mVal ) ;
      hip_err ( fatal, 0, hip_msg ) ; 
    }
    c5read_uint ( Fhyb, sE, 2*mBndFc, 2*mLen, pnFrmVx, "pnFrmVx" ) ;
    c5read_uint ( Fhyb, sE,   mBndFc,   mLen, pnGroup, "pnGroup" ) ;

    pBv = pUns->pBndFcVx ;
    for ( nB = 0 ; nB < mBndFc ; nB++ ) {
      pBv->mVx = 2 ;

      /* Boundary masks are given 1, 2 .... ppBc starts at 0. */
      if ( ((int)pnGroup[nB])-1 < 0  ||((int)pnGroup[nB])-1 > mPanels ||
           pPan2Grp[ pnGroup[nB]-1 ] < 0 ||
           pPan2Grp[ pnGroup[nB]-1 ] > mBc ) {
        sprintf ( hip_msg, 
                  "unidentified panel %d, group %d for 2D face %"FMT_ULG".\n",
                  pnGroup[nB], pPan2Grp[ pnGroup[nB]-1 ]+1, nB+1 ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      /* JDM, 18ct13: in 2-D pPanIsInt is now properly initialised to zero. */
      if ( !pPanIsInt[ pnGroup[nB]-1 ] ) {
        // JDM to fix: set geoType 
       /* This is a valid external boundary face. */
       mBndFcUsed++ ;
       pBv->ppVx[0] = pChunk->Pvrtx + pnFrmVx[2*nB] ;
       pBv->ppVx[1] = pChunk->Pvrtx + pnFrmVx[2*nB+1] ;
       /* Boundary masks are given 1, 2 .... ppBc starts at 0. */
       pBv->pBc = pUns->ppBc[ pPan2Grp[ pnGroup[nB]-1 ]-1 ] ;
       pBv++ ;
      }
    }
  }
  
  else {
    /* 3D */  
    pnFrmVx = arr_malloc ( "pnFrmVx in read_adf_bnd", pUns->pFam,
                           mBndFc, 8*sizeof( *pnFrmVx ) ) ;

    c5read_reclens ( Fhyb, v5, sE, &mVal, &mLen, "mBndFc" ) ;
    if ( mVal != mBndFc ) {
      sprintf ( hip_msg, 
                "mismatch in 3D cent_read_bnd: "
                "expected %"FMT_ULG", found %"FMT_ULG" faces.\n", 
                mBndFc, mVal ) ;
      hip_err ( fatal, 0, hip_msg ) ; 
    }
    c5read_uint ( Fhyb, sE, 8*mBndFc, 8*mLen, pnFrmVx, "pnFrmVx" ) ;
    c5read_uint ( Fhyb, sE,   mBndFc,   mLen, pnGroup, "pnGroup" ) ;

    pBv = pUns->pBndFcVx ;
    for ( nB = 0 ; nB < mBndFc ; nB++ ) {
      pnF = pnFrmVx +8*nB ;

      /* Boundary masks are given 1, 2 .... ppBc starts at 0. */
      if ( pnGroup[nB]-1 < 0  ||pnGroup[nB]-1 > mPanels ||
           pPan2Grp[ pnGroup[nB]-1 ] < 0 ||
           pPan2Grp[ pnGroup[nB]-1 ] > mBc ) {
        sprintf ( hip_msg, "unidentified panel %d, group %d for 3D face %"FMT_ULG".\n",
                  pnGroup[nB], pPan2Grp[ pnGroup[nB]-1 ]+1, nB+1 ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }


      mBndFcUsed++ ;
      pBv->ppVx[0] = pChunk->Pvrtx + pnF[0] ;
      pBv->ppVx[1] = pChunk->Pvrtx + pnF[1] ;
      pBv->ppVx[2] = pChunk->Pvrtx + pnF[2] ;
      
      if ( pnF[3] ) {
        pBv->ppVx[3] = pChunk->Pvrtx + pnF[3] ;
        pBv->mVx = 4 ;
      }
      else {
        pBv->ppVx[3] = NULL ;
        pBv->mVx = 3 ;
      }
           
      pBv->pBc = pUns->ppBc[ pPan2Grp[ pnGroup[nB]-1 ]-1 ] ;
      pBv++ ;
    }
  }

  if ( mBndFcUsed < mBndFc ) {
    sprintf( hip_msg, "dropping %"FMT_ULG" internal faces.", 
             mBndFc-mBndFcUsed ) ;
    hip_err ( info, 2, hip_msg ) ;
  }
  pUns->pRootChunk->mBndFaces = pUns->mBndFcVx = mBndFcUsed ;

  arr_free ( pnFrmVx ) ;
  arr_free ( pnGroup ) ;

  arr_free ( pPan2Grp ) ;
  arr_free ( pPanIsInt ) ;

  /* Skip the panel information, already read in cent_alloc. */
  bread_1int ( Fhyb, sE, &mPanels, "mPanels" ) ;
  bread_skip ( Fhyb, sE ) ; // "pan2Grp" ) ;
  bread_1int ( Fhyb, sE, &mBc, "mBc" ) ;
  bread_skip  ( Fhyb, sE ) ; // "bcType" ) ;
  bread_skip ( Fhyb, sE ) ; // "bcText" ) ;

  return ( 0 ) ;
}

/******************************************************************************
  cent_read_zones:   */

/*! read zones from a centaur hybrid file.
 */

/*
  
  Last update:
  ------------
  21Dec16; add doWarn arg to add_zone.
  23Sep14; promote fidx to unsigned int, mElemsZoned to ulong_t.
  27Jun14; rename zone_elem_add_range to zone_elem_mod_range
  12Dec13; new interface to zone_add.
  19Jul13; use bread_1int_mt for mZones, as this is given in the file
           not as zero, but as an empty record.
  7jul13: conceived.
  

  Input:
  ------
  Fhyb: file
  sE: 1 for same-endian.

  Changes To:
  -----------
  *pUns

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cent_read_zones ( FILE* Fhyb, int v5, int sE, uns_s *pUns ) {


  /* Zones. Only version >=5 treated here. */
  int mZones ;
  bread_1int_mt ( Fhyb, sE, &mZones, "mZones" ) ;
  if ( mZones ) {

    if ( mZones > MAX_ZONES ) {
      sprintf ( hip_msg, "found %d zones, hip can handle only %d"
                " in cent_read_zones Increase MAX_ZONES.", mZones, MAX_ZONES ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    // ! Read the number of zones and info about each zone
    // ! The zonestart array describes where the cells/faces for each zone
    // ! begin in the overall cell lists
    // ! zonestart(1,izone) = Starting Hexahedra Number in ihexnod array
    // ! zonestart(2,izone) = Starting Prism Number in iprisnod array
    // ! zonestart(3,izone) = Starting Pyramid Number in iprmdnod array
    // ! zonestart(4,izone) = Starting Tetrahedra Number in itetnod array
    // ! zonestart(5,izone) = Starting Boundrary Face Number in ibfnt array
    // ! zonename(izone) = Zone Name and this is a character string (*80)
    // ! zonefamily(izone) = Zone Family Name for this zone and 
    // ! this is a character string (*80)
    //
    // read(17) nzones
    // read(17,iostat=ierr) (zone_start(1:5,iz), zonename(iz), iz = 1,nzones)
    // if(version.gt.4.99)then
    //   read(17,iostat=ierr) (zonefamily(iz), iz = 1,nzones)
    // endif

    /* Extend this array by 1 to close the lidx. */
    unsigned int *fidxElT_zn = 
      arr_malloc ( "fidxElT_zn in cent_read_zones", 
                                   pUns->pFam, mZones+1, 
                                   5*sizeof(*fidxElT_zn) ) ;
    char *znName =  arr_malloc ( "znName in cent_read_zones", 
                                 pUns->pFam, mZones, 
                                 80*sizeof(*znName) ) ;

    /* Messy mixed type. This needs to be done by hand. */
    int recLen, rLen = mZones*( 5*sizeof( int ) + 80*sizeof( char ) ) ;
    unsigned int *fidx = fidxElT_zn ;
    char *pZNm = znName ;
    int k ; 

    /* Starting indices are listed for 5 elem types in 3-D, for 2 in 2-D. */
    int mElTFl = ( pUns->mDim == 2 ? 2 : 5 ) ;
    
    if ( sE ) {
      if ( !fread ( &recLen, sizeof( int ), 1, Fhyb ) || recLen < rLen )
        rfail ( "zone info" ) ;
      else {
        for ( k = 0 ; k < mZones ; k++ ) {
          fread ( fidx, sizeof( int ), mElTFl, Fhyb ) ;
          fidx += mElTFl ; 
          fread ( pZNm, sizeof(char), 80, Fhyb ) ; 
          pZNm += 80 ;
        }
      }
    }
    else {
      if ( !fread_linux ( &recLen, sizeof( int ), 1, Fhyb ) || recLen < rLen )
        rfail ( "zone info" ) ;
      else {
        for ( k = 0 ; k < mZones ; k++ ) {
          fread_linux ( fidx, sizeof( int ), mElTFl, Fhyb ) ;
          fidx += mElTFl ; 
          fread_linux ( pZNm, 1, 80, Fhyb ) ; 
          pZNm += 80 ;
        }
      }
    }
    fseek ( Fhyb, recLen - rLen + sizeof( int ), SEEK_CUR ) ;

    if ( v5 && pUns->mDim == 3 ) {
      /* Skip zone families, only given in 3-D for v5 format files. */
      bread_skip ( Fhyb, sE ) ;
    }



    /* Extend the fidx at the end to close. Note that fidx has been
       properly incremented at the exit of the previous loop. */
    int iTyp = 0 ;
    int mElT = ( pUns->mDim == 2 ? qua-tri+1 : hex-tet+1 ) ; // ignore faces.
    elType_e elT ;
    elType_e c2elT_3d[4] = { hex, pri, pyr, tet } ;
    elType_e c2elT_2d[2] = { tri, qua } ;
    elType_e *c2elT = ( pUns->mDim == 2 ? c2elT_2d : c2elT_3d ) ; 
    ulong_t mElemsZoned = 0 ;
    ulong_t elT_offset ; // first element of this type in the list. 
    ulong_t nElBeg, nElEnd ;
    for ( iTyp = 0 ; iTyp < mElT ; iTyp++ ) {
      elT = c2elT[iTyp] ;
      fidx[iTyp] = pUns->mElemsOfType[ elT ] + 1 ;
    }

    /* Add zones. */
    int nZone[MAX_ZONES] ;
    char znNm_c[TEXT_LEN] ;
    for ( k = 0 ; k < mZones ; k++, fidx += mElTFl ) {
      /* Add the zone. */
      strncpy( znNm_c, znName + k*80, 79) ;
      trim ( znNm_c ) ; // remove trailing blanks. 
      if ( verbosity > 1 ) {
        sprintf ( hip_msg, "            Reading zone:                 %-s", 
                  znNm_c) ; 
        hip_err ( blank, 0, hip_msg ) ;
      }
      const int doWarnDupl = 1 ;
      nZone[k] = zone_add ( pUns, znNm_c, 0, doWarnDupl ) ;
    }

    arr_free ( znName ) ;

    /* We exploit here that the elements are read in the same
       order (hex -> tet ) as the fidx. */
    for ( iTyp = 0 ; iTyp < mElT ; iTyp++ ) {
      elT = c2elT[iTyp] ;
      fidx = fidxElT_zn + iTyp ;
      elT_offset = mElemsZoned ;

      for ( k = 0 ; k < mZones ; k++, fidx += mElTFl ) {
        /* Add the elements of this type to this zone. */
        nElBeg = elT_offset+fidx[0] ;
        nElEnd = elT_offset+fidx[mElTFl]-1 ;
        if ( nElEnd - nElBeg +1 )
          zone_elem_mod_range ( pUns, nZone[k], nElBeg, nElEnd ) ;
      }
      mElemsZoned += pUns->mElemsOfType[elT] ;
    }

    arr_free ( fidxElT_zn ) ;
  }

  return ( 0 ) ;
}


/******************************************************************
 
 read_uns_centaur.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an hybrid Centaursoft file.

 Last update:
 ------------
  1Jul16; new interface to check_uns.
 22Sep14; promote mElems to ulong_t
 5Jul13; intro zones, tidy up.
 17Dec10; fix bug with TEXT_LEN in r1_fopen
 4Apr09; replace varTypeS with varList.
 11Dec07; fix bug with reading type 4 with zero mLen.
 28Jun07; treat broken lines with type 5 files.
 20Apr06; treat -1 hybFileType for 2D.
 5July1; conceived

 Input:
 ------


 Changes to:
 -----------


 Returns:
 --------
 
*/



int read_uns_centaur ( char* fileName ) {
  
//  vrtx_struct **ppVrtx, *pVrtx, *pVx ;
//  grid_struct *pGrid ;
//  
//  
//  int mDim, mVx,
//    mBndFc, mPanels,
//    mBc, *pPan2Grp, *pType, mElems2D, mElems3D, mElemsTotal, mConnEntries,
//      *nFrmVx, kVx, nB, mEl, mVxEl, *pnF, sE, v5, mLen ;
  
  
  
  hip_err ( blank, 2, "\n  Reading unstructured centaur." ) ;
  
  prepend_path ( fileName ) ;
  FILE *Fhyb ;
  if ( !( Fhyb = r1_fopen ( fileName, TEXT_LEN, "r" ) ) ) {
    sprintf ( hip_msg, "could not open %s\n", fileName ) ;
    hip_err ( fatal, 0, hip_msg ) ; }



  /* Get sizes and allocate. */
  int v5 ; // is it a v5 centaur file with chunks?
  int sE ; // same-endian?
  ulong_t  mElems[hex+1] = {0,0,0,0,0,0} ;
  int mPanels ;
  int *pPan2Grp ; // panel -> bc, filled in _alloc, freed in read_bnd. 
  int *pPanIsInt ; // 1 for each panel that is internal.
  uns_s *pUns = cent_alloc ( Fhyb, &v5, &sE, 
                             mElems, &mPanels, &pPan2Grp, &pPanIsInt ) ;





  /* Rewind and read data. */
  rewind ( Fhyb ) ;
  /* Skip header. */
  bread_skip ( Fhyb, sE ) ;

  cent_read_coor  ( Fhyb, v5, sE, pUns ) ;
  cent_read_elems ( Fhyb, v5, sE, pUns, mElems ) ;
  cent_read_bnd   ( Fhyb, v5, sE, pUns, mPanels, pPan2Grp, pPanIsInt ) ;
  

  cent_skip_per   ( Fhyb, sE ) ;
  /* 2D does not support internal panels */
  /* 2D dos not have a switch for no zones so we do not support zones in 2D */
  if ( pUns->mDim == 3 ) {
    cent_skip_intfc ( Fhyb, sE ) ;
    cent_read_zones ( Fhyb, v5, sE, pUns ) ;
  }







  /* Match elements to boundary faces. */
  if ( !match_bndFcVx ( pUns ) ) {
    sprintf ( hip_msg, "could not match boundary faces in read_adf_bnd.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
    

  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;
  
  return ( 1 ) ;
}














