/*
   adapt_meth.c:
   Adaptive methods.
 

   Last update:
   ------------
   18Dec10; new pBc->type
   7Jan05; replace heap_cmp_double with cmp_double.
   15May01; intro calc_avg
   14Sep98; write an emtpy adEdge file if there are no edges in write_adEdge.


   This file contains:
   -------------------
   calc_M_R_D
   calc_avg
   calc_diff
   number_uns_elemFromVerts:
   number_uns_elem_leafNprt:
   adaptLvl_elem:
   adapt_reset:
   adapt_nr:
   match_only_per_edges:
   find_per_edge:

   printelal:

   write_adEdge:
   read_adEdge:

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"


extern const int verbosity ;
extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const int bitEdge[MAX_EDGES_ELEM] ;

extern char hip_msg[] ;


/******************************************************************************

  std_distr:
  Calculate the average and mean deviation.
  
  Last update:
  ------------
  14Apr01; consider TOO_MUCH tags.
  1Apr01: conceived.
  
  Input:
  ------
  val[]: a list of scalars. A value > TOO_MUCH is considered a non-entry.
  mVals: how many

  Changes To:
  -----------
  pAvg: the average scalar.
  pDev: the mean deviation from the average.
  
*/

int std_distr ( double val[], int mVals, double *pAvg, double *pDev ) {
  
  int n, m = 0 ;
  double avg = 0., dev = 0. ;


  /* Calculate an average. */
  for ( avg = 0., n = 0 ; n < mVals ; n++ )
    if ( val[n] < TOO_MUCH ) {
      avg += val[n] ;
      m++ ;
    }
  avg /= m ;


  /* Calculate the average deviation. */
  for ( dev = 0., n = 0 ; n < mVals ; n++ )
    if ( val[n] < TOO_MUCH )
      dev += ABS( val[n] - avg ) ;
  dev = dev/m ;

  *pAvg = avg ;
  *pDev = dev ;


  if ( verbosity > 4 ) {
    FILE *dpl ;
    char dplName[LINE_LEN] ;
    int i ;
    double *vcp = arr_malloc ( "vcp", NULL, mVals, sizeof( double ) ) ;

    /* Copy the list and sort the copy. */
    memcpy ( vcp, val, mVals*sizeof( double ) ) ;
    qsort ( vcp, mVals, sizeof( double ), cmp_double ) ;

    
    printf ( " writing vf distribution to vf.ldpl.\n" ) ;
    
    strcpy ( dplName, "vf.ldpl" ) ;
    prepend_path ( dplName ) ;
    dpl = fopen ( dplName, "w" ) ;

    fprintf ( dpl, "vf distribution\n" ) ;

    fprintf ( dpl, "%d %g\n", mVals-1, avg-dev ) ;
    fprintf ( dpl, "%d %g\n", 0, avg-dev ) ;
    fprintf ( dpl, "%d %g\n", 0, avg ) ;
    fprintf ( dpl, "%d %g\n", mVals-1, avg ) ;
    fprintf ( dpl, "%d %g\n", mVals-1, avg+dev ) ;
    fprintf ( dpl, "%d %g\n", 0, avg+dev ) ;

    
    for ( i = mVals-1 ; i >= 0 ; i-- )
      if ( val[n] < TOO_MUCH )
        fprintf ( dpl, "%d %g\n", mVals-1-i, vcp[i] ) ;


    
    fclose ( dpl ) ;
    arr_free ( vcp ) ;
  }

  
  return (m) ;
}

		
/******************************************************************************
  mark_all_edges:   */

/*! Tag all edges for global refinement.
 *
 *
 */

/*
  
  Last update:
  ------------
  6Jan15: conceived.
  

  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns-> ... ->pElem->markdEdges
  
*/

void mark_all_edges ( uns_s *pUns ) {

  chunk_struct *pChunk = NULL ;
  elem_struct *pElBeg, *pElEnd, *pElem ;
  const elemType_struct *pElT ;
  int kEg ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) 
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      pElT = elemType + pElem->elType ;

      for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ )
        pElem->markdEdges |= bitEdge[kEg] ;
    }

  return;
}



void mark_edges ( elem_struct *pElem,
                  int (*x_scalar) ( const elem_struct*, const int,
                                    double* , double* , double* ),
                  const int kVar,
                  const double valDeref, const double valRef ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  double maxVal, avgVal, edgeVal[MAX_EDGES_ELEM] ;
  int kEg ;
  
  /* Calculate the scalar edge values. */
  (*x_scalar) ( pElem, kVar, &maxVal, &avgVal, edgeVal ) ;

  if ( maxVal < valDeref )
    /* Derefine. */
    pElem->derefElem = 1 ;

  else
    for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ )
      if ( edgeVal[kEg] > valRef )
        pElem->markdEdges |= bitEdge[kEg] ;

  return ;
}

/******************************************************************************
  calc_M_R_D :
  Calculate max of the divergence and vorticity in Elem
  with the Green relation
  
  Last update:
  ------------
  : conceived.  
  Input:
  ------
        Pelem point to the Elem
	elemnorms table of normals of the Elem
	
  Output:
  -------
        MRD max of vorticity and Divergence in the Elem
       
  Changes To:
  -----------  
  Returns:
  --------
  0 on failure, 1 on success.

*/

int calc_M_R_D ( const elem_struct *Pelem, const int kVar,
	         double *PMRD,
		 double *PAvgMRD,
		 double EdgeMRD[]) {
  
  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  const elemType_struct *Pelt = elemType + Pelem->elType ;
  int i, kvx0, kvx1, kEdge, mVxHg=0, kVxHg[MAX_ADDED_VERTS],
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1] ;
  double du, dv, dw, sigma, MRD, dV,
    dudi[MAX_DIM]={0.}, dvdi[MAX_DIM]={0.}, dwdi[MAX_DIM]={0.},
    vort[MAX_DIM]={0.}, div=0. , rot=0., AvgMRD=0.,
    nodeNorm[MAX_VX_ELEM][MAX_DIM]={{0.}} ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;
 
  /* Initialisation */
  du = 0. ; dv= 0. ; dw = 0. ;
  for ( kEdge = 0 ; kEdge < Pelt->mEdges ; kEdge++ )
    EdgeMRD[kEdge] = 0. ;
 
  /* Are there any Hanging Nodes? */
  get_drvElem_aE ( pUns, Pelem, &mVxHg, kVxHg, PhgVx, fixDiag, diagDir ) ;
  /* Calculate normals. */
  drvElem_normls ( Pelem, mVxHg, kVxHg, PhgVx, fixDiag, diagDir, nodeNorm ) ;
  
  /*calc surface or volume */
  sigma = drvElem_volume ( pUns, Pelem ) ;
  
  /* Vorticity and Divergence */
  for ( kEdge = 0 ; kEdge < Pelt->mEdges ; kEdge++ ) {
    /* Pick the two nods of the Edge */
    kvx0 = Pelt->edgeOfElem[kEdge].kVxEdge[0] ;
    kvx1 = Pelt->edgeOfElem[kEdge].kVxEdge[1] ;
    
    /* Calc. the differences of "composantes" between these nods */
    du =  Pelem->PPvrtx[kvx1]->Punknown[1]
      + Pelem->PPvrtx[kvx0]->Punknown[1] ;
    dv =  Pelem->PPvrtx[kvx1]->Punknown[2]
      + Pelem->PPvrtx[kvx0]->Punknown[2] ;
    if ( Pelt->mDim == 3 )
      dw =  Pelem->PPvrtx[kvx1]->Punknown[3]
	+ Pelem->PPvrtx[kvx0]->Punknown[3] ;
    else dw = 0. ;
    
    for ( dV=0., i = 0 ; i < Pelt->mDim ; i++ ) {
      /* Spatial Variations */ 
      dudi[i] = du*(nodeNorm[kvx0][i]+nodeNorm[kvx1][i]) ;
      dvdi[i] = dv*(nodeNorm[kvx0][i]+nodeNorm[kvx1][i]) ;
      dwdi[i] = dw*(nodeNorm[kvx0][i]+nodeNorm[kvx1][i]) ;       
      /* Composant of velocity variat. */
      dV =  Pelem->PPvrtx[kvx1]->Punknown[i+1]
	- Pelem->PPvrtx[kvx0]->Punknown[i+1] ;
      /* put in the table the sqnorm of velocity variation of this Edge */
      EdgeMRD[kEdge] += dV*dV ;  
    }
    /* Average */
    AvgMRD +=  EdgeMRD[kEdge] ;
    
    /* Vorticity vector and Trace => divergence */
    vort[0] += (dwdi[1] - dvdi[2]) ;
    vort[1] += (dudi[2] - dwdi[0]) ;
    vort[2] += (dvdi[0] - dudi[1]) ;
    div += (dudi[0] + dvdi[1] + dwdi[2]) ; 
  }
  /* Sq. Norm */ 
  for ( i = 0 ; i < 3 ; i++ )
    rot += vort[i]*vort[i] ;
  rot /= 4 ;
  rot /= sigma*sigma ;
  
  /* multiply by sqrdlength to have criterio as a difference */
  /*rot *= (double)pow( sigma , 2./Pelt->mDim )  ;*/
  rot *= sigma ;

  /* What the fuck? Tillier! */
  div /= 2 ;
  div *= .5*div ;
  
  /* MRD is the max of divergence and curl */
  MRD = MAX( div, rot ) ;
  *PMRD = MRD ; 
  
  AvgMRD /= Pelt->mEdges ;
  *PAvgMRD = AvgMRD ;
 
 return ( 1 ) ;
}
  
/******************************************************************************

  calc_diff:
  Calculate the difference along edges of an element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pelem
  kVar: 0-mEq: the state quantity, 101:uvw, 102: t

  Changes To:
  -----------
  absMax:  maximum
  absAvg:  average
  abs:     abs val per edge.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int calc_diff ( const elem_struct *Pelem, const int kVar,
                double *PabsMax, double *PabsAvg, double ve[] ) {
  
  const elemType_struct *pElT = elemType + Pelem->elType ;
  const int *kVxEdge ;
  const vrtx_struct *pVx ;
  const double *pUn ;
  
  double v0, v1, val[MAX_VX_ELEM] ;
  int k, nDim ;

  /* Initialisation */
  *PabsMax = -TOO_MUCH ;
  *PabsAvg = 0. ;

  for ( k = 0 ; k < pElT->mVerts ; k++ ) {
    pVx = Pelem->PPvrtx[k] ;
    pUn = pVx->Punknown ;

    if ( kVar == -1 )
      /* No unknowns, use the vertex numbers. This is used for faking global
       de/refinement. */
      val[k] = pVx->number ;
    else if ( kVar == 101 ) {
      for ( val[k] = 0., nDim = 1 ; nDim <= pElT->mDim ; nDim++ )
        /* Loop over the speeds, u,v,w. Calculate the abs val of norm of the velocity. */
	val[k] += pUn[nDim] * pUn[nDim] ;
      val[k] = sqrt( val[k] ) ;
    }
    else if ( kVar == 103 )
      /* Temperature, pressure is 4th. */
      val[k] = pUn[3]/pUn[0] ;
    else if ( kVar == 104 )
      /* Temperature, pressure is 5th. */
      val[k] = pUn[4]/pUn[0] ;
    else
      /*  Abs of var between the 2 Vertex */
      val[k] = pUn[kVar] ;
  }
  
  
  /* Average of  Abs */
  for ( k = 0 ; k < pElT->mEdges ; k++ ) {
    kVxEdge = pElT->edgeOfElem[k].kVxEdge ;
    v0 = val[ kVxEdge[0] ] ;
    v1 = val[ kVxEdge[1] ] ;
    ve[k] = ABS( v0-v1 ) ;

    *PabsAvg += ve[k] ;
    *PabsMax = MAX( *PabsMax, ve[k] ) ;
  }
  *PabsAvg /= pElT->mEdges ;

  return ( 1 ) ;
}
/******************************************************************************

  calc_abs:
  Calculate the absolute value of averages along edges of an element.
  Mainly the same as diff, but order_sensor insists on calling two
  different functions.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pelem
  kVar: 0-mEq: the state quantity, 101:uvw, 102: t

  Changes To:
  -----------
  absMax:  maximum
  absAvg:  average
  abs:     abs val per edge.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int calc_abs ( const elem_struct *Pelem, const int kVar, 
               double *PabsMax, double *PabsAvg, double ve[] ) {
  
  
  const elemType_struct *pElT = elemType + Pelem->elType ;
  const int *kVxEdge ;
  const vrtx_struct *pVx ;
  const double *pUn ;
  
  double v0, v1, val[MAX_VX_ELEM] ;
  int k, nDim ;

  /* Initialisation */
  *PabsMax = -TOO_MUCH ;
  *PabsAvg = 0. ;

  for ( k = 0 ; k < pElT->mVerts ; k++ ) {
    pVx = Pelem->PPvrtx[k] ;
    pUn = pVx->Punknown ;

    if ( kVar == -1 )
      /* No unknowns, use the vertex numbers. This is used for faking global
       de/refinement. */
      val[k] = pVx->number ;
    else if ( kVar == 101 ) {
      for ( val[k] = 0., nDim = 1 ; nDim <= pElT->mDim ; nDim++ )
        /* Loop over the speeds, u,v,w. Calculate the abs val of norm of the velocity. */
	val[k] += pUn[nDim] * pUn[nDim] ;
      val[k] = sqrt( val[k] ) ;
    }
    else if ( kVar == 103 )
      /* Temperature, pressure is 4th. */
      val[k] = pUn[3]/pUn[0] ;
    else if ( kVar == 104 )
      /* Temperature, pressure is 5th. */
      val[k] = pUn[4]/pUn[0] ;
    else
      /*  Abs of var between the 2 Vertex */
      val[k] = pUn[kVar] ;
  }
  
  
  /* Average of  Abs */
  for ( k = 0 ; k < pElT->mEdges ; k++ ) {
    kVxEdge = pElT->edgeOfElem[k].kVxEdge ;
    v0 = val[ kVxEdge[0] ] ;
    v1 = val[ kVxEdge[1] ] ;
    ve[k] = .5*ABS( v0+v1 ) ;

    *PabsAvg += ve[k] ;
    *PabsMax = MAX( *PabsMax, ve[k] ) ;
  }
  *PabsAvg /= pElT->mEdges ;

  return ( 1 ) ;
}

/******************************************************************************

  number_uns_elemFromVerts
  Loop over all elements and mark all in markdEdges with the number of vertices.
  
  Last update:
  ------------
  4jun19; number elems by type for leaves on buffered grids.
  22May19; use pUns->isBuffered, 
           increment pChunk->mElemsNumbered.
  11Jun99; allow type parent.
  7Jun99; moved here from uns_drvElem.
  12Jan96; add derived elements using fixed edges on quad faces.
  4May96: conceived.
  
  Input:
  ------
  pUns
  nrType: one of root, leaf, term, parent ;
  
  Changes to:
  ----------
  pUns->mElems_w_mVerts: A counter of elements with this type.
                         0 <= elType < MAX_ELEM_TYPES : base elements.
                         MAX_ELEM_TYPES <= iPos < MAX_ELEM_TYPES+MAX_REFINED_VERTS+1:
                         derived elements with mVerts = iPos-MAX_ELEM_TYPES+4.
  pUns->mElemsNumbered: Current count of numbered elements.

*/

int number_uns_elemFromVerts_adapt ( uns_s *pUns, numberedType_e nrType ) {
  
  elem_struct *pElem ;
  const elemType_struct *pElT ;
  chunk_struct *Pchunk ;
  int mVxHg, kVxHg[MAX_ADDED_VERTS], iType, iPos, kFace, fixedDiag,
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1] ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;

 
  /* Reset all counters.*/
  pUns->mElemsNumbered = 0 ;
  for ( iType = 0 ; iType < MAX_ELEM_TYPES ; iType++ )
    pUns->mElemsOfType[iType] = 0 ;
  // 0 .. hex = base elements, hex .. MAX_DRV are derived elems.
  for ( iType = 0 ; iType < MAX_DRV_ELEM_TYPES ; iType++ ) {
    pUns->mElems_w_mVerts[iType] = 0 ;
  }

  
  elType_e elType ;
  if ( pUns->pllAdEdge && !pUns->isBuffered &&
       ( nrType == leaf || nrType == term ) ) {
    /* There is a list of adapted edges. Find elements with hanging nodes or
       fixed diagonals for the finest grid. */
    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
      Pchunk->mElemsNumbered = 0 ;
      /* Loop over all cells. */
      for ( pElem = Pchunk->Pelem+1 ;
	    pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
        
	if ( !pElem->invalid  && ( (  pElem->root && nrType == root ) ||
                                   (  pElem->leaf && nrType == leaf ) ||
                                   (  pElem->term && nrType == term ) ||
                                   ( !pElem->leaf && nrType == parent ) ) ) {
	  pElT = elemType + pElem->elType ;
	  get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, PhgVx, fixDiag, diagDir ) ;
	  if ( mVxHg )
	    /* Derived element. */
	    iPos = pElT->mVerts + mVxHg + MAX_ELEM_TYPES - 4 ;
	  else {
	    /* Loop over all faces and find fixed diagonals. */
	    for ( fixedDiag = 0, kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
	      if ( fixDiag[kFace] ) {
		fixedDiag = 1 ;
		break ; }
	    
	    if ( fixedDiag )
	      /* Derived element. */
	      iPos = pElT->mVerts + MAX_ELEM_TYPES - 4 ;
	    else
	      /* Base element. */
	      iPos = pElem->elType ;
	  }
	    
	  /* markdEdges should reflect the sensor marking. For 
             counting use mVxTot.
            pElem->markdEdges = iPos ;
	  ++Pchunk->mElemsMarked ; // is this ever used? */
          pElem->mVxTot = pElT->mVerts + mVxHg ;
          pElem->number = ++pUns->mElemsNumbered ;
          ++Pchunk->mElemsNumbered ;
          ++pUns->mElemsOfType[ pElem->elType ] ;
	  ++pUns->mElems_w_mVerts[iPos] ;
	}
        else
          pElem->number = 0 ;
    }
  }

  
  else if ( pUns->pllAdEdge && nrType == parent )
    /* There is a list of adapted edges. We want base elements for all refined
       parents, derived elements for all buffered ones. */
    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
      Pchunk->mElemsNumbered = Pchunk->mElemsMarked = 0 ;
      /* Loop over all cells. */
      for ( pElem = Pchunk->Pelem+1 ;
	    pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
        
	if ( !pElem->invalid  && pElem->PrefType ) {

          if ( pElem->PrefType->refOrBuf == buf ) {
            /* Buffered element. */
            pElT = elemType + pElem->elType ;
            get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, PhgVx, fixDiag, diagDir ) ;

            if ( mVxHg )
              /* Derived element. */
              iPos = pElT->mVerts + mVxHg + MAX_ELEM_TYPES - 4 ;
            else {
              /* Loop over all faces and find fixed diagonals. */
              for ( fixedDiag = 0, kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
                if ( fixDiag[kFace] ) {
                  fixedDiag = 1 ;
                  break ; }
              
              if ( fixedDiag )
                /* Derived element. */
                iPos = pElT->mVerts + MAX_ELEM_TYPES - 4 ;
              else
                /* Base element. */
                iPos = pElem->elType ;
            }
	    
            pElem->markdEdges = iPos ;
            ++pUns->mElems_w_mVerts[iPos] ;
          }
          else {
            /* Fully refined element. List it as base element. */
            pElem->markdEdges = pElem->elType ;
            ++pUns->mElems_w_mVerts[ pElem->elType ] ;
          }
          
          pElem->number = ++pUns->mElemsNumbered ;
          ++Pchunk->mElemsNumbered ;
          ++pUns->mElemsOfType[ pElem->elType ] ;
          ++Pchunk->mElemsMarked ;
        }
        else
          pElem->number = 0 ;
    }

  
  else if ( nrType == leaf ) {
    /* Just base elements. */
    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
      Pchunk->mElemsNumbered = Pchunk->mElemsMarked = 0 ;

    for ( elType = tri ; elType <= hex ; elType++ ) {
      for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
        /* Loop over all cells. */
        for ( pElem = Pchunk->Pelem+1 ;
              pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
          if ( pElem->elType == elType ) {
            if ( !pElem->invalid  &&  (  pElem->leaf && nrType == leaf ) ) {
              //pElem->markdEdges = pElem->elType ;
              pElem->number = ++pUns->mElemsNumbered ;
              ++pUns->mElemsOfType[ pElem->elType ] ;
              ++pUns->mElems_w_mVerts[ pElem->elType ] ;
              ++Pchunk->mElemsMarked ;
              ++Pchunk->mElemsNumbered ;
            }
            else
              pElem->number = 0 ;
          }
      }
    }
  }
  else {
    hip_err ( fatal, 0,
              "unforeseen numbering configuration in number_uns_elemFromVerts_adapt." ) ;
  }
      
  return ( 1 ) ;
}


  
/******************************************************************************

  number_uns_elem_leafNprt:
  Loop over all elements and number all leafs and all parents independently.
  Reset the counters. In case the leafs are already numbered, do not renumber.
  In this way we can reorder the leafs while writing, e.g. by type, and
  preserve that numbering.
  
  Last update:
  4May96: conceived.
  
  Input:
  ------
  pUns
  
  Changes to:
  ----------
  Pchunk->Pvrtx->nr
  
*/

void number_uns_elem_leafNprt ( uns_s *pUns, int *PmLeafElems, 
			        int *PmPrtElems, int mPrtElemsPerType[],
                                int *PmPrtElem2VertP,
			        int *PmPrtElem2ChildP,
			        int *PmPrtBndPatches, int *PmPrtBndFaces ) {
  
  elem_struct *Pelem ;
  chunk_struct *Pchunk ;
  int mElemsBeforeThisChunk, foundPrtFaces ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *PbndFc, *pBndFcBeg, *pBndFcEnd ;
  elType_e elT ;
  
  *PmLeafElems = *PmPrtElems = *PmPrtElem2VertP = *PmPrtElem2ChildP = 0 ;
  *PmPrtBndPatches = *PmPrtBndFaces = 0 ;
  for ( elT = tri ; elT < MAX_ELEM_TYPES ; elT++ )
    mPrtElemsPerType[elT] = 0 ;

  
  /* Loop over all chunks. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )  {
    Pchunk->mElemsNumbered = 0 ;

    mElemsBeforeThisChunk = *PmLeafElems + *PmPrtElems ;
    /* Loop over all cells. */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->invalid ) 
        Pelem->number = 0 ;
      else if ( Pelem->leaf && pUns->numberedType != leaf )
        /* Do not renumber the leafs if there is already leaf numbering. */
        Pelem->number = ++(*PmLeafElems) ;
      else if ( Pelem->leaf )
        ++(*PmLeafElems) ;
      else {
        /* This is a parent element. */
	Pelem->number = ++(*PmPrtElems) ;
        mPrtElemsPerType[Pelem->elType]++ ;
	*PmPrtElem2VertP += elemType[Pelem->elType].mVerts ;
	
	/* Find the number of children, the type of refinement (ref or buf).*/
        *PmPrtElem2ChildP += Pelem->PrefType->mChildren ;
      }
    Pchunk->mElemsNumbered += *PmLeafElems + *PmPrtElems - mElemsBeforeThisChunk ;
  }

  /* Loop over all boundary patches and count the patches and faces with parents. */
  Pchunk = NULL ;
  while ( loop_bndFaces ( pUns, &Pchunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
    foundPrtFaces = 0 ;
    for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ ) 
      if ( !PbndFc->Pelem->invalid && !PbndFc->Pelem->leaf ) {
	/* This is a parent. */
	foundPrtFaces = 1 ;
	(*PmPrtBndFaces)++ ;
      }
    
    if ( foundPrtFaces )
      (*PmPrtBndPatches)++ ;
  }

  pUns->numberedType = leafNprt ;
  return ;
}




/******************************************************************************

  adaptLvl_elem:
  Find the depth of adaptation level of an element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pelem
  
  Returns:
  --------
  The adaptation level.
  
*/

int adaptLvl_elem ( const elem_struct *Pelem )
{
  const elem_struct *Pprt ;
  int adaptLvl = 0 ;

  if ( Pelem->root )
    return ( 0 ) ;
  else if ( Pelem->Pparent->PrefType->refOrBuf == buf )
    /* Ignore buffer levels. */
    Pprt = Pelem->Pparent ;
  else
    /* Normal refinement. */
    Pprt = Pelem ;

  while ( ( Pprt = Pprt->Pparent ) )
    adaptLvl++ ;

  return ( adaptLvl ) ;
}


/**************************************************************************************

  adapt_reset:
  Reset adaptation markers on a grid.
  
  Last update:
  ------------
  16Dec11; rename hrbMark to boxMark.
  : conceived.
  
  Input:
  ------
 
  Changes To:
  -----------
  
  Returns:
  --------
  0 on failure, 1 on success.

*/

int adapt_reset ( uns_s *pUns ) {
  
  chunk_struct *Pchunk ;
  elem_struct *Pelem ;

  /* Reset all */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf ) {
	Pelem->markdEdges = Pelem->derefElem = 0 ;
        // Pelem->boxMark = 0
        reset_elem_mark ( Pelem, BOX_MARK ) ;
      }

  return ( 1 ) ;
}


/******************************************************************************

  adapt_uns_nr:
  Adapt a grid. Apply a sensor and call the refinement functions.
  
  Last update:
  ------------
  5jun19; implement reading ascii file.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/


int adapt_uns_hierarchical_nr ( uns_s *pUns, char *fileType, char *fileName ) {
  
  chunk_struct *Pchunk ;
  elem_struct *Pelem ;
# define MAX_NR_ELEMS 1000
  ulong_t mElAlloc = MAX_NR_ELEMS, mEl2Ref = 0, mElDeref = 0, nEl2Ref ;

  int someInt ;
  ulong_t *el2Ref = arr_malloc( "el2Ref", pUns->pFam, mElAlloc, sizeof( *el2Ref ) ) ;
  int *mrkEdg = arr_malloc( "mrkEdg", pUns->pFam, mElAlloc, sizeof( *mrkEdg ) ) ;

  FILE *Fel ;
  char line[LINE_LEN] ;
    
  if ( fileType[0] == '\0' ) {
    /* Read up to 1000 numbers from the prompt. */
    while ( !eo_buffer() ) {
      read1int ( &someInt ) ; // seems we don't have a read1ulg, but won't matter here.
      el2Ref[mEl2Ref] = someInt ;
      if ( !eo_buffer() ) read1int ( mrkEdg+mEl2Ref ) ;
      else mrkEdg[mEl2Ref] = elemType[hex].allEdges ;
      
      if ( ++mEl2Ref >= MAX_NR_ELEMS ) {
        sprintf ( hip_msg, 
                  "too many ( > %d ) elems to nr-refine. Remaining ignored.",
                  MAX_NR_ELEMS ) ;
        hip_err ( warning, 1, hip_msg ) ;
        break ; }
    }
  }
  else if ( !strncmp ( fileType, "as", 2 ) ) {
    // Ascii list.
    if ( !(Fel = fopen ( prepend_path ( fileName ), "r" ) ) )
      hip_err ( warning, 1, "could not open ascii file with elements to adapt, ignoring." ) ;
    else {
      while ( !feof( Fel ) ) {
        fscanf ( Fel, "%[^\n]\n", line ) ;
        if ( !( line[0] == '!' || line[0] == '%' || line[0] == '#' ) ) {

          if ( mEl2Ref+1 > mElAlloc ) {
            mElAlloc *= REALLOC_FACTOR ;
            el2Ref = arr_realloc( "el2Ref", pUns->pFam, el2Ref, mElAlloc, sizeof( *el2Ref ) ) ;
            mrkEdg = arr_realloc( "mrkEdg", pUns->pFam, mrkEdg, mElAlloc, sizeof( *mrkEdg ) ) ;
          }
            
          if ( sscanf ( line, "%"FMT_ULG"", el2Ref+mEl2Ref ) ) {
            if ( 2 != sscanf ( line, "%"FMT_ULG" %d", el2Ref+mEl2Ref, mrkEdg+mEl2Ref ) )
              mrkEdg[mEl2Ref] = elemType[hex].allEdges ;
            else if ( mrkEdg[mEl2Ref] < 0 )
              mElDeref++ ;
            mEl2Ref++ ;
          }
        }
      }
    }
    sprintf ( hip_msg, "read %"FMT_ULG" elements to refine, %"FMT_ULG" to derefine from file.",
              mEl2Ref-mElDeref, mElDeref ) ;
    hip_err ( info, 1, hip_msg ) ;
  }
  else {
    sprintf ( hip_msg, "this filetype: %s not supported in adapt_nr.", fileType ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  
  /* Reset all elemcent markers for de/refinement. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      Pelem->markdEdges = Pelem->derefElem = 0 ;
  
  /* Mark the listed elems. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf )
	/* Loop over all listed elements and try to find a match. */
	for ( nEl2Ref = 0 ; nEl2Ref < mEl2Ref ; nEl2Ref++ )
	  if ( Pelem->number == el2Ref[nEl2Ref] ) {
            /* This is the one. */
	    if ( mrkEdg[nEl2Ref] == -1 ) {
	      /* Derefine. */
	      Pelem->derefElem = 1 ;
	      Pelem->markdEdges = 0 ;
            }
            else if ( Pelem->Pparent &&
                      Pelem->Pparent->PrefType->refOrBuf == buf ) {
              /* This is a buffer element. Apply full refinement to the parent. */
              Pelem->Pparent->markdEdges = elemType[ Pelem->Pparent->elType ].allEdges ;
            }
	    else if ( mrkEdg[nEl2Ref] < -1 ) {
	      /* Refine all. */
	      Pelem->derefElem = 0 ;
	      Pelem->markdEdges = elemType[ Pelem->elType ].allEdges ;
            }
	    else {
	      /* Refine the given edges. */
	      Pelem->derefElem = 0 ;
	      Pelem->markdEdges = mrkEdg[nEl2Ref] &
		                  elemType[ Pelem->elType ].allEdges ; }
	  }

  arr_free ( el2Ref ) ;
  arr_free ( mrkEdg ) ;

  return ( adapt_uns_hierarchical ( pUns, 1 ) ) ;
}
/******************************************************************************

  write_adEdge:
  Dump the list of adapted edges to file. Needed by write_avbp4.
  
  Last update:
  ------------
  14Sep98; write an empty adEdge file, if there are no edges. 
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_adEdge ( const uns_s *pUns, const char *adFile ) {
  
  ulong_t mEdges ;
  int mEgUsed, buffer[4], nEg ;
  const llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  vrtx_struct *pVx0, *pVx1, *pVxMid ;
  const adEdge_s *pAe, *pAdEdge = pUns->pAdEdge ;
  FILE *FadEdge ;

  if ( !( FadEdge = fopen ( adFile, "w" ) ) ) {
    printf ( " FATAL: could not open %s in write_adEdge.\n", adFile ) ;
    return ( 0 ) ; }
  
  /* How many are there. */
  if ( pllAdEdge )
    mEgUsed = get_number_of_edges ( pllAdEdge, &mEdges ) ;
  else
    mEgUsed = 0 ;

  if ( verbosity > 2 )
    printf ( "      special edges (%d) to %s\n", mEgUsed, adFile ) ;

  /* Number of edges, size per edge. */
  buffer[0] = 2*sizeof( int ) ;
  buffer[1] = mEgUsed ;
  buffer[2] = 3*sizeof( int ) ;
  buffer[3] = buffer[0] ;
  FWRITE ( buffer, sizeof ( int ), 4, FadEdge ) ;
  
  if ( !pllAdEdge ) {
    /* No edges to write. */
    fclose ( FadEdge ) ;
    return ( 0 ) ;
  }


  
  /* List the three vertices for each edge. */
  buffer[0] = 3*mEgUsed*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, FadEdge ) ;

  for ( nEg = 1 ; nEg <= mEdges ; nEg++ )
    if ( show_edge ( pllAdEdge, nEg, &pVx0, &pVx1 ) ) {
      /* Active edge. */
      buffer[0] = pVx0->number ;
      buffer[1] = pVx1->number ;

      /* Mid vertex. */
      pAe = pAdEdge + nEg ;
      if ( pAe->cpVxMid.nr ) {
        /* There is a mid vertex. */
        pVxMid = de_cptVx ( pUns, pAe->cpVxMid ) ;
        buffer[2] = pVxMid->number ;
      }
      else
        /* Just an edge. */
        buffer[2] = 0 ;

      FWRITE ( buffer, sizeof( int ), 3, FadEdge ) ;
    }

  buffer[0] = 3*mEgUsed*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, FadEdge ) ;

  fclose ( FadEdge ) ;
  return ( 1 ) ;
}

/******************************************************************************

  read_adEdge:
  Dump the list of adapted edges to file. Needed by read_avbp4.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_adEdge ( uns_s *pUns, FILE *adFile ) {
  
  int mEdges, buffer[4], nEg, dir, newEg ;
  llEdge_s *pllAdEdge, **ppllAdEdge ;
  const vrtx_struct *pVx0, *pVx1 ;
  const vrtx_struct *pVxMid, *pVrtx ;
  adEdge_s *pAdEdge, **ppAdEdge ;

  if ( !pUns )
    /* No grid. */
    return ( 0 ) ;
  pVrtx = pUns->pRootChunk->Pvrtx ;

  /* How many are there. */
  FREAD ( buffer, sizeof ( int ), 4, adFile ) ;
  mEdges = buffer[1] ;
  if ( buffer[2] != 3*sizeof( int ) ) {
    printf ( " FATAL: Need %d bytes per edge, found %d in read_adEdge.\n",
             (int)(3*sizeof( int )), buffer[2] ) ;
    return ( 0 ) ; }
  fseek ( adFile, buffer[0]+2*sizeof( int ), SEEK_SET ) ;

  if ( !mEdges ) {
    /* Empty file. */
    pUns->pAdEdge = NULL ;
    return ( 1 ) ;
  }

  /* Make a list of adapted edges. */
  ppAdEdge = ( adEdge_s ** ) &( pUns->pAdEdge ) ;
  if ( !( pllAdEdge = make_llEdge ( pUns, CP_NULL, mEdges, sizeof ( adEdge_s ), NULL,
                                    ( void ** ) ppAdEdge ) ) ) {
    printf ( " FATAL: could not make a list of adapted edges in read_adEdge.\n" ) ;
    return ( 0 ) ; }
  ppllAdEdge  = &(pUns->pllAdEdge) ;
  *ppllAdEdge = pllAdEdge ;
  pAdEdge = *ppAdEdge ;

  /* Recordlength for all edges. */
  FREAD ( buffer, sizeof( int ), 1, adFile ) ;
  if ( buffer[0] != 3*mEdges*sizeof( int ) ) {
    printf ( " FATAL: expected %d, found %d bytes for all edges in read_adEdge.\n",
             (int)(3*mEdges*sizeof( int )), buffer[0] ) ;
    return ( 0 ) ; }
  
  for ( nEg = 1 ; nEg <= mEdges ; nEg++ ) {
    FREAD ( buffer, sizeof( int ), 3, adFile ) ;
    
    pVx0 = pVrtx+buffer[0] ;
    pVx1 = pVrtx+buffer[1] ;
    if ( !add_edge_vrtx( pllAdEdge, ( void ** ) ppAdEdge, &pVx0, &pVx1,
                         &dir, &newEg ) ) {
      printf ( " FATAL: could not add edge in read_adEdge.\n" ) ;
      return ( 0 ) ; }

    if ( buffer[2] ) {
      /* There is a mid-vertex. */
      pVxMid = pVrtx+buffer[2] ;
      pAdEdge[nEg].cpVxMid = pVxMid->vxCpt ;
    }
    else
      pAdEdge[nEg].cpVxMid = CP_NULL ;
  }

  return ( 1 ) ;
}


/******************************************************************************
 Debugging utilities.
*/

#ifdef DEBUG
void printelal ( const elem_struct *Pelem ) {
 
  int kVx, nRefType ;
  elem_struct **PPch ;

  if ( !Pelem ) {
    printf ( " No such elem.\n" ) ;
    return ; }
  
  printf ( " el: %d, %s, rtli: %d%d%d%d, vx:",
	   Pelem->number, elemType[Pelem->elType].name,
	   Pelem->root, Pelem->term, Pelem->leaf, Pelem->invalid ) ;
  if ( Pelem->PPvrtx )
    for ( kVx = 0 ; kVx < elemType[Pelem->elType].mVerts ; kVx++ )
      if ( Pelem->PPvrtx[kVx] )
	printf ( " %d", Pelem->PPvrtx[kVx]->number ) ;
      else
	printf ( " inv." ) ;
  else
    printf ( " inv PPvrtx." ) ;

  if ( Pelem->PrefType ) {
    if ( Pelem->PrefType->refOrBuf == buf )
        printf ( ", buf" ) ;
    else {
      nRefType = Pelem->PrefType - elemType[Pelem->elType].PrefType ;
      if ( nRefType < 0 || nRefType >= elemType[Pelem->elType].mRefTypes )
	printf ( ", inv ref" ) ;
      else
	printf ( ", ref %d", Pelem->PrefType->nr ) ;
    }
  }
  else
    printf ( ", noRef" ) ;

  if ( Pelem->Pparent )
    printf ( ", prt %d", Pelem->Pparent->number ) ;
  else
    printf ( ", noPrt" ) ;
  
  if ( Pelem->PPchild ) {
    printf ( ", ch:" ) ;
    for ( PPch = Pelem->PPchild ;
	    *PPch && (*PPch)->Pparent && (*PPch)->Pparent == Pelem ; PPch++ )
      printf ( " %d", (*PPch)->number ) ;
  }
  else
    printf ( ", noChild" ) ;

  printf ( "\n" ) ;
}
#endif

