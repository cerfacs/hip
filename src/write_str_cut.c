/*
  write_mb_cut.c:
  Write a cut in a multi-block mesh.
   
  Last update:
  
  Contains:
  ---------
  write_mb_pts:
  mb_next_open_cell:
*/

#include "cpre.h"
#include "cpre_mb.h"
#include "proto.h"
#include "proto_mb.h"

#define BOUND_FACE 1
#define CUT_FACE 0

static int str_next_open_cell ( const block_struct *Pbl, int *PnCell,
				int ijk[], int dir[],
				int *PfaceStatus, int *Pchange, int nCellFirst );

/****************************************************************

  write_str_pts:
  Write a cut in a multi-block mesh to pts. Multi-block refers
  to the data-structure only in this context. This function can
  only treat single blocks. ;-(.  Loop over all boundaries
  of the grid. If the boundary surface is marked, list its present
  segments. If it is interrupted, open a new segement and collect
  all cut faces until a present boundary is hit. continue with
  that one as the next segment. 2D only.

  Input:
  ------
 
  Changes to:
  -----------


  Returns:
  --------
  0,1 on success, failure.
  
*/

int write_str_pts ( const mb_struct *Pmb, FILE *FptsOut )
{
  block_struct *Pbl ;
  int ijk[MAX_DIM], nCell, nCellFirst, mCuts, mBounds, change, found,
    nll, nVx1, nVx2, dir[MAX_DIM], dirFc[MAX_DIM], faceStatus, staticDim, writeBnd = 0 ;
  subFace_struct *Psf, *PsfFound ;
  const int mDim = 2 ;

  nCellFirst = mCuts = mBounds = change = 0 ; 
  Psf = PsfFound = NULL ;
  
  if ( Pmb->mDim != 2 ) {
    printf ( " FATAL: write_str_pts does only 2D.\n" ) ;
    return ( 0 ) ; }
  else if ( Pmb->mBlocks > 1 ) {
    printf ( " FATAL: write_str_pts does only single block.\n" ) ;
    return ( 0 ) ; }

  /* Find the cell with the lowest lexicographic index, ie. the
     first marked cell in the list. This implies that the -i and -j
     faces of this cell are exposed by either a cut or a boundary
     face. */
  for ( found = 0, Pbl = Pmb->PblockS+1 ;
	Pbl <= Pmb->PblockS+Pmb->mBlocks && !found ; Pbl++ )
    for ( nCell = 1 ; nCell <= Pbl->mElemsBlock ; nCell++ )
      if ( !Pbl->PelemMark[nCell] ) {
	found = 1 ;
	get_ijk_nElem ( Pbl->mVert, Pbl->mElemsBlock, nCell, mDim, ijk ) ;
	break ; }
  if ( !found ) {
    printf ( " FATAL: there are no cut faces to list in write_str_pts.\n" ) ;
    return ( 0 ) ; }

  /* Initialize a face orientation. Start with -j. */
  dir[0] = 0 ;
  dir[1] = -1 ;
  if ( ijk[1] == 1 )
    faceStatus = BOUND_FACE ;
  else
    faceStatus = CUT_FACE ;

  /* Scan the faces one after the other. Start a new boundary segment
     whenever the faceStatus changes. */
  mCuts = mBounds = change = 0 ;
  while ( str_next_open_cell ( Pbl, &nCell, ijk, dir, &faceStatus, &change, nCellFirst ) )
  { if ( !change && faceStatus == BOUND_FACE )
    { /* Change lights on from str_next_open_cell if there is a turn in
	 direction. Check for different segments with the same direction. */

      /* Find the static dimension and then the covering bc. */
      for ( staticDim = 0 ; staticDim <= mDim ; staticDim++ )
      { if ( dirFc[staticDim] )
	  break ;
      }
      find_mb_subFc_cell ( Pbl, ijk, mDim, staticDim, dirFc[staticDim], &PsfFound ) ;
      if ( Psf && Psf != PsfFound )
	change = 1 ;
    }

    if ( change )
    { /* There is a switch to a new boundary segment. */
      if ( !nCellFirst )
      { /* Restart the file. This is the first corner. */
	rewind ( FptsOut ) ;
	nCellFirst = nCell ;
      }

      if ( faceStatus == BOUND_FACE )
      { /* The boundary face sought lies pi/2 to dir. */
	dirFc[0] = dir[1] ;
	dirFc[1] = -dir[0] ;

	/* Find the static dimension and then the covering bc. */
	for ( staticDim = 0 ; staticDim <= mDim ; staticDim++ )
	{ if ( dirFc[staticDim] )
	    break ;
	}
	find_mb_subFc_cell ( Pbl, ijk, mDim, staticDim, dirFc[staticDim], &Psf ) ;
	mBounds++ ;
	fprintf ( FptsOut, "%% %s\n", Psf->Pbc->text ) ;
	if ( Psf->Pbc->mark )
	  /* This face is marked, write it to file. */
	  writeBnd = 1 ;
	else
	  writeBnd = 0 ;
      }
      else
      { /* Internal cut. */
	fprintf ( FptsOut, "%% cut nr. %d\n", ++mCuts ) ;
	writeBnd = 1 ;
      }
      
      if ( writeBnd )
      { /* Write only cut faces or marked surfaces to the file. */
	fprintf ( FptsOut, "NEWBND\n" ) ;
	fprintf ( FptsOut, "NAMEBN\n %d\n", mBounds+mCuts ) ;
	fprintf ( FptsOut, "NFRSBN\n %d\n", mBounds+mCuts-1 ) ;
	fprintf ( FptsOut, "NLSTBN\n %d\n", mBounds+mCuts+1 ) ;
	fprintf ( FptsOut, "ITYPBN\n -1\n" ) ;
	fprintf ( FptsOut, "BNDEXY\n" ) ;
	
	/* List both vertices of this first face. */
	nll = get_nVert_ijk ( mDim, ijk, Pbl->mVert ) ;
	if ( dir[1] == -1 )
	{ /* dir points -j, thus -i face. */
	  nVx1 = nll + Pbl->mVert[0] ;
	  nVx2 = nll ;
	}
	else if ( dir[1] == 1 )
	{ /* +i face. */
	  nVx1 = nll + 1 ;
	  nVx2 = nVx1 + Pbl->mVert[0] ;
	}
	else if ( dir[0] == 1 )
	{ /* -j face. */
	  nVx1 = nll ;
	  nVx2 = nVx1 + 1 ;
	}
	else if ( dir[0] == -1 )
	{ /* +j face. */
	  nVx2 = nll + Pbl->mVert[0] ; 
	  nVx1 = nVx2 + 1 ;
	}
	
	fprintf ( FptsOut, "%17.9e %17.9e\n",
		  Pbl->Pcoor[ 2*nVx1 ], Pbl->Pcoor[ 2*nVx1+1 ] ) ;
	fprintf ( FptsOut, "%17.9e %17.9e\n",
		  Pbl->Pcoor[ 2*nVx2 ], Pbl->Pcoor[ 2*nVx2+1 ] ) ;
      }
    }
    
    else if ( writeBnd ) 
    { /* No change. continue writing the higher vertex coordinates. */
      nll = get_nVert_ijk ( mDim, ijk, Pbl->mVert ) ;
      if ( dir[1] == -1 )
	/* -i. */
	nVx2 = nll ;
      else if ( dir[1] == 1 )
	/* +i */
	nVx2 = nll + Pbl->mVert[0] + 1 ; 
      else if ( dir[0] == 1 )
	/* -j */
	nVx2 = nll + 1 ;
      else if ( dir[0] == -1 )
	/* +j */
	nVx2 = nll + Pbl->mVert[0] ;
      fprintf ( FptsOut, "%17.7e %17.7e\n",
		Pbl->Pcoor[ 2*nVx2 ], Pbl->Pcoor[ 2*nVx2+1 ] ) ;
    }
  }
  return ( 1 ) ;
}

/******************************************************************************

  str_next_open_cell:
  For a given face dir of a cell nCell, ijk, find the cell that is attached
  to the next open face, either a boundary face or a cut face. 2D only.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int str_next_open_cell ( const block_struct *Pbl, int *PnCell,
			       int ijk[], int dir[],
			       int *PfaceStatus, int *Pchange, int nCellFirst )
{
  static int ijkNgh[MAX_DIM], ngh, ijkNghM90[MAX_DIM], nghM90, sDir ;
  static const int mDim = 2 ;

  if ( *PnCell == nCellFirst && dir[1] == -1 )
    /* This is it. Since the search was started with the lexicographically
       lowest cell, nCellFirst, finding its lowerleft corner terminates the
       loop. */
    return ( 0 ) ;

  if ( *PfaceStatus == BOUND_FACE )
  { /* Find out whether there is an element in direction dir. */
    ijkNgh[0] = ijk[0] + dir[0] ;
    ijkNgh[1] = ijk[1] + dir[1] ;
    if ( ( ngh = check_nElem_ijk ( mDim, ijkNgh, Pbl->mVert ) ) )
      if ( Pbl->PelemMark[ngh] )
      { /* We have found a marked cell in the boundary direction. This is the one. */
	*Pchange = 0 ;
	*PnCell = ngh ;
	ijk[0] = ijkNgh[0] ;
	ijk[1] = ijkNgh[1] ;
	return ( 1 ) ;
      }
      else
      { /* The next cell is not marked. Rotate dir by pi/2 and Cut.
	   Same cell. */
	*Pchange = 1 ;
	*PfaceStatus = CUT_FACE ;
	sDir = dir[0] ;
	dir[0] = -dir[1] ;
	dir[1] = sDir ;
	return ( 1 ) ;
      }
    else
    { /* There is no cell in dir. New boundary. And rotate by pi/2.
         Same cell. */
      *Pchange = 1 ;
      sDir = dir[0] ;
      dir[0] = -dir[1] ;
      dir[1] = sDir ;
      return ( 1 ) ;
    }
  }
  
  else    
  { /* This is a cut. Consider the cut turning 90 deg clockwise, ie.
       the cell in 1 dir and 1 dir-pi/2. ( E.g. for cell 1,1 and dir=0,1
       the neighbor 2,2. Note that, staying in the example notation, we
       know that cell 2,1 is unmarked since we found a cut face there.
       Thus if cell 2,2 is marked the next cut face is between 2,1 and 2,2.*/ 
    ijkNghM90[0] = ijk[0] + dir[0] + dir[1] ;
    ijkNghM90[1] = ijk[1] + dir[1] - dir[0] ;
    if ( ( nghM90 = check_nElem_ijk ( mDim, ijkNghM90, Pbl->mVert ) ) )
      /* There is a neighbor in this direction.*/
      if ( Pbl->PelemMark[nghM90] )
      { /* This is the next cut face. Rotate -pi/2. */
	*Pchange = 0 ;
	sDir = dir[0] ;
	dir[0] = dir[1] ;
	dir[1] = -sDir ;
	*PnCell = nghM90 ;
	ijk[0] = ijkNghM90[0] ;
	ijk[1] = ijkNghM90[1] ;
	return ( 1 ) ;
      }

    /* Find out whether there is an element in direction dir. */
    ijkNgh[0] = ijk[0] + dir[0] ;
    ijkNgh[1] = ijk[1] + dir[1] ;
    if ( ( ngh = check_nElem_ijk ( mDim, ijkNgh, Pbl->mVert ) ) )
      if ( Pbl->PelemMark[ngh] )
      { /* There is a marked neighbor in dir. Next Cell. */
	*Pchange = 0 ;
	*PnCell = ngh ;
	ijk[0] = ijkNgh[0] ;
	ijk[1] = ijkNgh[1] ;
	return ( 1 ) ;
      }
      else
      { /* The next face is a cut along the face of nCell. Rotate pi/2. */
	*Pchange = 0 ;
	sDir = dir[0] ;
	dir[0] = -dir[1] ;
	dir[1] = sDir ;
	return ( 1 ) ;
      }
    else
    { /* There is no neighbor in dir. Boundary face. Rotate pi/2. */
      sDir = dir[0] ;
      dir[0] = -dir[1] ;
      dir[1] = sDir ;
      *Pchange = 1 ;
      *PfaceStatus = BOUND_FACE ;
      return ( 1 ) ;
    }

  }
}

