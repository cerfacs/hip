/*
   uns_vxEnt.c
   List of vertex related entities with a fixed maximum length, e.g 4 noded faces.
   Organized as an initial entry for the lowest numbered vertex of the ent with
   a linked list of all other ents with that lowest vertex.

   Contains functions that manage the storage and debugging utilites.
 
   Last update:
   ------------
   30Jun14; minor correction doc.
   8Apr13; change calls to arr_[re]alloc to use the dereferenced pointer for size.
   6Apr13; promote possibly large int to type ulong_t.
   1Aug98; derived from ent_manage.c.
   
   Contains:
   ---------
   make_llEnt
   free_llEnt
   
   get_new_ent
   del_ent
   get_ent_vrtx
   add_ent_vrtx

   get_sizeof_llEnt
   show_ent

   listEnt
   listEntVx
   printEnt

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;
extern const cpt_s CP_MAX ;

/* Define a maximum number of vertices per entity. */
#define MAX_VX_ENT 27


struct _llVxEnt_s {

  uns_s *pUns ;         /* An entity list is valid for one grid only. */

  int mChunks ;         /* Size of pn1stEgChk. */
  ulong_t *pmVerts ;        /* How many entry pointers with each chunk. */
  ulong_t **ppn1stEntChk ;   /* An entry ent for each vertex for each chunk. */

  ulong_t mEnts ;           /* Number of elements in pEnt. */
  ulong_t nRootFreeEnt ;    /* Number of the first unused ent. */
  ulong_t mEntsUsed ;       /* Try to keep track how many are used. */

  int mVxEnt ;          /* How many vertex entries per entity in cpVx. */
  cpt_s *pcpVx ;        /* mEnts*mVxEnt vertices. */

  ulong_t *pnNxtEnt ;       /* One link per ent. */

  char **ppEntData ;    /* A field of data with each ent. Store it as a char to
                           enable pointer arithmetic. This pointer will point to a
                           fixed public location where the address of the field is
                           stored. */
  ulong_t dataSize ;     /* Length of the datafield for each ent. */
} ;


/******************************************************************************

  make_llEnt:
  Create a list of Ents. If pllEnt is given, the list is reallocated using
  the global REALLOC_FACTOR. If size is given, a list of that size is allocated.
  Else, pRootChunk is queried for the number of nodes and 2 egdes per node
  as for a regular quad grid are allocated.
  
  Last update:
  ------------
  1Oct16; fix bug with pointing to next open data slot after realloc.
  22Dec09; use ulong_t for large alloc.
  16Jul98; modified llVxEnt_s.
  : conceived.
  
  Input:
  ------
  pllEnt:     The linked list of entities.
  pUns:       The grid that the list is for
  ppEntData:  The location of the pointer to allocated public data fields for each ent.
  cptVxMax:   If given, sizing of the entry fields contains this cpt_s.
  mEnts:      If specified, that many Ents are allocated.
  dataSize:   A datafield of dataSize is also allocated.

  Changes To:
  -----------
  pllEnt:    The allocated memory in a struct.
  ppEntData: The allocated public data fields for each ent. Note that only reallocation
             takes place, a previously realloced ppEntData is not reparsed to the new
             field.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

llVxEnt_s *make_llEnt ( llVxEnt_s *pllEnt, uns_s *pUns, char **ppEntData,
                        cpt_s cptVxMax, ulong_t mEnts, int mVxEnt,
                        ulong_t dataSize ) {
  
  ulong_t rLen ;
  ulong_t nEnt, mOldEnts ;
  int nChk, mChk ;
  ulong_t *pnVx, mVx, *pn1stEg ;
  char *pData ;

  /*****
    Make a new list.
    *****/
  if ( !pllEnt ) {
    /* Malloc a linked list of Ents and reset it. */
    pllEnt = arr_malloc ( "pllEnt in make_llEnt", pUns->pFam, 1, sizeof( *pllEnt ) ) ;
    
    pllEnt->pUns = pUns ;
    pllEnt->mChunks = 0 ;
    pllEnt->pmVerts = NULL ;
    pllEnt->ppn1stEntChk = NULL ;
    pllEnt->mEnts = pllEnt->mEntsUsed = pllEnt->mVxEnt = 0 ;
    pllEnt->pcpVx = NULL ;
    pllEnt->pnNxtEnt = NULL ;
    pllEnt->dataSize = 0 ;
    *ppEntData = NULL ;
    mOldEnts = 0 ;
  }
  else {
    /* Realloc. */
    pUns = pllEnt->pUns ;
    ppEntData = pllEnt->ppEntData ;
    mOldEnts = pllEnt->mEnts ;
  }

  
  
  /*****
    Add more space to the entry list.
    *****/
  if ( pllEnt->mChunks < ( mChk = pUns->mChunks ) ) {
    /* Realloc the list of entries. */
    pllEnt->pmVerts = arr_realloc ( "pmVerts in make_llEnt", pUns->pFam, pllEnt->pmVerts,
                                    mChk, sizeof ( *pllEnt->pmVerts ) ) ;
    pllEnt->ppn1stEntChk = arr_realloc ( "ppn1stEntChk in make_llEnt", pUns->pFam,
                                         pllEnt->ppn1stEntChk, mChk, 
                                         sizeof ( *pllEnt->ppn1stEntChk ) ) ;

    /* Reset counters for the new chunks. */
    for ( nChk = pllEnt->mChunks ; nChk < mChk ; nChk++ ) {
      pllEnt->pmVerts[nChk] = 0 ;
      pllEnt->ppn1stEntChk[nChk] = NULL ;
    }
    pllEnt->mChunks = pUns->mChunks ;
  }
    
  /* Realloc entries for each chunk. */
  for ( nChk = 0 ; nChk < mChk ; nChk++ ) {
    mVx = pUns->ppChunk[nChk]->mVerts ;
    /* Make sure that cptVxMax is contained. */
    if ( cptVxMax.nr && cptVxMax.nCh == nChk )
      mVx = MAX( mVx, cptVxMax.nr ) ;
    if ( pllEnt->pmVerts[nChk] < mVx ) {
      if ( !( pn1stEg = pllEnt->ppn1stEntChk[nChk] =
              arr_realloc ( "pllEnt->ppn1stEntChk[nChk] in make_llEnt", pUns->pFam,
                            pllEnt->ppn1stEntChk[nChk], mVx+1, 
                            sizeof( *(pllEnt->ppn1stEntChk[nChk]) ) ) ) ) {
        printf ( " FATAL: could not allocate an entry list in make_llEnt.\n" ) ;
        return ( 0 ) ; }
      
      /* Reset the list of newly allocated entry Ents. */
      for ( pnVx = pn1stEg+pllEnt->pmVerts[nChk]+1 ; pnVx <= pn1stEg+mVx ; pnVx++ )
        *pnVx = 0 ;

      pllEnt->pmVerts[nChk] = mVx ;
    }
  }

  
  /******
    Get the size of the list of Ents.
    *****/
  if ( !mEnts && !mOldEnts )
    /* Default size. */
    mEnts = 100 ;

  if ( mEnts ) {
    /* A specific size is given. Use it. */
    if ( pllEnt && mEnts < pllEnt->mEnts && verbosity > 5 ) {
      sprintf ( hip_msg, "shrinking the list of Ents from %"FMT_ULG" to %"FMT_ULG" in"
                " make_llEnt.\n",  pllEnt->mEnts, mEnts ) ;
      hip_err ( info,3,hip_msg ) ;
    }
    
    /* Take the sizes of entry as fixed after initial alloc.  */
    if ( !pllEnt->mVxEnt ) {
      if ( mVxEnt < 1 ) {
        sprintf ( hip_msg, "cannot make a vxEnt list with %d mVxEnt.\n", mVxEnt ) ;
        hip_err ( fatal, 0, hip_msg ) ; }
      if ( mVxEnt > MAX_VX_ENT ) {
        sprintf ( hip_msg, " SORRY: uns_vxEnt is only compiled for %d vertices per entity.\n",
                 MAX_VX_ENT ) ;
        hip_err ( fatal, 0, hip_msg ) ; }
      pllEnt->mVxEnt = mVxEnt ;
    }
    mVxEnt  = pllEnt->mVxEnt ;
  }
  else if ( mOldEnts ) {
    /* Resize. */
    mEnts = mOldEnts * REALLOC_FACTOR + 1 ;
    if ( verbosity > 5 ) {
      sprintf ( hip_msg, "realloc ent list in make_llEnt to %"FMT_ULG" Ents.\n", mEnts ) ;
      hip_err ( info, 5, hip_msg ) ;
    }
    mVxEnt  = pllEnt->mVxEnt ;
  }
  
  /*****
    Add more space to the ent list.
    *****/
  if ( mOldEnts < mEnts ) {
    /* Note that pEnt[] starts with 1, the zero being used as nil-flag. */
    rLen = mEnts+1 ;
    rLen *= mVxEnt ;
    pllEnt->pcpVx = arr_realloc ( "pcpVx in make_llEnt", pUns->pFam, pllEnt->pcpVx,
                                  rLen, sizeof( *pllEnt->pcpVx ) ) ;
    pllEnt->pnNxtEnt = arr_realloc ( "pnNxtEnt in make_llEnt", pUns->pFam,  pllEnt->pnNxtEnt,
                                     mEnts+1, sizeof( *pllEnt->pnNxtEnt ) ) ;
    if ( !pllEnt->pcpVx || !pllEnt->pnNxtEnt ) {
      sprintf ( hip_msg, "failed to realloc llEnt list to %"FMT_ULG" in make_llEnt.\n",
               mEnts ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    
    /* The next free slot. */
    pllEnt->nRootFreeEnt = MAX(1,mOldEnts ) ; // NO! we count from one. mOldEnts+1 ;
    /* Link a list of free slots in the first entry. */
    for ( nEnt = mOldEnts+1 ; nEnt <= mEnts ; nEnt++ ) {
      pllEnt->pnNxtEnt[nEnt] = nEnt+1 ;
      pllEnt->pcpVx[nEnt*mVxEnt] = CP_NULL ;
    }
    /* New size. */
    pllEnt->mEnts = mEnts ;
  }


  
  /*****
    Change the size of the ent data.
    *****/
  if ( pllEnt->dataSize*mOldEnts != dataSize*mEnts ) {

    if ( dataSize ) {
      if ( !( *ppEntData = arr_realloc ( "ppEntData in make_llEnt", pUns->pFam, *ppEntData,
                                         mEnts+1, dataSize ) ) ) {
        sprintf ( hip_msg, "failed to realloc data list to %"FMT_ULG" in make_llEnt.\n",
                  mEnts ) ;
        hip_err ( fatal, 0, hip_msg ) ; 
      }
    }
    else if ( pllEnt->dataSize && !dataSize ) {
      /* Free the previously allocated data-field. */
      arr_free ( *ppEntData ) ;
      *ppEntData = NULL ;
    }
    
    /* Remember ppEntData in pllEnt. */
    pllEnt->ppEntData = ( char ** ) ppEntData ;
    
    /* Reset the newly allocated data fields. */
    for ( pData = ( char *)*ppEntData + pllEnt->dataSize*(mOldEnts+1) ;
          pData < ( char *)*ppEntData + dataSize*(mEnts+1) ; pData++ )
      *pData = ( char ) 0 ;
    
    /* New size. */
    pllEnt->dataSize = dataSize ;
  }

  return ( pllEnt ) ;
}

/******************************************************************************

  free_llEnt:
  Remove the complete list.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEnt.


  Changes To:
  -----------
  pllEnt
  
*/

void free_llEnt ( llVxEnt_s ** ppllEnt ) {
  
  llVxEnt_s *pllE ;
  int nChk ;

  if ( !ppllEnt )
    /* No such list of Ents. */
    return ;
  else if ( !( pllE = *ppllEnt ) )
    /* Empty list. */
    return ;

  arr_free ( pllE->pmVerts ) ;
  arr_free ( pllE->pcpVx ) ;
  arr_free ( pllE->pnNxtEnt ) ;

  if (pllE->ppEntData) {
    arr_free ( *(pllE->ppEntData) ) ;
    *(pllE->ppEntData) = NULL ;
  }

  for ( nChk = 0 ; nChk < pllE->mChunks ; nChk++ )
    arr_free ( pllE->ppn1stEntChk[nChk] ) ;
  arr_free ( pllE->ppn1stEntChk ) ;

  arr_free ( *ppllEnt ) ;
  *ppllEnt = NULL ;
  
  return ;
}
/******************************************************************************

  get_new_ent:
  Get the next free slot for an adaptive ent.
  
  Last update:
  ------------
  1Oct16; use hip_err rather than printf.
  : conceived.
  
  Input:
  ------
  pllEnt: the linked list.

  Changes To:
  -----------
  pllEnt:
  
  Returns:
  --------
  0 on failure, the vacant entity slot on success.
  
*/

static int get_new_ent ( llVxEnt_s *pllEnt )
{
  int nFreeEnt, mNewEnts ;
  
  /* Check if there is space in the list of Ents. */
  if ( pllEnt->nRootFreeEnt >= pllEnt->mEnts ) {
    mNewEnts = REALLOC_FACTOR*pllEnt->mEnts + 1 ;
    if ( !make_llEnt ( pllEnt, NULL, NULL, CP_NULL, mNewEnts, 0, pllEnt->dataSize ) ) {
      hip_err ( fatal, 0, "failed to realloc the list of Ents in get_new_ent.\n" ) ;
      return ( 0 ) ;
    }
  }

  /* Fill the next ent. */
  nFreeEnt = pllEnt->nRootFreeEnt ;
  pllEnt->nRootFreeEnt = pllEnt->pnNxtEnt[nFreeEnt] ;
  pllEnt->mEntsUsed++ ;

  return ( nFreeEnt ) ;
}


/******************************************************************************

  del_ent:
  Delete an ent from the list.
  
  Last update:
  ------------
  19Sep16; make size comparison >=, rather than >. 
           adjust mEntsUsed to be positive, as needed by ulong_t
  : conceived.
  
  Input:
  ------
  pllEnt:  The linked list of entities.
  nDelEnt: The entity to be removed from the list.

  Changes To:
  -----------
  pllEnt:
  
*/

void del_ent ( llVxEnt_s *pllEnt, ulong_t nDelEnt ) {
  
  cpt_s *pcpVxDel, cpVx, *pcpVxPrv ;
  ulong_t *pNtry, nPrvEnt, *pnNxtEnt=NULL, *pnNxtEntDel ;

  if ( nDelEnt > pllEnt->mEnts )
    /* Not a valid ent. */
    return ;

  /* Pointers on the entry to be deleted. */
  pcpVxDel    = pllEnt->pcpVx + nDelEnt*pllEnt->mVxEnt ;
  pnNxtEntDel = pllEnt->pnNxtEnt + nDelEnt ;
  
  if ( !pcpVxDel[0].nr )
    /* Ent already empty. */
    return ;

  cpVx = pcpVxDel[0] ;
  pNtry = pllEnt->ppn1stEntChk[cpVx.nCh] + cpVx.nr ;
  
  if ( *pNtry == nDelEnt )
    /* The ent to be deleted is the entry ent for that vertex. */
    *pNtry = *pnNxtEntDel ;
  else {
    /* Intermediate link. Find the previous link. */
    nPrvEnt = *pNtry ;
    while ( pcpVxPrv = pllEnt->pcpVx + nPrvEnt, pcpVxPrv->nr ) {
      /* Does the next edge match nDelEnt? */
      pnNxtEnt = pllEnt->pnNxtEnt + nPrvEnt ;
      if ( *pnNxtEnt == nDelEnt )
        /* Match. */
        break ;
      else 
        nPrvEnt = *pnNxtEnt ;
    }

    /* Remove nDelEnt from the linked list. */
    *pnNxtEnt = *pnNxtEntDel ;
  }

  /* Prepend the ent to the list of empty Ents. */
  *pnNxtEntDel         = pllEnt->nRootFreeEnt ;
  pcpVxDel[0]          = CP_NULL ;
  pllEnt->nRootFreeEnt = nDelEnt ;

  pllEnt->mEntsUsed = ( pllEnt->mEntsUsed <= 1 ? 0 : pllEnt->mEntsUsed-1 ) ;
  
  return ;
}

/******************************************************************************

  get_ent_vx:
  Scan the ordered/linked list of Ents for a match given a set of vertices.
  Private function.
  
  Last update:
  ------------
  7Apr13; promote large int to ulong_t
          move function up in file to avoid prototyping.
  5Nov10; only check for remaining entries to be zero if a match was found.
  : conceived.
  
  Input:
  ------
  pllEnt:  the list of Ents.
  mVxEnt:     the number of vertices on this ent.
  pVrtx:   the list of vertices on the ent.

  Changes To:
  -----------
  pkVxMin:  the position of the lowest numbered vertex in pVrtx,
  pnFirst: a pointer to the root link for the lowest numbered vertex,
  pnLast:  a pointer to the last link.
  
  Returns:
  --------
  0 on no match, the number of the matching ent nEnt on match.
  
*/

static int get_ent_vx ( const llVxEnt_s *pllEnt, int mVxEnt, 
                        const vrtx_struct *pVrtx[],
                        int *pkVxMin, ulong_t *pnPrvEnt ) {
  
  const vrtx_struct *pVx = NULL ;
  const cpt_s *pcpVxEnt ;
  ulong_t nEnt ;
  int kVx, match ;
  cpt_s cpVxMin ;
  
  /* Find the lowest numbered vertex on the ent. */
  for ( cpVxMin = CP_MAX, *pkVxMin = -1, kVx = 0 ; kVx < mVxEnt ; kVx++ )
    if ( pVrtx[kVx] && ( cmp_cpt( pVrtx[kVx]->vxCpt, cpVxMin ) < 0 ) ) {
      pVx = pVrtx[kVx] ;
      cpVxMin = pVrtx[kVx]->vxCpt ;
      *pkVxMin = kVx ;
    }
  if ( *pkVxMin == -1 )
    /* No vertices on this entity. */
    return ( 0 ) ;

  /* Loop over all ents with cpVxMin as the first vertex. */
  nEnt = *pnPrvEnt = 0 ;
  while ( loop_ent_vx ( pllEnt, pVx, &nEnt ) ) {

    pcpVxEnt = pllEnt->pcpVx + nEnt*pllEnt->mVxEnt ;
    /* Compare the two vertex lists. Note that the lowest vertex
       has been matched already. First compare in the opposite sense. */
    for ( match = 1, kVx = 1 ; kVx < mVxEnt ; kVx++ )
      if ( cmp_cpt( pVrtx[ (mVxEnt-kVx+*pkVxMin)%mVxEnt ]->vxCpt, pcpVxEnt[kVx] ) ) {
        /* Mismatch. */
        match = 0 ;
        break ;
      }

    if ( !match )
      /* Compare with the nodes ordered in the same sense. */
      for ( match = 1, kVx = 1 ; kVx < mVxEnt ; kVx++ )
        if ( cmp_cpt( pVrtx[ (kVx+*pkVxMin)%mVxEnt ]->vxCpt, pcpVxEnt[kVx] ) ) {
          /* Mismatch. */
          match = 0 ;
          break ;
        }

    if ( match ) 
      /* All remaining cpt must be zero. */
      for ( kVx = mVxEnt ; kVx < pllEnt->mVxEnt ; kVx++ )
        if ( pcpVxEnt[kVx].nr )
          /* This one shouldn't have been non-nil. */
          match = 0 ;

    if ( match )
      /* This one matches the sought type ent/face. */
      return ( nEnt ) ;

    *pnPrvEnt = nEnt ;
  }

  /* No matching ent found. */
  return ( 0 ) ;
}



/******************************************************************************

  get_ent_vrtx:
  Scan the ordered/linked list of Ents for a match given two vertices.
  Public function.
  
  Last update:
  ------------
  7Apr13; promote large int to ulong_t.
  : conceived.
  
  Input:
  ------
  pllEnt:  the list of Ents.
  mVx:     the number of vertices on this ent.
  pVrtx:   the list of vertices on the ent.

  Changes To:
  -----------
  pkVxMin:  the position of the lowest numbered vertex in pVrtx,
  
  Returns:
  --------
  0 on no match, the number of the matching ent nEnt on match.
  
*/

int get_ent_vrtx ( const llVxEnt_s *pllEnt, int mVxEng, const vrtx_struct *pVrtx[],
                   int *pkVxMin ) {
  ulong_t nPrvEnt ;

  return ( get_ent_vx ( pllEnt, mVxEng, pVrtx, pkVxMin, &nPrvEnt ) ) ;

}
/******************************************************************************

  add_ent_vrtx:
  Add to the ordered/linked list of Ents one given by a number of vertices.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEnt:     The list of Ents.
  mVx:        Number and
  pVrtx:      List of vertices to be added for this entity.

  Changes To:
  -----------
  *pkVxMin:   The position of the lowest numbered vertex in pVrtx.
  
  Returns:
  --------
  the number of the found/created ent.
  
*/

int add_ent_vrtx ( llVxEnt_s *pllEnt,
                   int mVxEnt, const vrtx_struct *pVrtx[], int *pkVxMin ) {

  const vrtx_struct *pVx ;
  cpt_s *pcpVxEnt ;
  ulong_t nEnt, *pnFirst, nPrvEnt, nr0 ;
  int kVx, nCh0 ;

  if ( ( nEnt = get_ent_vx ( pllEnt, mVxEnt, pVrtx, pkVxMin, &nPrvEnt ) ) )
    /* The ent exists. */
    return ( nEnt ) ;
  else if ( *pkVxMin == -1 )
    /* Empty pVrtx. */
    return ( 0 ) ;
  
  /* Make a new ent. */
  if ( !( nEnt = get_new_ent ( pllEnt ) ) ) {
    hip_err ( fatal, 0, "failed to add ent in add_ent_vrtx.\n" ) ;
  }

  /* Fill the ent. */
  pcpVxEnt = pllEnt->pcpVx + nEnt*pllEnt->mVxEnt ;
  for ( kVx = 0 ; kVx < mVxEnt ; kVx++ )
    pcpVxEnt[kVx] = pVrtx[ (kVx+*pkVxMin)%mVxEnt ]->vxCpt ;
  for ( kVx = mVxEnt ; kVx < pllEnt->mVxEnt ; kVx++ )
    pcpVxEnt[kVx] = CP_NULL ;

  /* Make the links. */
  pVx = pVrtx[*pkVxMin] ;
  nr0 =  pVx->vxCpt.nr ;
  nCh0 = pVx->vxCpt.nCh ;
  
  pnFirst = pllEnt->ppn1stEntChk[nCh0] + nr0 ;
  if ( !*pnFirst )
  /* First occurence of this vertex. Create an entry. */
    *pnFirst = nEnt ;
  else
    /* Append to the linked list. */
    pllEnt->pnNxtEnt[nPrvEnt] = nEnt ;
  pllEnt->pnNxtEnt[nEnt] = 0 ;
  
  return ( nEnt ) ;
}

/******************************************************************************

  loop_ent_vx:
  Loop over all the Ents attached to a given vertex.
  
  Last update:
  ------------
  7Apr13; promote possibly large int to ulong_t.
  : conceived.
  
  Input:
  ------
  pllEnt:    The list of Ents.
  pVx:        The vertex.
  pnEnt:     The last ent looked at. If given as 0, the list is initialized.

  Changes To:
  -----------
  pnEnt:     The last ent looked at. If given as 0, the list is initialized.
  
  Returns:
  --------
  0 on an exhausted/empty list, 1 on a new ent.
  
*/

int loop_ent_vx ( const llVxEnt_s *pllEnt, const vrtx_struct *pVx, ulong_t *pnEnt ) {

  const cpt_s *pcpVxEnt ;
  int nCh1 ;
  ulong_t nr1, nEnt ;
  
  nCh1 = pVx->vxCpt.nCh ;
  nr1  = pVx->vxCpt.nr ;

  if ( nCh1 >= pllEnt->mChunks || nr1 > pllEnt->pmVerts[nCh1] )
    /* No entry space for this vertex, thus there cannot be any Ents. */
    return ( 0 ) ;
  else if ( *pnEnt )
    /* Pick the next ent. */
    nEnt = pllEnt->pnNxtEnt[*pnEnt] ;
  else
    /* Initialize. */
    nEnt = pllEnt->ppn1stEntChk[nCh1][nr1] ;

  if ( nEnt ) {
    pcpVxEnt = pllEnt->pcpVx + nEnt*pllEnt->mVxEnt ;

    /* Check the vertex. */
    if ( pcpVxEnt[0].nCh == nCh1 && pcpVxEnt[0].nr == nr1 ) {
      *pnEnt = nEnt ;
      return ( 1 ) ;
    }
    else {
      /* No match. Did you tamper with *pnEnt? */
      *pnEnt = 0 ;
      if ( verbosity > 5 ) {
        sprintf ( hip_msg, "inconsistent ent %"FMT_ULG", has %d/%"FMT_ULG", but %d/%"FMT_ULG" sought.\n",
                 nEnt, pcpVxEnt[0].nCh, pcpVxEnt[0].nr, nCh1, nr1 ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      return ( 0 ) ;
    }
  }
  else
    /* No or no more Ents. Note that this leaves pnEnt unchanged. */
    return ( 0 ) ;
}

/******************************************************************************

  get_sizeof_llEnt:
  Get the allocated size of pllEnt->pEnt.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEnt: The list of Ents

  Returns:
  --------
  The number of used Ents, mEnts.
  
*/

int get_sizeof_llEnt ( const llVxEnt_s *pllEnt )
{
  if ( !pllEnt )
    return ( 0 ) ;
  else
    return ( pllEnt->mEnts ) ;
}


/******************************************************************************

  get_sizeof_llEnt:
  Get the allocated size of pllEnt->pEnt.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEnt: The list of Ents

  Returns:
  --------
  The number of used Ents, mEnts.
  
*/

int get_used_sizeof_llEnt ( const llVxEnt_s *pllEnt ) {
  if ( !pllEnt )
    return ( 0 ) ;
  else
    return ( pllEnt->mEntsUsed ) ;
}


/******************************************************************************

  show_ent:
  Return the contents of a given ent.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEnt: The list of Ents
  nEnt:   The ent sought.

  Changes To:
  -----------
  pVrtx:  The list of vertices on this entity.
  
  Returns:
  --------
  0 on invalid, 1 on filled ent.
  
*/

int show_ent ( const llVxEnt_s *pllEnt, const int nEnt, vrtx_struct *pVrtx[] )
{
  cpt_s *pcpVxEnt = pllEnt->pcpVx + nEnt*pllEnt->mVxEnt ;
  int kVx ;
  
  if ( nEnt > pllEnt->mEnts )
    /* No such ent. */
    return ( 0 ) ;

  if ( !pcpVxEnt[0].nr )
    /* Ent is empty. */
    return ( 0 ) ;

  for ( kVx = 0 ; kVx < pllEnt->mVxEnt ; kVx++ )
    pVrtx[kVx] = de_cptVx( pllEnt->pUns, pcpVxEnt[kVx] ) ;

  return ( 1 ) ;
}


/******************************************************************************/

void listEnt ( llVxEnt_s const * const pllEnt,
               void (*printData) ( const uns_s*, void const * const ) )
{
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;

  if ( !pllEnt ) {
    printf ( " Empty list of Ents.\n" ) ;
    return ; }
  else if ( !pllEnt->pUns ) {
    printf ( " Empty grid.\n" ) ;
    return ; }

  while ( loop_verts ( pllEnt->pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number )
	listEntVx ( pllEnt, pVrtx, printData ) ;

  printf ( "\n" ) ;
  return ;
}

/******************************************************************************/

void listEntVx ( llVxEnt_s const * const pllEnt,
                 const vrtx_struct *pVrtx,
                 void (*printData) ( const uns_s*, void const * const ) )
{
  ulong_t nEnt = 0 ;
  int nCh = pVrtx->vxCpt.nCh ;
  ulong_t nr = pVrtx->vxCpt.nr ;

  if ( !pVrtx ) {
    printf ( " Empty vertex.\n" ) ;
    return ; }
  
  printf ( "\n vx: %"FMT_ULG" (%d:%"FMT_ULG"), %"FMT_ULG"\n",
           pVrtx->number, nCh, nr, pllEnt->ppn1stEntChk[nCh][nr] ) ;
  
  while ( loop_ent_vx ( pllEnt, pVrtx, &nEnt ) )
    printEnt ( pllEnt, nEnt, printData ) ;

  return ;
}

/******************************************************************************/

void printEnt ( llVxEnt_s const * const pllEnt, ulong_t nEnt,
                void (*printData) ( const uns_s*, void const * const ) )
{
  const cpt_s *pcpVxEnt = pllEnt->pcpVx + nEnt*pllEnt->mVxEnt ;
  const chunk_struct **ppChunk = ( const chunk_struct ** ) pllEnt->pUns->ppChunk ;
  int kVx ; 
  ulong_t nr ;
  int nCh ;

  printf ( " ent %"FMT_ULG", nxt: %5"FMT_ULG", vx:", nEnt, pllEnt->pnNxtEnt[nEnt] ) ;
  for ( kVx = 0 ; kVx < pllEnt->mVxEnt ; kVx++ ) {
    nr  = pcpVxEnt[kVx].nr ;
    nCh = pcpVxEnt[kVx].nCh ;
    printf ( " %"FMT_ULG" (%"FMT_ULG"/%d)", ( nr ? ppChunk[nCh]->Pvrtx[nr].number : 0 ), nr, nCh ) ;
  }

  if ( pllEnt->ppEntData && printData )
    printData ( pllEnt->pUns, *(pllEnt->ppEntData) + nEnt*pllEnt->dataSize ) ;

  printf ( "\n" ) ;
  
  return ;
}
