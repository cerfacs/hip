/*
  uns_create.c:
  Make a 2D box mesh.

  Last update:
  ------------
  
  
  This file contains:
  -------------------

*/
#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern int check_lvl ;

extern Grids_struct Grids ;

/******************************************************************************

  uns_generate:
  Make a cartesian grid.
  
  Last update:
  ------------
  19Dec17; new interface to make_uns.
  11Nov17; update prompt with set_current_grid.
  1Jul16; new interface to check_uns.
  12Dec13; replace = with 'eq' in bc labels.
  4Apr09; replace varTypeS with varList.
  10Apr00: conceived.
  
  Input:
  ------
  ll, ur: lower-left and upper-right coordiantes.
  mI, mJ: number of points in x and y.

  Changes To:
  -----------
  global variable PcurrentGrid.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

/* hip_cython */
ret_s uns_generate ( double ll[2], double ur[2], int mI, int mJ ) {
  ret_s ret = ret_success () ;

  uns_s *pUns ;
  chunk_struct *pChunk ;
  int mVerts, mElems, mBndPatches, mBndFaces, i, j, n ;
  double dI, dJ, *pCo ;
  vrtx_struct *pVx, **ppVx ;
  elem_struct *pEl ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBF ;
  char line[LINE_LEN] ;
  bc_struct *pBc ;

  /* Size. */
  mI = MAX( mI, 2 ) ;
  mJ = MAX( mJ, 2 ) ;
  mVerts = mI*mJ ;
  mElems = ( mI-1 )*( mJ-1 ) ;
  mBndPatches = 4 ;
  mBndFaces = 2*( mI-1 ) + 2*( mJ-1 ) ;

  if ( ll[0] > ur[0] )
    dI = ur[0], ur[0] = ll[0], ll[0] = dI ;
  if ( ll[1] > ur[1] )
    dI = ur[1], ur[1] = ll[1], ll[1] = dI ;
  dI = ( ur[0]-ll[0] )/( mI-1 ) ;
  dJ = ( ur[1]-ll[1] )/( mJ-1 ) ;
    

  
  /* Allocate a grid. */
  if ( !( pUns = make_uns ( NULL ) ) ) {
    ret = hip_err ( fatal,0,"failed to alloc a new unstructured grid in read_uns_dpl." ) ;
    return ( ret ) ; }
  ret.pUns = pUns ;

  pUns->mDim = 2 ;
  pUns->varList.varType = noVar ;

  if ( !( pChunk = append_chunk( pUns, pUns->mDim, mElems, 4*mElems, 0,
                                 mVerts, mBndFaces, mBndPatches ) ) ) {
    ret = hip_err ( fatal, 0, "could not allocate the  connectivity, vertex,"
	     " coordinate or boundary space in read_uns_dpl." ) ;
    return ( ret ) ;
  }


  
  /* Create the vertices. */
  pVx = pChunk->Pvrtx ;
  pCo = pChunk->Pcoor ;
  reset_verts ( pChunk->Pvrtx, mVerts+1 ) ;
  n = 0 ;
  for ( j = 0 ; j < mJ ; j++ )
    for ( i = 0 ; i < mI ; i++ ) {
      n++ ;
      pVx++ ;
      pCo += 2 ;
      pVx->Pcoor = pCo ;
      pVx->number = n ;

      pCo[0] = ll[0] + i*dI ;
      pCo[1] = ll[1] + j*dJ ;
    }


  /* Create the elements. */
  pEl = pChunk->Pelem ;
  reset_elems ( pChunk->Pelem, mElems+1 ) ;
  /* PPvrtx starts customarily at 0. */
  ppVx = pChunk->PPvrtx - 4 ;
  n = 0 ;
  for ( j = 0 ; j < mJ-1 ; j++ )
    for ( i = 0 ; i < mI-1 ; i++ ) {
      n++ ;
      pEl++ ;
      ppVx += 4 ;
      pEl->PPvrtx = ppVx ;
      pEl->number = n ;
      pEl->elType = qua ;

      pEl->PPvrtx[0] = pChunk->Pvrtx + 1 + i +    j* mI ;
      pEl->PPvrtx[1] = pChunk->Pvrtx + 1 + i +    j* mI + 1 ;
      pEl->PPvrtx[2] = pChunk->Pvrtx + 1 + i + (1+j)*mI + 1 ;
      pEl->PPvrtx[3] = pChunk->Pvrtx + 1 + i + (1+j)*mI ;
    }


  /* Boundary faces. */
  pBP = pChunk->PbndPatch ;
  pBF = pChunk->PbndFc ;

  
  /* Bottom boundary. */
  sprintf ( line, "bottom_y_eq_%g", ll[1] ) ;
  pBc = find_bc ( line, 1 ) ;
  pBP++ ;
  pBP->PbndFc = pBF+1 ;
  pBP->mBndFc = mI-1 ;
  pBP->Pbc = pBc ;

  for ( i = 1 ; i < mI ; i++ ) {
    pBF++ ;
    pBF->Pelem = pChunk->Pelem + i ;
    pBF->nFace = 1 ;
    pBF->Pbc = pBc ;
  }

  
  /* Right boundary. */
  sprintf ( line, "right_x_eq_%g", ur[0] ) ;
  pBc = find_bc ( line, 1 ) ;
  pBP++ ;
  pBP->PbndFc = pBF+1 ;
  pBP->mBndFc = mJ-1 ;
  pBP->Pbc = pBc ;

  for ( j = 1 ; j < mJ ; j++ ) {
    pBF++ ;
    pBF->Pelem = pChunk->Pelem + j*( mI-1 ) ;
    pBF->nFace = 2 ;
    pBF->Pbc = pBc ;
  }

  
  /* Top boundary. */
  sprintf ( line, "top_y_eq_%g", ur[1] ) ;
  pBc = find_bc ( line, 1 ) ;
  pBP++ ;
  pBP->PbndFc = pBF+1 ;
  pBP->mBndFc = mI-1 ;
  pBP->Pbc = pBc ;

  for ( i = mI-1 ; i > 0 ; i-- ) {
    pBF++ ;
    pBF->Pelem = pChunk->Pelem + i + ( mJ-2 )*( mI-1 ) ;
    pBF->nFace = 3 ;
    pBF->Pbc = pBc ;
  }

  
  /* Left boundary. */
  sprintf ( line, "left_x_eq_%g", ll[0] ) ;
  pBc = find_bc ( line, 1 ) ;
  pBP++ ;
  pBP->PbndFc = pBF+1 ;
  pBP->mBndFc = mJ-1 ;
  pBP->Pbc = pBc ;

  for ( j = mJ-1 ; j > 0 ; j-- ) {
    pBF++ ;
    pBF->Pelem = pChunk->Pelem + ( j-1 )*( mI-1 ) +1 ;
    pBF->nFace = 4 ;
    pBF->Pbc = pBc ;
  }
  
  
  grid_struct *pGrid ;
  /* Unstructured. Make a new grid. */
  if ( !( pGrid = make_grid () ) ) {
    free_chunk ( pUns, &pChunk ) ;
    ret = hip_err ( fatal, 0,
                    "malloc for the linked list of grids failed in uns_generate." ) ;
  }
  ret.pGrid = pGrid ;

  /* Put the chunk into Grids. */
  pGrid->uns.type = uns ;
  pGrid->uns.pUns = pUns ;
  pGrid->uns.pVarList = &(pUns->varList) ;
  pGrid->uns.mDim = 2 ;
  pUns->nr = pGrid->uns.nr ;
  pUns->pGrid = pGrid ;

  /* Validate, count and number the grid. */
  check_uns ( pUns, check_lvl ) ;

  set_current_pGrid ( pGrid ) ;
  
  return ( ret ) ;
}
