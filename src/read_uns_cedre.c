/*
   read_uns_cedre.c:
   Read an unstructured cedre format.

   Last update:
   ------------
   15May07; derived from read_uns_fluent.

   This file contains:
   -------------------
   read_uns_cedre

*/
#include <string.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

#include "r1map.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;

/* A face out of the file, with forming nodes, formed cells, and a doubly linked
   list for each formed element. */
typedef struct _cdrFc_s cdrFc_s ;
struct _cdrFc_s {
  int mVxFc ; int nVx[MAX_VX_FACE] ;
  int nEl[2] ;
  cdrFc_s *pNxtFc[2] ;
} ;

/******************************************************************************

  cdr_count_conn:
  Given a list of faces pointing to elements, assign element types to each
  element and count the total number of connectivity entries.
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  4Apr09; remove duplicated make_grid, as make_uns_grid already does this.
  28Jun07; Extracted from read_uns_cedre.
  
  Input:
  ------
  mDim       = no of spatial dimensions
  mFlFc      = no of faces
  pFace      = list of faces
  mElems     = no of elems
  pElem      = list of elems
  mElemsType = counter for each elem type

  Changes To:
  -----------
  mElemsType
  
  Returns:
  --------
  mEl2Vx = total number of connectivity entries
  
*/

int cdr_count_conn ( int mDim, int mFlFc, flFc_s *pFace, 
                     int mElems, elem_struct *pElem, int mElemsType[] ) {

  int *pmBiFc, *pmTriFc, *pmQuadFc, nFc, mVxFc, nEl1, nEl2, mEl2Vx = 0, nEl ;
  elType_e elT ;
  flFc_s *pFc ;
  elem_struct *pEl ;

  for ( elT = tri ; elT < hex ; elT++ )
    mElemsType[elT] = 0 ; 


  /* Allocate temporary face counters. */
  pmBiFc   = arr_calloc ( "pmBiFc in read_uns_cedre"  ,NULL, mElems+1, sizeof(int)) ;
  pmTriFc  = arr_calloc ( "pmTriFc in read_uns_cedre" ,NULL, mElems+1, sizeof(int)) ;
  pmQuadFc = arr_calloc ( "pmQuadFc in read_uns_cedre",NULL, mElems+1, sizeof(int)) ;


  /* el2vx_from_fc needs the face numbers running from 1. */
  for ( nFc = 1, pFc = pFace+nFc ; nFc <= mFlFc ; pFc++, nFc++ ) {
    mVxFc = pFc->mVxFc ;
    nEl1 = pFc->nEl[0] ;
    nEl2 = pFc->nEl[1] ; 

 
    if      ( mVxFc == 2 ) { 
      pmBiFc[nEl1]++ ; 
      pmBiFc[nEl2]++ ; 
    }
    else if ( mVxFc == 3 ) { 
      pmTriFc[nEl1]++ ; 
      pmTriFc[nEl2]++ ; 
    }
    else if ( mVxFc == 4 ) { 
      pmQuadFc[nEl1]++ ; 
      pmQuadFc[nEl2]++ ; 
    }
    else if ( mVxFc > 4 )  {
      printf ( " FATAL: found face with %d nodes in read_uns_cedre\n", mVxFc ) ;
      return ( 0 ) ;
    }
    else if ( mVxFc > 2 && mDim == 2 )  {
      printf ( " FATAL: found face with %d nodes for 2-D grid in read_uns_cedre\n", 
               mVxFc ) ;
      return ( 0 ) ;
    }
    else if ( mVxFc < 3 && mDim == 3 )  {
      printf ( " FATAL: found face with %d nodes for 3-D grid in read_uns_cedre\n", 
               mVxFc ) ;
      return ( 0 ) ;
    }
  }

  for ( pEl = pElem+1, nEl = 1 ; nEl <= mElems ; pEl++, nEl++ ) {
    if ( mDim == 2 ) {
      if ( pmBiFc[nEl] == 3 ) {
        pEl->elType = tri ;
        mElemsType[pEl->elType]++ ;
      }
      else if ( pmBiFc[nEl] == 4 ) {
        pEl->elType = qua ;
        mElemsType[pEl->elType]++ ;
      }
      else {
        printf ( " FATAL: unrecognised element with %d bi faces"
                 " in read_uns_cedre.\n", pmBiFc[nEl] ) ;
        return ( 0 ) ;
      }
    }
    else {
      if ( pmQuadFc[nEl] == 0 && pmTriFc[nEl] == 4 ) {
        pEl->elType = tet ;
        mElemsType[pEl->elType]++ ;
      }
      else if ( pmQuadFc[nEl] == 1 && pmTriFc[nEl] == 4 ) {
        pEl->elType = pyr ;
        mElemsType[pEl->elType]++ ;
      }
      else if ( pmQuadFc[nEl] == 3 && pmTriFc[nEl] == 2 ) {
        pEl->elType = pri ;
        mElemsType[pEl->elType]++ ;
      }
      else if ( pmQuadFc[nEl] == 6 && pmTriFc[nEl] == 0 ) {
        pEl->elType = hex ;
        mElemsType[pEl->elType]++ ;
      }
      else {
        printf ( " FATAL: unrecognised element with %d tri and %d quad faces"
                 " in read_uns_cedre.\n", pmTriFc[nEl], pmQuadFc[nEl] ) ;
        return ( 0 ) ;
      }
    }
  }

  for ( mEl2Vx = 0, elT = tri ; elT <= hex ; elT++ )
    mEl2Vx += mElemsType[elT]*elemType[elT].mVerts ;

  arr_free ( pmBiFc ) ;
  arr_free ( pmTriFc ) ;
  arr_free ( pmQuadFc ) ;



  return ( mEl2Vx ) ;
}



/******************************************************************************

  read_uns_cedre:
  Read an unstructured cedre file.
  
  Last update:
  ------------
  1Jul16; new interface to check_uns.
  6Jul13; new interface to make_uns_grid.
  conceived?
  
  Input:
  ------
  flName:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_uns_cedre ( char *flName ) {
  
  unsigned int mDim ;
  int mVerts, mElems, mFlFc, nKey, nFile, mBc, mFlBnd, nVx, nDim, i,
    mEl2Vx, nFc, mBndFc, mElemsType[MAX_ELEM_TYPES] = {0}, ndummy,
    kFc, keySize, minALen, mElsFc, axi=0, nBf ;
  FILE *cdrFile ;
  char text[TEXT_LEN], magicKey[] = "1.", closeFiles[] = "t" ;

  grid_struct *pGrid ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pEl ;
  vrtx_struct *pVrtx, *pVx, **ppVrtx, **ppVx ;
  double *pCoor, *pCo ;
  bndFc_struct *pBndFc, *pBf ;
  flFc_s *pFace, *pFc ;
 
  /* Map the file. ascii only, bogus magicBKey. */
  keySize = sizeof( magicKey ) ;
  minALen = sizeof ( "2. FACES -> SOMMETS" ) ;
  r1map_reset ( magicKey, &minALen, magicKey, &keySize, 0, closeFiles, NULL, NULL ) ;
  prepend_path ( flName ) ;
  r1map_open_file ( flName, "a" ) ;


  /* Header. */
  nKey = 1 ; nFile = 0 ;
  if ( !r1map_pos_keyword ( "0. DONNEES GENERALES", &nFile, &nKey, &cdrFile ) ) {
    printf ( " FATAL: no section DONNEES GENERALES in read_uns_cedre.\n" ) ;
    return ( 0 ) ; }
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;




  /* Name of the geometry */
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;

  /* Dimension */
  fscanf( cdrFile, "%s%*[^\n]", text );
  r1_beginstring ( text, TEXT_LEN ) ;  
  if ( !strncmp ( text, "2D_PLAN", 6 ) ) {
    mDim = 2 ; }
  else if ( !strncmp ( text, "2D_AXI", 6 ) ) {
    mDim = 2 ;
    axi = 1 ; }
  else if ( !strncmp ( text, "3D", 2 ) ) {
    mDim = 3 ;}
  else {
    printf ( " FATAL: Wrong dimension statement.\n" ) ;
    return ( 0 ) ; 
  }
  fscanf ( cdrFile, "\n" ) ;

  /* Axis of the geometry */ 
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;

  /* Reference length scale */
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;

  /* Number of Nodes, faces.... */
  mVerts = mElems = mFlFc = -1 ;
  mBc = mBndFc = mFlBnd = 0 ;

  /* Number of vertices */
  fscanf ( cdrFile, "%d%*[^\n]", &mVerts );
  fscanf ( cdrFile, "\n" ) ;

  /* Number of internal Faces */
  fscanf ( cdrFile, "%d%*[^\n]", &mFlFc );
  fscanf ( cdrFile, "\n" ) ;

  /* Number of Elements */
  fscanf ( cdrFile, "%d%*[^\n]", &mElems );
  fscanf ( cdrFile, "\n" ) ;

  /* Number of boundary faces */
  fscanf ( cdrFile, "%d%*[^\n]", &mBndFc );
  fscanf ( cdrFile, "\n" ) ;


  /* Number of boundary limits */
  /* This point has to be added to the cedre mesh file */
  fscanf ( cdrFile, "%d%*[^\n]", &mBc );
  fscanf ( cdrFile, "\n" ) ;

  if ( mVerts == -1 || mElems == -1 || mFlFc == -1 ) {
    printf ( " FATAL: failed to read number of verts/elems/faces"
             " in read_uns_cedre.\n" ) ;
    return ( 0 ) ; }
  else if ( verbosity > 2 )
    printf ( "    INFO: found %d vertices, %d elements, %d faces, %d bc.\n",
             mVerts, mElems, mFlFc, mBc ) ;


  uns_s *pUns = NULL ;
  pGrid = make_uns_grid ( &pUns, mDim, mElems, 0, 0, mVerts, 0, mBndFc, mBc ) ;
  pUns = pGrid->uns.pUns ;
  pChunk = pUns->pRootChunk ;
  pVrtx = pChunk->Pvrtx ;
  pElem = pChunk->Pelem ;
  pCoor = pChunk->Pcoor ;
  pBndFc = pChunk->PbndFc ;




  /* Temporary face storage of all faces. Fluent numbers faces from 1. */
  pFace = arr_malloc ( "pFace in read_uns_cedre",
                       pUns->pFam, mFlFc+1, sizeof( cdrFc_s ) ) ;
  
  

  /*****
        Read the coordinates.
  *****/
  if ( !r1map_pos_keyword ( "1. SOMMETS DU MAILLAGE", &nFile, &nKey, &cdrFile ) ) {
    printf ( " FATAL: no section SOMMETS DU MAILLAGE in read_uns_cedre.\n" ) ;
    return ( 0 ) ; }
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;


  for ( nVx = 1 ; nVx <= mVerts ; nVx++)  {
    pCo = pCoor + nVx*mDim ;
    pVx = pVrtx + nVx ;
    pVx->number = nVx ;
    pVx->Pcoor = pCo ;
    fscanf ( cdrFile, "%d", &ndummy );
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      if ( !fscanf ( cdrFile, "%lf", pCo+nDim ) ) {
        printf ( " FATAL: failed to read coor. for %d in read_uns_cedre.\n", nVx ) ;
        return ( 0 ) ;
      }
    fscanf ( cdrFile, "%*[^\n]");
    fscanf ( cdrFile, "\n" ) ;
  }

 

  /*****
        Read the internal faces.
  *****/
  if ( !r1map_pos_keyword ( "2. FACES -> SOMMETS", &nFile, &nKey, &cdrFile ) ) {
    printf ( " FATAL: no section FACES -> SOMMETS in read_uns_cedre.\n" ) ;
    return ( 0 ) ; }
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;


  /* Fill pFace. First read connectivity between Faces and Vertices */
  for ( nFc = 1 ; nFc <= mFlFc ; nFc++ )  {
    fscanf ( cdrFile, "%d %d", &ndummy, &pFace[nFc].mVxFc );
    for ( i = 0 ; i < pFace[nFc].mVxFc ; i++ )
      fscanf ( cdrFile, "%"FMT_ULG"", pFace[nFc].nVx+i ); 
    fscanf ( cdrFile, "%*[^\n]" ) ;    
    fscanf ( cdrFile, "\n" ) ;
  }



  /* Then read connectivity between Faces and Cells */
  if ( !r1map_pos_keyword ( "3. FACES -> CELLULES", &nFile, &nKey, &cdrFile ) ) {
    printf ( " FATAL: no section FACES -> CELLULES in read_uns_cedre.\n" ) ;
    return ( 0 ) ; }
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;

  for ( nFc = 1 ; nFc <= mFlFc ; nFc++ )  {
    pFace[nFc].nEl[1] = 0 ;
    fscanf ( cdrFile, "%d %d", &ndummy, &mElsFc );
    for ( i = 0 ; i < mElsFc ; i++ )    {
      fscanf ( cdrFile, "%"FMT_ULG"", pFace[nFc].nEl+i ); 
    }
    fscanf ( cdrFile, "%*[^\n]" ) ;    
    fscanf ( cdrFile, "\n" ) ;
  }




  /* Boundary faces. */
  if ( !r1map_pos_keyword ( "4. FACES MARQUEES-> FACE", &nFile, &nKey, &cdrFile ) ) {
    printf ( " FATAL: no section FACES MARQUEES-> FACE in read_uns_cedre.\n" ) ;
    return ( 0 ) ; }
  fscanf ( cdrFile, "%*[^\n]" );
  fscanf ( cdrFile, "\n" ) ;

  for ( pBf = pBndFc+1, nBf = 1 ; nBf <= mBndFc ; pBf++, nBf++ ) {
    fscanf ( cdrFile, "%d %d %s", &ndummy, &nFc, text ) ;
    pBf->nFace = nFc ;
    pBf->Pbc = find_bc ( text, 1 ) ;
  }


  r1map_close_allfiles () ;


  /* Count the no of conn entries and set elem types of each elem. */
  mEl2Vx = cdr_count_conn ( mDim, mFlFc, pFace, mElems, pElem, mElemsType ) ;


  if ( verbosity > 3 )
    printf ( "     INFO: found %d tris, %d quads,"
             " %d tets, %d pyrs, %d prisms, %d hexes,\n"
             "           allocating %d element to vertex pointers.\n",
             mElemsType[0], mElemsType[1], 
             mElemsType[2], mElemsType[3], mElemsType[4], mElemsType[5],
             mEl2Vx ) ;
  







  /* Malloc for element to vertex pointers. */
  pChunk->PPvrtx = ppVrtx = arr_calloc ( "pChunk->PPvrtx in read_uns_cedre", pUns->pFam,
                                         mEl2Vx, sizeof( vrtx_struct * ) ) ;
  pChunk->mElem2VertP = mEl2Vx ;

  /* Set the vertex pointers for each element. */
  for ( pEl = pElem+1, ppVx = ppVrtx ; pEl <= pElem + mElems ; pEl++ ) {
    pEl->PPvrtx = ppVx ;
    ppVx += elemType[ pEl->elType ].mVerts ;
  }


  /* Assemble the element to vertex pointers from the list of faces. */
  elem2vx_from_fc ( mFlFc, pFace, mElems, pElem, pVrtx ) ;






  /* List boundary faces with elements. */
  for ( pBf = pBndFc+1, nBf = 1 ; nBf <= mBndFc ; pBf++, nBf++ ) {

    pFc = pFace + pBf->nFace ;
    pEl = pElem + pFc->nEl[0] ;
    if ( ( kFc = face_in_elem ( pEl, pFc->mVxFc, pFc->nVx ) ) ) {
      pBf->Pelem = pEl ;
      pBf->nFace = kFc ;
    }
    else {
      sprintf ( hip_msg, 
               "could not match boundary face %d to elem %"FMT_ULG" in read_uns_cedre\n",
               nBf, pEl->number ) ;
      hip_err( fatal, 0, hip_msg ) ;
    }
  }
  

  /* Dealloc temporary spaces. */
  arr_free ( pFace ) ;


  make_uns_bndPatch ( pUns ) ;



  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;

  Grids.epsOverlap = .9*pUns->hMin ;
  Grids.epsOverlapSq = Grids.epsOverlap* Grids.epsOverlap ;
  

  return ( 1 ) ;

}


