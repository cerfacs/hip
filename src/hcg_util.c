/*
  cg_util.c:
*/

/*! hip's implementation of the CGNS midlevel library.
 */


/* 
  Last update:
  ------------
  20Mar18; rename to hcg_util.
  4Apr11; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#include "hdf5.h"
#include "cgnslib.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;
herr_t      cg_status ;


/******************************************************************************
  cg_bcTypeDecode:   */

/*! Convert CGNS bc tags to hip types.
 *
 */

/*
  
  Last update:
  ------------
  19Mar18; moved here from read_uns_cgns.
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int hcg_bcTypeDecode ( BCType_t cg_BCType, char *bcType ) {
  /* ex cgnslib.h
typedef enum {
	BCTypeNull, BCTypeUserDefined,
	BCAxisymmetricWedge, BCDegenerateLine, BCDegeneratePoint,
	BCDirichlet, BCExtrapolate, BCFarfield, BCGeneral, BCInflow,
	BCInflowSubsonic,  BCInflowSupersonic, BCNeumann, BCOutflow,
	BCOutflowSubsonic, BCOutflowSupersonic, BCSymmetryPlane,
	BCSymmetryPolar, BCTunnelInflow, BCTunnelOutflow, BCWall,
	BCWallInviscid, BCWallViscous, BCWallViscousHeatFlux,
	BCWallViscousIsothermal, FamilySpecified
} BCType_t;

#define NofValidBCTypes 26
  */

  /* cpre.h:
   e:   & entry           &    o:   & outlet          & u:   & upper periodic  \\
   f:   & far field       &    p:   & any periodic    & v:   & viscous wall    \\
   i:   & inviscid wall   &    n:   & none            & w:   & any wall        \\
   l:   & lower periodic  &    s:   & symmetry plane  \\
  */

  switch ( cg_BCType ) {
  case BCFarfield              : strcpy ( bcType, "f" ) ; break ;
  case BCInflow                : strcpy ( bcType, "e" ) ; break ;
  case BCInflowSubsonic        : strcpy ( bcType, "e" ) ; break ;
  case BCInflowSupersonic      : strcpy ( bcType, "e" ) ; break ;
  case BCOutflow               : strcpy ( bcType, "o" ) ; break ;
  case BCOutflowSubsonic       : strcpy ( bcType, "o" ) ; break ;
  case BCOutflowSupersonic     : strcpy ( bcType, "o" ) ; break ;
  case BCSymmetryPlane         : strcpy ( bcType, "s" ) ; break ;
  case BCSymmetryPolar         : strcpy ( bcType, "s" ) ; break ;
  case BCTunnelInflow          : strcpy ( bcType, "e" ) ; break ;
  case BCTunnelOutflow         : strcpy ( bcType, "o" ) ; break ;
  case BCWall                  : strcpy ( bcType, "w" ) ; break ;
  case BCWallInviscid          : strcpy ( bcType, "i" ) ; break ;
  case BCWallViscous           : strcpy ( bcType, "v" ) ; break ;
  case BCWallViscousHeatFlux   : strcpy ( bcType, "v" ) ; break ;
  case BCWallViscousIsothermal : strcpy ( bcType, "v" ) ; break ;
  default                      : strcpy ( bcType, "n" ) ; 
  }
  return ( 0 ) ;
}

/* Same as cg_is_cgns, but for testing database opening.
int hcg_is_cgns(const char *filename, int *file_type)
{
    int cgio, ierr;
    double rootid, childid;

    *file_type = CG_FILE_NONE;
    if (cgio_open_file(filename, CG_MODE_READ, CG_FILE_NONE, &cgio))
        return CG_ERROR;
    cgio_get_root_id(cgio, &rootid);
    cgio_get_file_type(cgio, file_type);
    ierr = cgio_get_node_id(cgio, rootid, "CGNSLibraryVersion", &childid);
    cgio_close_file(cgio);
    return ierr ? CG_ERROR : CG_OK;
}
 */

/******************************************************************************
  cg_open:   */

/*! Open a CGNS database
 */

/*
  
  Last update:
  ------------
  14Dec19; add comment re mcgns.
  8Mar18; rename to hcg to avoid name clash with cgns lib.
  4Apr11: conceived.
  

  Input:
  ------
  fileName
  mode: CGNS access mode specified

  Output:
  -------
  file_id: integer file handle (hid_t).
    
  Returns:
  --------
  EXIT_FAILURE on failure, EXIT_SUCCESS on success
  
*/

int hcg_open ( const char *fileName, int mode ) {

  FILE* Ftest ;
  char cgnsFile[TEXT_LEN] ;
  strncpy ( cgnsFile, fileName, TEXT_LEN-1 ) ;
  prepend_path ( cgnsFile ) ;

  if ( mode == CG_MODE_READ ) {
    /* Test whether the existing file is openable. Do this outside of hdf, as
       hdf throws up ugly pages of error msgs. */
    if ( !( Ftest = r1_fopen ( cgnsFile, TEXT_LEN, "r" ) ) ) {
      sprintf ( hip_msg, "could not find file %s in hcg_open.\n", fileName ) ;
      hip_err ( fatal, 0, hip_msg ) ;
      return ( EXIT_FAILURE ) ;
    }
    else
      fclose ( Ftest ) ;
  }

  int fileType ;
  
  if ( ( cg_status = cg_is_cgns( cgnsFile, &fileType) ) ) {
    ssize_t msg_len = H5Eget_msg( cg_status, H5E_MAJOR, hip_msg, LINE_LEN ) ;
    if ( msg_len > 0 )
      hip_err ( fatal, 0, hip_msg ) ;
    else
      hip_err ( fatal, 0, "file exists, but cgns refuses to read.\n"
                "          Incompatible hdf versions?  hip expectes hdf > 1.8.\n"
                "          Or trying to read a multiblock structured cgns \n"
                "          as unstrucured? Use 'read mcgns' for that." ) ;
  }
  else if ( fileType != CG_FILE_HDF5 )
    hip_err ( fatal, 0, "hip only supports hdf backends. Use adf2hdf." ) ;


  /* Open the cgns file. */
  int file_id, ier ;
  if ( (ier = cg_open( cgnsFile, mode, &file_id ) ) ) {
    sprintf ( hip_msg,"failed to open CGNS file %s in hcg_open\n", cgnsFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( file_id ) ;

}
