/*
 read_uns_saturne.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an IDEAS grid file and a number of ensight gold
 solution files.

 contains:
 ---------
 findIdeasTag
 read_ideas_grid
 read_ensight_master
 read_uns_saturne
 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern int check_lvl ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;


/* Number of vertices to element type. Only prisms and hexas at the moment.*/
static const elType_e satElType[9] = { noEl, noEl, noEl, noEl, 
                                             noEl, noEl, pri, noEl, hex } ;

/* A conversion list from AVBP numbering to Saturne's. */
static const int a2s[6][8] = { 
  {0,0,0},          /* tri */
  {0,0,0,0},        /* quad */
  {0,0,0,0},       /* tet */
  {0,0,0,0,0},      /* pyramids. */
  {0,3,4,1,5,2},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;



/* Advance the file pointer to the beginning of the next line. */
static void nxtline ( FILE *file ) {
  fscanf ( file, "%*[^\n]" ) ;
  fscanf ( file, "%*[\n]" ) ;
  return ;
}

/* Locate a tag indicated by a -1 on a line followed by the tag on the next. */
static int findIdeasTag ( FILE* Fgrd, const int iTag ) {

  int tag, tag1 = 0, tag2 = 0 ;

  while ( 1 ) {
    if ( !fscanf ( Fgrd, "%d", &tag ) )
      tag = 0 ;

    if ( feof ( Fgrd ) )
      /* Tag not in file. */
      return ( 0 ) ;

    tag1 = tag2 ;
    tag2 = tag ; 
    nxtline ( Fgrd ) ;

    /* The tag has to have a -1 on the previous line. */     
    if ( tag1 == -1 && tag2 == iTag ) {
      return ( 1 ) ;
    }
  }
}

/* Read a number of doubles allowing exponential formats using a D. */
static int readNdbl ( FILE *file, const int n, double *dbl ) {

  int i, trailing ;
  char str[TEXT_LEN], *pStr, c ;

  for ( i = 0 ; i < n ; i++ ) {
    /* Read up to the next space character or comma. */
    for ( c = ' ', pStr = str, trailing = 0 ; c ;  ) {
      c = fgetc ( file ) ;
      if ( isspace ( c ) || c == ',' || c == '\0' || c == '\n' ) {
        if ( trailing ) {
          /* Return a newline to the stream. */
          ungetc ( c, file ) ;
          c = '\0' ;
        }
      }       
      else if ( isdigit ( c ) || c == '.' || tolower ( c ) == 'e' )
        /* There will always be a digit to toggle to trailing whitespace. */
        trailing = 1 ;
      else if ( c == '+' || tolower ( c ) == '-' )
        ;
      else if ( tolower ( c ) == 'd' )
        /* Convert the d to an e. */
        c = 'e' ;


      if ( pStr-str >= TEXT_LEN )
        return ( i ) ;
      else 
        *pStr++ = c ;
    }

    if ( !sscanf ( str, "%lf", dbl+i ) )
      break ;
  }
  
  return ( i ) ;
}


/******************************************************************
 
 read_uns_cfdrc.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an unstructured .cfdrc file for cpre.

 Last update:
 ------------
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
 9Jul15; fix bad logical bracketing with &iBc, &mVxEl ) != 2
 9Jul11; call init_elem.
 4Apr09; replace varTypeS with varList.
 11Jun03; derived from read_uns_cfdrc

 Input:
 ------
 FsatIn: the opened sat file.

 Changes to:
 -----------


 Returns:
 --------
 
*/

static int read_ideas_grid ( FILE *Fgrd, uns_s *pUns ) {
  
  const int mDim = 3 ;

  grid_struct *pGrid ;
  chunk_struct *pChunk ;

  int mVx=0, mEl=0, mEl2Vx=0, mBndFc=0, mBc=0, iNr, nVx, iarr[4], iBc, mVxEl, i, k, 
    nEl, kVx, nFrmVx[MAX_VX_ELEM], mVxFc, nBc, mFc ;
  vrtx_struct *pVx, **ppVx ;
  elem_struct *pEl ; 
  double *pCo ;

  char bcText[TEXT_LEN] ;
  bndFcVx_s * pBndFcVx, *pBf ;
  bc_struct *pBc ;

  
  /* This will be the next grid. */
  if ( verbosity > 2 )
    printf ( "  Reading unstructured saturne/IDEAS grid file.\n" ) ;



  /* IDEAS uses a -1 as a dividing header and a keyword thereafter. */
  if ( !findIdeasTag( Fgrd, 151 ) ) {
    printf ( " FATAL: file does not seem to be an IDEAS grid file.\n" ) ;
    return ( 0 ) ; }

  

  /* Numbers of nodes, cells, boundary faces. */
  if ( !findIdeasTag ( Fgrd, 2411 ) ) {
    printf ( " FATAL: could not locate nodes in read_uns_saturne.\n" ) ;
    return ( 0 ) ; }

  while ( 1 ) {
    iNr = 0 ;
    if ( fscanf ( Fgrd, "%d", &iNr ) && iNr > 0 ) {
      /* The IDEAS file has a line starting with node number and some junk
         followed by a line with the coordinates x,y,z. */
      nxtline ( Fgrd ) ;
      nxtline ( Fgrd ) ;
      mVx++ ; 
    }
    else if ( iNr == -1 )
      break ;
    else {
      printf ( " FATAL: could not read number of nodes in read_uns_saturne.\n" ) ;
      return ( 0 ) ; 
    }
  }
       

  /* Numbers of elements, boundary faces. */
  if ( !findIdeasTag ( Fgrd, 2412 ) ) {
    printf ( " FATAL: could not locate elements in read_uns_saturne.\n" ) ;
    return ( 0 ) ; }

  while ( 1 ) {
    if ( !fscanf ( Fgrd, "%d", &iNr ) )
      iNr = -1 ;
    else if ( iNr == -1 )
      ;
    else if ( fscanf ( Fgrd, "%*d%*d%*d%d%d%*[^\n]", &iBc, &mVxEl ) != 2 )
      iNr = -1 ;
    else {  
      nxtline ( Fgrd ) ;
      /* Skip the line with the element to node pointers. */
      nxtline ( Fgrd ) ;
    }

    if ( iNr > 0 && iBc == 0 ) {
      /* The IDEAS file has a line starting with element number, some junk,
         bcNr and number of vertices, followed by a line with the forming nodes. */
      mEl++ ;
      if ( mVxEl == 8 || mVxEl == 6 )
        /* Only prisms and hexas for the time being. */
        mEl2Vx += mVxEl ; 
      else {
        printf ( " FATAL: contact domino software headquaters to implement a"
                 " %d-noded element in read_ideas_grid.\n", mVxEl ) ;
        return ( 0 ) ;
      }
    }
    else if ( iNr >0 && iBc > 0 )
      /* Boundary face. */
      mBndFc++ ;
    else if ( iNr == -1 )
      break ;
    else {
      printf ( " FATAL: could not read number of elements, faces"
               " in read_uns_saturne.\n" ) ;
      return ( 0 ) ; }
  }
       


  /* Numbers of boundary conditions. */
  if ( !findIdeasTag ( Fgrd, 2435 ) ) {
    printf ( " FATAL: could not locate boundary conditions in read_uns_saturne.\n" ) ;
    return ( 0 ) ; }

  while ( 1 ) {
    if ( !fscanf ( Fgrd, "%d", &iNr ) )
      iNr = -1 ;
    else if ( iNr == -1 )
      break ;
    else if ( !fscanf ( Fgrd, "%*d%*d%*d%*d%*d%*d%d%*[^\n]", &mFc ) )
      iNr = -1 ;
    else {  
      nxtline ( Fgrd ) ;
    }

    if ( iNr != -1 ) {
      /* The IDEAS file has a line for each bc with the number of faces and nodes
         at the end. */
      if ( fscanf ( Fgrd, "%s%*[^\n]%*[\n]", bcText ) ) {
        mBc++ ;

        /* Skip iNr groups of 4 ints. */
        for ( i = 0 ; i < mFc*4 ; i++ )
          fscanf ( Fgrd, "%*d" ) ;

      } else {
        printf ( " FATAL: could not bc label in read_uns_saturne.\n" ) ;
        return ( 0 ) ; 
      }
   }
  }



  /* Allocate the fields of which the sizes are known by now. */
  if ( verbosity > 2 ) {
    printf ( "   Number of elems:              %-d\n", mEl ) ;
    printf ( "   Number of vertices:           %-d\n", mVx ) ;
    printf ( "   Number of boundary patches:   %-d\n", mBc ) ;
    printf ( "   Number of boundary faces:     %-d\n", mBndFc ) ; }


  pUns->mDim = mDim ;
  pUns->mBc = mBc ;
  /* No solution yet. */
  pUns->varList.mUnknowns = pUns->varList.mUnknFlow = 0 ;
  pUns->varList.varType = noVar ;

  if ( !( pChunk = append_chunk( pUns, pUns->mDim, mEl, mEl2Vx, 0, mVx, mBndFc, mBc ))) {
    printf ( " FATAL: could not allocate the  connectivity, vertex,"
	     " coordinate or boundary space in read_uns_sat.\n" ) ;
    return ( 0 ) ; }










  
  /* Read vertex coordinates. */
  rewind ( Fgrd ) ;
  findIdeasTag ( Fgrd, 2411 ) ;

  /* Note: Pcoor starts with index 1, ie. the first set of nDim coordinates skipped. */	
  for ( pCo = pChunk->Pcoor+mDim, pVx = pChunk->Pvrtx+1, nVx = 1 ;
	nVx <= mVx ; nVx++, pVx++, pCo += mDim ) {
    pVx->Pcoor = pCo ;
    pVx->Punknown = NULL ;
    pVx->number = nVx ;
    fscanf ( Fgrd, "%*d" ) ;
    nxtline ( Fgrd ) ;
    /* ICEM writes in an exponential format like 1.23D+00 which fscanf can't parse. */
    if ( readNdbl ( Fgrd, 3, pCo ) != 3 ) {
        printf ( " FATAL: could not bc label in read_uns_saturne.\n" ) ;
        return ( 0 ) ; 
    }
    nxtline ( Fgrd ) ;
  }




  
  /* Read element connectivity.  */
  findIdeasTag ( Fgrd, 2412 ) ;


  pEl = pChunk->Pelem ;
  ppVx = pChunk->PPvrtx ;
  pVx = pChunk->Pvrtx ;

  for ( nEl = 1 ; nEl <= mEl ; nEl++ ) {
    fscanf ( Fgrd, "%*d%*d%*d%*d%*d%d%*[^\n]", &mVxEl ) ;
    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) 
      fscanf ( Fgrd, "%d", nFrmVx+kVx ) ;
    nxtline ( Fgrd ) ;
    
#   ifdef CHECK_BOUNDS
      if ( ppVx + mVxEl - 1 > pChunk->PPvrtx + mEl2Vx - 1 ) {
        printf ( " FATAL: more connectivity pointers found than counted.\n" ) ;
        exit ( EXIT_FAILURE ) ; }
#   endif

    pEl++ ;
    init_elem ( pEl, satElType[mVxEl], nEl, ppVx ) ;
    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) 
      *(ppVx++) = pVx + nFrmVx[ a2s[ pEl->elType ][ kVx ] ] ;
    
    /* pEl is still unrefined. */
  } 



  


  if ( verbosity > 3 )
    printf ( "       Reading %d boundary faces.\n", mBndFc ) ;

  /* Alloc a temporary list of boundary faces to be sorted and a list of
     boundary conditions using the upper bound given in the file header. */
  pUns->pBndFcVx = pBndFcVx =
    arr_malloc ( "pUns->pBndFcVx in read_ideas_grid", pUns->pFam,
                 mBndFc, sizeof ( bndFcVx_s ) );
  pUns->mBndFcVx = mBndFc ;


  for ( pVx = pChunk->Pvrtx, pBf = pBndFcVx ; pBf < pBndFcVx+mBndFc ; pBf++) {

    /* How many? */
    fscanf ( Fgrd, "%*d%*d%*d%*d%*d%d%*[^\n]", &mVxFc ) ;
    if ( mVxFc > 4 ) {
      printf ( " FATAL: found a %d-noded face in read_ideas_grid.\n", mVxFc ) ;
      return ( 0 ) ; }

    pBf->mVx = mVxFc ;
    /* Forming vertices. . */
    for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
      fscanf ( Fgrd, "%d", &nVx ) ;
      pBf->ppVx[kVx] = pVx + nVx ;
    }
  }


  if ( verbosity > 3 )
    printf ( "       Reading %d boundary conditions.\n", mBc ) ;
    
  findIdeasTag ( Fgrd, 2435 ) ;

  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    /* The IDEAS file has a line for each bc with the number of faces and nodes
       at the end. */
    fscanf ( Fgrd, "%*d%*d%*d%*d%*d%*d%*d%d%*[^\n]", &iNr ) ;
    nxtline ( Fgrd ) ;
    fscanf ( Fgrd, "%s", bcText ) ;

    if ( nBc < mBc-1 || strncmp( bcText, "UNNAMED", 7 ) ) {
      /* This is a genuine bc. */
      pBc = find_bc ( bcText, 1 ) ;

      for ( k = 0 ; k < iNr ; k++ ) {
        /* Boundary face and node entries come in groups of 4. The first
           integer is either a 7, node, or an 8, face. */
        fscanf ( Fgrd, "%d%d%d%d", iarr, iarr+1, iarr+2, iarr+3 ) ;
        if ( iarr[0] == 8 ) {
          /* This is a face. The second integer points to the face belonging
             to this bc. */
          pBndFcVx[ iarr[1]-mEl-1 ].pBc = pBc ;
        } 
      }
    }
  }

  /* Match boundary faces. */
  if ( !match_bndFcVx ( pUns ) ) {
    printf ( " FATAL: could not match boundary faces in read_ideas_grid.\n" ) ;
    return ( 0 ) ;
  }
 




  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;

  /* Make a new grid. */
  if ( !( pGrid = make_grid () ) )
    hip_err ( fatal, 0, "malloc for the linked list of grids"
	     " failed in read_uns_sat.\n" ) ;
  else {
    /* Put the chunk into Grids. */
    pGrid->uns.type = uns ;
    pGrid->uns.pUns = pUns ;
    pGrid->uns.mDim = mDim ;
    pGrid->uns.pVarList = &(pUns->varList) ;
    pUns->nr = pGrid->uns.nr ;
    pUns->pGrid = pGrid ;
  }
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  
  return ( 1 ) ;
}

/* Strip the last mCharStrip characters off a filename, tag on a different one. */
char *switchExt ( char *fileName, int mCharStrip, char *newExt ) {
  int len = strlen ( fileName ) ;

  /* Strip the last mCharStrip characters. */
  fileName[ len-mCharStrip ] = '\0' ;

  /* Overlapping strings is not legal. */
  char shortNm[LINE_LEN] ;
  strncpy ( shortNm, fileName, LINE_LEN-1 ) ;

  snprintf ( fileName, LINE_LEN-1, "%s%s", shortNm, newExt ) ;
  return ( fileName ) ;
}


/******************************************************************************

  read_ensight_master:
  .
  
  Last update:
  ------------
  25May09; use full char length of LINE_LEN in r1_fopen.
  5Apr09; call check_var_name
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int read_ensight_master ( FILE* Fmstr, uns_s *pUns, int saveNr ) {

  /* There are by default 4 * at the end of the filename. */
  const int mCharStrip = 4, mEqFlow = 5, mDim = pUns->mDim ;

  FILE *Fsol ;
  char text[TEXT_LEN], pressFile[TEXT_LEN], densFile[TEXT_LEN], velFile[TEXT_LEN],
    specFile[TEXT_LEN] ;
  int mEq, mUnkn, mSpec=0, mVx, mEl=pUns->mElemsNumbered, kVar, mVxEl, i, *pmCon, kVx ;
  chunk_struct *pChunk ;
  double *pUnEl, *pUE, *pUV ;
  elem_struct *pEl ;
  vrtx_struct *pVx, **ppVx ;

  pressFile[0] = densFile[0] = velFile[0] = specFile[0] = '\0' ;

  /* Scan for VARIABLES section. */
  while ( fgets ( text, 9, Fmstr ) ) {
    if ( text[ strlen(text)-1 ] != '\n' ) 
      /* There is trailing junk on the line, skip it. */
      nxtline ( Fmstr ) ;
    
    if ( !strncmp ( text, "VARIABLE", 8 ) )
      break ; 
    else if ( feof ( Fmstr ) )
      return ( 0 ) ;
  }

  /* Sample lines:
     scalar per element: 1 Pression                 chr.Pression****
     vector per element: 1 VitesseX                 chr.VitesseX****

     Pick the scalar/vector, pick the number per element, pick the 
     label, pick the filename. */
  
  while ( 1 ) {
    fgets ( text, 7, Fmstr ) ;
    if ( !strncmp ( text, "scalar", 6 ) || !strncmp ( text, "vector", 6 ) ) {
      fscanf ( Fmstr, "%*s%*s%d", &mUnkn ) ;
      if ( mUnkn != 1 ) {
        printf ( " FATAL: cannot handle %d unknowns per element"
                 " in read_ensight_master.\n", mUnkn ) ;
        return ( 0 ) ;
      }

      /* Pression, brune ou blonde? */
      fscanf ( Fmstr, "%s", text ) ;
      if ( !strncmp ( text, "Pression", 5 ) )
         fscanf ( Fmstr, "%s", pressFile ) ;
      else if ( !strncmp ( text, "masse_vol", 4 ) )
         fscanf ( Fmstr, "%s", densFile ) ;
      else if ( !strncmp ( text, "Vitesse", 4 ) )
         fscanf ( Fmstr, "%s", velFile ) ;
      else if ( !strncmp ( text, "Especes", 4 ) )
         fscanf ( Fmstr, "%s", specFile ) ;

      nxtline ( Fmstr ) ;
    }
    else 
      break ;
  }


     
  if ( !pressFile[0] ) {
    printf ( " FATAL: no pressure file in read_ensight_master.\n" ) ;
    return ( 0 ) ; }
  if ( !densFile[0] ) {
    printf ( " FATAL: no density file in read_ensight_master.\n" ) ;
    return ( 0 ) ; }
  if ( !velFile[0] ) {
    printf ( " FATAL: no velocity file in read_ensight_master.\n" ) ;
    return ( 0 ) ; }

  /* Build the new extension. */
  sprintf ( text, "%4.4d", saveNr ) ;

  if ( specFile[0] ) {
    /* Species. Unfortunately, we don't know how many components. 
       Open the file, count.*/
    prepend_path ( specFile ) ;
    switchExt ( specFile, mCharStrip, text ) ;
    if ( !( Fsol = r1_fopen ( specFile, LINE_LEN, "r" ) ) ) {
      printf ( " FATAL: could not open species file %s\n", specFile ) ;
      return ( 0 ) ; }

    /* Start with skipping four lines. */
    for ( i = 0 ; i < 4 ; i++ )
      nxtline ( Fsol ) ;

    /* Check how many species there are. */
    for ( mSpec = 0 ; ; mSpec++ ) {
      for ( i = 1 ; i <= mEl ; i++ ) {
        fscanf ( Fsol, "%*f" ) ;
        fscanf ( Fsol, "%*[^\n]" ) ;
      }
      if ( feof ( Fsol ) )
        break ;
    }

    fclose ( Fsol ) ;
  }

  /* Unknowns at the cell centers. */
  mEq = mEqFlow + mSpec + mDim+1 ;
  pUE = pUnEl = arr_malloc ( "pUnEl in read_ensight_master", 
                             pUns->pFam, (mEl+1)*mEq, sizeof( double ) ) ;



  /* Pression. */
  kVar = 4 ;
  prepend_path ( pressFile ) ;
  switchExt ( pressFile, mCharStrip, text ) ;
  if ( !( Fsol = r1_fopen ( pressFile, LINE_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open pressure file %s\n", pressFile ) ;
    return ( 0 ) ; }

  /* Start with skipping four lines. */
  for ( i = 0 ; i < 4 ; i++ )
    nxtline ( Fsol ) ;

  for ( i = 1 ; i <= mEl ; i++ )
    fscanf ( Fsol, "%lf", pUnEl + i*mEq + kVar ) ;

  if ( feof ( Fsol ) ) {
    printf ( " FATAL: attempt to read beyond end of pressure file.\n" ) ;
    return ( 0 ) ; }

  fclose ( Fsol ) ;




  /* Density. */
  kVar = 0 ;
  prepend_path ( densFile ) ;
  switchExt ( densFile, mCharStrip, text ) ;
  if ( !( Fsol = r1_fopen ( densFile, LINE_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open density file %s\n", densFile ) ;
    return ( 0 ) ; }

  /* Start with skipping four lines. */
  for ( i = 0 ; i < 4 ; i++ )
    nxtline ( Fsol ) ;

  for ( i = 1 ; i <= mEl ; i++ )
    fscanf ( Fsol, "%lf", pUnEl + i*mEq + kVar ) ;

  if ( feof ( Fsol ) ) {
    printf ( " FATAL: attempt to read beyond end of density file.\n" ) ;
    return ( 0 ) ; }

  fclose ( Fsol ) ;



  /* Velocity. */
  prepend_path ( velFile ) ;
  switchExt ( velFile, mCharStrip, text ) ;
  if ( !( Fsol = r1_fopen ( velFile, LINE_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open velocity file %s\n", velFile ) ;
    return ( 0 ) ; }

  /* Start with skipping four lines. */
  for ( i = 0 ; i < 4 ; i++ )
    nxtline ( Fsol ) ;

  /* First u, then v, w. */
  for ( kVar = 1 ; kVar < 4 ; kVar ++ ) { 
    for ( i = 1 ; i <= mEl ; i++ )
      fscanf ( Fsol, "%lf", pUnEl + i*mEq + kVar ) ;
  }

  if ( feof ( Fsol ) ) {
    printf ( " FATAL: attempt to read beyond end of velocity file.\n" ) ;
    return ( 0 ) ; }

  fclose ( Fsol ) ;




  /* Species. */
  if ( specFile[0] ) {
    prepend_path ( specFile ) ;
    switchExt ( specFile, mCharStrip, text ) ;
    if ( !( Fsol = r1_fopen ( specFile, LINE_LEN, "r" ) ) ) {
      printf ( " FATAL: could not open species file %s\n", specFile ) ;
      return ( 0 ) ; }

    /* Start with skipping four lines. */
    for ( i = 0 ; i < 4 ; i++ )
      nxtline ( Fsol ) ;

    /* First all values of species 1, ... */
    for ( kVar = mEqFlow ; kVar < mEqFlow+mSpec ; kVar ++ ) { 
      for ( i = 1 ; i <= mEl ; i++ )
        fscanf ( Fsol, "%lf", pUnEl + i*mEq + kVar ) ;
    }

    if ( feof ( Fsol ) ) {
      printf ( " FATAL: attempt to read beyond end of species file.\n" ) ;
      return ( 0 ) ; }

    fclose ( Fsol ) ;
  }





  /* Unknowns at the nodes. */
  pUns->varList.mUnknFlow = 5 ;
  pUns->varList.mUnknowns = pUns->varList.mUnknFlow + mSpec ;
  pUns->varList.varType = prim ;
  pChunk = pUns->pRootChunk ;
  mVx = pUns->mVertsNumbered ;
  pChunk->Punknown = arr_calloc ( "pChunk->Punknown in read_ensight_master", 
                                  pUns->pFam, (mVx+1)*(mEq+mDim+1), sizeof( double ) ) ;
  pmCon = arr_calloc ( "pmCon in read_ensight_master", 
                       pUns->pFam, mVx+1, sizeof( int ) ) ;


  /* Set the vrtx pointer. */ 
  for ( pVx = pChunk->Pvrtx+1, pUV = pChunk->Punknown+mEq+mDim+1 ; 
        pVx <= pChunk->Pvrtx+mVx ; pVx++, pUV += mEq+mDim+1 )
    pVx->Punknown = pUV ;


  /* Scatter from cell to nodes. */
  for ( pEl = pChunk->Pelem+1 ; pEl <= pChunk->Pelem+mEl ; pEl++ ) {
    mVxEl = elemType[ pEl->elType ].mVerts ;
    ppVx = pEl->PPvrtx ;
    pUE = pUnEl + (pEl-pChunk->Pelem)*mEq ;

    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
      pUV = ppVx[kVx]->Punknown ;
      pmCon[ ppVx[kVx]->number ]++ ;

      for ( kVar = 0 ; kVar < mEq ; kVar++ ) 
        pUV[kVar] += pUE[kVar] ;
    }
  }

  /* Divide by the number of referencing cells. */
  for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx+mVx ; pVx++ ) {
    pUV = pVx->Punknown ;
    for ( kVar = 0 ; kVar < mEq ; kVar++ )
      pUV[kVar] /= pmCon[ pVx->number ] ;
  }

  arr_free ( pUnEl ) ;
  arr_free ( pmCon ) ;



  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 

  return ( 1 ) ;
}



/******************************************************************
 
 read_uns_saturne.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an unstructured .cfdrc file for cpre.

 Last update:
 ------------
  19Dec17; new interface to make_uns.
 25May09; use full char length of LINE_LEN in r1_fopen.
 11Jun03; derived from read_uns_cfdrc

 Input:
 ------
 gridFile:   grid file in I-Deas .unv
 masterFile: masterfile in ensight gold for various state variables.

 Changes to:
 -----------


 Returns:
 --------
 
*/

int read_uns_saturne ( char *gridFile, char *masterFile, int saveNr ) {
  
  FILE *Fgrd, *Fmstr=NULL ;
  uns_s *pUns ;

  /* Allocate an unstructured grid. */
  if ( !( pUns = make_uns ( NULL ) ) )
    hip_err ( fatal, 0, "failed to alloc an unstructured grid"
              " in read_uns_saturne.\n" ) ;


  prepend_path ( gridFile ) ;
  if ( !( Fgrd = r1_fopen ( gridFile, LINE_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open %s\n", gridFile ) ;
    return ( 0 ) ; }

  if ( !read_ideas_grid ( Fgrd, pUns ) ) {
    printf ( " FATAL: failed to read grid in read_uns_saturne.\n" ) ;
    return ( 0 ) ; }
  
  fclose ( Fgrd ) ;
  

  if ( masterFile[0] != '\0' ) {
    prepend_path ( masterFile ) ;
    if ( !( Fmstr = r1_fopen ( masterFile, LINE_LEN, "r" ) ) ) {
      printf ( " FATAL: could not open %s\n", masterFile ) ;
      printf ( "        no solution read.\n" ) ;
      return ( 1 ) ; }
  }  


  if ( Fmstr ) {
    /* There is a solution. */
    if ( !read_ensight_master ( Fmstr, pUns, saveNr ) ) {
      printf ( " FATAL: failed to read grid in read_uns_saturne.\n" ) ;
      printf ( "        no solution read.\n" ) ;
      return ( 1 ) ; }
    fclose ( Fmstr ) ;
  }


  return ( 1 ) ;
}


