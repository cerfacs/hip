/*
  file_name.c:
*/

/*! short descr. for doxygen
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  ; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;

// make interpolation weights between mixing lines more accurate than h.
#define RH_TOL (0.01)

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_radHeight_3d:
*/
/*! work out radius from axis (axial machine) or height (radial machine.)
 *
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

double sp_radHeight_3d ( const sp_geo_type_e spGeoType, double *pCoor ) {

  double retVal ;
  
  if ( spGeoType == sp_const_rx )
    /* Radial machine. MP varies in the x-dir. */
    retVal = pCoor[0] ;
  else if ( spGeoType == sp_const_ry )
    /* Radial machine. MP varies in the y-dir. */
    retVal = pCoor[1] ;
  else if ( spGeoType == sp_const_rz )
    /* Radial machine. MP varies in the z-dir. */
    retVal = pCoor[2] ;
  else if ( spGeoType == sp_const_x )
    /* Axial machine. MP varies in radius from x-axis. */
    retVal = sqrt( pCoor[1]*pCoor[1] + pCoor[2]*pCoor[2] ) ;
  else if ( spGeoType == sp_const_y )
    /* Axial machine. MP varies in radius from y-axis. */
    retVal = sqrt( pCoor[0]*pCoor[0] + pCoor[2]*pCoor[2] ) ;
  else if ( spGeoType == sp_const_z )
    /* Axial plane. MP varies in radius from z-axis. */
    retVal = sqrt( pCoor[1]*pCoor[1] + pCoor[0]*pCoor[0] ) ;
  else {
    hip_err ( fatal, 0, "undefined geometric type for sliding/mixing plane"
              "in sp_radHeight_3d." ) ;
    retVal = 0 ; // To pacify compiler.
  }
  return ( retVal ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_calc_vx_weight_mixing_lines:
*/
/*! For each node on an interface bc, compute interpolation weights between mixing lines
 *
 * Note that this maps mixing plane nodes to current node numbers, so must be recomputed
 * if the numbering changes from the numbering-invariant basic pointer data.
 */

/*
  
  Last update:
  ------------
  25Jul21: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void sp_free_vx_weight_mixing_lines ( const uns_s *pUns, slidingPlanePair_s *pSpP ) {
  
  int kSide ;
  slidingPlaneSide_s *pSpSThis ;
  for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
    pSpSThis  = pSpP->side[kSide] ;
    arr_free ( pSpSThis->pnVxMP2nVx ) ;
    arr_free ( pSpSThis->pnVxMP2lineLower ) ;
    arr_free ( pSpSThis->pwtnVxMPlineLower ) ;
  }
  return ;
}

ret_s sp_calc_vx_weight_mixing_lines ( uns_s *pUns, slidingPlanePair_s *pSpP ) {
#undef FUNLOC
#define FUNLOC "in sp_calc_vx_weight_mixing_lines"

  /* Throw it all away. */
  sp_free_vx_weight_mixing_lines ( pUns, pSpP ) ;
  
  int kSide ;
  slidingPlaneSide_s *pSpSThis, *pSpSOther ;
  int nVxMP ;
  chunk_struct *pChunk ;
  vrtx_struct *pVxBeg, *pVx, *pVxEnd ;
  int nBeg, nEnd ;
  double rh, drh, absdrh = TOO_MUCH, rhLower ;
  int kLine ;
  for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
    pSpSThis  = pSpP->side[kSide] ;
    pSpSOther = pSpP->side[1-kSide] ;

    // Mark all verts on this side, count.
    const int doReset = 1 ;
    set_vx_mark_k_pbc ( pUns, pSpSThis->pBc, 0, doReset ) ;
    pSpSThis->mVxMP = count_vx_mark ( pUns, 1,0,0 ) ;
    nVxMP = 0 ;

    // Allocate.
    pSpSThis->pnVxMP2nVx = arr_malloc ( "pSpSThis->pnVxMP2nVx "FUNLOC".", pUns->pFam,
                                       pSpSThis->mVxMP , sizeof ( *pSpSThis->pnVxMP2nVx ) ) ;
    pSpSThis->pnVxMP2lineLower = arr_malloc ( "pSpSThis->pwtnVxMP "FUNLOC".", pUns->pFam,
                                             pSpSThis->mVxMP , sizeof ( *pSpSThis->pnVxMP2lineLower ) ) ;
    pSpSThis->pwtnVxMPlineLower = arr_malloc ( "pSpSThis->pwtnVxMP "FUNLOC".", pUns->pFam,
                                              pSpSThis->mVxMP , sizeof ( *pSpSThis->pwtnVxMPlineLower ) ) ;


    // Loop over all marked verts
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( vx_has_markN ( pVx, 0 ) ) {
          // Add
          pSpSThis->pnVxMP2nVx[nVxMP] = pVx->number ;

          // Calculate its rh
          rh = sp_radHeight_3d ( pSpSThis->spGeoType, pVx->Pcoor ) ;

          // Find the two bracketing lines
          for ( kLine = 0 ; kLine < pSpSOther->mLines ; kLine++ ) {
            drh = pSpSOther->prh[kLine] - rh ;
            absdrh = ABS( drh ) ;
            if ( absdrh <= RH_TOL*Grids.epsOverlap || drh > 0. ) {
              // exact match or upper bracket.
              pSpSThis->pnVxMP2lineLower[nVxMP] = kLine ;
              break ;
            }
          }
          if ( kLine == pSpSOther->mLines ) {
            // Geometry mismatch?
            if ( ABS( rh-pSpSOther->prh[kLine]) > RH_TOL*Grids.epsOverlap ) {
                sprintf ( hip_msg, "Cannot bracket node %"FMT_ULG", " 
                          "rh %g with upper line %d, rh %g. Planes don't match.",
                          pVx->number, rh, kLine, pSpSOther->prh[kLine] ) ;
                hip_err ( warning, 0, hip_msg ) ;
              }
            // Snap to the highest line, record the lower line
            pSpSThis->pnVxMP2lineLower[nVxMP] = kLine-1 ;
            rhLower = pSpSOther->prh[kLine-1] ;
            // Snap to the line.
            pSpSThis->pwtnVxMPlineLower[nVxMP] = 0.0 ;
          }
          // Add interpolation weight.
          else if ( absdrh <= RH_TOL*Grids.epsOverlap )
            pSpSThis->pwtnVxMPlineLower[nVxMP] = 1.0 ;
          // Vx is on the mixing line
          else if ( pSpSOther->prh[kLine-1] < rh && kLine>0 ) {
            // Bracketed, record the lower line
            pSpSThis->pnVxMP2lineLower[nVxMP] = kLine-1 ;
            rhLower = pSpSOther->prh[kLine-1] ;
            pSpSThis->pwtnVxMPlineLower[nVxMP] =
              ( rh-rhLower )/( pSpSOther->prh[kLine]-rhLower ) ;
          }
          else {
            // Geometry mismatch?
            if ( ABS( rh-pSpSOther->prh[kLine]) > RH_TOL*Grids.epsOverlap ) {
                sprintf ( hip_msg, "Cannot bracket node %"FMT_ULG", " 
                          "rh %g with lower line %d, rh %g. Planes don't match. Snapping to nearest line.",
                          pVx->number, rh, kLine, pSpSOther->prh[kLine] ) ;
                hip_err ( warning, 0, hip_msg ) ;
              }
            // Bracketed, record the lower line
            pSpSThis->pnVxMP2lineLower[nVxMP] = kLine ;
            rhLower = pSpSOther->prh[kLine] ;
            // Snap to the line.
            pSpSThis->pwtnVxMPlineLower[nVxMP] = 1.0 ;
          }
          nVxMP++ ;
        }
    }
    if ( nVxMP != pSpSThis->mVxMP ) {
      sprintf ( hip_msg, "Miscount in "FUNLOC" for side %d:, expected %"FMT_ULG", found %d vertices.",
                kLine, pSpSThis->mVxMP, nVxMP ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }
                                      

                                      
  ret_s ret = ret_success () ;
  return ( ret ) ;
}





/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_init_spLine:
*/
/*! Init a sliding/mixing plane line to zero.
 *
 */

/*
  
  Last update:
  ------------
  3Dec19: conceived.
  
  Changes To:
  -----------
  *psp: the pointer to the line.
  
*/

void sp_init_spLine ( spLineX_s *pspL ) {

  pspL->mEgX = 0 ;
  pspL->mEgXAlloc = 0 ;
  pspL->egX = NULL ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_set_arc_ref ( spGeoType, pSp->xArcRef ) ;
  sp_set_arc_ref_3d:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  7Apr20; intro other planes.
  6Dec19: conceived.
  

  Input:
  ------
  spGeoType: type of sliding/mixing plane: const_r or const_x.

  Output:
  -------
  xArcRef: vector that defines arc len = 0.
  
*/

void sp_set_arc_ref_3d ( const sp_geo_type_e spGeoType, double *xArcRef ) {

  double x[MAX_DIM] = {0.} ;
  if ( spGeoType == sp_const_rx ) {
    /* Radial machine cylindrical around x,
       Arc len relative to y-axis*/
    x[1] = 1. ;
  }
  else if ( spGeoType == sp_const_ry ) {
    /* Radial machine cylindrical around y,
       Arc len relative to z-axis*/
    x[2] = 1. ;
  }
  else if ( spGeoType == sp_const_rz ) {
    /* Radial machine cylindrical around z,
       Arc len relative to x-axis*/
    x[0] = 1. ;
  }
  else if ( spGeoType == sp_const_x ) {
    /* Axial plane around x. Arc len relative to y-axis*/
    x[1] = 1. ;
  }
  else if ( spGeoType == sp_const_y ) {
    /* Axial plane around y. Arc len relative to z-axis*/
    x[2] = 1. ;
  }
  else if ( spGeoType == sp_const_z ) {
    /* Axial plane around z. Arc len relative to x-axis*/
    x[0] = 1. ;
  }
  else {
    hip_err ( fatal, 0, "undefined geometric type for sliding/mixing plane"
              "in sp_set_arc_ref_3d." ) ;
  }
  vec_copy_dbl ( x, 3, xArcRef ) ;
  
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_line_pos_coor_per:
*/
/*! Determine the radii/heights for sliding/mixing plane lines as those of 
 *  all nodes that are on both the lower periodic and the nBc mp bnd. 
 *
 */

/*
  
  Last update:
  ------------
  8jul24; fix bug with resetting marks for vx on per bc, fails for multiple per bc
          intro kMrkFc=0, kMrkPer=2
  7jul24; release vx mark if no nodes found.
  5Apr20; rework data-structure to use a single slidingPlaneSide.
  3Dec19; rename to sp
  28Nov19; add rh params to interface.
  20Sep19: conceived.
  

  Input:
  ------
  pUns: grid
  kBc: number of mixing plane patch
  spGeoType: geometric type of interface.

    
  Returns:
  --------
  number of lines
  
*/

int sp_line_pos_coor_per ( uns_s *pUns, slidingPlaneSide_s *pSpS, const int kBc,
                           const sp_geo_type_e spGeoType ) {

  const int kMrkFc = 0 ;
  reserve_vx_markN ( pUns, kMrkFc, "sp_line_pos_coor_per" ) ;
  reset_vx_markN ( pUns, kMrkFc ) ;
  const int kMrkPer = 2 ;
  reserve_vx_markN ( pUns, kMrkPer, "sp_line_pos_coor_per" ) ;
  reset_vx_markN ( pUns, kMrkPer ) ;

  /* Mark all vx on nBc with mark0 */
  const int dontReset = 0 ;
  /* kBc is the number in pUns->ppBc, need the global bc number here. */
  set_vx_mark_k_nbc ( pUns, pUns->ppBc[kBc]->nr, kMrkFc, dontReset ) ;

  /* Mark all vx on l periodic bcs with mark kMrkPer. */
  int iBc ;
  chunk_struct *pChunk ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;
  int mLines = 0;
  for ( iBc = 0 ; iBc < pUns->mBc ; iBc++ ) {
    if ( bc_is_l ( pUns->ppBc[iBc] ) ) {
      if ( kBc == iBc ) {
        hip_err ( warning, 0,
                  "mixing plane bc cannot be periodic in sp_line_pos_coor_per" );
        return ( 0 ) ;
      }
      else {
        set_vx_mark_k_nbc ( pUns, pUns->ppBc[iBc]->nr, kMrkPer, dontReset ) ;
      }
    }
  }

  /* Count the shared vx. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( vx_has_markN ( pVx, 0 ) &&
           vx_has_markN ( pVx, 2 ) )
        mLines++ ;
  }

  if ( !mLines ) {
    hip_err ( warning, 1, "no nodes were found that are on both the\n"
              " sliding/mixing plane and a perodic bc.\n"
              " Check your bc choices." ) ;
    release_vx_markN ( pUns, kMrkFc ) ;
    release_vx_markN ( pUns, kMrkPer ) ;
    return ( 0 ) ;
  }


  /* Allocate the line containers. */
  pSpS->spGeoType = spGeoType ;
  pSpS->mLines = mLines ;
  pSpS->prh =
    arr_calloc ( "prh in sp_line_pos_coor_per",
                 pUns->pFam, mLines, sizeof( *(pSpS->prh) ) ) ;
  pSpS->pspLine =
    arr_calloc ( "pspLine in sp_line_pos_coor_per",
                 pUns->pFam, mLines, sizeof( (*pSpS->pspLine) ) ) ;

  int kLine = 0 ;
  for ( kLine = 0 ; kLine < mLines ; kLine++ )
    sp_init_spLine ( pSpS->pspLine + kLine ) ;


  /* Set reference vec for zero arc length. */
  sp_set_arc_ref_3d ( spGeoType, pSpS->xArcRef ) ;

  /* List. */
  pChunk = NULL ;
  kLine = 0 ;
  egX_s *pspLine ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( vx_has_markN ( pVx, kMrkFc ) &&
           vx_has_markN ( pVx, kMrkPer ) ) {
        /* Start a new line. */
        pSpS->prh[kLine] = sp_radHeight_3d ( spGeoType, pVx->Pcoor ) ;
        kLine++ ;
      }
  }

  /* Sort for radius/height. */
  qsort ( pSpS->prh, mLines, sizeof ( double ), cmp_double ) ;


  release_vx_markN ( pUns, kMrkFc ) ;
  release_vx_markN ( pUns, kMrkPer ) ;
  return ( mLines ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_arc_r
*/
/*! compute arclength of an intersection point on a radius around z
 *
 */

/*
  
  Last update:
  ------------
  7Apr2; consider const_z
  : conceived.
  

  Input:
  ------
  spGeoType: const_rxyz (radial machine/plane) or const_xyz (axial machine)
  xArcRef: compute arc length relative to this starting point on the line.
  xyzInt: coordinates of the edge/line intersection to compute arc len for.
    
  Returns:
  --------
  arc length along the circular arc from xArcRef to xyzInt.
  
*/

double sp_arc_len_3d ( const sp_geo_type_e spGeoType,
                       const double xArcRef[MAX_DIM],
                       double xyzInt[3] ) {
  double t ;
  double x0[MAX_DIM] ={0}, x[MAX_DIM] ;

  int k0, k1 ;
  if ( spGeoType == sp_const_x ||
       spGeoType == sp_const_rx ) {
    /* Arc in y,z plane around x, zero is y-axis. */
    vec_copy_dbl ( xArcRef+1, 2, x0 ) ;
    vec_copy_dbl ( xyzInt+1, 2, x ) ;
    k0 = 1 ;
    k1 = 2 ;
  }
  else if ( spGeoType == sp_const_y ||
            spGeoType == sp_const_ry ) {
    /* Arc in z,x plane around y, zero is z-axis. */
    vec_copy_dbl ( xArcRef+2, 2, x0 ) ;
    vec_copy_dbl ( xyzInt+2, 2, x ) ;
    k0 = 2 ;
    k1 = 0 ;
  }
  else  if ( spGeoType == sp_const_z ||
             spGeoType == sp_const_rz ) {
    /* Arc in x,y plane around z, zero is x-axis. */
    vec_copy_dbl ( xArcRef, 2, x0 ) ;
    vec_copy_dbl ( xyzInt, 2, x ) ;
    k0 = 0 ;
    k1 = 1 ;
  }
  else {
    hip_err ( fatal, 0, "undefined geometric type for sliding/mixing plane"
              "in  sp_arc_len_3d." ) ;
    return ( -999. ) ; // to pacify the compiler. 
  }

  x0[0] = xArcRef[k0] ;
  x0[1] = xArcRef[k1] ;
  x[0] = xyzInt[k0] ;
  x[1] = xyzInt[k1] ;
 
  double r0 = vec_norm_dbl ( x0, 2 ) ;
  double r = vec_norm_dbl ( x, 2 ) ;

  /* JDM: do we want to allow non-normal mixing/sliding planes? Why not?
     Then this is not relevant. 
     Otherwise, to force cylindrical const_r scale xArcRef with suitable
     radius. 
  if ( ABS(r-r0) > Grids.epsOverlap ) {
    sprintf ( hip_msg, "sp_arc_len_3d: mixing plane radius at start of line is %g,"
              " at %g, %g, %g it is %g!", r0, xyzInt[0], xyzInt[1], xyzInt[2], r ) ;
    hip_err ( warning, 2, hip_msg ) ;
    } */

  double sp, cp[MAX_DIM] ;
  sp = scal_prod_dbl ( x0, x, 2 ) ;
  cross_prod_dbl ( x0, x, 2, cp ) ;
  double alRad = acos(sp) ;

  if ( cp[0] >= 0. && sp >= 0. ) {
    /* First quadrant. */
    t = r*alRad ;
  }
  else if ( cp[0] < 0. && sp > 0. ) {
    /* Second quadrant. */
    t = r*(PI-alRad) ;
  }
  if ( cp[0] < 0. && sp < 0. ) {
    /* Third quadrant. */
    t = r*(PI+alRad) ;
  }
  else {
    /* Fourth quadrant. */
    t = r*(-alRad ) ;
  }

  return ( t ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

   sp_cmp_egX_t:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int sp_cmp_egX_t ( const void *pX0, const void *pX1 ) {
  double diff =
    (( egX_s* )pX0 )->t -
    (( egX_s* )pX1 )->t ;
    
  if ( diff > 0. )
    return ( 1 ) ;
  else if ( diff < 0. )
    return ( -1 ) ;
  else
    return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_fill_egX:
*/
/*! fill an intersection edge with a single fully matching end node.
 *
 *
 */

/*
  
  Last update:
  ------------
  5Dec19: conceived.
  

  Input:
  ------
  pVx: intersection vertex

  Changes To:
  -----------
  pEgX
  
*/

void sp_fill_egX_one_vx  ( egX_s *pEgX, vrtx_struct *pVx ) {
  
  pEgX->pVx[0] = pVx ;
  pEgX->pVx[1] = pVx ;
  vec_copy_dbl ( pVx->Pcoor, 3, pEgX->xX ) ;
  pEgX->wt[0] = 1. ;
  pEgX->wt[1] = 0. ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_fill_egX:
*/
/*! fill an intersection edge between two vertices
 *
 *
 */

/*
  
  Last update:
  ------------
  5Dec19: conceived.
  

  Input:
  ------
  pVx0,1: intersection vertices
  dist0,1: distances from either vet4x to intersection between them.

  Changes To:
  -----------
  pEgX
  
*/


void sp_fill_egX_two_vx  ( egX_s *pEgX,
                           vrtx_struct *pVx0, double dist0,
                           vrtx_struct *pVx1, double dist1 ) {
  pEgX->pVx[0] = pVx0 ;
  pEgX->pVx[1] = pVx1 ;
  double sumDrh = dist0 + dist1 ;
  pEgX->wt[0] = dist1/sumDrh ;
  pEgX->wt[1] = dist0/sumDrh ;
  vec_ini_dbl ( 0., 3, pEgX->xX ) ;
  vec_add_mult_dbl ( pEgX->xX, pEgX->wt[0], pVx0->Pcoor, 3, pEgX->xX ) ;
  vec_add_mult_dbl ( pEgX->xX, pEgX->wt[1], pVx1->Pcoor, 3, pEgX->xX ) ;

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

   sp_list_lineX:
*/
/*! Count/Compute the vertices and their weights for the integration along a line.
 *
 */

/*
  
  Last update:
  ------------
  7Sep20; fix bug with missing update of pEgX after realloc of ppEgXtmp.
  7Apr20; rename to sp_make_lineX
  22Sep19: conceived.
  

  Input:
  ------
  pUns: grid
  kBc: pos of mixing plane bc in pUns->ppBc.
  spGeoType: geom type of the interface.
  rh: radius/height of the line.

  Changes To:
  -----------
  

  Output:
  -------
  xArcRef: arclen of each intersection, against x/y plane for r_const, z_const and x_const.
  pspLine: list of intersections.
    
  Returns:
  --------
  number of intersections on bnd nBc with the line.
  
*/

ulong_t sp_make_lineX ( uns_s *pUns, const int kBc, const sp_geo_type_e spGeoType,
                        const double rh,
                        int *pmEgXtmp, egX_s **ppEgXtmp,
                        const double xArcRef[MAX_DIM],
                        spLineX_s *pspLine ) {
  
  bndPatch_struct *pBp = NULL ;
  bndFc_struct *pBfBeg, *pBfEnd, *pBf ;
  const elem_struct *pElem ;
  vrtx_struct **ppVrtx ;
  int nFace ;
  const faceOfElem_struct *pFoE ;
  int mVxFc ;
  const int *kVxFace ;
  int kVx, kVx1 ;
  vrtx_struct *pVx0, *pVx1 ;
  double drh0, adrh0, drh1, adrh1, sumDrh ;
  const double rhTol = RH_TOL*Grids.epsOverlap ;
  int k ;

  /* Reset to beginning of work array. */
  ulong_t mEgX = 0 ;
  egX_s *pEgX = *ppEgXtmp, *pEgX1 ;
  
  
  while ( loop_bndFaces_bc ( pUns, kBc, &pBp,  &pBfBeg, &pBfEnd) )
    for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ ){
      pElem = pBf->Pelem ;
      ppVrtx = pElem->PPvrtx ;
      nFace = pBf->nFace ;
      pFoE = elemType[ pElem->elType ].faceOfElem + nFace ;
      mVxFc = pFoE->mVertsFace ;
      kVxFace = pFoE->kVxFace ;
      
      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
        pVx0 = ppVrtx[ kVxFace[kVx] ] ;
        // cyclic continuation.
        kVx1 = ( kVx+1 == mVxFc ? 0 : kVx+1 ) ;
        pVx1 = ppVrtx[ kVxFace[kVx1] ] ;

        /* Order such that pVx0 is the lower numbered one. */
        if ( pVx1->number < pVx0->number ) {
          pVx1 = pVx0 ;
          pVx0 = ppVrtx[ kVxFace[kVx1] ] ;
        }

        /* Look at each edge twice and filter later, as an edge
           may be on the edge of the bnd patch, and hence be present only once. */
        drh0 = rh-sp_radHeight_3d ( spGeoType, pVx0->Pcoor ) ;
        adrh0 = ABS(drh0) ;
        drh1 = rh-sp_radHeight_3d ( spGeoType, pVx1->Pcoor ) ;
        adrh1 = ABS(drh1) ;
        if ( adrh0 < rhTol ||
             adrh1 < rhTol ||
             drh0*drh1 < 0. ) {
          /* intersection or one of the edge endpoints are on the line. */

          /* There may be two intersections added, plus a NULL vx pad at the end. */
          if ( mEgX + 3 >= *pmEgXtmp ) {
            (*pmEgXtmp) *=  REALLOC_FACTOR ;
            *ppEgXtmp = arr_realloc ( "ppEgXtmp in sp_list_lineX", pUns->pFam,
                                      *ppEgXtmp, *pmEgXtmp, sizeof ( **ppEgXtmp ) ) ;
            pEgX = *ppEgXtmp + mEgX ;
          }

          /* Add. */
          pEgX1 = NULL ;
          if ( adrh0 < rhTol && adrh1 < rhTol ) {
            /* Both end vx on the line, edge is on the line. Two intersections. */
            sp_fill_egX_one_vx ( pEgX, pVx0 ) ;
            pEgX1 = pEgX+1 ;
            sp_fill_egX_one_vx ( pEgX1, pVx1 ) ;
          }
          else if ( adrh0 < rhTol ) {
            sp_fill_egX_one_vx ( pEgX, pVx0 ) ;
          }
          else if ( adrh1 < rhTol ) {
            sp_fill_egX_one_vx ( pEgX, pVx1 ) ;
          }
          else if ( drh0*drh1 < 0. ) {
            sp_fill_egX_two_vx  ( pEgX,
                                  pVx0, adrh0,
                                  pVx1, adrh1 ) ;
          }
          else {
            hip_err ( fatal, 0,
                      "there should have been an intersection in sp_list_lineX.");
          }
          pEgX->t = sp_arc_len_3d ( spGeoType, xArcRef, pEgX->xX ) ;
          mEgX++ ;
          pEgX++ ;
              
          if ( pEgX1 ) {
            /* one edge had two listed nodes. */
            pEgX1->t = sp_arc_len_3d ( spGeoType, xArcRef, pEgX1->xX ) ;
            pEgX++ ;
            mEgX++ ;
          }
        } // if ( adrh0 < rhTol ||

      } // for ( kVx
      
    }  // for ( pBf

  /* We compare to the pEgX+1, last pEgX could be a single occurence, so we need to make
     the one after it distinct. Sufficient array size was ensured before. */
  pEgX->pVx[0] = pEgX->pVx[1] = NULL ;

  
  int mEgXDel = 0 ;
  egX_s *pEgXLast = pEgX, *pEgXNext ;
  /* Sort by arclen. */
  qsort ( *ppEgXtmp, mEgX, sizeof( **ppEgXtmp ), sp_cmp_egX_t ) ;

  /* Remove duplicates. */
  int mX =  0 ;
  for ( pEgX = *ppEgXtmp ; pEgX < pEgXLast-1 ; pEgX = pEgXNext ) {
    mX++ ;
    pEgXNext = pEgX+1 ;
    while ( pEgX->pVx[0] == pEgXNext->pVx[0] &&
            pEgX->pVx[1] == pEgXNext->pVx[1] ) {
      /* Nodes have been ordered to have pVx in same pos viewed from either side,
         there can be only one intersection per edge. 
         Hence this is a duplicate. Move all remaining intersections one down. */
      pEgXNext++ ;
      mEgXDel++ ;
    }
  }

  
  /* Allocate. */
  pspLine->egX = arr_malloc ( "pspLine->egX in sp_make_lineX", pUns->pFam,
                              mX, sizeof( *pspLine->egX ) ) ;
  pspLine->mEgXAlloc = mX ;
  pspLine->mEgX = mX ;


  /* Copy. */
  egX_s *pEgXCp = pspLine->egX ;
  for ( pEgX = *ppEgXtmp ; pEgX < pEgXLast-1 ; pEgX = pEgXNext ) {
    *pEgXCp++ = *pEgX ;
    pEgXNext = pEgX+1 ;
    while ( pEgX->pVx[0] == pEgXNext->pVx[0] &&
            pEgX->pVx[1] == pEgXNext->pVx[1] ) {
      /* Nodes have been ordered to have pVx in same pos viewed from either side,
         there can be only one intersection per edge. 
         Hence this is a duplicate. Move all remaining intersections one down. */
      pEgXNext++ ;
    }
  }
  if ( pEgXCp -pspLine->egX != mX )
    hip_err ( fatal, 0, "miscount on pEgXCp in sp_list_lineX." ) ;
  

  
  return ( mX ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  sp_arc_lineX:
*/
/*! Compute arc lenghts of intersections along a line and sort.
 *
 */

/*
  
  Last update:
  ------------
  3Dec19; rename to sp_
  Nov19: conceived.
  

  Input:
  ------
  pMp: mixing plane
  pLine: line on mixing plane
    
  Returns:
  --------
  ret
  
*/

ret_s sp_arc_spLineX ( const sp_geo_type_e spGeoType,
                       const double rh, const double xArcRef[MAX_DIM],
                       spLineX_s *pspLine ) {
  ret_s ret = ret_success () ;

  egX_s *pEgX ;
  const double alZero = 0 ; // make arc len relative to a zero angle. */
  // Test for other sp_const is done in setup.
  //if ( spGeoType == sp_const_r || spGeoType == sp_const_x ) {
    for ( pEgX = pspLine->egX ; pEgX < pspLine->egX + pspLine->mEgX ; pEgX++ ) {
      pEgX->t = sp_arc_len_3d ( spGeoType, xArcRef, pEgX->xX ) ;
    }
    //  }
    //  else {
    //    hip_err ( warning, 0, "undefined sliding plane geo type in sp_arc_spLineX." ) ;
    //    ret.status = warning ;
    //  }

  qsort ( pspLine->egX, pspLine->mEgX, sizeof( *pspLine->egX ), sp_cmp_egX_t ) ;
  
  return ( ret ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------
  pUns: grid
  kBc: number of bc in pUns->ppBc
  spGeoType: const r or const x
  rh: list of radii/heights
  pmEgXtmp: pointer to size of 
  pEgXtmp: work list with intersections.


  Output:
  -------
  pspLine: the edge intersections on the sliding/mixing plane line 
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s sp_make2_lineX ( uns_s *pUns, const int kBc, const sp_geo_type_e spGeoType,
                      const double rh,
                      int *pmEgXtmp, egX_s **ppEgXtmp,
                      const double xArcRef[MAX_DIM],
                      spLineX_s *pspLine ) {
  ret_s ret = ret_success () ;


  /* Intersect all bnd faces of patch nBc */
  sp_make_lineX ( pUns, kBc, spGeoType, rh, pmEgXtmp, ppEgXtmp,
                  xArcRef, pspLine ) ;

  

  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  uns_int_mixingplane:
*/
/*! compute integration weights of nodes for a line on a sllding/mixing plane
 *  between periodic boundaries.
 */

/*
  
  Last update:
  ------------
  2Jun20; rename geoType to more uniquely spGeoType.
  13Jan19; note pBc with pSpS.
  20Sep19: conceived.
  

  Input:
  ------
  isMaster: if nonzero, increment the counter of masters/pairs.
  pUns: grid
  kBc: number of bc in pUns->ppBc to use as mixing plane
  mpGeoType: geometric type of this bc
  doComputeRh: if nonzero: compute the stack of radii/height. 
  Otherwise use provided from other side.
  pSpSOther: other side of the interface, if it defines r,h.

  Changes to:
  -----------
  ifcName: name of the interface

    
  Returns:
  --------
  pointer to slidingPlaneSide that was created. 
  
*/

slidingPlaneSide_s *uns_side_slidingplane_per ( const char ifcName[LINE_LEN],
                                                const int isMaster,
                                                uns_s *pUns,
                                                const int kBc, const sp_geo_type_e spGeoType,
                                                const int doComputeRh,
                                                const slidingPlaneSide_s *pSpSOther ) {

  if ( pUns->mDim != 3 ) {
    hip_err ( warning, 0,
              "current implementation of uns_int_mixingplane requires 3d." ) ;
    return ( 0 ) ;
  }
  if ( ( check_per_setup ( pUns ) ).status != success ) {
    sprintf ( hip_msg, "cannot find sliding/mixing planes without proper periodic setup.\n" ) ;
    hip_err ( warning, 0, hip_msg ) ;
    return ( 0 ) ;
  }
  if ( !pUns->mPerBcPairs ) {
    hip_err ( warning, 0,
              "current implementation of uns_int_mixingplane requires periodicity." ) ;
    return ( 0 ) ;
  }
 

  /* Determine type of coor system on mixing plane: axial or radial. 
     Determine min max values. */

  slidingPlaneSide_s *pSpS = make_slidingPlaneSide ( pUns, isMaster, ifcName ) ;
  pSpS->pBc = pUns->ppBc[kBc] ;
  if ( doComputeRh ) {
    /* Make a list of radii/heights */
    pSpS->mLines = sp_line_pos_coor_per ( pUns, pSpS, kBc, spGeoType ) ;
    if ( !pSpS->mLines )
      return ( 0 ) ;
  }
  else if ( !pSpSOther ) {
    hip_err ( warning, 1,
              "can't compute shadow mixing plane without given"
              "radii/heights in uns_int_mixingplane." ) ;
    return ( 0 ) ;
  }
  else {
    /* Compare extent of patch nBc to given rLine. */
    hip_err ( info, 2, "check whether mixing plane bcs match not yet implemented." ) ;

    /* Copy number of lines and their position. */
    pSpS->spGeoType = spGeoType ;
    vec_copy_dbl ( pSpSOther->xArcRef, MAX_DIM, pSpS->xArcRef ) ;
    pSpS->mLines = pSpSOther->mLines ;
    pSpS->pspLine = arr_calloc ( "pLine in uns_side_mixing_plane",
                                 pUns->pFam, pSpSOther->mLines,
                                 sizeof ( *pSpS->pspLine) ) ;
    
    pSpS->prh = pSpSOther->prh ;
  }

  /* Allocate a list of intersections, to be increased in size in sp_make_lineX. */
  int mEgXtmp = 500 ;
  egX_s *pEgXtmp = arr_malloc ( "pEgXtmp in uns_side_slidingplane_per", pUns->pFam,
                              mEgXtmp, sizeof( *pEgXtmp ) ) ;
  
  int kL ;
  for ( kL = 0 ; kL < pSpS->mLines ; kL++ ) {
    sp_make_lineX ( pUns, kBc, spGeoType, pSpS->prh[kL],
                    &mEgXtmp, &pEgXtmp,
                    pSpS->xArcRef, pSpS->pspLine+kL ) ;
  }

  arr_free ( pEgXtmp ) ;

  return ( pSpS ) ;
}


/***************************************************************************

  PUBLIC

**************************************************************************/

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  pair_slidingPlaneSides:
*/
/*! pair up sliding plane sides matching plane numbers declared at generation.
 *
 */

/*
  
  Last update:
  ------------
  6Apr20: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s pair_slidingPlaneSides ( uns_s *pUns ) {
  ret_s ret = ret_success () ;

  int mSpS = pUns->mSlidingPlaneSides ;
  if ( !mSpS )
    /* Nothing to be paired. */
    return ( ret ) ;

  /* Scan for the lowest and highest ifc number. */
  int kS ;
  int nSpSMin = INT_MAX, nSpSMax = INT_MIN ;
  slidingPlaneSide_s **ppSpS  = pUns->ppSlidingPlaneSide ;
  for ( kS = 0; kS < mSpS ; kS++ ) {
    nSpSMin = MIN( nSpSMin, ppSpS[kS]->nr ) ;
    nSpSMax = MAX( nSpSMax, ppSpS[kS]->nr ) ;
  }
  int mSpP = nSpSMax - nSpSMin + 1 ;

  
  /* Allocate. */
  if ( pUns->pSlidingPlanePair ) {
    /* Dealloc existing list. */
    arr_free ( pUns->pSlidingPlanePair ) ;
    pUns->pSlidingPlanePair = NULL ;
    pUns->mSlidingPlanePairs = 0 ;
  }
  slidingPlanePair_s *pSpP = pUns->pSlidingPlanePair =
    arr_calloc ( "slidingPlanePair in pair_slidingPlaneSides",
                 pUns->pFam, mSpP, sizeof ( *(pUns->pSlidingPlanePair) ) ) ;
  pUns->mSlidingPlanePairs = mSpP ;

  

  
  /* Collect. */
  slidingPlaneSide_s *pSpS ;
  int kSP ;
  for ( kS = 0; kS < mSpS ; kS++ ) {
    pSpS = ppSpS[kS] ;
    kSP =  ( pSpS->isMaster ? 0 : 1 ) ;
    pSpP[ pSpS->nr - nSpSMin ].side[ kSP ] = pSpS ;
  }

  /* Check full set, matching names. */
  int kP ;
  for ( kP = 0 ; kP < mSpP ; kP++ ) {
    if ( !pSpP[kP].side[1] ) {
      sprintf ( hip_msg, "missing shadow side for interface pair %d named %s.\\"
                "         No interfaces retained.",
                pSpP[kP].side[0]->nr, pSpP[kP].side[0]->name ) ;
      hip_err ( warning, 1, hip_msg ) ;
      pUns->mSlidingPlanePairs = 0 ;
      ret.status = fatal ;
    }
    else if ( !pSpP[kP].side[0] ) {
      sprintf ( hip_msg, "missing master side for interface pair %d named %s.\\"
                "         No interfaces retained.",
                pSpP[kP].side[1]->nr, pSpP[kP].side[1]->name ) ;
      hip_err ( warning, 1, hip_msg ) ;
      pUns->mSlidingPlanePairs = 0 ;
      ret.status = fatal ;
    }
    else if ( strcmp ( pSpP[kP].side[0]->name, pSpP[kP].side[1]->name ) ) {
      sprintf ( hip_msg, "mismatch in names for interface pair %d, master %s, shadow %s.",
                pSpP[kP].side[1]->nr, pSpP[kP].side[0]->name, pSpP[kP].side[1]->name ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }

  return ( ret ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  uns_int_mixingplane:
*/
/*! compute integration weights of nodes for a line on a mixing plane.
 */

/*
  
  Last update:
  ------------
  28Apr20; have both sides determine their own heights.
  26Nov19: conceived.
  

  Input:
  ------
  name: name of the interface.
  pUns0: the mesh that defines the mixing lines, one line for each periodic node on the mp.
  kBc0:  number of the mixing plane bc in pUns0->ppBc
  pUns1: the mesh that inherits the mixing lines
  kBc1:  number of the mixing plane bc in pUns1->ppBc
  mpGeotype: what geometric type of bc.
    
  Returns:
  --------
  Total number of mixing lines.
  
*/

int uns_sliding_plane ( const char ifcName[LINE_LEN],
                        uns_s *pUns0, const int kBc0,
                        uns_s *pUns1, const int kBc1,
                        const sp_geo_type_e spGeoType ) {

  const int doComputeR = 1, dontComputeR = 0 ;
  double *prh = NULL ;


  /* This side determines the number of lines pmLines, and their height rh */
  const int isMaster = 1 ;
  slidingPlaneSide_s *pSpS0 = 
    uns_side_slidingplane_per ( ifcName, isMaster, pUns0, kBc0, spGeoType,
                                doComputeR,
                                NULL ) ;
  if ( !pSpS0 || !pSpS0->mLines ) {
    hip_err ( warning, 1, "no sliding/mixing lines found on master in uns_sliding_plane.");
    return ( 0 ) ;
  }

  /* Other side. */
  const int isntMaster = 0 ;
  slidingPlaneSide_s *pSpS1 = 
    uns_side_slidingplane_per ( ifcName, isntMaster, pUns1, kBc1, spGeoType,
                                doComputeR,
                                pSpS0 ) ;
  if ( !pSpS1 || !pSpS1->mLines ) {
    hip_err ( warning, 1, "no sliding/mixing lines found on shadow in uns_sliding_plane.");
    return ( 0 ) ;
  }

  /* Link the two sides. */
  pSpS0->pOtherSide = pSpS1 ;
  pSpS1->pOtherSide = pSpS0 ;
  pSpS1->isMaster = 0 ;
  
  return ( pSpS0->mLines + pSpS1->mLines ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  7Apr20; include sp_const_xyz.
  3Apr20; use find_uns_expr.
  3dec19: conceived.
  

  Input:
  ------
  line: command line with arguments
    
  Returns:
  --------
  ret_s
  
*/

ret_s uns_interface_sliding_plane ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;

  /* Parse line of unix-style optional args. */
  char c ;
  /* colon behind arg means argument to be read,
     double colon means optional argument, but needs to be joined: -a0, not -a 0. */
  /* Set the arg counter back, global variable incremented by getopt. */
  optind = 1 ;
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( line, &ppArgs ) ;
  char ifcName[LINE_LEN] ;
  ifcName[0] = '\0' ;

  while ((c = getopt_long ( mArgs, ppArgs, "n:",NULL,NULL)) != -1) {
    switch (c)  {
    case 'n':
      strncpy ( ifcName, optarg, LINE_LEN ) ;
      break ;
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }

  
  /* Check presence of non-opt (non -) args. */
  int nBc, kBc[2] ;
  uns_s *pUns[2] ;
  char bcExpr[LINE_LEN], strGeoType[LINE_LEN] ;
  int k, iBc ;
  bc_struct *pBc ;
  sp_geo_type_e spGeoType ;
  if ( optind+5 > mArgs ) {
    ret.status = warning ;
    hip_err ( warning, 1,
              "not enough arguments in call for interface sliding/mixing." ) ;
    return ( ret ) ;
  }
  
  else {
    /* Two grids and their mixing/sliding plane bcs. */
    for ( k = 0 ; k < 2 ; k++ ) {
      if ( !(pUns[k] = find_uns_expr ( ppArgs[optind++] ) ) ) {
        sprintf ( hip_msg, "unstructured grid matching `%s' does not exist.", ppArgs[optind-1]  ) ;
        hip_err ( warning, 1, hip_msg ) ;
        ret.status = warning ;
        return ( ret ) ;
      }

      strcpy ( bcExpr, ppArgs[optind++] ) ;
      /* Find first match. Only one bc is expected. */
      iBc = -1 ;
      if ( !(pBc = loop_bc_uns_expr ( pUns[k], &iBc, bcExpr ) ) ) {
        sprintf ( hip_msg, "bc matching `%s' does not exist.", bcExpr ) ;
        hip_err ( warning, 1, hip_msg ) ;
        ret.status = warning ;
        return ( ret ) ;
      }
      else {
        kBc[k] = iBc ;
      }

      sprintf ( hip_msg, "using bc named %s on grid %d named %s to define mixing lines for side %d.",
                pUns[k]->ppBc[kBc[k]]->text, pUns[k]->nr, pUns[k]->pGrid->uns.name, k ) ;
      hip_err ( info, 2, hip_msg ) ;
    }
    

    /* Interface type, const_r, or const_x. */
    strcpy ( strGeoType, ppArgs[optind++] ) ;
    if ( !strncmp ( strGeoType, "rx", 2 ) ) {
      spGeoType = sp_const_rx ;
    }
    else if ( !strncmp ( strGeoType, "ry", 2 ) ) {
      spGeoType = sp_const_ry ;
    }
    else if ( !strncmp ( strGeoType, "rz", 2 ) ) {
      spGeoType = sp_const_rz ;
    }
    else if ( strGeoType[0] == 'r' ) {
      spGeoType = sp_const_rz ;
    }
    else if ( strGeoType[0] == 'x' ) {
      spGeoType = sp_const_x ;
    }
    else if ( strGeoType[0] == 'y' ) {
      spGeoType = sp_const_y ;
    }
    else if ( strGeoType[0] == 'z' ) {
      spGeoType = sp_const_z ;
    }
    else {
       spGeoType = sp_no_const ;
    }
    if ( spGeoType == sp_no_const ) {
      sprintf ( hip_msg, "unrecognised sliding/mixing plane geo type: %s.",
                strGeoType ) ;
      hip_err ( warning, 1, hip_msg ) ;
      ret.status = warning ;
      return ( ret ) ;
    }

    int mLines = uns_sliding_plane ( ifcName,
                                     pUns[0], kBc[0],
                                     pUns[1], kBc[1],
                                     spGeoType ) ;

    if ( mLines < 1 )
      ret.status = fatal ;

    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "found %d radial/circumferential lines", mLines ) ;
      hip_err ( info, 1, hip_msg ) ;
    }

    return ( ret ) ;
  }
}

