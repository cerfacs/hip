/*
 read_uns_dpl.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from a generic .dpl file for cpre.

 contains:
 read_uns_dpl

 Last update:
 ------------
 22Mar11; fix bug with canonical node numbering.
 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const char varTypeNames[][LEN_VAR_C] ;
extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;

/* Global control variables, what is read from file how. */

static char gmr_bc_name_prepend[LINE_LEN] ;
static int gmr_bc_tag_pos = 0 ;

typedef struct {
  int iDim ;
  int iPhysNm ;
  char label[LINE_LEN] ;
  bc_struct *pBc ;
  int iZone ;
} gmr_physNm_s ;


typedef struct {
  int iDim ;
  int iEnt ;
  gmr_physNm_s *pPhysNm ;
} gmr_ent_s ;


typedef struct {
  // the list of physical names
  int mPhysNm ;
  gmr_physNm_s *pPhysNm ;

  // the list of entities
  int mEnt ;
  gmr_ent_s *pEnt ;

  // zones, currently only if they exist
  // find by searching physNm.
  int mZones ;
} gmr_tag_s ;


void gmr_flag_reset () {
  strcpy(gmr_bc_name_prepend, "group_" );
  return ;
}


/* A conversion list from gmsh's format to hip. */
const int g2h[MAX_ELEM_TYPES][MAX_VX_ELEM] = { 
  {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,2,1,3},        /* tet */
  {0,1,2,3,4},      /* pyramid */
  {0,3,4,1,5,2},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;


/*
  Line:                   Line3:           Line4:
     
  0----------1 --> u      0-----2----1     0----2----3----1

  Triangle:               Triangle6:          Triangle9/10:          Triangle12/15:
     
  v
  ^                                                                   2
  |                                                                   | \
  2                       2                    2                      9   8
  |`\                     |`\                  | \                    |     \
  |  `\                   |  `\                7   6                 10 (14)  7
  |    `\                 5    `4              |     \                |         \
  |      `\               |      `\            8  (9)  5             11 (12) (13) 6
  |        `\             |        `\          |         \            |             \
  0----------1 --> u      0-----3----1         0---3---4---1          0---3---4---5---1

  Quadrangle:            Quadrangle8:            Quadrangle9:
     
  v
  ^
  |
  3-----------2          3-----6-----2           3-----6-----2
  |     |     |          |           |           |           |
  |     |     |          |           |           |           |
  |     +---- | --> u    7           5           7     8     5
  |           |          |           |           |           |
  |           |          |           |           |           |
  0-----------1          0-----4-----1           0-----4-----1

  Tetrahedron:                          Tetrahedron10:
     
  v
  .
  ,/
  /
  2                                     2
  ,/|`\                                 ,/|`\
  ,/  |  `\                             ,/  |  `\
  ,/    '.   `\                         ,6    '.   `5
  ,/       |     `\                     ,/       8     `\
  ,/         |       `\                 ,/         |       `\
  0-----------'.--------1 --> u         0--------4--'.--------1
  `\.         |      ,/                 `\.         |      ,/
  `\.      |    ,/                      `\.      |    ,9
  `\.   '. ,/                           `7.   '. ,/
  `\. |/                                `\. |/
  `3                                    `3
  `\.
  ` w

  Hexahedron:             Hexahedron20:          Hexahedron27:
     
  v
  3----------2            3----13----2           3----13----2
  |\     ^   |\           |\         |\          |\         |\
  | \    |   | \          | 15       | 14        |15    24  | 14
  |  \   |   |  \         9  \       11 \        9  \ 20    11 \
  |   7------+---6        |   7----19+---6       |   7----19+---6
  |   |  +-- |-- | -> u   |   |      |   |       |22 |  26  | 23|
  0---+---\--1   |        0---+-8----1   |       0---+-8----1   |
  \  |    \  \  |         \  17      \  18       \ 17    25 \  18
  \ |     \  \ |         10 |        12|        10 |  21    12|
  \|      w  \|           \|         \|          \|         \|
  4----------5            4----16----5           4----16----5

  Prism:                      Prism15:               Prism18:
     
  w
  ^
  |
  3                       3                      3
  ,/|`\                   ,/|`\                  ,/|`\
  ,/  |  `\               12  |  13              12  |  13
  ,/    |    `\           ,/    |    `\          ,/    |    `\
  4------+------5         4------14-----5        4------14-----5
  |      |      |         |      8      |        |      8      |
  |    ,/|`\    |         |      |      |        |    ,/|`\    |
  |  ,/  |  `\  |         |      |      |        |  15  |  16  |
  |,/    |    `\|         |      |      |        |,/    |    `\|
  ,|      |      |\        10     |      11       10-----17-----11
  ,/ |      0      | `\      |      0      |        |      0      |
  u   |    ,/ `\    |    v    |    ,/ `\    |        |    ,/ `\    |
  |  ,/     `\  |         |  ,6     `7  |        |  ,6     `7  |
  |,/         `\|         |,/         `\|        |,/         `\|
  1-------------2         1------9------2        1------9------2

  Pyramid:                     Pyramid13:                   Pyramid14:
     
  4                            4                            4
  ,/|\                         ,/|\                         ,/|\
  ,/ .'|\                      ,/ .'|\                      ,/ .'|\
  ,/   | | \                   ,/   | | \                   ,/   | | \
  ,/    .' | `.                ,/    .' | `.                ,/    .' | `.
  ,/      |  '.  \             ,7      |  12  \             ,7      |  12  \
  ,/       .' w |   \          ,/       .'   |   \          ,/       .'   |   \
  ,/         |  ^ |    \       ,/         9    |    11      ,/         9    |    11
  0----------.'--|-3    `.     0--------6-.'----3    `.     0--------6-.'----3    `.
  `\        |   |  `\    \      `\        |      `\    \     `\        |      `\    \
  `\     .'   +----`\ - \ -> v  `5     .'        10   \      `5     .' 13     10   \
  `\   |    `\     `\  \        `\   |           `\  \       `\   |           `\  \
  `\.'      `\     `\`          `\.'             `\`         `\.'             `\`
  1----------------2            1--------8-------2           1--------8-------2
  `\
  u

*/


/******************************************************************************

  gmr_open_ascii:
  Open the file, check whether it is ASCII, version >= 2.0.
  
  Last update:
  ------------
  14Mar22; rename to gmr_
  20Sep19; check for 2.x version number.
  19Feb10: conceived.
  
  Input:
  ------
  fileName

  Output:
  -------
  
  Returns:
  --------
  Fmsh: opened file, positioned after EndMeshFormat on success, NULL on Failure.

  
*/

FILE *gmr_open_ascii ( char *fileName, float *pVersion ) {

  FILE *Fmsh ;
  char someStr[TEXT_LEN] ;

  if ( ( Fmsh = fopen( prepend_path(fileName), "r")) == NULL) {
    sprintf ( hip_msg, "failed to open mesh file in read_gmsh:\n"
              "         %s\n", fileName ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Make sure it's a .msh > 2.0 */
  rewind ( Fmsh ) ;
  if ( fscanf ( Fmsh, "%s", someStr ) != 1 ||
       strncmp( someStr, "$MeshFormat", strlen( "$MeshFormat") ) ) {
    sprintf ( hip_msg, "file is not a recognised .msh type (version >= 2.0) .\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
    return ( NULL ) ; /* Not that you'd ever get here. */
  }
  fscanf ( Fmsh, "%*[^\n]" ) ;
  fscanf ( Fmsh, "%*[\n]" ) ;

  /* Version ?. */
  fscanf ( Fmsh, "%g", pVersion ) ;
  fscanf ( Fmsh, "%*[^\n]" ) ;
  fscanf ( Fmsh, "%*[\n]" ) ;
  sprintf ( hip_msg, "found gmsh version %g.", *pVersion ) ;
  hip_err ( info, 3, hip_msg ) ;

  int intVer = (int) *pVersion ;
  if ( !( intVer==2 || intVer == 4 ) ) {
    hip_err ( warning, 0," hip currently only reads 2.x and 4.x formats." ) ;
    return ( NULL ) ;
  }


  /* Make sure it's ASCII, the third line must be EndMeshFormat. */
  if ( fscanf ( Fmsh, "%s", someStr ) != 1 ||
       strncmp( someStr, "$EndMeshFormat", strlen( "$EndMeshFormat") ) ) {
    sprintf ( hip_msg, "only ASCII .msh formats are supported .\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
    return ( NULL ) ; /* Not that you'd ever get here. */
  }
  
  return ( Fmsh ) ;
}


/******************************************************************************

  gmr_seek1:
  Find a header tag in an ASCII gmsh file. This routine only scans further onward
  from the current positioning, issue a rewind if the tag could occur ealier
  in the file.
  If a non-zero mEntries is given, it is compared to the number after the 
  section tag (and the file pointer is advanced). A fatal error is issued
  on mismatch. 
  
  Last update:
  ------------
  14Mar22; rename to seek1, to enable wrapping it with gmr_seek.
  9Feb10: conceived.
  
  Input:
  ------
  Fmsh: opened file,
  secTag: section tag,

  Changes To:
  -----------
  Fmsh: repositioned file.

  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmr_seek1 ( FILE *Fmsh, const char *secTag ) {

  char someStr[TEXT_LEN] ;
  int tagLen =  strlen( secTag ) ;
  int mEntSec ;

  someStr[0] = someStr[1] = '\0' ;
  if ( !strncmp( someStr, secTag, tagLen ) )
    hip_err ( warning, 0, "calling gmr_seek with a zero tag doesn't do anything." );


  while ( strncmp( someStr+1, secTag, tagLen ) ) {
    /* Reset, look for the next $. */
    someStr[0] = '\0' ;

    if ( feof( Fmsh ) )
      return ( 0 ) ;
    
    /* All tags have to be preceded by a $. */
    while ( someStr[0] != '$' ) {
      /* Read the leading text off the next line and skip the rest.
         This assumes that the tag has no leading or embedded whitespace. */
      fscanf ( Fmsh, "%s", someStr ) ; // Why is a string read first?
      fscanf_end_line ( Fmsh ) ;
    }
  }

  return (1) ;
}

int gmr_seek ( FILE *Fmsh, const char *secTag ) {

  int retVal ;
  retVal = gmr_seek1 ( Fmsh, secTag ) ;

  if ( retVal == 0 ) {
    // Try again once from the start.
    rewind ( Fmsh ) ;
    retVal = gmr_seek1 ( Fmsh, secTag ) ;
  }

  return ( retVal ) ;
}

/******************************************************************************

  gmsh_elT:
  Translate the numerical iElT from a gmsh file into hip's elemType.
  
  Last update:
  ------------
  10Feb10: conceived.
  
  Input:
  ------
  iElT: gmsh type integer

  
  Returns:
  --------
  pElT: pointer to the corresponding elemType
  
*/

const elemType_struct *gmsh_elT ( const ulong_t iElT, int *pmVx ) {

  const elemType_struct *pElT = elemType ;

  switch ( iElT ) {
  case ( 15) : pElT = NULL, *pmVx = 1 ; break ; /* bnd node. */ 
  case ( 1 ) : pElT +=  bi, *pmVx = 2 ; break ; /* 2D face. */ 
  case ( 2 ) : pElT += tri, *pmVx = 3 ; break ; /* tri. */ 
  case ( 3 ) : pElT += qua, *pmVx = 4 ; break ; /* qua. */ 
  case ( 4 ) : pElT += tet, *pmVx = 4 ; break ; /* tet. */ 
  case ( 5 ) : pElT += hex, *pmVx = 8 ; break ; /* hex. */ 
  case ( 6 ) : pElT += pri, *pmVx = 6 ; break ; /* pri. */ 
  case ( 7 ) : pElT += pyr, *pmVx = 5 ; break ; /* pyr. */ 
  default :    pElT = NULL, *pmVx = 0 ;
  }

  return ( pElT) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_find_physNm:
*/
/*! Given some tag (int value), map it to the corresponding phys ent label.
 *
 */

/*
  
  Last update:
  ------------
  8jun23; renamed from find_label_from_tag
  27Apr23: conceived.
  

  Input:
  ------
  mPhysNm: number of phys names in pPhysNm
  pPhysNm: list of phys names
  iPhysNm: no of the phys name
  iDim: geom dim of the phys name
  

    
  Returns:
  --------
  nBc, 0 on failure, no of the assoc. phys ent. if there is a match.
  
*/

gmr_physNm_s *gmr_find_physNm ( const gmr_tag_s *pTags,
                                const int iDim, const int iPhysNm) {
#undef FUNLOC
#define FUNLOC "in gmr_find_physNm"
  
  gmr_physNm_s *pPNm ;
  for ( pPNm = pTags->pPhysNm ;
        pPNm < pTags->pPhysNm + pTags->mPhysNm ; pPNm++ )
    if ( pPNm->iDim == iDim && pPNm->iPhysNm == iPhysNm )
      return ( pPNm ) ;

  sprintf ( hip_msg, "failed to find matching tag %d dim %d "FUNLOC".",
            iPhysNm, iDim );
  hip_err ( warning, 0, hip_msg ) ;
        
  return ( NULL ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_physNm:
*/
/*! read gmsh physical names section
 *
 */

/*
  
  Last update:
  ------------
  27apr23: conceived.
  

  Input:
  ------
  Fmsh: file
  version: gmsh version
  mDim: spatial dimension

  Output:
  -------
  ***pppBc: list of bcs
  **pp_physNm: list of physical names
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int gmr_read_physNm ( FILE *Fmsh, float version,
                      const int mDim, int *pmBc, bc_struct ***pppBc,
                      gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_physnm"
 
  if ( !gmr_seek ( Fmsh, "PhysicalNames" ) ) {
    if ( (int)version != 2 )
      // No phys names in some v2 formats.
      sprintf ( hip_msg, 
                "could not find boundary labels, will use numbers.\n" ) ;
    hip_err ( info, 2, hip_msg ) ;
    return (0);
  }

  // JDM Feb 25, entity section can be incomplete, use both
  // // Remove default builds for v2.
  // pTags->mPhysNm = 0 ;
  // if ( pTags->pPhysNm ) {
  //   arr_free ( pTags->pPhysNm ) ;
  //   pTags->pPhysNm = NULL ;
  // }
  // /* No bcs yet. */
  // *pmBc = 0 ;
  
  char *someStr = NULL ;
  size_t strSz = 0 ;
  int mPhysNm ;
  getline ( &someStr, &strSz, Fmsh ) ;
  sscanf ( someStr, "%d", &(mPhysNm) ) ;

  // pTags->pPhysNm = arr_calloc ( FUNLOC, NULL, pTags->mPhysNm, sizeof(*pTags->pPhysNm) ) ;
  // *pppBc = arr_calloc ( FUNLOC, NULL, pTags->mPhysNm, sizeof(**pppBc) ) ;

  bc_struct *pBc ;
  gmr_physNm_s *pPNm ;
  int k, iDim, iPhysNm ;
  char label[LINE_LEN] ;
  for ( k=0 ; k<mPhysNm ; k++ ) {
    getline ( &someStr, &strSz, Fmsh ) ;
    sscanf ( someStr, "%d %d %s",
             &(iDim), &(iPhysNm), label ) ;
    r1_stripquote ( label, strlen(label) ) ;

    // Overwrite matching entities in list
    if ( !(pPNm = gmr_find_physNm ( pTags, iDim, iPhysNm ) ) ) {
      // All entities that are referenced are already in the list.
      // This one is hence not referenced, disregard.
      sprintf ( hip_msg, "PhysicalName %d, dim %d, label %s"
                " is not referenced by any element, disregarding.",
                iPhysNm, iDim, label ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
    else {      
      // Update phys name.
      strcpy ( pPNm->label, label ) ;
      if ( pPNm->iDim == mDim-1 ) {
        // Update phys name. Create a new bc, rather than updating
        // the text of the existing bc, as that bc might have been
        // pre-existing and ref'd in a different mesh
        pBc = find_bc ( pPNm->label, 1 ) ; // ppBc numbers from zero.
        pPNm->pBc = (*pppBc)[*pmBc] = pBc ;
        (*pmBc)++ ;
      }
      else
        pPNm->pBc = NULL ;

      if ( pPNm->iDim == 1 )
        sprintf ( hip_msg, "found line (1d) %d named %s.",
                  pPNm->iPhysNm, pPNm->label ) ;
      else if ( pPNm->iDim == 2 )
        sprintf ( hip_msg, "found surface (2d) %d named %s.",
                  pPNm->iPhysNm, pPNm->label ) ;
      else if ( pPNm->iDim == 3 )
        sprintf ( hip_msg, "found volume (3d) %d named %s.",
                  pPNm->iPhysNm, pPNm->label ) ;
      else
        sprintf ( hip_msg, "can't handle dim %d tagged %d named %s.",
                  pPNm->iDim, pPNm->iPhysNm, pPNm->label ) ;

      hip_err ( info, 2, hip_msg ) ;
    }
  }

  free ( someStr ) ;
  return ( 1 ) ;
}




/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_entities:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  27Apr23: conceived.
  

  Input:
  ------
  Fmesh: opened file
  version: 2 or 4

  Output:
  -------
  ppt2l: a map from tag to label.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int gmr_read_entities ( FILE *Fmsh, float version,
                        int mDim, gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_entities"

  if ( !gmr_seek ( Fmsh, "Entities" ) ) {
    if ( (int)version != 2 )
      // No entities in v2 formats.
      hip_err ( fatal, 0, "could not find an entities section,"
                " interpreting elem tags to refer to physical names." ) ;
    return ( 0 ) ;
  }

  // Remove default builds for v2.
  pTags->mEnt = 0 ;
  if ( pTags->pEnt ) {
    arr_free ( pTags->pEnt ) ;
    pTags->pEnt = NULL ;
  }


  char *someStr = NULL ;
  size_t strSz = 0 ;
  getline ( &someStr, &strSz, Fmsh ) ;
  ulong_t mEnt[4] = {0} ;
  sscanf ( someStr, "%"FMT_ULG" %"FMT_ULG" %"FMT_ULG" %"FMT_ULG"",
           mEnt , mEnt+1 , mEnt+2 , mEnt+3 ) ;
  ulong_t mEntAll = mEnt[0] + mEnt[1] + mEnt[2] + mEnt[3] ;
  pTags->pEnt = arr_calloc( FUNLOC, NULL, mEntAll, sizeof(*pTags->pEnt) ) ;
  pTags->mEnt = mEntAll ;
  gmr_ent_s *pEnt = pTags->pEnt ;
  

  // Points
  int iPhysNm ;
  int nLn ;
  for ( nLn = 0 ; nLn < mEnt[0] ; nLn++ ) {
    getline ( &someStr, &strSz, Fmsh ) ;
    sscanf ( someStr, "%d %*f %*f %*f %d",
             &(pEnt->iEnt), &iPhysNm ) ;
    pEnt->iDim = 0 ; // point
    pEnt++ ;
  }

  // Curves, Surfaces, Volumes.
  char *tok ;
  int kDim, k, iPhysTag ;
  for ( kDim = 1 ; kDim <= 3 ; kDim++ ) {
    for ( nLn = 0 ; nLn < mEnt[kDim] ; nLn++ ) {
      pEnt->iDim = kDim ; // curve
      getline ( &someStr, &strSz, Fmsh ) ;
      // ent no/tag
      tok = strtok( someStr, " " ) ;
      pEnt->iEnt = atoi(tok) ;
      // 6* float for min/max xyz, skip
      for ( k = 0 ; k < 6 ; k++ )
        tok = strtok( NULL, " " ) ;
      // No of phys tags
      tok = strtok( NULL, " " ) ;
      iPhysTag = atoi(tok) ;
      for ( k = 0 ; k < iPhysTag ; k++ ) {
        tok = strtok( NULL, " " ) ;
        // Use the first tag to point to name
        if ( k==0 ) {
          iPhysNm = atoi(tok) ;
          pEnt->pPhysNm = gmr_find_physNm ( pTags, pEnt->iDim, iPhysNm );
        }
      }
      // Forget the rest of the tags. 
      pEnt++ ;
    }
  }

  
  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_create_physNm:
*/
/*! given a list of entities, derive a matching set of phys names.
 *
 */

/*
  
  Last update:
  ------------
  8Jun23: conceived.
  

  Changes To:
  -----------
  pTags: the list of entities and names
  
*/

void gmr_create_physNm ( gmr_tag_s *pTags, int *pmBc, bc_struct ***pppBc ) {
#undef FUNLOC
#define FUNLOC "in gmr_create_physNm"

  int m = pTags->mEnt ;
  if ( !m )
    hip_err ( fatal, 0, "need a list of entities to derive names "FUNLOC"." );

  pTags->mPhysNm = m ;
  pTags->pPhysNm = arr_calloc ( FUNLOC, NULL,
                                pTags->mPhysNm, sizeof(*pTags->pPhysNm) ) ;
  *pppBc = arr_realloc ( "pppBc "FUNLOC"", NULL, *pppBc, pTags->mPhysNm,
                         sizeof( **pppBc ) ) ;
  
  int k ;
  char bcText[LINE_LEN] ;
  gmr_ent_s *pEnt ;
  gmr_physNm_s *pPNm ;
  for ( k = 0, pEnt = pTags->pEnt, pPNm = pTags->pPhysNm ;
        k < m ; k++, pEnt++, pPNm++ ) {
    sprintf ( bcText, "tag%dd_%d", pEnt->iDim, pEnt->iEnt ) ;
    strcpy ( pPNm->label, bcText ) ;
    pPNm->pBc = (*pppBc)[*pmBc] = find_bc ( bcText, 1 ) ;
    (*pmBc)++ ;
    pPNm->iDim = pEnt->iDim ;
    pPNm->iPhysNm = pEnt->iEnt ;
    pEnt->pPhysNm = pPNm ;
  }

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_find_entities:
*/
/*! Given some tag label (int value), map it to the corresponding ent.
 *
 */

/*
  
  Last update:
  ------------
  8jun23; derived from gmr_find_physNm
  

  Input:
  ------
  tag: tag data
  iDim: geom dim of the phys name
  

    
  Returns:
  --------
  nBc, 0 on failure, no of the assoc. phys ent. if there is a match.
  
*/

gmr_ent_s *gmr_find_entity ( const gmr_tag_s *pTags,
                             const int iDim, const int iEnt) {
#undef FUNLOC
#define FUNLOC "in gmr_find_entity"
  
  gmr_ent_s *pEnt ;
  int dim_mismatch = 0 ;
  for ( pEnt = pTags->pEnt ; pEnt < pTags->pEnt + pTags->mEnt ; pEnt++ )
    if ( pEnt->iDim == iDim && pEnt->iEnt == iEnt )
      return ( pEnt ) ;
    else if ( pEnt->iEnt == iEnt )
      dim_mismatch = 1 ;

  if ( dim_mismatch == 1 ) {
    sprintf ( hip_msg, "found matching tag %d but mismatching dim %d "FUNLOC".",
              iEnt, iDim );
    hip_err ( warning, 0, hip_msg ) ;
  }
  // else
    // This is gracefully handled by setting defaults, don't issue a warning.
    //sprintf ( hip_msg, "failed to find matching tag %d dim %d "FUNLOC".",
    //          iEnt, iDim );
        
  return ( NULL ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_add_tag:
*/
/*! add an entity to the list of entities in tags.
 *
 */

/*
  
  Last update:
  ------------
  8jun23: conceived.
  

  Input:
  ------
  iDim: dim of entity
  iTag: tag of entity

  Changes To:
  -----------
  pTags: list of entities and names
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int gmr_add_entity ( gmr_tag_s *pTags, int iDim, int iTag ) {
#undef FUNLOC
#define FUNLOC "in gmr_add_tag"

  gmr_ent_s *pEnt ;
  int kPos ;
  if ( (pEnt = gmr_find_entity ( pTags, iDim, iTag ) ) )
    kPos = pEnt - pTags->pEnt ;
  else {
    // Create a tag, add it.
    pTags->pEnt = arr_realloc ( "iEnt "FUNLOC"", NULL,
                                pTags->pEnt, pTags->mEnt+1,
                                sizeof( *pTags->pEnt ) ) ;
    pEnt = pTags->pEnt + pTags->mEnt ;
    pEnt->iDim = iDim ;
    pEnt->iEnt = iTag ;
    pEnt->pPhysNm = NULL;
    kPos = pTags->mEnt ;
    (pTags->mEnt)++ ;
  }
  return ( kPos ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_find_zone:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------
  pTag: pointer to the lists of tags, names
  iDim: dim of the entity
  iEnt: number tag of the entity
  
    
  Returns:
  --------
  number of the zone.
  
*/

int gmr_find_zone ( const gmr_tag_s *pTags, const int iDim, const int iEnt ) {
#undef FUNLOC
#define FUNLOC "in gmr_find_zone"

  gmr_ent_s *pEnt = gmr_find_entity ( pTags, iDim, iEnt ) ;
  int iZone = 0 ;
  if ( pEnt )
    iZone = pEnt->pPhysNm->iZone ;
  
  return ( iZone ) ;
}

  
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_free_tag:
*/
/*! free/zero the tag data
 *
 */

/*
  
  Last update:
  ------------
  8jun23: conceived.
  

  Changes To:
  -----------
  tag: the data struct with phys names and tag mapping
  
*/

void gmr_free_tag ( gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_free_tag"

  arr_free ( pTags->pPhysNm ) ;
  pTags->pPhysNm = NULL ;
  pTags->mPhysNm = 0 ;

  arr_free ( pTags->pEnt ) ;
  pTags->pEnt = NULL ;
  pTags->mEnt = 0 ;
  
  return ;
}



/******************************************************************************

  gmsh_sol_header:
  Read the header bits of a solution file, position the file at the first
  variable line.
  
  Last update:
  ------------
  21Feb10: conceived.
  
  Input:
  ------
  Fmsh: opened file
  mVx: number of vertices (1 var for each node).
  mDim: 
  mEq: number of var. components read so far

  Changes To:
  -----------

  Output:
  -------
  varName: name of variable (stem for vector.)
  
  Returns:
  --------
  number of components in variable: 1 for scalar, mDim for vector.
  
*/

int gmsh_sol_header ( FILE *Fvar, int mVx, int mDim, char *varName, int mEq ) {

  char someStr[LINE_LEN] ;
  int m, i ;
  int mComp, mLines ;

  /* Solution. */
  if ( !gmr_seek ( Fvar, "NodeData" ) ) {
    // hip_err ( fatal, 0, "hip expects nodal unknowns in gmesh_get_sizes.\n" ) ;
    return ( 0 ) ;
  }

  /* String tags, retain the final bit of the first as a variable name. */
  fscanf ( Fvar, "%d", &m ) ;
  fscanf_end_line ( Fvar ) ;
  int len ;
  if ( m ) {
    len = fscanf_str_line ( Fvar, LEN_VARNAME, varName ) ;
    if ( !len ) {
      sprintf ( hip_msg, 
                "read_gmsh_sol: zero len string tag for var %d, using default\n", 
                mEq+1 ) ; 
      hip_err ( warning, 1, hip_msg ) ; 
      sprintf ( varName, "var_%d", mEq+1 ) ;
    }
    else
      r1_stripquote ( varName, LEN_VARNAME ) ;

    
    /* Remaining tags */
    for ( i = 1 ; i < m ; i++ )
      fscanf_str_line ( Fvar, LINE_LEN, someStr ) ;
  }
  else {
    sprintf ( hip_msg, 
              "gmsh_sol_header: no string tags for var %d, using default\n", 
              mEq+1 ) ; 
    hip_err ( warning, 1, hip_msg ) ; 
    sprintf ( varName, "var_%d", mEq+1 ) ;
  }

  /* Real tags. Throw away. */
  fscanf ( Fvar, "%d", &m ) ;
  fscanf_end_line ( Fvar ) ;
  for ( i = 0 ; i < m ; i++ )
    fscanf_end_line ( Fvar ) ;

  /* Integer tags. Keep only first 3. */
  fscanf ( Fvar, "%d", &m ) ;
  fscanf_end_line ( Fvar ) ;
  if ( m < 3 )
    hip_err ( fatal, 0, "hip needs >=3 integer solution tags in gmesh_sol_header.\n" ) ;

  int someInt ; 
  fscanf ( Fvar, "%d", &someInt ) ;
  fscanf ( Fvar, "%d", &mComp ) ;
  fscanf ( Fvar, "%d", &mLines ) ;

  if ( !mComp )
    hip_err ( warning, 2, "gmsh_sol_header found zero components variable file.\n" ) ;

  /* Skip the remaining integer tags */
  for ( i = 3 ; i < m ; i++ )
    fscanf ( Fvar, "%d", &someInt ) ;



  if ( mLines != mVx ) {
    sprintf ( hip_msg, "expected %d nodal variables,"
              " found %d in gmsh_sol_header\n", mVx, mLines ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* For a 2D mesh, drop the z-velocity component, if given. */
  if ( mComp == 1 ) {
    /* Scalar. */
    ;
  }
  else if ( mComp == 3 ) {
    /* Vector, but read only 2 components in 2D. */
    ;
  }
  else
    hip_err ( fatal, 0, "variable dimension must be 1 or 3 in gmsh_sol_header\n" ) ;

  return ( mComp ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_add_el_bc:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  11Mar22: cut out from gmr_scan_sizes.
  

  Input:
  ------
  elBcTag: bc tag of this element

  Changes To:
  -----------
  pmBc: number of bcs
  pppBc: list of bcs
    
  Returns:
  --------
  index of the bc of this elem
  
*/

int gmr_add_el_bc ( int *pmBc, bc_struct ***pppBc, int elBcTag ) {
#undef FUNLOC
#define FUNLOC "in gmr_add_el_bc"
  
  ret_s ret = ret_success () ;

  /* Count bcs, include the 'internal' bc for the volume elems. */
  char bcText[TEXT_LEN] ;
  snprintf ( bcText, LINE_LEN-1, "%s%d", gmr_bc_name_prepend, elBcTag ) ;
  bc_struct *pBc ;
  int iBc ;
  if ( ( pBc = find_bc ( bcText, 2 ) ) ) {
    /* Bc already known to hip. New to this grid? */
    for ( iBc = 0 ; iBc < *pmBc ; iBc ++ )
      if ( pBc == (*pppBc)[iBc] )
        break ;
    if ( iBc == *pmBc ) {
      /* New to this grid, add. */
      *pppBc = arr_realloc ("pppBc in gmr_scan_sizes", NULL,  
                            *pppBc, (*pmBc)+1, sizeof ( **pppBc )) ;
      (*pppBc)[iBc] = pBc ;
      (*pmBc)++ ;
    }
  }
  else {
    /* New bc, add it. */
    *pppBc = arr_realloc ( "pppBc in gmr_scan_sizes", NULL, 
                           *pppBc, (*pmBc)+1, sizeof ( **pppBc )) ;
    iBc = *pmBc ;
    (*pppBc)[iBc] = find_bc ( bcText, 1 ) ;
    (*pmBc)++ ;
  }
  
  return ( iBc ) ;
}



/******************************************************************************

  gmsh_scan_el_v2:
  Scan the line describing an element or bnd fc and dissect the elements.
  Gmsh format 2.x
  
  Last update:
  ------------
  27Apr23; use gmr_bc_tag_pos to pick the bc no from the tags. 
  20Dec17; fix ULG format mismatches.
  15Mar11; read the first tag as bc.
  9Feb10: conceived.
  
  Input:
  ------
  Fmsh: positioned file.

  Changes To:
  -----------
  Fmsh: advanced file.

  Output:
  -------
  *pnr: element number,
  *iElT: element type,
  *pmiBc: 'entitiy' tag, typically the second one.
  *pmVx: number of forming nodes,
  nFrmVx: forming ndodes.
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmr_read_el_v2 ( FILE *Fmsh, ulong_t *pnr,
                     const elemType_struct **ppElT, int *piBc, 
                     int *pmVx, ulong_t nFrmVx[] ) {

  ulong_t iElT;
  int iRetVal ;
# define MAX_TAGS 10
  int mTags, iTag[MAX_TAGS] ;
  int k ;

  iRetVal = fscanf ( Fmsh, "%"FMT_ULG" %"FMT_ULG" %d", pnr, &iElT, &mTags ) ;
  if ( iRetVal !=3 ) {
    sprintf ( hip_msg, "failed to read tag for el %"FMT_ULG" in gmsh_scan_el.", *pnr ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  if ( mTags > MAX_TAGS ) {
    sprintf ( hip_msg, "gmsh_scan_el is configured for max %d tags, found %d.\n",
              MAX_TAGS, mTags ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( mTags < gmr_bc_tag_pos+1 ) {
    sprintf ( hip_msg, "You asked to use the %d-th tag as bc, but\n"
              "  elem %"FMT_ULG" only has %d tags.\n",
              gmr_bc_tag_pos+1, *pnr, mTags ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  /* Read the tags. */
  for ( k = 0 ; k < mTags ; k++ ) {
    iRetVal = fscanf ( Fmsh, "%d", iTag+k ) ;

    if ( iRetVal !=1 ) {
      sprintf ( hip_msg, "failed to read tag for el %"FMT_ULG" in gmsh_scan_el\n", *pnr ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  /* Use the first tag as bc, default option of gmsh. 
     Default 2nd tag is the id of the geometric entity that formst the elem.*/
  *piBc = iTag[gmr_bc_tag_pos] ;
    
  *ppElT = gmsh_elT ( iElT, pmVx ) ;

  int int_nFrmVx ;
  for ( k = 0 ; k < *pmVx ; k++ ) {
    /* replacement of %d with %"FMT_ULG" leads to double quote, convert explicity.*/
    iRetVal = fscanf ( Fmsh, "%d", &int_nFrmVx ) ;
    nFrmVx[k] = int_nFrmVx ;

    if ( iRetVal !=1 ) {
      sprintf ( hip_msg, "failed to read vertex for el %"FMT_ULG" in gmsh_scan_el\n", *pnr ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  fscanf ( Fmsh, "%*[^\n]" ) ;
  fscanf ( Fmsh, "%*[\n]" ) ;

  return (1) ;
}

 
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_scan_elems_v2:
*/
/*! scan all elements, find number per type, dimensions, bcs.
 *
 */

/*
  
  Last update:
  ------------
  11Mar22: extracted from gmr_scan_sizes.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

size_t gmr_scan_elems_v2 (FILE *Fmsh, 
                          ulong_t mElemsOfType[MAX_ELEM_TYPES], ulong_t *pmE1D,
                          gmr_tag_s *pTags,
                          int *pmBc, bc_struct ***pppBc
                          ) {
#undef FUNLOC
#define FUNLOC "in gmr_scan_elems_v2"
  
  ret_s ret = ret_success () ;

  // Do this when reading the elements.
  /* Were there already bcs defined in a PhysicalNames section? */
  //int needsBc = ( *pmBc ? 0 : 1 ) ;

  char *someStr = NULL ;
  size_t strSz = 0 ;
  getline ( &someStr, &strSz, Fmsh ) ;
  size_t mElLst ;
  sscanf ( someStr, "%lu", &mElLst ) ;

  
  int mVx ;
  ulong_t nFrmVx[MAX_VX_ELEM] ;
  ulong_t mE1D = 0 ;
  int maxDim = 0, nBcMaxDim = 0, mDimEl = 0, elBcTag, iBc, i ;
  ulong_t nr ; 
  const elemType_struct *pElT ;
  for ( i = 0 ; i < mElLst ; i++ ) {
    gmr_read_el_v2 ( Fmsh, &nr, &pElT, &elBcTag, &mVx, nFrmVx ) ;

    gmr_add_entity ( pTags, pElT->mDim, elBcTag ) ;

    if ( pElT && pElT->elType <= hex ) {
      mElemsOfType[pElT->elType]++ ;
      mDimEl = pElT->mDim ;
      maxDim = MAX(maxDim,mDimEl) ;
    }
    else if ( mVx == 2 ) {
      (*pmE1D)++ ;
      mDimEl = 2 ;
      maxDim = MAX(maxDim,mDimEl) ;
    }
    else if ( mVx == 1 )
      ; /* Bnd node, disregard. */
    else {
      sprintf ( hip_msg, 
                "unknown element with %d nodes in gmr_scan_sizes.\n", mVx ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    // Do this when reading the elements.
    /* if ( needsBc && mVx > 1 ) { */
    /*   iBc = gmr_add_el_bc ( pmBc, pppBc, elBcTag ) ; */
    /*   if ( mDimEl == maxDim ) nBcMaxDim = iBc ; */
    /* } */
  }

  // Create a matching list of physical names for the list of entities.
  gmr_create_physNm ( pTags, pmBc, pppBc ) ;

  free ( someStr ) ;
  return ( mElLst ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_el_v4:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------
  Fmsh: positioned mesh file
  pNr

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s gmr_read_el_v4 (  FILE *Fmsh, int mVx, ulong_t *pnr,
                        ulong_t nFrmVx[] ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_el_v4"
  
  ret_s ret = ret_success () ;
  

  // Watch out when adding binary read: gmsh binary has a size_t, not ulong_t.
  if ( fscanf ( Fmsh, "%"FMT_ULG"", pnr ) !=1 ) {
    sprintf ( hip_msg, "failed to read elem tag after el %"FMT_ULG" in gmsh_scan_el_v4.", *pnr ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  int k ;
  for ( k = 0 ; k < mVx ; k++ ) {
    if ( fscanf ( Fmsh, "%"FMT_ULG"", nFrmVx+k ) !=1 ) {
      sprintf ( hip_msg, "failed to read vertex for el %"FMT_ULG" in gmsh_scan_el_v4.", *pnr ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  
  fscanf ( Fmsh, "%*[^\n]" ) ;
  fscanf ( Fmsh, "%*[\n]" ) ;
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_scan_elems_v4:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

size_t gmr_scan_elems_v4 ( FILE *Fmsh, 
                           ulong_t mElemsOfType[MAX_ELEM_TYPES],
                           ulong_t *pmE1D //,int *pmBc, bc_struct ***pppBc
                           ) {
#undef FUNLOC
#define FUNLOC "in gmr_scan_elems_v4"


  char *someStr = NULL ;
  size_t strSz = 0 ;


  size_t mBlocks, mElLst, minElTag, maxElTag ;
  getline ( &someStr, &strSz, Fmsh ) ;
  sscanf ( someStr, "%lu %lu %lu %lu", &mBlocks, &mElLst, &minElTag, &maxElTag ) ;
  if ( minElTag != 1 || maxElTag != mElLst )
    hip_err ( warning, 1, "elem tags are not consecutive, order ignored" ) ;

  

  
  int iBc, iBl, kEl, mElRead = 0 ;
  int grpDim, grpTag, iElT ;
  const elemType_struct *pElT ;
  int maxDim = 0, mDimEl=0, mVx ;
  size_t mElGrp ;
  ulong_t elTag, nFrmVx[MAX_VX_ELEM] ;
  char bcText[TEXT_LEN] ;
  for ( iBl = 0 ; iBl < mBlocks ; iBl++ ) {
    // Read the block info.
    getline ( &someStr, &strSz, Fmsh ) ;
    if ( sscanf ( someStr, "%d %d %d %lu", &grpDim, &grpTag, &iElT, &mElGrp )
         != 4 ) {
      sprintf ( hip_msg, "not enough information for elem block %d "FUNLOC".", iBl ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    };

    if ( grpTag == 0 ) {
      // Does gmsh allow zero-valued group tags? I think not. Interpret as no tag, warn.
      hip_err ( warning, 1, "found a zero-valued group tag, not allowed in gmsh, ignoered.");
    }

    // Now mapped through gmr_scan_entities.
    /* else if ( grpTag > *pmBc || (*pppBc)[grpTag-1] == NULL ) { */
    /*   // bc/zone label not given in PhysicalNames, add it. */
    /*   snprintf ( bcText, LINE_LEN-1, "%s%d", gmr_bc_name_prepend, grpTag ) ; */
    /*   sprintf ( hip_msg, */
    /*             "no label for entityTag %d given in PhysicalNames, adding %s.", */
    /*             grpTag, bcText ) ; */
    /*   hip_err ( warning, 1, hip_msg ) ; */

    /*   if ( grpTag > *pmBc ) */
    /*     *pppBc = arr_realloc ("pppBc in gmr_scan_sizes", NULL,   */
    /*                           *pppBc, (*pmBc)+1, sizeof ( **pppBc ) ) ; */
    /*   for ( iBc = *pmBc ; iBc < grpTag ; iBc++ ) */
    /*     (*pppBc)[iBc] = NULL ; */
    /*   (*pppBc)[grpTag-1] = find_bc ( bcText, 1 ) ; */
    /*   *pmBc = grpTag ; */
    /* } */

    pElT = gmsh_elT ( iElT, &mVx ) ;
    if ( pElT && pElT->elType <= hex ) {
      // Note: bi comes after hex.
      mElemsOfType[pElT->elType] += mElGrp ;
      mDimEl = pElT->mDim ;
    }
    else if ( mVx == 2 ) {
      (*pmE1D) += mElGrp ;
      mDimEl = 2 ;
    }
    else if ( mVx == 1 )
      ; /* Bnd node, disregard. */
    else {
      sprintf ( hip_msg, 
                "unknown element with %d nodes in  "FUNLOC".", mVx ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    maxDim = MAX(maxDim,mDimEl) ;
    // now dealt with through gmr_scan_entities.
    //(*pppBc)[grpTag-1]->mark = mDimEl ;
 

    /* Step through the file to the next block. */
    for ( kEl = 0 ; kEl < mElGrp ; kEl++ ) {
      gmr_read_el_v4 ( Fmsh, mVx, &elTag, nFrmVx ) ; 
    }
  }

  free ( someStr ) ;
  return ( mElLst ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_elems:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  8jun23; add pTags
  14Mar22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t gmr_scan_elems ( FILE *Fmsh, float version,
                         ulong_t mElemsOfType[MAX_ELEM_TYPES], ulong_t *pmE1D,
                         gmr_tag_s *pTags, int *pmBc, bc_struct ***pppBc ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_elems"
  
  if ( !gmr_seek ( Fmsh, "Elements" ) )
    hip_err ( fatal, 0, "missing Elements section." ) ;

  ulong_t mElLst=0 ;
  if ( (int)version == 2 ) {
    /* Make a list of bcs used locally, since find_bc might re-discover
       a bc used in another grid, and hence wouldn't count that bc. */
    mElLst = gmr_scan_elems_v2( Fmsh, mElemsOfType, pmE1D,
                                pTags, pmBc, pppBc ) ;
  }
  else {
    // default is 4, test for correct format already done.
    mElLst = gmr_scan_elems_v4( Fmsh, mElemsOfType, pmE1D //, pmBc, pppBc
                                ) ;
  }

  return ( mElLst ) ;
}


/******************************************************************************

  gmr_scan_sizes:
  Scan the .msh file for numbers to allocate.
  
  Last update:
  ------------
  14Mar22; rename to _scan_ in line with other scan functions.
  3Apr20; use gmr_bc_name_prepend
  11May18; fix bug with writing beond mElemsOfType for bi elems.
  3Mar18; correctly increment mEq for separate variable files.
  20Dec17; fix ULG format mismatch.
  29Oct17; track a list of bcs for this grid, rather than assuming that
           all bc of this grid are new to hip.
  23Feb14; track $NodeData in main mesh file.
  15Dec11; fix loop exit bug with counting 3D elements. Fix bug with mConn count.
  9Feb10: conceived.
  
  Input:
  ------
  Fmsh: opened mesh file.
  *pmVarFl: number of variable files
  Fvar[]: names of variable files.

  Changes To:
  -----------
  Fmsh: rewound to beginning.
  version:
  *pmVarFl: set to zero if variables are found in main mesh file 

  Output:
  -------
  mDim: number of dimensions.
  mVx: numbers of vx.
  mEl, mElLst: numbers of elems to alloc, number of elem entries in the gmsh file.
  mConn: number of connectivity entries.
  mBndFc, mBnd: number of boundary faces and boundaries.
  mBc: number of bcs (physical names with mDim-1)
  pppBC: list of bc pointers
  pTags: tag and physical name mapping
  mZones: number of different volume tags, will become zones if > 1.
  mEq: number of equations, if any.
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmr_scan_sizes ( FILE *Fmsh, float version, int *pmVarFl, FILE *Fvar[],
                     int *pmDim, ulong_t *pmVx,
                     ulong_t *pmEl, ulong_t *pmElLst, ulong_t *pmConn, 
                     int *pmBndFc, int *pmBc, bc_struct ***pppBc,
                     gmr_tag_s *pTags,
                     int *pmZones, int *pmEq ) {
#undef FUNLOC
#define FUNLOC "in gmr_scan_sizes"

  char *someStr = NULL ;
  size_t strSz = 0 ;
  bc_struct *pBc ;
  int iVar ;

  /* Unknown dim so far. */
  *pmDim = 0 ;

  /* Nodes. Just read their number. */
  if ( !gmr_seek ( Fmsh, "Nodes" ) )
    hip_err ( fatal, 0, "missing Nodes section." ) ;
  
  getline ( &someStr, &strSz, Fmsh ) ;
  /* Issue: replacing "%d" with "%"FMT_ULG"" will put a fixed double quote.
     The numbers read here won't exceed int size, read
     that and explicity convert. */
  size_t mBlocks, mVxTotal, minVxTag, maxVxTag ;
  if ( (int)version == 2 )
    sscanf ( someStr, "%lu", &mVxTotal ) ;
  else {
    // default is 4, test for correct format already done.
    sscanf ( someStr, "%lu %lu %lu %lu", &mBlocks, &mVxTotal, &minVxTag, &maxVxTag ) ;
    if ( minVxTag != 1 || maxVxTag != mVxTotal )
      hip_err ( fatal, 0, "node tags are not consecutive, not yet suported." ) ;
  }
  *pmVx = mVxTotal ;




  
  /* Elems and boundary faces. */
  ulong_t mElemsOfType[MAX_ELEM_TYPES] = {0} ;
  ulong_t mE1D = 0 ;
  *pmElLst = gmr_scan_elems ( Fmsh, version, mElemsOfType, &mE1D,
                              pTags, pmBc, pppBc ) ;


  /* 2D or 3D? */
  ulong_t mE2D = mElemsOfType[tri]+mElemsOfType[qua] ;
  ulong_t mE3D=0 ;
  elType_e e ;
  for ( e = tet ; e <= hex ; e++ )
    mE3D += mElemsOfType[e] ;

  if ( mE3D ) {
    *pmDim = 3 ;
    *pmEl = mE3D ;
    *pmBndFc = mE2D ;
    for ( e = tet ; e <= hex ; e++ )
      *pmConn += mElemsOfType[e]*elemType[e].mVerts ;
  }
  else if ( mE2D ) {
    *pmDim = 2 ;
    *pmEl = mE2D ;
    *pmBndFc = mE1D ;
    *pmConn = 3*mElemsOfType[tri] + 4*mElemsOfType[qua] ;
  }
  else {
    sprintf ( hip_msg, "gmr_scan_sizes can't determine the dim of the grid"
              " found %"FMT_ULG" 1-D, %"FMT_ULG" 2-D and %"FMT_ULG" 3-D elements\n", mE1D, mE2D, mE3D ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  // a map of the tag to the physical name. gmsh 4 only.
  // we need to know what mDim is, before we can pick the
  // right entities.
  gmr_read_physNm ( Fmsh, version, *pmDim, pmBc, pppBc, pTags ) ;
  gmr_read_entities ( Fmsh, version, *pmDim, pTags ) ;

  
  /* Any variables listed in the main mesh file? */
  *pmEq = 0 ;
  int mComp ;
  char varName[TEXT_LEN] ;
  while ( ( mComp = 
            gmsh_sol_header ( Fmsh, *pmVx, *pmDim, varName, *pmEq ) ) ) {
    *pmEq += mComp ;
  }


  if ( *pmVarFl ) {
    if ( *pmEq ) {
      hip_err ( warning, 1, 
                "variables in mesh file used, disregarding sol. files\n" ) ;
      *pmVarFl = 0 ;
    }
    else
      for ( iVar = 0 ; iVar < *pmVarFl ; iVar++ ) {
        *pmEq += gmsh_sol_header ( Fvar[iVar], *pmVx, *pmDim, varName, *pmEq ) ;
      }
  }


  rewind ( Fmsh ) ;
  free ( someStr ) ;
  return (1) ;
}
/******************************************************************************

  gmr_next_var:
  Read one solution component from file.
  
  Last update:
  ------------
  14Mar22; rename to gmr_
  23Feb14; track var names.
  22Mar10; fix bug with reading grid only.
  19Feb10: conceived.
  
  Input:
  ------
  Fvar: opened file
  pUns:

  Changes To:
  -----------
  *pmCompRead: number of components (scalars +n*vec vars ) read.

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int gmr_next_var ( FILE *Fvar, uns_s *pUns, int *pmCompRead  ) {


  /* Position the file at the beginning of the variable listing. */
  int mVx =  pUns->mVertsNumbered ;
  char varName[LEN_VARNAME] ;
  int mComp = gmsh_sol_header ( Fvar, mVx, pUns->mDim, varName, *pmCompRead ) ;

  if ( !mComp )
    // No variables, or none left
    return ( 0 ) ;

  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;



  /* Variable name. */
  int nEq = *pmCompRead ;
  int mDim = pUns->mDim ;
  if ( mComp == 1 )
    /* scalar. */
    strncpy ( pVar[nEq].name, varName, LEN_VARNAME ) ;
  else if ( mComp == mDim ) {
    /* vector. */
    snprintf ( pVar[nEq+0].name, LEN_VARNAME, "%s_x", varName ) ;
    snprintf ( pVar[nEq+1].name, LEN_VARNAME, "%s_y", varName ) ;
    if ( mDim == 3 )
      snprintf ( pVar[nEq+2].name, LEN_VARNAME, "%s_z", varName ) ;
  }
  else {
    sprintf ( hip_msg, "mismatch between vector len %d and dim %d",
              *pmCompRead, mDim ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Read the components. */
  chunk_struct *pChunk = pUns->pRootChunk ;
  vrtx_struct *pVx, *pVxEnd = pChunk->Pvrtx + mVx ;
  int m1 = *pmCompRead;
  int m2 = *pmCompRead+mComp ;
  *pmCompRead += mComp ;
  int i ;
  for ( pVx = pChunk->Pvrtx+1 ; pVx <= pVxEnd ; pVx++ ) {
    fscanf ( Fvar, "%*d" ) ;
    for ( i = m1 ; i < m2 ; i++ )
      fscanf ( Fvar, "%lf", pVx->Punknown+i ) ;
    fscanf_end_line ( Fvar ) ;
  }

  return ( 1 ) ;
}


 
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_elems_v2:
*/
/*! read all elements and boundary faces gmsh 2.x formats
 *
 */

/*
  
  Last update:
  ------------
  11Mar22: extracted from gmr_scan_sizes.
  

  Input:
  ------
  Fmsh: file positioned after $Elements 
  mDim: spatial volume dimension of the mesh
  mEl: volume elements
  mBc: number of bcs
  ppBc: list of bcs.

  Changes To:
  -----------
  pChunk: adding elements and bnd faces.
    
  Returns:
  --------
  number of elems and bnd faces read.
  
*/

size_t gmr_read_elems_v2 (FILE *Fmsh, uns_s *pUns, gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_elems_v2"
  
  char *someStr = NULL ;
  size_t strSz = 0 ;
  getline ( &someStr, &strSz, Fmsh ) ;
  size_t mElLst ;
  sscanf ( someStr, "%lu", &mElLst ) ;

  
  int maxDim = 0, nBcMaxDim = 0, mDimEl, elBcTag, grpTag, i ;
  ulong_t nr ; 
  const elemType_struct *pElT ;
  chunk_struct *pChunk = pUns->pRootChunk ;
  reset_elems ( pChunk->Pelem+1, pChunk->mElems ) ;

  int elDim ;
  elem_struct *pEl ;
  vrtx_struct **PPvrtx ;
  bndFcVx_s *pBf ;
  ulong_t nEl ;
  int mVxEl ;
  ulong_t nFrmVx[MAX_VX_ELEM] ;
  int k ;
  gmr_ent_s *pEnt ;
  for ( pEl = pChunk->Pelem, PPvrtx = pChunk->PPvrtx, pBf = pUns->pBndFcVx, nEl = 1 ; 
        nEl <= mElLst ; nEl++ ) {
    gmr_read_el_v2 ( Fmsh, &nr, &pElT, &grpTag, &mVxEl, nFrmVx ) ;
    elDim  = ( pElT ? pElT->mDim : 0 ) ;

    if ( pElT && pElT->mDim == pUns->mDim ) {
      /* Add this volume element. */

      /* Nothing points into the elements, so reorder this list. */
      pEl++ ;
      if ( pEl > pChunk->Pelem+pChunk->mElems ) 
        hip_err ( fatal, 0, "too many elements in read_gmsh.\n" ) ;

      pEl->elType = pElT->elType ;
      pEl->PPvrtx = PPvrtx ;
    
      for ( k = 0 ; k < mVxEl ; k++ ) 
        *PPvrtx++ = pChunk->Pvrtx + nFrmVx[ g2h[pEl->elType][k] ] ;
    }
    else if ( ( pUns->mDim == 3 && elDim == 2 ) ||
              pUns->mDim == mVxEl ) {
      /* Tri or qua boundary face in 3D, or Linear bnd. fc in 2D. */
      if ( pBf > pUns->pBndFcVx+pUns->mBndFcVx ) 
        hip_err ( fatal, 0, "too many boundary faces "FUNLOC".\n" ) ;

      pBf->mVx = mVxEl ;
      pEnt = gmr_find_entity ( pTags, pElT->mDim, grpTag ) ;
      pBf->pBc = pEnt->pPhysNm->pBc ;

      /* Forming vertices. . */
      for ( k = 0 ; k < mVxEl ; k++ ) {
        pBf->ppVx[k] = pChunk->Pvrtx + nFrmVx[k] ;
      }
      pBf++ ;
    }
  }

  free ( someStr ) ;
  return ( mElLst ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_create_zones:
*/
/*! for each bc labelling a volumetric entity, create a zone, 
 *  if there's more than one.
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void gmr_create_zones ( uns_s *pUns, gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_create_zones"


  // Do we need zones?
  pTags->mZones = 0 ;
  gmr_physNm_s *pPNm ;
  for ( pPNm = pTags->pPhysNm ;
        pPNm <  pTags->pPhysNm + pTags->mPhysNm ; pPNm++ ) {
    if ( pPNm->iDim == pUns->mDim )
      (pTags->mZones)++ ;
  }
  
  
  if ( pTags->mZones < 2 )
    // No need to create a single zone.
    pUns->mZones = 0 ;
  
  else {
    // Create a zone for each bc on maxDim elems.
    for ( pPNm = pTags->pPhysNm ;
          pPNm < pTags->pPhysNm + pTags->mPhysNm ; pPNm++ ) {
      if ( pPNm->iDim == pUns->mDim )
        pPNm->iZone = zone_add ( pUns, pPNm->label, 0, 0 ) ;
      pUns->mZones++ ;
    }
  }


  return ;
}
 
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_elems_v4:
*/
/*! read all elements and boundary faces gmsh 2.x formats
 *
 */

/*
  
  Last update:
  ------------
  11Mar22: extracted from gmr_scan_sizes.
  

  Input:
  ------
  Fmsh: file positioned after $Elements 
  mDim: spatial volume dimension of the mesh
  mEl: volume elements
  mBc: number of bcs
  ppBc: list of bcs.

  Changes To:
  -----------
  pChunk: adding elements and bnd faces.
    
  Returns:
  --------
  number of elems and bnd faces read.
  
*/

size_t gmr_read_elems_v4 (FILE *Fmsh, uns_s *pUns,
                          gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_elems_v4"

  // Allocate a list of zones and their grpTag
  gmr_create_zones ( pUns, pTags ) ;

  chunk_struct *pChunk = pUns->pRootChunk ;
  elem_struct *pEl = pChunk->Pelem ;
  vrtx_struct **PPvrtx = pChunk->PPvrtx ;
  bndFcVx_s *pBf = pUns->pBndFcVx ;
  reset_elems ( pChunk->Pelem+1, pChunk->mElems ) ;
  
  char *someStr = NULL ;
  size_t strSz = 0 ;

  size_t mBlocks, mElLst, minElTag, maxElTag ;
  getline ( &someStr, &strSz, Fmsh ) ;
  sscanf ( someStr, "%lu %lu %lu %lu", &mBlocks, &mElLst, &minElTag, &maxElTag ) ;



  
  int iBl, kEl, mElRead = 0 ;
  int grpDim, grpTag, iElT ;
  const elemType_struct *pElT ;
  int maxDim = 0, mDimEl=0, mVxEl, iBc, nBcMax = 0 ;
  size_t mElGrp ;
  ulong_t elTag, nFrmVx[MAX_VX_ELEM] ;
  int k ;
  int iZone ;
  gmr_ent_s *pEnt ;
  for ( iBl = 0 ; iBl < mBlocks ; iBl++ ) {
    getline ( &someStr, &strSz, Fmsh ) ;
    if ( sscanf ( someStr, "%d %d %d %lu", &grpDim, &grpTag, &iElT, &mElGrp )
         != 4 ) {
      sprintf ( hip_msg, "not enough information for elem block %d "FUNLOC".", iBl ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    };
    

    pElT = gmsh_elT ( iElT, &mVxEl ) ;
    if ( pElT && pElT->mDim == pUns->mDim ) {
      // Vol elem
      if ( pTags->mZones && grpTag != 0 ) {
        // Zones are given.
        iZone = gmr_find_zone ( pTags, pElT->mDim, grpTag ) ;
      }
      else
        iZone = 0 ;

      // Read elems in the block.
      for ( kEl = 0 ; kEl < mElGrp ; kEl++ ) {
        gmr_read_el_v4 ( Fmsh, mVxEl, &elTag, nFrmVx ) ;

        /* Nothing points into the elements, so reorder this list. */
        pEl++ ;
        if ( pEl > pChunk->Pelem+pChunk->mElems ) 
          hip_err ( fatal, 0, "too many elements in read_gmsh.\n" ) ;

        pEl->elType = pElT->elType ;
        pEl->iZone = iZone ;
        pEl->PPvrtx = PPvrtx ;
        for ( k = 0 ; k < mVxEl ; k++ ) 
          *PPvrtx++ = pChunk->Pvrtx + nFrmVx[ g2h[pEl->elType][k] ] ;
      }
    }

    else if ( pElT && pElT->mDim == pUns->mDim-1 ) {
      // bnd face
      pEnt = gmr_find_entity ( pTags, pElT->mDim, grpTag ) ;
      if ( !pEnt ) {
        sprintf ( hip_msg, "found group tag %d,"
                  " but no matching entity "FUNLOC".", grpTag ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else if ( !pEnt->pPhysNm ) {
        sprintf ( hip_msg, "found group tag %d,"
                  " but no match in physical names "FUNLOC".", grpTag ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else if ( !pEnt->pPhysNm->pBc ) {
        sprintf ( hip_msg, "found group tag %d,"
                  " but no bc with matching physical name "FUNLOC".", grpTag ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      
      for ( kEl = 0 ; kEl < mElGrp ; kEl++ ) {
        gmr_read_el_v4 ( Fmsh, mVxEl, &elTag, nFrmVx ) ;
        if ( pBf > pUns->pBndFcVx + pUns->mBndFcVx ) 
          hip_err ( fatal, 0, "too many boundary faces "FUNLOC".\n" ) ;

        pBf->mVx = mVxEl ;
        // gmsh numbers from 1, ppBc runs from zero. 
        pBf->pBc = pEnt->pPhysNm->pBc ;

        /* Forming vertices. . */
        for ( k = 0 ; k < mVxEl ; k++ ) {
          pBf->ppVx[k] = pChunk->Pvrtx + nFrmVx[k] ;
        }
        pBf++ ;
      }
    } // if pElT
  } // loop iBl

  free ( someStr ) ;
  return ( mElLst ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_elems:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------
  Fmsh: opened gmsh file
  version: 2 or 4 is supported.
  pUns: grid
  ptag2label: a list of tags for each bc (physical entity) label.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t gmr_read_elems ( FILE *Fmsh, float version, uns_s *pUns,
                         gmr_tag_s *pTags ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_elems"
  

  // We already know it exists from the scan.
  if ( !gmr_seek ( Fmsh, "Elements" ) ) ;

  ulong_t mElLst ;
  if ( (int)version == 2 ) {
    /* Make a list of bcs used locally, since find_bc might re-discover
       a bc used in another grid, and hence wouldn't count that bc. */
    mElLst = gmr_read_elems_v2( Fmsh, pUns, pTags ) ;
  }
  else {
    // default is 4, test for correct format already done.
    mElLst = gmr_read_elems_v4( Fmsh, pUns, pTags ) ;
  }

  return ( mElLst ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_vx_v2:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t gmr_read_vx_v2 ( FILE *Fmsh, uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_vx_v2"
  
  char *someStr = NULL ;
  size_t strSz = 0 ;
  
  size_t mVx = 0 ;
  getline ( &someStr, &strSz, Fmsh ) ;
  sscanf ( someStr, "%lu", &mVx ) ;


  /* Read. Note that vertices are reset after alloc in make_uns_grid. */
  ulong_t nVx, nr ;
  int int_nr ;
  vrtx_struct *pVx ;
  double *pCo ;
  for ( nVx = 1 ; nVx <= mVx ; nVx++ ) {
    fscanf ( Fmsh, "%d", &int_nr ) ;
    nr = int_nr ;
    pVx = pUns->pRootChunk->Pvrtx + nr ;
    pVx->number = nr ;
    pCo = pVx->Pcoor ;

    fscanf ( Fmsh, "%lf %lf", pCo, pCo+1 ) ;
    if ( pUns->mDim == 3 )     fscanf ( Fmsh, "%lf", pCo+2 ) ;
    fscanf ( Fmsh, "%*[^\n]" ) ;
    fscanf ( Fmsh, "%*[\n]" ) ;
  }

  
  free( someStr ) ;
  return ( mVx ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_vx_v4:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t gmr_read_vx_v4 ( FILE *Fmsh, uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_vx_v4"
  
  char *someStr = NULL ;
  size_t strSz = 0 ;
  
  size_t mBlocks, mVx, minVxTag, maxVxTag ;
  getline ( &someStr, &strSz, Fmsh ) ;
  sscanf ( someStr, "%lu %lu %lu %lu", &mBlocks, &mVx, &minVxTag, &maxVxTag ) ;



  /* Read. Note that vertices are reset after alloc in make_uns_grid. */
  size_t grpDim, grpTag, isParam, mVxGrp ;
  ulong_t nVx = 0 ;
  size_t *nr = NULL ;
  vrtx_struct *pVx = pUns->pRootChunk->Pvrtx ;
  int iBl ;
  ulong_t kVx ;
  double *pCo ;
  for ( iBl = 0 ; iBl < mBlocks ; iBl++ ) {
    getline ( &someStr, &strSz, Fmsh ) ;
    sscanf ( someStr, "%lu %lu %lu %lu", &grpDim, &grpTag, &isParam, &mVxGrp ) ;

    nr = arr_realloc ( "nr "FUNLOC"", pUns->pFam, nr, mVxGrp, sizeof( *nr ) ) ; 
    
    for ( kVx = 0 ; kVx < mVxGrp ; kVx++ ) {
      getline ( &someStr, &strSz, Fmsh ) ;
      // Read the number tag, but non-consecutive numbering is currently experimental.
      sscanf ( someStr, "%lu", nr+kVx ) ;
    }
    
    for ( kVx = 0 ; kVx < mVxGrp ; kVx++ ) {
      pVx = pUns->pRootChunk->Pvrtx + nr[kVx] ;
      pVx->number = nr[kVx] ;
      pCo = pVx->Pcoor ;

      // 4.x formats always carry a z coor, also in 2D. 
      getline ( &someStr, &strSz, Fmsh ) ;
      sscanf ( someStr, "%lf %lf %lf", pCo, pCo+1, pCo+2 ) ;
    }
  }

  
  free ( someStr ) ;
  arr_free ( nr ) ;
  return ( mVx ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_vertices:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t gmr_read_vertices ( FILE *Fmsh, const float version, uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_vertices"

  ulong_t mVx = 0 ;

  gmr_seek ( Fmsh, "Nodes" ) ;

  if ( (int)version == 2 ) {
    /* Make a list of bcs used locally, since find_bc might re-discover
       a bc used in another grid, and hence wouldn't count that bc. */
    mVx = gmr_read_vx_v2( Fmsh, pUns ) ;
  }
  else {
    // default is 4, test for correct format already done.
    mVx = gmr_read_vx_v4( Fmsh, pUns ) ;
  }


  return ( mVx ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_read_sol:
*/
/*! Read a gmsh solution, either from mesh file, and/or a set of variable files.
 *
 */

/*
  
  Last update:
  ------------
  14Mar22: extracted from gmr_read_lvl
  

  Input:
  ------
  Fmsh: mesh file
  Fvar: list of variable files
  mVarFl: number of variable files
  version: 2.x and 4.x are supported, have the same format, so not used yet.

  Changes To:
  -----------
  pUns: solution on the mesh
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s gmr_read_sol (  FILE *Fmsh, FILE *Fvar[], int mVarFl, const float version, uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in gmr_read_sol"
  
  ret_s ret = ret_success () ;
  var_s *pVar ;
  int foundCons = 0, foundPrim = 0, foundVec[MAX_DIM] = {0} ;
  int len ;
  int mEq = pUns->varList.mUnknowns ;
  int i, mCompRead ;  
  if ( mEq ) {
    pUns->varList.varType = prim ;
    if ( !mVarFl ) {
      rewind ( Fmsh ) ;
      /* Read variables from main mesh file. */
      while ( gmr_next_var ( Fmsh, pUns, &mCompRead ) ) {
        ;
      }
    }
    else
      for ( mCompRead = i = 0 ; i < mVarFl ; i++ ) {
        rewind ( Fvar[i] ) ;
        gmr_next_var ( Fvar[i], pUns, &mCompRead ) ;
      }
      

    /* Default variable cats and names. */
    for ( i = 0 ; i <= mEq ; i++ ) {
      pVar =  pUns->varList.var + i ;
      pVar->flag = 1 ;

      if ( !strncmp( pVar->name, "Density", sizeof( "Density" ) ) ||
           !strncmp( pVar->name, "density", sizeof( "Density" ) )||
           !strncmp( pVar->name, "rho", sizeof( "rho" ) )
         ) {
        strcpy ( pVar->name, "rho" ) ;
        pVar->cat = ns ;
      }
      else if ( !strncmp( pVar->name, "Velocity_x", sizeof( "Velocity_x" ) ) ||
           !strncmp( pVar->name, "velocity_x", sizeof( "velocity_x" ) ) ||
           !strncmp( pVar->name, "u", sizeof( "u" ) )
         ) {
        strcpy ( pVar->name, "u" ) ;
        pVar->cat = ns ;
        foundPrim++ ;
        set_var_vec ( &(pUns->varList), i+1, 1 ) ;
        foundVec[0]++ ;
      }
      else if ( !strncmp( pVar->name, "Velocity_y", sizeof( "Velocity_y" ) ) ||
           !strncmp( pVar->name, "velocity_y", sizeof( "velocity_y" ) )||
           !strncmp( pVar->name, "v", sizeof( "v" ) )
         ) {
        strcpy ( pVar->name, "v" ) ;
        pVar->cat = ns ;
        foundPrim++ ;
        set_var_vec ( &(pUns->varList), i+1, 2 ) ;
        foundVec[1]++ ;
      }
      else if ( !strncmp( pVar->name, "Velocity_z", sizeof( "Velocity_z" ) ) ||
           !strncmp( pVar->name, "velocity_z", sizeof( "velocity_z" ) )||
           !strncmp( pVar->name, "w", sizeof( "w" ) )
         ) {
        strcpy ( pVar->name, "w" ) ;
        pVar->cat = ns ;
        foundPrim++ ;
        set_var_vec ( &(pUns->varList), i+1, 3 ) ;
        foundVec[2]++ ;
      }
      else if ( !strncmp( pVar->name, "Relative Stat. Pres.", 
                          sizeof( "Relative Stat. Pres." ) ) ||
           !strncmp( pVar->name, "Pressure", sizeof( "Pressure" ) ) ||
           !strncmp( pVar->name, "pressure", sizeof( "pressure" ) ) ||
           !strncmp( pVar->name, "p", sizeof( "p" ) )
         ) {
        strcpy ( pVar->name, "p" ) ;
        pVar->cat = ns ;
        foundPrim++ ;
      }
      else if ( !strncmp( pVar->name, "rhou", sizeof( "rhou" ) ) ) {
        /* Correctly named cons vel vars. */
        pVar->cat = ns ;
        foundCons++ ;
        set_var_vec ( &(pUns->varList), i+1, 1 ) ;
        foundVec[0]++ ;
      } 
      else if ( !strncmp( pVar->name, "rhou", sizeof( "rhov" ) ) ) {
        /* Correctly named cons vel vars. */
        pVar->cat = ns ;
        foundCons++ ;
        set_var_vec ( &(pUns->varList), i+1, 2 ) ;
        foundVec[1]++ ;
      } 
      else if ( !strncmp( pVar->name, "rhov", sizeof( "rhow" ) ) ) {
        /* Correctly named cons vel vars. */
        pVar->cat = ns ;
        foundCons++ ;
        set_var_vec ( &(pUns->varList), i+1, 3 ) ;
        foundVec[2]++ ;
      } 
      else if ( !strncmp( pVar->name, "rhoE", sizeof( "rhoE" ) ) ) {
        /* Correctly named cons energy. */
        pVar->cat = ns ;
        foundCons++ ;
      }
      else {
        /* Don't recognise this var name. */
        pVar->cat = other ;

        /* Is it a vector.? Check for _x, _y, or _z ending. */
        len = strlen ( pVar->name ) ;
        if ( pVar->name[len-2] == '_' ) {

          if (  pVar->name[len-1] == 'x' ) {
            /* x-cmponent. */
            set_var_vec ( &(pUns->varList), i+1, 1 ) ;
            foundVec[0]++ ;            
          }
          else if ( pVar->name[len-1] == 'y' ) {
            /* y-cmponent. */
            set_var_vec ( &(pUns->varList), i+1, 2 ) ;
            foundVec[1]++ ;            
          }if (  pVar->name[len-1] == 'z' ) {
            /* z-cmponent. */
            set_var_vec ( &(pUns->varList), i+1, 3 ) ;
            foundVec[2]++ ;            
          }
        }
      } 
    }

    if ( foundCons && foundPrim ) {
      sprintf ( hip_msg, "found %d primitive and %d conservative flow vars\n"
                "         Writing to file may fail.\n", foundPrim, foundCons );
      hip_err ( warning, 1, hip_msg ) ;
    }
    else if ( foundPrim + foundCons - pUns->mDim -1 ) {
      sprintf ( hip_msg, 
                "found %d flow vars, (apart from density), expecting %d.\n"
                "         Writing to file may fail.\n", 
                foundPrim + foundCons, pUns->mDim-1 );
      hip_err ( warning, 1, hip_msg ) ;
    }
    else if ( foundVec[0] != foundVec[1] ||
              ( foundVec[0] != foundVec[2] && pUns->mDim == 3 ) ) {
      sprintf ( hip_msg, "found %d x, %d y and %d z vector components\n"
                "         Writing to file may fail.\n", 
                foundVec[0], foundVec[1], foundVec[2] ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }


    check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 
  }
  else  
    pUns->varList.varType = noVar ;



  return ( ret ) ;
}


/******************************************************************
 
 gmr_read_lvl
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an unstructured .msh file.

 Last update:
 ------------
 27Apt23; add arg which tag to use for bc.
 27Jan23; pacify compiler, zero mZones at entry.
 14Mar22; support 4.x format
 3Apr20; make private.
 18Apr18; fix bug with null pElT for edges in 3D meshes.
 3Mar18; when no sol present, set noVar, not noType.
 20Dec17; fix ULG format mismatch.
 1Jul16; new interface to check_uns.
 23Mar14; analyse flow variable names. 
 23Feb14; reset mVarFl to zero in get_sizes if vars found in mesh file.
 6Jul13; new interface to make_uns_grid.
 6Apr13; removed unused variables.
 8Nov10; ingro g2h to map prisms correctly.
 14Sep10; new interface to make_uns_grid, rely on the vertex initialisation there.
 18Mar10; allow crlf dos files, fix bug in header reading.
 21Feb10; include reading variables.
 7Feb10; derived from read_uns_dpl and read_uns_saturne.

 Input:
 ------
 fileName: mesh file with geometry only.
 mVarFl: number of variable files
 varFileNm: array with variable file names.

 Changes to:
 -----------
 Grids

 Returns:
 --------
 0 on failure, 1 on success.
*/

ret_s gmr_read_lvl( char* fileName, int mVarFl, char varFileNm[][TEXT_LEN] ) {
  ret_s ret  = ret_success () ;

  FILE *Fmsh, *Fvar[MAX_UNKNOWNS] ;
  int iVar ;  
  int mDim ;
  ulong_t mVx ;
  ulong_t mEl, mConn ;
  int mEq ;
  grid_struct *pGrid ;
  chunk_struct *pChunk ;
  

  /* This will be the next grid. */
  sprintf ( hip_msg, "    Reading unstructured gmsh"); 
  hip_err ( blank, 2, hip_msg ) ;


  /* Read header, ist it ASCII 2.0? */
  float version ; 
  if ( !(Fmsh = gmr_open_ascii ( fileName, &version ) ) ) {
    ret = hip_err ( warning, 1, "failed to read gmsh file." )  ;
    return ( ret ) ;
  };
  for ( iVar = 0 ; iVar < mVarFl ; iVar++ )
    Fvar[iVar] = gmr_open_ascii ( varFileNm[iVar], &version ) ;
  
  int mBc = 0 ;
  int mBndFc ;
  int mZones=0 ;
  ulong_t mElLst=0 ;
  bc_struct **ppBc = NULL ;
  gmr_tag_s tags = {0} ;
  /* Scan for sizes. 
     Gmsh could list nodes and elems out of order, so work by the
     given index. mEl is the max el no found, i.e. alloc for that many,
     but mElLst is how many list entries (lines in the file) there are. */
  gmr_scan_sizes ( Fmsh, version, &mVarFl, Fvar,  
                   &mDim, &mVx, &mEl, &mElLst, &mConn, 
                   &mBndFc, &mBc, &ppBc, &tags,
                   &mZones, &mEq ) ;

  sprintf ( hip_msg, "      Found %"FMT_ULG" coordinates for %d-D grid.",mVx, mDim ) ;
  hip_err ( blank, 2, hip_msg ) ;
  sprintf ( hip_msg, "      Found %"FMT_ULG" elements.",mEl ) ;
  sprintf ( hip_msg, "      Found %d boundaries/zones.",mBc);
  sprintf ( hip_msg, "      Found %d boundary faces.",mBndFc);


  /* Alloc. */
  uns_s *pUns = NULL ;
  pGrid = make_uns_grid ( &pUns, mDim, mEl, mConn, 0, mVx, mEq, mBndFc, mBc ) ;
  pChunk = pUns->pRootChunk ;
  pUns->ppBc = ppBc ;
  pUns->mZones = mZones ;
  pUns->varList.mUnknowns = pUns->varList.mUnknFlow = mEq ;


  /* Alloc for temporary list of boundary faces given by forming nodes. */
  if ( verbosity > 3 )
    printf ( "       Reading %d boundary faces.\n", mBndFc ) ;

  /* Alloc a temporary list of boundary faces to be sorted and a list of
     boundary conditions using the upper bound given in the file header. */
  pUns->pBndFcVx = 
    arr_malloc ( "pUns->pBndFcVx in read_gmsh", pUns->pFam,
                 mBndFc, sizeof ( bndFcVx_s ) );
  pUns->mBndFcVx = mBndFc ;




  /* Vertex coordinates, Pcoor starts with index 1. Gmsh
     could list nodes out of order. */
  if ( verbosity > 3 )
    printf ( "       Reading %"FMT_ULG" nodes.\n", mVx ) ;
  rewind ( Fmsh ) ;

  gmr_read_vertices ( Fmsh, version, pUns ) ;
  


  

  /* Read grid connectivity, including boundary faces. */
  if ( verbosity > 3 )
    printf ( "       Reading %"FMT_ULG" elements.\n", mElLst ) ;
  gmr_read_elems( Fmsh, version, pUns, &tags ) ;
  gmr_free_tag ( &tags ) ;


  
  /* Match boundary faces. */
  if ( !match_bndFcVx ( pUns ) )
    hip_err ( fatal, 0, "could not match boundary faces in read_gmsh.\n" ) ;

  arr_free ( pUns->pBndFcVx ) ; 







  /* Validate, count and number the grid. */
  check_uns ( pUns, check_lvl ) ;



  /* Read solution, if any. */
  gmr_read_sol ( Fmsh, Fvar, mVarFl, version, pUns ) ;


  fclose ( Fmsh ) ;

  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  gmr_args:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived a long time ago
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void gmr_args ( char argLine[],
                char gridFile[LINE_LEN],
                int *pmVarFl,
                char varFileNm[MAX_UNKNOWNS][TEXT_LEN] ) {

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  gridFile[0] = '\0' ;
  int mVarFile = 0 ;

  /* Parse line of unix-style optional args. */
  char c ;
  while ((c = getopt_long ( mArgs, ppArgs, "b::t::",NULL,NULL)) != -1) {
    switch (c)  {
      case 'b':
        if ( !optarg )
          /* 0 arg given. use -b0, no change of bc names */
          gmr_bc_name_prepend[0] = '\0' ;
        else if ( optarg[0] == '0' )
          /* 0 arg given. use -b0, no change of bc names */
          gmr_bc_name_prepend[0] = '\0' ;
        else 
          snprintf ( gmr_bc_name_prepend, LINE_LEN-1, "%s_", optarg ) ;
        break;
      case 't':
        if ( !optarg )
          /* No arg given. use -b0, no change of bc names */
          gmr_bc_tag_pos = 0 ;
        else
          // Let the user number from 1, but internally number from 0
          gmr_bc_tag_pos = atoi(optarg)-1 ;
        break;
      case '?':
        if (isprint (optopt)) {
          sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
          hip_err ( warning, 1, hip_msg ) ;
          break ;
        }
        else {
          sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
          hip_err ( warning, 1, hip_msg ) ;
          break ;
        }
      default:
        sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    }
  
  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( gridFile, ppArgs[optind] ) ;
  else
    hip_err ( fatal, 0, "missing grid file name for read gmsh\n" ) ;

  *pmVarFl = mArgs-optind-1 ;
  if ( mArgs-1 > MAX_UNKNOWNS ) {
    sprintf ( hip_msg, "too many unknowns, only read the first %d.",
              MAX_UNKNOWNS ) ;
    hip_err ( warning, 1, hip_msg ) ;
    *pmVarFl = MAX_UNKNOWNS ;
  }

  int i ;
  for ( i = 0 ; i < *pmVarFl ; i++ ) {
    strcpy (  varFileNm[i], ppArgs[optind+2+i] ) ;
  }


  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  read_gmsh:
*/
/*! brief descr for doxygen
 *
 * Read a mesh and possibliy solution file in hdf5.
 *
 */

/*
  
  Last update:
  ------------
  3Apr20: conceived.
  

  Input:
  ------
  argline: command line string with linux style args

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s read_gmsh ( char* argLine ) {
  ret_s ret = ret_success () ;

  char gridFile[LINE_LEN] ;
  int mVarFl ;
  char varFileNm[MAX_UNKNOWNS][TEXT_LEN] ;

  gmr_flag_reset () ;
  gmr_args ( argLine, gridFile, &mVarFl, varFileNm ) ;
  
  
  ret = gmr_read_lvl( gridFile, mVarFl, varFileNm ) ;

  
  return ( ret ) ;
}
