/*
  buffer_uns:
  Refine elements in order to buffer hanging nodes. No new nodes on the surface
  of an element may be created at this stage.
  
  Last update:
  15May19; use hip_err.
  
  This file contains:
  ---------
  buffer_uns:
  buffer_elems:
  debuffer_uns:
  debuffer_elem:
  
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const int bitEdge[MAX_EDGES_ELEM] ;
extern Grids_struct Grids ;
extern int check_lvl ;
extern char hip_msg[] ;

/******************************************************************************

  buffer_uns:
  Buffer elements with hanging nodes.
  
  Last update:
  ------------
  31May19; leave PbufChunk in linked list of chunks, intro test condition for
           pChunk=pBufChunk in loops of called functions.
  18Jun06; intro pUns->eps.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int buffer_uns ( uns_s *pUns ) {
  
  int mNewElems, mNewElem2ChildP, mNewElem2VertP, mNewVerts,
    mNewBndFc, mNewBndPatch, mOldVerts, mOldElems, change ;
  chunk_struct *pBufChunk ;

  pUns->isBuffered = 1 ;
  
  mNewElems = mNewElem2ChildP = mNewElem2VertP = 0 ;
  mNewVerts = mNewBndFc = mNewBndPatch = 0 ;
  mOldVerts = pUns->nHighestVertNumber ;
  mOldElems = pUns->nHighestElemNumber ;

  if ( pUns->mDim == 2 )
    count_buf_elems_2D ( pUns, &mNewElems, &mNewElem2VertP ) ;
  else 
    estim_buf_elems_3D ( pUns, &mNewElems, &mNewElem2VertP, &mNewVerts,
			 &mNewBndFc, &mNewBndPatch ) ;
  
  mNewElem2ChildP = mNewElems ;

  if ( !mNewElems )
    /* No buffering necessary. */
    return ( 1 ) ;


  /* Append a new chunk. */
  pBufChunk = append_chunk ( pUns, pUns->mDim,
                             mNewElems, mNewElems*5, mNewElem2ChildP,
                             mNewVerts, mNewBndFc, mNewBndPatch ) ;
  pBufChunk->pVxCpt = arr_calloc ( "pVxCpt in buffer_uns", pUns->pFam,
                                   mNewVerts+1, sizeof (cpt_s ) ) ;



  /* Buffering of highly stretched cells in 3D can drastically change hMin.
     Set it to 0 and recalculate. */
  Grids.epsOverlap = Grids.epsOverlapSq = 0. ;
  pUns->epsOverlap = pUns->epsOverlapSq = 0. ;
  if ( !buffer_elems ( pUns, pBufChunk ) ) {
    sprintf ( hip_msg, "could not buffer grid in buffer_uns.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  /* Recalculate hMin. */
  check_elems ( pUns, &change, check_lvl ) ;
  pUns->epsOverlap = .9*pUns->hMin ;
  pUns->epsOverlapSq = pUns->epsOverlap*pUns->epsOverlap ;
  Grids.epsOverlap = pUns->epsOverlap ;
  Grids.epsOverlapSq = pUns->epsOverlapSq ;
  
  
  
  /* Count and number. */
  pUns->numberedType = noNum ;
  number_uns_grid ( pUns ) ;

    check_uns( pUns, 5 ) ;
#ifdef DEBUG
  check_uns ( pUns ) ;
#endif
  
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "buffered grid to %"FMT_ULG" (+%"FMT_ULG") elements, %"FMT_ULG" (+%"FMT_ULG") verts\n",
	     pUns->mElemsNumbered, pBufChunk->mElemsNumbered,
	     pUns->mVertsNumbered, pBufChunk->mVertsNumbered ) ;
    hip_err ( info, 1, hip_msg ) ;
  }
  return ( 1 ) ;
}
 

/******************************************************************************

  buffer_elems:
  Buffer a grid.
  
  Last update:
  ------------
  31May19; exclude PbufChunk from loops, so does not need to be removed from the linked list.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int buffer_elems ( uns_s *pUns, chunk_struct *PbufChunk ) {
  
  const llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  const int mDim = pUns->mDim ;

  chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  const vrtx_struct *Pvrtx1, *Pvrtx2 ;
  vrtx_struct *PvxElem[MAX_REFINED_VERTS] ;
  const elemType_struct *PelT ;
  int nAe, kEdge, kVxHg[MAX_ADDED_VERTS], mVxHg,
    mOldElems = 0, mOldVerts = 0, dir ;
  childSpc_s childSpc ;
  surfTri_s *PsurfTri ;

  /* Count the elements and vertices in the current grid. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    if ( Pchunk != PbufChunk ) {
      mOldElems += Pchunk->mElems ;
      mOldVerts += Pchunk->mVerts ; }
  pUns->nHighestElemNumber = mOldElems ;
  pUns->nHighestVertNumber = mOldVerts ;

  /* Initialize the childSpc. */
  init_childSpc ( &childSpc, pUns, PbufChunk ) ;
  
  /* Loop over all valid elements. Find the buffered leaf elements with
     hanging vertices and debuffer them. Buffer the parents that have become
     leafs in the next loop. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    /* Loop over all cells. Find the refined sides. */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems && Pchunk != PbufChunk ; Pelem++ )
      if ( Pelem->leaf & !Pelem->term ) {
	/* This is a buffered leaf. */
	PelT = elemType + Pelem->elType ;
	/* Find all edges of this element. No need to check faces, since
	   a refined face with unrefined edges cannot occur. */
	for ( kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ ) {
          nAe = get_elem_edge ( pllAdEdge, Pelem, kEdge, &Pvrtx1, &Pvrtx2, &dir ) ;
          if ( nAe && pUns->pAdEdge[nAe].cpVxMid.nr ) {
	    /* This is a hanging edge from the coarse side. */
	    debuffer_elem ( Pelem ) ;
	    break ;
	  }
	}
      }

  /* Loop over all valid elements. */
  const int doCheckVol = 1, doBuf = 1 ;
  int makesNegVol, doesAddCtrVx ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    /* Loop over all cells. Find the refined sides. */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems && Pchunk != PbufChunk ; Pelem++ ) {
      Pelem->mark = 0 ;
      if ( Pelem->leaf ) {

        /* Add cross edges for all quad faces with 4 hanging nodes. */
        int mVxAdded = add_elem_crossFc ( pUns, Pelem,
                                          childSpc.PnewChunk,
                                          &(childSpc.PlstVrtx),
                                          &(childSpc.PlstCoor),
                                          &(childSpc.PlstUnknown),
                                          doBuf ) ;
        
	PsurfTri = make_surfTri ( pUns, Pelem, &mVxHg, kVxHg, PvxElem ) ;
	if ( mVxHg ) {
	  if ( mDim == 2 )
	    buffer_2D_elem ( Pelem, pUns, &childSpc ) ;
	  else
	    buffer_3D_elem ( pUns, Pelem, mVxHg, kVxHg, PvxElem, PsurfTri, &childSpc,
                             doCheckVol, &makesNegVol, &doesAddCtrVx ) ;
        }
      }
    }

  /* Mark all unused elements as invalid. */
  Pchunk = PbufChunk ;
  for ( Pelem = childSpc.PlstElem+1 ;
        Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ ) {
    Pelem->invalid = 1 ;
  }

  if ( !make_refbuf_bndfc ( pUns, PbufChunk ) ) {
    hip_err ( fatal, 0, "could not find boundary faces in buffer_elems." ) ;
   }
  
  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  buf2ref_vol_ctr:
*/
/*! Upgrade to refinement if buffering of the adapted grid results 
 *  in all negative volumes and/or added central vertices.
 *
 */

/*
  
  Last update:
  ------------
  11Sep:19 conceived.
  

  Input:
  ------
  pUns: grid
  doRefNegVol: if buffering produces neg. vols, convert to [full] refinement 
  doRefCtrVx:  if buffering adds a central vx, convert to [full] refinement 

  Changes to:
  -----------
  pChange: non-zero if edges were added.
    
  Returns:
  --------
  1 if refinement edges were added, 0 otherwise
  
*/

void buf2ref_vol_ctr ( uns_s *pUns,
                       const int doRefNegVol, const int doRefCtrVx,
                       int *pChange) {

  int mDim = pUns->mDim ;
  if ( mDim == 2 ) {
    hip_err ( warning, 1, "buffering volume check not yet implemented in 2d, ignored.");
  }

  // max children      MAX_CHILDS_ELEM      
  // max nodes MAX_EDGES_ELEM+MAX_FACES_ELEM+1+1
  int maxChElemsBuf = MAX_FACES_ELEM*4 ;
  /* Use append_chunk: rebuilds ppChunk wich is needed by llEdge to search 
     cross edges ( between hanging nodes on a face) across chunks. */
  chunk_struct *pBufChunk = append_chunk ( pUns, pUns->mDim,
                                           maxChElemsBuf, MAX_VX_ELEM*maxChElemsBuf+1,
                                           4*MAX_FACES_ELEM+1,
                                           MAX_EDGES_ELEM+MAX_FACES_ELEM+1+1,
                                           0,0) ;
  pBufChunk->pVxCpt = arr_calloc ( "pVxCpt in buf2ref_vol_ctr", pUns->pFam,
                                   MAX_EDGES_ELEM+MAX_FACES_ELEM+1+1, sizeof (cpt_s ) ) ;

  /* Loop over all valid terminal elements. */
  int makesNegVol, doesAddCtrVx ;
  childSpc_s childSpc ;
  elem_struct elemCp, *pElCp = &elemCp ;
  const int doCheckVol = 1 ;
  chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  surfTri_s *PsurfTri ;
  int mVxHg,  kVxHg[MAX_ADDED_VERTS] ;
  vrtx_struct *PvxElem[MAX_REFINED_VERTS], *pVxMid ;
  vrtx_struct *pVrtx[2] ;
  const int *kVxEdge ;
  const int doFaces = 1, doBuf = 1 ; 
  int nAe[MAX_EDGES_ELEM], nCrossAe[MAX_FACES_ELEM+1][MAX_VX_FACE] ;
  vrtx_struct *ppVrtx[MAX_VX_ELEM+MAX_ADDED_VERTS ] ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    /* Loop over all cells. Find the refined sides. */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems && Pchunk != pBufChunk ; Pelem++ ) {
      Pelem->mark = 0 ; // JDM: what does this change?
      if ( Pelem->term && !Pelem->PrefType ) {
        *pElCp = *Pelem ; // Write child pointers to a throwaway copy of the elem.
        init_childSpc ( &childSpc, pUns, pBufChunk ) ;

        /* Temporarily add the mid-edge/midface vertices, if not yet present. */
        add_elem_aE_vx ( pUns, Pelem,
                         doFaces, doBuf, 
                         ppVrtx,
                         childSpc.PnewChunk, &childSpc.PlstVrtx, &childSpc.PlstCoor,
                         &childSpc.PlstUnknown,
                         nAe, nCrossAe ) ;
      
        /* Needs mid-eg/fc vertices to have been filled. */
	PsurfTri = make_surfTri ( pUns, pElCp, &mVxHg, kVxHg, PvxElem ) ;
	if ( mVxHg ) {
	  if ( mDim == 2 )
            // Needs 2d implementation.
	    buffer_2D_elem ( pElCp, pUns, &childSpc ) ;
	  else
	    buffer_3D_elem ( pUns, pElCp, mVxHg, kVxHg, PvxElem, PsurfTri, &childSpc,
                             doCheckVol, &makesNegVol, &doesAddCtrVx ) ;

          if ( ( makesNegVol && doRefNegVol )
               || ( doesAddCtrVx && doRefCtrVx ) ) {
            /* Mark elem for full refinement. */
            Pelem->refdEdges = elemType[Pelem->elType].allEdges ;
            adapt_uh_match_elem_eg ( pUns, Pelem, 1, pChange ) ;
          }
        }

        /* Remove the temporary mid-vertices and cross edges from the adapted edges. */
        rm_adEdgeVx_elem ( pUns, Pelem, nAe, nCrossAe ) ;
      }
    }


  arr_free ( pBufChunk->pVxCpt ) ;
  free_chunk ( NULL, &pBufChunk ) ;

  return ;
}



/******************************************************************************

  debuffer_uns:
  Remove all buffer elements.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void debuffer_uns ( uns_s *pUns ) {
  
  static const chunk_struct *Pchunk ;
  static elem_struct *Pelem, *Pprt, *Pch, **PPch ;
  static int mRemElems, mNewPrts ;
  
  for ( mNewPrts = mRemElems = 0, Pchunk = pUns->pRootChunk ;
        Pchunk ; Pchunk = Pchunk->PnxtChunk )
    /* Loop over all cells.  */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf && Pelem->Pparent && Pelem->Pparent->PrefType->refOrBuf == buf ){
        /* This is a buffer element. Go to to its parent. */
	Pprt = Pelem->Pparent ;
	Pprt->number = Pprt->PPchild[0]->number ;
	Pprt->leaf = 1 ;
	Pprt->PrefType = NULL ;
	mNewPrts++ ;
	/* Keep the child pointer to detect derefined elements.
	   Pprt->PPchild = NULL ; */
	
	/* Remove all the children. */
	for ( PPch = Pprt->PPchild ; ( Pch = *PPch ) && Pch->Pparent == Pprt ; PPch++ ){
          Pch->number = 0 ;
	  /* Well, a buffered child can never be a term, but what the heck. */
	  Pch->leaf = Pch->term = 0 ;
	  Pch->invalid = 1 ;
	  mRemElems++ ;
	}
      }
  
  /* Count and number. */
  pUns->isBuffered = 0 ;
  pUns->numberedType = noNum ;

  /*   number_uns_grid ( pUns ) ;
       number_uns_grid numbers by type, why not number chrono, use this: */
  const int dontUseNumber = 0 ;
  const int doReset = 1 ;
  const int doBound = 1 ;
  number_uns_grid_types ( pUns, noEl, noEl, dontUseNumber, doReset, doBound ) ;

  
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "debuffered grid to %"FMT_ULG" elements, %"FMT_ULG" verts.\n",
	     pUns->mElemsNumbered, pUns->mVertsNumbered ) ;
    hip_err ( info, 1, hip_msg ) ;
  }
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "debuffering removed %d, opened %d, %d fewer elements.\n",
	     mRemElems, mNewPrts, mRemElems-mNewPrts ) ;
    hip_err ( info, 1, hip_msg ) ;
  }
  return ;
}

/******************************************************************************

  debuffer_elem:
  Remove all buffer elements.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  

  Changes To:
  -----------

  
  Returns:
  --------
  The debuffered parent that has become a leaf, or Pelem if there was no
  buffering.
  
*/

elem_struct * debuffer_elem ( elem_struct *Pelem ) {
  
  elem_struct *Pprt, *Pch, **PPch ;

  if ( !Pelem->leaf && !Pelem->invalid &&
       Pelem->PrefType->refOrBuf == buf )
    /* This is a buffered parent element. */
    Pprt = Pelem ;
  else if ( Pelem->leaf && Pelem->Pparent &&
	    Pelem->Pparent->PrefType->refOrBuf == buf )
    /* This is a buffer element. Remove it and all its siblings. */
    Pprt = Pelem->Pparent ;
  else
    /* No buffering. */
    return ( Pelem ) ;

  /* Loop over all children. */
  for ( PPch = Pprt->PPchild ; *PPch && (*PPch)->Pparent == Pprt ; PPch++ )  {
    Pch = *PPch ;
    Pprt->number = Pch->number ;
    Pch->leaf = Pch->term = Pch->number = 0 ;
    Pch->invalid = 1 ;
  }
  /* Keep the child pointer to detect derefined elements.
  Pprt->PPchild = NULL ; */
  Pprt->leaf = 1 ;
  Pprt->PrefType = NULL ;

  return ( Pprt ) ;
}


/******************************************************************************

  init_childSpc:
  Initialize the childSpc.
  
  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void init_childSpc ( childSpc_s *PchildSpc, uns_s *pUns, chunk_struct *PbufChunk ) {
  
  /* Initialize the childSpc. Note that the elem space starts at 1.
  PchildSpc->Pelem = PbufChunk->Pelem ;
  PchildSpc->PPchild = PbufChunk->PPchild ;
  PchildSpc->PPvrtx = PbufChunk->PPvrtx ;
  PchildSpc->Pvrtx = PbufChunk->Pvrtx ;
  PchildSpc->Pcoor = PbufChunk->Pcoor ;
  PchildSpc->Punknown = PbufChunk->Punknown ;*/
  PchildSpc->pUns = pUns ;
  PchildSpc->PnewChunk = PbufChunk ;

  PchildSpc->PlstElem = PbufChunk->Pelem ;
  PchildSpc->PPlstChild = PbufChunk->PPchild-1 ;
  PchildSpc->PPlstVx = PbufChunk->PPvrtx-1 ;
  PchildSpc->PlstVrtx = PbufChunk->Pvrtx ;
  PchildSpc->PlstCoor = PbufChunk->Pcoor ;
  PchildSpc->PlstUnknown = PbufChunk->Punknown ;
  
  PchildSpc->Pelem_max = PbufChunk->Pelem + PbufChunk->mElems ;
  PchildSpc->PPchild_max = PbufChunk->PPchild + PbufChunk->mElem2ChildP-1 ;
  PchildSpc->PPvrtx_max = PbufChunk->PPvrtx + PbufChunk->mElem2VertP-1 ;
  PchildSpc->Pvrtx_max = PbufChunk->Pvrtx + PbufChunk->mVerts ;
  PchildSpc->Pcoor_max = PbufChunk->Pcoor + pUns->mDim * PbufChunk->mVerts ;
  PchildSpc->Punknown_max = PbufChunk->Pcoor +
                            pUns->varList.mUnknowns * PbufChunk->mVerts ;
  return ;
}
