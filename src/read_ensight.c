/*
  read_ensight.c:
*/

/*! read a binary ensight mesh
 */


/* 
  Last update:
  ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  15Jan12; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const char version[] ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;
extern const int negVol_flip ;

typedef enum { ensr_noType, ensr_caseFl, ensr_geoFl, ensr_solFl, ensr_vecSolFl } ensFlType_e ;
typedef enum { ensr_noFmt, ensr_cBinary, ensr_cBinary6, 
               ensr_fBinary, ensr_ascii } ensFlFmt_e ;
typedef enum { ensr_noVar, ensr_char, ensr_int, ensr_float, ensr_vec } ensVar_e ;
typedef enum { ensr_litEndian, ensr_bigEndian } ensFlEnd_e ;

#define VAR_NAME_LEN (80)
typedef struct {
  int mVarFls ;
  FILE *pfVar[MAX_UNKNOWNS] ;
  char varFlName[MAX_UNKNOWNS][LINE_LEN] ;
  int useTimeSet[MAX_UNKNOWNS] ;
  char varName[MAX_UNKNOWNS][VAR_NAME_LEN] ;
  int kVarPos[MAX_UNKNOWNS+1] ;
} ensrVar_s ;

/* Reference pointer, compute elem2vx pointers relative to that base, 
   set correct pointer relative to pChunk->Pvrts after final realloc. */
static vrtx_struct * const pVxBase = NULL ;
static double      * const pCoBase = NULL ;
static double      * const pUnBase = NULL ;


/* Same as write_ensight
   kVx_ensight[k] = kVx_hip[ h2e[ k ] ]
   writing:
            for ( i = 0 ; i < mVerts ; i++ )
              iBuf[i] = ppVx[ h2e[elType][i] ]->number ;
*/
static const int h2e[6][8] =
{ {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,2,1,3},        /* tet */
  {0,1,2,3,4},      /* pyramid */
  /* Fix for CD-adapco intake mesh with wrong handedness for prisms */
  /* {0,5,3,1,4,2},    *//* prism */
  {0,3,5,1,2,4},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;

/******************************************************************************

  ensr_open:
  Attach suffix to roofile, prepend path and open file.
  
  Last update:
  ------------
  15Jan12; derived from ensr_open_w
  27May09: conceived.
  
  Input:
  ------
  rootFile
  suffix

  Returns:
  --------
  opened file
  
*/

FILE* ensr_open ( const char *openFile ) {

  char openFl[LINE_LEN] ;
  FILE *file ;

  strncpy ( openFl, openFile, LINE_LEN ) ;
  prepend_path ( openFl ) ;
  if ( ( file = fopen ( openFl, "r" ) ) == NULL ) {
    sprintf ( hip_msg, " in ensr_open:\n"
              "        could not open file %s.\n",
              openFl ) ; 
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( file ) ;
}

/******************************************************************************
  ensr_skip_record:   */

/*! Skip a number of records.
 *
 */

/*
  
  Last update:
  ------------
  22Apr14: conceived.
  

  Input:
  ------
  mSkip: number of records to skip.

  Changes To:
  -----------
  fGeo: opened, positioned file

  
*/

void ensr_skip_record ( FILE* fGeo, ensFlFmt_e flFmt, ensVar_e ensVar, int mItems ) {

  int n, mRead ;
  ensFlEnd_e flEnd ;
  size_t recLen=0 ;
  switch ( ensVar ) {
  case ( ensr_char ) :
    recLen = mItems ; break ;
  case ( ensr_int ) :
    recLen = sizeof( int )*mItems ; break ;
  case ( ensr_float ) :
    recLen = sizeof( float )*mItems ; break ;
  default :
    hip_err ( fatal, 0, "unknown data type in ensr_skip_record." ) ;
  }

  char someChar ;
  int someInt ;
  float someFloat ;
  switch ( flFmt ) {
  case ( ensr_fBinary ) :
    fseek ( fGeo, recLen+2*sizeof(int), SEEK_CUR ) ;
    break ;
  case ( ensr_cBinary) :
    fseek ( fGeo, recLen, SEEK_CUR ) ;
    break ;
    
  case ( ensr_ascii ) :
    switch ( ensVar ) {
    case ( ensr_char ) :
      // Need to step through each item.
      for ( n = 0 ; n < mItems ; n++ ) {
        fscanf ( fGeo, "%c", &someChar ) ;
      }
      fscanf ( fGeo, "%*[^\n]" ) ; fscanf ( fGeo, "%*[\n]" ) ;
      break ;
    case ( ensr_int ) :
      // Need to step through each item.
      for ( n = 0 ; n < mItems ; n++ ) {
        fscanf ( fGeo, "%d", &someInt ) ;
      }
      fscanf ( fGeo, "%*[^\n]" ) ; fscanf ( fGeo, "%*[\n]" ) ;
      break ;
    case ( ensr_float ) :
      // Need to step through each item.
      for ( n = 0 ; n < mItems ; n++ ) {
        fscanf ( fGeo, "%f", &someFloat ) ;
      }
      fscanf ( fGeo, "%*[^\n]" ) ; fscanf ( fGeo, "%*[\n]" ) ;
      break ;
    default :
      ;
    }
  default :
    ;
  }

  return  ;
}

/******************************************************************************
  ensr_skip_section:   */

/*! Skip the section identified by a keyword in an ensight file.
 */

/*
  
  Last update:
  ------------
  16Dec12; avoid resetting feof when repositioning file.
  15Jan12: conceived.
  

  Input:
  ------
  fl: positioned file
  ensFlFmt: c-binary, f-binary, ascii?
  ensFlType: case or geo

  Changes To:
  -----------
  fl: repositioned file
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ensr_skip_section ( FILE* fl, 
                       const ensFlFmt_e ensFlFmt, const ensFlType_e ensFlType ) {

  if ( ensFlType == ensr_caseFl ) {
    /* In a casefile, do nothing. Every line can contain a tag, but skip comments. */
    fpos_t fPos ;
    fgetpos ( fl, &fPos ) ;
    char firstChar ;
    fscanf ( fl, "%c", &firstChar ) ;
    if ( firstChar == '#' ) {
      /* Comment, skip. */
      fscanf ( fl, "%*[^\n]" ) ;
      fscanf ( fl, "%*[\n]" ) ;
    }
    else if ( !feof(fl) )
      /* non-comment, repos to start of line. 
         Note: this resets feof, so only do if end hasn't been reached yet.*/
      fsetpos ( fl, &fPos ) ;
  }
  else {
    hip_err ( fatal, 0, "this filetype is not yet implemented in ensr_skip_section." ) ;
  }

  if ( feof ( fl ) )
    return ( 0 ) ;
  else
    return ( 1 ) ;
}

/******************************************************************************
  ensr_scan_file:   */

/*! Find a section keyword in the file.
  */

/*
  
  Last update:
  ------------
  15Jan12: conceived.
  

  Input:
  ------
  fl: opened, positioned file
  flType: one of c_binary, f_binary, ascii 
  tag: character string tag to match.

  Changes To:
  -----------
  fl: repositioned
    
  Returns:
  --------
  1 if found, 0 otherwise.
  
*/

int ensr_scan_file ( FILE* fl, ensFlFmt_e ensFlFmt, ensFlType_e ensFlType, 
                    const char *tag ) {

  rewind ( fl ) ;

  int taglen = strlen( tag ) ;

  if ( ensFlFmt == ensr_ascii ) {

    char line[LINE_LEN] ;

    while ( !feof ( fl ) ) {
      fscanf ( fl, "%s[^\n]", line ) ; // tag must be the first word on the line.
      fscanf ( fl, "%*[^\n]" ) ;
      fscanf ( fl, "%*[\n]" ) ;
      if ( !strncmp( tag, line, taglen ) )
        return ( 1 ) ;
      else 
        ensr_skip_section ( fl, ensFlFmt, ensFlType ) ;
    }
  }
  else 
    hip_err ( fatal, 0, "this file format is not yet implemented in ensr_scan_file" ) ;


  return ( 0 ) ;
}


/******************************************************************************
  ensr:   */

/*! read data from ensight files, taking account of the file format and var type.
 */

/*
  
  Last update:
  ------------
  6Apr13; fix bug with incrementing void pointder pTo.
  8Feb12; set trailing \0 for 80 char strings.
  15Jan12: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ensr_fread ( void *pTo, const ulong_t size, const int mItems, FILE *fEns, 
                const ensVar_e ensVar, const ensFlFmt_e flFmt, const ensFlEnd_e flEnd ) {

  int retVal = 0 ;
  int rLen, mRead ;
  void *pData = pTo ;
  int i ;
  char *pChar  = (char*)pTo, someChar ;
  int *pInt    = (int*)pTo ;
  float *pFloat = (float*)pTo ;

  switch ( flFmt ) {
  case ( ensr_fBinary ) :
    if ( flEnd == ensr_bigEndian )
      mRead = FREAD ( &rLen, sizeof(int), 1, fEns ) ;
    else
      mRead = fread ( &rLen, sizeof(int), 1, fEns ) ;
    if ( mRead !=1 ) {
      if ( feof ( fEns ) )
        return ( 0 ) ;
      else 
        hip_err ( fatal, 0, "rec len read failed in ensr_fread" ) ;
    }
    if ( rLen < size*mItems ) {
      sprintf ( hip_msg, "expected %"FMT_ULG" * %d = %"FMT_ULG" bytes,\n"
                "        found only %d in ensr_fread",
                size, mItems, size*mItems, rLen ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    if ( flEnd == ensr_bigEndian )
      retVal = FREAD ( pTo, size, mItems, fEns ) ;
    else
      retVal = fread ( pTo, size, mItems, fEns ) ;
    if ( retVal != mItems )
      hip_err ( fatal, 0, "fortran data read failed in ensr_fread" ) ;

    mRead = FREAD ( &rLen, sizeof(int), 1, fEns ) ;
    break ;


  case ( ensr_cBinary) :
  case ( ensr_cBinary6 ) :
    if ( flEnd == ensr_bigEndian )
      retVal = FREAD ( pTo, size, mItems, fEns ) ;
    else
      retVal = fread ( pTo, size, mItems, fEns ) ;

    if ( feof ( fEns ) )
      return ( 0 ) ;
    else if ( retVal != mItems )
      hip_err ( fatal, 0, "c data read failed in ensr_fread" ) ;
    if ( retVal != mItems )
      hip_err ( fatal, 0, "c data read failed in ensr_fread" ) ;
    break ;


  case ( ensr_ascii ) :
    switch ( ensVar ) {
    case ( ensr_char ) : // Allow fewer than the max of 80 char.
      for ( i = 0 ; i < mItems ; i++ ) {
        retVal += fscanf ( fEns, "%c", pChar ) ;
        if ( *pChar == '\n' || feof (fEns) ) break ;
        pChar++ ;
      }
      // Mop up any remains up to line feed.
      someChar = *pChar ;
      while ( someChar != '\n' && !feof (fEns) ) {
        fscanf ( fEns, "%c", &someChar ) ;
      }
      
      if ( feof ( fEns ) )
        return ( 0 ) ;
      break ;

    case ( ensr_int ) :
      for ( i = 0 ; i < mItems ; i++ ) {
        retVal += fscanf ( fEns, "%d", pInt++ ) ; 
      }
      // Only expect mItems on one line.
      fscanf ( fEns, "%*[^\n]" ) ; fscanf ( fEns, "%*[\n]" ) ;

      if ( feof ( fEns ) )
        return ( 0 ) ;
      else if ( retVal != mItems )
        hip_err ( fatal, 0, "ascii int read failed in ensr_fread" ) ;
      break ;
      
    case ( ensr_float ) :
      for ( i = 0 ; i < mItems ; i++ ) {
        retVal += fscanf ( fEns, "%f", pFloat++ ) ; 
      }
      // Only expect mItems on one line.
      fscanf ( fEns, "%*[^\n]" ) ; fscanf ( fEns, "%*[\n]" ) ;

      if ( feof ( fEns ) )
        return ( 0 ) ;
      else if ( retVal != mItems )
        hip_err ( fatal, 0, "ascii float read failed in ensr_fread" ) ;
      break ;
      
    default :
      hip_err ( fatal, 0, "unknown ensight variable type in ensr_fread." ) ;
    } // end switch ensvar
    break ;

  default :
    hip_err ( fatal, 0, "unknown ensight file format in ensr_fread" ) ;
  } // end switch file format


  /* Strip trailing blanks of character strings. */
  char *pStr = ( char * ) pTo ;
  if ( ensVar == ensr_char ) {
    for ( i = mItems-1 ; i >= 0 ; i-- )
      if ( pStr[i] != ' ' ) {
        /* First non-blank character from the back, terminate after this one. */
        pStr[i+1] = '\0' ;
        break ;
      }
  }


  return ( retVal ) ;
}

/******************************************************************************
  ensr_is_case_hdr:   */

/*! confirm whether a tag constitutes a valid section header in ensight case file.
 *
 */

/*
  
  Last update:
  ------------
  27Apr15: conceived.
  

  Input:
  ------
  keyword
    
  Returns:
  --------
  1 if it is not a header, 0 otherwise
  
*/

int ensr_is_case_hdr ( const char* keyword ) {

  if ( !strncmp( keyword, "FORMAT", strlen( "FORMAT" ) ) )
    return ( 1 ) ;
  else  if ( !strncmp( keyword, "GEOMETRY", strlen( "GEOMETRY" ) ) )
    return ( 1 ) ;
  else  if ( !strncmp( keyword, "VARIABLE", strlen( "VARIABLE" ) ) )
    return ( 1 ) ;
  else  if ( !strncmp( keyword, "TIME", strlen( "TIME" ) ) )
    return ( 1 ) ;
  else  if ( !strncmp( keyword, "MATERIAL", strlen( "MATERIAL" ) ) )
    return ( 1 ) ;

  return ( 0 ) ;
}

/******************************************************************************
   ensr_ts_flName:   */

/*! replace wildcards with the timeset instance number.
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ensr_ts_flName ( char *outFl, char *inFl, int useTs, int nStepToRead ) {

  strcpy ( outFl, inFl ) ;

  if ( !useTs ) return ;

  /* A rather ugly way to convert single digit ints to char. */
  char itoa[] = {"0123456789"} ;

  char *pEnd = outFl+strlen ( outFl )-1 ;
  char *pWild = pEnd ;

  while ( *pEnd == '*' && pEnd >= outFl ) pEnd-- ;

  int mStar = pWild-pEnd ;
  if ( nStepToRead > (10^mStar)-1 ) {
    sprintf ( hip_msg, "ensr_ts_flName: %d wildcards can't reprsent step # %d.",
              mStar, nStepToRead ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  for (  ; pWild > pEnd ; pWild-- ) {
    *pWild = itoa[ nStepToRead%10 ] ;
    nStepToRead/= 10 ;
  } 

  return ;
}



/******************************************************************************
  ensr_case:   */

/*! read an ensight case file.
 */

/*
  
  Last update:
  ------------
  8Jul15; allow arbitrary numbers of spaces between keywords and args.
  15Jan12: conceived.
  

  Input:
  ------
  caseFile
  nStepToRead, 

  Output:
  -------
  *pfGeo: opened geometry file
  pVarFl: data on the variable files.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ensr_case ( const char caseFile[], int nStepToRead,
                 FILE **pfGeo, ensrVar_s *pVarFl ) {


  FILE* fCase = ensr_open ( caseFile ) ;


  /* File FORMAT must be 'type: ensight gold'. */
  ensr_scan_file ( fCase, ensr_ascii, ensr_caseFl, "FORMAT" ) ;
  char line[LINE_LEN] ;
  fscanf ( fCase, "type: %[^\n]", line ) ;
  if ( strncmp ( line, "ensight gold", 18 ) ) {
    sprintf ( hip_msg, "found `%s' expecting `type: ensight gold'", line ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }


  char geomFlName[LINE_LEN] ;
  int ts, fs, timeSetDecl = 0, timeSet ;
  int useTimeSetGeo = 0 ;
  ensr_scan_file ( fCase, ensr_ascii, ensr_caseFl, "GEOMETRY" ) ;
  fscanf ( fCase, "%[^\n]", line ) ;
  fscanf ( fCase, "%*[\n]" ) ;
  /* The model line can have two optional timestep no, fileset no args. */
  if ( sscanf ( line, "model: %d %d %[^\n]", &ts, &fs, geomFlName )
       == 3 ) {
    sprintf ( hip_msg, "handling different filesets is currently not implemented."
              " Please contact your friendly hip developer or, easier,"
              " simplify your case file." ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
    ;
  }
  else if ( sscanf ( line, "model: %d' '%[^\n]", &timeSet, geomFlName ) == 2 ) {
    timeSetDecl = 1 ;
    useTimeSetGeo = 1 ;
  }
  else if ( sscanf ( line, "model: %[^\n]", geomFlName ) == 1 )
    ; 
  else {
    hip_err ( fatal, 0, "ensr_case: can't find the `model:' line" ) ;
  }
    


  /* Any variables? */
  char varType[LINE_LEN] ;
  char varName[LINE_LEN] ;
  char varFlName[LINE_LEN] ;
  int mVarFls = 0 ;
  pVarFl->kVarPos[0] = 0 ;
  if ( ensr_scan_file ( fCase, ensr_ascii, ensr_caseFl, "VARIABLE" ) ) {
    /* There are variables. */

    fscanf ( fCase, "%[^\n]", line ) ;
    while (!feof( fCase ) && !ensr_is_case_hdr ( line ) ) {

      if ( sscanf ( line, "%[^:] %*[:] %d %d %s %[^\n]", 
                    varType, &ts, &fs, varName, varFlName ) == 5 ) {
        /* Timeset and fileset specified, not supported. */
        sprintf ( hip_msg, 
                  "handling different filesets is currently not implemented."
                  " Please contact your friendly hip developer or, easier,"
                  " simplify your case file." ) ;
        hip_err ( fatal, 0, hip_msg ) ; 
      }
      else if (  sscanf ( line, "%[^:] %*[:] %d %s %[^\n]", 
                          varType, &ts, varName, varFlName ) == 4 ) {
        /* Timeset and fileset specified, check if the same. */
        if ( timeSetDecl && ts != timeSet ) {
          sprintf ( hip_msg, "timeset %d differs from ts %d for variable %s."
                    "Currently only a single filset is supported,"
                    " edit your case file.", timeSet, ts, varName ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }
        else {
          timeSetDecl = 1 ;
          pVarFl->useTimeSet[mVarFls] = 1 ;
          timeSet = ts ;
        }
      }
      else if ( sscanf ( line, "%[^:] %*[:] %s %[^\n]", 
                         varType, varName, varFlName ) == 3 )
        ; 
      else {
        sprintf ( hip_msg, "ensr_case: failed to parse line: %s", line ) ;
        hip_err (fatal, 0, hip_msg ) ; 
      }

      /* List. */
      strncpy ( pVarFl->varName[mVarFls], varName, VAR_NAME_LEN ) ;
      strncpy ( pVarFl->varFlName[mVarFls], varFlName, LINE_LEN ) ;
      /* What type of variable? */
      if ( !strncmp( varType, "scalar per node", 
                     sizeof("scalar per node") ) ) {
        pVarFl->kVarPos[ mVarFls+1 ] = pVarFl->kVarPos[ mVarFls ]+1 ;
      }
      else if ( !strncmp( varType, "vector per node", 
                          sizeof("vector per node") ) ) {
        pVarFl->kVarPos[ mVarFls+1 ] = pVarFl->kVarPos[ mVarFls ]+MAX_DIM ;
      }
      else if ( isblank( line[0] ) ) {
        ; /* Assume leading blank is all blank. Fix if needed. */
      }
      else {
        sprintf ( hip_msg, "ensr_case: unknown type %s for variable %s",
                  varType, varName ) ;
        hip_err( warning, 3, hip_msg ) ;
      }
      pVarFl->mVarFls = ++mVarFls ;

      fscanf ( fCase, "%*[\n]" ) ;
      fscanf ( fCase, "%[^\n]", line ) ;
    }
  }

  /* Do we need to read the timeset? */
  int mSteps, nStart, nIncr ;
  char keyword[LINE_LEN] ;
  if ( timeSetDecl ) {
    if ( ensr_scan_file ( fCase, ensr_ascii, ensr_caseFl, "TIME" ) ) {
      /* There are variables. */

      fscanf ( fCase, "%[^\n]", line ) ;
      while (!feof( fCase ) && !ensr_is_case_hdr ( line ) ) {
        sscanf ( line, "%[^:]%*[:]", keyword ) ;

        if ( !strncmp( keyword, "time set", strlen("time set") ) ) {
          if ( sscanf ( line, "%*[^:]%*[:]%d %*[^\n]", &ts ) !=1 ) {
            hip_err ( fatal, 0, "missing arg for `time set' in TIME section."); 
          }
          else if ( ts != timeSet ) {
            sprintf ( hip_msg, "expected timeset %d, found %d in case file.",
                      timeSet, ts ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }
        }
        else if ( !strncmp( keyword, "number of steps", 
                            strlen("number of steps") ) ) {
          if ( sscanf ( line, "%*[^:]%*[:]%d %*[^\n]", &mSteps ) != 1) {
            hip_err ( fatal, 0, "missing arg for `number of steps'"
                      " in TIME section."); 
          }
          else  if ( nStepToRead > mSteps ) {
            sprintf ( hip_msg, 
                      "requested step %d, but only %d steps given in case.",
                      nStepToRead, mSteps ) ;
            hip_err ( warning, 1, hip_msg ) ;
          }
        }
        else if ( !strncmp( keyword, "filename start number", 
                            strlen("filename start number") ) ) {
          if ( sscanf ( line, "%*[^:]%*[:]%d %*[^\n]", &nStart ) != 1) {
            hip_err ( fatal, 0, "missing arg for `filename start number'"
                      " in TIME section."); 
          }
        }
        else if ( !strncmp( keyword, "filename increment", 
                            strlen("filename increment") ) ) {
          if ( sscanf ( line, "%*[^:]%*[:]%d %*[^\n]", &nIncr ) != 1 ) {
            hip_err ( fatal, 0, "missing arg for `filename increment'"
                      " in TIME section."); 
          }
        }
        else if ( !strncmp( keyword, "time values", 
                            strlen("time values") )) {
          ; /* Skip. */
        }
        else if ( isblank( line[0] ) ) {
          ; /* Assume leading blank is all blank. Fix if needed. */
        }
        else {
          sprintf ( hip_msg, "unrecognised keyword in case file: `%s',"
                    " ignored.", line ) ;
          hip_err ( warning, 1, hip_msg ) ;
        }



        fscanf ( fCase, "%*[\n]" ) ;
        fscanf ( fCase, "%[^\n]", line ) ;

      }
    }
  }

  /* Actual timestep number. */
  nStepToRead = nStart + (nStepToRead-1)*nIncr ;

  /* Open Geo file. */
  ensr_ts_flName ( varFlName, geomFlName, useTimeSetGeo, nStepToRead ) ;
  *pfGeo = ensr_open ( varFlName ) ;

  
  /* Open variable files. */
  int k ;
  for ( k = 0 ; k < mVarFls ; k++ ) {
    ensr_ts_flName ( varFlName, pVarFl->varFlName[k], 
                     pVarFl->useTimeSet[k], nStepToRead ) ;
    pVarFl->pfVar[k] = ensr_open( varFlName ) ;
  }
    

  fclose ( fCase ) ;
  return ;
}
/******************************************************************************
  ensr_file_format:   */

/*! determine the file format of an ensight file.
 */

/*
  
  Last update:
  ------------
  21Oct16; guard else block as per compiler warning.
  15Jan12: conceived.
  

  Input:
  ------
  fGoe: file to determine the type of
    
  Returns:
  --------
  file format: one of ensr_fBinary, ensr_cBinary, ensr_ascii
  
*/

void ensr_file_format ( FILE *fGeo, ensFlFmt_e *pflFmt, ensFlEnd_e *pflEnd ) {

  rewind ( fGeo ) ;

  *pflFmt = ensr_noFmt ;
  *pflEnd = ensr_litEndian ;

  int someInt[8] ;
  FREAD ( someInt, sizeof(int), 1, fGeo ) ;
  char line[LINE_LEN] ;
  if ( someInt[0] == 80 ) {
    /* Should be Fortran binary. */
    FREAD( line, sizeof(char), 80, fGeo ) ; 
    r1_str_tolower ( line ) ;
    if ( strncmp ( line, "fortran binary", strlen("fortran binary")  ) )
      hip_err ( fatal, 0, "file structure suggests Fortran, but doesn't say so"
                " in ensr_file_format" ) ;
    FREAD ( someInt, sizeof(int), 1, fGeo ) ;
    hip_err ( info, 1, "Using Fortran binary format" ) ;
    *pflFmt = ensr_fBinary ;
  }
  
  if ( *pflFmt == ensr_noFmt ) {
    /* Is it a C binary? */
    rewind ( fGeo ) ;
    FREAD( line, sizeof(char), 80, fGeo ) ; 
    r1_str_tolower ( line ) ;
    if ( !strncmp ( line, "c binary", strlen("c binary") ) ) {
      *pflFmt = ensr_cBinary ;
      hip_err ( info, 1, "Using C binary format" ) ;
    }
  }

  if ( *pflFmt == ensr_noFmt ) {
    /* Assume ASCII. */
    rewind ( fGeo ) ;
    *pflFmt = ensr_ascii ;
    hip_err ( info, 1, "Using Ascii format" ) ;
  }




  else {
    /* Big- or little-endian binary ? Read up to the first part number. */
    /* Two description lines */
    ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, *pflFmt, *pflEnd ) ;
    ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, *pflFmt, *pflEnd ) ;
    /* Two node/elem_id lines. */
    if ( strncmp( line, "node id", strlen( "node id" ) ) )
         /* Two lines of comment + C/F binary line. Read node id next. */
      ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, *pflFmt, *pflEnd ) ;
    ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, *pflFmt, *pflEnd ) ;

    /* Extent given? */
    ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, *pflFmt, *pflEnd ) ;
    if ( !strncmp ( line, "extents", strlen("extents") ) ) {
      /* Skip 6 floats. */
      float dumVar[6] ;
      ensr_fread ( dumVar, sizeof(float), 6, fGeo, ensr_float, *pflFmt, *pflEnd ) ;
      ensr_fread ( line,   sizeof(char), 80, fGeo, ensr_char,  *pflFmt, *pflEnd ) ;
    }

    if ( strncmp ( line, "part", strlen("part") ) )
      hip_err ( fatal, 0, "expected a 'part' keyword in ensr_file_format." ) ;

    /* The next number is the first part number, ought to be a low int. */
    fpos_t fPos ; 
    fgetpos ( fGeo, &fPos ) ;
    int nPart ;
    ensr_fread ( &nPart, sizeof(int), 1, fGeo, ensr_int, *pflFmt, *pflEnd ) ;
  
    if ( nPart < 0 || nPart > 1000 ) {
      /* Not a sensible part number. Try Big-endian. */
      fsetpos ( fGeo, &fPos ) ;
      *pflEnd = ensr_bigEndian ;
      ensr_fread ( &nPart, sizeof(int), 1, fGeo, ensr_int, *pflFmt, *pflEnd ) ;
    }
    
    if ( nPart < 0 || nPart > 1000 )
      /* Still not a sensible part number. Fail. */
      hip_err( fatal, 0,
               "could not determine little or big endianness in ensr_file_format." ) ;


    rewind ( fGeo ) ;
    /* Strip off the format header, if binary. */
    ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, *pflFmt, *pflEnd ) ;
  }

  return ;
}

/******************************************************************************
  ensr_part:   */

/*! read a part in a geo file
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  23Apr14; intro skip.
  14Jan12: conceived.
  

  Input:
  ------
  fGeo: file to read from
  flFmt: f/c binary or ascii?
  pUns: grid

  Output:
  -------
  partName: name of the part

  Returns:
  --------
  the part number.
  
*/

int ensr_part ( FILE *fGeo, int skip, ensFlFmt_e flFmt, ensFlEnd_e flEnd,
                char *partName, uns_s *pUns ) {


  /* Read this part. */
  int partNo ; 
  ensr_fread ( &partNo, sizeof(int), 1, fGeo, ensr_int, flFmt, flEnd ) ;

  ensr_fread ( partName, sizeof(char), 80, fGeo, ensr_char, flFmt, flEnd ) ;
  r1_endstring ( partName, LINE_LEN ) ;

  if ( !skip ) {
    /* Declare a b.c. with this partName. */
    pUns->mBc++ ;


    /* Make a bc for this part, may already exist. */
    find_bc ( partName, 1 ) ;

  }

  return ( partNo ) ;
}

/******************************************************************************
  ensr_coor:   */

/*! Read coordinates for a one part from an Ensight geo file
 */

/*
  
  Last update:
  ------------
  17Dec15; correct tags of calling routine in calls to arr_malloc/realloc.
  22Apr14; introduce skip.
  30Jun12; use node_id when given.
  9Feb12; set pmVxOffset.
  20Jan12: conceived.
  

  Input:
  ------
  fGeo: positioned file.
  skip: if nonzero, only parse, don't read the data.
  flFmt: f/c binary or ascii.
  flEnd: endianness of file.
  node_id: 1 if given.
  pUns: unstructured grid.
  pmVxOffset: global vertex number of vertex numbered 0 in this part.

  Changes To:
  -----------
  ppVxBasePart: address of pointer where vertices for this part start.
    
  Returns:
  --------
  number of nodes in this part.
  
*/

int ensr_coor ( FILE *fGeo, int skip, 
                ensFlFmt_e flFmt, ensFlEnd_e flEnd, 
                const int node_id, uns_s *pUns, 
                int *pmVxOffset, int **ppNodeIds,
                ensrVar_s *pVarFl ) {

  int mVxPart ;
  ensr_fread ( &mVxPart, sizeof(int), 1, fGeo, ensr_int, flFmt, flEnd ) ;

  if ( !skip && verbosity > 3 )
    printf ( "         with %d nodes\n", mVxPart ) ;

  if ( node_id ) {
    if ( !skip ) {
      /* pNodeIds runs from one to allow simple indirect addresing from 
         elements and faces. */
      *ppNodeIds = 
        arr_realloc ( "node_id in ensr_coor", pUns->pFam, 
                      *ppNodeIds, mVxPart+1, sizeof( **ppNodeIds ) ) ;
      ensr_fread( (*ppNodeIds)+1, sizeof(int), mVxPart, 
                  fGeo, ensr_int, flFmt, flEnd ) ;
    }
    else {
      ensr_skip_record ( fGeo, flFmt, ensr_int, mVxPart ) ;
    }
  }


  int k ;
  FILE *fVar ;
  char line[LINE_LEN] ;
  if ( !skip ) {
    /* Advance all solution files past the 'coordinates' line. */
    for ( k = 0 ; k < pVarFl->mVarFls ; k++ ) {
      fVar = pVarFl->pfVar[k] ;
      ensr_fread ( line, sizeof(char), 80, 
                   fVar, ensr_char, flFmt, flEnd ) ;
      if ( strncmp( line, "coordinates", sizeof("coordinates") ) ) {
        sprintf ( hip_msg, "ensr_coor: expected `coordinates' in var file,"
                  " found %s", line ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

    }
  }

  float *pXYZ ;
  float *pX, *pY, *pZ ;
  int mDim ;
  if ( !skip ) {
    mDim = pUns->mDim ;
    pXYZ = arr_malloc ( "pXYZ in ensr_coor", pUns->pFam, 
                        mDim*mVxPart, sizeof( *pXYZ ) ) ;
    pX = pXYZ, pY = pX+mVxPart, pZ = pY+mVxPart ;
    ensr_fread ( pX, sizeof(float), mVxPart, fGeo, ensr_float,flFmt,flEnd ) ;
    ensr_fread ( pY, sizeof(float), mVxPart, fGeo, ensr_float,flFmt,flEnd ) ;
    if ( mDim == 3 )
      ensr_fread ( pZ, sizeof(float), mVxPart, fGeo, ensr_float,flFmt,flEnd ) ;
    else
      /* Skip z-coord. */
      ensr_skip_record ( fGeo, flFmt, ensr_float, mVxPart ) ;
  }
  else {
    ensr_skip_record ( fGeo, flFmt, ensr_float, mVxPart ) ;
    ensr_skip_record ( fGeo, flFmt, ensr_float, mVxPart ) ;
    ensr_skip_record ( fGeo, flFmt, ensr_float, mVxPart ) ;
  }

  chunk_struct *pChunk ;
  int mVerts ;
  int i ;
  int mUn  ;
  if ( !skip ) {
    pChunk = pUns->pRootChunk ;
    mVerts = *pmVxOffset = pChunk->mVerts ;
    mUn = pUns->varList.mUnknowns ;
    /* vertex coordinates, always 3D, numbering starts at 1. */
    if ( node_id ) { 
      /* Find the max node number. */
      for ( i = 1 ; i <= mVxPart ; i++ )
        mVerts = MAX( mVerts, (*ppNodeIds)[i] ) ;
    }
    else {
      mVerts += mVxPart ;
    }

    if ( mVerts > pChunk->mVerts ) {
      /* Add new verts. This implies either no node ids given, or the volume
         elements are given in more than one part. */
      pChunk->Pcoor = 
        arr_realloc ( "pCoor in ensr_coor", pUns->pFam, 
                      pChunk->Pcoor, mDim*(mVerts+1), sizeof( *pChunk->Pcoor ));
      pChunk->Pvrtx = 
        arr_realloc ( "pVrtx in ensr_coor", pUns->pFam, 
                      pChunk->Pvrtx, mVerts+1, sizeof( *pChunk->Pvrtx ) ) ;
      if ( pUns->varList.mUnknowns ) {
        /* Alloc uknown. */
        pChunk->Punknown = 
          arr_realloc ( "pUnknown in ensr_coor", pUns->pFam, 
                        pChunk->Punknown, mUn*(mVerts+1), 
                        sizeof( *pChunk->Punknown ) ) ;
      }
      reset_verts ( pChunk->Pvrtx + pChunk->mVerts+1, mVerts - pChunk->mVerts );
      pChunk->mVerts = mVerts ;
    }

    /* Skip treatment, data will be read a second time */
    if ( skip ) return ( mVxPart ) ;

    /* Pack the vertices, coordinates. */
    vrtx_struct *pVx ;
    double *pCo ;
    double *pUn ;
    int nVx ;
    int *pNdId = *ppNodeIds+1 ;
    for ( i = 1  ;  i <= mVxPart ; i++ ) {
      if ( node_id ) 
        nVx = *pNdId++ ;
      else
        nVx = *pmVxOffset+i ;

      pVx = pChunk->Pvrtx+nVx ;
      pVx->Pcoor = pCoBase + mDim*nVx ;
      pVx->Punknown = pUnBase + mUn*nVx ;

      pCo = pChunk->Pcoor + mDim*(nVx) ;
      *pCo++ = *pX++ ;
      *pCo++ = *pY++ ;
      if ( mDim == 3 ) *pCo++ = *pZ++ ;
      pVx->number = nVx ;
    }



    /* Pack the unknowns. */
    int kPos ;
    int varDim ;
    for ( k= 0 ; k < pVarFl->mVarFls ; k++ ) {

      kPos = pVarFl->kVarPos[k] ;
      fVar = pVarFl->pfVar[k] ;
      varDim =  pVarFl->kVarPos[k+1] - kPos ;

      if ( varDim == mDim ) {
        /* Vector. */
        pX = pXYZ, pY = pX+mVxPart, pZ = pY+mVxPart ;
        ensr_fread ( pX, sizeof(float), mVxPart, fVar,ensr_float,flFmt,flEnd );
        ensr_fread ( pY, sizeof(float), mVxPart, fVar,ensr_float,flFmt,flEnd );
        if ( mDim == 3 )
          ensr_fread (pZ,sizeof(float), mVxPart, fVar,ensr_float,flFmt,flEnd );
        else
          /* Skip z-coord. */
          ensr_skip_record ( fVar, flFmt, ensr_float, mVxPart ) ;
      }
      else {
        /* Scalar. */
        pX = pXYZ ;
        ensr_fread ( pX, sizeof(float), mVxPart, fVar,ensr_float,flFmt,flEnd );
      }


      pNdId = *ppNodeIds+1 ;
      for ( i = 1  ;  i <= mVxPart ; i++ ) {
        if ( node_id ) 
          nVx = *pNdId++ ;
        else
          nVx = *pmVxOffset+i ;

        pUn = pChunk->Punknown + mUn*(nVx) + kPos ;
        *pUn++ = *pX++ ;
        if ( varDim > 1 ) *pUn++ = *pY++ ;
        if ( varDim > 2 ) *pUn++ = *pZ++ ;
      }

    }
 
    arr_free ( pXYZ ) ;
  }

  return ( mVxPart ) ;
}

/******************************************************************************
  ensr_elem:   */

/*! read the volumetric elements of a part in Ensight geo.
 */

/*
  
  Last update:
  ------------
  17Dec15; correct tag for calling function in calls to arr_malloc/realloc.
           eliminate elemIds from computing the largest element number.
  30Jun12; use node ids.
  9Feb12; use mVxOffset.
  2Feb12; read one block of connectivity.
  20Jan12: conceived.
  

  Input:
  ------
  fGeo: file positioned after element section.
  flFmt: f/c binary or ascii
  flEnd: file endianness
  elem_id: non-zero if an element id number is given
  elType: element type to be read
  partNo: number of this part,
  pUns: grid
  mVxOffsete: global vertexnumber of vertex 0 in this part.

  Returns:
  --------
  number of elements read.
  
*/

int ensr_elem ( FILE *fGeo, int skip, 
                ensFlFmt_e flFmt, ensFlEnd_e flEnd, 
                const int elem_id, const elType_e elType, const int partNo,
                uns_s *pUns, int mVxOffset, 
                const int node_id, const int *pNodeIds ) {

  /* How many elems of this type? */
  int mTypeElems = 0 ;
  ensr_fread ( &mTypeElems, sizeof(int), 1, fGeo, ensr_int, flFmt, flEnd ) ;

  if ( !skip && verbosity > 3 )
    printf ( "         with %d %s\n", mTypeElems, elemType[elType].name ) ;


  /* Element ids given? */
  int *pElemIds = NULL ;
  if ( elem_id ) {
    if ( !skip ) {
    pElemIds = arr_malloc ( "elem_id in ensr_elem", pUns->pFam, 
                            mTypeElems, sizeof( *pElemIds) ) ;
    ensr_fread( pElemIds, sizeof(int), mTypeElems, 
                fGeo, ensr_int, flFmt, flEnd ) ;
    }
    else {
      ensr_skip_record ( fGeo, flFmt,  ensr_int, mTypeElems ) ;
    }
  }


  const elemType_struct *pElT = elemType + elType ;
  int mVxEl = pElT->mVerts ;
  elem_struct *pElem;
  int *pnFrmVx ;
  vrtx_struct **ppVx ;
    int i ;
  if ( !skip ) {
    chunk_struct *pChunk = pUns->pRootChunk ;
    int mElems = pChunk->mElems ;
    // Why do we need to track the largest referenced id number 
    // when allocating for this type of element?
    // if ( elem_id ) {
    //   /* Find the max elem number. */
    //   for ( i = 0 ; i < mTypeElems ; i++ )
    //     mElems = MAX( mElems, pElemIds[i] ) ;
    // }
    // else {
      mElems += mTypeElems ;
      // }

    pChunk->Pelem = 
      arr_realloc ( "pElem in ensr_elem", pUns->pFam, 
                    pChunk->Pelem, mElems+1, sizeof( *pChunk->Pelem ) ) ;

    /* Pointer to the 0th new element. */
    pElem = pChunk->Pelem+ pChunk->mElems ;
    pChunk->mElems = mElems ;


    int mNewElem2VertP = pChunk->mElem2VertP + mVxEl*mTypeElems ;
    pChunk->PPvrtx = arr_realloc ( "ppVrtx in ensr_elem", pUns->pFam, 
                                   pChunk->PPvrtx, mNewElem2VertP, 
                                   sizeof( *pChunk->PPvrtx ) ) ;
    /* Pointer to the 0th new conn entry. */
    ppVx = pChunk->PPvrtx + pChunk->mElem2VertP ;
    pChunk->mElem2VertP = mNewElem2VertP ;

    /* Reset newly alloc'ed elems. */
    reset_elems ( pElem+1, mTypeElems ) ;
  


    /* Read element connectivity. */
    pnFrmVx = arr_malloc ( "pnFrmVx in ensr_elem", pUns->pFam, 
                           mVxEl*mElems, sizeof( *pnFrmVx ) ) ;
    ensr_fread( pnFrmVx, sizeof(int), mVxEl*mTypeElems, 
                fGeo, ensr_int, flFmt, flEnd ) ;
  }
  else {
    ensr_skip_record ( fGeo, flFmt, ensr_int, mVxEl*mTypeElems ) ; 
  }
  

  if ( !skip ) {
    /* Fill element connectivity. */
    elem_struct *pEl ;
    int nEl ;
    int *pnFVx ;
    int nVx ;
    for ( pEl = pElem+1, nEl=1, pnFVx = pnFrmVx ; 
          pEl <= pElem+mTypeElems ; nEl++ ) {

      pEl->PPvrtx = ppVx ;
      for ( i = 0 ; i < mVxEl ; i++ ) {
        if ( node_id ) 
          nVx = pNodeIds[ pnFVx[i] ] ;
        else
          nVx = mVxOffset + pnFVx[i] ;

        ppVx[ h2e[elType][i] ] = pVxBase + nVx ;
      }
      ppVx += mVxEl ;
      pnFVx += mVxEl ;

      pEl->elType = elType ;
      pEl->number = ( elem_id ? pElemIds[nEl] : nEl ) ;
      pEl->iZone = partNo ;
      pEl++ ;
    }

    arr_free ( pnFrmVx ) ;
  }

  return ( mTypeElems ) ;
}


/******************************************************************************
  ensr_face:   */

/*! read the boundary faces of a part in Ensight geo.
 */

/*
  
  Last update:
  ------------
  3Mar16; check whether elem_id is on.
  23Apr15; intro skip.
  30Jun12; use node ids.
  9Feb12; use mVxOffset.
  2Feb12; read face forming vertices in one block.
  20Jan12: conceived.
  

  Input:
  ------
  fGeo: file to read
  flFmt: f/c binary or ascii
  elem_id: elem_ids given or not?
  partName: name of the part, becomes name of the bc.
  mVxFc: number of forming vertices for this type of face
  pUns: unstructured grid.
  mVxOffset: what global vertex number is vertex number zero in this part.
    
  Returns:
  --------
  the number of faces read.
  
*/

int ensr_face ( FILE *fGeo, int skip,
                const ensFlFmt_e flFmt,  const ensFlEnd_e flEnd, 
                const int elem_id, const char partName[], const int mVxFc, 
                uns_s *pUns, int mVxOffset,
                const int node_id, const int *pNodeIds ) {

  /* How many faces of this type? */
  int mTypeFaces ;
  ensr_fread ( &mTypeFaces, sizeof(int), 1, fGeo, ensr_int, flFmt, flEnd ) ;

  if ( verbosity > 3 )
    printf ( "         with %d %d-noded faces\n", mTypeFaces, mVxFc ) ;

  /* Face ids given? If so, does it point to the internal elem? Can't be sure.*/
  /* Face ids are ignored, skip */
  if ( elem_id ) {
    ensr_skip_record ( fGeo, flFmt, ensr_int, mTypeFaces ) ;
  }

//  int *pFaceIds ;
//  if ( elem_id ) {
//    if ( !skip ) {
//      /* Always ignore elem_id with faces.
//         When code is verified, wrap into DEBUG and replace with fseek. */
//      pFaceIds = arr_malloc ( "face_id in ensr_geo_face", pUns->pFam, 
//                              mTypeFaces, sizeof( *pFaceIds ) ) ;
//      ensr_fread( pFaceIds, sizeof(int), mTypeFaces, 
//                  fGeo, ensr_int, flFmt, flEnd ) ;
//      free ( pFaceIds ) ;
//      pFaceIds = NULL ;
//    }
//    else {
//      ensr_skip_record ( fGeo, flFmt, mTypeFaces*sizeof(int) ) ;
//    }
//  }
//  pFaceIds = NULL ;


  int *pnFrmVx ;
  bndFcVx_s *pBv ;
  bc_struct *pBc;
  if ( !skip ) {
    /* Get the bc for this part, created in read_part. */
    pBc = find_bc ( partName, 1 ) ;


    /* See e.g. read_uns_centaur for use of pBndFcVx. */
    int mBndFcVx = pUns->mBndFcVx + mTypeFaces ;
    pUns->pBndFcVx = 
      arr_realloc ( "pUns->pBndFcVx in ensr_face", pUns->pFam,
                    pUns->pBndFcVx, mBndFcVx, sizeof( *pUns->pBndFcVx ) ) ;
    reset_bndFcVx( pUns->pBndFcVx+pUns->mBndFcVx, mTypeFaces ) ;
    pBv = pUns->pBndFcVx + pUns->mBndFcVx ;
    pUns->mBndFcVx = mBndFcVx ;



    /* Read face connectivity. */
    pnFrmVx = arr_malloc ( "pnFrmVx in ensr_face", pUns->pFam, 
                            mVxFc*mTypeFaces, sizeof( *pnFrmVx ) ) ;
    ensr_fread( pnFrmVx, sizeof(int), mVxFc*mTypeFaces, 
                fGeo, ensr_int, flFmt, flEnd ) ;
  }
  else {
    ensr_skip_record ( fGeo, flFmt, ensr_int, mVxFc*mTypeFaces ) ;
  }
  

  if ( !skip ) {
    /* Fill face connectivity. */
    int nB ;
    int i, nVx, *pnFVx = pnFrmVx ;
    for ( nB = 0 ; nB < mTypeFaces ; nB++ ) {
      for ( i = 0 ; i < mVxFc ; i++ ) {
        if ( node_id ) 
          nVx = pNodeIds[ pnFVx[i] ] ;
        else
          nVx = mVxOffset + pnFVx[i] ;
        pBv->ppVx[i] = pVxBase + nVx ;
      }
      pBv->mVx = mVxFc ;
      pBv->pBc = pBc ;
      pBv++ ;

      pnFVx += mVxFc ;
    }


    arr_free ( pnFrmVx ) ;
  }

  return ( mTypeFaces ) ;
}


/******************************************************************************
  ensr_set_ppVx:   */

/*! After final realloc, recalculate vx2co and el/fc2vx pointers from pRootChunk->Pvrtx
 */

/*
  
  Last update:
  ------------
  25Apr15; set Puknown to allow reading solutions.
  9Feb12; update also pUns->pBndFcVx and pVrtx->Pcoor ;
  22Jan12: conceived.
  

  Input:
  ------
  pUns
  
*/

void ensr_set_ppVx ( uns_s *pUns ) {

  /* Vertices. */
  chunk_struct *pChunk = pUns->pRootChunk ;
  vrtx_struct *pVx ;
  int nCo, nUn ;
  for ( pVx = pChunk->Pvrtx+1 ; 
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ ) {
    nCo = pVx->Pcoor - pCoBase ;
    pVx->Pcoor = pChunk->Pcoor + nCo ;
    nUn = pVx->Punknown - pUnBase ;
    pVx->Punknown = pChunk->Punknown + nUn ;
  }

  /* Elements. */
  vrtx_struct *pVrtx = pChunk->Pvrtx ;
  vrtx_struct **ppVx = pChunk->PPvrtx ;
  elem_struct *pEl ;
  const elemType_struct *pElT ;
  int mVxEl ;
  int kVx, nVx ;
  for ( pEl = pChunk->Pelem+1 ; pEl <= pChunk->Pelem + pChunk->mElems ; pEl++ ) {
    pElT = elemType + pEl->elType ;
    mVxEl = pElT->mVerts ;

    pEl->PPvrtx = ppVx ;    
    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
      nVx = *ppVx - pVxBase ;
      *ppVx++ = pVrtx + nVx ;
    }
  }

  /* Boundary faces given by nodes. */
  bndFcVx_s *pBv ;
  for ( pBv = pUns->pBndFcVx ; pBv < pUns->pBndFcVx + pUns->mBndFcVx ; pBv++ ) {
    ppVx = pBv->ppVx ;    
    for ( kVx = 0 ; kVx < pBv->mVx ; kVx++ ) {
      nVx = ppVx[kVx] - pVxBase ;
      ppVx[kVx] = pVrtx + nVx ;
    }
  }

  return ;
}

/******************************************************************************
  ensr_name_elt:
*/
/*! Given an ensight element string, return the element type if its a 3D elem.
 */
  
/*
  Last update:
  ------------
  2Feb12; limit to 3D elements.
  26Jan11; mirrored from ensr_elt_name in write_ensight.
  
  Input:
  ------
  str: ensight element type descriptor.
  
  Returns:
  --------
  the element type.
  
*/

elType_e ensr_name_elt ( char *str, int mDim ) {

  if ( mDim == 3 ) {
    if (!strncmp(str, "tetra4"   ,strlen("tetra4"  ))){return ( tet  ) ;}
    else if (!strncmp(str, "hexa8"    ,strlen("hexa8"   ))){return ( hex  ) ;}
    else if (!strncmp(str, "pyramid5" ,strlen("pyramid5"))){return ( pyr  ) ;}
    else if (!strncmp(str, "penta6"   ,strlen("penta6"  ))){return ( pri  ) ;}
    /* Why do we get a segfault without this? Can't see anyth. wrong.*/
    else if (!strncmp(str, "blah"     ,strlen("blah"    ))){return ( noEl ) ;}
    else                                                   {return ( noEl ) ;}
  } 
  else {
    if (!strncmp (str, "tria3"   ,strlen("tria3"   ))){return ( tri ) ;}
    else if (!strncmp (str, "quad4"   ,strlen("quad4"   ))){return ( qua ) ;}
    else                                                   {return ( noEl );}
  }
}


/******************************************************************************/


/******************************************************************************
  ensr_set_ppVx:   */

/*! Given an ensight element string, return the number of nodes if its a face.
 */

/*  
  Last update:
  ------------
  2Feb12; derived from ensr_name_elt
  
  Input:
  ------
  str: ensight element type descriptor.
  
  Returns:
  --------
  number of forming nodes if it is a 2D face, 0 otherwise.
  
*/

int ensr_isFace ( char *str, int mDim ) {
  if ( mDim == 3 ) {
    if ( !strncmp ( str, "tria3", strlen( "tria3" ) ) ) { return ( 3  ) ; }
    else if ( !strncmp ( str, "quad4", strlen( "quad4" ) ) ) { return ( 4  ) ; }
    else                                                      return ( 0 ) ;
  }
  else{
    if ( !strncmp ( str, "bar2", strlen( "tria3" ) ) ) { return ( 2  ) ; }
    else                                                      return ( 0 ) ;
  }
}

/******************************************************************************
  ensr_var_part_hdr:   */

/*! Read the 'part' headers in all variable files.
 */

/*
  
  Last update:
  ------------
  26Apr15: conceived.
  

  Input:
  ------
  

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ensr_var_part_hdr ( ensrVar_s *pVarFl, int partNo, 
                         ensFlFmt_e flFmt, ensFlEnd_e flEnd ) {

  int k ;
  FILE *fVar ; 
  char line[LINE_LEN] ;
  int partNoV ;
  for ( k = 0 ; k < pVarFl->mVarFls ; k++ ) {

    fVar = pVarFl->pfVar[k] ;
    ensr_fread ( line, sizeof(char), 80, fVar, ensr_char, flFmt, flEnd ) ;

    if ( strncmp( line, "part", sizeof("part") ) ) {
      sprintf ( hip_msg, "ensr_geo: expected `part' in var file,"
                " found %s", line ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    ensr_fread ( &partNoV, sizeof(int), 1, fVar, 
                 ensr_int, flFmt, flEnd ) ;

    if ( partNoV != partNo ) {
      sprintf ( hip_msg, "ensr_geo: expected part %d in var file,"
                " found %d.\n"
                "Parts need to be ordered the same in geo and var", 
                partNo, partNoV ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  return;
}


/******************************************************************************
  ensr_geo:   */

/*! Read the ensight geometry file
 */

/*
  
  Last update:
  ------------
  15jul17; init mVxOffset to silence -O3 compiler warning.
  1Jul16; new interface to check_uns.
  25Apr15; read solution.
  22Apr15; allow 2d grids.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  30Jun12; update to read correctly if node ids are given.
  6Feb12; pass pVxBasePart.
  15Jan12: conceived.
  

  Input:
  ------
  fGeo: opened geometry file.
  skip: if non-zero, only parse the file and find the dimension
  mDim: expected mesh dimension
  pVarFl: data on the variable files.

  Output:
  -------
  *pmVolElems: number of elements of mDim
  *pmBndFaces: number of elements of mDim-1
    
  Returns:
  --------
  pGrid: filled grid structure.
  
*/

grid_struct *ensr_geo_sol ( FILE *fGeo, int skip, int mDim, 
                            int *pmVolElems, int *pmBndFaces, 
                            ensrVar_s *pVarFl ) {

  /* Determine file format and strip off first line w/ format header. */
  ensFlFmt_e flFmt ;
  ensFlEnd_e flEnd ;
  ensr_file_format ( fGeo, &flFmt, &flEnd ) ;

  grid_struct *pGrid = NULL ;
  uns_s *pUns = NULL ;
  chunk_struct *pChunk ;
  int k, kOld, kOldp ;
  char line[LINE_LEN] ;
  if ( !skip ) {
    pGrid = make_grid () ;
    pGrid->uns.type = uns ;
    pUns = make_uns ( pGrid ) ;
    pUns->mDim = mDim ; 
    pGrid->uns.pUns = pUns ;
    pGrid->uns.pVarList = &(pUns->varList) ;
    pUns->nr = pGrid->uns.nr = Grids.mGrids ;

    /* Alloc a chunk, but no vertices, elements, faces. */
    pChunk = append_chunk ( pUns, mDim, 0, 0, 0, 0, 0, 0 ) ;
  
    /* Make this grid the current one. */
    Grids.PcurrentGrid = pGrid ;


    /* Re-stagger the variable names, since we now know what
       the actual dimension of the mesh is. */
    if ( mDim != MAX_DIM ) {
      kOld = pVarFl->kVarPos[0] ;
      for ( k = 0 ; k < pVarFl->mVarFls ; k++ ) {
        kOldp = pVarFl->kVarPos[k+1] ;
        pVarFl->kVarPos[k+1] = 
          ( kOldp - kOld == MAX_DIM ? 
            pVarFl->kVarPos[k]+mDim : pVarFl->kVarPos[k]+1 ) ;
        kOld = kOldp ;
      }
    }

    /* Define the variables with the grid. */
    pUns->varList.mUnknowns = pVarFl->kVarPos[ pVarFl->mVarFls] ; 
    pUns->varList.mUnknFlow = mDim+2 ;
    pUns->varList.varType = noType ; // Type is not defined by format.
    var_s *pVar = pUns->varList.var ;


    int nDim ;
    char strDir[MAX_DIM][3] = { "_x", "_y", "_z" } ;
    for ( k = 0 ; k < pVarFl->mVarFls ; k++ ) {

      if ( pVarFl->kVarPos[k+1] - pVarFl->kVarPos[k] == mDim ) {
        /* Vector. */
        sprintf ( hip_msg, "     found vector var `%s'", pVarFl->varName[k] ) ;
        hip_err ( blank, 1, hip_msg ) ;
        for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
          strncpy( pVar->name, pVarFl->varName[k], LEN_VARNAME ) ;
          strncat( pVar->name, strDir[nDim], LEN_VARNAME-2 ) ;
          pVar->flag = 1;
          strcpy ( pVar->grp,"add");
          pVar->isVec = nDim+1 ;
          pVar++ ;
        }
      }
      else {
        /* Scalar. */
        sprintf ( hip_msg, "found scalar var `%s'", pVarFl->varName[k] ) ;
        hip_err ( info, 1, hip_msg ) ;
        strncpy( pVar->name, pVarFl->varName[k], LEN_VARNAME ) ;
        strcpy ( pVar->grp,"add");
        pVar->flag = 1;
        pVar->cat = add; 
        pVar++ ;
      } 
    }



    for ( k = 0 ; k < pVarFl->mVarFls ; k++ ) {
      /* Scan the first line of the var file, warn if different. */
      ensr_fread ( line, sizeof(char), 80, 
                   pVarFl->pfVar[k], ensr_char, flFmt, flEnd ) ;
      if ( strncmp( line, pVarFl->varName[k], strlen( pVarFl->varName[k] ))) {
        sprintf ( hip_msg, 
                  " variable name in case file is `%s' (used),\n"
                  "             which differs from"
                  " name in file `%s' (disregarded).", 
                  pVarFl->varName[k], line ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
    }
  }
  ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, flFmt, flEnd ) ;
  /* Pick the first description line as grid name. */

  if ( !skip )
    sscanf ( line, "%s", pUns->pGrid->uns.name ) ;

  /* Skip the second description line. */
  ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, flFmt, flEnd ) ;

  /* node id. */
  ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, flFmt, flEnd ) ;
  int node_id ;
  char keyword[LINE_LEN] ;
  sscanf ( line, "node id %s", keyword ) ;
  switch ( keyword[0] ) {
  case ( 'o' ) : node_id = 0 ; break ; /* off. */
  case ( 'a' ) : node_id = 0 ; break ; /* assign, = off. */
  case ( 'g' ) : node_id = 1 ; break ; /* given. */
  case ( 'i' ) : node_id = 1 ; break ; /* 'ignore' for ensight, but not us. */
  default : node_id = 0 ;
  }


  /* element id. */
  ensr_fread ( line, sizeof(char), 80, fGeo, ensr_char, flFmt, flEnd ) ;
  int elem_id ;
  sscanf ( line, "element id %s", keyword ) ;
  switch ( keyword[0] ) {
  case ( 'o' ) : elem_id = 0 ; break ; /* off. */
  case ( 'a' ) : elem_id = 0 ; break ; /* assign, = not given = off. */
  case ( 'g' ) : elem_id = 1 ; break ; /* given. */
  case ( 'i' ) : elem_id = 1 ; break ; /* ignore. */
  default :      elem_id = 0 ;
  }

  /* Loop over all parts. */
  float dumVar[6] ;
  char partName[LINE_LEN] ;
  int partNo=0 ;
  elType_e elType ;
  int mVolVx = 0, mVxPart ;
  int mVxOffset=0 ;
  int *pNodeIds = NULL ;
  int mVxFc ;
  int mParts = 0 ;
  FILE *fVar ;
  int partNoV ;
  *pmVolElems = *pmBndFaces = 0 ;
  while (!feof( fGeo) ) {
    if ( ensr_fread ( line, sizeof(char), 80, 
                      fGeo, ensr_char, flFmt, flEnd ) ) {


      if ( !strncmp ( line, "extents", strlen("extents") ) ){
        /* Skip 6 floats. */
        if ( skip ) hip_err ( info, 4, "Found mesh extents");
        ensr_fread ( dumVar, sizeof(float), 6, 
                     fGeo, ensr_float, flFmt, flEnd ) ;

        }
      else if ( !strncmp ( line, "part", strlen("part") ) ) {
        /* part header only. */
        partNo = ensr_part ( fGeo, skip, flFmt, flEnd, partName, pUns ) ;
        mParts++ ;
        sprintf ( hip_msg, "Found part %d : '%s'",partNo,partName) ;
        if ( skip ) hip_err ( info, 0, hip_msg);
        mVxPart = 0 ;

        if ( !skip ) {
          ensr_var_part_hdr ( pVarFl, partNo, flFmt, flEnd ) ;
          /* Take the first listed part as the volumetric one. */
          if ( !mVolVx ) mVolVx = pUns->pRootChunk->mVerts ;
          /* Move all variable files forward past this part header. */
        }
      }


      else if ( !strncmp ( line, "coordinates", strlen("coordinates") ) ) {
        /* Coordinates. */
        if ( !skip ) hip_err ( info, 4, "Found mesh coordinates");
        mVxPart = ensr_coor ( fGeo, skip, flFmt, flEnd, node_id, pUns, 
                              &mVxOffset, &pNodeIds, pVarFl ) ;
      }


      else if ( ( elType = ensr_name_elt ( line, mDim ) ) != noEl ) {
        /* Elements. */
        if ( !skip ) hip_err ( info, 4, "Found mesh elements");
        *pmVolElems += 
        ensr_elem ( fGeo, skip, flFmt, flEnd, elem_id, elType, partNo, 
                    pUns, mVxOffset, node_id, pNodeIds ) ;
      }


      else if ( ( mVxFc = ensr_isFace ( line, mDim ) ) ) {
        /* Boundary faces. */
        if ( !skip ) hip_err ( info, 4, "Found mesh boundary faces");
        *pmBndFaces += ensr_face ( fGeo, skip, flFmt, flEnd, elem_id, partName, 
                                   mVxFc, pUns, mVxOffset, node_id, pNodeIds ) ;
      }


      else if ( mDim== 3 && ( mVxFc = ensr_isFace ( line, 2 ) ) ) {
        /* Boundary faces of lower dim. */
        if ( skip ) hip_err ( info, 4, "Found mesh lower dim boundary faces");
        ensr_face ( fGeo, skip, flFmt, flEnd, elem_id, partName, 
                    mVxFc, pUns, mVxOffset, node_id, pNodeIds ) ;
      }


      else if ( !strncmp ( line, "block", strlen("block") ) ) {
        hip_err ( fatal, 0, "block-structured meshes are not yet supported." ) ;
      }


      else {
        sprintf ( hip_msg, "unknown section keyword %s in ensr_geo.", line ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  }


  if ( !skip ) {
    /* Node ids are kept live through each part. re-alloced when 
       reading new coors, finished now. */
    arr_free ( pNodeIds ) ;

    /* No further reallocs of vertex pointers, final setting of pointers. */
    ensr_set_ppVx ( pUns ) ;

    /* Alloc for boundary faces/patches. */
    append_bndFc ( pUns->pRootChunk, mParts, *pmBndFaces ) ;

    /* Make a list of chunks, set vxCpt needed for match_bndFcVx. */
    make_uns_ppChunk ( pUns ) ;

    int mDuplVx ;
    if ( !node_id ) {
      hip_err ( info, 1, "No node ids found, performing tree search for coordinates,\n"
                         "             this search will use extra CPU and memory." ) ;

      /* Remove duplicate vertices from boundary parts. */
      mDuplVx = merge_vrtx_chunk ( pChunk, mVolVx, pChunk->mVerts ) ;
      /*
      if ( pChunk->mVerts - mVolVx - mDuplVx  ) {
        sprintf ( hip_msg, "expected %"FMT_ULG", but found %d duplicate vertices"
                  " in the boundary parts in ensr_geo.", 
                  pChunk->mVerts - mVolVx, mDuplVx );
        hip_err ( warning, 1, hip_msg ) ;
      }
      */
    }


    if ( negVol_flip )
      /* Check for element types that have all negative volumes and flip. */
      flip_negative_volumes ( pUns ) ;


    /* Find boundary face to element pointers. */
    match_bndFcVx ( pUns ) ;
    arr_free ( pUns->pBndFcVx ) ; pUns->pBndFcVx = NULL ;

    /* If there is only one volumetric zone, remove it. */
    if ( pUns->mZones == 1 )
      zone_del ( pUns, "1" ) ;

    /* Housekeeping. */
    check_uns ( pUns, check_lvl ) ;
  }
    
  return ( pGrid ) ;
}

/******************************************************************************
  ensr_close:   */

/*! close geometry and variable files.

 */

/*
  
  Last update:
  ------------
  25Apr15: conceived.
  

  Input:
  ------
  mflVars: number of open geometry files

  Changes To:
  -----------
  fGeo: open geometry file
  pflVars: list of geometry files.

  
*/

void ensr_close  ( FILE *fGeo, ensrVar_s *pVarFl ) {

  fclose ( fGeo ) ;

  int k ;
  for ( k = 0 ; k < pVarFl->mVarFls ; k++ ) 
    fclose ( pVarFl->pfVar[k] ) ;

  return ;
}

/******************************************************************************
  ensr_args:   */

/*! parse command line arguments for read_ensight.
 *
 */

/*
  
  Last update:
  ------------
  27Apr15: derived from h5r_args.
  

  Input:
  ------
  argLine: command lines with the command 

  Output:
  -------
  caseFile: name of 
  nStepToRead: optional arg of timeset no to read.
    
*/

void ensr_args  ( char argLine[], char*caseFile, int *pnStepToRead ) {

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  /* Parse line of unix-style optional args. */
  char c ;
  while ((c = getopt_long ( mArgs, ppArgs, "t:",NULL,NULL)) != -1) {
    switch (c)  {
      case 't':
        if ( optarg )
          /* 0 arg given. use -a0 */
          *pnStepToRead = atoi( optarg ) ;
        else 
          *pnStepToRead = 1 ;
        break;
    }
  }
  
  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( caseFile, ppArgs[optind] ) ;
  else
    hip_err ( fatal, 0, "missing case file name for read ensight\n" ) ;

  return ;
}


/******************************************************************************
  reaad_ensight:   */

/*! read an ensight mesh 
 */

/*
  
  Last update:
  ------------
  15Jan12: conceived.
  

  Input:
  ------
  caseFile: name of case file without path.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int read_ensight ( char *argLine ) {

  char caseFile[LINE_LEN] ;
  int nStepToRead = 1 ;
  ensr_args ( argLine, caseFile, &nStepToRead ) ;


  FILE *fGeo ;
  ensrVar_s varFl ;
  ensr_case ( caseFile, nStepToRead, &fGeo, &varFl ) ;

  /* Count 3D elements and 2D faces. */
  int mDim = 3 ;
  int mVolElems, mBndFaces ;
  ensr_geo_sol ( fGeo, 1, mDim, &mVolElems, &mBndFaces, &varFl ) ;
  if ( !mVolElems && !mBndFaces )
    hip_err ( fatal, 0, "found neither 3D nor 2D elements in geo file." ) ;
  else if  ( !mVolElems )
    mDim = 2 ;

  sprintf ( hip_msg, "reading %d-dim grid", mDim ) ;
  hip_err ( info, 1, hip_msg ) ;

  /* Read with volumetric elements of mDim, faces of mDim-1. */
  ensr_geo_sol ( fGeo, 0, mDim, &mVolElems, &mBndFaces, &varFl ) ; 

  /* Close all files. */
  ensr_close ( fGeo, &varFl ) ;

  return ( EXIT_SUCCESS ) ;
}
