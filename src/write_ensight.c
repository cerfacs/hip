/*
   write_ensight.c:
   Write to binary ensight.

   Last update:
   ------------
   22Apr14; rename private write_ensight to ensw_
   30Jun14; increase MAX_VEC to 24.
   10Feb12; fix h2e element definition table.
   18Sep09; debugging completed.
   26May09; conceived.

   This file contains:
   -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

extern const char lastUpdate[] ;
extern const char version[] ;


extern const int verbosity ;
extern char hip_msg[] ;

extern const Grids_struct Grids ;
extern const char version[] ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;
extern const char varCatNames[][LEN_VAR_C] ;


static int ensw_iBuf[MAX_VX_ELEM+1], one=1, t8=80 ;

/* Number of vector fields in the solution.
   Currently only velocity and liquid phase velocity vectors are treated. */
#define MAX_VEC (24)


/* A conversion list from hip to ensight node numbering within an element. 
   Only difference: prism:
   Doc:     hip: 1 2 3 4 5 6
            ens: 1 4 5 2 6 3
   C-code:  hip: 0 1 2 3 4 5
            ens: 0 3 4 1 5 2
*/

/* kVx_ensight[k] = kVx_hip[ h2e[ k ] ]
   writing:
            for ( i = 0 ; i < mVerts ; i++ )
              iBuf[i] = ppVx[ h2e[elType][i] ]->number ;
*/
static const int h2e[6][8] =
{ {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,2,1,3},        /* tet */
  {0,1,2,3,4},      /* pyramid */
  {0,3,5,1,2,4},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;

static const char faceTypeName[5][6] = { "\0", "\0", "bar2\0", "tria3\0", "quad4\0" } ;

/* Default values applied for each write in ensw_args. */
static int ensw_node_id ;
static int ensw_ascii ;
static int ensw_doPromote3D ;
static int ensw_doSurface ;


/******************************************************************************
  ensw_ftn_len:   */

/*! record length.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ensw_ftn_len ( size_t size, size_t mItems, FILE *outFile ) {

  int iData = mItems*size ;
  if ( !ensw_ascii ) {
    FWRITE ( &iData, sizeof(int), 1, outFile ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************
  ensw_ftn_item:   */

/*! single or vector of data item, no size beginning/end.
 *  In the case of binary, the items are just appended, 
 *  in the case of ascii, the items go on the same line, conclueded by a line feed.
 *
 */

/*
  
  Last update:
  ------------
  6Jul16: conceived.
  

  Input:
  ------
  pData: pointer to data to write
  size: size of one data item
  mItems: how many
  fmt: ascii: format for each of the items, space inbetween, \n at end.
  outFile: file handle to write to.
  
*/

void ensw_ftn_item ( const void* pData, size_t size, int mItems,
                     char *len, char type, FILE *outFile ) {

  int i ; 
  char fmt[LINE_LEN] ;
  char cData ;
  int iData ;
  double dData ;
  if ( !ensw_ascii ) {
    FWRITE ( pData, size, mItems, outFile ) ;
  }
  else {
    sprintf ( fmt, "%%%s%c", len, type ) ;
    for ( i = 0 ; i < mItems ; i++ ) {
      switch (type) {
      case 'c' :
        cData = *( (char*) pData ) ;
        fprintf ( outFile, fmt, cData ) ;
        break ;
      case 'd' :
        iData = *( (int*) pData ) ;
        fprintf ( outFile, fmt, iData ) ;
        break ;
      case 'e' :
        dData = *( (float *) pData ) ;
        fprintf ( outFile, fmt, dData ) ;
        break ;
      }
      pData = ( void* ) ( ((char*) pData ) + size ) ;
      // Do not add additional space. Conflicts with connectivity  
      //if ( i != mItems-1 ) fprintf ( outFile, " ")  ;
    }
    fprintf ( outFile, "\n" ) ; 
  }

  return  ;
}



/******************************************************************************
  fun_name:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ensw_ftn_rec ( const void* pData, size_t size, int mItems,
                    char *len, char type, FILE *outFile ) {

  if ( !ensw_ascii )
    ftn_write_rec ( pData, size, mItems, outFile ) ;
  else {
    ensw_ftn_item ( pData, size, mItems, len, type, outFile ) ;
  }

  return ;
}


/******************************************************************************
  ensw_ftn_string_rec:   */

/*! Write a string to binary or ascii.
 *
 *
 */

/*
  
  Last update:
  ------------
  16Dec16; replace a %80s ascii string with %s
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ensw_ftn_string_rec ( const void* pData, size_t size, int mItems,
                           char *fmt, FILE *outFile ) {

  if ( !ensw_ascii )
    ftn_write_rec ( pData, size, mItems, outFile ) ;
  else {
    if ( strcmp ( fmt, "%80s" ) ) // why is there a leading blank with fixed width?
      fprintf ( outFile, fmt, pData ) ;
    else
      fprintf ( outFile, "%s", (char*) pData ) ;

    fprintf ( outFile, "\n" ) ; 
  }

  return ;
}



/******************************************************************************

  ens_elt_name:
  Given an element type, return the ensight style string.
  Note that hip only defines elTypes for volumetric elements, not for 
  boundary faces. 
  
  Last update:
  ------------
  20Dec16; add bi for ensight skeleton edges.
  26May09; conceived
  
  Input:
  ------
  elT: the element type.

  Output:
  ------
  str: ensight element type descriptor.
  
  Returns:
  --------
  1 if the element was recognised, 0 otherwise.
  
*/

int ensw_elt_name ( elType_e elT, char *str ) {

       if ( elT == tri ) { strcpy ( str, "tria3"    ) ; return ( 1 ) ; }
  else if ( elT == qua ) { strcpy ( str, "quad4"    ) ; return ( 1 ) ; }
  else if ( elT == tet ) { strcpy ( str, "tetra4"   ) ; return ( 1 ) ; }
  else if ( elT == pyr ) { strcpy ( str, "pyramid5" ) ; return ( 1 ) ; }
  else if ( elT == pri ) { strcpy ( str, "penta6"   ) ; return ( 1 ) ; }
  else if ( elT == hex ) { strcpy ( str, "hexa8"    ) ; return ( 1 ) ; }
  else if ( elT == bi  ) { strcpy ( str, "bar2"     ) ; return ( 1 ) ; }
  else                                                  return ( 0 ) ;
}


/******************************************************************************

  ensw_open:
  Attach suffix to roofile, prepend path and open file.
  
  Last update:
  ------------
  15Jan12; rename to ensw_open.
  27May09: conceived.
  
  Input:
  ------
  rootFile
  suffix

  Returns:
  --------
  opened file
  
*/

FILE* ensw_open ( const char *rootFile, const char *suffix ) {

  char openFile[LINE_LEN] ;
  FILE *file ;

  sprintf ( openFile, "%s.%s", rootFile, suffix ) ;
  prepend_path ( openFile ) ;
  if ( ( file = fopen ( openFile, "w" ) ) == NULL ) {
    sprintf ( hip_msg, " in ensw_open:\n"
              "        could not open file %s.\n",
              openFile ) ; 
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( file ) ;
}


/******************************************************************************

  ensw_coor:
  Write the a list of coordinates to file, numbered vx only.
  
  Last update:
  ------------
  22Apr14; supply an empty z coordinate field in 2D.
  30Jun12; use only vx flag to determine writing of a vertex or not.
  17Sep09; use and expect mVxToWrite coors, not pUns->mVertsnumbered.
           use global ensw_iBuf.
  26May09: conceived.
  
  Input:
  ------
  pUns: pointer to the unstructured grid.
  mVxToWrite: number of vx to be written.
  fGeo: file.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

void ensw_coor ( uns_s *pUns, int mVxToWrite, int *pNodeIds, FILE *fGeo ) {

  int nDim, nBeg, nEnd ;
  int mVx ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  float fBuf ;


  char someStr[TEXT_LEN] ;  
  sprintf ( someStr, "coordinates" ) ; 
  ftnString ( someStr, 80, someStr ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;

  /* Number of nodes. */
  ensw_iBuf[0]  = mVxToWrite ;
  ensw_ftn_rec ( ensw_iBuf, sizeof(int), 1, "10", 'd', fGeo ) ;

  ulong_t *pNr2 ;
  if ( ensw_node_id ) {
    /* Write out list of node id's. Binary: one record. Ascii: one per line. */

    ensw_ftn_len ( mVxToWrite, sizeof(int), fGeo ) ;

    mVx = 0 ;
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) ) {
      pNr2 = pChunk->pVrtxNr2 ;
        
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( is_vx_flagged( pVrtx ) ) {
          ensw_iBuf[0] = pNr2[ pVrtx->vxCpt.nr ] ;
          ensw_ftn_item  ( ensw_iBuf, sizeof(int), 1, "10", 'd', fGeo ) ;
          mVx++ ;
        }

      ensw_ftn_len ( mVxToWrite, sizeof(int), fGeo ) ;

      if ( mVx != mVxToWrite ) {
        sprintf ( hip_msg, " when writing node_ids in ensw_coor:\n"
                  "        %d nodes expected, but %d found.", mVxToWrite, mVx ) ; 
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  }

  /* List of coordinates. */
  mVx = 0 ;
  for ( nDim = 0 ; nDim < pUns->mDim ; nDim++ ) {

    /* Each dir on its own line. */
    ensw_ftn_len ( mVxToWrite, sizeof(float), fGeo ) ;

    mVx = 0 ;
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( is_vx_flagged( pVrtx ) ) {
          mVx++ ;
          fBuf = (float) pVrtx->Pcoor[nDim] ;
          ensw_ftn_item ( &fBuf, sizeof( float), 1, "12.5", 'e', fGeo ) ;
        }

    if ( mVx != mVxToWrite ) {
      sprintf ( hip_msg, " when writing coors in ensw_coor:\n"
                "        %d nodes expected, but %d found.", mVxToWrite, mVx ) ; 
      hip_err ( fatal, 0, hip_msg ) ;
    }

    ensw_ftn_len ( mVxToWrite, sizeof(float), fGeo ) ;
  }

  /* In 2-D: supply a zero z-coordinate. */
  int nVx ;
  fBuf = 0.0 ;
  if ( pUns->mDim < 3 ) {
    /* Each dir on its own line. */
    ensw_ftn_len ( mVxToWrite, sizeof(float), fGeo ) ;
    for ( nVx = 0 ; nVx < mVxToWrite ; nVx++ )
      ensw_ftn_item ( &fBuf, sizeof( float), 1, "12.5", 'e', fGeo ) ;
    ensw_ftn_len ( mVxToWrite, sizeof(float), fGeo ) ;
  }



  return ;
}


/******************************************************************************

  ensw_part_hdr:
  Write the header for a new part to ensight.
  
  Last update:
  ------------
  17Jun09; use new intfc to ftnString.
  26May09: conceived.
  
  Input:
  ------
  fGeo: file

*/

void ensw_part_hdr ( int no, FILE *file ) {

  char someStr[TEXT_LEN] ;
  ftnString ( someStr, 80, "part" ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , file ) ;

  ensw_ftn_rec ( &no, sizeof(int), 1, "10", 'd', file ) ;

  return ;
}



/******************************************************************************

  ensw_part_hdr_geo:
  Write the header for a new part in the geo file to ensight.
  
  Last update:
  ------------
  26May09: conceived.
  
  Input:
  ------
  fGeo: file

*/

void ensw_part_hdr_geo ( int no, char *partName, FILE *file ) {

  ensw_part_hdr ( no, file ) ;

  char someStr[TEXT_LEN] ;
  sprintf ( someStr, "%s", partName ) ; 
  ftnString ( someStr, 80, someStr ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , file ) ;

  return ;
}




/******************************************************************************

  ensw_sol_part:
  Write the solution of part nPart to an ensight gold case file. This routine
  requires that numbering runs only over the nodes of that part.
  
  Last update:
  ------------
  9Jul15; initialise mVxWr to suppress compiler warning.
  25Apr15; allow 2D solutions.
  28Aug11; allow more than just the flow vector.
  20Sep09; test whether flV files are opened.
  18Sep09; add 'coordinates' tag.
           allow for mEqu=0.
  17Sep09; fix bug with init of pChunk prior to loop_verts.
           close flV inside the if-block.
           redefine kFlo[3].
           pass opened file pointers in rather than reopening.
  26May09: conceived.
  
  Input:
  ------
  pUns: pointer to the unstructured grid.
  nPart: part number
  mVxNum: number of active vertices in this part.
  mVec: number of vectors
  kVec[][0,1,2]: index of the 3 vector variables.
  flVec[]: opened files for the velocity vectors
  flVars: table of opend file for the scalar variables.

*/

void ensw_sol_part ( uns_s *pUns, const int nPart, const int mVxNum,
                          const int mVec, int kVec[][MAX_DIM], FILE *flVec[], 
                          FILE *pflVars[MAX_UNKNOWNS] ) {

  FILE *flV ;
  int nD=0, mVxWr=0 ;
  chunk_struct *pChunk ;
  vrtx_struct *pVxBeg, *pVxEnd ;
  const vrtx_struct *pVrtx;
  int nBeg, nEnd ;
  int nVa ;
  const varList_s *pVL = &pUns->varList ;
  const var_s *pVar ;
  float fBuf ;
  
  if ( pUns->varList.mUnknowns == 0 )
    return ;



  /* Write the vectors. */
  int iVec ;
  const int *kFlo ;
  char someStr[TEXT_LEN] ;
  for ( iVec = 0 ; iVec < mVec ; iVec++ ) {
    flV = flVec[iVec] ;
    kFlo = kVec[iVec] ;

    if ( !flV ) {
      sprintf ( hip_msg, "in ensw_sol_part:"
                "        flow vector solution file not opened." ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }


    ensw_part_hdr( nPart, flV ) ;

    ftnString ( someStr, 80, "coordinates" ) ;
    ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , flV ) ;
     
    for ( nD = 0 ; nD < pUns->mDim ; nD++ ) {
      ensw_ftn_len ( mVxNum, sizeof(float), flV ) ;

      pChunk = NULL ;
      mVxWr = 0 ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
        for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
          if ( is_vx_flagged( pVrtx ) ) {
            fBuf = pVrtx->Punknown[ kFlo[nD] ] ;
            ensw_ftn_item( &fBuf, sizeof( float ), 1, "12.5", 'e', flV ) ;
            mVxWr++ ;
          }

      if ( mVxWr != mVxNum ) {
        sprintf ( hip_msg, "in ensw_sol_part:"
                  "        for flow vector dim %d, expected %d, found %d vertices.", 
                  nD+1, mVxNum, mVxWr ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      ensw_ftn_len ( mVxNum, sizeof(float), flV ) ;
    }

    /* In 2-D: supply a zero z-coordinate. */
    int nVx ;
    fBuf = 0.0 ;
    if ( pUns->mDim < 3 ) {
      /* Each dir on its own line. */
      ensw_ftn_len ( mVxNum, sizeof(float), flV ) ;
   
      for ( nVx = 0 ; nVx < mVxWr ; nVx++ )
        ensw_ftn_item ( &fBuf, sizeof( float), 1, "12.5", 'e', flV ) ;

      ensw_ftn_len ( mVxNum, sizeof(float), flV ) ;
    }

  }
    



  /* Remaining scalar variables for this part. */
  for ( nVa = 0 ; nVa < pVL->mUnknowns ; nVa++ ) {
    pVar = pVL->var + nVa ;

    /* Exclude the velocity vector. */
    if ( pVL->var[nVa].isVec )
      ;
    else if ( pVar->flag || pVar->cat == ns ) {
      /* This variable is flagged, file it. */
      flV = pflVars[nVa] ;
      if ( !flV ) {
        sprintf ( hip_msg, "in ensw_sol_part:"
                  "        scalar solution file for var no %d not opened.", nVa ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      ensw_part_hdr( nPart, flV ) ;

      ftnString ( someStr, 80, "coordinates" ) ;
      ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , flV ) ;


      ensw_ftn_len ( mVxNum, sizeof(float), flV ) ;

      pChunk = NULL ;
      mVxWr = 0 ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
        for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
          if ( is_vx_flagged( pVrtx ) ) {
            mVxWr++ ;
            fBuf = pVrtx->Punknown[nVa] ;
            ensw_ftn_item ( &fBuf, sizeof( float), 1, "12.5", 'e', flV ) ;
          }

      if ( mVxWr != mVxNum ) {
        sprintf ( hip_msg, "in ensw_sol_part:"
                  "        for scalar %d, expected %d, found %d vertices", 
                  nVa+1, mVxNum, mVxWr ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      ensw_ftn_len ( mVxNum, sizeof(float), flV ) ;
    }
  }

  return ;
}


/******************************************************************************

  ensw_headers:
  Write the header to an ensight gold geom file.
  Write headers to all solution files
  Write the case file.
  
  Last update:
  ------------
  1Jul14; fix bug with mixed-up enums: using noVar where it should have been noCat.
  30Jun14; test whether number of vec vars exceeds max size.
  24Feb14; remove stary if (0) block around vector variable treatment.
  2Feb12; write grid name as first descriptor.
  31Jan12; turn node_id on.
  26Apr11; remove duplicate prepend_path.
  20Sep09; include ns as compulsory vars.
  16Sep09; close fCase at end of routine.
  17Jun09; use new intfc to ftnString.
  26May09: conceived.
  
  Input:
  ------
  pUns: pointer to the unstructured grid.
  rootFile: name of the root file.

  Changes To:
  -----------

  Output:
  -------
  pfGeo: open .geo file
  pmVec: number of vector variables
  kVec[][0..3]: index of the vector var in 0,1,2 dir, for each vector
  flVec[]: opened file for the velocity vectors
  flVars[] : array of files for the remaining scalars.
  
  
  Returns:
  --------
  file handle to the geometry file.
  
*/

void  ensw_headers ( uns_s *pUns, const char rootFile[], FILE **pfGeo, 
                              int *pmVec, int kVec[MAX_VEC][MAX_DIM], 
                              FILE *pflVec[MAX_VEC], 
                              FILE *pflVars[MAX_UNKNOWNS] ) {

  char rtFlPath[LINE_LEN] ;
  FILE *fGeo, *fCase ;
  char someStr[TEXT_LEN] ;
  varList_s *pVL = &(pUns->varList) ;
  int nVa ;
  char fileName[TEXT_LEN] ;
  FILE *flV = NULL ;
  const var_s *pVar ;

  /* Reset flVars. */
  *pflVec = NULL ;
  for ( nVa = 0 ; nVa < MAX_UNKNOWNS ; nVa++ )
    pflVars[nVa] = NULL ;


  /* Case file. */
  sprintf ( rtFlPath, "%s", rootFile ) ;
  
  fCase = ensw_open ( rtFlPath, "case" ) ;

  fprintf ( fCase, "FORMAT\ntype: ensight gold\n\n" ) ;
  fprintf ( fCase, "GEOMETRY\nmodel: %s.geo\n\n", rootFile ) ;
  
  


  /* Geometry. */
  *pfGeo = fGeo = ensw_open ( rtFlPath, "geo" ) ;

  if ( !ensw_ascii ) {
    ftnString ( someStr, 80, "Fortran Binary" ) ;
    ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;
  }
  sprintf ( someStr, "%s", pUns->pGrid->uns.name ) ; 
  ftnString ( someStr, 80, someStr ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;

  sprintf ( someStr, "hip, version %s of %s", version, lastUpdate ) ; 
  ftnString ( someStr, 80, someStr ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;

  if ( ensw_node_id )
    sprintf ( someStr, "node id given" ) ; 
  else
    sprintf ( someStr, "node id off" ) ; 
  ftnString ( someStr, 80, someStr ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;

  sprintf ( someStr, "element id off" ) ; 
  ftnString ( someStr, 80, someStr ) ;
  ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;



  /* Solution. */
  int kVC = -1 ;
  *pmVec = 0 ;
  if ( pUns->varList.mUnknowns ) {
    fprintf ( fCase, "VARIABLE\n" ) ;

    /* Vector variables. */    
    while ( next_vec_var ( pVL, pUns->mDim, noCat, &kVC, kVec[*pmVec] ) ) {

      if ( *pmVec+1 >= MAX_VEC )
        hip_err ( fatal, 0, "too many vector variables in ensw,"
                  " increase MAX_VEC." ) ;

      /* Build a filename. */
      if ( pVL->var[ kVec[*pmVec][0] ].cat == ns )
        strcpy ( someStr, "velocity" ) ;
      else if ( pVL->var[ kVec[*pmVec][0] ].cat == tpf )
        strcpy ( someStr, "liquidVelocity" ) ;
      else {
        /* Assume a _x, _y .. namoing scheme for vectors. */
        strcpy( someStr, pUns->varList.var[ kVec[*pmVec][0] ].name ) ;
        someStr[ strlen (  pUns->varList.var[ kVec[*pmVec][0] ].name) -2 ] = '\0' ; 
      }
      snprintf ( fileName, LINE_LEN-1, "%s_%s.var", rootFile, someStr ) ;

      if ( verbosity > 2 )
        printf ( "          writing vec. '%s' to: %s\n", someStr, fileName ) ;
      fprintf ( fCase, "vector per node:    %s    %s\n", someStr, fileName ) ;
    
      prepend_path ( fileName ) ;
      if ( !( pflVec[*pmVec] = flV = fopen ( fileName, "w" ) ) ) {
        sprintf ( hip_msg, "could not open variable file %s", fileName );
        hip_err ( fatal, 0, hip_msg ) ;
      }

      (*pmVec)++ ;

    
      /* Header. Ensight expects the tag 'velocities' for all vectors. */
      sprintf ( someStr, "velocities") ;
      ftnString ( someStr, 80, someStr ) ;
      ensw_ftn_string_rec ( someStr, sizeof( char ), 80, "%80s", flV ) ;
    }
  



    /* Loop over all remaining scalars. */
    for ( nVa = 0 ; nVa < pVL->mUnknowns ; nVa++ ) {
      /* Exclude the velocity vector. */
      if ( !pVL->var[nVa].isVec ) {
        pVar = pVL->var + nVa ;

        if ( pVar->flag || pVar->cat == ns ) {
          /* This variable is flagged, file it. */
          sprintf ( fileName, "%s_%s.var", rootFile, pVar->name ) ;
          if ( verbosity > 2 )
            printf ( "          writing var. '%s' to: %s\n", pVar->name, fileName ) ;
          fprintf ( fCase, "scalar per node:    %s    %s\n", pVar->name, fileName ) ;

          prepend_path ( fileName ) ;
          if ( !( pflVars[nVa] = flV = fopen ( fileName, "w" ) ) ) {
            sprintf ( hip_msg, "in ensw_headers:"
                      "        could not open file %s", fileName ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }

          /* Header. */
          sprintf ( someStr, "%s", pVar->name ) ;
          ftnString ( someStr, 80, someStr ) ;
          ensw_ftn_string_rec ( someStr, sizeof( char ), 80, "%80s", flV ) ;
        }
      }
    }
  }

  fclose ( fCase ) ;

  return ;
}

/******************************************************************************

  ensw_elem_conn:
  Write all element connectivity.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  26Aug11; reset mConnWr within the elem type loop.
  18Sep09; base count on mElemsOfType, rather than mElems_w_mVerts.
           all conn entries for same type on same record.
  17Sep09: extracted from ensw_volume.
  
  Input:
  ------
  pUns: grid to write.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int ensw_elem_conn ( uns_s *pUns, FILE *fGeo ) {

  elType_e elType ;
  ulong_t mElems ;
  ulong_t mVerts ;
  char someStr[TEXT_LEN] ;
  chunk_struct *pChunk ;
  elem_struct *pElEnd, *pElBeg ;
  const elem_struct *pEl ;
  vrtx_struct **ppVx ;
  ulong_t i ;
  ulong_t mConnWr ;


  /* Elements.
     ----------------*/
  for ( elType = tri ; elType <= hex ; elType++ ) {
    /* Base elements only, for the time being. */
    mElems = pUns->mElemsOfType[elType] ;

    if ( mElems ) {
      mVerts = elemType[elType].mVerts ;

      /* Which element type? */
      if ( !ensw_elt_name ( elType, someStr ) ) {
        sprintf ( hip_msg, " in ensw_elem_conn:\n"
                  "        unrecognised element type %d.",
                  elType ) ; 
        hip_err ( fatal, 0, hip_msg ) ;
      }

      ftnString ( someStr, 80, someStr ) ;
      ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;

      ensw_ftn_rec ( pUns->mElemsOfType+elType, sizeof(int), 1, "10", 'd', fGeo ) ;
      
      /* Connectivity, all for the same type on same record. */
      ensw_ftn_len ( mElems*mVerts, sizeof(int), fGeo ) ;

      pChunk = NULL ;
      mConnWr = 0 ;
      while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
        for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
          if ( pEl->number && pEl->elType == elType ) {
            ppVx = pEl->PPvrtx ;

            for ( i = 0 ; i < mVerts ; i++ )
              ensw_iBuf[i] = ppVx[ h2e[elType][i] ]->number ;

            ensw_ftn_item ( ensw_iBuf, sizeof(int), mVerts, "10", 'd', fGeo ) ;
            mConnWr += mVerts ;
          }

      /* Trailing record length. */
      ensw_ftn_len ( mElems*mVerts, sizeof(int), fGeo ) ;


      if ( mConnWr != mElems*mVerts ) {
        sprintf ( hip_msg, " in ensw_elem_conn:\n"
                  "        expected %"FMT_ULG" conn entries, wrote %"FMT_ULG".",
                  mElems*mVerts, mConnWr ) ; 
        hip_err ( fatal, 0, hip_msg ) ;
      }

    }
  }

  return ( 1 ) ;
}


/******************************************************************************

  ensw_volume:
  Write the header to an ensight gold geom file.
  
  Last update:
  ------------
  16Dec16; exclude writing volume for surface grids.
  21Sep16; new interface to increment_uns_vert_number
  01Jul12; renumber vertices before writing.
  17Sep09; write volume solution, call ensw_sol_part.
           extract conn writing into its own function.
  26May09: conceived.
  
  Input:
  ------
  pUns: pointer to the unstructured grid.
  rootFile: name of the root file.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int ensw_volume ( uns_s *pUns, FILE *fGeo, 
                  int mVec, int kVec[][MAX_DIM], FILE *flVec[], 
                  FILE *flVars[MAX_UNKNOWNS] ) {

  if ( pUns->specialTopo != surf ) {
    /* New part, number the vol grid mBc+1.*/
    ensw_part_hdr_geo ( pUns->mBc+1, "Volume Grid", fGeo ) ;

    /* Coordinates.*/

    /* Flag all volume-referenced vertices. */
    int mVxVol =  uns_flag_vx_vol ( pUns, "ensw_volume" ) ;
    const int doReset = 1 ;
    increment_uns_vert_number ( pUns, doReset ) ;
    ensw_coor ( pUns, mVxVol, NULL, fGeo ) ;
    free_vx_flag ( pUns ) ;


    /* Elements. */
    ensw_elem_conn ( pUns, fGeo ) ;

    /* Solution, part mBc+1 is the volume. */
    ensw_sol_part ( pUns, pUns->mBc+1,  pUns->mVertsNumbered,
                    mVec, kVec, flVec, flVars ) ;
  }

  return (1) ;
}

/******************************************************************************

  ensw_bnd_conn:
  Write connectivity of boundary faces.
  
  Last update:
  ------------
  5jul16; write boundary perimeter and feature edges, if present.
  7Apr13; promote possibly large int to type ulong_t.
  18Sep09; write all connectivity info for the same type on one record.
  17Sep09: extracted from ensw_bnd.
  
  Input:
  ------
  pUns: pointer to grid to write.
  nBc: boundary to write.
  mFaces[] : number of faces of each type, identified by number of forming nodes.
  fGeo: opened file to write to.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int ensw_bnd_conn ( uns_s *pUns, const int nBc, 
                         const ulong_t mFaces[MAX_VX_FACE+1], FILE* fGeo ) {

  int mVxFc ;
  ulong_t mFc ;
  char someStr[TEXT_LEN] ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcBeg, *pBndFcEnd, *pBndFc ;
  const elem_struct *pElem ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int i ;


  /* Boundary faces.
     ----------------*/
  for ( mVxFc = 2 ; mVxFc <= 4 && ensw_doSurface ; mVxFc++ ) {
    /* We want boundary faces only. */
    mFc = mFaces[mVxFc] ;

    if ( mFc ) {
      /* Which element type? */
      ftnString ( someStr, 80, faceTypeName[mVxFc] ) ;
      ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;
      ensw_ftn_rec ( &mFc, sizeof(int), 1, "10", 'd', fGeo ) ;

            
      /* Connectivity, all entries for the same elem/face type on one line. */
      ensw_ftn_len ( mFc*mVxFc, sizeof(int), fGeo ) ;

      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pElem = pBndFc->Pelem ;
          if ( pElem && pElem->number && pBndFc->nFace ) {
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            if ( mVxFc == pFoE->mVertsFace ) {
              kVxFc = pFoE->kVxFace ;
              ppVx = pElem->PPvrtx ;

              for ( i = 0 ; i < mVxFc ; i++ )
                ensw_iBuf[i] = ppVx[ kVxFc[i] ]->number ;

              ensw_ftn_item ( ensw_iBuf, sizeof(int), mVxFc, "10", 'd', fGeo ) ;
            }
          }
        }

      /* Trailing record length */
      ensw_ftn_len ( mFc*mVxFc, sizeof(int), fGeo ) ;
    }
  }



  /* 3D perimeter and feature edges, if present. 
     Can only be in the root chunk. */
  pBndPatch = pUns->pRootChunk->PbndPatch+nBc+1 ; 
  int mEgPerFeat = pBndPatch->mBndEg ;
  egVx_s *pEgPF ;
  if ( pUns->mDim == 3 && mEgPerFeat ) {
    ftnString ( someStr, 80, faceTypeName[2] ) ;
    ensw_ftn_string_rec ( someStr, sizeof(char), 80, "%80s" , fGeo ) ;
    ensw_ftn_rec ( &mEgPerFeat, sizeof(int), 1, "10", 'd', fGeo ) ;

    /* Connectivity, for binary all entries for the same elem/face type on one line. */
    ensw_ftn_len ( mEgPerFeat*2, sizeof(int), fGeo ) ;

    for ( pEgPF = pBndPatch->pBndEg ; 
          pEgPF < pBndPatch->pBndEg + mEgPerFeat ; pEgPF++ ) {
      ensw_iBuf[0] = pEgPF->pVx[0]->number ;
      ensw_iBuf[1] = pEgPF->pVx[1]->number ;
      ensw_ftn_item ( ensw_iBuf, sizeof(int), 2, "10", 'd', fGeo ) ;
    }

    /* Trailing record length */
     ensw_ftn_len ( mEgPerFeat*2, sizeof(int), fGeo ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  ensw_bnd:
  Write the boundaries to an ensight gold geom file.
  
  Last update:
  ------------
  21Sep16; new interface to increment_uns_vert_number
  30Jun12; use local numbering for connectivity, write node_id record separately.
  18Sep09; fix bug with index vs. part no.
  17Sep09; pass number of bnd vx, not all vx, to ensw_coor.
           pass pointer, not ref for mFc into ftn_write_rec.
  26May09: conceived.
  
  Input:
  ------
  pUns: pointer to the unstructured grid.
  fGeo: opened geo file.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int ensw_bnd ( uns_s *pUns, FILE *fGeo, 
                        int mVec, int kVec[][MAX_DIM], FILE *flVec[], 
                        FILE *pflVars[MAX_UNKNOWNS] ) {

  int nBc ;
  bndPatch_struct *pBndPatch ;
  ulong_t mVxBc, mFaces[MAX_VX_FACE+1] ;
  int *pNodeIds = NULL, *pNdId ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;


  use_vx_flag ( pUns, "ensw_bnd" ) ;

  /* Loop over all boundaries. */
  for ( nBc = 0 ; pBndPatch = NULL, nBc < pUns->mBc ; nBc++ ) {
    /* Count and flag the vx on this bc. */

    
    unflag_vx ( pUns ) ;
    mVxBc =  uns_flag_vx_bnd ( pUns, nBc, mFaces, NULL ) ;


    mVxBc = number_uns_vert_bc ( pUns, 0, 1, &nBc, mFaces ) ;

    if ( mFaces[2] || mFaces[3] || mFaces[4] ) {

      /* New part header. Boundary indexed nBc [0..mBc-1] is listed as part nBc+1. */
      ensw_part_hdr_geo ( nBc+1, pUns->ppBc[nBc]->text, fGeo ) ;

      /* Coordinates.*/
      ensw_coor ( pUns, mVxBc, pNodeIds, fGeo ) ;

      /* Boundary faces. Note: use of nBc is correct here as we need the bc index, 
         not the part number. */
      ensw_bnd_conn ( pUns, nBc, mFaces, fGeo ) ;

      /* Solution. */
      ensw_sol_part ( pUns, nBc+1, mVxBc, mVec, kVec, flVec, pflVars ) ;

      if ( ensw_node_id )
        arr_free ( pNodeIds ) ;

    }
  }

  free_vx_flag ( pUns ) ;

  return (1) ;
}

/******************************************************************************

  ensw_close_files:
  Close the open concatenated files.
  
  Last update:
  ------------
  17Sep09: conceived.
  
  Input:
  ------
  fGeo: geo file
  flVec: variable file for the velocity vector
  flVars[]: table of scalar variable files.

  Changes To:
  -----------
  fGeo
  flVec
  flVars[]

  Output:
  -------
  none
  
  Returns:
  --------
  none
  
*/

void ensw_close_files ( FILE *fGeo, int mVec, FILE *flVec[], FILE *flVars[] ) {

  int n ;

  fclose ( fGeo ) ;

  for ( n = 0 ; n < mVec ; n++ )
    fclose ( flVec[n] ) ;

  for ( n = 0 ; n < MAX_UNKNOWNS ; n++ )
    if ( flVars[n] )
      fclose ( flVars[n] ) ;

  return ;
}


/******************************************************************************
  h5w_args:   */

/*! pull of arguments from the line with arguments.
 */

/*
  
  Last update:
  ------------
  20Dec16; make all command line args global vars, add doSurface
  5jul16; intro ascii writing for decimated meshes.
  30June12: derived from h5w_args.

  Input:
  ------
  argLine: line with rootFile name and arguments.

  Changes to:
  -----------
  ensw_node_id = global node numbers to be written, if non-zero
  ensw_ascii = write ascii file if non-zero
  ensw_doPromote3D = promote 2D grids to 3D if non-zero
  ensw_doSurface = write the surface grid if non-zero


  Output:
  -------
  rootFile: common stem of the filenames

  */

void ensw_args  ( char argLine[], char rootFile[] ) {

  /* Reset. */
  ensw_node_id = 0 ;
  ensw_ascii = 0 ;
  ensw_doPromote3D = 1 ;
  ensw_doSurface = 1 ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;


  /* Parse line of unix-style optional args. */
  char c ;
  while ((c = getopt_long ( mArgs, ppArgs, "23an:s:",NULL,NULL)) != -1) {
    switch (c)  {
      case '2':
        ensw_doPromote3D = 0 ;
        break ;
      case '3':
        ensw_doPromote3D = 1 ;
        break ;
      case 'a':
        /* Ascii if given (no options), default is binary. */
          ensw_ascii = 1 ;
        break;
      case 'n':
        /* Node id on or off, default is off. */
        if ( strncmp ( optarg, "on", 2 ) || strncmp ( optarg, "1", 1 ) )
          ensw_node_id = 1 ;
        else 
          ensw_node_id = 0 ;
        break;
      case 's':
        /* Node id on or off, default is on. */
        if ( strncmp ( optarg, "off", 2 ) || strncmp ( optarg, "0", 1 ) )
          ensw_doSurface = 0 ;
        else
          ensw_doSurface = 1 ;
        break;
      default:
        /* See h5w_arg in write_hdf for full example. */
        sprintf ( hip_msg, "getopt error `\\x%x'.", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    }
  
  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( rootFile, ppArgs[optind] ) ;
  else
    /* Standard filename. */
    strcpy ( rootFile, "grid\0" ) ;

  return ;
}


/******************************************************************************

  ensw:
  Write an ensight gold file.
  
  Last update:
  ------------
  20Dec16; fix pb with nodeid give: use the new pNr2 field to stash global numbers.
  30Jun14; fix bug with size of kVec, needs to be MAX_VEC+1
  30Jun12; copy a 2d grid to 3d rather than just bitching about it.
  29Aug11; allow multiple vectors.
  17Sep09; don't reflag to avoid double listing of vel. vector, use kFlo instead.
           allocate all files in _header, close in _close.
  26May09: conceived.
  
  Input:
  ------
  rootFile: name of the root file.

  Changes To:
  -----------

  Output:
  -------
  
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int write_ensight ( char *argLine ) {

  uns_s *pUns=NULL ;
  FILE *fGeo, *flVec[MAX_VEC], *flVars[MAX_UNKNOWNS] ;
  // when searching for next vec, kVec is zeroed for the next possible vec. 
  // hence size MAX_VEC+1 in kVec.
  int mVec, kVec[MAX_VEC+1][MAX_DIM] ; 

  char rootFile[LINE_LEN] ;
  ensw_args ( argLine, rootFile ) ;


  if ( verbosity >=1 ) {
    if ( !ensw_ascii )
      sprintf ( hip_msg, " writing grid in binary ensight to %s", rootFile ) ;
    else 
      sprintf ( hip_msg, " writing grid in ascii ensight to %s", rootFile ) ;
    hip_err ( info, 1, hip_msg ) ;
  }
  

  if ( Grids.PcurrentGrid && Grids.PcurrentGrid->uns.type == uns )
    pUns = Grids.PcurrentGrid->uns.pUns ;
  else {
    sprintf ( hip_msg, "there is no unstructured grid to write." ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }


  if ( ensw_doPromote3D && pUns->mDim != 3 ) {
    /* 2D mesh, extrude to 3D. */
    sprintf ( hip_msg, "in write_ensight: copying 2-D grid to 3-D." ) ; 
    hip_err ( info, 1, hip_msg ) ;

    cp_uns2D_uns3D ( 0., 2*pUns->hMin, 1, 'z' ) ;
    pUns = Grids.PcurrentGrid->uns.pUns ;
  }

  if ( pUns->numberedType != leaf )
    number_uns_grid_leafs ( pUns ) ;

  /* In case global node numbers are needed, create the nr2 field. */
  if ( ensw_node_id )
    fill_chunk_vrtxNr2 ( pUns ) ;

  /* Open and initalise all files, including solution files. */
  ensw_headers ( pUns, rootFile, &fGeo, &mVec, kVec, flVec, flVars ) ;

  /* Write geometry and solution for the volume grid. */
  ensw_volume ( pUns, fGeo, mVec, kVec, flVec, flVars ) ;

  /* Write geometry and solution for the boundaries.  */
  ensw_bnd    ( pUns, fGeo, mVec, kVec, flVec, flVars ) ;

  ensw_close_files ( fGeo, mVec, flVec, flVars ) ;

  /* Mark numbering as invalid, to trigger renumbering if needed. */
  pUns->numberedType = invNum ;
  if ( ensw_node_id )
    free_chunk_vrtxNr2 ( pUns ) ;


  return (1) ;
}
