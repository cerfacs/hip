/*
  read_uns_hyd.c:
  read hydra hdf files. 

  Last update:
  ------------
  derived from read_hdf5.

  This file contains:
   -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

#include "hdf5.h"
#include "proto_hdf.h"


extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const Grids_struct Grids ;
extern const char version[] ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;
extern const char varCatNames[][LEN_VAR_C] ;
extern const char h5GrpNames[][LEN_GRPNAME] ;
extern const int  parTypeSize[] ;

/* Global control variables, what is written to file. */
#define DEFAULT_hyr_flag_all 0 /* read all variables. */
static int hyr_flag_all  = DEFAULT_hyr_flag_all  ; 
#define DEFAULT_hyr_flag_zone 1 /* read or not read zones. */
static int hyr_flag_zone  = DEFAULT_hyr_flag_zone  ; 

void hyr_flag_reset () {
  int hyr_flag_all  = DEFAULT_hyr_flag_all ;
  int hyr_flag_zone  = DEFAULT_hyr_flag_zone ;
  return ;
}


/******************************************************************************
  hyr_args:   */

/*! pull of arguments from the line with arguments.
 */

/*
  
  Last update:
  ------------
  18Dec14; intro -s argument for solfile, allow to leave out asciiBound.
  6Apr14; accept hdf5a and hdfa to trigger -a option.
  1Apr12: derived from h5w_args.

  Input:
  ------
  argLine[]: text line with all args, including hdf keyword
  
  Output:
  ------
  gridFile{}: 
  abndFile{}: asciBound 
  solFile{}: 

*/

void hyr_args  ( char argLine[], char *gridFile, char *caseFile, 
                 char *solFile, char *adjFile ) {

  /* Reset. */
  hyr_flag_all  = 0 ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  caseFile[0] = '\0' ;
  solFile[0]  = '\0' ;
  adjFile[0]  = '\0' ;

  /* Parse line of unix-style optional args. */
  char c ;
  while ((c = getopt_long ( mArgs, ppArgs, "a:c:s:",NULL,NULL)) != -1) {
    switch (c)  {
      case 'a':
        if ( optarg )
          /* 0 arg given. use -a0 */
          strcpy ( adjFile, optarg ) ;
        else 
          /* No arg given, or arg not 0, use -a1 */
          hip_err ( warning, 1, 
                    "option a needs a filename argument, ignored.\n" ) ;
        break;


      case 'c':
        if ( optarg )
          /* 0 arg given. use -a0 */
          strcpy ( caseFile, optarg ) ;
        else 
          /* No arg given, or arg not 0, use -a1 */
          hip_err ( warning, 1, 
                    "option c needs a filename argument, ignored.\n" ) ;
        break;

      case 's':
        if ( optarg )
          /* 0 arg given. use -a0 */
          strcpy ( solFile, optarg ) ;
        else 
          /* No arg given, or arg not 0, use -a1 */
          hip_err ( warning, 1, 
                    "option s needs a filename argument, ignored.\n" ) ;
        break;

      case '?':
        if (isprint (optopt)) {
          sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
          hip_err ( warning, 1, hip_msg ) ;
          break ;
        }
        else {
          sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
          hip_err ( warning, 1, hip_msg ) ;
          break ;
        }
      default:
        sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    }
  
  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( gridFile, ppArgs[optind] ) ;
  else
    hip_err ( fatal, 0, "missing grid file name for read hdf\n" ) ;

  if ( optind+1 < mArgs )
    strcpy ( caseFile, ppArgs[optind+1] ) ;


  if ( optind+2 < mArgs )
    strcpy ( solFile, ppArgs[optind+2] ) ;

  return ;
}



/******************************************************************************

  hyr_varCat:
  Deduce a variable category from the h5 group name.
  
  Last update:
  ------------
  5Apr09: conceived.
  
  Input:
  ------
  grpName:

  
  Returns:
  --------
  the matching varCat, 'other' if there is no match.
  
*/

varCat_e hyr_varCat ( const char *grpName ) {

  varCat_e k ;

  for ( k=1 ; k<other ; k++ )
    if ( !strcmp( grpName, h5GrpNames[k] ) )
      return ( k ) ;

  return ( other ) ;
}



/******************************************************************************
  hyr_add_pList:   */

/*! add a global scalar to restart.hdf for later regurgitation
 */

/*
  
  Last update:
  ------------
  2Jul10: conceived.
  
*/
void hyr_add_pList( hid_t grp_id, char *dset_name, hid_t dset_id, hid_t dspc_id, 
                   restart_u *pRestart ) {

  h5pList_s *pL = pRestart->hdf.h5pList + pRestart->hdf.m5pList ;
  hid_t dtype_id, class_id ;


  if ( pRestart->hdf.m5pList >= MAX_H5_PARAMS-1 )
    hip_err( fatal, 0, "out of memory for hdf parameters in hyr_add_pList\n" ) ;

  /* Find the type, int, dbl or string */
  dtype_id = H5Dget_type(dset_id);
  strcpy ( pL->i.label, dset_name ) ;
  class_id = H5Tget_class( dtype_id ) ;
  if ( class_id ==  H5T_INTEGER ) {
    pL->i.type = h5_i ;
    H5Dread( dset_id, HH_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(pL->i.iVal) ) ;
  }
  else if ( class_id ==  H5T_FLOAT ) {
    double someDbl ;
    pL->i.type = h5_d ;
    H5Dread( dset_id, HH_DBL, H5S_ALL, H5S_ALL, H5P_DEFAULT,  &someDbl ) ;
    H5Dread( dset_id, HH_DBL, H5S_ALL, H5S_ALL, H5P_DEFAULT,  &(pL->d.dVal) ) ;
  }
  else if ( class_id ==  H5T_STRING ) {
    pL->i.type = h5_s ;
    H5Dread( dset_id, HH_STR, H5S_ALL, H5S_ALL, H5P_DEFAULT, pL->s.str ) ;
  }
  else {
    /* No datatype we know how to treat. */
    H5Tclose ( dtype_id ) ;
    H5Dclose ( dset_id ) ;
    return ;
  }

  H5Tclose ( dtype_id ) ;
  H5Dclose ( dset_id ) ;
  pRestart->hdf.m5pList++ ;
  return ;
}

/******************************************************************************
  hyr_isVec:   */

/*! Given a variable category and name, return its vector component number.
 */

/*
  
  Last update:
  ------------
  29Aug11: conceived.
  

  Input:
  ------
  catName: variable cateogry name
  varName: variable name
    
  Returns:
  --------
  0 if scalar, 1..3 if vector component x..z
  
*/

int hyr_isVec ( const char thisCatName[], const char thisVarName[] ) {

#define mVCVec (2)
  const char catName[mVCVec][13] = { "GaseousPhase", "LiquidPhase" } ;
  const char varName[mVCVec][MAX_DIM][13] = {
    { "rhou", "rhov", "rhow" },
    { "alphalrholul", "alphalrholvl", "alphalrholwl" } } ;



  int kC ;
  for ( kC = 0 ; kC < mVCVec ; kC++ ) {
    if ( !strcmp( thisCatName, catName[kC] ) )
      break ;
  }
  if ( kC == mVCVec )
    /* Can't have vector components in this category. */
    return (0) ;



  int kV ;
  for ( kV = 0 ; kV < MAX_DIM ; kV++ )
    if ( !strcmp( thisVarName, varName[kC][kV] ) )
      /* This is a matching vector component. */
      return (kV+1) ;


  /* No match. */
  return ( 0 ) ;
}

/******************************************************************************

  hyr_sizes:
  Given a hdf5 file pointer, read the mesh dimensions for allocation.
  
  Last update:
  ------------
  26Sep14; rename iBuf to uBuf, to make type ulong_t clearer.
  5Apr13; promote iBuf to ulong_t
  11Apr08; we are testing the length of the conn vector, not the # of els.
  May07: conceived.
  
  Input:
  ------
  file_id = opened hdf5 file

  Changes To:
  -----------
  mDim    = spatial dimension, 2 or 3.
  mEl     = number of elements
  mConn   = number of connectivity entries (total number of element to node ptr.)
  mVx     = number of vertices
  mBndFc  = number of boundary faces
  mBc     = number of boundary conditions (patches)
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hyr_sizes ( hid_t file_id, 
                int *pmDim, ulong_t *pmEl, ulong_t *pmConn, 
                ulong_t *pmVx, ulong_t *pmBndFc, ulong_t *pmFcConn, 
                int *pmBc ) {

  hid_t grp_id ;
  herr_t      status;

  char grpName[MAX_GROUPS][TEXT_LEN] ;
  ulong_t uBuf[6], i ;
  elType_e elType ;

  // hydra files are flat.
  grp_id = file_id ;

  // Spatial dimension, number of nodes/coors.
  if ( !h5_read_iarr ( grp_id, "dimension", 1, 1, pmDim ) )
    hip_err ( fatal, 0, "could not read dimension statment in hydra hdf." ) ;
  else if ( *pmDim == 2 )
    hip_err ( fatal, 0, "2D hydra files not yet implemented." ) ;
     

  *pmVx   = h5_read_darr ( grp_id, "node_coordinates", 0, 0, NULL ) ;
  *pmVx = (*pmVx)/3 ; // hydra gives a z=0 in 3D.




  /* Connectivity */
  uBuf[0] = h5_read_iarr ( grp_id, "tri-->node", 0, 0, NULL ) ;
  uBuf[1] = h5_read_iarr ( grp_id,"quad-->node", 0, 0, NULL ) ;
  uBuf[2] = h5_read_iarr ( grp_id, "tet-->node", 0, 0, NULL ) ;
  uBuf[3] = h5_read_iarr ( grp_id, "prm-->node", 0, 0, NULL ) ;
  uBuf[4] = h5_read_iarr ( grp_id, "pri-->node", 0, 0, NULL ) ;
  uBuf[5] = h5_read_iarr ( grp_id, "hex-->node", 0, 0, NULL ) ;


  *pmConn = *pmEl = 0 ;
  *pmBndFc = *pmFcConn = 0 ;
  /* Doc says nothing about tri-->node. How are tet grids written? */
  for (  elType = tri ; elType <= qua ; elType++ ) {
    i = (int) elType ;
    *pmBndFc   += uBuf[i]/elemType[elType].mVerts ;
    *pmFcConn += uBuf[i] ;
  }

  for ( elType = tet ; elType <= hex ; elType++ ) {
    i = (int) elType ;
    *pmEl   += uBuf[i]/elemType[elType].mVerts ;
    *pmConn += uBuf[i] ;
  }



  /* Number of bcs. */
  *pmBc   = h5_read_iarr ( grp_id, "group-->zone", 0, 0, NULL ) ;

  return ( 1 ) ;
}

/******************************************************************************

  hyr_coor:
  Read coordinates.
  
  Last update:
  ------------
  26Sep14; promote node index n to ulong_t
  May07: conceived.
  
  Input:
  ------
  file_id = opened hdf5 file
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hyr_coor ( hid_t file_id, uns_s *pUns, chunk_struct *pChunk ) {

  const ulong_t mVx = pChunk->mVerts ;
  const int mDim = pUns->mDim ;

  hid_t  grp_id = file_id ;
  herr_t status;

  int i ;
  ulong_t n ;
  vrtx_struct *pVrtx = pChunk->Pvrtx+1 ;
  double *pCo = pChunk->Pcoor ;

  /* Coordinates: mDim, mVx. */
  h5_read_darr ( grp_id, "node_coordinates", mVx, 3, pCo ) ;

  /* Translate into vertices. */
  for ( n = 0 ; n < mVx ; n++ ) {
    pVrtx[n].number = n+1 ;
    pVrtx[n].Pcoor = pCo ;
    pCo += mDim ;
  }

  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "      Found %"FMT_ULG" coordinates for %d-D grid.", 
              mVx, mDim ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  hyr_conn:
  Read connectivity.
  
  Last update:
  ------------
  1Apr16; derived from h5r_conn.
  
  Input:
  ------
  file_id = opened hdf5 file
  pUns    = grid container
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hyr_conn ( hid_t file_id, uns_s *pUns, chunk_struct *pChunk ) {

  hid_t  grp_id = file_id ; // hydra hdf files are flat.
  herr_t status;

  ulong_t mConnEl[6] ;
  int i ;
  ulong_t mConnMax, mElMax, mEl, *uBuf ;
  ulong_t n ;
  int mVxEl, k ;
  ulong_t mZ ;
  char connName[6][11] = {"tri-->node","quad-->node",
                          "tet-->node","prm-->node","pri-->node","hex-->node"} ;
  char znName[LINE_LEN] ;
  vrtx_struct *pVrtx = pChunk->Pvrtx, **ppVx = pChunk->PPvrtx ;
  elType_e elType ;



  /* Find the max buffer size. 3D only: start with tets.*/
  for ( mConnMax = mElMax = 0, elType = tet ; elType <= hex ; elType++ ) {
    i = (int) elType ;
    mConnEl[i] = h5_read_uarr ( grp_id, connName[i], 0, 0, NULL ) ;
    mConnMax = MAX( mConnEl[i], mConnMax ) ;

    mEl = mConnEl[i]/elemType[elType].mVerts ;
    mElMax = MAX( mElMax, mEl ) ;
  }

  /* Malloc a buffer. */
  uBuf = arr_malloc ( "uBuf in hyr_coor", pUns->pFam, mConnMax, sizeof(*uBuf));


  elem_struct *pEl = pChunk->Pelem+1 ;
  ulong_t *uB ;
  for ( elType = tet ; elType <= hex ; elType++ ) {
    i = (int) elType ;

    if ( mConnEl[i] ) {
      mVxEl = elemType[elType].mVerts ;
      mEl = mConnEl[i]/mVxEl ;

      h5_read_uarr ( grp_id, connName[i], mEl, elemType[elType].mVerts, uBuf ) ;

      for ( uB = uBuf, n = 0 ; n < mEl ; n++, pEl++ ) {
        /* Fill the elements. */
        init_elem ( pEl, elType, n+1, ppVx ) ;
        for ( k = 0 ; k < mVxEl ; k++ )
          *ppVx++ = pVrtx + *uB++ ;
      }

      if ( verbosity > 3 )
        printf ( "       Found %"FMT_ULG" %s.\n", mEl, elemType[elType].name ) ;

    }
  }

  arr_free ( uBuf ) ;
  return ( 1 ) ;
}
/******************************************************************************

  hyr_bnd:
  Read boundary info.
  
  Last update:
  ------------
  6Apr12; include test to compare mBc (e.g. from asciiBound) to Lidx.
  11Apr08; have ppBc, pBndPatch and pBndFc start at their start indices. (see cpre_uns.h)
  May 07: conceived.
  
  Input:
  ------
  file_id = opened hdf5 file
  pUns    = grid container
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hyr_bnd ( hid_t file_id, uns_s *pUns, chunk_struct *pChunk, 
              int mFcConn, char *caseFile ) {


  const int mBc = pChunk->mBndPatches ; 

  /* Alloc a list of bcs. */
  bc_struct **ppBc = pUns->ppBc= arr_malloc ( "ppBc in hyr_bnd", pUns->pFam, 
                                               mBc, sizeof( *ppBc ) ) ;

  /* Read boundary names from case file .dat, or just number. */
  char bcText[MAX_BC_CHAR] ;
  char line[LINE_LEN] ;
  FILE* Fcase ;
  if ( caseFile[0] != '\0' ) {
    /* AsciiBound file given, use these patch labels. */
    if ( !( Fcase = fopen ( caseFile, "r" ) ) ) {
      sprintf ( hip_msg, "could not open .hyd case file in readhyd.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    /* Find the bcs header. */
    while ( !feof(Fcase ) ) {
      fscanf ( Fcase, "%[^\n]%*[\n]", line ) ;
      if ( !strncmp ( line, "*** bcs", sizeof( "*** bcs" ) ) ) {
        fscanf ( Fcase, "%[^\n]%*[\n]", line ) ; // post-header ***
        break ;
      }
    }

    while ( line[0] != '\0' && !feof(Fcase) ) {
      fscanf ( Fcase, "%[^\n]%*[\n]", line ) ;
      if ( !strncmp ( line, "bc(", 3 ) ) {
        sscanf ( line+4, "%[^\"]s", bcText ) ;  
        if ( !( *ppBc = find_bc ( bcText, 1 ) ) ) {
          hip_err ( fatal, 0, "could not add for boundary in read_hyd." ) ;
        }
        ppBc++ ;
      }
      else 
        line[0] = '\0' ;
    }

    if ( ppBc - pUns->ppBc != mBc ) {
      sprintf ( hip_msg, "expected %d, found %d boundaries, ignoring case file.",
                mBc, (int)(ppBc - pUns->ppBc) ) ;
      hip_err ( warning, 1, hip_msg ) ;
      caseFile[0] = '\0' ;
    }
    ppBc = pUns->ppBc ;

    fclose ( Fcase ) ;
  }

  int iBc ;
  if ( caseFile[0] == '\0' ) {
    /* no case file. or no bcs in case file, just number. */
    for ( iBc = 0 ; iBc < mBc ; iBc++ ) {
      sprintf ( bcText, "bnd_no_%d", iBc ) ;
      if ( !( ppBc[iBc] = find_bc ( bcText, 1 ) ) ) {
        hip_err ( fatal, 0, "could not add for boundary in read_hyd." ) ;
      }
    }
  }




  const int mBndFc = pChunk->mBndFaces ;
  hid_t  grp_id = file_id ; // hydra hdf files are flat.
  herr_t status;
  bndFc_struct *pBf = pChunk->PbndFc+1 ; /* note that PbndFc starts with 1. */
  bndPatch_struct *pBP ;
  elem_struct *pElem = pChunk->Pelem ;
  bc_struct *pBc ;

  sprintf ( hip_msg, "      Reading %d bnd faces.\n", mBndFc ) ; 
  hip_err ( blank, 3, hip_msg ) ;


  /* Malloc a buffer. */
  int *pnFcVx = arr_malloc ( "pnFcVx in hyr_bnd", pUns->pFam, 
                                 mFcConn, sizeof( *pnFcVx ) ) ;
  int *pnFcGrp = arr_malloc ( "pnFcGrp in hyr_bnd", pUns->pFam, 
                                  pChunk->mBndFaces, sizeof( *pnFcGrp ) ) ;
  pUns->pBndFcVx = 
    arr_malloc ( "pUns->PbndFcVx in hyr_bnd", 
                 pUns->pFam, pChunk->mBndFaces, sizeof( *pUns->pBndFcVx ));

  size_t mFcConnT, mFcT, mFc = 0 ; 
  char fcType[3][4] = {"tri", "quad" }, fcName[TEXT_LEN] ;
  elType_e kFcT ; 
  int kVx, mVxFc ;
  int *pnFV ; 
  bndFcVx_s *pBv = pUns->pBndFcVx ;
  vrtx_struct *pVxBase = pChunk->Pvrtx ;
  for ( kFcT = tri ; kFcT <= qua ; kFcT++ ) {
    mVxFc = ( kFcT == tri ? 3 : 4 ) ; // fix this for now. 
    
    sprintf ( fcName, "%s-->node", fcType[kFcT] ) ;
    mFcConnT = h5_read_iarr ( grp_id, fcName,  0, 0, NULL ) ;
    mFcT = mFcConnT/mVxFc ;
    h5_read_iarr ( grp_id, fcName,  mFcT, mVxFc, pnFcVx ) ;

    sprintf ( fcName, "%s-->group", fcType[kFcT] ) ;
    mFcT = h5_read_iarr ( grp_id, fcName,  0, 0, NULL ) ;
    h5_read_iarr ( grp_id, fcName,  mFcT, 1, pnFcGrp ) ;

    for ( pnFV = pnFcVx, mFc = 0 ; mFc < mFcT ; mFc++ ) {
      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
        pBv->ppVx[kVx] = pVxBase + *pnFV++ ;
      }
      pBv->mVx = mVxFc ;
      pBv->pBc = ppBc[pnFcGrp[mFc]-1] ;
      pBv++ ;
    }
  }
  if ( mFc != pChunk->mBndFaces ) {
    sprintf ( hip_msg, "expected %zu, found %"FMT_ULG" boundary faces in hyr_bnd.",
              mFc, pChunk->mBndFaces ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Find boundary face to element pointers. */
  pUns->mBndFcVx = mFc ;
  match_bndFcVx ( pUns ) ;
  arr_free ( pUns->pBndFcVx ) ; pUns->pBndFcVx = NULL ;


  arr_free ( pnFcVx ) ;
  arr_free ( pnFcGrp ) ;
  return ( 1 ) ;
}

/***************************************************************************

  PUBLIC

**************************************************************************/


/******************************************************************************

  readhyd_sol:
  .
  
  Last update:
  ------------
  7Sep15; add ielee to the restart.hdf list.
  1Apr12; use hyr_flag_all to trigger hdfa behaviour, drop keyword.
  29Aug11; call h5_isVec to set pVar->isVec.
  20Dec10; fix bug with cut&paste error for FiciveSpecies
  3Jul10; read and carry scalar and node-based vectors in Parameters.
  20Sep09; init mEq to zero.
  7Apr09; allow hdfa without GaseousPhase.
  4Apr09; GS: read rhol only if it exists. 
          JDM: pass keyword, to allow reading the entire sol.
          JDM: replace varTypeS with varList. 
          set pVar->grp from the hdf group name.
  15Apr08; check for file existence using fopen.
  11Dec07; fix bugs in reading fictiveSpecies (merci GS)
  15Sep06: derived from writehyd_sol
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int readhyd_sol ( uns_s *pUns, char *gridFile, char *solFile, char *adjFile ) {

  hid_t grd_id, sol_id, adj_id, dspc_id, dset_id ;   /* identifier */
  herr_t status ;

 
  const int mDim = pUns->mDim ;
  const hbool_t follow_link = {0} ;


  int nEq ;
  hsize_t iDim ;
  double valMin, valMax, *pDbl, *pUn, *dBuf ;


  char dset_name[TEXT_LEN] ;



  /* Try to open the file with fopen to avoid the ugly and non-descript
     err msg of hdf5 in case of a non-existent file. */
  FILE* Ftest ;
  Ftest = fopen ( solFile, "r" ) ;
  if ( !Ftest ) {
    sprintf ( hip_msg, "could not open solution file in readhyd_sol.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else   
    fclose ( Ftest ) ;

  if ( adjFile[0] != '\0' ) {
    Ftest = fopen ( adjFile, "r" ) ;
    if ( !Ftest ) {
      sprintf ( hip_msg, "could not open adjoint file in readhyd_sol.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else   
      fclose ( Ftest ) ;
  }


  sol_id = H5Fopen( solFile, H5F_ACC_RDONLY, H5P_DEFAULT ) ;
  ulong_t mVals = h5_read_darr ( sol_id, "flow", 0, 0, NULL ) ;
  chunk_struct *pChunk = pUns->pRootChunk ;
  ulong_t mVx = pChunk->mVerts ;
  int  mEq, mEqFlow ;
  mEq = mEqFlow = mVals/mVx ;

  ulong_t mValsA = 0,  mValsN = 0 ;
  if ( adjFile[0] != '\0' ) {
    adj_id = H5Fopen( adjFile, H5F_ACC_RDONLY, H5P_DEFAULT ) ;
    mValsA = h5_read_darr ( adj_id, "adjoint flow", 0, 0, NULL ) ;

    if ( mVals != mValsA )
      hip_err ( fatal, 0, "number of flow and adjoint variables differ." ) ;
    mEq += mEq ;

    /* Normal displacement sensitivities. */
    mValsN = h5_read_darr ( adj_id, "adjoint djdxn", 0, 0, NULL ) ;
    if ( mValsN )
      mEq++ ;
  }


  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "Reading hdf5 solution with %d Eq from %s\n", 
              mEq, solFile ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }






  /* Get a list of all variables to store, with group and varname, 
     store all global scalars to be kept in restart.hdf */
  char gasVarName[5][5] ;
  strcpy ( gasVarName[0], "rho" ) ;
  strcpy ( gasVarName[1], "u" ) ;
  strcpy ( gasVarName[2], "v" ) ;
  if ( mDim == 2 ) {
    strcpy ( gasVarName[3], "p" ) ;
  }
  else {
    strcpy ( gasVarName[3], "w" ) ;
    strcpy ( gasVarName[4], "p" ) ;
  }

  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;
  mEq=0 ;
  /* Flow. */
  int k ;
  for ( k = 0 ; k < mDim+2 ; k++ ) {
    pVar[mEq].cat = ns ;
    pVar[mEq].flag = 1 ;
    pVar[mEq].isVec = hyr_isVec( "GaseousPhase", gasVarName[k] ) ;
    strncpy ( pVar[mEq].name, gasVarName[k], LEN_VARNAME ) ;
    strncpy ( pVar[mEq].grp, "GaseousPhase", LEN_VARNAME ) ;
    mEq++ ;
  }
  /* Turb. */
  for ( k = mDim+2 ; k < mEqFlow ; k++ ) {
    pVar[mEq].cat = rans ;
    pVar[mEq].flag = 1 ;
    pVar[mEq].isVec = 0 ;
    sprintf ( pVar[mEq].name, "turb_%d", k-mDim-1 ) ;
    strncpy ( pVar[mEq].grp, "Turbulence", LEN_VARNAME ) ;
    mEq++ ;
  }

  if ( mValsA ) {
    /* Adjoint. */
    for ( k = 0 ; k < mDim+2 ; k++ ) {
      pVar[mEq].cat = other ;
      pVar[mEq].flag = 1 ;
      pVar[mEq].isVec = hyr_isVec( "GaseousPhase", gasVarName[k] ) ;
      sprintf ( pVar[mEq].name, "adj_%s", gasVarName[k] ) ;
      strncpy ( pVar[mEq].grp, "Adjoint", LEN_VARNAME ) ;
      mEq++ ;
    }
    /* Turb. */
    for ( k = mDim+2 ; k < mEqFlow ; k++ ) {
      pVar[mEq].cat = other ;
      pVar[mEq].flag = 1 ;
      pVar[mEq].isVec = 0 ;
      sprintf ( pVar[mEq].name, "adj_turb_%d", k-mDim-1 ) ;
      strncpy ( pVar[mEq].grp, "Adjoint", LEN_VARNAME ) ;
      mEq++ ;
    }
  }

  /* Surface sensitivity. */
  if ( mValsN ) {
    pVar[mEq].cat = other ;
    pVar[mEq].flag = 1 ;
    pVar[mEq].isVec = 0 ;
    sprintf ( pVar[mEq].name, "adj_djdxn" ) ;
    strncpy ( pVar[mEq].grp, "Adjoint", LEN_VARNAME ) ;
    mEq++ ;
  }

  pVL->mUnknowns = mEq ;
  pVL->mUnknFlow = mDim+2 ;
  pVL->varType = prim ;




  /* Alloc. */
  pChunk->Punknown = 
    arr_malloc ( "Pchunk->Punknown in read_avbp_sol", pUns->pFam,
                 ( mVx + 1 )*mEq, sizeof( *pChunk->Punknown ) ) ;


  /* All vars are written as a full array, so allocate one. */
  dBuf = arr_malloc ( "dBuf in readhyd_sol", pUns->pFam, 
                      mVals, sizeof( *dBuf ) ) ;

  /* Set the vrtx pointer. */ 
  vrtx_struct *pVx ;
  for ( pVx = pChunk->Pvrtx+1, pUn = pChunk->Punknown + mEq ;
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pUn += mEq )
    pVx->Punknown = pUn ;





  /* Read the flow unknowns. */
  h5_read_darr ( sol_id, "flow", mVx, mEqFlow, dBuf ) ;
  for ( pVx = pChunk->Pvrtx+1, pUn = dBuf ;
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pUn += mEq )
    memcpy ( pVx->Punknown, pUn, mEqFlow*sizeof(*pUn) ) ;
  

  if ( mValsA ) {
    /* Read the adjoint unknowns. */
    h5_read_darr ( adj_id, "adjoint flow", mVx, mEqFlow, dBuf ) ;
    for ( pVx = pChunk->Pvrtx+1, pUn = dBuf ;
          pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pUn += mEq )
      memcpy ( pVx->Punknown+mEqFlow, pUn, mEqFlow*sizeof(*pUn) ) ;
  }

  ulong_t *uBuf ;
  size_t mB2N ;
  ulong_t nBVx ;
  if ( mValsN ) {
    /* There are Surface sensitivities. Reset all, including volume nodes to zero.*/
    for ( pVx = pChunk->Pvrtx+1 ;
          pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ )
      pVx->Punknown[mEq-1] = 0.0 ;

    /* bnode to node pointer. */
    uBuf = arr_malloc ( "uBuf in readhyd_sol", pUns->pFam, 
                        mValsA, sizeof( *uBuf ) ) ;
    grd_id = H5Fopen( gridFile, H5F_ACC_RDONLY, H5P_DEFAULT ) ;

    mB2N = h5_read_uarr ( grd_id, "bnd_node-->node", 0, 0, NULL ) ;
    if ( mB2N != mValsN ) {
      sprintf ( hip_msg, "expected %d, found %d boundary node to node ptrs.",
                (int)mValsN, (int)mB2N ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    h5_read_uarr ( grd_id, "bnd_node-->node", mB2N, 1, uBuf ) ;
    h5_read_darr ( adj_id, "adjoint djdxn", mB2N, 1, dBuf ) ;
    int m=0 ;
    for ( nBVx = 0 , pUn = dBuf ; nBVx < mB2N ; nBVx++, pUn++ ) {
      if ( uBuf[nBVx] > mVx )
        hip_err ( fatal, 0, "invalid bnd_node-->node." ) ;
      m++ ;
      pVx = pChunk->Pvrtx + uBuf[nBVx] ;
      pVx->Punknown[mEq-1] = *pUn ;
    }
    
    arr_free ( uBuf ) ;                      
    status = H5Fclose(grd_id); 
  }



  arr_free ( dBuf ) ;

  /* Terminate access to the file. */
  status = H5Fclose(sol_id); 
  if ( mValsA )
    status = H5Fclose(adj_id); 



  /* Set default variable names. */
  if ( !hyr_flag_all )
    check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 


  return ( 1 ) ;

}




/******************************************************************************

  readhyd:
  Read a mesh and possibliy solution file in hdf5.
  
  Last update:
  ------------
  1Jul16; new interface to check_uns.
  17Dec15; detect v7 asciiBound files by looking for tag on first line,
           if detected, read bnd info from hdf instead.
  18Dec14; delegate treatment of zero filenames to prepend_path.
  12Dec13; read solParam from solution file.
  6Jul13; new interface to make_uns_grid.
  1Apr12; allow command line UNIX-style arguments.
  23May08; test for file existence.
  11Apr08; add renumber etc at the end.
  May07: conceived.
  
  Input:
  ------
  argline

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_hyd ( char* argLine ) {

  hip_err ( warning, 0, "tested only for hex-only grids so far." ) ;

  FILE* Fcase, *Ftest ;

  hid_t       file_id ;   /* identifier */
  herr_t      status ;

  chunk_struct *pChunk ;
  int mDim ;
  ulong_t mEl, mConn, mVx, mBndFc, mFcConn ;
  int mBc ;

  
  char gridFile[LINE_LEN] ;
  char caseFile[LINE_LEN] ; 
  char solFile[LINE_LEN] ;
  char adjFile[LINE_LEN] ;
  hyr_flag_reset () ;
  hyr_args ( argLine, gridFile, caseFile, solFile, adjFile ) ;
  prepend_path ( gridFile ) ;
  prepend_path ( caseFile ) ;
  prepend_path ( solFile ) ;
  prepend_path ( adjFile ) ;


  /* Test whether the file is openable. Do this outside of hdf, as
     hdf throws up ugly pages of error msgs. */
  if ( !( Ftest = r1_fopen ( gridFile, TEXT_LEN, "r" ) ) ) {
    sprintf ( hip_msg, "could not open grid file in readhyd.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else
    fclose ( Ftest ) ;

  if ( solFile[0] != '\0' ) {
    if ( !( Ftest = r1_fopen ( solFile, TEXT_LEN, "r" ) ) ) {
      sprintf ( hip_msg, "could not open solution file in readhyd.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else
      fclose ( Ftest ) ;
  }


  if ( adjFile[0] != '\0' ) {
    if ( !( Ftest = r1_fopen ( adjFile, TEXT_LEN, "r" ) ) ) {
      sprintf ( hip_msg, "could not open adjoint solution file in readhyd.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else
      fclose ( Ftest ) ;
  }



  if ( caseFile[0] != '\0' ) {
    if ( !( Ftest = r1_fopen ( caseFile, TEXT_LEN, "r" ) ) ) {
      sprintf ( hip_msg, "could not open case file in readhyd.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else
      fclose ( Ftest ) ;
  }



  /* Open the hdf file. */
  file_id = H5Fopen( gridFile, H5F_ACC_RDONLY, H5P_DEFAULT ) ;



  /* Get sizes. */
  if ( !hyr_sizes ( file_id, &mDim, &mEl, &mConn, &mVx, &mBndFc, &mFcConn, &mBc ) ) {
    sprintf ( hip_msg, "could not read grid dimensions in readhyd.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  } 

  /* Alloc. */
  uns_s *pUns = NULL ;
  if ( !make_uns_grid ( &pUns, mDim, mEl, mConn, 0, mVx, 0, mBndFc, mBc ) ) {
    sprintf ( hip_msg, "failed to alloc for grid in readhyd.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Read. */
  pChunk = pUns->pRootChunk ;
  hyr_coor ( file_id, pUns, pChunk ) ;
  hyr_conn ( file_id, pUns, pChunk ) ;
  hyr_bnd  ( file_id, pUns, pChunk, mFcConn, caseFile ) ;

  /* Terminate access to the file. */
  status = H5Fclose(file_id); 



  make_uns_bndPatch ( pUns ) ;

  /* number_uns_grid ( pUns ) ; Done in check_uns. */

  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;




  /* Is there a solution to read? */
  if ( solFile[0] != '\0' ) {
    readhyd_sol ( pUns, gridFile, solFile, adjFile ) ;
  }
  

  return ( 1 ) ;
}

