/*
   cpre_adapt.h:
   Unstructured adaptation. Needs defines from cpre_uns.h.

   Last update:
   ------------
   29Jun14; typedef vxPair_s
   1Jul12; intro vxPair.
   22Jun99; intro Versions.
   21Sep98; pad elMark to 4 byte word boundaries.
   15Jul98; intro mChElem2Vert ;
   15Jan98; fixed diagonals lead to at most 8 facets per face.
   6Dec96; remove adEdges, develop the tree-structures.
   26Nov96: cut from cpre_uns.h

*/
#define REFTYPE_VERSION "1.0"

/* Maximum number of vertices per face. For the time being,
   only simplices are considered. Defined in cpre_uns. 
#define MAX_CHILDS_ELEM 10
#define MAX_CHILDS_FACE 8
*/


/* From children to faces. */
typedef struct {
  int kParentFc ; /* The face of the parent. [1...6] */
  int kChild ;    /* The number of the child having a face on the parent face. */
  int kChildFc ;  /* The number of the child's face. */
} refFc_struct ;

/* The children. */
typedef struct {
  /* Type of element of the child, as in elemType, ranging from 1=tri to 6=hex. */
  elType_e elType ;
  /* Position of the child's vertices in the extended list of
     the parent's vertices. This extended list is composed of
     first the Parent->type vertices, then the mEdgeVerts, then
     the mFaceVerts. */
  int kParentVert[MAX_VX_ELEM] ;
  /* From children to faces. */
  refFc_struct *PrefFc[MAX_FACES_ELEM+1] ;
} child_struct ;

/* From parent's faces to children. */
typedef struct {
  /* The number of children on this face. */
  int mChildsFc ;
  /* The pointers to the children's patterns. */
  refFc_struct refFc[MAX_CHILDS_FACE] ;

  /* Is this face fully or just directionally refined? */
  int halfRef ;
  unsigned int refdVx ;
} parentFc_struct ;

/* Refinement patterns. */
typedef struct {
  int kEdge ;
  int kVert ;
  int kChild ;    /* One instance of the vertex appearing in kChild as */
  int kChildVx ;  /* vertex kChildVx. Very helpful for derefinement. */
} edgeVert_s ;

/* Vertices added on faces. Note that faces run 1....*/
typedef struct {
  int kFace ;
  int kVert ;
  int kChild ;    /* One instance of the vertex appearing in kChild as */
  int kChildVx ;  /* vertex kChildVx. Very helpful for derefinement. */
} faceVert_s ;

/* One refinement pattern. */
typedef enum { noRf, ref, buf } refType_enum ;
struct _refType_struct {
  /* Type enum. Like all enums, set in cpre.h. */
  elType_e elType ;
  
  /* Vertices added on edges. Note that edges run 0...*/
  int mEdgeVerts ;
  edgeVert_s edgeVert[MAX_EDGES_ELEM] ;

  /* Vertices added on faces. Note that faces run 1....*/
  int mFaceVerts ;
  faceVert_s faceVert[MAX_FACES_ELEM+1] ;

  /* Vertex added at the center of the element. */
  int kElemVert ;

  /* The children. */
  int mChildren ;
  child_struct child[MAX_CHILDS_ELEM] ;

  /* Initialization of the patterns in init_uns goes up to here. The
     rest is calculated. */

  /* The faces. */
  parentFc_struct prntFc[MAX_FACES_ELEM+1] ;

  /* Bit pattern for each refined edge. */
  unsigned int refEdges ;
  /* The same, but as an array and with the vertex number on the
     edge as an entry. */
  int vxOnEdge[MAX_EDGES_ELEM] ;

  /* Vertex number for each vertex added on a face. */
  int vxOnFace[MAX_FACES_ELEM+1] ;

  /* Total number of element to vertex pointers (ppVrtx) in all children. */
  int mChElem2Vert ;
  /* Number of quad faces that have been half refined. */
  int mHalfRefQuadFc ;
  
  refType_enum refOrBuf ;       /* A refinement or buffer type? */

  int nr ;
} ;

/* A packed long int to be shipped out. */
#define ELMARK_VERSION "1.0"

typedef struct {
  elType_e elType:4 ;
  unsigned int hgEdge:12 ;
  unsigned int collEdge:12 ;
  /* Align to a 4byte word. */
  unsigned int dummy0:4 ;
  
  unsigned int hgFace:7 ;
  unsigned int fixDiag:7 ;
  unsigned int diagDir:7 ;
  unsigned int dummy1:11 ;
} elMark_s ;


struct _adEdge_s {
  cpt_s cpVxMid ;               /* The vertex at midedge. */
  unsigned int used: 1 ;        /* Is this edge used by any terminal element? */
  unsigned int mark: 1 ;        /* Is this edge used by any terminal element? */
} ;
  


typedef struct _vxPairList_s vxPairList_s ;
typedef struct _vxPair_s vxPair_s ;

// Max number of shadow vertices for one periodic node.
#define MAX_VX_PAIR 256
