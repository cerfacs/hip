/*
 read_uns_dpl.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from a generic .dpl file for cpre.

 contains:
 read_uns_dpl
 
*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const char varTypeNames[][LEN_VAR_C] ;
extern const int dg_fix_lrgAngles ;

/******************************************************************
 
 read_uns_dpl.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an unstructured .dpl file for cpre.

 Last update:
 ------------
  9Jul19; rename to ADAPT_HIERARCHIC
 21Sep18; new interface to merge_uns, but call commented out.
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
 5Jul11; set varCat.
 14Sep10; new interface to merge_uns.
 4Apr09; replace varTypeS with varList.
 30Jun07; use only compact storage for unknowns as allocated per append_chunk.
 12Jul96: derived from readsolmg.

 Input:
 ------
 FdplIn: the opened dpl file.

 Changes to:
 -----------


 Returns:
 --------
 
*/

int read_uns_dpl( FILE *FdplIn ) {
  
  extern Grids_struct Grids ;
  
  char c5[6], text[MAX_BC_CHAR], bcText[MAX_BC_CHAR] ;
  fpos_t filePosFaces ;
  int nFrmVx[MAX_VX_ELEM+1], nFace, kVrtx, nVrtx1, nVrtx2, nNghElem, mVrtx,
      mVerts, mElems, kFace, mConnEntries, nElem, nVrtx, mBoundaries,
      mBndFaces, nBound, mFaces, mOutFaces, nameBound, found, fcType, partNumber,
      mScan, nEq ;
  const int mDim = 2, mEq = 4 ;
  double *Pcoor, *Punknown ;
  chunk_struct *pChunk ;
  bndPatch_struct *PbndPatch ;
  elem_struct *Pelem ;
  bndFc_struct *PbndFc ;
  vrtx_struct **PPvrtx, *Pvrtx, **PPvxFc[MAX_VX_FACE] ;
  uns_s *pUns ;

  /* This will be the next grid. */
  partNumber = Grids.mGrids + 1 ;
  printf ( "  Reading unstructured dpl as part %d.\n", partNumber ) ;

  /* Make sure it's an unstructured .dpl. */
  rewind ( FdplIn ) ;
  if ( fscanf ( FdplIn, "%[uU]%[nN]%[sS]%[tT]%[rR]%*[^\n]",
                &c5[0], &c5[1], &c5[2], &c5[3], &c5[4] ) != 5 ) {
    printf ( " FATAL: file is not of dpl type.\n" ) ;
    return ( 0 ) ;
  }
  fscanf ( FdplIn, "%*[\n]" ) ;

  
  /* Number of elems. */
  fscanf ( FdplIn, "%d%*[^\n]", &mElems ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;

  /* Memory allocation for the elems. */
  printf ( "   Number of elems:              %-d\n", mElems ) ;

  /* Get a file pointer to reread the connectivity after
     the all connectivity entries have been counted. */
  if ( fgetpos ( FdplIn, &filePosFaces ) ) {
    printf ( " FATAL: failure while getting file" ) ;
    printf ( " pointer to elems.\n" ) ;
    exit ( EXIT_FAILURE ) ;
  }

  /* Grid connectivity. */
  for ( mConnEntries = 0, nElem = 1 ; nElem <= mElems ; nElem++ ) {
    fscanf ( FdplIn, "%d", &mVrtx ) ;
    if ( mVrtx < 3 || mVrtx > 4 ) {
      printf ( " FATAL: elem %d is neither quad nor triangle.\n", nElem ) ;
      return ( 0 ) ;
    }
    else {
      mConnEntries += mVrtx ;
      fscanf ( FdplIn, "%*[^\n]" ) ;
      fscanf ( FdplIn, "%*[\n]" ) ;
    }
  }
  
  /* Number of vertices. */
  fscanf ( FdplIn, "%d", &mVerts ) ;



  /* Allocate a chunk. */
  if ( !( pUns = make_uns ( NULL ) ) ) {
    hip_err ( fatal, 0, "failed to alloc a new unstructured grid"
                     " in read_uns_dpl.\n" ) ; }
  else {
    pUns->mDim = mDim ;
    pUns->varList.mUnknowns = pUns->varList.mUnknFlow = mEq ;
    pUns->varList.varType = cons ;
    for ( nEq = 0 ; nEq <  mEq ; nEq++ )
      pUns->varList.var[nEq].cat = ns ;
  }

  if ( !( pChunk = append_chunk( pUns, pUns->mDim, mElems, mConnEntries, 0,
                                 mVerts, 0, 0 ) ) ) {
    sprintf ( hip_msg, "could not allocate the  connectivity, vertex,"
              " coordinate or boundary space in read_uns_dpl.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
    

  /* Reposition the .dpl file. */
  if ( fsetpos ( FdplIn, &filePosFaces ) ) {
    hip_err ( fatal, 0, "failure while repositioning"
              " file pointer to elems.\n" ) ;
  }

  /* Reread the grid connectivity. */
  for ( Pelem = pChunk->Pelem, PPvrtx = pChunk->PPvrtx, Pvrtx = pChunk->Pvrtx,
        nElem = 1 ; nElem <= mElems ; nElem++ ) {
    fscanf ( FdplIn, "%d", &mVrtx ) ;
    for ( kVrtx = 0 ; kVrtx < mVrtx ; kVrtx++ ) 
      fscanf ( FdplIn, "%d", nFrmVx+kVrtx ) ;
    fscanf ( FdplIn, "%*[^\n]" ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
    
#   ifdef CHECK_BOUNDS
      if ( PPvrtx + mVrtx - 1 > pChunk->PPvrtx + mConnEntries - 1 )
      { printf ( " FATAL: more connectivity pointers found than counted.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
#   endif

    Pelem++ ;
    Pelem->PPvrtx = PPvrtx ;
    if ( mVrtx == 3 )
      Pelem->elType = tri ;
    else if ( mVrtx == 4 )
      Pelem->elType = qua ;
    else
    { printf ( " FATAL: cannot deal with a %d-noded element"
	       " in 2-D in read_uns_dpl.\n", mVrtx ) ;
      return ( 0 ) ;
    }
    
#ifdef ADAPT_HIERARCHIC
    /* Pelem is still unrefined. */
    Pelem->refdEdges = Pelem->markdEdges = 0 ;
    Pelem->PrefType = NULL ;
    Pelem->Pparent = NULL ;
    Pelem->PPchild = NULL ;
    Pelem->root = Pelem->leaf = Pelem->term = 1 ;
#endif
    Pelem->invalid = 0 ;
    Pelem->number = nElem ;
    for ( kVrtx = 0 ; kVrtx < mVrtx ; kVrtx++ ) 
      *PPvrtx++ = Pvrtx + nFrmVx[kVrtx] ;
  }
  pChunk->mElems =  pChunk->mElemsNumbered = mElems ;
  pChunk->mElem2VertP = mConnEntries ; 

  
  /* Number of vertices, already read before. */
  fscanf ( FdplIn, "%*[^\n]" ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;
  printf ( "   Number of vertices:           %-d\n", mVerts ) ;
  pChunk->mVerts = pChunk->mVertsNumbered = mVerts ;

  /* Free stream states. */
  for ( nEq = 0 ; nEq < mEq ; nEq++ ) 
    fscanf ( FdplIn, "%lf",  pUns->varList.freeStreamVar+nEq ) ;
  fscanf ( FdplIn, "%*[^\n]" ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;

  /* Vertex coordinates and forget the solution. Note: Pcoor starts
     with index 1, ie. the first set of nDim coordinates skipped. */	
  for ( Pcoor = pChunk->Pcoor, Punknown = pChunk->Punknown, Pvrtx = pChunk->Pvrtx,
        nVrtx = 1 ; nVrtx <= mVerts ; nVrtx++ ) {
    Pvrtx++ ;
    Pcoor += mDim ;
    Punknown += mEq ;

#   ifdef CHECK_BOUNDS
      if ( Pcoor + mDim - 1 > pChunk->Pcoor + mDim*( mVerts+1 ) - 1 )
      { printf ( " FATAL: too many coordinates in read_uns_dpl.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
      else if ( Pvrtx > pChunk->Pvrtx + ( mVerts+1 ) - 1 )
      { printf ( " FATAL: too many vertices in read_uns_dpl.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
#   endif

    Pvrtx->Pcoor = Pcoor ;
    Pvrtx->Punknown = Punknown ;
    Pvrtx->number = nVrtx ;
    fscanf ( FdplIn, "%lf %lf %lf %lf %lf %lf",
	     Pcoor, Pcoor+1, Punknown, Punknown+1, Punknown+2, Punknown+3 ) ;
    fscanf ( FdplIn, "%*[^\n]" ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
  }

  /* Get a file pointer to reread the boundary faces after
     the memory has been allocated. */
  if ( fgetpos ( FdplIn, &filePosFaces ) ) {
    printf ( " FATAL: failure while getting file" ) ;
    printf ( " pointer to faces.\n" ) ;
    exit ( EXIT_FAILURE ) ;
  }

  /* Number of boundaries */
  fscanf ( FdplIn, "%d%*[^\n]", &mBoundaries ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;
  pChunk->mBndPatches = mBoundaries ;

  /* Scan the .dpl for the number of boundary faces. */
  for ( mBndFaces = 0, nBound = 1 ; nBound <= mBoundaries ; nBound++ ) {
    fscanf ( FdplIn, "%d%*[^\n]", &mFaces ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
    mBndFaces += mFaces ;
    for ( nFace = 1 ; nFace <= mFaces ; nFace++ )
    { fscanf ( FdplIn, "%*[^\n]" ) ;
      fscanf ( FdplIn, "%*[\n]" ) ;
    }
  }
  
  /* Scan the .dpl for the number of outer boundary faces. */
  fscanf ( FdplIn, "%d%*[^\n]", &mOutFaces ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;
  if ( mOutFaces ) {
    /* Make this one more boundary. */
    mBoundaries++ ;
    mBndFaces += mOutFaces ;
  }

  /* Alloc for boundary faces. */
  if ( mBndFaces ) {
    pChunk->PbndFc = arr_malloc ( "pChunk->PbndFc in read_uns_dpl", pUns->pFam,
                                  mBndFaces+1, sizeof( bndFc_struct ) ) ;
    pChunk->PbndPatch = arr_malloc ( "pChunk->PbndPatch in read_uns_dpl", pUns->pFam,
                                     mBoundaries+1, sizeof( bndPatch_struct ) ) ;
    pChunk->mBndFaces = mBndFaces ;

    /* Reposition the .dpl file. */
    if ( fsetpos ( FdplIn, &filePosFaces ) ) {
      printf ( " FATAL: failure while repositioning file pointer to faces.\n" ) ;
      exit ( EXIT_FAILURE ) ;
    }

    /* List the boundary patches. */
    PbndFc = pChunk->PbndFc ;
    PbndPatch = pChunk->PbndPatch ;
    /* Write to the allocated space starting with PbndPatch[1]. */
    PbndPatch->Pchunk = NULL ;
    /* PbndPatch->PprvBndPatch = NULL ;
       PbndPatch->PnxtBndPatch = PbndPatch + 1 ;
       */
    PbndPatch->PnxtBcPatch = NULL ;
    PbndPatch->Pbc = NULL ;
    PbndPatch->PbndFc = PbndFc + 1 ;
    PbndPatch->mBndFc = 0 ;
    
    /* Number of boundaries, already read and possibly modified for
       outer boundaries. */
    fscanf ( FdplIn, "%*[^\n]" ) ; fscanf ( FdplIn, "%*[\n]" ) ;
    
    /* Boundary elems for each boundary. */
    for ( PbndFc = pChunk->PbndFc, PbndPatch = pChunk->PbndPatch+1,
	  nBound = 1 ; nBound <= mBoundaries ; nBound++, PbndPatch++ ) {
      nameBound = 0 ;
      if ( !fscanf ( FdplIn, "%d", &mFaces ) || feof( FdplIn ) ) {
	sprintf ( hip_msg, "failed to read mFaces for boundary %d in read_uns_dpl.\n",
		 nBound ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      fgets ( text, MAX_BC_CHAR, FdplIn ) ;
      fscanf ( FdplIn, "%*[\n]" ) ;
      mScan = sscanf ( text, "%d %s", &nameBound, bcText ) ;
      if ( mScan < 1 )
	nameBound = nBound ;

      /* Make a new bc entry. */
      if ( mScan == 2 ) {
	/* There is a given text. */
	strncpy ( bcText, text, MAX_BC_CHAR ) ;
	r1_beginstring ( bcText, MAX_BC_CHAR ) ;
      }
      else {
	if ( nBound == mBoundaries && mOutFaces )
	  /* This is an outer boundary. */
	  sprintf ( bcText, "%d: Grid part nr. %d, outer boundary",
		    nBound, partNumber ) ;
      else
	/* This is a normal boundary. */
	sprintf ( bcText, "%d: Grid part nr. %d, segment %d",
		  nameBound, partNumber, nameBound ) ;
      }
      PbndPatch->Pbc = find_bc ( bcText, 1 ) ;
      

#     ifdef CHECK_BOUNDS
        if ( PbndPatch > pChunk->PbndPatch + ( mBoundaries+1 ) - 1 )
	  hip_err ( fatal, 0, "too many boundaries in read_uns_dpl.\n" ) ;
#     endif

      /* Update the patch. */
      PbndPatch->mBndFc = mFaces ;
      /* PbndFc points to the element just written. The first face
	 of PbndPatch is the next one. */
      PbndPatch->PbndFc = PbndFc+1 ;
      PbndPatch->Pchunk = pChunk ;

      printf ( "   Reading boundary %d with %d faces, named %s\n",
	       nameBound, mFaces, bcText ) ;
    
      for ( nFace = 1 ; nFace <= mFaces ; nFace++ )
      { /* Note that delaundo, the only unstructured dpl provider at the
	   moment, lists the neighboring elems. */
	nNghElem = 0 ;
	fscanf ( FdplIn, "%d %d %d%*[^\n]", &nVrtx1, &nVrtx2, &nNghElem ) ;
	fscanf ( FdplIn, "%*[\n]" ) ;
	
	if ( nNghElem == 0 )
	{ printf ( " SORRY: To recreate the boundary connectivity, hip reads an\n"
		   "        extra integer for each face that indicates the number of\n"
		   "        the interior cell rather than calculating that off the\n"
		   "        full grid. Use hip or delaundo to make your grids.\n" ) ;
	  return ( 0 ) ;
	}
	/* Find the edge back in the neighbor. */
	Pelem = pChunk->Pelem + nNghElem ;
	for ( found = 0, kFace = 1 ;
	      get_uns_face ( Pelem, kFace, PPvxFc, &fcType ) && !found ;
	      kFace++ )
	  /* Disregard the orientation of the edge. */
	  if ( ( (*PPvxFc[0])->number == nVrtx1 && (*PPvxFc[1])->number == nVrtx2 ) ||
	       ( (*PPvxFc[0])->number == nVrtx2 && (*PPvxFc[1])->number == nVrtx1 ) )
	  { found = 1 ;
	    PbndFc++ ;
	    PbndFc->Pelem = Pelem ;
	    PbndFc->nFace = kFace ;
	    PbndFc->Pbc = PbndPatch->Pbc ;
	    break ;
	  }
      
	if ( !found )
	{ printf ( " FATAL: couldn't find back boundary edge %d from %d to %d\n",
		   nFace, nVrtx1, nVrtx2 ) ;
	  printf ( "        in element %d.\n", nNghElem ) ;
	  return ( 0 ) ;
	}
      }
    }
  }
  else {
    pChunk->mBndFaces = 0 ;
    hip_err ( warning, 1, "found no boundary faces in read_uns_dpl.\n" ) ;
    return ( 1 ) ;
  }
  
  /* Unstructured. Make a new grid. */
  grid_struct *pGrid ;
  if ( !( pGrid = make_grid () ) ) {
    hip_err ( fatal, 0, "malloc for the linked list of grids"
              " failed in read_uns_dpl.\n" ) ; }

  /* Put the chunk into Grids. */
  pGrid->uns.type = uns ;
  pGrid->uns.pUns = pUns ;
  pGrid->uns.mDim = 2 ;
  pGrid->uns.pVarList = &(pUns->varList) ;
  pUns->nr = pGrid->uns.nr ;
  pUns->pGrid = pGrid ;
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  

  /* This is only here to run the ALVAST mesh.
  if ( dg_fix_lrgAngles && !merge_uns ( pUns, 0, 1 ) ) {
    printf ( " SORRY: merging of unstructured grids in read_uns_dpl failed.\n" ) ;
    return ( 0 ) ; } */

  /* Validate, count and number the grid. */
  check_uns ( pUns, check_lvl ) ;


  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 

  return ( 1 ) ;
}


/******************************************************************
 
 read_uns_dpl3d.c:
 Read mesh connectivity, vertex coordinates, boundary information
 and solution from an unstructured .dpl3d file for cpre.

 Last update:
 ------------
 3May19; fix bug with un-intialised pUns->pGrid.
  1Jul16; new interface to check_uns.
 6Jul13; new interface to make_uns_grid.
 9Jul11; call init_elem
 4Apr09; replace varTypeS with varList.
 7Feb06; call a final renumber at the end.
 21Dec2; fix alloc bug with unknowns from vrtx_struct to double.
 19Dec00; add solution.
 15Jul99; drop the face to element pointer, use bndFcVx instead.
 15Apr97;derived from read_uns_dpl.

 Format:
 -------
 uns3d
 mVerts
   x,y
   ...
 mElems, mElem2VertPointers
   mVertsElem, n1, n2, ...
   ....
 mBndPatches, mTotalBndFaces
   mBndFaces, [ bndNr, bndName ]
     nElem, kFace
     .....

 Input:
 ------
 FdplIn: the opened file.

 Changes to:
 -----------


 Returns:
 --------
 
*/

int read_uns_dpl3d ( FILE *FdplIn, FILE *FsolIn ) {
  
  extern Grids_struct Grids ;
  const int mDim = 3 ;
  
  char c5[6], text[MAX_BC_CHAR], bcText[MAX_BC_CHAR] ;
  int nFrmVx, nFace, kVrtx, mVrtx, mVSol, mUn = 0, n,
      mVerts, mElems, kFace, mConnEntries, nElem, nVrtx, mBndPatches,
      mBndFaces, nBound, mFaces, nameBound, partNumber, mScan ;
  double *Pcoor, *pUn = NULL ;
  chunk_struct *pChunk ;
  bndPatch_struct *PbndPatch=NULL ;
  elem_struct *Pelem ;
  bndFc_struct *PbndFc=NULL ;
  vrtx_struct **PPvrtx, *Pvrtx ;
  grid_struct *Pgrid ;
  uns_s *pUns ;

  /* This will be the next grid. */
  partNumber = Grids.mGrids + 1 ;
  printf ( "  Reading unstructured dpl as part %d.\n", partNumber ) ;

  /* Make sure it's an unstructured .dpl. */
  rewind ( FdplIn ) ;
  if ( fscanf ( FdplIn, "%[uU]%[nN]%[sS]%[3]%[dD]%*[^\n]\n",
                &c5[0], &c5[1], &c5[2], &c5[3], &c5[4] ) != 5 ) {
    hip_err ( fatal, 0, "file is not of dpl3d type.\n" ) ;
  }
  fscanf ( FdplIn, "%*[\n]" ) ;

  
  
  /* Allocate a chunk. */
  if ( !( pUns = make_uns ( NULL ) ) || !( pChunk = make_chunk ( pUns ) ) ) {
    hip_err ( fatal, 0, "failed to alloc a new unstructured grid"
              " in read_uns_dpl_3d.\n" ) ;}
  else {
    pUns->mChunks = 1 ;
    pUns->pRootChunk = pChunk ;
    pUns->mDim = mDim ;
    
    pChunk->nr = 1 ;
    pChunk->PprvChunk = pChunk->PnxtChunk = NULL ;
    pChunk->Punknown = NULL ;
  }

  /* Number of vertices. No unknowns, so far. */
  fscanf ( FdplIn, "%d", &mVerts ) ;
  printf ( "   Number of verts:              %-d\n", mVerts ) ;
  Pvrtx = pChunk->Pvrtx = arr_malloc ( "pChunk->Pvrtx in read_uns_dpl3d", pUns->pFam,
                                       mVerts+1, sizeof( vrtx_struct ) ) ;
  Pcoor = pChunk->Pcoor = arr_malloc ( "pChunk->Pcoor in read_uns_dpl3d", pUns->pFam,
                                       mVerts+1, mDim*sizeof( double ) ) ;
  pChunk->mVerts = pChunk->mVertsNumbered = mVerts ;




  /* Unknowns? And if yes, how many? */
  if ( FsolIn ) {
    char solType[LINE_LEN], *st ;
    
    rewind ( FsolIn ) ;
    if ( fscanf ( FsolIn, "%d %d", &mVSol, &mUn ) != 2 ) {
      printf ( " WARNING: incomprehensible solution format. Reading grid only.\n" ) ;
      mUn = 0 ;
      FsolIn = NULL ;
    }
    pUns->varList.mUnknowns = pUns->varList.mUnknFlow = mUn ;

    for ( st = solType ; ( *st = getc( FsolIn ) ) ; st++ )
      if ( st[0] == '\n' )
        break ;
      else if ( isspace( st[0] ) )
        st-- ;
    st[0] = '\0' ;

    if ( !strncmp( solType, "prim", 4 ) )
      pUns->varList.varType = prim ;
    else if ( !strncmp( solType, "cons", 4 ) )
      pUns->varList.varType = cons ;
    else {
      printf ( "   WARNING: no variable type found, using conservative.\n" ) ;
      pUns->varList.varType = cons ;
    }

    if ( verbosity > 2 )
      printf ( "   INFO: found %d %s variables.\n",
               mUn, varTypeNames[  pUns->varList.varType ] ) ;
  }


  
  
  
  if ( mUn && mVerts != mVSol ) {
    sprintf ( hip_msg, 
              "mismatch in unknowns: expected %d, found %d.", mVerts, mVSol ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( mUn ){
    pUns->varList.mUnknowns = pUns->varList.mUnknFlow = mUn ;
    pUn = pChunk->Punknown = arr_malloc ( "pChunk->Pvrtx in read_uns_dpl3d", pUns->pFam,
                                          (mVerts+1)*mUn, sizeof( double ) ) ;
  }

  

  
  for ( nVrtx = 1 ; nVrtx <= mVerts ; nVrtx++ ) {
    Pvrtx++ ;
    Pcoor += mDim ;

#   ifdef CHECK_BOUNDS
      if ( Pcoor + mDim - 1 > pChunk->Pcoor + mDim*( mVerts+1 ) - 1 )
      { printf ( " FATAL: too many coordinates in read_uns_dpl.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
      else if ( Pvrtx > pChunk->Pvrtx + ( mVerts+1 ) - 1 )
      { printf ( " FATAL: too many vertices in read_uns_dpl.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
#   endif

    Pvrtx->Pcoor = Pcoor ;
    Pvrtx->number = nVrtx ;
    fscanf ( FdplIn, "%lf %lf %lf%*[^\n]", Pcoor, Pcoor+1, Pcoor+2 ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;

    if ( mUn ) {
      pUn += mUn ;
      Pvrtx->Punknown = pUn ;
      for ( n = 0 ; n < mUn ; n++ )
        fscanf ( FsolIn, "%lf", pUn+n ) ;
      fscanf ( FsolIn, "%*[^\n]" ) ;
      fscanf ( FsolIn, "\n" ) ;
    }
    else
      Pvrtx->Punknown = NULL ;
      
  }





  
  /* Number of elems. */
  fscanf ( FdplIn, "%d %d%*[^\n]", &mElems, &mConnEntries ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;

  /* Memory allocation for the elems. */
  printf ( "   Number of elems:              %-d\n", mElems ) ;
  Pelem = pChunk->Pelem = arr_malloc ( "pChunk->Pelem in read_uns_dpl3d", pUns->pFam,
    mElems+1, sizeof( elem_struct ) ) ;
  PPvrtx = pChunk->PPvrtx = arr_malloc ( "pChunk->PPvrtx in read_uns_dpl3d", pUns->pFam,
    mConnEntries, sizeof( vrtx_struct * ) ) ;


  
  /* Read the grid connectivity. */
  for ( Pvrtx = pChunk->Pvrtx, nElem = 1 ; nElem <= mElems ; nElem++ ) {
    fscanf ( FdplIn, "%d", &mVrtx ) ;
    
#   ifdef CHECK_BOUNDS
      if ( PPvrtx + mVrtx - 1 > pChunk->PPvrtx + mConnEntries - 1 )
      { printf ( " FATAL: more connectivity pointers found than counted.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
#   endif

    Pelem++ ;
    elType_e elType ;
    if ( mVrtx == 4 )
      elType = tet ;
    else if ( mVrtx == 5 )
      elType = pyr ;
    else if ( mVrtx == 6 )
      elType = pri ;
    else if ( mVrtx == 8 )
      elType = hex ;
    else
    { printf ( " FATAL: cannot deal with a %d-noded element"
	       " in 3-D in read_uns_dpl3d.\n", mVrtx ) ;
      return ( 0 ) ;
    }

    init_elem ( Pelem, elType, nElem, PPvrtx ) ; 

    for ( kVrtx = 0 ; kVrtx < mVrtx ; kVrtx++ ) 
    { fscanf ( FdplIn, "%d", &nFrmVx ) ;
      *PPvrtx++ = Pvrtx + nFrmVx ;
    }
    fscanf ( FdplIn, "%*[^\n]" ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
  }
  pChunk->mElems = pChunk->mElemsNumbered = mElems ;
  pChunk->mElem2VertP = mConnEntries ; 


  
  /* Number of boundaries */
  fscanf ( FdplIn, "%d %d%*[^\n]", &mBndPatches, &mBndFaces ) ;
  fscanf ( FdplIn, "%*[\n]" ) ;
  printf ( "   Number of patches/faces:      %-d %-d\n", mBndPatches, mBndFaces ) ;


  
  /* Alloc for boundary faces. */
  pChunk->mBndPatches = mBndPatches ;
  pChunk->mBndFaces = mBndFaces ;
  if ( mBndFaces )
  { PbndFc = pChunk->PbndFc = arr_malloc ( "pChunk->PbndFc in read_uns_dpl", pUns->pFam,
                                           mBndFaces+1, sizeof( bndFc_struct ) ) ;
    PbndPatch = pChunk->PbndPatch
              = arr_malloc ( "pChunk->PbndPatch in read_uns_dpl", pUns->pFam,
                             pChunk->mBndPatches+1, sizeof( bndPatch_struct ) ) ;

    /* Write to the allocated space starting with PbndPatch[1]. */
    PbndPatch->Pchunk = NULL ;
    PbndPatch->PnxtBcPatch = NULL ;
    PbndPatch->Pbc = NULL ;
    PbndPatch->PbndFc = PbndFc + 1 ;
    PbndPatch->mBndFc = 0 ;
    PbndPatch++ ;
  }


  
  /* Boundary elems for each boundary. */
  for ( nBound = 1 ; nBound <= mBndPatches ; nBound++, PbndPatch++ ) {
    nameBound = 0 ;
    fscanf ( FdplIn, "%d", &mFaces ) ;
    fgets ( text, MAX_BC_CHAR, FdplIn ) ;
    fscanf ( FdplIn, "%*[\n]" ) ;
    mScan = sscanf ( text, "%d %s", &nameBound, bcText ) ;
    if ( mScan < 1 )
      nameBound = nBound ;

    /* Make a new bc entry. */
    if ( mScan == 2 ) {
      /* There is a given text. */
      strncpy ( bcText, text, MAX_BC_CHAR ) ;
      r1_beginstring ( bcText, MAX_BC_CHAR ) ; }
    else
      /* Make a text. */
      sprintf ( bcText, "%d: Grid part nr. %d, segment %d",
	        nameBound, partNumber, nameBound ) ;
    PbndPatch->Pbc = find_bc ( bcText, 1 ) ; 

#   ifdef CHECK_BOUNDS
      if ( PbndPatch > pChunk->PbndPatch + mBndPatches ) {
        printf ( " FATAL: too many boundaries in read_uns_dpl3d.\n" ) ;
        exit ( EXIT_FAILURE ) ; }
#   endif

    /* Update the patch. */
    PbndPatch->mBndFc = mFaces ;
    /* PbndFc points to the element just written. The first face
       of PbndPatch is the next one. */
    PbndPatch->PbndFc = PbndFc+1 ;
    PbndPatch->Pchunk = pChunk ;

    printf ( "   Reading boundary %d with %d faces, named %s\n",
	     nameBound, mFaces, bcText ) ;
    
    for ( nFace = 1 ; nFace <= mFaces ; nFace++ ) {
      fscanf ( FdplIn, "%d %d%*[^\n]", &nElem, &kFace ) ;
      fscanf ( FdplIn, "%*[\n]" ) ;

      PbndFc++ ;
#     ifdef CHECK_BOUNDS
        if ( PbndFc > pChunk->PbndFc + pChunk->mBndFaces )
	{ printf ( " FATAL: too many boundary faces in read_uns_dpl3d.\n" ) ;
	  exit ( EXIT_FAILURE ) ;
	}
#     endif

      PbndFc->Pelem = pChunk->Pelem+nElem ;
      PbndFc->nFace = kFace ;
      PbndFc->Pbc = PbndPatch->Pbc ;
    }
  }
  
  /* Unstructured. Make a new grid. */
  if ( !( Pgrid = make_grid () ) ) {
    printf ( " WARNING: malloc for the linked list of grids"
	     " failed in read_uns_dpl3d.\n" ) ;
    free_chunk ( pUns, &pChunk ) ;
    return ( 0 ) ; }




  
  /* Put the chunk into Grids. */
  Pgrid->uns.type = uns ;
  Pgrid->uns.pUns = pUns ;
  Pgrid->uns.pVarList = &(pUns->varList) ;
  Pgrid->uns.mDim = mDim ;
  pUns->pGrid = Pgrid ;
  pUns->nr = Pgrid->uns.nr ;

  /* Make this grid the current one. */
  Grids.PcurrentGrid = Pgrid ;
  
  /* Validate, count and number the grid. */
  number_uns_grid ( pUns ) ;
  check_uns ( pUns, check_lvl ) ;



  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 


  Grids.epsOverlap = .9*pUns->hMin ;
  Grids.epsOverlapSq = Grids.epsOverlap* Grids.epsOverlap ;
      
  return ( 1 ) ;
}
