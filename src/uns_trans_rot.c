/*
  file_name.c:
*/

/*! short descr. for doxygen
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  ; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;


/* *****************************************************************
   Map of reflected intra-cell connectivity: */

  /* Map from old vertex to reflected one. */
  const int reflectVx[MAX_ELEM_TYPES][MAX_VX_ELEM] = { 
  {0,2,1}, /* tri */ 
  {0,3,2,1}, /* qua */
  {0,2,1,3}, /* tet */
  {0,3,2,1,4}, /* pyr */
  {3,2,1,0,4,5}, /* pri */
  {4,5,6,7,0,1,2,3} /* hex */
  } ;

  /* Map from old face to reflected one. */
  const int reflectFc[MAX_ELEM_TYPES][MAX_FACES_ELEM+1] = { 
  {0,1,3,2}, /* tri */ 
  {0,4,3,2,1}, /* qua */
  {0,1,3,2,4}, /* tet */
  {0,1,5,4,3,2}, /* pyr */
  {0,2,1,3,4,5}, /* pri */
  {0,1,2,3,4,6,5} /* hex */
  } ;


/******************************************************************************
  tr_reflect_elems:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  19Dec17; intro useMark.
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  28Aug11: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int tr_reflect_elems ( uns_s *pUns, const int useMark ) {

  chunk_struct *pChunk ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  const elemType_struct *pElT ;
  ulong_t mVx ;
  vrtx_struct **ppVx ;
  int kVx ;
  vrtx_struct *ppVxOld[MAX_VX_ELEM] ;

   /* Test whether vols are preserved. */ 
    /*
      double *elVol = arr_malloc( "elVol", pUns->pFam, pUns->mElemsNumbered, 
      sizeof( double)), d, d2 ; 
    */


  /* Elements. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( (useMark ? pEl->mark  : pEl->number ) ) {

        /*
          elVol[pEl->number] = get_elem_vol ( pEl ) ; 
        */

        pElT = elemType + pEl->elType ;
        mVx = pElT->mVerts ;
        ppVx = pEl->PPvrtx ;
        /* Swap all vertices around for positive volumes. */
        for ( kVx = 0 ; kVx < mVx ; kVx++ )
          ppVxOld[kVx] = ppVx[kVx] ;
        for ( kVx = 0 ; kVx < mVx ; kVx++ )
          ppVx[ reflectVx[pEl->elType][kVx] ] = ppVxOld[kVx] ;
      }


  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBF, *pBFcBeg, *pBFcEnd ;

  /* Boundary faces. */
  pChunk = NULL ;
  pBndPatch = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBFcBeg, &pBFcEnd ) )
    for ( pBF = pBFcBeg ; pBF <= pBFcEnd ; pBF++ )
      if ( pBF->Pelem && (useMark ? pBF->Pelem->mark : pBF->Pelem->number ) ) {
        pBF->nFace = reflectFc[pBF->Pelem->elType][pBF->nFace] ;
      } 
  
  /*
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &nBeg, &pElEnd, &nEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
    if ( pEl->number ) {

    d = elVol[pEl->number] + get_elem_vol ( pEl ) ;
    d = ABS( d ) ;
    d2 += d*d ;
    if ( d > 1.e-12 ) {
    printf ( " Obacht: el vol diff: %g\n", d ) ; }
    }
    arr_free ( elVol ) ;
  */

  return ( 0 ) ;
}


/******************************************************************************
  tr_trans_vec:   */

/*! transform a vector given a transformation operation.
 *
 */

/*
  
  Last update:
  ------------
  17Dec17; fix erroneous description of dval: for rot it is cal, then sal.
  28Aug11: cut out of transform.
  

  Input:
  ------
  kVarVec[]: position of the x,y,[z] components in the vector.
  mDim: number of spatial dimensions
  tr_op: tranformation operation.
  k1, k2: for rotatation k1,k2 identify the x,y like coord for rot around z.
  k1:     for reflection, k1 is the reflected coordinate
  dval:   for translation it is the translation vector 
          for scaling it is the vector of scaling factors in x,y,z
          for rotation it is {cos alpha, sin alpha}
  isCoor: one if a coordinate is transformed (flow vectors are not translated.).
          
  Changes To:
  -----------
  pCo: pointer to the double vector to transform.
  
*/

void tr_trans_vec ( double *pCo, const int kVarVec[MAX_DIM], const int mDim, 
                    transf_e tr_op, int k1, int k2, double dval[MAX_DIM],
                    const int isCoor ) {

  double x1, x2 ;
  int k ;
  double cal, sal ;

  if ( tr_op == trans && isCoor ) {
    /* Translation, modifies only coordinates, not unknowns. */
    for ( k = 0 ; k < mDim ; k++ )
      pCo[ kVarVec[k] ] += dval[k] ;
  }


  else if ( tr_op == scale ) {
    /* Scale. */
    for ( k = 0 ; k < mDim ; k++ )
      pCo[ kVarVec[k] ] *= dval[k] ;
  }


  else if ( tr_op >= rot_x && tr_op <= rot_z ) {
    /* Rotate. Compiler will optimise out the aliasing of sal, cal, left for clarity.*/
    cal = dval[0] ;
    sal = dval[1] ;
    x1 = cal*pCo[ kVarVec[k1] ] + sal*pCo[ kVarVec[k2] ] ;
    x2 = cal*pCo[ kVarVec[k2] ] - sal*pCo[ kVarVec[k1] ] ;

    pCo[ kVarVec[k1] ] = x1 ;
    pCo[ kVarVec[k2] ] = x2 ;

  }


  else if ( tr_op >= ref_x && tr_op <= ref_z ) {
    /* Flip coordinate. */
    pCo[ kVarVec[k1] ] = -pCo[ kVarVec[k1] ] ;

  }
  return ;
}

/******************************************************************************
  tr_setup:   */

/*! set up transformation indices and parameters.
 *
 */

/*
  
  Last update:
  ------------
  26Feb19; switch to rad from deg for rotAngle.
  28Aug11: cut out of transform.
  

  Input:
  ------
  pUns:
  dval: transformation parameters from the user interface.
        for translation: x,y,z displacements.
        scaling: x,y,z scale
        rotation: -rot angle in rad. (JDM 12/17: why minus?)

  Output:
  -------
  trval: transformation parameters packed for tr_trans_vec, see there.
  *pk1, *pk2: transformation indices fortr_trans_vec.
  
*/

void tr_setup  ( uns_s *pUns, transf_e tr_op,
                 const double dval[], 
                 double trval[], int *pk1, int *pk2 ) {

  int k, k1=-1, k2 ;
  const int mDim = pUns->mDim ;

  if ( tr_op == trans ) {
    /* Translate. Adjust ll/urBox. */
    for ( k = 0 ; k < mDim ; k++ ) {
      pUns->llBox[k] += dval[k] ;
      pUns->urBox[k] += dval[k] ;
      trval[k] = dval[k] ;
    }
  }


  else if ( tr_op == scale ) {
    double minSc = 1.e25 ;

    /* Scale. Adjust epsilon */
    for ( k = 0 ; k < mDim ; k++ )
      minSc = MIN( minSc, ABS( dval[k] ) ) ;

    pUns->epsOverlap   = Grids.epsOverlap   *= minSc ;
    pUns->epsOverlapSq = Grids.epsOverlapSq *= minSc*minSc ;

    sprintf ( hip_msg, "adjusting eps by scale factor to %g", Grids.epsOverlap ) ;
    hip_err ( info, 1, hip_msg ) ;

    /* Adjust ll/urBox. */
    for ( k = 0 ; k < mDim ; k++ ) {
      pUns->llBox[k] *= dval[k] ;
      pUns->urBox[k] *= dval[k] ;
      trval[k] = dval[k] ;
    }
  }


  else if ( tr_op >= rot_x && tr_op <= rot_z ) {
    /* Rotation, vector params, etc. also need treatment. */
    /* Rotation. */
    double alrad = -dval[0], cal = cos(alrad), sal = sin(alrad) ;
    trval[0] = cal ;
    trval[1] = sal ;
          
    if ( tr_op == rot_z || mDim == 2 ) {
      /* z. */
      k1 = 0 ;
      k2 = 1 ; }
    else if ( tr_op == rot_x ) {
      /* x. */
      k1 = 1 ;
      k2 = 2 ; }
    else { 
      /* y. */
      k1 = 2 ;
      k2 = 0 ; }

    *pk1 = k1 ;
    *pk2 = k2 ;
  }


  else if ( ( tr_op >= ref_x && tr_op <= ref_z ) ) {
    /* Reflection, vector params, etc. also need treatment. */
    switch ( tr_op ) {
    case ref_x: k1=0 ; break ;
    case ref_y: k1=1 ; break ;
    case ref_z: k1=2 ; break ;
    default : ; // silence the compiler warning.
    }
    *pk1 = k1 ;

  }
  return ;
}

/******************************************************************************
  transform:   */

/*! Apply translation, rotation, scaling or reflection to a marked vx coor and solution.
 */

/*
  Last update:
  ------------
  26Feb19; switch to rad rather than deg for rotAngle.
  6Jul18; recompute bounding box and per bc setup at end.
  19Dec17; intro useMark.
  1Jul14; fix bug with mixed-up enums: using noVar where it should have been noCat.
  17Dec13; also rotate solParam vectors.
  15Jul13; use zone_loop to hide internal data-structure.
  28Aug11; moved to own file, restructured.
  16Sep10; intro transf_e.
  4Apr09; replace varTypeS with varList.
  14May07; adjust epsOverlap also with pUns. 
  14Mar06; add rot of velocities and 2pf quantities.
  06Jan05; complete connectivity reflection.
  24Sep04; reflect element connectivity as well, for reflection cases.
  : 10Apr3; conceived.
  
  Input:
  ------
  pGrid;
  tr_op: noTr: no op
         trans: translate by dval in each direction.
         scale: scale by dval in each direction.
         rot_x/y/z: rotate around x,y,z direction with dval[0] ;
         ref_x/y/z: flip coordinate values (change sign) in x,y,z.
  dval:  transformation factors (see tr_setup).
         translation: vector
         rotation: -angle in rad around axis
         scale: factor
  useMark: base inclusion on vx and elem mark, rather than number.
  doCheck: if true, re-establish boundaries and check per setup.

  Changes To:
  -----------
  pGrid
  
*/

void transform ( grid_struct *pGrid, const transf_e tr_op,
                 const double dval[],
                 const int useMark, const int doCheck ) {

  uns_s *pUns=NULL ;
  if ( pGrid->uns.type == uns )
    pUns = pGrid->uns.pUns ;
  else {
    hip_err ( fatal, 0, 
              "transform can only operate on unstructured grids.\n" ) ;
  }

  const int mDim = pUns->mDim ;


  /* Set up parameters for the transformation operations. */
  double trval[MAX_DIM] ;
  int k1, k2 ;
  tr_setup ( pUns, tr_op, dval, trval, &k1, &k2 ) ; 



  /* Perform the transformation operation on the grid coordinates. */
  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  int kVarVecId[MAX_DIM] = { 0,1,2 } ;
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( ( useMark ? pVx->mark : pVx->number ) ) {
        tr_trans_vec ( pVx->Pcoor, kVarVecId, mDim, tr_op, k1, k2, trval, 1 ) ;
      }



  /* Perform the transformation operation on the unknowns. */
  int kC = -1 ;
  int kVarVec[MAX_DIM] ;
  if ( pUns->varList.mUnknowns && tr_op != trans ) {
    while ( next_vec_var ( &pUns->varList, pUns->mDim, noCat, &kC, kVarVec ) ) {

      pChunk = NULL ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
        for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
          if ( ( useMark ? pVx->mark : pVx->number ) )
            tr_trans_vec ( pVx->Punknown, kVarVec, mDim, tr_op, 
                           k1, k2, trval, 0 ) ;
    }
  }



  /* Rotate zones. */
  zone_s *pZ = NULL ;
  param_s *pPar ;
  if ( pUns->mZones && tr_op != trans ) {
    while ( zone_loop ( pUns, &pZ ) ) {
      /* Mesh parameters ->pParam: */
      for ( pPar = pZ->pParam ; pPar ; pPar = pPar->pNxtPar )
        if ( pPar->parType == parVec ) {
          tr_trans_vec ( pPar->pv, kVarVecId, mDim, tr_op, k1, k2, trval, 0 ) ;
        }
      /* Solution parameters ->pSolParam: */
      for ( pPar = pZ->pSolParam ; pPar ; pPar = pPar->pNxtPar )
        if ( pPar->parType == parVec ) {
          tr_trans_vec ( pPar->pv, kVarVecId, mDim, tr_op, k1, k2, trval, 0 ) ;
        }
    }
  }



  /* Reflect connectivity as well for reflection operations for positive volumes. */
  if ( tr_op >= ref_x && tr_op <= ref_z ) {
    tr_reflect_elems ( pUns, useMark ) ;
  }

  // Update the bounding box.
  get_uns_box ( pUns ) ;

  if ( doCheck ) {
    // Update setup of periodic bcs
    unset_all_perBc ( pUns ) ;
    check_bnd_setup ( pUns ) ;
  }

  return ;
}

