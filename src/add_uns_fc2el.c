/*
   uns_fc2el.c
   Add an unstructured face to element pointer to the list.
 
   Last update:
   ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.

   Contains:
   ---------
   add_uns_elem_fc2el:
   add_uns_bnd_fc2el:
   add_uns_int_fc2el:
   add_uns_match_fc2el:
*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

const extern int verbosity ;

/******************************************************************************

  add_uns_fc2el:
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  **PPfc2el:     The linked list of fc2el faces,
  PmFaces:       Size of that list,
  Pelem/nFace:   Element and face number in that element,
  Pbnd/intFc:    Boundary or cut face,
  PmatchFc:      Matching face and face number in the element.
  nLoVx:         Lowest numbered vertex, the entry point.
  PnLstFc:       Number of the last face used.
  PnLstFcLoVx:   Number of the last face with that lowest numbered vertex.

  Changes To:
  -----------
  PPfc2el,     mFaces: Realloc.
  PnLstFc:     Number of the last face used.
  PnLstFcLoVx:   Number of the last face with that lowest numbered vertex.
  
  Returns:
  --------
  0 on failure, the changed/added face PnewFc on success.
  
*/

#if defined (ELEM_FACE) 
  fc2el_struct *add_uns_elem_fc2el ( fc2el_struct **PPfc2el, int *PmFaces,
				     elem_struct *Pelem, const int nFace,
				     const int nLoVx, int *PnLstFc, 
				     int *PnLstFcLoVx )
#elif defined (BND_FACE)
  fc2el_struct *add_uns_bndFc_fc2el ( fc2el_struct **PPfc2el, int *PmFaces,
				      bndFc_struct *PbndFc,
				      const int nLoVx, int *PnLstFc,
				      int *PnLstFcLoVx )
#elif defined (INT_FACE)
  fc2el_struct *add_uns_intFc_fc2el ( fc2el_struct **PPfc2el, int *PmFaces,
				      intFc_struct *PintFc,
				      const int nLoVx, int *PnLstFc,
				      int *PnLstFcLoVx )
#elif defined (MATCH_FACE)
  fc2el_struct *add_uns_matchFc_fc2el ( fc2el_struct **PPfc2el, int *PmFaces,
				        matchFc_struct *PmatchFc, const int nFace,
					const int nLoVx, int *PnLstFc,
				        int *PnLstFcLoVx )
#endif

{
  fc2el_struct *PnewFc ;
  int nLstFc, kSide ;

  /* Find the space for the new face. */
  if ( !(*PPfc2el)[nLoVx].side[0].elem.Pelem ) {
    /* First face with this lowest vertex. */
    PnewFc = *PPfc2el + nLoVx ;
    *PnLstFcLoVx = nLoVx ; }

  else {
   /* Append at the end. */
    ++(*PnLstFc) ;
  
    /* Check bounds. */
    if ( *PnLstFc > *PmFaces ) {
      /* Bounds exceeded. Realloc. */
      if ( verbosity > 5 )
	printf ( "   INFO: realloc of the face/edge list to %d faces.\n", *PmFaces ) ;
      *PmFaces += *PmFaces/2 ;
      *PPfc2el = arr_realloc ( "PPfc2el in add_uns_fc2el", NULL,
                               *PPfc2el, *PmFaces+1, sizeof( **PPfc2el ) ) ;
      if ( !*PPfc2el ) {
	printf ( " FATAL: could not reallocate a face/edge list in add_uns_fc2el.\n" ) ;
	return ( NULL ) ; }

      /* Reset the newly allocated part of the list. */
      for ( PnewFc = *PPfc2el + *PnLstFc ; PnewFc <= *PPfc2el + *PmFaces ; PnewFc++ )
	for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
	  PnewFc->side[kSide].elem.type = ( faceType_enum ) 99 ;
	  PnewFc->side[kSide].elem.Pelem = NULL ;
	  PnewFc->side[kSide].elem.nFace = 0 ;
	  PnewFc->side[kSide].elem.kFacet = PnewFc->side[kSide].elem.drvFc = 0 ;
	}
    }
      
    PnewFc = *PPfc2el + *PnLstFc ;
    /* Append it to the linked list of nLoVx. */
    (*PPfc2el)[*PnLstFcLoVx].nxtFcLoVx = *PnLstFc ;
    *PnLstFcLoVx = *PnLstFc ;
  }
  
  /* Append. */
  PnewFc->nxtFcLoVx = 0 ;
# if defined (ELEM_FACE) 
    PnewFc->side[0].elem.type = elem ;
    PnewFc->side[0].elem.Pelem = Pelem ;
    PnewFc->side[0].elem.nFace = nFace ;
    PnewFc->side[0].elem.kFacet = PnewFc->side[0].elem.drvFc = 0 ;
# elif defined (BND_FACE)
    PnewFc->side[0].bndFc.type = bndFc ;
    PnewFc->side[0].bndFc.PbndFc = PbndFc ;
# elif defined (INT_FACE)
    PnewFc->side[0].intFc.type = intFc ;
    PnewFc->side[0].intFc.PintFc = PintFc ;
# elif defined (MATCH_FACE)
    PnewFc->side[0].matchFc.type = matchFc ;
    PnewFc->side[0].matchFc.PmatchFc = PmatchFc ;
    PnewFc->side[0].matchFc.side = nFace ;
# endif

  return ( PnewFc ) ;
}

