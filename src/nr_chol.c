/*
   nr_chol.c:
   Cholesky deco and solve from the Num. Rec.

   Last update:
   ------------
   18Dec2; collated.

   This file contains:
   -------------------
   choldc
   cholsl
*/

#include <math.h>
#include "nr.h"


/******************************************************************************

  choldc:
  Cholesky Deco.
  
  Last update:
  ------------
  22Dec02: modified return values from NR util.
  
  Input:
  ------
  a[1..n][1..n]: matrix in NR format.
  n:             size

  Changes To:
  -----------
  p: pivot vector?
  
  Returns:
  --------
  0 on success, the number of the degen column on failure.
  
*/

int choldc(double **a, int n, double p[], double tol ) {
  int i,j,k;
  double sum;

  for (i=1;i<=n;i++) {
    for (j=i;j<=n;j++) {
      for (sum=a[i][j],k=i-1; k>=1; k--)
        sum -= a[i][k]*a[j][k];
      if (i == j) {
        if (sum <= tol )
          /*  nrerror("choldc failed"); */
          return (i) ;
        p[i]=sqrt(sum);
      }
      else
        a[j][i]=sum/p[i];
    }
  }
  return (0) ;
}

void cholsl( double **a, int n, double p[], double b[], double x[] ){
  
  int i,k;
  double sum;

  for ( i=1; i<=n; i++ ) {
    sum=b[i] ;
    for ( k=i-1; k>=1; k-- )
      sum -= a[i][k]*x[k];
    x[i]=sum/p[i];
  }

  for ( i=n; i>=1; i-- ) {
    sum=x[i] ;
    for ( k=i+1; k<=n; k++ )
      sum -= a[k][i]*x[k];
    x[i]=sum/p[i];
  }
}

/******************************************************************************

  qrdcmp:
  QR decomposition.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/


void qrdcmp(double **a, int n, double *c, double *d, int *sing) {
  
  int i,j,k;
  double scale,sigma,sum,tau;

  *sing=0;
  
  for ( k = 1 ; k < n ; k++ ) {
    scale = 0.0 ;
    for ( i = k ; i <= n ; i++ )
      scale = FMAX( scale, fabs( a[i][k] ) ) ;
    
    if ( scale == 0.0) {
      *sing = 1 ;
      c[k] = d[k] = 0.0 ; }
    else {
      for ( i = k ; i <= n ; i++ )
        a[i][k] /=  scale ;
      
      for ( sum = 0., i = k ; i <= n ; i++ )
        sum += SQR( a[i][k] ) ;

      sigma = SGN( sqrt(sum), a[k][k] ) ;
      a[k][k] + =  sigma ;
      c[k] = sigma*a[k][k];
      d[k]  =  -scale*sigma;
      for (j = k+1;j< = n;j++) {
        for (sum = 0.0,i = k;i< = n;i++) sum + =  a[i][k]*a[i][j];
        tau = sum/c[k];
        for (i = k;i< = n;i++) a[i][j] - =  tau*a[i][k];
      }
    }
  }
  d[n] = a[n][n];
  if (d[n]  =  =  0.0) *sing = 1;
}



void qrsolv(double **a, int n, double c[], double d[], double b[]) {
  
  void rsolv(double **a, int n, double d[], double b[]);
  int i,j;
  double sum,tau;

  for (j = 1;j<n;j++) {
    for (sum = 0.0, i = j;i <= n;i++) sum +=  a[i][j]*b[i];
    tau = sum/c[j];
    for (i = j;i< = n;i++) b[i] -= tau*a[i][j];
  }
  rsolv(a,n,d,b);
}
