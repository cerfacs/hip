/*
   write_uns_cgns.c:
   Unstructured grid to a cgns database.

   Last update:
   ------------
   24May18; fix bug with dim of topoSTring.
   18Dec11; instrument cg_ calls with cgh_ier, rename own cg_ routines to cgh_.
   27Aug11; resurrected from 1.17 version.
   28Aug03; derived from write_uns_adf.c

   This file contains:
   -------------------

*/
#include <strings.h>
#include <time.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

#include "cgnslib.h"
#include "proto_cgns.h"


#define CV const vrtx_struct
#define MAX_BASE_LEN (16)


const ElementType_t cgElemType[] = { TRI_3, QUAD_4, 
                                     TETRA_4, PYRA_5, PENTA_6, HEXA_8, 
                                     BAR_2 } ;
const char cgElemName[][5]  = { "Tri", "Quad", "Tet", "Pyr", "Prism", "Hexa", "Edge" } ;

/* numbered with mVertsFace */
const ElementType_t cgFaceType[] = { ElementTypeNull, ElementTypeNull, 
                                     BAR_2, TRI_3, QUAD_4, 
                                     TETRA_4, PYRA_5, PENTA_6, HEXA_8, 
                                     BAR_2 } ;
const char cgFaceName[][5]  = { "", "", "Edge", "Tri", "Quad" } ;



/* Note: this duplicates the definition in read_uns_cgns. */
static const int cg2a[6][8] = 
{ {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,2,1,3},        /* tet */
  {0,1,2,3,4},      /* pyramids */
  {0,3,5,1,2,4},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;
const char cgVarTypeName[][15] = { "UserDefinedVar", "Navier-Stokes", "Species", 
                                   "ReactionRates", "TwoPhaseFlow", "RANS", 
                                   "Additional", "Mean" } ;

extern const char version[], lastUpdate[] ;

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern const Grids_struct Grids ;


extern const char varCatNames[][LEN_VAR_C] ;
extern const char varTypeNames[][LEN_VAR_C] ;
extern const char topoString[][MINITXT_LEN] ;




/******************************************************************************

CGNS macros.

*/

/* Avoids handing error status back and forth between functions. */
int cg_ier ;


#define LEN_CHAR_CGNS 33
#define MAX_USR_NODES 20
/* Stupid CGNS does not allow us to check whether a node exists or not
   when open for write. So we need to track ourselves. */
typedef struct {
  int mData ;
  char nodeName[MAX_USR_NODES][LEN_CHAR_CGNS] ;
} cgNodes_s ;

cgNodes_s cgUsrNodes, cgSolNodes ;

static void cgh_err () {
  if ( verbosity > 1 ) 
    cg_error_print () ;

  hip_err ( fatal, 0, "cgns write error, turn up verbosity if there is no cgns msg.\n"); 
  return ;
}

static void cgh_reset_nodes ( cgNodes_s cgNodes ) {
  cgNodes.mData = 0 ;
  return ;
}


static int cgh_write2 ( const char *arrName, DataType_t DataType, int mDim1, int mDim2, 
                       const void *pData ) {
  cgsize_t size[2] ;
  size[0] = mDim1 ;
  size[1] = mDim2 ;
  return ( cg_array_write ( arrName, DataType, 2, size, pData ) ) ;
}

static int cgh_write1 ( const char *arrName, DataType_t DataType, int mDim, 
                       const void *pData ) {
  cgsize_t size = mDim ;
  return ( cg_array_write ( arrName, DataType, 1, &size, pData ) ) ;
}
  

/* Check whether the user datat node exists.  
   Returns: node index +1 (range 1 ...)  when found ,
   0 otherwise. */
static int cgh_node_exists ( cgNodes_s *pcgNodes, const char *nodeName ) {

  int i ;

  for ( i = 0 ; i < pcgNodes->mData ; i++ ) {
    if ( !strncmp( nodeName, pcgNodes->nodeName[i], LEN_CHAR_CGNS ) ) {
      /* Name already exists. */
      return ( i+1 ) ;
    }
  }

  return ( 0 ) ;
}

/* Add a user node
   Returns: node index +1 (range 1 ...)  when added,
   0 otherwise. */
static int cgh_node_add ( cgNodes_s *pcgNodes, const char *nodeName ) {

  int m = pcgNodes->mData+1 ;

  if ( m >= MAX_USR_NODES ) {
    sprintf ( hip_msg,
              "in cgh_zone_user_data_add: increase MAX_USR_NODES to > %d.\n",
              MAX_USR_NODES ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  strncpy ( pcgNodes->nodeName[m-1], nodeName, LEN_CHAR_CGNS ) ;
  pcgNodes->mData = m ;

  return ( m ) ;
}

/* Wrapper. First check whether the node doesn't exist. If not, create. 
   Returns: cg error code,

   JDM, Feb 17: this used to return the id of the user zone. Is that needed
                anywhere?
            the user node index otherwise. */     
static int cgh_zone_user_data_write ( const int flNr, const int idBase, 
                                      const int idZone,  const char *nodeName ) {

  int idUsr ;

  /* Make the zone the current node. */
  cg_ier = cg_goto ( flNr, idBase, "Zone_t", idZone, "end" ) ;
  if ( cg_ier ) cgh_err () ;

  idUsr = cgh_node_exists ( &cgUsrNodes, nodeName ) ;
  if ( cg_ier ) cgh_err () ;

  if ( !idUsr ) {
    /* Create. */
    idUsr = cgh_node_add ( &cgUsrNodes, nodeName ) ;
    cg_ier = cg_user_data_write ( nodeName ) ;

    if ( !idUsr || cg_ier ) {
      cgh_err () ;
      if ( verbosity > 1 ) cg_error_print () ;
      hip_err ( fatal, 0, " in cgh_zone_user_data_write:  Could not create node\n" ) ;
    }
  }

  cg_ier = cg_goto ( flNr, idBase, "Zone_t", idZone, "UserDefinedData_t", idUsr, "end" ) ;
  if ( cg_ier ) cgh_err () ;

  return ( cg_ier ) ;
}


static BCType_t cgnsBcType ( char hipBcType ) {

  switch ( hipBcType ) {
  case 'e' :
    /* entry, (inlet) */
    return ( BCInflow ) ;
  case 'f' :
    /* far field */
    return ( BCFarfield ) ;
  case 'i' :
    /* inviscid wall */
    return ( BCWallInviscid ) ;
  case 'l' :
    /* lower periodic */
    return ( BCTypeUserDefined ) ;
  case 'o' :
    return ( BCTypeUserDefined ) ;
  case 'p' :
    /* any periodic */
    return ( BCTypeUserDefined ) ;
  case 's' :
    /* symmetry */
    return ( BCSymmetryPlane ) ;
  case 'u' :
    return ( BCTypeUserDefined ) ;
  case 'v' :
    /* viscous wall */
    return ( BCWallViscous ) ;
  case 'w' :
    /* any wall */
    return ( BCWall ) ;
  default  :
    /* 'n', none */
    return ( BCTypeNull ) ;
  }
}


/******************************************************************************

  write_cgns_coor:
  Write the coordinates.
  
  Last update:
  ------------
  18Sep18; add comment on writing dataclass for coor.
  16Feb17; add cg_grid_write. Is that needed? Does this suppress cgnscheck warning?
  15Feb17; test cg_coord_write for ier.
  19Dec11; use cgh_err.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_cgns_coor ( uns_s *pUns, const int flNr, 
                             const int idBase, const int idZone ) {

  const char coorName[][12] = { "CoordinateX","CoordinateY","CoordinateZ"} ;

  /* Seems that this is not explicitly needed, examples show that
     writing a coordinate array triggers generating the GridCoordinates_t node. */
  int grNr ;
  int cg_ier = cg_grid_write ( flNr, idBase, idZone, "GridCoordinates", &grNr ) ;
    if ( cg_ier ) cgh_err () ;
    /* This would suppress the warning on missing dataclass, but
       it would require to state units.
    cg_ier = cg_dataclass_write ( Dimensional ) ;
    if ( cg_ier ) cgh_err () ; */

  int mVx = pUns->mVertsNumbered ;
  double *pCoor = arr_malloc ( "pCo in write_cgns_coor", pUns->pFam, 
                               mVx, sizeof( double ) ) ;

  int kDim ;
  double *pCo ;
  chunk_struct *pChunk ;
  int nBeg, nEnd, idC ;
  vrtx_struct *pVx, *pVxEnd, *pVxBeg ;
  for ( kDim = 0 ; kDim < pUns->mDim ; kDim++ ) {
    /* Loop over all vertices and list numbered coordinates. */
    pCo = pCoor ;
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ))
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number ) {
          *pCo++ =  pVx->Pcoor[kDim] ;
        }

    if ( pCo-pCoor != mVx ) {
      sprintf ( hip_msg, "%d coordinates expected, but %td found in write_cgns_coor.\n",
                mVx, pCo-pCoor ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }


    cg_ier = cg_coord_write ( flNr, idBase, idZone, RealDouble, coorName[kDim], pCoor, &idC ) ;
    if ( cg_ier ) cgh_err () ;
  }

  arr_free ( pCoor ) ;




  if ( pUns->pUnsCoarse ) {
    /* Multigrid vertex connectivity. */
    if ( pUns->mVertsNumbered != pUns->mVxCollapseTo ) {
      printf ( hip_msg, "expected %d collapse pointers, found %d in write_cgns_coor.\n",
               pUns->mVertsNumbered, pUns->mVxCollapseTo ) ;
      hip_err ( fatal, 0, hip_msg) ;
    }

    hip_err ( fatal, 0, " node->mg_node to be done in write_cgns_coor.\n" ) ;
  }
  
  return ( 1 ) ;
}

/******************************************************************************

  write_cgns_conn:
  Write the connectivity. Renumber all elements in the proper element type order.
  
  Last update:
  ------------
  1Jun22; switch pEl2Vx from int to cgsize_t.
  14Aug19; use pEl->elType to filter for type, rather than pEl->markdEdges.
  1Jul14; remove MAX_DRV_ from loop syntax, as pgcc compiler complains about
          tautology.
  6Apr13; promote possibly large int to type ulong_t.
  19Dec11; use cgh_err.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_cgns_conn ( uns_s *pUns, const int flNr, 
                             const int idBase, const int idZone, 
                             int *pFoundDrv ) {

  const int *cg2aE ;

  vrtx_struct **ppVx ;
  chunk_struct *pChunk ;
  int mVerts ;
  ulong_t mVerts_szt ;
  //int *pEl2Vx, *pEV, mElems, mElW ;
  cgsize_t *pEl2Vx, *pEV, mElems, mElW ;
  ulong_t mElems_szt ;
  ulong_t mEN = 0 ;
  int found ;
  ulong_t nVx ;
  int idSec ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  elType_e elType ;

  /* This implementation is based on the faulty CGNS-MLL, which only supports
     up to int. */
 


  /* Element connectivities. First all base elements. */
  for ( elType = tri, mElW = 0 ; elType <= hex ; elType++, mElW += mElems ) {

    cg2aE = cg2a[ (int) elType ] ;

    mVerts = mVerts_szt = elemType[elType].mVerts ;
    /* We want clean base elements only. These are counted in the first tri-hex
       positions of mElems_w_mVerts. */
    mElems = mElems_szt = pUns->mElems_w_mVerts[elType] ;
    pEl2Vx = NULL ;

    if ( mVerts != mVerts_szt || mElems != mElems_szt ) {
      hip_err ( fatal, 0, "CGNS MLL only supports up to int types, grid is too big." ) ;
    }

    if ( mElems ) {      
      pEV = pEl2Vx = arr_malloc ( "pEl2Vx in write_cgns_conn", pUns->pFam,
                                  mVerts*mElems, sizeof( ulong_t ) ) ;
      
      /* Make a consecutive list of pointers. */
      pChunk = NULL ;
      while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
        for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
          if ( pEl->number &&
               // why markdEdges here? pEl->markdEdges
               pEl->elType == elType ) {
            pEl->number = ++mEN ;
            ppVx = pEl->PPvrtx ;
            for ( nVx = 0 ; nVx < mVerts ; nVx++ )
              *pEV++ = ppVx[ cg2aE[nVx] ]->number ;
          }
      
      if ( pEV-pEl2Vx != mVerts*mElems ) {
        sprintf ( hip_msg, "%"FMT_ULG" conn. entries expected, but %"FMT_ULG" found"
                  " in write_cgns_conn.\n", (ulong_t) mVerts*mElems, (ulong_t)(pEV-pEl2Vx) ) ; 
        hip_err ( fatal, 0, hip_msg ) ;
      }

      /* Create the cgns entry. */
      /* It appears that even for 64bit builds of CGNS, cgnstypes.h lets the
         cgsize_t be an int. */
      cg_ier = cg_section_write ( flNr, idBase, idZone, 
                         cgElemName[ (int) elType ], cgElemType[ (int) elType ],
                         mElW+1, mElW+mElems, 0, pEl2Vx, &idSec ) ;
      if ( cg_ier ) cgh_err () ;
 
      arr_free ( pEl2Vx ) ;
    }
  }
  

  
  /* List the connectivity of the derived elements. */
  elType_e elTMax = MAX_DRV_ELEM_TYPES ; // Attempt to pacify the pgcc compiler.
  for ( found = 0, elType = hex+1 ;
        elType < elTMax ; elType++ )
    if ( ( mElems = pUns->mElems_w_mVerts[elType] ) ) {
      found += mElems ;
    }

  if ( found ) {
    /* There were derived elements. Make an entry with the elMark version. */
    hip_err ( fatal, 0, "implement derived elements in write_cgns_conn.\n" ) ;
  }



  if ( mEN != pUns->mElemsNumbered ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", found %"FMT_ULG" numbered elements in write_cgns_conn.\n",
              pUns->mElemsNumbered, mEN ) ;
    hip_err ( fatal,0, hip_msg) ; }
  
  return ( 1 ) ;
}

/******************************************************************************

  write_cgns_bnd:
  Boundary faces.
  
  Last update:
  ------------
  1Jun22; switch pFc2Vx, pnFcBc from int to cgsize_t
  14Feb17; fix bug with mem overvlow in pEl2Vx
  6Apr13; remove pBWt from arg list to write_cgns_bnd.
  19Dec11; use cgh_err.
  18Dec11; fix bug: initialise pFE 
  1Sep11; rewritten for FamilySpecified ElementLists.
  : conceived.
  
  Input:
  ------
  pUns


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_cgns_bnd ( uns_s *pUns, const int flNr, 
                            const int idBase, const int idZone, 
                            int *pmAllBndVx ) {
  /* Add code to fill this. */
  bndVxWt_s *pBWt ;

  /* How many faces and conn entries to alloc for? */
  int mFc = pUns->mBiAllBc ;
  mFc = MAX( mFc, pUns->mTriAllBc ) ;
  mFc = MAX( mFc, pUns->mQuadAllBc ) ;

  if ( !mFc ) {
    hip_err ( warning, 0, "no boundary faces found in write_cgns_bnd." ) ;
    return (0) ;
  }

  int *pFc2El, *pFE ;
  pFE = pFc2El = arr_malloc ( "pFc2El in write_cgns_bnd", pUns->pFam,
                              mFc, sizeof(*pFc2El  ) ) ;

  /* Alloc for connectivity. */
  int mConn = 2*pUns->mBiAllBc ;
  mConn = MAX( mConn, 3*pUns->mTriAllBc ) ;
  mConn = MAX( mConn, 4*pUns->mQuadAllBc ) ;
  // int *pFc2Vx, *pFV ;
  cgsize_t *pFc2Vx, *pFV ;
  pFc2Vx = arr_malloc ( "pFc2Vx in write_cgns_bnd", pUns->pFam,
                        mConn, sizeof( *pFc2Vx ) ) ;


  /* Alloc for face lists with each boundary. */
  const int mBc = pUns->mBc ;
  //int **pnFcBc
  cgsize_t **pnFcBc
    = arr_malloc ( "pnFcBc in write_cgns_bnd", pUns->pFam, 
                              mBc, sizeof( int* ) ) ;
  //int **pnFB
  cgsize_t **pnFB
    = arr_malloc ( "pmFcBc in write_cgns_bnd", pUns->pFam, 
                           mBc, sizeof( int* ) ) ;
  int nBc ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    pnFcBc[nBc] = arr_malloc ( "pnFcBc[nBc] in write_cgns_bnd", pUns->pFam,
                               pUns->pmFaceBc[nBc], sizeof( *pnFcBc[nBc] ) ) ;
    /* Iterating pointer. */
    pnFB[nBc] = pnFcBc[nBc] ;
  }




  /* Loop over the element types, boundaries. Number faces in sequence
     and extract face->node pointers. */
  int mVFc ;
  int mElemsWritten = pUns->mElemsNumbered, mElLastSec ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  const faceOfElem_struct *pFoE ;
  int mVerts ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int kVx ;
  int idSec ;
  for ( mVFc = 2 ; mVFc <=4 ; mVFc++ ) {
    pFV = pFc2Vx ;
    pFE = pFc2El ;
    mElLastSec = mElemsWritten ;

    for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
      pBndPatch = NULL ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pElem = pBndFc->Pelem ;

          if ( pElem && pElem->number && pBndFc->nFace ) {
            /* Valid bnd face. */
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mVerts = pFoE->mVertsFace ;

            if ( mVerts == mVFc ) {
              /* Number faces consecutively after the elements. */
              *(pnFB[nBc])++ = ++mElemsWritten ;
              /* 'ParentData' */
              *pFE++ = pElem->number ;
 
              kVxFc = pFoE->kVxFace ;
              ppVx = pElem->PPvrtx ;
              /* List the face connectivity. */
              for ( kVx = 0 ; kVx < mVerts ; kVx++ ) 
                *pFV++ = ppVx[ pFoE->kVxFace[ kVx ] ]->number ;
            }
          }
        }
    } // for nBc

    if ( mElemsWritten-mElLastSec ) {
      /* Create the cgns entry. */
      cg_ier = cg_section_write ( flNr, idBase, idZone, 
                                  cgFaceName[mVFc], cgFaceType[mVFc],
                                  mElLastSec+1, mElemsWritten, 0, pFc2Vx, &idSec ) ;
      if ( cg_ier ) cgh_err () ;
    }

    /* ParentInfo.
       Do we need this? 
    cg_ier = cg_parent_data_write( flNr, idBase, idZone, idSec, pFc2El ) ;
    if ( cg_ier ) cgh_err () ;  */
  }



  /* Write face lists for each bc. */
  BCType_t bcType ;
  bc_struct *pBc ;
  int idBc ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    if ( pnFB[nBc] - pnFcBc[nBc] != pUns->pmFaceBc[nBc] ) {
      sprintf ( hip_msg, "miscount in faces for bnd %d: expected %"FMT_ULG", "
                "found %"FMT_ULG" faces in write_cgns_bnd.",
                nBc,  pUns->pmFaceBc[nBc], (ulong_t) (pnFB[nBc] - pnFcBc[nBc]) ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }


    pBc = pUns->ppBc[nBc] ;
    bcType = cgnsBcType( pBc->type[0] ) ;

    /* bnd facess. */
    cg_ier = cg_boco_write( flNr, idBase, idZone, pBc->text, bcType, 
                   ElementList, pUns->pmFaceBc[nBc], pnFcBc[nBc], &idBc ) ;
    if ( cg_ier ) cgh_err () ;


    /* Write b.c. info, declare the 'family'. */
    cg_ier = cg_goto( flNr, idBase, "Zone_t", 1, "ZoneBC_t", 1, "BC_t",idBc,"end" ) ;
    if ( cg_ier ) cgh_err () ;
    cg_ier = cg_famname_write( pBc->text ) ;
    if ( cg_ier ) cgh_err () ;
    /* cg_ier = cg_gridlocation_write( FaceCenter ) ;
       if ( cg_ier ) cgh_err () ; */
  }


  /* Dealloc. */
  arr_free ( pFc2El ) ;
  arr_free ( pFc2Vx ) ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ )
    arr_free ( pnFcBc[nBc] ) ;
  arr_free ( pnFcBc ) ;
  arr_free ( pnFB ) ;

  return ( 1 ) ;
}


/******************************************************************************

  write_cgns_per:
  Write a list of periodic vertex pairs. When mulitple periodicity occurs,
  all combinations are listed once. Symmetry vertices are listed at the
  beginning of the list as periodic with themselves. The behaviour of both
  symmetry and periodicity is not specified.
  
  Last update:
  ------------
  6Apr13; promote possibly large int to type ulong_t.
  24Apr02; list mSymmVx only for hydra.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_cgns_per ( uns_s *pUns, const int flNr, 
                            const int idBase, const int idZone ) {

  int iVx, mVx, *pnVxPer, *pnV ;

  mVx = pUns->mPerVxPairs ;
    
  if ( !mVx )
    /* Nothing to write. */
    return ( 1 ) ;


  pnV = pnVxPer = arr_malloc ( "pnVxPer in write_cgns_per", pUns->pFam,
                                   2*mVx, sizeof( int ) ) ;
  
  /* List the periodic vertex pairs. */
  pnV = pnVxPer ;
  for ( iVx = 0 ; iVx < pUns->mPerVxPairs ; iVx++ ) {
    *pnV++ = pUns->pPerVxPair[iVx].In->number ;
    *pnV++ = pUns->pPerVxPair[iVx].Out->number ;
  }

  if ( pnV - pnVxPer != 2*mVx ) {
    sprintf ( hip_msg, "%"FMT_ULG" periodic pairs expected, %td found in write_cgns_per.\n",
             pUns->mPerVxPairs, (pnV - pnVxPer)/2 ) ;
    hip_err( fatal, 0, hip_msg ) ; }


  cg_ier = cg_goto ( flNr, idBase, "Zone_t", idZone, "end" ) ;
  if ( cg_ier ) cgh_err () ;
  cg_ier = cgh_zone_user_data_write ( flNr, idBase, idZone, "SpecialNodes" ) ;
  if ( cg_ier ) cgh_err () ;
  cg_ier = cgh_write2 ( "PerNodePairs",  Integer, 2, mVx, ( void * ) pnVxPer ) ;
  if ( cg_ier ) cgh_err () ;


  arr_free ( pnVxPer ) ;

  return ( 1 ) ;
}



/* Wrapper. First check whether the node doesn't exist. If not, create. 
   Returns: 0 for failure,
            1 for new node,
            2 for existing node. */     

static int cgh_sol_write ( const int flNr, const int idBase, const int idZone, 
                    const char *nodeName, GridLocation_t gLoc, int *pid ) {

  int idSol ;

  /* Make the zone the current node. */
  cg_ier = cg_goto ( flNr, idBase, "Zone_t", idZone, "end" ) ;
  if ( cg_ier ) cgh_err () ;

  idSol = cgh_node_exists ( &cgSolNodes, nodeName ) ;

  if ( !idSol ) {
    /* Create. */
    idSol = cgh_node_add ( &cgSolNodes, nodeName ) ;
    cg_ier = cg_sol_write ( flNr, idBase, idZone, nodeName, gLoc, pid ) ;
    if ( !idSol || cg_ier ) {
      cgh_err () ;
      hip_err ( fatal, 0, "in cgh_sol_write: Could not create node\n" ) ;
    }
    else if ( *pid != idSol ) {
      sprintf ( hip_msg, "in cgh_sol_write: idSol %d and *pid %d differ!\n", 
                idSol, *pid );
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  else {
    /* Existing. */
    *pid = idSol ;
  }

  cg_ier = cg_goto ( flNr, idBase, "Zone_t", idZone, "FlowSolution_t", *pid, "end" ) ;
  if ( cg_ier ) cgh_err () ;

  return ( 0 ) ;
}


void getOneUnkn ( const uns_s *pUns, const int nUnkn, double *pUnkn) {

  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;

  /* Pack the solution variables. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number )
        *pUnkn++ = pVx->Punknown[nUnkn] ;
    
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

cgh_write_var
*/
/*! Write a variable field to cgns.
 *
 */

/*
  
  Last update:
  ------------
  16Feb17; use one flowsolution node for all fields.
  : conceived.
  

  Input:
  ------
  flNr = number of opened cgns file
  idBase = number of base
  idZone = number of zone
  idSol = number of flow solution node
  pUns = grid
  nUnkn = number of unknown to write

  Changes to:
  -----------
  pUnknown = pointer to work array of at least mVx.

  Returns:
  --------
  0 on success
  
*/


int cgh_write_var ( const int flNr, const int idBase, const int idZone, const int idSol,
                    const uns_s *pUns, const int nUnkn, double *pUnknown ) {

  const char *varName = NULL ;
  //const char *varTypeName ; 
  const char nsVarName[][25] = { "Density", "MomentumX", "MomentumY", "MomentumZ", 
                                 "EnergyStagnationDensity" } ;
  const char speciesVarName[][25] = { "MassFraction" };
  const varList_s *pVL = &pUns->varList ;
  const var_s *pVar ;
  const int mUn = pVL->mUnknowns, mDim = pUns->mDim ;

  int mUnSoFar = 0, kUn, idField ;

  if ( pUns->varList.varType == noVar ) {
    /* No solution. Don't write the file. */
    return ( 1 ) ;
  }

  if ( nUnkn >= mUn || nUnkn < 0 ) {
    sprintf ( hip_msg, "in cgh_write_var: only %d unknowns available.\n", mUn ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Find the containing varType. */
  while ( 1 ) {
    if ( nUnkn >= mUnSoFar && nUnkn < mUn )
      break ;
    else {
      mUnSoFar += pVL->mUnknowns ;
    }
  }

  kUn = nUnkn - mUnSoFar ;

  /* Patch up names for N-S vars. */
  if ( nUnkn < mDim+2 ) {
    if ( mDim == 2 && nUnkn == 3 )
      /* Skip MomentumZ. */
      varName = nsVarName[4] ;
    else
      varName = nsVarName[nUnkn] ;
  }
  else 
  {
      
      pVar = pVL->var+nUnkn ; // +nUnkn ;
      varName = pVar->name;

//      if ( pVar[nUnkn].cat == species) {
      // We need to concat  MassFraction before the species symbol
      //
//      }
  }

  getOneUnkn ( pUns, nUnkn, pUnknown ) ;

  // Only one solution node per zone.
  // cgh_sol_write ( flNr, idBase, idZone, varName, Vertex, &idSol ) ;
  cg_ier = cg_field_write ( flNr, idBase, idZone, idSol, RealDouble, 
                   varName, pUnknown, &idField ) ;
  if ( cg_ier ) cgh_err () ;

  return (0) ;
}


/******************************************************************************

  write_cgns_sol:
  .
  
  Last update:
  ------------
  17Dec13; comment out check_var_name, should be done at read time.
  25Apr02; derived from write_cgns_sol_hydra.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_cgns_sol ( uns_s *pUns, const int flNr, const int idBase, 
                            const int idZone ) {

  const int mUnkn = pUns->varList.mUnknowns ;
  double *pUnknown, dt_sum = 0.0, dt_av = 0.0 ;
  int nUnkn, niterd = 0 ;

  /* Make sure all vars have individual var types. */
  conv_uns_var ( pUns, cons ) ;
  /* Should have been done at read time.
  check_var_name ( &pUns->varList, &pUns->restart, pUns->mDim ) ; */

  


  /* Conv Hist. */
  cg_ier = cg_goto ( flNr, idBase, "end" ) ;
  if ( cg_ier ) cgh_err () ;
  cg_ier = cg_convergence_write ( niterd, "" ) ;
  if ( cg_ier ) cgh_err () ;
  cg_ier = cg_goto ( flNr, idBase, "ConvergenceHistory_t", 1, "end" ) ;
  if ( cg_ier ) cgh_err () ;

  cgh_write1 ( "dt_sum",  RealDouble, 1, ( void * ) &dt_sum ) ;
  cgh_write1 ( "dt_av",   RealDouble, 1, ( void * ) &dt_av ) ;

  int idSol ;
  cgh_sol_write ( flNr, idBase, idZone, "FlowSolution", Vertex, &idSol ) ;


  /* Allocate for a single unknown. */
  pUnknown = arr_malloc ( "write_cgns_sol: pUnknown", pUns->pFam, 
                          pUns->mVertsNumbered, sizeof( double ) ) ;


  for ( nUnkn = 0 ; nUnkn < mUnkn ; nUnkn++ )
      cgh_write_var ( flNr, idBase, idZone, idSol, pUns, nUnkn, pUnknown ) ;

  arr_free ( pUnknown ) ;
  
  return ( 1 ) ;
}


/******************************************************************************

  write_cgns_header:
  .
  
  Last update:
  ------------
  4Sep17; rename pUns->perAngleRad ;
  15Dec13; remove annCasc topo, intro axiZ.
  : conceived.
  
  Input:
  ------
  pUns = grid
  flNr = number of opened cgns file
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int cgh_add_header ( uns_s *pUns, int flNr ) {

  char string[LINE_LEN] ;

  /* position at top. */
  
  /* version. */
  sprintf ( string, "%s of %s.", version, lastUpdate ) ;
  cg_ier = cg_descriptor_write ( "hip_version", string ) ;
  if ( cg_ier ) cgh_err () ;

  /* topology */
  cg_ier = cg_descriptor_write ( "topology", topoString[ (int) pUns->specialTopo ] ) ;
  if ( cg_ier ) cgh_err () ;
  cg_ier = cg_descriptor_write ( "name", pUns->pGrid->uns.name ) ;
  if ( cg_ier ) cgh_err () ;

    
  /* Periodic angle.

  Needs to be extracted from perBc as each can have their own. 

  if ( pUns->perAngleRad != 0. || 
       ( pUns->specialTopo >= axiX &&pUns->specialTopo <= axiZ ) )
    cgh_write1 ( "periodic_angle", RealDouble, 1, ( void * ) &pUns->perAngleRad ) ;
     */  

  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! add a base to a file and position there.
 */

/*
  
  Last update:
  ------------
  16Feb17: conceived.
  

  Input:
  ------
  pUns = grid
  flNr = number of opened cgns file
  baseName = name of base

  Changes To:
  -----------

  Output:
  -------
  pidBase = number of added base
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cgh_add_base ( uns_s *pUns, int flNr, const char *baseName, int *pidBase ) {
  
  cg_ier = cg_base_write ( flNr, baseName, pUns->mDim, pUns->mDim, pidBase ) ;
  if ( cg_ier ) cgh_err () ;

  cg_ier = cg_goto ( flNr, *pidBase, NULL ) ; 
  if ( cg_ier ) cgh_err () ;

  return ( 0 ) ;
}

 
/******************************************************************************

  write_cgns_level:
  Write one level to the database under node lvl_id.
  
  Last update:
  ------------
  5Sep19; correct info msg to say grid.cgns.
  15Sep16; renamed to number_uns_elems_by_type
  10Sep16; new interface to number_uns_elemFromVerts, 
  6Apr13; remove pBWt from arg list to write_cgns_bnd.
  
  Input:
  ------
  pUns
  pRootName = name of grid file
  flNr = 
  pidBase = 
  nLevel

  Changes To:
  -----------

  Output:
  -------
  firstBase = name of the first base.
  firstZone = name of the first zone.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void write_cgns_level ( uns_s *pUns, const char *pRootName, 
                        const int flNr, int *pidBase, int nLevel,
                        char *firstBase, char* firstZone ) {

  int returnValue = 1, foundDrv = 0, nBc, mAllBndVx ;
  cgsize_t grSize[3] ;
  int idZone ;
  char levelTag[LINE_LEN] ;

  /* Make a base name. */
  if ( nLevel || pUns->pUnsCoarse ) {
    /* Multi-grid sequnce. */
    strncpy ( firstBase, pUns->pGrid->uns.name, MAX_BASE_LEN-4 ) ;
    sprintf ( levelTag, "_%02d", nLevel ) ;
    strcat ( firstBase, levelTag ) ;
    
    if ( verbosity > 2 ) {
      printf ( "   Writing multi-grid level %d as %s to: %s.sol.cgns\n",
               nLevel, firstBase, pRootName ) ;
    }
  }
  else {
    /* Single grid. */
    strncpy ( firstBase, pRootName, MAX_BASE_LEN-1 ) ;
    
    if ( verbosity > 2 ) {
      printf ( "   Writing mesh as %s to: %s.grid.cgns\n",
               firstBase, pRootName ) ;
    }
  }


  cgh_add_base ( pUns, flNr, firstBase, pidBase ) ;
  cgh_add_header ( pUns, flNr ) ;


  
  /* Number the leafs with the number of vertices. */
  number_uns_elems_by_type ( pUns, leaf, tri, hex, 1 ) ;
  
  /* Make pairs of periodic nodes. Faces are listed non-periodically. */
  if ( !special_verts ( pUns ) ) {
    hip_err ( fatal, 0, "failed to match periodic vertices in write_cgns_level." ) ;
  }
  else if ( pUns->multPer && verbosity > 2 ) {
    hip_err ( info, 1, "found multiple periodicity." ) ;
   }

  /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
     for the current numbering of elements. */
  count_uns_bndFaces ( pUns ) ;



  /* Open the zone. */
  grSize[0] = pUns->mVertsNumbered ;
  grSize[1] = pUns->mElemsNumbered ;
  grSize[2] = 0 ;
  strcpy ( firstZone, "Zone_000" ) ;
  cg_ier = cg_zone_write ( flNr, *pidBase, firstZone, grSize, Unstructured, &idZone ) ;
  if ( cg_ier ) cgh_err () ;

  cgh_reset_nodes ( cgSolNodes ) ;
  cgh_reset_nodes ( cgUsrNodes ) ;

  
  /* Finest grid. Start numbering elements above 0. */
  write_cgns_coor  ( pUns, flNr, *pidBase, idZone ) ;
  write_cgns_conn  ( pUns, flNr, *pidBase, idZone, &foundDrv ) ;
  write_cgns_bnd   ( pUns, flNr, *pidBase, idZone, &mAllBndVx ) ;
  write_cgns_per   ( pUns, flNr, *pidBase, idZone ) ;

  return ;
}




/******************************************************************************

  write_uns_cgns:
  Write all levels of a grid to cgns.
  
  Last update:
  ------------
  1Jun22; promote grsize from int to cgsize_t.
  17Sep18; add slashes to path firstBase.
  21Apr17; extend firstBase, firstZone buffers by 1 to include \n. 
  19Dec11; use cgh_err.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_uns_cgns ( char *pRootName ) {

  uns_s *pUns=NULL ;
  uns_s *pUnsLvl ;
  char solFile[TEXT_LEN], gridFile[TEXT_LEN], string[LINE_LEN] ;
  int flNr, idBase, idZone ;
  cgsize_t grSize[3] ;

  hip_err ( info, 1, "writing grid to unstructured cgns format.\n" ) ;



  if ( Grids.PcurrentGrid && Grids.PcurrentGrid->uns.type == uns )
    pUns = Grids.PcurrentGrid->uns.pUns ;
  else {
    hip_err ( warning, 0, "there is no unstructured grid to write in write_uns_cgns.\n" ) ;
  }


  /* Check for existence of the directory. */
  strcpy ( string, "." ) ;
  FILE* Ftest ;
  if ( !( Ftest = r1_fopen ( prepend_path ( string ), TEXT_LEN, "r" ) ) ) {
    hip_err ( fatal, 0, "could not open directory in write_uns_cgns.\n" ) ;
  }
  else
    fclose ( Ftest ) ;

  

  if ( !pUns->validGrid )
    hip_err ( fatal, 0, "you were told that this grid is invalid, weren't you?.\n" ) ;
  else if ( (check_bnd_setup ( pUns )).status != success )
    hip_err ( fatal, 0, "cannot write a grid without proper boundary setup.\n" ) ;


  /* Build a file name with a local path. */
  size_t pathLen = MAX( strlen ( Grids.path ), strlen ( "./" ) ) ;
  size_t rootLen = strlen ( pRootName) ;
  size_t suffLen = strlen ( ".grids.cgns" ) ;
  if ( pathLen+rootLen+suffLen > LINE_LEN-1 ) {
    hip_err ( warning, 1, "root file name too long, truncated." ) ;
    pRootName[LINE_LEN-pathLen-suffLen-1] = '\0' ;
  }

  sprintf ( gridFile, "%s.grid.cgns", pRootName ) ;
  prepend_path ( gridFile ) ;


  /* Is there a solution? Build the file name for the link in the grid file.*/
  if ( pUns->varList.varType != noVar ) {
    sprintf ( solFile, "./%s.sol.cgns", pRootName ) ;
  }
  

      
  cg_ier = cg_open ( gridFile, CG_MODE_WRITE, &flNr ) ;
  if ( cg_ier ) {
    /* File open error? */
    cgh_err () ;
    sprintf ( hip_msg, "file: %s could not be opened.\n", gridFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Write out the mesh for all grid levels, one base per level. */
  int nLevel ;
  char firstBase[LINE_LEN+1], firstBase0[LINE_LEN+1] ;
  char firstZone[LINE_LEN+1], firstZone0[LINE_LEN+1] ;
  for ( pUnsLvl = pUns, nLevel = 0 ; pUnsLvl ;
        pUnsLvl = pUnsLvl->pUnsCoarse, nLevel++ ) {

    /* Make a new zone for each level if there is a grid sequence. */
    write_cgns_level ( pUnsLvl, pRootName , flNr, &idBase, nLevel, firstBase, firstZone ) ;

    
    if ( nLevel ==0 && pUns->varList.varType != noVar ) {
      /* Link the solution file into the mesh file. */
      strcpy ( firstBase0, firstBase ) ;
      strcpy ( firstZone0, firstZone ) ;
      cg_ier = cg_goto( flNr, idBase, "Zone_t", 1, NULL );
      if (cg_ier) cgh_err () ;

      strncat ( firstBase, "/", LINE_LEN ) ;
      strncat ( firstBase, firstZone, LINE_LEN ) ;
      strncat ( firstBase, "/", LINE_LEN ) ;
      strncat ( firstBase, "FlowSolution", LINE_LEN ) ;
      cg_ier = cg_link_write( "FlowSolution", solFile, firstBase );
      if (cg_ier) cgh_err () ;
    }
  }

  
  cg_ier = cg_close ( flNr ) ;
  if ( cg_ier ) cgh_err () ;



  
  /* Is there a solution? */
  if ( pUns->varList.varType != noVar ) {

    strcpy ( solFile, pRootName ) ;
    prepend_path ( solFile ) ;
    strcat ( solFile, ".sol.cgns" ) ;

    if ( verbosity > 2 ) {
      if ( nLevel || pUns->pUnsCoarse )
        printf ( "   Writing solution for level %d to: %s.\n",
                 nLevel, solFile ) ;
      else 
        printf ( "   Writing solution to: %s.\n",
                 solFile ) ;
    }
    

    
    cg_ier = cg_open( solFile, CG_MODE_WRITE, &flNr ) ;
    if ( cg_ier ) {
      /* File open error? */
      cgh_err () ;
      sprintf ( hip_msg, "file: %s could not be opened.\n", gridFile ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    cgh_add_base ( pUns, flNr, firstBase0, &idBase ) ;
    cgh_add_header ( pUns, flNr ) ;

    grSize[0] = pUns->mVertsNumbered ;
    grSize[1] = pUns->mElemsNumbered ;
    grSize[2] = 0 ;
    cg_ier = cg_zone_write ( flNr, idBase, firstZone0, grSize, Unstructured, &idZone ) ;
    if ( cg_ier ) cgh_err () ;


    if ( !write_cgns_sol ( pUns, flNr, idBase, idZone ) ) {
      hip_err ( fatal, 0, "could not write solution in write_uns_cgns.\n" ) ;
    }

    cg_ier = cg_close ( flNr ) ;
    if ( cg_ier ) cgh_err () ;

  }



  return ( 1 ) ;
}
