/*
  check_uns.c:
  Check the unstructured grid.
  
  Last update:
  -----------
  8Apr13; use dereferenced pointer in sizeof when allocating.
  3Jul10; intro update_h_vol.
  20Dec01; list smallest/largest element on verbosity > 2i n check_vol,
  14Dec98; list smallest element in check_vol, the largest angle in check_elem.
  4May96; conceived.
  
  Contains:
  ---------
  check_uns:
  check_elems:
  update_face:
  fill_uns_fc2el:
  get_degenEdges:
  hex22prisms:
*/

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

/* Timing stuff. Do we really need types.h? */
#include <sys/types.h>
#include <sys/times.h>
#include <unistd.h>

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;
// extern int check_lvl ;


extern double default_epsOverlap ;
extern const int negVol_abort ;
extern Grids_struct Grids ;
extern elemType_struct elemType[] ;
extern const int fix_degenElems ;
extern const doFc_struct doWarn, doRemove ;
extern const int dg_fix_lrgAngles ;
extern const double dg_lrgAngle ;

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  chk_args:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  31May22: conceived.
  

  Input:
  ------
  argline

  Output:
  -------
  *pmBuckets: number of buckets in distribution
  *pdoVis: plot worst elements in vtk.
  *pmWorst: number of worst elements to show in detail.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void chk_args ( char argLine[], int *pmBuckets, int *pmVis, int *pmWorst ) {
#undef FUNLOC
#define FUNLOC "in chk_args"


  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;
  
  // default values
  *pmBuckets = 10 ;
  *pmWorst = 0 ;
  *pmVis = 0 ;

  char c ;
  while ((c = getopt_long ( mArgs, ppArgs, "b:v:w:",NULL,NULL)) != -1) {
    switch (c)  {
    case 'b':
      *pmBuckets = atoi(optarg) ;
      break;
    case 'v':
      *pmVis = atoi(optarg) ;
      break;
    case 'w':
      *pmWorst = atoi(optarg) ;
      break;
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_elem_squish:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

double calc_elem_squish ( const elem_struct *pElem ) {
#undef FUNLOC
#define FUNLOC "in calc_elem_squish"

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const edgeOfElem_struct *pEoE ;

  int mVxEl ;
  const vrtx_struct *pVxEl[MAX_VX_ELEM] ;
  double elemGC[MAX_DIM] ;
  // Elem grav ctr:
  elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;

  const vrtx_struct *pVxFc[MAX_VX_FACE] ;
  double elSquish = -TOO_MUCH ;
  double fcNorm[MAX_DIM], faceGC[MAX_DIM], el2fc[MAX_DIM] ;
  int mTimesNormal ;
  int kFc ;
  double nrm ;
  int mVxFc ;
  double scProd ;
  // If a face area is only this fraction of a square with epsOverlap,
  // consider it degenerate into a line.
  double nrmFrac=1.e-7 ;
  // Avoid division by zero when normalising face-element centroid distances.
  double distFrac=1.e-7 ;
  for ( kFc = 1 ; kFc <= pElT->mFaces ; kFc++ ) {
    // Face normal:
    uns_face_normal ( pElem, kFc, fcNorm, &mTimesNormal ) ;
    nrm = vec_len_dbl ( fcNorm, pElT->mDim ) ;
    if ( nrm > nrmFrac*Grids.epsOverlapSq ) {
      // Non-degenerate face, we have a normal.
      vec_mult_dbl ( fcNorm, 1./nrm, pElT->mDim ) ;

      face_grav_ctr ( pElem,  kFc, faceGC, &pFoE, &mVxFc, pVxFc ) ;
      // Vector fc grav to elem grav.
      vec_diff_dbl ( faceGC, elemGC, pElT->mDim, el2fc ) ;
      nrm = vec_len_dbl ( el2fc, pElT->mDim ) ;
      if ( nrm > distFrac*Grids.epsOverlap ) {
        // Non-degenerate face, we have a normal.
        vec_mult_dbl ( el2fc, 1./nrm, pElT->mDim ) ;
        scProd = scal_prod_dbl( fcNorm, el2fc, pElT->mDim ) ;
        elSquish = MAX( elSquish, 1 - scProd ) ;
      }
    }
  }

  return ( elSquish ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_elem_smoothness:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

double calc_elem_smoothness
( const elem_struct *pElem, double elVol,
  double minVolElemWithVx[], double maxVolElemWithVx[] ) {
#undef FUNLOC
#define FUNLOC "in calc_elem_smoothness"


  const elemType_struct *pElT = elemType + pElem->elType ;
  double smooth = -TOO_MUCH ;

  int kVx ;
  int nVx ;
  double minNghVol = TOO_MUCH, maxNghVol = -TOO_MUCH ;
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    nVx = pElem->PPvrtx[kVx]->number ;
    minNghVol = MIN( minNghVol, minVolElemWithVx[nVx] ) ;
    maxNghVol = MAX( maxNghVol, maxVolElemWithVx[nVx] ) ;
  }

  smooth = MAX( maxNghVol/elVol, elVol/minNghVol ) ;
  return ( smooth ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  ideal_elem_properties:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  31May22: conceived.
  

  Input:
  ------
  pElem:
  hAvg: average edge length

  Output:
  -------
  *palEquilat: minimum angle in the equilateral element
  *pvolEquilat: volume of the equilateral element.

  
*/

void ideal_elem_properties ( const elem_struct *pElem, const double hAvg,
                             double *palEquilat, double *pvolEquilat ) {
#undef FUNLOC
#define FUNLOC "in ideal_elem_properties"

  double hAvg2 = hAvg*hAvg ;
  double hAvg3 = hAvg2*hAvg ;

  switch ( pElem->elType ) {
  case tri :
    *palEquilat = 60. ;
    *pvolEquilat = hAvg2*sqrt(3)/4. ;
    break;
  case qua :
    *palEquilat = 90. ;
    *pvolEquilat = hAvg2 ;
    break;
  case tet :
    // https://en.wikipedia.org/wiki/Tetrahedron
    // dihedral angle is 70.4 deg, acos(1/3),
    // in-face angle is lower with 60deg, as this is a regular equilat tri
    *palEquilat = 60. ;
    *pvolEquilat = hAvg3*sqrt(2)/12. ;
    break;
  case pyr :
     // in-face angle is 60, as this is a regular equilat tri
    *palEquilat = 60. ;
    *pvolEquilat = hAvg3*sqrt(2)/6. ;
    break;
  case pri :
    *palEquilat = 60. ;
    *pvolEquilat = hAvg3/2. ;
    break;
   case hex :
   *palEquilat = 90. ;
   *pvolEquilat = hAvg3 ;
    break;
  default :
    // edge, just give something.
    *palEquilat = 1. ;
   *pvolEquilat = 1. ;
  }
   
   
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_elem_qual:
*/
/*! Calculate the quality of an element given a metric.
 *
 */

/*
  
  Last update:
  ------------
  30/5/22: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void calc_elem_qual ( const elem_struct *pElem,
                      double minVolElemWithVx[], double maxVolElemWithVx[],
                      double *pEquAngleSkew, double *pEquVolSkew,
                      double *pSquishIndex, double *pSmoothness
                      ) {
#undef FUNLOC
#define FUNLOC "in calc_elem_qual"

  double alIdeal, volIdeal ;
  
  double elVol = 0., hMinSq = TOO_MUCH, hAvg ;
  double elMaxDihAngle = -TOO_MUCH, elMaxFcAngle = -TOO_MUCH ;
  double elMinAngle = TOO_MUCH ;
  int isNotColl ;
  double cosAlMax = maxMinAngle(pElem, &elVol, &isNotColl, &hMinSq, &hAvg,
                              &elMaxDihAngle,&elMaxFcAngle, &elMinAngle) ;
  double alMax = acos(cosAlMax)/PI*180. ;
  alMax = ( alMax < 0 ? alMax+180 : alMax ) ;
  double alMin = acos(elMinAngle)/PI*180. ;


  double alEquilat, volEquilat ;
  ideal_elem_properties( pElem, hAvg, &alEquilat, &volEquilat) ;
   


  // Compute quality indicators.
  *pEquAngleSkew = MAX( (alMax-alEquilat)/180., (alEquilat-alMin)/alEquilat ) ;

  *pEquVolSkew = ( volEquilat - elVol )/volEquilat ;

  *pSquishIndex = calc_elem_squish ( pElem ) ;

  *pSmoothness = calc_elem_smoothness ( pElem, elVol,
                                        minVolElemWithVx, maxVolElemWithVx ) ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  calc_elem_qual_stats:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s calc_elem_qual_stats ( char *argLine ) {
#undef FUNLOC
#define FUNLOC "in calc_elem_qual_stats"
  
  ret_s ret = ret_success () ;


  int mBuckets, mVis, mWorst ;
  chk_args ( argLine, &mBuckets, &mVis, &mWorst ) ;

  // Calculate min, max elem volumes for all elems formed with
  // each node.
  const uns_s *pUns =  Grids.PcurrentGrid->uns.pUns ;
  double *minVolElemWithVx = NULL, *maxVolElemWithVx = NULL ;
  calc_minmax_elem_vol_with_vx( pUns, &minVolElemWithVx, &maxVolElemWithVx ) ;

  // Create array mappings for the quality indicators.
#define MAXQ (4)
  char metricName[MAXQ][LINE_LEN] ;
  int mQ = 0 ;
  strcpy( metricName[mQ], "Equi-angle_Skew" );           const int iEAS = mQ++ ;
  strcpy( metricName[mQ], "Equi-volume_Skew" );          const int iEVS = mQ++ ;
  strcpy( metricName[mQ], "Element_squish_index" );      const int iSqu = mQ++ ;
  strcpy( metricName[mQ], "Element_volume_smoothness" ); const int iSmo = mQ++ ;
  if ( mQ > MAXQ ) hip_err ( fatal, 0, "need to resize maxQ "FUNLOC"." ) ;

  // First pass, calc quality metrics, get min, max
  chunk_struct *pCh = NULL ;
  elem_struct *pElBeg, *pElem, *pElEnd ;
  double elQ[MAXQ], minQ[MAXQ]={TOO_MUCH}, maxQ[MAXQ]={-TOO_MUCH} ;
  int iQ ;
  while ( loop_elems ( pUns, &pCh, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      calc_elem_qual ( pElem, minVolElemWithVx, maxVolElemWithVx,
                       elQ+iEAS, elQ+iEVS, elQ+iSqu, elQ+iSmo ) ;

      for ( iQ = 0 ; iQ < mQ ; iQ++ ) {
        minQ[iQ] = MIN( minQ[iQ], elQ[iQ] ) ; 
        maxQ[iQ] = MAX( maxQ[iQ], elQ[iQ] ) ; 
      }
    }
  }
 

  // Alloc buckets.
  int **bktQ = arr_calloc( "bktQ "FUNLOC".", pUns->pFam, mQ, sizeof(*bktQ) ) ;
  char arrName[LINE_LEN] ;
  for ( iQ = 0 ; iQ < mQ ; iQ++ ) {
    sprintf ( arrName, "bktQ for indicator %d "FUNLOC".", iQ ) ;
    bktQ[iQ] = arr_calloc( arrName, pUns->pFam, mBuckets, sizeof(*bktQ[iQ] ) ) ;
  }

  
  // Alloc list of worst elements.
  int mElList = MAX( mVis, mWorst ) ;
  elem_data_s **listElemData = arr_calloc( "listElemData "FUNLOC".", pUns->pFam, mQ, sizeof(*listElemData) ) ;
  for ( iQ = 0 ; iQ < mQ ; iQ++ ) {
    sprintf ( arrName, "listElemData for indicator %d "FUNLOC".", iQ ) ;
    listElemData[iQ] = arr_calloc( arrName, pUns->pFam, mElList, sizeof(*listElemData[iQ] ) ) ;
  }

  

  // Second pass, bucket, track mWorst
  int nBkt ;
  double delQ ;
  pCh = NULL ;
  while ( loop_elems ( pUns, &pCh, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      calc_elem_qual ( pElem, minVolElemWithVx, maxVolElemWithVx,
                       elQ+iEAS, elQ+iEVS, elQ+iSqu, elQ+iSmo ) ;

      for ( iQ = 0 ; iQ < mQ ; iQ++ ) {
        // which bucket?
        nBkt = (1.*(mBuckets-1)*( elQ[iQ]-minQ[iQ]))/( maxQ[iQ] - minQ[iQ] ) ;
        bktQ[iQ][nBkt] += 1 ;

        add_elem_list ( listElemData[iQ], mElList, pElem, elQ[iQ] ) ;
      }
    }
  }




  
  // Display
  const elem_struct **ppWorstElems = NULL ;
  if ( mVis ) {
    ppWorstElems = arr_calloc( arrName, pUns->pFam, mVis, sizeof(*ppWorstElems ) ) ;
  }
  char fileName[LINE_LEN] ;
  double qLo, qHi, dQ ;
  int iB, mVEl, iV, iW ;
  for ( iQ = 0 ; iQ < mQ ; iQ++ ) {
    printf ( "\n Distribution of %s, zero is best\n", metricName[iQ] ) ;
    printf ( "  From     To    elements\n" ) ;
    dQ = ( maxQ[iQ] - minQ[iQ] )/mBuckets ;
    qHi =  minQ[iQ] ;
    for ( iB = 0 ; iB < mBuckets ; iB++ ) {
      qLo = qHi ;
      qHi += dQ ;
      printf ( " %5.2f - %5.2f: %6.0f %%\n", qLo, qHi, 100.*bktQ[iQ][iB]/pUns->mElemsNumbered ) ;
    }


    // Print worst elems to screen
    if ( mWorst > 0 ) {
      printf ( "\n The %d elements with worst %s (highest metric value) are:\n",
               mWorst, metricName[iQ] ) ;
      printf ( " metric val   element:\n" ) ;
      for ( iW = mWorst-1 ; iW > -1 ; iW-- ) {
        printf ( " %5.2f, ", listElemData[iQ][iW].dblData ) ;
        if ( verbosity > 4 )
          printelal ( listElemData[iQ][iW].pElem ) ;
        else
          printel ( listElemData[iQ][iW].pElem ) ;
      }
    }

    // visualise worst elems
    if ( mVis ) {
      // There may be not enough elements listed, there could be empty slots.
      for ( mVEl=0, iV = mElList-1 ; iV >= mElList-mVis && listElemData[iQ][iV].pElem ; iV-- ) {
        ppWorstElems[mVEl++] = listElemData[iQ][iV].pElem ;
      }

      snprintf ( hip_msg, LINE_LEN-1, "Writing %d elements to worst_elements_%s.vtk",
                 mVEl, metricName[iQ] ) ;
      hip_err ( blank,1,hip_msg ) ;
      snprintf ( fileName, LINE_LEN-1, " worst_elements_%s.vtk", metricName[iQ] ) ;
      viz_elems_vtk0 ( fileName, mVEl, ppWorstElems, NULL ) ;
    } 
  }
  printf ("\n") ;

  
  // free
  if ( mVis ) { arr_free ( ppWorstElems ) ;}
  arr_free ( minVolElemWithVx ) ;
  arr_free ( maxVolElemWithVx ) ;
  for ( iQ = 0 ; iQ < mQ ; iQ++ ) {
    arr_free ( listElemData[iQ] ) ;  
  }
  arr_free ( listElemData ) ;
  
  return ( ret ) ;
}


/******************************************************************************

  set_grid_eps:
  Loop over all pUns and find the smallest epsOverlap, set that to Grids.eps.
  
  Last update:
  ------------
  8Jul15; allow for pUnsNew that is not yet added to the list in Grids.
  8Dec07; use DEFAULT_epsOverlap from cpre.h.
  18Jun06: conceived.

  Changes To:
  -----------
  Grids.
  
*/

void set_grids_eps ( uns_s *pUnsNew ) {

  
  grid_struct *Pgrid ;
  uns_s *pUns ;
  double eps ;

  if ( !Grids.PcurrentGrid && !pUnsNew ) {
    /* No grid present. */
    eps = DEFAULT_epsOverlap ;
    return ;
  }
  else if  ( pUnsNew ) {
    /* initialise with the new grid, which may or may not yet have
       been added to Grids. */
    eps =  pUnsNew->epsOverlap ;
  }
  else 
    eps = TOO_MUCH ;

  for ( Pgrid = Grids.PfirstGrid ; Pgrid ; Pgrid = Pgrid->uns.PnxtGrid ) {
    if ( Pgrid->uns.type == uns ) {
      pUns = Pgrid->uns.pUns ;
      eps = MIN( eps, pUns->epsOverlap ) ;
    }
  }

  Grids.epsOverlap = eps ;
  Grids.epsOverlapSq = eps*eps ;

  return ;
}

/******************************************************************************
  calc_hMin:   */

/*! Calculate the minimum edge length in the mesh and list the elements with those.
 *
 */

/*
  
  Last update:
  ------------
  20Dec14; track elMin, elMax.
  6Apr13; move src before call in check_uns.c to ensure prototyping. 
          remove unused variables.
  3Sep12: cut out of check_elems.
  

  Input:
  ------
  pUns: grid

  Output:
  -------
  pUns->hMin,hMax: min/max h in grid
  *ppElhMin: pointer to element with hMin
  *ppElhMax: pointer to element with hMax
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int calc_hMin ( uns_s *pUns, elem_struct **ppElhMin, elem_struct **ppElhMax ) {

  chunk_struct *pChunk ;
  elem_struct *pElem ;
  double hMinSq=TOO_MUCH, hMaxSq=-TOO_MUCH, dist ;
  double hMinSqOld = hMinSq, hMaxSqOld = hMaxSq ;

  hip_err ( blank, 4, "\n    Computing hMin." ) ;

  pUns->hMin = TOO_MUCH ; pUns->hMax = -TOO_MUCH ;
 
  /* Find the mesh scales for all chunks.  */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    //pChunk->hMin = TOO_MUCH ;
    //pChunk->hMax = -TOO_MUCH ;

    /* Loop over all leaf elements in this chunk. */
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( pElem->term && !pElem->invalid ) {
        
        get_degenEdges ( pElem, pChunk, &hMinSq, &hMaxSq, &dist,
                         fix_degenElems, pUns->epsOverlapSq ) ;
	/*pChunk->hMin = MIN( pChunk->hMin, hMin ) ;
	pChunk->hMax = MAX( pChunk->hMax, hMax ) ;	
        }*/
    
    /* if ( pChunk->hMin < TOO_MUCH )
      pChunk->hMin = sqrt( pChunk->hMin ) ;
    if ( pChunk->hMax > -TOO_MUCH )
    pChunk->hMax = sqrt( pChunk->hMax ) ;*/

        if ( hMinSq < hMinSqOld ) {
          hMinSqOld = hMinSq ;
          *ppElhMin = pElem ;
        }
        if ( hMaxSqOld < hMaxSqOld ) {
          hMaxSqOld = hMaxSq ;
          *ppElhMax = pElem ;
        }
      }
  }

  pUns->hMin = sqrt( hMinSqOld ) ;
  pUns->hMax = sqrt( hMaxSqOld ) ;

  pUns->epsOverlap = .9*pUns->hMin ;
  pUns->epsOverlapSq = pUns->epsOverlap*pUns->epsOverlap ;


  return ( 0 ) ;
}

  
/******************************************************************************

  hex22prisms:
  Given a hex with a large face angle at vertex iVxFc on kFace, cut it into two
  prisms and generate the two hanging edges. Located here because it needs to
  know how large an adEdge_s is.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  5Apr13; make all large counters ulong_t.
  25Oct99; hardwire reference to mAttEdges as 4.
  Aug98: conceived.
  
  Input:
  ------
  pUns:            the grid,
  pElem:           the element,
  kFace:           the face with the large angle,
  iVxFc:           and the vertex that opens the large angle.
  pNewChunk:       The chunk with the extra element and pointer spaces,
  ppLstElem:       the last element slot used,
  pppLstElem2Vert: and the last element to vertex pointer slot used.

  Changes To:
  -----------
  pNewChunk:       The chunk with the extra element and pointer spaces,
  ppLstElem:       the last element slot used,
  pppLstElem2Vert: and the last element to vertex pointer slot used.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hex22prisms ( uns_s *pUns, elem_struct *pElem, int kFace, int iVxFc,
                  chunk_struct *pNewChunk,
                  elem_struct **ppLstElem, vrtx_struct ***pppLstElem2Vert,
                  bndFc_struct **ppBndFc, ulong_t *pnLstBf, ulong_t *pmBndFc ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const edgeOfElem_struct *pEoE ;
  const int *kVxFace ;
  
  int kVx, iVx, iEg, kEg, dir, kBndFc, mVx, mNewFc ;
  int kNewFc[2] = {0} ; // Silence -O3 compiler 
  int match, nVx, iEl, kFc, iNewFc, newEg ;
  vrtx_struct *pVxTop[MAX_VX_FACE], *pVxBot[MAX_VX_FACE], **ppVxHex, **ppVxNew ;
  const vrtx_struct *pVx0, *pVx1 ;
  elem_struct *pNewEl[2], *pBndEl[2] = {NULL} ; // Silence -O3 compiler
  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  vrtx_struct *pVxBndFc[MAX_VX_FACE] = {NULL} ; // Silence -O3 compiler
  bndFc_struct *pBf ;

  if ( pElem->elType != hex || pElem->invalid )
    return ( 0 ) ;

  /* Split the hex into two prisms. Make a list of opposite vertices.
     The top is listed counterclockwise seen from outside the element. The
     new diagonal will run from 0-2 on the top and bottom face. */
  pElT = elemType + pElem->elType ;
  pFoE = pElT->faceOfElem + kFace ;
  ppVxHex = pElem->PPvrtx ;
  for ( iVx = 0 ; iVx < 4 ; iVx++ ) {
    kVx = pFoE->kVxFace[ (iVxFc+iVx)%4 ] ;
    pVxTop[iVx] = ppVxHex[kVx] ;

    /* Loop over all attached edges, find the one with kVx. */
    for ( iEg = 0 ; iEg < 4 ; iEg++ ) {
      kEg = pFoE->kAttEdge[iEg] ;
      pEoE = pElT->edgeOfElem + kEg ;
      if ( pEoE->kVxEdge[0] == kVx ) {
        pVxBot[iVx] = ppVxHex[ pEoE->kVxEdge[1] ] ;
        break ;
      }
      else if ( pEoE->kVxEdge[1] == kVx ) {
        pVxBot[iVx] = ppVxHex[ pEoE->kVxEdge[0] ] ;
        break ;
      }
    }
    if ( iEg == 4 ) {
      sprintf ( hip_msg, "could not find attached edge in hex22prisms.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  }

  /* Check for sufficient space. A prism has 6 vertices. */
  if ( !check_elem_space ( 6, pNewChunk, ppLstElem, pppLstElem2Vert ) ) {
    sprintf ( hip_msg, "out of space in hex22prisms.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  /* Stats of the new prism. */
  pNewEl[0] = ++(*ppLstElem) ;
  ppVxNew = *pppLstElem2Vert + 1 ;
  init_elem ( pNewEl[0], pri, ++pNewChunk->mElemsNumbered, ppVxNew ) ;

  /* List the new vertices. */
  ppVxNew[0] = pVxBot[0] ;
  ppVxNew[1] = pVxTop[0] ;
  ppVxNew[2] = pVxTop[1] ;
  ppVxNew[3] = pVxBot[1] ;
  ppVxNew[4] = pVxTop[2] ;
  ppVxNew[5] = pVxBot[2] ;
  *pppLstElem2Vert += 6 ;
  

  /* Check for sufficient space. A prism has 6 vertices. */
  if ( !check_elem_space ( 6, pNewChunk, ppLstElem, pppLstElem2Vert ) ) {
    sprintf ( hip_msg, "out of space in hex22prisms.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  /* Stats of the new prism. */
  pNewEl[1] = ++(*ppLstElem) ;
  ppVxNew = *pppLstElem2Vert + 1 ;
  init_elem ( pNewEl[1], pri, ++pNewChunk->mElemsNumbered, ppVxNew ) ;

  /* List the new vertices. */
  ppVxNew[0] = pVxBot[0] ;
  ppVxNew[1] = pVxTop[0] ;
  ppVxNew[2] = pVxTop[2] ;
  ppVxNew[3] = pVxBot[2] ;
  ppVxNew[4] = pVxTop[3] ;
  ppVxNew[5] = pVxBot[3] ;
  *pppLstElem2Vert += 6 ;
  
  /* Create the two hanging edges on the top and bottom face. */
  if ( !pUns->pllAdEdge )
    *( ( llEdge_s ** ) &pUns->pllAdEdge ) = 
      make_llEdge ( pUns, CP_NULL, 0, sizeof( adEdge_s ),
                    pUns->pllAdEdge, ( void ** ) &pUns->pAdEdge ) ;

  /* Oh cursed be the day when I decided to have add_edge_vrtx switch vertices
     in to proper order.... */
  pVx0 = ( const vrtx_struct * ) pVxTop[0] ;
  pVx1 = ( const vrtx_struct * ) pVxTop[2] ;
  if ( !add_edge_vrtx ( pUns->pllAdEdge, ( void ** ) &pUns->pAdEdge,
                        &pVx0, &pVx1, &dir, &newEg ) ) {
    sprintf ( hip_msg, "could not add hanging edge in hex22prisms.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  pVx0 = ( const vrtx_struct * ) pVxBot[0] ;
  pVx1 = ( const vrtx_struct * ) pVxBot[2] ;
  if ( !add_edge_vrtx ( pUns->pllAdEdge, ( void ** ) &pUns->pAdEdge,
                        &pVx0, &pVx1, &dir, &newEg ) ) {
    sprintf ( hip_msg, "could not add hanging edge in hex22prisms.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }



  /* Correct boundary faces. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem == pElem && pBndFc->nFace ) {
        /* This is a boundary face of the hex. Find the list of nodes on the face. */
        kBndFc = pBndFc->nFace ;
        pFoE = elemType[hex].faceOfElem + kBndFc ;
        mVx = pFoE->mVertsFace ;
        for ( iVx = 0 ; iVx < mVx ; iVx++ ) {
          kVx = pFoE->kVxFace[iVx] ;
          pVxBndFc[iVx] = pElem->PPvrtx[kVx] ;
        }

        /* Find out whether that face is one of the split ones. */
        mNewFc = 0 ;
        if ( kBndFc == kFace ) {
          /* That was the easy one. Two new tri faces. */
          mNewFc = 2 ;
          pBndEl[0] = pNewEl[0] ;
          kNewFc[0] = 5 ;
          pBndEl[1] = pNewEl[1] ;
          kNewFc[1] = 5 ;
        }
        else {
          for ( match = 1, nVx = 0 ; nVx < 4 ; nVx++ ) {
            for ( iVx = 0 ; iVx < 4 ; iVx++ )
              if ( pVxBndFc[iVx] == pVxBot[nVx] )
                break ;
            if ( iVx >= 4 ) {
              /* Vertex nVx is not on this face. Try the next face. */
              match = 0 ;
              break ; }
          }
          if ( match ) {
            /* This is the boundary face sought. Create two triangular boundary faces. */
            mNewFc = 2 ;
            pBndEl[0] = pNewEl[0] ;
            kNewFc[0] = 4 ;
            pBndEl[1] = pNewEl[1] ;
            kNewFc[1] = 4 ;
          }
        }
        
                  
        /* If none of the two split faces match, find the face back in one of the
           quad faces of the new elements. */
        for ( iEl = 0 ; iEl < 2 && !mNewFc ; iEl++ )
          for ( kFc = 1 ; kFc <= 5 ; kFc++ ) {
            pFoE = elemType[pri].faceOfElem + kFc ;
            if ( pFoE->mVertsFace == 4 ) {
              /* Number of vertices match. Loop over all vertices in this face. */
              kVxFace = pFoE->kVxFace ;
              for ( match = 1, nVx = 0 ; nVx < 4 ; nVx++ ) {
                for ( iVx = 0 ; iVx < 4 ; iVx++ )
                  if ( pVxBndFc[iVx] == pNewEl[iEl]->PPvrtx[ kVxFace[nVx] ] )
                    break ;
                if ( iVx >= 4 ) {
                  /* Vertex nVx is not on this face. Try the next face. */
                  match = 0 ;
                  break ; }
              }
              if ( match ) {
                /* Create an entry into the sorting structure. */
                mNewFc = 1 ;
                pBndEl[0] = pNewEl[iEl] ;
                kNewFc[0] = kFc ;
                break ;
              }
            }
          }

        if ( !mNewFc && kFc > 5 ) {
          sprintf ( hip_msg, "could not match quad prism face in hex22prisms.\n" ) ;
          hip_err ( fatal, 0, hip_msg ) ; }

        if ( *pnLstBf + mNewFc >= *pmBndFc ) {
          /* Realloc. */
          *pmBndFc = REALLOC_FACTOR*(*pmBndFc)+2 ;
          *ppBndFc = arr_realloc ( "ppBndFc in hex22prisms", pUns->pFam, *ppBndFc,
                                   *pmBndFc, sizeof( **ppBndFc ) ) ;
        }

        for ( iNewFc = 0 ; iNewFc < mNewFc ; iNewFc++ ) {
          pBf = *ppBndFc + ++(*pnLstBf) ;
          pBf->Pbc = pBndFc->Pbc ;
          pBf->Pelem = pBndEl[iNewFc] ;
          pBf->nFace = kNewFc[iNewFc] ;
        }
      }


  /* Mark the parent element as invalid. */
  pElem->invalid = 1 ;
  pElem->number = 0 ;
  pElem->term = 0 ;
#ifdef ADAPT_HIERARCHIC
  pElem->root = pElem->leaf = 0 ;
#endif
  
  return ( 1 ) ;
}


/******************************************************************************

  bcNrCompare:
  Comparison function for qsort to sort for boundary condition numbers.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int bcNrCompare ( const void *Pbs1, const void *Pbs2 ) {
  return ( ( ( bndFc_struct * ) Pbs1 )->Pbc->nr -
           ( ( bndFc_struct * ) Pbs2 )->Pbc->nr ) ;
}



/******************************************************************************

  check_elems:
  Find degenerate elements.
  
  Last update:
  ------------
  21Apr20; replace cutMark with mark[0].
  22Mar18; count elems with negative volumes.
  30Sep16; fix bug with printing empty hip_msg.
  1Jul16; new interface to check_uns.
  9Mar16; return zero also on negative volumes.
  21Apr14; reset volDomain to zero.
  20Dec14; fix bug with using hMinSq returned from get_degenEdges as
           hMin. Remove pChunk->hMin, as not used. 
  4Apr13; modified interface to loop_elems.
          make all large counters ulong_t.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  3Sep12; switch prints to hip_err.
  3Sep12; enter diagnostic block large dihedral angles upon check_lvl
            rather than verbosity test.
  9Jul11; use ADAPT_REF.
  18Jun06; compute local epsOverlap for each pUns.
  14Dec98; list the element with the largest angle.
  4May96: conceived.
  
  Input:
  ------
  pUns:
  mDim:         
  
  Returns:
  --------
  1 on valid grid, 0 otherwise
  
*/

int check_elems ( uns_s *pUns, int *Pchange, const int check_lvl ) {
  
  const elemType_struct *pElT ;
  chunk_struct *pChunk, *pNewChunk = NULL ;
  elem_struct *pElem, *pLstElem, *pElBeg, *pElEnd, *pElMaxDihAngle=NULL ;
  vrtx_struct **ppLstElem2Vert, *pLstVrtx, *pVx ;
  ulong_t mDegenEdges, mFixedElems=0, mUnFixedElems=0, mDegenElems=0 ;
  const int mDim  = pUns->mDim ;
  ulong_t mNewVerts, mNewElems ;
  int kFace, iVxFc ;
  ulong_t mFixedLrgAngles=0, mDgElems=0, mDegenFaces=0, mBndFc, nBndFc ;
  int mBc, nrBc, mFacesThisPatch, isNotColl, foundElem ;
  double hMinSq = TOO_MUCH, hMaxSq = -TOO_MUCH ;
  double *pLstCoor, maxDihAngle, dihAngle, dist, elVol, hMinElem, dihAng, fcAng ;
  bndPatch_struct *pBndPatch, *pBP ;
  bndFc_struct *pBndFc, *pBF ;
  double elemVol ;

  if ( pUns->specialTopo == surf ) {
    /* no elements to be checked, all fine. */
    return ( 1 ) ;
  }
  
  hip_err ( blank, 4, "    Checking unstructured grid for degenerate"
            " edges, angularity." ) ;


  pUns->volElemMin = TOO_MUCH, pUns->volElemMax = -TOO_MUCH ;

  if ( fix_degenElems || dg_fix_lrgAngles ) {
    /* Allocate some memory for the elements to be created. Note that if,
       after counting no degnerate elements are found, pNewChunk=NULL.*/

    /* Count the number of degenerate multiblock faces. Each degenerate face
       will give at most rise to one new element. */
    for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
      mDegenFaces += pChunk->mDegenFaces ;

    mDgElems = 6*mDegenFaces+2*pUns->mCutElems ;
    if ( mDgElems ) {
      /* pNewChunk = append_chunk ( pUns, mDgElems, 6*mDgElems, 0, mDgElems,
         pUns->mCutBndFc, pUns->mBc ) ; */
      /* For the time being, no new boundary faces. */
      pNewChunk = append_chunk ( pUns, pUns->mDim, mDgElems, 6*mDgElems, 10, 
                                 mDgElems, 0, 0 ) ;
      
      /* Make some pointers to the last open slots. */
      pLstElem = pNewChunk->Pelem ;
      pNewChunk->mElemsMarked = 0 ;
      ppLstElem2Vert = pNewChunk->PPvrtx - 1 ;
      pLstVrtx = pNewChunk->Pvrtx ;
      pLstCoor = pNewChunk->Pcoor + mDim - 1 ;
      *Pchange = 1 ;
    }
  }



  /* Find the mesh scales for all chunks. Fix all elements with degenerate
     edges. */
  pUns->volDomain = 0. ;
  ulong_t mNegVolElems = 0 ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    //pChunk->hMin = TOO_MUCH ;
    //pChunk->hMax = -TOO_MUCH ;

    /* Loop over all leaf elements in this chunk. */
    for ( foundElem = 0, pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( pElem->term && !pElem->invalid ) {
        
        foundElem = 1 ;
        mDegenEdges = get_degenEdges ( pElem, pChunk, &hMinSq, &hMaxSq, &dist,
                                       fix_degenElems, pUns->epsOverlapSq ) ;
        elemVol = get_elem_vol ( pElem ) ;
        pUns->volDomain += elemVol ;
        if ( elemVol < 0. )
          mNegVolElems++ ;
        if ( elemVol < pUns->volElemMin ) {
          pUns->pMinElem = pElem ;
          pUns->volElemMin = elemVol ;
        }
        if ( elemVol > pUns->volElemMax ) {
          pUns->pMaxElem = pElem ;
          pUns->volElemMax = elemVol ;
        }

        /* Try to convert the degenerate element to a simplex. */
	if ( mDegenEdges && fix_degenElems ) {
          if ( !pNewChunk )
            hip_err ( fatal, 0, "no degenerate faces listed, but degenerate"
                      "  edges found in check_elems." ) ;
	  else if ( pChunk == pNewChunk ) {
            sprintf ( hip_msg, "new degenerate edges %g (shorter than epsOverlap %g) in\n"
		     "        the new elements to remove degeneracies in check_elems.",
		     sqrt( hMinSq ), pUns->epsOverlap ) ;
            hip_err ( warning, 1, hip_msg ) ;
	    mUnFixedElems++ ;
	  }
	  else if ( !fix_elem ( pElem, mDegenEdges, pChunk, pUns, pNewChunk, 
	            &pLstElem, &ppLstElem2Vert, &pLstVrtx, &pLstCoor ) ) {
              mUnFixedElems++ ;
              hip_err ( warning, 4, "could not fix this degenerate element." ) ;
	    }
	    else
	      mFixedElems++ ;
	}
	else if ( mDegenEdges )
	  mDegenElems++ ;
      }
  }

  pUns->hMin = sqrt( hMinSq ) ;
  pUns->hMax = sqrt( hMaxSq ) ;

  pUns->epsOverlap = .9*pUns->hMin ;
  pUns->epsOverlapSq = pUns->epsOverlap*pUns->epsOverlap ;


  
  if ( check_lvl > 3 ) {
    const int doPrint = 1 ;
    check_angles ( pUns, doPrint ) ;
  }
  
  
  /* Check for face angles. */
  if ( dg_fix_lrgAngles && pNewChunk ) {

    /* Alloc a temporary list of boundary faces to be sorted and a list of
       boundary conditions using the upper bound given in the file header. */
    mBndFc = 99, nBndFc = 0 ;
    pBndFc = arr_malloc ( "pBndFc in check_elems", pUns->pFam,
                          mBndFc, sizeof( *pBndFc ) ) ;
    
    /* Loop over all faces of all terminal elements, calculate maximum face angles. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      if ( pChunk != pNewChunk )
        for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
          if ( elem_has_mark ( pElem, CUT_MARK ) ) {
              //pElem->cutMark ) {
            pElT = elemType + pElem->elType ;
            /* OK, 3-D only, so far. */
            for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
              if ( get_face_lrgstAngle ( pElem, kFace, &iVxFc ) < dg_lrgAngle ) {
                /* This element has a large face angle at kVxFc. */
                if ( hex22prisms ( pUns, pElem, kFace, iVxFc,
                                   pNewChunk, &pLstElem, &ppLstElem2Vert,
                                   &pBndFc, &nBndFc, &mBndFc ) )
                  mFixedLrgAngles++ ;
                break ;
              }
          }
    
    if ( nBndFc > 0 ) {
      /* Shrink pBndFc to the actual size. */
      mBndFc = nBndFc ;
      pBndFc = arr_realloc ( "pBndFc in check_elems", pUns->pFam, pBndFc,
                             mBndFc+ 1, sizeof ( *pBndFc ) ) ;
      pNewChunk->PbndFc = pBndFc ;
      pNewChunk->mBndFaces = mBndFc ;

      /* There are new boundary faces. Sort them for bc and make new patches. Sort
         for bc number. Note that pBndFc starts at 1.*/
      qsort ( pBndFc+1, mBndFc, sizeof ( bndFc_struct ), bcNrCompare ) ;

      /* Count the number of new boundary patches. */
      mBc = 1 ;
      nrBc = pBndFc[1].Pbc->nr ;
      for ( pBF = pBndFc+2 ; pBF <= pBndFc + mBndFc ; pBF++ )
        if ( pBF->Pbc->nr != nrBc ) {
          /* New patch. */
          mBc++ ;
          nrBc = pBF->Pbc->nr ;
        }

      /* Alloc boundary patches. */
      pBndPatch = pNewChunk->PbndPatch =
        arr_malloc ( "PbndPatch in check_elems", pUns->pFam,
                     mBc+1, sizeof( *pNewChunk->PbndPatch ));

      pNewChunk->mBndPatches = mBc ;
     
      /* List the new boundary patches. */
      nrBc = pBndFc[1].Pbc->nr ;
      pBP = pBndPatch + 1 ;
      pBP->PbndFc = pBndFc+1 ;
      pBP->Pbc = pBndFc[1].Pbc ;
      mFacesThisPatch = 0 ;
      for ( pBF = pBndFc+1 ; pBF <= pBndFc + mBndFc ; pBF++ ) {
        if ( pBF->Pbc->nr != nrBc ) {
          /* New patch. Close the old one. */
          pBP->mBndFc = mFacesThisPatch ;
          mFacesThisPatch = 0 ;
          pBP++ ;
          pBP->PbndFc = pBF ;
          pBP->Pbc = pBF->Pbc ;
          nrBc = pBF->Pbc->nr ;
        }
        mFacesThisPatch++ ;
      }

      /* Close the final patch. */
      pBP->mBndFc = mFacesThisPatch ;
    }
    else {
      /* No new boundary faces. */
      arr_free ( pBndFc ) ;
      pNewChunk->mBndPatches = pNewChunk->mBndFaces = 0 ;
    }

    /* Cutmark was stored in the first elem mark.
       release_elem_mark ( pUns, 0 ) ; */

  }
  
  if ( mFixedLrgAngles ) {
    sprintf ( hip_msg, "found %"FMT_ULG" elements with excessively large angles.",
              mFixedLrgAngles ) ;
    hip_err ( info, 2, hip_msg ) ;
  }
  
  /* Count the new vertices and elements in the new chunk. */
  if ( pNewChunk ) {
    /* Vertices. */
    for ( mNewVerts = 0, pVx = pNewChunk->Pvrtx+1 ;
	  pVx <= pNewChunk->Pvrtx + pNewChunk->mVerts ; pVx++ )
      if ( pVx->number )
	mNewVerts++ ;

    /* Elements. */
    for ( mNewElems = 0, pElem = pNewChunk->Pelem+1 ;
	  pElem <= pNewChunk->Pelem + pNewChunk->mElems ; pElem++ )
      if ( pElem->number )
	mNewElems++ ;

    if ( !mNewVerts && !mNewElems ) {
      /* Nothing in this new chunk. Remove it. */
      free_chunk ( pUns, &pNewChunk ) ;
    }
    else if ( !mNewVerts ) {
      arr_free ( pNewChunk->Pvrtx ) ;
      pNewChunk->Pvrtx = NULL ;
      pNewChunk->mVerts = 0 ;
    }
    else if ( !mNewElems ) {
      arr_free ( pNewChunk->Pelem ) ;
      pNewChunk->Pelem = NULL ;
      pNewChunk->mElems = 0 ;
    }
  }

  

  if ( pNewChunk ) {
    if ( pNewChunk->mVerts || pNewChunk->mElems ) {
      sprintf ( hip_msg, "created %"FMT_ULG" new vertices and %"FMT_ULG" new elements"
                " against degeneracy in check_elems.",
                pNewChunk->mVerts, pNewChunk->mElems ) ;
      hip_err ( info, 2, hip_msg ) ; 
    }
  }



  
  if ( mNegVolElems ) {
    sprintf ( hip_msg, "found %"FMT_ULG" elements with negative volumes.", 
              mNegVolElems ) ;
    hip_err ( info, 2, hip_msg ) ; 
  }


  
  if ( mFixedElems ) {
    sprintf ( hip_msg, "fixed %"FMT_ULG" elements against degeneracy.", 
              mFixedElems ) ;
    hip_err ( info, 2, hip_msg ) ; 
  }

  if ( mUnFixedElems ) {
    sprintf ( hip_msg, "could not fix %"FMT_ULG" degenerate elements.", mUnFixedElems ) ;
    hip_err ( warning, 1, hip_msg ) ; 
  }

  if ( mDegenElems ) {
    sprintf ( hip_msg, "grid contains %"FMT_ULG" degenerate elements.", mDegenElems ) ;
    hip_err ( warning, 1, hip_msg ) ; 
  }


  if ( pUns->epsOverlap < pUns->epsOverlap ) {
    sprintf ( hip_msg, "in this grid epsOverlap %g is smaller than global %g.\n"
              "          global value is retained.",
	     pUns->epsOverlap, Grids.epsOverlap ) ;
    hip_err ( warning, 1, hip_msg ) ; 
  }

  if ( mUnFixedElems || pUns->volElemMin < 0. )
    /* Grid seems invalid. */
    return ( 0 ) ;
  else
    return ( 1 ) ;
}

/******************************************************************************
  check_angles:   */

/*! Find largest dihedral and face angles.
 *
 */

/*
  
  Last update:
  ------------  
  2Jun19; exclude invalid elements.
  8Jun18; look at dihedral angles, not max(dihedral, face) angles.
  8Apr16: extracted from check_elems.
  

  Input:
  ------
  pUns
  doPrint: be verbose if true.
    
  Returns:
  --------
  angMax: max dihedral or face angle.
  
*/

double check_angles ( uns_s *pUns, const int doPrint ) {

  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  double elVol ;
  int isNotColl ;
  double hMinElem, scDihAng, scFcAng ;
  elem_struct *pElMaxDihAngle = NULL ;
  double scAngMax = TOO_MUCH, angMax ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->term 
#ifdef ADAPT_HIERARCHIC
           && pElem->leaf
#endif
           && !pElem->invalid ) {
        scDihAng = maxAngle ( pElem, &elVol, &isNotColl, &hMinElem, &scDihAng, &scFcAng ) ;
        angMax = scDihAng ; // test, test
        if ( scDihAng < scAngMax ) {
          scAngMax = scDihAng ;
          pElMaxDihAngle = pElem ;
        }
      }

  if ( scAngMax < -1. )
    angMax = 180.+acos( scAngMax + 2. )/PI*180 ;
  else 
    angMax = acos( scAngMax )/PI*180 ;

  
  if ( doPrint ) {
    sprintf ( hip_msg, "largest dihedral/face angle %5.1f deg., elem %"FMT_ULG".",
              angMax, pElMaxDihAngle->number ) ;
    hip_err ( info, 1, hip_msg ) ;

    if ( verbosity > 3 )
      viz_one_elem_vtk ( "elemMaxDihedralAngle.vtk", pElMaxDihAngle, NULL ) ;
    if ( verbosity > 4 )
      printelco ( pElMaxDihAngle ) ;
  }

  return ( angMax ) ;
}


/******************************************************************************
  comp_vol:   */

/*! compute min and max element volumes.
 */

/*
  
  Last update:
  ------------
  13jul20; exclude invalid elements from vol calc.
  7Apr13; use ulong_t for possibly large integers.
  25Jul11; fix bug with pointer increment of mNegVols.
  9Jul11; check on elem->term rather than elem->leaf if compiling w/o ADAPT_REF.
  3Jul10: extracted from check_vol.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

void comp_vol ( uns_s *pUns, 
                const elem_struct **ppMinElem, const elem_struct **ppMaxElem,
                double *pVolDomain, double *pMinVol, double *pMaxVol, 
                ulong_t *pmNegVols ) {
  
  chunk_struct *pChunk ;
  const elem_struct *pElem ;
  double elemVol, hSqMin=TOO_MUCH, hSqMax=-TOO_MUCH, dist ;

  *pVolDomain = 0. ;
  *pMinVol = TOO_MUCH ; 
  *pMaxVol = -TOO_MUCH ;
  *pmNegVols = 0 ;


  /* Loop over all leaf elements in this chunk. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    for ( pElem = pChunk->Pelem+1 ;
	  pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) 
      if ( !pElem->invalid && 
#ifdef ADAPT_HIERARCHIC
           pElem->leaf
#else
           1
#endif
         ) {
        /* Compute edge lengths. */
        get_degenEdges ( pElem, pChunk, &hSqMin, &hSqMax, &dist, 0, pUns->epsOverlapSq ) ;

        /* Compute element volumes. */
#ifdef ADAPT_HIERARCHIC
	elemVol = drvElem_volume ( pUns, pElem ) ;
#else
        elemVol = get_elem_vol ( pElem ) ;
#endif
	*pVolDomain += elemVol ;
	
	if ( elemVol < 0. ) {
          if ( verbosity > 3 ) {
	    sprintf ( hip_msg, "negative volume %g in element %"FMT_ULG", (%d in chunk %d).\n",
		     elemVol, pElem->number, (int)(pElem - pChunk->Pelem), pChunk->nr ) ;
            hip_err ( warning, 4, hip_msg ) ;
            if ( verbosity > 4 ) printelco ( pElem ) ;
          }
	  (*pmNegVols)++ ;
        }

        if ( elemVol < *pMinVol ) {
          *ppMinElem = pElem ;
          *pMinVol = elemVol ;
        }
        if ( elemVol > *pMaxVol ) {
          *ppMaxElem = pElem ;
          *pMaxVol = elemVol ;
        }
      }

  pUns->volElemMin = *pMinVol ;
  pUns->volElemMax = *pMaxVol ;

  pUns->hMin = sqrt( hSqMin ) ;
  pUns->hMax = sqrt( hSqMax ) ;

  return ;
}

/******************************************************************************

  check_vol:
  Check for negative volumes.
  
  Last update:
  ------------
  7Apr13; use ulong_t for possibly large integers.
  15Dec12; write pMin/MaxElem, volDomain, etc. to cpre_uns.
           call update_h_vol in check_vol.
  19Sep09; track minVol in pUns.
  18May07; list all neg vol elemens on verb > 4
  20Dec01; list smallest/largest element on verbosity > 2
  14Dec98; list the smallest element on verbosity > 4.
  14Jan98: Cut out of check_elems.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void update_h_vol ( uns_s *pUns, ulong_t *pmNegVols ) {
  
  comp_vol ( pUns, &pUns->pMinElem, &pUns->pMaxElem, 
             &pUns->volDomain, &pUns->volElemMin, &pUns->volElemMax, pmNegVols ) ;

  return ;
}


int check_vol ( uns_s *pUns ) {
  
  if ( pUns->specialTopo == surf ) {
    /* no elements to be checked, all fine. */
    return ( 1 ) ;
  }

  ulong_t mNegVols ;
  update_h_vol ( pUns, &mNegVols ) ;

  if ( mNegVols ) {
    sprintf ( hip_msg, "found %"FMT_ULG" elements with negative volumes.\n", 
              mNegVols ) ;
    if ( negVol_abort ) hip_err ( fatal, 0, hip_msg ) ;
    else                hip_err ( warning, 1, hip_msg ) ;
  }

  return ( 1 ) ;
}

/******************************************************************************
  check_bndFc:   */

/*! Check boundary faces. Currently only check is if there are any.
 */

/*
  
  Last update:
  ------------
  17Dec15: conceived.
  

  Input:
  ------
  pUns

  Returns:
  --------
  1 on failure, 0 on success
  
*/

int check_bndFc ( uns_s *pUns ) {

  int mBndFc = 0 ;
  chunk_struct *pCh = NULL ;
  while ( loop_chunks ( pUns, &pCh ) ) {
    mBndFc += pCh->mBndFaces ;
  }

  return ( mBndFc ) ;
}


/******************************************************************************

  get_degenEdges:
  Loop over all elements and find degenerate edges, ie edges that are shorter
  than epsOverlap.
  
  Last update:
  ------------
  21Sep21; allow for pChunk = NULL.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  18Jun06; use local eps.
  1Oct99; Intro pDist.
  : conceived.
  
  Input:
  ------
  Pelem  = the elem to look at
  Pchunk = the chunk this element is with 
  check_pVx: if non-zero: count collapsed edges.


  Changes To:
  -----------
  phSqMin  = min h^2 so far
  phSqMax  = max h^2 so far
  pDist  = min h in this element.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_degenEdges ( const elem_struct *Pelem, const chunk_struct *Pchunk,
                     double *phSqMin, double *phSqMax, double *pDist,
                     const int check_pVx, const double epsOverlapSq ) {
  
  double hSq ;
  int mDegenEdges, kEdge ;
  const elemType_struct *PelT ;
  const edgeOfElem_struct *PEoE ;
  vrtx_struct *Pvrtx1, *Pvrtx2 ;
  
  PelT = elemType + Pelem->elType ;
  PEoE = PelT->edgeOfElem ;
  *pDist = TOO_MUCH ; 
	
  /* Loop over all edges. */
  for ( mDegenEdges = 0, kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ ) {
    Pvrtx1 = Pelem->PPvrtx[ PEoE[kEdge].kVxEdge[0] ] ;
    Pvrtx2 = Pelem->PPvrtx[ PEoE[kEdge].kVxEdge[1] ] ;

    if ( Pvrtx1 != Pvrtx2 ) {
      /* Non-collapsed edge. */
      hSq = sq_distance_dbl ( Pvrtx1->Pcoor, Pvrtx2->Pcoor, PelT->mDim ) ;
      if ( hSq <= epsOverlapSq ) {
        mDegenEdges++ ;
        *pDist = MIN( *pDist, sqrt( hSq ) ) ;
        
        if ( Pchunk && verbosity > 4 ) {
          if ( mDegenEdges == 1 )
            printf ( "    Degenerate Element %"FMT_ULG" in chunk %d:\n",
                     Pelem->number, Pchunk->nr ) ;
          printf   ( "     %d. collapsed edge number %d, from %"FMT_ULG" to %"FMT_ULG".\n",
                     mDegenEdges, kEdge+1,
                     Pvrtx1->number, Pvrtx2->number ) ;
          printvxco ( Pvrtx1, PelT->mDim ) ; printvxco ( Pvrtx2, PelT->mDim ) ;
        }
      }
      else {
        *phSqMin = MIN( *phSqMin, hSq ) ;
        *phSqMax = MAX( *phSqMax, hSq ) ;
      }
    }
    else if ( check_pVx )
      /* Collapsed edge. */
      mDegenEdges++ ;
  }
  
  return ( mDegenEdges ) ;
}
  
  

/******************************************************************************

  set_degenVx:
  Set a pair of degenerate vertex pointers to the same.
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  18Jun06; pass a local value of epsOverlap.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int set_degenVx ( uns_s *pUns ) {
  
  const elemType_struct *PelT ;
  const faceOfElem_struct *PFoE ;
  const edgeOfElem_struct *PEoE ;
  chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  double hSq ;
  vrtx_struct *Pvrtx1, *Pvrtx2 ;
  int mDegenFaces, mDegenEdges, mDegenElems, kEdge, kDgEdge[MAX_EDGES_ELEM],
    kFace, iEg ;
  

  /* Find degenerate edges. */
  for ( mDegenElems = mDegenFaces = 0, Pchunk = pUns->pRootChunk ;
        Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ; Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( 
#ifdef ADAPT_HIERARCHIC
           Pelem->leaf
#else
           1
#endif
&& !Pelem->invalid ) {
        PelT = elemType + Pelem->elType ;
	for ( mDegenEdges = 0, kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ )
	{ PEoE = PelT->edgeOfElem + kEdge ;
	  Pvrtx1 = Pelem->PPvrtx[ PEoE->kVxEdge[0] ] ;
	  Pvrtx2 = Pelem->PPvrtx[ PEoE->kVxEdge[1] ] ;
	  hSq = sq_distance_dbl ( Pvrtx1->Pcoor, Pvrtx2->Pcoor, PelT->mDim ) ;
	  if ( hSq <= pUns->epsOverlapSq ) {
	    mDegenEdges++ ;
	    kDgEdge[kEdge] = 1 ;
	    
	    if ( verbosity > 4 ) {
	      if ( mDegenEdges == 1 )
		printf ( "    Degenerate Element %"FMT_ULG" in chunk %d:\n",
			Pelem->number, Pchunk->nr ) ;
	      printf ( "     %d. collapsed edge number %d, from %"FMT_ULG" to %"FMT_ULG".\n",
		       mDegenEdges, kEdge+1, Pvrtx1->number, Pvrtx2->number ) ;
	      printvxco ( Pvrtx1, PelT->mDim ) ; printvxco ( Pvrtx2, PelT->mDim ) ;
	    }

	    /* Set Pvrtx2 on Pvrtx1 */
	    Pelem->PPvrtx[ PEoE->kVxEdge[1] ] = Pvrtx1 ;
	  }
	  else
	    kDgEdge[kEdge] = 0 ;
	}

	if ( mDegenEdges )
	{ mDegenElems++ ;
	  /* There were degenerate edges. Count the degenerate faces. */
	  for ( kFace = 1 ; kFace <= PelT->mFaces ; kFace++ )
	  { PFoE = PelT->faceOfElem+kFace ;
	    for ( iEg = 0 ; iEg < PFoE->mFcEdges ; iEg++ )
	    { kEdge = PFoE->kFcEdge[iEg] ;
	      if ( kDgEdge[kEdge] )
	      { /* This is a degenerate face. */
		mDegenFaces++ ;
		break ;
	      }
	    }
	  }
	}
      }

  if ( mDegenFaces && verbosity > 0 )
  { printf ( " WARNING: found %d degenerate faces in %d elements.\n",
	     mDegenFaces, mDegenElems ) ;
    if ( fix_degenElems)
      printf ( "        Will try to fix them.\n" ) ;
    else
      printf ( "        Will not fix them.\n" ) ;
  }
  
  pUns->pRootChunk->mDegenFaces += mDegenFaces ;
  return ( 1 ) ;
}


/******************************************************************************

  check_uns:
  Validate an unstructured grid.
  
  Last update:
  ------------
  18Dec19; warning instead of fatal for no boundary faces.
  16Apr18; add description of check levels.
  21Dec16; fix warnings for surface grids.
  19Dec16; by default, number chrono in memory, so call the new number_uns_grid_leafs.
  1Jul16; make check_lvl a parameter.
  9Jan15; remove redundant second call to update_h_vol.
  20Dec14; move to bottom of file for prototyping. 
           Tidy up calculation of hMin, etc. for check-level=0.
           Drop use of calc_hMin, use update_h_vol instead.
  15Dec13; replace x_axis_verts with axis_verts.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  5Apr09; set default variable names. 
  8Dec07; depending on abort_negVol, abort or not on negative vols.
  1Jul05; list number of cells etc.
  11Apr03; run check_vol only on check_lvl option.
  4May96: conceived.
  
  Input:
  ------
  pUns:
  
  Returns:
  --------
  1 unless exiting on error.
  
*/

int check_uns ( uns_s *pUns, const int check_lvl ) {
  
  int change, validElems = 1, validConn = 1, validVol = 1, mVxAxis ;
  ulong_t mNegVols = 0 ;
  
  change =  0 ;

  if ( verbosity < 3 )
    hip_err ( blank, 1, "\n   Checking unstructured grid." ) ;
  else {
    sprintf ( hip_msg, "\n   Checking unstructured grid at check level %d, i.e. checking for",
              check_lvl ) ;
    hip_err ( blank, 1, hip_msg ) ;
    if ( check_lvl == 0 )
      hip_err ( blank, 1, "      edge lengths" ) ;
    if ( check_lvl == 1 )
      hip_err ( blank, 1, "      collapsed edges, angularity" ) ;
    if ( check_lvl < 5 )
      hip_err ( blank, 1, "      element volumes" ) ;
    if ( check_lvl == 5 )
      hip_err ( blank, 1, "      consistent element connectivity (expensive!) and\n"
                          "       boundary setup.\n" ) ;
  }
 

  if ( pUns->specialTopo != surf ) {
  
    if ( check_lvl > 0) {
      if ( !check_bndFc ( pUns ) ) {
        hip_err (warning, 1,  " no boundary faces found in check_uns." ) ;
        // hip_err (fatal, 0,  " no boundary faces found in check_uns." ) ;
      }

      
      /* Check element edge lengths. Look for degenerate elements. */
      if ( !( validElems = check_elems ( pUns, &change, check_lvl ) ) )
        hip_err ( warning, 1, "grid seems invalid due to "
                  "invalid or negative elements in check_uns." ) ;
    }
    else {
      /* Check element edge lengths. Look for degenerate elements. */
      if ( !( validElems = check_vol ( pUns ) ) ) {
        hip_err ( warning, 1, "found negative volumes." ) ;
        if ( negVol_abort ) pUns->validGrid = 0 ;
      }
      pUns->epsOverlap = .9*pUns->hMin ;
      pUns->epsOverlapSq = pUns->epsOverlap*pUns->epsOverlap ;
    }
  }


  /* Grids.epsOverlap is the smallest of all eps on all unstr. meshes. */
  set_grids_eps ( pUns ) ;

  /* Recount and number elements and verts. */
  validate_elem_onPvx ( pUns ) ;
  number_uns_grid_leafs ( pUns ) ;
  


  /* Check the connectivity. */
  int bcGeoType_change ;
  if ( pUns->specialTopo != surf && check_lvl > 4 && !pUns->mBndFcVx && 
       !( validConn = check_conn ( pUns, &bcGeoType_change ) ) )
    hip_err ( warning, 1, "grid seems invalid due to unmatched or duplicated faces." ) ;
  if ( bcGeoType_change ) {
    /* The connectivity test has changed the geoType of matching bc,
       rebuild ppBc. */
    make_uns_ppBc ( pUns ) ;
  }

  /* Get the containing rectangle for the grid. */
  get_uns_box ( pUns ) ;
  
  
  /* Count all boundary faces, link all boundary patches,
     since check_conn might have removed some. */
  if ( pUns->numberedType == invNum )
    /* Some cleanup affected numbering, redo. */
    number_uns_grid_leafs ( pUns ) ;

  if ( pUns->specialTopo != surf && check_lvl > 4 )
    if ( ( check_bnd_setup ( pUns ) ).status != success ) {
    sprintf ( hip_msg, "grid does not have proper boundary setup.\n" ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }

  if ( pUns->specialTopo != surf ) {
    /* Make a list of vertices on axes. */
    mVxAxis = axis_verts ( pUns, pUns->specialTopo ) ;
    if ( mVxAxis && ( pUns->specialTopo >= axiX &&
                      pUns->specialTopo <= axiZ ) )
      pUns->specialTopo = axiX ;
    if ( mVxAxis && verbosity > 2 ) {
      sprintf ( hip_msg, "found %d vertices on the x-Axis"
                " to be treated specially.\n", mVxAxis ) ;
      hip_err ( info, 2, hip_msg ) ;
    }
  }


  /* Do allow negative volumes. It is the user's discretion. */
  if ( validElems && validConn && pUns->validGrid == 1 ) {
    sprintf ( hip_msg, "grid seems valid." ) ;
    hip_err( info, 1, hip_msg ) ;
  }
  else {
    pUns->validGrid = 0 ;
    sprintf ( hip_msg, "grid is invalid.\n" ) ;
    hip_err( warning, 1, hip_msg ) ;
  }


  // JDM: Refactor this with list_grid_info in uns_listNprint.
  // That fun is called in read_uns_hdf after outline read, no pUns yet, lotsa args.
  // Needs a wrapper.
  sprintf ( hip_msg, "grid contains\n"
            "          %"FMT_ULG" cells, \n"
            "          %"FMT_ULG" nodes, \n"
            "          %"FMT_ULG" bnd. faces.\n", 
            pUns->mElemsNumbered, pUns->mVertsNumbered, pUns->mFaceAllBc ) ;
  hip_err ( info, 2, hip_msg ) ;

  sprintf ( hip_msg, "hMin: %g, hMax: %g.", pUns->hMin, pUns->hMax ) ;
  hip_err ( info, 1, hip_msg ) ; 
  
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "element volumes: min = %g, max = %g:", 
              pUns->volElemMin, pUns->volElemMax ) ;
    hip_err ( info, 3, hip_msg ) ;
  }
  else if ( verbosity > 3 ) {
    sprintf ( hip_msg, "smallest element with volume %g:\n", pUns->volElemMin ) ;
    hip_err ( info, 4, hip_msg ) ;
    printelco ( pUns->pMinElem ) ;
    printf ( hip_msg, "largest element with volume %g:\n", pUns->volElemMax ) ;
    hip_err ( info, 4, hip_msg ) ;
    printelco ( pUns->pMaxElem ) ;
  }


  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "total grid volume: %g\n", pUns->volDomain ) ;
    hip_err ( info, 3, hip_msg ) ;
  }


  sprintf ( hip_msg, "\n   Topology: %s", specialTopoString(pUns) ) ;
  hip_err ( blank, 1, hip_msg ) ;
  sprintf ( hip_msg, "\n   Domain" ) ; hip_err ( blank, 1, hip_msg ) ;
  char axDir ;
  double axMin, axMax ;
  if ( pUns->mDim == 3 ) {
    sprintf ( hip_msg, "     min x,y,z:   %16.9e, %16.9e, %16.9e",
              pUns->llBox[0], pUns->llBox[1], pUns->llBox[2] ) ;
    hip_err ( blank, 1, hip_msg ) ;
    sprintf ( hip_msg, "     max x,y,z:   %16.9e, %16.9e, %16.9e",
              pUns->urBox[0], pUns->urBox[1], pUns->urBox[2] ) ;
    hip_err ( blank, 1, hip_msg ) ;

    if ( pUns->specialTopo == axiX ) {
      axDir = 'x' ;
      axMin = pUns->llBox[0] ;
      axMax = pUns->urBox[0] ;
    }
    else if ( pUns->specialTopo == axiY ) {
      axDir = 'y' ;
      axMin = pUns->llBox[1] ;
      axMax = pUns->urBox[1] ;
    }
    else if ( pUns->specialTopo == axiZ ) {
      axDir = 'z' ;
      axMin = pUns->llBox[2] ;
      axMax = pUns->urBox[2] ;
    }
    if ( pUns->specialTopo >= axiX && pUns->specialTopo <= axiZ ) {
      sprintf ( hip_msg, "     min r,th,%c:  %16.9e, %16.9e, %16.9e",
                axDir, pUns->llBoxCyl[0], pUns->llBoxCyl[1], axMin ) ;
      hip_err ( blank, 1, hip_msg ) ;
      sprintf ( hip_msg, "     max r,th,%c:  %16.9e, %16.9e, %16.9e",
                axDir, pUns->urBoxCyl[0], pUns->urBoxCyl[1], axMax ) ;
      hip_err ( blank, 1, hip_msg ) ;
    }
  }
  else {
    sprintf ( hip_msg, "     min x,y: %16.9e, %16.9e", pUns->llBox[0], pUns->llBox[1] ) ;
    hip_err ( blank, 1, hip_msg ) ;
    sprintf ( hip_msg, "     max x,y: %16.9e, %16.9e", pUns->urBox[0], pUns->urBox[1] ) ;
    hip_err ( blank, 1, hip_msg ) ;
  }
  hip_err ( blank, 1, "" ) ;

  
# ifdef DEBUG
  if ( pUns->pUnsFine && pUns->pUnsFine->mVxCollapseTo ) {

    /* Check the inter-mesh pointers. */
    const uns_s *pUnsC = pUns, *pUnsF = pUns->pUnsFine ;
    const vrtx_struct *pVxCoarse = pUnsC->ppChunk[0]->Pvrtx ;
    int iVx, mVxC = pUnsC->mVertsNumbered, nC, *pUsed ;
    
    pUsed = arr_calloc ( "pUsed in check_uns", pUns->pFam, mVxC+1, sizeof( *pUsed ) ) ;

    for ( iVx = 1 ; iVx <= pUnsF->mVxCollapseTo ; iVx++ ) {
      nC = pVxCoarse[ pUnsF->pnVxCollapseTo[iVx] ].number ;
      if ( nC < 1 || nC > mVxC ) {
        sprintf ( hip_msg, "found coarse grid vx for %d pinched off in check_uns.\n", 
                  iVx ) ;
        hip_err ( warning, 2, hip_msg ) ;
      }
      else
        pUsed[nC] = 1 ;
    }

    for ( iVx = 1 ; iVx <= mVxC ; iVx++ )
      if ( !pUsed[iVx] ) {
        sprintf ( hip_msg, "found unreferenced coarse grid vx %d in check_uns.\n", iVx ) ;
        hip_err ( warning, 2, hip_msg ) ;
      }

    arr_free ( pUsed ) ;
  }
# endif



  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  check_valid_uns:
*/
/*! check basic validity of an unstructured grid.
 *
 */

/*
  
  Last update:
  ------------
  14Mar18: extracted from write_hdf5.
  

  Input:
  ------
  pUns = grid
  hasVol = check for existence of volumetric grid.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int check_valid_uns ( uns_s *pUns, const int doCheckVol ) {


  /* Can we actually write this grid to hdf? */
  if ( pUns->pGrid->uns.type != uns )
    hip_err ( fatal, 0, "there is no unstructured grid to write." ) ;


  if ( !pUns->validGrid ) {
    sprintf ( hip_msg, 
              "you were told that this grid is invalid, weren't you?.\n" ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }
  if ( (check_bnd_setup ( pUns )).status != success ) {
    sprintf ( hip_msg, "cannot write grid without proper boundary setup.\n" ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }
  
  if ( doCheckVol ) {
    if ( pUns->specialTopo == surf ) {
      hip_err ( warning, 0, "writing surface grid only to hdf." ) ;
    }
  }
  
  return ( 0 ) ;
}
