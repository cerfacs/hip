/*
  get_edge_weights.c:
  Calculate edge weights for a given unstructured grid. 

  Last update:
  ------------
  4July16; rename eg_s to be egWt_s ;

  4Apr13; reorganise order of functions in file to avoid explicit prototyping.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  29Sep99; remove the useless scalar product switch in get_elem_weights.
  21Jun99; allow for non-lp simiplex grids.

  This file contains:
  -------------------
  make_edge_normals
  add_elem_edge
  get_ewts_elem
  get_ewts_bnd
  add_ewt
  lp_edge
  check_edge_normals_lp

  */

#include "cpre.h"
#include "proto.h"

#include "cpre_uns.h"
#include "proto_uns.h"

extern const Grids_struct Grids ;
extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;
extern const int bitEdge[] ;


/******************************************************************************

  fix_reflex_edges:
  Look for reflex edges, edges that have the same vertex on both ends, and
  average the contributions such that only symmetric parts remain.
  
  Last update:
  ------------
  4Apr13; moved to the top of file to avoid separate prototyping.
  : conceived.
  
  Input:
  ------
  mEdges:   Number and
  pllEdge   List of edges,
  pEdgeNorm The weights
  mDim      The number of dimensions.

  Changes To:
  -----------
  pEdgeNorm
  
*/

void fix_reflex_edges ( const int mEdges, const llEdge_s *pllEdge,
                        double *pEdgeNorm, const int mDim ) {
  int nDim, nEg ;
  vrtx_struct *pVxEg[2] ;
  double *pEN0, *pEN1, avg ;
  
  for ( nEg = 1 ; nEg <= mEdges ; nEg++ )
    if ( show_edge ( pllEdge, nEg, pVxEg, pVxEg+1 ) )
      if ( pVxEg[0] == pVxEg[1] ) {
        /* This is a reflex edge. */
        pEN0 = pEdgeNorm + mDim*2*nEg ;
        pEN1 = pEN0 + mDim ;
        /* Average the quantities. */
        for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
          avg = .5*( pEN0[nDim] + pEN1[nDim] ) ;
          pEN0[nDim] = pEN1[nDim] = avg ;
        }
      }
  
  return ;
}


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %zu for ulong_t types.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
void comb_wt ( int mDim, double *pEgWt, int nEg0, int nEg1, int swap,
               const perBc_s *pPerBc ) {

  double *wt0 = pEgWt+2*mDim*nEg0+mDim, *wt1 = pEgWt+2*mDim*nEg1+mDim, wt[MAX_DIM] ;

  rot_coor_dbl ( wt1, pPerBc->rotOut2in, mDim, wt ) ;
  
  
  if ( swap )
    vec_diff_dbl ( wt0, wt, mDim, wt0 ) ;

  else
    vec_add_dbl ( wt0, wt, mDim, wt0 ) ;

  vec_ini_dbl ( 0., mDim, wt1 ) ;
  wt1[0] = 1.e-100;


  return ;
}







/******************************************************************************
  comb_ewts:   */

/*! Combine antisymmetric edges along periodic boundaries.
 */

/*
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  4Apr13; moved to the top of file to avoid separate prototyping.
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int comb_ewts ( uns_s *pUns, const int mEdges, double *pEdgeWeights ) {

  const vrtx_struct *pVxPer[2] ;
  vrtx_struct *pVx[2] ;
  perVxPair_s vxPair, *pVxPair ;
  int kVx, nEg0, nEg1, sw ;


  /* Mark all periodic vertices on the inlet, l,  side. */
  reset_vx_mark ( pUns ) ;
  for ( kVx = 0 ; kVx < pUns->mPerVxPairs ; kVx++ )
    pUns->pPerVxPair[kVx].In->mark = 1 ;


  /* Loop over all edges. */
  for ( nEg0 = 1 ; nEg0 <= mEdges ; nEg0++ )
    if ( show_edge ( pUns->pllEdge, nEg0, pVx, pVx+1 ) &&
         pVx[0]->mark && pVx[1]->mark ) {
      
      vxPair.In = pVx[0] ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[0] = ( pVxPair ? pVxPair->Out : NULL ) ;

      vxPair.In = pVx[1] ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[1] = ( pVxPair ? pVxPair->Out : NULL ) ;

      if ( !pVxPer[0] || !pVxPer[1] ) {
        sprintf ( hip_msg, "failed to match periodic edge between %"FMT_ULG"-%"FMT_ULG".\n",
                 pVx[0]->number, pVx[1]->number ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

      
      nEg1 = get_edge_vrtx ( pUns->pllEdge, pVxPer, pVxPer+1, &sw ) ;

      if ( nEg0 < nEg1 )
        comb_wt ( pUns->mDim, pEdgeWeights, nEg0, nEg1, sw, pUns->pPerBc ) ;
    }

  return ( 1 ) ;
}



/******************************************************************************

  ewt_to_symmAntim:
  Convert the set of edgeweights that is given as contribution to 1st from 2nd
  and vice versa into a list of symmetric and antimetric edges. Return the
  count of symmetric and antimetric edges.
  
  Last update:
  ------------
  4Apr13; moved to the top of file to avoid separate prototyping.
  4Apr13; make all large counters ulong_t.
  1Oct99; intro egWtCutOff ;
  3Aug99; count all elemetal edges as anti symmetric.
  : conceived.
  
  Input:
  ------
  

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

  
void ewt_to_symmAntim ( uns_s *pUns,
                        const ulong_t mEdges, double *pEdgeWeights, const int mDim,
                        ulong_t *pmSymmEg, ulong_t *pmAntimEg ) {

  ulong_t nEg ;
  int nDim, foundSymm, foundAntim ;
  double *pEN1, *pEN2, symm, antim ;

  *pmAntimEg = *pmSymmEg = 0 ;

  for ( nEg = 1 ; nEg <= mEdges ; nEg++ ) {
    pEN1 = pEdgeWeights + 2*mDim*nEg ;
    pEN2 = pEN1 + mDim ;
    
    for ( foundSymm = foundAntim = nDim = 0 ; nDim < mDim ; nDim++ ) {
      symm  = pEN1[nDim] + pEN2[nDim] ;
      antim = pEN1[nDim] - pEN2[nDim] ;

      if ( ABS( symm ) > Grids.egWtCutOff ) {
	pEN1[nDim] = symm ;
	foundSymm = 1 ; }
      else
	pEN1[nDim] = 0. ;

      if ( ABS( antim ) > Grids.egWtCutOff ) {
	pEN2[nDim] = antim ;
	foundAntim = 1 ;
      }
      else
	pEN2[nDim] = 0. ;
    }
    if ( foundSymm  ) ++(*pmSymmEg  ) ;
    if ( foundAntim ) ++(*pmAntimEg ) ;
  }

  return ;
}



/******************************************************************************

  check_vx_lp:
  Check whether a vertex is lp given a set of one-sided weights.
  
  Last update:
  ------------
  16Sep98; redefine vol as div( coor ). 
  : conceived.
  
  Input:
  ------
  pUns:       A grid with a valid edge-list pUns->pllEdge
  pEdgeNorm:  and its list of edge normals.
  pVx:        The vertex to be tested.

  Returns:
  --------
  1 if the node is LP, 0 otherwise.
  
*/

int check_vx_lp ( uns_s *pUns, const double *pEdgeNorm, const vrtx_struct *pVx ) {

  const int mDim = pUns->mDim ;
  int nDim, nEg, n1Eg, side, nD, isLP ;
  const double *pEgN1, *pEgN2 ;
  double grad[MAX_DIM][MAX_DIM], egC[MAX_DIM][MAX_DIM] = {0}, vol, diagErr, offDiagErr ;
  vrtx_struct *pVx1, *pVx2 ;

  for ( nDim = 0 ; nDim < MAX_DIM ; nDim++ )
    grad[0][nDim]= grad[1][nDim]= grad[2][nDim]= 0.0 ;
  
  /* Gather the gradient, collecting all one-sided contributions. */
  nEg = 0 ;
  while ( loop_edge_vx ( pUns->pllEdge, pVx, &n1Eg, &nEg, &side ) ) {
    show_edge ( pUns->pllEdge, nEg, &pVx1, &pVx2 ) ;
    pEgN1 = pEdgeNorm + 2*mDim*nEg ;
    pEgN2 = pEgN1 + mDim ;

    if ( pVx1 == pVx ) 
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        for ( nD = 0 ; nD < mDim ; nD++ )
          grad[nD][nDim] += pVx2->Pcoor[nD]*pEgN1[nDim] ;

    if ( pVx2 == pVx ) 
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        for ( nD = 0 ; nD < mDim ; nD++ )
          grad[nD][nDim] += pVx1->Pcoor[nD]*pEgN2[nDim] ;
  }

  /* Test whether the calculated gradient is LP. */
  if ( mDim == 2 ) {
    vol = ( grad[0][0] + grad[1][1] )/2  ;
    diagErr = MAX( ABS( vol-grad[0][0] ), ABS( vol-grad[1][1] ) ) ;
    offDiagErr = MAX( ABS( grad[0][1] ), ABS( grad[1][0] ) ) ;
  }
  else {
    vol = ( grad[0][0] + grad[1][1] + grad[2][2] )/3  ;
    diagErr = MAX( ABS( vol-grad[0][0] ), ABS( vol-grad[1][1] ) ) ;
    diagErr = MAX( diagErr, ABS( vol-grad[2][2] ) ) ;
    offDiagErr = -TOO_MUCH ;
    for ( nDim = 0 ; nDim < 2 ; nDim++ )
      for ( nD = nDim+1 ; nD < 3 ; nD++ ) {
        offDiagErr = MAX( offDiagErr, ABS( grad[nDim][nD] ) ) ;
        offDiagErr = MAX( offDiagErr, ABS( grad[nD][nDim] ) ) ;
      }
  }
  diagErr /= vol ;
  offDiagErr /= vol ;

  isLP = ( diagErr > Grids.lp_tolerance || offDiagErr > Grids.lp_tolerance ? 0 : 1 ) ;
  if ( ( isLP && verbosity < 9 ) || verbosity < 7 )
    return ( isLP ) ;
  
  else if ( ( !isLP && verbosity > 6 ) || verbosity > 8 ) {
    /* Print the coordinate jacobian. */
    printf ( " vertex: %"FMT_ULG":               diagErr %+10.5e, offDiagErr %+10.5e\n",
             pVx->number, diagErr, offDiagErr ) ;
    if ( verbosity <= 7 )
      return ( 0 ) ;
  
    if ( mDim == 2 )
      printf ( "  Jacobian:         x: %+10.5e, %+10.5e \n"
               "                    y: %+10.5e, %+10.5e \n", 
               grad[0][0], grad[0][1],
               grad[1][0], grad[1][1] ) ;
    else
      printf ( "  Jacobian:         x: %+10.5e, %+10.5e %+10.5e\n"
               "                    y: %+10.5e, %+10.5e %+10.5e\n"
               "                    z: %+10.5e, %+10.5e %+10.5e\n",
               grad[0][0], grad[0][1], grad[0][2],
               grad[1][0], grad[1][1], grad[1][2],
               grad[2][0], grad[2][1], grad[2][2] ) ;
  }

  if ( isLP || verbosity < 9 )
    return ( isLP ) ;


  
  /* Print edge-wise contributions. */
  nEg = 0 ;
  while ( loop_edge_vx ( pUns->pllEdge, pVx, &n1Eg, &nEg, &side ) ) {
    show_edge ( pUns->pllEdge, nEg, &pVx1, &pVx2 ) ;
    pEgN1 = pEdgeNorm + 2*mDim*nEg ;
    pEgN2 = pEgN1 + mDim ;
    
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      egC[0][nDim] = egC[1][nDim] = egC[2][nDim] = 0.0 ;

    if ( pVx1 == pVx ) 
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        for ( nD = 0 ; nD < mDim ; nD++ )
          egC[nD][nDim]  += pVx2->Pcoor[nD]*pEgN1[nDim] ;

    if ( pVx2 == pVx ) 
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
        for ( nD = 0 ; nD < mDim ; nD++ )
          egC[nD][nDim]  += pVx1->Pcoor[nD]*pEgN2[nDim] ;

    if ( mDim == 2 ) 
      printf ( "  %6d:, %3"FMT_ULG"-%3"FMT_ULG": x: %+10.5e, %+10.5e y: %+10.5e, %+10.5e\n", 
               nEg, pVx1->number, pVx2->number,
               egC[0][0], egC[0][1], egC[1][0], egC[1][1] ) ;
    else
      printf ( "  %6d:, %3"FMT_ULG"-%3"FMT_ULG": x: %+10.5e, %+10.5e %+10.5e\n"
               "                    y: %+10.5e, %+10.5e %+10.5e\n"
               "                    z: %+10.5e, %+10.5e %+10.5e\n", 
               nEg, pVx1->number, pVx2->number,
               egC[0][0], egC[0][1], egC[0][2],
               egC[1][0], egC[1][1], egC[1][2],
               egC[2][0], egC[2][1], egC[2][2] ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************

  check_edge_normals_lp:
  .
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  : conceived.
  
  Input:
  ------
  pUns:       A grid with a valid edge-list pUns->pllEdge
  pEdgeNorm:  and its list of edge normals.

  Changes To:
  -----------
  pVrtx->mark: Set to 1 for non-LP nodes.
  
  Returns:
  --------
  The number of non-lp nodes.
  
*/

int check_edge_normals_lp ( uns_s *pUns, const double *pEdgeNorm ) {

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int mNonLpVx = 0, nVxBeg, nVxEnd ;

  /* Reset all marks of vertices. f*/
  reset_vx_mark ( pUns ) ;

  /* Loop over all active vertices. */
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number )
        if ( !check_vx_lp ( pUns, pEdgeNorm, pVx ) ) {
          /* This one is not LP. */
          mNonLpVx++ ;
          pVx->mark = 1 ;
        }
            
  return ( mNonLpVx ) ;
}

/******************************************************************************

  add_ewt:
  Add a weight to one side of an edge, split the weights into antimetric and
  symmetric such as w1 = 1/2(w1-w2) + 1/2(w1+w2).
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  oneWeight[]: weight to be added
  side:        0 or 1, depending on whether 1st or 2nd vertex of the edge.
  factor:      multiplying oneWeight
  pEN:         storage of the weights for that edge, mDim*antim., mDim*symm.

  Changes To:
  -----------
  pEN[]:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void add_ewt ( const double oneWeight[], const double factor,
               const int side, const int mDim,
               double *pEdgeWeight, const int nEdge ) {

  static int nDim ;
  static double *pEN ;
  pEN = pEdgeWeight + mDim*( 2*nEdge + side ) ;
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    pEN[nDim] += factor*oneWeight[nDim] ;
  
  return ;
}


/********************************************C*********************************

  lp_edge:
  Correct a simple half edge with all the contributions to the median dual
  half edge, such that it is linearity perserving.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem        The element
  kEdge        and the face of interest,
  pllEdge      the list of edges,
  ppEdgeNorm   the list of weights.
  
  Changes To:
  -----------
  pElem->markdEdges: mark the edge as LP.
  pllEdge      possible reallocation
  ppEdgeNorm   corrected edge weights

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int lp_edge ( elem_struct *pElem, const int kEdge,
 	      llEdge_s *pllEdge, double **ppEdgeNorm ) {
  
  static const elemType_struct *pElT ;
  static const edgeOfElem_struct *pEoE ;
  static const faceOfElem_struct *pFoE ;
  static const vrtx_struct *pVrtx1, *pVrtx2, *pVxEl[MAX_VX_ELEM], *pVxFc[MAX_VX_FACE],
    *pVxEg[2] ;
  static const int *kVxEdge ;
  static int kVx, iVx, mDim, kFace, failure, iFct, mVxEl, mVxFc,
    nVxEg[2][MAX_VX_ELEM], sideVxEg[2][MAX_VX_ELEM], nElEg, sideElEg,
    nFcEg[2][MAX_VX_FACE], sideFcEg[2][MAX_VX_FACE], nRxEg[2], rxSide, newEg ;
  static double surfNorm[MAX_DIM], elemGC[MAX_DIM], edgeGC[MAX_DIM], edgeVec[MAX_DIM],
    faceGC[MAX_DIM], elem2FaceGC[MAX_DIM], elem2EdgeGC[MAX_DIM], avg ;

  if ( pElem->markdEdges & bitEdge[kEdge] )
    /* This edge is already treated. */
    return ( 1 ) ;
  
  /* Mark the egde as been treated lp. */
  pElem->markdEdges |= bitEdge[kEdge] ;

  /* Gravity center, etc. of the element. */
  elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;
  mDim = pElT->mDim ;

  pEoE = pElT->edgeOfElem + kEdge ;
  kVxEdge = pEoE->kVxEdge ;

  /* The weighted edge. */
  pVrtx1 = pVxEg[0] = pElem->PPvrtx[ kVxEdge[0] ] ;
  pVrtx2 = pVxEg[1] = pElem->PPvrtx[ kVxEdge[1] ] ;

  if ( pVrtx1 == pVrtx2 )
    /* Collapsed edge. Nothing to be done. */
    return ( 1 ) ;

  /* Add the edge. */
  nElEg = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                          &pVrtx1, &pVrtx2, &sideElEg, &newEg ) ;

  /* Add reflex edges. */
  for ( iVx = failure = 0 ; iVx < 2 ; iVx++ ) {
    nRxEg[iVx] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                 pVxEg+iVx, pVxEg+iVx, &rxSide, &newEg ) ;
    if ( !nRxEg[iVx] ) failure = 1 ;
  }


  /* Find all vertices connected to both end vertices of the edge. */
  for ( iVx = 0 ; iVx < 2 ; iVx++ )
    for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
      pVrtx1 = pVxEg[iVx] ;
      pVrtx2 = pVxEl[kVx] ;
      nVxEg[iVx][kVx] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                        &pVrtx1, &pVrtx2, sideVxEg[iVx]+kVx, &newEg ) ;
      if ( !nVxEg[iVx][kVx] ) failure = 1 ;
    }

  if ( !nElEg || failure ) {
    printf ( " FATAL: could not add edges in lp_edge.\n" ) ;
    return ( 0 ) ; }
  
  if ( pElem->elType == tri || pElem->elType == qua ) {
    /* Have surfNorm point into the direction of the elemental edge. */
    pElT = elemType + pElem->elType ;
    med_normal_edge_2D ( pElem, elemGC, kEdge, surfNorm ) ;
    
    /* Undo the non LP lumped treatment. */
    /* Subtract from the 0th vertex. */
    add_ewt ( surfNorm, -.5,       sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
    add_ewt ( surfNorm, -.5,              0, mDim, *ppEdgeNorm, nVxEg[0][0] ) ;
    /* Subtract from the 1st vertex. */
    add_ewt ( surfNorm,  .5,     1-sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
    add_ewt ( surfNorm,  .5,              0, mDim, *ppEdgeNorm, nVxEg[1][0] ) ;
      
    /* Distribute LP weights. */
    /* Edge average. */
    add_ewt ( surfNorm,  .25,       sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
    add_ewt ( surfNorm,  .25,              0, mDim, *ppEdgeNorm, nVxEg[0][0] ) ;
    add_ewt ( surfNorm, -.25,     1-sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
    add_ewt ( surfNorm, -.25,              0, mDim, *ppEdgeNorm, nVxEg[1][0] ) ;
      
    /* Cell average. */
    avg = 1./pElT->mVerts/2 ;
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
      add_ewt ( surfNorm,  avg, sideVxEg[0][kVx], mDim, *ppEdgeNorm, nVxEg[0][kVx] ) ;
      add_ewt ( surfNorm, -avg, sideVxEg[1][kVx], mDim, *ppEdgeNorm, nVxEg[1][kVx] ) ;
    }
  }

  else {
    /* Generic 3D. In 3D there are two triangular facets to each edge of an element. */
    edge_grav_ctr ( pElem, kEdge, edgeGC ) ;
    /* Vector from the center of the edge to the elements gravity center. */
    vec_diff_dbl ( edgeGC, elemGC, 3, elem2EdgeGC ) ;
    /* Vector along the geometric edge. */
    vec_diff_dbl ( pElem->PPvrtx[ kVxEdge[1] ]->Pcoor,
                   pElem->PPvrtx[ kVxEdge[0] ]->Pcoor, 3, edgeVec ) ;
    
    /* Calculate the areas of the two triangular facets of the median dual. */
    for ( iFct = 0 ; iFct < 2 ; iFct++ ) {
      kFace = pEoE->kFcEdge[iFct] ;
      face_grav_ctr ( pElem, kFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;

      if ( mVxFc > 2 ) {
        /* This is a non-collapsed face. Add some weights. */
        vec_diff_dbl ( faceGC, elemGC, 3, elem2FaceGC ) ;


        cross_prod_dbl ( elem2FaceGC, elem2EdgeGC, 3, surfNorm ) ;
    
        /* Find out the orientation of the geometric edge via scalar product. */
        if ( scal_prod_dbl ( edgeVec, surfNorm, 3 ) < 0. )
          /* The facet normal runs opposite to the geometric edge. Note that the cross
             product is twice the triangular area. */
          vec_mult_dbl ( surfNorm, -.5, 3 ) ;
        else                     
          vec_mult_dbl ( surfNorm,  .5, 3 ) ;

        /* Make an unordered list of edges on the face. */
        for ( iVx = 0 ; iVx < 2 ; iVx++ )
          for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
            pVrtx1 = pElem->PPvrtx[ kVxEdge[ iVx ] ] ;
            pVrtx2 = pVxFc[kVx] ;
            nFcEg[iVx][kVx] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                              &pVrtx1, &pVrtx2,
                                              sideFcEg[iVx]+kVx, &newEg ) ;
          }


        /* Undo the non LP lumped treatment. */
        /* Subtract from the 0th vertex. */
        add_ewt ( surfNorm, -.5,   sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
        add_ewt ( surfNorm, -.5,          0, mDim, *ppEdgeNorm, nRxEg[0] ) ;
        /* Subtract from the 1st vertex. */
        add_ewt ( surfNorm,  .5, 1-sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
        add_ewt ( surfNorm,  .5,          0, mDim, *ppEdgeNorm, nRxEg[1] ) ;

        
        /* Add LP weights. The three corners of the facet carry a one third average of
           the edge's, the face centroid's and the element centroid's value. */
        /* Edge. */
        avg = 1./3/2 ;
        add_ewt ( surfNorm,  avg,       sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
        add_ewt ( surfNorm,  avg,              0, mDim, *ppEdgeNorm, nRxEg[0] ) ;
        add_ewt ( surfNorm, -avg,     1-sideElEg, mDim, *ppEdgeNorm, nElEg ) ;
        add_ewt ( surfNorm, -avg,              0, mDim, *ppEdgeNorm, nRxEg[1] ) ;
        /* Face centroid. */
        avg = 1./3/mVxFc ;
        for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
          add_ewt (  surfNorm,  avg, sideFcEg[0][kVx], mDim, *ppEdgeNorm, nFcEg[0][kVx]);
          add_ewt (  surfNorm, -avg, sideFcEg[1][kVx], mDim, *ppEdgeNorm, nFcEg[1][kVx]);
        }                                                                              
        /* Element centroid. */                                                        
        avg = 1./3/mVxEl ;                                                             
        for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
          add_ewt (  surfNorm,  avg, sideVxEg[0][kVx], mDim, *ppEdgeNorm, nVxEg[0][kVx]);
          add_ewt (  surfNorm, -avg, sideVxEg[1][kVx], mDim, *ppEdgeNorm, nVxEg[1][kVx]);
        }
      }
    }
  }
  return ( 1 ) ;
}


/******************************************************************************

  lp_bnd_edge:
  Given an edge on a boundary face, turn it into lp.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge:    The list of edges,
  ppEdgeNorm: the list of normals
  pElem:      the element,
  kFace:      the boundary face,
  iEg:        the edge in the face, 0 corresp. to an edge running from kVxFace[0]->[1].

  Changes To:
  -----------
  pllEdge
  ppEdgeNorm
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int lp_bnd_edge ( bndFc_struct *pBndFc, const int iEg,
                  llEdge_s *pllEdge, double **ppEdgeNorm ) {

  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static const elem_struct *pElem ;
  static int kEdge, side0, iVx, someSide, mTimes, mDim, mVx, kVx, kFace, mVxFc,
    nElEg, nEgEg, nRxEg, nVxEg[MAX_VX_FACE], side[MAX_VX_FACE], sideRx, sideEg, newEg ;
  static double bndNorm[MAX_DIM], edgeVec[MAX_DIM], faceGC[MAX_DIM],
    vx2faceGC[MAX_DIM], avg ;
  static const vrtx_struct *pVrtx1, *pVrtx2, *pVxFc[MAX_VX_FACE], *pVxEg[2] ;

  pElem = pBndFc->Pelem ;
  kFace = pBndFc->nFace ;
  pElT = elemType + pElem->elType  ;
  pFoE = pElT->faceOfElem + kFace ;
  mDim = pElT->mDim ;

  if ( mDim == 2 ) {
    if ( pBndFc->markdEdges )
      /* Edge already fixed. */
      return ( 1 ) ;
    else
      pBndFc->markdEdges = 1 ;
    
    /* Simplified 2D treatment. */
    kEdge = pFoE->kFcEdge[0] ;

    /* The edge along the boundary. side0 refers to the orientation of the
       geometric edge, not the weighted edge. */ 
    nElEg = add_elem_edge ( pllEdge, (void**) ppEdgeNorm, pElem, kEdge,
                            &pVrtx1, &pVrtx2, &side0, &newEg ) ;
  
    /* The two vertices. Switch the two sides if the linked edge runs opposite. */
    nVxEg[   side0 ] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                       &pVrtx1, &pVrtx1, &someSide, &newEg ) ;
    nVxEg[ 1-side0 ] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                       &pVrtx2, &pVrtx2, &someSide, &newEg ) ;
  
    /* Boundary normal. */
    uns_face_normal ( pElem, kFace, bndNorm, &mTimes ) ;
    vec_mult_dbl ( bndNorm, .5*mTimes, mDim ) ;

    /* Subtract non-lp normals. */
    add_ewt (  bndNorm, -1., 0, mDim, *ppEdgeNorm, nVxEg[0] ) ;
    add_ewt (  bndNorm, -1., 0, mDim, *ppEdgeNorm, nVxEg[1] ) ;

    /* Add normals. */
    add_ewt (  bndNorm, 1./4, 0, mDim, *ppEdgeNorm, nElEg ) ;
    add_ewt (  bndNorm, 1./4, 1, mDim, *ppEdgeNorm, nElEg ) ;
    add_ewt (  bndNorm, 3./4, 0, mDim, *ppEdgeNorm, nVxEg[0] ) ;
    add_ewt (  bndNorm, 3./4, 0, mDim, *ppEdgeNorm, nVxEg[1] ) ;
  }

  else {
    if ( pBndFc->markdEdges & bitEdge[iEg] )
      /* Edge already fixed. */
      return ( 1 ) ;
    else
      pBndFc->markdEdges |= bitEdge[iEg] ;
    
    mVx = pFoE->mVertsFace ;
    pVxEg[0] = pElem->PPvrtx[ pFoE->kVxFace[ iEg        ] ] ;
    pVxEg[1] = pElem->PPvrtx[ pFoE->kVxFace[(iEg+1)%mVx ] ] ;

    if ( pVxEg[0] == pVxEg[1] )
      /* Collapsed edge, nothing to be done. */
      return ( 1 ) ;

    /* Add the edge itself. */
    pVrtx1 = pVxEg[0] ;
    pVrtx2 = pVxEg[1] ;
    nEgEg = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                            &pVrtx1, &pVrtx2, &sideEg, &newEg ) ;

    /* Treat triangular and quadrilateral faces generically by splitting the
       quadrilateral facet of the median dual into triangles along each edge. */
    face_grav_ctr ( pElem, kFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;

    if ( mVxFc > 2 ) {
      /* Non-collapsed face. Vector along the edge. */
      vec_diff_dbl ( pVxEg[1]->Pcoor, pVxEg[0]->Pcoor, 3, edgeVec ) ;
      /* Vector from the vertex to the face centroid. */
      vec_diff_dbl ( faceGC, pVrtx1->Pcoor, 3, vx2faceGC ) ;
      
      /* The outward normal of the forward/backward triangular facet on iFct=0/1. */
      cross_prod_dbl ( edgeVec, vx2faceGC, 3, bndNorm ) ;
      /* Note that the backward edge vector points towards iEg, no sign change
         necessary. Note also that edgeVec is normalized to the entire edge length,
         the area along the edge adjacent to each node is formed with a quarter of it. */
      vec_mult_dbl ( bndNorm, .25, mDim ) ;
      
      for ( iVx = 0 ; iVx < 2 ; iVx++ ) {
        /* Add the reflex edge. */
        pVrtx1 = pVrtx2 = pVxEg[iVx] ;
        nRxEg = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                &pVrtx1, &pVrtx2, &sideRx, &newEg ) ;
        
        /* Add all possible edges on the face. */
        for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
          pVrtx1 = pVxEg[iVx] ;
          pVrtx2 = pVxFc[kVx] ;
          nVxEg[kVx] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                       &pVrtx1, &pVrtx2, side+kVx, &newEg ) ;
        }
        
        /* Remove non-lp weights. */
        add_ewt (  bndNorm, -1., 0, mDim, *ppEdgeNorm, nRxEg ) ;
        
        /* The three corners of the facet carry a one third average of
           the vertex's value, the centroid value and the edge average. */
        /* Vertex. */
        add_ewt (  bndNorm, 1./3, 0, mDim, *ppEdgeNorm, nRxEg ) ;
        /* Centroid. */
        avg = 1./3/mVxFc ;
        for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
          add_ewt (  bndNorm, avg, side[kVx], mDim, *ppEdgeNorm, nVxEg[kVx] ) ;
        /* Edge. */
        someSide = ( iVx ? 1-sideEg : sideEg ) ;
        add_ewt (  bndNorm, 1./6,        0, mDim, *ppEdgeNorm, nRxEg ) ;
        add_ewt (  bndNorm, 1./6, someSide, mDim, *ppEdgeNorm, nEgEg ) ;
      }
    }
  }

  return ( 1 ) ;
}

/******************************************************************************

  get_ewts_elem:
  Calculate the complete set of edgeweights for an element. Treat triangles
  fully LP, quads only with the exterior edges.
  
  Last update:
  ------------
  29Sep99; what was that stupid scalar product switch for?
  : conceived.
  
  Input:
  ------
  pllEdge      list of edges
  ppEdgeNorm   list of edge weights.
  pElem        the element

  Changes To:
  -----------
  pllEdge      add the edges of the element, if not present
  ppEdgeNorm:  possible realloc, accumulate the weights of the edges.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_ewts_elem ( llEdge_s *pllEdge, double **ppEdgeNorm,
		    elem_struct *pElem ) {
  
  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static const edgeOfElem_struct *pEoE ;
  static const int *kVxEdge ;
  static const vrtx_struct *pVx[MAX_EDGES_ELEM][2], *pVrtx, *pVx0, *pVx1,
    *pVxEl[MAX_VX_ELEM], *pVxFc[MAX_VX_FACE] ;
  static int kEdge, nElEg[MAX_EDGES_ELEM], side[MAX_EDGES_ELEM], nVxEg[MAX_VX_ELEM],  
    kFace, kVx, someSide, mDim, iFc, mVxEl, mVxFc[MAX_FACES_ELEM+1], newEg ;
  static double surfNorm[2][MAX_DIM], elemGC[MAX_DIM], faceGC[MAX_DIM], edgeGC[MAX_DIM],
    elem2EdgeGC[MAX_DIM], elem2FaceGC[MAX_FACES_ELEM+1][MAX_DIM] ;


  pElT = elemType + pElem->elType ;
  mDim = pElT->mDim ;

  /* Initally, all edges are treated non-LP. */
  pElem->markdEdges = 0 ;

  /* Find/add all edges of the element. */
  for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
    if ( !( nElEg[kEdge] =
            add_elem_edge ( pllEdge, (void**) ppEdgeNorm, pElem, kEdge,
                            pVx[kEdge], pVx[kEdge]+1, side+kEdge, &newEg ) ) ) {
      printf ( " FATAL: could not add edge in get_ewts_elem.\n" ) ;
      return ( 0 ) ; }

  /* Find/add all edges of the vertices of the element. */
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ ) {
    pVrtx = pElem->PPvrtx[kVx] ;
    if ( !( nVxEg[kVx] =
	    add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
			    &pVrtx, &pVrtx, &someSide, &newEg ) ) ) {
      printf ( " FATAL: could not add vrtx edge in get_ewts_elem.\n" ) ;
      return ( 0 ) ; }
  }

  if ( pElem->elType == tri || pElem->elType == qua ) {
    /* Basic median dual. */
    elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;
    for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
      kVxEdge = pElT->edgeOfElem[kEdge].kVxEdge ;
      /* Have surfNorm point away from the 0th vertex on the geometric edge. */
      med_normal_edge_2D ( pElem, elemGC, kEdge, surfNorm[0] ) ;
      
      /* Add to the 0th vertex. */
      add_ewt ( surfNorm[0], .5,   side[kEdge], mDim, *ppEdgeNorm, nElEg[kEdge] ) ;
      add_ewt ( surfNorm[0], .5,             0, mDim, *ppEdgeNorm, nVxEg[kVxEdge[0]] ) ;
      /* Add to the 1st vertex. */
      add_ewt ( surfNorm[0],-.5, 1-side[kEdge], mDim, *ppEdgeNorm, nElEg[kEdge] ) ;
      add_ewt ( surfNorm[0],-.5,             0, mDim, *ppEdgeNorm, nVxEg[kVxEdge[1]] ) ;
    }
  }

  else {
    /* 3D: Loop over all faces, calculate the face gravity center and
       the median dual surface attached to each edge of the face. */
    
    elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;
    for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
      face_grav_ctr ( pElem, kFace, faceGC, &pFoE, mVxFc+kFace, pVxFc ) ;
      vec_diff_dbl ( faceGC, elemGC, 3, elem2FaceGC[kFace] ) ;
    }

    /* Loop over all edges. */
    for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
      pEoE = pElT->edgeOfElem + kEdge ;
      kVxEdge = pEoE->kVxEdge ;
      pVx0 = pElem->PPvrtx[ kVxEdge[0] ] ;
      pVx1 = pElem->PPvrtx[ kVxEdge[1] ] ;
      
      if ( pVx0 != pVx1 ) {
        /* Non-collapsed edge. */
        edge_grav_ctr ( pElem, kEdge, edgeGC ) ;
        /* Vector from the center of the edge to the gravity center of the element. */
        vec_diff_dbl ( edgeGC, elemGC, 3, elem2EdgeGC ) ;

        /* Calculate the areas of the two triangular facets of the median dual. */
        for ( iFc = 0 ; iFc < 2 ; iFc++ ) {
          kFace = pEoE->kFcEdge[iFc] ;
          if ( mVxFc[kFace] > 2 )
            /* Non-collapsed face. */
            cross_prod_dbl ( elem2FaceGC[kFace], elem2EdgeGC, 3, surfNorm[iFc] ) ;
          else
            vec_ini_dbl ( 0., 3, surfNorm[iFc] ) ;
        }
        /* The two parts have opposite directions. Add them reversing one. */
        vec_diff_dbl ( surfNorm[0], surfNorm[1], 3, surfNorm[0] ) ;


        /* Add to the 0th vertex. Note that the cross product is twice the
           triangular area, each vertex contributes half to the edge average. */
        add_ewt ( surfNorm[0], .25,   side[kEdge], 3, *ppEdgeNorm, nElEg[kEdge] ) ;
        add_ewt ( surfNorm[0], .25,             0, 3, *ppEdgeNorm, nVxEg[kVxEdge[0]] );
        /* Add to the 1st vertex. */
        add_ewt ( surfNorm[0],-.25, 1-side[kEdge], 3, *ppEdgeNorm, nElEg[kEdge] ) ;
        add_ewt ( surfNorm[0],-.25,             0, 3, *ppEdgeNorm, nVxEg[kVxEdge[1]] );
      }
    }
  }
  
  return ( 1 ) ;
}


/******************************************************************************

  get_ewts_bnd:
  Add symmetric terms for the boundary edges.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge:    The list of edges with weights.
  ppEdgeNorm: The weights
  pElem:      The element
  kFace:      and the face.
  
  Changes To:
  -----------
  ppEdgeNorm: possible realloc, the accumulated weights.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_ewts_bnd ( bndFc_struct *pBndFc, llEdge_s *pllEdge, double **ppEdgeNorm ){
  
  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static const edgeOfElem_struct *pEoE ;
  static const elem_struct *pElem ;
  static int kEdge, iVx, someSide, mTimes, mDim, nVxEg[MAX_VX_FACE], kFace, mVxFc,
    newEg ;
  static double bndNorm[MAX_DIM], edgeVec[MAX_DIM], faceGC[MAX_DIM],
    vx2faceGC[MAX_DIM] ;
  static const vrtx_struct *pVrtx1, *pVrtx2, *pVxFc[MAX_VX_FACE] ;

  pElem = pBndFc->Pelem ;
  kFace = pBndFc->nFace ;
  pElT = elemType + pElem->elType  ;
  mDim = pElT->mDim ;

  /* Initially, all boundary edges are treated non-LP. */
  pBndFc->markdEdges = 0 ;
  
  if ( mDim == 2 ) {
    /* Simplified 2D treatment. */
    kEdge = pElT->faceOfElem[kFace].kFcEdge[0] ;
    pEoE = pElT->edgeOfElem + kEdge ;
    pVrtx1 = pElem->PPvrtx[ pEoE->kVxEdge[0] ] ;
    pVrtx2 = pElem->PPvrtx[ pEoE->kVxEdge[1] ] ;
  
    /* The two reflex edges. */
    nVxEg[0] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                               &pVrtx1, &pVrtx1, &someSide, &newEg ) ;
    nVxEg[1] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                               &pVrtx2, &pVrtx2, &someSide, &newEg ) ;
  
    /* Boundary normal. */
    uns_face_normal ( pElem, kFace, bndNorm, &mTimes ) ;
    vec_mult_dbl ( bndNorm, .5*mTimes, mDim ) ;

    /* Add normals. */
    add_ewt (  bndNorm, 1., 0, mDim, *ppEdgeNorm, nVxEg[0] ) ;
    add_ewt (  bndNorm, 1., 0, mDim, *ppEdgeNorm, nVxEg[1] ) ;
  }

  else {
    /* Treat triangular and quadrilateral faces generically by splitting the
       quadrilateral facet of the median dual into two triangles along each edge. */
    face_grav_ctr ( pElem, kFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;

    if ( mVxFc > 2 ) {
      /* Non-collapsed face. Add all reflex edges. */
      for ( iVx = 0 ; iVx < mVxFc ; iVx++ ) {
        pVrtx1 = pVxFc[iVx] ;
        nVxEg[iVx] = add_edge_vrtx ( pllEdge, (void **) ppEdgeNorm,
                                     &pVrtx1, &pVrtx1, &someSide, &newEg ) ;
      }
      
      for ( iVx = 0 ; iVx < mVxFc ; iVx++ ) {
        pVrtx1 = pVxFc[  iVx          ] ;
        pVrtx2 = pVxFc[ (iVx+1)%mVxFc ] ;
        
        if ( pVrtx1 != pVrtx2 ) {
          /* Non-collapsed edge. Vector along the edge. */
          vec_diff_dbl ( pVrtx2->Pcoor, pVrtx1->Pcoor, 3, edgeVec ) ;
          /* Vector from the vertex to the face centroid. */
          vec_diff_dbl ( faceGC, pVrtx1->Pcoor, 3, vx2faceGC ) ;
          
          /* The outward normal of the forward/backward triangular facet on iFct=0/1. */
          cross_prod_dbl ( edgeVec, vx2faceGC, 3, bndNorm ) ;
          /* Note that the backward edge vector points towards iVx, no sign change
             necessary. Note also that edgeVec is normalized to the entire edge length,
             our area is formed with half of it. */
          vec_mult_dbl ( bndNorm, .5, mDim ) ;
          
          /* Piecewise constant weights. Each vertex gets one half. */
          add_ewt (  bndNorm, .5, 0, mDim, *ppEdgeNorm, nVxEg[  iVx          ] ) ;
          add_ewt (  bndNorm, .5, 0, mDim, *ppEdgeNorm, nVxEg[ (iVx+1)%mVxFc ] ) ;
        }
      }
    }
  }

  return ( 1 ) ;
}



/******************************************************************************

  make_edge_normals:
  Make a list of all the edges in a grid. In the course, a list of edges is
  generated and possibly reused later on. Thus, it is not deallocated.
  
  Last update:
  ------------
  4Apr13; make all large counters ulong_t.
  4Apr13; modified interface to loop_elems
  13Jan99; don't automatically fix up simplices to lp.
  : conceived.
  
  Input:
  ------
  pUns
  ppEdgeNorm:  


  Changes To:
  -----------
  ppEdgeNorm: a list of 2*mDim weights for each edge, starting at 1. First mDim
              weights for the first vx of the edge by the second, and then vice
	      versa.
  pmEdges:    the size of the list of edges.
  pmSymmEg:   the number of symmetric
  pmAntimEg:  the number of antimetric edges.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_edge_normals ( uns_s *pUns,
			double **ppEdgeNorm, ulong_t *pmEdges,
			ulong_t *pmSymmEg, ulong_t *pmAntimEg, ulong_t *pmElemEg ) {
  
  const int dataSize = 2*pUns->mDim*sizeof(double), *kVxEdge ;
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVx[2] ;

  llEdge_s *pllEdge ;
  chunk_struct *pChunk ;
  elem_struct *pElem=NULL, *pElemBeg, *pElemEnd ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  int kEdge ;
  ulong_t nLstEg, mVxToFix, iEg ;
  int side, newEg ;
  
  /* Make some initial storage for the list. */
  free_llEdge ( &(pUns->pllEdge) ) ;
  if ( !( pUns->pllEdge = pllEdge =
          make_llEdge ( pUns, CP_NULL, 0, dataSize, NULL, (void **) ppEdgeNorm ) ) ) {
    printf ( " FATAL: could not make an edge list in make_edge_normals.\n" ) ;
    return ( 0 ) ; }

  /* Place all elemental edges first in the list. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number ) {
        pElT = elemType + pElem->elType ;
        for ( kEdge = 0 ; kEdge < elemType[ pElem->elType ].mEdges ; kEdge++ ) {
          kVxEdge = pElT->edgeOfElem[kEdge].kVxEdge ;
          pVx[0] = pElem->PPvrtx[ kVxEdge[0] ] ;
          pVx[1] = pElem->PPvrtx[ kVxEdge[1] ] ;
          if ( pVx[0] != pVx[1] )
            add_edge_vrtx ( pllEdge, (void**) ppEdgeNorm, pVx, pVx+1, &side, &newEg ) ;
        }
      }
  

  /* Now all elemental edges have been added in sequence. Get their number. */
  get_number_of_edges ( pllEdge, pmElemEg ) ;


  
  /* Loop over all elements and add their edges to the list. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number )
	get_ewts_elem ( pllEdge, ppEdgeNorm, pElem ) ;

  
  
  /* Boundary terms. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace  )
        /* get_ewts_bnd takes care of lp-ness for tris and tets. */
	get_ewts_bnd ( pBndFc, pllEdge, ppEdgeNorm ) ;



  
  /* Pretreat all simplices if non-lp weights are wanted. */
  if ( Grids.lp_sweeps ) {
    /* Simplex elements. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
      for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
        if ( pElem->number && ( pElem->elType == tri || pElem->elType == tet ) )
          for ( kEdge = 0 ; kEdge < elemType[pElem->elType].mEdges ; kEdge++ )
            lp_edge ( pElem, kEdge, pllEdge, ppEdgeNorm ) ;

    /* Boundary faces of simplices. */
    pChunk = NULL ;
    while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace  &&
             ( pElem->elType == tri || pElem->elType == tet ) ) {
	  pElT = elemType + pBndFc->Pelem->elType ;
          pFoE = pElT->faceOfElem + pBndFc->nFace ;
          for ( iEg = 0 ; iEg < pFoE->mFcEdges ; iEg++ )
            lp_bnd_edge ( pBndFc, iEg, pllEdge, ppEdgeNorm ) ;
        }
  }



  /* Fix up non-lp edges. */
  if ( Grids.lp_sweeps ) {

    /* Loop over all elements, fix all edges. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
      for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
	if ( pElem->number ) {
	  pElT = elemType + pElem->elType ;
	  for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
            lp_edge ( pElem, kEdge, pllEdge, ppEdgeNorm ) ;
	  }
	}

    /* Fix all boundary faces. */
    pChunk = NULL ;
    while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace  ) {
	  pElT = elemType + pBndFc->Pelem->elType ;
          pFoE = pElT->faceOfElem + pBndFc->nFace ;
          for ( iEg = 0 ; iEg < pFoE->mFcEdges ; iEg++ ) {
            kEdge = pFoE->kFcEdge[iEg] ;
            lp_bnd_edge ( pBndFc, iEg, pllEdge, ppEdgeNorm ) ;
	  }
	}
  }



  
  /* All edges fixed? */
  if ( ( mVxToFix = check_edge_normals_lp ( pUns, *ppEdgeNorm ) ) ) {
    sprintf ( hip_msg, "%"FMT_ULG" non-LP vertices left.\n", mVxToFix ) ;
    hip_err( warning, 1, hip_msg ) ;
  }


  
  /* Count the edges. */
  *pmEdges = get_number_of_edges ( pllEdge, &nLstEg ) ;

  /* Fix up 'reflex' edges, edges pointing to the same vertex on both ends.
     These may only have symmetric components. */
  fix_reflex_edges ( *pmEdges, pllEdge, *ppEdgeNorm, pUns->mDim ) ;
  
  /* Convert to Cindy's symmetric/antimetric edges. */
  ewt_to_symmAntim ( pUns, nLstEg, *ppEdgeNorm, pUns->mDim, pmSymmEg, pmAntimEg ) ;

  /* Combine antisymmetric edges along periodic boundaries. */
  comb_ewts ( pUns, nLstEg, *ppEdgeNorm ) ;
  
  return ( 1 ) ;
}























#ifdef DEBUG

/**********************************************************************************
  debug.
  */

void printEgwt ( uns_s *pUns, const double *pEdgeNorm, const ulong_t nVx ) {
  
  ulong_t nLst, mEdges = get_number_of_edges ( pUns->pllEdge, &nLst ), nEg ;
  int nDim ;
  vrtx_struct *pVx1, *pVx2 ;
  double sumNorm[MAX_DIM] = {0.} ;
  const double *pEN ;

  printf ( "vx %3"FMT_ULG":\n", nVx ) ;

  for ( nEg = 1 ; nEg <= mEdges ; nEg++ )
    if ( show_edge ( pUns->pllEdge, nEg, &pVx1, &pVx2 ) ) {
      pEN = pEdgeNorm + 2*pUns->mDim*nEg ;

      if ( pVx1->number == nVx ) {
        printf ( " edge %3"FMT_ULG": %3"FMT_ULG"-%3"FMT_ULG":", 
                 nEg, pVx1->number, pVx2->number ) ;
        for ( nDim = 0 ; nDim < pUns->mDim ; nDim++ )
          printf ( " %+10.5e,", pEN[nDim] ) ;
        printf ( "\n" ) ;
        vec_add_dbl ( pEN, sumNorm, pUns->mDim, sumNorm ) ;
      }
      
      if ( pVx2->number == nVx ) {
        printf ( " edge %3"FMT_ULG": %3"FMT_ULG"-%3"FMT_ULG":", 
                 nEg, pVx1->number, pVx2->number ) ;
        for ( nDim = 0 ; nDim < pUns->mDim ; nDim++ )
          printf ( " %+10.5e,", pEN[pUns->mDim+nDim] ) ;
        printf ( "\n" ) ;
        vec_add_dbl ( pEN+pUns->mDim, sumNorm, pUns->mDim, sumNorm ) ;
      }
    }
	
  printf ( "                    %+10.5e, %+10.5e, %+10.5e\n",
           sumNorm[0], sumNorm[1], sumNorm[2] ) ;
}

typedef struct {
  ulong_t nVx ;
  double wt[3] ;
} egWt_s ;

#define MEG 200

static egWt_s *addEg ( egWt_s *pEg, ulong_t *pmEg, const ulong_t nVx ) {

  egWt_s *pE ;
  
  for ( pE = pEg ; pE < pEg+*pmEg ; pE++ )
    if ( pE->nVx == nVx )
      return ( pE ) ;

  (*pmEg)++ ;
  if ( pE >= pEg+MEG ) {
    printf ( " FATAL: need more edges in addEg.\n" ) ;
    return ( NULL ) ; }

  pE->nVx = nVx ;
  pE->wt[0] = pE->wt[1] = pE->wt[2] = 0. ;
  return ( pE ) ;
}

void checkEgwt ( uns_s *pUns, const double *pEdgeNorm, const ulong_t nVx ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const edgeOfElem_struct *pEoE ;
  const int *kVxEdge ;
  const vrtx_struct  *pVxEl[12], *pVxFc[4] ;

  ulong_t mEg = 0 ;
  egWt_s eg[MEG], *pE ;
  
  int kVx, mElVx = 0, mVxEl, kFace, mVxFc[6], kEdge, iFc ;
  ulong_t nVx2 ;
  elem_struct *pElVx[200], *pElem, *pElemBeg, *pElemEnd, fctElem ;
  vrtx_struct *ppVx[4], *pVx0, *pVx1, vxFct[4], *pVx ;
  chunk_struct *pChunk ;
  double wt[3], surfNorm[2][3], elemGC[3], faceGC[2][3], edgeGC[3], elem2EdgeGC[3],
    elem2FaceGC[3], edgeVec[3], coor[4][3] ;
  FILE *glutFile = fopen ( "oneGrid.glut", "w" ) ;

  /* Generic face. */
  fctElem.PPvrtx = ppVx ;
  fctElem.elType = tri ;
  ppVx[0] = vxFct ;   vxFct[0].number = 0 ; vxFct[0].Pcoor = coor[0] ;
  ppVx[1] = vxFct+1 ; vxFct[1].number = 0 ; vxFct[1].Pcoor = coor[1] ;
  ppVx[2] = vxFct+2 ; vxFct[2].number = 0 ; vxFct[2].Pcoor = coor[2] ;

  vec_ini_dbl ( 0., 3, wt ) ;
  
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number ) {
        pElT = elemType + pElem->elType ;

        for ( pVx = NULL, kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
          if ( pElem->PPvrtx[kVx]->number == nVx ) {
            pVx = pElem->PPvrtx[kVx] ;
            break ;
          }

        if ( pVx ) {
          /* nVx forms this element. */
          elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;


          /* Loop over all edges. */
          for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
            pEoE = pElT->edgeOfElem + kEdge ;
            kVxEdge = pEoE->kVxEdge ;
            pVx0 = pElem->PPvrtx[ kVxEdge[0] ] ;
            pVx1 = pElem->PPvrtx[ kVxEdge[1] ] ;
      
            if ( pVx0 != pVx1 && ( pVx0->number == nVx || pVx1->number == nVx ) ) {
              /* Non-collapsed edge. */
              edge_grav_ctr ( pElem, kEdge, edgeGC ) ;
              /* Vector from the center of the edge
                 to the gravity center of the element. */
              vec_diff_dbl ( edgeGC, elemGC, 3, elem2EdgeGC ) ;
        
              /* Calculate the areas of the two triangular facets of the median dual. */
              for ( iFc = 0 ; iFc < 2 ; iFc++ ) {
                kFace = pEoE->kFcEdge[iFc] ;
                face_grav_ctr ( pElem, kFace, faceGC[iFc], &pFoE, mVxFc+kFace, pVxFc ) ;
                vec_diff_dbl ( faceGC[iFc], elemGC, 3, elem2FaceGC ) ;
                if ( mVxFc[kFace] > 2 )
                  /* Non-collapsed face. */
                  cross_prod_dbl ( elem2FaceGC, elem2EdgeGC, 3, surfNorm[iFc] ) ;
                else
                  vec_ini_dbl ( 0., 3, surfNorm[iFc] ) ;
              }
              /* The two parts have opposite directions. Add them reversing one. */
              vec_diff_dbl ( surfNorm[0], surfNorm[1], 3, surfNorm[0] ) ;

              if ( pVx0->number == nVx ) {
                /* printf ( " %d - %d in el %d: %+10.5e %+10.5e %+10.5e\n",
                   pVx0->number, pVx1->number, pElem->number,
                   surfNorm[0][0], surfNorm[0][1], surfNorm[0][2] ) ; */
                pE = addEg ( eg, &mEg, pVx1->number ) ;
                vec_add_mult_dbl ( pE->wt,  .5, surfNorm[0], 3, pE->wt ) ;
              }
              else {
                /* printf ( " %d - %d in el %d: %+10.5e %+10.5e %+10.5e\n",
                         pVx1->number, pVx0->number, pElem->number,
                         -surfNorm[0][0], -surfNorm[0][1], -surfNorm[0][2] ) ; */
                pE = addEg ( eg, &mEg, pVx0->number ) ;
                vec_add_mult_dbl ( pE->wt, -.5, surfNorm[0], 3, pE->wt ) ;
              }

#ifdef DEBUG
              /* Make a virtual element for this face. */
              nVx2 = ( pVx0 == pVx ? pVx1->number : pVx0->number ) ;
              vxFct[0].number = nVx2 ;
              vxFct[2].number = pElem->number ;

              vec_copy_dbl ( edgeGC,     3, vxFct[0].Pcoor ) ; 
              vec_copy_dbl ( faceGC[0],  3, vxFct[1].Pcoor ) ; 
              vec_copy_dbl ( elemGC,     3, vxFct[2].Pcoor ) ;
              viz_one_elem_vtk( "egWtElem.vtk", &fctElem, NULL ) ;
              
              vec_copy_dbl ( edgeGC,     3, vxFct[0].Pcoor ) ; 
              vec_copy_dbl ( faceGC[1],  3, vxFct[1].Pcoor ) ; 
              vec_copy_dbl ( elemGC,     3, vxFct[2].Pcoor ) ;
              viz_one_elem_vtk ( "egWtElem.vtk", &fctElem, NULL ) ;
#endif
            }
          }
        }
      }

  for ( pE = eg ; pE < eg + mEg ; pE++ ) {
    printf ( "           %3"FMT_ULG"-%3"FMT_ULG": %+10.5e, %+10.5e, %+10.5e.\n",
             nVx, pE->nVx, pE->wt[0], pE->wt[1], pE->wt[2] ) ;
    vec_add_dbl ( pE->wt, wt, 3, wt ) ;
  }

  printf ( "                wt: %+10.5e, %+10.5e, %+10.5e.\n", wt[0], wt[1], wt[2] ) ;
  
  fclose ( glutFile ) ;
  system ( "glutGrid" ) ;

  return ;
}

#endif
