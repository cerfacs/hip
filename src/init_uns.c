/*
  init_uns.c:
  Fill the tables of faces and edges.
  Fill a table of permissible refinement patterns for each unstructured element type.
  
  Last update:
  ------------
  3Apr19; move init_uns and init_uns_var here.
  5Jul17; fill kOppSide
  6Jan15; rename pUnsInit to more descriptive pArrFamUnsInit
  7Apr13; fix missing braces in intialiser of elemType.
  9Jul11; wrap adaptive initialisation in ADAPT_REF
  5Apr11; intro elemType for 'bi', but list only mDim and mVerts for it.
  24Jan99; intro edgeOfElem.kParaEg, make_parallel_edges, get_attFc ;
  29Oct98; intro edgeOfElem.kEgInFc ;
  15Jun98; add add_fc2eg.
  1Sep97; reintegrate init_uns2/3D.c.
  27Feb97: Use faces as faces of a 3D element. Thus, quads and tris have no faces.
  
  This file contains:
  -------------------
  init_uns
  init_uns_tri
  init_uns_quad
  init_uns_tet
  init_uns_pyr
  init_uns_prism
  init_uns_hex
  rotate_elem
  fill_edgeOfFace ;
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"
#include "proto_uns.h"

/* The patterns blocked out with this directive are the ones that
   directionally refine a triangular face.  
  #define USE_ALL_DIR_REF  
*/

extern const int verbosity ;
extern char hip_msg[] ;
extern Grids_struct Grids ;

/* Statically allocated virtual element. */
extern double vrtCoor[MAX_VX_ELEM*MAX_DIM] ;
extern vrtx_struct vrtVrtx[MAX_VX_ELEM], *pVrtVrtx[MAX_VX_ELEM] ;
extern elem_struct vrtElem ;


extern arrFam_s *pArrFamUnsInit ;

/* An array of bitmasks for each integer. */
int bitEdge[MAX_EDGES_ELEM], mask ;

/* Make a buffertype for each number of children. There may be at most 6
   faces with 6 children each. */
refType_struct bufferType[MAX_FACES_ELEM*MAX_CHILDS_FACE+1] ;

/* Initialize elemType, see cpre_uns.h for its definition. */
elemType_struct elemType[MAX_ALL_ELEM_TYPES] = { 
  /* Tri. */
  { tri,"tri",2,3,3,3,0, // elType, name, mDim, mVerts, mEdges, mSides, mFaces
    { {0, {0,0,0,0}}, // faces are numbered from 1, 0 face is empty.
      {2,{1,2}, 0, 2,{2,3}},// faceOfElem from 1.
      {2,{2,0}, 0, 2,{1,3}},
      {2,{0,1}, 0, 2,{1,2}} }, 
    { {{0,1}}, {{0,2}}, {{1,2}} }, // edgeOfEllem from 0.
    0, NULL, 0 // mRefTypes, PrefType, allEdges.
  },
      /* Quad. Note that the 'quad' keyword is reserved.*/
  { qua,"qua",2,4,4,4,0,
    { {0, {0,0,0,0}},
      {2, {0,1,0,0}, 3, 1,{3}},
      {2, {1,2,0,0}, 4, 1,{4}},
      {2, {2,3,0,0}, 1, 1,{1}},
      {2, {3,0,0,0}, 2, 1,{2}} },
    { {{0,1}}, {{0,3}}, {{1,2}}, {{2,3}} },
    0, NULL, 0
  },
      /* Tet. */
  { tet,"tet", 3,4,6,4,4,
    { {0, {0,0,0,0}, 0, 0,{0} },
      {3,{1,3,2,0}, 0, 0,{0} },
      {3,{0,2,3,0}, 0, 0,{0} },
      {3,{0,3,1,0}, 0, 0,{0} },
      {3,{0,1,2,0}, 0, 0,{0} } },
    { {{0,1}}, {{0,2}}, {{0,3}}, {{1,2}}, {{1,3}}, {{2,3}} },
    0, NULL, 0
  },
    /* Pyr. */
  { pyr,"pyr",3,5,8,5,5,
    { {0, {0,0,0,0}},
      {4,{0,3,2,1}, 0, 0,{0}},
      {3,{0,1,4,0}, 0, 0,{0}},
      {3,{1,2,4,0}, 0, 0,{0}},
      {3,{2,3,4,0}, 0, 0,{0}},
      {3,{3,0,4,0}, 0, 0,{0}} },
    { {{0,1}}, {{0,3}}, {{0,4}}, {{1,2}}, {{1,4}}, {{2,3}}, {{2,4}}, {{3,4}} },
    0, NULL, 0
  },
    /* Prism. */
  { pri,"pri",3,6,9,5,5,
    { {0, {0,0,0,0}},
      {4, {2,3,5,4}, 0, 2,{2,3}},
      {4, {0,1,4,5}, 0, 2,{1,3}},
      {4, {0,3,2,1}, 0, 2,{1,2}},
      {3, {3,0,5,0}, 5, 1,{5} },
      {3, {1,2,4,0}, 4, 1,{4} } },
    { {{0,1}}, {{0,3}}, {{0,5}}, {{1,2}}, {{1,4}}, {{2,3}}, {{2,4}}, {{3,5}}, {{4,5}} },
    0, NULL, 0
  },
    /* Hex. */
  { hex,"hex",3,8,12,6,6,
    { {0, {0,0,0,0}},
      {4, {0,1,5,4}, 3, 1,{3}},
      {4, {5,1,2,6}, 4, 1,{4}},
      {4, {7,6,2,3}, 1, 1,{1}},
      {4, {0,4,7,3}, 2, 1,{2}},
      {4, {0,3,2,1}, 6, 1,{6}},
      {4, {4,5,6,7}, 5, 1,{5}} },
    { {{0,1}}, {{0,3}}, {{0,4}}, {{1,2}}, {{1,5}}, {{2,3}},
      {{2,6}}, {{3,7}}, {{4,5}}, {{4,7}}, {{5,6}}, {{6,7}} },
    0, NULL, 0
  },
  /* Bi. The seventh sin. */
  { bi, "bi", 1,2,0,0,0,
   { {0},
      {0,{0,0}, 0, 0,{0,0}},
      {0,{0,0}, 0, 0,{0,0}},
      {0,{0,0}, 0, 0,{0,0}} },
    { {{0,0}}, {{0,0}}, {{0,0}} },
    0, NULL, 0
  }
} ;

/* The following arrays are used in init_uns_whateverElem... */
/* Define a list of vertices and initialize their numbers. */
vrtx_struct vx[MAX_VX_ELEM] = { {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7} } ;
/* Define a linear list of vertex pointes to be used by the elements. */
vrtx_struct *Pvx[MAX_VX_ELEM] = { vx, vx+1,vx+2,vx+3,vx+4,vx+5,vx+6,vx+7 } ;
vrtx_struct *PvxPerm[MAX_VX_ELEM] ;

#ifdef DEBUG
#ifndef ADAPT_METH
/******************************************************************************
  check_minnorm:   */

/*! Test min-norm interpolation
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  19Feb16: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void  mult_el_coor_alpha ( elem_struct *pElem, int mDim, int mVx, 
                           double alpha[], double comult[] ) {
  int kD ;
  for ( kD = 0 ; kD < mDim ; kD++ ) 
    comult[kD] = 0. ;

  int kV ;
  vrtx_struct **ppVx = pElem->PPvrtx, *pVx ;
  for ( kV = 0 ; kV < mVx ; kV++ ) {
    pVx = ppVx[kV] ;
    comult[0] += alpha[kV]*pVx->Pcoor[0] ;
    comult[1] += alpha[kV]*pVx->Pcoor[1] ;
    if ( mDim == 3 )
      comult[2] += alpha[kV]*pVx->Pcoor[2] ;
  }
  return ;
}

void check_1minnorm ( char *text, double co0, double co1, double co2 ) {

  vrtx_struct vx ;
  double co[MAX_DIM] ;
  int retVal, k ;
  double alpha[MAX_VX_ELEM] ;
  double comult[MAX_DIM] ;

  vx.Pcoor = co ;
  co[0] = co0, co[1] = co1, co[2] = co2 ;

  printf ( "\n %s:\n%7.2e, %7.2e, %7.2e\n", text,
           vx.Pcoor[0], vx.Pcoor[1], vx.Pcoor[2] ) ;

  retVal = minNormEl(&vrtElem,3,8,vx.Pcoor,0,0,alpha) ;

  for ( k=0 ; k<8 ; k++ ) {
    printf (" %g,", alpha[k] ) ; printf ( "\n" ) ;
  }

  mult_el_coor_alpha ( &vrtElem, 3, 8, alpha, comult ) ;
  double delta = sq_distance_dbl ( comult, co, 3 ) ;
  
  printf ( "   Found:\n%7.2e, %7.2e, %7.2e, delta %7.2e\n", 
           comult[0], comult[1], comult[2], sqrt(delta) ) ;

  return ;
}

int check_minnorm (  ) {

  // Generic element in 3D
  vrtElem.elType = hex ;
  vrtx_struct **ppVx = vrtElem.PPvrtx, *pVx ;

  int kVx = 0 ;
  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 0. ;
  pVx->Pcoor[1] = 0. ;
  pVx->Pcoor[2] = 0. ;

  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 0. ;
  pVx->Pcoor[1] = 1. ;
  pVx->Pcoor[2] = 0. ;

  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 0. ;
  pVx->Pcoor[1] = 1. ;
  pVx->Pcoor[2] = 1. ;

  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 0. ;
  pVx->Pcoor[1] = 0. ;
  pVx->Pcoor[2] = 1. ;


  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 1. ;
  pVx->Pcoor[1] = 0. ;
  pVx->Pcoor[2] = 0. ;

  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 1. ;
  pVx->Pcoor[1] = 1. ;
  pVx->Pcoor[2] = 0. ;

  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 1. ;
  pVx->Pcoor[1] = 1. ;
  pVx->Pcoor[2] = 1. ;

  pVx = ppVx[kVx++] ;
  pVx->Pcoor[0] = 1. ;
  pVx->Pcoor[1] = 0. ;
  pVx->Pcoor[2] = 1. ;

  check_1minnorm ( "Center:", .5,.5,.5) ;
  check_1minnorm ( "Corner:", .1,.9,.1) ;
  check_1minnorm ( "Face center:",1e-14,.5,.5) ;
  check_1minnorm ( "Face corner:",1e-14,.1,.1) ;
  check_1minnorm ( "Edge:",1e-14,1.e-14,.5) ;
  check_1minnorm ( "Corner:",1e-14,1.e-14,1.e-14) ;

  check_1minnorm ( "Face center:",-1e-14,.5,.5) ;
  check_1minnorm ( "Face corner, projection:", -1e-14,.1,.1) ;
  check_1minnorm ( "Edge projection:",-1e-14,-1.e-14,.5) ;
  check_1minnorm ( "Corner, projection:", -1e-14,1.-1e-14,-1.e-14 ) ;
  


  return ( 0 ) ;
}

int check_elem_is_convex () {

  double co[3*8], *pCo ;
  vrtx_struct vx[8], vxtra ;
  vrtx_struct *Pvx[MAX_VX_ELEM] = { vx, vx+1,vx+2,vx+3,vx+4,vx+5,vx+6,vx+7 } ;

  Grids.epsOverlap = .8 ;
  Grids.epsOverlapSq = .8*.8 ;

  int kVx ;
  for ( kVx = 0 ; kVx < 8 ; kVx++ ) {
    vx[kVx].Pcoor = co+3*kVx ; vx[kVx].number = kVx ; }


  elem_struct aTet={ 1, tet, 1, 0,0,0,0, 0, 0, 0, Pvx } ;
  elem_struct aHex = { 1, hex, 1, 0,0,0,0, 0, 0, 0, Pvx } ;

  /* Make a unit tet. */
  pCo = co ;
  pCo[0] = 0., pCo[1] = 0., pCo[2] = 0. ;
  pCo = co+3 ;
  pCo[0] = 1., pCo[1] = 0., pCo[2] = 0. ;
  pCo = co+6 ;
  pCo[0] = 0., pCo[1] = 0., pCo[2] = 1. ;
  pCo = co+9 ;
  pCo[0] = 0., pCo[1] = 1., pCo[2] = 0. ;
  
  double vol, vol2, volMin = 0. ;
  vol2 = get_elem_vol ( &aTet ) ;
  int isNotCollapsed ;  
  int isConvex = elem_is_convex ( &aTet, volMin, &vol, &isNotCollapsed ) ;
  if ( ABS( vol - vol2 ) > 1.e-12 )
    hip_err ( fatal, 0, "failed volume calc for elem_is_convex." ) ;

  /* Collapse it. */
  pCo[0] = 0., pCo[1] = 0., pCo[2] = 0. ;
  vol2 = get_elem_vol ( &aTet ) ;
  vol = elem_is_convex ( &aTet, volMin, &vol, &isNotCollapsed ) ;
  if ( ABS( vol - vol2 ) > 1.e-12 )
    hip_err ( fatal, 0, "failed tet coll volume calc for elem_is_convex." ) ;
  

  /* Unit hex. */
  pCo = co ;
  pCo[0] = 0., pCo[1] = 0., pCo[2] = 0. ;
  pCo = co+3 ;
  pCo[0] = 1., pCo[1] = 0., pCo[2] = 0. ;
  pCo = co+6 ;
  pCo[0] = 1., pCo[1] = 1., pCo[2] = 0. ;
  pCo = co+9 ;
  pCo[0] = 0., pCo[1] = 1., pCo[2] = 0. ;
  
  pCo = co+12 ;
  pCo[0] = 0., pCo[1] = 0., pCo[2] = 1. ;
  pCo = co+15 ;
  pCo[0] = 1., pCo[1] = 0., pCo[2] = 1. ;
  pCo = co+18 ;
  pCo[0] = 1., pCo[1] = 1., pCo[2] = 1. ;
  pCo = co+21 ;
  pCo[0] = 0., pCo[1] = 1., pCo[2] = 1. ;

  vol2 = get_elem_vol ( &aHex ) ;
  isConvex = elem_is_convex ( &aHex, volMin, &vol, &isNotCollapsed ) ;
  if ( ABS( vol - vol2 ) > 1.e-12 )
    hip_err ( fatal, 0, "failed hex volume calc for elem_is_convex." ) ;
  
  /* Collapse 6 -> 2. */
  Pvx[6] = Pvx[2] ;
  vol2 = get_elem_vol ( &aHex ) ;
  isConvex = elem_is_convex ( &aHex, volMin, &vol, &isNotCollapsed ) ;
  if ( ABS( vol - vol2 ) > 1.e-12 )
    hip_err ( fatal, 0, "failed hex 1 coll volume calc for elem_is_convex." ) ;
  
  /* Collapse 5 -> 1. */
  Pvx[5] = Pvx[1] ;
  vol2 = get_elem_vol ( &aHex ) ;
  isConvex = elem_is_convex ( &aHex, volMin, &vol, &isNotCollapsed ) ;
  if ( ABS( vol - vol2 ) > 1.e-12 )
    hip_err ( fatal, 0, "failed hex 2 coll volume calc for elem_is_convex." ) ;
  
  /* Collapse 7 -> 3. */
  Pvx[7] = Pvx[3] ;
  vol2 = get_elem_vol ( &aHex ) ;
  isConvex = elem_is_convex ( &aHex, volMin, &vol, &isNotCollapsed ) ;
  if ( ABS( vol - vol2 ) > 1.e-12 )
    hip_err ( fatal, 0, "failed hex 3 coll volume calc for elem_is_convex." ) ;

  return ( 0 ) ;
}

#endif // ADAPT_METH
#endif // DEBUG



/******************************************************************************
  init_elem:   */

/*! initialise an element.

 */

/*
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; intro mark2/3.
  26Sep14; promote nEl to ulong_t
  16Dec11; rename hrbMark to boxMark.
  9jul11: conceived.
  

  Input:
  ------
  pEl

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void init_elem ( elem_struct *pEl, elType_e elType, 
                 ulong_t nEl, vrtx_struct **ppVx ) {

    pEl->number = nEl ;
    pEl->elType = elType ;
    pEl->term = 1 ;
    reset_elem_all_mark ( pEl ) ;
    /*pEl->mark = 0 ;
    pEl->mark2 = 0 ;
    pEl->mark3 = 0 ;
    pEl->cutMark = 0 ;
    pEl->boxMark = 0 ; */
    pEl->invalid = 0 ;
    pEl->iZone = 0 ;
    pEl->markdEdges = 0 ;


    pEl->PPvrtx = ppVx ;

#ifdef ADAPT_HIERARCHIC
    pEl->refdEdges = 0 ;
    pEl->PrefType = NULL ;
    pEl->Pparent = NULL ;
    pEl->PPchild = NULL ;
    pEl->root = pEl->leaf = 1 ;
#endif

  return ;
}


/******************************************************************************
  init_vrtElem:   */

/*! Initialise the global virtual element.
 */

/*
  
  Last update:
  ------------
  16Feb16; moved here from uns_mg.
  : conceived.
  
  Changes To:
  -----------
  global: vrtElem
  
*/

void init_uns_vrtElem () {

  init_elem ( &vrtElem, noEl, 0, pVrtVrtx ) ;

  int kVx ;

  /* Make a generic element to be filled with the collapsed geometry. */
  vrtx_struct **ppVx = vrtElem.PPvrtx ;
  for ( kVx = 0 ; kVx < MAX_VX_ELEM ; kVx++ ) {
    ppVx[kVx] = vrtVrtx + kVx ;
    ppVx[kVx]->number = kVx ;
    ppVx[kVx]->Pcoor = vrtCoor + kVx*MAX_DIM ;
  }

  return ;
}

/******************************************************************************
  init_bndFc:   */

/*! initialise a boundary face.
 */

/*
  
  Last update:
  ------------
  17Dec19; intro geoType, rename to init_
  29Jun12: conceived.
  

  Input:
  ------
  pBf: boundary face to reset.
  
*/

void init_bndFc ( bndFc_struct *pBf ) {

  pBf->Pelem = NULL ;
  pBf->nFace = 0 ;
  pBf->Pbc = NULL ;
  pBf->invalid = 0 ;
  pBf->markdEdges = 0 ;
  pBf->geoType = bnd ;

  return ;
}



/******************************************************************************

  rotate_elem:
  Rotate an unstructured element around the axis normal to a face.
  
  Last update:
  ------------
  2May19; move before 1st invocation.
  : conceived.
  
  Input:
  ------
  Pelem:
  nFace:

  Changes To:
  -----------
  Pelem:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int rotate_elem ( elem_struct *Pelem, int nFace, const int mDim ) {

  /* Rotation patterns. Note that faces start with 1. */ 
  const int rotElem[MAX_ELEM_TYPES][MAX_FACES_ELEM+1][MAX_VX_ELEM] =
  { /* Triangle. */
    { {0}, {2,0,1} },
      /* Quad. */
    { {0}, {3,0,1,2} },
      /* Tet. */
    { {0}, {0,2,3,1}, {2,1,3,0}, {3,0,2,1}, {2,0,1,3} },
      /* Pyr. */
    { {0}, {3,0,1,2,4} },
      /* Prism. */
    { {0}, {0}, {0}, {0}, {5,4,1,0,2,3}, {3,2,4,5,1,0} },
      /* Hex. */
    { {0}, {4,0,3,7, 5,1,2,6}, {4,5,1,0, 7,6,2,3}, {1,5,6,2, 0,4,7,3},
           {3,2,6,7, 0,1,5,4}, {1,2,3,0, 5,6,7,4}, {3,0,1,2, 7,4,5,6} }
  } ;

  int kVert, mVerts ;
  const int *PpermVx = rotElem[Pelem->elType][nFace] ;
  vrtx_struct *Pvrtx[MAX_VX_ELEM] ;

  if ( Pelem->elType > MAX_ELEM_TYPES || Pelem->elType < 0 )
  { printf ( " FATAL: no such element type %d in rotate_elem.\n",
	     Pelem->elType ) ;
    return ( 0 ) ;
  }
  if ( !PpermVx[0] && !PpermVx[1] )
  { printf ( " FATAL: cannot rotate around face %d in rotate_elem.\n", nFace ) ;
    return ( 0 ) ;
  }

  mVerts = elemType[ Pelem->elType ].mVerts ;
  /* Copy the permuted vertex pointers into Pvrtx. */
  for ( kVert = 0 ; kVert < mVerts ; kVert++ )
    Pvrtx[kVert] = Pelem->PPvrtx[ PpermVx[kVert] ] ;

  /* Copy the permutation back to Pelem. */
  for ( kVert = 0 ; kVert < mVerts ; kVert++ )
    Pelem->PPvrtx[kVert] = Pvrtx[kVert] ;

  return ( 1 ) ;
}


/******************************************************************************

  init_uns_tri:
  Initialize refinement patterns for triangles.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; add mark2/3
  : conceived.
  
  Changes To:
  -----------
  elemType[tri]
  
*/

void init_uns_tri () {

  elem_struct aTri={ 1, tri, 1, 0, 0, 0,0, PvxPerm } ;

  /* Note that face numbers run from 1 to mFaces, 
     thus 0 being an indicator of an unknown neighbor, while the
     vertex numbers run from 0 to mVerts-1, as in elem->PPvrtx.*/
  /* Triangle, 1 edge, sample for edge1, 1->2. */
#ifdef ADAPT_HIERARCHIC
  const refType_struct refTri1 =
  { tri,
    1, {{0,3}},
    0, {{0}}, 0,
    2, {{ tri, {0,3,2} },
        { tri, {3,1,2} } }
  } ;
  /* Triangle, 3 edges. */
  const refType_struct refTri3 =
  { tri,
    3, {{2,3}, {1,4}, {0,5} },
    0, {{0}}, 0,
    4, {{ tri, {0,5,4} },
        { tri, {5,1,3} },
        { tri, {4,3,2} },
        { tri, {5,3,4} } }
  } ;
    
  int kVx ;

  /* Fill the const refType. */
  for ( kVx = 0 ; kVx < elemType[aTri.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;

#ifdef USE_ALL_DIR_REF  
  
  /* Triangle, Type 0: 1 edge, edge0 1->2. */
  add_uns_refType ( &refTri1, &aTri ) ;

  /* Triangle, Type 1: 1 edge, edge1 1->3. */
  rotate_elem ( &aTri, 1, 2 ) ;
  add_uns_refType ( &refTri1, &aTri ) ;
  
  /* Triangle, Type 2: 1 edge, edge 2 2->3. */
  rotate_elem ( &aTri, 1, 2 ) ;
  add_uns_refType ( &refTri1, &aTri ) ;
#endif
  
  /* Triangle, Type 3: 3 edges. Reset the tri. */
  for ( kVx = 0 ; kVx < elemType[aTri.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;

  add_uns_refType ( &refTri3, &aTri ) ;
#endif

  return ;
}

/******************************************************************************

  init_uns_quad:
  Initialize refinement Types for quadrilaterals.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; add mark2/3
  9Jul11; reflect modification of elem_struct in initialiser.
  : conceived.

  Changes To:
  -----------
  RefType
*/

void init_uns_quad () {

  elem_struct aQuad={ 1, qua, 1, 0, 0, 0, 0, PvxPerm } ;

#ifdef ADAPT_HIERARCHIC
  /* Two edges 1&2, 1->4, 2->3. */
  const refType_struct refQuad2 =
  { qua,
    2, {{2,4}, {1,5}},
    0, {{0}}, 0,
    2, {{ qua, {0,1,4,5} },
        { qua, {5,4,2,3} } }
  } ;
  /* All edges. */
  const refType_struct refQuad4 =
  { qua,
    4, {{0,4}, {2,5}, {3,6}, {1,7} },
    0, {{0}}, 8,
    4, {{ qua, {0,4,8,7} },
        { qua, {4,1,5,8} },
        { qua, {8,5,2,6} },
        { qua, {7,8,6,3} } }
  } ;
    
  int kVx ;

  /* Make a quad. */
  for ( kVx = 0 ; kVx < elemType[aQuad.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;

  /* Type 0: Two edges 1&2, 1->4, 2->3. */
  add_uns_refType ( &refQuad2, &aQuad ) ;
  
  /* Type 1: Two edges 0&3, 1->2, 3->4. */
  rotate_elem ( &aQuad, 1, 2 ) ;
  add_uns_refType ( &refQuad2, &aQuad ) ;
  
  /* Type 2: 4 edges. Reset the quad. */
  for ( kVx = 0 ; kVx < elemType[aQuad.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  add_uns_refType ( &refQuad4, &aQuad ) ;
#endif

  return ;
}


/******************************************************************************

  init_uns_tet:
  Initialize refinement types for tetrahedra.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; add mark2/3
  9Jul11; reflect modification of elem_struct in initialiser.
  : conceived.

  Changes To:
  -----------
  RefType
*/

void init_uns_tet () {

  elem_struct aTet={ 1, tet, 1, 0, 0, 0, 0, PvxPerm } ;

#ifdef ADAPT_HIERARCHIC
  /* One edge: 0, 1->2. */
  const refType_struct refTet1 =
  { tet,
    1, {{0,4}},
    0, {{0}}, 0,
    2, {{ tet, {0,4,2,3} },
        { tet, {4,1,2,3} } }
  } ;
  /* All edges, with an internal diagonal drawn from edge 0 to 5. */
  const refType_struct refTet6 =
  { tet,
    6, {{0,4}, {1,5}, {2,6}, {3,8}, {4,7}, {5,9} },
    0, {{0}}, 0,
    8, {{ tet, {0,4,5,6} },
        { tet, {4,1,8,7} },
        { tet, {5,8,2,9} },
        { tet, {6,7,9,3} },
        { tet, {5,4,9,6} },
        { tet, {5,4,8,9} },
        { tet, {4,7,8,9} },
        { tet, {4,7,9,6} } }
  } ;

  int kVx ;

  /* Make a tet. */
  for ( kVx = 0 ; kVx < elemType[aTet.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;

#ifdef USE_ALL_DIR_REF  
  /* Type 0:  0, 1->2 */
  add_uns_refType ( &refTet1, &aTet ) ;
  
  /* Type 1: 1, 1->3. */
  rotate_elem ( &aTet, 4, 3 ) ;
  add_uns_refType ( &refTet1, &aTet ) ;
  
  /* Type 2: 2, 1->4. */
  rotate_elem ( &aTet, 3, 3 ) ;
  add_uns_refType ( &refTet1, &aTet ) ;
  
  /* Type 3: 5, 3->4. */
  rotate_elem ( &aTet, 3, 3 ) ;
  add_uns_refType ( &refTet1, &aTet ) ;
  
  /* Type 4: 4, 2->4. */
  rotate_elem ( &aTet, 4, 3 ) ;
  add_uns_refType ( &refTet1, &aTet ) ;
  
  /* Type 5: 3, 2->3. */
  rotate_elem ( &aTet, 4, 3 ) ;
  add_uns_refType ( &refTet1, &aTet ) ;
#endif  

  /* 6 edges. */
  for ( kVx = 0 ; kVx < elemType[aTet.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  /* Type 13: 6 edges, internal diagonal 0, 1->2 to 5, 3->4. */
  add_uns_refType ( &refTet6, &aTet ) ;

  /* For the time being there is no logic to choose the direction
     of the diagonal. Moreover, there should be a fourth cut. */
#ifdef USE_ALL_DIR_REF  
  /* Type 14: 1, 1->3 and 4, 2->4. */
  rotate_elem ( &aTet, 4, 3 ) ;
  add_uns_refType ( &refTet6, &aTet ) ;
  
  /* Type 15: 2, 1->4 and 3, 2->3. */
  rotate_elem ( &aTet, 4, 3 ) ;
  add_uns_refType ( &refTet6, &aTet ) ;
#endif  
#endif
  
  return ;
}


/******************************************************************************

  init_uns_pyr:
  Initialize refinement types for pyramids.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; add mark2/3
  9Jul11; reflect modification of elem_struct in initialiser.
  : conceived.

  Changes To:
  -----------
  RefType
*/

void init_uns_pyr () {

  elem_struct aPyr = { 1, pyr, 1, 0, 0, 0, 0, PvxPerm } ;

  /* One edge refined that is connected to the apex: 3, 1->5.
     4 children tets.*/
  const refType_struct refPyr1A =
  { pyr,
    1, {{2,5}},
    0, {{0}}, 0,
    3, {{ pyr, {0,1,2,3,5} },
        { tet, {5,1,4,2} },
        { tet, {3,5,4,2} } }
  } ;

#ifdef ADAPT_HIERARCHIC
  /* Two opposed edges refined that form the quadrilateral face:
     1:1->2, 6:3->4*/
  const refType_struct refPyr2 =
  { pyr,
    2, {{0,5}, {5,6}},
    0, {{0}}, 0,
    2, {{ pyr, {0,5,6,3,4}, },
        { pyr, {5,1,2,6,4}, } }
  } ;

  /* All edges.*/
  const refType_struct refPyr8 =
  { pyr,
    8, {{0,5}, {1,6}, {2,7}, {3,8}, {4,9}, {5,10}, {6,11}, {7,12}},
    1, {{1,13}}, 0,
    10, {{ pyr, {0,5,13,6,7} },
	 { pyr, {1,8,13,5,9} },
	 { pyr, {2,10,13,8,11} },
	 { pyr, {3,6,13,10,12} },
	 { tet, {5,9,7,13} },
	 { tet, {8,11,9,13} },
	 { tet, {10,12,11,13} },
	 { tet, {6,7,12,13} },
	 { pyr, {9,7,12,11,13} },
	 { pyr, {7,9,11,12,4} } }
  } ;

  int kVx ;

#ifdef USE_ALL_DIR_REF  
  /* Refine one egde forming the apex. */
  for ( kVx = 0 ; kVx < elemType[aPyr.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  /* Type 0:  3:1->5*/
  add_uns_refType ( &refPyr1A, &aPyr ) ;
  
  /* Type 1: 8, 4->5. */
  rotate_elem ( &aPyr, 1, 3 ) ;
  add_uns_refType ( &refPyr1A, &aPyr ) ;
  
  /* Type 2: 7, 3->5. */
  rotate_elem ( &aPyr, 1, 3 ) ;
  add_uns_refType ( &refPyr1A, &aPyr ) ;
  
  /* Type 3: 5, 2->5. */
  rotate_elem ( &aPyr, 1, 3 ) ;
  add_uns_refType ( &refPyr1A, &aPyr ) ;

  /* Refine two opposed edges on the quadrilateral. */
  for ( kVx = 0 ; kVx < elemType[aPyr.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  /* Type 8:  1:1->2. and 6:3->4. */
  add_uns_refType ( &refPyr2, &aPyr ) ;
  
  /* Type 9: 2:1->4 and 4:2->3. */
  rotate_elem ( &aPyr, 1, 3 ) ;
  add_uns_refType ( &refPyr2, &aPyr ) ;
#endif  


  /* Refine all edges. */
  for ( kVx = 0 ; kVx < elemType[aPyr.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  add_uns_refType ( &refPyr8, &aPyr ) ;
#endif

  return ;
}

/******************************************************************************

  init_uns_prism:
  Initialize refinement types for prism.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; add mark2/3
  9Jul11; reflect modification of elem_struct in initialiser.
  : conceived.

  Changes To:
  -----------
  RefType
*/

void init_uns_prism () {

  elem_struct aPrism = { 1, pri, 1, 0, 0, 0, 0, PvxPerm } ;

#ifdef ADAPT_HIERARCHIC
  /* Two opposed edges on a quad face: 1, 1->4 and 3, 2->3.*/
  const refType_struct refPrism2 =
  { pri,
    2, {{1,6}, {3,7}},
    0, {{0}}, 0,
    2, {{ pri, {0,1,7,6,4,5} },
        { pri, {6,7,2,3,4,5} }}
  } ;
  /* Three edges one all quad faces: 0, 1->2, 5, 3->4, 8, 5. 5->6. */
  const refType_struct refPrism3 =
  { pri,
    3, {{0,6}, {5,7}, {8,8}},
    0, {{0}}, 0,
    2, {{ pri, {0,6,7,3,8,5} },
        { pri, {6,1,2,7,4,8} } }
  } ;
  /* Six edges one all tri faces: */
  const refType_struct refPrism6 =
  { pri,
    6, {{2,6}, {1,7}, {7,8}, {4,9}, {3,10}, {6,11} },
    0, {{0}}, 0,
    4, {{ pri, {0,1,10,7,9,6} },
        { pri, {7,10,2,3,11,8} },
        { pri, {6,9,11,8,4,5} },
        { pri, {7,10,11,8,9,6} } }
  } ;
  /* All edges. */
  const refType_struct refPrism9 =
  { pri,
    9, {{0,15}, {1,7}, {2,6}, {3,13}, {4,12}, {5,16}, {6,14}, {7,8}, {8,17} },
    3, {{1,11}, {2,9}, {3,10}}, 0,
    8, {{ pri, {0,15,10,7,9,6} },
        { pri, {7,10,16,3,11,8} },
        { pri, {6,9,11,8,17,5} },
        { pri, {7,10,11,8,9,6} },
        { pri, {15,1,13,10,12,9} },
        { pri, {10,13,2,16,14,11} },
        { pri, {9,12,14,11,4,17} },
        { pri, {10,13,14,11,12,9} } }
  } ;

  int kVx ;

#ifdef USE_ALL_DIR_REF  
  /* Type 0:  1, 1->4 and 3, 2->3. */
  for ( kVx = 0 ; kVx < elemType[aPrism.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  add_uns_refType ( &refPrism2, &aPrism ) ;
  
  /* Type 1: 1, 1->3. */
  rotate_elem ( &aPrism, 4, 3 ) ;
  add_uns_refType ( &refPrism2, &aPrism ) ;
  
  /* Type 2: 2, 1->4. */
  rotate_elem ( &aPrism, 4, 3 ) ;
  add_uns_refType ( &refPrism2, &aPrism ) ;
#endif

  /* Type 3: edges 0, 1->2, 5, 3->4, 8, 5->6. */
  for ( kVx = 0 ; kVx < elemType[aPrism.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  add_uns_refType ( &refPrism3, &aPrism ) ;
  
  /* Type 4: six edges on tri faces, 2,1,7,4,3,6. */
  add_uns_refType ( &refPrism6, &aPrism ) ;

  /* Type 5: all. */
  add_uns_refType ( &refPrism9, &aPrism ) ;
#endif
  
  return ;
}


/******************************************************************************

  init_uns_hex:
  Initialize refinement types for hex.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  20Jan18; add mark2/3
  9Jul11; reflect modification of elem_struct in initialiser.
  : conceived.

  Changes To:
  -----------
  RefType
*/

void init_uns_hex () {

  elem_struct aHex = { 1, hex, 1, 0, 0, 0, 0, PvxPerm } ;

#ifdef ADAPT_HIERARCHIC
  /* Four opposed edges, 0,5,8,11. */
  const refType_struct refHex4 =
  { hex,
    4, { {0,8}, {5,11}, {8,9}, {11,10} },
    0, {{0}}, 0,
    2, { { hex, {0,8,11,3,4,9,10,7} },
         { hex, {8,1,2,11,9,5,6,10} } }
  } ;
  /* Eight edges on two opposed faces 0,1,3,4,5,8,9,10. */
  const refType_struct refHex8 =
  { hex,
    8, { {0,8}, {1,12}, {3,15}, {11,10}, {5,11}, {8,9}, {9,13}, {10,14} },
    2, { {5,16}, {6,17} }, 0,
    4, { { hex, {0,8,16,12,4,9,17,13} },
         { hex, {8,1,15,16,9,5,14,17} },
         { hex, {16,15,2,11,17,14,6,10} },
         { hex, {12,16,11,3,13,17,10,7}} }
  } ;
  /* All. */
  const refType_struct refHex12 =
  { hex,
    12, { {0,14}, {1,17}, {2,24}, {3,15}, {4,20}, {5,16},
	  {6,18}, {7,21}, {8,25}, {9,23}, {10,19}, {11,22} },
    6, { {1,13}, {2,9}, {3,10}, {4,11}, {5,8}, {6,12} }, 26,
    8, { { hex, {0,14,8,17,24,13,26,11} },
         { hex, {14,1,15,8,13,20,9,26} },
         { hex, {8,15,2,16,26,9,18,10} },
         { hex, {17,8,16,3,11,26,10,21} },
	 { hex, {24,13,26,11,4,25,12,23} },
	 { hex, {13,20,9,26,25,5,19,12} },
	 { hex, {26,9,18,10,12,19,6,22} },
	 { hex, {11,26,10,21,23,12,22,7} } }
  } ;

  int kVx ;

  /* Four opposed edges. */
  for ( kVx = 0 ; kVx < elemType[aHex.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  /* Type 0: 0,5,8,11. */
  add_uns_refType ( &refHex4, &aHex ) ;

  /* Type 1: 4,6,2,7. */
  rotate_elem ( &aHex, 3, 3 ) ;
  add_uns_refType ( &refHex4, &aHex ) ;
  
  /* Type 2: 3,10,1,9. */
  rotate_elem ( &aHex, 6, 3 ) ;
  add_uns_refType ( &refHex4, &aHex ) ;

  
  /* 8 edges. */
  for ( kVx = 0 ; kVx < elemType[aHex.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  /* Type 3: edges 0,1,3,4,5,8,9,10. */
  add_uns_refType ( &refHex8, &aHex ) ;
  
  /* Type 4: 1,2,3,4,6,7,9,10. */
  rotate_elem ( &aHex, 3, 3 ) ;
  add_uns_refType ( &refHex8, &aHex ) ;
  
  /* Type 5: 0,2,4,5,6,7,8,11. */
  rotate_elem ( &aHex, 2, 3 ) ;
  add_uns_refType ( &refHex8, &aHex ) ;


  
  /* Type 6: all. */
  for ( kVx = 0 ; kVx < elemType[aHex.elType].mVerts ; kVx++ )
    PvxPerm[kVx] = Pvx[kVx] ;
  add_uns_refType ( &refHex12, &aHex ) ;
#endif
  
  return ;
}

/******************************************************************************

  add_fc2eg:
  For each edge of an element, list the adjacent faces.
  
  Last update:
  ------------
  15Jun98: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_fcOfEg ( edgeOfElem_struct *pEoE, const int side,
                  const int kFace, const int kEgInFc, const int mDim ) {

  /* There may be one face in 2D, two in 3D. */
  if ( !pEoE->kFcEdge[side] ) {
    /* This is an open entry. Fill it. */
    pEoE->kFcEdge[side] = kFace ;
    pEoE->kEgInFc[side] = kEgInFc ;
    return ( 1 ) ;
  }
  
  /* No open slot was found. */
  printf ( " FATAL: no open slot for fc2eg in add_fcOfEg.\n" ) ;
  return ( 0 ) ;
}



/******************************************************************************

  fill_edgeOfFace:
  Calculate the list of edges on each face using edgeOfElem and faceOfElem. The
  edges are ordered for each face such that edge 0 runs from the forming vx 0
  of that face to vx 1, etc.
  
  Last update:
  ------------
  25Oct99; reorganise kAttEdge.
  : conceived.
  
  Changes To:
  -----------
  edgeOfFace[]
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fill_edgeOfFace () {
  
  elemType_struct *PelT ;
  faceOfElem_struct *PFoE ;
  edgeOfElem_struct *PEoE ;
  int kFace, kVx, kEdge, mEdgesFound, nVxF1, nVxF2, nVxE1, nVxE2,
             returnVal, mVxFace, e1onFace, e2onFace, mAttEdges ;
  elType_e nElT ;
  returnVal = 1 ;
  
  /* Find the list of edges for each face. */
  for ( nElT = tri ; nElT <= hex ; nElT++ ) {
    PelT = ( elemType_struct * ) elemType + nElT ;

    /* Reset all eg2fc pointers. */
    for ( kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ ) {
      PEoE = PelT->edgeOfElem + kEdge ;
      for ( kFace = 0 ; kFace < PelT->mDim - 1 ; kFace++ )
        PEoE->kFcEdge[kFace] = 0 ;
    }

    /* Loop over all faces. */
    for ( kFace = 1 ; kFace <= PelT->mSides ; kFace++ ) {
      PFoE = PelT->faceOfElem + kFace ;
      mEdgesFound = 0 ;
      
      /* Loop over all edges of the face and match the edges.
         Note that in 2-D edges and faces are the same entities, but
	 differently numbered. A 2-D 'face' is thus just an edge
	 that does not form a closed loop. */
      mVxFace =PFoE->mVertsFace ;
      for ( kVx = 0 ; kVx < ( PelT->mDim == 3 ? mVxFace : 1 ) ; kVx++ ) {
        nVxF1 = PFoE->kVxFace[kVx] ;
	nVxF2 = PFoE->kVxFace[(kVx+1)%mVxFace] ;

	/* Loop over all edges on this element. */
	for ( kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ ) {
	  PEoE = PelT->edgeOfElem + kEdge ;
	  nVxE1 = PEoE->kVxEdge[0] ;
	  nVxE2 = PEoE->kVxEdge[1] ;

	  if ( nVxF1 == nVxE1 && nVxF2 == nVxE2 ) {
            /* List the face at the left the edge. Seen from the outside. */
            add_fcOfEg ( PEoE, 0, kFace, mEdgesFound, PelT->mDim ) ;

	    /* This is a match with the edge running counter-cw. Seen from the outside?*/
	    PFoE->kFcEdge[mEdgesFound] = kEdge ;
	    PFoE->edgeDir[mEdgesFound++] = 1 ;
            break ;
          }
	  else if ( nVxF1 == nVxE2 && nVxF2 == nVxE1 ) {
            /* List the face to the right of the edge. */
            add_fcOfEg ( PEoE, 1, kFace, mEdgesFound, PelT->mDim ) ;

	    /* This is a match with the edge running cw. */
	    PFoE->kFcEdge[mEdgesFound] = kEdge ;
	    PFoE->edgeDir[mEdgesFound++] = 0 ;
            break ;
          }
	}
      }

      /* Loop over all edges on this element and check whether the edge is an attached
	 edge that has one vertex that is not on the face and one vertex that is. */
      for ( kEdge = mAttEdges = 0 ; kEdge < PelT->mEdges ; kEdge++ ) {
	PEoE = PelT->edgeOfElem + kEdge ;
	nVxE1 = PEoE->kVxEdge[0] ;
	nVxE2 = PEoE->kVxEdge[1] ;

	for ( e1onFace = e2onFace = -1, kVx = 0 ; kVx < mVxFace ; kVx++ ) {
	  if ( nVxE1 == PFoE->kVxFace[kVx] ) e1onFace = kVx ;
	  if ( nVxE2 == PFoE->kVxFace[kVx] ) e2onFace = kVx ;
        }
	  
	if ( e1onFace > -1 && e2onFace == -1 ) {
	  /* This is an attached edge at the lower end. */
	  PFoE->kAttEdge[e1onFace] = kEdge ;
          PFoE->edgeAttAt[e1onFace] = 0 ;
        }
	else if ( e1onFace == -1 && e2onFace > -1 ) {
	  /* This is an attached edge at the lower end. */
	  PFoE->kAttEdge[e2onFace] = kEdge ;
          PFoE->edgeAttAt[e2onFace] = 1 ;
        }
      }

      
      if ( mEdgesFound != ( PelT->mDim == 3 ? mVxFace : 1 ) ) {
	printf ( " FATAL: counted %d edges for face %d in a %s in fill_edgeOfFace.\n",
		 mEdgesFound, kFace, PelT->name ) ;
	returnVal = 0 ; }
      else
	PelT->faceOfElem[kFace].mFcEdges = mEdgesFound ;
    }
  }

  return ( returnVal ) ;
}
/******************************************************************************

  make_edge_bitmask:
  Make a bitmask for each edge.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void make_edge_bitmask () {

  int mask, kEdge, kFace, nEl ;
  elemType_struct *PelT ;
  faceOfElem_struct *PFoE ;
  
  /* Make a bitmask for each edge. */
  for ( kEdge = 0, mask = 1 ; kEdge < MAX_EDGES_ELEM ; kEdge++, mask <<= 1 )
    bitEdge[kEdge] = mask ;

  /* Loop over all element types. */
  for ( nEl = tri ; nEl <= hex ; nEl++ ) {
    PelT = elemType + nEl ;

    /* Make a bitmask for all edges of each element. */
    PelT->allEdges = 0 ;
    for ( kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ )
      PelT->allEdges |= bitEdge[kEdge] ;

    /* Make a bitmask for each edge of each face of each element. */
    for ( kFace = 1 ; kFace <= PelT->mFaces ; kFace++ ) {
      PFoE = PelT->faceOfElem + kFace ;
      PFoE->bitEdgeFace = 0 ; 
      for ( kEdge = 0 ; kEdge < PFoE->mFcEdges ; kEdge++ )
	PFoE->bitEdgeFace |= bitEdge[ PFoE->kFcEdge[kEdge] ] ;
    }
  }
      
  return ;
}

/******************************************************************************

  make_parallel_edges:
  Make a list of parallel edges for each element. A parallel edge is one that
  shares no vertex with its parallel sibling but is attached to the same faces.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/


int get_attFc ( const elemType_struct *pElT, const int kEg, int kAttFc[3] ) {

  int mAttFc = 0, kFc, kkEg ;
  const faceOfElem_struct *pFoE ;

  /* This edge needs to be attached to two faces. */
  for ( kFc = 1 ; kFc <= pElT->mSides ; kFc++ ) {
    pFoE = pElT->faceOfElem + kFc ;
    
    for ( kkEg = 0 ; kkEg < pFoE->mVertsFace ; kkEg++ )
      if ( pFoE->kAttEdge[kkEg] == kEg )
        kAttFc[mAttFc++] = kFc ;
  }

  if ( mAttFc > 3 )
    printf ( " FATAL: unexpected %d attached faces in get_attFc.\n", mAttFc ) ;
  return ( mAttFc ) ;
}




void make_parallel_edges () {

  int kEg, kkEg, *kVx, *kkVx, mAttFc2, kAttFc[3], kAttFc2[3] ;
  elType_e elType ;
  elemType_struct *pElT ;
  edgeOfElem_struct *pEoE, *pEoE2 ;

  for ( elType = tri ; elType <= hex ; elType++ ) {
    pElT = elemType + elType ;

    for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
      pEoE = pElT->edgeOfElem + kEg ;
      pEoE->mParaEg = 0 ;
      kVx = pEoE->kVxEdge ;

      /* This edge needs to be attached to two faces. */
      if ( get_attFc ( pElT, kEg, kAttFc ) == 2 )

        /* Loop over all other edges, make sure they don't share a vertex but
           are attached to the same two faces. */
        for ( kkEg = 0 ; kkEg < pElT->mEdges ; kkEg++ ) {
          pEoE2 = pElT->edgeOfElem + kkEg ;
          kkVx = pEoE2->kVxEdge ;
          mAttFc2 = get_attFc ( pElT, kkEg, kAttFc2 ) ;

          if ( mAttFc2 == 2 &&
               !( kVx[0] == kkVx[0] || kVx[1] == kkVx[1] ||
                  kVx[0] == kkVx[1] || kVx[1] == kkVx[0] ) &&
               ( ( kAttFc[0] == kAttFc2[0] && kAttFc[1] == kAttFc2[1] ) ||
                 ( kAttFc[0] == kAttFc2[1] && kAttFc[1] == kAttFc2[0] ) ) )
            /* This is a parallel edge. */
            pEoE->kParaEg[ pEoE->mParaEg++ ] = kkEg ;
        }

      if ( pEoE->mParaEg > 4 )
        printf ( " FATAL: %d parallel edges for edge %d in a %s"
                 " in make_parallel_edges.\n", pEoE->mParaEg, kEg, pElT->name ) ;
    }
  }
  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  init_slidingPlanePair:
*/
/*! Initialise two side of a sliding plane pair/non-conformal interface to zero.
 *
 */

/*
  
  Last update:
  ------------
  21Jul21: add vxMP2 initialisations.
  2Jun20; rename geoType to more uniquely spGeoType.
  5Apr20: cut out of init_uns.
  

  Input:
  ------
  *pSlidingPlanePair
  
*/

void init_slidingPlaneSide ( slidingPlaneSide_s *pSpS ) {

  pSpS->pUns = NULL ;
  pSpS->name[0] = '\0' ;
  pSpS->isMaster = 0 ;
  pSpS->pOtherSide = NULL ;
  pSpS->pBc = NULL ;
  pSpS->spGeoType = sp_no_const ;
  pSpS->mLines = 0 ;
  pSpS->prh = NULL ;
  int kDim ;
  for ( kDim = 0 ; kDim < MAX_DIM ; kDim++ ) 
    pSpS->xArcRef[kDim] = 0. ;
  pSpS->pspLine = NULL ;

  pSpS->mVxMP = 0 ;
  pSpS->pnVxMP2nVx = NULL ;
  pSpS->pnVxMP2lineLower = NULL ;
  pSpS->pwtnVxMPlineLower = NULL ;
 

  return ;
}


/******************************************************************************

  init_uns:
  Initialize unstructured tables.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  1Apr19; rename to init_uns_core_data to enable an init_uns for uns_s.
  6Jan15; rename pUnsInit to more descriptive pArrFamUnsInit
  29Aug11; use ADAPT_REF pragma.
  : conceived.
  
*/

void init_uns_core_data () {


  pArrFamUnsInit = make_arrFam ( "uns_init" ) ;
  
#ifdef ADAPT_HIERARCHIC
  int mCh ;
  /* There is only one buffer type. */
  for ( mCh = 0 ; mCh < MAX_FACES_ELEM*MAX_CHILDS_FACE+1 ; mCh++ ) {
    bufferType[mCh].refOrBuf = buf ;
    bufferType[mCh].mChildren = mCh ;
  }
#endif
  
  /* Get the edges of the faces. */
  fill_edgeOfFace () ;
  /* Make a bitmask for each edge. */
  make_edge_bitmask () ;
  /* Make a list of parallel edges for quad, prism and hex. */
  make_parallel_edges () ;
    
  /* Define standard elements. For the time being just refinement patterns. */
  init_uns_tri () ;
  init_uns_quad () ;
  init_uns_tet () ;
  init_uns_pyr () ;
  init_uns_prism () ;
  init_uns_hex () ;

  init_uns_vrtElem () ;


#ifdef DEBUG
  //check_minnorm () ;
  check_elem_is_convex () ;
#endif

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  init_uns:
*/
/*! Initialised content of an uns_s.
 *
 */

/*
  
  Last update:
  ------------
  26sep23; fix typo with useVxMark3
  14May20; intro HyVol ;
  29Apr20; intro Bvx2Vx
  16Apr20; tidy up, treat link to pGrid first.
  11Jul19; init all fields of uns_s.
  22May19; intro isBuffered.
  2Apr19; moved here from uns_meth.c
  1Apr19: extracted from make_uns.
  

  Input:
  ------
  pUns: grid.
  pGrid: if nonzero, link to this grid_s.

  
*/

void init_uns ( uns_s *pUns, grid_struct *pGrid  ) {

  pUns->pFam = NULL ;


  if ( pGrid ) {
    /* Grid is provided, add this pUns. */
    pGrid->uns.type = uns ;
    pGrid->uns.pUns = pUns ;
    pGrid->uns.pVarList = &(pUns->varList) ;
    pGrid->uns.mDim = pUns->mDim ;
    pUns->nr = pGrid->uns.nr ;
    pUns->pGrid = pGrid ;
  }
  else {
    pUns->pGrid = NULL ;
    pUns->nr = 0 ; // overwritten later.
  }

  pUns->validGrid = 1 ;
  pUns->adapted = 0 ;
  pUns->specialTopo = noTopo ;

  pUns->mDim = 0 ;
  pUns->llBox[0] = -TOO_MUCH ;
  pUns->llBox[1] = -TOO_MUCH ;
  pUns->llBox[2] = -TOO_MUCH ;
  pUns->urBox[0] = TOO_MUCH ;
  pUns->urBox[1] = TOO_MUCH ;
  pUns->urBox[2] = TOO_MUCH ;
  pUns->llBoxCyl[0]= TOO_MUCH ;
  pUns->llBoxCyl[1] = TOO_MUCH ;
  pUns->urBoxCyl[0] = -TOO_MUCH ;
  pUns->urBoxCyl[1] = -TOO_MUCH ;
  pUns->hMin = TOO_MUCH ;
  pUns->hMax = -TOO_MUCH ;
  pUns->volElemMin = TOO_MUCH ;
  pUns->pMinElem = NULL ;
  pUns->volElemMax = -TOO_MUCH ;
  pUns->pMaxElem = NULL ;
  pUns->volDomain = -TOO_MUCH ;

  pUns->epsOverlap = 1.e-25 ;
  pUns->epsOverlapSq = 1.e-50 ;
  
  pUns->mChunks = 0 ;
  pUns->ppChunk = NULL ;
  pUns->pRootChunk = NULL ;
  pUns->mElemsAlloc = 0 ;
  pUns->mVertsAlloc = 0 ;

  pUns->mElemsNumbered = 0 ;
  int k ;
  for ( k = 0 ; k < MAX_ELEM_TYPES ; k++ )
    pUns->mElemsOfType[k] = 0 ;
  for ( k = 0 ; k < MAX_DRV_ELEM_TYPES ; k++ )
    pUns->mElems_w_mVerts[k] = 0 ;
  pUns->mVertsNumbered = 0 ;
  pUns->nHighestElemNumber = 0 ;
  pUns->nHighestVertNumber = 0 ;
  pUns->pVxColor = NULL ;

  
  /* Mark use. */
  int kMark ;
  for ( kMark = 0 ; kMark < SZ_ELEM_MARK ; kMark++ ) {
    pUns->useElemMark[kMark] = 0 ;
    pUns->useElemMarkBy[kMark][0] = '\0' ;
  }

   /* Flag activity. */
  pUns->vxFlag1Active = 0 ;
  pUns->vxFlag1UsedBy[0] = '\0' ;

  pUns->useVxMark = 0 ;
  pUns->useVxMarkBy[0] = '\0' ;
  pUns->useVxMark2 = 0 ;
  pUns->useVxMark2By[0] = '\0' ;
  pUns->useVxMark3 = 0 ;
  pUns->useVxMark3By[0] = '\0' ;
 
  
  
  /* Variables. */
  varList_s *pVL = &(pUns->varList) ;
  pVL->mUnknowns = pVL->mUnknFlow = 0 ;
  pVL->varType = noVar ;
  var_s *pVar = pVL->var ;
  for ( k = 0 ; k < MAX_UNKNOWNS ; k++ ) {
    pVar[k].name[0] = '\0' ;
    pVar[k].grp[0] = '\0' ;
    pVar[k].cat = noCat ;
    pVar[k].isVec = 0 ;
    pVar[k].flag = 0 ;
  }


  /* Boundaries. */
  pUns->mBc = 0 ;
  pUns->mBcBnd = 0 ;
  pUns->ppBc = NULL ;
  pUns->ppRootPatchBc = NULL ;

  
  /* Temp list of bnd faces described by vertex lists. */
  pUns->mBndFcVx = 0 ;
  pUns->pBndFcVx = NULL ;
  
  /* Temp list of bnd nodes described by vertex lists. */
  pUns->pnBvx2Vx_fidx = NULL ;
  pUns->mBvx2Vx = 0 ;
  pUns->pnBvx2Vx = NULL ;
  
  pUns->pmVertBc = NULL ;
  pUns->pmBiBc   = NULL ;
  pUns->pmTriBc  = NULL ;
  pUns->pmQuadBc = NULL ;
  pUns->pmFaceBc = NULL ;

  pUns->mVertAllBc = 0 ;   
  pUns->mBiAllBc = 0 ;
  pUns->mTriAllBc = 0 ;
  pUns->mQuadAllBc = 0 ;
  pUns->mFaceAllBc = 0 ;   

  pUns->mVertAllInter = 0 ;
  pUns->mBiAllInter = 0 ;
  pUns->mTriAllInter = 0 ;
  pUns->mQuadAllInter = 0 ;
  pUns->mFaceAllInter = 0 ;

  
  /* Zones: */
  pUns->mZones = 0 ;
  for ( k = 0 ; k < MAX_ZONES+1 ; k++ )
    pUns->pZones[k] = NULL ;


  llEdge_s **ppllE  = ( llEdge_s ** ) &pUns->pllAdEdge ;
  *ppllE = NULL ;
  pUns->pAdEdge = NULL ;
  ppllE  = ( llEdge_s ** ) &pUns->pllEdge ;
  *ppllE = NULL ;
  pUns->isBuffered = 0 ;

  pUns->pllVxToElem = NULL ;

  pUns->pUnsFine = NULL ;
  pUns->pUnsCoarse = NULL ;
  pUns->pUnsFinest = pUns ;
  pUns->pEgLen = NULL ;
  pUns->mVxCollapseTo = 0 ;
  pUns->pnVxCollapseTo = NULL ;

  pUns->ppElContain = NULL ;
  pUns->pElContainVxWt = NULL ;


  /* Periodicity */
  pUns->mPerBcPairs = 0 ;
  pUns->pPerBc = NULL ;
  pUns->mPerVxPairs = 0 ;
  pUns->pPerVxPair = NULL ;
  pUns->multPer = 0 ;

  pUns->mSymmVx = 0 ;
  pUns->ppSymmVx = NULL ;

  pUns->mCutElems = 0 ;
  pUns->mCutBndFc = 0 ;

  pUns->restart.any.iniSrc = 0 ;


  /* Mixing plane. */
  pUns->mSlidingPlaneSides = 0 ;
  pUns->ppSlidingPlaneSide = NULL ;
  
  pUns->mSlidingPlanePairs = 0 ;
  pUns->pSlidingPlanePair = NULL ;

  for ( k = 0 ; k < MAX_ELEM_TYPES ; k++ )
    pUns->mStackFcOfType[k] = 0 ;
  pUns->pArrStackFc = NULL ;
  pUns->pStackFc = NULL ;

  pUns->mHyVol = 0 ;
  pUns->pHyVol->noGeo.type = noGeo ;

  pUns->pArrStackFcBeg = pUns->pArrStackFcEnd = NULL ; 
  pUns->pStackFcBeg = pUns->pStackFcEnd = NULL ;

  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  init_one_var:
*/
/*! Modifiy the value of one or more specific variables.
 *
 *
 */

/*
  
  Last update:
  ------------
  18dec24; change p in init x
  19Sep21: renamed from init_uns_var.
  

  Input:
  ------
  pUns
  keyword: one of those in the case statement.
  kVar: for case 'v', apply value to var number kVar
  value: for case 'v', apply this value.

  Changes To:
  -----------
  pUns->Punknown

  Returns:
  --------
  1 on failure, 0 on success
  
*/


int init_one_var ( uns_s *pUns, const char keyword[], const int kVar, const double value ) {
#undef FUNLOC
#define FUNLOC "in init_one_var"

  
  chunk_struct *Pchunk ;
  vrtx_struct *Pvrtx ;
  const int mDim = pUns->mDim ;
  const double *pFr ;
  double *pUn, x, y, r ;
  int k ;
# define SHOCK .5
# define PEAK 1.

  /* Allocate if needed. */
  if ( pUns->varList.varType == noVar )
    make_uns_sol ( pUns, mDim+2, "prim" ) ;


  int kp = pUns->varList.mUnknFlow-1 ;
  pFr = pUns->varList.freeStreamVar ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
          Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
      if ( ( pUn = Pvrtx->Punknown ) )
        switch ( keyword[0] ) {
        case '0': /* zero. */
          for ( k = 0 ; k <= kp ; k++ )
            pUn[k] = 0. ;
          break ;

        case '1': // unit
          for ( k = 0 ; k <= kp ; k++ )
            pUn[0] = 1. ;
          break ;
          
        case 'e': // rho=1
          pUn[kp] = 100. ;
          break ;
           
        case 'f': // freestream
          for ( k = 0 ; k <= kp ; k++ )
            pUn[k] = pFr[k] ;
          break ;
          
        case 'r': // rho=1
          pUn[0] = 1. ;
          break ;
         
        case 's': // some shock, discontinuity
          /* Keep density, w and energy. */
          pUn[0] = 1. ;
          pUn[2] = 0. ;
          if ( mDim == 3 ) {
            pUn[2] = pUn[3] = 0. ;
            pUn[4] = 99. ; }
          else {
            pUn[2] = 0. ;
            pUn[3] = 99. ; }
             
          if ( mDim == 4 && Pvrtx->Pcoor[2] > 0. )
            /* Set only the bottom values at z=0. 
               Leave others at {1., 3*0., 99.}*/
            pUn[1] = pUn[2] = 0. ;
          else
            { /* Set u,v. */
              x = Pvrtx->Pcoor[0] ; y = Pvrtx->Pcoor[1] ;
              if ( x < 1. )
                if ( y < .5 )
                  pUn[1] = PEAK*(x-1.)*(x-1.) ;
                else
                  { r = MIN( 1., sqrt( x*x + (y-.5)*(y-.5) ) ) ;
                    pUn[1] = PEAK*MAX( 0., (r-1.)*(r-1.) ) ;
                  }
              else
                if ( y <= 1.2 && x > 1.9 )
                  pUn[1] = SHOCK ;
                else if ( y > 1.2 && x > 1.1 )
                  { r = sqrt( (x-1.1)*(x-1.1) + (y-1.2)*(y-1.2) ) ;
                    if ( r > .8 )
                      pUn[1] = SHOCK ;
                    else
                      pUn[1] = 0. ;
                  }
                else
                  pUn[1] = 0. ;
            }
          break ;

        case 'v': // set the kVar-th var to value.
          pUn[kVar] = value ;
          break ;
          
        case 'x': // const, lin and quad in coor
          pUn[0] = 1. ;
          pUn[1] = Pvrtx->Pcoor[0] ;
          pUn[2] = Pvrtx->Pcoor[1] ;
          if ( mDim == 3 ) pUn[3] =  Pvrtx->Pcoor[2] ;
          pUn[mDim+1] = 1.e5 + pUn[1]*pUn[1] + pUn[2]+pUn[2]+ pUn[3]+pUn[3] ;
          break ;
 
        default :
          hip_err ( fatal, 0, "unrecognised option in init_uns_var." ) ;
        }


  
  // if ( keyword[0] == '0' ) {
  //   pFr = pUns->varList.freeStreamVar ;
  // 
  //   for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  //     for ( Pvrtx = Pchunk->Pvrtx+1 ;
  //           Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
  //       if ( ( pUn = Pvrtx->Punknown ) )
  //         for ( k = 0 ; k <= kp ; k++ )
  //           pUn[k] = 0. ;
  // }
  // 
  // 
  // else if ( keyword[0] == 'f' ) {
  //   pFr = pUns->varList.freeStreamVar ;
  // 
  //   for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  //     for ( Pvrtx = Pchunk->Pvrtx+1 ;
  //           Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
  //       if ( ( pUn = Pvrtx->Punknown ) )
  //         for ( k = 0 ; k <= kp ; k++ )
  //           pUn[k] = pFr[k] ;
  // }
  // 
  // 
  // else if ( keyword[0] == 'r' ) {
  //   for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  //     for ( Pvrtx = Pchunk->Pvrtx+1 ;
  //           Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
  //       if ( ( pUn = Pvrtx->Punknown ) )
  //         pUn[0] = 1. ;
  // }
  // 
  // else if ( keyword[0] == 'e' ) {
  //   for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  //     for ( Pvrtx = Pchunk->Pvrtx+1 ;
  //           Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
  //       if ( ( pUn = Pvrtx->Punknown ) )
  //         pUn[kp] = 100. ;
  // }
  // 
  // else if ( keyword[0] == 'x' ) {
  //   /* rho = 1., u = x, v = y. */
  //   pUns->varList.varType = prim ;
  // 
  //   for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  //     for ( Pvrtx = Pchunk->Pvrtx+1 ;
  //           Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
  //       if ( ( pUn = Pvrtx->Punknown ) ) {
  //         pUn[0] = 1. ;
  //         pUn[1] = Pvrtx->Pcoor[0] ;
  //         pUn[2] = Pvrtx->Pcoor[1] ;
  //         if ( mDim == 3 )
  //           pUn[3] =  Pvrtx->Pcoor[2] ;
  //         pUn[mDim+1] = 1. + pUn[1]*pUn[2] + pUn[2]+pUn[3]+ pUn[3]+pUn[1] ;
  //       }
  // }
  // 
  // else if ( strncmp ( keyword, "shock", 2 ) ) {
  //   /* Some shock. */
  //   for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  //     for ( Pvrtx = Pchunk->Pvrtx+1 ;
  //           Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
  //       if ( ( pUn = Pvrtx->Punknown ) ) {
  //         /* Keep density, w and energy. */
  //         pUn[0] = 1. ;
  //         pUn[2] = 0. ;
  //         if ( mDim == 3 ) {
  //           pUn[2] = pUn[3] = 0. ;
  //           pUn[4] = 99. ; }
  //         else {
  //           pUn[2] = 0. ;
  //           pUn[3] = 99. ; }
  //           
  //         if ( mDim == 4 && Pvrtx->Pcoor[2] > 0. )
  //           /* Set only the bottom values at z=0. Leave others at {1., 3*0., 99.}*/
  //           pUn[1] = pUn[2] = 0. ;
  //         else
  //           { /* Set u,v. */
  //             x = Pvrtx->Pcoor[0] ; y = Pvrtx->Pcoor[1] ;
  //             if ( x < 1. )
  //               if ( y < .5 )
  //                 pUn[1] = PEAK*(x-1.)*(x-1.) ;
  //               else
  //                 { r = MIN( 1., sqrt( x*x + (y-.5)*(y-.5) ) ) ;
  //                   pUn[1] = PEAK*MAX( 0., (r-1.)*(r-1.) ) ;
  //                 }
  //             else
  //               if ( y <= 1.2 && x > 1.9 )
  //                 pUn[1] = SHOCK ;
  //               else if ( y > 1.2 && x > 1.1 )
  //                 { r = sqrt( (x-1.1)*(x-1.1) + (y-1.2)*(y-1.2) ) ;
  //                   if ( r > .8 )
  //                     pUn[1] = SHOCK ;
  //                   else
  //                     pUn[1] = 0. ;
  //                 }
  //               else
  //                 pUn[1] = 0. ;
  //           }
  //       }
  // }
  
  return ( 1 ) ;
}



/******************************************************************************

  init_uns_var:
  Initialize variables.
  
  Last update:
  ------------
  19Sep21; intro 'v' option, init_one_var to directly set a var given by name.
  3Apr19; moved to init_uns.c
  2Mar18; revise form of init sol for arg 'x'. 
          move case inside loop.
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/


int init_uns_var ( uns_s *pUns, const char keyword[], const double value ) {

  int kVar = -1 ;
  if ( find_kVar ( &pUns->varList,kVar, keyword ) != -1 ) {
    /* Keyword match at least one var name. Invoke init_one_var for each of the
       matches. */

    kVar = -1 ;
    while ( ( kVar = find_kVar (  &pUns->varList, kVar, keyword ) ) != -1 ) {
      if ( verbosity > 1 ) {
        sprintf ( hip_msg, "setting variable %s to %g",pUns->varList.var[kVar].name, value ) ;
        hip_err ( info, 1, hip_msg ) ;
      }
      init_one_var ( pUns, "v", kVar, value ) ;
    }
  }
  else
    init_one_var ( pUns, keyword, kVar, value ) ;

  return (0);
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  init_match:
*/
/*! Set all elements in a match_s to zero.
 *
 */

/*
  
  Last update:
  ------------
  6May20: conceived.
  

  Changes To:
  -----------
  pMatch: pointer to the match control structure.
  
*/

void init_match ( match_s *pMatch ) {
#undef FUNLOC
#define FUNLOC "in init_match"

  pMatch->hasNumber  = 0 ;
  pMatch->matchZone  = 0 ;
  pMatch->matchMarks  = 0 ;
  pMatch->matchElType  = 0 ;
#ifdef ADAPT_HIERARCHIC
  pMatch->matchAdaptType = 0 ;
#endif
  pMatch->matchVxPer  = 0 ;

  return ;
}
