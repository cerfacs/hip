/*
   edge_manage.c
   List of edges formed by two vertices organized as an initial entry for
   the lowest numbered vertex of the edge with a linked list of all
   other edges with that lowest vertex.

   For each vertex the list of formed edges is linked such that the edges that
   have the vertex as their first node (ie. lower numbered) come first.

   Contains functions that manage the storage and debugging utilites.
 
   Last update:
   ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
   4Apr13; make all large counters ulong_t.
   3Nov99; intro make_llEdge_bnd.
   25Jul98; add get_sizeof, fix del_edge. Intro mEdgesUsed.
   19Dec97; derived from adEdge_manage.
   
   Contains:
   ---------
   make_llEdge
   free_llEdge
   make_llEdge_grid
   
   get_new_edge
   del_edge
   get_edge_vrtx
   get_elem_edge
   add_edge_vrtx
   add_elem_edge
   get_number_of_edges
   get_sizeof_llEdge
   show_edge

   listEdge
   listEdgeVx
   printEdge

*/

#include "cpre.h"
#include "cpre_uns.h"

/* Needed for hip_err. */
#include "proto.h"
#include "proto_uns.h"


extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;

/* Linked list of storage for the adapted/hanging edges. */
typedef struct {
  cpt_s cpVx[2] ;         /* the two vertices on that edge. */
  int nNxtEdge[2] ;       /* The next edge with each vertex. */
} edge_s ;

struct _llEdge_s {

  uns_s *pUns ;         /* An edge list is valid for one grid only. */

  int mChunks ;         /* Size of pn1stEgChk. */
  int *pmVerts ;        /* How many entry pointers with each chunk. */
  int **ppn1stEgChk ;   /* An entry edge for each vertex for each chunk. */

  ulong_t mEdges ;          /* Size of pEdge. */
  edge_s *pEdge ;       /* List of edges. */
  ulong_t nRootFreeEdge ;   /* Number of the first unused edge. */
  ulong_t mEdgesUsed ;      /* Try to keep track how many are used. */

  char **ppEdgeData ;   /* A field of data with each edge. Store it as a char to
                           enable pointer arithmetic. This pointer will point to a
                           fixed public location where the address of the field is
                           stored. */
  ulong_t dataSize ;     /* Length of the datafield for each edge. */
} ;

static int get_new_edge ( llEdge_s *pllEdge, void **ppEdgeData );
static int get_edge_vx ( const llEdge_s *pllEdge,
			 const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
			 int *pSwitch,
			 int *pn1stEdge, int *pnLstEdge1, int *pnFrstEdge2 );
/******************************************************************************

  make_llEdge:
  Create a list of edges. If pllEdge is given, the list is reallocated using
  the global REALLOC_FACTOR. If size is given, a list of that size is allocated.
  Else, pRootChunk is queried for the number of nodes and 2 egdes per node
  as for a regular quad grid are allocated.
  
  Last update:
  ------------
  4Apr13;  make all large counters ulong_t.
  16Jul98; modified llEdge_s.
  : conceived.
  
  Input:
  ------
  pUns:       If specified and size is not, used for approximate sizing.
  cptVxMax:   If given, sizing of the entry fields contains this cpt_s.
  mEdges:     If specified, that many edges are allocated.
  dataSize:   A datafield of dataSize is also allocated.
  pllEdge:    The list of edges.

  Changes To:
  -----------
  pllEdge:    The allocated memory in a struct.
  ppEdgeData: The allocated public data fields for each edge.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

llEdge_s *make_llEdge ( uns_s *pUns, cpt_s cptVxMax, ulong_t mEdges, ulong_t dataSize,
                        llEdge_s *pllEdge, void **ppEdgeData ) {
  
  int nEdge, mOldEdges, nChk, mChk, *pnVx, mVx, *pn1stEg ;
  edge_s *pEdge ;
  char *pData ;

  /*****
    Make a new list.
    *****/
  if ( !pllEdge ) {
    /* Malloc a linked list of edges and reset it. */
    pllEdge = arr_malloc ( "pllEdge in make_llEdge", pUns->pFam,
                           1, sizeof( *pllEdge )) ;
    pllEdge->pUns = pUns ;
    pllEdge->mChunks = 0 ;
    pllEdge->pmVerts = NULL ;
    pllEdge->ppn1stEgChk = NULL ;
    pllEdge->mEdges = pllEdge->mEdgesUsed = 0 ;
    mOldEdges = 0 ;
    pllEdge->pEdge = NULL ;

    pllEdge->dataSize = 0 ;
    if ( ppEdgeData ) *ppEdgeData = NULL ;
    pllEdge->ppEdgeData = ( char ** ) ppEdgeData ;
  }
  else 
    mOldEdges = pllEdge->mEdges ;

  
  
  /*****
    Add more space to the entry list.
    *****/
  if ( pllEdge->mChunks < ( mChk = pUns->mChunks ) ) {
    /* Realloc the list of entries. */
    pllEdge->ppn1stEgChk = arr_realloc ( "pllEdge->ppn1stEgChk in make_llEdge", pUns->pFam,
                                         pllEdge->ppn1stEgChk, mChk, 
                                         sizeof ( *pllEdge->ppn1stEgChk  ) ) ;
    pllEdge->pmVerts = arr_realloc ( "pllEdge->pmVerts in make_llEdge", pUns->pFam,
                                     pllEdge->pmVerts, mChk, sizeof ( *pllEdge->pmVerts ) ) ;

    /* Reset counters for the new chunks. */
    for ( nChk = pllEdge->mChunks ; nChk < mChk ; nChk++ ) {
      pllEdge->pmVerts[nChk] = 0 ;
      pllEdge->ppn1stEgChk[nChk] = NULL ;
    }
    pllEdge->mChunks = pUns->mChunks ;
  }
    
  /* Realloc entries for each chunk. */
  for ( nChk = 0 ; nChk < mChk ; nChk++ ) {
    mVx = pUns->ppChunk[nChk]->mVerts ;
    /* Make sure that cptVxMax is contained. */
    if ( cptVxMax.nr && cptVxMax.nCh == nChk ) mVx = MAX( mVx, cptVxMax.nr ) ;
    if ( pllEdge->pmVerts[nChk] < mVx ) {
      pn1stEg = pllEdge->ppn1stEgChk[nChk] =
        arr_realloc ( "pllEdge->ppn1stEgChk[nChk] in make_llEdge", pUns->pFam,
                      pllEdge->ppn1stEgChk[nChk], mVx+1, 
                      sizeof( *(pllEdge->ppn1stEgChk[nChk]) ) ) ;
      
      /* Reset the list of newly allocated entry edges. */
      for ( pnVx = pn1stEg+pllEdge->pmVerts[nChk]+1 ; pnVx <= pn1stEg+mVx ; pnVx++ )
        *pnVx = 0 ;

      pllEdge->pmVerts[nChk] = mVx ;
    }
  }

  
  /******
    Get the size of the list of edges.
    *****/
  if ( mEdges ) {
    /* A specific size is given. Use it. */
    if ( pllEdge && mEdges < pllEdge->mEdges && verbosity > 5 )
      printf ( " INFO: shrinking the list of edges from %"FMT_ULG" to %"FMT_ULG" in make_llEdge.\n",
	       pllEdge->mEdges, mEdges ) ;
  }
  else if ( mOldEdges ) {
    /* Resize. */
    mEdges = mOldEdges * REALLOC_FACTOR + 1 ;
    if ( verbosity > 5 )
      printf ( " INFO: realloc edge list in make_llEdge to %"FMT_ULG" edges.\n", mEdges ) ;
  }
  else
    /* No list for this chunk, yet. */
    mEdges = 100 ;


  
  /*****
    Add more space to the edge list.
    *****/
  if ( mOldEdges < mEdges ) {
    /* Note that pEdge[] starts with 1, the zero being used as nil-flag. */
    pllEdge->pEdge = arr_realloc ( "pllEdge->pEdge in make_llEdge",  pUns->pFam, pllEdge->pEdge,
                                   mEdges+1, sizeof( *pllEdge->pEdge ) ) ;
    
    /* The next free slot. */
    pllEdge->nRootFreeEdge = mOldEdges+1 ;
    /* Link a list of free slots. */
    for ( nEdge = mOldEdges+1 ; nEdge <= mEdges ; nEdge++ ) {
      pEdge = pllEdge->pEdge + nEdge ;
      pEdge->nNxtEdge[0] = nEdge+1 ;
      pEdge->cpVx[0] = pEdge->cpVx[1] = CP_NULL ;
    }
    /* New size. */
    pllEdge->mEdges = mEdges ;
  }


  
  /*****
    Change the size of the edge data.
    *****/
  if ( pllEdge->dataSize*mOldEdges != dataSize*mEdges ) {

    if ( dataSize ) {
      if ( !( *ppEdgeData = 
               arr_realloc ( "*ppEdgeData in make_llEdge", pUns->pFam, *ppEdgeData,
                             mEdges+1, dataSize ) ) ) {
        sprintf ( hip_msg, "failed to realloc adEdge list to %"FMT_ULG" in make_llEdge.\n",
               mEdges ) ;
        hip_err ( fatal, 0, hip_msg ) ;
        return ( 0 ) ; 
      }
    }
    else if ( pllEdge->dataSize && !dataSize ) {
      /* Free the previously allocated data-field. */
      arr_free ( *ppEdgeData ) ;
      *ppEdgeData = NULL ;
    }
    
    /* Remember ppEdgeData in pllEdge. */
    pllEdge->ppEdgeData = ( char ** ) ppEdgeData ;
    
    /* Reset the newly allocated data fields. */
    for ( pData = ( char *)*ppEdgeData + pllEdge->dataSize*(mOldEdges+1) ;
          pData < ( char *)*ppEdgeData + dataSize*(mEdges+1) ; pData++ )
      *pData = ( char ) 0 ;
    
    /* New size. */
    pllEdge->dataSize = dataSize ;
  }

  return ( pllEdge ) ;
}

/******************************************************************************

  free_llEdge:
  .
  
  Last update:
  ------------
  16Jul98; modified llEdge_s.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void free_llEdge ( llEdge_s ** ppllEdge )
{
  llEdge_s *pllE ;
  int nChk ;

  if ( !ppllEdge )
    /* No such list of edges. */
    return ;
  else if ( !( pllE = *ppllEdge ) )
    /* Empty list. */
    return ;

  arr_free ( pllE->pmVerts ) ;
  arr_free ( pllE->pEdge ) ;

  if (pllE->ppEdgeData) {
    arr_free ( *(pllE->ppEdgeData) ) ;
    *(pllE->ppEdgeData) = NULL ;
  }

  for ( nChk = 0 ; nChk < pllE->mChunks ; nChk++ )
    arr_free ( pllE->ppn1stEgChk[nChk] ) ;
  arr_free ( pllE->ppn1stEgChk ) ;

  arr_free ( *ppllEdge ) ;
  *ppllEdge = NULL ;
  
  return ;
}


/******************************************************************************

  make_llEdge_grid:
  Make a list of edges for a grid. This contiguous  list will contain all
  edges of the mesh.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems.
  13Jul99; Pass ppEdgeData down properly.
  : conceived.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns->pllEdge: The list of edges.
  pmEdges:       The number of edges.
  
  Returns:
  --------
  NULL on failure, pllEdge on success.
  
*/

llEdge_s *make_llEdge_grid ( uns_s *pUns, ulong_t *pmEdges,
                             ulong_t dataSize, void **ppEdgeData ) {
  
  const elemType_struct *pElT ;
  const int *kVxEg ;
  const vrtx_struct *pVx[2] ;
  const cpt_s cptVxNull = {0,0} ;
  llEdge_s *pllEdge = NULL ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElemBeg, *pElemEnd ;
  int kEdge, side, newEg ;
  
  /* Make some initial storage for the list. */
  free_llEdge ( ( llEdge_s ** ) &(pUns->pllEdge) ) ;
  if ( !( pllEdge = make_llEdge ( pUns, cptVxNull, 99, dataSize,
                                  pllEdge, ppEdgeData ) ) ) {
    printf ( " FATAL: could not make an edge list in make_llEdge_grid.\n" ) ;
    return ( NULL ) ; }

  /* Loop over all elements and add their edges to the list. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->term ) {
	pElT = elemType + pElem->elType ;
	for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
          kVxEg = pElT->edgeOfElem[kEdge].kVxEdge ;
          if ( pElem->PPvrtx[ kVxEg[0] ] != pElem->PPvrtx[ kVxEg[1] ] ) {
            /* This edge is not degenerate. Add it to the list. */
            if ( !add_elem_edge ( pllEdge, ppEdgeData,
                                  pElem, kEdge, pVx, pVx+1, &side, &newEg ) ) {
              printf ( " FATAL: could not add edge in make_llEdge_grid.\n" ) ;
              return ( NULL ) ; }
          }
        }
      }
  
  get_number_of_edges ( pllEdge, pmEdges ) ;

  return ( pllEdge ) ;
}


/******************************************************************************

  make_llEdge_bnd:
  Make a list of edges for the boundaries. This contiguous  list will contain all
  edges of the boundaries.
  
  Last update:
  ------------
  3Nov99; derived from make_llEdge_grid.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns->pllEdge: The list of edges.
  pmEdges:       The number of edges.
  
  Returns:
  --------
  NULL on failure, pllEdge on success.
  
*/

llEdge_s *make_llEdge_bnd ( uns_s *pUns, ulong_t *pmEdges,
                            ulong_t dataSize, void **ppEdgeData ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxEg ;
  const vrtx_struct *pVx[2] ;
  const cpt_s cptVxNull = {0,0} ;
  
  llEdge_s *pllEdge = NULL ;
  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  int kEdge, side, kkEg, newEg ;
  
  /* Make some initial storage for the list. */
  free_llEdge ( ( llEdge_s ** ) &(pUns->pllEdge) ) ;
  if ( !( pllEdge = make_llEdge ( pUns, cptVxNull, 99, dataSize,
                                  pllEdge, ppEdgeData ) ) )
    hip_err ( fatal, 0, "could not make an edge list in make_llEdge_bnd." ) ;

  /* Loop over all boundary faces and add their edges to the list. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->Pelem->term && pBndFc->nFace ) {
        pElem = pBndFc->Pelem ;
	pElT = elemType + pElem->elType ;
        pFoE = pElT->faceOfElem + pBndFc->nFace ;
	for ( kkEg = 0 ; kkEg < pFoE->mFcEdges ; kkEg++ ) {
          kEdge = pFoE->kFcEdge[kkEg] ;
          kVxEg = pElT->edgeOfElem[kEdge].kVxEdge ;
          if ( pElem->PPvrtx[ kVxEg[0] ] != pElem->PPvrtx[ kVxEg[1] ] ) {
            /* This edge is not degenerate. Add it to the list. */
            if ( !add_elem_edge ( pllEdge, ppEdgeData,
                                  pElem, kEdge, pVx, pVx+1, &side, &newEg ) )
              hip_err ( fatal, 0, "could not add edge in make_llEdge_bnd." ) ;
          }
        }
      }
  
  get_number_of_edges ( pllEdge, pmEdges ) ;

  return ( pllEdge ) ;
}


/******************************************************************************

  get_new_edge:
  Get the next free slot for an adaptive edge.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int get_new_edge ( llEdge_s *pllEdge, void **ppEdgeData ) {
  
  int nFreeEdge, mNewEdges ;
  const cpt_s cptVxNull = {0,0} ;
  
  /* Check if there is space in the list of edges. */
  if ( pllEdge->nRootFreeEdge > pllEdge->mEdges ) {
    mNewEdges = REALLOC_FACTOR*pllEdge->mEdges + 1 ;
    if ( !make_llEdge ( pllEdge->pUns, cptVxNull, mNewEdges, pllEdge->dataSize,
                        pllEdge, ppEdgeData ) ) {
      hip_err ( fatal, 0, "failed to realloc the list of edges in get_new_edge." ) ;
      return ( 0 ) ;
    }
  }

  /* Fill the next edge. */
  nFreeEdge = pllEdge->nRootFreeEdge ;
  pllEdge->nRootFreeEdge = pllEdge->pEdge[nFreeEdge].nNxtEdge[0] ;
  pllEdge->mEdgesUsed++ ; // Note: for debug only, not used.

  return ( nFreeEdge ) ;
}


/******************************************************************************

  del_edge:
  Delete an edge from the list.
  
  Last update:
  ------------
  19Sep19; fix bug with decrementing mEdgesUsed.
  4Apr13; make all large counters ulong_t.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void del_edge ( llEdge_s *pllEdge, ulong_t nDelEdge ) {

  edge_s *pEdge = pllEdge->pEdge, *pDelEdge = pEdge+nDelEdge, *pPrvEg ;
  cpt_s cpVx ;
  int *pNtry, iVx, side=-1 ;

  if ( nDelEdge > pllEdge->mEdges )
    /* Not a valid edge. */
    return ;
  else if ( !pDelEdge->cpVx[0].nr )
    /* Edge already empty. */
    return ;

  for ( iVx = 0 ; iVx < 2 ; iVx++ ) {
    cpVx = pDelEdge->cpVx[iVx] ;
    pNtry = pllEdge->ppn1stEgChk[cpVx.nCh] + cpVx.nr ;
  
    if ( *pNtry == nDelEdge )
      /* The edge to be deleted is the entry edge for that vertex. */
      *pNtry = pDelEdge->nNxtEdge[iVx] ;
    else {
      /* Intermediate link. Find the previous link. */
      pPrvEg = pEdge + *pNtry ;
      while ( pPrvEg->cpVx[0].nr ) {
        if ( pPrvEg->cpVx[0].nr == cpVx.nr && pPrvEg->cpVx[0].nCh == cpVx.nCh ) {
          /* Vertex 0 on this edge matches iVx on nDelEdge. */
          if ( pPrvEg->nNxtEdge[0] == nDelEdge ) {
            side = 0 ;
            break ;
          }
          else
            pPrvEg = pEdge+ pPrvEg->nNxtEdge[0] ;
        }
        else if ( pPrvEg->cpVx[1].nr == cpVx.nr && pPrvEg->cpVx[1].nCh == cpVx.nCh ) {
          /* Vertex 1 on this edge matches iVx on nDelEdge. */
          if ( pPrvEg->nNxtEdge[1] == nDelEdge ) {
            side = 1 ;
            break ;
          }
          else
            pPrvEg = pEdge+ pPrvEg->nNxtEdge[1] ;
        }
        else {
          hip_err ( fatal, 0, "inconsistent edge in del_edge." ) ;
          return ;
        }
      }
      
      pPrvEg->nNxtEdge[side] = pDelEdge->nNxtEdge[iVx] ;
    }
  }

  /* Prepend the edge to the list of empty edges. */
  pDelEdge->nNxtEdge[0] = pllEdge->nRootFreeEdge ;
  pDelEdge->cpVx[0] = pDelEdge->cpVx[1] = CP_NULL ;
  pllEdge->nRootFreeEdge = nDelEdge ;

  pllEdge->mEdgesUsed-- ; // Note: for debug only, not used.
  
  return ;
}


/******************************************************************************

  get_edge_vrtx:
  Scan the ordered/linked list of edges for a match given two vertices.
  Public function.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge:  the list of edges.
  pVrtx1/2: the two vertices of an edge, or the two diagonal vertices of a face.

  Changes To:
  -----------
  pVrtx1/2:     the two vertices ordered for node numbers.
  pSwitch:      if the two vertices have been switched for proper ordering.
  
  Returns:
  --------
  0 on no match, the number of the matching edge nEdge on match.
  
*/

int get_edge_vrtx ( const llEdge_s *pllEdge,
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch ) {
  
  int n1stEdge, nLstEdge1, nFrstEdge2 ;

  return ( get_edge_vx ( pllEdge, ppVrtx1, ppVrtx2, pSwitch,
			 &n1stEdge, &nLstEdge1, &nFrstEdge2 ) ) ;

}
/******************************************************************************

  get_elem_edge:
  Scan the ordered/linked list of edges for a match given two vertices.
  Public function.
  
  Last update:
  ------------
  14Feb16; return -1 if the edge is collapsed.
  : conceived.
  
  Input:
  ------
  pllEdge:  the list of edges.
  pElem:    the element
  kEdge:    and the edge sought.
  
  Changes To:
  -----------
  pVrtx1/2:     the two vertices ordered for node numbers.
  pSwitch:      if the two vertices have been switched for proper ordering.
  
  Returns:
  --------
  0 on no match, -1 for a collapsed edge, or the number of the matching edge nEdge on match.
  
*/

int get_elem_edge ( const llEdge_s *pllEdge,
                    const elem_struct *pElem, int kEdge,
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch ) {
  
  const int *kVxEdge ;
  int n1, nLstEdge1, nFrstEdge2 ;

  kVxEdge = elemType[ pElem->elType].edgeOfElem[kEdge].kVxEdge ;
	  
  /* The two forming nodes of the edge. */
  *ppVrtx1 = pElem->PPvrtx[ kVxEdge[0] ] ;
  *ppVrtx2 = pElem->PPvrtx[ kVxEdge[1] ] ;

  if ( *ppVrtx1 == *ppVrtx2 )
    /* Collapsed edge. */
    return ( -1 ) ;
  else 
    return ( get_edge_vx ( pllEdge, ppVrtx1, ppVrtx2, pSwitch,
                           &n1, &nLstEdge1, &nFrstEdge2 ) ) ;

}
/******************************************************************************

  get_edge_vx:
  Scan the ordered/linked list of edges for a match given two vertices.
  Private function.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge:  the list of edges.
  pVrtx1/2: the two vertices of an edge, or the two diagonal vertices of a face.

  Changes To:
  -----------
  pVrtx1/2:     the two vertices ordered for node numbers.
  pSwitch:      if the two vertices have been switched for proper ordering.
  pn1stEdge:   a pointer to the entry location for the lowest numbered vertex. 
  pnLstEdge1:  the index of the last edge found that was formed with pVrtx1 in the
                first position.
  pnFrstEdge2: the index of the first edge found that is formed with pVrtx1 as the
                second vertex. This is only filled if the search exhausts the list
	        with pVrtx1 as first vertex.
  
  Returns:
  --------
  0 on no match, the number of the matching edge nEdge on match.
  
*/

static int get_edge_vx ( const llEdge_s *pllEdge,
			 const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
			 int *pSwitch,
			 int *pn1stEdge, int *pnLstEdge1, int *pnFrstEdge2 ) {
  
  int nEdge, nCh2, nr2, side ;
  const vrtx_struct *pVx ;
  edge_s *pEdge ;

  *pn1stEdge = 0 ;
  *pSwitch = 0 ;

  if ( !*ppVrtx1 || !*ppVrtx2 )
    /* Such an edge does not exist. */
    return ( 0 ) ;

  if ( (*ppVrtx2)->vxCpt.nCh < (*ppVrtx1)->vxCpt.nCh ||
    /* Second chunk is lower numbered, switch the two. */
       ( (*ppVrtx2)->vxCpt.nCh == (*ppVrtx1)->vxCpt.nCh &&
	 (*ppVrtx2)->vxCpt.nr < (*ppVrtx1)->vxCpt.nr ) ) {
    /* Second vertex is lower numbered, switch the two. */
    pVx = *ppVrtx2 ; *ppVrtx2 = *ppVrtx1 ; *ppVrtx1 = pVx ; *pSwitch = 1 ; }

  nCh2 = (*ppVrtx2)->vxCpt.nCh ;
  nr2  = (*ppVrtx2)->vxCpt.nr ;

  /* Loop over all edges that have ppVrtx1 as the first vertex. */
  nEdge = *pnLstEdge1 = *pnFrstEdge2 = 0 ;
  while ( loop_edge_vx ( pllEdge, *ppVrtx1, pn1stEdge, &nEdge, &side ) ) {

    if ( side ) {
      /* This is the first edge with ppVrtx1 in the second position. */
      *pnFrstEdge2 = nEdge ;
      break ;
    }

    pEdge = pllEdge->pEdge + nEdge ;
    if ( pEdge->cpVx[1].nCh == nCh2 && pEdge->cpVx[1].nr == nr2 ) {
      /* This one matches the sought type edge/face. */
      return ( nEdge ) ;
    }
    *pnLstEdge1 = nEdge ;
  }

  /* No matching edge found. */
  return ( 0 ) ;
}

/******************************************************************************

  add_edge_vrtx:
  Add to the ordered/linked list of edges one given by two vertices.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge: The list of edges.
  ppEdgeData: The location of the pointer for the datafield associated
              with the edges.
  *ppVrtx1/2: the two vertices of an edge.

  Changes To:
  -----------
  *ppEdgeData: The datafield associated with the edges might be reallocated.
  *ppVrtx1/2:  The lower numbered one will be switched into first position.
  *pSwitch:    1 if the vertices are switched in the list.
  *pNew:       0 if the edge existed, 1 if it had to be created.
  
  Returns:
  --------
  0 on no match, 1 on match.
  
*/

int add_edge_vrtx ( llEdge_s *pllEdge, void **ppEdgeData, 
		    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
		    int *pSwitch, int *pNew ) {
  
  int nEdge, n1, *pn1, nr1, nCh1, n2, *pn2, nLast=0, nFirst=0, nLast2, side2 ;
  edge_s *pEdge ;
  cpt_s cpVxMax ;

  *pNew = 0 ;

  if ( !*ppVrtx1 || !*ppVrtx2 )
    /* Such an edge does not exist. */
    return ( 0 ) ;

  if ( ( nEdge = get_edge_vx ( pllEdge, ppVrtx1, ppVrtx2, pSwitch,
			       &n1, &nLast, &nFirst ) ) )
    /* The edge exists. */
    return ( nEdge ) ;
  
  /* Make a new edge. */
  if ( !( nEdge = get_new_edge ( pllEdge, ppEdgeData ) ) ) {
    hip_err ( fatal,0, "failed to add edge in add_edge_vrtx." ) ;
    return ( 0 ) ;
  }

  /* Find the end of the linked list for ppVrtx2 before the current edge is
     introduced. */
  nLast2 = side2 = 0 ;
  while ( loop_edge_vx ( pllEdge, *ppVrtx2, &n2, &nLast2, &side2 ) )
    ;


  /* Fill the edge. */
  pEdge = pllEdge->pEdge + nEdge ;
  pEdge->cpVx[0] = (*ppVrtx1)->vxCpt ;
  pEdge->cpVx[1] = (*ppVrtx2)->vxCpt ;

  /* Insert it into the linked list for pVrtx1 between the last edge with
     pVrtx1 as first and the first with pVrtx1 as the second vertex. */
  cpVxMax = max_cpt ( (*ppVrtx1)->vxCpt, (*ppVrtx2)->vxCpt ) ;
  if ( pllEdge->mChunks <= cpVxMax.nCh ||
       pllEdge->pmVerts[cpVxMax.nCh] < cpVxMax.nr ) {
    /* There is no entry slot for this vertex. realloc. */
    if ( !make_llEdge ( pllEdge->pUns, (*ppVrtx1)->vxCpt, pllEdge->mEdges,
                        pllEdge->dataSize, pllEdge, ppEdgeData ) ) {
      hip_err ( fatal,0,"could not reallocate llEdge entries in add_edge_vrtx." ) ;
      return ( 0 ) ; }
  }

  /* Main entry vertex, the lower numbered one. */
  nr1 = (*ppVrtx1)->vxCpt.nr ;
  nCh1 = (*ppVrtx1)->vxCpt.nCh ;
  pn1 = pllEdge->ppn1stEgChk[nCh1] + nr1 ;
  if ( !*pn1 ) {
  /* Create a en entry for this lowest numbered vertex, if there isn't any. */
    *pn1 = nEdge ;
    pEdge->nNxtEdge[0] = 0 ;
  }
  else if ( !nFirst ) {
    /* Append to the linked list, there are no edges with pVrtx1 as second. */
    pllEdge->pEdge[nLast].nNxtEdge[0] = nEdge ;
    pEdge->nNxtEdge[0] = 0 ;
  }
  else if ( nFirst == n1 ) {
    /* There are only edges with pVrtx1 as second. Prepend. */
    pllEdge->ppn1stEgChk[nCh1][nr1] = nEdge ;
    pEdge->nNxtEdge[0] = nFirst ;
  }
  else {
    /* Insert it after nLast. Note that the case of no edges with pVrtx1 as second
       is dealt with properly by nFirst = 0. */
    pllEdge->pEdge[nLast].nNxtEdge[0] = nEdge ;
    pEdge->nNxtEdge[0] = nFirst ;
  }
    
  /* Append the edge to the linked list for pVrtx2. */
  pn2 = pllEdge->ppn1stEgChk[ (*ppVrtx2)->vxCpt.nCh ] + (*ppVrtx2)->vxCpt.nr ;
  if ( *ppVrtx1 == *ppVrtx2 ) {
    /* Both vertices are the same. Have them point to the same edge on both ends. */
    pEdge->nNxtEdge[1] = pEdge->nNxtEdge[0] ;
  }
  else if ( !*pn2 ) {
    /* First entry for that vertex. */
    *pn2 = nEdge ;
    pEdge->nNxtEdge[1] = 0 ;
  }
  else {
    /* Append. */
    pllEdge->pEdge[nLast2].nNxtEdge[side2] = nEdge ;
    pEdge->nNxtEdge[1] = 0 ;
  }
  
  *pNew = 1 ;
  return ( nEdge ) ;
}

/******************************************************************************

  add_elem_edge:
  Add one edge of an element to the list.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge     The list of edges.
  pElem       The element and its edge to insert.
  kEdge

  Changes To:
  -----------
  ppVrtx1/2:  The two forming vertices of the edge.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_elem_edge ( llEdge_s *pllEdge, void **ppEdgeData,
                    const elem_struct *pElem, const int kEdge,
                    const vrtx_struct **ppVrtx1, const vrtx_struct **ppVrtx2,
                    int *pSwitch, int *pNew ) {
  
  static const int *kVxEdge ;

  kVxEdge = elemType[ pElem->elType ].edgeOfElem[kEdge].kVxEdge ;
  
  /* The two vertices of the edge. */
  *ppVrtx1 = pElem->PPvrtx[ kVxEdge[0] ] ;
  *ppVrtx2 = pElem->PPvrtx[ kVxEdge[1] ] ;

  /* Find the edge. */
  return ( add_edge_vrtx ( pllEdge, ppEdgeData, ppVrtx1, ppVrtx2,
                           pSwitch, pNew ) ) ;
}

/******************************************************************************

  loop_edge_vx:
  Loop over all the edges attached to a given vertex.
  
  Last update:
  ------------
  17Feb13; change print format specifiers to %"FMT_ULG" for ulong_t args. 
  : conceived.
  
  Input:
  ------
  pllEdge:    The list of edges.
  pVx:        The vertex.
  pnEdge:     The last edge looked at. If given as 0, the list is initialized.

  Changes To:
  -----------
  pn1stEdge:  The pointer to the root entry.
  pnEdge:     The number of the new edge, unchanged if the list is exhausted.
  pSide:      The side of the vertex on pnEdge.
  
  Returns:
  --------
  0 on an exhausted/empty list, 1 on a new edge.
  
*/

int loop_edge_vx ( const llEdge_s *pllEdge, const vrtx_struct *pVx,
		   int *pn1stEdge, int *pnEdge, int *pSide ) {
  
  int nCh1, nr1, nEdge ;
  const edge_s *pEdge ;
  
  nCh1 = pVx->vxCpt.nCh ;
  nr1  = pVx->vxCpt.nr ;

  if ( *pnEdge ) {
    /* Pick the next edge. */
    if ( *pSide )
      /* pVx is in the second position. */
      nEdge = pllEdge->pEdge[*pnEdge].nNxtEdge[1] ;
    else 
      nEdge = pllEdge->pEdge[*pnEdge].nNxtEdge[0] ;
  }
  else if ( nCh1 >= pllEdge->mChunks || nr1 > pllEdge->pmVerts[nCh1] )
    /* No entry space for this vertex, thus there cannot be any edges. */
    return ( 0 ) ;
  else
    /* Initialize: Travel the linked list and compare until a match is found. */
    nEdge = *pn1stEdge = pllEdge->ppn1stEgChk[nCh1][nr1] ;

  if ( nEdge ) {
    pEdge = pllEdge->pEdge + nEdge ;

    /* Check the ordering. */
    if ( pEdge->cpVx[0].nCh == nCh1 && pEdge->cpVx[0].nr == nr1 ) {
      *pSide = 0 ;
      *pnEdge = nEdge ;
      return ( 1 ) ;
    }
    else if ( pEdge->cpVx[1].nCh == nCh1 && pEdge->cpVx[1].nr == nr1 ) {
      /* This one matches the sought type edge/face. */
      *pSide = 1 ;
      *pnEdge = nEdge ;
      return ( 1 ) ;
    }
    else {
      /* No match. Did you tamper with *pnEdge? */
      *pSide = -1 ;
      *pnEdge = 0 ;
      if ( verbosity > 5 )
        sprintf ( hip_msg,
                 "inconsistent edge %d, %"FMT_ULG"/%"FMT_ULG" and %"FMT_ULG"/%"FMT_ULG" mismatch %d/%d.\n",
                 nEdge, pEdge->cpVx[0].nr, pEdge->cpVx[0].nr,
                 pEdge->cpVx[1].nr, pEdge->cpVx[1].nr, nCh1, nr1 ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 0 ) ;
    }
  }
  else
    /* No or no more edges. Note that this leaves pnEdge and pSide unchanged. */
    return ( 0 ) ;
}

/******************************************************************************

  get_number_edges:
  .
  
  Last update:
  ------------
  4Apr13; make all large counters ulong_t.
  : conceived.
  
  Input:
  ------
  pllEdge: The list of edges


  Changes To:
  -----------
  pnLstEdge: the index of the last used edge
  
  Returns:
  --------
  The number of used edges, mEdges.
  
*/

ulong_t get_number_of_edges ( const llEdge_s *pllEdge, ulong_t *pnLstEdge ) {
  
  ulong_t mEdges = 0, nEdge ;
  edge_s *pEdge = pllEdge->pEdge ;

  *pnLstEdge = 0 ;

  for ( nEdge = 1 ; nEdge <= pllEdge->mEdges ; nEdge++ )
    if ( pEdge[nEdge].cpVx[0].nr && pEdge[nEdge].cpVx[1].nr ) {
      /* This edge is used. */
      mEdges++ ;
      *pnLstEdge = nEdge ;
    }
  
  return ( mEdges ) ;
}



/******************************************************************************

  get_sizeof_llEdge:
  Get the allocated size of pllEdge->pEdge.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge: The list of edges


  Changes To:
  -----------
  pnLstEdge: the index of the last used edge
  
  Returns:
  --------
  The number of used edges, mEdges.
  
*/

int get_sizeof_llEdge ( const llEdge_s *pllEdge )
{
  return ( pllEdge->mEdges ) ;
}



/******************************************************************************

  show_edge:
  Return the contents of a given edge.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge: The list of edges
  nEdge:   The edge sought.

  Changes To:
  -----------
  ppVrtx1:    The first vertex.
  ppVrtx2:    The second vertex.
  
  Returns:
  --------
  0 on invalid, 1 on filled edge.
  
*/

int show_edge ( const llEdge_s *pllEdge, const int nEdge,
		vrtx_struct **ppVrtx1, vrtx_struct **ppVrtx2 ) {
  
  edge_s *pEdge = pllEdge->pEdge + nEdge ;
  
  if ( nEdge > pllEdge->mEdges )
    /* No such edge. */
    return ( 0 ) ;

  if ( !pEdge->cpVx[0].nr || !pEdge->cpVx[1].nr )
    /* Edge is empty. */
    return ( 0 ) ;

  *ppVrtx1 = de_cptVx( pllEdge->pUns, pEdge->cpVx[0] ) ;
  *ppVrtx2 = de_cptVx( pllEdge->pUns, pEdge->cpVx[1] ) ;

  return ( 1 ) ;
}


/******************************************************************************/

void listEdge ( const uns_s *pUns, llEdge_s const * const pllEdge,
                void (*printData) ( const uns_s*, void const * const ) )
{
  const chunk_struct *pChunk ;
  const vrtx_struct *pVrtx ;

  if ( !pUns ) {
    printf ( " Empty grid.\n" ) ;
    return ; }
  else if ( !pllEdge ) {
    printf ( " Empty list of edges.\n" ) ;
    return ; }
  
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    for ( pVrtx = pChunk->Pvrtx+1 ; pVrtx < pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
      if ( pVrtx->number )
	listEdgeVx ( pUns, pllEdge, pVrtx, printData ) ;

  printf ( "\n" ) ;
  return ;
}

/******************************************************************************/

void listEdgeVx ( const uns_s *pUns, llEdge_s const * const pllEdge,
                  const vrtx_struct *pVrtx,
                  void (*printData) ( const uns_s*, void const * const ) )
{
  int nEdge = 0, side = 0, n2 ;

  if ( !pVrtx ) {
    printf ( " Empty vertex.\n" ) ;
    return ; }
  
  printf ( "\n vx: %"FMT_ULG" (%d:%"FMT_ULG"), %d\n",
	     pVrtx->number, pVrtx->vxCpt.nCh, pVrtx->vxCpt.nr, nEdge ) ;
  
  while ( loop_edge_vx ( pllEdge, pVrtx, &n2, &nEdge, &side ) )
    printEdge ( pUns, pllEdge, nEdge, printData ) ;

  return ;
}

/******************************************************************************/

void printEdge ( const uns_s *pUns, llEdge_s const * const pllEdge, int nEdge,
                 void (*printData) ( const uns_s*, void const * const pData ) )
{
  const edge_s *pEdge = pllEdge->pEdge + nEdge ;
  ulong_t nr1 = pEdge->cpVx[0].nr, nr2 = pEdge->cpVx[1].nr ;

  printf ( " edge %d, vx: %"FMT_ULG" -> %"FMT_ULG", nxt: %d, %d", nEdge,
	   ( nr1 ? pllEdge->pUns->ppChunk[pEdge->cpVx[0].nCh]->Pvrtx[nr1].number : 0 ),
	   ( nr2 ? pllEdge->pUns->ppChunk[pEdge->cpVx[1].nCh]->Pvrtx[nr2].number : 0 ),
	   pEdge->nNxtEdge[0], pEdge->nNxtEdge[1] ) ;

  if ( pllEdge->ppEdgeData && printData )
    printData ( pUns, *(pllEdge->ppEdgeData) + nEdge*pllEdge->dataSize ) ;

  printf ( "\n" ) ;
  
  return ;
}


