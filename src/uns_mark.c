/*
  file_name.c:
*/

/*! short descr. for doxygen
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  ; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern const elemType_struct elemType[] ;


/***************************************************************************

  PUBLIC

**************************************************************************/




/******************************************************************************

  reset_vx_mark:
  Reset all chunk and vertex marks.
  
  Last update:
  ------------
  3Apr  9; intro reset_vx_markN.
  6Sep18; split out _1chunk functions, so we can target chunks.
  20Jan18; intro elem_mark2/3.
  : conceived.
  
  Input:
  ------
  pUns/pChunk


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void reset_vx_mark_1chunk ( chunk_struct *pChunk ) {
  vrtx_struct *pVrtx ;
  pChunk->chunkMark = 0 ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    pVrtx->mark = 0 ;
  return ;
}

void reset_vx_mark ( uns_s *pUns ) {
  chunk_struct *pChunk ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    reset_vx_mark_1chunk ( pChunk );
  return ;
}


void reset_vx_mark2_1chunk ( chunk_struct *pChunk ) {
  vrtx_struct *pVrtx ;
  pChunk->chunkMark = 0 ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    pVrtx->mark2 = 0 ;
  return ;
}
void reset_vx_mark2 ( uns_s *pUns ) {
  chunk_struct *pChunk ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    reset_vx_mark2_1chunk ( pChunk );
  return ;
}


void reset_vx_mark3_1chunk ( chunk_struct *pChunk ) {
  vrtx_struct *pVrtx ;
  pChunk->chunkMark = 0 ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    pVrtx->mark3 = 0 ;
  return ;
}
void reset_vx_mark3 ( uns_s *pUns ) {
  chunk_struct *pChunk ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    reset_vx_mark3_1chunk ( pChunk );
  return ;
}

void reset_vx_mark_all_1chunk ( chunk_struct *pChunk ) {
  vrtx_struct *pVrtx ;
  pChunk->chunkMark = 0 ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    pVrtx->mark = pVrtx->mark2 = pVrtx->mark3 = 0 ;

  return ;
}
void reset_vx_mark_all ( uns_s *pUns ) {
  chunk_struct *pChunk ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    reset_vx_mark_all_1chunk ( pChunk ) ;
  return ;
}



void reset_1chunk_vx_mark_k ( chunk_struct *pChunk, const int kMark ) {
  vrtx_struct *pVrtx ;
  pChunk->chunkMark = 0 ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    reset_vrtx_mark_k ( pVrtx, kMark ) ;
  return ;
}
void reset_all_vx_mark_k ( uns_s *pUns, const int kMark ) {
  chunk_struct *pChunk ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    reset_1chunk_vx_mark_k ( pChunk, kMark );
  return ;
}

void reset_1chunk_vx_mark_k ( chunk_struct *pChunk, const int kMark ) ;
void reset_all_vx_mark_k ( uns_s *pUns, const int kMark ) ;


void reset_vx_per_1chunk ( chunk_struct *pChunk ) {
  vrtx_struct *pVrtx ;
  pChunk->chunkMark = 0 ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    pVrtx->per = 0 ;
  return ;
}
void reset_vx_per ( uns_s *pUns ) {
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    reset_vx_per_1chunk ( pChunk ) ;
  return ;
}


void check_vx_mark_1chunk ( chunk_struct *pChunk ) { 
  vrtx_struct *pVrtx ;
  for ( pVrtx = pChunk->Pvrtx + 1 ;
        pVrtx <= pChunk->Pvrtx + pChunk->mVerts ; pVrtx++ )
    if ( pVrtx->number && pVrtx->mark )
      printf ( " Found mark for vertex %"FMT_ULG"\n", pVrtx->number ) ;
  return ;
}
void check_vx_mark ( uns_s *pUns ) { 
  /* Check mark. */
  chunk_struct *pChunk ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx2 ;
  int nBeg, nEnd ;
  pChunk = NULL ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    check_vx_mark_1chunk ( pChunk ) ;
  return ;
}


void reset_vx_markN ( uns_s *pUns, const int markNo ) {
  switch ( markNo ) {
  case 0:
  case 1:
    reset_vx_mark ( pUns );
    break ;
  case 2:
    reset_vx_mark2 ( pUns );
    break ;
  case 3:
    reset_vx_mark3 ( pUns );
    break ;
  default:
    hip_err ( fatal, 0, "no such mark in reset_vx_markN." ) ;
    return ;
  }
}




/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  reserve_vxMark/ release_vxMark:
*/
/*! reserve/release the vertex/elem mark, check for clash in use of mark.
 *
 *
 */

/*
  
  Last update:
  ------------
  3Apr19; unify nomenclature, generic markN routines with markNo arg 
  15Dec18: conceived.
  

  Input:
  ------
  pUns: grid
  markNo: number of mark.
  useBy: name of reserving routine
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int reserve_vx_markN ( uns_s *pUns, const int markNo, const char* useBy ) {

  switch ( markNo ) {
  case 0:
  case 1:
    if ( pUns->useVxMark ) {
      sprintf ( hip_msg, "%s wants to use vx->mark,"
                " but this is already used by %s", useBy, pUns->useVxMarkBy ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else {
      strncpy ( pUns->useVxMarkBy, useBy, LINE_LEN ) ;
      pUns->useVxMark = 1 ;
    }
    break ;
  case 2:
    if ( pUns->useVxMark2 ) {
      sprintf ( hip_msg, "%s wants to use vx->mark2,"
                " but this is already used by %s", useBy, pUns->useVxMark2By ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else {
      strncpy ( pUns->useVxMark2By, useBy, LINE_LEN ) ;
      pUns->useVxMark2 = 1 ;
    }
    break ;
  case 3:
    if ( pUns->useVxMark3 ) {
      sprintf ( hip_msg, "%s wants to use vx->mark3,"
                " but this is already used by %s", useBy, pUns->useVxMark3By ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else {
      strncpy ( pUns->useVxMark3By, useBy, LINE_LEN ) ;
      pUns->useVxMark3 = 1 ;
    }
    break ;
  default:
    hip_err ( fatal, 0, "no such mark in reserve_vxMarkN" ) ;  
  }

  return ( 0 ) ;
}

int release_vx_markN ( uns_s *pUns, const int markNo ) {

  switch ( markNo ) {
  case 0:
  case 1:
    pUns->useVxMark = 0 ;
    break ;
  case 2:
    pUns->useVxMark2 = 0 ;
    break ;
  case 3:
    pUns->useVxMark3 = 0 ;
    break ;
  default:
    hip_err ( fatal, 0, "no such mark in reserve_vxMark" ) ;
  }
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_vx_mark_k:
*/
/*! set the kth mark of a vertex.
 */

/*
  
  Last update:
  ------------
  7Sep18: conceived.
  

  Input:
  ------
  pVx: vertex to have mark changed.
  kMark: index of mark to set
  
*/

void reset_vrtx_mark_k ( vrtx_struct *pVx, const int kMark ) {

  switch ( kMark ) {
  case 0:
  case 1:
    pVx->mark = 0 ;
    break ;
  case 2:
    pVx->mark2 = 0 ;
    break ;
  case 3:
    pVx->mark3 = 0 ;
    break ;
  default:
    hip_err ( fatal, 0, "invalid mark number in reset_vrtx_mark_k" ) ;
    return ;
  }
}



void set_vrtx_mark_k ( vrtx_struct *pVx, const int kMark ) {

  switch ( kMark ) {
  case 0:
  case 1:
    pVx->mark = 1 ;
    break ;
  case 2:
    pVx->mark2 = 1 ;
    break ;
  case 3:
    pVx->mark3 = 1 ;
    break ;
  default:
    hip_err ( fatal, 0, "invalid mark number in reset_vrtx_mark_k" ) ;
    return ;
  }
}

int check_vrtx_mark_k ( vrtx_struct *pVx, const int kMark ) {

  switch ( kMark ) {
  case 0:
  case 1:
    if ( pVx->mark == 1 )
      return ( 1 ) ;
    break ;
  case 2:
    if ( pVx->mark2 == 1 )
      return ( 1 ) ;
    break ;
  case 3:
    if ( pVx->mark3 == 1 )
      return ( 1 ) ;
    break ;
  default:
    hip_err ( fatal, 0, "invalid mark number in check_vrtx_mark_k" ) ;
    return ( 0 ) ;
  }
  // Not set.
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_vx_elem:
*/
/*! Mark all forming vertices of an element.
 *
 *
 */

/*
  
  Last update:
  ------------
  7Sep18; derived from mark_vx_elem.
  

  Input:
  ------
  pElem: the element
  nFace: its face
  kMark: 1..3, the vx mark to set.

  Returns:
  --------
  number of vx on the face.
  
*/

int set_vx_mark_face_k ( const elem_struct *pElem, const int nFace,
                     const int kMark ) {
  if ( !pElem || pElem->invalid )
    return ( 0 ) ;

  const faceOfElem_struct *pFoE = elemType[pElem->elType].faceOfElem + nFace ;
  
  int kVx ;
  for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ )
    set_vrtx_mark_k ( pElem->PPvrtx[ pFoE->kVxFace[kVx] ], kMark ) ; 
    
  return ( pFoE->mVertsFace ) ;
}





/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_vx_mark_k_nbc:
*/
/*! Set the kth vx mark for all nodes on a particular bc.
 *
 */

/*
  
  Last update:
  ------------
  21Jul21; intro pBc variant.
  7Sep18: conceived.
  

  Input:
  ------
  pUns
  nBc
  kMark: mark to set
  doReset: if true, reset all marks.
    
  Returns:
  --------
  mVxMarked = number of marked vx

  
*/

ulong_t set_vx_mark_k_nbc ( uns_s *pUns, const int nBc,
                            const int kMark, const int doReset ) {

  if ( doReset )
    reset_all_vx_mark_k ( pUns, kMark ) ;

  /* Loop over all faces, don't require to rebuild linked list of patches
     for each bc. */
  chunk_struct *pCh = NULL ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBndFc, *pBfBeg, *pBfEnd ;
  ulong_t mVxMarked = 0 ;
  while ( loop_bndFaces ( pUns, &pCh, &pBP, &pBfBeg, &pBfEnd ) ) {
    if ( pBP->Pbc->nr == nBc ) {
      for ( pBndFc = pBfBeg ; pBndFc <= pBfEnd ; pBndFc++ ) {
        if ( pBndFc->Pelem->number && pBndFc->nFace )
          mVxMarked += set_vx_mark_face_k ( pBndFc->Pelem, pBndFc->nFace, kMark ) ; 
      }
    }
  }

  return ( mVxMarked ) ;
}


ulong_t set_vx_mark_k_pbc ( uns_s *pUns, const bc_struct *pBc,
                            const int kMark, const int doReset ) {

  if ( doReset )
    reset_all_vx_mark_k ( pUns, kMark ) ;

  /* Loop over all faces, don't require to rebuild linked list of patches
     for each bc. */
  chunk_struct *pCh = NULL ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBndFc, *pBfBeg, *pBfEnd ;
  ulong_t mVxMarked = 0 ;
  while ( loop_bndFaces ( pUns, &pCh, &pBP, &pBfBeg, &pBfEnd ) ) {
    if ( pBP->Pbc == pBc ) {
      for ( pBndFc = pBfBeg ; pBndFc <= pBfEnd ; pBndFc++ ) {
        if ( pBndFc->Pelem->number && pBndFc->nFace )
          mVxMarked += set_vx_mark_face_k ( pBndFc->Pelem, pBndFc->nFace, kMark ) ; 
      }
    }
  }

  return ( mVxMarked ) ;
}




/******************************************************************************
  mark2_bndVx:   */

/*! Set the mark2 for all boundary vertices of a grid.
 *
 * primitive elements only.
 *
 */

/*
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  16De16; yikes! fixed bug with ppVx[k], which needs to be ppVx[ kVxFc[k] ] on the face.
  17Mar12: conceived.
  

  Input:
  ------
  pUns

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void mark2_bndVx ( uns_s *pUns ) {

  reset_vx_mark2 ( pUns ) ;

  chunk_struct *pChunk = NULL ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcFirst, *pBndFcLast, *pBF ;
  const elem_struct *pElem ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  int mVxFc, k ;
  vrtx_struct **ppVx ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcFirst, &pBndFcLast ) )
    for ( pBF = pBndFcFirst ; pBF <= pBndFcLast ; pBF++ )
      if ( pBF->Pelem && pBF->Pelem->number ) {

        pElem = pBF->Pelem ;
        pFoE = elemType[ pElem->elType ].faceOfElem + pBF->nFace ;
        kVxFc = pFoE->kVxFace ;
        mVxFc = pFoE->mVertsFace ;
        ppVx = pElem->PPvrtx ;
        for ( k = 0 ; k < mVxFc ; k++ ) 
          ppVx[ kVxFc[k] ]->mark2 = 1 ;
      }

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_vx_elem:
*/
/*! Mark all forming vertices of an element.
 *
 *
 */

/*
  
  Last update:
  ------------
  7May20; intro markN_vx_elem.
  20Jan18; create mark2/3 copies
  19Dec17: conceived.
  

  Input:
  ------
  pElem

  Returns:
  --------
  number of vx in the element.
  
*/

int mark_vx_elem ( elem_struct *pElem ) {

  int mVx = elemType[pElem->elType].mVerts ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  int kVx = 0 ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    ppVx[kVx]->mark = 1 ;
    
  return ( mVx ) ;
}
int mark2_vx_elem ( elem_struct *pElem ) {

  int mVx = elemType[pElem->elType].mVerts ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  int kVx = 0 ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    ppVx[kVx]->mark2 = 1 ;
    
  return ( mVx ) ;
}
int mark3_vx_elem ( elem_struct *pElem ) {

  int mVx = elemType[pElem->elType].mVerts ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  int kVx = 0 ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    ppVx[kVx]->mark3 = 1 ;
    
  return ( mVx ) ;
}

int markN_vx_elem ( elem_struct *pElem, const int kMark ) {
  
  int mVx = elemType[pElem->elType].mVerts ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  int kVx = 0 ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ )
    set_vrtx_mark_k ( ppVx[kVx], kMark ) ;
    
  return ( mVx ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_vx_elem_regions:
*/
/*! Mark all vertices and elements in two lists of regions.
 *
 *
 */

/*
  
  Last update:
  ------------
  27apr23; derived from mark_vx_elem_zones.
  

  Input:
  ------
  pUns: grid 
  mRegL: number of regions in L list
  iRegL: list of region identifiers in L list
  mRegU: number of regions in U list
  iRegU: list of region identifiers in U list
  useMark: if non-zero, regions are defined by element marks, otherwise zones.

  Output:
  -------
  *pmElemsZone: number of elements in those regions 
  *pmConnZone: number of connectivity entries in those regions
  *pmVertsZone: number of vertices in those regions
  *pmBndFcZone: number of vertices in those regions

  Returns:
  --------
  ret_s status.
  
*/

int mark_vx_elem_regions ( uns_s *pUns,
                           const int mRegL, const int iRegL[mRegL],
                           const int mRegU, const int iRegU[mRegU],
                           const int useMark,
                           ulong_t *pmElemsZone, ulong_t *pmConnZone,
                           ulong_t *pmVertsZone,
                           ulong_t *pmBndFcZone ) {
#undef FUNLOC
#define FUNLOC "in fun_name"

  // gcc 11.3 throws a segfault when compiling uns_copy with
  // a ret_s return type prototype for mark_vx_elem_regions.
  // hence return an int.
  //ret_s ret = ret_success () ;

  /* Reset marks. Release vx mark after fun return and then transform. */
  reserve_vx_markN ( pUns, 0, "mark_vx_elem_regions" ) ;
  reset_vx_markN ( pUns,0 ) ;
  int kElMrk = reserve_next_elem_mark ( pUns, "mark_vx_elem_regions" ) ;
  reset_all_elem_mark ( pUns, kElMrk ) ;

  /* Mark all elements in the zones. */
  int kReg ;
  chunk_struct *pCh ;
  elem_struct *pEl, *pElB, *pElE ;
  *pmElemsZone = 0 ;
  *pmConnZone = 0 ;
  int iReg ;
  int isMatch ;
  for ( kReg = 0 ; kReg < mRegL+mRegU ; kReg++ ) {
    iReg = ( kReg >= mRegL ? iRegU[kReg-mRegL] : iRegL[kReg] ) ;
    pCh = NULL ;
    while ( loop_elems ( pUns, &pCh, &pElB, &pElE ) ) 
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( pEl->number && !pEl->invalid ) {
          if ( useMark )
            isMatch = elem_has_marks ( pEl, 1, &iReg ) ;
          else
            isMatch = zone_match_list ( 1, &iReg, pEl->iZone ) ;
        
          if ( isMatch
#ifdef ADAPT_HIERARCHIC
               &&
               ( pUns->pllAdEdge ? pElem->leaf : 1 )
#endif
               ) {
            /* Add. */
            (*pmElemsZone)++ ;
            (*pmConnZone) += mark_vx_elem ( pEl ) ;
            set_elem_mark(pEl,kElMrk) ;
          }
        }
  }

  /* Mark and count all vertices in the zones. */
  pCh = NULL ;
  vrtx_struct *pVx, *pVxB, *pVxE ;
  int nB, nE ;
  *pmVertsZone = 0 ;
  while ( loop_verts ( pUns, &pCh, &pVxB, &nB, &pVxE, &nE ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ ) 
      if ( pVx->mark )
        (*pmVertsZone)++ ;


  /* Count all bnd faces in the zones. */
  pCh = NULL ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBf, *pBfB, *pBfE ;
  *pmBndFcZone = 0 ;
  while ( loop_bndFaces ( pUns, &pCh, &pBP, &pBfB, &pBfE ) )
    for ( pBf = pBfB ; pBf <= pBfE ; pBf++ ) {
      pEl = pBf->Pelem ;
      if ( pEl && elem_has_mark(pEl,kElMrk) && pBf->nFace ){
        // add
        (*pmBndFcZone)++ ;
      }
    }

  reset_all_elem_mark ( pUns, kElMrk ) ;
  release_elem_mark ( pUns, kElMrk ) ;
  
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_vx_elem_zones:
*/
/*! Mark all vertices and elements in two lists of zones.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  19Dec17: refactored from cp_uns_zones. 
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mark_vx_elem_zones ( uns_s *pUns0,
                         const int mZonesL, const int iZoneL[mZonesL],
                         const int mZonesU, const int iZoneU[mZonesU],
                         ulong_t *pmElemsZone, ulong_t *pmConnZone,
                         ulong_t *pmVertsZone,
                         ulong_t *pmBndFcZone ) {

  /* Reset marks. */
  reserve_vx_markN ( pUns0, 0, "mark_vx_elem_zones" ) ;
  reset_vx_markN ( pUns0,0 ) ;
  reserve_elem_mark ( pUns0, 0, "mark_vx_elem_zones" ) ;
  reset_all_elem_mark ( pUns0, 0 ) ;

  /* Mark all elements in the zones. */
  int kZ ;
  chunk_struct *pCh ;
  elem_struct *pEl, *pElB, *pElE ;
  *pmElemsZone = 0 ;
  *pmConnZone = 0 ;
  int iZ ;
  for ( kZ = 0 ; kZ < mZonesL+mZonesU ; kZ++ ) {
    iZ = ( kZ >= mZonesL ? iZoneU[kZ-mZonesL] : iZoneL[kZ] ) ;
    pCh = NULL ;
    while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) 
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( pEl->number && pEl->iZone == iZ ) {
          /* Add. */
          (*pmElemsZone)++ ;
          (*pmConnZone) += mark_vx_elem ( pEl ) ;
          pEl->mark = 1 ;
        }
  }

  /* Mark and count all vertices in the zones. */
  pCh = NULL ;
  vrtx_struct *pVx, *pVxB, *pVxE ;
  int nB, nE ;
  *pmVertsZone = 0 ;
  while ( loop_verts ( pUns0, &pCh, &pVxB, &nB, &pVxE, &nE ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ ) 
      if ( pVx->mark )
        (*pmVertsZone)++ ;


  /* Count all bnd faces in the zones. */
  pCh = NULL ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBf, *pBfB, *pBfE ;
  *pmBndFcZone = 0 ;
  while ( loop_bndFaces ( pUns0, &pCh, &pBP, &pBfB, &pBfE ) )
    for ( pBf = pBfB ; pBf <= pBfE ; pBf++ ) {
      if ( pBf->Pelem && pBf->Pelem->mark && pBf->nFace )
        (*pmBndFcZone)++ ;
    }



  
  return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark3_vx_elem_zones:
*/
/*! Mark3 all vertices on interfaces between four zones. 
    Uses also mark, mark2 in the process.
 *
 *
 */

/*
  
  Last update:
  ------------
  12jul19; derived from mark3_vx_elem_zones
  

  Input:
  ------
  pUns0: grid
  mZones: number of zones
  iZone[]: list of zones
  doReset: if non-zero, reset mark3, start from scratch. Otherwise, accumulate.
  doPer: also mark3 (fix) vx on periodic bc.

  Output:
  -------
  *pmElZoneF/L/C/U: number of elements in the fixed, lower, core, upper zones.
  *pmConnZoneF/L/C/U: number of conn entries in the fixed, lower, core, upper zones.
  *pmVxZoneF/L/C/U: number of vertices in the fixed, lower, core, upper zones.

  Returns:
  --------
  0 on success
  
*/

int mark3_vx_zones_per ( uns_s *pUns0, int mZones, int iZone[],
                         const int doReset, const int doPer ) {

  /* Reset marks. */
  if ( doReset )
    reset_vx_mark3 ( pUns0 ) ;



  /* One pass for each zone type. */
  int kZ ;
  chunk_struct *pCh ;
  elem_struct *pEl, *pElB, *pElE ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;
  for ( kZ = 0 ; kZ < mZones ; kZ++ ) {
    reset_vx_mark ( pUns0 ) ;
    reset_vx_mark2 ( pUns0 ) ;

    pCh = NULL ;
    while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) 
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( pEl->number ) {
          if ( pEl->iZone == iZone[kZ] ) {
            /* Mark all forming vertices of elems in zone iZone. */
            mark_vx_elem ( pEl ) ;
          }
          else {
            /* Mark2 all forming vertices of all elems not in zone iZone */
            mark2_vx_elem ( pEl ) ;
          }
        }

    /* Mark3 all vertices on zone interfaces. */
    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number && pVx->mark ) {
          pVx->mark3 = ( pVx->mark2 ? 1 : 0 ) ;
        }
  }


  if ( doPer ) {
    /* Mark3 all vertices on periodic boundaries. */ 
    int markN[2] = {0,2} ;
    reset_vx_mark ( pUns0 ) ;
    reset_vx_mark2 ( pUns0 ) ;
    int mBcPer=0, nBcPer[2*MAX_PER_PATCH_PAIRS] ;
    mark_vx_per ( pUns0, markN, &mBcPer, nBcPer, doReset ) ;

    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number &&
             ( vx_has_markN( pVx, markN[0] ||vx_has_markN( pVx, markN[1] ) ) ) )
          vx_set_markN ( pVx, 3 ) ;
  }

  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark3_vx_elem_mark_per:
*/
/*! Mark3 all vertices on interfaces between four regions given by elem marks. 
    Uses also vx mark, mark2 in the process.
 *
 *
 */

/*
  
  Last update:
  ------------
  25apr23; move mgmt of vx->mark,mark2 here instead of in calling fun.
  21jul21; derived from mark3_vx_zones_per
  

  Input:
  ------
  pUns0: grid
  mZones: number of zones
  iZone[]: list of zones
  doReset: if non-zero, reset mark3, start from scratch. Otherwise, accumulate.
  doPer: also mark3 (fix) vx on periodic bc.

  Output:
  -------
  *pmElZoneF/L/C/U: number of elements in the fixed, lower, core, upper zones.
  *pmConnZoneF/L/C/U: number of conn entries in the fixed, lower, core, upper zones.
  *pmVxZoneF/L/C/U: number of vertices in the fixed, lower, core, upper zones.

  Returns:
  --------
  0 on success
  
*/

int mark3_vx_elem_mark_per ( uns_s *pUns0, int mMarks, int iMark[],
                             const int doReset, const int doPer ) {

  /* Reset marks. */
  if ( doReset )
    reset_vx_mark3 ( pUns0 ) ;

  /* Working marks. */
  int kVxMarkWork[2] = {0,2} ;
  reserve_vx_markN ( pUns0, kVxMarkWork[0], "FUNLOC" ) ;
  reserve_vx_markN ( pUns0, kVxMarkWork[1], "FUNLOC" ) ;


  /* One pass for each zone type. */
  int kM ;
  chunk_struct *pCh ;
  elem_struct *pEl, *pElB, *pElE ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;
  for ( kM = 0 ; kM < mMarks ; kM++ ) {
    reset_vx_mark ( pUns0 ) ;
    reset_vx_mark2 ( pUns0 ) ;

    pCh = NULL ;
    while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) 
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( pEl->number ) {
          //if ( pEl->mark == iMark[kM] ) {
          if ( elem_has_mark ( pEl, iMark[kM] ) ) {
            /* Mark all forming vertices of elems in zone iMark. */
            mark_vx_elem ( pEl ) ;
          }
          else {
            /* Mark2 all forming vertices of all elems not in zone iMark */
            mark2_vx_elem ( pEl ) ;
          }
        }

    /* Mark3 all vertices on zone interfaces. */
    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number && pVx->mark ) {
          pVx->mark3 = ( pVx->mark2 ? 1 : 0 ) ;
        }
  }


  if ( doPer ) {
    /* Mark3 all vertices on periodic boundaries. */ 
    int markN[2] = {0,2} ;
    reset_vx_mark ( pUns0 ) ;
    reset_vx_mark2 ( pUns0 ) ;
    int mBcPer=0, nBcPer[2*MAX_PER_PATCH_PAIRS] ;
    mark_vx_per ( pUns0, markN, &mBcPer, nBcPer, doReset ) ;

    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number &&
             ( vx_has_markN( pVx, markN[0] ||vx_has_markN( pVx, markN[1] ) ) ) )
          vx_set_markN ( pVx, 3 ) ;
  }

  // No longer needed. vx->mark3 is still in use.
  release_vx_markN ( pUns0, kVxMarkWork[0] ) ;
  release_vx_markN ( pUns0, kVxMarkWork[1] ) ;

  return ( 0 ) ;
}





/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark3_vx_elem_zones:
*/
/*! Mark3 all vertices on interfaces between four zones. 
    Uses also mark, mark2 in the process.
 *
 *
 */

/*
  
  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  20Jan18: derived from mark_vx_elem_zones. 
  

  Input:
  ------
  pUns0: grid
  iZoneF/L/C/U: numbers of the fixed, lower, core, upper zones.

  Output:
  -------
  *pmElZoneF/L/C/U: number of elements in the fixed, lower, core, upper zones.
  *pmConnZoneF/L/C/U: number of conn entries in the fixed, lower, core, upper zones.
  *pmVxZoneF/L/C/U: number of vertices in the fixed, lower, core, upper zones.

  Returns:
  --------
  0 on success
  
*/

int mark3_vx_elem_zones ( uns_s *pUns0,
                          const int iZoneF,
                          const int iZoneL,
                          const int iZoneC,
                          const int iZoneU,
                          ulong_t *pmElZoneF, ulong_t *pmConnZoneF, ulong_t *pmVxZoneF,
                          ulong_t *pmElZoneL, ulong_t *pmConnZoneL, ulong_t *pmVxZoneL,
                          ulong_t *pmElZoneC, ulong_t *pmConnZoneC, ulong_t *pmVxZoneC,
                          ulong_t *pmElZoneU, ulong_t *pmConnZoneU, ulong_t *pmVxZoneU ) {

  /* Reset marks. */
  reset_vx_mark3 ( pUns0 ) ;

  /* Reset counters. */
  *pmElZoneF = *pmConnZoneF = *pmVxZoneF = 0 ;
  *pmElZoneL = *pmConnZoneL = *pmVxZoneL = 0 ;
  *pmElZoneC = *pmConnZoneC = *pmVxZoneC = 0 ;
  *pmElZoneU = *pmConnZoneU = *pmVxZoneU = 0 ;


  /* One pass for each zone type. */
  int iZone ;
  int zT ;
  chunk_struct *pCh ;
  elem_struct *pEl, *pElB, *pElE ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;
  ulong_t *pmElZone, *pmConnZone, *pmVxZone ;
  for ( zT = 0 ; zT < 4 ; zT++ ) {
    switch ( zT ) {
    case 3: // U
      iZone = iZoneU ;
      pmElZone = pmElZoneU ;
      pmConnZone = pmConnZoneU ;
      pmVxZone = pmVxZoneU ;
      break ;
    case 2: // C
      iZone = iZoneC ;
      pmElZone = pmElZoneC ;
      pmConnZone = pmConnZoneC ;
      pmVxZone = pmVxZoneC ;
      break ;
    case 1: // L
      iZone = iZoneL ;
      pmElZone = pmElZoneL ;
      pmConnZone = pmConnZoneL ;
      pmVxZone = pmVxZoneL ;
      break ;
    default: // Fixed
      iZone = iZoneF ;
      pmElZone = pmElZoneF ;
      pmConnZone = pmConnZoneF ;
      pmVxZone = pmVxZoneF ;
    }

    
    reset_vx_mark ( pUns0 ) ;
    reset_vx_mark2 ( pUns0 ) ;

    pCh = NULL ;
    while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) 
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( pEl->number ) {
          if ( pEl->iZone == iZone ) {
            (*pmElZone)++ ;
            /* Mark all forming vertices of elems in zone iZone. */
            (*pmConnZone) += mark_vx_elem ( pEl ) ;
          }
          else {
            /* Mark2 all forming vertices of all elems not in zone iZone */
            mark2_vx_elem ( pEl ) ;
          }
        }

    /* Mark3 all vertices on zone interfaces. */
    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number && pVx->mark ) {
          (*pmVxZone)++ ;
          pVx->mark3 = ( pVx->mark2 ? 1 : 0 ) ;
        }
  }

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_vx_per:
*/
/*! mark all vx on the l side with markNoL, u side with markNoU.
 *
 */

/*
  
  Last update:
  ------------
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  4Sep19: rename to nrBcPer to clarify meaning.
  15jul19; remove Pair from doc.
  9Jul19; rename to ADAPT_HIERARCHIC
  30Apr19; make work for ADAPT_REF
  3Apr19: conceived.
  

  Input:
  ------
  pUns:
  markN[2]: the vx mark used for lower and upper periodic bcs.
  doReset: reset the marks before marking for periodics.

  Output:
  -------
  *pmBcPer: number of periodic boundaries,
  nrBcPer[]: list of periodic boundaries using global numbering bc.nr.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mark_vx_per ( uns_s *pUns, int markN[2], int *pmBcPer, int nrBcPer[],
                  const int doReset ) {

  reset_vx_markN ( pUns, markN[0] ) ;
  reset_vx_markN ( pUns, markN[1] ) ;

  int kPer, kDir, nBc, kOpp ;
  const bc_struct *pBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  vrtx_struct **ppVx, *pVx, *pVxFc[2*MAX_VX_FACE+1] = {NULL} ;
  bndPatch_struct *PbndPatch = NULL ;
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  int mVx, kVx ;
  //#ifdef ADAPT_HIERARCHIC
  int fixDiag, diagDir ;
  //#endif
  int mVxMarked =0 ;
  for ( kPer = 0 ; kPer < pUns->mPerBcPairs ; kPer++ )
    for ( kDir = 0 ; kDir < 2 ; kDir ++ ) {
      pBc =  pUns->pPerBc[kPer].pBc[kDir] ;
      nrBcPer[ (*pmBcPer)++ ] = pBc->nr ;
      nBc = find_nBc( pUns, pBc ) ;
      kOpp = 1-kDir ;
    
      while ( loop_bndFaces_bc ( pUns, nBc, &PbndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) 
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
            /* Get the pointers to the vertices forming this face. */
            pElT = elemType + pBndFc->Pelem->elType ;
            pFoE = pElT->faceOfElem + pBndFc->nFace ;
            mVx = pFoE->mVertsFace ;
	

#ifdef ADAPT_HIERARCHIC
            if ( pUns->pllAdEdge ) {
              /* This might be a derived element.
                 Note that pVxFc can have empty mid-edge slots. */
              get_drvSide_aE ( pUns, pBndFc->Pelem, pBndFc->nFace,
                               &mVx, pVxFc, &fixDiag, &diagDir ) ;
              /* Scan all possible vertices. */
              mVx = 2*MAX_VX_FACE+1 ;
            }
            else {
#endif
              ppVx = pBndFc->Pelem->PPvrtx ;
              for ( kVx = 0 ; kVx < mVx ; kVx++ )
                pVxFc[kVx] = ppVx[ pFoE->kVxFace[kVx] ] ;
#ifdef ADAPT_HIERARCHIC
            }
#endif

        
            /* Mark all vertices of that face. */
            for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
              pVx = pVxFc[kVx] ;

              if ( vx_has_markN( pVx, markN[kOpp] ) ) {
                const bc_struct *pBcL = pUns->pPerBc[kPer].pBc[0] ;
                const bc_struct *pBcU = pUns->pPerBc[kPer].pBc[1] ;
                sprintf ( hip_msg, "two periodic pair bnd %d: %s and %d: %s"
                          " touch each other.\n"
                          " Can't separately mark them in mark_vx_perPair.\n"
                          " Make sure all periodic patches that touch have the same\n"
                          " type l or u.\n",
                          pBcL->nr, pBcL->text, pBcU->nr, pBcU->text ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
            
              if ( !vx_has_markN( pVx, markN[kDir]) ) {
                ++mVxMarked ;
                vx_set_markN ( pVx, markN[kDir] ) ;
              }
            }
          }
    }

  return ( mVxMarked ) ;
}



/******************************************************************************

  mark_uns_vertBc:
  Reset the vertex mark, then loop over all patches with this Bc and
  mark the vertices of those patches that are part of the grid pointed
  to by pUns.
  Disregard special vertices, e.g. ones on the xAxis if specialTopo is
  set to axiX. Add periodic siblings on non-periodic patches.
  
  Last update:
  ------------
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  6Sep18; rename reset_vx_mark.
  6Jan15; correct bracketing of ADAPT_REF block.
  16Dec08; into doAxis.
  15May06; intro sglNrm = 3, and removal.
  5Apr06; rework singleNormal conditions, intro wallBc.
  14Jan01; no real change, just a remark. Hydra doesn't need doPer any
           longer, the only calls with doPer come from oxd which should
           disappear.
  25Feb00; treat also derived elements. 
  11Feb98; add periodic siblings on non-periodic patches.
  17Dec98; for special patches, do not list singular vertices.
  27Jan98; fix bug in *PmVxBc, eliminate call to get_uns_face.
  4May96: conceived.
  
  Input:
  ------
  pUns:
  nBc:   The bc to mark for
  doPer: What to do about periodic boundary nodes. If doPer is switched
         on, periodic siblings are added for each non-periodic patch.
  doAxis: exclude nodes on a singular axis marked by pVx->singular.
  sglNrm: 0,1,2, meaning as per singleBndVxNormal
  PmBi/Tri/QuadFc: counters for edges, triangular and quad faces.
  
  Changes to:
  ----------
  pChunk->Pvrtx->mark: Mark set to 1 for each boundary vertex in this
                       patch.
  PmVxBc:              Number of vertices in this patch.
  PmBi/Tri/QuadFc:     number of triangular/quadrilateral
                       boundary faces in this patch.
  
*/

void mark_uns_vertBc ( uns_s *pUns, const int nBc, 
                       const int doPer, const int doAxis, const int sglNrm, 
                       int *pFoundPer, 
		       ulong_t *PmVxBc, ulong_t *PmBiFc, ulong_t *PmTriFc, ulong_t *PmQuadFc ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  spec_bc_e bc_e ;
  int nVx, mVx, nPerVx ;
  //#ifdef ADAPT_HIERARCHIC
  int fixDiag, diagDir ;
  //#endif
  bndFc_struct *PbndFc, *pBndFcBeg, *pBndFcEnd ;
  vrtx_struct **ppVx, *pVx, *pVxFc[2*MAX_VX_FACE+1] = {NULL} ;
  bndPatch_struct *PbndPatch = NULL ;

  reset_vx_mark ( pUns ) ;
  *PmVxBc = *PmBiFc = *PmTriFc = *PmQuadFc = *pFoundPer = 0 ;

  /* Is this a periodic boundary, needing special treatment? */
  bc_e = set_bc_e (  pUns->ppBc[nBc] ) ;
  
  
  while ( loop_bndFaces_bc ( pUns, nBc, &PbndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ ) 
      if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
	/* Get the pointers to the vertices forming this face. */
	pElT = elemType + PbndFc->Pelem->elType ;
	pFoE = pElT->faceOfElem + PbndFc->nFace ;
	mVx = pFoE->mVertsFace ;
	
	if ( mVx == 2 )
	  ++(*PmBiFc ) ;
	else if ( mVx == 3 )
	  ++(*PmTriFc ) ;
	else 
	  ++(*PmQuadFc ) ;

#ifdef ADAPT_HIERARCHIC
        if ( pUns->pllAdEdge ) {
          /* This might be a derived element.
             Note that pVxFc can have empty mid-edge slots. */
          get_drvSide_aE ( pUns, PbndFc->Pelem, PbndFc->nFace,
                           &mVx, pVxFc, &fixDiag, &diagDir ) ;
          /* Scan all possible vertices. */
          mVx = 2*MAX_VX_FACE+1 ;
        }
        else {
#endif
          ppVx = PbndFc->Pelem->PPvrtx ;
          for ( nVx = 0 ; nVx < mVx ; nVx++ )
            pVxFc[nVx] = ppVx[ pFoE->kVxFace[nVx] ] ;
#ifdef ADAPT_HIERARCHIC
        }
#endif

        
	/* Mark all vertices of that face. */
	for ( nVx = 0 ; nVx < mVx ; nVx++ ) {
	  pVx = pVxFc[nVx] ;
	  if ( pVx && !pVx->mark && !(doAxis && pVx->singular && bc_e == perBc ) ) {
            /* This vertex is not marked yet, and not a singular vertex on a
               periodic patch.
               In the case of a sglNrm, the ->mark2 field serves
               as a flag for bnd nodes listed with an earlier patch. */
            if ( sglNrm == 0 || bc_e == perBc ) {
              /* List again. */
              pVx->mark = 1 ;
              ++(*PmVxBc) ;
            }
            else if  ( sglNrm == 1 ) {
              if ( !pVx->mark2 ) {
                /* First listing of this bnd node. Don't list it again. */
                pVx->mark = pVx->mark2 = 1 ;
                ++(*PmVxBc) ;
              }
            }
            else if  ( sglNrm == 2 ) {
              if ( bc_e != wallBc) {
                /* This is not a wall bc, list it again. */
                pVx->mark = 1 ;
                ++(*PmVxBc) ;
              }
              else if ( !pVx->mark2 ) {
                /* First listing of this bnd node with a wall
                   bc. Don't list it again for another wall. */
                pVx->mark = pVx->mark2 = 1 ;
                ++(*PmVxBc) ;
              }
            }

          }
	}
      }


  if ( doPer && bc_e != perBc )
    /* Non-periodic patch. Loop over all vertices in the list of periodic ones,
       increment the counter if just one isn't listed, mark the sibling. */
    for ( nPerVx = 0 ; nPerVx < pUns->mPerVxPairs ; nPerVx++ )
      if ( ( pUns->pPerVxPair[nPerVx].In->mark && !pUns->pPerVxPair[nPerVx].Out->mark) ||
           ( !pUns->pPerVxPair[nPerVx].In->mark && pUns->pPerVxPair[nPerVx].Out->mark)) {
        ++(*pFoundPer) ;
        ++(*PmVxBc) ;
        pUns->pPerVxPair[nPerVx].In->mark = pUns->pPerVxPair[nPerVx].Out->mark = 1 ;
      }
  
  
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  markN_uns_vertBc:
*/
/*! set the nth mark for vertices on a particular bc.
 *
  Reset the vertex mark and mark1, then loop over all patches with this Bc and
  mark the vertices of those patches that are part of the grid pointed
  to by pUns.
  Disregard special vertices, e.g. ones on the xAxis if specialTopo is
  set to axiX. Add periodic siblings on non-periodic patches.
 *
 */

/*
  
  Last update:
  ------------
  17Apr20; test for adapt hierarchic by looking at pllAdEdge.
  9Jul19; rename to ADAPT_HIERARCHIC
  3Apr19; derived from mark_uns_vertBc

  
  Input:
  ------
  pUns:
  nMark: which mark to use
  nBc:   the bc to mark for
  doPer: What to do about periodic boundary nodes. If doPer is switched
         on, periodic siblings are added for each non-periodic patch.
  doAxis: exclude nodes on a singular axis marked by pVx->singular.
  sglNrm: 0,1,2, meaning as per singleBndVxNormal
  PmBi/Tri/QuadFc: counters for edges, triangular and quad faces.
  
  Changes to:
  ----------
  pChunk->Pvrtx->mark: Mark set to 1 for each boundary vertex in this
                       patch.
  PmVxBc:              Number of vertices in this patch.
  PmBi/Tri/QuadFc:     number of triangular/quadrilateral
                       boundary faces in this patch.

  Output:
  -------
    
*/


void markN_uns_vertBc ( uns_s *pUns, const int nMark, const int nBc, 
                       const int doPer, const int doAxis, const int sglNrm, 
                       int *pFoundPer, 
		       ulong_t *PmVxBc, ulong_t *PmBiFc, ulong_t *PmTriFc, ulong_t *PmQuadFc ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  spec_bc_e bc_e ;
  int nVx, mVx, nPerVx ;
  //#ifdef ADAPT_HIERARCHIC
  int fixDiag, diagDir ;
  //#endif
  bndFc_struct *PbndFc, *pBndFcBeg, *pBndFcEnd ;
  vrtx_struct **ppVx, *pVx, *pVxFc[2*MAX_VX_FACE+1] = {NULL} ;
  bndPatch_struct *PbndPatch = NULL ;

  reset_vx_mark ( pUns ) ;
  *PmVxBc = *PmBiFc = *PmTriFc = *PmQuadFc = *pFoundPer = 0 ;

  /* Is this a periodic boundary, needing special treatment? */
  bc_e = set_bc_e (  pUns->ppBc[nBc] ) ;
  
  
  while ( loop_bndFaces_bc ( pUns, nBc, &PbndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ ) 
      if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
	/* Get the pointers to the vertices forming this face. */
	pElT = elemType + PbndFc->Pelem->elType ;
	pFoE = pElT->faceOfElem + PbndFc->nFace ;
	mVx = pFoE->mVertsFace ;
	
	if ( mVx == 2 )
	  ++(*PmBiFc ) ;
	else if ( mVx == 3 )
	  ++(*PmTriFc ) ;
	else 
	  ++(*PmQuadFc ) ;

#ifdef ADAPT_HIERARCHIC
        if ( pUns->pllAdEdge ) {
          /* This might be a derived element.
             Note that pVxFc can have empty mid-edge slots. */
          get_drvSide_aE ( pUns, PbndFc->Pelem, PbndFc->nFace,
                           &mVx, pVxFc, &fixDiag, &diagDir ) ;
          /* Scan all possible vertices. */
          mVx = 2*MAX_VX_FACE+1 ;
        }
        else {
#endif
          ppVx = PbndFc->Pelem->PPvrtx ;
          for ( nVx = 0 ; nVx < mVx ; nVx++ )
            pVxFc[nVx] = ppVx[ pFoE->kVxFace[nVx] ] ;
#ifdef ADAPT_HIERARCHIC
        }
#endif

        
	/* Mark all vertices of that face. */
	for ( nVx = 0 ; nVx < mVx ; nVx++ ) {
	  pVx = pVxFc[nVx] ;
	  if ( pVx && !pVx->mark && !(doAxis && pVx->singular && bc_e == perBc ) ) {
            /* This vertex is not marked yet, and not a singular vertex on a
               periodic patch.
               In the case of a sglNrm, the ->mark2 field serves
               as a flag for bnd nodes listed with an earlier patch. */
            if ( sglNrm == 0 || bc_e == perBc ) {
              /* List again. */
              pVx->mark = 1 ;
              ++(*PmVxBc) ;
            }
            else if  ( sglNrm == 1 ) {
              if ( !pVx->mark2 ) {
                /* First listing of this bnd node. Don't list it again. */
                pVx->mark = pVx->mark2 = 1 ;
                ++(*PmVxBc) ;
              }
            }
            else if  ( sglNrm == 2 ) {
              if ( bc_e != wallBc) {
                /* This is not a wall bc, list it again. */
                pVx->mark = 1 ;
                ++(*PmVxBc) ;
              }
              else if ( !pVx->mark2 ) {
                /* First listing of this bnd node with a wall
                   bc. Don't list it again for another wall. */
                pVx->mark = pVx->mark2 = 1 ;
                ++(*PmVxBc) ;
              }
            }

          }
	}
      }


  if ( doPer && bc_e != perBc )
    /* Non-periodic patch. Loop over all vertices in the list of periodic ones,
       increment the counter if just one isn't listed, mark the sibling. */
    for ( nPerVx = 0 ; nPerVx < pUns->mPerVxPairs ; nPerVx++ )
      if ( ( pUns->pPerVxPair[nPerVx].In->mark && !pUns->pPerVxPair[nPerVx].Out->mark) ||
           ( !pUns->pPerVxPair[nPerVx].In->mark && pUns->pPerVxPair[nPerVx].Out->mark)) {
        ++(*pFoundPer) ;
        ++(*PmVxBc) ;
        pUns->pPerVxPair[nPerVx].In->mark = pUns->pPerVxPair[nPerVx].Out->mark = 1 ;
      }
  
  
  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  vrtx_is_marked:
*/
/*! Check if one of the three vertex flags is set. 
 *
 *
 */

/*
  
  Last update:
  ------------
  3Apr19; intro vx_set_markN, unify nomenclature for vrtx_has_markN
  17Dec18; fix c&p bug with mark3.
  20Jan18: conceived.
  

  Input:
  ------
  pVrtx
  mrkPos: 1 for ->mark, 2 for mark2, 3 for mark3.

  Returns:
  --------
  1 if set, 0 otherwise
  
*/

int vx_set_markN ( vrtx_struct *pVx, const int markNo ) {
  int retVal ;
  switch ( markNo) {
  case ( 3 ) :
    retVal = pVx->mark3 ;
    pVx->mark3 = 1 ;
    return ( retVal ) ;
  case ( 2 ) :
    retVal = pVx->mark2 ;
    pVx->mark2 = 1 ;
    return ( retVal ) ;
  default :
    retVal = pVx->mark ;
    pVx->mark = 1 ;
    return ( retVal ) ;
  }
}

int vx_has_markN ( const vrtx_struct *pVx, const int markNo ) {

  switch ( markNo) {
  case ( 3 ) :
    return ( pVx->mark3 ) ;
  case ( 2 ) :
    return ( pVx->mark2 ) ;
  default :
    return ( pVx->mark ) ;
  }
}





/******************************************************************************
 ************************** 
 ************************** Elements
 **************************
 ***************************************************************************/



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  reserve_elem_mark/ release_elem_mark:
*/
/*! reserve/release the elem mark, check for clash in use of mark.
 *
 *
 */

/*
  
  Last update:
  ------------
  21Apr20; extracted from uns_number.
  3Apr19; unify nomenclature, generic markN routines with markNo arg 
  15Dec18: conceived.
  

  Input:
  ------
  pUns: grid
  kMark: mark component to use.
  useBy: name of reserving routine
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/



void reserve_elem_mark ( uns_s *pUns, const int kMark, const char* useBy ) {

  if ( kMark >= SZ_ELEM_MARK ) 
    hip_err ( fatal, 0, "no such mark in reserve_elemMark" ) ;
    

  if ( pUns->useElemMark[kMark] ) {
    sprintf ( hip_msg, "%s wants to use elem->mark %d,"
              " but this is already used by %s", useBy, kMark, pUns->useElemMarkBy[kMark] ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else {
    strncpy ( pUns->useElemMarkBy[kMark], useBy, LINE_LEN ) ;
    pUns->useElemMark[kMark] = 1 ;
  }

  return ;
}


int reserve_next_elem_mark ( uns_s *pUns, const char* useBy ) {

  int kMark ;
  for ( kMark = 0 ; kMark < SZ_ELEM_MARK ; kMark++ ) {
    if ( !pUns->useElemMark[kMark] ) {
      strncpy ( pUns->useElemMarkBy[kMark], useBy, LINE_LEN ) ;
      pUns->useElemMark[kMark] = 1 ;
      break ;
    }
  }

  if ( kMark == SZ_ELEM_MARK ) 
    hip_err ( fatal, 0, "no free element mark found in reserve_next_elem_mark" ) ;
  
  return ( kMark ) ;
}

int release_elem_mark ( uns_s *pUns, const int kMark ) {

  if ( kMark >= SZ_ELEM_MARK ) 
    hip_err ( fatal, 0, "no such mark in reserve_elemMark" ) ;
    
  pUns->useElemMark[kMark] = 0 ;

  return ( 1 ) ;
}




/******************************************************************************
  reset_elem_mark:   */

/*! reset a given element mark field. 
 *
 * 
 *
 */

/*
  
  Last update:
  ------------
  21Apr20: conceived.
  

  Input:
  ------
  pUns: grid
  kMarkBeg/End: first/last mark component to reset.

*/

void reset_all_elem_mark_range ( uns_s *pUns, const int kMarkBeg, const int kMarkEnd ) {

  int kMarkB = MAX( kMarkBeg, 0 ) ;
  int kMarkE = MIN( kMarkEnd, SZ_ELEM_MARK-1 ) ;
  
  chunk_struct *pChunk = NULL ;
  elem_struct *pElemBeg, *pElem, *pElemEnd ;
  int kMark ;
  while ( loop_elems( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )

      for ( kMark = kMarkB ; kMark <= kMarkE ; kMark++ )
        set_elem_mark_val ( pElem, kMark, 0 ) ;


  return ;
}


void reset_all_elem_all_mark ( uns_s *pUns ) {
  reset_all_elem_mark_range ( pUns, 0, SZ_ELEM_MARK-1 ) ;
  return ;
}

void reset_all_elem_mark ( uns_s *pUns, const int kMark ) {
  reset_all_elem_mark_range ( pUns, kMark, kMark ) ;
  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  reset_elem_mark 2/3:
*/
/*! Reset element mark, mark2, mark3 or all of them.
 */

/*
  
  Last update:
  ------------
  6Sep18; intro _1chunk functions to selectively target chunks.
  20Jan18: extended from reset_elem_mark
  

  Input:
  ------
  pUns: grid
  
*/

void reset_elem_mark_1chunk ( chunk_struct *pChunk, const int kMark ) {
  elem_struct *pElem, *pElB = pChunk->Pelem+1, *pElE = pElB+ pChunk->mElems ;
  for ( pElem = pElB ; pElem <= pElE ; pElem++ )
    //pElem->mark = 0 ;
    reset_elem_mark ( pElem, kMark ) ;

  return ;
}


void reset_elem_all_mark_1chunk ( chunk_struct *pChunk ) {
  elem_struct *pElem, *pElB = pChunk->Pelem+1, *pElE = pElB+ pChunk->mElems ;
  for ( pElem = pElB ; pElem <= pElE ; pElem++ )
    //pElem->mark = pElem->mark2 = pElem->mark3 = 0 ;
    reset_elem_mark_range ( pElem, 0, SZ_ELEM_MARK-1 ) ;
  return ;
}




/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_elem_mark:
*/
/*! set a given mark for a given element. 
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void set_elem_mark_val ( elem_struct *pElem, const int kMark, const int val ) {

  if ( val )
    pElem->mark |= 0x1 << kMark ;
  else
    pElem->mark &= ~(0x1 << kMark) ;

  return ;
}


int elem_has_marks ( const elem_struct *pElem, const int mMarks, const int iMark[] ) {

  int kM, retVal=0 ;
  for ( kM=0 ; kM < mMarks ; kM++ ) {
    if ( elem_has_mark ( pElem, iMark[kM] ) )
      retVal = 1 ;
  }
  return ( retVal ) ;
}


int elem_has_mark ( const elem_struct *pElem, const int kMark ) {
  
  int retVal = pElem->mark & (0x1 << kMark) ;
  return ( retVal ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  elem_mark2int:
*/
/*! Return the index of the first non-zero mark, or zero if there is none.
 *
 */

/*
  
  Last update:
  ------------
  18Sep23: conceived.
  

  Input:
  ------
  pElem
    
  Returns:
  --------
  the index (from 1) of the first non-zero mark if there is, zero otherwise.
  
*/


int elem_mark2int ( const elem_struct *pElem ) {
  
  int kMark ;
  for ( kMark = 0 ; kMark < SZ_ELEM_MARK ; kMark++ ) {
    if ( pElem->mark & (0x1 << kMark) ) {
      return kMark ;
    }
  }
  return ( 0 ) ;
}

void elem_int2mark ( elem_struct *pElem, int intMrk ) {
  
  int kMark = intMrk ;
  reset_elem_all_mark ( pElem ) ;
  set_elem_mark ( pElem, kMark ) ;

  return ;
}



void set_elem_mark ( elem_struct *pElem, const int kMark ) {
  
  set_elem_mark_val ( pElem, kMark, 1 ) ;

  return ;
}

void reset_elem_mark ( elem_struct *pElem, const int kMark ) {
  
  set_elem_mark_val ( pElem, kMark, 0 ) ;

  return ;
}

void reset_elem_mark_range ( elem_struct *pElem, const int kMarkBeg, const int kMarkEnd ) {
  int kMark ;
  int kMarkB = MAX( kMarkBeg, 0 ) ;
  int kMarkE = MIN( kMarkEnd, SZ_ELEM_MARK-1 ) ;
  

  for ( kMark = kMarkB ; kMark <= kMarkE ; kMark++ )
    set_elem_mark_val ( pElem, kMark, 0 ) ;

  return ;
}

void reset_elem_all_mark ( elem_struct *pElem ) {
  
  reset_elem_mark_range ( pElem, 0, SZ_ELEM_MARK-1 ) ;

  return ;
}



/******************************************************************************
  mark_elem_type:   */

/*! Set a mark for all elements of a type range
 */

/*
  
  Last update:
  ------------
  5May20; derived from zone_elem_mod_type.
  

  Input:
  ------
  pUns: 
  kMark: Mark to use, 0 < kMark <  SZ_ELEM_MARK-1
  nElBeg, nElEnd: First and last element to be added, expected to be 
  consecutive in the pRootChunk.

  Changes To:
  -----------
  *pUns

  Output:
  -------
  mElAdded: number of elements added to this zone.
    
  Returns:
  --------
  number of elements marked.
  
*/

int mark_elem_type ( uns_s *pUns, const int kMark, 
                     const elType_e elTBeg, const elType_e elTEnd ) {
#undef FUNLOC
#define FUNLOC "in mark_elem_type"
  if ( kMark < 0 || kMark > SZ_ELEM_MARK-1 )
    hip_err ( fatal, 0, "kMark out of range in"FUNLOC"." ) ;

  
  /* Tag the elements in the expr with the zone number. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  int overwriteZone = 0 ;
  int mElemAdded = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) 
      if ( pEl->number && pEl->elType >= elTBeg && pEl->elType <= elTEnd ) {
        if ( !elem_has_mark ( pEl, kMark ) ) {
          set_elem_mark ( pEl, kMark ) ;
          ++mElemAdded ;
        }
      }

  return ( mElemAdded ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_elem_perBcLayer 
*/
/*! Set either mark for all elements within a distance from either side of per bc pair.
 *
 */

/*
  
  Last update:
  ------------
  5May20; derived from zone_elem_mod_perBcLayer
  

  Input:
  ------
  pUns
  kElMark[2] number of the elem mark to be set for per l/u 
  mLayer: number of layers to add, starting from bc faces
  elTypeLo/Hi: only zone elements with elTypeLo <= elType <= elTypeHi
  *pmBcPer: pointer to number of periodic/internal bcs
  nrBcPer[]: list of bc.nr for internal/per bcs.

  Returns:
  --------
  number of elements in the zone.
  
*/

ulong_t mark_elem_perBcLayer ( uns_s *pUns, int kElMark[2],
                               const int mLayer,
                               const elType_e elTypeLo, const elType_e elTypeHi,
                               int *pmBcPer, int nrBcPer[] ) {
#undef FUNLOC
#define FUNLOC "in mark_elem_perBcLayer"

  int kDir ;
  for ( kDir = 0 ; kDir < 2 ; kDir++ )
    if ( kElMark[kDir] < 0 || kElMark[kDir] > SZ_ELEM_MARK-1 )
      hip_err ( fatal, 0, "kMark out of range "FUNLOC"." ) ;

  // Mark all vx on per bc. Reserve the first vertex mark for l, second for u.
  int kVxMark[2] = {0,2} ;
  reserve_vx_markN ( pUns, kVxMark[0], FUNLOC ) ;
  reserve_vx_markN ( pUns, kVxMark[1], FUNLOC ) ;

  const int doReset = 1 ;
  mark_vx_per ( pUns, kVxMark, pmBcPer, nrBcPer, doReset ) ;

  
  /* Repeatedly loop over all elements, propagate the flag and
     set the zone of any element with a flagged node. */
  int kL ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  ulong_t mVx ;
  int kVx ;
  vrtx_struct *pVx ;
  ulong_t mElemsAdded[2] = {0} ;
  int kOpp ;
  int elVx_markN[2] ;
  for ( kL = 0 ; kL < mLayer ; kL++ ) {
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( !elem_has_mark(pEl,kElMark[0]) &&
             !elem_has_mark(pEl,kElMark[1]) &&
             pEl->elType >= elTypeLo &&
             pEl->elType <= elTypeHi ) {

          mVx = elemType[ pEl->elType ].mVerts ;
          elVx_markN[0] = elVx_markN[1] = 0 ;
          for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
            pVx = pEl->PPvrtx[kVx] ;
            for ( kDir = 0 ; kDir < 2 ; kDir++ ) {
              if ( vx_has_markN ( pVx, kVxMark[kDir] ) )
                elVx_markN[kDir] = 1 ;
            }
          }

          if ( elVx_markN[0] == elVx_markN[1] ) {
            /* If both zero: not yet in any layer, do nothing.
               If both non-zero: this yet unzoned element has vertices in 
               either layer, layers have hence grown together. 
               Stop here, do nothing. */
            ;
          }
          else if ( elVx_markN[0] ) {
            /* Add elem to L layer. */
            set_elem_mark ( pEl, kElMark[0] ) ;
            mElemsAdded[0]++ ;
          }
          else {
            /* Add to U layer. */
            set_elem_mark ( pEl, kElMark[1] ) ;
            mElemsAdded[1]++ ;
          }
        }

    /* Now mark all nodes formed with the marked elements
       to enable propagation. Not needed for the final layer. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) &&
            kL+1 == mLayer )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        for ( kDir = 0 ; kDir < 2 ; kDir++ )
          if ( elem_has_mark (pEl, kElMark[kDir] ) ) {
            mVx = elemType[ pEl->elType ].mVerts ;
            for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
              vx_set_markN ( pEl->PPvrtx[kVx], kVxMark[kDir] ) ;
            }
          }
  }

  
  release_vx_markN ( pUns, kVxMark[0] ) ;
  release_vx_markN ( pUns, kVxMark[1] ) ;

  //pUns->pZones[iZone[0]]->mElemsZone += mElemsAdded[0] ;
  //pUns->pZones[iZone[1]]->mElemsZone += mElemsAdded[1] ;
  return ( mElemsAdded[0]+mElemsAdded[1] ) ;
}




/******************************************************************************
  mark_elem_remaining:   */

/*! Add/remove all remaining, non-zoned elements of a grid to a zone.
 *
 */

/*
  
  Last update:
  ------------
  25Apr23; fix bug with confusion over masks and marks. Compare masks.
           include case of kMaskTrue=0
  21Dec21; check for mark overlap
  5May20; derived from zone_elem_mod_remaining

  Input:
  ------
  pUns     = grid with elements to tag.
  kMaskTrue = elems to mark must have these marks set. 
              Allows for kMaskTrue=0, i.e. mark any element other than those
              that match kMaskFalse.
  kMaskFalse = elems to mark must not have these marks set
  elTBeg, elTEnd = inclusive range of elements to consider.
  kMark2set    = mark to set for unmarked elements
    
  Returns:
  --------
  number of added elements.
  
*/

int mark_elem_remaining ( uns_s *pUns,
                          const int kMaskTrueArg,
                          const int kMaskFalseArg, 
                          const elType_e elTBeg, const elType_e elTEnd,
                          const int kMark2set ) {

#undef FUNLOC
#define FUNLOC "in mark_elem_type"
  if ( kMark2set < 0 || kMark2set > SZ_ELEM_MARK-1 )
    hip_err ( fatal, 0, "kMark out of range in"FUNLOC"." ) ;
  // Create a mask for kMark2set to enable & comparison
  int kMarksUsed[SZ_ELEM_MARK] = {0} ;
  kMarksUsed[kMark2set] = 1 ;
  unsigned int kMask2set = i32_packNi ( SZ_ELEM_MARK, kMarksUsed ) ;

  /* Make sure the comparison fields have the same width as pElem->mark. */
  unsigned int kMaskTrue = kMaskTrueArg & MASK_ELEM_MARK ;
  unsigned int kMaskFalse = kMaskFalseArg & MASK_ELEM_MARK ;
  if ( kMask2set & kMaskTrue || kMask2set & kMaskFalse )
    hip_err ( fatal, 0, "kMask2Set overlaps with kMaskTrue/False "FUNLOC"." ) ;

  
  int mElemsMarked = 0 ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( !pEl->invalid &&
           pEl->elType >= elTBeg &&
           pEl->elType <= elTEnd ) {
        // Good start, valid elem of the right range of types.

        if ( ( // Case of 'any other', i.e. kMaskTrue=0:
              pEl->mark == kMaskTrue ||
              // Case of non-zero kMaskTrue, match the mask: 
              pEl->mark & kMaskTrue )
             // but never match kMaskFalse
             && !( pEl->mark & kMaskFalse ) ) {
          set_elem_mark ( pEl, kMark2set ) ;
          mElemsMarked++ ;
        }
      }

  return ( mElemsMarked ) ;
}








/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mark_vx_elem_mark_per:
*/
/*! Mark all vertices on interfaces between elements with different marks.
    Uses also two additional vx marks in the process.
 *
 *
 */

/*
  
  Last update:
  ------------
  6May20; derived from mark3_vx_zones_per
  

  Input:
  ------
  pUns0: grid
  mZones: number of zones
  iZone[]: list of zones
  doReset: if non-zero, reset mark3, start from scratch. Otherwise, accumulate.
  doPer: also mark3 (fix) vx on periodic bc.

  Output:
  -------
  *pmElZoneF/L/C/U: number of elements in the fixed, lower, core, upper zones.
  *pmConnZoneF/L/C/U: number of conn entries in the fixed, lower, core, upper zones.
  *pmVxZoneF/L/C/U: number of vertices in the fixed, lower, core, upper zones.

  Returns:
  --------
  0 on success
  
*/

int mark_vx_elem_match_per ( uns_s *pUns0, const match_s *pMultMatch,
                             int kVxMark2Set, int kVxMarkWork[2],
                             const int doReset, const int doPer ) {

  /* Reset marks. */
  if ( doReset )
    reset_vx_markN ( pUns0, kVxMark2Set ) ;



  /* One pass for each match. */
  int kZ ;
  chunk_struct *pCh ;
  elem_struct *pEl, *pElB, *pElE ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  int nBeg, nEnd ;

  int mMatches = count_elem_matches ( pMultMatch ) ;
  int kM ;
  match_s oneMatch, *pMatch = &oneMatch ;
  for ( kM = 0 ; kM < mMatches ; kM++ ) {
    const int doReset = 1 ;
    make_elem_match_k ( pMultMatch, kM, doReset, pMatch ) ;
    
    reset_vx_markN ( pUns0, kVxMarkWork[0] ) ;
    reset_vx_markN ( pUns0, kVxMarkWork[1] ) ;

    pCh = NULL ;
    while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) 
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( pEl->number ) {
          if ( elem_matches ( pEl, pMatch ) ) {
            /* Mark all forming vertices of elems as matching. */
            markN_vx_elem ( pEl, kVxMarkWork[0] ) ;
          }
          else {
            /* Mark2 all forming vertices of all elems not matching. */
            markN_vx_elem ( pEl, kVxMarkWork[1] ) ;
          }
        }

    /* Mark3 all vertices on zone interfaces. */
    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number &&
             vx_has_markN ( pVx, kVxMarkWork[0] ) &&
             vx_has_markN ( pVx, kVxMarkWork[1] )
             ) {
          set_vrtx_mark_k ( pVx, kVxMark2Set ) ;
        }
  }


  if ( doPer ) {
    /* Mark3 all vertices on periodic boundaries.
    int markN[2] = {0,2} ;
    reset_vx_mark ( pUns0 ) ;
    reset_vx_mark2 ( pUns0 ) ; */
    reset_vx_markN ( pUns0, kVxMarkWork[0] ) ;
    reset_vx_markN ( pUns0, kVxMarkWork[1] ) ;
    
    int mBcPer=0, nBcPer[2*MAX_PER_PATCH_PAIRS] ;
    mark_vx_per ( pUns0, kVxMarkWork, &mBcPer, nBcPer, doReset ) ;

    pCh = NULL ;
    while ( loop_verts ( pUns0, &pCh, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->number &&
             ( vx_has_markN( pVx, kVxMarkWork[0] ) ||
               vx_has_markN( pVx, kVxMarkWork[1] ) ) )
          vx_set_markN ( pVx, kVxMark2Set ) ;
  }

  return ( 0 ) ;
}




/******************************************************************************
  elem_invalidate_mark:   */

/*! Delete (invalidate) all elements and their bnd faces that have a mark. 
 */

/*
  
  Last update:
  ------------
  1Mar22; derived from zone_elem_invalidate
  

  Input:
  ------
  pUns: 
  iMark: the mark to use 

  Output:
  -------
  mElDeleted: number of elements deleted.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ulong_t elem_invalidate_mark ( uns_s *pUns, const int iMark ) {

  /* Make a mask. */
  int mark = 1 << iMark ;
  
  /* Set to 'invalid' all elements with zone number. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  ulong_t mElemDeleted = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) 
      if ( pEl->number && ( pEl->mark & mark ) ) {
        pEl->invalid = 1 ;
        ++mElemDeleted ;
      }


  /* Invalidate all boundary faces pointing to invalid elems. */
  pChunk = NULL ;
  bndPatch_struct *pBP = NULL ;
  bndFc_struct *pBF, *pBndFcF, *pBndFcL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBP, &pBndFcF, &pBndFcL ) ) {
    for ( pBF = pBndFcF ; pBF <= pBndFcL ; pBF++ )
      if ( pBF->Pelem->invalid )
        pBF->invalid = 1 ;
  }

  return ( mElemDeleted ) ;
}

