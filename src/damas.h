/* Parameters for Damas calls. */
const int ONE = 1 ;
const int TWO = 2 ;
const int SIX = 6 ;

/* Number of blocks. */
#define DMS_MXBLF2 1000
const int MXBLF2 = DMS_MXBLF2 ;

/* Number of boundary conditions. */
#define DMS_MXCL 99
const int MXCL = DMS_MXCL ;

/* Number of subfaces per block. */
#define DMS_MXS3 5000
const int MXS3 = DMS_MXS3 ;

/* Number of nodes per block. */
#define DMS_MXNOBL 1000000
const int MXNOBL = DMS_MXNOBL ;

