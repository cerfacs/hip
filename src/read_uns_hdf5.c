/*
  read_uns_hdf5.c:
  read from hdf.

  Last update:
  ------------
  16Aug17; Add hmin/hmax to skeleton read/json output.
  5Mar15; Skipping reading datasets that are not of size mVx instead of fatal error.
  26Jun14; intro h5w_flag_reset.
  5Apr13; don't forget to include proto_hdf.h
  1Apr12; rename more compactly internal (static) routines from read_hdf5_ to h5r_
  3Jul10; move h5_ utilities into new file h5_util.c
  13Apr08; update H5Dopen, H5Gopen, H5Dcreate interfaces to hdf-1.8.0
  13May07; read also the mesh.
  16Oct06; solution only for the time being.

  This file contains:
   -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

#include "hdf5.h"
#include "proto_hdf.h"

extern const char lastUpdate[] ;
extern const char version[] ;


extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const Grids_struct Grids ;
extern const char version[] ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;
extern const int  parTypeSize[] ;

/* Global control variables, what is written to file. */
#define DEFAULT_h5r_flag_all 0 /* read all variables. */
static int h5r_flag_all  = DEFAULT_h5r_flag_all  ;

#define DEFAULT_h5r_flag_info 0 /* read all variables. */
static int h5r_flag_info  = DEFAULT_h5r_flag_all  ;


#define DEFAULT_h5r_flag_zone 1 /* read or not read zones. */
static int h5r_flag_zone  = DEFAULT_h5r_flag_zone  ;

void h5r_flag_reset () {
  int h5r_flag_info  = DEFAULT_h5r_flag_info ;
  int h5r_flag_all  = DEFAULT_h5r_flag_all ;
  int h5r_flag_zone  = DEFAULT_h5r_flag_zone ;
  return ;
}


/******************************************************************************
  h5r_args:   */

/*! pull of arguments from the line with arguments.
 */

/*

  Last update:
  ------------
  25Feb18; prepend path to solfile before checking existence.
  15Dec16; intro -i flag for skeleton read
  18Dec14; intro -s argument for solfile, allow to leave out asciiBound.
  6Apr14; accept hdf5a and hdfa to trigger -a option.
  1Apr12: derived from h5w_args.

  Input:
  ------
  argLine[]: text line with all args, including hdf keyword

  Output:
  ------
  gridFile{}:
  abndFile{}: asciBound
  solFile{}:

*/

void h5r_args  ( char argLine[], char gridName[LINE_LEN], char *gridFile, char *abndFile,
                 char *solFile ) {

  /* Reset. */
  h5r_flag_all  = DEFAULT_h5r_flag_all  ;
  h5r_flag_info = DEFAULT_h5r_flag_info ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  abndFile[0] = '\0' ;
  solFile[0]  = '\0' ;

  /* Backward compatibility, parse augmented hdf identifier. */
  if ( !strncmp( ppArgs[0], "hdf5a", 4 ) || !strncmp( ppArgs[0], "hdfa", 4  ) )
    h5r_flag_all = 1 ;


  /* Parse line of unix-style optional args. */
  char c ;
  while ((c = getopt_long ( mArgs, ppArgs, "ain:s:z::",NULL,NULL)) != -1) {
    switch (c)  {
      case 'a':
        h5r_flag_all = 1;
        break;
      case 'i':
        h5r_flag_info = 1;
        break;
      case 'n':
        strncpy ( gridName, optarg, LINE_LEN ) ;
        break;
      case 's':
        if ( optarg ){
          /* 0 arg given. use -a0 */
          strcpy ( solFile, optarg ) ;
          /* GS Check format to overide default options for average solutions
             since users do not read the documentation */
          hid_t file_id = H5Fopen( prepend_path(solFile), H5F_ACC_RDONLY, H5P_DEFAULT);
          /* If Average exists, read all the data */
          if (H5Lexists( file_id,"Average", H5P_DEFAULT ) > 0 ) {
            h5r_flag_all = 1 ;
          }
          herr_t status  = H5Fclose(file_id);
        }
        else
          /* No arg given, or arg not 0, use -a1 */
          hip_err ( warning, 1,
                    "option s needs a filename argument, ignored.\n" ) ;
        break;

      case 'z':
        if ( optarg && atoi( optarg ) == 0 )
          /* 0 arg given. use -a0 */
          h5r_flag_zone = 0;
        else
          /* No arg given, or arg not 0, use -a1 */
          h5r_flag_zone = 1;
        break;
      case '?':
        if (isprint (optopt)) {
          sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
          hip_err ( warning, 1, hip_msg ) ;
          break ;
        }
        else {
          sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
          hip_err ( warning, 1, hip_msg ) ;
          break ;
        }
      default:
        sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    }

  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( gridFile, ppArgs[optind] ) ;
  else
    hip_err ( fatal, 0, "missing grid file name for read hdf\n" ) ;

  if ( optind+1 < mArgs )
    strcpy ( abndFile, ppArgs[optind+1] ) ;


  if ( optind+2 < mArgs )
    strcpy ( solFile, ppArgs[optind+2] ) ;

  return ;
}



void test_dread ( char *fileName, char *grpName, char* dsetName, double *dBuf ) {

   hid_t       file_id, dset_id, grp_id ;  /* identifiers */
   herr_t status ;



   /* Open an existing file.   */
   file_id = H5Fopen(fileName, H5F_ACC_RDWR, H5P_DEFAULT);

   grp_id = H5Gopen(file_id, grpName, H5P_DEFAULT ) ;

    h5_read_dbl ( grp_id, "rho", 20856, dBuf ) ;



   /* Open an existing dataset. */
    dset_id = H5Dopen(grp_id, dsetName, H5P_DEFAULT );



   status = H5Dread(dset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                    dBuf);

   /* Close the dataset. */
   status = H5Dclose(dset_id);


   /* Close the dataset. */
   status = H5Gclose(grp_id);

   /* Close the file. */
   status = H5Fclose(file_id);

   /* exit ( EXIT_SUCCESS ) */
  return ;
}

void test_read ( ) {

   hid_t       file_id, dataset_id, dataspace_id ;  /* identifiers */
   herr_t      status;
   int         i, j, iBuf[4][6], iiBuf[4][6] ;
   double      dBuf[2][10];
   hsize_t     dims[2];

   /* Initialize the first dataset. */
   for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
         iBuf[i][j] = i + j + 1;

   /* Initialize the second dataset. */
   for (i = 0; i < 2; i++)
      for (j = 0; j < 10; j++)
         dBuf[i][j] = i + j + 1.;


   /* Create a file.  */
   file_id = H5Fcreate("blah.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

   /* Create the data space for the dataset. */
   dims[0] = 2;
   dataspace_id = H5Screate_simple(1, dims, NULL);

   /* Create the dataset. */
   dataset_id = H5Dcreate(file_id, "dset_int", HH_INT, dataspace_id,
                     H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

   /* Write the dataset. */
   status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                     iBuf);

   /* Close the dataset. */
   status = H5Dclose(dataset_id);

   /* Close the file. */
   status = H5Fclose(file_id);





   /* Open an existing file.   */
   file_id = H5Fopen("blah.h5", H5F_ACC_RDWR, H5P_DEFAULT);
   /* Open an existing dataset. */
   dataset_id = H5Dopen(file_id, "dset_int", H5P_DEFAULT );


   status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                    iiBuf);

   /* Close the dataset. */
   status = H5Dclose(dataset_id);

   /* Close the file. */
   status = H5Fclose(file_id);

}


/******************************************************************************
  h5r_add_pList:   */

/*! add a global scalar to restart.hdf for later regurgitation
 */

/*

  Last update:
  ------------
  25Apr16; fix reading character parameters.
  2Jul10: conceived.

*/
void h5r_add_pList( hid_t grp_id, char *dset_name, hid_t dset_id, hid_t dspc_id,
                   restart_u *pRestart ) {

  h5pList_s *pL = pRestart->hdf.h5pList + pRestart->hdf.m5pList ;
  hid_t dtype_id, class_id ;
  herr_t status ;


  if ( pRestart->hdf.m5pList >= MAX_H5_PARAMS-1 )
    hip_err( fatal, 0, "out of memory for hdf parameters in h5r_add_pList\n" ) ;

  /* Find the type, int, dbl or string */
  dtype_id = H5Dget_type(dset_id);
  strcpy ( pL->i.label, dset_name ) ;
  class_id = H5Tget_class( dtype_id ) ;
  if ( class_id ==  H5T_INTEGER ) {
    pL->i.type = h5_i ;
    H5Dread( dset_id, HH_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(pL->i.iVal) ) ;
  }
  else if ( class_id ==  H5T_FLOAT ) {
    //double someDbl ;
    pL->i.type = h5_d ;
    //H5Dread( dset_id, HH_DBL, H5S_ALL, H5S_ALL, H5P_DEFAULT,  &someDbl ) ;
    H5Dread( dset_id, HH_DBL, H5S_ALL, H5S_ALL, H5P_DEFAULT,  &(pL->d.dVal) ) ;
  }
  else if ( class_id ==  H5T_STRING ) {
    int sdim = H5Tget_size ( dtype_id ) ;
    hid_t mTyp_id = H5Tcopy (H5T_C_S1);
    sdim = MAX( TEXT_LEN-1, sdim ) ;
    H5Tset_size ( mTyp_id, sdim ) ;
    pL->i.type = h5_s ;
    status = H5Dread( dset_id, mTyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                          pL->s.str );
  }
  else {
    /* No datatype we know how to treat. */
    H5Tclose ( dtype_id ) ;
    H5Dclose ( dset_id ) ;
    return ;
  }

  H5Tclose ( dtype_id ) ;
  H5Dclose ( dset_id ) ;
  pRestart->hdf.m5pList++ ;
  return ;
}
/******************************************************************************

  h5r_sizes:
  Given a hdf5 file pointer, read the mesh dimensions for allocation.

  Last update:
  ------------
  16Jun19; fix bug with adding Quad + Tet face bc numbers for hybrid grids.
  2Mar19; fix reading sizes for number of bnd fc when only node conn given.
  22Oct19; Read parameters group only if it exists.
  17Feb17; extract periodicity flag, specialTopo. Allow surface meshes.
  15Dec16; add components for skeleton read.
  26Sep14; rename iBuf to uBuf, to make type ulong_t clearer.
  5Apr13; promote iBuf to ulong_t
  11Apr08; we are testing the length of the conn vector, not the # of els.
  May07: conceived.

  Input:
  ------
  file_id = opened hdf5 file

  Output:
  -----------
  mDim    = spatial dimension, 2 or 3.
  mEl     = number of elements
  mConn   = number of connectivity entries (total number of element to node ptr.)
  mVx     = number of vertices
  mBndFc  = number of boundary faces
  mBndNde = number of boundary nodes, if used to specify the bc patches.
  mBc     = number of boundary conditions (patches)
  pVolGrid= grid volume
  bcLabel = list of bnd patch names#
  bndPatchArea = list of bnd patch surface areas
  ll/urBox = n-tuple of lower-left and upper-right bounding box limits
  ll/urBoxCyl = r and theta min/max relative to x-axis. In 2D: only r.
  isPer = set non-zero if there is a list with periodic nodes.

  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5r_sizes ( hid_t file_id,
                int *pmDim, ulong_t *pmEl, ulong_t *pmConn,
                ulong_t *pmVx, ulong_t *pmBndFc, ulong_t *pmBndNde, int *pmBc,
                char gridName[LINE_LEN],
                double *pVolGrid, double *pVolmin,
                double *pHmin, double *pHmax, char **pBcLabel,
                size_t *plbl_len, double **ppBndPatchArea,
                double llBox[], double urBox[],
                double llBoxCyl[], double urBoxCyl[], int *pisPer,
                specialTopo_e *pTopo ) {

  hid_t grp_id ;
  herr_t      status;

  char grpName[MAX_GROUPS][TEXT_LEN] ;
  ulong_t uBuf[6], i ;
  elType_e elType ;

  *pTopo = noTopo ;
  *pmBndNde = 0 ;

  /* Coordinates: mDim, mVx. */
  strcpy ( grpName[0], "Coordinates" ) ;
  grp_id = h5_open_group ( file_id, grpName[0] ) ;

  *pmVx   = h5_read_dbl ( grp_id, "x", 0, NULL ) ;
  uBuf[1] = h5_read_dbl ( grp_id, "y", 0, NULL ) ;
  uBuf[2] = h5_read_dbl ( grp_id, "z", 0, NULL ) ;

  *pmDim = ( uBuf[2] == 0 ? 2 : 3 ) ;

  if ( uBuf[1] != *pmVx || ( *pmDim == 3 && uBuf[2] != *pmVx ) ) {
    sprintf ( hip_msg, "mismatch in lengths of coordinate vectors: %"FMT_ULG",%"FMT_ULG",%"FMT_ULG"\n",
              *pmVx, uBuf[1], uBuf[2] ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  status = H5Gclose(grp_id);




  /* Connectivity */
  strcpy ( grpName[0], "Connectivity" ) ;
  grp_id = h5_open_group ( file_id, grpName[0] ) ;

  uBuf[0] = h5_read_int ( grp_id, "tri->node", 0, NULL ) ;
  uBuf[1] = h5_read_int ( grp_id, "qua->node", 0, NULL ) ;
  uBuf[2] = h5_read_int ( grp_id, "tet->node", 0, NULL ) ;
  uBuf[3] = h5_read_int ( grp_id, "pyr->node", 0, NULL ) ;
  uBuf[4] = h5_read_int ( grp_id, "pri->node", 0, NULL ) ;
  uBuf[5] = h5_read_int ( grp_id, "hex->node", 0, NULL ) ;

  if ( *pmDim == 2 && ( uBuf[2]/4 + uBuf[3]/5 + uBuf[4]/6 + uBuf[5]/8 ) ) {
    sprintf ( hip_msg,
              "found %"FMT_ULG" tets, %"FMT_ULG" pyrs, %"FMT_ULG" prisms, %"FMT_ULG" hexa in 2-D grid.\n",
              uBuf[2]/4, uBuf[3]/5, uBuf[4]/6, uBuf[5]/8 ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( *pmDim == 3 && (uBuf[0]/3 + uBuf[1]/4 ) ) {
    sprintf ( hip_msg, "found %"FMT_ULG" tris, %"FMT_ULG" quads in 3-D grid.\n"
              "          reading as surface grid.\n",
              uBuf[0]/3, uBuf[1]/4 ) ;
    hip_err ( info, 1, hip_msg ) ;
    *pTopo = surf ;
  }

  for ( *pmConn = *pmEl = 0, elType = tri ; elType <= hex ; elType++ ) {
    i = (int) elType ;
    *pmEl   += uBuf[i]/elemType[elType].mVerts ;
    *pmConn += uBuf[i] ;
  }

  status = H5Gclose(grp_id);


  if ( *pTopo != surf ) {
    /* Boundary. */
    strcpy ( grpName[0], "Boundary" ) ;
    grp_id = h5_open_group ( file_id, grpName[0] ) ;


    uBuf[0] = h5_read_int ( grp_id, "bnd_bi_lidx",  0, NULL ) ;
    uBuf[1] = h5_read_int ( grp_id, "bnd_tri_lidx", 0, NULL ) ;
    uBuf[2] = h5_read_int ( grp_id, "bnd_qua_lidx", 0, NULL ) ;

    if ( *pmDim == 2 && ( uBuf[1] + uBuf[2] ) ) {
      sprintf ( hip_msg,
                "found %"FMT_ULG" bnd_tri, %"FMT_ULG" bnd_quads 2-D grid.\n", uBuf[1], uBuf[2] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else if (  *pmDim == 3 && uBuf[0] ) {
      sprintf ( hip_msg, "found %"FMT_ULG" bnd_bi in 3-D grid.\n", uBuf[0] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( *pmDim == 3 && uBuf [2] && uBuf[1] && uBuf[1] != uBuf[2] ) {
      sprintf ( hip_msg,
                "differing number of bc for tri (%"FMT_ULG") and quad (%"FMT_ULG") in 3-D grid.\n",
                uBuf[1], uBuf[2] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }


    if ( *pmDim == 2 ) {
      *pmBc = 0;
      *pmBc    = uBuf[0] ;
      *pmBndFc = 0;

      int hasFc2El = h5_obj_exists ( grp_id,"bnd_bi->elem" ) == H5I_DATASET &&
                     h5_obj_exists ( grp_id,"bnd_bi->face" ) == H5I_DATASET &&
                     h5_obj_exists ( grp_id,"bnd_bi_lidx" ) == H5I_DATASET ;

      int hasFc2Nd = h5_obj_exists ( grp_id,"bnd_bi->node" ) == H5I_DATASET &&
                     h5_obj_exists ( grp_id,"bnd_bi_lidx" ) == H5I_DATASET ;

      int hasBnd2Nd = h5_obj_exists ( grp_id,"bnode->node" ) == H5I_DATASET &&
                     h5_obj_exists ( grp_id,"bnode_lidx" ) == H5I_DATASET ;

      if ( hasFc2El ) {
        /* element and face pointers given. */
        *pmBndFc = h5_read_int ( grp_id, "bnd_bi->elem",  0, NULL ) ;
        *pmBc = h5_read_int ( grp_id, "bnd_bi_lidx",  0, NULL ) ;
      }
      else if ( hasFc2Nd ) {
        /* no face pointers, read node pointers. */
        *pmBndFc = h5_read_int ( grp_id, "bnd_bi->node",  0, NULL )/2 ;
        *pmBc = h5_read_int ( grp_id, "bnd_bi_lidx",  0, NULL ) ;
      }
      else if ( hasBnd2Nd ) {
        /* no face pointers, read node pointers. */
        *pmBndNde = h5_read_int ( grp_id, "bnode->node",  0, NULL ) ;
        *pmBc = h5_read_int ( grp_id, "bnode_lidx",  0, NULL ) ;
      }
      else {
        hip_err ( warning, 1,
                  "neither face nor node pointers given for boundary faces."
                  " No faces read." ) ;
        *pmBndFc =  0;
      }
    }
    else { // 3-d
      *pmBc = 0;
      *pmBc    = ( uBuf[1] ? uBuf[1] : uBuf[2] ) ;

      int hasT2El = h5_obj_exists ( grp_id,"bnd_tri->elem" ) == H5I_DATASET &&
                    h5_obj_exists ( grp_id,"bnd_tri->face" ) == H5I_DATASET &&
                    h5_obj_exists ( grp_id,"bnd_tri_lidx" ) == H5I_DATASET ;
      int hasQ2El = h5_obj_exists ( grp_id,"bnd_qua->elem" ) == H5I_DATASET &&
                    h5_obj_exists ( grp_id,"bnd_qua->face" ) == H5I_DATASET &&
                    h5_obj_exists ( grp_id,"bnd_qua_lidx" ) == H5I_DATASET ;
      int hasFc2El = hasT2El || hasQ2El ;

      int hasT2nd = h5_obj_exists ( grp_id,"bnd_tri->node" ) == H5I_DATASET &&
                    h5_obj_exists ( grp_id,"bnd_tri_lidx" ) == H5I_DATASET ;
      int hasQ2nd = h5_obj_exists ( grp_id,"bnd_qua->node" ) == H5I_DATASET &&
                    h5_obj_exists ( grp_id,"bnd_qua_lidx" ) == H5I_DATASET ;
      int hasFc2Nd = hasT2nd || hasQ2nd ;

      int hasBnd2Nd = h5_obj_exists ( grp_id,"bnode->node" ) == H5I_DATASET &&
                      h5_obj_exists ( grp_id,"bnode_lidx" ) == H5I_DATASET ;

     if ( hasFc2El ) {
        /* element and face pointers given. */
        uBuf[1] = h5_read_int ( grp_id, "bnd_tri->elem",  0, NULL ) ;
        uBuf[2] = h5_read_int ( grp_id, "bnd_qua->elem",  0, NULL ) ;
        *pmBndFc = uBuf[1] + uBuf[2] ;
      }
      else if ( hasFc2Nd ) {
        /* no face pointers, read node pointers. */
        uBuf[1] = h5_read_int ( grp_id, "bnd_tri->node",  0, NULL )/3 ;
        uBuf[2] = h5_read_int ( grp_id, "bnd_qua->node",  0, NULL )/4 ;
        *pmBndFc = uBuf[1] + uBuf[2] ;
      }
      else if ( hasBnd2Nd ) {
        /* no face pointers, read node pointers. */
        *pmBndNde = h5_read_int ( grp_id, "bnode->node",  0, NULL ) ;
      }
      else {
        hip_err ( warning, 1,
                  "neither face nor node pointers given for boundary faces."
                  " No faces read." ) ;
        *pmBndFc =  0;
      }


      /* Number of bcs in the 3D case. */
      int mBcT=0, mBcQ=0 ;
      if ( hasFc2El || hasFc2Nd ) {
        /* Tri or qua lidx given. */
        if ( h5_obj_exists ( grp_id,"bnd_tri_lidx" ) == H5I_DATASET )
          mBcT = h5_read_int ( grp_id, "bnd_tri_lidx",  0, NULL ) ;
        if ( h5_obj_exists ( grp_id,"bnd_qua_lidx" ) == H5I_DATASET )
          mBcQ = h5_read_int ( grp_id, "bnd_qua_lidx",  0, NULL ) ;

        if ( mBcT != 0 && mBcQ != 0 && mBcT != mBcQ )
            hip_err ( fatal, 0, "lidx lengths for tri and quad faces differ." ) ;
        else if ( mBcT )
          *pmBc = mBcT ;
        else
          *pmBc = mBcQ ;
      }
      else {
        /* bnode_lidx given. */
        *pmBc = h5_read_int ( grp_id, "bnode_lidx",  0, NULL ) ;
      }
    } //     if ( *pmDim == 2 )


    // For summary info on patches, base the number of bc on the patch labes.
    // Don't even bother reading areas if none there.
    if ( h5_obj_exists ( grp_id,"PatchLabels" ) == H5I_DATASET ) {
      int mBc = h5_read_fxStr ( grp_id, "PatchLabels", 0, fxStr240, NULL ) ;
      if ( mBc != *pmBc )
        hip_err ( fatal, 0, "size of lidx and number of PatchLabel disagree." ) ;

      size_t len_str = h5_read_fxStr_len ( grp_id, "PatchLabels" ) ;

      // Malloc for labels and areas.
      *pBcLabel = arr_malloc ( "bcLabels in h5r_bcLabels", NULL,
                               mBc, len_str+1 ) ;
      // Not clean: we read len_str, but then use a fixed size anyways.
      *plbl_len = 240 ;
      h5_read_fxStr ( grp_id, "PatchLabels", mBc, fxStr240, *pBcLabel ) ;

      *ppBndPatchArea = arr_malloc ( "bndPatchArea in h5r_bcLabels", NULL,
                                     mBc, sizeof( **ppBndPatchArea ) ) ;

      int nBc ;
      if ( mBc == h5_read_dbl ( grp_id, "Patch->area",  0, NULL ) )
        h5_read_dbl ( grp_id, "Patch->area",  *pmBc, *ppBndPatchArea ) ;
      else {
        for ( nBc = 0 ; nBc < mBc ; nBc++ )
          (*ppBndPatchArea)[nBc] = 0. ;
      }
    }

    status = H5Gclose(grp_id); // close group Boundary



    /* Bounding boxes, in Parameters. */
    strcpy ( grpName[0], "Parameters" ) ;
    if ( h5_grp_exists ( file_id, "Parameters" ) ){
      grp_id = h5_open_group ( file_id, grpName[0] ) ;

      if ( !h5_read_fxStr ( grp_id, "gridName", 1, fxStr240, gridName ) )
        gridName[0] = '\0' ;

      if ( !h5_read_dbl ( grp_id, "vol_domain",  1, pVolGrid ) )
        *pVolGrid = 0. ;
      if ( !h5_read_dbl ( grp_id, "vol_elem_min",  1, pVolmin ) )
        *pVolmin = 0. ;
      if ( !h5_read_dbl ( grp_id, "h_min",  1, pHmin ) )
        *pHmin = 0. ;
      if ( !h5_read_dbl ( grp_id, "h_max",  1, pHmax ) )
        *pHmax = 0. ;

      if ( *pmDim == h5_read_dbl ( grp_id, "x_min",  0, NULL ) )
        h5_read_dbl ( grp_id, "x_min",  *pmDim, llBox ) ;
      else
        llBox[0] = llBox[1] = llBox[2] = 0 ;
      if ( *pmDim == h5_read_dbl ( grp_id, "x_max",  0, NULL ) )
        h5_read_dbl ( grp_id, "x_max",  *pmDim, urBox ) ;
      else
        urBox[0] = urBox[1] = urBox[2] = 0 ;

      if ( *pmDim-1 == h5_read_dbl ( grp_id, "r_min",  0, NULL ) )
        h5_read_dbl ( grp_id, "r_min",  *pmDim-1, llBoxCyl ) ;
      else
        llBoxCyl[0] = llBoxCyl[1] ;
      if ( *pmDim-1 == h5_read_dbl ( grp_id, "r_min",  0, NULL ) )
        h5_read_dbl ( grp_id, "r_min",  *pmDim-1, urBoxCyl ) ;
      else
        urBoxCyl[0] = urBoxCyl[1] ;


      status = H5Gclose(grp_id); // close group Parameters
    } // end if surf2d, surf3d.
    else {
      /* If Parameters group does not exist, initialize variables */
      gridName[0] = '\0' ;
      *pVolGrid = 0. ;
      *pVolmin = 0. ;
      *pHmin = 0. ;
      *pHmax = 0. ;
      llBox[0] = llBox[1] = llBox[2] = 0 ;
      urBox[0] = urBox[1] = urBox[2] = 0 ;
      llBoxCyl[0] = llBoxCyl[1] ;
      urBoxCyl[0] = urBoxCyl[1] ;
    }
  }

  /* Periodicity. */
  if ( h5_grp_exists ( file_id, "Periodicity" ) )
    *pisPer = 1 ;
  else
    *pisPer = 0 ;

  return ( 1 ) ;
}

/******************************************************************************

  h5r_coor:
  Read coordinates.

  Last update:
  ------------
  1May20; set mVertsNumbered.
  26Sep14; promote node index n to ulong_t
  May07: conceived.

  Input:
  ------
  file_id = opened hdf5 file
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]

  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5r_coor ( hid_t file_id, uns_s *pUns, chunk_struct *pChunk ) {

  const ulong_t mVx = pChunk->mVerts ;
  const int mDim = pUns->mDim ;

  hid_t  grp_id ;
  herr_t status;

  int i ;
  ulong_t n ;
  double *dBuf ;
  char coorName[3][2] = {"x", "y", "z"}, grpName[] = {"Coordinates"} ;
  vrtx_struct *pVrtx = pChunk->Pvrtx+1 ;

  /* Malloc a buffer. */
  dBuf = arr_malloc ( "dBuf in h5r_coor", pUns->pFam, mVx, sizeof(double));



  /* Coordinates: mDim, mVx. */
  strcpy ( grpName, "Coordinates" ) ;
  grp_id = h5_open_group ( file_id, grpName ) ;


  for ( i = 0 ; i < mDim ; i++ ) {
    h5_read_dbl ( grp_id, coorName[i], mVx, dBuf ) ;

    /* Translate into vertices. */
    for ( n = 0 ; n < mVx ; n++ ) {
      pVrtx[n].number = n+1 ;
      pVrtx[n].Pcoor[i] = dBuf[n] ;
    }
  }

  pChunk->mVertsNumbered = mVx ;

  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "      Found %"FMT_ULG" coordinates for %d-D grid.",
              mVx, mDim ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }

  arr_free ( dBuf ) ;
  status = H5Gclose(grp_id);
  return ( 1 ) ;
}


/******************************************************************************

  h5r_conn:
  Read connectivity.

  Last update:
  ------------
  1May20; set mElemsNumbered.
  7Apr14; fix too short string length in strncpy
  11Apr08; element indices range from 1, set ->invalid, ->term, etc.
  May07: conceived.

  Input:
  ------
  file_id = opened hdf5 file
  pUns    = grid container
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]

  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5r_conn ( hid_t file_id, uns_s *pUns, chunk_struct *pChunk ) {

  hid_t  grp_id ;
  herr_t status;

  ulong_t mConnEl[6] ;
  int i ;
  ulong_t n ;
  int mVxEl, k ;
  ulong_t mZ ;
  char connName[6][10] = {"tri->node","qua->node",
                          "tet->node","pyr->node","pri->node","hex->node"} ;
  char eltName[6][10] = {"Triangles","Quads",
                          "Tets","Pyramids","Prisms","Hexas"};
  char grpName[] = {"Connectivity"} ;
  char znName[LINE_LEN] ;
  vrtx_struct *pVrtx = pChunk->Pvrtx, **ppVx = pChunk->PPvrtx ;
  elType_e elType ;


  /* Conndinates: mDim, mVx. */
  grp_id = h5_open_group ( file_id, grpName ) ;


  /* Find the max buffer size. */
  ulong_t mConnMax, mElMax, mEl ;
  for ( mConnMax = mElMax = 0, elType = tri ; elType <= hex ; elType++ ) {
    i = (int) elType ;
    mConnEl[i] = h5_read_int ( grp_id, connName[i], 0, NULL ) ;
    mConnMax = MAX( mConnEl[i], mConnMax ) ;

    mEl = mConnEl[i]/elemType[elType].mVerts ;
    mElMax = MAX( mElMax, mEl ) ;
    if ( mEl > 0 ) {
      sprintf ( hip_msg, "      Found %"FMT_ULG"  %s.", mEl,eltName[i] ) ;
      hip_err ( blank, 3, hip_msg ) ;
    }
  }

  /* Malloc a buffer. */
  ulong_t *uBuf ;
  uBuf = arr_malloc ( "uBuf in h5r_coor", pUns->pFam, mConnMax, sizeof(*uBuf));
  int *zBuf ; // element to zone pointers, int is fine.
  zBuf = arr_malloc ( "zBuf in h5r_coor", pUns->pFam, mElMax,   sizeof(*zBuf));




  elem_struct *pEl = pChunk->Pelem+1 ;
  ulong_t *uB ;
  int *iZ ;
  for ( elType = tri ; elType <= hex ; elType++ ) {
    i = (int) elType ;

    if ( mConnEl[i] ) {
      mVxEl = elemType[elType].mVerts ;
      mEl = mConnEl[i]/mVxEl ;

      h5_read_ulg ( grp_id, connName[i], mConnEl[i], uBuf ) ;

      /* Is there zone info. */
      strncpy ( znName, connName[i], 5 ) ;
      sprintf ( znName+5, "zone" ) ;
      mZ = h5_read_int ( grp_id, znName, 0, NULL ) ;
      if ( mZ )
        h5_read_int ( grp_id, znName, mZ, zBuf ) ;

      for ( uB = uBuf, iZ = zBuf, n = 0 ; n < mEl ; n++, pEl++ ) {
        /* Fill the elements. */
        init_elem ( pEl, elType, n+1, ppVx ) ;
        for ( k = 0 ; k < mVxEl ; k++ )
          *ppVx++ = pVrtx + *uB++ ;
        if ( mZ )
          pEl->iZone = *iZ++ ;
      }


      if ( verbosity > 3 )
        printf ( "       Found %"FMT_ULG" %s.\n", mEl, elemType[elType].name ) ;


    }
  }
  pChunk->mElemsNumbered = mEl ;

  arr_free ( uBuf ) ;
  arr_free ( zBuf ) ;
  status = H5Gclose(grp_id);
  return ( 1 ) ;
}

/******************************************************************************
  h5r_bcLabels:   */

/*! Read bc labels from Boundary/PatchLabels in the hdf file.
 */

/*

  Last update:
  ------------
  15jul19; implement reading of PatchGeoType.
  21Mar18; pacify compiler by ensuring mPerPairs in correct range.
  21Dec16; catch case of missing Patchlabels.
  9Jul15; initialise pBc to suppress compiler warning.
  21Apr15; fix bug with setting periodic bcs: needs to be ppBc[ nPerPairs[n]-1 ]
  21Dec14; read periodicity information.
  17Dec14: conceived.


  Input:
  ------
  file_id: opened hdf database
  pUns: grid

  Changes To:
  -----------
  pUns

  Returns:
  --------
  the number of boundary patches specified.

*/


int h5r_bcLabels ( hid_t file_id, uns_s *pUns ) {


  hid_t  grp_id ;
  herr_t status;


  /* Read the labels and add the bcs. */
  grp_id = h5_open_group ( file_id, "Boundary" ) ;

  size_t len_str = h5_read_fxStr_len ( grp_id, "PatchLabels" ) ;
  if ( !len_str )
    /* No group 'PatchLabels'. */
    return ( 0 ) ;


  int mBc = h5_read_fxStr ( grp_id, "PatchLabels", 0, fxStr240, NULL ) ;

  if ( !mBc ) {
    hip_err ( warning, 1, "no boundaries found." ) ;
    return ( 0 ) ;
  }

  char *bcLabels = arr_malloc ( "bcLabels in h5r_bcLabels", pUns->pFam,
                                mBc, len_str+1 ) ;

  pUns->mBc = mBc ;
  /* A list of bcs for this rootchunk only. */
  pUns->ppBc = arr_malloc ( "pUns->ppBc in h5r_bcLabels", pUns->pFam,
                            mBc, sizeof ( *pUns->ppBc ) ) ;


  if ( verbosity > 1 ) {
    sprintf ( hip_msg, "      Found %d bnd patches.", mBc ) ;
    hip_err ( blank, 3, hip_msg ) ;
  }

  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "      Reading labels : ") ;
    hip_err ( blank, 3, hip_msg ) ;
  }

  h5_read_fxStr ( grp_id, "PatchLabels", mBc, fxStr240, bcLabels ) ;

  char *pBcLbl ;
  size_t ldiff = mBc*len_str ;
  bc_struct *pBc = NULL ;
  int nBc=0 ;
  for ( pBcLbl = bcLabels ; pBcLbl < bcLabels+ldiff ; pBcLbl += len_str ) {
    trim ( pBcLbl ) ;

    if ( ( pBc = find_bc ( pBcLbl, 2 ) ) ) {
	sprintf ( hip_msg, "bc '%s' already assigned. Bc's will coalesce.",
		 pBcLbl ) ;
        hip_err ( warning, 1, hip_msg ) ;
    }
    else if ( !( pBc = find_bc ( pBcLbl, 1 ) ) ) {
      hip_err ( fatal, 0, "could not add bc in h5r_bcLabels." ) ;
    }
    else if ( verbosity > 3 ) {
      trim(pBcLbl);
      sprintf ( hip_msg, "             %s", pBcLbl ) ;
      hip_err ( blank, 3, hip_msg ) ;
    }

    pUns->ppBc[nBc++] = pBc ;
  }

  arr_free ( bcLabels ) ;




  /* Read boundary types (geoType). */
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "      Reading boundary types : ") ;
    hip_err ( blank, 3, hip_msg ) ;
  }

  if ( h5_dset_exists ( grp_id, "PatchGeoType" ) ) {

    char *bcGeoType = arr_malloc ( "bcGeoType in h5r_bcLabels", pUns->pFam,
                                   mBc, sizeof ( *bcGeoType ) ) ;

    h5_read_char ( grp_id, "PatchGeoType", mBc, bcGeoType ) ;

    for ( nBc = 0 ; nBc < mBc ; nBc++ )
      pUns->ppBc[nBc]->geoType = char2geoType ( bcGeoType[nBc] ) ;

    arr_free ( bcGeoType ) ;
  }
  else {
    /* Assume all boundaries are full boundaries. */
    for ( nBc = 0 ; nBc < mBc ; nBc++ )
      pUns->ppBc[nBc]->geoType = bnd ;
  }


  /* Done with 'Boundaries'. */
  status = H5Gclose(grp_id);


  /* Check for periodicity information. */
  int mPerEntries, n ;
  ulong_t nPerPairs[2*MAX_PER_PATCH_PAIRS] = {0} ;
  char typeLabel[5] ;
  if ( h5_grp_exists ( file_id, "Periodicity" ) ) {
    grp_id = h5_open_group ( file_id, "Periodicity" ) ;

    mPerEntries = h5_read_ulg ( grp_id, "periodic_patch", 0, NULL ) ;
    if ( mPerEntries > 2*MAX_PER_PATCH_PAIRS )
      hip_err ( fatal, 0, "too many periodic patches, only 10 compiled." ) ;
    h5_read_ulg ( grp_id, "periodic_patch", mPerEntries, nPerPairs ) ;

    sprintf ( hip_msg, "      Found %d periodic patch pair[s].", mPerEntries/2 ) ;
    hip_err ( blank, 3, hip_msg ) ;

    status = H5Gclose(grp_id);

    if ( mPerEntries < 0 ) {
      hip_err ( fatal, 0, "negative number of periodic pairs, using zero." );
      mPerEntries = 0 ;
    }
    else if ( mPerEntries > 2*MAX_PER_PATCH_PAIRS ){
      hip_err ( fatal, 0, "too many periodic pairs, using MAX_PER." );
      mPerEntries = 2*MAX_PER_PATCH_PAIRS ;
    }

    /* Set bc types. first ones are lower, second ones are upper . */
    for ( n = 0 ; n < mPerEntries ; n++ ){
      sprintf ( typeLabel, "l%02d", n/2 ) ;
      if ( n%2 )
        // in C numbering from 0, first is even, second is odd.
        typeLabel[0] = 'u' ;
      strncpy ( pUns->ppBc[ nPerPairs[n]-1 ]->type, typeLabel, MAX_BC_CHAR ) ;

      /* Update summary type. */
      set_bc_e ( pBc ) ;
    }
  }

  return ( mBc ) ;
}

/******************************************************************************

  h5r_bnd_fc2el:
  Read boundary info given face to element pointers.

  Last update:
  ------------
  8jul20; check whether face to element pointer points to valid element.
  17Apr18; rename as fc2el.
  6Apr12; include test to compare mBc (e.g. from asciiBound) to Lidx.
  11Apr08; have ppBc, pBndPatch and pBndFc start at their start indices. (see cpre_uns.h)
  May 07: conceived.

  Input:
  ------
  grp_id = hdf5 file positioned in boundaries group
  pUns    = grid container
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]

  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5r_bnd_fc2el ( hid_t grp_id, uns_s *pUns, chunk_struct *pChunk ) {

  const int mBndFc = pChunk->mBndFaces, mBc = pChunk->mBndPatches ;

  herr_t status;

  ulong_t *uEl ; // face to element pointer.
  int *nFc ; // canonical face number in the element.
  ulong_t *lBc ; // lidx of the faces for each bc.
  int i, j ;
  ulong_t n ; // index for faces.
  ulong_t mFc ; // number of faces, shouldn't be large but use ulong_t anyways.
  char fcType[3][4] = {"bi", "tri", "qua" }, fcName[TEXT_LEN] ;
  bndFc_struct *pBf = pChunk->PbndFc+1 ; /* note that PbndFc starts with 1. */
  bndPatch_struct *pBP ;
  elem_struct *pElem = pChunk->Pelem ;
  bc_struct *pBc ;

  /* Malloc a buffer. */
  uEl = arr_malloc ( "uEl in h5r_bnd_fc2el", pUns->pFam, mBndFc, sizeof( *uEl ));
  nFc = arr_malloc ( "nFc in h5r_bnd_fc2el", pUns->pFam, mBndFc, sizeof( *nFc ));
  lBc = arr_malloc ( "lBc in h5r_bnd_fc2el", pUns->pFam, mBc,    sizeof( *lBc ));



  sprintf ( hip_msg, "      Reading %d bnd faces.\n", mBndFc ) ;
  hip_err ( blank, 3, hip_msg ) ;


  for ( i = 0 ; i < 3 ; i++ ) {
    /* loop over all 3 face types. */
    sprintf ( fcName, "bnd_%s->elem", fcType[i] ) ;
    mFc = h5_read_int ( grp_id, fcName,  0, NULL ) ;

    if ( mFc ) {
      h5_read_ulg ( grp_id, fcName,  mFc, uEl ) ;

      sprintf ( fcName, "bnd_%s->face", fcType[i] ) ;
      h5_read_int ( grp_id, fcName,  mFc, nFc ) ;

      sprintf ( fcName, "bnd_%s_lidx", fcType[i] ) ;
      h5_read_ulg ( grp_id, fcName,  mBc, lBc ) ;

      /* Loop over all boundaries. Note that pBndPatch numbers from 1, while
         ppBc numbers from 0. Oh the beauty of Fortran and C ... */
      for ( n = j = 0 ; j < mBc ; j++ ) {
        pBc = pUns->ppBc[j] ;
        pBP = pChunk->PbndPatch + j+1 ;
        pBP->mBndFc = lBc[j] - n ;
        pBP->PbndFc = pBf ;
        pBP->Pchunk = pChunk ;

        /* Loop over all faces. Note that pUns->pBndFc numbers from 1. */
        for ( ; n < lBc[j] ; n++, pBf++ ) {
          if ( uEl[n] > pUns->pRootChunk->mElems ) {
            sprintf ( hip_msg, "face %"FMT_ULG" references element %"FMT_ULG",\n"
                      "         but there are only %"FMT_ULG" elements in the grid.",
                      n, uEl[n],  pUns->pRootChunk->mElems ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }
          pBf->Pelem = pElem + uEl[n] ;
          pBf->nFace = nFc[n] ;
          pBf->Pbc   = pBc ;
        }
      }
    }
  }

  arr_free ( uEl ) ;
  arr_free ( nFc ) ;
  arr_free ( lBc ) ;

  return ( 1 ) ;
}



/******************************************************************************

  h5r_bnd_fc2vx:
  Read boundary info given face to vertex pointers.

  Last update:
  ------------
  2Mar19; fix bug with duplicate allocation of pBndFc.
  17Apr18; conceived.

  Input:
  ------
  grp_id  = hdf5 file positioned in boundaries group
  pUns    = grid container
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]

*/

void h5r_bnd_fc2vx ( hid_t grp_id, uns_s *pUns, chunk_struct *pChunk ) {

  /* Find the number of faces of each type, and the total. */
  int i ;
  char fcName[TEXT_LEN] ;
  char fcType[3][4] = {"bi", "tri", "qua" } ;
  int mVxFc[3] = {2,3,4} ;
  int mFcConn ;
  int maxConnType = 0 ;
  int mBndFc = 0 ;
  for ( i = 0 ; i < 3 ; i++ ) {
    /* loop over all 3 face types. */
    sprintf ( fcName, "bnd_%s->node", fcType[i] ) ;
    mFcConn = h5_read_int ( grp_id, fcName,  0, NULL ) ;

    maxConnType = MAX( maxConnType, mFcConn ) ;
    mBndFc += mFcConn/mVxFc[i] ;
  }
  if ( mBndFc != pChunk->mBndFaces ) {
    sprintf ( hip_msg, "Expected %"FMT_ULG", found %d boundary faces in h5r_bnd_fc2vx.",
              pChunk->mBndFaces, mBndFc ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Record sizes and alloc. */
  pUns->mBndFcVx = mBndFc ;
  bndFcVx_s *pBndFcVx = pUns->pBndFcVx =
    arr_malloc ( "pUns->pBndFcVx in h5r_bnd_fc2vx", pUns->pFam,
                 mBndFc, sizeof ( *pBndFcVx ) );

  ulong_t *pnVxFc = arr_malloc ( "nVxFc in h5r_bnd_fc2vx", pUns->pFam,
                                maxConnType, sizeof( *pnVxFc ));

  const int mBc = pChunk->mBndPatches ;
  ulong_t *plBc = arr_malloc   ( "lBc   in h5r_bnd_fc2vx", pUns->pFam,
                                 mBc,    sizeof( *plBc ));



  /* Read face to node pointers. */
  int n, j, k ;
  bndFcVx_s *pBf = pBndFcVx ;
  ulong_t *pnVF ;
  bc_struct *pBc ;
  for ( i = 0 ; i < 3 ; i++ ) {
    /* loop over all 3 face types. */
    sprintf ( fcName, "bnd_%s->node", fcType[i] ) ;
    mFcConn = h5_read_int ( grp_id, fcName,  0, NULL ) ;

    if ( mFcConn ) {
      /* There are faces of this type. */
      h5_read_ulg ( grp_id, fcName,  mFcConn, pnVxFc ) ;

      sprintf ( fcName, "bnd_%s_lidx", fcType[i] ) ;
      h5_read_ulg ( grp_id, fcName,  mBc, plBc ) ;

      /* Loop over all boundaries. Note that pBndPatch numbers from 1, while
         ppBc numbers from 0. Oh the beauty of Fortran and C ... */
      pnVF = pnVxFc ;
      for ( n = j = 0 ; j < mBc ; j++ ) {
        pBc = pUns->ppBc[j] ;

        /* Loop over all faces. Note that pUns->pBndFc numbers from 1. */
        for ( ; n < plBc[j] ; n++, pBf++ ) {
           /* Tri or qua boundary face in 3D, or Linear bnd. fc in 2D. */
          if ( pBf > pBndFcVx+mBndFc )
            hip_err ( fatal, 0, "too many boundary faces in h5r_bnd_fc2vx." ) ;

          pBf->pBc = pBc ;
          pBf->mVx = mVxFc[i] ;
          /* Forming vertices. . */
          for ( k = 0 ; k < mVxFc[i] ; k++ ) {
            pBf->ppVx[k] = pChunk->Pvrtx + *pnVF++ ;
          }
        }
      }
    }
  }


  /* Compute boundary face to element pointers. */
  if ( !match_bndFcVx ( pUns ) )
    hip_err ( fatal, 0, "could not match boundary faces in h5r_bnd_fc2vx." ) ;



  /* Free. */
  pUns->mBndFcVx = 0 ;
  arr_free ( pUns->pBndFcVx ) ;
  pUns->pBndFcVx = NULL ;
  arr_free ( pnVxFc ) ;
  arr_free ( plBc ) ;

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  h5r_bnd_bvx2vx;
*/
/*! Read ordered list of boundary nodes and lidx from file, recreate bface->el conn.
 *
 */

/*

  Last update:
  ------------
  29Apr20: conceived.


  Input:
  ------
  grp_id  = hdf5 file positioned in boundaries group
  pUns    = grid container
  pChunk  = allocated chunk

*/

void h5r_bnd_bvx2vx ( hid_t grp_id, uns_s *pUns, chunk_struct *pChunk ) {
#define FUNLOC "in h5r_bnd_bvx2vx"

  /* Count/check number of bcs. */
  int mBc = h5_read_int ( grp_id, "bnode_lidx",  0, NULL ) ;
  if ( mBc != pUns->mBc ) {
    sprintf ( hip_msg, "found %d, expected %d boundaries "FUNLOC".",
              mBc, pUns->mBc ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  int mBvx2Vx = pUns->mBvx2Vx = h5_read_int ( grp_id, "bnode->node",  0, NULL ) ;
  if ( !mBvx2Vx )
    hip_err ( fatal, 0, "zero boundary nodes found "FUNLOC"." ) ;


  pUns->pnBvx2Vx_fidx = arr_malloc ( "pUns->pnBvx2Vx_fidx in h5r_bnd_bvx2vx", pUns->pFam,
                                     mBc+1, sizeof ( *pUns->pnBvx2Vx_fidx ) ) ;
  pUns->pnBvx2Vx = arr_malloc ( "pUns->pnBvx2Vx in h5r_bnd_bvx2vx", pUns->pFam,
                                     mBvx2Vx, sizeof ( *pUns->pnBvx2Vx ) ) ;


  /* Read list and lidx from file. */
  h5_read_int ( grp_id, "bnode_lidx",  mBc, pUns->pnBvx2Vx_fidx ) ;
  /* Can be done in-situ. */
  ilidx2fidx ( pUns->pnBvx2Vx_fidx, mBc, pUns->pnBvx2Vx_fidx ) ;

  h5_read_int ( grp_id, "bnode->node", mBvx2Vx, pUns->pnBvx2Vx ) ;




  /* Compute boundary face to element pointers. */
  if ( !match_bvx2vx ( pUns ) )
    hip_err ( fatal, 0, "could not match boundary faces "FUNLOC"." ) ;


  /* Free. */
  arr_free ( pUns->pnBvx2Vx_fidx ) ;
  pUns->pnBvx2Vx_fidx = NULL ;
  arr_free ( pUns->pnBvx2Vx ) ;
  pUns->pnBvx2Vx = NULL ;

  return  ;
}


/******************************************************************************

  h5r_bnd:
  Read boundary info.

  Last update:
  ------------
  17Apr18; conceived

  Input:
  ------
  file_id = opened hdf5 file
  pUns    = grid container
  pChunk  = allocated chunk

  Changes To:
  -----------
  pChunk[0]

  Returns:
  --------
  0 on failure, 1 on success.

*/

int h5r_bnd ( hid_t file_id, uns_s *pUns, chunk_struct *pChunk ) {
#undef FUNLOC
#define FUNLOC "in h5r_bnd"


  /* Boundary. */
  char grpName[] = {"Boundary"} ;
  hid_t grp_id = h5_open_group ( file_id, grpName ) ;


  /* Check if fc2el info is present. */
  char fcType[3][4] = {"bi", "tri", "qua" } ;
  char setName[TEXT_LEN] ;
  int i ;
  int has_fc2el = 0, fc2el, fc2nFc ;
  int has_fc2vx = 0 ;
  for ( i = 0 ; i < 3 ; i++ ) {
    /* Loop over all 3 face types and check whether both pointer sets for that
       type exist. But existence for one type of face is enough. */
    sprintf ( setName, "bnd_%s->elem", fcType[i] ) ;
    fc2el = ( h5_obj_exists ( grp_id, setName ) == H5I_DATASET ? 1 : 0 ) ;
    sprintf ( setName, "bnd_%s->face", fcType[i] ) ;
    fc2nFc = ( h5_obj_exists ( grp_id, setName ) == H5I_DATASET ? 1 : 0 ) ;
    if ( fc2el && fc2nFc ) {
      has_fc2el = 1 ;
      break ;
    }

    sprintf ( setName, "bnd_%s->node", fcType[i] ) ;
    has_fc2vx |= ( h5_obj_exists ( grp_id, setName ) == H5I_DATASET ? 1 : 0 ) ;
  }

  int has_bvx2vx = 0 ;
  if ( !has_fc2el && !has_fc2vx ) {
    /* Is there a list of boundary nodes? */
    int bvx2vx = 0 , bvxl = 0 ;
    sprintf ( setName, "bnode->node" ) ;
    has_bvx2vx = ( h5_obj_exists ( grp_id, setName ) == H5I_DATASET ? 1 : 0 ) ;
  }


  /* Need ppBc to assign to faces. If none given yet, create some. */
  if ( !pUns->ppBc ) {
    int kBc ;
    char bcName[LINE_LEN] ;
    pUns->ppBc = arr_realloc ( "ppBc "FUNLOC".", pUns->pFam, pUns->ppBc,
                               pUns->mBc, sizeof( *pUns->ppBc ) ) ;
    for ( kBc = 0 ; kBc < pUns->mBc ; kBc++ ) {
      sprintf ( bcName, "hip_bc_%d", kBc+1 ) ;
      pUns->ppBc[kBc] = find_bc ( bcName, 1 ) ;
    }
  }



  int retVal = 0 ;
  if ( has_fc2el ) {
    /* Read face to element pointers. */
    hip_err ( info, 3, "Reading boundary face to element pointers." ) ;
    retVal = h5r_bnd_fc2el ( grp_id, pUns, pChunk ) ;
  }

  else if ( has_fc2vx ) {
    /* Read face to vertex pointers, recreate face to elem pointers. */
    hip_err ( info, 3, "Reading boundary face to vertex pointers,\n"
              "             recreating face to element connectivity." ) ;
    h5r_bnd_fc2vx ( grp_id, pUns, pChunk ) ;
  }

  else if ( has_bvx2vx ) {
   /* Read bnode to node pointers, recreate face to elem pointers. */
    hip_err ( info, 3, "Reading boundary node to vertex pointers,\n"
              "             recreating face to element connectivity." ) ;
    h5r_bnd_bvx2vx ( grp_id, pUns, pChunk ) ;
   }

  else {
    /* No boundaries. */
    hip_err ( warning, 1, "no boundary description found. This may fail." ) ;
    retVal = 1 ;
  }

  herr_t status = H5Gclose(grp_id) ;
  return ( retVal ) ;
}

/******************************************************************************
  h5r_zone:   */

/*! Read zonal info from hdf5 grid file.
 *
 */

/*

  Last update:
  ------------
  17Feb17; set parType properly for vectors here, drop isVec.
  21Dec16; add doWarnDupl arg to zone_add.
  27Jun14; use chronological numbering per grid for zones, ignore
           the zone numbering from the mesh.
  26Jun14; introduce -z flag to skip over zones.
  12Dec13; intro solParam
  15Jul13; have kZn run 1 .. mZones.
  5Apr13; fix argument bug in call of h5_nxt_grp, h5_read_dat.
  2Sep12; dont check variable names/types for read -all.
  28Aug11; development bug fixes completed.
  26Jul11: conceived.


  Input:
  ------
  file_id: opened file
  pUns: grid
  is_sol: non-zero if the zone/params are read from a solution file,
          i.e. Parameters are stored as solParam. Otherwise look for 'Parameters'.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int h5r_zone  ( hid_t file_id, uns_s *pUns, int isSol ) {

  if ( !h5r_flag_zone )
    /* Ignore zones. */
    return ( 0 ) ;
  else if ( !h5_grp_exists ( file_id, "Zones" ) )
    /* No zonal info given. */
    return ( 0 ) ;

  hid_t grp_id = h5_open_group ( file_id, "Zones" ) ;

  hid_t zn_id = 0, parGrp_id, parType_id ;
  char zn_char_no[LINE_LEN], zn_name[LINE_LEN], par_name[LINE_LEN],
    expr[LINE_LEN] ;
  int kZn = 0, kPar, dim ;
  ulong_t dsize ;
  void *pv ;
  parType_e parType ;
  int isVec ;
  hid_t par_id ;
  htri_t isVecExists ;
  char parLabel[2][LINE_LEN] = { "Parameters", "SolParameters" } ;

  /* restrict isSol to 0, 1 */
  isSol = ( isSol ? 1 : 0 ) ;

  while ( h5_nxt_grp ( grp_id, &kZn, zn_char_no ) ) {
    zn_id = h5_open_group ( grp_id, zn_char_no ) ;


    /* Declare the zone. */
    h5_read_one_fxStr ( zn_id, "ZoneName", fxStr240, zn_name ) ;
    /* Note that zn_number runs 1...mZones and is referenced by the elements,
       kZn is the position in pUns->pZones, now also runs 1..mZones. */
    /* int iZone = atoi( zn_char_no ) ; Use chrono numbering per grid. */
    const int doWarnDupl =0 ;
    kZn = zone_add ( pUns, zn_name, 0, doWarnDupl ) ;


    if ( h5_grp_exists ( zn_id, parLabel[isSol] ) ) {
      /* Read all parameters. */
      parGrp_id = h5_open_group ( zn_id, parLabel[isSol] ) ;

      kPar = 0 ;
      while( h5_nxt_dset ( parGrp_id, &kPar, par_name ) ) {
        dim = h5_read_dat ( parGrp_id, par_name, &parType_id, 0, NULL ) ;
        parType = h5_dclass2parType( parType_id ) ;
        dsize = parTypeSize[parType] ;
        pv = arr_malloc ( "pv in h5r_zone", pUns->pFam, dim, dsize ) ;
        h5_read_dat ( parGrp_id, par_name, &parType_id, dim, pv ) ;

        /* Check for vector attribute. */
        isVec = 0 ;
        par_id =  H5Dopen( parGrp_id, par_name, H5P_DEFAULT ) ;
        isVecExists = H5Aexists( par_id, "IsVector" ) ;
        if ( isVecExists > 0 ) {
          if ( dim != pUns->mDim ) {
            sprintf ( hip_msg, "array size %d does not match vector length %d,"
                      " vector flag ignored.", dim, pUns->mDim ) ;
            hip_err ( warning, 1, hip_msg ) ;
          }
          else {
            isVec = 1 ; // no longer passed down into zn_param_mod, still needed?
            parType = parVec ;
          }
        }
        H5Dclose( par_id ) ;

        /* Zones are numbered internally from 1. */
        sprintf ( expr, "%d", kZn ) ;
        if ( isSol )
          zone_add_solParam ( pUns, expr, parType, isVec, par_name, dim, pv ) ;
        else
          zone_add_param ( pUns, expr, parType, isVec, par_name, dim, pv ) ;
      }
      H5Gclose ( parGrp_id ) ;
    }
    H5Gclose ( zn_id ) ;
  }
  H5Gclose ( grp_id ) ;

  return ( 0 ) ;
}

/***************************************************************************

  PUBLIC

**************************************************************************/


/******************************************************************************

  read_hdf5_sol:
  .

  Last update:
  ------------
  8may24; use renamed unslice_scalar_var
  27Jan23; pacify compiler, zeor grp_id.
  9May22; ignore nnode in Parameters when reading all fields 
          read Parameters section only if it exists.
  18Dec19; factor out set_one_var_cat_name.
  5Sep19; make mVx and mVar ulong_t.
  16Apr18: GS: correct versionstring test by adding test on gitid.
  21Feb17; flag warnings for unread datasets.
  7Sep15; add ielee to the restart.hdf list.
  1Apr12; use h5r_flag_all to trigger hdfa behaviour, drop keyword.
  29Aug11; call h5_isVec to set pVar->isVec.
  20Dec10; fix bug with cut&paste error for FiciveSpecies
  3Jul10; read and carry scalar and node-based vectors in Parameters.
  20Sep09; init mEq to zero.
  7Apr09; allow hdfa without GaseousPhase.
  4Apr09; GS: read rhol only if it exists.
          JDM: pass keyword, to allow reading the entire sol.
          JDM: replace varTypeS with varList.
          set pVar->grp from the hdf group name.
  15Apr08; check for file existence using fopen.
  11Dec07; fix bugs in reading fictiveSpecies (merci GS)
  15Sep06: derived from write_hdf5_sol

  Input:
  ------


  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int read_hdf5_sol ( uns_s *pUns, char *solFile ) {

  hid_t file_id=0, grp_id=0, dspc_id, dset_id ;   /* identifier */
  herr_t status ;

  avbpFmt_e avbpFmt = v6_0 ;

  const int mDim = pUns->mDim ;
  const hbool_t follow_link = {0} ;


  FILE* Fsol ;
  ulong_t mVx = pUns->pRootChunk->mVerts ;
  ulong_t mVar ;
  int mGrps, mGrpsGP, idx ;
  int nEq ;
  hsize_t iDim ;

  chunk_struct *pChunk = pUns->pRootChunk ;
  vrtx_struct *pVx ;

  char grpName[MAX_GROUPS][TEXT_LEN], gasVarName[5][LINE_LEN], dset_name[TEXT_LEN] ;

  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;


  /* Try to open the file with fopen to avoid the ugly and non-descript
     err msg of hdf5 in case of a non-existent file. */
  Fsol = fopen ( solFile, "r" ) ;
  if ( !Fsol ) {
    sprintf ( hip_msg, "could not open solution file in read_hdf5_sol.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else
    fclose ( Fsol ) ;

  file_id = H5Fopen( solFile, H5F_ACC_RDONLY, H5P_DEFAULT ) ;


  /* If solution type is average switch by default to flag_all mode */
  const char grp_name[7] = "Average";
  if ( H5Lexists( file_id, grp_name, H5P_DEFAULT ) ) {
    h5r_flag_all = 1;
  }

  avbpFmt = h1_0 ;
  if ( h5r_flag_all )
    pUns->restart.hdf.iniSrc = hdfa ;
  else
    pUns->restart.hdf.iniSrc = hdf ;
  pUns->restart.hdf.m5pList = 0 ;



  if ( verbosity > 2 )
    printf ( "  Reading hdf5 solution from %s\n", solFile ) ;

  /* All vars are written as vectors, so allocate one. */
  double *dBuf = arr_malloc ( "dBuf in read_hdf_sol", pUns->pFam,
                              mVx, sizeof( double ) ) ;








  /* Get a list of all variables to store, with group and varname,
     store all global scalars to be kept in restart.hdf */


  /* Search for a GaseousPhase. */
  for ( mGrps=0, idx = 0 ; h5_nxt_grp ( file_id, &idx, grpName[0] ) ;  )
    if ( !strcmp ( grpName[0], "GaseousPhase" ) ) {
      /* GaseousPhase needs to be treated block-wise. */
      mGrps = 1 ;
      break ;
    }

  /* A GaseousPhase has to be present with hdf, is optional with hdfa. */
  if ( !h5r_flag_all &&  mGrps == 0 ) {
    hip_err ( fatal, 0, "could not find a GaseousPhase in hdf solution file.\n" ) ;
  }

  /* There is a GaseousPhase, look for std. variables. The first five
     have to be conservative flow variables in order. */
  int   mEq=0, mEqF=0, mEqP=0, k ;
  if ( !strcmp( grpName[0], "GaseousPhase" ) ) {
    if ( 0 <= H5Gget_objinfo(file_id, grpName[0], follow_link, NULL  ) ) {
      grp_id = H5Gopen(file_id, grpName[0], H5P_DEFAULT ) ;

      strcpy ( gasVarName[0], "rho" ) ;
      strcpy ( gasVarName[1], "rhou" ) ;
      strcpy ( gasVarName[2], "rhov" ) ;
      switch ( mDim ) {
      case 2:
        strcpy ( gasVarName[3], "rhoE" ) ;
        break ;
      case 3 :
        strcpy ( gasVarName[3], "rhow" ) ;
        strcpy ( gasVarName[4], "rhoE" ) ;
        break ;
      default :
        hip_err ( fatal, 0, "no such dimension in read_hdf5_sol."
                  " This shouldn't have happened." ) ;
      }

      for ( k = 0 ; k < mDim+2 ; k++ ) {
        /* Read the named variables in that order. */
        mVar = h5_read_dbl ( grp_id, gasVarName[k], mVx, NULL ) ;
        if ( mVar != mVx ) {
          sprintf ( hip_msg,
                    "need %"FMT_ULG" unknowns in vector %s,"
                    " found %"FMT_ULG" in read_hdf5_sol\n",
                    mVx, gasVarName[k], mVar ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }
        /*
        pVar[mEq].cat = ns ;
        pVar[mEq].flag = 1 ;
        pVar[mEq].isVec = h5r_isVec( "GaseousPhase", gasVarName[k] ) ;
        strncpy ( pVar[mEq].name, gasVarName[k], LEN_VARNAME ) ;
        strncpy ( pVar[mEq].grp, "GaseousPhase", LEN_VARNAME ) ; */
        set_one_var_cat_name ( pVar, mEq, gasVarName[k], "GaseousPhase" ) ;
        mEq++ ;
        mEqF++ ;
      }

      if ( verbosity > 1 ) {
        /* Check for any unread vars in GaseousPhase. */
        int idx = 0 ;
        char nxtVarName[LINE_LEN] ;
        h5_list_unread_dset ( grp_id, grpName[0], mDim+2, gasVarName ) ;
      }


      /* Close GaseousPhase. */
      status = H5Gclose(grp_id);
    }
    else {
      sprintf ( hip_msg, "file contains no group %s in read_hdf5_sol.\n",
                grpName[0] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }




  /* Parameters. */
  if ( h5_grp_exists ( file_id, "Parameters" ) ) {
    grp_id = H5Gopen(file_id, "Parameters", H5P_DEFAULT ) ;

    /* Identification string.
       h5_read_char ( grp_id, "versionstring", 20, versionString ) ; */


    if ( h5r_flag_all ) {
      /* hdfa: duplicate all fields. */
      for ( idx = 0 ; h5_nxt_dset ( grp_id, &idx, dset_name ) ;  ) {

        /*printf( "-> %s \n",&dset_name); */
        if ( strncmp( dset_name,"versionstring",5) &&
             strncmp( dset_name,"nnode",5) &&
             strncmp( dset_name,"gitid",5)  ) {
          /* Dataset */
          dset_id =  H5Dopen(grp_id, dset_name, H5P_DEFAULT ) ;
          /* Dataspace */
          dspc_id = H5Dget_space(dset_id);
          /* Get the dims. */
          iDim = H5Sget_simple_extent_dims( dspc_id, NULL, NULL ) ;

          if ( iDim != 1 ) {
            H5Sclose ( dspc_id ) ;
            sprintf ( hip_msg,
                      "found %d dimensions for vector %s in read_hdf5_sol.\n", mDim,dset_name ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }

          /* How many elements in that vector? */
          H5Sget_simple_extent_dims( dspc_id, &iDim, NULL ) ;
          if ( iDim == 1 ) {
            /* A single scalar, stick it into hdf-restart. */
            h5r_add_pList( grp_id, dset_name, dset_id, dspc_id, &pUns->restart ) ;
          }
          else if ( mVx == h5_read_dbl ( grp_id, dset_name, mVx, NULL ) ) {
            /* A vector of dbls defined over all nodes, make it a 'parameter' variable. */
            /* pVar[mEq].cat = param ;
               pVar[mEq].flag = 1 ;
               strncpy ( pVar[mEq].name, dset_name, LEN_VARNAME ) ;
               strncpy ( pVar[mEq].grp, "Parameters", LEN_VARNAME ) ;*/
            set_one_var_cat_name ( pVar, mEq, dset_name, "Parameters" ) ;
            mEq++ ;
            mEqP++ ;
          }
          else {
            sprintf ( hip_msg, "found parameter vector of length %d named %s\n"
                      "     hip can only treat or node-based scalars\n",
                      (int) iDim,  dset_name ) ;
            hip_err ( warning, 1, hip_msg ) ;
          }
        }
        /* Were there any solution fields in Parameters? */
        if ( mEqP ) {
          strcpy( grpName[mGrps], "Parameters" ) ;
          mGrps++ ;
        }
      }
    }
    else {
      /* Not hdfa, read only predefined fields. */
      h5_read_int ( grp_id, "niter", 1, &pUns->restart.hdf.itno ) ;
      h5_read_dbl ( grp_id, "dtsum", 1, &pUns->restart.hdf.dtsum ) ;
      if ( h5_dset_exists ( grp_id, "rhol" ) ) {
        h5_read_dbl ( grp_id, "rhol",  1, &pUns->restart.hdf.rhol ) ;
      }
      if ( h5_dset_exists ( grp_id, "ielee" ) ) {
        // If it exists, it should only be 'true'. Read first char.
        h5_read_char ( grp_id, "ielee",  1, &pUns->restart.hdf.ielee ) ;
      }
    }
  }


  /* Close parameters. */
  status = H5Gclose(grp_id);


  /* Which other groups are there to be scanned? */
  mGrpsGP = mGrps ;
  if ( h5r_flag_all ) {
    /* Read all present variables and categories. */
    for ( idx = 0 ; h5_nxt_grp ( file_id, &idx, grpName[mGrps] ) ;  )
      if ( strcmp ( grpName[mGrps], "GaseousPhase" ) &&
           strcmp ( grpName[mGrps], "Parameters" ) ) {
        /* GaseousPhase and Parameters already counted. */
        mGrps++ ;
      }
  }
  else {
    /* Other Fields for standard read. */
    strcpy ( grpName[1], "RhoSpecies" ) ;
    /* Note: maximum number of groups is MAX_GROUPS in cpre.h, currently 10. */
    mGrps = 2 ;
    if ( h5_grp_exists ( file_id, "LiquidPhase" ) ) {
      strcpy ( grpName[mGrps], "LiquidPhase" ) ;
      mGrps += 1 ;
      }
    if ( h5_grp_exists ( file_id, "FictiveSpecies" ) ) {
      strcpy ( grpName[mGrps], "FictiveSpecies" ) ;
      mGrps += 1 ;
      }
  }




  /* Count how many equations there are in each of the other groups. */
  for ( k = mGrpsGP ; k < mGrps ; k++ ) {
    /* Open only if group exists */
    grp_id = H5Gopen(file_id, grpName[k], H5P_DEFAULT ) ;
    for ( idx = 0 ; h5_nxt_dset ( grp_id, &idx, dset_name ) ;  ) {
      mVar = h5_read_dbl ( grp_id, dset_name, mVx, NULL ) ;
      if ( mVar != mVx ) {
        sprintf ( hip_msg, " Dataset %s is not the same size as the mesh. Skipping.",
                  dset_name);
        hip_err ( warning, 0, hip_msg ) ;
      }
      else {
        /* Add this unknown field/equation. */
        /* pVar[mEq].cat = h5r_varCat( grpName[k] ) ;
           pVar[mEq].isVec = h5r_isVec( grpName[k], dset_name ) ;
           pVar[mEq].flag = 1 ;
           strncpy ( pVar[mEq].name, dset_name, LEN_VARNAME ) ;
           strncpy ( pVar[mEq].grp, grpName[k], LEN_VARNAME ) ; */
        set_one_var_cat_name ( pVar, mEq, dset_name, grpName[k] ) ;
        mEq++ ;
      }

      if ( mEq >= MAX_UNKNOWNS ) {
        sprintf ( hip_msg, "found more unknowns than the %d allowed.\n"
                  "       Ask your friendly hip developer"
                  " to increase MAX_UNKNOWNS in cpre.h\n", MAX_UNKNOWNS ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }

    }
    /* Close Group. */
    status = H5Gclose(grp_id);
  }

  pVL->mUnknowns = mEq ;
  pVL->mUnknFlow = mEqF ;
  pVL->varType = cons ;




  /* Alloc. */
  pChunk->Punknown = arr_malloc ( "Pchunk->Punknown in read_hdf5_sol", pUns->pFam,
                                  ( mVx + 1 )*mEq, sizeof( double ) ) ;

  /* Set the vrtx pointer. */
  double *pUn ;
  for ( pVx = pChunk->Pvrtx+1, pUn = pChunk->Punknown + mEq ;
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pUn += mEq )
    pVx->Punknown = pUn ;


  /* Read the unknowns. */
  ulong_t nMin, nMax ;
  double valMin, valMax ; //, *pDbl ;
  grpName[0][0] = '\0' ;
  grp_id = 0 ;
  int kEq ;
  for ( kEq = 0 ; kEq < mEq ; kEq++ ) {
    if ( strcmp( grpName[0], pVar[kEq].grp ) ) {
      /* New group. */
      if ( grp_id ) H5Gclose(grp_id) ;
      strcpy( grpName[0], pVar[kEq].grp ) ;
      grp_id = H5Gopen( file_id, grpName[0], H5P_DEFAULT ) ;
    }

    /* Read solution file into buffer. */
    h5_read_dbl ( grp_id, pVar[kEq].name, mVx, dBuf ) ;

    /* unslice/spread into nodal storage for unknowns. */
    /* valMin = TOO_MUCH, valMax = -TOO_MUCH ;
    for ( pDbl = dBuf, pVx = pChunk->Pvrtx+1 ;
          pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pDbl++ ) {
      pVx->Punknown[kEq] = *pDbl ;
      traceMinMax ( pDbl, pVx - pChunk->Pvrtx, &valMin, &nMin, &valMax, &nMax ) ;
      } */
    unslice_scalar_var ( pChunk, pChunk->mVerts, kEq, dBuf,
                         &valMin, &nMin, &valMax, &nMax ) ;

    if ( verbosity > 3 )
      printf ( "      %2d: Var %s of %s: min %g at %"FMT_ULG", max %g at %"FMT_ULG".\n",
               kEq+1, pVar[kEq].name, grpName[0], valMin, nMin, valMax, nMax ) ;

  }

  if ( grp_id ) H5Gclose(grp_id) ;

  arr_free ( dBuf ) ;

  /* Any solParam to read? */
  int isSol = 1 ;
  h5r_zone  ( file_id, pUns, isSol ) ;


  /* Terminate access to the file. */
  status = H5Fclose(file_id);



  /* Set default variable names. */
  if ( !h5r_flag_all )
    check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ;


  return ( 1 ) ;

}




/******************************************************************************

  read_hdf5:
  Read a mesh and possibliy solution file in hdf5.

  Last update:
  ------------
  3Apr19; init all grid sizes to zero, fix nobound bug.
  17Feb17; allow surface meshes with surf
  16Dec16; fix bug with rewinding closed Fabnd file when ignoring asciibound
  1Jul16; new interface to check_uns.
  17Dec15; detect v7 asciiBound files by looking for tag on first line,
           if detected, read bnd info from hdf instead.
  18Dec14; delegate treatment of zero filenames to prepend_path.
  12Dec13; read solParam from solution file.
  6Jul13; new interface to make_uns_grid.
  1Apr12; allow command line UNIX-style arguments.
  23May08; test for file existence.
  11Apr08; add renumber etc at the end.
  May07: conceived.

  Input:
  ------
  argline

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

int read_hdf5 ( char* argLine ) {

  FILE* Fabnd, *Ftest ;

  hid_t       file_id ;   /* identifier */
  herr_t      status ;

  chunk_struct *pChunk ;
  int mDim ;
  ulong_t mEl=0, mConn=0, mVx=0, mBndFc=0, mBndNde=0 ;
  int mBc=0 ;


  char gridNameArgs[LINE_LEN] ;
  gridNameArgs[0] = '\0' ;
  char gridFile[LINE_LEN] ;
  char abndFile[LINE_LEN] ;
  char solFile[LINE_LEN] ;
  char line[LINE_LEN] ;
  h5r_flag_reset () ;
  h5r_args ( argLine, gridNameArgs, gridFile, abndFile, solFile ) ;
  prepend_path ( gridFile ) ;
  prepend_path ( abndFile ) ;
  prepend_path ( solFile ) ;


  /* Test whether the file is openable. Do this outside of hdf, as
     hdf throws up ugly pages of error msgs. */
  if ( !( Ftest = r1_fopen ( gridFile, TEXT_LEN, "r" ) ) ) {
    sprintf ( hip_msg, "could not open grid file in read_hdf5.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else
    fclose ( Ftest ) ;

  if ( solFile[0] != '\0' ) {
    if ( !( Ftest = r1_fopen ( solFile, TEXT_LEN, "r" ) ) ) {
      sprintf ( hip_msg, "could not open solution file in read_hdf5.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else
      fclose ( Ftest ) ;
  }



  /* Open the hdf file. */
  file_id = H5Fopen( gridFile, H5F_ACC_RDONLY, H5P_DEFAULT ) ;



  /* Get sizes. */
  double llBox[MAX_DIM], urBox[MAX_DIM], llBoxCyl[MAX_DIM-1], urBoxCyl[MAX_DIM-1] ;
  double volGrid, volMin, hmin, hmax, *pBndPatchArea = NULL ;
  char *bcLabel = NULL, gridNameFile[LINE_LEN] ;
  size_t lbl_len ;
  int isPer ;
  specialTopo_e topo ;
  if ( !h5r_sizes ( file_id, &mDim, &mEl, &mConn, &mVx, &mBndFc, &mBndNde, &mBc,
                    gridNameFile,
                    &volGrid,&volMin,&hmin,&hmax,&bcLabel, &lbl_len, &pBndPatchArea,
                    llBox, urBox, llBoxCyl, urBoxCyl, &isPer, &topo ) ) {
    sprintf ( hip_msg, "could not read grid dimensions in read_hdf5.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  if ( h5r_flag_info ) {
    version_banner () ;
    list_grid_info ( mDim, mEl, mConn, mVx, mBndFc, mBc,
                     volGrid, volMin, hmin, hmax, bcLabel, lbl_len, pBndPatchArea,
                     llBox, urBox, llBoxCyl, urBoxCyl, isPer, topo ) ;
    /*Write json file*/
    list_grid_json ( gridFile, mDim, mEl, mConn, mVx, mBndFc, mBc,
                     volGrid, volMin, hmin, hmax, bcLabel, lbl_len, pBndPatchArea,
                     llBox, urBox, llBoxCyl, urBoxCyl, isPer ) ;
    /* Terminate access to the file. */
    status = H5Fclose(file_id) ;
    return ( 1 ) ;
  }
  arr_free ( bcLabel ) ;
  arr_free ( pBndPatchArea ) ;


  /* Alloc. */
  uns_s *pUns = NULL ;
  if ( !make_uns_grid ( &pUns, mDim, mEl, mConn, 0, mVx, 0, mBndFc, mBc ) ) {
    sprintf ( hip_msg, "failed to alloc for grid in read_hdf5.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  pUns->specialTopo = topo ;
  if ( gridNameArgs[0] != '\0' )
    strncpy ( pUns->pGrid->uns.name, gridNameArgs, LINE_LEN ) ;
  else if  ( gridNameFile[0] != '\0' )
    strncpy ( pUns->pGrid->uns.name, gridNameFile, LINE_LEN ) ;
  else
    snprintf ( pUns->pGrid->uns.name, LINE_LEN, "grid_%d", pUns->nr ) ;




  if ( abndFile[0] != '\0' ) {
    /* AsciiBound file given, use these patch labels. */
    if ( !( Fabnd = r1_fopen ( abndFile, TEXT_LEN, "r" ) ) ) {
      sprintf ( hip_msg, "could not open asciiBound file in read_hdf5.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    /* If the first line contains '$BOUNDARY-PATCHES', then it is v7, and
       read the hdf instead. */
    fscanf ( Fabnd, "%s", line ) ;
    if ( !strncmp ( line, "$BOUNDARY-PATCHES", sizeof( "$BOUNDARY-PATCHES" ) ) ) {
      /* This is v7, ignore. */
      abndFile[0] = '\0' ;
      fclose ( Fabnd ) ;
      if ( verbosity > 2 )
        hip_err ( info, 2, "ignoring v7 asciiBound file." ) ;
    }
    else
      rewind ( Fabnd ) ;
  }


  if ( abndFile[0] != '\0' ) {
    read_avbp_asciiBound_4p7 ( Fabnd, pUns ) ;
    if ( pUns->mBc != pUns->pRootChunk->mBndPatches ) {
      sprintf ( hip_msg, "expected %"FMT_ULG" bc from mesh file,"
                " found %d in asciibound.",
                pUns->pRootChunk->mBndPatches,  pUns->mBc ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    fclose ( Fabnd ) ;
  }
  else if ( topo != surf ) {
    /* extract bc-labels from the hdf file. */
    mBc = h5r_bcLabels ( file_id, pUns ) ;

    if ( !mBc ) {
      hip_err ( warning, 0, "no boundary labels found under hdf PatchLabels! Creating generic labels" ) ;
    }
  }


  /* Read. */
  pChunk = pUns->pRootChunk ;
  h5r_coor ( file_id, pUns, pChunk ) ;
  h5r_conn ( file_id, pUns, pChunk ) ;
  if ( topo != surf )
    h5r_bnd  ( file_id, pUns, pChunk ) ;

  int isSol = 0 ;
  h5r_zone  ( file_id, pUns, isSol ) ;

  /* Terminate access to the file. */
  status = H5Fclose(file_id);



  if ( topo != surf )
    make_uns_bndPatch ( pUns ) ;

  /* number_uns_grid ( pUns ) ; Done in check_uns. */

  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;




  /* Is there a solution to read? */
  if ( solFile[0] != '\0' ) {
    read_hdf5_sol ( pUns, solFile ) ;
  }


  return ( 1 ) ;
}

