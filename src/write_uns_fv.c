/*
  write_uns_fv.c:
  Write a 3D grid in fieldview format. If it is 2D, do an extrusion in z
  before.
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  Oct00; conceived.
  
  This file contains:
  -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

#include <strings.h>

extern const int verbosity ;
extern char hip_msg[] ;
extern const Grids_struct Grids ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const double R ;


/*
** Support functions for writing binary FIELDVIEW unstructured data files.
*/

/* Include the defines for the FV_* codes and wall_info values.
   #include "fv_reader_tags.h" */

/* Numeric tags (codes) for FIELDVIEW binary file format. */

#define FV_MAGIC	0x00010203	/* decimal 66051 */

#define FV_NODES        1001
#define FV_FACES        1002
#define FV_ELEMENTS     1003
#define FV_VARIABLES    1004

#define FV_TET_ELEM_ID          1
#define FV_HEX_ELEM_ID          2
#define FV_PRISM_ELEM_ID        3
#define FV_PYRA_ELEM_ID         4

/* Values for "wall_info" array (see comments in fv_encode_elem_header). */
#define A_WALL         (07u)
#define NOT_A_WALL     (0u)



/* Don't change these - used by fv_encode_elem_header ! */
#define MAX_NUM_ELEM_FACES     6
#define BITS_PER_WALL  3
#define ELEM_TYPE_BIT_SHIFT    (MAX_NUM_ELEM_FACES*BITS_PER_WALL)


/*
** fv_encode_elem_header:  return an encoded binary element header
**
** Input:
**    elem_type:  integer element type as shown in fv_reader_tags.h
**    wall_info:  array of integer "wall" flags, one for each face of
**                the element.  The wall flags are used during streamline
**                calculation.  Currently, the only meaningful values are
**                A_WALL and NOT_A_WALL as shown in fv_reader_tags.h.
**                Streamlines are forced away from faces marked as
**                "A_WALL", by limiting velocity and position very near
**                the wall.
** Output:
**    Function return value is the encoded binary element header.
*/

void fv_fail ( char *msg ) {
  printf ( "FATAL: %s.\n", msg ) ;
  exit ( EXIT_FAILURE ) ;
}


static unsigned int fv_encode_elem_header ( int elem_type, int wall_info[] ) {

  unsigned int header;
    int i, nfaces;

    switch (elem_type) {
        case FV_TET_ELEM_ID:
            header = (1 << ELEM_TYPE_BIT_SHIFT);
            nfaces = 4;
            break;
        case FV_HEX_ELEM_ID:
            header = (4 << ELEM_TYPE_BIT_SHIFT);
            nfaces = 6;
            break;
        case FV_PRISM_ELEM_ID:
            header = (3 << ELEM_TYPE_BIT_SHIFT);
            nfaces = 5;
            break;
        case FV_PYRA_ELEM_ID:
            header = (2 << ELEM_TYPE_BIT_SHIFT);
            nfaces = 5;
            break;
        default:
            fprintf(stderr, "ERROR:  Unknown element type\n");
            return 0;
    }

    for (i = 0; i < nfaces; i++) {
        unsigned int u = wall_info[i];
        if (u > A_WALL) {
            fprintf(stderr, "ERROR:  Bad wall value\n");
            return 0;
        }
        header |= (u << (i*BITS_PER_WALL));
    }
    return header;
}

const elemType_struct * fv_decode_elem_header ( unsigned int header, int *pmVx ) {

  unsigned int elemId ;
  const elemType_struct *pElT=NULL ;

  elemId = header >> ELEM_TYPE_BIT_SHIFT ;

  switch (elemId) {
  case 1:
    pElT = elemType + tet ;
    break;
  case 4:
    pElT = elemType + hex ;
    break;
  case 3:
    pElT = elemType + pri ;
    break;
  case 2:
    pElT = elemType + pyr ;
    break;
  default:
    printf ( " elemId: %d,", elemId ) ;
    fv_fail ( " unknown element type" ) ;
  }

  *pmVx = pElT->mVerts ;
  
  return ( pElT ) ;
}

/*
** fwrite_str80:  write out a string padded to 80 characters.
**
** Like fwrite, this returns the number of items written, which
** should be 80 if successful, and less than 80 if it failed.
*/
static ulong_t fwrite_str80 (char *str, FILE *fp) {
    char cbuf[80];
    ulong_t len;
    int i;

    /* Most of this just to avoid garbage after the name. */
    len = strlen(str);
    strncpy(cbuf, str, len < 80 ? len : 80);

    for (i = len; i < 80; i++)
        cbuf[i] = '\0';  /* pad with zeros */

    return fwrite(cbuf, sizeof(char), 80, fp);
}

ulong_t fread_str80 ( char *str, FILE *fp ) {
  return fread(str, sizeof(char), 80, fp);
}

int fv_test ( char* fileName ) {

  FILE *outfp;
  unsigned int elem_header;
  int i, mBc, nBc, mVx=0, mFc, nFc, mtet, mhex, mpri, mpyr, nEl, mEl, mVxEl,
    ibuf[10], reading, kVar, mEq ;
  char string[81] ;

  /* XYZ coordinates of the nodes. */
  float *x, *y, *z ;
  
  /* Open the file for binary write access.
  strcpy ( fileName, "field.uns" ) ; */
  if ((outfp = fopen( prepend_path(fileName), "r")) == NULL) {
    fv_fail ("Cannot open output file");
    exit( EXIT_FAILURE );
  }

  /* Output the magic number. */
  fread(ibuf, sizeof(int), 1, outfp);
  if ( ibuf[0] != FV_MAGIC )
    fv_fail ( " no magic" ) ;

  /* Output file header and version number. */
  fread_str80(string, outfp);
  if ( strncmp ( "FIELDVIEW", string, 80 ) )
    fv_fail ( " no FIELDVIEW" ) ;

  fread(ibuf, sizeof(int), 2, outfp);
  if ( ibuf[0] != 2 || ibuf[1] != 4 )
    fv_fail ( " not 2.4" ) ;

  

  /* Output constants for time, fsmach, re, and alpha. */
  { float consts[4] ;
    fread(consts, sizeof(float), 4, outfp);
    printf ( " consts: %g %g %g %g\n",
             consts[0], consts[1], consts[2], consts[3] ) ;
  }

  /* Output the number of grids. */
  fread(ibuf, sizeof(int), 1, outfp);
  ibuf[0] = 1;
  if ( ibuf[0] != 1 )
    fv_fail ( " not 1 grid" ) ;
  


    
  /* Output the table of boundary types. */
  fread(&mBc, sizeof(int), 1, outfp);
  printf ( " found %d mBc\n", mBc ) ; 

  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    fread_str80(string, outfp);
    printf ( " bc %d: %s\n", nBc, string ) ;
  }


    
  /* Output the table of variable names. */
  fread(ibuf, sizeof(int), 1, outfp);
  mEq = ibuf[0] ;
  printf ( " found %d vars\n", mEq ) ; 

  for (i = 0; i < mEq; i++) {
    fread_str80(string, outfp);
    printf ( " i %d: %s\n", i, string ) ;
  }


    
  /* Output grid data. */
  for ( reading = 1 ; reading ; ) {
    fread(ibuf, sizeof(int), 1, outfp);
    if ( feof( outfp ) )
      break ;

    if ( ibuf[0] == FV_NODES ) {
      /* Output the node definition section for this grid. */
      fread(ibuf+1, sizeof(int), 1, outfp);
      mVx = ibuf[1] ;
      printf ( " found %d nodes.\n", mVx ) ;

      /*
      ** Output the X, then Y, then Z node coordinates.
      ** Note that all of the X coordinates are output before any of
      ** the Y coordinates.
      */
      x = arr_malloc ( "fv.x", NULL, mVx, sizeof( float ) ) ;
      y = arr_malloc ( "fv.y", NULL, mVx, sizeof( float ) ) ;
      z = arr_malloc ( "fv.z", NULL, mVx, sizeof( float ) ) ;
    
      if ( fread(x, sizeof(float), mVx, outfp) == mVx &&
           fread(y, sizeof(float), mVx, outfp) == mVx &&
           fread(z, sizeof(float), mVx, outfp) == mVx )
        printf ( " found %d coors.\n", mVx ) ;
      else
        fv_fail ( " no coors" ) ;

      arr_free ( x ) ;
      arr_free ( y ) ;
      arr_free ( z ) ;
    }

    else if ( ibuf[0] == FV_FACES ) {
      /*
      ** Output boundary faces.
      ** All faces have 4 vertices.  If the face is triangular,
      ** the last vertex should be zero.
      */
      fread(ibuf+1, sizeof(int), 2, outfp);
      nBc = ibuf[1] ;
      mFc = ibuf[2] ;
      printf ( " found %d faces for bc %d,", mFc, nBc ) ;

      for ( nFc = 0 ; nFc < mFc ; nFc++ ) {
        fread(ibuf, sizeof(int), 4, outfp);
        if ( nFc == 0 || nFc == mFc-1 ) {
          printf ( "(%d,%d,%d,%d) ", ibuf[0], ibuf[1], ibuf[2], ibuf[3] ) ;
          if ( nFc == 0 )
            printf ( "- " ) ;
          else
            printf ( ".\n" ) ;
        }
      }
    }

    else if ( ibuf[0] == FV_ELEMENTS ) {
      int found[9] = {0,0,0,0,0,0,0,0,0} ;
      
      fread(ibuf+1, sizeof(int), 4, outfp);
      mtet = ibuf[1] ;
      mhex = ibuf[2] ;
      mpri = ibuf[3] ;
      mpyr = ibuf[4] ;
      mEl = mtet + mhex + mpri + mpyr ;

      for ( nEl = 0 ; nEl < mEl ; nEl++ ) {
        fread (&elem_header, sizeof(elem_header), 1, outfp);
  
        if ( !fv_decode_elem_header(elem_header, &mVxEl ) ) {
          printf ( " on el %d: ", nEl ) ;
          fv_fail ( " failed to decode element header" ) ;
        }
        found[mVxEl]++ ;
        fread( ibuf, sizeof(int), mVxEl, outfp);
      }

      if ( mtet != found[4] ||
           mhex != found[8] ||
           mpri != found[6] ||
           mpyr != found[5] ) {
        printf ( " expected %d tet, %d hex, %d mpri, %d mpyrs, found %d, %d, %d, %d\n",
                 mtet, mhex, mpri, mpyr, found[4], found[8], found[6], found[5] );
        fv_fail ( "" ) ;
      }
      else
        printf ( " found %d tet, %d hex, %d mpri, %d mpyrs\n",mtet, mhex, mpri, mpyr ) ;
    }

    else if ( ibuf[0] == FV_VARIABLES ) {

      x = arr_malloc ( "fv.x", NULL, mVx, sizeof( float ) ) ;

      for ( kVar = 0 ; kVar < mEq ; kVar++ )
        if ( fread ( x, sizeof(float), mVx, outfp ) != mVx ) {
          printf ( " on var %d: ", kVar ) ;
          fv_fail ( " failed to read variables" ) ;
        }
        else {
          int i ;
          float valMin = TOO_MUCH, valMax = -TOO_MUCH ;

          for ( i = 0 ; i < mVx ; i++ ) {
            valMin = MIN( valMin, x[i] ) ;
            valMax = MAX( valMax, x[i] ) ;
          }
          
          printf ( " found var %d: min: %f, max: %f\n", kVar, valMin, valMax ) ;
        }

      arr_free ( x ) ;
    }

    else {
      printf ( " header code: %d ", ibuf[0] ) ;
      fv_fail ( "unknown" ) ;
    }
    
  }

  fclose(outfp) ;
  
  return 1;
}

/******************************************************************************

  write_fieldview:
  write grid and solution to fieldview.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems.
          make all large counters ulong_t.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------
  fileName: to write to.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_fieldview ( char* fileName ) {

  FILE *outfp;
  int mEq;

  unsigned int elem_header;
  int i, ibuf[10], elemId ;

  /* XYZ coordinates of the nodes. */
  float *x, *y, *z ;

  /* Wall values for element faces. */
  static int no_walls[6] = { NOT_A_WALL, NOT_A_WALL, NOT_A_WALL,
                             NOT_A_WALL, NOT_A_WALL, NOT_A_WALL };

  const int *kVxFc ;
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  
  ulong_t nVx[MAX_VX_ELEM], mVx, mVerts, mFc, nr ;;
  int mBc, kVx, kVar, nBc ;
  ulong_t mEl ;
  uns_s *pUns ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  vrtx_struct *pVx, *pVxE, *pVxB, **ppVx ;
  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;


  pUns = Grids.PcurrentGrid->uns.pUns ;

  if ( verbosity > 0 )
    printf ( "   Writing finest grid to fieldview %s\n", fileName ) ;


  if ( !pUns->validGrid ) {
    printf ( " FATAL: you were told that this grid is invalid, weren't you?.\n" ) ;
    return ( 0 ) ; }

  
  if ( pUns->mDim == 2 ) {
    if ( verbosity > 2 )
      printf ( "   Copying 2D grid to 3D.\n" ) ;
    if ( !cp_uns2D_uns3D ( -1.e-3, 0., 1, 'z' ) ) {
      printf ( " FATAL: failed to copy to 3D.\n" ) ;
      return ( 0 ) ; }

    pUns = Grids.PcurrentGrid->uns.pUns ;
  }


  conv_uns_var ( pUns, prim ) ;
     
  
  if ( (check_bnd_setup ( pUns )).status != success ) {
    printf ( " FATAL: cannot write grid without proper boundary setup.\n" ) ;
    return ( 0 ) ; }


  
  
  /* Number the leaf elements only. Force a recount */
  pUns->numberedType = invNum ;
  number_uns_elem_leafs ( pUns ) ;
  count_uns_bndFaces ( pUns ) ;



  
  /* Open the file for binary write access. */
  if ((outfp = fopen( prepend_path(fileName), "wb")) == NULL) {
    perror ("Cannot open output file");
    exit( EXIT_FAILURE );
  }

  /* Output the magic number. */
  ibuf[0] = FV_MAGIC;
  fwrite(ibuf, sizeof(int), 1, outfp);

  /* Output file header and version number. */
  fwrite_str80("FIELDVIEW", outfp);
  /*
    ** This version of the FIELDVIEW unstructured file is "2.4".
    ** This is written as two integers.
    */
  ibuf[0] = 2;
  ibuf[1] = 4;
  fwrite(ibuf, sizeof(int), 2, outfp);

  /* Output constants for time, fsmach, re, and alpha. */
  { float consts[4] = { 1., 0., 0., 0. };
    fwrite(consts, sizeof(float), 4, outfp);
  }

  /* Output the number of grids. */
  ibuf[0] = 1;
  fwrite(ibuf, sizeof(int), 1, outfp);



    
  /* Output the table of boundary types.  */
  for ( nBc = mBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] )
      mBc++ ;
  fwrite(&mBc, sizeof(int), 1, outfp);

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] )
      fwrite_str80(pUns->ppBc[nBc]->text, outfp);


    
  /* Output the table of variable names. Note all is 3D here. */
  { int mEqF = 5, mSpec = 0, mReac = 0, mEqT = 0, mAdd = 0 ;
    char name[81],
      *var_names[6] = { "rho", "uvel; velocity", "vvel", "wvel", "p", "T" } ;
    
    mEq = pUns->varList.mUnknowns ;
    if ( pUns->restart.avbp.iniSrc ) {
      mReac = pUns->restart.avbp.nreac ; 
      mEqT  = pUns->restart.avbp.neqt ; 
      mAdd  = pUns->restart.avbp.nadd ; 
      mSpec = MAX( 0, mEq - mEqF - mReac - mEqT - mAdd ) ;
    }

  
    ibuf[0] = mEq|+1 ;
    fwrite(ibuf, sizeof(int), 1, outfp);

    /* First 5 primitive + T. */
    for (i = 0; i < 6 ; i++)
      fwrite_str80(var_names[i], outfp);

    /* Species. */
    for (i = 1 ; i <= mSpec ; i++) {
      sprintf ( name, "species %d", i ) ;
      fwrite_str80(name, outfp) ;
    }

    /* Reaction rates. */
    for (i = 1 ; i <= mReac ; i++) {
      sprintf ( name, "reac. rate %d", i ) ;
      fwrite_str80(name, outfp) ;
    }

    /* Turbulence. */
    for (i = 1 ; i <= mEqT ; i++) {
      sprintf ( name, "turb %d", i ) ;
      fwrite_str80(name, outfp) ;
    }

    /* Added. */
    for (i = 1 ; i <= mAdd ; i++) {
      sprintf ( name, "added %d", i ) ;
      fwrite_str80(name, outfp) ;
    }
  }

    
    

    
  /* Output grid data. */

  /* Output the node definition section for this grid. */
  ibuf[0] = FV_NODES;
  ibuf[1] = mVx = pUns->mVertsNumbered ;
  fwrite(ibuf, sizeof(int), 2, outfp);

  /*
  ** Output the X, then Y, then Z node coordinates.
  ** Note that all of the X coordinates are output before any of
  ** the Y coordinates.
  */
  x = arr_malloc ( "fv.x", pUns->pFam, mVx, sizeof( float ) ) ;
  y = arr_malloc ( "fv.y", pUns->pFam, mVx, sizeof( float ) ) ;
  z = arr_malloc ( "fv.z", pUns->pFam, mVx, sizeof( float ) ) ;

  pChunk = NULL ;
  int nB, nE ;
  while ( loop_verts ( pUns, &pChunk, &pVxB, &nB, &pVxE, &nE ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
      if ( ( nr = pVx->number ) ) {
        x[nr-1] = pVx->Pcoor[0] ;
        y[nr-1] = pVx->Pcoor[1] ;
        z[nr-1] = pVx->Pcoor[2] ;
      }
    
  fwrite(x, sizeof(float), mVx, outfp);
  fwrite(y, sizeof(float), mVx, outfp);
  fwrite(z, sizeof(float), mVx, outfp);

  /* Keep x for the variables. */
  arr_free ( y ) ;
  arr_free ( z ) ;

  
    
  /*
  ** Output boundary faces.
  ** All faces have 4 vertices.  If the face is triangular,
  ** the last vertex should be zero.
  */
  for ( nBc = mBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] ) {
      ibuf[0] = FV_FACES;
      ibuf[1] = ++mBc ; /* first boundary type */
      ibuf[2] = pUns->pmFaceBc[nBc] ; /* number of faces of this type */
      fwrite(ibuf, sizeof(int), 3, outfp);

      pBndPatch = NULL ;
      mFc = 0 ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pElem = pBndFc->Pelem ;
          if ( pElem && pElem->number && pBndFc->nFace ) {
            mFc++ ;
            pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
            mVerts = pFoE->mVertsFace ;
            kVxFc = pFoE->kVxFace ;
            ppVx = pElem->PPvrtx ;

            nVx[0] = ppVx[ kVxFc[0] ]->number ;
            nVx[1] = ppVx[ kVxFc[1] ]->number ;
            nVx[2] = ppVx[ kVxFc[2] ]->number ;
            nVx[3] = ( mVerts == 4 ? ppVx[ kVxFc[3] ]->number : 0 ) ;

            fwrite(nVx, sizeof(int), 4, outfp);
          }
        }

      if ( mFc != pUns->pmFaceBc[nBc] ) {
        sprintf ( hip_msg, "expected %"FMT_ULG", found %"FMT_ULG" boundary faces for %s.\n",
                  pUns->pmFaceBc[nBc], mFc, pUns->ppBc[nBc]->text ) ;
        hip_err( fatal, 0, hip_msg ) ;
      }
    }


  /*
    ** Start an elements section.
    ** There may be as many elements sections as needed.
    ** Each section may contain a single element type or a
    ** mixture of element types.
    ** For maximum efficiency, each section should contain
    ** a significant percentage of the elements in the grid.
    ** The most efficient case is a single section containing
    ** all elements in the grid.
    */
  ibuf[0] = FV_ELEMENTS;
  ibuf[1] = pUns->mElemsOfType[tet] ;  /* tet count */
  ibuf[2] = pUns->mElemsOfType[hex] ;  /* hex count */
  ibuf[3] = pUns->mElemsOfType[pri] ;  /* prism count */
  ibuf[4] = pUns->mElemsOfType[pyr] ;  /* pyramid count */
  fwrite(ibuf, sizeof(int), 5, outfp);

  mEl = 0 ;
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        mEl++ ;
        pElT = elemType + pElem->elType ;
        mVerts = pElT->mVerts ;
        ppVx = pElem->PPvrtx ;

        switch ( pElem->elType ) {
        case tet:
          elemId = FV_TET_ELEM_ID ;
          break ;
        case pyr:
          elemId = FV_PYRA_ELEM_ID ;
          break ;
        case pri:
          elemId = FV_PRISM_ELEM_ID ;
          break ;
        case hex:
          elemId = FV_HEX_ELEM_ID ;
          break ;
        default:
          elemId = 0 ;
        }
          
        if ( !( elem_header = fv_encode_elem_header(elemId, no_walls) ) ) {
          sprintf ( hip_msg, "fv_encode_elem_header failed for elem %"FMT_ULG".\n",
                    pElem->number);
          hip_err ( fatal, 0, hip_msg ) ; 
        }
        fwrite (&elem_header, sizeof(elem_header), 1, outfp);

        
        if ( pElem->elType == hex ) {
          /* fieldview has the same node numbering scheme, except for the hex. */
          nVx[0] = ppVx[0]->number ;
          nVx[1] = ppVx[1]->number ;
          nVx[2] = ppVx[3]->number ;
          nVx[3] = ppVx[2]->number ;
          
          nVx[4] = ppVx[4]->number ;
          nVx[5] = ppVx[5]->number ;
          nVx[6] = ppVx[7]->number ;
          nVx[7] = ppVx[6]->number ;
        }
        else
          for ( kVx = 0 ; kVx < mVerts ; kVx++ )
            nVx[kVx] = ppVx[kVx]->number ;

          
        fwrite( nVx, sizeof(int), mVerts, outfp);
      }

  if ( ibuf[1]+ibuf[2]+ibuf[3]+ibuf[4] != mEl ) {
    sprintf ( hip_msg, "expected %d elements, found %"FMT_ULG" in write_fieldview.\n",
             ibuf[1]+ibuf[2]+ibuf[3]+ibuf[4], mEl ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
          


  if ( pUns->varList.varType != noVar ) {
  
    /* Output the variables data. */
    ibuf[0] = FV_VARIABLES;
    fwrite(ibuf, sizeof(int), 1, outfp);
  
    /*
    ** Note that all of the data for the first variable is output
    ** before any of the data for the second variable.
    */

    /* 5 Primitve variables. */
    for ( kVar = 0 ; kVar < 5 ; kVar++ ) {
      float valMin = TOO_MUCH, valMax = -TOO_MUCH ;
    
      pChunk = NULL ;
      while ( loop_verts ( pUns, &pChunk, &pVxB, &nB, &pVxE, &nE ) )
        for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
          if ( ( nr = pVx->number ) ) {
            x[nr-1] = pVx->Punknown[kVar] ;

            valMin = MIN( valMin, x[nr-1] ) ;
            valMax = MAX( valMax, x[nr-1] ) ;
          }

      if ( verbosity > 3 )
        printf ( "     INFO: flow variable %d: min: %f, max: %f\n",
                 kVar, valMin, valMax ) ;
    
      fwrite ( x, sizeof(float), mVx, outfp ) ;
    }

    /* Temperature. */
    {
      float valMin = TOO_MUCH, valMax = -TOO_MUCH ;
      double *pUn ;
    
      pChunk = NULL ;
      while ( loop_verts ( pUns, &pChunk, &pVxB, &nB, &pVxE, &nE ) )
        for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
          if ( ( nr = pVx->number ) ) {
            pUn = pVx->Punknown ;
            x[nr-1] = pUn[4]/pUn[0]/R ;

            valMin = MIN( valMin, x[nr-1] ) ;
            valMax = MAX( valMax, x[nr-1] ) ;
          }

      if ( verbosity > 3 )
        printf ( "     INFO: variable T: min: %f, max: %f\n", valMin, valMax ) ;
    
      fwrite ( x, sizeof(float), mVx, outfp ) ;
    }

    /* Remaining. neqs,nreac,neqt,nadd. */
    for ( kVar = 6 ; kVar < mEq+1 ; kVar++ ) {
      float valMin = TOO_MUCH, valMax = -TOO_MUCH ;
    
      pChunk = NULL ;
      while ( loop_verts ( pUns, &pChunk, &pVxB, &nB, &pVxE, &nE ) )
        for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
          if ( ( nr = pVx->number ) ) {
            x[nr-1] = pVx->Punknown[kVar] ;

            valMin = MIN( valMin, x[nr-1] ) ;
            valMax = MAX( valMax, x[nr-1] ) ;
          }

      if ( verbosity > 3 )
        printf ( "     INFO: xtra variable %d: min: %f, max: %f\n",
                 kVar, valMin, valMax ) ;
    
      fwrite ( x, sizeof(float), mVx, outfp ) ;
    }
  }
  else
    printf ( "      INFO: no variables found with this grid.\n" ) ;
  
  
  fclose(outfp) ;


  /* test. */
  if ( verbosity > 5 )
    fv_test ( fileName ) ;

  
  return 1;
}

