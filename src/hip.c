/*
Copyright Jens-Dominik Mueller and CERFACS
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

Hip is a package for manipulating unstructured computational grids and their
associated datasets. https://inle.cerfacs.fr/projects/hip

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. The users must also reference the original authors on each
use/communicaiton/publication.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and, more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/*
  hip.c:
  Domino software's answer to all of the grid generation problems
  you never had. Note that hip is not square.

  Contains:
  ---------
  hip:
*/

#include <string.h>
#include <ctype.h>
/* Timing stuff. Do we really need types.h? */
#include <sys/types.h>
#include <sys/times.h>
#include <getopt.h>
#include <sys/resource.h>

/* Needed for licence check. */
#include <time.h>
/* Needed for wildcard matching in se bc-text. */
#include <fnmatch.h>

#include "cpre.h"

#include "proto.h"
#include "proto_mb.h"
/* hip shouldn't know about uns datastructures. However, some references are
   made in proto_uns.h that don't distinguish between public and private
   functions. Fix this. */
#include "cpre_uns.h"
#include "proto_uns.h"
#include "proto_mb_uns.h"
#include "version.h"


/* Global variables. */

const char lastUpdate[] = "11 Feb  2025, 12:04 CET" ;
/* Hip version , major, minor, revision */
const int hipversion[3] = {25,4,0};
/* Version name  */


const char hipname[] = "'tbc'" ;
// const char hipname[] = "'Hyacinthoides non-scripta'" ;
// const char hipname[] = "'Catalpa bignoides'" ;
// const char hipname[] = "'Dactylorhiza incarnata'" ; // Knabenkraut
// const char hipname[] = "'Primula veris'" ;
// const char hipname[] = "'Poa pratensis'" ;


// Version string now built in main from hipversion and hipname.
char version[LINE_LEN] ;
/*
const char hipname[] = {24,9,0} "'Nerium oleander'" ;
const char hipname[] = {24,5,0} "'Allium ursinum'" ;
const char hipname[] = {24,1,0} "'Eriocapitella japonica'" ;
const char hipname[] = {23,8,0} "'Dianthus carthusianorum'" ;
const char hipname[] = {22,6,0} "'Teucrium fruticans'" ;
const char hipname[] = {22,3,0} 'Viburnum tinus'" ;
const char hipname[] = {21,9,0} "'Iris pseudoacorus'" ;
const char hipname[] = {20,12,1}"'Aster amelius'" ;
const char hipname[] = {20,9,0}"'Argyranthemum frutescens'" ;
const char hipname[] = {20,04,5 "'Muscari Armeniacum'" ;
const char hipname[] = 19.12 "'Rhytidiadelphus squarrosus'" ;
const char hipname[] = 19.09 "'Ulmus glabra'" ;
const char hipname[] = 19.07 "'Solanum tuberosum'" ;
const char hipname[] = 19,04 "'Helleborus foetidus'" ;
const char hipname[] = 18,10;"'Convulvulus arvensis'" ;
const char hipname[] = 18,7;"'Ribes rubrum'" ;
const char hipname[] = 18.5 "'Bellis perennis'" ;
const char hipname[] = 18.3 "'Mangifera indica'" ;
const char version[] = "17.09.02, 'Lagerstroemia indica'" ;
const char version[] = "17.07.4, 'Buddleia Davidii Dark Knight'" ;
const char version[] = "17.03.1, 'Jasminum nudiflorum'" ;
const char version[] = "17.01, 'Carpinus betulus'" ;
const char version[] = "16.10, 1.51.0, 'Ficus carica'" ;
const char version[] = "1.50.0, 'Gunnera Manicata'" ;
const char version[] = "1.48.1, 'Eschscholzia californica'" ;
const char version[] = "1.47.2, , ''Schlumbergera Truncata''" ;
const char version[] = "1.46.2 ''Rudbeckia fulgida Goldsturm''" ;
never deployed: const char version[] = "1.45.0 ''Hemerocallis fulva''" ;
const char version[] = "1.44.0 ''Camelia japonica''" ; Apr 15
const char version[] = "1.43.4 ''Phalaenopsis amabilis''" ; Dec 14
const char version[] = "1.42  ''Tanacetum parthenium''" ;
const char version[] = "1.41.1 'Narcissus pseudonarcissus'" ; Apr 14
const char version[] = "1.40.0 'Cyclamen Hederifolium'" ;
const char version[] = "1.39.0 'Rosa Dublin Bay'" ; Jul 13
const char version[] = "1.38.0 'Galanthus nivalis'" ; Apr 13
const char version[] = "1.37.0 'Rhodochiton atrosanguineum'" ; Dec 12
const char version[] = "1.36.0 'Achillea filipendulina'" ;
const char version[] = "1.35.0 'Ichtyosaurus'" ;
const char version[] = "1.34.0 'Pollywog'" ;
const char version[] = "1.33.2 'Glis glis'" ;
const char version[] = "1.32.4 'Daphnia'" ; Sep 2011
const char version[] = "1.31.4 'Magnolia'" ;
const char version[] = "1.30.2 'Nanuuq'" ;
const char version[] = "1.29.4 'Meriones unguiculatus'" ;
const char version[] = "1.28.3 'Big Foot'" ;
const char version[] = "1.28.2 'Yeti'" ;
const char version[] = "1.27.2 'Ida'" ;
const char version[] = "1.25.0 'Dromaius novaehollandia'" ;
const char version[] = "1.24.0 'Ursus ursus'" ;
const char version[] = "1.23.3 'Kaa'" ;
const char version[] = "1.22.0 'Tortoise'" ;
const char version[] = "1.21.0 'Feline'" ;
const char version[] = "1.20.1 'Ameise'" ;
const char version[] = "1.19   'Aardvark'" ;
const char version[] = "1.18.1 'Humpback'" ;
const char version[] = "1.17.2 'Bat'" ;
const char version[] = "1.16.5 'Hirondelle'" ;
const char version[] = "1.15.6 'Blackbird'" ;
const char version[] = "1.15.5 'Igel'" ;
const char version[] = "1.15.4 'Viper'" ;
const char version[] = "1.15.3 'Cheetah'" ;
*/


/* June/July: Penstemon 'Pensham Plum Jerkum' */

/*
  Last update:
  ------------
  25.4:
        11 Feb 25:
        - enable expressions to spec bcs in mark menu
        - fix bug with applying q2t to second mesh, reset invalid face flag
        Dec 24:
        - add howto in markup with examples
  24.9: Aug 24
### included in sec.changes.tex from here:
        - introduce write stl
        - expand specialTopo syntax to apply to specific grids, not
          just the current one. Intro nxtSpecialTopo to set topo
          before reading for correct check and bb calculation.
  24.05: May 24
  10/6/24 24.05.1: support reading unstructured cgns when boundaries are given as point lists.
        - fix bug in mmg_put_mesh_3d that omitted listing pyramids.
        - support NULLTERM and NULLPAD strings in hdf read.
  24/1, Jan 24
        - fix final bugs in adapt perio working with flags 
  23/9: Sep 23
        - remove loop to reset bc order in mmg_2hip.
  23/8: Aug 23
        - Switch to mmg feature/split-nonrid-edg-connecting-rid-pts to split all edges bnd faces when using mmg.
  23/1: Dec 23:
        - upgrade to mmg 5.7.1
        - fix warnings for gcc 11.3 compiler about array size mismatch
  22/3: Jun22: `Viburnum tinus'
        - augment syntax for set gridname, allow single arg to apply to current grid.
        May22:
        - restore compilation with Lapack, instead of Clapack
        - introduce check quality
        - read Parameter section in hdf sol only if it exists.
        - restore prepend-path in write_hdf_sol.
        - exclude reading nnode from hdf solution read.
        Mar22:
        - support gmsh 4.x, read any labels given by gmsh, create zones for volume labels.
        Dec 21:
        - fix bug with hdf write segfault when joined meshes have interfaces.
        - start work on mmg_adapt using element marks rather than zones
  21.9: Sep 21: Iris pseudoacorus
        - revise edge length calculation for mmg_adapt_per, fix bug to
      preserve grid refinement level.
        - add var init var_name_expr value option to change var values.
        -  vis elems -p: write all elements above/below some property threshold
          to a vtk file.
        Aug 21
        - treat mismatches of sliding planes gracefully by snapping to the
          extremal line in sp_calc_vx_weight_mixing_lines
        - write interpolation weights for nodes on mixing planes.
        Jul 21
         - add list of mixing plane nodes and interpolation weights for mixing plane.
         - fix bug with lidx for periodic faces.
         - write periodic faces to hdf.
  20.12.1: Jan 21
         - missing initialisation of variables if Parameters group is missing
         - split and correction of set_bc_mark based on set_bc_type
         - correct scope of pVar in adapt_mmg to avoid overwritten variable
  20.12: Oct/Nov/Dec 20;
         - introduce copy uns mCopies per syntax.
         - introduce support for 'boxes' for nodal flagging in cylindrical coordinates.
         - set nFace to 0 for invalid bndFc identified as internal if doRemove=1.
         - support tranlational copy uns, tidy up and fix bugs in
           claculation of matching bcs that become interior
         - switch asciibound output default to v7.X for write_hdf5
         - remove periodicity for decimate_mmgs_3d.
         - fix bug with uninitialised mDim in adapt_mmg_3D_per
         - fix bug with double increment of pVar index in fixing periodic
           solution in uns_per
         - generate fatal if transfer/rotation operation for uns_copy is
           not correctly declared.
  20.09: Sep 20; (not released)
         - fix bug with missing update of pointer after realloc in uns_slidingplane.
         Aug 20
         - fix bug with upper limit for number of bc when using expr
         - enable -n arg for cgns read
         July 20
         - change prism ordering in xmf mapping for positive volume
         - hybrid grid xmf mapping: write two entries for each patch
           with qua and tri faces.
         - fix typo in hdf write with qua_fidx.
         - run special_verts et al for bndAndInter, not just bnd patches.
         - fix c&p bug in allocation of pBcShift in h5w_per.
         - add (re-enable) tri/quad faces for hybrid grids.
         - fix bug with interpolating adapted grids
         - support writing hierachically refined grids to hdf.
  20.07, 20:04: April/May 20
         - fix various bugs with in-situ collapse: mg volmin.
         - check whether face to element pointer points to valid element in hdf bnd face
           read.
         - fix bug with missed internal faces when recreating bnd faces from bnd nodes
           during hdf read.
         - read general connectivity patches as bc patches for mb cgns.
         - fix bug with late calc of epsOverlap in read_mg_cgns.
         - fix bug with testing return code of read_mb_cgns.
         - 16Jun19; fix bug with adding Quad + Tet face bc numbers for hybrid grids.
         - support translation in periodic grids written with hdf.
         - fix sp_geo_type_string to match spGeoType_e, fixes hdf write bug.
         - add bc patch split functionality.
         - introduce se hy to define hypervolume/plane for splitting
         - add syntax variant to replace 'se bc-command' with 'bc command'
         - correct help menu for ifc mixing.
         - fixed bug with ADAPT_HIERARCHIC in uns_2tet.
         - in hdf formats: for every _lidx write out an alternative _fidx.
         - add support for reading grids with only boundary nodes given.
         - add  pnVxPerBc_lidx to periodicity section of hdf uns write.
         - fix bugs arising from buffering neg vol elems during adaptation
         - fix bug with resetting epsOverlap after hier. adapt.
         - rewrite reading bnd faces in CGNS to support Salome CGNS files,
         - use field of mark[SZ_ELEM_MARK] for all element marks,
           replace pElem->cut/boxMark with mark 0/SZ.
         - ensure periodic solution at start of adapt_mmg_3d_per
         - fix bug in per_solution: omission of indexing, only first
           variable was made periodic.
         - fix segfault with missing pGrid in init_uns.
         - remove use of h5w_one_fxStr, fails to read back in.
           Use h5w_fxStr with dim=1 instead.
         - support for multiple sliding/mixing planes,
         - support for sliding/mixing planes for all axial planes xyz.
         - support for addressing bcs and grids by name.
  19:12: March 20
         - add forgotten prepend_path in read_uns_cgns.
         Feb 20:
         - fix bug with duplicate allocation of pBndFc in   h5r_bnd_fc2el,
           correct reading of number of bndFc in  h5r_sizes  when only
           bndFc->node is given.
         - fix bug in full face conn check that was not executed, amend interface to make_llfc.
         - complete argline read for unstructured cgns.
         Dec 19
         - read solutions for unstructured cgns
         - recognise boundaries as internal after attaching mesh.
         - remove path from file names inside xmf.
`        - carry zone information to mmg-adapted grid.
         - removed support for deprecated structured formats: dlr flower, aerospatiale as.
         - fix issue with zero pointers from boundary face to element in merge.
         - repace hip's quad tree with kdtree in uns_per to improver performance.
  19.09.2 ; Dec 19; intro sliding/mixing plane functionality.
  19.09.1; Oct 19
         - Support for pyhip and automatic docker build
    19.09; Oct 19
         - fix bug with buf 2 ref upgrade under neg vols.
         - support mixing plane line averaging, part implementation
         - only warning on i32 elGraph for long int hdf writes, no abort.
         - fix bug in makeElGraph, make work for ulong_t.
         Sep 19
         - intro buf2ref: under hierarchic refinement, promote cells
           to iso refinement whose buffering would lead to negative vols.
         - fix bugs with periodic adaptation
         - change default of ad-per back to 1
         - restructure main and hip_err to make py callable.
  19.07: Aug 19
         - support Point/ElementRange for uns cgns.
         - fix bug with writing uns cgns.
         Jul 19
         - read/write of interfaces as any other boundary in hdf,
           new parameter Boundary/PatchGeoType
         - intro interface read from Centaur
         - new, but bkwd-compat, interface for mmg adapt: isoXXX keyword obsolete.
         - fix bugs with metric calculation for mmg adapt periodic.
         - fix bug with layer propagation during mmg adapt on periodic grids.
         - correct bug with array index for variable number for mmg metric.
         - write PatchLabels also to group Patch, if bnd conn is written.
         - rename interDuplicate to duplicateInter.
         - rename to ADAPT_HIERARCHIC
  19.04: May-jun 19
         - revive hierarchical adaptation.
         - re-establish adapt file
         Apr 19
         - mmg adaptation of periodic grids working.
         - allow to address variables in mmg by name/matching expr.
         - fix bug with non-initialised values for grids without boundaries.
         - fix bug with periodic/non-per switch for 2D.
         - allow fixing bcs during adaptation using bc mark.
         - correct doc on mmg isovar: scaling compared to orginal, not new length.
         - fix issue with numerical accuracy for recognition of 180 deg periodicity.
         - fix issues with reading/writing surface grids without boundaries.
  18.09: Sep 18
         - correctly abort in mmg menu if there is no grid.
  18.07: July 18
         - fix pb with negative periodic angle and negative rotation copy angle.
         - fix segfault with mmg -s
         - fix problem with translate operations on periodic grids.
  18.05: Apr/May 18
         - fix bug with reading 2D gmsh
         - if fluent zone is missing, add it.
         - intro blockNo arg to read mbcgns
         - read internal boundaries in fluent format.
         - use long ints in read_fluent to support very large meshes.
         - fix bug with reading edges in 3D meshes given in gmsh formats.
         - add support for reading hdf without face to element pointers.
         - write surface patch area with li su area
         - intro check per to check periodic vertex matching without writing.
         - write periodic angle to hdf.
         - produce a new bc of unmatched faces if doWarn.listUnMatched != 0.
  18.03: Mar18:
       - force conversion to cons when writing hdf, unless -p stated.
       - intro doWarn.abort, doFc.listUnMatchedFc.
       - write mb to uns vx map to hdf.
       - introduce reading of multi-block CGNS grids.
  18.02,Feb18:
       - initialise default hMin/epsOverlap when reading plot3d.
       - reinstate reading plot3d mesh and ibc topology files.
       - add blanking arg to read_mg_cstruct/plot3d.
  18.01,Jan18:
      - fix bug with distance computation in point-dist-tri.
       - fix bugs with brace edit error in multigrid collapsing
       - fix bugs with computing ridges/corners in multigrid collapsing
       - change the way collapse limit is computed at boundaries in vx_properties.
       - fix bug with writing mg coefficients to gmsh
        Dec 2017
       - 15Dec17; upgrade PCLINUX makefile.h to use repo-build of mmg
  17.9.2, Nov 2017
       - 11Nov17; fix math error with distance calculation to triangular faces.
       - 11Nov17; intro set_current_grid, update prompt after generate.
       - 10Nov17; change logic in find_el_tree_walk for elements 'just' outside.
       - 10Nov17; edit comments about intPolRim, etc, for clarity
  17.9.1, Oct 2017
       - allow to read several gmsh grids who have the same bcs.
  17.9 Sep 2017
       - write out min norm interpolation coefficients for a mg sequence
          in gmsh format.
       - include new routines for future support of mmg mesh adaptation
          of periodic cases.
       - no longer impose positive periodicity angles when reading files.
       - enable to have periodicity and copy angles with different signs.
  17.7.3 Aug 2017
        - execute writing internal face group to hdf only if internal faces
          are present.
  17.7.2 July 2017
        - store mg coeff for hip coarsening for writing to hdf
        - compute mg coeff for independently generated sequences.
        - ship out multigrid coefficients with minnorm.
  17.7; June 2017:
        - major refactoring of h5w_bnd.
        - order boundary patches in the list by type, with bnd patches before interfaces, etc.
          write hdf surface connectivity ('Patch') only for boundary patches.
        - clarify online help for copy and attach commands regarding treatment of
          duplicated boundary faces.
        - count doubly matched faces in grid merging even if warning and removal flags are off.
        - write boundary info to hdf only if active boundaries (internal or external)
          are found.
  17.6; May 2017:
        - add support for interface boundaries.
  17.3; Feb 2017:
        - allow users to mark bc which then are treated as fixed by mmg3d.
        - introduce hip -v and banner with version string for read hdf -i
        - fix problem with mmg adapting hybrid meshes with periodicity.
        - fix bugs with writing multi-element meshes to CGNS
        - fix bugs with mixup of solution and mesh parameters.
        - intro reco-flag to "interpolate" flags picking the nearest value if non-zero.
        - switch mg collapse max angle test to convexity test based on positive subvolumes.
  17.1; Dec 2016:
        - introduce writing of cell-vertex style nodal volumes to hdf.
        - reset tolerance for periodic translation threshold ot 1e-2
        - compute perimeter edges and write surface grid to ensight with or without faces.
        - complete implementation of decimation,
        - fix bug with vertex to element list, which corrupted walk-on-mesh searching
        - fix bug with setting hMin/hMax with mesh decimation. Adding Hausdorff parameter
        - fix bug in bnd face counting when reading hydra files.
        - fix bug with rewinding closed Fabnd file when ignoring asciibound
        - fix bug with applying variable metric to mmg hybrid
        - list grid info for hdf with -i flag,
        - clean up calculation of surface areas.
  16.10: Oct 2016:
        - Cecill B license.
        - Switch to year.month numbering of version.
        - fix bug with maxAngle calculation for multiply collapsed elems.

  1.51: Sep 2016:
        - introduce support for mmg on hybrid grids.
        - fix bug with projection of min-norm solution on sub-manifolds
        - fix bug with end-condition of walk of cells
  1.50: Jun 2016:
        - intro decimate_mmg to write decimated meshes for GUIs.
        - support writing of ascii Ensight files.
  1.49: Apr 2016;
        - allow the specific value of hGrad=-1.0 as a flag.
        - fix reading character parameters.
        - ensure boundary normals are periodic.
        - write patchlabels in fortran style with trailing blanks.
  1.48; Mar 2016;
        - fix bugs with mg angle measurements.
        - introduce partial read of hydras hdf.
        - introduce swapping elements for ensight, intro negVol parameters.
        - introduce support for mmg remeshing.
        - fix problem with reading ensight files with boundary faces without ids.
        - improve error message text when interpolating 2-D to 3-D.
  1.47; Dec 2015-Feb 2016
        - fix problem with singular minnorm matrix in planar 3D cases.
        - check also for edge angles in a face "in-face angles" when collapsing.
  1.47; Feb 2016
        - support removal of periodic setup.
        Dec 2015
        - switch the interpolation algorithm default to minimum norm.
        - modify search logic during interpolation: only look for
          a good element to interpolate inside the domain interpolation rim.
        - set the default value of int-fc-tol to 9999, hence practically
          preventing any global search unless this value is reduced.
        - detect v7 asciiBound files by looking for tag on first line,
          if detected, read bnd info from hdf instead.
        - abort if no boundary faces are found.
        - fix bug with reading multi-element ensight.
        - fix bug with periodic edges and multi-grid coarsening.
        - GS fixes to read ensight
  1.46; Sep 15;
        - introduce intFcTol set by in-fc to control when a vertex is
        considered outside an element for interpolation.
        - only write axis verts when periodic boundaries are present.
        - Add clapack which replaces lapack to allow pure cc compilation
        for enhanced portability.
        - Translate the per_label lu into the internally used 00 when
        periodic patches are only declared as l, u.
        - Fix bug with periodic difference diagnostics in uns_per.
        base rotation angle computation on the angles in the plane
        normal to the rotational axis to help with skewed periodic
        planes.
  1.45; Jul 15:
        introduce element-based interpolation using the minimum norm solution.
        modify asciibound.key file format: print rotAngle with trailing d0.
        read ensight case files with arbitrary whitespace.
        support merging of meshes after copy without requiring periodicity.
        fix bug with checking periodic setup before the Grid is declared.
        increase MAX_BC in Fluent to 512.
  1.44: Apr 15:
          fix bug with setting periodic bcs read from hdf in h5r_bcLabels,
          fix bug with failing to reset volDomain in check_elems.
          fix bug with bad variable indices in per_solution
             (fixing up solution to be periodic).
          extend ensight to read 2-D grids.
          extend ensight to read solutions.
  1.43.1
  11Jan15; first working version of para_refine, entailed some changes
           /bug fixes to shared hip routines.
    6Jan15; rename pUnsInit to more descriptive pArrFamUnsInit
  Dec14; 1.43.0
         replace blanks in bc names with an underscore.
         add a record to each zone of all nodes forming any of the cells
           in that zone.
         fix bug with not correctly computing volumes, hMin, etc,
           when using check-level = 0.
         for merging operations retain user-defined epsOverlap if smaller.
         fix bug with manipulating Centaur meshes that have internal
           boundaries.
         allow to omit asciiBound file for hdf, read PatchLabels
          and Periodicity from hdf5 instead.
         Switch to hdf-1.8.14, which fixes memory bug when -DHIP_USE_ULONG
           is not defined.
         make HH_ULG dependent on the setting of USE_ULONG, correctly
           use HH_NATIVE_UINT for 32bit unsigned int compilation.
         replace checklic with library (GS)

  Sep14; 1.42.2
         use ulong_t and unsigned ints in read_centaur and read_hdf
           to read large meshes.
  Aug14; 1.42.1
          allow domain zones not numbered '2' for fluent.
  June14; 1.42.0
          introduce -z flag ro read or not read zones with hdf5
          introduce h5r/w_flag_reset to allow multiple reads or writes
            with non-default flags
          rework next_vec_var to fix bug with no vector variables in
            averaged solutions.
          allow both zone list and list zone syntax variants.
          introduce -a0, -b0 arguments to suppress writing all, bnd.
          introduce -z arguments to write or not write zones.
          introduce writing of coarse levels to hdf, using the -l option.
          fix bug h5w_grid with wrong numbering after -b: renumber.
          write vertex to element connectivity in hdf with the -v flag
  Apr 14, 1.41.0
          introduce n3s format with variable size/transposed TAB field
          reinterpret how variable names are read
          fix issues with strncpy when calling write_hdf
  Feb 14  1.40.1
          Update read_n3s to format with transposed TAB with variable size,
          fix pb with variable indices differeing between doc and examples
          rewrite variable names with _# for vars with mult. component.
  23Mar14; allow check_var-name to read sol fields without flow variables,
           check in writing to avbp and hdf for flow fields.
           read_gmsh: recognise variable names.
           replace all blanks in var names with underscores.
  24Feb14; write_ensight: remove stray if (0) around vector variable treatment.
  23Feb14; read gmsh solution in mesh file.
  Dec 13; 1.40
         Remove call to check_var_name in write_hdf, this should only be
           done after reading.
         Added support for XMF of solutions when using all var flag ( -a ) (GS)
         Add supoort for compressing only certain hdf files, but not all.
         Rotate zonal solution parameter vectors, same as zonal mesh params.
         Remove annCascade as a generic rotational topology, use axiX/Y/Z
           instead. This fixes a bug with where nodes on the x-axis are
           taken as special vertices, while the annular axis is z.
         Make writing to hdf recognise the flags for solution fields, just
           as done for ensight before.
         Make the -a flag default for write_hdf, All processed solution
           fields are written. This makes the -a flag obsolete for writing.
         Recognise variable flags set with var flag, only list flagged
           variable.
         List boundary names in hdf record.
         For hdf, write a modified asciibound.key file format as well.
         Introduce variable zonal parameters written with the solution file.
         Fix bug in mb_ele_vert, calling init_uns instead of init_elem.
           This fixes a bug with reading any structured grid, a feature
           that seems to have been unused since that part of the code was
           brokenin July 2011.
         Fix bug in read_avbp with wrong loop scope when converting
           node integers to pointers, introduced with switch from printf
           to sprintf. This must have broken any read_avbp since Jan 2012.
  Oct13; 1.39.6
         xdmf output for hdf mesh file. GS.
  Oct13; 1.39.5
         Fix final problems with reading 2-D centaur introduced by support for zones.
         introduce check for contiguous vector variables.
         use vector flag in per_solution, fix bug with odd behaviour without flow vars.
  Sep13; 1.30.4
         a number of GS fixes to read centaur 2D, broken by 1.39.0
  Jul13; 1.39
         fix bug in h52_per with listing bc->nr, rather than the order of writing.
         fix pb with poor formatting of Centaur without zones.
         re-allow tuples of bc, bc-or with expressions
         allow addressing zones by name as well as number.
         allow matching of all bc-action with text and wildcards.
         include bounding box in the hdf grid file header.
         fix problems with writing files to hdf when using ULONG
         print zone numbers 3 digits wide with leading zeros for hdf groups.
         read zones from Centaur files
         disregard internal interface panels declared in Centaur
         fix bug #563 with bc-compress.
         fix bug with interpolation failing
         initialise potentially non-initialised variables in non-executed paths.

  Apr13; 1.38
         promote all large counters/numbers (nodes, elements, faces ) to
           ulong_t, turned on with using -DHIP_USE_ULONG
         bug fix with reversely traversed loop using unsigned int counters. (GS)
         switch default for "WALL" bc to v, so as not to collide with
            AVBP's use of 'w' for forced corrners (mpVx).
         various bug-fixes resulting from non-initialised variables.
         move many more diagnostic prints to hip_err calls (GS)

  Dec12; 1.37
         check for existence of periodic setup if copy to 360deg.
         compute domain volume and write to hdf
         write surface connectivity to hdf by patch.

  Sep12; 1.36:
         fix bug with segfault on adding zones.
         set default for pe_thresh to 1.e-5
         correct help menu for zone.
         bug #466: fix bug with least-squares interpolation
         bug #464: remove epsOverlap from thresholdtest in rotational peridicity
         bug #463: fix bug leading to segfault on reading meshes with coalescing bc
         bug #468: turn off any checking of flow variables for hdf read/write -all.
         bug #457: increase fixed bc number limit in Fluent read to 200,
         bug #457: avoid segfault in logic when missing Fluent faces,
         bug #465: update online help/doc for in-rim,
         bug #465: fix bug with calculating face distance in point_dist_face that
           led to a miscalculation of the interpolation rim.
  July12; 1.35.0 fix bug with initialisation of bndFc in append_bndFc,
                 leading to meshes with mismatched faces.
           Flag added w-component as 3rd vector component.
           Fix ensight write bug with node listing for parts when node id is on.
           use provided node ids in ensight read.
           fix bug with walking search leading to 0 faced elements.
           revise behaviour of partial interpolation, see table in doc.
  Apr12; 1.34.0: introduce command line arguments for hdf
           add wrinting internal /Faces to hdf
           rework CGNS read interface to work for mixed element sections.
  23Mar12; fix bug with reading single precision CGNS coordinates.
           extend understanding of boundary descriptions possible in CGNS.
  14Mar12; doWarn and DoRemove now initialised in init.
           revise doWarn and doRemove
           remove interior boundary faces of thin walls upon doRemove
  9Feb12; 1.33.2; read ensight gold files.
  20 Dec11, 1.33.1; fix bug with mg connecdtivy, don't add collapsed faces in uns_llFc.
  Dec11; 1.33: fix bug with reading hexes and connectivity size in gmsh.
               fix bug with writing hybrid meshes in hdf, avbp4.
               introduce simple operations to set nodal flags, to then set zones.
               modify zone interface, require explict element adding step.
               change default value of interpolation rim to infinity,
                 i.e. extrapolation everywhere.
  Sep11; 1.32.4: init pUns->mZones to zero.
         1.32.3; fix bug with xone counter under copy
                 complete support for writing cgns, beta though.
  Aug11; 1.32.2; fix bug with connectivity counting in write_ensight.
                 complete reading of unstructured cgns meshes.
                 #define wrapper for CGNS.
                 fix bug with periodic translation computation.
                 remove duplicate prepend_path in write_ensight.
                 allow translation in other directions than x,y,z only.
                 allow *? wildcards in num ranges in addition to -, but ony 1 per group.
                 introduce vector flag to variables.
                 treat vel and tpf vectors in hdf/ensight.
  Jul11; 1.32; include limits.h for INT_MAX, remove MAX_INT.
               carry periodic tag from hip_per_inlet into lu types.
  5Jul11;      fix bugs with automatic recognition of periodicity.
  22Jun11;     replace libdo's tree with kdtree in uns_interpolate.
  3Jun11;      consistently define sizes for cpt_s under IPTR64.
  6Apr11; 1.31; rework computation of periodicity operation.
               intro pe_trans_rot_threshold.
               GS fix for parameter numbers in write hdfa.
               switch default interpolation to reco_1
               read fluent bc tags and use as bcText.
               intro hdf5b to write only boundary information.
               remove support for cfdrc-mfg.
               warnfor box size mismatch in grid interpolation.
               fix bug in mg related to bad pointer reference in debug-only part
               use normalised directions in elem_contains_co to allow for oddly
                 scaled meshes.
               fix bug with [re]allocation in reallocate_unknowns.
  Dec10; 1.30; fix bug with filename char length in read avbp4, centaur and cfdrc.
               allow copy_uns with multiple periodic pairs
               introduce lu with tags
               bug fix when reading avh.
               when interpolating in the void, keep existing, matching unknowns.
               fix bugs with reading gmsh tets,
               add mg_conn to write_gmsh.
  8Nov10; 1.29.4; fix bug with prims node ordering in gmsh.
                  change tag format in write_gmsh.
  7Nov10, 1.29.3; drop hip_bnde_mp, use bc-type = 'w' instead.
                  fix bug in match_llFc, affecting read centaur
  24Oct10, 1.29.2; incorporate GS bug fixes in read_hdf,
                   fix bug with connectivity calculation in copy_uns.
  22Oct10, 1.29.1: complete coding of copy 360.
  16Oct10; intro set bc-mark
  Sep10; intro cp_uns2uns.
  Aug10; specify flow var names with ini.
  Jul10; 1.29.0; read all elements of the Parameters group under hdfa.
          add min/max h/Vol to mesh file header under hdf5.
          handle most (hopefully all relevant ones) errors with hip_err.
          up standard character field length to 1024.
          be more generous in recognising bc types. WALL or wall leads to type w now.
          base the test for inclusion of bcs in the multi-patch list
            on the hip_bnde_mp tag in the bc-text, rather than the bc-type = 'w'.
  23Jun10; 1.28.3; fix bug with bc no for internal elements.
  April10: 1.28.3; GS: read header to distinguish beteween format variants,
                   GS: read new format
  22Mar10; 1.28.3; fix bug with reading grid-only gmsh.
  20Mar10; 1.28.2; workaround for multi-species n3s variable conversion.
  Feb10; 1.28.1; intro gmsh read/write.
  Dec09; 1.28; finalise large memory support
  Sep09; 1.27.1; finalise write_ensight.
                 intro var flags.
                 document write hdfa in help.
                 write hMin, volElemMin to hdf grid file.
                 write char strings, not char arrays to hdf.
                 fix bug with reading hdf solution files without GaseousPhase.
  May09; 1.26.0; test for existence of hdf files in read_hdf.
                 fix bug in writing tpf variables to hdf.
                 fix bug with reading/writing hdfa.
                 fix bug with copying up solution fields to make space for w.
                 use space as separator between command line arguments in help.c.
                 new feature: n3sa reads all of an n3s solution file.
                 new feature: write_ensight.

  May06; 1.25.1; added check_lic for licence.
  Apr09; 1.25; new doc.
               swap periodic patches to keep a positive rotation angle in r.h. rule.
               minor changes in what params are written in read/write_hdf (GS).
               fix bug with double incrementation with make_grid.
               allow reading hdf solutions with arbitrary group names.
               set successful return from main to EXIT_SUCCESS,
               fix bug with 360deg copy3d.
               correctly treat name and type of the variables in copy3d.
               all exit() return either EXIT_SUCCESS or EXIT_FAILURE.
               bug fix with Fluent binary read.
  Dec08; 1.24; add a workaround in read_avbp_inBound to accept files with
                 bc numbers ranging 0 to mPerBc-1.
               set topo to annCasc if a rotation is found.
               intro MINITXT_LEN to cpre.h.
               list grid topo with 'li gr', remove topo from general listing 'se'.
               change the split of an int (32bit) in cpt_s to 4/28, i.e.
                 a max of 16 chunks, but 268M nodes.
               fix pb with x-axis-verts in bnode->node and per-vx.
               return nothing (not even exit value =1 ) on exit.
  May08; 1.23.2; fix bug with fidx2lidx, this fixes writing mp_bndVx to hdf
                 fix bug with writing elGraph to hdf.
  Apr08; 1.23; fix bugs with reading hdf5 files.
               fix bugs with reading n3s solution files.
               report correct variable type for paraT.
               translate Fluent binary files from big-endian to the appropriate arch.
               switch to new API of hdf5 1.8.0.
               add comment on set bc-order and interpolation to doc.
               enable reread old AVBP3 (hip version <= 1.14.0 ) inBound files.
               fix bug with writing hybrid meshes to hdf.
               intro option hdf5s to write solution only.
               use cp and gamma of multi-spec solution for conv2cons when reading n3s.
               supply fluent variable names.
               check for file existence using fopen in read_hdf_sol.
  Dec07; 1.22; fix bug with PPvrtx allocation in fl_read_elemType.
               implement reading of double precision bindary files, code "(30"
                 in fl_read_dbl and fl_read_int,
               fix size check bug in fl_read_sol.
               fix bug in counting number of unknowns in read_n3s_sol.
               intro switch ab-vol to abort or not on neg vols.
               fix reading and writing prisms in n3s
               fix allocation bug in writing n3s (iVal[6]) in n3s_write_bnd.
               write doubles rather than floats in n3s_write_sol.
               fix organisation of records in n3s_write_sol.
               switch from rho to using T as req. variable
               fix bugs in reading/writing fictive species to/from hdf. (merci GS)
               allow single quotes \' to delimit strings.
               fix bug in reading centaur type 4 files.
  Oct07; 1.21; rework error messages to allow zero output and written error logs
               move mp_bound from write_hdf5_per to write_hdf5_bnd
               only write periodicity section in hdf if it exists.
               read/write fictive species from/to hdf5.
               read Fluent binary files and N-S solution.
               write solution to n3s.
  Jun07; 1.20; intro read_uns_cedre
               fix bug with endless loop in interpolate.
               upgrade reading Centaur to filetype 5.
               adding METIS elGraph to hdf5 mesh files.
               major cleanup using -Wall.
  May07; 1.19.; intro reading and writing hdf5,
                add rhol to the Parameters set of hdf5
                fix bugs with reading and writing avh sets without solution
                fix bug with reading n3s without solution but with set path.
                fix bug with adjusting epsOverlap under tr sc.
                list all neg vols with verbosity > 4
  22Feb07; 1.18.1; fix flowfield init
  20Dec06; 1.18.0; fix bug reading avh without solution
                   tidy up and document naming of variables,
                   fix pb with treating 2-d translationary periodicity
                   intro hdf5 mesh format, writing only at the moment.
                   set special Topo axiX automatically in mult_per_vert.
  17Oct06; 1.17.2; intro read/write avh using hdf5 solution files.
  Sep06; 1.17.1; read solution for n3s
                 fix bug with alloc for double rather than vrtx_s in read_avbp_flowsol
                 allow partial interpolation.
  Jul06; 1.17.0: intro uns_int_bnd

  Jun06; 1.16.5 fix bug in make_mp_bndVx with triply ref'd vx.
         intro local epsOverlap for each grid.
         intro find_nBc for cases when patches are dropped.
         allow any axis for periodicity
  6Jun06; fix bug in make_mp_bndVx, incorrect multiplicity in 3D.
  5Jun06; intro test for range of singleBndVxNormal.
  17May06; limit treatment of multi-patch bnd vx to wall boundaries.
           Allow wildcards in bc-type.
  16May06; drop oxd files,
           support for list of multi-patch bnd vx.
  20Apr06; 1.16; allow 2D centaur files

  Apr06; 1.15.6; rotate/flip also velocities in transform
                 support reading tpf 5.1 and tpf 2.0 file formats
                 support add var in AVSP 3. format
                 fix wrong node table with writing prisms to n3s
                 fix bug with writing quad faces to n3s
                 improve finding a containing element in make_coarser_level for mg.
                 complete hex->tet cutting in co 2tet.
                 change sign of rotation to right hand rule in tr rot.
                 intro se no-single =2 for walls only
                 change default se pe-wr = 1
                 fix bug with reading larger number of add vars.
                 fix bug when comparing nBcIn in match_perBc.
  Feb06; 1.15.5; alter TFP header string to 2.
                 change METIS output lists in write_avbp_elGraph
                 add coll_insitu to deal with few small cells.
                 change the way integrate_rectangle computes the integration points
                   to allow for left-handed (wrong way round) specification.
                 Changed Makefile, intro LITTLE_ENDIAN flag for OSF1 (imhotep)
                 Intro hex2tet.
  Sep05; 1.15.4: fix pb with reading "modular" centaur files.
                 update the manual
                 keep track of hMin when merging grids.
                 deal with little and big-endian centaur files.
  Jun05; 1.15.3; enable coarsened grids to be written to AVBP.
                 fix bug with reading inBound
                 intro rm_match_fc
                 update online help re co 3D, ma, att.
                 fix bug with freestream vars not initialised to zero reading 5.3.
                 add info on periodicity errors for single periodicity
                 add mesh count stats to check and list functions.
  Mar05; 1.15.2; fix bug in the axisymmetric extrusion relating to ABS.
                  fix bug with unclean use of the bc number.
                  fix bug in writing hybrid n3s.
                  reverse boundary face orientation for n3s compared to AVBP.
                  place ADF and CGNS calls within compile options.
  Jan05; 1.15.1; implement connectivity reflection.
                 modify n3s connectivity table for 2D,
                 fix bug with orientation of cutting planes,
                 introduce reading n3s mesh files.
                 update the manual.
  Sep04; 1.14.12; fix bug with buffer alloc in write_avbp_inBound.
                  fix pb with writing n3s connecitivity.
                  rewrite uns_int_plane.
  Jul04; 1.14.11; check connectivity for bounds on node and element numbers
                    fix realloc bug in uns_int_line.
                    fix realloc bug triggered by quad 2 tet
                    fix uns_int_line to deal properly with planar intersections.
                    modify conn. table for tets in write_n3s.
                    filenames now of TEXT_LEN.
  30Apr04; 1.14.10; add coarse grid cell info to mg.
                    rework n3s format
                    add tpf solution file format
                    ensure periodicity upon interpolation, writing.
  6Feb4; 1.14.9:    fix bug in mem alloc leading to seg. fault.
                    intro solfile in 5.3 (add fictive species)
                    fix bug with nadd ne. 0
                    intro reading and writing of averaged sols
                    change least square solver from Cholesky to SVD.
                    fix bug with element counters 2D/3D for reading Centaursoft.
                    fix bug with reading 2D Centaursoft.
                    fix reading characters.
                    intro writing n3s formats.
  22Sep03; 1.14.8b; intro cgns.
                    intro setting var names and types for cgns
                    adjust eps with scale operations.
                    intro uns_int_plane.
                    be more generous with AVBP versions, all above 5. is 5.
  11Jun03; 1.14.7;  intro read_uns_saturne
                    fix bug in prim2cons
                    recognise anyVar in write_avbp_sol
  16Apr03; 1.14.6;  intro single-node-normal switch,
                    geometric transformations,
                    intro numerical range in se bc-text.
                    intro text wildcard match in se bc-text.
                    intro se bc-compress.
                    intro check_lvl.
                    update mean flow solution format to 5.0, make va list names.
                    turn off debugging info when reading fluent files.
                    up to max no of unknowns = 40.
  23Dec02; 1.14.5;  var init now allocates for a missing solution.
                    intro Least Squares interpolation.
                    accept 5.0 as a valid 5.1 string for the avbp solution file.
  10Sep2; 1.14.4;   intro pyramids to CFDRC.
                    new interface to heap, interpolate.
                    intro AVBP Format 5.1
  29Apr2; 1.14.3;   new adf format for AVBP,
                    remove set_var_default.
  20Dec01; 1.14.2;  fix bug in .exBound with normals, always write a number
                    of axi nodes.
                    always write a 8th and a 9th record, possibly empty with axi nodes.
                    add face to vertex pointers as a 10th record to .exBound
                    change match_perBc to construct an orhogonal split vector.
                    exclude vertices on singular boundaries from being listed
                      with a normal in make_bndVxWts.
                    intro default 4.7 asciiBound, add option for 4.2.
                    remove duplication of boundary faces with se pe-wr=1.
                    reread periodic boundary names properly with se pe-wr=0.
                    list smallest/largest element on verbosity > 2
                    number boundary patches from 1 rather than 0.
  10Aug01; 1.14.1;  limit mRef in adapt to be 0<= mRef <= mEl.
                    add Centaursoft hybrid format.
                    enlarge box by a factor rather than a constant for periodic match,
                    use init var to reset r, e.
                    read adjoint fields as an alternative to flow in adf.
                    fix bug in line interpolation in 2D.
                    add zone number to boundary label in read_uns_fluent.
                    write boundary node weights to AVBP boundary files.
  15May01; 1.14.0;  extend line interpolation to 2D,
                    increment number of flow variables when extruding,
                    link one version with damas.
                    reopen derefinement,
                    limit the refinement upgrade to isotropic full.
                    intro reading averaged variables for avbp.
                    fix problem with reading extra variables in avbp.
                    allow to set r with va r.
  11Mar01; 1.13.14; intro mg-adapt
                    properly intialise axi in uns_interpolate.
                    block directional refinement of triangular faces.
                    fix bug in call to ad diff, reverting to div-rot.
                    rework v.f.
                    fix bug in calculating bwts for nonordered sequences.
                    carry avbp-restart in extruded grids.
  20Nov00; 1.13.13; add rot_3d.
                    fix bug in treating boundary weights in symmetry planes.
                    add solution to dpl3d.
                    fix bugs in writing boundary info of parent elements/non-hydra.
                    fix bugs in removing non-used cross-refinement.
                    copy restart info when interpolating,
                    fix bug with circumferential velocity,
                    write/read one record per nadd in avbp.
                    fix pb with unspecified nadd.
                    fix fieldview format.
                    clean adaption modules, intro Grids.adapt.maxLevels.
                    change the adapt syntax and use iso, drop edgeRatio.
                    clean up use of mark_uns_vertBc using only dontPer except ox formats
                    following the cleanup of the boundary normal treatment in 1.13.12.
  14Nov00; 1.13.12; fix bug in writing collapsed edges in adf,
                    simplify boundary normal treatment in hydgrd style.
  25Oct00; 1.13.11; intro uns_int_line.
                    intro limiting to interpolate_elem.
                    intro writing to fieldview.
                    fix bugs in rewriting avbp solution header.
                    read all 5 sets of avbp variables.
  5Sep00;  1.13.10; read a list of elements to be refined from adf.
                    store restart data for avbp, clean up adapt.c.
                    fix bug in setting mElem2VertP in read_uns_fluent.
  26Jul00; 1.13.9; combine periodic edge weights.
  21Jul00; 1.13.8; another attempt to make aniso adapt work, intro negative
                   volume bug workaround for CFDRC.
  28Jun00; 1.13.7; use two rotational copies for the wall distance, ensure periodicity.
  4Jun00;  1.13.7; fix a problem where match_perBc reset the topology to annCasc.
  12May00; 1.13.7; fix problem with wall distance projection.
                   intro overWrite to adf writing.
                   fix bug with sign of scProd in 2D.
  27Mar00; 1.13.5, anisotropic adaptation works. But not really.
           allow 2d-3d axi interpolation
           allow 360 degree extrusion
           fix some problems with recognising periodic geometries
           allow for periodicity in calc_wall_distance.
           change the tree to store intermediate value extractions.
           intro uns_generate.
           Tue May  9 11:50:35 BST 2000: fix problem with dimensionality in adf.
  6Mar00;  1.13.4.
           fix problem with reading solution files in oxd.
           fix bug with 2-D coarsening and mVxColl test in uns_mg.
           intro periodicity in the adaption.
  9Feb00;  1.13.3; list quad-->edge circularly.
  27Nov99; 1.13.0; more or less final version of the multigrid with accelerated
           angle condition and no length control.
           Validated metrics in adf against hydgrd.
  27Sep99; 1.12.1; better memory management, intro list arr,
           fix bug in node normals for adfh,
           recover boundary faces from nodal info in read_uns_adf.c,
           treat collapsed faces for bnd_quad-->node in write_uns_adf,
           omit collapsed edges from the edge list,
           intro egWtCutOff,
           fix periodic vertices if fixPerVx is set.,
           adjust epsOverlap to the meshsize.
           remove bugs in reading 2D adfh.
           check all wall types for zeroing normals at corners, set normAnglCut .9
           change the keyword structure of adf to align with hydra files.
           introduce the volume aspect ratio.
  7Sep99; 1.12.0; intro interpolation.
  16Jul99; 1.10.1; periodicity for the hybrid collapsing, read boundary faces
           via boundary vertices as well, flite format.
  8Feb99; 1.9.13; axisymmetric extrusion.
  16Dec98; 1.9.12; various extra listings on verb=5, low-storage option, treat
           special topologies like axisymmetry along x, give an optional axis for
           extrusion, fix a bug in reading CFDRC prisms, check for proper periodic
           boundary setup, refuse to write invalid grids.
  7Dec98; 1.9.11; fix input bug for cut iso.
  26Nov98; 1.9.10; fix bug in read_str_dpl and get_surfVx_drvElem.
  17Nov98; 1.9.9; allow to change the order of boundary patches with set bc-order.
  6Nov98; 1.9.8; fix various bugs, notably in mb_cut, read_mb_cfdrc.
  28Oct98; 1.9.7; intro mgSemiMid, fix bug with boundary node ordering in write_oxd2.
  26Oct98; 1.9.6; wall vertex normals in corners/edges can be set to zero.
  20Oct98; 1.9.5; change treatment of periodic node normals.
  Jul89; 1.9.0, new data structure of adapted edges.
  June98; 1.8.3, 3D edge-weights, initialize periodicity to 0, carry fxDiag in elMark.
  June98; 1.8.2, add periodic edges to cindy 3 format.
  May98; 1.8. new periodicity, fix CFDRC formats.
  April98; 1.7.1, start working on Fluent-UNS interface.
  27Mar98; 1.7, first release of the hybrid edge-weights, multigrid coarsening.
  23Mar98; 1.6.4, treat interface boundaries with tri-quad matches out of CFDRC
           properly.
  23Jan98; version 1.6.3, edge-based stuff, oxd format.
  12Jan98; version 1.6.2, fix bugs in reading cfdrc-mixed.
  10Dec97; version 1.6.1, introduce switch perBc_in_exBound.
  11Nov97, version 1.6.
  12Oct97; version 1.5.1, fix various bugs, intro a list PPbc with each chunk.
  Sep97; version 1.5
  Jan96; version 1.3
  15Nov96; version 1.2
  15Jul96; derived from cpre.
*/


/***********************************************************
  Constants, immutable */

const char fatal_log_file[] = "hip-fatal.log" ;
const char warning_log_file[] = "hip-warning.log" ;


/* Make sure this matches with the grid_type_enum. */
const char gridTypeNames[3][7] = { "noGrid", "mb", "uns" } ;

/* Specially treated topologies. */
const char topoString[][MINITXT_LEN] = {
  "noTopo", "axi-x", "axi-y", "axi-z", "noBc", "surf" } ;

/* Define a NULL pointer for cpt. */
/*const cpt_s CP_NULL = {0,0} ;
const cpt_s CP_MAX = {  MAX_CPT_CHUNKS, MAX_CPT_VXNR } ;*/


/* State variables.
typedef enum { noVar, cons, prim, primT, para, anyVar } varType_e ; */
const char varTypeNames[][LEN_VAR_C] =
{ "noVaTyp\0", "cons\0", "prim\0", "primT\0", "para\0", "anyType\0" } ;

/* Variable categories. */
const char varCatNames[][LEN_VAR_C] =  { "noCat", "ns",
                                         "species", "rrates",
                                         "tpf", "rans",
                                         "add", "mean",
                                         "fictive", "add_tpf",
                                         "param",
                                         "other" } ;

/* Groupnames for the hdf5 files. */
const char h5GrpNames[][LEN_GRPNAME] =  { "noCat", "GaseousPhase",
                                           "RhoSpecies", "rrates",
                                           "LiquidPhase", "rans",
                                           "add", "mean",
                                           "FictiveSpecies", "add_tpf",
                                           "Parameters",
                                           "other" } ;

/* parameters, associated with zones.
cpre.h:
typedef enum { noPar, parInt, parDbl, parVec } parType_e ; */
const int parTypeSize[] = {0, sizeof(int), sizeof( double), sizeof(double) } ;


/* bc_type stuff. */
char stdBcType[] = { 'e', 'f', 'i', 'l', 'o', 'p', 's', 'u', 'v', 'w', 'n' } ;
char stdBbcTypeNames[][15] = { "entry", "far field", "inviscid wall", "lower periodic",
                               "outlet", "any periodic", "symmetry plane",
                               "upper periodic", "viscous wall", "any wall", "none" } ;


/* Sliding/mixing plane. */
const char sp_geo_type_string[][MINITXT_LEN] = {
  "no_const",
  "const_r", "const_rx", "const_ry", "const_rz",
  "const_x", "const_y", "const_z" } ;

/* Realloc by this factor. */
double reallocFactor = 1.2 ;
double minArrSize = 100 ;

/* Lines are considered parallel if the det is para_tol*eps.*/
const double para_tol = .0001 ;
/* An intersection is given if the system is solved to that multiple of eps. */
const double x_fac = 1.e-4 ;

/* What to do with exposed cut faces? Put them into a new boundary?
int cutFc_to_bnd = 1 ; */

/* Make sure this matches with the reco_enum. */
const char recoString[][MINITXT_LEN] = { "elem", "ls 1", "ls 2", "minnorm", "flag", "none" } ;
const double minnorm_tol = -1.e-10 ; // Need to make set-able param.


/* AVBP Format flavour.
  Corresponding def in cpre_uns.h:
typedef enum { noFmt, v4_2, v4_7, v5_1, v5_3, t5_1, t2_0, s3_0, v6_0 } avbpFmt_e ;  */
char avbpFmtStr[][MAX_BC_CHAR] = { "???", "Version 4.2", "Version V4.7",
                                   "Version V5.1", "Version V5.3",
                                   "TPF Version V5.1",
                                   "TPF Version V2.0", "AVSP Version V3.0",
                                   "Version V6.0" } ;

/* Magic keys for the keyword library. */
const char magicAKey[] = "%%%_", magicBKey[] = "%%%_" ;
const int magicASize = 4, magicBSize = 4 ;


/**********************************************************
 set-able parameters via set_menu. */

int verbosity = 3 ;
hip_output_e hip_output ;


/* How to deal with negative volumes. */
int negVol_abort ;
int negVol_flip ;


/* How to treat duplicate, matching and cut faces. */
doFc_struct doWarn, doRemove ;

double Gamma = 1.4, GammaM1 = .4 ;
double R = 287. ;

/* Symmetry plane is in which coordinate? Default y=0. */
int symmCoor ;


/* What to do with periodic boundaries. Write them as external boundaries? */
int perBc_in_exBound ;

/* When to consider faces to be parallel (i.e. periodic translation
   or oblique (i.e. periodic rotation). Minor component of the
   difference of the unit normals on the matching patches. */
double per_thresh_is_rot ;


/* By how much can an edge lengthen in the edge-collapsing? */
double mgLen ;
/* What is the cosine of the largest angle we want to tolerate in a coarser grid. */
double mgLrgstAngle ;
/* Minimum face twist, 1. being linear, -1. being completely folded over. */
double mgTwistMin ;
/* Minimum volumetric aspect ratio. */
double mgVolAspect ;
/* Minimum volume of a subelement in convexity test as
   fraction of epsOver Cube. */
double  mgVolMinSub ;
/* What do we consider stretched in order to apply semi-coarsening? */
double mgArCutoff ;
double mgArCutoff2 ;
double mgRamp ;

// MMG
int mmg_mLayerBcZone ;


/* Find degenerate multiblock faces by length comparisons? */
int find_mbDegenFaces ;

/* How to treat elements with degenerate edges. */
int fix_degenElems ;

/* Fix elements with too large angles? And what is the cosine of the threshold? */
int dg_fix_lrgAngles ;
double dg_lrgAngle ;

/* When calculating nodal normals, set these to zero if the face angles vary more
   than this cosine from the total node normal. */
double normAnglCut ;
int singleBndVxNormal ;

/* Chord length for the NACA0012 surface reco. */
double chord = 1. ;

/* Low storage option. E.g., no face connectivity check. */
int check_lvl ;



/* Least squares reconstruction for interpolation. */
reco_enum reco ;
double mVxRecoFactor ;

double intPolRim ; /* the allowable distance to interpolate outside the domain is
   intPolRim*hEdge where hEdge is the max edge-length of the nearest face. */

double intPol_dist_inside ; /* When to consider a location covered by a grid:
   the nearest vertex is closer than this many multiples of epsOverlap. */

double intFcTol ; /* When to consider a vertex to be inside an element:
   the smallest face distance is closer than this many multiples of hElem,
   the typical size of the element. */

double intFullTol ; /* If there is no containment in elmeent-based searches,
    search all of those cells where the donor is within  intFullTol*hElem. */

/* Weighting function. */
double choldc_tol ;
double ls_lambda ;



/******************************************************************************
  Global variables, fields: */


/* These have to be set for the read1 package. */
FILE *InFile, *OutFile ;
char line[LINE_LEN] ;

/* Families for initialisation storage. */
arrFam_s *pArrFamUnsInit ;
arrFam_s *pArrFamMbInit ;
/* For simplicity, we keep only one multiblock array family. */
arrFam_s *pArrFamMb ;


/* Statically allocated virtual element. */
double vrtCoor[MAX_VX_ELEM*MAX_DIM] ;
vrtx_struct vrtVrtx[MAX_VX_ELEM], *pVrtVrtx[MAX_VX_ELEM] ;
elem_struct vrtElem ;


/* Timing. */
struct tms timings ;
clock_t nowTime, iniTime ;
int clk_ticks ;

char hip_msg[MAX_MSG_LINES*LINE_LEN] ;
char hip_out_buf[MAX_MSG_LINES*LINE_LEN] ;
char *pHip_out_buf_wrt = hip_out_buf ;

ret_s memory(){
  ret_s ret = ret_success () ;

  struct rusage RU;
  double max_memory;
  getrusage(RUSAGE_SELF, &RU);
  max_memory = ((double) RU.ru_maxrss)/1024.0;
  // On mac ru_maxrss is in bytes
#ifdef MAC
  max_memory = ((double) RU.ru_maxrss)/1024.0/1024.0;
#endif
  sprintf ( hip_msg, "Max memory %.3f MBytes.",max_memory);
  hip_err ( info, 0, hip_msg ) ;
  flush_buffer () ;
  return ( ret ) ;
}

/* The mother of all roots. */
Grids_struct Grids ;

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  bc_menu:
*/
/*! modify bcs.
 *
 */

/*

  Last update:
  ------------
  14May20: conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s bc_menu ( char line[LINE_LEN] ) {

  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;


  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There are no bc to modify." ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }


  char keyword[LINE_LEN] ;
  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;


  if ( !strncmp ( keyword, "text", 2 ) ) {
    ret = set_bc_text_arg (  ) ;
  }
  else if ( !strncmp ( keyword, "type", 2 ) ) {
    ret = set_bc_type_arg () ;
  }
  else if ( !strncmp ( keyword, "mark", 2 ) ) {
    ret = set_bc_mark_arg () ;
  }
  else if ( !strncmp ( keyword, "order", 2 ) ) {
    ret = set_bc_order_arg () ;
  }
  else if ( !strncmp ( keyword, "compress", 2 ) ) {
    if ( Grids.PcurrentGrid->uns.type == uns )
      uns_compress_bc ( Grids.PcurrentGrid->uns.pUns ) ;
  }
  else if (  !strncmp ( keyword, "split", 2 ) ) {
    split_uns_bcPatch_arg ( ) ;
  }
  return ( ret ) ;
}
  /******************************************************************************
  de_menu:   */

/*! decimate a grid. Currently only surface.
 *
 */

/*

  Last update:
  ------------
  12Oct20; read mDim from pUns, not from its grid wrapper.
  30jun16: derived from mm_menu

*/

/* hip_cython */
ret_s de_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], argLine[LINE_LEN] ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to decimate." ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }

  else if ( Grids.PcurrentGrid->uns.type != uns ) {
    sprintf ( hip_msg,
              "Can only decimate unstructured grids. Copy to uns first." ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1line ( argLine ) ;

  #ifdef MMG
  if ( Grids.PcurrentGrid->uns.pUns->mDim == 3 ) {
    if ( decimate_mmgs_3d ( Grids.PcurrentGrid->uns.pUns, argLine ) ) {
      sprintf ( hip_msg, "Failed to decimate this grid." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  flush_buffer () ;
  #else
  flush_buffer () ;
  ret = hip_err ( fatal, 0, "requesting mmg, but not compiled." ) ;
  #endif

  ret.pGrid = Grids.PcurrentGrid ;
  ret.pUns = Grids.PcurrentGrid->uns.pUns ;
  return ( ret ) ;
}

/******************************************************************************
  flag_menu:   */

/*! read parameters for flagging mesh entities, currently only nodes allowed.
 *
 */

/*

  Last update:
  ------------
  6Dec20; introduce sector, read cylinder.
  1Feb12; rename uns_flag_vx to uns_flag_vx_geo.
  16Dec11: conceived.


*/
/* hip_cython */
ret_s flag_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN] ;
  geo_s geo ;
  geo.box.type = noGeo ;


  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    ret = hip_err ( warning, 0, "there is nothing to flag." ) ;
    flush_buffer () ;
    return ( ret ) ;
  }
  else if (  Grids.PcurrentGrid->uns.type != uns ) {
    ret = hip_err ( warning, 0, "currently only unstructured grids can be flagged." ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  ret.pGrid =  Grids.PcurrentGrid ;
  uns_s *pUns = ret.pUns = Grids.PcurrentGrid->uns.pUns ;
  const int mDim = Grids.PcurrentGrid->uns.mDim ;


  if ( eo_buffer () ) {
    /* Reset and free all flags, currently only nodes. */
    unflag_vx ( pUns ) ;
    free_vx_flag ( pUns ) ;
  }


  else {
    /* There is flag operation specified, execute it. */
    read1lostring ( keyword ) ;


    if ( !strncmp ( keyword, "nodes", 2 ) ) {
      /* Flag nodes. */

      /* Which geometric type? */
      if ( eo_buffer () ) {
        /* Reset. */
        unflag_vx ( pUns ) ;
        free_vx_flag ( pUns ) ;
      }
      else {
        read1lostring ( keyword ) ;

        /* Get type parameters. */
        int k ;
        if ( !strncmp ( keyword, "all", 2 ) ) {
          geo.box.type = allGeo ;
        }
        else if ( !strncmp ( keyword, "box", 2 ) ) {
          geo.box.type = box ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.box.ll+k ) ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.box.ur+k ) ;
        }
        else if ( !strncmp ( keyword, "plane", 2 ) ) {
          geo.box.type = plane ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.plane.loc+k ) ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.plane.norm+k ) ;
        }
        else if ( !strncmp ( keyword, "sphere", 2 ) ) {
          geo.box.type = sphere ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.sph.loc+k ) ;
          read1double ( &geo.sph.rad ) ;
        }
        else if ( !strncmp ( keyword, "cylinder", 2 ) ) {
          geo.box.type = cyl ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.cyl.loc+k ) ;
          for ( k = 0 ; k < mDim ; k++ ) {
            read1double ( geo.cyl.axis+k ) ;
          }
          read1double ( &geo.cyl.rad ) ;
        }
        else if ( !strncmp ( keyword, "sector", 2 ) ) {
          char axis = 'x' ;
          geo.box.type = sector ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.sec.llf+k ) ;
          for ( k = 0 ; k < mDim ; k++ )
            read1double ( geo.sec.urr+k ) ;

          // Default x axis:
          geo.sec.kDim[0] = 0 ;
          geo.sec.kDim[1] = 1 ;
          geo.sec.kDim[2] = 2 ;

          if ( !eo_buffer () ) {
            read1char ( &axis ) ;
            axis = tolower ( axis ) ;
            switch ( axis ) {
            case 'x' :
              break ;
            case 'y' :
              geo.sec.kDim[0] = 1 ;
              geo.sec.kDim[1] = 2 ;
              geo.sec.kDim[2] = 0 ;
              break ;
            case 'z' :
              geo.sec.kDim[0] = 2 ;
              geo.sec.kDim[1] = 0 ;
              geo.sec.kDim[2] = 1 ;
              break ;
            default:
              sprintf ( hip_msg, "unrecognised axis %c, using x.\n", axis ) ;
              hip_err ( warning, 1, hip_msg ) ;
            }
          }

          // Find min/max r, theta.
          cart2cyl ( geo.sec.llf, geo.sec.kDim[0], pUns->mDim,
                     geo.sec.r+0, geo.sec.th+0 ) ;
          cart2cyl ( geo.sec.urr, geo.sec.kDim[0], pUns->mDim,
                     geo.sec.r+1, geo.sec.th+1 ) ;
          if ( geo.sec.r[0] > geo.sec.r[1] ) {
            /* Swap. */
            double rSwap = geo.sec.r[0] ;
            geo.sec.r[0] = geo.sec.r[1] ;
            geo.sec.r[1] = rSwap ;
          }
          if ( geo.sec.th[0] > geo.sec.th[1] ) {
            /* Swap. */
            double thSwap = geo.sec.th[0] ;
            geo.sec.th[0] = geo.sec.th[1] ;
            geo.sec.th[1] = thSwap ;
          }

          if ( geo.sec.th[0] < -PI/2 &&  geo.sec.th[1] > PI/2 ) {
            /* Start in fourth, finsh in third quadrant.
               Warn to make sure geometry doesn't cross -kDim axis. */
            sprintf ( hip_msg,
                      "Your sectors starts in fourth, finishes in third quadrant,\n"
                      "          hip takes the branch cut between those two. Make sure your\n"
                      "          geometry doesn't cross the negative %c-axis.\n", axis ) ;
            hip_err ( warning, 1, hip_msg ) ;
          }
        }
        else {
          ret = hip_err ( warning, 1,
                          "unrecognised geometric type for flag operation." ) ;
          return ( ret ) ;
        }

        uns_flag_vx_geo ( pUns, &geo, "flag_menu" ) ;
      }
    }

    else
      ret = hip_err ( warning, 0, "currently only flagging of nodes is supported." ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}




/*****************************************************************/

/* hip_cython */
ret_s generate_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  double ll[MAX_DIM], ur[MAX_DIM] ;
  int mX, mY ;

  read1double ( ll ) ;
  read1double ( ll+1 ) ;
  read1double ( ur ) ;
  read1double ( ur+1 ) ;

  read1int ( &mX ) ;
  read1int ( &mY ) ;

  ret = uns_generate ( ll, ur, mX, mY ) ;

  flush_buffer () ;
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  interface_menu:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  3dec19: conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s interface_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], argLine[LINE_LEN] ;
  size_t keyLen ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to write." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;


  if ( !strncmp ( keyword, "mixingplane", 2 ) ||
       !strncmp ( keyword, "slidingplane", 2 ) ) {

    strcat ( keyword, " " ) ;
    keyLen = strlen ( keyword ) ;
    strncpy ( argLine, keyword, keyLen+1 ) ;
    /* Stick the keyword at the beginning of the argLine, similar to getopt. */
    if ( !eo_buffer () )
      read1line ( argLine+keyLen ) ;


    uns_interface_sliding_plane ( argLine ) ;
  }
  else {
    ret.status = warning ;
    hip_err ( warning, 1, "unknown interface option" ) ;
  }

  return ( ret ) ;
}


/******************************************************************************
  read_transf_opt:   */

/*! read the parameters for transformation operations from the prompt.
 *
 *
 *
 */

/*

  Last update:
  ------------
  13Dec20; introduce per option.
  27Feb19; convert rotAngle to rad.
  18Oct10 initialise dVal.
  16Sep10; extracted from transform for reuse in uns_copy.:

  Changes To:
  -----------
  read buffer through readline lib.

  Output:
  -------
  pTr_op: transform operation enum, see def in cpre.h
  dVal[]: array of double parameters for the transform operation:
          trans: the x,y,z components of the translation vector
          scale: the x,y,z scaling factors
          rot_x,y,z: the rotation angle in rad (one value only.)
          ref_x,y,z: no value.

  Returns:
  --------
  0 on failure, 1 on success

*/


ret_s read_transf_op ( transf_e *pTr_op, double *dVal ) {
  ret_s ret = ret_success () ;
  int k ;
  char keyword[LINE_LEN], coorDir ;

  *pTr_op = noTr ;

  /* Initialise cleanly, even though the hitherto unitiliased values are unused. */
  for ( k = 0 ; k < MAX_DIM ; k++ )
    dVal[k] = 0.0 ;

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;


  if ( !strncmp ( keyword, "translation", 2 ) ) {
    /* Translation. Which direction? */

    for ( k = 0 ; k < MAX_DIM ; k++ ) {
      /* Is there a component given? */
      if ( eo_buffer () )
        dVal[k] = 0. ;
      else {
        *pTr_op = trans ;
        read1double ( dVal+k ) ;
      }
    }

    if ( !*pTr_op ) {
      sprintf ( hip_msg,
                "zero translation is no translation." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }

  else if ( !strncmp ( keyword, "scale", 2 ) ) {
    /* Scaling. */

    for ( k = 0 ; k < MAX_DIM ; k++ ) {
      /* Is there a component given? */
      if ( eo_buffer () )
        dVal[k] = 1. ;
      else {
        *pTr_op = scale ;
        read1double ( dVal+k ) ;
      }
    }

    if ( !*pTr_op ) {
      sprintf ( hip_msg, "unit scaling in each direction is no scaling." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }

  else if ( !strncmp ( keyword, "rotate", 2 ) ) {
    /* Rotation. */

    if ( eo_buffer () )
      coorDir = '\0' ;
    else
      read1char ( &coorDir ) ;
    coorDir = tolower( coorDir ) ;

    if      ( coorDir == 'x' ) *pTr_op = rot_x ;
    else if ( coorDir == 'y' ) *pTr_op = rot_y ;
    else if ( coorDir == 'z' ) *pTr_op = rot_z ;
    else {
      sprintf ( hip_msg, "unrecognised coordinate direction %d.", coorDir ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }

    if ( !eo_buffer () ) {
      /* Read the rotation angle around the axis. */
      read1double ( dVal ) ;
      // convert to rad.
      dVal[0] *= PI/180 ;
    }
    else {
      *pTr_op = noTr ;
      sprintf ( hip_msg, "zero rotation is no rotation." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }


  else if ( !strncmp ( keyword, "reflect", 2 ) ) {
    /* Reflect around a plane, ie. change sign of a particular coordinate. */

    if ( eo_buffer () )
      coorDir = '\0' ;
    else
      read1char ( &coorDir ) ;
    coorDir = tolower( coorDir ) ;

    if      ( coorDir == 'x' ) *pTr_op = ref_x ;
    else if ( coorDir == 'y' ) *pTr_op = ref_y ;
    else if ( coorDir == 'z' ) *pTr_op = ref_z ;
    else {
      sprintf ( hip_msg, "unrecognised coordinate direction %d.", coorDir ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }

  else if (  !strncmp ( keyword, "per", 2 ) ) {
    /* Use existing periodicity. */
    if ( !Grids.PcurrentGrid ) {
      /* No grid present. */
      sprintf ( hip_msg, "There is no grid to take periodicity information from." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }
    else if  ( Grids.PcurrentGrid->uns.type != uns ) {
      sprintf ( hip_msg, "Only unstructured grids carry periodicity information." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }

    uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
    if ( !pUns->mPerBcPairs ) {
      sprintf ( hip_msg, "This grid does not carry periodicity information." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }
    else if  ( pUns->mPerBcPairs > 1 ) {
      sprintf ( hip_msg, "This grid carrys more than one periodicity. Using the first." ) ;
      ret = hip_err ( warning, 1, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }

    /* Establish periodic pairs and their tr_op. */
    if ( !special_verts ( pUns ) ) {
      sprintf ( hip_msg, "failed to match periodic vertices in read_transf_op.\n" ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }
    perBc_s *pPerBc = pUns->pPerVxPair->pPerBc ;

    *pTr_op = pPerBc->tr_op ;
    switch ( *pTr_op ) {
    case rot_x :
    case rot_y :
    case rot_z :
      dVal[0] = pPerBc->rotAngleRad ;
      break ;
    case trans :
      vec_copy_dbl ( pPerBc->shftIn2out, pUns->mDim, dVal ) ;
      break ;
    default :
      sprintf ( hip_msg, "A periodicity operation that is neither rotation nor translation."
                "        Can't handle that." ) ;
      ret = hip_err ( warning, 1, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }
  }

  return ( ret ) ;
}


/******************************************************************************
  interpolate_menu:   */

/*! interpolate menu
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  19Dec24; enable searching for grids by name for interpolation source
  6Sep18; new arg for merge_uns.
  7Apr14; fix too short string length in strncpy
  : conceived.

*/

/* hip_cython */
ret_s interpolate_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  int gridNr = 1 ;
  char axi = 'n', keyword[LINE_LEN], fileName[LINE_LEN], expr[LINE_LEN] ;
  double xBeg[3], xEnd[3], xP[3][3], z ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There are no grids to interpolate." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    return ( ret ) ;
  }

  if ( !eo_buffer () ) {
    read1lostring ( keyword ) ;


    if ( !strncmp ( keyword, "grid", 2 ) ) {
      if ( !eo_buffer () )
        read1string ( expr ) ;
      if ( !eo_buffer () )
        read1char ( &axi ) ;

      grid_struct  *pGrid = find_grid ( expr, noGr ) ;
      if ( !pGrid) {
        return(hip_err ( warning, 1, "could not find a matching grid to interpolate from, skipping" ));
      }
      else if ( pGrid->uns.type != uns ) {
        return( hip_err ( warning, 1, "matching grid to interpolate from needs to be unstructured, skipping" ));
      }
      gridNr = pGrid->uns.pUns->nr ;

      
      if ( !interpolate ( gridNr, axi ) ) {
        sprintf ( hip_msg, "failed to interpolate a solution for this grid." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }

    else if ( !strncmp ( keyword, "line", 2 ) ) {

      read1lostring( keyword ) ;

      read1double ( xBeg ) ;
      read1double ( xBeg+1 ) ;
      read1double ( xBeg+2 ) ;

      read1double ( xEnd ) ;
      read1double ( xEnd+1 ) ;
      read1double ( xEnd+2 ) ;

      read1string ( fileName ) ;

      if ( !uns_int_line ( xBeg, xEnd, fileName, keyword ) ) {
        sprintf ( hip_msg, "failed to interpolate a line in this grid." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    else if ( !strncmp ( keyword, "rectangle", 2 ) ) {
      double swf[3], sef[3], nwf[3], swr[3] ;
      int mE, mN ;

      read1lostring( keyword ) ;

      read1double ( swf ) ;
      read1double ( swf+1 ) ;
      read1double ( swf+2 ) ;

      read1double ( sef ) ;
      read1double ( sef+1 ) ;
      read1double ( sef+2 ) ;

      read1double ( nwf ) ;
      read1double ( nwf+1 ) ;
      read1double ( nwf+2 ) ;

      read1double ( swr ) ;
      read1double ( swr+1 ) ;
      read1double ( swr+2 ) ;

      read1int ( &mE ) ;
      read1int ( &mN ) ;

      read1string ( fileName ) ;

      if ( !integrate_rectangle ( swf, sef, nwf, swr, mE, mN, fileName, keyword ) ) {
        sprintf ( hip_msg, "failed to interpolate a line in this grid." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    else if ( !strncmp ( keyword, "plane", 2 ) ) {

      read1double ( xP[0] ) ;
      read1double ( xP[0]+1 ) ;
      read1double ( xP[0]+2 ) ;

      read1double ( xP[1] ) ;
      read1double ( xP[1]+1 ) ;
      read1double ( xP[1]+2 ) ;

      read1double ( xP[2] ) ;
      read1double ( xP[2]+1 ) ;
      read1double ( xP[2]+2 ) ;

      if ( !uns_int_plane ( xP ) ) {
        sprintf ( hip_msg, "failed to interpolate a plane in this grid." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    else if (!strncmp ( keyword, "bnd", 2 ) ) {
      if ( !eo_buffer () )
        read1double ( &z ) ;
      else
        z = 0. ;

      if ( !uns_int_bnd ( z ) ) {
        sprintf ( hip_msg,
                  "failed to reduce to boundary surface in this grid." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    else {
      sprintf ( hip_msg, "Not enough arguments" ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }
  else {
    sprintf ( hip_msg, "Not enough arguments" ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }


  flush_buffer () ;
  return ( ret ) ;
}



/*****************************************************************/

/* hip_cython */
ret_s list_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN] ;
  grid_struct *Pgrid ;
  bc_struct *Pbc ;
  const varList_s *pVarList ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "there is no data to list." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "grid", 2 ) && verbosity > 0 ) {
    /* List all grids stored. */
    ret = list_grids () ;
  }

  else if ( !strncmp ( keyword, "surf", 2 ) && verbosity > 0 ) {
    /* List all surfaces of the current grid. */

    if ( eo_buffer () )
      keyword[0] = '\0' ;
    else
      read1lostring ( keyword ) ;

    ret = list_surfaces ( keyword ) ;
  }

  else if ( !strncmp ( keyword, "periodic", 2 ) && verbosity > 0 ) {
    /* List all periodic patch pairs of the current grid. */
    list_per_pairs ( Grids.PcurrentGrid ) ;
  }

  else if ( !strncmp ( keyword, "storage", 2 ) && verbosity > 0 ) {
    /* List all allocated arrays. */
    show_arrUse ( NULL ) ;
  }

  else if ( !strncmp ( keyword, "zone", 2 ) && verbosity > 0 ) {
    /* List zones . */

    if ( eo_buffer () )
      keyword[0] = '\0' ;
    else
      read1lostring ( keyword ) ;

    if ( !strncmp ( keyword, "all", 2 ) )
      zone_list_all ( ) ;
    else
      zone_list ( Grids.PcurrentGrid->uns.pUns, NULL ) ;
  }

  else {
    /* no argument. */
    ret = hip_err ( warning, 1,
                    "list needs an argument of [grids,current,surfaces,periodic]." ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}

/*****************************************************************/

/* hip_cython */
ret_s mark_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], expr[LINE_LEN] ;
  bc_struct *Pbc, *PbcNr ;
  int bcNr, k, found ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to mark." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "surf", 2 ) )  {
    /* Mark the given surfaces. */

    if ( eo_buffer () ) {
      /* No arguments, clear all marks. */
      for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc )
	Pbc->mark = 0 ;
    }
    else
      /* Mark all given bc numbers. */
      while ( !eo_buffer() ) {
        read1string ( expr ) ;
        found = 0 ;
        for ( Pbc = NULL, found = 0 ; loop_bc_expr( &Pbc, expr ) ; ) {
          found = 1 ;
	  Pbc->mark = 1 ;
        }
        if (!found) {
	  sprintf ( hip_msg, "no boundary condition that matches expression." ) ;
          ret = hip_err ( warning, 1, hip_msg ) ;
        }

        // read1int ( &bcNr ) ;
	// /* Find the bc. */
	// for ( PbcNr = NULL, Pbc = find_bc ( "", 0 ) ;
	//       Pbc ; Pbc = Pbc->PnxtBc )
	//   if ( Pbc->nr == bcNr )
	//     PbcNr = Pbc ;
        // 
	// if ( !PbcNr ) {
	//   sprintf ( hip_msg, "no boundary condition %d found.", bcNr ) ;
        //   ret = hip_err ( fatal, 0, hip_msg ) ;
        // }
	// else
	//   PbcNr->mark = 1 ;
      }
  }


  else if ( !strncmp ( keyword, "interface", 2 ) )  {

    if ( eo_buffer () ) {
      /* No argument. Erase all interface declarations. */
      for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc ) {
        if ( Pbc->geoType == inter || Pbc->geoType == duplicateInter )
          Pbc->geoType = bnd ;
      }
    }
    else {
      /* Read a list of bcs to match. */
      while ( !eo_buffer () ) {

        for ( k = 0 ; k < 2 && !eo_buffer () ; k++ ) {
          read1string ( expr ) ;

          found = 0 ;
          for ( Pbc = NULL, found = 0 ; loop_bc_expr( &Pbc, expr ) ; ) {
            found++ ;
            if ( k>0 )
              /* duplicate, eliminate. */
              Pbc->geoType = duplicateInter ;
            else
              Pbc->geoType = inter ;
          }
          if (!found) {
            sprintf ( hip_msg, "no boundary condition that matches expression." ) ;
            ret = hip_err ( warning, 1, hip_msg ) ;
          }
          else if ( found>1 ){
            if ( k == 0 ) {
              sprintf ( hip_msg,
                        "found %d bc that matches first expression,"
                        " all taken as first side of interface",
                        found ) ;
            }
            else {
              sprintf ( hip_msg,
                        "found %d bc that matches first expression,"
                        " all taken as second/duplicate side of interface",
                        found ) ;
            }
            ret = hip_err ( warning, 1, hip_msg ) ;
          }
          }
        }
      }
      
     // for ( k = 0 ; k < 2 && !eo_buffer () ; k++ ) {
     //   read1int ( &bcNr ) ;
     // 
     //   /* Find the bc. */
     //   for ( PbcNr = NULL, Pbc = find_bc ( "", 0 ) ;
     //         Pbc ; Pbc = Pbc->PnxtBc )
     //     if ( Pbc->nr == bcNr )
     //       PbcNr = Pbc ;
     // 
     //   if ( !PbcNr ) {
     //     sprintf ( hip_msg, "no boundary condition %d found.", bcNr ) ;
     //     ret = hip_err ( warning, 1, hip_msg ) ;
     //   }
     //   else if ( k>0 )
     //     /* duplicate, eliminate. */
     //     PbcNr->geoType = duplicateInter ;
     //   else
     //     PbcNr->geoType = inter ;
     // }
  }


  else if ( !strncmp ( keyword, "match", 2 ) )  {

    if ( eo_buffer () ) {
      /* No argument. Erase all matchings. */
      for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc )
        if ( Pbc->geoType == match )
          Pbc->geoType = bnd ;
    }
    else {
      /* Read a list of bcs to match. */
      while ( !eo_buffer () ) {
        // read1int ( &bcNr ) ;
        read1string ( expr ) ;
        found = 0 ;
        for ( Pbc = NULL, found = 0 ; loop_bc_expr( &Pbc, expr ) ; ) {
          found = 1 ;
	  Pbc->geoType = match ;
        }
        if (!found) {
	  sprintf ( hip_msg, "no boundary condition that matches expression." ) ;
          ret = hip_err ( warning, 1, hip_msg ) ;
        }
      }

      // read1int ( &bcNr ) ;
      // /* Find the bc. */
      // for ( PbcNr = NULL, Pbc = find_bc ( "", 0 ) ;
      //       Pbc ; Pbc = Pbc->PnxtBc )
      //   if ( Pbc->nr == bcNr )
      //     PbcNr = Pbc ;
      // 
      // if ( !PbcNr ) {
      //   sprintf ( hip_msg, "no boundary condition %d found.", bcNr ) ;
      //   ret = hip_err ( warning, 1, hip_msg ) ;
      // }
      // if ( !strncmp ( keyword, "match", 2 ) )
      //   PbcNr->geoType = match ;
    }
  }

  else {
    /* No argument. Clear all marks and matches. */
    for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc )
      Pbc->mark = Pbc->geoType = bnd ;
  }

  flush_buffer () ;
  return ( ret ) ;
}

/*****************************************************************/

/* hip_cython */
ret_s cut_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], dirType[LINE_LEN], avgType[LINE_LEN] ;
  double cutDist, isoVal ;
  int cutType ;
  isotype_enum isoType ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to cut." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "dist", 2 ) ) {
    /* Cut according to distance. */
    read1double ( &cutDist ) ;
    if ( eo_buffer () )
      /* Default cutType 0. */
      cutType = 0 ;
    else
      read1int ( &cutType ) ;

    if ( Grids.PcurrentGrid->uns.type == uns ) {
      sprintf ( hip_msg,
                " no unstructured cutting yet. Sorry. Call back later." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
    else if ( Grids.PcurrentGrid->mb.type == mb )
      /* Mark a cut in a multiblock grid. */
      cut_mb_dist ( Grids.PcurrentGrid->mb.Pmb, cutDist, cutType ) ;
  }

  else if ( !strncmp ( keyword, "iso", 2 ) )
  { /* Cut according to isotropy. */

    /* Type of isotropy. */
    if ( eo_buffer () )
      isoType = minmax_all ;
    else {
      read1lostring ( avgType ) ;

      if ( eo_buffer () )
        strcpy ( dirType, "all" ) ;
      else
        read1lostring ( dirType ) ;

      if ( !strncmp( avgType, "min", 2 ) &&
	   !strncmp( dirType, "all", 2 ) )
	isoType = minmax_all ;
      else if ( !strncmp( avgType, "avg", 2 ) &&
	        !strncmp( dirType, "all", 2 ) )
	isoType = avg_all ;
      else if ( !strncmp( avgType, "min", 2 ) &&
	        !strncmp( dirType, "dir", 2 ) )
	isoType = minmax_dir ;
      else if ( !strncmp( avgType, "avg", 2 ) &&
	        !strncmp( dirType, "dir", 2 ) )
	isoType = avg_dir ;
      else {
        sprintf ( hip_msg, "no isoType for %s %s, isoType set to min all.",
		  dirType, avgType ) ;
        ret = hip_err ( warning, 0, hip_msg ) ;
	isoType = minmax_all ;
      }
    }

    /* Cutoff value. */
    if ( eo_buffer () )
      isoVal = 1. ;
    else
      read1double ( &isoVal ) ;

    if ( Grids.PcurrentGrid->uns.type == uns ) {
      sprintf ( hip_msg, "  no unstructured cutting yet. Sorry. Call back later." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
    else if ( Grids.PcurrentGrid->mb.type == mb )
      /* Mark a cut in a multiblock grid. */
      cut_mb_iso ( Grids.PcurrentGrid->mb.Pmb, isoVal, isoType ) ;
    }

  else {
    /* no argument. */
    printf ( hip_msg, " cut needs an argument of [distance]." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }


  flush_buffer () ;
  return ( ret ) ;
}

/*****************************************************************/
/* hip_cython */
ret_s copy_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], someString[TEXT_LEN], axis ;
  int doMap=0 ;
  double zBeg = 0., zEnd = 1. ;
  int mSlices = 1 ;
  splitType_e splitType ;

  if ( !Grids.PcurrentGrid ) { /* No grid present. */
    sprintf ( hip_msg, "There is no grid to copy." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "2uns", 2 ) ) {
    /* Copy to unstructured. */
      if ( !eo_buffer() ) {
        /* Include a vx mapping from mb index to uns no? */
        read1string ( someString ) ;
        if ( !strncmp ( someString, "map", 1 ) )
          doMap = 1 ;
      }
      else
        doMap = 0 ;


    if ( Grids.PcurrentGrid->uns.type == mb ) {
      if ( !cp_mb2uns ( doMap ) ) {
        sprintf ( hip_msg, "copy to unstructured type failed." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  }


  else if ( !strncmp ( keyword, "uns", 2 ) ) {
    /* Duplicate and possibly merge unstructured grids. */
    if ( Grids.PcurrentGrid->uns.type != uns ) {
      ret = hip_err ( warning, 0, "cp uns expects that grid to be copied is unstructured."
                " Maybe you want cp 2uns, which copies multi-block to uns?" ) ;
      return ( ret ) ;
    }
    else {
      int mZ = 1 ;
      transf_e trOp = noTr ;
      double dVal[MAX_DIM] ;

      if ( !eo_buffer() ) {
        /* How many copies? */
        read1int( &mZ ) ;
        /* Read transf. parameters. rotAngle in rad.*/
        ret = read_transf_op ( &trOp, dVal ) ;
        if ( ret.status != success )
          hip_err ( fatal, 0, "can't continue with incomplete transfer/rotation"
                    " operation in copy_menu" ) ;
      }

      if ( !ucopy_uns2uns ( mZ, trOp, dVal ) ) {
        sprintf ( hip_msg, "copy of unstructured type failed." ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }

      /* Done in cp_uns2uns. */
      //set_current_grid ( Grids.PcurrentGrid ) ;
    }
  }


  else if ( !strncmp ( keyword, "3d", 2 ) || !strncmp ( keyword, "3D", 2 ) ) {
    /* Copy 2D to 3D. Needs parameters zBeg, zEnd, mSlices. */
    if ( !eo_buffer () )
      read1double ( &zBeg ) ;
    else
      zBeg = 0. ;
    if ( !eo_buffer () )
      read1double ( &zEnd ) ;
    else
      zEnd = 1. ;
    if ( !eo_buffer () )
      read1int ( &mSlices ) ;
    else
      mSlices = 1 ;

    if ( !eo_buffer () )
      read1char ( &axis ) ;
    else
      axis = 'z' ;

    cp_uns2D_uns3D ( zBeg, zEnd, mSlices, axis ) ;
  }

  else if ( !strncmp ( keyword, "q2t", 2 ) || !strncmp ( keyword, "Q2T", 2 ) ) {
    /* Cut all quads into two triangles. */
    someString[0] = '\0' ;
    if ( !eo_buffer () )
      read1string ( someString ) ;

    if      ( !strncmp( someString, "ll2ur", 2 ) ) splitType = ll2ur ;
    else if ( !strncmp( someString, "ul2lr", 2 ) ) splitType = ul2lr ;
    else if ( !strncmp( someString, "1-3",   2 ) ) splitType = _0_2 ;
    else if ( !strncmp( someString, "2-4",   2 ) ) splitType = _1_3 ;
    else                                          splitType = maxAng ;

    if ( !split_quads ( splitType ) ) {
      sprintf ( hip_msg, "cutting quads failed." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  else if ( !strncmp ( keyword, "2tet", 2 ) ) {
    /* Cut all elems ( hexes for the time being ) into tets. */
    if ( !uns_2tet( ) ) {
      sprintf ( hip_msg, "cutting to tets failed." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  else if ( !strncmp ( keyword, "x-zy", 4 ) ) {
    rot_3d ( keyword ) ;
  }
  else {
    /* no argument. */
    sprintf ( hip_msg, " copy needs an argument of [unstructured,3D]." ) ;
    ret = hip_err ( fatal, 0, hip_msg ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}

/*****************************************************************/

/* hip_cython */
ret_s attach_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  ret = add_uns_grids ( line ) ;

  flush_buffer () ;
  return ( ret ) ;
}

/*****************************************************************/

/* hip_cython */
ret_s check_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], argLine[LINE_LEN] ;
  size_t keyLen ;

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !Grids.PcurrentGrid ) {
    sprintf ( hip_msg, "There is no grid to check." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }
  else if ( !strncmp ( keyword, "grid", 2 ) || keyword[0] == '\0' )
    check_grid ( Grids.PcurrentGrid ) ;

  else if ( !strncmp ( keyword, "bnd", 2 ) ) {
    if ( Grids.PcurrentGrid->uns.type == uns )
      check_bnd_setup ( Grids.PcurrentGrid->uns.pUns ) ;
    else
      ret = hip_err ( warning, 1, "boundary check works only with unstructured grids." ) ;
  }
  else if ( !strncmp ( keyword, "per", 2 ) ) {
    if ( Grids.PcurrentGrid->uns.type == uns )
      special_verts ( Grids.PcurrentGrid->uns.pUns ) ;
    else
      ret = hip_err ( warning, 1, "periodic check works only with unstructured grids." ) ;
  }
  else if  ( !strncmp ( keyword, "quality", 2 ) ) {
    if ( Grids.PcurrentGrid->uns.type == uns ) {
      strcat ( keyword, " " ) ;
      keyLen = strlen ( keyword ) ;
      strncpy ( argLine, keyword, keyLen+1 ) ;
      /* Stick the hdf keyword at the beginning of the argLine, similar to getopt. */
      if ( !eo_buffer () )
        read1line ( argLine+keyLen ) ;

      ret = calc_elem_qual_stats ( argLine ) ;
    }
    else
      ret = hip_err ( warning, 1, "periodic check works only with unstructured grids." ) ;

  }
  else
    ret = hip_err ( warning, 1, "unknown check option." ) ;
  flush_buffer () ;
  return ( ret ) ;
}

/* hip_cython */
ret_s vis_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], argLine[LINE_LEN] ;
  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !Grids.PcurrentGrid ) {
    sprintf ( hip_msg, "There is no grid to visualise bits from." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }
  else if ( Grids.PcurrentGrid->uns.type != uns ) {
    sprintf ( hip_msg, "grid to be part-visualised  must be unstructured." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }


  else if ( !strncmp ( keyword, "elems", 2 ) || keyword[0] == '\0' ){
    /* a property, the some args. */
    sprintf ( argLine, "%s", keyword ) ;
    /* Stick the vis keyword at the beginning of the argLine, similar to getopt. */
    read1line ( strchr( argLine, '\0') ) ;

    vis_elems ( argLine ) ;
  }

  else {
    hip_err ( warning, 1, "unknown keyword in vis menu, ignored." ) ;
  }
  flush_buffer () ;
  return ( ret ) ;
}


/*****************************************************************/
#ifdef ADAPT_HIERARCHIC


/* hip_cython */
ret_s adapt_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], fileType[LINE_LEN], fileName[LINE_LEN] ;
  grid_struct *Pgrid ;
  double deref = .1, ref = .3, llEnd[MAX_DIM], urEnd[MAX_DIM], radius ;
  int nCoor, mDim, iso = 1 ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to adapt." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }
  else {
    Pgrid = Grids.PcurrentGrid ;
    mDim = Pgrid->uns.mDim ;
  }

  if ( Pgrid->uns.type != uns ) {
    sprintf ( hip_msg, "grid to be adapted must be unstructured." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "reset", 2 ) )
    adapt_reset ( Pgrid->uns.pUns ) ;

  else if ( !strncmp ( keyword, "nr", 2 ) )
    adapt_uns_hierarchical_nr ( Pgrid->uns.pUns, "\0", "\0" ) ;

  else if ( !strncmp ( keyword, "file", 2 ) ) {

    strncpy ( fileType, "asc", strlen( "asc")+1 ) ;
    if ( !eo_buffer () )
      read1lostring ( fileType ) ;
    strncpy ( fileName, "ref.txt", strlen( "ref.txt")+1 ) ;
    if ( !eo_buffer () )
      read1string ( fileName ) ;

    adapt_uns_hierarchical_nr ( Pgrid->uns.pUns, fileType, fileName ) ;
  }

  else if ( !strncmp ( keyword, "hrb-reset", 6 ) ) {
    /* Reset all boxes. */
    Grids.hrbs.mHrbs = 0 ;
    arr_free ( Grids.hrbs.pHrb ) ;
  }

  else if ( !strncmp ( keyword, "hrb-add", 6 ) ) {
    /* Add a box. Read coordinates for beginning and end and a radius. */
    for ( nCoor = 0 ; nCoor < mDim ; nCoor++ )
      read1double ( llEnd+nCoor ) ;
    for ( nCoor = 0 ; nCoor < mDim ; nCoor++ )
      read1double ( urEnd+nCoor ) ;
    read1double ( &radius ) ;

    add_hrb ( &(Grids.hrbs), llEnd, urEnd, radius, mDim ) ;
  }

  else if ( !strncmp ( keyword, "vf", 2) ) {
    /* Residual error, adjoint weighting. */
    int abs = 1, mSmooth = 0, mSmoothShock = 0 ;

    if ( !eo_buffer () ) read1string ( fileName ) ;
    if ( !eo_buffer () ) read1double ( &deref ) ;
    if ( !eo_buffer () ) read1double ( &ref ) ;
    if ( !eo_buffer () ) read1int    ( &iso ) ;
    if ( !eo_buffer () ) read1int    ( &mSmooth ) ;
    if ( !eo_buffer () ) read1int    ( &mSmoothShock ) ;
    if ( !eo_buffer () ) read1int    ( &abs ) ;

    adapt_vf ( fileName, deref, ref, iso, mSmooth, mSmoothShock, abs ) ;
  }

  else {
    /* Get refinement and derefinement thresholds. */
    if ( !eo_buffer () ) read1double ( &deref ) ;
    if ( !eo_buffer () ) read1double ( &ref ) ;
    if ( !eo_buffer () ) read1int    ( &iso ) ;

    reserve_elem_mark ( Pgrid->uns.pUns, SZ_ELEM_MARK-1, "mark_elems_in_hrb" ) ;
    mark_elems_in_hrb ( Pgrid->uns.pUns, &Grids.hrbs ) ;
    adapt_hierarchical_sensor ( keyword, deref, ref, iso ) ;
    release_elem_mark ( Pgrid->uns.pUns, SZ_ELEM_MARK-1 ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}

/*****************************************************************/

/* hip_cython */
ret_s buffer_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN] ;
  grid_struct *Pgrid ;

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to [de]buffer." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }
  else {
    Pgrid = Grids.PcurrentGrid ;

    if ( Pgrid->uns.type != uns ) {
      sprintf ( hip_msg, "grid to be buffered must be unstructured." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }

    else if ( !strncmp ( keyword, "un", 2 ) )
      debuffer_uns ( Pgrid->uns.pUns ) ;

    else
      buffer_uns ( Pgrid->uns.pUns ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}
#endif
/*****************************************************************/

/* hip_cython */
ret_s mg_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN] ;
  int keyLen ;
  char argLine[LINE_LEN] ;

  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to write." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }

  /* Stick the mg keyword at the beginning of the argLine, similar to getopt. */
  sprintf ( argLine, "mg " ) ;
  keyLen = strlen ( argLine ) ;
  if ( !eo_buffer () ) {
    read1line ( argLine+keyLen ) ;
  }

  if ( uns_mg ( argLine ) )
    ret = hip_err ( fatal, 0, "failed to apply multigrid method." ) ;


  flush_buffer () ;
  return ( ret ) ;
}


/*****************************************************************/
/* hip_cython */
ret_s mm_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  char argline[LINE_LEN] ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], argLine[LINE_LEN] ;


  if ( eo_buffer () )
    argline[0] = '\0' ;
  else {
    read1line ( argLine ) ;
    flush_buffer () ;
  }

#ifdef MMG
  adapt_mmg ( Grids.PcurrentGrid, argLine ) ;
#else
  ret = hip_err ( fatal, 0, "mmg requested, but not compiled." ) ;
#endif
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mp_menu:
*/
/*! mixing plane menu.
 *
 */

/*

  Last update:
  ------------
  23Sep19: conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

/* hip_cython */
ret_s mp_menu (  char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], argLine[LINE_LEN] ;


  if ( eo_buffer () )
    argLine[0] = '\0' ;
  else {
    read1line ( argLine ) ;
    flush_buffer () ;
  }

  if ( !strncmp ( argLine, "type", 2 ) ) {
  }
  else if ( !strncmp ( argLine, "type", 2 ) ) {
  }
  else if ( !strncmp ( argLine, "type", 2 ) ) {
  }
  else {
    hip_err ( warning, 1, "unrecognised mp option" ) ;
  }


  return ( ret ) ;
}


/******************************************************************************

  read_menu:
  .

  Last update:
  ------------
  8Sep18; new arg list to transform.
  19Dec17; new interface for transform.
  21Feb17; remove warnings for obsolete adf and oxd.
  Oct07; intro solution for read_fluent
  15Jul96: conceived.

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

/* hip_cython */
ret_s read_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], fileType[LINE_LEN], file1in[LINE_LEN], file2in[LINE_LEN],
    file3in[LINE_LEN], file2type, newPrompt[TEXT_LEN] ;
  char argLine[LINE_LEN] ;
  int skip, allRead = 0, someInt ;
  int withBlanking ;


  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "scri", 2 ) ) {
    /* Script. */
    read1string ( file1in ) ;
    r1_switch_input_file ( file1in ) ;
  }

  else if ( !strncmp ( keyword, "bae", 2 ) ) {
    /* 3 files for the flite format. */
    read1string ( file1in ) ;
    read1string ( file2in ) ;
    read1string ( file3in ) ;

    allRead = read_uns_flite ( file1in, file2in, file3in ) ;
  }


  else if ( !strncmp ( keyword, "cdre", 2 ) ) {
    /* Unstructured cedre */
    read1string ( file1in ) ;

    allRead = read_uns_cedre ( file1in ) ;
  }


  else if ( !strncmp ( keyword, "centaur", 2 ) ) {
    /* Mixed element Centaur. */
    read1string ( file1in ) ;
    allRead = read_uns_centaur ( file1in ) ;
  }

  else if ( !strncmp ( keyword, "cgns", 2 ) ) {
#ifdef CGNS
    /* Unstructured CGNS. */
    sprintf ( argLine, "%s", keyword ) ;
    read1line ( strchr( argLine, '\0') ) ;

    //read1string ( file1in ) ;
    //file2in[0] = file3in[0] = '\0' ;
    //if ( !eo_buffer () )read1string ( file2in ) ;

    ret = read_uns_cgns ( argLine ) ;
    allRead = ( ret.status == success ? 1 : 0 ) ;
#else
    /* Write an unstructured grid to cgns formats. */
    ret = hip_err ( fatal, 0, "compile with -DCGNS for CGNS support." ) ;
#endif
  }


  /*
    else if ( !strncmp ( keyword, "cfdrc-mfg", 2 ) ) {
    read1string ( file1in ) ;

    flip_negative_quads = 0 ;
    if ( !eo_buffer () ) {
    read1string ( keyword ) ;
    if ( !strncmp ( keyword, "flip", 2 ) )
    flip_negative_quads = 1 ;
    }
    allRead = read_uns_cfdrc ( file1in, flip_negative_quads ) ;
    }
  */

  else if ( !strncmp ( keyword, "cstruct", 2 ) ||
	    !strncmp ( keyword, "dlr-flower", 2 )) {
    /* Structured grids with coordinate and topology file. */
    read1string ( file1in ) ;
    read1string ( file2in ) ;

    if ( !eo_buffer () ) {
      /* Optional type of the coor file, defaults to ascii. */
      read1lostring ( fileType ) ;
      file2type = fileType[0] ; }
    else
      file2type = 'a' ;

    if ( !eo_buffer () )
      /* Optional skip. */
      read1int ( &skip ) ;
    else
      skip = 1 ;

    if ( !eo_buffer () )
      /* Optional blankding info. */
      read1int ( &withBlanking ) ;
    else
      withBlanking = 0 ;

    /* Go, do it. */
    if ( !strncmp ( keyword, "cstruct", 2 ) )
      allRead = read_mb_cfdrc ( file1in, file2in, file2type, skip, withBlanking ) ;
    else if ( !strncmp ( keyword, "dlr-flower", 2 ) )
      //allRead = read_mb_flower ( file1in, file2in, file2type, skip ) ;
      hip_err ( warning, 1, "DLR flower input is no longer supported." ) ;
  }


  /*   else if ( !strncmp ( keyword, "damas", 2 ) ) { */
  /*     /\* Old AS-style multi-block. *\/ */
  /*     read1string ( file1in ) ; */

  /*     if ( !eo_buffer () ) */
  /*       /\* Optional skip. *\/ */
  /*       read1int ( &skip ) ; */
  /*     else { */
  /*       skip = 1 ; */

  /*     /\* Go, do it. *\/ */
  /* #   ifdef DAMAS */
  /*       allRead = read_mb_damas ( file1in, skip ) ; */
  /* #   else */
  /*       sprintf ( hip_msg, " damas currently disabled." ) ; */
  /*       ret = hip_err ( warning, 0, hip_msg ) ; */
  /* #   endif */
  /*     } */
  /*   } */


  else if ( !strncmp ( keyword, "ensight", 2 ) ) {
    /* Read ensight format's case file name. */
    read1line ( strchr( keyword, '\0') ) ;
    sprintf ( argLine, "%s", keyword ) ;

    if ( read_ensight ( argLine ) != EXIT_SUCCESS ) {
      sprintf ( hip_msg, "failed to  readthis grid from ensight." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }


  else if ( !strncmp ( keyword, "gmsh", 2 ) ) {

    sprintf ( argLine, "%s", keyword ) ;
    read1line ( strchr( argLine, '\0') ) ;

    allRead =0  ;
    if ( (read_gmsh ( argLine ).status == success ) )
      allRead = 1 ;
  }


  else if ( !strncmp ( keyword, "hdf5", 2 ) ) {
    /* hdf5, 2 filenames. */
    sprintf ( argLine, "%s", keyword ) ;
    /* Stick the hdf keyword at the beginning of the argLine, similar to getopt. */
    read1line ( strchr( argLine, '\0') ) ;

    allRead = read_hdf5 ( argLine ) ;
  }


  else if ( !strncmp ( keyword, "hydra", 2 ) ) {
    /* hdf5, 2 filenames. */
    sprintf ( argLine, "%s", keyword ) ;
    /* Stick the hdf keyword at the beginning of the argLine, similar to getopt. */
    read1line ( strchr( argLine, '\0') ) ;

    allRead = read_hyd ( argLine ) ;
  }


  else if ( !strncmp ( keyword, "mcgns", 2 ) ) {
    int kBlock ;
    /* cgns multi-block. */
    read1string ( file1in ) ;

    if ( !eo_buffer () )
      /* Optional skip. */
      read1string ( file2in ) ;
    else {
      file2in[0] = '\0' ;
    }
    if ( file2in[0] == ' ' )
      file2in[0] = '\0' ;

    if ( !eo_buffer () )
      /* Optional skip. */
      read1int ( &skip ) ;
    else {
      skip = 1 ;
    }
    if ( !eo_buffer () )
      /* Optional skip. */
      read1int ( &kBlock ) ;
    else {
      kBlock = 0 ;
    }

    allRead = read_mb_cgns ( file1in, file2in, skip, kBlock ) ;
  }


  else if ( !strncmp ( keyword, "n3s", 2 ) ) {
    /* Mixed element Centaur. */
    read1string ( file1in ) ;
    if ( !eo_buffer () )
      read1string ( file2in ) ;
    else
      file2in[0] = '\0' ;
    allRead = read_uns_n3s ( file1in, file2in, keyword ) ;
  }

  else if ( !strncmp ( keyword, "saturne", 2 ) ) {
    /* Saturne. Needs two filenames and a save number. */
    read1string ( file1in ) ;
    if ( !eo_buffer () )
      read1string ( file2in ) ;
    else
      file2in[0] = '\0' ;

    if ( !eo_buffer () )
      read1int ( &someInt ) ;
    else
      someInt = 1 ;
    allRead = read_uns_saturne ( file1in, file2in, someInt ) ;
  }


  else if ( !strncmp ( keyword, "fluent", 2 ) ) {
    /* Read fluent. */
    read1string ( file1in ) ;
    if ( !eo_buffer () )
      read1string ( file2in ) ;
    else
      file2in[0] = '\0' ;
    allRead = read_fluent ( keyword, file1in, file2in ) ;
  }


  else if ( !strncmp ( keyword, "\0", 1 ) ) {
    printf ( hip_msg,
             " read needs an argument of [script, avbp, "
	     "           avbp4, cfdrc-mfg, cstruct, dpl]." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
  }
  else {
    /* All remaining formats need one filename. */
    read1string ( file1in ) ;

    file2in[0] = '\0' ;
    if ( !eo_buffer () )
      read1string ( file2in ) ;


    if ( !strncmp ( keyword, "dpl", 2 ) )
      allRead = read_dpl ( file1in, file2in ) ;

    else if ( !strncmp ( keyword, "avbp4", 5 ) )
      /* Read a avbp 4.0. */
      allRead = read_uns_avbp4 ( file1in ) ;
    else if ( !strncmp ( keyword, "avh", 3 ) )
      /* Read a avbp 5.X with an hdf solution file. */
      allRead = read_uns_avbp ( file1in, 1 ) ;
    else if ( !strncmp ( keyword, "avbp", 2 ) )
      /* Read a avbp 5.X with avbp sol file. */
      allRead = read_uns_avbp ( file1in, 0 ) ;
    else {
      sprintf ( hip_msg, "unknown file-type %s, nothing read.", keyword ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }

  if ( allRead && Grids.PcurrentGrid ) {
    // JDM, Nov 17: include set_current_grid in all read/ge/tr routines.
    set_prompt ( Grids.PcurrentGrid ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}



/*****************************************************************/

/* hip_cython */
ret_s transform_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  double dval[MAX_DIM] ;
  transf_e tr_op = noTr ;


  read_transf_op ( &tr_op, dval ) ;


  if ( tr_op )
    transform (  Grids.PcurrentGrid, tr_op, dval, 0, 1 ) ;


  flush_buffer () ;
  return ( ret ) ;
}


/******************************************************************************

  write_menu:
  .

  Last update:
  ------------
  29Jun14; add header
           fix pb with read ensight w/o arguments.
  15Jul96: conceived.

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

/* hip_cython */
ret_s write_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char rootFile[LINE_LEN], file1in[LINE_LEN], file2in[LINE_LEN] ;
  char keyword[LINE_LEN], argLine[LINE_LEN] ;
  size_t keyLen ;


  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to write." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "scre", 2 ) ) {
    /* Write to screen. */
    write_screen ( ) ;
  }

  else if ( !strncmp ( keyword, "avbp", 2 ) ) {
    /* Write to avbp formats. */
    int level = 0 ;

    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "grid\0" ) ;
    else {
      read1string ( rootFile ) ;
      if ( !eo_buffer () )
        read1int ( &level ) ;
    }

    if ( !write_av ( rootFile, keyword, level ) ) {
      sprintf ( hip_msg, "failed to write this grid to avbp." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }


  else if ( !strncmp ( keyword, "cgns", 2 ) ) {
#ifdef CGNS
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "grid\0" ) ;
    else {
      read1string ( rootFile ) ;
    }

    /* Write an unstructured grid to cgns formats. */
    write_uns_cgns ( rootFile ) ;
#else
    /* Write an unstructured grid to cgns formats. */
    ret = hip_err ( fatal, 0, "compile with -DCGNS for CGNS support." ) ;
#endif
  }

  else if ( !strncmp ( keyword, "dpl", 2 ) ) {
    /* Write to dpl format. */
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "grid.dpl\0" ) ;
    else
      read1string ( rootFile ) ;

    if ( !write_dpl ( rootFile, keyword ) ) {
      sprintf ( hip_msg, "failed to write this grid to dpl." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
    }
  }



  else if ( !strncmp ( keyword, "ensight", 2 ) ) {
    /* Write to ensight format. */
    strcat ( keyword, " " ) ;
    keyLen = strlen ( keyword ) ;
    strncpy ( argLine, keyword, keyLen+1 ) ;
    /* Stick the keyword at the beginning of the argLine, similar to getopt. */
    if ( !eo_buffer () )
      read1line ( argLine+keyLen ) ;

    if ( !write_ensight ( argLine ) ) {
      sprintf ( hip_msg, "failed to  write this grid to ensight." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }


  else if ( !strncmp ( keyword, "fieldview", 2 ) ) {
    /* Write to fieldview format. */
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "fieldview.fv\0" ) ;
    else
      read1string ( rootFile ) ;

    if ( !write_fieldview ( rootFile ) ) {
      sprintf ( hip_msg, "failed to  write this grid to fieldview." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }



  else if ( !strncmp ( keyword, "gmsh", 2 ) ) {
    /* Write to gmsh format. */
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "grid.msh\0" ) ;
    else
      read1string ( rootFile ) ;

    if ( !write_gmsh ( rootFile, 1 ) )
      ret = hip_err ( fatal, 0, " failed to  write this grid to gmsh." ) ;
  }



  else if ( !strncmp ( keyword, "hdf5", 2 ) ) {
    /* Write to hdf5 formats. */
    strcat ( keyword, " " ) ;
    keyLen = strlen ( keyword ) ;
    strncpy ( argLine, keyword, keyLen+1 ) ;
    /* Stick the hdf keyword at the beginning of the argLine, similar to getopt. */
    if ( !eo_buffer () )
      read1line ( argLine+keyLen ) ;


    if ( !write_hdf5 ( argLine ) )
      ret = hip_err ( fatal, 0, "failed to write this grid to hdf5." ) ;
  }



  else if ( !strncmp ( keyword, "n3s", 2 ) ) {
    /* Write to n3s format. */
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( file1in, "n3s.geom\0" ) ;
    else
      read1string ( file1in ) ;

    if ( !eo_buffer () )
      read1string ( file2in ) ;
    else
      strcpy ( file2in, "\0" ) ;


    if ( !write_n3s ( file1in, file2in ) ) {
      sprintf ( hip_msg, "failed to write this grid to n3s." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  else if ( !strncmp ( keyword, "mhdf", 2 ) ) {
    /* mb to uns vx map in hdf. */
     if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( file1in, "mb2uns_map.h5\0" ) ;
    else
      read1string ( file1in ) ;

     if ( write_mbMap_hdf5 ( file1in ) )
       ret = hip_err ( warning, 1, "failed to write mb to uns vx map to hdf5" ) ;

  }

  else if ( !strncmp ( keyword, "pts", 2 ) ) {
    /* Write cut. */
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "cut.pts\0" ) ;
    else
      read1string ( rootFile ) ;

    if ( eo_buffer () )
      /* By default, write the kept part. */
      strcpy ( keyword, "keep\0" ) ;
    else
      read1string ( keyword ) ;

    if ( !write_cut ( rootFile, keyword ) ) {
      sprintf ( hip_msg, " SORRY: can\'t write a cut of this grid." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  else if ( !strncmp ( keyword, "stl", 2 ) ) {
    /* Write surface to stl in 3d. */
    if ( eo_buffer () )
      /* Standard filename. */
      strcpy ( rootFile, "surface\0" ) ;
    else
      read1string ( rootFile ) ;

    ret = write_stl ( Grids.PcurrentGrid, rootFile ) ;
    if ( ret_is_failure (ret) ) {
      sprintf ( hip_msg, " SORRY: can\'t write an STL of this grid." ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  else {
    /* no argument. */
    sprintf ( hip_msg,
              "write needs an argument of [screen,adf,ox,avbp,dpl,pts]." ) ;
    ret = hip_err ( warning, 1, hip_msg ) ;
  }

  flush_buffer () ;
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  : conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s version_banner ( ) {
  ret_s ret = ret_success () ;

  printf (  "HIP Version : %s\n"
            "Last update : %s\n"
            "Git Id      : %s\n"
#ifdef HIP_USE_ULONG
            "Integers    : 64bit\n"
#else
            "Integers    : 32bit\n"
#endif
             "(C): Copyright: Jens-Dominik Mueller"
             " and CERFACS, Toulouse, 2002-2017\n",
            version, GIT_DATE,GIT_COMMIT);
  return ( ret ) ;
}



/******************************************************************************
/ Function added to make calls to menus from a
/ string. mainly introduced to make calls from a python
/ script
*/
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! Python interface to the main loop.
 *
 */

/*

  Last update:
  ------------
  9Sep19; use ret_s, use renamed r1_put_string.
  Aug19: conceived by Aymad.


  Input:
  ------
  string: typically one line of hip script.

  Returns:
  --------
  1 on failure, 0 on success

*/

/* hip_cython */
ret_s call_menu_from_input ( char InputLine[LINE_LEN]) {
  ret_s ret = ret_success () ;
  r1_put_string ( line ) ;

  while ( read1lostring ( line ) ) {
    if ( !r1_skip_keyword ( line, LINE_LEN,  "attach", 2 ) )
      ret = attach_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "check", 2 ) )
      ret = check_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "clicense", 2 ) )
      ret = license ( ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "copy", 2 ) )
      ret = copy_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "cut", 2 ) )
      ret = cut_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "decimate", 2 ) )
      ret = de_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "flag", 2 ) )
      ret = flag_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "generate", 2 ) )
      ret = generate_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "interpolate", 2 ) )
      ret = interpolate_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "list", 2 ) )
      ret = list_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mark", 2 ) )
      ret = mark_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mg", 2 ) )
      ret = mg_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mm", 2 ) )
      ret = mm_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "read", 2 ) )
      ret = read_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "set", 2 ) )
      ret = set_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "transform", 2 ) )
      ret = transform_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "var", 2 ) )
      ret = var_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "write", 2 ) )
      ret = write_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "zone", 2 ) )
      ret = zone_menu ( line ) ;
  }
  return (ret);
}



int hip_version () {
  /* Bulid version string. */
  sprintf(version, "%d.%02d.%d %s",hipversion[0],hipversion[1],hipversion[2],hipname);

  /* Disable standard buffering */
  setbuf(stdout, NULL);

  return ( 0 ) ;
}

int hip_banner () {

  if ( verbosity > 0 ) {
    printf ( " Welcome to Domino Software.\n"
             "   This is hip, version %s, %s,"
#ifdef HIP_USE_ULONG
             " compiled for 64bit integers.\n"
#else
             " compiled for 32bit integers.\n"
#endif
             "   (C): Copyright: Jens-Dominik Mueller"
             " and CERFACS, Toulouse, 2002-2024\n\n",
             version, GIT_DATE ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  : conceived.


  Input:
  ------
  argc: # number of commandline args
  argv: list of command line args

  Returns:
  --------
  0 on success

*/

int hip_args ( int argc, char *argv[]  ) {
  ret_s ret = ret_success () ;

  if ( argc > 1 )
    /* A command-line argument for a script file is given. */
    r1_switch_input_file ( argv[1] ) ;

  if ( argc > 2 )
    /* A command-line argument for a log file is given. */
    if ( !( OutFile = r1_switch_output_file ( argv[2] ) ) ) {
      sprintf ( hip_msg, "log file named:%s could not be opened.", argv[1] ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ; }

  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  hip_menu:
*/
/*! interactive command line/script menu
 *
 *
 */

/*

  Last update:
  ------------
  5Sep19: extract from main.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

/* hip_cython */
ret_s hip_menu ( char inLine[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( inLine && inLine[0] != '\0' )
    r1_put_string ( inLine ) ;

  while ( read1lostring ( line ) )  {
    if ( !r1_skip_keyword ( line, LINE_LEN,  "help", 2 ) )
      ret = help_menu ( line ) ;

    else if ( !r1_skip_keyword ( line, LINE_LEN,  "adapt", 2 ) ) {
#ifdef ADAPT_HIERARCHIC
      ret = adapt_menu ( line ) ;
#else
      hip_err ( warning, 1, "hierarchic adaptation not compiled in,"
                " set ADAPT_HIER=1 in Makefile." ) ;
#endif
    }
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "attach", 2 ) )
      ret = attach_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "bc", 2 ) )
      ret = bc_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "buffer", 2 ) ) {
#ifdef ADAPT_HIERARCHIC
      ret = buffer_menu ( line ) ;
#else
      hip_err ( warning, 1, "hierarchic adaptation not compiled in,"
                " set ADAPT_HIER=1 in Makefile." ) ;
#endif
    }
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "check", 2 ) )
      ret = check_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "clicense", 2 ) )
      ret = license ( ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "copy", 2 ) )
      ret = copy_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "cut", 2 ) )
      ret = cut_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "decimate", 2 ) )
      ret = de_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "flag", 2 ) )
      ret = flag_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "generate", 2 ) )
      ret = generate_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "interpolate", 2 ) )
      ret = interpolate_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "iface", 2 ) )
      ret = interface_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "list", 2 ) )
      ret = list_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mark", 2 ) )
      ret = mark_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mg", 2 ) )
      ret = mg_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mm", 2 ) )
      ret = mm_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "mem", 2 ) )
      ret = memory();
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "read", 2 ) )
      ret = read_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "set", 2 ) )
      ret = set_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "transform", 2 ) )
      ret = transform_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "var", 2 ) )
      ret = var_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "vis", 2 ) )
      ret = vis_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "write", 2 ) )
      ret = write_menu ( line ) ;
    else if ( !r1_skip_keyword ( line, LINE_LEN,  "zone", 2 ) )
      ret = zone_menu ( line ) ;

    /* other stuff. */
    else if ( !strncmp ( line, "echo", 2 ) ) {
      read1line ( line ) ;
      flush_buffer () ;
      ret = hip_err ( blank,1, line ) ;
    }
    else if ( !strncmp ( line, "time", 2 ) && verbosity > 0 ) {
      nowTime = times ( &timings ) ;
      sprintf ( hip_msg, "used %g user, %g system, %g sec elapsed time.\n",
               1.*timings.tms_utime/clk_ticks, 1.*timings.tms_stime/clk_ticks,
               (nowTime-iniTime)*1./clk_ticks ) ;
      ret = hip_err ( info, 1, hip_msg ) ;
    }
    else if ( !strncmp ( line, "\0", 1 ) || !strncmp ( line, "\n", 1 ) ||
	      !strncmp ( line, "%", 1 ) || !strncmp ( line, "/*", 2 ) ||
	      !strncmp ( line, "//", 2 ) || !strncmp ( line, "#", 1 ) )
      /* Ignore this line. */
      flush_buffer () ;
    else if ( !strncmp ( line, "quit", 2 ) ){
      ret = ret_success () ;
      return ( ret ) ;
      }
    else if ( !strncmp ( line, "exit", 2 ) ) {
      ret = ret_success () ;
      return ( ret ) ;
      }
    else if ( !strncmp ( line, "bye", 2) && verbosity > 0 ) {
      return  ( bye () ) ;
    }
    else {
      sprintf ( hip_msg, "unknown command: %s\n", line ) ;
      ret = hip_err ( warning, 1, hip_msg ) ;
      flush_buffer () ;
    }
  }
  return ( ret ) ;
}



/******************************************************************************

  main:
  Toplevel.

  Last update:
  ------------
  5Sep19; refactored.
  11Oct07: intro init ;


*/



int hip_main( int argc, char *argv[] ) {

  hip_init () ;
  hip_args ( argc, argv ) ;
  hip_version () ;

  if ( argc > 1 && ( !strcmp( argv[1], "-v" ) ||  !strcmp( argv[1], "--version" ) ) ) {
    /* You say hello, and I say goodbye ... */
    version_banner() ;
    exit ( EXIT_SUCCESS ) ;
  }

  hip_banner () ;

  ret_s ret = hip_menu ( NULL ) ;

  if ( ret.status == success )
    hip_err ( blank, 5, "successful exit" ) ;

  return ( ret.status ) ;
}


int main( int argc, char *argv[] ) {

  /* Test the screen/string output */

  /* Test basic listing/printing. */
  /*
  ret_s ret ;
  hip_init () ;
  hip_output = string ; // no screen output, other than pulled out with puts ().

  //hip_menu ( "he" ) ;

  ret = hip_menu ( "ge 0 0 1 1 3 3 " ) ;
  puts ( hip_out_buf ) ;
  flush_hip_out_buf () ;

  ret = hip_menu ( "wr hd blah" ) ;
  puts ( hip_out_buf ) ;
  flush_hip_out_buf () ;

  ret = hip_menu ( "ex" ) ;
  puts ( hip_out_buf ) ;
  flush_hip_out_buf () ;

  return ( 0 ) ;

  double ll[2] = {0,0}, ur[2]={1,1} ;
  ret = uns_generate ( ll, ur, 3, 3 ) ;
  puts ( hip_out_buf ) ; // get the buffer content
  flush_hip_out_buf () ; // reset

  hprintf( " test of hprintf, %s with num arg=%d\n", "second line", 5 ) ;

  ret = set_bc_text ( "1", "gr1_bc1" ) ;
  ret = set_bc_text ( "2", "gr1_bc2" ) ;
  ret = set_bc_text ( "3", "gr1_bc3" ) ;
  ret = set_bc_text ( "4", "gr1_bc4" ) ;
  puts ( hip_out_buf ) ;
  flush_hip_out_buf () ;

  double llb[2] = {1,1}, urb[2]={2,2} ;
  ret = uns_generate ( ll, ur, 3, 3 ) ;
  puts ( hip_out_buf ) ;
  flush_hip_out_buf () ;

  ret = list_grids () ;
  ret = list_surfaces ( "\0" ) ;
  ret = list_surfaces ( "all" ) ;
  puts ( hip_out_buf ) ;
  flush_hip_out_buf () ;
  flush_hip_out_buf () ;


  return (0);
  */

  return ( hip_main ( argc, argv ) ) ;
}
