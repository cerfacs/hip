/*
   write_uns_avbp_bound.c
   Write an unstructured, multichunked grid as single block into an
   avbp format 4.2. This file collects the boundary files.
 
   Last update:
   ------------
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   18Dec10; new pBc->type
   20Dec01; intro 4.7 asciiBound.
   14Dec01; fix bug in record length of face normals, write face->vertex pointers
   10Aug01; add writing of boundary face normals.
   10Dec97; intro perBc_in_exBound.
   22Nov96; cut out boundary routines.
   4May96; conceived.

   Contains:
   ---------
   write_uns_bound:
   write_uns_int_bound:
   write_uns_bound_cond:
   
*/

#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const char version[] ;
extern char hip_msg[] ;
extern const int verbosity ;
extern const int perBc_in_exBound ;
extern const elemType_struct elemType[] ;

/****************************************************************

  write_avbp_exBound:
  Write the external boundary faces of a chunked unstructured mesh to AVBP.
  Note that this requires that the cells are numbered in the same sequence
  that they are written.

  Last update:
  ------------
  15Dec13; replace x_axis_verts with axis_verts.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  17May06; list number of m-p vx also in first header line.
  15May06; write a list of bnd vx on multiple patches.
  14Dec01; fix bug in record length of face normals, write face->vertex pointers
  10Aug01; add writing of boundary face normals.
  14Jan01; use dontPer in call to mark_uns_vertBc.
  5May96: conceived..

  Input:
  ------
  pUns:
  PboundFile:
 
  Changes to:
  -----------
  *PboundFile:

*/

int write_avbp_exBound ( uns_s *pUns, char *PboundFile ) {

  const int *kVxFc ;
  const faceOfElem_struct *pFoE ;

  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  FILE *boundFile ;
  int buffer[8], nBc ; 
  ulong_t mVertNdx, mFaceNdx, mVxAllBc, mBi, mTri, mQuad, 
    mBndFacesWritten, mVxAxi, mVxFc, mVxFcWr, mVxMP, mEntries ;
  int nBeg, nEnd ;
  elem_struct *pElem ;
  vrtx_struct **ppVx ;
  bndFc_struct *PbndFc, *pBndFcBeg, *pBndFcEnd ;
  bndPatch_struct *PbndPatch ;
  bndVxWt_s *pBWt ;
  mp_bndVx_s mpVx ;
  
  if ( verbosity > 2 )
    printf ( "      boundary data to %s\n", PboundFile ) ;

  if ( ( boundFile = fopen ( PboundFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PboundFile ) ;
    return ( 0 ) ; }




  /* Boundary nodes, weights. */
  pBWt = arr_malloc ( "pBWt in write_avbp_bound", pUns->pFam,
                      pUns->mBc, sizeof( bndVxWt_s ) ) ;

  /* List all bnd vx that are marked with pVx->mark. The flag
     is set by mark_uns_vertBc depending on the value of singleNormal. 

     As of 18Dec06, make_bndVxWts allows to switch off normal calc.
     To verify whether we need a physical record in the .exBound file. */
  make_bndVxWts ( pUns, bnd, pBWt, &mVxAllBc, 0, 1 ) ;

  /* Make a list of multi-patch bnd vx. */
  make_mp_bndVx ( pUns, &mpVx ) ;


  
  buffer[0] = 5*sizeof( int ) ;
  /* Number of blocks. */
  buffer[1] = 1 ;
  /* Number of all patches. */
  buffer[2] = pUns->mBc ;
  /* Number of all boundary vertices/elements over all patches. */
  buffer[3] = pUns->mVertAllBc ;
  buffer[4] = pUns->mFaceAllBc ;
  buffer[5] = mpVx.mVxMP ;
  buffer[6] = buffer[0] ;
  FWRITE ( buffer, sizeof( int ), 7, boundFile ) ;

  /* Number of boundary subfaces(windows),
     boundary vertices, faces(cells) in this block. */
  buffer[0] = 5*sizeof( int ) ;
  buffer[1] = pUns->mBc ;
  buffer[2] = 1 ;
  buffer[3] = pUns->mVertAllBc ;
  buffer[4] = 1 ;
  buffer[5] = pUns->mFaceAllBc ;
  buffer[6] = buffer[0] ;
  FWRITE ( buffer, sizeof( int ), 7, boundFile ) ;




  /* Indices in the list of boundary vertices. */
  buffer[0] = 2*pUns->mBc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  for ( mVertNdx = nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    buffer[0] = mVertNdx+1 ;
    buffer[1] = pUns->pmVertBc[nBc] ;
    mVertNdx += pUns->pmVertBc[nBc] ;
    FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
  }
  /* Trailing length of the record. */
  buffer[0] = 2*pUns->mBc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;


  
  /* Opening length of the record of boundary nodes. */
  buffer[0] = mVxAllBc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
  FWRITE ( pBWt->pnVx, sizeof( int ), mVxAllBc, boundFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  /* Boundary normals. */
  buffer[0] = pUns->mDim * mVxAllBc * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
  FWRITE ( pBWt->pWt, sizeof ( double ), pUns->mDim*mVxAllBc, boundFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  
  arr_free ( pBWt->pWt ) ;
  arr_free ( pBWt->pnVx ) ;
  arr_free ( pBWt->pVx ) ;
  arr_free ( pBWt ) ;
  







  /* Index of all boundary faces. */
  /* Length of the record: 4*number of all boundary subFaces. */
  buffer[0] = 4*pUns->mBc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  /* Index of the list of boundary faces. */
  for ( mFaceNdx = 0, nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    if ( pUns->mDim == 2 ) {
      /* List 2-D faces as quads, leave the triangles void. */
      buffer[0] = mFaceNdx+1 ;
      buffer[1] = 0 ;
      buffer[2] = mFaceNdx+1 ;
      mFaceNdx += pUns->pmBiBc[nBc] ;
      buffer[3] = pUns->pmBiBc[nBc] ;
    }
    else {
      /* 3-D. */
      buffer[0] = mFaceNdx+1 ;
      buffer[1] = pUns->pmTriBc[nBc] ;
      mFaceNdx += pUns->pmTriBc[nBc] ;
      buffer[2] = mFaceNdx+1 ;
      buffer[3] = pUns->pmQuadBc[nBc] ;
      mFaceNdx += pUns->pmQuadBc[nBc] ;
    }
    FWRITE ( buffer, sizeof( int ), 4, boundFile ) ;
  }
  /* Trailing length of the record. */
  buffer[0] = 4*pUns->mBc*sizeof( int ) ;
  
  /* List all boundary faces. */
  /* Opening length of the record of boundary faces. */
  buffer[1] = 2*pUns->mFaceAllBc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;

  /* Loop over all boundaries. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] ) {
      PbndPatch = NULL ;
      mBndFacesWritten = 0 ;
      
      while ( loop_bndFaces_bc ( pUns, nBc, &PbndPatch, &pBndFcBeg, &pBndFcEnd ) )
        if ( pUns->mDim == 2 ) {
          /* In 2-D all faces of two nodes are listed as quads. */
          for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
            if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
              /* List the internal element and the number of this face. */
              mBndFacesWritten++ ;
              buffer[0] = PbndFc->Pelem->number ;
              buffer[1] = PbndFc->nFace ;
              FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
            }
        }
        else {
          /* 3D. List triangles and quads seperately. First triangles. */
          if ( pUns->pmTriBc[nBc] )
            for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
              if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
                pFoE = elemType[PbndFc->Pelem->elType].faceOfElem + PbndFc->nFace ;
                if ( pFoE->mVertsFace == 3 ) {
                  /* List the internal element and the number of this face. */
                  mBndFacesWritten++ ;
                  buffer[0] = PbndFc->Pelem->number ;
                  buffer[1] = PbndFc->nFace ;
                  FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
                }
              }
          
          /* Quads. */
          if ( pUns->pmQuadBc[nBc] )
            for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
              if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
                pFoE = elemType[PbndFc->Pelem->elType].faceOfElem + PbndFc->nFace ;
                if ( pFoE->mVertsFace == 4 ) {
                  /* List the internal element and the number of this face. */
                  mBndFacesWritten++ ;
                  buffer[0] = PbndFc->Pelem->number ;
                  buffer[1] = PbndFc->nFace ;
                  FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
                }
              }
        }
      
      /* Check. */
      if ( mBndFacesWritten != pUns->pmFaceBc[nBc] ) {
        sprintf ( hip_msg, " miscount of boundary faces for boundary %d:\n"
                 "        %"FMT_ULG" expected, but %"FMT_ULG" found.\n",
                 nBc+1, pUns->pmFaceBc[nBc], mBndFacesWritten ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  
  /* Trailing length of the record. */
  buffer[0] = 2*pUns->mFaceAllBc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;




  
  /* Vertices on the axis. */
  if ( pUns->specialTopo != axiX ) {
    /* No special verts, list two empty records for the counter and the list. */
    buffer[0] = buffer[2] = sizeof( int ) ;
    buffer[1] = 0 ;
    FWRITE ( buffer, sizeof( int ), 3, boundFile ) ;
    buffer[0] = buffer[1] = 0 ;
    FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
  }
  else {
    /* How many are there? */
    mVxAxi = axis_verts ( pUns, pUns->specialTopo ) ;

    buffer[0] = buffer[2] = sizeof( int ) ;
    buffer[1] = mVxAxi ;
    FWRITE ( buffer, sizeof( int ), 3, boundFile ) ;

    /* Loop over all vertices, list all the ones on the axis. */
    buffer[0] = mVxAxi*sizeof( int ) ;
    FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
    
    pChunk = NULL ;
    while ( loop_verts( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
        if ( pVx->singular ) {
          buffer[0] = pVx->number ;
          FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
        }

    buffer[0] = mVxAxi*sizeof( int ) ;
    FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
  }




  


  /* List of boundary faces, same index as record 6, but this time listed
     with the node numbers rather than the canonical face. */
  /* Opening length of the record of boundary faces. */
  mBi   = pUns->mBiAllBc ;
  mTri  = pUns->mTriAllBc ;
  mQuad = pUns->mQuadAllBc ;
  mVxFc = 2*mBi + 3*mTri + 4*mQuad ;
  buffer[0] = mVxFc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  /* Loop over all boundaries. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] ) {
      PbndPatch = NULL ;
      mVxFcWr = 0 ;
      
      while ( loop_bndFaces_bc ( pUns, nBc, &PbndPatch, &pBndFcBeg, &pBndFcEnd ) )
        if ( pUns->mDim == 2 ) {
          /* In 2-D all faces of two nodes are listed as quads. */
          for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
            if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
              /* List the forming vertices. */
              pElem = PbndFc->Pelem ;
              pFoE = elemType[ pElem->elType ].faceOfElem + PbndFc->nFace ;
              kVxFc = pFoE->kVxFace ;
              ppVx = pElem->PPvrtx ;

              buffer[0] = ppVx[ kVxFc[0] ]->number ; 
              buffer[1] = ppVx[ kVxFc[1] ]->number ;

              mVxFcWr += 2 ;
              FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
            }
        }
        else {
          /* 3D. List triangles and quads seperately. First triangles. */
          if ( pUns->pmTriBc[nBc] )
            for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
              if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
                pElem = PbndFc->Pelem ;
                pFoE = elemType[pElem->elType].faceOfElem + PbndFc->nFace ;
                kVxFc = pFoE->kVxFace ;
                ppVx = pElem->PPvrtx ;

                if ( pFoE->mVertsFace == 3 ) {
                  /* List the internal element and the number of this face. */
                  buffer[0] = ppVx[ kVxFc[0] ]->number ; 
                  buffer[1] = ppVx[ kVxFc[1] ]->number ;
                  buffer[2] = ppVx[ kVxFc[2] ]->number ;
                  
                  mVxFcWr += 3 ;
                  FWRITE ( buffer, sizeof( int ), 3, boundFile ) ;
                }
              }
          
          /* Quads. */
          if ( pUns->pmQuadBc[nBc] )
            for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
              if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
                pElem = PbndFc->Pelem ;
                pFoE = elemType[pElem->elType].faceOfElem + PbndFc->nFace ;
                kVxFc = pFoE->kVxFace ;
                ppVx = pElem->PPvrtx ;

                if ( pFoE->mVertsFace == 4 ) {
                  /* List the internal element and the number of this face. */
                  /* List the internal element and the number of this face. */
                  buffer[0] = ppVx[ kVxFc[0] ]->number ; 
                  buffer[1] = ppVx[ kVxFc[1] ]->number ;
                  buffer[2] = ppVx[ kVxFc[2] ]->number ;
                  buffer[3] = ppVx[ kVxFc[3] ]->number ;
                  
                  mVxFcWr += 4 ;
                  FWRITE ( buffer, sizeof( int ), 4, boundFile ) ;
                }
              }
        }
      
      /* Check. */
      if ( mVxFcWr != 2*pUns->pmBiBc[nBc] +3*pUns->pmTriBc[nBc] +4*pUns->pmQuadBc[nBc] ){
        sprintf ( hip_msg, 
                  "miscount of boundary face to vertex ptrs for boundary %d:\n"
                 "        %"FMT_ULG" expected, but %"FMT_ULG" found.\n", nBc+1,
                 2*pUns->pmBiBc[nBc] +
                 3*pUns->pmTriBc[nBc] +
                 4*pUns->pmQuadBc[nBc], mVxFcWr ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  
  /* Trailing length of the record. */
  buffer[0] = mVxFc*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;






  /* List of multipatch boundary vertices. */
  /* How many? */
  mVxMP =  mpVx.mVxMP ;
  buffer[0] = buffer[2] = sizeof( int ) ;
  buffer[1] = mVxMP ;
  FWRITE ( buffer, sizeof( int ), 3, boundFile ) ;

  /* List of the mp vx. */
  buffer[0] = mVxMP*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
  FWRITE ( mpVx.nVxMP, sizeof( int ), mVxMP, boundFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  /* Index of patch entries. */
  buffer[0] = (mVxMP+1)*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
  FWRITE ( mpVx.ndxVxMP, sizeof( int ), mVxMP+1, boundFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

  /* List of patches. */
  mEntries = mpVx.ndxVxMP[ mVxMP ]-1 ;
  buffer[0] = mEntries*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
  FWRITE ( mpVx.lsVxMP, sizeof( int ), mEntries, boundFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;

 
  arr_free ( mpVx.nVxMP ) ;
  arr_free ( mpVx.ndxVxMP ) ;
  arr_free ( mpVx.lsVxMP ) ;
  
  
  fclose ( boundFile ) ;
  return ( 1 ) ;
}



/****************************************************************

  write_avbp_inBound:
  Write internal boundary faces between blocks to AVBP. This
  information is void, since in the unstructured case there is
  only one block.

  Last update:
  ------------
  21Jan18; update matchFc_s to number 0,1 rather than 1,2.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  14Sep04; allocate proper length for buffer.
  20Dec01; don't write periodic faces if they are written to the .exBound.
  8May96: derived from write_mb_int_bound.
  
  Input:
  ------
  PboundFile:
 
  Changes to:
  -----------

  Returns:
  --------
  1

*/

int write_avbp_inBound ( uns_s *pUns, char *PboundFile ) {
  
  FILE *boundFile ;
  int buffer[6*MAX_PER_PATCH_PAIRS+3], mPerBc, nPerBc, nPerFc, mFcAllDir, nVx ;
  perBc_s *pPerBc ;
  matchFc_struct *pPerFc ;

  /* Open input file. */
  if ( ( boundFile = fopen ( PboundFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PboundFile ) ;
    return ( 0 ) ; }

  if ( ( mPerBc = pUns->mPerBcPairs ) ) {
    /* There are periodic boundaries.  */
    
    if ( verbosity > 2 )
      printf ( "      int. bound. to %s\n", PboundFile ) ;
    if ( verbosity > 3 )
      printf ( "        found %d pair[s] of periodic boundaries, %"FMT_ULG" pairs of vertices\n",
	       pUns->mPerBcPairs, pUns->mPerVxPairs ) ;

    /* One block, one patch. */
    buffer[0] = 3*sizeof( int ) ;
    buffer[1] = 1 ;
    buffer[2] = 2*pUns->mPerVxPairs ;
    buffer[3] = 1 ;
    buffer[4] = buffer[0] ;
    FWRITE ( buffer, sizeof( int ), 5, boundFile ) ;

    buffer[0] = 5*sizeof( int ) ;
    buffer[1] = 1 ;
    buffer[2] = 2*pUns->mPerVxPairs ;
    buffer[3] = 0 ;
    buffer[4] = 1 ;
    buffer[5] = 0 ;
    buffer[6] = buffer[0] ;
    FWRITE ( buffer, sizeof( int ), 7, boundFile ) ;


    /* Index of periodic vertices. */
    buffer[0] = 4*sizeof( int ) ;
    buffer[1] = 1 ;
    buffer[2] = pUns->mPerVxPairs ;
    buffer[3] = 1;
    buffer[4] = 1;
    buffer[5] = buffer[0] ;
    FWRITE ( buffer, sizeof( int ), 6, boundFile ) ;


    
    /* Record length of the pairs of periodic vertices. */
    buffer[0] = 2*pUns->mPerVxPairs*sizeof( int ) ;
    FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
    
    /* List all periodic pairs. Loop over all inlets. */
    for ( nVx = 0 ; nVx < pUns->mPerVxPairs ; nVx++ ) {
      buffer[0] = pUns->pPerVxPair[nVx].In->number ;
      FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
    }
    /* outlet. Listed backwards in AVBP. */
    for ( nVx = pUns->mPerVxPairs-1 ; nVx >= 0 ; nVx-- ) {
      buffer[0] = pUns->pPerVxPair[nVx].Out->number ;
      FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
    }

    /* Trailing length of the record. */
    buffer[0] = 2*pUns->mPerVxPairs * sizeof( int ) ;
    FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;


    
    if ( perBc_in_exBound ) {
      /* Empty record of faces. */
      buffer[0] = buffer[1] = 0 ;
      FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
      FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
    }
    else {
      /* List faces. This is added info not defined in the AVBP 4.2 format.
       Number of faces per patch. */
      buffer[0] = ( 3*mPerBc+1 )*sizeof( int ) ;
      /* Number of faces per patch. */
      for ( mFcAllDir = nPerBc = 0 ; nPerBc < mPerBc ; nPerBc++ ) {
        mFcAllDir += pUns->pPerBc[nPerBc].mPerFcPairs ;
        buffer[nPerBc+1] = 2*pUns->pPerBc[nPerBc].mPerFcPairs ;
      }

      /* Place a -9999 as a flag that there are bc numbers next. */
      buffer[mPerBc+1] = -9999 ;

      /* Corresponding b.c. numbers. In, then Out for each pair. */
      for ( nPerBc = 0 ; nPerBc < mPerBc ; nPerBc++ ) {
        buffer[2*nPerBc+mPerBc+2] =  pUns->pPerBc[nPerBc].pBc[0]->nr ;
        buffer[2*nPerBc+mPerBc+3] =  pUns->pPerBc[nPerBc].pBc[1]->nr ;
      }
      buffer[3*mPerBc+2] = ( 3*mPerBc+1 )*sizeof( int ) ;
      FWRITE ( buffer, sizeof( int ), 3*mPerBc+3, boundFile ) ;

      /* List all periodic boundary faces with a pointer to the elem and
       a face number. */
      buffer[0] = 4*mFcAllDir*sizeof( int ) ;
      FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
      /* Loop over all boundaries. */
      for ( nPerBc = 0 ; nPerBc < mPerBc ; nPerBc++ ) {
        pPerBc = pUns->pPerBc + nPerBc ;
        /* _inlet_ */
        for ( nPerFc = 0 ; nPerFc < pPerBc->mPerFcPairs ; nPerFc++ ) {
          pPerFc = pPerBc->pPerFc + nPerFc ;
          buffer[0] = pPerFc->pElem0->number ;
          buffer[1] = pPerFc->nFace0 ;
          FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
        }
        /* _outlet_ */
        for ( nPerFc = 0 ; nPerFc < pPerBc->mPerFcPairs ; nPerFc++ ) {
          pPerFc = pPerBc->pPerFc + nPerFc ;
          buffer[0] = pPerFc->pElem1->number ;
          buffer[1] = pPerFc->nFace1 ;
          FWRITE ( buffer, sizeof( int ), 2, boundFile ) ;
        }
      }

      /* Trailing length of the record. */
      buffer[0] = 4*mFcAllDir*sizeof( int ) ;
      FWRITE ( buffer, sizeof( int ), 1, boundFile ) ;
    }
  }



  
  else {
    /* File is void. */
    if ( verbosity > 2 )
      printf ( "      void int. bound. to %s\n", PboundFile ) ;

    buffer[0] = 3*sizeof( int ) ;
    /* One block. */
    buffer[1] = 1 ;
    buffer[2] = 0 ;
    buffer[3] = 0 ;
    buffer[4] = buffer[0] ;
    FWRITE ( buffer, sizeof( int ), 5, boundFile ) ;
    
    buffer[0] = 5*sizeof( int );
    buffer[1] = 0 ;
    buffer[2] = 0 ;
    buffer[3] = 0 ;
    buffer[4] = 0 ;
    buffer[5] = 0 ;
    buffer[6] = buffer[0] ;
    
    buffer[7] = 0 ;
    buffer[8] = 0 ;
    
    buffer[9] = 0 ;
    buffer[10] = 0 ;
    
    FWRITE ( buffer, sizeof( int ), 11, boundFile ) ;
  }

  fclose ( boundFile ) ;
  return ( 1 ) ;
}




/****************************************************************

  write_avbp_asciiBound_4p2:
  Write the ascii file with boundary conditions for AVBP Version pre 4.7

  Last update:
  ------------
  15Dec13; replace x_axis_verts with axis_verts.
  8May96: derived from mb.

  Input:
  ------
  mDim:        number of spatial dimensions,
  PboundFile:

 
  Changes to:
  -----------
  *PboundFile:

*/

int write_avbp_asciiBound_4p2 ( uns_s *pUns, char *PboundFile ) {
  
  FILE *boundFile ;
  int nBc ;
  const bc_struct *Pbc ;

  if ( verbosity > 2 )
    printf ( "      void 4.2 bc data to %s\n", PboundFile ) ;

  if ( ( boundFile = fopen ( PboundFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PboundFile ) ;
    return ( 0 ) ; }
  
  fprintf ( boundFile, "  Grid processing by hip version %s.\n %d boundary patches.\n",
	    version, pUns->mBc ) ;

  
  for ( nBc = 0 ; Pbc = pUns->ppBc[nBc], nBc < pUns->mBc ; nBc++ ) {

    fprintf ( boundFile, "---------------------------------------------\n" ) ;
    fprintf ( boundFile, "  Patch: %d\n  %s\n",
              nBc+1,  pUns->ppBc[nBc]->text ) ;
	
    /* Guess a type. */
    if ( Pbc->type[0] == 'w' ||  Pbc->type[0] == 'v' )
      /* Wall. */
      fprintf ( boundFile, "  1\n  11" ) ;
    else if ( Pbc->type[0] == 's'  )
      /* Symmetry. */
      fprintf ( boundFile, "  2\n  20" ) ;
    else if ( Pbc->type[0] == 'f'  )
      /* Farfield. */
      fprintf ( boundFile, "  3\n  30" ) ;
    else
      /* Unknown. */
      fprintf ( boundFile, "  0\n  00" ) ;
	
    /* Remaining 6 type parameters. */
    fprintf ( boundFile, " 0 0 0 0 0 0\n" ) ;
    /* 7 bc_coeff. */
    fprintf ( boundFile, " 0. 0. 0. 0. 0. 0. 0.\n" ) ;
  }

  if ( pUns->specialTopo == axiX ) {
    /* List nodes on the x-axis. */
    axis_verts ( pUns, pUns->specialTopo ) ;
    
    fprintf ( boundFile, "---------------------------------------------\n" ) ;
    fprintf ( boundFile, "  Xtra-Info: %d\n  singular x-axis\n", pUns->mBc+1 ) ;
    
    fprintf ( boundFile, "  0\n" ) ;
    fprintf ( boundFile, "  00 0 0 0 0 0 0\n" ) ;
    fprintf ( boundFile, " 0. 0. 0. 0. 0. 0. 0.\n" ) ;
  }

  fclose ( boundFile ) ;
  return ( 1 ) ;
}

/****************************************************************

  write_avbp_asciiBound_4p7:
  Write the ascii file with boundary conditions for AVBP 4.7.

  Last update:
  ------------
  28Feb18: rename rotAngle to rotAngleRad to clarify meaning.
  15Dec13; replace x_axis_verts with axis_verts.
  19May04; allow for no bc.
  16Dec01; derived from the renamed write_avbp_asciiBound.

  Input:
  ------
  mDim:        number of spatial dimensions,
  PboundFile:

 
  Changes to:
  -----------
  *PboundFile:

*/

static void add_bc_4p7 ( FILE *boundFile,
                         int nBc, const char *text,
                         const char *label, int mLines, char line[][LINE_LEN] ) {

  int k ;
  
  fprintf ( boundFile, "---------------------------------------------\n" ) ;
  fprintf ( boundFile, "  Patch: %d\n  %s\n", nBc+1, text ) ;
  fprintf ( boundFile, "  %s\n", label ) ;

  for ( k = 0 ; k < mLines ; k++ )
    fprintf ( boundFile, "  %s\n", line[k] ) ;

  return ;
}


int write_avbp_asciiBound_4p7 ( uns_s *pUns, char *PboundFile ) {
  
  const bc_struct *Pbc ;

  FILE *boundFile ;
  int nBc, mVxAxis ;
  char emptyLines[5][LINE_LEN] = { "\0","\0","\0","\0","\0" }, someLines[5][LINE_LEN] ;

  if ( verbosity > 2 ){
    sprintf ( hip_msg, "writing estimated bc to %s", PboundFile ) ;
    hip_err ( info, 1, hip_msg ) ;
  }

  if ( ( boundFile = fopen ( PboundFile, "w" ) ) == NULL ) {
    sprintf ( hip_msg, "file: %s could not be opened.", PboundFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  
  fprintf ( boundFile, "  Grid processing by hip version %s.\n  %d boundary patches.\n",
	    version, pUns->mBc ) ;



  
  /* ex doc:
   e:   & entry           &    o:   & outlet          & u:   & upper periodic  \\
   f:   & far field       &    p:   & any periodic    & v:   & viscous wall    \\
   i:   & inviscid wall   &    n:   & none            & w:   & any wall        \\
   l:   & lower periodic  &    s:   & symmetry plane  \\
  */
  
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    Pbc = pUns->ppBc[nBc] ;
    switch ( Pbc->type[0] ) {
      case 'e' :
        /* entry, (inlet) */
        strcpy ( someLines[0], "1\0" ) ;
        strcpy ( someLines[1], "1\0" ) ;
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "INLET_WAVE_UVW_T", 2, someLines ) ;
        break ;
      case 'f' :
        /* far field */
        strcpy ( someLines[0], "1\0" ) ;
        strcpy ( someLines[1], "1\0" ) ;
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "INLET_FREESTREAM", 2, someLines ) ;
        break ;
      case 'i' :
        /* inviscid wall */
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "WALL_SLIP_ADIAB", 0, emptyLines ) ;
        break ;
      case 'l' :
        /* lower periodic */
        if ( pUns->ppBc[nBc]->pPerBc->rotAngleRad == 0. )
          /* Translation. */
          add_bc_4p7 ( boundFile, nBc, Pbc->text, "NO_BOUNDARY", 0, emptyLines ) ;
        else {
          /* Rotation. */
          strcpy ( someLines[0], "-1\0" ) ;
          sprintf ( someLines[1], "%g", pUns->ppBc[nBc]->pPerBc->rotAngleRad/PI*180. ) ;
          add_bc_4p7 ( boundFile, nBc, Pbc->text, "PERIODIC_AXI", 2, someLines ) ;
        }
        break ;
      case 'o' :
        /* outlet, exit */
        strcpy ( someLines[0], "1\0" ) ;
        strcpy ( someLines[1], "1\0" ) ;
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "OUTLET_WAVE_P", 2, someLines ) ;
        break ;
      case 'p' :
        /* any periodic */
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "NO_BOUNDARY", 0, emptyLines ) ;
        break ;
      case 's' :
        /* symmetry */
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "SYMMETRY", 0, emptyLines ) ;
        break ;
      case 'u' :
        /* upper periodic */
        if ( pUns->ppBc[nBc]->pPerBc->rotAngleRad == 0. )
          /* Translation. */
          add_bc_4p7 ( boundFile, nBc, Pbc->text, "NO_BOUNDARY", 0, emptyLines ) ;
        else {
          /* Rotation. */
          strcpy ( someLines[0], "1\0" ) ;
          sprintf ( someLines[1], "%g", pUns->ppBc[nBc]->pPerBc->rotAngleRad/PI*180. ) ;
          add_bc_4p7 ( boundFile, nBc, Pbc->text, "PERIODIC_AXI", 2, someLines ) ;
        }
        break ;
      case 'v' :
        /* viscous wall */
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "WALL_NOSLIP_ADIAB", 0, emptyLines ) ;
        break ;
      case 'w' :
        /* any wall */
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "WALL", 0, emptyLines ) ;
        break ;
      default  :
        /* 'n', none */
        add_bc_4p7 ( boundFile, nBc, Pbc->text, "NO_BOUNDARY", 0, emptyLines ) ;
        break ;
      }
  }
  fprintf ( boundFile, "---------------------------------------------\n" ) ;

    
  if ( pUns->specialTopo == axiX ) {
    /* List nodes on the x-axis. */
    mVxAxis = axis_verts ( pUns, pUns->specialTopo ) ;
    
    fprintf ( boundFile, "  Xtra-Info: %d\n  singular x-axis\n", pUns->mBc+1 ) ;
    fprintf ( boundFile, "  %d nodes\n", mVxAxis ) ;
    fprintf ( boundFile, "---------------------------------------------\n" ) ;
  }

  fclose ( boundFile ) ;
  return ( 1 ) ;
}

