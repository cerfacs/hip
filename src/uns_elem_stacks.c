/*
  file_name.c:
*/

/*! short descr. for doxygen
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  ; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern const elemType_struct elemType[] ;

#define MAX_ELEM_IN_STACK (100)


/***************************************************************************

  PUBLIC

**************************************************************************/


/******************************************************************************
  fun_name:   */

/*! create lines of stacked elements, e.g. stacks of prisms in boundary layers.
 *
 *
 */

/*
  
  Last update:
  ------------
  25Feb20: new interface to make_llfc.
  5July17: conceived.
  

  Input:
  ------
  pUns

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int make_elem_stack ( uns_s *pUns ) {

  // Count the vertices only forming prisms or hexa at the start of the list.
  // Easiest done by numbering.
  const int doUseNumber = 1 ;
  const int doReset = 1, doBound = 1 ;
  const int dontReset = 0, dontBound = 0 ;
  number_uns_grid_types ( pUns, pri, hex, doUseNumber, doReset, doBound ) ;

  // Propagate the numbered zone to node neighbours.
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  const elemType_struct *pElT ;
  int mVx ;
  int kVx ;
  vrtx_struct **ppVx ;
  ulong_t mVertsStack = pUns->mVertsNumbered ;
  ulong_t mElemsStack = pUns->mElemsNumbered ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid &&
           !( pElem->elType == pri || pElem->elType == hex ) ) {
        pElT = elemType + pElem->elType ;
        mVx = pElT->mVerts ;
        ppVx = pElem->PPvrtx ;

        // Is there a numbered vx in here that is part of a stack?
        for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
          if ( ppVx[kVx]->number > 0 && ppVx[kVx]->number <= mVertsStack )
            break ;
        }

        if ( kVx < mVx ) {
          // Add all unnumbered vertices.
          for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
            if ( ppVx[kVx]->number == 0 )
              ppVx[kVx]->number = ++pUns->mVertsNumbered ;
          }

          // Add the element.
          if ( pElem->number )
            hip_err ( fatal, 0, "oops, numbered non-stack element, this shouldn't occur!") ;

          pElem->number = ++pUns->mElemsNumbered ;
          pUns->mElemsOfType[ pElem->elType ]++ ;
        }
      }
  
  

  // Create the list of faces.
  fc2el_s *pFc2El ;
  ulong_t mFcBecomeInt, mFcDupl, mFcOtherDupl, mFcRemoved, mFcUnMtch ;
  llVxEnt_s *pllVxFc = make_llFc ( pUns, bnd, &pFc2El, 0, 0, 1,
                                   &mFcBecomeInt, &mFcDupl,&mFcOtherDupl ) ;

  // unmark all elems.
  reserve_elem_mark ( pUns, 0, "make_elem_stacks" ) ;
  reset_all_elem_mark ( pUns, 0 ) ;


  // Initialise stacks, use make_array from lib_do/array
  size_t initAllocSize = 1000 ;
  pUns->pArrStackFc = make_array ( pUns->pArrStackFc, (char **) &(pUns->pStackFc),
                                   initAllocSize, sizeof ( *pUns->pStackFc ),
                                   pUns->pFam, "pStackFc" ) ;

  pUns->pArrStackFcBeg = make_array ( pUns->pArrStackFcBeg, (char **) &(pUns->pStackFcBeg),
                                      initAllocSize, sizeof ( *pUns->pStackFcBeg ),
                                      pUns->pFam, "pStackFcBeg" ) ;
 
  pUns->pArrStackFcEnd = make_array ( pUns->pArrStackFcEnd, (char **) &(pUns->pStackFcEnd),
                                      initAllocSize, sizeof ( *pUns->pStackFcEnd ),
                                      pUns->pFam, "pStackFcEnd" ) ;
 

  // Start stacks from boundary faces.
  pChunk = NULL ;
  bndPatch_struct *pBndPatch = NULL ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  int nFc ;
  int nOppFc ;
  stackFcTerm_s *pStFcBeg ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
      // Is this the stat of a stack?
      pElem = pBndFc->Pelem ;
      nFc = pBndFc->nFace ;

      if ( ( nOppFc = elemType[ pElem->elType ].faceOfElem[nFc].nOppSide ) ) {
        // There is an opposite face. Start a stack.

        pStFcBeg = get_new_data ( pUns->pArrStackFcBeg, (char**)&(pUns->pStackFcBeg), arr_add, 0, 1 ) ;

        if ( !pStFcBeg )
          hip_err ( fatal, 0, "failed to get empty stack face slots in make_elem_stack.\n");

        pStFcBeg->pBndFc = pBndFc ;
        pStFcBeg->pCapElem = NULL ;
      }
    }

  
  // Follow the lines of the stacks.
  stackFcTerm_s *pStFcBegNxtFree =
    get_new_data ( pUns->pArrStackFcBeg, (char**) &(pUns->pStackFcBeg),
                   arr_probe, 0, 0 ) ;
  int mElInStack ;
  stackFc_s *pStFc ;
  ulong_t nEnt ;
  int mVxFc ;
  vrtx_struct *pVxFc[MAX_VX_FACE] ;
  int kMin ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace  ;
  elem_struct *pEl0, *pEl1 ;
  int nFc0, nFc1 ;
  stackFcTerm_s *pStFcEnd ;
  for ( pStFcBeg = pUns->pStackFcBeg ; pStFcBeg < pStFcBegNxtFree ; pStFcBeg++ ) {
    pBndFc = pStFcBeg->pBndFc ;
    pElem = pBndFc->Pelem ;
    nFc = pBndFc->nFace ;
    mElInStack = 0 ;

    while ( pElem && mElInStack < MAX_ELEM_IN_STACK ) {
      mElInStack++ ;
      pStFc = get_new_data ( pUns->pArrStackFc, (char**) &(pUns->pStackFc), arr_add, 0, 1 ) ;
      pStFcBeg->pStackFc = pStFc ;
      pStFc->pElem = pElem ;
      pStFc->nFc = nFc ;

      pFoE = elemType[ pElem->elType ].faceOfElem + nFc ;
      if ( ( nOppFc = pFoE->nOppSide ) ) {
        mVxFc = pFoE->mVertsFace ;
        kVxFace = pFoE->kVxFace ;

        /* Make a list of vertices that form this facet. */
        for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ )
          pVxFc[kVx] = pElem->PPvrtx[ kVxFace[kVx] ] ;

        
        // Find the element sharing the opposite face.
        if ( !(nEnt = get_ent_vrtx ( pllVxFc, mVxFc, (const vrtx_struct **) pVxFc, &kMin ) ) )
          hip_err ( fatal, 0, "there should have been a face in make_elem_stacks." ) ;
        
        if ( !show_fc2el_elel ( pFc2El, nEnt, &pEl0, &nFc0, &pEl1, &nFc1 ) ) {

          if ( pEl0 == pElem ) {
            pElem =  pEl1 ;
            nFc = nFc1 ;
          }
          else if ( pEl1 == pElem ) {
            pElem = pEl0 ;
            nFc = nFc0 ;
          }
          else
            hip_err ( fatal, 0, "element and face don't match in make_elem_stacks." ) ;
        }
        else
          pElem = NULL ;
      }

      // end of stack.
      if ( mElInStack >= MAX_ELEM_IN_STACK ) {
        sprintf ( hip_msg, "more than %d elements in stack, overwflow. Check grid.",
                  mElInStack ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      pStFcEnd = get_new_data ( pUns->pArrStackFcEnd, (char**)&pUns->pStackFcEnd, arr_add, 0, 1 ) ;
      pStFcEnd->pStackFc = pStFc ;
      // Note, pElem is just a flag here, the face is stored with pStackFc.       
      pStFcEnd->pCapElem = pElem ; 
    }
  }
  


  // Loop over all faces in the list, if it is a hybrid = end face,
  // then check whether it has a stack ending there, if not start
  // a stack backwards.

  
  // Loop over all pri and hex elements, check whether they are
  // in a stack.
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid &&
           ( pElem->elType != pri || pElem->elType != hex ) ) {
        // This is a non-stack element. Only the ones that share nodes with
        // stackable elements have been listed.
        ;
      }


  release_elem_mark ( pUns, 0 ) ;
  
  return ( 0 ) ;
}
