/*
  uns_cut_line.c:
  Find the intersections of a given line with all the faces of the grid
  and interpolat the values on these faces.

  Last update:
  ------------
  6Apr13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  6Jul04; compute the integral along the line, treat the case of line being 
  parallel to the face properly. 
  Oct00; conceived.

  This file contains:
  -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;
extern const Grids_struct Grids ;

static lineX_s lineX = { {0.,0.,0.}, {0.,0.,0.},
                         /*{0.,0.,0.}, {0.,0.,0.},*/
                         {0.,0.,0.},
                         0.,
                         0,
                         0, 0, NULL } ;

/* Fudge factor to be on the safe side in the radius test. */
const double close_fac = 1.1 ;
/* Two intersections are considered distinct if their arclength differs by this much. */
const double distinct_fac = 1.e-7 ;
extern const double x_fac ;
extern const double para_tol ;
/******************************************************************************
 Order fcInts for arclength. */
static int fcInt_cmp ( const void *pFcInt0, const void *pFcInt1) {
  double diff = ( *( fcInt_s* )pFcInt0 ).t - ( *( fcInt_s* )pFcInt1 ).t ;
  if ( diff > 0. )
    return ( 1 ) ;
  else if ( diff < 0. )
    return ( -1 ) ;
  else
    return ( 0 ) ;
  
}
/******************************************************************************

  face_is_rhs:
  Is a face rhs? Find the lowest numbered node on the face and compare the
  the numbers of the one before and after. If the one after is higher, this
  face is rhs.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int face_is_rhs ( elem_struct *pElem, int kFc,
                         int *pmVxFc, vrtx_struct *pVxFc[] ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE = pElT->faceOfElem + kFc ;
  const int *kVxFc = pFoE->kVxFace ;
  const int mDim = pElT->mDim ;

  vrtx_struct *pVx ;
  int mVxFc = pFoE->mVertsFace, kVxMin, nVxMin, kVx, nVx, nrP1, nrM1 ;
  *pmVxFc = mVxFc ;

  if ( mDim == 3 ) {
    /* Find the position of the lowest numbered node on the face. */
    kVxMin = 0 ;
    nVxMin = pElem->PPvrtx[0]->number + 999 ;
    for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
      pVx = pVxFc[kVx] = pElem->PPvrtx[ kVxFc[kVx] ] ;
      nVx = pVx->number ;
      if ( nVx < nVxMin ) {
        nVxMin = nVx ;
        kVxMin = kVx ;
      }
    }

    kVx =  kVxFc[ (kVxMin+1)%mVxFc ] ;
    nrP1 = pElem->PPvrtx[kVx]->number ;

    kVx =  kVxFc[ (kVxMin+mVxFc-1)%mVxFc ] ;
    nrM1 = pElem->PPvrtx[kVx]->number ;
  }

  else {
    /* 2D. Face is rhs if the element is to the right viewed from lower to
       higher node on the edge/face. */
    pVxFc[0] = pElem->PPvrtx[ kVxFc[0] ] ;
    pVxFc[1] = pElem->PPvrtx[ kVxFc[1] ] ;
    nrM1 = pVxFc[0]->number ;
    nrP1 = pVxFc[1]->number ;
  }

  if ( nrM1 < nrP1 )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}
  
/******************************************************************************

  find_t:
  Given the intersection point xInt along lineX, find the arclength.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  xInt: the point along the line.
  
  Returns:
  --------
  the arclength of the point.
  
*/

static double find_t ( double xInt[3], const int mDim ) {

  double dx[3], d ;

  dx[0] = xInt[0] - lineX.xyzBeg[0] ;
  dx[1] = xInt[1] - lineX.xyzBeg[1] ;
  dx[2] = ( mDim == 3 ? xInt[2] - lineX.xyzBeg[2] : 0 ) ;

  d = sqrt( dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2] ) ;

  return ( d/lineX.len ) ;
}

/******************************************************************************

  solve_x:
  Calculate the intersection between an edge and a line.
  
  Last update:
  ------------
  10Nov17; new interface to x_lin.
  : conceived.
  
  Input:
  ------
  pCo[2][]:    the coordinates of the vertices on the edge

  Changes To:
  -----------
  *pt:         the length along the edge.
  
  Returns:
  --------
  0 if parallel
  1-4 if intersecting, see x_lin.
  
*/

int solve_x_lin ( double *pCo[2], double *pt ) {

  double s ;
  return ( x_lin( (const double *) pCo[0], (const double *) pCo[1], pt,
                  lineX.xyzBeg, lineX.xyzEnd, &s ) ) ;
}


/******************************************************************************

  x_tri:
  Calculate the intersection between a triangular face and a line.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pCo[3][]:    the coordinates of the vertices of the face

  Changes To:
  -----------
  xInt:        the intersection with the triangular face, if any.
               If the line is in the plane of the face, xInt is taken as the centroid.
  
  Returns:
  --------
  1 if intersecting, 2 if line is in the plane of the face, 0 otherwise.
  
*/

int solve_x_tri ( double *pCo[3], double xInt[3] ) {
    
  double al[3] ;

  return ( x_tri ( (const double **) pCo, lineX.xyzBeg, lineX.xyzEnd, xInt, al ) ) ;

}

/******************************************************************************

  add_fc:
  Add a face to the list.
  
  Last update:
  ------------
  5Jul04; fix bug in mFcAlloc *= mFcAlloc.
  : conceived.
  
  Input:
  ------
  pUns:    needed for realloc
  pVxFc[]: the forming vertices
  wt[]:    their interpolation weights.
  xInt[]:  the intersection point.

  Changes To:
  -----------
  lineX.fcInt
  
*/

void add_fc ( uns_s *pUns, vrtx_struct *pVxFc[], double wt[],
              double xInt[] ) {

  fcInt_s *pFc ;
  int mFcInt = lineX.mFcInt ;
  
  lineX.mFcInt++ ;
  if ( lineX.mFcInt >= lineX.mFcAlloc ) {
    lineX.mFcAlloc *= REALLOC_FACTOR ;
    lineX.mFcAlloc++ ;
    lineX.mFcAlloc = MAX( lineX.mFcAlloc, 100 ) ;

    lineX.fcInt = arr_realloc ( "lineX.mFcInt", pUns->pFam,
                                lineX.fcInt, lineX.mFcAlloc, sizeof( *lineX.fcInt ) ) ;
  }

  pFc = lineX.fcInt + mFcInt  ;

  pFc->pVx[0] = pVxFc[0] ;
  pFc->pVx[1] = pVxFc[1] ;
  pFc->pVx[2] = pVxFc[2] ;
  pFc->pVx[3] = pVxFc[3] ;

  pFc->wt[0] = wt[0] ;
  pFc->wt[1] = wt[1] ;
  pFc->wt[2] = wt[2] ;
  pFc->wt[3] = wt[3] ;

  pFc->xInt[0] = xInt[0] ;
  pFc->xInt[1] = xInt[1] ;
  pFc->xInt[2] = ( pUns->mDim == 3 ? xInt[2] : 0. ) ;

  /* Calculate the arc-length of the intersection along the line. */
  pFc->t = find_t ( xInt, pUns->mDim ) ;

  return ;
}

/******************************************************************************

  get_3_wts:
  Given the vertex coordinates of a triangular face and an point on it,
  find the interpolation weights.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  co[]: vertex coordinates
  xInt[]: point to interpolate for
  
  Changes To:
  -----------
  wt[]: the interpolation weights.
  
*/

static void get_3_wts ( const double *co[3], double xInt[], double wt[] ) {

  double d[3][3], e[3][3], a[3], at, a0, a1, a2, w ;

  /* From vertex to xInt. */
  d[0][0] = xInt[0] - co[0][0] ;
  d[0][1] = xInt[1] - co[0][1] ;
  d[0][2] = xInt[2] - co[0][2] ;
  d[1][0] = xInt[0] - co[1][0] ;
  d[1][1] = xInt[1] - co[1][1] ;
  d[1][2] = xInt[2] - co[1][2] ;
  d[2][0] = xInt[0] - co[2][0] ;
  d[2][1] = xInt[1] - co[2][1] ;
  d[2][2] = xInt[2] - co[2][2] ;

  /* Edge vectors. */
  e[0][0] = co[2][0] - co[1][0] ;
  e[0][1] = co[2][1] - co[1][1] ;
  e[0][2] = co[2][2] - co[1][2] ;
  e[1][0] = co[0][0] - co[2][0] ;
  e[1][1] = co[0][1] - co[2][1] ;
  e[1][2] = co[0][2] - co[2][2] ;
  e[2][0] = co[1][0] - co[0][0] ;
  e[2][1] = co[1][1] - co[0][1] ;
  e[2][2] = co[1][2] - co[0][2] ;


  /* Areas. */
  cross_prod_dbl ( e[0], d[1], 3, a ) ;
  a0 = sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2] ) ;
  cross_prod_dbl ( e[1], d[2], 3, a ) ;
  a1 = sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2] ) ;
  cross_prod_dbl ( e[2], d[0], 3, a ) ;
  a2 = sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2] ) ;
  /* Total area. */
  at = a0+a1+a2 ;
           

  wt[0] = a0/at ;
  wt[1] = a1/at ;
  wt[2] = a2/at ;

  /* Remove roundoff. */
  wt[0] = MAX( 0., MIN( 1., wt[0] ) ) ;
  wt[1] = MAX( 0., MIN( 1., wt[1] ) ) ;
  wt[2] = MAX( 0., MIN( 1., wt[2] ) ) ;
  
  
  w = wt[0] + wt[1] + wt[2] ;
  if ( ABS( w-1. ) > 1.e-7 )
    printf ( " FATAL: interpolation outside triangle (%g) in get_3_wts.\n", w ) ;

  return ;
}
/******************************************************************************

  add_vx:
  Add an exact nodal match with weight 1 to the list.
  
  Last update:
  ------------
  7Jul4: conceived.
  
  Input:
  ------
  pUns:  to pass on.
  pVx:   matching vertices.

  Changes To:
  -----------
  lineX
  
*/

static void add_vx ( uns_s *pUns, vrtx_struct *pVx ) {

  vrtx_struct *pVxFc[MAX_VX_FACE] ;
  double wt[MAX_VX_FACE]  = { 1., 0., 0., 0. } ;
  int k ;

  for ( k = 0 ; k < MAX_VX_FACE ; k++ ) 
    pVxFc[k] = pVx ;

  add_fc ( pUns, pVxFc, wt, pVx->Pcoor ) ;

  return ;
}


/******************************************************************************

  add_2_face:
  Add an edge to the list, given the vertices and the intersection
  point.
  
  Last update:
  ------------
  8Aug01; fix bug with middle face interpolation using .25 rather than .5.
  : conceived.
  
  Input:
  ------
  pUns:  to pass on.
  pVxFc: forming vertices.
  inFc:  1: intersection, 2: line in plane of face, use the centroid.
  xInt[4]: the intersection point.

  Changes To:
  -----------
  lineX
  
*/

static void add_2_face ( uns_s *pUns, vrtx_struct *pVxFc[], double t ) {

  const double *pCo[3] ;
  double wt[MAX_VX_FACE], xInt[3] ;

  /* Vertex coordinates. */
  pCo[0] = pVxFc[0]->Pcoor ;
  pCo[1] = pVxFc[1]->Pcoor ;

  wt[0] = 1.-t ;
  wt[1] = t ;

  /* Empty third and fourth vertex. */
  pVxFc[2] = pVxFc[0] ;
  wt[2] = 0. ;
  pVxFc[3] = pVxFc[0] ;
  wt[3] = 0. ;

  /* Recalculate xInt with the weights. */
  xInt[0] = wt[0]*pCo[0][0] + wt[1]*pCo[1][0]  ;
  xInt[1] = wt[0]*pCo[0][1] + wt[1]*pCo[1][1]  ;
  if ( pUns->mDim == 2 ) 
    xInt[2] = lineX.xyzBeg[2] ;
  else 
    xInt[2] = wt[0]*pCo[0][2] + wt[1]*pCo[1][2]  ;
  

  add_fc ( pUns, pVxFc, wt, xInt ) ;

  return ;
}

/******************************************************************************

  add_3_face:
  Add a triangular face to the list, given the vertices and the intersection
  point.
  
  Last update:
  ------------
  7Jul5 ; remove test for line in plane. This is taken care of by the
          edge intersection test.
  : conceived.
  
  Input:
  ------
  pUns:  to pass on.
  pVxFc: forming vertices.
  inFc:  1: intersection, 2: line in plane of face, use the centroid.
  xInt[4]: the intersection point.

  Changes To:
  -----------
  lineX
  
*/

static void add_3_face ( uns_s *pUns, vrtx_struct *pVxFc[], double xInt[] ) {

  const double *pCo[3] ;
  double wt[MAX_VX_FACE], w ;

  /* Vertex coordinates. */
  pCo[0] = pVxFc[0]->Pcoor ;
  pCo[1] = pVxFc[1]->Pcoor ;
  pCo[2] = pVxFc[2]->Pcoor ;

  get_3_wts( pCo, xInt, wt ) ;

  /* Empty fourth vertex. */
  pVxFc[3] = pVxFc[0] ;
  wt[3] = 0. ;

  /* Renormalise. */
  w = wt[0] + wt[1] + wt[2] ;
  wt[0] /= w ;
  wt[1] /= w ;
  wt[2] /= w ;

  /* Recalculate xInt with the weights. */
  xInt[0] = wt[0]*pCo[0][0] + wt[1]*pCo[1][0] + wt[2]*pCo[2][0] ;
  xInt[1] = wt[0]*pCo[0][1] + wt[1]*pCo[1][1] + wt[2]*pCo[2][1] ;
  xInt[2] = wt[0]*pCo[0][2] + wt[1]*pCo[1][2] + wt[2]*pCo[2][2] ;

  add_fc ( pUns, pVxFc, wt, xInt ) ;

  return ;
}

/******************************************************************************

  add_4_face:
  Add a quad face to the list, given the vertices and the intersection
  point.
  
  Last update:
  ------------
  7Jul5 ; remove test for line in plane. This is taken care of by the
          edge intersection test.
  : conceived.
  
  Input:
  ------
  pUns:    to pass on.
  pVxFc:   forming vertices.
  inFc:    1: intersection, 2: line in plane of face, use the centroid for
           each of the 4 subtriangles in canonical order:
           0-1-2, 0-2-3, 0-1-3, 1-2-3.
  xInt[4]: the intersection on each subtriangle.

  Changes To:
  -----------
  lineX
  
*/

static void add_4_face  ( uns_s *pUns, vrtx_struct *pVxFc[],
                          int inFc[4], double xInt[4][3] ) {
  /* Average both possible triangulations. */

  const double *pCo[4] ;
  double wt[MAX_VX_FACE], wtT[2][MAX_VX_FACE] = {{0.}}, w ;


  /* Diag 0-2 */
  if ( inFc[0] ) {
    /* Intersection in triangle 0-1-2. */
    pCo[0] = pVxFc[0]->Pcoor ;
    pCo[1] = pVxFc[1]->Pcoor ;
    pCo[2] = pVxFc[2]->Pcoor ;
    get_3_wts( pCo, xInt[0], wt ) ;
    wtT[0][0] = wt[0] ;
    wtT[0][1] = wt[1] ;
    wtT[0][2] = wt[2] ;
    wtT[0][3] = 0. ;
  }
  else if ( inFc[1] ) {
    /* Intersection in triangle 0-2-3. */
    pCo[0] = pVxFc[0]->Pcoor ;
    pCo[1] = pVxFc[2]->Pcoor ;
    pCo[2] = pVxFc[3]->Pcoor ;
    get_3_wts( pCo, xInt[1], wt ) ;
    wtT[0][0] = wt[0] ;
    wtT[0][1] = 0. ;
    wtT[0][2] = wt[1] ;
    wtT[0][3] = wt[2] ;
  }

  if ( inFc[0] && inFc[1] ) {
    /* Both tris intersect, intersection must be on the diagonal. Remove the
       off-diagonal weights. */
    wtT[0][0] += wtT[0][1]/2 ;
    wtT[0][2] += wtT[0][1]/2 ;
    wtT[0][1]  = 0. ;
  }
  else if ( !inFc[0] && !inFc[1] ) {
    printf ( " FATAL: there should have been one intersected triangle"
             " in add_4_face.\n");
  }


  /* Diag 1-3 */
  if ( inFc[2] ) {
    /* Intersection in triangle 0-1-3. */
    pCo[0] = pVxFc[0]->Pcoor ;
    pCo[1] = pVxFc[1]->Pcoor ;
    pCo[2] = pVxFc[3]->Pcoor ;
    get_3_wts( pCo, xInt[2], wt ) ;
    wtT[1][0] = wt[0] ;
    wtT[1][1] = wt[1] ;
    wtT[1][2] = 0. ;
    wtT[1][3] = wt[2] ;
  }
  else if ( inFc[3] ) {
    /* Intersection in triangle 1-2-3. */
    pCo[0] = pVxFc[1]->Pcoor ;
    pCo[1] = pVxFc[2]->Pcoor ;
    pCo[2] = pVxFc[3]->Pcoor ;
    get_3_wts( pCo, xInt[3], wt ) ;
    wtT[1][0] = 0. ;
    wtT[1][1] = wt[0] ;
    wtT[1][2] = wt[1] ;
    wtT[1][3] = wt[2] ;
  }

  if ( inFc[2] && inFc[3] ) {
    /* Both tris intersect, intersection must be on the diagonal. */
    wtT[1][1] += wtT[1][0]/2 ;
    wtT[1][3] += wtT[1][0]/2 ;
    wtT[1][0]  = 0. ;
    wtT[1][2]  = 0. ;
  }
  else if ( !inFc[2] && !inFc[3] ){
    hip_err ( fatal, 0, "there should have been one intersected triangle"
              " in add_4_face.\n");
  }

  /* Average the weights. */
  wt[0] = .5*( wtT[0][0] + wtT[1][0] ) ;
  wt[1] = .5*( wtT[0][1] + wtT[1][1] ) ;
  wt[2] = .5*( wtT[0][2] + wtT[1][2] ) ;
  wt[3] = .5*( wtT[0][3] + wtT[1][3] ) ;

  /* Renormalise. */
  w = wt[0] + wt[1] + wt[2] + wt[3] ;
  wt[0] /= w ;
  wt[1] /= w ;
  wt[2] /= w ;
  wt[3] /= w ;

  /* Recalculate xInt with the weights. */
  pCo[0] = pVxFc[0]->Pcoor ;
  pCo[1] = pVxFc[1]->Pcoor ;
  pCo[2] = pVxFc[2]->Pcoor ;
  pCo[3] = pVxFc[3]->Pcoor ;
  xInt[0][0] = wt[0]*pCo[0][0] + wt[1]*pCo[1][0] + wt[2]*pCo[2][0] + wt[3]*pCo[3][0] ;
  xInt[0][1] = wt[0]*pCo[0][1] + wt[1]*pCo[1][1] + wt[2]*pCo[2][1] + wt[3]*pCo[3][1] ;
  xInt[0][2] = wt[0]*pCo[0][2] + wt[1]*pCo[1][2] + wt[2]*pCo[2][2] + wt[3]*pCo[3][2] ;

  add_fc ( pUns, pVxFc, wt, xInt[0] ) ;

  return ;
}

  
/******************************************************************************

  line_x_face:
  Does a line in 3space cross a face. If yes, add it to the list.
  
  Last update:
  ------------
  7Jul4; rework, intro edge intersection check.
  : conceived.
  
  Input:
  ------
  pUns:
  mVxFc:       number of vertices on the face
  pVxFc:       vertices on the face

  Changes To:
  -----------
  lineX.
  
  Returns:
  --------
  1 on intersection, 0 on none.
  
*/

static int line_x_face ( uns_s *pUns, const int mVxFc, vrtx_struct *pVxFc[] ) {

  vrtx_struct *pVxFc2[2] ;
  double *pCo[MAX_VX_FACE], xInt[4][3], t, t2 ;
  int inFc[4], k, k1, egX ;

  if ( mVxFc == 2 ) {
    /* 2D edge/face. */
    pCo[0] = pVxFc[0]->Pcoor ;
    pCo[1] = pVxFc[1]->Pcoor ;
    if ( ( inFc[0] = solve_x_lin ( pCo, &t ) ) ) {
      add_2_face ( pUns, pVxFc, t ) ;
      return ( 1 ) ;
    }
  }
  else if ( mVxFc == 3 ) {
    /* Triangular face. */
    pCo[0] = pVxFc[0]->Pcoor ;
    pCo[1] = pVxFc[1]->Pcoor ;
    pCo[2] = pVxFc[2]->Pcoor ;

    /* First check whether the line intersects with any edge of the face. */      
    for ( k = egX = 0 ; k < 3 ; k ++ ) {
      k1 = ( k+1 )%3 ;
      if ( x_line3d ( pCo[k], pCo[k1], lineX.xyzBeg, lineX.xyzEnd, &t, &t2 ) ) {
        /* Line and edge intersect, add. */
        egX = 1 ;
        pVxFc2[0] = pVxFc[k] ;
        pVxFc2[1] = pVxFc[k1] ;
        add_2_face ( pUns, pVxFc2, t ) ; 
      }
    }

    if ( !egX && solve_x_tri ( pCo, xInt[0] ) ) {
      add_3_face ( pUns, pVxFc, xInt[0] ) ;
      return ( 1 ) ;
    }
  }
  else {
    /* Quad.  */
    pCo[0] = pVxFc[0]->Pcoor ;
    pCo[1] = pVxFc[1]->Pcoor ;
    pCo[2] = pVxFc[2]->Pcoor ;
    pCo[3] = pVxFc[3]->Pcoor ;

    /* First check whether the line intersects with any edge of the face. */      
    for ( k = egX = 0 ; k < 4 ; k ++ ) {
      k1 = ( k+1 )%4 ;
      if ( x_line3d ( pCo[k], pCo[k1], lineX.xyzBeg, lineX.xyzEnd, &t, &t2 ) ) {
        /* Line and edge intersect, add. */
        egX = 1 ;
        pVxFc2[0] = pVxFc[k] ;
        pVxFc2[1] = pVxFc[k1] ;
        add_2_face ( pUns, pVxFc2, t ) ; 
      }
    }

    if ( !egX ) {
      
      /* No edge intersection. Split into triangles both ways and average. */
      pCo[0] = pVxFc[0]->Pcoor ;
      pCo[1] = pVxFc[1]->Pcoor ;
      pCo[2] = pVxFc[2]->Pcoor ;
      inFc[0] = solve_x_tri ( pCo, xInt[0] ) ;
      
      /*pCo[0] = pVxFc[0]->Pcoor ;*/
      pCo[1] = pVxFc[2]->Pcoor ;
      pCo[2] = pVxFc[3]->Pcoor ;
      inFc[1] = solve_x_tri ( pCo, xInt[1] ) ;

      if ( inFc[0] || inFc[1] ) {
        /* Match, try the other triangulation for averaging. */
        pCo[0] = pVxFc[0]->Pcoor ;
        pCo[1] = pVxFc[1]->Pcoor ;
        pCo[2] = pVxFc[3]->Pcoor ;
        inFc[2] = solve_x_tri ( pCo, xInt[2] ) ;
        
        pCo[0] = pVxFc[1]->Pcoor ;
        pCo[1] = pVxFc[2]->Pcoor ;
        /*pCo[2] = pVxFc[3]->Pcoor ;*/
        inFc[3] = solve_x_tri ( pCo, xInt[3] ) ;

        add_4_face ( pUns, pVxFc, inFc, xInt ) ;
        return ( 1 ) ;
      }
    }
  }

  /* No intersection. */
  return ( 0 ) ;
}
/******************************************************************************

  line_x_vxElem:
  Check whether the line intersects with vertices of the element.
  
  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  : conceived.
  
  Input:
  ------
  pElem:
  
  Returns:
  --------
  1 if all intersections are nodal, 0 otherwise.
  
*/

static int line_x_vxElem ( uns_s *pUns, elem_struct *pElem ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mDim = pElT->mDim, mVx = pElT->mVerts ;

  int kVx, k, kMax = lineX.kMax, mCross = 0 ;
  double *pCo, d, s, x ;
  vrtx_struct *pVx, *pVxZero[MAX_VX_ELEM] ;
  
  /* Compute the distance d[kVx] of all nodes to the line. */
  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    pVx =  pElem->PPvrtx[kVx] ; 
    pCo = pVx->Pcoor ;

    /* Solve for s in pCo = xyzBeg + s*l1 using the kMax coor */
    d = pCo[kMax] - lineX.xyzBeg[kMax] ;
    s = d/lineX.l1[kMax] ;

    if ( s >= 0. && s <= lineX.len ) {
      /* Compute the distance of this point on the line with pCo. */
      for ( k = 0, d = 0. ; k < mDim ; k++ ) {
        x = lineX.xyzBeg[k] + s*lineX.l1[k] - pCo[k] ;
        d += x*x ;
      }
      
      if ( ABS( d ) < para_tol*Grids.epsOverlapSq ) {
        /* Point is close enough. */
        pVxZero[mCross++] = pVx ;
      }
    }
  }


  if ( mCross == 0 ) {
    /* No exact matches with vertices. */
    return ( 0 ) ;
  }
  else if ( mCross == 1 ) {
    /* One match, list that one. Hence another face as well. */
    add_vx ( pUns, pVxZero[0] ) ; 
    return ( 0 ) ;
  }
  else if ( mCross == 2 ) {
    /* 2 exact matches, we're done. */
    add_vx ( pUns, pVxZero[0] ) ; 
    add_vx ( pUns, pVxZero[1] ) ; 
    return ( 1 ) ;
  }
  else {
    sprintf ( hip_msg, "%d forming nodes of element %"FMT_ULG" are on the line. Disregarded.", 
              mCross, pElem->number ) ;
    hip_err( warning, 1, hip_msg ) ;
  }

  return ( 0 ) ;
}


/******************************************************************************

  line_x_elem:
  Estimate whether a line can crosse an element. Calculate the gravity center,
  and a containing circle. Then calculate the normal distance to the line
  from the center and compare to the radius.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem:
  
  Returns:
  --------
  1 on possible intersection, 0 otherwise.
  
*/

int line_x_elem ( elem_struct *pElem ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mDim = pElT->mDim, mVx = pElT->mVerts ;

  int kVx ;
  double xCtr[MAX_DIM], *pCo, rad, dx, dy, dz = 0., d ;

  /* Centroid. */
  xCtr[0] = xCtr[1] = xCtr[2] = 0 ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    pCo = pElem->PPvrtx[kVx]->Pcoor ;
    xCtr[0] += pCo[0] ;
    xCtr[1] += pCo[1] ;
    if ( mDim > 2 ) xCtr[2] += pCo[2] ;
  }
  xCtr[0] /= mVx ;
  xCtr[1] /= mVx ;
  xCtr[2] /= mVx ;

  /* Radius from centroid to furthest vertex. */
  rad = 0. ;
  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    pCo = pElem->PPvrtx[kVx]->Pcoor ;
    dx = xCtr[0] - pCo[0] ;
    dy = xCtr[1] - pCo[1] ;
    if ( mDim > 2 ) dz = xCtr[2] - pCo[2] ;
    
    d = dx*dx + dy*dy + dz*dz ;
    rad = MAX( rad, d ) ;
  }
  rad = sqrt( rad ) ;

  /* Distance from line to centroid. */
  dx = xCtr[0] - lineX.xyzBeg[0] ;
  dy = xCtr[1] - lineX.xyzBeg[1] ;
  if ( mDim > 2 ) dz = xCtr[2] - lineX.xyzBeg[2] ;
  d = dx*lineX.l1[0] + dy*lineX.l1[1] + dz*lineX.l1[2] ;

  /* Normal. */
  dx  = xCtr[0] - lineX.xyzBeg[0] - d*lineX.l1[0] ;
  dy  = xCtr[1] - lineX.xyzBeg[1] - d*lineX.l1[1] ;
  if ( mDim > 2 ) dz  = xCtr[2] - lineX.xyzBeg[2] - d*lineX.l1[2] ;
  d = dx*dx + dy*dy + dz*dz ;

  if ( d < close_fac*rad )
    /* Close enough to make a full check. */
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

/******************************************************************************

  cut_elems_by_line:
  Make a list of faces of elements cut by a line in 3 space.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  : conceived.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  lineX.
  
  Returns:
  --------
  1.
  
*/

static int cut_elems_by_line ( uns_s *pUns ) {

  const elemType_struct *pElT ;
  
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  vrtx_struct *pVxFc[MAX_VX_FACE] ;
  ulong_t mFc ;
  int mVxFc, kFc ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBf, *pBfBeg, *pBfEnd ;

  
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        pElT = elemType + pElem->elType ;
        mFc = pElT->mSides ;

        if ( line_x_elem ( pElem ) )
          if ( !line_x_vxElem ( pUns, pElem ) )
            /* Loop over all faces of the element. Only do each face once, */
            for ( kFc = 1 ; kFc <= mFc ; kFc++ )
              if ( face_is_rhs ( pElem, kFc, &mVxFc, pVxFc ) )
                /* This is a 'rhs' face. Calculate intersections and add to the list. */
                line_x_face ( pUns, mVxFc, pVxFc ) ;
      }


  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBfBeg, &pBfEnd ) )
    for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ )
      if ( pBf->Pelem && pBf->nFace && pBf->Pelem->number ) {
        pElem = pBf->Pelem ;
        kFc = pBf->nFace ;
        pElT = elemType + pElem->elType ;

        if ( line_x_elem ( pElem ) )
          /* Loop over all faces of the element. Only do each face once, here
             we need to look at the other side of the face. */
          if ( !face_is_rhs ( pElem, kFc, &mVxFc, pVxFc ) )
            /* This is a 'rhs' face. Calculate intersections and add to the list. */
            line_x_face ( pUns, mVxFc, pVxFc ) ;
      }
      
  return ( 1 ) ;
}

/******************************************************************************

  remove_dupl:
  Loop over all intersected faces, if duplicates of arclength are found,
  set the arclength to sth. beyond 1. A subsequent sort for arc-length
  will place them at the end where they can be easily removed.
  
  Last update:
  ------------
  : conceived.
  

  Changes To:
  -----------
  lineX.fcInt->t
  
  Returns:
  --------
  mFc, the number of distinct faces.
  
*/

static int remove_dupl ( ) {

  fcInt_s *pFc = lineX.fcInt ;
  double t ;
  int mFc = lineX.mFcInt ;

  if ( !lineX.mFcInt ) {
    hip_err ( warning, 1,"no faces for intersection found.\n" ) ;
    return ( 0 ) ;
  }

  t = pFc->t ;
  for ( pFc = lineX.fcInt+1 ; pFc < lineX.fcInt+lineX.mFcInt ; pFc++ ) {
    if ( pFc->t - t < distinct_fac ) {
      pFc->t += 99. ;
      mFc-- ;
    }
    else
      t = pFc->t ;
  }
  
  return ( mFc ) ;
}




/******************************************************************************

  inter_line:
  Given a lineX, do the interpolation bit.
  
  Last update:
  ------------
  6jul04; compute the integral along the line.
  : conceived.
  
  Input:
  ------
  pUns:
  fileName:
  var:      variable to interpolate for:
            1 .. mUnknowns just uses that variable,
            r,u,v,w,p,q,t do the obvious.

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int inter_line ( uns_s *pUns, char *fileName, char *var, double *pVdx ) {

  const int mDim  = pUns->mDim ;

  const fcInt_s *pFc ;
  const double *wt, *x, *xPrv = NULL ;

  
  FILE *fcut = NULL ;
  int iFc ;
  double v, vPrv=0, vdx = 0., vVx[MAX_VX_FACE], dx ;

  if ( fileName ) {
    /* Write results to file. */
    if ( !( fcut = fopen ( prepend_path( fileName ), "w" ) ) ) {
      printf ( " FATAL: could not open file %s\n", fileName ) ;
      return ( 0 ) ; }

    fprintf ( fcut, "# variable %s from %g, %g, %g to  %g, %g, %g.\n",
              var,
              lineX.xyzBeg[0], lineX.xyzBeg[1], lineX.xyzBeg[2],
              lineX.xyzEnd[0], lineX.xyzEnd[1], lineX.xyzEnd[2] ) ;
  }



  for ( iFc = 0 ; iFc < lineX.mFcInt ; iFc++ ) {
    pFc = lineX.fcInt + iFc ;
    wt = pFc->wt ;

    if ( pUns->varList.varType == noVar ) {
      v = pFc->xInt[0] ;
    }
    else {
      vVx[0] = get_var( &pUns->varList, pFc->pVx[0]->Punknown, var ) ;
      vVx[1] = get_var( &pUns->varList, pFc->pVx[1]->Punknown, var ) ;
      vVx[2] = get_var( &pUns->varList, pFc->pVx[2]->Punknown, var ) ;
      vVx[3] = get_var( &pUns->varList, pFc->pVx[3]->Punknown, var ) ;

      v = wt[0]*vVx[0] + wt[1]*vVx[1] + wt[2]*vVx[2] + wt[3]*vVx[3] ;
      x = pFc->xInt ;

      /* Compute the integral. */
      if ( iFc ) {
        dx = sqrt( sq_distance_dbl ( xPrv, x, mDim ) ) ;
        vdx += .5*(v+vPrv)*dx ;
      }
      vPrv = v ;
      xPrv = x ;
    }

    if ( fileName ) 
      fprintf ( fcut, "%16.8e %16.8e %16.8e  %16.8e  %16.8e  %16.8e\n",
                pFc->xInt[0], pFc->xInt[1], pFc->xInt[2], pFc->t, v, vdx ) ;
  }

  if ( fileName ) 
    fclose ( fcut ) ;

  *pVdx = vdx ;

  return ( 1 ) ;
}

/******************************************************************************

  uns_int_line:
  Given a line in space, interpolate values at each intersection with a face.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  xyzBeg/End: beginning and end of the line.
  fileName:
  var:        variable to interpolate for. See inter_line.

  Changes To:
  -----------
  lineX.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double uns_int_line ( double xyzBeg[3], double xyzEnd[3],
                      char *fileName, char *var ) {

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  int mDistFc, ivar = var[0], k ;
  double cMax, cA, vdx ;

  if (  Grids.PcurrentGrid->uns.type != uns ) {
    hip_err ( fatal,0,"uns_int_line only works on unstructured grids." ) ;
    return ( 0. ) ; }

  if ( verbosity > 2 ) {
    sprintf ( hip_msg,
              "writing interpolated line cut for %s  to %s.\n", var, fileName);
    hip_err ( info, 1, hip_msg ) ;
  }
  
  if ( lineX.mFcInt )
    /* There is an existing line. Is it the same? */
    if ( xyzBeg[0] != lineX.xyzBeg[0] || xyzBeg[0] != lineX.xyzBeg[0] ||
         xyzBeg[1] != lineX.xyzBeg[1] || xyzBeg[1] != lineX.xyzBeg[1] ||
         xyzBeg[2] != lineX.xyzBeg[2] || xyzBeg[2] != lineX.xyzBeg[2] ) {
      /* They are different. Make a new one. */
      arr_free ( lineX.fcInt ) ;
      lineX.fcInt = NULL ;
      lineX.mFcInt = lineX.mFcAlloc = 0 ;
    }


  
  if ( !lineX.mFcInt ) {
    vec_copy_dbl ( xyzBeg, MAX_DIM, lineX.xyzBeg ) ;
    vec_copy_dbl ( xyzEnd, MAX_DIM, lineX.xyzEnd ) ;

    lineX.len = sqrt( sq_distance_dbl ( xyzEnd, xyzBeg, MAX_DIM ) ) ;
    if ( lineX.len < 1.e-20 ) {
      sprintf ( hip_msg, "the given line is too short: %g\n", lineX.len ) ;
      hip_err ( warning, 1, hip_msg ) ;
      return ( 0 ) ; }
   
    /* Find a bounding box for the line.
    vec_min_dbl ( xyzBeg, xyzEnd, MAX_DIM, lineX.ll ) ;
    vec_max_dbl ( xyzBeg, xyzEnd, MAX_DIM, lineX.ur ) ; */

    /* Unit vector. */
    vec_diff_dbl ( xyzEnd, xyzBeg, MAX_DIM, lineX.l1 ) ;
    vec_norm_dbl ( lineX.l1, MAX_DIM ) ;
    
    /* Direction of the largest comp. */
    cMax = ABS( lineX.l1[0] ) ;
    for ( lineX.kMax = 0, k = 1 ; k < MAX_DIM ; k++ ) {
      cA = ABS ( lineX.l1[k] ) ;
      if ( cA > cMax ) {
        cMax = cA ;
        lineX.kMax = k ;
      }
    }
    

    /* Find the intersections. */
    cut_elems_by_line ( pUns ) ;

    /* Order them. */
    qsort ( lineX.fcInt, lineX.mFcInt, sizeof( fcInt_s ), fcInt_cmp ) ;

    /* Remove duplicates. */
    mDistFc = remove_dupl () ;
    qsort ( lineX.fcInt, lineX.mFcInt, sizeof( fcInt_s ), fcInt_cmp ) ;

    lineX.mFcInt = lineX.mFcAlloc = mDistFc ;
    lineX.fcInt = arr_realloc ( "lineX.mFcInt", pUns->pFam,
                                lineX.fcInt, lineX.mFcAlloc, sizeof( *lineX.fcInt ) ) ;

    if ( verbosity > 2 )
      printf ( "    INFO: found %d intersected faces.\n", lineX.mFcInt ) ;
  }


  if ( isalpha(ivar) )
    conv_uns_var ( pUns, prim ) ;

  

  /* Interpolate. */
  inter_line ( pUns, fileName, var, &vdx ) ;
  
  return ( vdx ) ;
}


/******************************************************************************

  integrate_plan:

  Given a rectangle, integrate the values along a grid of lines
  perpendicular to the rectangle. 3D only.
  
  Last update:
  ------------
  6Feb06; change the way xBeg is incremented to allow for left-handed specification.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int integrate_rectangle ( double swf[], double sef[], double nwf[], double swr[], 
                          int mE, int mN, char *fileName, char *var) {

  FILE * fRect ;

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  double e[3], n[3], r[3], vdx, xBeg[3], xEnd[3], cMax, cA ;
  int k, kE, kN, ivar = var[0] ;

  if ( Grids.PcurrentGrid->uns.type != uns ) {
    printf ( " SORRY: integrate_rectangle does only unstructured grids.\n" ) ;
    return ( 0 ) ; }
  else if ( pUns->mDim != 3 ) {
    printf ( " SORRY: integrate_rectangle does only 3D.\n" ) ;
    return ( 0 ) ; }


  /* Variable. */
  if ( isalpha(ivar) )
    conv_uns_var ( pUns, prim ) ;

 



  /* Compute the vectors along the 3 sides of the box. */
  for ( k = 0 ; k < 3 ; k++ ) {
    e[k] = sef[k] - swf[k] ;
    n[k] = nwf[k] - swf[k] ;
    r[k] = swr[k] - swf[k] ;
  }





  /* New line. */
  arr_free ( lineX.fcInt ) ;
  lineX.fcInt = NULL ;
  lineX.mFcInt = lineX.mFcAlloc = 0 ;



  /* Initialise the line. */
  vec_copy_dbl ( swf, 3, lineX.xyzBeg ) ;
  vec_copy_dbl ( swr, 3, lineX.xyzEnd ) ;

  lineX.len = sqrt( sq_distance_dbl ( swf, swr, 3 ) ) ;
  if ( lineX.len < 1.e-20 ) {
    printf ( " FATAL: the given line is too short: %g\n", lineX.len ) ;
    return ( 0 ) ; }
   
  /* Find a bounding box for the line.
     vec_min_dbl ( xyzBeg, xyzEnd, MAX_DIM, lineX.ll ) ;
     vec_max_dbl ( xyzBeg, xyzEnd, MAX_DIM, lineX.ur ) ; */

  /* Unit vector. */
  vec_diff_dbl ( swr, swf, 3, lineX.l1 ) ;
  vec_norm_dbl ( lineX.l1, 3 ) ;
    
  /* Direction of the largest comp. */
  cMax = ABS( lineX.l1[0] ) ;
  for ( lineX.kMax = 0, k = 1 ; k < MAX_DIM ; k++ ) {
    cA = ABS ( lineX.l1[k] ) ;
    if ( cA > cMax ) {
      cMax = cA ;
      lineX.kMax = k ;
    }
  }
    




  /* Header. */
  if ( !( fRect = fopen ( prepend_path( fileName ), "w" ) ) ) {
    printf ( " FATAL: could not open file %s\n", fileName ) ;
    return ( 0 ) ; }

  fprintf ( fRect, "# variable %s, %d x %d points, from %g, %g, %g to  %g, %g, %g.\n",
            var, mE, mN, 
            swf[0], swf[1], swf[2],
            swf[0]+e[0]+n[0]+r[0], swf[1]+e[1]+n[1]+r[1], swf[2]+e[2]+n[2]+r[2] ) ;





  /* Scan the rectangle. */
  for ( kE = 0 ; kE < mE ; kE++ )
    for ( kN = 0 ; kN < mN ; kN++ ) {
      
      /* Line coordinates at this grid point. */
      for ( k = 0 ; k < 3 ; k++ ) {
        xBeg[k] = swf[k] + ((double) kE)/(mE-1.)*e[k] + ((double) kN)/(mN-1.)*n[k] ;
        xEnd[k] = xBeg[k] + r[k] ;
      }
      vec_copy_dbl ( xBeg, 3, lineX.xyzBeg ) ;
      vec_copy_dbl ( xEnd, 3, lineX.xyzEnd ) ;


      /* lineX.mFcInt = 0 ;  */
      /* vdx = uns_int_line ( lineX.xyzBeg, lineX.xyzEnd, NULL, var ) ; */

      /* Find the intersections. */
      lineX.mFcInt = 0 ;
      cut_elems_by_line ( pUns ) ;

      /* Order them. */
      qsort ( lineX.fcInt, lineX.mFcInt, sizeof( fcInt_s ), fcInt_cmp ) ;

      inter_line ( pUns, NULL, var, &vdx ) ;

      fprintf ( fRect, "%16.8e %16.8e %16.8e %16.8e\n",
                xBeg[0], xBeg[1], xBeg[2], vdx ) ;
    }

  fclose ( fRect ) ;

  return ( 1 ) ;
}

