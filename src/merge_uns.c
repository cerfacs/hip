/*
   uns_meth.c
   Unstructured grid methods.

   Last update:
   ------------
   9Jul11; remove ref to cutFc_toBnd.
   9Oct98; remove uns_matchFc in merge_uns, remove interior boundary faces in check_uns.
   3Aug98; remove fc2el_struct.
   29Aug96; cut out of uns_meth.c

   Contains:
   ---------
   add_uns:
   merge_uns:
   cmp_unsVrtx:
   vrtx2coor:
*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern int verbosity ;
extern int check_lvl ;
extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;

extern const int dg_fix_lrgAngles ;
extern const double dg_lrgAngle ;

extern char hip_msg[] ;


/******************************************************************************

  add_uns:
  Add unstructured grids together, i.e. share the overhead data structures,
  but don't eliminate duplcate nodes or faces.

  Last update:
  ------------
  19Dec17; rename to add_uns_grid, to clarify intent.
  19Dec14; retain user-defined Grids.epsOverlap, if smaller.
  27Jun14; use modified interface to zone_elem_mod_all.
  15Jul13; use zone_loop, zone_add instead of directly accessing the data.
  29Aug11; move zones across.
  4Sep05; track smallest epsoverlap
  : conceived.

  Input:
  ------
  PgridTo:    Chunk to add to,
  PgridAdded: chunk to be added.

  Changes To:
  -----------
  *PgridTo:
  *PgridAdded:

  Returns:
  --------
  0 on failure, 1 on success.

*/

ret_s add_uns_grid ( uns_s *pUnsTo, uns_s *pUnsAttached ) {

  ret_s ret = ret_success () ;
  chunk_struct *pChunk ;

  if ( verbosity > 2 ) {
    /* Info. */
    sprintf ( hip_msg, "\n  Adding grid %d to grid %d.",
              pUnsAttached->nr, pUnsTo->nr ) ;
    hip_err ( blank, 1, hip_msg ) ;
  }


  /* Keep the smallest epsoverlap. */
  pUnsTo->hMin = MIN( pUnsTo->hMin, pUnsAttached->hMin ) ;
  Grids.epsOverlap = MIN( Grids.epsOverlap, .9*pUnsTo->hMin ) ;
  Grids.epsOverlapSq = Grids.epsOverlap*Grids.epsOverlap ;
  pUnsTo->epsOverlap = Grids.epsOverlap ;
  pUnsTo->epsOverlapSq = Grids.epsOverlapSq ;


  /* Retag elements for new zone numbers, move zones to new mesh. */
  int nOldZone ;
  zone_s *pZ = NULL, *pNewZn ;
  while ( ( nOldZone = zone_loop ( pUnsAttached, &pZ ) ) ) {
    /* Filled zone, add. */
    pNewZn = zone_copy ( pUnsTo, pZ ) ;
    zone_elem_mod_all ( pUnsAttached, nOldZone, pNewZn ) ;
  }

  /* Move slidingPlane pairs. Collect pairs later. */
  int mSpSides = move_slidingPlaneSides ( pUnsTo, pUnsAttached ) ;


  /* Travel to the end of the list of chunks in pUnsTo. */
  for ( pChunk = pUnsTo->pRootChunk ; pChunk->PnxtChunk ;
        pChunk = pChunk->PnxtChunk )
    ;

  /* Append the chunks of pUnsAttached. */
  pChunk->PnxtChunk = pUnsAttached->pRootChunk ;
  pUnsAttached->pRootChunk->PprvChunk = pChunk ;

  /* Loop over all appended grids and renumber. */
  for ( pChunk = pUnsAttached->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    pChunk->nr = pChunk->PprvChunk->nr + 1 ;

  /* Close the linked list of Grids. */
  if ( pUnsAttached->pGrid->uns.PprvGrid )
    pUnsAttached->pGrid->uns.PprvGrid->uns.PnxtGrid =
      pUnsAttached->pGrid->uns.PnxtGrid ;
  else
    /* pGridAdded was the first grid. Update Grids. */
    Grids.PfirstGrid = pUnsAttached->pGrid->uns.PnxtGrid ;
  if ( pUnsAttached->pGrid->uns.PnxtGrid )
    pUnsAttached->pGrid->uns.PnxtGrid->uns.PprvGrid = pUnsAttached->pGrid->uns.PprvGrid ;
  else
    /* pGridAdded was the last grid. Update Grids. */
    Grids.PlastGrid = pUnsAttached->pGrid->uns.PprvGrid ;


  /* Destroy any periodic setup. */
  if ( pUnsTo->mPerBcPairs ) {
    arr_free ( pUnsTo->pPerBc ) ;
    pUnsTo->pPerBc = NULL ;
  }
  pUnsTo->mPerBcPairs = 0 ;


  /* Remove the empty grid.
  arr_free ( pUnsAttached ) ; */
  arr_free ( pUnsAttached->pGrid ) ;

  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  add_uns_grids:
*/
/*! parse command line string for attaching one grid to another.
 *
 */

/*

  Last update:
  ------------
  9Dec19: extracted from attach_menu.


  Input:
  ------
  line: command line string

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s add_uns_grids ( char *line ) {
  ret_s ret = ret_success () ;
  char keyword[LINE_LEN] ;
  size_t keyLen ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;
  else {
    strncpy ( keyword, "attach ", 8 ) ;
    //strcat ( keyword, " " ) ;
    size_t keyLen = strlen ( keyword ) ;
    strncpy ( line, keyword, keyLen+1 ) ;
    /* Stick the keyword at the beginning of the argLine, similar to getopt. */
    if ( !eo_buffer () )
      read1line ( line+keyLen ) ;
  }

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( line, &ppArgs ) ;
  char exprGridTo[LINE_LEN] = {'\0'} ;
  int doMerge = 1 ;

  /* Parse line of unix-style optional args. */
  char c ;
  /* Set the arg counter back, global variable incremented by getopt. */
  optind = 1 ;
  /* colon behind arg means argument to be read,
     double colon means optional argument, but needs to be joined: -a0, not -a 0. */
  while ((c = getopt_long ( mArgs, ppArgs, "m::t:",NULL,NULL)) != -1) {
    switch (c)  {
    case 'm':
      if ( optarg && atoi( optarg ) == 0 )
        // Don't merge.
        doMerge = 0 ;
      break;
    case 't':
      if ( !optarg || optarg[0] == '\0' )
        /* Target grid. no arg or 0 arg given.  */
        exprGridTo[0] = '\0' ;
      else
        /* No arg given, or arg not 0, use -a1 */
        strncpy ( exprGridTo, optarg, LINE_LEN ) ;
      break;
    case '?':
      if (isprint (optopt)) {
        sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
      else {
        sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }

  uns_s *pUnsTo = NULL ;
  if ( !Grids.PcurrentGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to attach." ) ;
    ret = hip_err ( warning, 0, hip_msg ) ;
    flush_buffer () ;
    return ( ret ) ;
  }
  else if ( exprGridTo[0] != '\0' )  {
    /* Does it exist, and is it uns? */
    if ( ( pUnsTo = find_uns_expr ( exprGridTo ) ) ) {
      /* Then make it current. */
      set_current_grid_expr ( exprGridTo ) ;
    }
    else {
      sprintf ( hip_msg, "The grid to attach to does not exist or is not uns." ) ;
      ret = hip_err ( warning, 0, hip_msg ) ;
      flush_buffer () ;
      return ( ret ) ;
    }
  }
  else
           /* Grid to attach to is the current grid. */
    pUnsTo = Grids.PcurrentGrid->uns.pUns ;

  char exprGridAttached[LINE_LEN] ;
  uns_s *pUnsAttached = NULL ;
  /* Grid[s] to be attached. */
  int k ;
  while ( optind < mArgs ) {                   \
    //%  while ( !eo_buffer () || !PgridAttached ) {
    strncpy( exprGridAttached, ppArgs[optind++], LINE_LEN ) ;
      //read1int ( &nGridAttached ) ;
    pUnsAttached = find_uns_expr ( exprGridAttached ) ;
    if ( !pUnsAttached ) {
      sprintf ( hip_msg, "grid matching `%s' to be attached could not be found.",
                exprGridAttached ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( pUnsAttached == pUnsTo ) {
      sprintf ( hip_msg, "cannot attach grid to itself." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( pUnsTo->mDim - pUnsAttached->mDim ) {
      sprintf ( hip_msg, "mismatching grid dimensions %d-%d.\n",
                pUnsTo->mDim, pUnsAttached->mDim ) ;
     ret =  hip_err ( fatal, 0, hip_msg ) ;
    }


    /* Attach the grid. */
    ret = add_uns_grid ( pUnsTo, pUnsAttached ) ;
    if ( ret.status != success ) {
      sprintf ( hip_msg, "could not attach unstructured grids in add_uns_grids." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  /* Merge the grid. */
  if ( doMerge ) {
    if ( !merge_uns ( pUnsTo, 0, 1 ) ) {
      printf ( "merging of unstructured grids in attach_uns failed in add_uns_grids." ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  else {
        /* Mark all used vertices and renumber. */
    validate_elem_onPvx ( pUnsTo ) ;
    /* Renumber continously into the attached chunks. */
    pUnsTo->numberedType = invNum ;
    number_uns_grid ( pUnsTo ) ;

    /* Remove duplicate faces. */
    rm_special_faces ( pUnsTo ) ;

    check_uns ( pUnsTo, check_lvl ) ;
  }


  return ( ret ) ;
}



/******************************************************************************

  cmp_unsVrtx:
  Compare an unstructured vertex with the ones present in the tree. If there
  is a duplicate, set the element to vertex pointer to the duplicate. If not,
  add it to the tree.

  Last update:
  ------------
  12Mar18; map vxCpt of decoupled/merged vx to the merged one.
  22Oct10; pass epsOverlap through interface
  15May06; replace flag vx->number = -1 with vx->mark2.
  4Jun96: conceived.

  Input:
  ------
  PPvrtx:     List of element to vertex pointers.
  mVrtx:      Number of ele to vert pointers to treat.
  Ptree:
  epsOverlap: Radius for vertex overlap.

  Changes to:
  -------
  *PPvrtx:
  *Ptree:

  Returns:
  --------
  1.

*/

static int cmp_unsVrtx ( vrtx_struct **PPvxFc[], const int mVrtx,
                         root_struct *Ptree, const int mDim, double epsOverlap ) {
  int kVert ;
  vrtx_struct *Pvrtx, *PtreeVrtx ;
  double nearestDist ;

  for ( kVert = 0 ; kVert < mVrtx ; kVert++ ) {
    Pvrtx = *PPvxFc[kVert] ;

    /* Search for the nearest vertex and check for overlap. */
    PtreeVrtx = nearest_data ( Ptree, ( DATA_TYPE *) Pvrtx, &nearestDist ) ;
    if ( Pvrtx == PtreeVrtx )
      /* This is its own self. Eg. a vertex in the same chunk at the
	 border of two boundary patches. This case would be dealt with
	 properly in the next if, but keep it separate for clarity. */
      ;
    else if ( nearestDist <= epsOverlap ) {
      /* Match. Replace this vertex pointer by the match and unmark the
	 vertex. */
      *PPvxFc[kVert] = PtreeVrtx ;
      Pvrtx->vxCpt = PtreeVrtx->vxCpt ;
      Pvrtx->mark2 = 1 ;
    }
    else
      /* Stick it in. */
      add_data ( Ptree, ( DATA_TYPE *) Pvrtx ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************
  merge_vrtx:   */

/*! Find and disconnect duplicate vertices in a single monolithic chunk.
 */

/*

  Last update:
  ------------
  6Feb12: derived from merge_uns.


  Input:
  ------
  pVrtx: list of vertices, with invalid zero element, numbered from 1.
  mVxInt: number of interior vertices that should not be duplicated.
  mVrtx: number of vertices total.

  Returns:
  --------
  number of duplicate/disconnected vertices.

*/

int merge_vrtx_chunk ( chunk_struct *pChunk, int mVxInt, int mVrtx ) {

  uns_s *pUns = pChunk->pUns ;
  /* Get the bounding box. */
  get_uns_box ( pUns ) ;

  /* Initialize the quadtree. Make it larger, such that the borders of
     the bounding box fit inside and not just on the edges.
     This can't be wrapped into ini_tree, as ini_tree does not
     only deal with floating point numbers. */
  int nDim, mDim = pUns->mDim ;
  double size ;
  double llBox[MAX_DIM], urBox[MAX_DIM] ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    size = .1*( pUns->urBox[nDim] - pUns->llBox[nDim] ) ;
    llBox[nDim] = pUns->llBox[nDim] - size ;
    urBox[nDim] = pUns->urBox[nDim] + size ;
  }

  kdroot_struct *pTree = kd_ini_tree ( pUns->pFam, "merge_vrtx", mDim,
                                       Grids.epsOverlap, llBox, urBox, vrtx2coor ) ;


  vrtx_struct *pVx ;
  vrtx_struct *pTreeVx ;
  double nearestDist ;
  int mDuplVx = 0 ;
  for ( pVx = pChunk->Pvrtx+1 ; pVx <= pChunk->Pvrtx+mVxInt ; pVx++ ) {
    /* Search for the nearest vertex and check for overlap. */
    pTreeVx = kd_nearest_data ( pTree, pVx, &nearestDist ) ;
    if ( pTreeVx && nearestDist <= Grids.epsOverlap ) {
      /* Match. Replace this vertex pointer by the match and unmark the
	 vertex. */
      pVx->number = pTreeVx->number ;
      pVx->vxCpt = pTreeVx->vxCpt ;
      mDuplVx++ ;
    }
    else
      /* Stick it in. */
      kd_add_data ( pTree, pVx ) ;
  }

  if ( mDuplVx ) {
    sprintf ( hip_msg, "found %d duplicate vertices in the volume part in merge_vrtx.",
              mDuplVx );
    hip_err ( warning, 1, hip_msg ) ;
  }


  /* Continue with boundary vertices, they are expected to duplicate. */
  for ( ; pVx <= pChunk->Pvrtx+mVrtx ; pVx++ ) {
    /* Search for the nearest vertex and check for overlap. */
    pTreeVx = kd_nearest_data ( pTree, pVx, &nearestDist ) ;
    if ( pTreeVx && nearestDist <= Grids.epsOverlap ) {
      /* Match. Replace this vertex pointer by the match and unmark the
	 vertex. */
      pVx->number = pTreeVx->number ;
      pVx->vxCpt = pTreeVx->vxCpt ;
      mDuplVx++ ;
    }
    else
      /* Stick it in. */
        kd_add_data ( pTree, pVx ) ;
  }

  kd_del_tree ( &pTree ) ;




  /* Replace all pointers in elements. */
  elem_struct *pElem ;
  vrtx_struct **ppVx ;
  int mVerts ;
  for ( pElem = pChunk->Pelem+1 ; pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) {
    ppVx = pElem->PPvrtx ;
    mVerts = elemType[ pElem->elType ].mVerts ;
    for ( ppVx = pElem->PPvrtx ; ppVx < pElem->PPvrtx + mVerts ; ppVx++ ) {
      pVx = de_cptVx( pUns, (*ppVx)->vxCpt ) ;
      *ppVx = pVx ;
    }
  }


  /* Replace pointers in boundary faces. */
  bndFc_struct *pBndFc ;
  vrtx_struct **ppVxFc[MAX_VX_FACE] ;
  vrtx_struct ***pppVx ;
  int mVxFc ;
  for ( pBndFc = pChunk->PbndFc + 1 ;
        pBndFc <= pChunk->PbndFc + pChunk->mBndFaces ; pBndFc++ )
    if ( pBndFc->Pelem ) {
      /* Get the vertex pointers and add them to the tree. */
      get_uns_face ( pBndFc->Pelem, pBndFc->nFace, ppVxFc, &mVxFc ) ;

      for ( pppVx = ppVxFc ; pppVx < ppVxFc + mVxFc ; pppVx++ ) {
        pVx = de_cptVx( pUns, (**pppVx)->vxCpt ) ;
        **pppVx = pVx ;
      }
    }



  /* Replace pointers in pbndFcVx. */
  bndFcVx_s *pBf ;
  for ( pBf = pUns->pBndFcVx ; pBf < pUns->pBndFcVx + pUns->mBndFcVx ; pBf++ ) {
    for ( ppVx = pBf->ppVx ; ppVx < pBf->ppVx + pBf->mVx ; ppVx++ ) {
      pVx = de_cptVx( pUns, (*ppVx)->vxCpt ) ;
      *ppVx = pVx ;
    }
  }


  return ( mDuplVx ) ;
}

/******************************************************************************

  vrtx2coor:
  From data to comparable contiguous values for the tree.

  Last update:
  ------------
  3Jun96; conceived.

  Input:
  ------
  Pdata: data pointer.

  Returns:
  --------
  Pdata->Pcoor:

*/

inline VALU_TYPE *vrtx2coor( const DATA_TYPE *Pdata ) {

  /* Suppress the compiler warning. */
  vrtx_struct *Pvrtx ;
  Pvrtx = ( vrtx_struct * ) Pdata ;
  return ( Pvrtx->Pcoor ) ;
}

/******************************************************************************
  merge_vx_face:   */

/*! given two matching faces, merge the vertices.
 *
 *
 */

/*

  Last update:
  ------------
  20Dec17; correct ULG format mismatch.
  1Oct16; update not only pVx in Elem, but also vxCpt in vx for elems that don't have
          a fixed face, but share an edge or a node.
  18Sep16: conceived.


  Input:
  ------
  pCh0: chunk of pEl0,
  pEl0: element 0, retain its vertex pointers.
  kFc0: face of elment 0 to match.
  pCh1: chunk of pEl1.
  pEl1: element 1, map the vx pointers of nFace to the matching ones of pEl0
  kFc1: face of element 1 to match.

  Changes To:
  -----------
  pEl1->PPvrtx

  Returns:
  --------
  1 on failure, 0 on success

*/

int merge_vx_face ( const int mDim,
                   elem_struct *pEl0, int kFc0,
                   elem_struct *pEl1, int kFc1 ) {

  int retVal = 0 ;

  const faceOfElem_struct *pFoE0 = elemType[pEl0->elType].faceOfElem + kFc0 ;
  const faceOfElem_struct *pFoE1 = elemType[pEl1->elType].faceOfElem + kFc1 ;

  const int mVxFc0 = pFoE0->mVertsFace ;
  const int mVxFc1 = pFoE1->mVertsFace ;

  if ( mVxFc0 != mVxFc1 ) {
    sprintf ( hip_msg, "mismatch of face type in merge_vx_face: %d vs %d", mVxFc0, mVxFc1 ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  vrtx_struct **ppVx0 = pEl0->PPvrtx ;
  vrtx_struct **ppVx1 = pEl1->PPvrtx ;
  const int *kVxFc0 = pFoE0->kVxFace ;
  const int *kVxFc1 = pFoE1->kVxFace ;



  // Find the permutation offset of 1 into 0
  vrtx_struct *pVx0 = ppVx0[ kVxFc0[0] ] ;
  int kkOff ;
  for ( kkOff = 0 ; kkOff < mVxFc0 ; kkOff++ )
    if ( sq_distance_dbl ( ppVx1[ kVxFc1[kkOff] ]->Pcoor, pVx0->Pcoor, mDim ) < Grids.epsOverlapSq ) {
      // match
      break ;
    }


  int kkVx1, kkVx0 ; // index in the face.
  int kVx1 ; // index in the element.
  vrtx_struct *pVx1 ;
  double sqDist ;
  if ( kkOff == mVxFc0 )
    hip_err ( fatal, 0, "could not match first face vertex in merge_vx_face." ) ;
  else {
    for  ( kkVx0  = 0 ; kkVx0 < mVxFc0 ; kkVx0++ ) {
      kkVx1 = (kkOff-kkVx0+3)%mVxFc0 ;
      pVx0 = ppVx0[ kVxFc0[kkVx0] ] ;
      kVx1 = kVxFc1[kkVx1] ;
      pVx1 = ppVx1[ kVx1 ] ;


      if ( kkVx0 )
        // Check vertex spatial match. Already done for kVx==0
        sqDist =  sq_distance_dbl ( pVx1->Pcoor, pVx0->Pcoor, mDim ) ;
      if ( !kkVx0 || sqDist < Grids.epsOverlapSq ) {
        // match. Set the vertex/chunk pointer of this vx to the new one,
        pVx1->vxCpt = pVx0->vxCpt ;
        // change the vx pointer in the element.
        ppVx1[ kVx1 ] = pVx0 ;
      }
      else {
        sprintf ( hip_msg, "mismatch of %g for vertex %d in elem %"FMT_ULG".",
                  sqDist, kkVx0, pEl0->number ) ;
        hip_err ( warning, 1, hip_msg ) ;
        retVal = 1 ;
      }
    }
  }

  return ( retVal ) ;
}



/******************************************************************************

  merge_uns:
  Loop over all matching interfaces between chunks and set the vertex pointers
  of the elements on the higher side of the face to the ones on the lower side.

  Last update:
  ------------
  21Apr20; replace cutMark with mark[0].
  16Dec19; check for pFc->pElem != NULL before evaluating element data. E.g.
           removing internal faces sets pBndFc->Pelem = NULL .
  1Apr19; rename arg doUseVxMark3, re-instate its purpose.
  21Sep18; run renumbering also when doCheck=0.
  6Sep18; rename reset_vx_mark. Intro doCheck.
  4Sep18; include nodes with vx->mark3=1, if doUseVxMark3 != 0.
  23Feb18; fix bug with PPvxFc offset by fcType1, rather than fcType0.
  12May17; rename rm_match_fc to rm_special_faces which also treats interfaces.
  1Jul16; new interface to check_uns.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  30Jun12; use new interfaect to validate_vert.
  22Oct10; new interface to cmp_unsVrtx
  16Oct10; use get_uns_box.
  15May06; replace vx->number == -1 with vx->mark2.
  9Oct98; remove uns_matchFc, remove interior boundary faces in check_uns.
  11Aug98; look for large face angles.
  2May96: conceived.

  Input:
  ------
  pUns:
  doUseVxark3: if 1, include also all vx with nonzero mark3 in the list to match,
               if 3, include only those.
  doCheck: if true, perform standard mesh checks.

  Changes to:
  -------
  *pUns:

  Returns:
  --------
  1/0: on success/failure.

*/

int merge_uns ( uns_s *pUns, const int doUseVxMark3, int doCheck ) {

  /* Get the bounding box. */
  get_uns_box ( pUns ) ;

  /* Initialize the quadtree. Make it larger, such that the borders of
     the bounding box fit inside and not just on the edges.
     This can't be wrapped into ini_tree, as ini_tree does not
     only deal with floating point numbers. */
  const int mDim = pUns->mDim ;
  int nDim ;
  double llBox[MAX_DIM], urBox[MAX_DIM], size ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    size = .1*( pUns->urBox[nDim] - pUns->llBox[nDim] ) ;
    llBox[nDim] = pUns->llBox[nDim] - size ;
    urBox[nDim] = pUns->urBox[nDim] + size ;
  }

  root_struct *Ptree = ini_tree ( pUns->pFam, "merge_uns", mDim,
                                  llBox, urBox, vrtx2coor ) ;

  /* Loop over the vertices of all faces, boundary, matching, internal, whatever, and
     check the tree for a duplicate vertex. If there is, set the element to
     vertex pointer to that one and unmark the vertex in the current
     chunk. If there isn't, stick it in the tree. */

  if ( doCheck && verbosity > 3 )
    printf ( "   Comparing vertices of chunk     " ) ;



  const elemType_struct *pElT ;
  int fcType0, fcType1, fcType, kFace, iVxFc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  bndPatch_struct *pBndPatch ;
  matchFc_struct *pMatchFc ;
  intFc_struct *pIntFc ;
  degenFc_struct *pDegenFc ;
  chunk_struct *pChunk ;
  elem_struct *pElem ;
  /* Note that the matching faces are done both sides at a time, hence the
     factor 2. */
  vrtx_struct **PPvxFc[2*(MAX_VX_FACE)], **PPvrtx ;
  vrtx_struct *pVx, *pVx2 ;
  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    if ( doCheck && verbosity > 3 ) {
      printf ( "\b\b\b\b%4d", pChunk->nr ) ;
      fflush ( stdout ) ; }

    if ( doUseVxMark3 ) {
      /* Add all interface nodes tagged with mark3. */
      for ( pVx = pChunk->Pvrtx+1 ;
            pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ )
        if ( pVx->number && pVx->mark3 ) {
          /* cmp_unsVrtx changes the value of the vertex pointer. Make
             a changeable copy. */
          pVx2 = pVx, PPvxFc[0] = &pVx2 ;
          cmp_unsVrtx ( PPvxFc, 1, Ptree, mDim, pUns->epsOverlap ) ;
        }
    }

    if ( doUseVxMark3 < 2 ) {
      /* Also look for merges on boundary faces. */

      /* Boundary faces. */
      for ( pBndFc = pChunk->PbndFc + 1 ;
            pBndFc <= pChunk->PbndFc + pChunk->mBndFaces ; pBndFc++ ) {
        if ( pBndFc->Pelem ) {
          if ( pBndFc->Pelem->number && pBndFc->nFace ) {
            /* Get the vertex pointers and add them to the tree. */
            get_uns_face ( pBndFc->Pelem, pBndFc->nFace, PPvxFc, &fcType ) ;
            cmp_unsVrtx ( PPvxFc, fcType, Ptree, mDim, pUns->epsOverlap ) ;
          }
        }
      }

      /* Loop over all matching faces of this chunk. */
      for ( pMatchFc = pChunk->PmatchFc + 1 ;
            pMatchFc <= pChunk->PmatchFc + pChunk->mMatchFaces ; pMatchFc++ ) {
        /* Get the vertex pointers on both sides. */
        if ( pMatchFc->pElem0 ) {
          if ( pMatchFc->pElem0->number && pMatchFc->nFace0 )
            set_vx_mark_face_k ( pMatchFc->pElem0, pMatchFc->nFace0, 2 ) ;
        }
        if ( pMatchFc->pElem1 ) {
          if ( pMatchFc->pElem1->number && pMatchFc->nFace1 ) {
            /* Get the vertex pointers on both sides. */
            get_uns_face ( pMatchFc->pElem0, pMatchFc->nFace0, PPvxFc, &fcType0 ) ;
            get_uns_face ( pMatchFc->pElem1, pMatchFc->nFace1, PPvxFc+fcType0, &fcType1 ) ;
            cmp_unsVrtx ( PPvxFc, fcType0+fcType1, Ptree, mDim, pUns->epsOverlap ) ;
          }
        }
      }

      /* Internal, ie. cut faces. */
      for ( pIntFc = pChunk->PintFc + 1 ;
            pIntFc <= pChunk->PintFc + pChunk->mIntFaces ; pIntFc++ ) {
        /* Get the vertex pointers. */
        if ( pIntFc->Pelem ) {
          if ( pIntFc->Pelem->number && pIntFc->nFace ) {
            /* Get the vertex pointers. */
            get_uns_face ( pIntFc->Pelem, pIntFc->nFace, PPvxFc, &fcType ) ;
            cmp_unsVrtx ( PPvxFc, fcType, Ptree, mDim, pUns->epsOverlap ) ;
          }
        }
      }

      /* Degenerate faces. */
      for ( pDegenFc = pChunk->PdegenFc + 1 ;
            pDegenFc <= pChunk->PdegenFc + pChunk->mDegenFaces ; pDegenFc++ ) {
        /* Get the vertex pointers. */
        if ( pDegenFc->Pelem ) {
          if ( pDegenFc->Pelem->number && pDegenFc->nFace ) {
            /* Get the vertex pointers. */
            get_uns_face ( pDegenFc->Pelem, pDegenFc->nFace, PPvxFc, &fcType ) ;
            cmp_unsVrtx ( PPvxFc, fcType, Ptree, mDim, pUns->epsOverlap ) ;
          }
        }
      }
    }
  }


  if ( doCheck && verbosity > 3 )
    printf ( "\n" ) ;


  /* In each element, replace the vertex pointers that have become unmarked.
     This will merge vertices of elements that touch the interface without a face,
     of if interface nodes have only been tracked with a mark.
      */
  pChunk = NULL ;
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    for ( pElem = pChunk->Pelem+1 ;
          pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ )
      if ( pElem->number )
        for ( PPvrtx = pElem->PPvrtx ;
              PPvrtx < pElem->PPvrtx + elemType[pElem->elType].mVerts ; PPvrtx++ )
          if ( (*PPvrtx)->mark2 ) {
            /* This is a replaced pointer. */
            cmp_unsVrtx ( &PPvrtx, 1, Ptree, mDim, pUns->epsOverlap ) ;
            //*PPvrtx = nearest_data ( Ptree, ( DATA_TYPE *) *PPvrtx, &nearestDist ) ;
          }
  }


  if ( doCheck && verbosity > 2 ) {
    int mVxDuplicated = 0 ;
    vrtx_struct *pVx ;

    /* Count the number of replaced vertices. */
    pChunk = NULL ;
    for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
      for ( pVx = pChunk->Pvrtx+1 ; pVx <=  pChunk->Pvrtx+pChunk->mVerts ; pVx++ )
        if ( pVx->mark2 )
          mVxDuplicated++ ;

    sprintf ( hip_msg, "removed %d duplicated vertices.", mVxDuplicated ) ;
    hip_err ( info, 1, hip_msg ) ;
  }

  /* Remove the tree. */
  del_tree ( &Ptree ) ;


  double maxAngle ;
  pUns->mCutElems = 0 ;
  if ( doCheck && dg_fix_lrgAngles ) {
    /* Use first mark as 'cutMark' */
    reserve_elem_mark ( pUns, CUT_MARK, "merge_uns max dg_fix_lrgAngles" ) ;

    /* Loop over all faces of all terminal elements and calculate maximum face angles. */
    for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
      for ( pElem = pChunk->Pelem+1 ;
            pElem <= pChunk->Pelem + pChunk->mElems ; pElem++ ) {
        //pElem->cutMark = 0 ;
        reset_elem_mark ( pElem, CUT_MARK ) ;
        if ( pElem->term ) {
          pElT = elemType + pElem->elType ;
          /* OK, 3-D only, so far. */
          for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
            maxAngle = get_face_lrgstAngle ( pElem, kFace, &iVxFc ) ;
            if ( maxAngle < dg_lrgAngle ) {
              // pElem->cutMark = 1 ;
              set_elem_mark ( pElem, CUT_MARK ) ;
              pUns->mCutElems++ ;
              break ;
            }
          }
        }
      }

    /* Count the number of affected boundary faces. */
    pChunk = NULL ;
    pUns->mCutBndFc = 0 ;
    while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        //if ( pBndFc->Pelem->cutMark )
        if ( elem_has_mark ( pBndFc->Pelem, CUT_MARK ) )
             pUns->mCutBndFc++ ;

    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "found %"FMT_ULG" elems %"FMT_ULG" bndFc with angles larger than %g.\n",
                pUns->mCutElems, pUns->mCutBndFc, dg_lrgAngle ) ;
      hip_err ( info, 3, hip_msg ) ;
    }

    /* Released in check_elem, after the elems with too large fc angels have been split.
       release_elem_mark ( pUns, 0 ) ;
    */

  }// if ( doCheck && dg_fix_lrgAngles ) {


  if ( doCheck ) {
    /* Mark all used vertices and renumber. */
    validate_elem_onPvx ( pUns ) ;
    /* Renumber continously into the attached chunks. */
    pUns->numberedType = invNum ;
    number_uns_grid ( pUns ) ;

    /* Remove duplicate faces. */
    rm_special_faces ( pUns ) ;

    check_uns ( pUns, check_lvl ) ;
  }
  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  merge_zones:
*/
/*! merge zoned parts of a mesh.
 *
 */

/*
  
  Last update:
  ------------
  26Feb22; derived from zone_merge.
  

  Input:
  ------
  pUns: grid
  mZ: number of zones to retain in numbering
  iZones: list of zones
  doReset: if nonz=-zero, reset numbering.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


int merge_regions ( uns_s *pUns, const int mReg, const int iReg[], const int doReset ) {

  
  /* Renumber, considering only the mZ zones in iZoneos. */
  const int doUseMark=1 ;
  number_uns_elems_in_regions ( pUns, leaf, mReg, iReg, doReset, doUseMark ) ;

  
  /* Merge the grid. Include nodes on interfaces, carrying vxMark3.*/
  if ( !merge_uns ( pUns, 1, 0 ) ) {
    sprintf ( hip_msg,
              "merging of unstructured grids failed in zone_merge.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 0 ) ;
}
