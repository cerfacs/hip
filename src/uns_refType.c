/*
  uns_refType:
  Take the connectivity info of the children, a permuted vertex pattern
  in the parent, and fill a refinement pattern.
  
  Last update:
  ------------
  6Jan15; rename pUnsInit to more descriptive pArrFamUnsInit
  27Oct97; make all internal functions static, prototype them here.
  7May97; realloced array of refTypes rather than a linked list.
  
  This file contains:
  -------------------
  add_uns_refType
  make_uns_refType
  face_include
  face_match
  rot_edge
  rot_edge
  get_uns_reffaces

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"

extern const int verbosity ;
extern elemType_struct elemType[] ;
extern const int bitEdge[MAX_EDGES_ELEM] ;
extern arrFam_s *pArrFamUnsInit ;

static int make_uns_refType ( const refType_struct *PrefType, const elem_struct *Pelem,
			      refType_struct *PrefT );

static int face_include ( const int mVxChFc, const int nVxChFc[MAX_VX_FACE],
			  int nVxPrtFc[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
			  const int mVxPrtFc[MAX_FACES_ELEM+1], 
			  const int mChildsFc[MAX_FACES_ELEM+1], const int mPrtFc, 
			  int *PkPrtFc, int *PkRefFc );
static int face_match ( const refType_struct *Prt,
			const int mVxChFc, const int nVxChFc[MAX_VX_FACE],
			const int kChild, int *PkSibl, int *PkSbFc );

static int rot_edge ( const elem_struct *Pelem, int kEdge, int *PkRotEdge );
static int rot_face ( const elem_struct *Pelem, int kFace, int *PkRotFace );

static int get_uns_reffaces
( const refType_struct *Prt, const elemType_struct *PelT,
  int nVxPrtFc[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
  int mVxPrtFc[MAX_FACES_ELEM+1], int mChildsFc[MAX_FACES_ELEM+1] );




/******************************************************************************

  add_uns_refType:
  
  
  Last update:
  ------------
  6Jan15; rename pUnsInit to more descriptive pArrFamUnsInit
  : conceived.
  
  Input:
  ------
  PrefType: The initialized sample pattern.
  Pelem     The element of proper type with rotated vertex numbers.

  Changes To:
  -----------
  elemType[Pelem->elType]:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_uns_refType ( const refType_struct *PsampleRefType, const elem_struct *Pelem ) {
  
  refType_struct *PrefType, *PnewRefType ;
  elemType_struct *PelT = elemType + Pelem->elType ;

  /* Increment counter. */
  PelT->mRefTypes++ ;

  /* Realloc for another refType. */
  PrefType = PelT->PrefType = 
    arr_realloc ( "PrefType in add_uns_refType", pArrFamUnsInit, 
                  PelT->PrefType, PelT->mRefTypes, sizeof ( refType_struct ));
  
  if ( !PrefType ) {
    hip_err ( fatal, 0, "could not alloc a new refType in add_uns_refType." ) ;
  }
  PnewRefType = PrefType + PelT->mRefTypes - 1 ;
  PnewRefType->nr = PelT->mRefTypes - 1 ;

  /* Fill it. */
  PnewRefType->refOrBuf = ref ;
  if ( !make_uns_refType ( PsampleRefType, Pelem, PnewRefType ) ) {
    hip_err ( fatal, 0, "could not make refType in add_uns_refType." ) ;
    return ( 0 ) ;
  }
  else
    return ( 1 ) ;
}

/******************************************************************************

  make_uns_refType:
  Calculate all the information stored in a refType given the parent element
  and the vertex positions of the children's vertices in the parent.
  
  Last update:
  ------------
  15Jul98; intro mChElem2Vert ;
  : conceived.
  
  Input:
  ------
  pRefType: The initialized sample type.
  Pelem     The element of proper type with rotated vertex numbers.
  pRefT:    The type to fill. 

  Changes To:
  -----------
  PrefT:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int make_uns_refType ( const refType_struct *pRefType, const elem_struct *Pelem,
			      refType_struct *pRefT )
{
  const elemType_struct *PelT, *PelTChild ;
  int kVx, kCh, kEdge, kChFc, mChVerts, nElT, kPrtFc, kRefFc, kVert,
    returnVal, kSibl, kSbFc, kRotEdge, kVxNonRot, kVxRot, found,
    mVxChFc, kVxCh, nVxChFc[MAX_VX_FACE], kFace, kRotFace, iEdge, iFace ;
  int nVxPrtFc[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
    mVxPrtFc[MAX_FACES_ELEM+1], mChildsFc[MAX_FACES_ELEM+1] ;
  refFc_struct *PrefFc ;
  const faceOfElem_struct *PFoE ;
  
  returnVal = 1 ;

  /* The current element stats. */
  nElT = Pelem->elType ;
  PelT = elemType+nElT ;

  pRefT->elType = Pelem->elType ;

  /* Reset all edge marks. */
  for ( kEdge = 0 ; kEdge < MAX_EDGES_ELEM ; kEdge++ )
    pRefT->vxOnEdge[kEdge] = 0 ;
  /* Reset all face marks. */
  for ( kFace = 1 ; kFace <= MAX_FACES_ELEM ; kFace++ )
    pRefT->vxOnFace[kFace] = 0 ;

  /* Added vertices on edges. */
  pRefT->mEdgeVerts = pRefType->mEdgeVerts ;
  for ( kVx = 0 ; kVx < pRefType->mEdgeVerts ; kVx++ ) {
    rot_edge ( Pelem, pRefType->edgeVert[kVx].kEdge, &kRotEdge ) ;
    pRefT->edgeVert[kVx].kEdge = kRotEdge ;
    pRefT->vxOnEdge[kRotEdge] = pRefT->edgeVert[kVx].kVert =
      pRefType->edgeVert[kVx].kVert ;
  }

  /* Construct the compact bit pattern. */
  for ( pRefT->refEdges = 0, kEdge =  PelT->mEdges-1 ; kEdge >= 0 ; kEdge-- ) {
    pRefT->refEdges <<= 1 ;
    if ( pRefT->vxOnEdge[kEdge] )
      pRefT->refEdges |= 1 ;
  }
  
  /* Added vertices on faces. */
  pRefT->mFaceVerts = pRefType->mFaceVerts ;
  for ( kVx = 0 ; kVx < pRefType->mFaceVerts ; kVx++ ) {
    rot_face ( Pelem, pRefType->faceVert[kVx].kFace, &kRotFace ) ;
    pRefT->faceVert[kVx].kFace = kRotFace ;
    pRefT->vxOnFace[kRotFace] = pRefT->faceVert[kVx].kVert =
      pRefType->faceVert[kVx].kVert ;
  }

  /* Vertex at the center, hexes only. */
  pRefT->kElemVert = pRefType->kElemVert ;

  /* Connectivity of the children. */
  pRefT->mChildren = pRefType->mChildren ;
  for ( kCh = 0 ; kCh < pRefType->mChildren ; kCh++ ) {
    pRefT->child[kCh].elType = pRefType->child[kCh].elType ;
    
    mChVerts = elemType[ pRefType->child[kCh].elType ].mVerts ;
    for ( kVx = 0 ; kVx < mChVerts ; kVx++ ) {
      kVxNonRot = pRefType->child[kCh].kParentVert[kVx] ;
      if ( kVxNonRot < PelT->mVerts )
	/* A rotated corner vertex of the parent element. */
	kVxRot = Pelem->PPvrtx[kVxNonRot]->number ;
      else
	/* A new edge vertex, the edge has been rotated already. */
	kVxRot = kVxNonRot ;
      pRefT->child[kCh].kParentVert[kVx] = kVxRot ;
    }
  }

  /* Get the all the subfaces that the faces of the parent have been
     refined into. */
  get_uns_reffaces ( pRefT, PelT, nVxPrtFc, mVxPrtFc, mChildsFc ) ;

  /* Reset the number of children per parent face. */
  for ( kPrtFc = 1 ; kPrtFc <= PelT->mSides ; kPrtFc++ ) {
    pRefT->prntFc[kPrtFc].mChildsFc = mChildsFc[kPrtFc] ;
    for ( kRefFc = 0 ; kRefFc < MAX_CHILDS_FACE ; kRefFc++ )
      pRefT->prntFc[kPrtFc].refFc[kRefFc].kParentFc = 0 ;
  }
  
  /* Loop over all children. */
  for ( kCh = 0 ; kCh < pRefT->mChildren ; kCh++ ) {
    PelTChild = elemType + pRefT->child[kCh].elType ;

    /* Loop over all faces of the children. */
    for ( kChFc = 1 ; kChFc <= PelTChild->mSides ; kChFc++ ) {
      /* Reset the pointer, indicating a face internal to the parent. */
      pRefT->child[kCh].PrefFc[kChFc] = NULL ;

      /* Get the vertices of the child's face. */
      mVxChFc = PelTChild->faceOfElem[kChFc].mVertsFace ;
      for ( kVx = 0 ; kVx < mVxChFc ; kVx++ ) {
        kVxCh = PelTChild->faceOfElem[kChFc].kVxFace[kVx] ;
	nVxChFc[kVx] = pRefT->child[kCh].kParentVert[kVxCh] ;
      }

      /* Find the matching parent face. */
      if ( face_include ( mVxChFc, nVxChFc, nVxPrtFc, mVxPrtFc, mChildsFc,
			  PelT->mSides, &kPrtFc, &kRefFc ) ) {
        /* kPrtFc matches at kRefFc. */
	PrefFc =  pRefT->prntFc[kPrtFc].refFc + kRefFc ;
	if ( PrefFc->kParentFc ) {
          printf ( " FATAL: two children for face %d, facet %d, in %s"
		   " in add_uns_refType.\n", kPrtFc, kChFc, PelT->name ) ;
          returnVal = 0 ; }
	else {
          /* List the match into the pattern. */
	  PrefFc->kChild = kCh ;
	  PrefFc->kChildFc = kChFc ;
	  PrefFc->kParentFc = kPrtFc ;
	}
      }
      else if ( face_match ( pRefT, mVxChFc, nVxChFc, kCh, &kSibl, &kSbFc ) )
	/* kSbFc matches. */
	;
      else {
        printf ( " FATAL: no matching face for child %d, face %d in"
		 " a %s, in add_uns_refType.\n", kCh, kChFc, PelT->name ) ;
	returnVal = 0 ; }
    }
  }

  /* Check if all parent faces were filled. */
  for ( kPrtFc = 1 ; kPrtFc <= PelT->mSides ; kPrtFc++ ) {
    for ( kRefFc = 0 ; kRefFc < mChildsFc[kPrtFc] ; kRefFc++ )
      if ( !pRefT->prntFc[kPrtFc].refFc[kRefFc].kParentFc ) {
	printf ( " FATAL: no child to fill face %d facet %d in a %s"
		 " in add_uns_refType.\n", kPrtFc, kRefFc, PelT->name ) ;
	returnVal = 0 ; }

    /* Mark half-refined faces (directional refinement). */
    if ( PelT->mDim == 3 && mChildsFc[kPrtFc] == 2 ) {
      pRefT->prntFc[kPrtFc].halfRef = 1 ;
      
      /* Make a pattern of refined edges in that face. */
      PFoE = PelT->faceOfElem + kPrtFc ;
      pRefT->prntFc[kPrtFc].refdVx = 0 ;
      for ( iEdge = 0 ; iEdge < PFoE->mFcEdges ; iEdge++ ) {
	kEdge = PFoE->kFcEdge[iEdge] ;
	if ( pRefT->vxOnEdge[kEdge] )
	  pRefT->prntFc[kPrtFc].refdVx |= bitEdge[iEdge] ;
      }
    }
    else
      pRefT->prntFc[kPrtFc].halfRef = 0 ;
  }

  /* Find an instance of the edge vertex in a child, in order to find back
     the vertex pointer on derefinement. */
  for ( iEdge = 0 ; iEdge < pRefType->mEdgeVerts ; iEdge++ ) {
    kVert = pRefType->edgeVert[iEdge].kVert ;
    /* Loop over all children and try to find kVert. */
    for ( found = 0, kCh = 0 ; kCh < pRefType->mChildren ; kCh++ ) {
      for ( kVxCh = 0 ;
	    kVxCh < elemType[ pRefType->child[kCh].elType ].mVerts ; kVxCh++ )
	if ( pRefType->child[kCh].kParentVert[kVxCh] == kVert )	{
          /* This is the one. */
	  pRefT->edgeVert[iEdge].kChild = kCh ;
	  pRefT->edgeVert[iEdge].kChildVx = kVxCh ;
	  found = 1 ;
	  break ;
	}
      if ( found )
	break ;
    }
    if ( !found ) {
      printf ( " FATAL: could not find an instance of edge vertex %d in any child"
	       " in add_uns_refType.\n", kVert ) ;
      returnVal = 0 ; }
  }
	  

  /* Find an instance of the face vertex in a child, in order to find back
     the vertex pointer on derefinement. */
  for ( iFace = 0 ; iFace < pRefType->mFaceVerts ; iFace++ ) {
    kVert = pRefType->faceVert[iFace].kVert ;
    /* Loop over all children and try to find kVert. */
    for ( found = 0, kCh = 0 ; kCh < pRefType->mChildren ; kCh++ ) {
      for ( kVxCh = 0 ;
	    kVxCh < elemType[ pRefType->child[kCh].elType ].mVerts ; kVxCh++ )
	if ( pRefType->child[kCh].kParentVert[kVxCh] == kVert )	{
          /* This is the one. */
	  pRefT->faceVert[iFace].kChild = kCh ;
	  pRefT->faceVert[iFace].kChildVx = kVxCh ;
	  found = 1 ;
	  break ;
	}
      if ( found )
	break ;
    }
    if ( !found ) {
      printf ( " FATAL: could not find an instance of face vertex %d in any child"
	       " in add_uns_refType.\n", kVert ) ;
      returnVal = 0 ; }
  }

  /* Count the number of new element to vertex pointers in all children. */
  /* Loop over all children and count the number of elem2VertP. */
  for ( pRefT->mChElem2Vert = kCh = 0 ; kCh < pRefT->mChildren ; kCh++ )
    pRefT->mChElem2Vert += elemType[ pRefT->child[kCh].elType ].mVerts ;

  /* Count the number of half-refined quad faces. */
  for ( pRefT->mHalfRefQuadFc = 0, kPrtFc = 1 ; kPrtFc < PelT->mFaces ; kPrtFc++ )
    if ( pRefT->prntFc[kPrtFc].halfRef )
      pRefT->mHalfRefQuadFc++ ;
  
  return ( returnVal ) ;
}

/******************************************************************************

  face_include:
  Given a table of vertices for each face nVxPrtFc of a refType, find the
  including parent face kPrtFc for a face kChFc of a child kChild.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  mVxChFc  the number of vertices on the child's face.
  nVxChFc: the vertices forming the child's face.
  nVxPrtFc[kPrtFc][kRefFc][kVx]: the vertices of the parent's facets, organized
           for each parent face kPrtFc=[1..6], kRefFc=[0..3], kVx=[0..3].
  mVxPrtFc[kPrtFc]: the number of vertices per parent face. All faces are
           divided congruently in faces of the same number of vertices.
  mChildsFc[]: Number of children per parent face.
  mPrtFc:  Number of faces in the parent.

  Changes To:
  -----------
  *PkPrtFc: The number of the parent face that matches,
  *PkRefFc: The number of the subface that matches on the parent face.
  
  Returns:
  --------
  0 on exclusion, 1 on inclusion.
  
*/

/* Note that fields with more than one index as nVxPrtFc cannot be const in ANSI C. */
static int face_include ( const int mVxChFc, const int nVxChFc[MAX_VX_FACE],
			  int nVxPrtFc[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
			  const int mVxPrtFc[MAX_FACES_ELEM+1], 
			  const int mChildsFc[MAX_FACES_ELEM+1], const int mPrtFc, 
			  int *PkPrtFc, int *PkRefFc )
{
  static int kPrtFc, kRefFc, foundAll, kVxChFc, foundThisOne, kVxPrtFc ;

  /* Loop over all parent surfaces. */
  for ( kPrtFc = 1 ; kPrtFc <= mPrtFc ; kPrtFc++ )
    for ( kRefFc = 0 ; kRefFc < mChildsFc[kPrtFc] ; kRefFc++ )
    { foundAll = 1 ;
      /* Compare all vertices on the child's face */
      for ( kVxChFc = 0 ; kVxChFc < mVxChFc ; kVxChFc++ )
      { foundThisOne = 0 ;
	/* to the vertices on the parent face. */
	for ( kVxPrtFc = 0 ; kVxPrtFc < mVxPrtFc[kPrtFc] ; kVxPrtFc++ )
	  if ( nVxPrtFc[kPrtFc][kRefFc][kVxPrtFc] == nVxChFc[kVxChFc] )
	  { foundThisOne = 1 ;
	    break ;
	  }
	if ( !foundThisOne )
	{ /* Cannot match a vertex on this face. Try the next one. */
	  foundAll = 0 ;
	  break ;
	}
      }
      
      if ( foundAll )
      { /* All vertices matched, this is the one. */
	*PkPrtFc = kPrtFc ;
	*PkRefFc = kRefFc ;
	return ( 1 ) ;
      }
    }

  /* No other path to here than a mismatch. */
  return ( 0 ) ;
}

/******************************************************************************

  face_match:
  Check whether two children faces match.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------
  *PkSibl:
  *PkSbFc:
  
  Returns:
  --------
  0 on failure, 1 on match.
  
*/

static int face_match ( const refType_struct *Prt,
			const int mVxChFc, const int nVxChFc[MAX_VX_FACE],
			const int kChild, int *PkSibl, int *PkSbFc )
{
  static int kSibl, kSbFc, foundAll, mVxSbFc, kVx, kVxSb, nVxSbFc[MAX_VX_FACE],
      kVxChFc, foundThisOne, kVxSbFc ;
  static const elemType_struct *PelTSb ;
  
  /* Loop over all siblings. */
  for ( kSibl = 0 ; kSibl < Prt->mChildren ; kSibl++ )
    if ( kSibl != kChild )
    { PelTSb = elemType + Prt->child[kSibl].elType ;

      /* Loop over the faces of the sibling. */
      for ( kSbFc = 1 ; kSbFc <= PelTSb->mSides ; kSbFc++ )
      { foundAll = 1 ;
	/* Get the vertices of the sibling's face. */
	mVxSbFc = PelTSb->faceOfElem[kSbFc].mVertsFace ;
	for ( kVx = 0 ; kVx < mVxSbFc ; kVx++ )
	{ kVxSb = PelTSb->faceOfElem[kSbFc].kVxFace[kVx] ;
	  nVxSbFc[kVx] = Prt->child[kSibl].kParentVert[kVxSb] ;
	}

	/* Compare all vertices on the child's face */
	for ( kVxChFc = 0 ; kVxChFc < mVxChFc ; kVxChFc++ )
	{ foundThisOne = 0 ;
	  /* to the vertices on the sibling face. */
	  for ( kVxSbFc = 0 ; kVxSbFc < mVxSbFc ; kVxSbFc++ )
	    if ( nVxSbFc[kVxSbFc] == nVxChFc[kVxChFc] )
	    { foundThisOne = 1 ;
	      break ;
	    }
	  if ( !foundThisOne )
	  { /* Cannot match a vertex on this face. Try the next one. */
	    foundAll = 0 ;
	    break ;
	  }
	}

	if ( foundAll )
	{ /* This is the one. */
	  *PkSibl = kSibl ;
	  *PkSbFc = kSbFc ;
	  return ( 1 ) ;
	}
      }
    }

  /* The only way is out! */
  return ( 0 ) ;
}

/******************************************************************************

  rot_edge:
  Given an edge number and a rotated vertex list in pRefType, find back the
  edge number that corresponds to the permuted vertex numbers.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int rot_edge ( const elem_struct *Pelem, int kEdge, int *PkRotEdge ) {
  
  static int kVx0, kVx1, nVx0, nVx1, kVxEdge0, kVxEdge1 ;

  /* The two permuted vertex numbers. */
  kVx0 = elemType[Pelem->elType].edgeOfElem[kEdge].kVxEdge[0] ;
  kVx1 = elemType[Pelem->elType].edgeOfElem[kEdge].kVxEdge[1] ;
  nVx0 = Pelem->PPvrtx[kVx0]->number ;
  nVx1 = Pelem->PPvrtx[kVx1]->number ;

  /* Loop over all edges to find the matching one. */
  for ( kEdge = 0 ; kEdge < elemType[Pelem->elType].mEdges ; kEdge++ ) {
    kVxEdge0 = elemType[Pelem->elType].edgeOfElem[kEdge].kVxEdge[0] ;
    kVxEdge1 = elemType[Pelem->elType].edgeOfElem[kEdge].kVxEdge[1] ;
    if ( ( kVxEdge0 == nVx0 && kVxEdge1 == nVx1 ) ||
	 ( kVxEdge0 == nVx1 && kVxEdge1 == nVx0 ) ) { 
      *PkRotEdge = kEdge ;
      return ( 1 ) ;
    }
  }

  /* No matching edge found. */
  hip_err ( fatal, 0,"no matching edge found in rot_edge.\n" ) ;
  *PkRotEdge = 0 ;
  return ( 0 ) ;
}
/******************************************************************************

  rot_face:
  Given an face number and a rotated vertex list in pRefType, find back the
  face number that corresponds to the permuted vertex numbers.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int rot_face ( const elem_struct *Pelem, int kFace, int *PkRotFace ) {
  
  static int nVxFcRot[MAX_VX_FACE], mVxFc, kFc, kVx, kVxFc, kVxFcRot, foundAll,
    foundThisOne ;
  static const elemType_struct *PelT ;

  PelT = elemType + Pelem->elType ;

  /* The permuted face. */
  mVxFc = PelT->faceOfElem[kFace].mVertsFace ;
  for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
    nVxFcRot[kVx] = Pelem->PPvrtx[ PelT->faceOfElem[kFace].kVxFace[kVx] ]->number ;

  /* Loop over all other faces of this element. */
  for ( kFc = 1 ; kFc <= PelT->mSides ; kFc++ )
    if ( mVxFc == PelT->faceOfElem[kFc].mVertsFace )
    { /* At least they have the same number of verts. Compare. */
      foundAll = 1 ;
      
	/* Compare all vertices on the permuted face */
	for ( kVxFc = 0 ; kVxFc < mVxFc ; kVxFc++ )
	{ /* to the vertices on the rotated face. */
	  for ( foundThisOne = 0, kVxFcRot = 0 ; kVxFcRot < mVxFc ; kVxFcRot++ )
	    if ( nVxFcRot[kVxFcRot] == PelT->faceOfElem[kFc].kVxFace[kVxFc] )
	    { foundThisOne = 1 ;
	      break ;
	    }
	  if ( !foundThisOne )
	  { /* Cannot match a vertex on this face. Try the next one. */
	    foundAll = 0 ;
	    break ;
	  }
	}

	if ( foundAll )
	{ /* This is the one. */
	  *PkRotFace = kFc ;
	  return ( 1 ) ;
	}

      }
  
  /* No matching face found. */
  *PkRotFace = 0 ;
  hip_err (fatal,0,"no matching face found in rot_face.\n" ) ;
  return ( 0 ) ;
}

/******************************************************************************

  get_uns_reffaces:
  List all the refined faces in order for a face of an element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int get_uns_reffaces
           ( const refType_struct *Prt, const elemType_struct *PelT,
	     int nVxPrtFc[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
	     int mVxPrtFc[MAX_FACES_ELEM+1], int mChildsFc[MAX_FACES_ELEM+1] )
{
  static int kPrtFc, kRefEdge[MAX_EDGES_FACE], mRefEdges, kEdge, nEdge, nDir, kVx,
    nVxCenter, *PmChildsFc, *PmVxPrtFc ;
  
  /* Loop over the faces of the parent. */
  for ( kPrtFc = 1 ; kPrtFc <= PelT->mSides ; kPrtFc++ )
  { PmChildsFc = mChildsFc+kPrtFc ;
    PmVxPrtFc = mVxPrtFc+kPrtFc ;
    
    if ( PelT->mDim == 2 )
    { /* 2-D, thus the face is an edge. */
      *PmVxPrtFc = 2 ;
      if ( Prt->vxOnEdge[ PelT->faceOfElem[kPrtFc].kFcEdge[0] ] )
      { /* Two halves. */
	*PmChildsFc = 2 ;
	nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[0] ;
	nDir = PelT->faceOfElem[kPrtFc].edgeDir[0] ;
	/* First half. */
	nVxPrtFc[kPrtFc][0][0] = PelT->edgeOfElem[nEdge].kVxEdge[1-nDir] ;
	nVxPrtFc[kPrtFc][0][1] = Prt->vxOnEdge[nEdge] ;
	/* Second half. */
	nVxPrtFc[kPrtFc][1][0] = Prt->vxOnEdge[nEdge] ;
	nVxPrtFc[kPrtFc][1][1] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
      }
      else
      { /* There is only the full face. */
	*PmChildsFc = 1 ;
	nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[0] ;
	nDir = PelT->faceOfElem[kPrtFc].edgeDir[0] ;
	PmVxPrtFc[0] = 2 ;
	nVxPrtFc[kPrtFc][0][0] = PelT->edgeOfElem[nEdge].kVxEdge[1-nDir] ;
	nVxPrtFc[kPrtFc][0][1] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
      }
    }
    else
    { /* 3-D. Loop over all edges of this face and find the number
	 of refined edges. */
      for ( mRefEdges = kEdge = 0 ;
	    kEdge < PelT->faceOfElem[kPrtFc].mFcEdges ; kEdge++ )
      { /* Number of the edge in the parent. */
	nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[kEdge] ;
	if ( Prt->vxOnEdge[nEdge] )
	  /* This edge is refined. */
	  kRefEdge[ mRefEdges++ ] = kEdge ;
      }

      if ( PelT->faceOfElem[kPrtFc].mVertsFace == 3 )
      { /* Triangular face. */
	*PmVxPrtFc = 3 ;
	if ( mRefEdges == 0 )
	{ /* Full face. */
	  *PmChildsFc = 1 ;
	  for ( kEdge = 0 ; kEdge < 3 ; kEdge++ )
	  { nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[kEdge] ;
	    nDir = PelT->faceOfElem[kPrtFc].edgeDir[kEdge] ;
	    nVxPrtFc[kPrtFc][0][kEdge] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	  }
	}
	else if ( mRefEdges == 1 )
	{ /* One refined edge, two half faces. List both triangles with the
	     vertex opposite the refined edge as first vertex. */
	  *PmChildsFc = 2 ;
	  PmVxPrtFc[0] = PmVxPrtFc[1] = 3 ;
	  /* Edge following the refined edge. */
	  nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[ (kRefEdge[0]+1)%3 ] ;
	  nDir = PelT->faceOfElem[kPrtFc].edgeDir[ (kRefEdge[0]+1)%3 ] ;
	  nVxPrtFc[kPrtFc][0][0] = nVxPrtFc[kPrtFc][1][0] =
	    PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	  /* Refined edge. */
	  nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[ kRefEdge[0] ] ;
	  nDir = PelT->faceOfElem[kPrtFc].edgeDir[ kRefEdge[0] ] ;
	  nVxPrtFc[kPrtFc][0][1] = PelT->edgeOfElem[nEdge].kVxEdge[1-nDir] ;
	  nVxPrtFc[kPrtFc][1][2] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	  /* Refined vertex. */
	  nVxPrtFc[kPrtFc][0][2] = nVxPrtFc[kPrtFc][1][1] = Prt->vxOnEdge[nEdge] ;
	}
	else if ( mRefEdges == 3 )
	{ /* Full refinement. */
	  *PmChildsFc = 4 ;
	  for ( kEdge = 0 ; kEdge < 3 ; kEdge++ )
	  { nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[kEdge] ;
	    nDir = PelT->faceOfElem[kPrtFc].edgeDir[kEdge] ;
	    /* Corner vertices are listed such that lower-left always
	       stays the first vertex. Corner children listed as 0,1,2.
	       The center cell 3 is listed upside down, ie upper-right is first.*/
	    nVxPrtFc[kPrtFc][kEdge][kEdge] = PelT->edgeOfElem[nEdge].kVxEdge[1-nDir] ;
	    /* The refined vertex. */
	    nVxPrtFc[kPrtFc][kEdge][(kEdge+1)%3] = nVxPrtFc[kPrtFc][(kEdge+1)%3][kEdge] =
	      nVxPrtFc[kPrtFc][3][(kEdge+2)%3] = Prt->vxOnEdge[nEdge] ;
	  }
	}
	else
	{ printf ( " FATAL: tried to refine two edges of a tri face in"
		   " get_uns_refface.\n" ) ;
	  exit ( EXIT_FAILURE ) ;
	}
      }

      else
      { /* Quadrilateral face. */
	*PmVxPrtFc = 4 ;
	if ( mRefEdges == 0 )
	{ /* Full face. */
	  *PmChildsFc = 1 ;
	  for ( kEdge = 0 ; kEdge < 4 ; kEdge++ )
	  { nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[kEdge] ;
	    nDir = PelT->faceOfElem[kPrtFc].edgeDir[kEdge] ;
	    nVxPrtFc[kPrtFc][0][kEdge] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	  }
	}
	else if ( mRefEdges == 2 )
	{ /* Two half faces. */
	  *PmChildsFc = 2 ;
	  *PmVxPrtFc = 4 ;
	  /* Edge opposite the refined edge. */
	  nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[ (kRefEdge[0]+2)%4 ] ;
	  nDir = PelT->faceOfElem[kPrtFc].edgeDir[ (kRefEdge[0]+2)%4 ] ;
	  nVxPrtFc[kPrtFc][0][0] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	  nVxPrtFc[kPrtFc][1][3] = PelT->edgeOfElem[nEdge].kVxEdge[1-nDir] ;
	  nVxPrtFc[kPrtFc][0][3] = nVxPrtFc[kPrtFc][1][0] = Prt->vxOnEdge[nEdge] ;
	  /* Refined edge. */
	  nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[ kRefEdge[0] ] ;
	  nDir = PelT->faceOfElem[kPrtFc].edgeDir[ kRefEdge[0] ] ;
	  nVxPrtFc[kPrtFc][0][1] = PelT->edgeOfElem[nEdge].kVxEdge[1-nDir] ;
	  nVxPrtFc[kPrtFc][1][2] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	  /* Refined vertex. */
	  nVxPrtFc[kPrtFc][0][2] = nVxPrtFc[kPrtFc][1][1] = Prt->vxOnEdge[nEdge] ;
	}
	else if ( mRefEdges == 4 )
	{ /* Full refinement. */
	  /* Find the vertex at the center of the face. */
	  for ( nVxCenter = kVx = 0 ; kVx < Prt->mFaceVerts ; kVx++ )
	    if ( Prt->faceVert[kVx].kFace == kPrtFc )
	    { nVxCenter = Prt->faceVert[kVx].kVert ;
	      break ;
	    }
	  if ( nVxCenter == 0 )
	  { printf ( " FATAL: could not find center vertex in"
		     " get_uns_refface.\n" ) ;
	    exit ( EXIT_FAILURE ) ;
	  }
	  
	  *PmChildsFc = 4 ;
	  for ( kEdge = 0 ; kEdge < 4 ; kEdge++ )
	  { nEdge = PelT->faceOfElem[kPrtFc].kFcEdge[kEdge] ;
	    nDir = PelT->faceOfElem[kPrtFc].edgeDir[kEdge] ;
	    /* Corner vertices are listed such that lower-left always
	       stays the first vertex. Corner children listed as 0,1,2.
	       The center cell 3 is listed upside down, ie upper-right is first.*/
	    nVxPrtFc[kPrtFc][kEdge][kEdge] = PelT->edgeOfElem[nEdge].kVxEdge[nDir] ;
	    /* The refined vertex on that edge. */
	    nVxPrtFc[kPrtFc][kEdge][(kEdge+3)%4] = nVxPrtFc[kPrtFc][(kEdge+3)%4][kEdge] =
	      Prt->vxOnEdge[nEdge] ;
	    /* The center vertex. */
	    nVxPrtFc[kPrtFc][kEdge][(kEdge+2)%4] = nVxCenter ;
	  }
	}
	else
	{ printf ( " FATAL: tried to refine 1 or 3 edges of a quad face in"
		   " get_uns_refface.\n" ) ;
	  exit ( EXIT_FAILURE ) ;
	}
      }
    }
  }

  return ( 1 ) ;
}
