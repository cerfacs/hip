/*
  file_name.c:
*/

/*! short descr. for doxygen
 *
 *   more details
 *
 */


/* 
   Last update:
   ------------
   ; conceived

  
  
   This file contains:
   -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  cp_marked_vx:
*/
/*! Add the marked vx to an allocated grid.
 *
 */

/*
  
  Last update:
  ------------
  26Feb22; remove refs to zones.
  6Sep18; drop extraneous add from fun name.
  5Sep18; pass pCh2
  19Dec17: refactored from cp_uns_zones.
  

  Input:
  ------
  pUns0: sourcegrid
  mVertsZone: allocated number of vertices
  useMark: if non-zero, base inclusion on mark, otherwise on number.
  pCh2: chunk to copy to
    
  Returns:
  --------
  0
  
*/

int cp_marked_vx ( uns_s *pUns0,
                   const ulong_t mVertsReg,
                   const int useMark, chunk_struct *pCh2,
                   const int doUnknowns ) {
  
  vrtx_struct *pVx2 = pCh2->Pvrtx+1 ;
  const int mDim = pUns0->mDim ;
  double *pCo2 = pCh2->Pcoor+mDim ;
  double *pUnk2 = NULL ;
  ulong_t mUn = ( doUnknowns ? pUns0->varList.mUnknowns : 0 ) ;
  if ( mUn ) pUnk2 = pCh2->Punknown + mUn ;

  /* copy vertices */
  chunk_struct *pCh = NULL ;
  vrtx_struct *pVx, *pVxB, *pVxE ;
  int nB, nE ;
  ulong_t mVx = 0 ;
  while ( loop_verts ( pUns0, &pCh, &pVxB, &nB, &pVxE, &nE ) ) {
    if ( pCh == pCh2 )
      /* Don't copy the new chunk. */
      break ;
    else {
      for ( pVx = pVxB ; pVx <= pVxE ; pVx++ ) 
        if ( ( useMark ? pVx->mark : pVx->number ) ) {
          mVx++ ;
          if ( useMark )
            /* Renumber if marks are used. */
            pVx->number = mVx ;
          else
            /* Set mark for copy, if number is used. */
            pVx->mark = 1 ;
          /* Copy */
          *pVx2 = *pVx ;
          pVx2->vxCpt.nCh = pCh2->nr ;
          pVx2->vxCpt.nr = pVx2->number = mVx ;
          memcpy ( pCo2, pVx->Pcoor, mDim*sizeof(double) ) ;
          pVx2->Pcoor = pCo2 ;
          if ( mUn ) {
            pVx2->Punknown = pUnk2 ;
            memcpy ( pUnk2, pVx->Punknown, mUn*sizeof(double) ) ;
            pUnk2 += mUn ;
          }
          pVx2++ ; 
          pCo2 += mDim ;
        }
    }
  }
  if ( mVx != mVertsReg ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", written  %"FMT_ULG" "
              "vx in cp_marked_vx.", mVertsReg, mVx ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  cp_numbered_elem:
*/
/*! Copy numbered elements to an allocated grid.
 *
 *
 */

/*
  
  Last update:
  ------------
  26Dec21; disable use for zones, work on number only, change name to _numbered.
  3Apr19; fix sprtinf formats for ptrdiff values, need to be %ld.
  6Sep18; drop extraneous add from fun name.
  5Sep8; pass pCh2
  19Dec17: refactored from cp_uns_zones
  

  Input:
  ------
  pUns0: source grid
  mElemsZone: allocated size for elements
  mConnZone: allocated size for connectivity
  pCh2: chunk to copy into.

  Returns:
  --------
  0
  
*/

int cp_numbered_elem ( uns_s *pUns0, 
                       const ulong_t mElemsReg, const ulong_t mConnReg,
                       chunk_struct *pCh2 ) {
#undef FUNLOC
#define FUNLOC "in cp_numbered_elem"
  
  elem_struct *pEl2 = pCh2->Pelem+1 ;
  vrtx_struct **ppVx2 = pCh2->PPvrtx ;
  vrtx_struct *pVrtx2 = pCh2->Pvrtx ;
  int mEl = 0, mVxEl ;
  int k ;
  chunk_struct *pCh = NULL ;
  elem_struct *pEl, *pElB, *pElE ;
  while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) {
    if ( pCh == pCh2 )
      /* Don't copy the new chunk. */
      break ;
    else {
      for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
        if ( !pEl->invalid && pEl->number ) {
          mEl++ ;
          /* Copy element. */
          *pEl2 = *pEl ;
          // pEl2->iZone = iReg2 ;
          /* Correct connectivity. */
          mVxEl = elemType[pEl->elType].mVerts ;
          pEl2->PPvrtx = ppVx2 ;
          for ( k = 0 ; k < mVxEl ; k++ ) 
            pEl2->PPvrtx[k] = pVrtx2 + pEl->PPvrtx[k]->number ;
          ppVx2 += mVxEl ;
          pEl2++ ;
        }
    }
  }
  if ( pEl2 - (pCh2->Pelem+1) != mElemsReg ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", written  %ld "
              "elems "FUNLOC".",
              mElemsReg,pEl2 - (pCh2->Pelem+1)) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  if ( ppVx2 - (pCh2->PPvrtx) != mConnReg ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", written  %ld "
              "conn "FUNLOC".",
              mConnReg, ppVx2 - (pCh2->PPvrtx)) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  cp_marked_bnd_fc:
*/
/*! Add boundaries and faces to an allocated grid.
 *
 *
 */

/*
  
  Last update:
  ------------
  26Feb22; remove ref to zones.
  3Apr19; use correct %ld format specifier for ptrdiff values.
  6Sep18; drop extraneous add from fun name.
  5Sep18; pass pCh2.
  19Dec17: refactored from cp_uns_zones.
  

  Input:
  ------
  pUns: grid
  mBndFcReg: the allocated size for faces.
  useMark: if non-zero, include if marked, otherwise if numbered.
  renameDuplPerBc: if true, append unique identifier to bc labels.
  pCh2: chunk to copy into.
    
  Returns:
  --------
  0 on success
  
*/

int cp_marked_bnd_fc ( uns_s *pUns0, ulong_t mBndFcReg,
                       const int useMark, const int renameDuplPerBc,
                       chunk_struct *pCh2 ) {

  bndFc_struct *pBf2 = pCh2->PbndFc+1 ;
  elem_struct *pElem2 = pCh2->Pelem ;
  int nBc ;
  bc_struct *pBc, *pBc2 ;
  char bcTxt[LINE_LEN] ;
  bndPatch_struct *pBP2 ;
  ulong_t mBc = pUns0->mBc ;
  bndPatch_struct *pBP ;
  bndFc_struct *pBf, *pBfB, *pBfE ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    if ( pUns0->pmFaceBc[nBc] ) {
      pBc2 = pBc = pUns0->ppBc[nBc] ;

      /* Duplicate bc's, if required. */
      if ( ( bc_is_per( pBc ) || pBc->mark ) && renameDuplPerBc ) {
        /* Any periodic or marked bc will be duplicated. */
        //sprintf ( bcTxt, "%s_copy%s", pBc->text, cpLbl ) ;
        pBc2 = pBc  ;//find_bc ( bcTxt, 1 ) ;

        /* Copy user-assigned/defined parameters from the original boundary. */
        strcpy( pBc2->type, pBc->type ) ;
        pBc2->pPerBc  = pBc->pPerBc ; 
        pBc2->order   = pBc->order ; 
        pBc2->mark    = pBc->mark ;  
        pBc2->geoType = pBc->geoType ;
      }


      pBP2 = pCh2->PbndPatch+nBc+1 ;
      pBP2->PbndFc = pBf2 ;
      pBP2->Pchunk = pCh2 ;
      
      pBP = NULL ;
      while ( loop_bndFaces_bc ( pUns0, nBc, &pBP, &pBfB, &pBfE ) )
        for ( pBf = pBfB ; pBf <= pBfE ; pBf++ )
          if ( pBf->Pelem &&
               ( useMark ? pBf->Pelem->mark : pBf->Pelem->number ) &&
               pBf->nFace ) {
            *pBf2 = *pBf ;
            pBf2->Pelem = pElem2 + pBf->Pelem->number ; 
            pBf2->Pbc = pBc2 ;
            pBf2++ ;
          }
      pBP2->mBndFc = pBf2 - pBP2->PbndFc ;
      pBP2->Pbc = pBc2 ;
    }
  }
  
  if ( pBf2 - (pCh2->PbndFc+1) != mBndFcReg ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", written  %ld "
              "bndFc in cp_bnd_fc.",
              mBndFcReg, pBf2 - (pCh2->PbndFc+1)) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  
  return ( 0 ) ;
}


/******************************************************************************
  cp_uns_zone:   */

/*! copy one unstructured grid.
 * No longer called.
 *
 * Copy zones of a grid verbatim, such that the copy can be translated/rotated and
 * attached.
 *
 */
/*
  
  Last update:
  ------------
  4Sep18; simplified: only copy a single zone, no merge.
  19Dec17; derived from cp_oneUns.  

  Input:
  ------
  pUns2           = grid to copy to
  pUns0           = grid to copy from
  iZone2           = number of zone copy
  iZone2           = number of zone to be copied
  renameDuplPerBc = if nonzero, modify name of duplicate periodic bcs.
  doUnknown = if nonzero, do allocate for uknowns

  Returns:
  --------
  pUns2: point to the unstructured grid.
  
*/

// Replaced by ucopy_uns_zone
uns_s *cp_uns_zone ( uns_s *pUns2, uns_s *pUns0,
                     const int iZone2, const int iZone0, 
                     const int renameDuplPerBc,
                     const int doUnknown ) {

  /* Mark1 all vx in this zone.
     ulong_t mElZ, mConnZ, mVxZ, mBndFcZ ;
     int zero ;
     mark_vx_elem_zones ( pUns0, 1, &iZone0, 0, &zero,
     &mElZ, &mConnZ, &mVxZ, &mBndFcZ ) ;*/

  const int doUseNumber = 1 ;
  const int doReset = 1 ;
  const int doBound = 1 ;
  const int dontUseMark = 0 ;
  // number_uns_grid_zones
  number_uns_grid_elem_regions
    ( pUns0, 1, &iZone0, 
      doUseNumber, doReset, doBound, dontUseMark ) ;

  /* make a grid and allocate. */
  int mDim = pUns0->mDim ;
  ulong_t mUn = ( doUnknown ? pUns0->varList.mUnknowns : 0 ) ;
  ulong_t mBc = pUns0->mBc ;
  ulong_t mBndFcZones = pUns0->mFaceAllBc + pUns0->mFaceAllInter ;
  chunk_struct *pCh2 ;
  if ( pUns2 ) {
    /* Add a chunk to the existing grid. */
    pCh2 = append_chunk ( pUns2, mDim, pUns0->mElemsNumbered,
                          4*pUns0->mElemsNumbered, 0,
                          pUns0->mVertsNumbered, pUns0->mFaceAllBc, mBc ) ;

    
    /* Copy key parameters such as hMin. */
    pUns2->hMin         = MIN( pUns2->hMin, pUns0->hMin ) ;
    pUns2->hMax         = MAX( pUns2->hMax, pUns0->hMax ) ;
    pUns2->epsOverlap   = MIN( pUns2->epsOverlap, pUns0->epsOverlap ) ;
    pUns2->epsOverlapSq = MAX( pUns2->epsOverlapSq, pUns0->epsOverlapSq ) ;
  }
  else {
    /* Make a new grid. */
    make_uns_grid ( &pUns2, mDim, pUns0->mElemsNumbered,
                    4*pUns0->mElemsNumbered, 0,
                    pUns0->mVertsNumbered, mUn, pUns0->mFaceAllBc, mBc ) ;
    pCh2 = pUns2->pRootChunk ;
  
    /* Copy key parameters such as hMin. */
    pUns2->hMin = pUns0->hMin ;
    pUns2->hMax = pUns0->hMax ;
    pUns2->epsOverlap = pUns0->epsOverlap ;
    pUns2->epsOverlapSq = pUns0->epsOverlapSq ;
    /* Copy solution setup. */
    pUns2->varList = pUns0->varList ;
  }
  
  /* Copy vertices. */
  const int useMark = 0 ;
  cp_marked_vx ( pUns0, pUns0->mVertsNumbered, useMark, pCh2, doUnknown ) ;

  /* Elems. */
  const int dontUseNumber = 0 ;
  /*cp_marked_elem ( pUns0, iZone2, iZone0,
                   pUns0->mElemsNumbered, 4*pUns0->mElemsNumbered, 
                   dontUseNumber, pCh2 ) ;*/
  cp_numbered_elem ( pUns0, pUns0->mElemsNumbered, 4*pUns0->mElemsNumbered, 
                   pCh2 ) ;

  /* Boundaries, bnd faces. */
  cp_marked_bnd_fc ( pUns0, pUns0->mFaceAllBc, useMark, renameDuplPerBc, pCh2 ) ;

  /* Unmark elems and vx of the donor parts, 
     so operations such as transform can target the copy. */
  chunk_struct *pCh = NULL ;
  while ( loop_chunks ( pUns0, &pCh ) ) {
    if ( pCh == pCh2 )
      /* Not this one, done. */
      break ;
    reset_vx_mark_1chunk ( pCh ) ;
    reset_elem_mark_1chunk ( pCh, 0 ) ;
  }
  
  return ( pUns2 ) ;
}



/******************************************************************************
  cp_oneUns:   */

/*! copy one unstructured grid.
 *
 * Copy a grid verbatim, such that the copy can be translated/rotated and
 * attached.
 *
 */
/*
  
  Last update:
  ------------
  15Dec18; rename ucopy to indicate public/prototyped call.
  11May17; switch matchBc to geoType in bc_struct.
  20Feb17; intro renameDuplPerBc.
  15Jul13; extract zone_copy_all.
  4Apr13; modified interface to loop_elems
  make all large counters ulong_t
  18Dec10; allow multiple lu pairs and/or _inlet/outlet pairs.
  new interface to cp_oneUns
  24Oct10; fix bug with calc of mConn.
  15Oct10; copy over pBndPatch info.
  14Sep10: conceived.
  

  Input:
  ------
  pGr0         = grid to copy
  nCp          = copy number to add to duplicated b.c.
  ppGrCp       = pointer to the copied grid.
  pmVxNumbered = increment node numbers from this value.
  renameDuplPerBc = if nonzero, modify name of duplicate periodic bcs.

  Output:
  -------
  ppGrCp       = copied grid.
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

uns_s *ucopy_oneUns ( grid_struct *pGr0, int nCp, 
                      grid_struct **ppGrCp,
                      int *pmVxNumbered, // JDM: Sep 2017: should be ulong_t?
                      const int renameDuplPerBc ) {


  uns_s *pUns0 = pGr0->uns.pUns ;
  int mDim = pUns0->mDim ;
  ulong_t mEl = pUns0->mElemsNumbered ;
  ulong_t mVx = pUns0->mVertsNumbered ;
  ulong_t mUn = pUns0->varList.mUnknowns ;
  ulong_t mBc = pUns0->mBc ;
  ulong_t mBndFc = pUns0->mFaceAllBc ;
  ulong_t mConn = 0 ;
  elType_e elT ;
  chunk_struct *pCh, *pCh2 ;
  vrtx_struct *pVx2 ;
  vrtx_struct *pVx, *pVxB, *pVxE ;
  double *pCo2 ;
  double *pUnk2 = NULL ;
  elem_struct *pEl2 ;
  elem_struct *pEl, *pElB, *pElE ;
  vrtx_struct **ppVx2 ;
  int mVxEl, k ;
  vrtx_struct *pVrtx2 ;
  int nBc ;
  bc_struct *pBc, *pBc2 ;
  char bcTxt[LINE_LEN] ;
  bndPatch_struct *pBP, *pBP2 ;
  bndFc_struct *pBf2 ;
  elem_struct *pElem2 ;
  bndFc_struct *pBf, *pBfB, *pBfE ;
  int mBFc ;


  /* Count the connectivity entries. */
  for ( elT = tri ; elT <= hex ; elT++ ) {
    mVxEl = elemType[elT].mVerts ;
    mConn += mVxEl*pUns0->mElemsOfType[elT] ;
  }

  /* make a grid and allocate. */
  uns_s *pUns2 = NULL ;
  *ppGrCp = make_uns_grid ( &pUns2, 
                            mDim, mEl, mConn, 0, mVx, mUn, mBndFc, mBc ) ;
  pCh2 = pUns2->pRootChunk ; 

  /* Copy solution setup. */
  pUns2->varList = pUns0->varList ;

  /* Vertices get filled from 1. */
  pVx2 = pCh2->Pvrtx+1 ;
  pCo2 = pCh2->Pcoor+mDim ;
  if ( mUn ) pUnk2 = pCh2->Punknown + mUn ;

  /* copy vertices */
  pCh = NULL ;
  int nB, nE ;
  while ( loop_verts ( pUns0, &pCh, &pVxB, &nB, &pVxE, &nE ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ ) 
      if ( pVx->number ) {
        *pVx2 = *pVx ;
        pVx2->number = (*pmVxNumbered)++ ;
        memcpy ( pCo2, pVx->Pcoor, mDim*sizeof(double) ) ;
        pVx2->Pcoor = pCo2 ;
        if ( mUn ) {
          pVx2->Punknown = pUnk2 ;
          memcpy ( pUnk2, pVx->Punknown, mUn*sizeof(double) ) ;
          pUnk2 += mUn ;
        }
        pVx2++ ; 
        pCo2 += mDim ;
      }

  pUns2->mVertsNumbered = mVx ;




  /* Elems get filled from 1, connectivity gets filled from zero. */
  pEl2 = pCh2->Pelem+1 ;
  ppVx2 = pCh2->PPvrtx ;
  pVrtx2 = pCh2->Pvrtx ;

  /* copy elements. */
  pCh = NULL ;
  while ( loop_elems ( pUns0, &pCh, &pElB, &pElE ) ) 
    for ( pEl = pElB ; pEl <= pElE ; pEl++ ) 
      if ( pEl->number ) {
        *pEl2 = *pEl ;
        
        /* Reset connectivity. */
        mVxEl = elemType[pEl->elType].mVerts ;
        pEl2->PPvrtx = ppVx2 ;
        for ( k = 0 ; k < mVxEl ; k ++ ) 
          pEl2->PPvrtx[k] = pVrtx2 + pEl->PPvrtx[k]->number ;
        ppVx2 += mVxEl ;
        pEl2++ ;
      }

  pUns2->mElemsNumbered = mEl ;




  /* copy boundaries. */
  pBf2 = pCh2->PbndFc+1 ;
  pElem2 = pCh2->Pelem ;

  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    if ( pUns0->pmFaceBc[nBc] ) {
      pBc2 = pBc = pUns0->ppBc[nBc] ;

      /* Duplicate bc's, if required. */
      if ( ( bc_is_per( pBc ) || pBc->mark ) && renameDuplPerBc ) {
        /* Any periodic or marked bc will be duplicated. */
        sprintf ( bcTxt, "%s_copy%d", pBc->text, nCp ) ;
        pBc2 = find_bc ( bcTxt, 1 ) ;

        /* Copy user-assigned/defined parameters from the original boundary. */
        strcpy( pBc2->type, pBc->type ) ;
        pBc2->pPerBc  = pBc->pPerBc ; 
        pBc2->order   = pBc->order ; 
        pBc2->mark    = pBc->mark ;  
        pBc2->geoType = pBc->geoType ;
      }

      mBFc = 0 ;

      pBP2 = pCh2->PbndPatch+nBc+1 ;
      pBP2->PbndFc = pBf2 ;
      pBP2->Pchunk = pCh2 ;
      
      pBP = NULL ;
      while ( loop_bndFaces_bc ( pUns0, nBc, &pBP, &pBfB, &pBfE ) )
        for ( pBf = pBfB ; pBf <= pBfE ; pBf++ )
          if ( pBf->Pelem && pBf->Pelem->number && pBf->nFace ) {
            *pBf2 = *pBf ;
            pBf2->Pelem = pElem2 + pBf->Pelem->number ; 
            pBf2->Pbc = pBc2 ;
            pBf2++ ;
          }
      pBP2->mBndFc = pBf2 - pBP2->PbndFc ;
      pBP2->Pbc = pBc2 ;
    }
  }

  /* Copy key parameters such as hMin. */
  pUns2->hMin = pUns0->hMin ;
  pUns2->hMax = pUns0->hMax ;
  pUns2->epsOverlap = pUns0->epsOverlap ;
  pUns2->epsOverlapSq = pUns0->epsOverlapSq ;


  /* Copy zones. */
  zone_copy_all ( pUns0, pUns2 ) ;

  /* Count all boundary faces, link all boundary patches. */
  count_uns_bndFc_chk ( pUns2 ) ;
  make_uns_ppChunk ( pUns2 ) ;
  make_uns_ppBc ( pUns2 ) ;
  count_uns_bndFaces ( pUns2 ) ;


  return ( pUns2 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_bc_to_match_after_copy:
*/
/*! Given a numer of copied sectors, tag all bc as 'matching' that will be interior.
 *
 *
 */

/*
  
  Last update:
  ------------
  23Nov20: factorized out from ucopy_uns2uns
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s find_bc_to_match_after_copy ( uns_s *pUns2, const int nSec, const int mSec,
                                    const transf_e tr_op, const double dVal[],
                                    const int is360, int *pFoundPerBc ) {
#undef FUNLOC
#define FUNLOC "in find_bc_to_match_after_copy"
  
  ret_s ret = ret_success () ;

  *pFoundPerBc = 0 ;

  /* Note that we have a valid ppBc for pUns2. */
  int nBc ;
  bc_struct *pBc ;
  const perBc_s *pPerBc ;
  const int mDim = pUns2->mDim ;
  double vd[MAX_DIM], nrmDelFwd, nrmDelBkwd ;
  for ( nBc = 0 ; nBc < pUns2->mBc ; nBc++ ) {
    pBc = pUns2->ppBc[nBc] ;

    if ( pBc->bc_e == perBc ) {
      /* This is a periodic bc. */
      pPerBc = pBc->pPerBc ;
      if ( pPerBc->tr_op == tr_op ) {
        *pFoundPerBc = 1 ;
        
        if ( tr_op >= rot_x && tr_op <= rot_z ) {
          /* It rotates in the same direction, check for handedness. */
          if ( is360 ) {
            /* All match, even first in original and last in last sector. */
            pBc->geoType = match ;
          }
          else if ( nSec == mSec ) {
            /* Last sector. */
            if ( ( bc_is_l(pBc) && dVal[0]*pPerBc->rotAngleRad > 0 ) ||
                 ( bc_is_u(pBc) && dVal[0]*pPerBc->rotAngleRad < 0 ) ) {
              /* l if per and rot align, u if opposite. */
              pBc->geoType = match ; }
          }
          else if ( nSec == 0 ) {
            /* Original sector. */
            if ( ( bc_is_u(pBc) && dVal[0]*pPerBc->rotAngleRad > 0 ) ||
                 ( bc_is_l(pBc) && dVal[0]*pPerBc->rotAngleRad < 0 ) ) {
              /* u if per and rot align, l if opposite. */
              pBc->geoType = match ; }
          }
          else {
            /* Internal sectors. All match. */
            pBc->geoType = match ;
          }
        }
        else if ( tr_op == trans ) {
          /* Directions match? */
          vec_diff_dbl ( dVal, pPerBc->shftIn2out, mDim, vd ) ;
          nrmDelFwd = vec_norm_dbl ( vd, mDim ) ;
          vec_diff_dbl ( dVal, pPerBc->shftOut2in, mDim, vd ) ;
          nrmDelBkwd = vec_norm_dbl ( vd, mDim ) ;
            
          if ( nSec == mSec ) {
            /* Final sector, the per bnd in the dir of the translation remains. */
            if ( nrmDelFwd < Grids.epsOverlap && bc_is_l (pBc) ) {
              /* The declared u is at the duplicated end. */
              pBc->geoType = match ;
            }
            else if ( nrmDelBkwd < Grids.epsOverlap && bc_is_u (pBc) ) {
              /* Per and trans in opposite directions, l part remains. */
              pBc->geoType = match ;
            }
          }
          else if ( nSec == 0 ) {
            /* Original sector, the per bnd in the dir opposite to
               the translation remains. */
            if ( nrmDelFwd < Grids.epsOverlap && bc_is_u (pBc) ) {
              /* The declared u is at the duplicated end. */
              pBc->geoType = match ;
            }
            else if ( nrmDelBkwd < Grids.epsOverlap && bc_is_l (pBc) ) {
              /* Per and trans in opposite directions, l part remains. */
              pBc->geoType = match ;
            }
          }
          else {
            /* interior sector. */
            if  ( nrmDelFwd < Grids.epsOverlap ||
                  nrmDelBkwd < Grids.epsOverlap ) {
              /* The translation and periodicity match,  all bc to match. */ 
              pBc->geoType = match ;
            }
          } /* else inner/final/first sector. */
        } /* else if rotation/translation. */
      } /* if matching tr_op */
    } /* if pPerBc ... */
  } /* for nBc ... */
  
  
  return ( ret ) ;
}


/******************************************************************************
  ucopy_uns2uns:   */

/*! Copy and translate a sector of a mesh mSec-1 times and assemble.
 */

/*
  Update:
  -------
  23Nov20; factorise out matching of bcs, fix bugs with that.
  27Feb19; use rotAngle (dVal[0]) in rad.
  5Sep17; allow negative periodicity angles, allow period. angle and copy angle to 
  have different signs.


  Input:
  ------
  mSec: number of sectors, including the original one. I.e., a quarter circle
  with a repeat of 30 deg will have mSec=3.
  tr_op: translation/rotation operation, see transform.
  dVal: translation/rotation values, see transform.
  Note, for rot: dVal[0] is rotAngle in rad.

  Changes To:
  -----------
  Grids.

  Returns:
  --------
  0 on failure, 1 on success

*/

/*
  
  Last update:
  ------------
  26Oct20; correctly treat translational periodicity.
  28Feb18: rename rotAngle to rotAngleRad to clarify meaning.
  15Dec18; rename ucopy, to indicate public/prototyped call.
  8Sep18; new arg list to transform.
  6Sep18; new arg doCheck for merge_uns.
  4Sep18; new interface for merge_uns
  5Jul18; correctly treat all combinations of rot and per angles.
  1Mar18; base transfrom call on vx->number, not useMark.
  19Dec17; return pUns, rather than int.
  11May17; switch matchBc=1 to geoType = 1 in bc_struct. 
  6Apr13; remove unused vars.
  18Dec10; allow multiple lu pairs and/or _inlet/outlet pairs.
  new interface to cp_oneUns
  18Oct10; set matchBc indicator.
  14Sep10: conceived.
  
  
*/

uns_s *ucopy_uns2uns ( int mSec, const transf_e tr_op, const double dVal[] ) {

  grid_struct *pGr0 = Grids.PcurrentGrid ;
  int is360 = 0 ;
  uns_s *pUns = pGr0->uns.pUns ;
  int nBc ;
  bc_struct *pBc ;
  grid_struct *pGrCp[MAX_UNS_CP+1] ;
  int mVxNumbered = pUns->mVertsNumbered ;
  uns_s *pUns2 = NULL ;
  double dValN[MAX_DIM] ;
  int nDim ;

  if ( pGr0->uns.type != uns )
    /* unstructured only. */
    hip_err ( fatal, 0, "copy uns2uns needs an unstructured grid." ) ;
  else if ( mSec > MAX_UNS_CP )
    hip_err ( fatal, 0, "too many copies, increase MAX_UNS_CP in cpre_uns.h." ) ;

  const int mDim = pUns->mDim ;
  
  /* Establish correct declaration of periodicity, but don't compute any 
     transformation operators. Rename patches for duplication. */
  //fix_per_setup ( pUns) ;
  /* Need angles and rotation axes here. */
  check_bnd_setup ( pUns ) ;


  if ( tr_op >= rot_x && tr_op <= rot_z ) {
    /* Rotation, Is this a full 360? */
    // was, before using dVal as rad: double a = (mSec+1)*dVal[0] - 360. ;
    double a = (mSec+1)*dVal[0] - (2.0*PI) ;
    is360 = ( ABS( a ) < 1.e-3 ? 1 : 0 ) ; 
  }

  int nSec ;
  int foundPerBc ;
  for  ( nSec = 1 ; nSec <= mSec ; nSec++ ) {
    /* Copy the mesh. Note that this uses make_uns_grid, hence 
       updates currentGrid, but this is set back to pGr0 at end. */
    const int doRenameDuplPerBc = 1 ;
    pUns2 = ucopy_oneUns ( pGr0, nSec, pGrCp+nSec, &mVxNumbered, doRenameDuplPerBc ) ;

    /* Tag all bcs that will become interior as matching. */
    find_bc_to_match_after_copy ( pUns2, nSec, mSec, tr_op, dVal, is360, &foundPerBc ) ;

    /* Perform geometric transformation, translate, rotate, ... */
    if ( tr_op ) {
      for ( nDim = 0 ; nDim < pUns2->mDim ; nDim++ )
        dValN[nDim] = nSec*dVal[nDim] ;
      // any particular reason to useMark, rather than number?
      //transform (  pGrCp[nSec], tr_op, dValN, useMark ) ; 
      transform (  pGrCp[nSec], tr_op, dValN, 0, 1 ) ; 
    }
    
  }

  /* Tag the matched bc of the original sector only after copying the bcs
     to other sectors, in order not to overwrite any user match settings. */
  
  find_bc_to_match_after_copy ( pUns, 0, mSec, tr_op, dVal, is360,
                                &foundPerBc ) ;
  
  if ( is360 && !foundPerBc ) {
    hip_err ( warning, 1, 
              "this is a 360deg configuration, but has no periodic setup.\n"
              "            The begin/end patches will remain in the file.\n" ) ;
  }

  

  
  /* Attach. */
  for  ( nSec = 1; nSec <= mSec ; nSec++ ) {
    add_uns_grid ( pGr0->uns.pUns, pGrCp[nSec]->uns.pUns ) ;
  }

  /* Sequence the names of zones if they originate from one stem. */
  zone_name_sequence ( pUns ) ;

  /* List the boundaries. */
  make_uns_ppBc ( pUns ) ;

  fix_per_setup ( pUns ) ;

  
  /* Merge the grid. */
  if ( tr_op && !merge_uns ( pGr0->uns.pUns, 0, 1 ) ) {
    printf ( "merging of unstructured grids in cp_uns2uns failed." ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  set_current_pGrid ( pGr0 ) ;

  return ( pUns2 ) ;
}


/******************************************************************************
  cp_perPart:   */

/*! Copy and translate a part of a mesh and glue on the other periodic bc.
 *  Add this part to the existing grid, retaining boundaries and zone
 *  labels, but merging coordinates.
 */

/*
  Update:
  -------
  26Feb22; intro useMark arg
  6Sep18; new arg doCheck for merge_uns.
  20Jan18; pass core zones as args
  16Sep17; derived from cp_uns2uns
  28Feb18: rename rotAngle to rotAngleRad to clarify meaning.
  15Dec18; renamed ucopy as for all public funs, standardised order of args.
  remove unused doMerge arg.
  8Sep18; new arg list to transform.
  3Sep18; review documentation.
  17Dec17; cut out of ucopy_uns2uns.


  Input:
  ------
  pUns2: grid to copy to.
  pUns0: grid to copy from.
  iZone2: number of the copied zone
  iZone0: number of the zone to co[y
  pPerBc: pointer to one of the periodic boundary setup
  (they have to be all the same).
  perDir: copy with (>0) or against (<0) dir of perBc
  doUnknowns:
  doMerge: merge the list of vertices in pUns2.

  Returns:
  --------
  pointer to a new or appended Grid.

*/

/*
  
  Last update:
  ------------
  
  
*/

uns_s *ucopy_per_part ( uns_s *pUns0,  const int iReg0, 
                        uns_s *pUns2, const int iReg2,
                        perBc_s *pPerBc, const int perDir,
                        const int doUnknowns, const int useMark ) {


  if ( !pUns0->mPerBcPairs )
    hip_err ( fatal, 0, "no perodicity, but is needed for cp_per_part.\n" ) ; 

  

  /* Copy the zone. */
  const int dontRenameDuplPerBc = 0 ;
  reset_vx_mark ( pUns2 ) ;
  pUns2 = ucopy_elem_region ( pUns0, iReg0, pUns2, iReg2,
                              dontRenameDuplPerBc, doUnknowns, useMark ) ;

  

  double dVal[MAX_DIM], dPerDir = perDir*1.0 ;
  transf_e tr_op = pPerBc->tr_op ;
  /* Rotate/Translate copied zone, L if perDir == 1, U if perDir == -1. */
  if ( tr_op == rot_x )
    // rotAngle in rad. But is the negative angle! To do: sort this.
    dVal[0] = perDir*pPerBc->rotAngleRad ;
  else if ( tr_op == trans ) {
    // direction of translate operation needs to be validated. Not done yet.
    vec_copy_dbl ( pPerBc->shftIn2out, MAX_DIM, dVal ) ;
    vec_mult_dbl ( dVal, dPerDir, MAX_DIM ) ;
  }
  
  /* mark all vx in the zone,  rotate/shift them. */
  ulong_t mElemsZ ;
  ulong_t mConnZ ;
  ulong_t mVxZ ;
  ulong_t mBFcZ ;
  int zero ;
  mark_vx_elem_regions ( pUns2, 1, &iReg2, 0, &zero, useMark,
                        &mElemsZ, &mConnZ, &mVxZ, &mBFcZ ) ;
  /* mark_vx_elem_zones ( pUns2, 1, &iReg2, 0, &zero, */
  /*                        &mElemsZ, &mConnZ, &mVxZ, &mBFcZ ) ; */
  const int useVxMark = 1;
  const int dontCheck = 0 ;
  // transform wants the negative rot angle? Fix this.
  transform ( pUns2->pGrid, tr_op, dVal, useVxMark, dontCheck ) ;
  release_vx_markN ( pUns2, 0 ) ;

  return ( pUns2 ) ;
}


/******************************************************************************
  ucopy_elem_region:   */

/*! copy a region defined by a set of elements of an unstructured grd
 *
 * Copy zones of a grid verbatim, such that the copy can be translated/rotated and
 * attached.
 *
 */
/*
  
  Last update:
  ------------
  6Mar22; fix bug with updating zone/region ro iReg2 after copy. 
  Feb22; switch from zones to regions, to enable use of zone or mark ids.
  16Dec18; copy number of elements in the zone.
  15Dec18; modified from cp_uns_zone.

  Input:
  ------
  pUns2           = grid to copy to
  pUns0           = grid to copy from
  iReg0           = number of zone/mark to be copied
  iReg2           = number of zone/mark of the copy
  renameDuplPerBc = if nonzero, modify name of duplicate periodic bcs.
  doUnknown = if nonzero, do allocate for uknowns
  useMark = if true, regions are given by mark, otherwise by zone

  Returns:
  --------
  pUns2: point to the unstructured grid.
  
*/

uns_s *ucopy_elem_region ( uns_s *pUns0,  const int iReg0, 
                           uns_s *pUns2, const int iReg2,
                           const int renameDuplPerBc,
                           const int doUnknown,
                           const int useMark ) {
#undef FUNLOC
#define FUNLOC "in ucopy_region"  

  if ( useMark ) {
    /* Check whether mark numbers are in range. */
    if ( iReg0 < 0 || iReg0 > SZ_ELEM_MARK-1 )
      hip_err ( fatal, 0, "kMark out of range in"FUNLOC"." ) ;
      
    if ( iReg2 < 0 || iReg2 > SZ_ELEM_MARK-1 )
      hip_err ( fatal, 0, "kMark out of range in"FUNLOC"." ) ;
  }

  
  const int doUseNumber = 1 ;
  const int doReset = 1 ;
  const int doBound = 2 ; // Don't recompute ppBc.
  number_uns_grid_elem_regions ( pUns0, 1, &iReg0, 
                                 doUseNumber, doReset, doBound, useMark ) ;

  /* Count conn entries in the zone. */
  ulong_t mConnReg ;
  ulong_t mElemReg = count_uns_elems_region ( pUns0, iReg0, &mConnReg, useMark ) ;
  
  /* make a grid and allocate. */
  int mDim = pUns0->mDim ;
  ulong_t mUn = ( doUnknown ? pUns0->varList.mUnknowns : 0 ) ;
  ulong_t mBc = pUns0->mBc ;
  ulong_t mBndFcRegs = pUns0->mFaceAllBc + pUns0->mFaceAllInter ;
  chunk_struct *pCh2 ;
  if ( pUns2 ) {
    /* Add a chunk to the existing grid. */
    pCh2 = append_chunk ( pUns2, mDim, mElemReg, mConnReg, 0,
                          pUns0->mVertsNumbered, pUns0->mFaceAllBc, mBc ) ;

    
    /* Copy key parameters such as hMin. */
    pUns2->hMin         = MIN( pUns2->hMin, pUns0->hMin ) ;
    pUns2->hMax         = MAX( pUns2->hMax, pUns0->hMax ) ;
    pUns2->epsOverlap   = MIN( pUns2->epsOverlap, pUns0->epsOverlap ) ;
    pUns2->epsOverlapSq = MIN( pUns2->epsOverlapSq, pUns0->epsOverlapSq ) ;
  }
  else {
    /* Make a new grid. */
    make_uns_grid ( &pUns2, mDim, mElemReg, mConnReg, 0,
                    pUns0->mVertsNumbered, mUn, pUns0->mFaceAllBc, mBc ) ;
    pCh2 = pUns2->pRootChunk ;
  
    /* Copy key parameters such as hMin. */
    pUns2->hMin = pUns0->hMin ;
    pUns2->hMax = pUns0->hMax ;
    pUns2->epsOverlap = pUns0->epsOverlap ;
    pUns2->epsOverlapSq = pUns0->epsOverlapSq ;
    /* Copy solution setup. */
    pUns2->varList = pUns0->varList ;
  }
  
  /* Copy vertices, don't use mark but number to indicate "to be copied". */
  const int dontUseMark = 0 ;
  cp_marked_vx ( pUns0, pUns0->mVertsNumbered, dontUseMark, pCh2, doUnknown ) ;

  /* Elems. */
  chunk_struct *pCh = NULL ;
  elem_struct *pEl, *pElB, *pElE ;
  cp_numbered_elem ( pUns0, mElemReg, mConnReg, pCh2 ) ;
  /* And now set region/zone for copied elems. */
  for ( pEl = pCh2->Pelem+1 ; pEl <= pCh2->Pelem+mElemReg ; pEl++ ) {
    if ( !pEl->invalid ) {
      if ( useMark ) {
        if ( elem_has_mark(pEl,iReg0) ) {
          // set_elem_mark sets the bit for iReg2, but doesn't unset#
          // the one for iReg0.
          // 
          reset_elem_mark( pEl, iReg0 ) ;
          set_elem_mark( pEl, iReg2 ) ;
        }
      }
      else {
        if ( pEl->iZone == iReg0 )
          pEl->iZone = iReg2 ;
      }
    }
    if ( pUns2->pZones && pUns2->pZones[iReg2] )
      pUns2->pZones[iReg2]->mElemsZone += pUns0->mElemsNumbered ;
  }

  /* Boundaries, bnd faces. */
  cp_marked_bnd_fc ( pUns0, pUns0->mFaceAllBc, dontUseMark, renameDuplPerBc, pCh2 ) ;


  
  if ( !useMark ) {
    /* Unmark elems and vx of the donor parts, 
       so operations such as transform can target the copy. */
    pCh = NULL ;
    while ( loop_chunks ( pUns0, &pCh ) ) {
      if ( pCh == pCh2 )
        /* Not this one, done. */
        break ;
      reset_vx_mark_1chunk ( pCh ) ;
      reset_elem_mark_1chunk ( pCh, 0 ) ;
    }
  }
  
  return ( pUns2 ) ;
}







/******************************************************************************
  ucopy_match:   */

/*! copy a region with marked elements to an unstructured grd
 *
 * Copy zones of a grid verbatim, such that the copy can be translated/rotated and
 * attached.
 *
 */
/*
  
  Last update:
  ------------
  6May20; derived from ucopy_zone. Currently not used.

  Input:
  ------
  pUns2           = grid to copy to
  pUns0           = grid to copy from
  iZone2           = number of zone copy
  iZone2           = number of zone to be copied
  renameDuplPerBc = if nonzero, modify name of duplicate periodic bcs.
  doUnknown = if nonzero, do allocate for uknowns

  Returns:
  --------
  pUns2: point to the unstructured grid.
  
*/

uns_s *ucopy_match ( uns_s *pUns0, const match_s *pMatch,
                     uns_s *pUns2,
                     const int doSetMark, const int kElMark2,
                     const int doSetZone, const int iZone2,
                     const int renameDuplPerBc, const int doUnknown ) {

  const int doUseNumber = 1 ;
  const int doReset = 1 ;
  const int doBound = 2 ; // Don't recompute ppBc.
  /* number_uns_grid_zones ( pUns0, 1, &iZone0, 
     doUseNumber, doReset, doBound ) ;*/
  ulong_t mElemMatch, mConnMatch ;
  mElemMatch = number_uns_grid_match ( pUns0, pMatch,
      doUseNumber, doReset, doBound,
      &mConnMatch) ;

  /* Count conn entries in the zone. */
  //ulong_t mElemMatch = count_uns_elems_zone ( pUns0, iZone0, &mConnMatch ) ;
  
  /* make a grid and allocate. */
  int mDim = pUns0->mDim ;
  ulong_t mUn = ( doUnknown ? pUns0->varList.mUnknowns : 0 ) ;
  ulong_t mBc = pUns0->mBc ;
  ulong_t mBndFcZones = pUns0->mFaceAllBc + pUns0->mFaceAllInter ;
  chunk_struct *pCh2 ;
  if ( pUns2 ) {
    /* Add a chunk to the existing grid. */
    pCh2 = append_chunk ( pUns2, mDim, mElemMatch, mConnMatch, 0,
                          pUns0->mVertsNumbered, pUns0->mFaceAllBc, mBc ) ;

    
    /* Copy key parameters such as hMin. */
    pUns2->hMin         = MIN( pUns2->hMin, pUns0->hMin ) ;
    pUns2->hMax         = MAX( pUns2->hMax, pUns0->hMax ) ;
    pUns2->epsOverlap   = MIN( pUns2->epsOverlap, pUns0->epsOverlap ) ;
    pUns2->epsOverlapSq = MIN( pUns2->epsOverlapSq, pUns0->epsOverlapSq ) ;
  }
  else {
    /* Make a new grid. */
    make_uns_grid ( &pUns2, mDim, mElemMatch, mConnMatch, 0,
                    pUns0->mVertsNumbered, mUn, pUns0->mFaceAllBc, mBc ) ;
    pCh2 = pUns2->pRootChunk ;
  
    /* Copy key parameters such as hMin. */
    pUns2->hMin = pUns0->hMin ;
    pUns2->hMax = pUns0->hMax ;
    pUns2->epsOverlap = pUns0->epsOverlap ;
    pUns2->epsOverlapSq = pUns0->epsOverlapSq ;
    /* Copy solution setup. */
    pUns2->varList = pUns0->varList ;
  }
  
  /* Copy vertices, don't use mark but number to indicate "to be copieD". */
  const int dontUseMark = 0 ;
  cp_marked_vx ( pUns0, pUns0->mVertsNumbered, dontUseMark, pCh2, doUnknown ) ;

  /* Elems. */
  /*
  const int iZone0=0; // JDM: needs switch to pMatch.
  const int dontUseNumber = 0 ;
  cp_marked_elem ( pUns0, iZone2, iZone0,
                   mElemMatch, mConnMatch,
                   dontUseNumber, pCh2 ) ; */
  cp_numbered_elem ( pUns0, mElemMatch, mConnMatch, pCh2 ) ;
  //pUns2->pZones[iZone2]->mElemsZone += pUns0->mElemsNumbered ;

  /* Boundaries, bnd faces. */
  cp_marked_bnd_fc ( pUns0, pUns0->mFaceAllBc, dontUseMark, renameDuplPerBc, pCh2 ) ;


  
  if ( dontUseMark ) {
    /* Unmark elems and vx of the donor parts, 
       so operations such as transform can target the copy. */
    chunk_struct *pCh = NULL ;
    while ( loop_chunks ( pUns0, &pCh ) ) {
      if ( pCh == pCh2 )
        /* Not this one, done. */
        break ;
      reset_vx_mark_1chunk ( pCh ) ;
      reset_elem_mark_1chunk ( pCh, 0 ) ;
    }
  }
  
  return ( pUns2 ) ;
}



