/*
  uns_per.c:
  Support for periodic boundaries.

  Last update:
  ------------
  8Apr13; switch all arr_[re]alloc to deference the pointer to get the allocation size.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  7Jul11; limits.h now included in cpre.h
  3Jul10; handle FATAL errors with hip_err.
  13Dec08; in match_perBc: set topo to annCasc if a rotation is found.
  30Mar05; make match_perBc static, change interface.
  20Oct98; use bsearch to address periodic vertices.


  This file contains:
  -------------------
  set_per_rotation:
  set_per_corners:
  match_per_faces:
  gravFc2coor:
  per_cmpVx:
  special_verts:
  perVx2coor:
  list_per_pairs:
  check_bnd_setup:
*/

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;

extern const double per_thresh_is_rot ;



/* Pack some information to be thrown into the tree. */
typedef struct {
  bndFc_struct *pBndFc ;    /* A pointer to the boundary face. */
  double rotGC[MAX_DIM] ; /* The center of gravity of a face. */
  int matched ;             /* Just for safety, flag every matched face. */
} gravFc_s ;

/* Note: the typdefs for the following two are in cpre_uns.h */
/* To search for a match in an _inlet_ and _outlet_ pair. */
struct _perVx_s {
  vrtx_struct *pVx[2] ;      /* The periodic vertex at _inlet in 0, _outlet in 1. */
  double rotCoor[MAX_DIM] ;
  int matched ;
} ;

struct _ndxPerVx_s {
  vrtx_struct *pPerVx ;      /* A vertex pointer as a key. */
  unsigned int nBc:5 ;       /* What periodic patch pair is the vertex on.
                                Note that 2^5=32 contains 2*MAX_PER_PATCH_PAIRS=20.*/
  unsigned int nPerVx:27 ;   /* Which one is it in the patch. */
} ;

#define MAX_COS .9
#define MAX_COS_DIFF 1.e-4

typedef struct {
  char perLabel[MAX_BC_CHAR] ;
  int nBcU ;
  int nBcL ;
} perBcTag_s ;

static int per_cmpVx ( const void* pNdx0, const void* pNdx1 );

/******************************************************************************
  bc_is_per:   */

/*! Given a pointer to a b.c., return 1 if it is a periodic one.
  
  Input:
  ------
  bc_struct *pBc
    
  Returns:
  --------
  1 if bc is per, 0 otherwise

 */

/*
  
  Last update:
  ------------
  18Dec10: conceived.
  
*/

int bc_is_per ( const bc_struct *pBc ) {

  if      ( bc_is_u ( pBc ) )
    return (1) ;
  else if ( bc_is_l ( pBc ) )
    return (1) ;

  else 
    return (0) ;
}

/******************************************************************************
  bc_is_u:   */

/*! Given a pointer to a b.c., return 1 if it is an upper periodic one.
  
  Input:
  ------
  bc_struct *pBc
    
  Returns:
  --------
  1 if bc is u, 0 otherwise

 */

/*
  
  Last update:
  ------------
  18Dec10: conceived.
  
*/

int bc_is_u ( const bc_struct *pBc ) {

  if      ( pBc->type[0] == 'u' )
    return (1) ;

  else if ( !strncmp( pBc->text, "hip_per_outlet", 14 ) )
    return (1) ;

  else 
    return (0) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_perBcPair:
*/
/*! Given a bc, find out whether it is periodic, and if so, l or u.
 *
 *
 */

/*
  
  Last update:
  ------------
  18Dec18: conceived.
  

  Input:
  ------
  pUns: grid
  pBc: bc to check

  Output:
  -------
  *pIsL: 1 if is l, 0 otherwise.
    
  Returns:
  --------
  The pointer to the periodic bc pair on success, Null on failure.
  
*/

perBc_s *find_perBcPair ( const uns_s *pUns, const bc_struct *pBc, int *pIsL ) {

  int nPer ;
  for ( nPer = 0 ; nPer < pUns->mPerBcPairs ; nPer++ ) {
    if ( pBc ==  pUns->pPerBc[nPer].pBc[0] ) {
      *pIsL = 1 ;
      return ( pUns->pPerBc ) ;
    }
    else if  ( pBc ==  pUns->pPerBc[nPer].pBc[1] ) {
      *pIsL = 0 ;
      return ( pUns->pPerBc + nPer ) ;
    }
  }

  return ( NULL ) ;
}


/******************************************************************************
  bc_is_l:   */

/*! Given a pointer to a b.c., return 1 if it is a lower periodic one.
  
  Input:
  ------
  bc_struct *pBc
    
  Returns:
  --------
  1 if bc is l, 0 otherwise

 */

/*
  
  Last update:
  ------------
  18Dec10: conceived.
  
*/

int bc_is_l ( const bc_struct *pBc ) {

  if      ( pBc->type[0] == 'l' )
    return (1) ;

  else if ( !strncmp( pBc->text, "hip_per_inlet", 13 ) )
    return (1) ;

  else 
    return (0) ;
}



/******************************************************************************

  unset_per:
  Remove periodic matching information.
  
  Last update:
  ------------
  12Oct20; fix bug with down copy, add -1 to rhs.
           fix bug with using wrong ptr pPerBc, rather than pUns->pPerBc in free.
  7Feb16; actually made to work and put to use across grids. Remove pUns arg.
  20Dec01: conceived.
  
  Input:
  ------
  pBc: the bc to be unpaired.

  Changes To:
  -----------
  pBc, pPerBc, the matching pBc.

*/


void unset_per ( bc_struct *pBc ) {


  perBc_s *pPerBc ;
  bc_struct *pBcIn, *pBcOut ;
  
  if ( !pBc )
    return ;

  else if ( !( pPerBc = pBc->pPerBc ) )
    /* Nothing to undo. */
    return ;

  pBcIn  = pPerBc->pBc[0] ;
  pBcOut = pPerBc->pBc[1] ;

  pBcIn->pPerBc = pBcOut->pPerBc = NULL ;


  /* Correct the list of periodic bcs in every uns grid. 
     Needs to be done in all grids as the l/u labels are properties of the
     bc, even removing the pair from a current grid would recreate the pair
     from the labels at the next operation. */
  
  grid_struct  *pGrid ;
  uns_s *pUns ;
  perBc_s *pPb ;
  for ( pGrid = Grids.PfirstGrid ; pGrid ; pGrid = pGrid->uns.PnxtGrid ) {
    
    if ( pGrid->uns.type == uns ) {
      pUns = pGrid->uns.pUns ;
      if ( pUns->mPerBcPairs ) {
        /* Find pBc among the pPerBc in this grid. */
        for ( pPb = pUns->pPerBc ; 
              pPb < pUns->pPerBc + pUns->mPerBcPairs ; pPb++ ) {
          if ( pPb->pBc[0] == pBc || pPb->pBc[1] == pBc ) {

            sprintf ( hip_msg, "removing periodic setup for pair:  %s"
                      "  in grid:  %s.", pBc->text, pUns->pGrid->uns.name ) ;
            hip_err ( info, 1, hip_msg ) ;

            /* Copy following pPerBc down. */
            for ( ; pPb < pUns->pPerBc + pUns->mPerBcPairs-1 ; pPb++ ) {
              memcpy ( pPb, pPb+1, sizeof (*pPb ) ) ;
            }
  
            /* Shrink. */
            --pUns->mPerBcPairs ;
            if ( pUns->mPerBcPairs ) {
              /* Realloc. */
              pUns->pPerBc = arr_realloc ( "pPerBc in rm_per_bc", 
                                           pUns->pFam, pUns->pPerBc,
                                           pUns->mPerBcPairs, sizeof( *pPb ) );
            } else {
              /* No perbc left. */
              arr_free ( pUns->pPerBc ) ;
              pUns->pPerBc = NULL ;
            }

            /* Restore the previous pBc->pPerBc pointers invalidated 
               due to the realloc. */
            for ( pPb = pUns->pPerBc ; 
                  pPb < pUns->pPerBc+pUns->mPerBcPairs ; pPb++ ) {
              pPb->pBc[0]->pPerBc = pPb ;
              pPb->pBc[1]->pPerBc = pPb ;
            }
          }
        }
      }
    } // if ( Pgrid->uns.typ
  } // for ( Pgrid = Grids.PfirstGr

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! Remove all periodic boundary setups.
 */

/*
  
  Last update:
  ------------
  12Oct20; new interface to unset_per, pass pUns.
  6Jul18: conceived.
  

  Input:
  ------
  pUns: unstructured grid
  
*/

void unset_all_perBc ( uns_s *pUns ) {

  int k ;
  perBc_s *pPerBc ;
  for ( k = 0 ; k < pUns->mPerBcPairs ; k++ ) {
    unset_per ( pUns->pPerBc[k].pBc[0] ) ;
  }
  pUns->mPerBcPairs = 0 ;
  arr_free ( pUns->pPerBc ) ;
  pUns->pPerBc = NULL ;
  
  return ;
}


/******************************************************************************

  trans_or_rot:
  Given two patch normals, find the matching transformation operation.
  Only specific types of transformation, the usual ones, are considered:
  - translation along one of the coordinate axes
  - rotation around the x-axis in 3D.
  
  
  Last update:
  ------------
  28Apr20; fix copy&paste bug with rot_x assigned for y,z. 
  7Apr13; fix bug with & instead of && in else if condition.
  3Sep12; remove epsOverlap from rotation threshold.
  30Aug11; allow translation in other directions than x,y,z only.
           simplify logic, look at the sum of normals only.
  5Jul11; tread 2D.
          fix bug with wrong oder args in/out in set_trans.
  1Apr11; intro threshold, fix bugs with logic.
  22Dec10; change the way a rotation is computed.
  16Dec08; new interface to mark_uns_vertBc.
  13Dec08; set topo to annCasc if a rotation is found.
  20Jun06; allow all axes for periodicity.
  5Apr06; fix bug when comparing nBcIn (ranging from 0 ..) to pBc->nr
    (ranging from 1.)
  4Apr06; new interface to mark_uns_vertBc with dontSglNrm.
  30Mar05; make static
  15Dec00; Use radii from the origin/x-axis rather than vec-split.
  13Apr00; when splitting a vector for the gc calculation, set the x component
           to zero, unless the translation is along x.
  : conceived.
  
  Input:
  ------
  nrmIn[]: normal on the in-patch
  nrmOut[]: normal on the out-patch
  mDim: number of dims

  Changes To:
  -----------
  prDir: rotation direction.

  
  Returns:
  --------
  1 if it is rotation, -1 on translation, 0 on error.
  
*/
 

int trans_or_rot ( const double nrmIn[],  const double nrmOut[], 
                   const int mDim, int *prDir ) {
  int k, kZeroDir=0 ;
  int mZeroDir = 0, mOppDir = 0 ;


  for ( k = 0 ; k < mDim ; k++ ) {
    if (  ABS( nrmIn[k] + nrmOut[k] ) < per_thresh_is_rot ) {
      mOppDir++ ;
      *prDir = k ;
    }
    if (  ABS( nrmIn[k] ) < per_thresh_is_rot && ABS( nrmOut[k] ) < per_thresh_is_rot ) {
      mZeroDir++ ;
      kZeroDir = k ;
    }
  }

  if ( mOppDir == mDim )
    /* Translation, all components point in exactly opposite directions. */
    return ( -1 ) ;

  else if ( mOppDir == 1 )
    /* Rotation around one of the major axes, the one with opposing components. */
    return ( 1 ) ;

  else if ( mOppDir == 2 && mZeroDir ==1 ) {
    /* An additional component is zero due to symmetry. But both normals
       have the same zero component in one direction, use that direction. */
    *prDir = kZeroDir ;
    return ( 1 ) ;
  }
  
  else if ( mOppDir == 2 && mDim == 3 ) {
    /* An additional component is zero due to symmetry. Find the 
       coordinate axis that is most orthogonal. But for now, guess x*/
    *prDir = 0 ;
    return ( 1 ) ;
  }
  
  else if ( mDim == 2 ) {
    /* Rotation in 2D. */
    *prDir = 2 ;
    return ( 1 ) ;
  }
  else {
    /* Not a simple case, have the user sort it. */
    sprintf ( hip_msg, "this seems to be a rotation, but not around one of\n"
              "          the major axes. Set it up yourself." ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  return ( 0 ) ;
}




/******************************************************************************

  set_per_corners:
  Make an entry of a periodic patch pair with an unstructured grid. For that
  the grid and the properly labeled boundary patches must exist. The geometric
  operation is specified by three coordinates that specify matching locations
  on each patch. The points must be given such that the set is not linearly
  degenerate.
  
  Last update:
  ------------
  28Feb19; rename rotAngle to rotAngleRad.
  15Dec17; state eqs for computing the rotation matrices.
  4Sep17; allow negative rot angles.
  5Sep15; translate 'lu' into '00'
  8Jul15; pass pUns as argument rather than looking it up in Grids.
  7Jul11; carry qualifying tags into lu label when specified with _inlet_tag.
  20Dec10; treat lu with qualifying tags.
  3April09; reverse rotation direction to maintain positive sense in r.h. rule.
  21Sep98; fix bug in parsing coor argument.
  : conceived.
  
  Input:
  ------
  per_pair0: the ending of the name of the periodic pair.
  coor:     the coordinates of the 2 points in 2D, the 3 in 3D, given as
            xyz_in[0], xyz_out[0], xyz_in[1], ...
  reverse_rot: 1 if the direction is reversed from u->l to keep r.h. rule positive sense.

  Changes To:
  -----------
  pUns->mPerBcPairs: Update pairs with the current grid.
  pUns->pPerBc

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int set_per_corners ( uns_s *pUns,
                      const char per_pair0[], const double coor[6*MAX_DIM],
                      int reverse_rot ) {

  double zeroVec[] = {0.,0.,0.} ;
  bc_struct **ppBc, *pBcIn = NULL, *pBcOut = NULL, *pBcTmp ;
  char bc_name[MAX_BC_CHAR], bc_type[MAX_BC_CHAR] ;
  int nPair, mDim = 0, nIn, nOut, nCoor, nDim, nTmp ;
  double *xIn, *xOut, *vecIn, *vecOut, cosIn, cosOut, vecParall[MAX_DIM],
    tp[MAX_DIM*MAX_DIM], mapVecIn[MAX_DIM], mapVecOut[MAX_DIM], vxCoor[MAX_DIM] ;
  double crProd[MAX_DIM] ;
  perBc_s *pPerBc, *pPb ;
  if ( !pUns ) {
    sprintf ( hip_msg, "there is no grid to have periodic patches." ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  mDim = pUns->mDim ;

  /* pair lu is translated in bc-type to l00 and u00. Do the same here. */
  char per_pair[MAX_BC_CHAR] ;
  if ( strncmp( per_pair0, "lu", 2 ) )
    strncpy ( per_pair, per_pair0, MAX_BC_CHAR ) ;
  else 
    strncpy ( per_pair, "00\0", 3 ) ;
  
  /* Find a matching pair of bcs. Inlet first. */
  /* Couldn't find_bc be used? Does nIn have to be ppBc-pUns->ppBc, or could
     it be (*ppBc)->nr ? */
  snprintf ( bc_name, MAX_BC_CHAR-1, "hip_per_inlet_%s", per_pair ) ;
  snprintf ( bc_type, MAX_BC_CHAR-1, "l%s", per_pair ) ;
  for ( nIn = -1, ppBc = pUns->ppBc ; ppBc < pUns->ppBc + pUns->mBc ; ppBc++ )
    if ( !strcmp ( bc_name, (*ppBc)->text ) ||
         !strcmp ( bc_type, (*ppBc)->type ) ) {
      pBcIn = *ppBc ;
      nIn = ppBc - pUns->ppBc ;
      break ;
    }
  snprintf ( bc_name, MAX_BC_CHAR-1, "hip_per_outlet_%s", per_pair ) ;
  snprintf ( bc_type, MAX_BC_CHAR-1, "u%s", per_pair ) ;
  for ( nOut = -1, ppBc = pUns->ppBc ; ppBc < pUns->ppBc + pUns->mBc ; ppBc++ )
    if ( !strcmp ( bc_name, (*ppBc)->text ) ||
         !strcmp ( bc_type, (*ppBc)->type ) ) {
      pBcOut = *ppBc ;
      nOut = ppBc - pUns->ppBc ;
      break ;
    }

  /* Now there has to be a match. */
  if ( nIn == -1 || nOut == -1 ) {
    sprintf ( hip_msg, "could not find patch hip_per_in/outlet_%s in set_per_corners.",
	     per_pair ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  /* We now accept negative rot angles. */
  /* if (reverse_rot) { */
  /*   /\* Swap patches to maintain a positive rotation using the right hand rule. *\/ */
  /*   pBcTmp = pBcIn ; */
  /*   pBcIn = pBcOut ; */
  /*   pBcOut = pBcTmp ; */
  /*   nTmp = nIn ; */
  /*   nIn = nOut ; */
  /*   nOut = nTmp ; */
  /*   /\* Swap of text lables no longer required as hip no longer needs the patch name to be fixed for perio*\/ */
  /*   /\* Swap text labels. *\/ */
  /*   // sprintf ( pBcIn->text,  "hip_per_inlet_swap_%s",  per_pair ) ; */
  /*   //  sprintf ( pBcOut->text, "hip_per_outlet_swap_%s", per_pair ) ; */
  /*   sprintf ( pBcIn->type,  "l%s",  per_pair ) ; */
  /*   sprintf ( pBcOut->type, "u%s", per_pair ) ; */
  /* }*/


  
  /* Set the types to be l, u. */
  snprintf ( pBcIn->type,  MAX_BC_CHAR-1, "l%s", per_pair ) ;
  snprintf ( pBcOut->type, MAX_BC_CHAR-1, "u%s", per_pair ) ;


  

  /* Is there already such a pair listed with the current grid? */
  for ( pPerBc = pUns->pPerBc ; pPerBc < pUns->pPerBc + pUns->mPerBcPairs ; pPerBc++ )
    if ( pPerBc->pBc[0] == pBcIn && pPerBc->pBc[1] == pBcOut )
      /* Matching pair. */
      break ;
    else if ( pPerBc->pBc[0] == pBcIn || pPerBc->pBc[1] == pBcOut ) {
      /* Only one of them matches. */
      sprintf ( hip_msg, "periodic pair mismatch %d to %d.",
	       pPerBc->pBc[0]->nr, pPerBc->pBc[1]->nr ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  
  if ( pPerBc >= pUns->pPerBc + pUns->mPerBcPairs ) {
    /* New pair, alloc. */
    if ( pUns->mPerBcPairs >= MAX_PER_PATCH_PAIRS ) {
      printf ( "only %d pairs of periodic patches are compiled.\n"
	       "        change MAX_PER_PATCH_PAIRS in cpre_uns.h.",
	       MAX_PER_PATCH_PAIRS ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    
    pUns->pPerBc = arr_realloc ( "pPerBc in set_per_corner", 
                                 pUns->pFam, pUns->pPerBc,
                                 ++pUns->mPerBcPairs, sizeof(*pPerBc ) );
    pPerBc = pUns->pPerBc + pUns->mPerBcPairs - 1 ;
    pPerBc->pPerFc = NULL ;
    pPerBc->mPerFcPairs = 0 ;

    /* Restore the previous pBc->pPerBc pointers invalidated due 
       to the realloc. We added the new pair at the end, so that
       does not need correcting. */
    for ( pPb = pUns->pPerBc ; 
          pPb < pUns->pPerBc+pUns->mPerBcPairs-1 ; pPb++ ) {
      pPb->pBc[0]->pPerBc = pPb ;
      pPb->pBc[1]->pPerBc = pPb ;
    }
  }



  
  /* Fill the boundary patch entries. */
  pPerBc->pBc[0] = pBcIn ;
  pPerBc->pBc[1] = pBcOut ;
  pBcIn->pPerBc  = pPerBc ;
  pBcOut->pPerBc = pPerBc ;

  /* Fill the coordinates. */
  xIn  = pPerBc->xIn ;
  xOut = pPerBc->xOut ;
  for ( nPair = 0 ; nPair < mDim ; nPair++ )
    for ( nCoor = 0 ; nCoor < mDim ; nCoor++ ) {
      xIn [ mDim*nPair + nCoor ] = coor[             mDim*nPair + nCoor ] ;
      xOut[ mDim*nPair + nCoor ] = coor[ mDim*mDim + mDim*nPair + nCoor ] ;
    }

  /* Build the bases and check for degeneracy. */
  vecIn  = pPerBc->vecIn ;
  vecOut = pPerBc->vecOut ;
  vec_diff_dbl ( xIn+mDim,  xIn,  mDim, vecIn ) ;
  vec_norm_dbl ( vecIn, mDim ) ;
  vec_diff_dbl ( xOut+mDim, xOut, mDim, vecOut ) ;
  vec_norm_dbl ( vecOut, mDim ) ;
  
  if ( mDim == 3 ) {
    /* Check whether the vectors in the "patch plane" are not linearly degenerate.
       Of the second vector use only the component normal to the first. */
    /* _inlet_ */
    vec_diff_dbl ( xIn+2*mDim, xIn, mDim, vecIn+mDim ) ;
    vec_norm_dbl ( vecIn+mDim, mDim ) ;
    if ( ( cosIn = scal_prod_dbl ( vecIn, vecIn+mDim, mDim ) > MAX_COS ) ) {
      sprintf ( hip_msg, "the vectors spanning %s are linearly dependent.\n"
	       "          Results may be wrong. Choose another set of three points.",
	       pBcIn->text ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
    /* Project. Note that vec_diff_dbl should allow to copy into one of the summands. */
    vec_copy_dbl ( vecIn, mDim, vecParall ) ;
    vec_mult_dbl ( vecParall, cosIn, mDim ) ;
    vec_diff_dbl ( vecIn+mDim, vecParall, mDim, vecIn+mDim ) ;
    vec_norm_dbl ( vecIn+mDim, mDim ) ;
    /* And make a normal. */
    cross_prod_dbl ( vecIn, vecIn+mDim, mDim, vecIn+2*mDim ) ;
    
    /* _outlet_ */
    vec_diff_dbl ( xOut+2*mDim, xOut, mDim, vecOut+mDim ) ;
    vec_norm_dbl ( vecOut+mDim, mDim ) ;
    if ( ( cosOut = scal_prod_dbl ( vecOut, vecOut+mDim, mDim ) > MAX_COS ) ) {
      sprintf ( hip_msg, "the vectors spanning %s are linearly dependent.\n"
	       "          Results may be wrong. Choose another set of three points.",
	       pBcOut->text ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }

    /* Project. */
    vec_copy_dbl ( vecOut, mDim, vecParall ) ;
    vec_mult_dbl ( vecParall, cosOut, mDim ) ;
    vec_diff_dbl ( vecOut+mDim, vecParall, mDim, vecOut+mDim ) ;
    vec_norm_dbl ( vecOut+mDim, mDim ) ;
    /* And make a normal. */
    cross_prod_dbl ( vecOut, vecOut+mDim, mDim, vecOut+2*mDim ) ;

    /* Compare the angles. */
    if ( ABS( cosIn - cosOut ) > MAX_COS_DIFF ) {
      sprintf ( hip_msg, " the vectors spanning %s and %s\n"
	       "          open different angles. Results may be wrong.\n"
	       " Choose another set of three points.", pBcIn->text, pBcOut->text ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
  else if ( mDim == 2 ) {
    /* Make a normal. */
    vecIn[2]  =  -vecIn[1] ;
    vecIn[3]  =   vecIn[0] ;
    vecOut[2] = -vecOut[1] ;
    vecOut[3] =  vecOut[0] ;
  }



  
  /* Shift or rotation? For the time being allow only one of them. If it is a
     rotation, we require it to be one of the axes. Thus the origin maps to itself. */
  vec_diff_dbl ( zeroVec, pPerBc->xIn, mDim, vxCoor ) ;
  rot_coor_dbl ( vxCoor, pPerBc->vecIn, mDim, mapVecIn ) ;
  vec_diff_dbl ( zeroVec, pPerBc->xOut, mDim, vxCoor ) ;
  rot_coor_dbl ( vxCoor, pPerBc->vecOut, mDim, mapVecOut ) ;
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    if ( ABS( mapVecIn[nDim] ) > 1.e-9 ||  ABS( mapVecOut[nDim] ) > 1.e-9 )
      break ;

  int kRot ;
  if ( nDim >= mDim ) {
    /* Rotation only. */
    vec_ini_dbl ( 0., mDim, pPerBc->shftIn2out ) ;
    vec_ini_dbl ( 0., mDim, pPerBc->shftOut2in ) ;

    /* Calculate the transformation matrices between the two bases.

    We constructed an orthonormal basis with the two first axes tangent to 
    the plane of _inlet, the last one normal. Hence the coordinates of Cartesian
    point X_C in the system attached to the _outlet patch can be written as 
    x_o = x_{o,1}b_{o,1}+x_{o,2}b_{o,2}+x_{o,3}b_{o,3} = [b_{o,}|b_{o,2}|b_{o,3}] x_C
    x_o = B_o x_C
    Since B_o is orthonormal,
    x_C = B_o^T x_o

    Similarly for the _inlet patch.
    x_i = B_i x_C
    x_C = B_i^T x_i

    The rotation from x1 on inlet to x2 on outlet concatenates the two rotations
    x_2 = B_i B_o^T x_1
    and
    x_1 = B-O B_i^T x_2

    */
    transpose_dbl ( pPerBc->vecIn,  mDim, tp ) ;
    matmult_dbl ( tp, pPerBc->vecOut, mDim, pPerBc->rotOut2in ) ;
    transpose_dbl ( pPerBc->vecOut, mDim, tp ) ;
    matmult_dbl ( tp, pPerBc->vecIn, mDim, pPerBc->rotIn2out ) ;

    /* Store a rotation angle around x. Use the cos between the normals to
       the two planes. */
    cosIn = scal_prod_dbl ( vecIn+2*mDim, vecOut+2*mDim, mDim ) ;

    
    pPerBc->rotAngleRad = acos( cosIn ) ;

    /* Axis? */
    trans_or_rot (  vecIn+2*mDim, vecOut+2*mDim, mDim, &kRot ) ;
    switch ( kRot ) {
    case 0: pPerBc->tr_op = rot_x ; break ;
    case 1: pPerBc->tr_op = rot_y ; break ;
    case 2: pPerBc->tr_op = rot_z ; break ;
    default:  pPerBc->tr_op = noTr ;
    }
    
    /* Positive or negative? */
    cross_prod_dbl ( vecIn+2*mDim, vecOut+2*mDim, mDim, crProd ) ;
    if ( crProd[kRot] < 0. ) pPerBc->rotAngleRad = -pPerBc->rotAngleRad ;
  }
  else {
    /* Shift only. */
    pPerBc->tr_op = trans ;
    pPerBc->rotAngleRad = 0. ;
    vec_diff_dbl ( pPerBc->xOut, pPerBc->xIn, mDim, pPerBc->shftIn2out ) ;
    vec_diff_dbl ( pPerBc->xIn, pPerBc->xOut, mDim, pPerBc->shftOut2in ) ;

    /* Rotation matrix is the identity. */
    for ( nDim = 0 ; nDim < MAX_DIM*MAX_DIM ; nDim++ )
      pPerBc->rotOut2in[nDim] = pPerBc->rotIn2out[nDim] = 0. ;
    if ( mDim == 2 ) {
      pPerBc->rotOut2in[0] = pPerBc->rotIn2out[0] = 1. ;
      pPerBc->rotOut2in[3] = pPerBc->rotIn2out[3] = 1. ;
    }
    else {
      pPerBc->rotOut2in[0] = pPerBc->rotIn2out[0] = 1. ;
      pPerBc->rotOut2in[4] = pPerBc->rotIn2out[4] = 1. ;
      pPerBc->rotOut2in[8] = pPerBc->rotIn2out[8] = 1. ;
    }
  }


  return ( 1 ) ;
}


/******************************************************************************

  set_per_rotation:
  Define a periodic pair via rotation axis and angle.
  
  Last update:
  ------------
  8Sep17; remove pUns->rotAngle assignment, no longer in pUns.
  4Sep17; allow negative rotation angles.
  8Jul15; pass pUns as argument rather than looking it up in Grids.
  20Jun06; fix bug as columns were rotated, but indices were not fixed.
  10Apr00; declare axiX topology for rotation around x.
  6Oct99; use radian for rotAngle.
  22Jun99; store rotAngle with pUns. 
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int set_per_rotation ( uns_s *pUns, 
                       const char per_pair[], const char axis[],
                       const double rotAngle ) {

  int reverse_rot = 0 ;
  double *pCo, coor[6*MAX_DIM], rotRad = rotAngle/180*PI ;

  if ( !pUns ) {
    sprintf ( hip_msg, "there is no grid to have periodic patches." ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }

  /* No longer needed, JDM Sep 2017
  if ( rotAngle < 0. ) {
    hip_err ( warning, 3, "reversing periodicity rotation to right-hand positive" ) ;
    reverse_rot = 1 ;
    rotRad = -rotRad ;
    } */

  // No longer stored in pUns, each perBc keeps their own.
  //pUns->perAngleRad = rotRad ;
  
  if ( pUns->mDim == 2 ) {
    /* Rotation about the z-Axis, pointing out of the screen. */
    pUns->specialTopo = axiZ ;

    pCo = coor     ; pCo[0] = 0. ; pCo[1] = 0. ; 
    pCo = coor + 2 ; pCo[0] = 1. ; pCo[1] = 0. ; 
    pCo = coor + 4 ; pCo[0] = 0. ; pCo[1] = 0. ; 
    pCo = coor + 6 ; pCo[0] = cos( rotRad ) ; pCo[1] = sin( rotRad ) ; 
  }
  else if ( axis[0] == 'x' || axis[0] == 'X' ) {
    pUns->specialTopo = axiX ;
    
    pCo = coor ;      pCo[0] = 0. ; pCo[1] = 0. ;            pCo[2] = 0. ;
    pCo = coor + 3 ;  pCo[0] = 1. ; pCo[1] = 0. ;            pCo[2] = 0. ;
    pCo = coor + 6 ;  pCo[0] = 0. ; pCo[1] = 1. ;            pCo[2] = 0. ;
    pCo = coor + 9 ;  pCo[0] = 0. ; pCo[1] = 0. ;            pCo[2] = 0. ;
    pCo = coor + 12 ; pCo[0] = 1. ; pCo[1] = 0. ;            pCo[2] = 0. ;
    pCo = coor + 15 ; pCo[0] = 0. ; pCo[1] = cos( rotRad ) ; pCo[2] = sin( rotRad ) ;
  }
  else if ( axis[0] == 'y' || axis[0] == 'Y' ) {
    pUns->specialTopo = axiY ;
    
    pCo = coor ;      pCo[0] = 0. ;            pCo[1] = 0. ; pCo[2] = 0. ;            
    pCo = coor + 3 ;  pCo[0] = 0. ;            pCo[1] = 1. ; pCo[2] = 0. ;            
    pCo = coor + 6 ;  pCo[0] = 0. ;            pCo[1] = 0. ; pCo[2] = 1. ;            
    pCo = coor + 9 ;  pCo[0] = 0. ;            pCo[1] = 0. ; pCo[2] = 0. ;            
    pCo = coor + 12 ; pCo[0] = 0. ;            pCo[1] = 1. ; pCo[2] = 0. ;            
    pCo = coor + 15 ; pCo[0] = sin( rotRad ) ; pCo[1] = 0. ; pCo[2] = cos( rotRad ) ; 
  }
  else if ( axis[0] == 'z' || axis[0] == 'Z' ) {
    pUns->specialTopo = axiZ ;
    
    pCo = coor ;      pCo[0] = 0. ;            pCo[1] = 0. ;            pCo[2] = 0. ; 
    pCo = coor + 3 ;  pCo[0] = 0. ;            pCo[1] = 0. ;            pCo[2] = 1. ; 
    pCo = coor + 6 ;  pCo[0] = 1. ;            pCo[1] = 0. ;            pCo[2] = 0. ; 
    pCo = coor + 9 ;  pCo[0] = 0. ;            pCo[1] = 0. ;            pCo[2] = 0. ; 
    pCo = coor + 12 ; pCo[0] = 0. ;            pCo[1] = 0. ;            pCo[2] = 1. ; 
    pCo = coor + 15 ; pCo[0] = cos( rotRad ) ; pCo[1] = sin( rotRad ) ; pCo[2] = 0. ; 
  }
  
  return ( set_per_corners ( pUns, per_pair, coor, reverse_rot ) ) ;
}

/******************************************************************************

  match_per_faces:
  Given an unstructured grid with a list of periodic patch pairs, find how the
  faces match and store them in pUns->pPerBc.
  
  Last update:
  ------------
  14Dec19; switch from hip's quad-tree to kdtree
  21Jan18; update matchFc_s to number 0,1, not 1,2.
  18Feb13; convert int to ulong_t where so defined.
  16Dec08; new interface to mark_uns_vertBc.
  20Jun06; use find_nBc in case a patch has been dropped.
  : conceived.
  
  Input:
  ------
  pUns:  The grid.

  Changes To:
  -----------
  pUns->pPerBc
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int match_per_faces ( uns_s *pUns ) {
  
  const int mDim = pUns->mDim, dontPer = 0, dontAxis = 0, dontSglNrm = 0 ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxFc[MAX_VX_FACE] ;
  
  int nIn, nOut, mPerFc=0, nDim ;
  ulong_t mVxBc[2], mBiBc[2], mTriBc[2], mQuadBc[2] ;
  int mVxFc, foundPer ;
  perBc_s *pPerBc ;
  double llRotBox[MAX_DIM], urRotBox[MAX_DIM], faceGC[MAX_DIM], dist ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  gravFc_s *pGravFc, *pGF, outGF ;
  matchFc_struct *pPerFc, *pPF ;
  //root_struct *pTree ;
  kdroot_struct *pTree ;
  
  for ( pPerBc = pUns->pPerBc ; pPerBc < pUns->pPerBc + pUns->mPerBcPairs ; pPerBc++ ) {
    arr_free ( pPerBc->pPerFc ) ;
    pPerBc->mPerFcPairs = 0 ;

    /* The two boundary patches to match.  Note that ppBc is ordered by the ordering 
       in memory. nIn and nOut refer to the position of the bc in ppBc et al. */
    nIn  = find_nBc ( pUns, pPerBc->pBc[0] ) ;
    nOut = find_nBc ( pUns, pPerBc->pBc[1] ) ;

    if ( nIn < 0 || nOut < 0 ) {
      printf ( "in match_per_faces: could not find per pair %s", 
               pPerBc->pBc[0]->text ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    
    /* Count the number of periodic faces on these patches. Count only faces,
       don't bother to include periodic vertex siblings. */
    mark_uns_vertBc ( pUns, nIn, dontPer, dontAxis, dontSglNrm, &foundPer, 
                      mVxBc, mBiBc, mTriBc, mQuadBc ) ;
    mark_uns_vertBc ( pUns, nOut, dontPer, dontAxis, dontSglNrm, &foundPer,
                      mVxBc+1, mBiBc+1, mTriBc+1, mQuadBc+1 ) ;
    if ( mBiBc[0] - mBiBc[1] || mTriBc[0] - mTriBc[1] || mQuadBc[0] - mQuadBc[1] ) {
      sprintf ( hip_msg, "mismatch in face numbers of periodic patches:\n"
	       "        %s : %"FMT_ULG", %"FMT_ULG", %"FMT_ULG"  bi, tri, quad faces,\n"
	       "        %s : %"FMT_ULG", %"FMT_ULG", %"FMT_ULG".",
	       pUns->ppBc[nIn]->text, mBiBc[0], mTriBc[0], mQuadBc[0],
	       pUns->ppBc[nOut]->text, mBiBc[1], mTriBc[1], mQuadBc[1] ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    else
      mPerFc = mBiBc[0] + mTriBc[0] + mQuadBc[0] ;

    /* Alloc for mPerFc matching pairs. */
    pPerBc->pPerFc = pPerFc = arr_malloc ( "pPerFc in match_per_faces", pUns->pFam,
                                           mPerFc, sizeof ( *pPerBc->pPerFc ) ) ;
    pGravFc = arr_malloc ( "pGravFc in match_per_faces", pUns->pFam,
                           mPerFc, sizeof ( *pGravFc ) );
    pPerBc->mPerFcPairs = mPerFc ;

    /* Make a list of all periodic faces of this patch and track its bounding box. */
    vec_ini_dbl ( -TOO_MUCH, mDim, urRotBox ) ;
    vec_ini_dbl (  TOO_MUCH, mDim, llRotBox ) ;
    pBndPatch = NULL ;
    pGF = pGravFc-1 ;
    while ( loop_bndFaces_bc ( pUns, nIn, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
	if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {

	  if ( ++pGF >= pGravFc + mPerFc ) {
	    sprintf ( hip_msg, "too many periodic faces (%d) in %s in match_per_faces.",
                      (int)(pGF-pGravFc), pBndPatch->Pbc->text ) ;
	    hip_err ( fatal, 0, hip_msg ) ; }
	  pGF->pBndFc = pBndFc ;
	  pGF->matched = 0 ;
	  face_grav_ctr ( pBndFc->Pelem, pBndFc->nFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;
	  /* What is rotGC in the the basis attached to the patch? */
	  vec_diff_dbl ( faceGC, pPerBc->xIn, mDim, faceGC ) ;
	  rot_coor_dbl ( faceGC, pPerBc->vecIn, mDim, pGF->rotGC ) ;
	  vec_min_dbl ( pGF->rotGC, llRotBox, mDim, llRotBox ) ;
	  vec_max_dbl ( pGF->rotGC, urRotBox, mDim, urRotBox ) ;
	}

    hip_err ( info, 4, "adding faces to the tree structure." ) ;
	    
    /* Initialize the tree. Enlarge the box a bit. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      dist = .1*( urRotBox[nDim] - llRotBox[nDim] ) ;
      dist = MAX( dist, Grids.epsOverlap ) ;
      llRotBox[nDim] -= dist ;
      urRotBox[nDim] += dist ; }
    //pTree = ini_tree ( pUns->pFam, "match_per_faces",
    //                   mDim, llRotBox, urRotBox, gravFc2coor ) ;
    pTree = kd_ini_tree ( pUns->pFam, "match_per_faces",
                          mDim, Grids.epsOverlap,
                          llRotBox, urRotBox, gravFc2coor ) ;
    /* Add all _inlet_ faces to the tree. */
    for ( pGF = pGravFc ; pGF < pGravFc + mPerFc ; pGF++ )
      /* Add the face. */
      //add_data ( pTree, pGF ) ;
      kd_add_data ( pTree, pGF ) ;

    hip_err ( info, 4, "searching for matching periodic faces in the tree structure." ) ;

    /* Loop over all outlet faces. */
    pBndPatch = NULL ;
    pPF = pPerFc - 1 ;
    while ( loop_bndFaces_bc ( pUns, nOut, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
	if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
	  face_grav_ctr ( pBndFc->Pelem, pBndFc->nFace, faceGC, &pFoE, &mVxFc, pVxFc ) ;
	  /* What is rotGC in the the basis attached to the patch? */
	  vec_diff_dbl ( faceGC, pPerBc->xOut, mDim, faceGC ) ;
	  rot_coor_dbl ( faceGC, pPerBc->vecOut, mDim, outGF.rotGC ) ;

	  /* Find a match in the tree. Note that dist has the root taken. */
	  //pGF = nearest_data ( pTree, &outGF, &dist ) ;
	  pGF = kd_nearest_data ( pTree, &outGF, &dist ) ;

          if ( !pGF ) {
            sprintf ( hip_msg, "no matching face found, bad coordinate setup." ) ;
            hip_err ( fatal, 0, hip_msg ) ; }
	  else if ( dist > Grids.epsOverlap )
	    printf ( "face mismatch by %g in match_per_faces.", dist ) ;
	  else if ( pGF->matched ) {
	    sprintf ( hip_msg, "second match on inlet face in match_per_faces." ) ;
	    hip_err ( fatal, 0, hip_msg ) ; }

	  /* List the face. */
	  if ( ++pPF >= pPerFc + mPerFc ) {
	    sprintf ( hip_msg, "too many periodic pairs in match_per_faces." ) ;
	    hip_err ( fatal, 0, hip_msg ) ; }
	  pPF->pElem0 = pGF->pBndFc->Pelem ;
	  pPF->nFace0 = pGF->pBndFc->nFace ;
	  pPF->pElem1 = pBndFc->Pelem ;
	  pPF->nFace1 = pBndFc->nFace ;
	  pGF->matched = 1 ;

	  if ( verbosity > 6 ) {
	    printf ( " match %d:\n", (int)(pPF-pPerFc) ) ;
	    printfcco ( pGF->pBndFc->Pelem, pGF->pBndFc->nFace ) ;
	    printfcco ( pBndFc->Pelem, pBndFc->nFace ) ; }
	}

    /* free tree and spaces. */
    //del_tree ( &pTree ) ;
    kd_del_tree ( &pTree ) ;
    arr_free ( pGravFc ) ;
    pGravFc = NULL ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  gravFc2coor:
  Extract the coordinates from a gravFc_s for the tree.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

VALU_TYPE *gravFc2coor ( const DATA_TYPE *pGravFc ){
  
  return ( ( ( gravFc_s * )pGravFc )->rotGC ) ;
}


/******************************************************************************

  make_solution_per:
  Enforce periodicity on a solution.
  
  Last update:
  ------------
  7Oct20: fix bug with double increment on pVar for vec vars.
  17Apr20; fix bug with missing kUn in simple periodicity,
           rename to make_solution_per.
  4Sep15; fix bug with mDim components for scalar in max_diff_dbl
  21Apr15; fix major bug with non-incremented pV0, pV1, pUn0, pUn1.
  18Oct13; use the vec flag to treat vector variables.
  4Apr09; replace varTypeS with varList.
  20Apr04: conceived.
  
  Input:
  ------
  pUns
  
*/

void make_solution_per ( uns_s *pUns ) {

  const perVxPair_s *pPV = pUns->pPerVxPair ;
  int mVP = pUns->mPerVxPairs ; 
  if ( !mVP )
    /* No perodicity. */
    return ;

  if ( pUns->varList.varType == noVar )
    /* No solution. */
    return ;



  int *ndx ;
  double *val ;
  const int mUn = pUns->varList.mUnknowns ;
  int *mp ;
  int n ;
  int kNdx ;
  vrtx_struct *pVx1, *pVx0 ;
  double *pUn1, *pUn0 ;
  double *pV0, *pV1 ;
  const perBc_s *pPbc ;
  int kUn ;
  int mComp ;
  const int mDim = pUns->mDim ;
  var_s *pVar ;
  int n0, n1 ;
  int ndx0, ndx1 ;
  double vel0[MAX_DIM], vel1[MAX_DIM] ;
  double fac ;
  double dm = 0.0 ;
  if ( pUns->multPer ) {
    /* Multiple periodicity. Each combination is present once in the
       list of periodic edges, no particular order/direction of these
       periodic edges is assumed here.
       Loop over all edges, accumulate the sum of values of corresponding vertices
       and a counter of how may there are. Then divide. */ 

    /* An index from global vertex number to position in val. 
       JDM, Oct 2013: this is brutal. Replace this with a hash table. */
    ndx = arr_calloc ( "ndx in make_solution_per", pUns->pFam, pUns->mVertsNumbered+1,
                       sizeof( *ndx ) ) ;
    /* a table of unknowns for each periodic vertex. There can be at most 2*mPerVxPair
       times number of periodic nodes. */
    val = arr_calloc ( "val in make_solution_per", pUns->pFam, 2*pUns->mPerVxPairs+1,
                       mUn*sizeof( *val ) ) ;
    /* a counter of incident vertices. */
    mp = arr_calloc ( "mp in make_solution_per", pUns->pFam, 2*pUns->mPerVxPairs+1,
                      sizeof( *mp ) ) ;


    /* Initialise ndx and val. */
    for ( n = kNdx = 0 ; n < mVP ; n++ ) {
      pVx0 = pPV[n].In ;
      n0 = pVx0->number ;
      if ( !ndx[n0] ) { 
        /* First encounter for this vertex. Add to the list. */
        ndx0 = ndx[n0] = ++kNdx ;
        mp[ndx0] = 1 ;
        /* Initialise the averaged value. */
        vec_copy_dbl ( pVx0->Punknown, mUn, val + mUn*ndx0 ) ;
      }

      pVx1 = pPV[n].Out ;
      n1 = pVx1->number ;
      if ( !ndx[n1] ) {
        /* First encounter for this vertex. Add to the list. */
        ndx1 = ndx[n1] = ++kNdx ;
        mp[ndx1] = 1 ;
        /* Initialise the averaged value. */
        vec_copy_dbl ( pVx1->Punknown, mUn, val + mUn*ndx1 ) ;
      }
    }


    /* Add the other periodic edge contributions and increment counters. */
    for ( n = 0 ; n < mVP ; n++ ) {
      pVx0 = pPV[n].In ;
      pVx1 = pPV[n].Out ;
      ndx0 = ndx[pVx0->number] ;
      ndx1 = ndx[pVx1->number] ;
      pV0 = val + mUn*ndx0 ;
      pV1 = val + mUn*ndx1 ;
      pUn0 = pVx0->Punknown ;
      pUn1 = pVx1->Punknown ;
      mp[ndx0]++ ;
      mp[ndx1]++ ;
      pPbc = pPV[n].pPerBc ;

      /* Loop over all variables, treat vecs different from scalars. */
      for ( kUn = 0 ; kUn < mUn ; kUn += mComp ) {
        pVar = pUns->varList.var + kUn ;
        if ( pVar->isVec == 1 ) {
          /* First component of a vector variables. Treat the next mDim 
             as vector with mDim comp. */
          mComp = mDim ;
          rot_coor_dbl ( pUn0+kUn, pPbc->rotIn2out, mDim, vel0 ) ;
          rot_coor_dbl ( pUn1+kUn, pPbc->rotOut2in, mDim, vel1 ) ;
          max_diff_vec_dbl ( pUn0+kUn, vel1, mDim, &dm ) ;
          max_diff_vec_dbl ( pUn1+kUn, vel0, mDim, &dm ) ;
          vec_add_dbl ( pV0+kUn, vel1, mDim, pV0+kUn ) ;
          vec_add_dbl ( pV1+kUn, vel0, mDim, pV1+kUn ) ;
        }
        else {
          /* Scalar. */
          mComp = 1 ;
          max_diff_vec_dbl ( pUn0+kUn, pUn1+kUn, 1, &dm ) ;
          vec_add_dbl (pV0+kUn,  pUn1+kUn, 1, pV0+kUn ) ;
          vec_add_dbl (pV1+kUn,  pUn0+kUn, 1, pV1+kUn ) ;
        }
      }
    }


    /* Copy back and normalise. */
    for ( n = 0 ; n < mVP ; n++ ) {
      pVx0 = pPV[n].In ;
      n0 = pVx0->number ;
      if ( ( ndx0 = ndx[n0] ) ) {
        pV0 = val + mUn*ndx0 ;
        vec_mult_dbl ( pV0, 1.0/mp[ndx0], mDim ) ;
        vec_copy_dbl ( pV0, mDim, pVx0->Punknown ) ;
        ndx[n0] = 0 ;
      }

      pVx1 = pPV[n].Out ;
      n1 = pVx1->number ;
      if ( ( ndx1 = ndx[n1] ) ) {
        pV1 = val + mUn*ndx1 ;
        vec_mult_dbl ( pV1, 1.0/mp[ndx1], mDim ) ;
        vec_copy_dbl ( pV1, mDim, pVx1->Punknown ) ;
        ndx[n1] = 0 ;
      }
    }

    arr_free ( ndx ) ;
    arr_free ( val ) ;
    arr_free ( mp ) ;

    hip_err ( info, 3, "ensured multiple periodicity of solution." ) ;
  }


  else {
    /* Single periodicity. Just average across each periodic edge, but rotate
       vectors before averaging. */
    for ( n = 0 ; n < mVP ; n++ ) {
      pVx0 = pPV[n].In ;
      pVx1 = pPV[n].Out ;
      pUn0 = pVx0->Punknown ;
      pUn1 = pVx1->Punknown ;
      pPbc = pPV[n].pPerBc ;

      for ( kUn = 0 ; kUn < mUn ; kUn += mComp ) {
        pVar = pUns->varList.var + kUn ;
        if ( pVar->isVec == 1 ) {
           /* First component of a vector variable. Treat the next mDim 
             as vector with mDim comp. */
          mComp = mDim ;
          rot_coor_dbl ( pUn0+kUn, pPbc->rotIn2out, mDim, vel0 ) ;
          rot_coor_dbl ( pUn1+kUn, pPbc->rotOut2in, mDim, vel1 ) ;

          if ( pVx0 == pVx1 ) {
            /* Node on an axis. Should align vec with axis, but we don't know which.*/
            max_diff_vec_dbl ( vel1, vel0, mDim, &dm ) ;
            vec_avg_dbl ( vel1, vel0, mDim, pUn1+kUn ) ;
          }
          else {
            /* Two different vertces. */
            max_diff_vec_dbl ( pUn0+kUn, vel1, mDim, &dm ) ;
            max_diff_vec_dbl ( pUn1+kUn, vel0, mDim, &dm ) ;
            vec_avg_dbl ( pUn0+kUn, vel1, mDim, pUn0+kUn ) ;
            vec_avg_dbl ( pUn1+kUn, vel0, mDim, pUn1+kUn ) ;
          }
        }
        else {
          /* Scalar. */
          mComp = 1 ;
          max_diff_vec_dbl ( pUn0+kUn, pUn1+kUn, 1, &dm ) ;
          vec_avg2_dbl ( pUn0+kUn, pUn1+kUn, 1 ) ;
        }
      }
    }
  }

  sprintf ( hip_msg, "corrected a maximal periodic error in solution of %g.", dm ) ;
  hip_err ( info, 1, hip_msg ) ;

  return ;
}

/******************************************************************************

  sort_per_pairs:
  Find all pairs of periodic vertices and sort them for number.
  Pairings are one-sided, point from lower to upper, ordered for lower. 
  Per nodes on the lower surface can be matched with bsearch through
  the list.
  
  Last update:
  ------------
  29Apr20; rename to make_ to more clearly identify purpose.
  27Apr20; add doc on one-sidedness.
  14Dec19; switch from hip's quad-tree to kdtree
  20Dec16; fix bug with tracking max periodic vertex discrepancy.
  4Apr13; modified interface to loop_elems
  18Feb13; convert int to ulong_t where so defined.
  16Dec08; call mark_uns_vertBc with doAxis.
  20Jun06; use find_nBc in case a patch has been dropped.
  4Apr06; new interface to mark_uns_vertBc with dontSglNrm.
  14Jan01; use dontPer in call to  mark_uns_vertBc.
  : conceived.
  
  Input:
  ------
  pUns       = 
  pPerVxBc   = 
  ndxPerVxBc = 
  mPerVxBc   = 

  Changes To:
  -----------
  pPerVxBc   = array of pointers to lists of periodic pairs for each bc.
  ndxPerVxBc = an index for each vx on each bc which periodic pair it belongs to.
  mPerVxBC   = number of periodic vertices on each per. pair.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_perVxPairs ( uns_s *pUns,
                       perVx_s *pPerVxBc[MAX_PER_PATCH_PAIRS],
                       ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS],
                       int mPerVxBc[MAX_PER_PATCH_PAIRS] ) {
  
  const int mDim = pUns->mDim, dontPer = 0, dontSglNrm = 0, doAxis = 1 ;

  int nIn, nOut, nDim ;
  ulong_t mVxBc[2], mBiBc[2], mTriBc[2], mQuadBc[2], mPerVx=0, mVxIn=0, mVxOut=0 ;
  int llNr, urNr, nPerBc, nVxBeg, nVxEnd, nPerVx, foundPer, change = 1 ;
  perBc_s *pPerBc ;
  double llRotBox[MAX_DIM], urRotBox[MAX_DIM], vxCoor[MAX_DIM], dist,
    maxDist = -TOO_MUCH ;
  //root_struct *pTree ;
  kdroot_struct *pTree ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd, *pVxMaxIn = NULL, *pVxMaxOut = NULL ;
  perVx_s *pPerVx=NULL, *pPV, outPV ;
  ndxPerVx_s *pndxIn, *pndxOut ;


  /* Reset the list of periodic vertex marks. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      pVrtx->per = 0 ;
  
          
          
  /* Reset the list, so it can be freed on failure w/o problems. */
  for ( nDim = 0 ; nDim < MAX_PER_PATCH_PAIRS ; nDim++ )
    pPerVxBc[nDim] = NULL ;

  /* Keep a bounding box of vertex numbers.
  jDM, 15Dec2017: what for?
  llNr = INT_MAX ;
  urNr = INT_MIN ; */
  
  for ( nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ ) {
    pPerBc = pUns->pPerBc + nPerBc ;
    /* The two boundary patches to match. Note that ppBc is ordered by
       pBc->order. nIn and nOut refer to the position of the bc in ppBc et al. */
    nIn  = find_nBc ( pUns, pPerBc->pBc[0] ) ;
    nOut = find_nBc ( pUns, pPerBc->pBc[1] ) ;

    if ( nIn < 0 || nOut < 0 ) {
      sprintf ( hip_msg, "could not find per pair %s in make_perVxPairs.", 
               pPerBc->pBc[0]->text ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    
    /* Count the number of periodic vertices on these patches. Mark all vertices
       on this patch. */
    mark_uns_vertBc ( pUns, nIn, dontPer, doAxis, dontSglNrm, 
                      &foundPer, mVxBc, mBiBc, mTriBc, mQuadBc ) ;
    mPerVx = mPerVxBc[nPerBc] = mVxBc[0] ;
    
    
    /* Alloc for mPerVx matching pairs. */
    pPerVx = arr_malloc ( "pPerVx in make_perVxPairsair", pUns->pFam,
                                  mPerVx, sizeof ( *pPerVx ) ) ;
    pPerVxBc[nPerBc] = pPerVx ;
    
    /* Make a list of all _inlet_ vertices of this patch and track 
       the bounding box of all patch vertices in the coor system aligned with the patch,
       vecIn. */
    vec_ini_dbl ( -TOO_MUCH, mDim, urRotBox ) ;
    vec_ini_dbl (  TOO_MUCH, mDim, llRotBox ) ;
    pChunk = NULL ;
    pPV = pPerVx-1 ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->mark ) {
        if ( ++pPV >= pPerVx + mPerVx ) {
            sprintf ( hip_msg, "too many periodic vertices (%d) in %s"
                      " in make_perVxPairs.",
                      (int)(pPV-pPerVx), pUns->ppBc[nIn]->text ) ;
            hip_err ( fatal, 0, hip_msg ) ; }

          pVrtx->per = 1 ;
	  pPV->pVx[0] = pVrtx ;
	  pPV->matched = 0 ;
	  /* What are the vertex' coordinates in the the basis attached to the patch? */
	  vec_diff_dbl ( pVrtx->Pcoor, pPerBc->xIn, mDim, vxCoor ) ;
	  rot_coor_dbl ( vxCoor, pPerBc->vecIn, mDim, pPV->rotCoor ) ;
	  vec_min_dbl ( pPV->rotCoor, llRotBox, mDim, llRotBox ) ;
	  vec_max_dbl ( pPV->rotCoor, urRotBox, mDim, urRotBox ) ;
          /* JDM, 15Dec17, what for?
	  llNr = MIN( llNr, pVrtx->number ) ;
	  urNr = MAX( urNr, pVrtx->number ) ;*/
	}


    hip_err ( info, 4, "adding periodic nodes to the tree structure." ) ;
    
    /* Initialize the tree. Enlarge the box a bit. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      dist = .1*( urRotBox[nDim] - llRotBox[nDim] ) ;
      dist = MAX( dist, Grids.epsOverlap ) ;
      llRotBox[nDim] -= dist ;
      urRotBox[nDim] += dist ;
    }
    /*pTree = ini_tree ( pUns->pFam, "sort_perVxPairs",
      mDim, llRotBox, urRotBox, perVx2coor ) ;*/
    pTree = kd_ini_tree ( pUns->pFam, "sort_perVxPairs",
                          mDim, Grids.epsOverlap,
                          llRotBox, urRotBox, perVx2coor ) ;

    
    /* Add all _inlet_ vertices, in the patch coor sys, to the tree. */
    mVxIn = 0 ;
    for ( pPV = pPerVx ; pPV < pPerVx + mPerVx ; pPV++ ) {
      /* Add the face. */
      mVxIn++ ;
      // add_data ( pTree, pPV ) ;
      kd_add_data ( pTree, pPV ) ;
    }

    

    /* Loop over all _outlet_ verts. */
    mark_uns_vertBc ( pUns, nOut, dontPer, doAxis, dontSglNrm, &foundPer,
                      mVxBc+1, mBiBc+1, mTriBc+1, mQuadBc+1 ) ;
    if ( mVxBc[0] - mVxBc[1] ) {
      sprintf ( hip_msg, "mismatch in number of nodes of periodic patches:\n"
	       "        %d:%s : %"FMT_ULG", \n        %d:%s : %"FMT_ULG".",
                nIn+1, pUns->ppBc[nIn]->text, mVxBc[0], 
                nOut+1, pUns->ppBc[nOut]->text, mVxBc[1] ) ;
      //hip_err ( fatal, 0, hip_msg ) ; 
    }


    hip_err ( info, 4, "searching for matching periodic nodes in the tree structure." ) ;
    
    mVxOut = 0 ;
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
	if ( pVrtx->mark ) {
          pVrtx->per = 1 ;
          mVxOut++ ; 
          /* What are the vertex' coordinates in the the basis 
             attached to the _outlet patch? */
          vec_diff_dbl ( pVrtx->Pcoor, pPerBc->xOut, mDim, vxCoor ) ;
          rot_coor_dbl ( vxCoor, pPerBc->vecOut, mDim, outPV.rotCoor ) ;

          /* Find a match in the tree. Note that dist has the root taken. */
	  //pPV = nearest_data ( pTree, &outPV, &dist ) ;
	  pPV = kd_nearest_data ( pTree, &outPV, &dist ) ;

          if ( !pPV ) {
	    sprintf ( hip_msg, "no match in make_perVxPairs."
                      " Bad coordinate setup?" );
            printf ( "               vertex on _outlet: " ) ;
            printvxco ( pVrtx, mDim ) ;
            printf ( "     rotated coordinates in _inlet frame: " ) ;
            printco ( outPV.rotCoor, mDim ) ;
            hip_err ( fatal, 0, hip_msg ) ;
	    goto failure ;
          }
          else if ( pPV->matched ) {
	    sprintf ( hip_msg, "found double match for vertex %"FMT_ULG" in"
                      " make_perVxPairs.",
		     pPV->pVx[0]->number ) ;
            hip_err ( fatal, 0, hip_msg ) ;
	    goto failure ;
          }

          if ( dist > Grids.epsOverlap ) {
            if ( verbosity > 4 ) {
              sprintf ( hip_msg, "periodic vertex mismatch by %g"
                        " in make_perVxPairs.\n"
                        "               vertex on _outlet: ", dist ) ;
              printvxco ( pVrtx, mDim ) ;
              printf ( "     rotated coordinates in _inlet frame: " ) ;
              printco ( outPV.rotCoor, mDim ) ;
              printf ( "               vertex on _inlet:  " ) ;
              printvxco ( pPV->pVx[0], mDim ) ;
              hip_err ( warning, 1, hip_msg ) ;
            }
            else if ( verbosity > 3 ){
              sprintf ( hip_msg, "found periodic vertex mismatch by %g between"
                        " in/out %"FMT_ULG"/%"FMT_ULG".",
                        dist, pPV->pVx[0]->number, pVrtx->number ) ;
              hip_err ( warning, 1, hip_msg ) ;
            }
          }
          if ( dist > maxDist ) {
            pVxMaxIn = pPV->pVx[0] ;
            pVxMaxOut = pVrtx ;
            maxDist = dist ;
          }
          
	  if ( Grids.fixPerVx ) {
            /* Fix the outlet patch vertex. */
            rot_coor_dbl ( pPV->pVx[0]->Pcoor, pPerBc->rotIn2out, mDim, 
                           pVrtx->Pcoor ) ;
            vec_add_dbl ( pVrtx->Pcoor, pPerBc->shftIn2out, mDim, pVrtx->Pcoor ) ;

            /* Check rotation by rotating back. */
            /*  double someCoor[3], diff[3] ;
             rot_coor_dbl ( pVrtx->Pcoor, pPerBc->rotOut2in, mDim, 
                            someCoor ) ;
            vec_add_dbl ( pVrtx->Pcoor, pPerBc->shftOut2in, mDim, pVrtx->Pcoor ) ;
            vec_diff_dbl ( someCoor, pPV->pVx[0]->Pcoor, mDim, diff ) ;
            int blah=1; */
          }
          
          pPV->pVx[1] = pVrtx ;
	  pPV->matched = 1 ;
	}
    
    /* free tree. */
    // del_tree ( &pTree ) ;
    kd_del_tree ( &pTree ) ;
  }



  /* Checking. */
  if ( mVxIn - mVxOut ) {
    sprintf ( hip_msg, "differing number of per vx:"
              " in %"FMT_ULG", out: %"FMT_ULG"\n", 
              mVxIn, mVxOut ) ; 
    //hip_err ( fatal, 0, hip_msg ) ;
  }

  for ( pPV = pPerVx ; pPV < pPerVx + mPerVx ; pPV++ ) {
    if ( !pPV->matched ) {
      sprintf ( hip_msg, "unmatched periodic vertex"
                " %"FMT_ULG" on bnd %d ppV %td\n", 
                pPV->pVx[0]->number, nPerBc, pPV-pPerVx ) ;
      //hip_err ( fatal, 0 hip_msg ) ;
    }
  }

  
  /* Make a sorted index for each list. */
  for ( nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ ) {
    pPerVx = pPerVxBc[nPerBc] ;

    ndxPerVxBc[2*nPerBc] = 
      arr_malloc ( "ndxPerVxBc[0] in make_perVxPairs", pUns->pFam,
                   mPerVxBc[nPerBc], sizeof( *(ndxPerVxBc[2*nPerBc]) ) ) ;
    ndxPerVxBc[2*nPerBc+1] = 
      arr_malloc ( "ndxPerVxBc[1] in make_perVxPairs",
                   pUns->pFam,
                   mPerVxBc[nPerBc],sizeof( *(ndxPerVxBc[2*nPerBc+1]) ) );
    pndxIn  = ndxPerVxBc[2*nPerBc] ;
    pndxOut = ndxPerVxBc[2*nPerBc+1] ;
         
    for ( nPerVx = 0 ; nPerVx < mPerVxBc[nPerBc] ; nPerVx++ ) {
      pPerVx = pPerVxBc[nPerBc] + nPerVx ;

      /* _inlet. */
      pndxIn->pPerVx = pPerVx->pVx[0] ;
      pndxIn->nBc    = 2*nPerBc ;
      pndxIn->nPerVx = nPerVx ;
      pndxIn++ ;

      /* _Outlet. */
      pndxOut->pPerVx = pPerVx->pVx[1] ;
      pndxOut->nBc    = 2*nPerBc+1 ;
      pndxOut->nPerVx = nPerVx ;
      pndxOut++ ;
    }

    /* Sort the two indices. */
    qsort ( ndxPerVxBc[2*nPerBc],   mPerVxBc[nPerBc], 
            sizeof( ndxPerVx_s ), per_cmpVx ) ;
    qsort ( ndxPerVxBc[2*nPerBc+1], mPerVxBc[nPerBc], 
            sizeof( ndxPerVx_s ), per_cmpVx ) ;
  }


  char hip_msg2[LINE_LEN] ;
  if ( verbosity > 1 ) {
    if ( Grids.fixPerVx )
      sprintf ( hip_msg2, "corrected a" ) ;
    else
      sprintf ( hip_msg2, "found and left a" ) ;
    sprintf ( hip_msg, "%s maximal periodic mismatch of %g\n"
              "             between vertices %"FMT_ULG", %"FMT_ULG".", hip_msg2,
              maxDist, pVxMaxIn->number, pVxMaxOut->number ) ;
    hip_err ( info, 1, hip_msg ) ;
  }

  if ( change ) {
    /* Some vertices have moved. Compare minimum edge lengths to epsOverlap. */
    chunk_struct *pChunk = NULL ;
    elem_struct *pElem, *pElBeg, *pElEnd ;
    double hMin = TOO_MUCH, hMax = -TOO_MUCH ;

    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd )  )
      for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
        if ( pElem->number && pElem->term && 
             get_degenEdges ( pElem, pChunk, &hMin, &hMax, &dist, 0, 
                              pUns->epsOverlapSq ) ) {
          sprintf ( hip_msg, "fixing periodic vertices leads to a degenerate edge"
                   " in sort_perVx_pairs.\n"
                   "        Either give a better mesh, adjust periodicity or\n"
                   "        set epsOverlap by hand to value smaller than %g.", 
                   dist ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }
  }

  return ( 1 ) ;


  failure:
  //del_tree ( &pTree ) ;
  kd_del_tree ( &pTree ) ;
  for ( nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ )
    arr_free ( pPerVxBc[nPerBc] ) ;

  return ( 0 ) ;  
}



/******************************************************************************

  speical_verts:
  Make lists of special vertices. These can be symmetry vertices (their normal
  changes), vertices on a degenerate axis (specially listed), periodic vertices.
  
  Last update:
  ------------
  15Dec13; replace x_axis_verts with axis_verts.
  18Feb13; convert int to ulong_t where so defined.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  16Dec08; new interface to mark_uns_vertBc.
  16Dec08; compute x_axis_verts before computing periodic pairs.
  27Nov99; fix symmetry treatment, change the name to special_verts.
  22Dec98; add periodic partners to each patch.
  : conceived.
  
  Input:
  ------
  pUns:  The grid.

  Changes To:
  -----------
  pUns->pmPerVxPairs
  pUns->pPerVxPair
  pUns->multPer

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int special_verts ( uns_s *pUns ) {

  const int dontPer = 0, doAxis = 1, dontSglNrm = 0 ;
  
  ulong_t mVxBc[2], mBiBc[2], mTriBc[2], mQuadBc[2] ;
  int mPerVxBc[MAX_PER_PATCH_PAIRS], nVxBeg, nVxEnd, nBc, mSymmVx, foundPer ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd, **ppSymmVx = NULL ;
  perVx_s *pPerVxBc[MAX_PER_PATCH_PAIRS] ;
  ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS] ;

  
  /* Loop over all boundaries and make a list of symmetry vertices. If there are
     more than one symmetry patch, a symmetry vertex might be listed twice. */
  for ( mSymmVx = nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->ppBc[nBc]->type[0] == 's' ) {
      mark_uns_vertBc ( pUns, nBc, dontPer, doAxis, dontSglNrm, 
                        &foundPer, mVxBc, mBiBc, mTriBc, mQuadBc ) ;

      /* Realloc the list of periodic pairs. */
      ppSymmVx = arr_realloc ( "ppSymmVx in special_verts", pUns->pFam, ppSymmVx,
                               mSymmVx+mVxBc[0], sizeof( *ppSymmVx ) ) ;
      
      /* Add the symmetry vertices to the list. */
      pChunk = NULL ;
      while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
        for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
          if ( pVrtx->mark )
            ppSymmVx[mSymmVx++] = pVrtx ;
    }
  pUns->ppSymmVx = ppSymmVx ;
  pUns->mSymmVx = mSymmVx ;



  /* Find a geometric translation/rotation for each periodic pair if not given. */
  if ( (check_bnd_setup ( pUns ) ).status != success ) {
    sprintf ( hip_msg, "failed to find a geometric match for periodic bc"
             " in special_verts" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  


  /* Mark all singular vertices. */
  axis_verts ( pUns, pUns->specialTopo ) ;


  /* Establish periodicity. */
  arr_free ( pUns->pPerVxPair ) ;
  pUns->pPerVxPair = NULL ;
  
  if ( pUns->mPerBcPairs == 0 ) {
    /* No periodicity. */
    pUns->mPerVxPairs = 0 ;
    pUns->multPer = 0 ;
  }
  else {

    /* Make a sorted list of one to one pairs. */
    if ( !make_perVxPairs ( pUns, pPerVxBc, ndxPerVxBc, mPerVxBc ) ) {
      sprintf ( hip_msg, "failed to sort periodic vertices in special_verts." ) ;
      hip_err ( fatal, 0, hip_msg ) ; }

  
    /* Make two lists of in/out and check for multiplicity. */
    if ( !mult_per_vert ( pUns, mPerVxBc, pPerVxBc, ndxPerVxBc, 1 ) ) {
      sprintf ( hip_msg, "couldn't establish multiple periodicity. in special_verts." ) ;
      hip_err ( fatal, 0, hip_msg ) ; }


    for ( nBc = 0 ; nBc < pUns->mPerBcPairs ; nBc++ ) {
      arr_free ( pPerVxBc[nBc] ) ;
      arr_free ( ndxPerVxBc[2*nBc  ] ) ;
      arr_free ( ndxPerVxBc[2*nBc+1] ) ;
    }

  
    /* Enforce perodicity of the solution. */
    make_solution_per ( pUns ) ;

  }

  return ( 1 ) ;
}

/******************************************************************************

  mult_per_vert:
  Find vertices with multiple periodicity. Use bsearch to make a list
  of multiply equivalent vertices.  For multiple periodicity, all
  possible combinations appear in the list.
  
  Last update:
  ------------
  19Dec06; set special Topo axiX automatically when self-referencing periodic nodes
    are found.
  30Mar00; new pPerVxPair that carries pPerBc.
  22Dec98; move ppVxIn/Out into pUns.
  20Oct98; use bsearch instead of the tree.
  : conceived.
  
  Input:
  ------
  pUns
  ll/urNr
  mPerVxBc
  pPerVxBc
  ndxPerVxBc
  onlyIn = 1/2, if 1 as called from special_verts, then sort per edges to
           have _in vertices in the lower, 0/first position of the edge.

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mult_per_vert ( uns_s *pUns, const int mPerVxBc[], perVx_s *pPerVxBc[],
                    ndxPerVx_s *ndxPerVxBc[2*MAX_PER_PATCH_PAIRS],
                    const int onlyIn ) {
  
  perVxPair_s *pPerVxPair ;
     
  int nPerBc, mVxList, nVx, nnVx, mAllPerVx, mPerVxUsed, nVxList, nNdxBc, kVx ;
  vrtx_struct *pVxList[MAX_PER_VX] ;
  perVx_s *pPV, *pMatchPV ;
  ndxPerVx_s *pNdx, ndxT ;

  pUns->multPer = 0 ;
  
  /* How many periodic pairs are there?. */
  for ( mAllPerVx = nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ )
    mAllPerVx += mPerVxBc[nPerBc] ;

  /* Malloc for a list of periodic pairs. */
  arr_free ( pUns->pPerVxPair ) ;
  pPerVxPair  = arr_malloc ( "pPerVxPair in mult_per_vert", pUns->pFam,
                             mAllPerVx, sizeof( *pPerVxPair ) );
  mPerVxUsed = 0 ;
       
  /* Loop over all periodic vertices. Mark them as unused, unmatched. */
  for ( nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ )
    for ( pPV = pPerVxBc[nPerBc] ; pPV < pPerVxBc[nPerBc] + mPerVxBc[nPerBc] ; pPV++ )
      pPV->matched  = 0 ;

  /* Loop over all periodic vertices. */
  for ( nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ )
    for ( pPV = pPerVxBc[nPerBc] ; pPV < pPerVxBc[nPerBc] + mPerVxBc[nPerBc] ; pPV++ )
      if ( !pPV->matched ) {
        pPV->matched = 1 ;

        /* This vertex is periodic, unmatched so far. Make a list of all vertices
           that are periodic with this one. */
        mVxList = 2 ;
        pVxList[0] = pPV->pVx[0] ;
        pVxList[1] = pPV->pVx[1] ;
      
        /* Loop over all the periodic vertices in the list to find further links. */
        for ( nVxList = 0 ; nVxList < mVxList ; nVxList++ ) {
          ndxT.pPerVx = pVxList[nVxList] ;

          /* Search in all indices. Research the current one to mark usage properly. */
          for ( nNdxBc = 0 ; nNdxBc < 2*pUns->mPerBcPairs ; nNdxBc++ ) 
            if ( ( pNdx = bsearch ( &ndxT, ndxPerVxBc[nNdxBc],  mPerVxBc[nNdxBc/2],
                                    sizeof( ndxPerVx_s ), per_cmpVx ) ) ) {
            
              /* Found a match. Is the other vertex already in the list? */
              pMatchPV = pPerVxBc[ nNdxBc/2 ] + pNdx->nPerVx ;
              pMatchPV->matched = 1 ;
              for ( nnVx = 0 ; nnVx < mVxList ; nnVx++ )
                if ( pVxList[nnVx] == pMatchPV->pVx[ 1-nNdxBc%2 ] )
                  break ;
              if ( nnVx >= mVxList ) {
                /* New item. Append it to the list. */
                if ( mVxList >= MAX_PER_VX ) {
                  printf ( "multiplicity %d for periodic vert. too low.\n"
                           "        increase MAX_PER_VX in cpre_uns.h",
                           MAX_PER_VX ) ;
                  hip_err ( fatal, 0, hip_msg ) ; }
                pUns->multPer = 1 ;
                pVxList[mVxList++] = pMatchPV->pVx[ 1-nNdxBc%2 ] ;
              }
            }
        }
	    
        /* It might be neat to sort the list for node numbers.
        swap = 1 ;
        while ( swap )
          for ( swap = nnVx = 0 ; nnVx < mVxList-1 ; nnVx++ )
            if ( pVxList[nnVx+1]->number < pVxList[nnVx]->number ) {
              swap = 1 ;
              pVx = pVxList[nnVx+1] ;
              pVxList[nnVx+1] = pVxList[nnVx] ;
              pVxList[nnVx] = pVx ;
            } */

        
        /* Append each combination once. */
        for ( nVx = 0 ; nVx < mVxList - 1 ; nVx++ )
          for ( nnVx = nVx+1 ; nnVx < mVxList ; nnVx++ ) {
            if ( mPerVxUsed >= mAllPerVx ) {
              /* Realloc. */
              mAllPerVx = mAllPerVx*REALLOC_FACTOR + 1 ; 
              pPerVxPair = arr_realloc ( "pPerVxPair in mult_per_vert", pUns->pFam,
                                         pPerVxPair, mAllPerVx, sizeof( *pPerVxPair ) );
            }
            pPerVxPair[mPerVxUsed].In     = pVxList[nVx] ;
            pPerVxPair[mPerVxUsed].Out    = pVxList[nnVx] ;
            pPerVxPair[mPerVxUsed].pPerBc = pUns->pPerBc+nPerBc ;
            pPerVxPair[mPerVxUsed].revDir = 0 ;
            mPerVxUsed++ ;

            /* Set the axi flag if the node points to itself. */
            if ( pVxList[nVx] == pVxList[nnVx] )
              pUns->specialTopo = axiX ;
          }
      }


  if ( onlyIn == 1 || !mPerVxUsed ) {
    /* Keep only inlet, l, vertices on the left side (.In) in pPerVxPair,
       as required for writing to file. Final realloc. */
    pUns->mPerVxPairs = mPerVxUsed ;
    if ( !mPerVxUsed ) {
      /* arr_realloc will not handle realloc to 0. Delete. */
      arr_free ( pPerVxPair ) ;
      pUns->pPerVxPair = NULL ;
    }
    else
      pUns->pPerVxPair  = arr_realloc ( "pPerVxPair in mult_per_vert", pUns->pFam,
                                        pPerVxPair, mPerVxUsed, 
                                        sizeof( *pUns->pPerVxPair ) ) ;
  }
  
  else {
    /* Make a list where all vertices are listed on the left (In)-side for
       search operations in mg and adapt. */
    pUns->mPerVxPairs = 2*mPerVxUsed ;
    pPerVxPair = pUns->pPerVxPair  =
      arr_realloc ( "pPerVxPair in mult_per_vert", pUns->pFam,
                    pPerVxPair, 2*mPerVxUsed, sizeof( *pUns->pPerVxPair ) ) ;
    /* Duplicate. */
    for ( kVx = 0 ; kVx < mPerVxUsed ; kVx++ ) {
      pPerVxPair[kVx+mPerVxUsed].In     = pPerVxPair[kVx].Out ;
      pPerVxPair[kVx+mPerVxUsed].Out    = pPerVxPair[kVx].In ;
      pPerVxPair[kVx+mPerVxUsed].pPerBc = pPerVxPair[kVx].pPerBc ;
      pPerVxPair[kVx+mPerVxUsed].revDir = 1-pPerVxPair[kVx].revDir ;
    }
    
    /* Resort. */
    qsort ( pPerVxPair, 2*mPerVxUsed, sizeof( perVxPair_s ), cmp_perVxPair ) ;
  }

  return ( 1 ) ;
}




/******************************************************************************

  find_perVxPartner:
  Given a vertex pointer, find its periodic partner, if there is one.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

vrtx_struct *find_perVxPartner ( uns_s *pUns,
                                 const vrtx_struct *pVx, const int mPerVxBc[],
                                 perVx_s *pPerVxBc[], ndxPerVx_s *ndxPerVxBc[] ) {

  ndxPerVx_s *pNdx, ndxT ;
  int nNdxBc ;

  ndxT.pPerVx = ( vrtx_struct * ) pVx ;

  /* Loop over all boundary lists to find an entry for pVx. */
  for ( nNdxBc = 0 ; nNdxBc < 2*pUns->mPerBcPairs ; nNdxBc++ ) 
    if ( ( pNdx = bsearch ( &ndxT, ndxPerVxBc[nNdxBc], mPerVxBc[nNdxBc/2],
                            sizeof( ndxPerVx_s ), per_cmpVx ) ) )
      /* Match for pVx. Return its partner. */
      return ( pPerVxBc[ nNdxBc/2 ][pNdx->nPerVx].pVx[ 1-nNdxBc%2 ] ) ;
      
  /* No match found. */
  return ( NULL ) ;
}

/******************************************************************************

  perVx2coor:
  Extract the coordinates from a perVx_s for the tree.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

VALU_TYPE *perVx2coor ( const DATA_TYPE *pPerVx ) {
  
  return ( ( ( perVx_s * )pPerVx )->rotCoor ) ;
}

/******************************************************************************

  cmpPerVx:
  Given two vertex pointers, compare them for qsort and bsearch.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int per_cmpVx ( const void* pNdx0, const void* pNdx1 ) {
  return ( (( ndxPerVx_s * ) pNdx0)->pPerVx -
           (( ndxPerVx_s * ) pNdx1)->pPerVx ) ;
}

/******************************************************************************

  list_per_pairs:
  List all periodic pairs in a grid.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int list_per_pairs ( const grid_struct *pGrid ) {
  
  const uns_s *pUns=NULL ;
  int mDim=0, nPt ;
  const perBc_s *pPerBc ;
  
  if ( !pGrid ) {
    sprintf ( hip_msg, "there is no grid to have periodic patches." ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  if ( pGrid->uns.type != uns ) {
    sprintf ( hip_msg, "only unstructured grids can be made periodic." ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  else {
    pUns = pGrid->uns.pUns ;
    mDim = pUns->mDim ;
  }

  for ( pPerBc = pUns->pPerBc ; pPerBc < pUns->pPerBc + pUns->mPerBcPairs ; pPerBc++ ) {
    printf ( "        %-30s          %-30s\n",
	     pPerBc->pBc[0]->text, pPerBc->pBc[1]->text ) ;
    for ( nPt = 0 ; nPt < mDim ; nPt++ ) {
      printf ( " xyz %d: %+10.4e, %+10.4e", nPt,
	       pPerBc->xIn[nPt*mDim], pPerBc->xIn[nPt*mDim+1] ) ;
      if ( mDim == 3 ) printf ( " %+10.4e;", pPerBc->xIn[nPt*mDim+2] ) ;
      else printf ( ";            " ) ;
      printf ( "   %+10.4e, %+10.4e",
	       pPerBc->xOut[nPt*mDim], pPerBc->xOut[nPt*mDim+1] ) ;
      if ( mDim == 3 ) printf ( " %+10.4e\n", pPerBc->xOut[nPt*mDim+2] ) ;
      else printf ( ";\n" ) ;
    }
    for ( nPt = 0 ; nPt < mDim ; nPt++ ) {
      printf ( " vec %d:      %+4.3f,      %+4.3f", nPt,
	       pPerBc->vecIn[nPt*mDim], pPerBc->vecIn[nPt*mDim+1] ) ;
      if ( mDim == 3 ) printf ( "      %+4.3f;", pPerBc->vecIn[nPt*mDim+2] ) ;
      else printf ( ";            " ) ;
      printf ( "        %+4.3f,      %+4.3f",
	       pPerBc->vecOut[nPt*mDim], pPerBc->vecOut[nPt*mDim+1] ) ;
      if ( mDim == 3 ) printf ( "      %+4.3f\n", pPerBc->vecOut[nPt*mDim+2] ) ;
      else printf ( ";\n" ) ;
    }
    printf ( " pairs of faces: %d\n\n", pPerBc->mPerFcPairs ) ;
  }

  return ( 1 ) ;
}

/******************************************************************************
  set_trans:   */

/*! Compute vector for translation periodicity. 
 */

/*
  
  Last update:
  ------------
  29Aug11; drop requirement for xyz axis alignment.
  : conceived.
  

  Input:
  ------
  perLabel
  gcIn: coor of patch gravity centre on bcIn
  gcOut: coor of patch gravity centre on bcOut
  mDim

*/

int set_trans ( uns_s *pUns, const char* perLabel, 
                const double gcIn[], const double gcOut[], 
                const int mDim) {

  double gcDiff[MAX_DIM] ;
  double perCoor[2*MAX_DIM*MAX_DIM] ;
 
  /* Difference between the gravity centers. */
  vec_diff_dbl ( gcOut, gcIn, mDim, gcDiff ) ;


  /* Initialise the bases. */
  vec_ini_dbl ( 0., MAX_DIM*MAX_DIM, perCoor ) ;
  if ( mDim == 2 ) {
    perCoor[2] = 1. ;
    vec_copy_dbl ( perCoor, 4, perCoor+4 ) ;
    vec_add_dbl ( perCoor+4, gcDiff, 2, perCoor+4 ) ;
    vec_add_dbl ( perCoor+6, gcDiff, 2, perCoor+6 ) ;
  }
  else {
    perCoor[3] = 1. ;
    perCoor[7] = 1. ;
    vec_copy_dbl ( perCoor, 9, perCoor+9 ) ;
    vec_add_dbl ( perCoor+9,  gcDiff, 3, perCoor+9 ) ;
    vec_add_dbl ( perCoor+12, gcDiff, 3, perCoor+12 ) ;
    vec_add_dbl ( perCoor+15, gcDiff, 3, perCoor+15 ) ;
  }

    
  if ( set_per_corners ( pUns, perLabel, perCoor, 0 ) && verbosity > 2 ) {
    sprintf ( hip_msg, "found translation by %g, %g", gcDiff[0], gcDiff[1] ) ;
    if ( mDim == 3) sprintf ( strchr ( hip_msg, '\0' ),  ", %g", gcDiff[2] ) ;
    sprintf ( strchr ( hip_msg, '\0' ),   " to match  surface pair %s.", perLabel ) ;
    hip_err ( info, 1, hip_msg ) ;
    return ( 1 ) ;
  }
  else {
    sprintf ( hip_msg, "tried translation, but failed to match." ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 0 ) ;
}


/* Superseded version, allows translation in x, y or z dir only. */
int set_trans_xyz ( uns_s *pUns, const char* perLabel, 
                    const double gcIn[], const double gcOut[], 
                    const int mDim) {

  double gcDiff[MAX_DIM] ;
  int dir=0 ;
  double perCoor[2*MAX_DIM*MAX_DIM] ;
  const char cdir[] = { 'x', 'y', 'z' } ;

 
  /*   difference between the gravity centers. */
  vec_diff_dbl ( gcOut, gcIn, mDim, gcDiff ) ;

  /* Allow only translation in a coordinate direction to avoid problems
     with roundoff. */
  vec_norm_dbl ( gcDiff, mDim ) ;
  int nonStdTrans = 0 ;
  if      ( ABS( gcDiff[0] ) > .9999 ) { dir = 0 ;}
  else if ( ABS( gcDiff[1] ) > .9999 ) { dir = 1 ;}
  else if ( ABS( gcDiff[2] ) > .9999 ) { dir = 2 ;}
  else {
    nonStdTrans = 1 ;
  }



  /* Get the unnormalised shift again . */
  vec_diff_dbl ( gcOut, gcIn, mDim, gcDiff ) ;
  if ( nonStdTrans ) {
    sprintf ( hip_msg, "cannot automatically set the transformation for %s.\n"
              "        the vector between patches must be normal to x, y or z:"
              " %g, %g, %g", perLabel, gcDiff[0], gcDiff[1], gcDiff[2] ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }
  else {
    /* Set all other components to exact zero. */
    gcDiff[ (dir+1)%mDim ] = 0. ;
    if ( mDim == 3 )
      gcDiff[ (dir+2)%3 ] = 0. ; 
  }



  /* Initialise the bases. */
  vec_ini_dbl ( 0., MAX_DIM*MAX_DIM, perCoor ) ;
  if ( mDim == 2 ) {
    perCoor[2] = 1. ;
    vec_copy_dbl ( perCoor, 4, perCoor+4 ) ;
    vec_add_dbl ( perCoor+4, gcDiff, 2, perCoor+4 ) ;
    vec_add_dbl ( perCoor+6, gcDiff, 2, perCoor+6 ) ;
  }
  else {
    perCoor[3] = 1. ;
    perCoor[7] = 1. ;
    vec_copy_dbl ( perCoor, 9, perCoor+9 ) ;
    vec_add_dbl ( perCoor+9,  gcDiff, 3, perCoor+9 ) ;
    vec_add_dbl ( perCoor+12, gcDiff, 3, perCoor+12 ) ;
    vec_add_dbl ( perCoor+15, gcDiff, 3, perCoor+15 ) ;
  }

    
  if ( set_per_corners ( pUns, perLabel, perCoor, 0 ) && verbosity > 2 ) {
    sprintf ( hip_msg, " found translation by %g in %c to match  surface pair %s.", 
                        gcDiff[dir], cdir[dir], perLabel ) ;
    hip_err ( info, 1, hip_msg ) ;
    return ( 1 ) ;
  }
  else {
    sprintf ( hip_msg, "tried translation, but failed to match." ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************
  set_rot:   */

/*! set rotation angle for periodic pairs.
 *
 */

/*
  
  Last update:
  ------------
  4Apr19; bound scaled scProd -1 <= scProd <= 1.
  5Sep15; project the normal vectors into the plane normal to the rot axis.
  15Dec13; intro axiY, axiZ.
  long ago: conceived.
  

  Input:
  ------
  pUns     = grid
  perLabel = lable of the pair this rotation connects
  gcIn[]   = centre of gravity of inlet
  gcOut[]  = centre of gravity of outlet
  nrmIn[]  = normal to inlet face
  nrmOut[] = normal to outlet face
  mDim     = number of spatial dimensions
  kRot     = rotation direction with 0,1,2 being x,y,z.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int set_rot ( uns_s *pUns, 
              const char perLabel[], const double gcIn[], const double gcOut[], 
              double nrmIn[], double nrmOut[], const int mDim, 
              const int kRot ) {

  const char cdir[] = { 'x', 'y', 'z' } ;

  /* Rotation. Compute the angle. */
  switch ( kRot ) {
  case 0:
    pUns->specialTopo = axiX ;
    break ;
  case 1:
    pUns->specialTopo = axiY ;
    break ;
  case 2:
    pUns->specialTopo = axiZ ;
    break ;
  default:
    sprintf ( hip_msg, "in set_rot: unknown rotation direction %d", kRot ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Remove the axial component. */
  nrmIn[kRot] = nrmOut[kRot] = 0. ;
  /* Renormalise. */
  vec_norm_dbl ( nrmIn,  mDim ) ;
  vec_norm_dbl ( nrmOut, mDim ) ;
  
  double scProd = scal_prod_dbl ( nrmIn, nrmOut, mDim ) ;
  scProd = MAX(-1,scProd) ;
  scProd = MIN(1,scProd) ;
  double angRad = PI - acos ( scProd ) ;

  /* Positive or negative? */
  double crProd[MAX_DIM] ;
  cross_prod_dbl ( gcIn, gcOut, mDim, crProd ) ;
  if ( crProd[kRot] < 0. ) angRad = -angRad ;

  /* Should we correct a rounding error? All periodic cases ought to be integer
     divisions of the full circle. */
  int nTimes = rint( 2*PI/angRad ) ;
  if ( ABS( 2*PI/angRad - nTimes ) < 1.e-4 ) {
    angRad = 2*PI/nTimes ;
  }

  if ( set_per_rotation ( pUns, perLabel, cdir+kRot, angRad/PI*180. ) ) {
    if ( verbosity > 2 ) {
       sprintf ( hip_msg, "found rotation around %c by %g deg for surface pair %s.",
                 cdir[kRot], angRad/PI*180., perLabel ) ;
       hip_err ( info, 1, hip_msg ) ;
     }
    return ( 1 ) ;
  }
  else {
    sprintf ( hip_msg, "tried rotation around x by %g deg for pair %s,\n"
              " failed to match.", angRad/PI*180., perLabel ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 1 ) ;
}



int match_perBc ( uns_s *pUns, int nBcIn, int nBcOut, const char perLabel[] ) {

  int nPerBc ;
  double gcIn[MAX_DIM],  gcOut[MAX_DIM] ;
  double nrmIn[MAX_DIM],  nrmOut[MAX_DIM] ;
  double areaIn, areaOut ;
  int mFcIn, mFcOut ;
  double gcDiff[MAX_DIM] ; 
  double dist ;
  const int mDim = pUns->mDim ;
  int isRot ;
  int rotDir ;

  


  
  for ( nPerBc = 0 ; nPerBc < pUns->mPerBcPairs ; nPerBc++ )
    if ( pUns->pPerBc[nPerBc].pBc[0] == pUns->ppBc[nBcIn] )
      break ;
  
  if ( nPerBc < pUns->mPerBcPairs )
    /* This pair has a description. */
    return ( 1 ) ;    

  mFcIn  = bcPatch_nrm_gc ( pUns, nBcIn,  nrmIn,  gcIn,  &areaIn ) ;
  mFcOut = bcPatch_nrm_gc ( pUns, nBcOut, nrmOut, gcOut, &areaOut ) ;

  vec_norm_dbl ( nrmIn,  mDim ) ;
  vec_norm_dbl ( nrmOut, mDim ) ;

  

  if ( mFcIn != mFcOut ) {
    sprintf ( hip_msg, "different number of faces, %d vs. %d, on per pair %d/%d.",
             mFcIn, mFcOut, nBcIn+1, nBcOut+1 ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  /* Vector between the two gravity centers. */
  vec_diff_dbl ( gcOut, gcIn, mDim, gcDiff ) ;
  dist = vec_norm_dbl ( gcDiff, mDim ) ;

  /* Don't bother to treat the non-distinct case. However, thin slices for
     axisymmetric cases can undershoot when having only one row of cells.
  if ( dist < .1*Grids.epsOverlap ) {
    sprintf ( hip_msg, "unable to compute transformation for periodic pair labelled %s.\n"
             "        the patches are too close in match_perBc: %g", perLabel, dist ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  } */


  /* Compare the two patch normals. If they are aligned: translation. If not,
     use them to compute the rot-dir and the angle. */ 
  isRot = trans_or_rot ( nrmIn, nrmOut, mDim, &rotDir ) ;


  if ( isRot == -1 ) {
    /* Translation. */
    set_trans ( pUns, perLabel, gcIn, gcOut, mDim ) ;     
  }
  else if ( isRot == 1 ) {
    /* Rotation. */
    set_rot ( pUns, perLabel, gcIn, gcOut, nrmIn, nrmOut, mDim, rotDir ) ;
  }

  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  16Dec18: conceived.
  

  Input:
  ------
  pBcRef: one bc of a periodic pair
  mPerBc: number of periodic paris
  pPerBc: list of periodic pairs
  znIsL: -1 if non-periodic, 0 if U, 1 if L.
    
  Returns:
  --------
  the bc on the side of the pair matching the zone.
  
*/

bc_struct *match_perPair_lu ( bc_struct *pBcRef, const int mPerBc, perBc_s *pPerBc, const int znIsL ) {

  int k ;
  bc_struct *pBc = NULL ;
  for ( k = 0 ; k < mPerBc && !pBc ; k++ ) {
    if ( znIsL && pPerBc->pBc[0] ) {
      pBc = pPerBc->pBc[0] ;
    }
    else if ( znIsL==0 && pPerBc->pBc[1] ) {
      pBc = pPerBc->pBc[1] ;
    }
  }

  return ( pBc ) ;
}



/******************************************************************************
  add_pbt:   */

/*! Add a boundary number and tag to a list of periodic boundary pairs.
 *
 *  Input:
 *  ------
 *  perBcTag: allocated array with bc tag, and l+u bc numbers, 
 *            allocated to MAX_PER_PATCH_PAIRS
 *  pmBT: number of entries so far
 *  nBc: number of bc to add
 *  lu: is it l or u (char)
 *  perLabel: tag of the periodic bc.
 * 
 *  Changes To:
 *  -----------
 *  perBcTag: 
 *    
 *  Returns:
 *  --------
 *  the number of the new bc group in the perBcTag list.
 *
 */

/*
  
  Last update:
  ------------
  5Jul11; use -1 as empty tag.
  : conceived.
  

  
*/

int add_pbt ( perBcTag_s perBcTag[], int *pmBT, int nBc, char lu, char *perLabel ) {

  int n ;
  perBcTag_s *pBT = NULL ;

  for ( n=0; n< *pmBT ; n++ ) {
    pBT = perBcTag + n ;
    if ( !strcmp ( perLabel, pBT->perLabel ) ) {
      /* Matching tag. */

      if ( ( lu == 'u' && pBT->nBcU != -1 ) ||  ( lu == 'l' && pBT->nBcL != -1 ) ) {
        sprintf ( hip_msg, "duplicated periodic boundary with tag in add_pbt: %s", 
                  perLabel ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else
        break ;
    }
  }

  if ( n == *pmBT ) {
    /* No match found, add a record if possible. */

    if ( n == MAX_PER_PATCH_PAIRS ) {
        sprintf ( hip_msg, "too many periodic boundary pairs in add_pbt: %d", n ) ;
        hip_err ( fatal, 0, hip_msg ) ;
    }
    else {
      /* New record. */
      (*pmBT)++ ;
      pBT = perBcTag + n ;
      strncpy ( pBT->perLabel, perLabel, MAX_BC_CHAR ) ;
      pBT->nBcU = pBT->nBcL = -1 ;
    }
  }

  /* Add the bc number entry. */
  if ( lu == 'u') {
    pBT->nBcU = nBc ;
    return ( n ) ;
  }
  else if ( lu == 'l') {
    pBT->nBcL = nBc ;
    return ( n ) ;
  }
  

  return ( n ) ;
}


/******************************************************************************

  fix_per_setup:
  Modify periodic bnd declaration to allow non-360 periodic copy.
  
  Last update:
  ------------
  11May17; switch matchBc to geoType in bc_struct.
  19Dec10; derived from check_bnd_setup.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fix_per_setup ( uns_s *pUns ) {

  int nBc, nBcL=0, nBcU=0 ;
  bc_struct *pBc ;
  char perLabel[LINE_LEN], *pStr ;
  perBcTag_s perBcTag[MAX_PER_PATCH_PAIRS], *pBT, *pBT2 ;
  int mBT = 0 ;


  /* Make a list of all periodic bcs. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBc = pUns->ppBc[nBc] ;

    if ( pBc->geoType == match ) {
      sprintf( pBc->type, "%s", "n" ) ;
    }
    else {
      /* Check for proper setup of periodic pairs. */
      if ( pBc->type[0] == 'l' ) {
        strncpy ( perLabel, pBc->type+1, LINE_LEN-1 ) ;
        add_pbt ( perBcTag, &mBT, nBc, 'l', perLabel ) ; 
      }
      else if ( pBc->type[0] == 'u' ) {
        strncpy ( perLabel, pBc->type+1, LINE_LEN-1 ) ;
        add_pbt ( perBcTag, &mBT, nBc, 'u', perLabel ) ;
      }
      else if ( !strncmp( pBc->text, "hip_per_inlet", 13 ) ) {
        strncpy ( perLabel, pBc->text+14, LINE_LEN-1 ) ;
        add_pbt ( perBcTag, &mBT, nBc, 'l', perLabel ) ; 
      }
      else if ( !strncmp( pBc->text, "hip_per_outlet", 14 ) ) {
        strcpy ( perLabel, pBc->text+15 ) ;
        add_pbt ( perBcTag, &mBT, nBc, 'u', perLabel ) ;
      }
    }
  }



  /* Fix incomplete periodic information, e.g. due to copying. */
  for ( pBT = perBcTag ; pBT < perBcTag+mBT ; pBT++ ) {

    if ( !(pBT->nBcL && pBT->nBcU) ) {
      /* Missing partner. Treat the side with a copy in the bc text. */
      if ( strstr( pBT->perLabel, "_copy" ) ) {
        /* This is a copied bc, find the matching original. */
        /* Build the original label without _copy. */
        sprintf ( perLabel, "%s", pBT->perLabel ) ;
        pStr = strstr( perLabel, "_copy" ) ;
        *pStr = '\0' ;

        /* Find the matching counterpart in the list*/
        for ( pBT2 = perBcTag ; pBT2 < perBcTag+mBT ; pBT2++ ) {
          if ( !strcmp( perLabel, pBT2->perLabel ) ) {
            /* Match. Make sure the two pBTs complement each other. */
            if ( ( pBT2->nBcL && pBT->nBcL ) ||
                 ( pBT2->nBcU && pBT->nBcU ) ) {
              sprintf ( hip_msg, "non-complementary periodic patch info for per tags"
                        " %s and %s,\n             can't repair in fix_per_setup.",
                        pBT->perLabel, pBT2->perLabel ) ;
              hip_err ( fatal, 0, hip_msg ) ;
            }
            else if ( ( nBcL = pBT->nBcL ) ) {
              nBcU = pBT2->nBcU ;
            }
            else if ( ( nBcL = pBT2->nBcL ) ) {
              nBcU = pBT->nBcU ;
            }
          }

          /* Assign lu labels. Override hip_per specification with lu. */
          strcpy( pUns->ppBc[nBcL]->type, pBT2->perLabel ) ;
          strcpy( pUns->ppBc[nBcU]->type, pBT2->perLabel ) ;
        }
      }
    }
  }

  return (1) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  check_per_setup:
*/
/*! check for matching periodic pairs of bcs.
 *
 */

/*
  
  Last update:
  ------------
  7Apr20: extracted from check_bnd_setup.
  

  Input:
  ------
  pUns: grid to check.
    
  Returns:
  --------
  ret
  
*/

ret_s check_per_setup ( uns_s *pUns ) {
  ret_s ret = ret_success () ;


  int nBc ;
  bc_struct *pBc ;
  char perLabel[LINE_LEN] ;
  perBcTag_s perBcTag[MAX_PER_PATCH_PAIRS], *pBT ;
  int mBT = 0 ;


  /* Make a list of all periodic bcs. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBc = pUns->ppBc[nBc] ;

    set_bc_e ( pBc ) ;

    /* Check for proper setup of periodic pairs. */
    if ( pBc->type[0] == 'l' ) {
      strncpy ( perLabel, pBc->type+1, LINE_LEN-1 ) ;
      add_pbt ( perBcTag, &mBT, nBc, 'l', perLabel ) ; 
    }
    else if ( pBc->type[0] == 'u' ) {
      strncpy ( perLabel, pBc->type+1, LINE_LEN-1 ) ;
      add_pbt ( perBcTag, &mBT, nBc, 'u', perLabel ) ;
    }
    else if ( !strncmp( pBc->text, "hip_per_inlet", 13 ) ) {
      strncpy ( perLabel, pBc->text+14, LINE_LEN-1 ) ;
      add_pbt ( perBcTag, &mBT, nBc, 'l', perLabel ) ; 
    }
    else if ( !strncmp( pBc->text, "hip_per_outlet", 14 ) ) {
      strcpy ( perLabel, pBc->text+15 ) ;
      add_pbt ( perBcTag, &mBT, nBc, 'u', perLabel ) ;
    }
  }


  /* Make sure all pairs match. */
  for ( pBT = perBcTag ; pBT < perBcTag+mBT ; pBT++ ) {

    if ( pBT->nBcL != -1 && pBT->nBcU != -1 ) {
      /* All info complete, match this pair. */
      if ( !match_perBc ( pUns, pBT->nBcL, pBT->nBcU, pBT->perLabel ) ) {
        sprintf ( hip_msg, "failed to find a geometric match for periodic bc %s"
                  " in check_bnd_setup", pUns->ppBc[nBc]->text ) ;
        ret = hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    else {
      /* Incomplete information. */
      sprintf ( hip_msg, " incomplete periodic bc pair with label %s, l: %d, u: %d"
                " in check_bnd_setup", pBT->perLabel, pBT->nBcL, pBT->nBcU ) ;
      ret = hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  

  return ( ret ) ;
}


/******************************************************************************

  check_bnd_setup:
  Check for proper boundary setup. At the moment, make sure a periodic pairs
  are matching and have transformation info.
  
  Last update:
  ------------
  5Jul11; use -1 as empty tag, as 0 is a valid bcNr.
  19Dec10; rewrite, allow multiple lu with tags, 
           intro add_pbt.
  19Sep09; use strncpy.
  17May06; use set_bc_e, discard const qualifier from *pBc.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

ret_s check_bnd_setup ( uns_s *pUns ) {
  ret_s ret = ret_success () ;

  if ( pUns->specialTopo == surf )
    // nothing to be done.
    return ( ret ) ;
  
  ret = check_per_setup ( pUns ) ;
  if ( ret.status != success )
    return ( ret ) ;

  /* Pair all non-conformal interfaces, sliding planes, mixing planes, etc. */
  ret = pair_slidingPlaneSides ( pUns ) ;
  
  return ( ret ) ;
}
