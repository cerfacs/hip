/*
   uns_2tet.c:
   Convert a grid with primitives to tet only.

   Last update:
   ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
   6Apr13; fix bug with decimal rather than comma in init for h2t.
   3Apr06; debugging hex2tet. Change ot h2t[][][].

   This file contains:
   -------------------

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;


/* For simpler verification, use f77-style indices from 1-8. */
const int rtb[8][8] = {{ 1,2,3,4,5,6,7,8 }, 
                       { 2,1,5,6,3,4,8,7 }, 
                       { 3,2,6,7,4,1,5,8 }, 
                       { 4,1,2,3,8,5,6,7 }, 
                       { 5,1,4,8,6,2,3,7 }, 
                       { 6,2,1,5,7,3,4,8 }, 
                       { 7,3,2,6,8,4,1,5 }, 
                       { 8,4,3,7,5,1,2,6 } } ;

const int mTet[4] = { 5,6,6,6 } ;

/* h2t is given for numbering 1-8 and in the opposite sens
   (negative volume compared to the definition of the canonical element 
   in AVBP). */
const int h2t[4][6][4] = {
/* 0 Diag at 8:
{ {1, 1, 1, 1, 3,-1}, 
  {2, 3, 3, 6, 8,-1}, 
  {3, 8, 4, 8, 6,-1}, 
  {6, 6, 8, 5, 7,-1} }; */
                     { {1,2,3,6},  
                       {1,3,8,6},  
                       {1,3,4,8},  
                       {1,6,8,5},  
                       {3,8,6,7},  
                       {0,0,0,0} },
/* 1 Diag
{ {1, 1, 2, 1, 1, 2},  
  {6, 2, 7, 8, 8, 8},  
  {8, 8, 8, 3, 2, 7},  
  {5, 6, 6, 4, 3, 3} }; */
                     { {1,6,8,5},
                       {1,2,8,6},
                       {2,7,8,6},
                       {1,8,3,4},
                       {1,8,2,3},
                       {2,8,7,3} },                     
/* 2 Diag
 { {1, 1, 1, 1, 1, 1},  
   {5, 4, 8, 2, 4, 7},  
   {6, 8, 5, 3, 7, 6},  
   {7, 7, 7, 6, 3, 3} }; */
                     { {1,5,6,7},
                       {1,4,8,7},
                       {1,8,5,7},
                       {1,2,3,6},
                       {1,4,7,3},
                       {1,7,6,3} },
/* 3 Diag
{ {1, 1, 1, 1, 2, 2},  
  {3, 4, 8, 6, 6, 7},  
  {4, 8, 5, 7, 7, 3},  
  {7, 7, 7, 5, 1, 1} }; */
                      { {1,3,4,7},   
                        {1,4,8,7},   
                        {1,8,5,7},   
                        {1,6,7,5},   
                        {2,6,7,1},   
                        {2,7,3,1} }
} ;
                     

/******************************************************************************

  smallestElVx:
  Find the index of the lowest number vertex.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int smallestElVx ( elem_struct *pElem, int *pnMin ) {

  int k, kMin, mVx = elemType[ pElem->elType ].mVerts, nVx, nMin ;
  vrtx_struct **ppVrtx = pElem->PPvrtx ;

  kMin = 0 ;
  nMin = ppVrtx[0]->number ;
  for ( k = 1 ; k < mVx ; k++ ) {
    nVx = ppVrtx[k]->number ;

    if ( nVx < nMin ) {
      nMin = nVx ;
      kMin = k ;
    }
  }
    

  return ( kMin ) ;
}



/******************************************************************************

  rotHexCrnr:
  Rotate a hex for the smallest vertex to bottom left front.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int rotHexCrnr ( elem_struct *pHex, const int kMin, int nrVx[] ) {

  int k ;
  vrtx_struct *pVx[8] ;

  if ( pHex->elType != hex ) {
    printf ( " WARNING: this is not a hex in rotHex.\n" ) ;
    return ( 0 ) ; }

  for ( k = 0 ; k < 8 ; k++ )
    pVx[k] = pHex->PPvrtx[k] ;

  for ( k = 0 ; k < 8 ; k++ ) {
    pHex->PPvrtx[k] = pVx[ rtb[k][kMin]-1 ] ;
    nrVx[k] = pHex->PPvrtx[k]->number ;
  }


  return ( 1 ) ;
}
/******************************************************************************

  rot120:
  Axis rotation 0-6 (in C notation) with 120deg.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int rot120 ( elem_struct *pHex ) {

  vrtx_struct *pVx ;
  vrtx_struct ** const ppVx = pHex->PPvrtx ;

  pVx = ppVx[1] ;
  ppVx[1] = ppVx[4] ;
  ppVx[4] = ppVx[3] ;
  ppVx[3] = pVx ;  

  pVx = ppVx[5] ;
  ppVx[5] = ppVx[7] ;
  ppVx[7] = ppVx[2] ;
  ppVx[2] = pVx ;  

  return ( 1 ) ;
}

/******************************************************************************

  h2tet:
  Filter a tet out of a given hex.
  
  Last update:
  ------------
  5May20; ADAPT_RES should be ADAPT_HIERARCHICAL
  9Jul11; use ADAPT_RES
  3April06; reverse the sens of the element.
  : conceived.
  
  Input:
  ------
  pHex   = the hex elem to cut
  iTet[] = indices of the four nodes in the hex to form a new tet, f77 notation.
  pTet   = the tet to form
  ppVx   = the storage for the el->vx pointers for the tet.


  Changes To:
  -----------
  *pTet
  *ppVx
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int h2tet ( elem_struct *pHex, const int iTet[], 
            elem_struct *pTet, vrtx_struct **ppVx ) {

  pTet->term = 1 ;
  pTet->invalid = 0 ;
  pTet->number = pHex->number ;
  pTet->elType = tet ;

  pTet->PPvrtx = ppVx ;
    /* Note that the connectivity tables from Dompierre's paper are
       given in Fortran notation, indices range from 1-8 and in
       the opposite sens. */
  ppVx[0] = pHex->PPvrtx[ iTet[0]-1 ] ;
  ppVx[1] = pHex->PPvrtx[ iTet[2]-1 ] ;
  ppVx[2] = pHex->PPvrtx[ iTet[1]-1 ] ;
  ppVx[3] = pHex->PPvrtx[ iTet[3]-1 ] ;

#ifdef ADAPT_HIERARCHIC
  pTet->root = pTet->leaf = 1 ;
  pTet->Pparent = NULL ;
#endif

  return ( 1 ) ;
}



/******************************************************************************

  hex2tet:
  --------
  Implementation of Dompierre's paper on Hecht's algorithm. Cut one hex into
  a number of tets. Note that this hardwires the std AVBP element description.
  
  Last update:
  ------------
  Apr06; debugging.
  Feb06: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int hex2tets ( elem_struct *pHex, elem_struct **ppTet, vrtx_struct ***pppVrtx ) {

  int kMin, nMin, nrVx[8], fc[4], mD, kT ;

  /* Rotate the lowest numbered node into bottom left front. */
  kMin = smallestElVx( pHex, &nMin ) ;
  rotHexCrnr( pHex, kMin, nrVx ) ;

  /* Axis rotation around node 6 (7 if counted from one). */
  fc[1] = ( MIN( nrVx[1], nrVx[6] ) < MIN( nrVx[2], nrVx[5] ) ? 1 : 0 ) ;
  fc[2] = ( MIN( nrVx[3], nrVx[6] ) < MIN( nrVx[2], nrVx[7] ) ? 1 : 0 ) ;
  fc[3] = ( MIN( nrVx[4], nrVx[6] ) < MIN( nrVx[5], nrVx[7] ) ? 1 : 0 ) ;
  
  /* How many 120deg rotations? */
  if ( fc[2] != fc[3] ) {
    if ( fc[1] == fc[3] ) {
      rot120 ( pHex ) ;
      rot120 ( pHex ) ;
    }
    else
      rot120 ( pHex ) ;
  }

  /* How many diags at vx 6? */
  mD = fc[1] + fc[2] + fc[3] ;

  /* Make the tets. */
  for ( kT = 0 ; kT < mTet[mD] ; kT++ ) {
    h2tet ( pHex, h2t[mD][kT], *ppTet, *pppVrtx ) ;
    (*ppTet)++ ;
    *pppVrtx += 4 ;
  }


  return ( 1 ) ;
}
/******************************************************************************

  list_triBc:
  Make a list of boundary faces split into triangles along the 
  lowest numbered node. Hex only, for the time being.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  number of new bndFc on return.
  
*/

int list_triBc ( uns_s *pUns ) {

  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;

  bndFcVx_s *pBv ;
  int mFc, mVx, kVx, nVxFc[MAX_VX_FACE], kMin, nMin ;
  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  vrtx_struct * const *ppVx, * const pVrtx = pUns->ppChunk[0]->Pvrtx ;

  mFc = pUns->mBndFcVx = 2*pUns->mFaceAllBc ;
  pBv = pUns->pBndFcVx = arr_malloc ( "pUns->pBndFcVx in uns_2tet", pUns->pFam,
                                      mFc, sizeof( *pUns->pBndFcVx ) ) ;

    
  /* Record the forming vertices of all boundary faces. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->nFace ) {

        pFoE = elemType[ pBndFc->Pelem->elType ].faceOfElem + pBndFc->nFace ;
        mVx = pFoE->mVertsFace ;
        ppVx = pBndFc->Pelem->PPvrtx ;
        kVxFace = pFoE->kVxFace ;

        /* List all vx on the face, find the position of the lowest numbered. */
        kMin = 0 ;
        nMin = nVxFc[0] = ppVx[ kVxFace[0] ]->number ;
        for ( kVx = 1 ; kVx < mVx ; kVx++ ) {
          nVxFc[kVx] = ppVx[ kVxFace[kVx] ]->number ;
          if ( nVxFc[kVx] < nMin ) {
            nMin = nVxFc[kVx] ;
            kMin = kVx ;
          }
        }

        /* Fc 1: */
        pBv->pBc = pBndFc->Pbc ;
        pBv->mVx = 3 ;
        pBv->ppVx[0] = pVrtx + nVxFc[  kMin      ] ;
        pBv->ppVx[1] = pVrtx + nVxFc[ (kMin+1)%4 ] ;
        pBv->ppVx[2] = pVrtx + nVxFc[ (kMin+2)%4 ] ;
        pBv++ ;


        /* Fc 2: */
        pBv->pBc = pBndFc->Pbc ;
        pBv->mVx = 3 ;
        pBv->ppVx[0] = pVrtx + nVxFc[  kMin      ] ;
        pBv->ppVx[1] = pVrtx + nVxFc[ (kMin+2)%4 ] ;
        pBv->ppVx[2] = pVrtx + nVxFc[ (kMin+3)%4 ] ;
        pBv++ ;
      }

  return ( mFc ) ;
}



/******************************************************************************

  uns2tet:
  .
  
  Last update:
  ------------
  1Jul16; new interface to check_uns.
  4Apr13; modified interface to loop_elems
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int uns_2tet () {

  uns_s *pUns ;
  int mElems, mConn, mNewBndFc ;
  elem_struct *pTet, *pTets, *pEl, *pElBeg, *pElEnd ;
  vrtx_struct **ppVrtx, **ppVx ;
  chunk_struct *pChunk ;

  if ( Grids.PcurrentGrid->uns.type != uns ||
       Grids.PcurrentGrid->uns.pUns->mDim != 3 ) {
    printf ( " FATAL: grid to split to must be 3D unstructured.\n" ) ;
    return ( 0 ) ;
  }
  else 
    pUns = Grids.PcurrentGrid->uns.pUns ;

  /* Hexes only, for the time being. */
  if ( pUns->mElemsOfType[hex] - pUns->mElemsNumbered ) {
    printf ( " SORRY: uns_2tet is currently only implemented for pure hex meshes.\n") ;
    return ( 0 ) ; 
  }
  else if ( pUns->pRootChunk->PnxtChunk ) {
    printf ( " SORRY: uns_2tet is currently only implemented for single chunk meshes.\n") ;
    return ( 0 ) ; 
   }

  /* Make a vertex-based list of boundary faces, already split into
     triangles. */
  mNewBndFc = list_triBc ( pUns ) ;


  /* Alloc a new elem and ppVx spaces */
  mElems = 6*pUns->mElemsNumbered ;
  mConn = 4*mElems ;

  pTets = arr_malloc ( "pElems in uns_2tet", pUns->pFam,
                       mElems+1, sizeof( *pTets ) ) ;
  /* Elems count from 1. */
  pTet = pTets + 1 ;
  pTets[0].number = 0 ;
  ppVx = ppVrtx = arr_malloc ( "ppVrtx in uns_2tet", pUns->pFam,
                               mConn, sizeof( *ppVrtx ) ) ;



  /* Fill the new tets. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && !pEl->invalid )
        hex2tets( pEl, &pTet, &ppVx ) ;
 

  /* Dealloc hexas. */
  pChunk = pUns->ppChunk[0] ;
  arr_free ( pChunk->Pelem ) ;
  arr_free ( pChunk->PPvrtx ) ;

  pChunk->mElems = pTet - pTets - 1 ;
  pChunk->Pelem = pTets ;

  pChunk->mElem2VertP = ppVx - ppVrtx ;
  pChunk->PPvrtx = ppVrtx ;

  pChunk = pUns->ppChunk[0] ;
  pChunk->PbndFc = arr_realloc ( "PbndFc in append_chunk", pUns->pFam, pChunk->PbndFc,
                                 mNewBndFc+1, sizeof( *pChunk->PbndFc ) ) ;
  pChunk->mBndFaces = mNewBndFc ;

  /* Match the bnd Fc to the elements. */
  if ( !match_bndFcVx ( pUns ) )
    hip_err( fatal, 0, "could not match boundary faces in uns_2tet." ) ;


  /* Make a new grid. */
  number_uns_grid ( pUns ) ;
  check_uns ( pUns, check_lvl ) ;

  return ( 1 ) ;
}

