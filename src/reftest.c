/*
   _.c:


   Last update:
   ------------
   20Sep19; rename add_nd, add_xnd, add_fc to rt_ad_* to avoid duplication.


   This file contains:
   -------------------

*/

#include "cpre.h"
#include "proto.h"

#define ML 4
/* WD must be > 2^ML-1 */
#define WD (2*2*2*2+1) 
#define MF 100
#define MN 100

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )

typedef struct {
  /* forming nodes, refinement type and children. */
  int fc2nd[4] ;
  int rt ;
  int chld[4] ;
} fc_s ;


typedef struct {
  /* A list of faces. */
  int mFc ;
  fc_s fc[MF] ;
} faces_s ;

typedef struct {
  /* A square grid of nodes and edges: n = 0: empty, n>0: node, n<0: edge */
  int g[WD][WD] ;

  /* nodes with coordinates 0 <= i,j < ML */
  int mNd ;
  int nd[MN+1][2] ;

  /* A list of left faces with number, forming nodes, refinement type and children. */
  faces_s lFaces ;
  
  /* Same for the right. */
  faces_s rFaces ;
  
} lvl_s ;

/* level 0 ist the unrefined grid. */
lvl_s L[ML+1], *pl ;

int mLvl = ML, verb = 0, mComb = 0, err = 0 ;

/*******************************************************************************/


void plotg () {

  int i, j, n ;

  for ( j = WD-1 ; j > -1 ; j-- ) {
    for ( i = 0 ; i < WD ; i++ ) {
      n = pl->g[i][j] ;
      if ( n > 0 )
        printf ( "%3d", pl->g[i][j] ) ;
      else if ( n < 0 )
        printf ( " + " ) ;
      else
        printf ( "   " ) ;
    }
    printf ( "\n" ) ;
  }
  
  return ;
}


int fail ( int err, char *pMsg ) {

  printf ( " FATAL: %d: %s\n\n", err, pMsg ) ;
  if ( verb > 2 )
    plotg () ;

  if ( err < 2 )
    return ( 1 ) ;

  exit ( EXIT_FAILURE ) ;
}
  

int get_nd ( int nd0, int nd1 ) {

  int i, j ;

  if ( !nd0 || !nd1 )
    /* No such node. */
    return ( 0 ) ;
  
  i = ( pl->nd[nd0][0] + pl->nd[nd1][0] )/2 ;
  j = ( pl->nd[nd0][1] + pl->nd[nd1][1] )/2 ;

  if ( pl->g[i][j] > 0 )
    return ( pl->g[i][j] ) ;
  else
    return ( 0 ) ;
}

int rt_add_xnd ( int nd0, int nd1, int nd2, int nd3, int *pNew ) {
  /* Add a centernode to four surrounding mid-edge ones. Those must be in
     circular order. */

  int nx0, nx1 ;

  nx0 = rt_add_nd ( nd0, nd2, pNew ) ;
  nx1 = rt_add_nd ( nd1, nd3, pNew ) ;

  if ( nx0 != nx1 )
    fail ( 3, "cross node mismatch." ) ;

  return ( nx0 ) ;
}

void calc_mid ( int nd0, int nd1, int *pi, int *pj ) {

  *pi = ( pl->nd[nd0][0] + pl->nd[nd1][0] )/2 ;
  *pj = ( pl->nd[nd0][1] + pl->nd[nd1][1] )/2 ;

  return ;
}
 

int rt_add_nd ( int nd0, int nd1, int *pNew ) {
  /* Add a mid-edge node. */
  
  int i, j, mNd ;
  
  if ( pl->mNd > MN )
    fail ( 2, "too many nodes." ) ;

  calc_mid ( nd0, nd1, &i, &j ) ;


  if ( pl->g[i][j] < 1 ) {
    mNd = ++pl->mNd ;
    pl->g[i][j] = mNd ;
    pl->nd[mNd][0] = i ;
    pl->nd[mNd][1] = j ;
    (*pNew)++ ;
  }
  
  return ( pl->g[i][j] ) ;
}

int rt_add_fc ( faces_s *pFaces, int n0, int n1, int n2, int n3 ) {

  fc_s *pFc = pFaces->fc + pFaces->mFc ;
  
  if ( pFaces->mFc >= MF )
    fail ( 2, "too many faces." ) ;


  pFc->fc2nd[0] = n0 ;
  pFc->fc2nd[1] = n1 ;
  pFc->fc2nd[2] = n2 ;
  pFc->fc2nd[3] = n3 ;

  pFc->rt       = 0 ;

  pFc->chld [0] = 0 ;
  pFc->chld [1] = 0 ;
  pFc->chld [2] = 0 ;
  pFc->chld [3] = 0 ;

  pFaces->mFc++ ;

  return ( pFaces->mFc-1 ) ;
}

int match_ref ( fc_s *pFc,  int *pNew ) {

  unsigned int rt ;
  const int *n = pFc->fc2nd ;
  int n4, n5, n6, n7, n8, n04, n14, n15, n25, n26, n36, n37, n07, n48, n58, n68, n78 ;

  rt = -pFc->rt ;

  /* Mid edges. */
  n4 = get_nd( n[0], n[1] ) ;
  n5 = get_nd( n[1], n[2] ) ;
  n6 = get_nd( n[2], n[3] ) ;
  n7 = get_nd( n[3], n[0] ) ;

  /* Center. */
  n8 = get_nd( n4,    n6    ) ;

  /* Check for quarter edges. */
  n04 = get_nd( n[0], n4 ) ;
  n14 = get_nd( n[1], n4 ) ;
  n15 = get_nd( n[1], n5 ) ;
  n25 = get_nd( n[2], n5 ) ;
  n26 = get_nd( n[2], n6 ) ;
  n36 = get_nd( n[3], n6 ) ;
  n37 = get_nd( n[3], n7 ) ;
  n07 = get_nd( n[0], n7 ) ;

  n48 = get_nd( n4,   n8 ) ;
  n58 = get_nd( n5,   n8 ) ;
  n68 = get_nd( n6,   n8 ) ;
  n78 = get_nd( n7,   n8 ) ;


  
  /* Centerpoint rule on full face. */
  if ( n4 && n5 && n6 && n6 )
    n8 = ( n8 ? n8 : rt_add_nd( n4, n6, pNew ) ) ;
  
  /* Half face rule. */
  if ( n4 && n8 && n15 && n07 ) n48 = ( n48 ? n48 : rt_add_nd ( n4, n8, pNew ) ) ;
  if ( n5 && n8 && n26 && n14 ) n58 = ( n58 ? n58 : rt_add_nd ( n5, n8, pNew ) ) ;
  if ( n6 && n8 && n37 && n25 ) n68 = ( n68 ? n68 : rt_add_nd ( n6, n8, pNew ) ) ;
  if ( n7 && n8 && n36 && n04 ) n78 = ( n78 ? n78 : rt_add_nd ( n7, n8, pNew ) ) ;
  
  
  
  if ( pFc->rt <= 0 ) {
    /* This face is to be refined. */
    rt = -pFc->rt ;

    /* Level rule. */
    if ( n04 || n14 || n26 || n36 )
      /* Need the split b-t. */
      rt |= 2 ;
  
    if ( n15 || n25 || n37 || n07 )
      /* Need the split r-l. */
      rt |= 1 ;

    pFc->rt = -rt ;

  
    if ( rt == 1 ) {
      /* Split r-l, ie. between 1-2 and 3-0. */
      n5 = ( n5 ? n5 : rt_add_nd( n[1], n[2], pNew ) ) ;
      n7 = ( n7 ? n7 : rt_add_nd( n[3], n[0], pNew ) ) ;
    }                                        
                                           
    else if ( rt == 2 ) {                         
      /* Split b-t, ie. between 0-1 and 2-3. */
      n4 = ( n4 ? n4 : rt_add_nd( n[0], n[1], pNew ) ) ;
      n6 = ( n6 ? n6 : rt_add_nd( n[2], n[3], pNew )  );
    }                                        
                                           
    else if ( rt == 3 ) {                         
      /* Split all. */                       
      n4 = ( n4 ? n4 : rt_add_nd( n[0], n[1], pNew ) ) ;
      n5 = ( n5 ? n5 : rt_add_nd( n[1], n[2], pNew ) ) ;
      n6 = ( n6 ? n6 : rt_add_nd( n[2], n[3], pNew ) ) ;
      n7 = ( n7 ? n7 : rt_add_nd( n[3], n[0], pNew ) ) ;
    
      n8 = ( n8 ? n8 : rt_add_xnd( n4, n5, n6, n7, pNew ) ) ;
    }
  }
  
  return ( 1 ) ;
}

int split_fc ( int nFc, faces_s *pFaces ) {

  fc_s *pFc = pFaces->fc + nFc ;
  const int *n = pFc->fc2nd ;
  int n4, n5, n6, n7, n8 ;

  if ( pFc->rt >= 0 ) {
    /* Adaptation done or no adaptation to be done. */
    return ( 1 ) ;
  }
  
  pFc->rt = -pFc->rt ; 

  
  if ( pFc->rt == 1 ) {
    /* Split r-l, ie. between 1-2 and 3-0. */
    n4 = get_nd( n[1], n[2] ) ;
    n5 = get_nd( n[3], n[0] ) ;

    pFc->chld[0] = rt_add_fc ( pFaces, n[0], n[1], n4, n5 ) ;
    pFc->chld[1] = rt_add_fc ( pFaces, n5, n4, n[2], n[3] ) ;
  }

  else if ( pFc->rt == 2 ) {
    /* Split b-t, ie. between 0-1 and 2-3. */
    n4 = get_nd( n[0], n[1] ) ;
    n5 = get_nd( n[2], n[3] ) ;

    pFc->chld[0] = rt_add_fc ( pFaces, n[0], n4, n5, n[3] ) ;
    pFc->chld[1] = rt_add_fc ( pFaces, n4, n[1], n[2], n5 ) ;
  }

  else if ( pFc->rt == 3 ) {
    /* Split all. */
    n4 = get_nd( n[0], n[1] ) ;
    n5 = get_nd( n[1], n[2] ) ;
    n6 = get_nd( n[2], n[3] ) ;
    n7 = get_nd( n[3], n[0] ) ;
    n8 = get_nd( n4,    n6    ) ;

    pFc->chld[0] = rt_add_fc ( pFaces, n[0], n4, n8, n7 ) ;
    pFc->chld[1] = rt_add_fc ( pFaces, n4, n[1], n5, n8 ) ;
    pFc->chld[2] = rt_add_fc ( pFaces, n8, n5, n[2], n6 ) ;
    pFc->chld[3] = rt_add_fc ( pFaces, n7, n8, n6, n[3] ) ;
  }

  else
    fail ( 3, "no such split: %d." ) ;


  return ( 1 ) ;
}




void ini2fc ( ) {

  int i, j ;
  
  /* ini */
  for ( j=0 ; j < WD ; j++ )
    for ( i=0 ; i < WD ; i++ )
      pl->g[i][j] = 0 ;

  /* 4 ini corner nodes, counter clockwise from bottom left. */
  pl->mNd = 4 ;

  pl->nd[1][0]      = 0 ;
  pl->nd[1][1]      = 0 ;
  pl->g[0][0]       = 1 ;
  
  pl->nd[2][0]      = WD-1 ;
  pl->nd[2][1]      = 0 ;
  pl->g[WD-1][0]    = 2 ;

  pl->nd[3][0]      = WD-1 ;
  pl->nd[3][1]      = WD-1 ;
  pl->g[WD-1][WD-1] = 3 ;

  pl->nd[4][0]      = 0 ;
  pl->nd[4][1]      = WD-1 ;
  pl->g[0][WD-1]    = 4 ;

  /* 2 matching faces. */
  pl->lFaces.mFc = 0 ;
  rt_add_fc( &pl->lFaces, 1, 2, 3, 4 ) ;

  pl->rFaces.mFc = 0 ;
  rt_add_fc( &pl->rFaces, 1, 2, 3, 4 ) ;


  return ;
}

void ini_from_lvl ( const lvl_s *pFrom, const int l[MF+1], const int r[MF+1] ) {

  const fc_s *pFcF ;
  fc_s *pFc ;
  int i, j, mFc ;

  /* ini */
  for ( j=0 ; j < WD ; j++ )
    for ( i=0 ; i < WD ; i++ )
      pl->g[i][j] = pFrom->g[i][j] ;


  pl->mNd = pFrom->mNd ;
  for ( i=1 ; i <= pl->mNd ; i++ ) {
    pl->nd[i][0] = pFrom->nd[i][0] ;
    pl->nd[i][1] = pFrom->nd[i][1] ;
  }

  mFc = pl->lFaces.mFc = pFrom->lFaces.mFc ;
  for ( pFc = pl->lFaces.fc, pFcF = pFrom->lFaces.fc, i = 0 ;
        i < mFc ; pFc++, pFcF++, i++ ) {
    
    pFc->fc2nd[0] = pFcF->fc2nd[0] ;
    pFc->fc2nd[1] = pFcF->fc2nd[1] ;
    pFc->fc2nd[2] = pFcF->fc2nd[2] ;
    pFc->fc2nd[3] = pFcF->fc2nd[3] ;

    pFc->rt       = ( pFcF->rt > 0 ? pFcF->rt : -l[i] ) ;

    pFc->chld[0]  = pFcF->chld[0] ;
    pFc->chld[1]  = pFcF->chld[1] ;
    pFc->chld[2]  = pFcF->chld[2] ;
    pFc->chld[3]  = pFcF->chld[3] ;
  }

  
  mFc = pl->rFaces.mFc = pFrom->rFaces.mFc ;
  for ( pFc = pl->rFaces.fc, pFcF = pFrom->rFaces.fc, i = 0 ;
        i < mFc ; pFc++, pFcF++, i++ ) {
    
    pFc->fc2nd[0] = pFcF->fc2nd[0] ;
    pFc->fc2nd[1] = pFcF->fc2nd[1] ;
    pFc->fc2nd[2] = pFcF->fc2nd[2] ;
    pFc->fc2nd[3] = pFcF->fc2nd[3] ;

    pFc->rt       = ( pFcF->rt > 0 ? pFcF->rt : -r[i] ) ;

    pFc->chld[0]  = pFcF->chld[0] ;
    pFc->chld[1]  = pFcF->chld[1] ;
    pFc->chld[2]  = pFcF->chld[2] ;
    pFc->chld[3]  = pFcF->chld[3] ;
  }


  return ;
}


void reset_g () {

  int i,j ;

  for ( j=0 ; j < WD ; j++ )
    for ( i=0 ; i < WD ; i++ )
      if ( pl->g[i][j] < 0 )
        pl->g[i][j] = 0 ;

  return ;
}


void draw_1edge ( int n0, int n1 ) {

  int i, i0, i1, j, j0, j1 ;

  if ( ( i = pl->nd[n0][0] ) == pl->nd[n1][0] ) {
    /* const i */
    j0 = pl->nd[n0][1] ;
    j1 = pl->nd[n1][1] ;
    if ( j0 > j1 ) { j = j0, j0 = j1, j1 = j ; }

    for ( j = j0+1 ; j < j1 ; j++ ) {
      if ( pl->g[i][j] > 0 )
        fail ( 3, "node on half edge." ) ;
      else
        pl->g[i][j] = -1 ;
    }
  }
  
  else if  ( ( j = pl->nd[n0][1] ) == pl->nd[n1][1] ) {
    /* const i */
    i0 = pl->nd[n0][0] ;
    i1 = pl->nd[n1][0] ;
    if ( i0 > i1 ) { i = i0, i0 = i1, i1 = i ; }

    for ( i = i0+1 ; i < i1 ; i++ ) {
      if ( pl->g[i][j] > 0 )
        fail ( 3, "node on half edge." ) ;
      else
        pl->g[i][j] = -1 ;
    }
  }
  else
    fail ( 3, "diagonal edge." ) ;

  
  return ;
}

void draw_eg ( const faces_s *pFaces ) {

  const fc_s *pFc ;
  const int *n ;
  int nfc, n4, n5, n6, n7 ;
  
  reset_g () ;
  
  /* Inscribe all the edges of the terminal faces of one half onto the grid.
     Note: an edge may be split by a hanging node. */
  for ( pFc = pFaces->fc ; pFc < pFaces->fc + pFaces->mFc ; pFc++ )
    if ( pFc->rt == 0 ) {
      n = pFc->fc2nd ;

      n4 = get_nd( n[0], n[1] ) ;
      n5 = get_nd( n[1], n[2] ) ;
      n6 = get_nd( n[2], n[3] ) ;
      n7 = get_nd( n[3], n[0] ) ;


      if ( !n4 )
        draw_1edge ( n[0], n[1] ) ;
      else {
        draw_1edge ( n[0], n4 ) ;
        draw_1edge ( n4,    n[1] ) ;
      }
    
      if ( !n5 )
        draw_1edge ( n[1], n[2] ) ;
      else {
        draw_1edge ( n[1], n5 ) ;
        draw_1edge ( n5,    n[2] ) ;
      }

      if ( !n6 )
        draw_1edge ( n[2], n[3] ) ;
      else {
        draw_1edge ( n[2], n6 ) ;
        draw_1edge ( n6,    n[3] ) ;
      }

      if ( !n7 )
        draw_1edge ( n[3], n[0] ) ;
      else {
        draw_1edge ( n[3], n7 ) ;
        draw_1edge ( n7,    n[0] ) ;
      }
    }

  return ;
}

int is_eg ( int n0, int n1 ) {

  int i, j, i0, j0, i1, j1 ;

  i0 = pl->nd[n0][0] ;
  j0 = pl->nd[n0][1] ;
  i1 = pl->nd[n1][0] ;
  j1 = pl->nd[n1][1] ;

  /* Order. */
  if ( i0 > i1 ) { i = i0, i0 = i1, i1 = i ; }
  if ( j0 > j1 ) { j = j0, j0 = j1, j1 = j ; }

  
  if ( i0 == i1 ) {
    /* const i */
    for ( j = j0+1 ; j < j1 ; j++ )
      if ( pl->g[i][j] )
        return ( 1 ) ;
  }
  
  else if ( j0 == j1 ) {
    /* const i */
    for ( i = i0+1 ; i < i1 ; i++ )
      if ( pl->g[i][j] )
        return ( 1 ) ;
  }

  else {
    /* a rectangle. */
    for ( i = i0+1 ; i < i1 ; i++ )
      for ( j = j0+1 ; j < j1 ; j++ )
        if ( pl->g[i][j] )
          return ( 1 ) ;
  }

  
  return ( 0 ) ;
}


int check_fc ( const faces_s *pFaces ) {

  const fc_s *pFc ;
  const int *n ;
  int nfc, n4, n5, n6, n7, n8, mMid ;
  
  for ( pFc = pFaces->fc ; pFc < pFaces->fc + pFaces->mFc ; pFc++ ) {
    if ( pFc->rt == 0 ) {
      n = pFc->fc2nd ;
      
      n4 = get_nd( n[0], n[1] ) ;
      n5 = get_nd( n[1], n[2] ) ;
      n6 = get_nd( n[2], n[3] ) ;
      n7 = get_nd( n[3], n[0] ) ;

      if ( n4 && n6 )
        n8 = get_nd( n4, n6 ) ;
      else if ( n5 && n7 )
        n8 = get_nd( n5, n7 ) ;
      else
        n8 = 0 ;

      if ( !n8 ) {
        /* No center vx. */

        if ( n4 && n5 && n6 && n7 ) {
          fail ( 3, "fully refined face w/o center vx." ) ;
        }
        else if ( n4 && n6 ) {
          if ( is_eg ( n[0], n6 ) ||
               is_eg ( n4, n[2] ) )
            /* There may be no elemental edge n5-n7. */
            err += fail ( 1, "there should not have been a horiz. edge." ) ;
        }
        else if ( n5 && n7 ) {
          if ( is_eg ( n[0], n5 ) ||
               is_eg ( n7, n[2] ) )
            /* There may be no elemental edge n4-n6. */
            err += fail ( 1, "there should not have been a vertic. edge." ) ;
        }
        else {
          if ( is_eg ( n[0], n[2] ) )
            err += fail ( 1, "there should not have been any edge." ) ;
        }
      }

      else {
        /* There must be 3 or 4 midvx. */
        mMid = 0 ;
        if ( n4 ) mMid++ ;
        if ( n5 ) mMid++ ;
        if ( n6 ) mMid++ ;
        if ( n7 ) mMid++ ;

        
        if ( mMid < 3 )
          fail ( 3, " only 2 midvx with a center vx." ) ;

        else if ( mMid == 4 ) {
          if ( is_eg ( n[0], n8 ) || 
               is_eg ( n[1], n8 ) || 
               is_eg ( n[2], n8 ) || 
               is_eg ( n[3], n8 ) )
            err += fail ( 1, "there should not have been a quarter edge." ) ;
        }
        
        else {
          /* Find the unrefined edge. */
          if ( !n4 ) {
            if ( is_eg ( n[0], n5 ) ||
                 is_eg ( n[2], n8 ) || 
                 is_eg ( n[3], n8 ) )
              err += fail ( 1, "there should not have been a center edge." ) ;
          }
          else if ( !n5 ) {
            if ( is_eg ( n[1], n6 ) ||
                 is_eg ( n[3], n8 ) || 
                 is_eg ( n[0], n8 ) )
            err += fail ( 1, "there should not have been a center edge." ) ;
          }
          else if ( !n6 ) {
            if ( is_eg ( n[2], n7 ) ||
                 is_eg ( n[0], n8 ) || 
                 is_eg ( n[1], n8 ) )
              err += fail ( 1, "there should not have been a center edge." ) ;
          }
          else {
            if ( is_eg ( n[3], n4 ) ||
                 is_eg ( n[1], n8 ) || 
                 is_eg ( n[2], n8 ) )
              err += fail ( 1, "there should not have been a center edge." ) ;
          }
        }
      }
    }

    if ( err )
      break ;
  }
  
  return ( err ) ;
}

void zero ( int m, int i[] ) {

  int n ;
  
  for ( n = 0 ; n < m ; n++ )
    i[n] = 0 ;
  
  return ;
}

void inc3( int *i3, int m ) {
  /* increment a number in a quartary system, 0-3. */
  
  int n, d ;

  i3[0]++ ;
  
  for ( n = 0 ; n < m ; n++ )
    if ( i3[n] > 3 ) {
      /* carry */
      i3[n] = 0 ;
      i3[n+1]++ ;
    }
    
  return ;
}

int gt ( int ml, const int l[], int mr, const int r[] ) {
  /* is l lexicographically larger than r? */

  int i, m = MAX(ml,mr) ;
  
  for ( i = 0 ; i < m ; i++ )
    if ( r[i] > l[i] )
      return ( 0 ) ;
  
  return ( 1 ) ;
}

void print_lr ( int nLvl, int mlFc, int l[], int mrFc, int r[] ) {

  int i ;

  printf ( " Failed lvl %d: l:", nLvl ) ;
  for ( i = 0 ; i < mlFc ; i ++ )
    printf ( " %d", l[i] ) ;
  printf ( ";  r:" ) ;
  for ( i = 0 ; i < mrFc ; i ++ )
    printf ( " %d", r[i] ) ;
  printf ( "\n" ) ;
  
  return ;
}

int ref_lvl ( int mlFc, int l[], int mrFc, int r[] ) {

  int newNd, newNdSweep, nFc, mFc ;
  fc_s *pFc ;

  pl++ ;
  /* Initialise from the previous level. */
  ini_from_lvl ( pl-1, l, r ) ;

        
  for ( newNd = 0, newNdSweep = 1 ; newNdSweep ; ) {
    newNdSweep = 0 ;
            
    for ( pFc = pl->lFaces.fc ; pFc < pl->lFaces.fc + pl->lFaces.mFc ; pFc++ )
      match_ref ( pFc, &newNdSweep ) ;

    for ( pFc = pl->rFaces.fc ; pFc < pl->rFaces.fc + pl->rFaces.mFc ; pFc++ )
      match_ref ( pFc, &newNdSweep ) ;

    newNd += newNdSweep ;
  }

  if ( newNd ) {
    mComb++ ;
          
    /* Only scan the old faces. */
    mFc = pl->lFaces.mFc ;
    for ( nFc = 0 ; nFc < mFc ; nFc++ )
      split_fc ( nFc, &pl->lFaces ) ;
          
    mFc = pl->rFaces.mFc ;
    for ( nFc = 0 ; nFc < mFc ; nFc++ )
      split_fc ( nFc, &pl->rFaces ) ;
  
  

    draw_eg  ( &pl->lFaces ) ;
    check_fc ( &pl->rFaces ) ;
    if ( err ) return ;
          
    draw_eg  ( &pl->rFaces ) ;
    err += check_fc ( &pl->lFaces ) ;
    if ( err ) return ;

    if ( verb > 2 )
      plotg () ;
  }

  return ;
}

int ref_loop ( ) {

  const int mlFc = pl->lFaces.mFc, mrFc = pl->rFaces.mFc ;
  int l[MF+1], r[MF+1] ;


  for ( zero( MF+1, l ), l[0] = 1 ; !l[mlFc] ; inc3( l, mlFc ) )
    for ( zero( MF+1, r ) ; !r[mrFc] ; inc3( r, mrFc ) )
      if ( gt( mlFc, l, mrFc, r ) ) {

        ref_lvl ( mlFc, l, mrFc, r ) ;

        if ( err ) {
          pl-- ;
          print_lr ( pl-L, mlFc, l, mrFc, r ) ;
          return ( 0 ) ;
        }
        
        else if ( !err && pl < L+mLvl ) {
          ref_loop () ;
        }
        pl-- ;
      }
  
  return ( err ) ;
}





int main ( int argc, char *argv[] ) {

  int l[2], r[2], ll[4], rr[4] ;

  if ( argc > 1 )
    mLvl = atoi ( argv[1] ) ;
  if ( argc > 2 )
    verb = atoi ( argv[2] ) ;

  /* No refinement to start with. */
  pl = L ;
  reset_g () ;
  ini2fc () ;

  /* Cross ref on 1st lvl. */
  l[0] = 2 ;
  r[0] = 1 ;
  ref_lvl ( 1, l, 1, r ) ;


  /* Only work on the first child on 2nd. level. */
  if ( mLvl > 1 ) {

    ll[0] = 0, ll[1] = 1, ll[2] = 0, ll[3] = 0 ;
    for (  ; !ll[2] ; inc3( ll, 2 ) ) {
      rr[0] = 0, rr[1] = 1, rr[2] = 0, rr[3] = 0 ;
      for ( ; !rr[2] ; inc3( rr, 2 ) )
        if ( gt( 3, ll, 3, rr ) ) {
          
          ref_lvl ( 3, ll, 3, rr ) ;
          
          if ( err ) {
            pl-- ;
            print_lr ( pl-L, 3, ll, 3, rr ) ;
            return ( 0 ) ;
          }
          
          else if ( !err && pl < L+mLvl ) {
            ref_loop () ;
          }
          pl-- ;
        }
    }
  }
  
  if ( err )
    print_lr ( 0, 1, l, 1, r ) ;
  else
    printf ( " Tested %d levels, %d combinations, all match.\n", mLvl, mComb ) ;

  return ( 1 ) ;
}
