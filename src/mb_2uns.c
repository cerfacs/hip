/*
   mb_2uns.c:
   Create an unstructured grid from a structured multiblock grid. The functions in this
   file have in common the temporary list of block to chunk and chunk to block pointers
   blk2chk and chk2blk.

   Last update:
   ------------
   18Dec10; new pBc->type
   14Sep10; rename cp_mb2uns, reorder to avoid protopying.
   10Jul98; move CHECK_MATCH from get_mbMatchFc to mb_degen_subFc.
   24Apr98; move typdef of blk2chk here.
   15Nov96; split off mb_fc.c and mb_ele_vert.c.
   5May96: intro bndPatch in get_mbBndFc.
   25Apr96: cleanup.
   15Apr96; conceived.

   Contains:
   ---------
   cp_mb2uns:
   mb_2uns:
   add_mb2uns:
   get_mbMatchFc:
   
*/

#include "cpre.h"
#include "cpre_mb.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_mb_uns.h"
#include "proto_mb.h"
#include "proto_uns.h"

/* mb_2uns.c: */
typedef struct _blk2chk_struct blk2chk_struct ;
typedef struct _blk2chk_struct chk2blk_struct ;  /* Just the same. */

/* Relation between blocks and chunks, only known to mb_2uns. */
struct _blk2chk_struct {
  block_struct *Pblock ;
  chunk_struct *Pchunk ;
} ;

extern Grids_struct Grids ;
extern const int verbosity ;
extern char hip_msg[] ;

extern const int find_mbDegenFaces ;

int get_mbMatchFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk,
			   blk2chk_struct *blk2chk );


/*************************************************************************

 add_mb2uns:
 Creator for an unstructured mesh from a structured multiblock mesh.
 Add one block. 

 Last update:
 ------------
 12Mar18; intro doMap.

 Input:
 ------
 PBL:         block
 mDim:        dimensions,
 mUnknown:    number of unknowns
 doMap:       if non-zero, allocate a field for a map of mb vx to uns vx.
 
 Changes to:
 -----------
 Pchunk:     
 blk2chk: list of chunk for each block, 
 chk2blk:         block          chunk.

 Returns:
 --------
 0:          on failure,
 1:          on success.

*/

int add_mb2uns ( block_struct *PBL, const int mDim, const int mUnknown,
                 uns_s *pUns, chunk_struct **PPchunk, 
                 blk2chk_struct blk2chk[], chk2blk_struct chk2blk[],
                 const int doMap ) {
  
  chunk_struct *Pchunk, *PnewChunk ;
  vrtx_struct base, * const Pbase = &base ;

  Pchunk = *PPchunk ;
  
  if ( !Pchunk ) {
    /* There is no root chunk, make one. */
    Pchunk = make_chunk ( pUns ) ;
    Pchunk->nr = 1 ;
    Pchunk->pUns = pUns ;
  }
  else {
    /* Find the last chunk and check whether it contains anything.
       If there are neither vertices nor elements, it is considered
       empty. */
    while ( Pchunk->PnxtChunk )
      Pchunk = Pchunk->PnxtChunk ;

    if ( PBL->mElemsMarked && ( Pchunk->Pvrtx || Pchunk->Pelem ) ) {
      /* There are more elements in this chunk, but this chunk is full,
	 so make a new one. */
      PnewChunk = make_chunk ( pUns ) ;
      PnewChunk->pUns = pUns ;
      PnewChunk->PprvChunk = Pchunk ;
      PnewChunk->nr = Pchunk->nr + 1 ;
      Pchunk->PnxtChunk = PnewChunk ;
      Pchunk = PnewChunk ;
    }
  }

  /* Relate chunks to blocks. */
  blk2chk[PBL->nr].Pblock = chk2blk[Pchunk->nr].Pblock = PBL ;
  blk2chk[PBL->nr].Pchunk = chk2blk[Pchunk->nr].Pchunk = Pchunk ;

  double hMinBlSq ;
  if ( PBL->mElemsMarked ) {
    /* There are cells within the cut of this block. */
    strncpy ( Pchunk->name, PBL->name, TEXT_LEN-1 ) ; 

    /* Swap the coordinates if all volumes are negative.
    transpose_block ( PBL, mDim ) ; */
    
    /* Get the cells of the block. */
    if ( get_mbElems ( PBL, mDim, Pbase, Pchunk ) )

      /* Get the cell to vertex pointers. */
      if ( get_mbVerts ( PBL, mDim, mUnknown, Pbase, Pchunk, doMap ) )
	
	/* Get external boundary faces. */
	if ( get_mbBndFc ( PBL, mDim, Pchunk ) )
    
	  /* Get internal boundary faces. */
	  if ( get_mbIntFc ( PBL, mDim, Pchunk ) )

	    /* Get degenerate faces. */
	    if ( get_mbDegenFc ( PBL, mDim, Pchunk ) ) {

              /* Valid mb grid. Get hMin of non-degen edges. */
              hMinBlSq = get_block_hMinSq ( PBL, mDim ) ;
              pUns->epsOverlapSq = MIN( pUns->epsOverlapSq, .9*.9*hMinBlSq ) ;
              pUns->epsOverlap = sqrt ( pUns->epsOverlapSq ) ;
	      *PPchunk = Pchunk ;
	      return ( 1 ) ;
            }

    
    /* Some failure. */
    hip_err  ( fatal, 0, "failure in add_mb2uns." ) ;
    *PPchunk = NULL ;
    return ( 0 ) ;
  }
  
  return ( 1 ) ;
}
  
  
/**********************************************************************

  get_mbMatchFc:
  get matching interfaces between blocks. These faces are between
  the blocks where cells on both sides are in the cut. The matching
  faces are only created for the lower numbered block of the interface.

  Last update:
  ------------
  21Jul18; adapt matchFc_s to use 0,1 numbering rather than 1,2.
  10Jul98; move CHECK_MATCH to mb_degen_subFc for fix before copying to uns.
  26Apr96: derived from get_mbIntFc.
  
  Input:
  ------
  PBL:        structured grid block,
  mDim:       dimensions,
  Pchunk:     current unstructured chunk,
  
  Changes to:
  -----------
  Pchunk:

  Returns:
  --------
  0: on failure,
  1: on success.

*/

int get_mbMatchFc ( block_struct *PBL, const int mDim, chunk_struct *Pchunk,
			   blk2chk_struct blk2chk[] )
{
  int mMatchFaces, nSubFace, *elemMrk ;
  subFace_struct *PSF ;
  int ll[MAX_DIM], ur[MAX_DIM], ijk[MAX_DIM], ijkO[MAX_DIM], nCellO ;
  int index1, multVert1, multCell1, index2, multVert2, multCell2, nCell, nDim,
      offsetVert, offsetCell, indexStatic, dll, dlr, dur, dul, n1, n2 ;
  matchFc_struct *PmatchFc ;
  block_struct *PBLO ;
  
  /* Aliases for the block quantities. */
  elemMrk = PBL->PelemMark ;
  
  /* Count all matching faces between blocks. Note that there
     is such an interface only if there are marked cells on one side of
     the interface. */
  mMatchFaces = 0 ;
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace ) {
    PSF = PBL->PPsubFaces[nSubFace] ;
    if ( PSF->PrBlock ) {
      /* This subface is internal. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      if ( PSF->PlBlock == PBL )
	/* The other block is on the right. */
	PBLO = PSF->PrBlock ;
      else
	/* The other block is on the left. */
	PBLO = PSF->PlBlock ;

      /* Treat each surface only once. */
      if ( PBL->nr <= PBLO->nr ) {
	/* Find the proper static bound. */
	if ( ll[indexStatic] == 1 )
	  /* -ijk face. */
	  ijk[indexStatic] = ll[indexStatic] ;
	else
	  /* +ijk face. */
	  ijk[indexStatic] = ll[indexStatic]-1 ;
	  
	/* Loop over the subface. */
	n2 = ll[index2] ;
	n1 = ll[index1] - 1 ; 
	while ( ( nCell = cell_loop_subfc ( ll, ur, mDim,
			  		    &n1, index1, multCell1,
                                            &n2, index2, multCell2,
                                            offsetCell ) ) )
	  if ( elemMrk[nCell] ) {
	    /* This cell is marked. Find out about the status of
	       the cell on the other side of the interface. For that,
	       calculate the indices nI,nJ,nK of the llf node of the cell,
	       translate them to the other block and calculate
	       the linear cell number on the other side. */
	    ijk[index1] = n1 ;
	    ijk[index2] = n2 ;
	    
	    if ( PSF->PlBlock == PBL )
	      /* The other block is on the right. */
	      trans_l2r ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	    else
	      /* The other block is on the left. */
	      trans_r2l ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	    nCellO = get_nElem_ijk ( mDim, ijkO, PBLO->mVert ) ;
	    
	    if ( PBLO->PelemMark[nCellO] )
	      /* Both cells are marked. This is a matching interface. */
	      mMatchFaces++ ;
	  }
      }
    }
  }

  /* Alloc for matching interfaces. */
  if ( mMatchFaces ) {
    Pchunk->PmatchFc = arr_malloc ( "Pchunk->PmatchFc in get_mbMatchFc", Pchunk->pUns->pFam,
                                    mMatchFaces+1, sizeof( matchFc_struct ) ) ;
    if ( !Pchunk->PmatchFc )
      hip_err ( fatal,0, "could not allocate space for MatchFc" ) ;
  }
  else {
    /* No matching faces found. */
    Pchunk->mMatchFaces = mMatchFaces ;
    return ( 1 ) ; }

  /* List the interior interfaces into PmatchFc, starting for aesthetics
     with PmatchFc[1]. */
  PmatchFc = Pchunk->PmatchFc ;
  for ( nSubFace = 0 ; nSubFace < PBL->mSubFaces ; ++nSubFace ) {
    PSF = PBL->PPsubFaces[nSubFace] ;
    if ( PSF->PrBlock ) {
      /* This subface is internal. */
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      if ( PSF->PlBlock == PBL )
	/* The other block is on the right. */
	PBLO = PSF->PrBlock ;
      else
	/* The other block is on the left. */
	PBLO = PSF->PlBlock ;

      if ( PBL->nr <= PBLO->nr ) {
	/* Find the proper static bound. */
	if ( ll[indexStatic] == 1 )
	  /* -ijk face. */
	  ijk[indexStatic] = ll[indexStatic] ;
	else
	  /* +ijk face. */
	  ijk[indexStatic] = ll[indexStatic]-1 ;
	
	/* Loop over the subface. */
	n2 = ll[index2] ;
	n1 = ll[index1] - 1 ; 
	while ( ( nCell = cell_loop_subfc ( ll, ur, mDim,
					    &n1, index1, multCell1,
                                            &n2, index2, multCell2,
					    offsetCell ) ) )
	  if ( elemMrk[nCell] ) {
	    /* This cell is marked. Find out about the status of
	       the cell on the other side of the interface. For that,
	       calculate the indices nI,nJ,nK of the llf node of the cell,
	       translate them to the other block and calculate
	       the linear cell number on the other side. */
	    ijk[index1] = n1 ;
	    ijk[index2] = n2 ;
	  
	    if ( PSF->PlBlock == PBL )
	      /* The other block is on the right. */
	      trans_l2r ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	    else
	      /* The other block is on the left. */
	      trans_r2l ( ijk, PSF->ProtL2R->rotMatrix, PSF->elemShift, ijkO ) ;
	    nCellO = get_nElem_ijk ( mDim, ijkO, PBLO->mVert ) ;
	  
	    if ( PBLO->PelemMark[nCellO] ) {
	      /* Both cells marked. This is a matching interface. */
	      PmatchFc++ ;

#             ifdef CHECK_BOUNDS
		if ( PmatchFc - Pchunk->PmatchFc > mMatchFaces ) {
                  printf ( " FATAL: %d beyond PmatchFc' bounds %d in get_mbMatchFc\n",
                           (int)(PmatchFc - Pchunk->PmatchFc), mMatchFaces ) ;
		  return ( 0 ) ; }
#             endif
	    
	      /* The cell in this block, PBL. */
	      PmatchFc->pElem0 = Pchunk->Pelem + elemMrk[nCell] ;
	      PmatchFc->nFace0 =
		get_faceNr_ijk ( indexStatic, ll[indexStatic], mDim ) ;
	    
	      /* The other cell in PBLO. */
	      PmatchFc->pElem1 = blk2chk[PBLO->nr].Pchunk->Pelem +
		PBLO->PelemMark[nCellO] ;
	      /* Find the orientation of the matching face in the other
		 cell. */
	      if ( PSF->PrBlock == PBLO ) {
		/* The other block is on the right. */
		for ( nDim = 0 ; nDim < mDim ; nDim++ )
		  if ( PSF->llRBlock[nDim] == PSF->urRBlock[nDim] ) {
		    /* This is the static dimension. */
		    if ( PSF->llRBlock[nDim] == 1 )
		      /* -ijk face. */
		      PmatchFc->nFace1 =
			get_faceNr_ijk ( nDim, 1, mDim ) ;
		    else
		      /* +ijk face. */
		      PmatchFc->nFace1 =
			get_faceNr_ijk ( nDim, 99, mDim ) ;
                  }
	      }
	      else {
		/* The other block is on the left. */
		for ( nDim = 0 ; nDim < mDim ; nDim++ )
		  if ( PSF->llLBlock[nDim] == PSF->urLBlock[nDim] ) {
		    /* This is the static dimension. */
		    if ( PSF->llLBlock[nDim] == 1 )
		      /* -ijk face. */
		      PmatchFc->nFace1 =
			get_faceNr_ijk ( nDim, 1, mDim ) ;
		    else
		      /* +ijk face. */
		      PmatchFc->nFace1 =
			get_faceNr_ijk ( nDim, 99, mDim ) ;
                  }
	      }
	      
	    }
	  }
      }
    }
  }

  Pchunk->mMatchFaces = mMatchFaces ;
  return ( 1 ) ;
}

/************************************************************************

  mb_2uns.c:
  Create an unstructured grid from a structured multiblock grid.

  Last update:
  25Apr96: cleanup.

  Input:
  ------
  Pmb: mb grid
  doMap: if non-zero, create a map for the mb verts to the uns verts.

  Output:
  -------
  **ppUns: new unstructured grid. 
   
  Changes to:
  -----------
  *PProotChunk: root of the linked list of chunks of unstructured grids.

  Returns:
  --------
  nBlock: the number of blocks successfully converted.

*/

int mb_2uns ( mb_struct *Pmb, uns_s **ppUns, const int doMap ) {
  
  block_struct *blockS = Pmb->PblockS ;
  const int mDim = Pmb->mDim, mUnknown = Pmb->mUnknowns;
  int mBlocks = Pmb->mBlocks, nBlock, noBlocks ;
  chunk_struct *Pchunk ;
  blk2chk_struct *blk2chk ;
  chk2blk_struct *chk2blk ;

  /* Make an uns root. */
  if ( !( *ppUns = make_uns ( NULL ) ) )
    hip_err ( fatal, 0, "could not alloc for an uns root in mb_2uns." ) ;

  /* Init the epsOverlap for the new grid. */
  (*ppUns)->epsOverlap = Grids.epsOverlap ;
  (*ppUns)->epsOverlapSq = Grids.epsOverlapSq ;
  

  /* Alloc two pointer fields between blocks and chunks. */
  blk2chk = arr_malloc ( "blk2chk in mb_2uns", (*ppUns)->pFam,
                         mBlocks+1, sizeof( *blk2chk ) ) ;
  chk2blk = arr_malloc ( "chk2blk", (*ppUns)->pFam,
                         mBlocks+1, sizeof( *chk2blk ) ) ;

  
  /* Check whether all blocks have a marker field for the vertices. */
  for ( noBlocks = 1, nBlock = 1 ; nBlock <= mBlocks ; nBlock++ )
    if ( blockS[nBlock].PintMark )
      noBlocks = 0 ;

  if ( noBlocks )
    /* No blocks have marker fields. Take the entire grid. */
    mark_mb_all ( Pmb ) ;
    
  /* Initialize the chunk to NULL, such that it is made in add_mb2uns. */
  Pchunk = NULL ;

  /* Loop over all blocks and add the ones with markers. */
  (*ppUns)->mDim = mDim ;
  for ( nBlock = 1 ; nBlock <= mBlocks ; nBlock++ )
    if ( blockS[nBlock].PintMark ) {
      if ( !add_mb2uns ( blockS+nBlock, mDim, mUnknown, *ppUns, 
			 &Pchunk, blk2chk, chk2blk, doMap ) )
	return ( nBlock-1 ) ;
      else if ( nBlock == 1 )
	/* Set the root. */
	(*ppUns)->pRootChunk = Pchunk ;
    }
  
  /* Loop over all blocks and get the matching face information.
     This requires that for all blocks involved, the base address
     of the elements Pchunk->Pelem and the incremental element
     marker PelemMark are set. */
  for ( nBlock = 1 ; nBlock <= mBlocks ; nBlock++ )
    if ( blockS[nBlock].PintMark )
      /* Get internal boundary faces. */
      if ( !get_mbMatchFc ( blockS+nBlock, mDim,
			    blk2chk[nBlock].Pchunk, blk2chk ) )
	return ( nBlock-1 ) ;
  
  arr_free ( blk2chk ) ;
  arr_free ( chk2blk ) ;

  return ( mBlocks ) ;
}


/************************************************************************

  cp_mb2uns.c:
  Create an unstructured grid from some other type.

  Last update:
  ------------
  6Sep18; new arg doCheck for merge_uns.
  4Sep18; new interface to merge_uns
  12Mar18: intro doMap.
  19Dec17; new interface to make_uns.
  14Sep10; rename to cp_mb2uns, to distinguish with cp_uns2uns.
  30Oct00; initialise varType with Pgrid.
  25Apr96: cleanup.

  Input:
  ------
  doMap: compute a map from the mb to the uns grid.

  Returns:
  --------
  0/1.

*/


int cp_mb2uns ( int doMap ) {
  
  mb_struct *Pmb ;
  int uBlocks, mDim, nSubFace ;
  subFace_struct *Psf ;
# define DONT_CHECK 0
# define DO_CHECK 1
  grid_struct *Pgrid = NULL ;
  uns_s *pUns ;
  
  if ( Grids.PcurrentGrid->mb.type != mb ) {
    hip_err ( warning, 1, "could not convert grid of this type." ) ;
  }

    
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "  Copying multiblock grid %d to unstructured grid %d.\n",
             Grids.PcurrentGrid->mb.nr, Grids.mGrids + 1 ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }
  Pmb = Grids.PcurrentGrid->mb.Pmb ;
  mDim = Pmb->mDim ;

  /* Loop over all subfaces with no right block and a bc.
     Test for face degeneracies. */
  for ( nSubFace = 1 ; nSubFace <= Pmb->mSubFaces ; nSubFace++ ) {
    Psf = Pmb->subFaceS + nSubFace ;
    if ( Psf->Pbc && !strncmp ( Psf->Pbc->text, "hip_mb_degen", 12 ) )
      /* Degenerate by label. */
      Psf->Pbc = NULL ;
    else if ( find_mbDegenFaces &&
              !Psf->PrBlock && Psf->Pbc &&
              is_degen_subfc ( Psf->PlBlock, Psf, mDim ) ) {
      /* Degenerate by epsOverlap. Set the bc to NULL. */
      if ( verbosity > 4 ) {
        sprintf ( hip_msg, "block %d, subface %d, bc %s is degenerate.\n",
                 Psf->PlBlock->nr, Psf->nr, Psf->Pbc->text ) ;
        hip_err ( info, 4, hip_msg ) ;
      }
      Psf->Pbc = NULL ;
    }
  }
      

  /* Make an unstructured chunk for each block. */
  uBlocks =  mb_2uns ( Pmb, &pUns, doMap ) ;
  if ( uBlocks != Pmb->mBlocks )
    hip_err ( warning, 1, "could not convert all blocks to unstructured.\n" ) ;


  /* Make a new grid. */
  if ( ( Pgrid = make_grid () ) ) {
    Pgrid->uns.pUns = pUns ;
    Pgrid->uns.type = uns ;
    Pgrid->uns.mDim = mDim ;
    pUns->nr = Pgrid->uns.nr ;
    pUns->pGrid = Pgrid ;

    /* As far as I can recall, there is no solution with any structured format. */
    pUns->varList.varType = noVar ;
    pUns->varList.mUnknowns = 0 ;
    Pgrid->uns.pVarList = &(pUns->varList) ;
  }
  else
    hip_err ( fatal, 0, "alloc for the linked list of grids failed" ) ;

  /* Merge the chunks to one consistent mesh. For this, the number of unknowns
     has to be set. */
  if ( !merge_uns ( pUns, 0, 1 ) )
    hip_err ( fatal, 0, "could not match all unstructured blocks in cp_mb2uns." ) ;

  set_current_pGrid ( Pgrid ) ;

  return (1) ;
}
