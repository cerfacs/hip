/*
  get_edge_weights.c:
  Calculate edge weights for a given unstructured grid. 

  Last update:
  ------------
  11Mar01; derived from get_edge_weights.

  This file contains:
  -------------------
  ewts_elem
  make_edge_weights
  */

#include "cpre.h"
#include "proto.h"

#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

extern const Grids_struct Grids ;
extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const int bitEdge[] ;


/******************************************************************************

  get_ewts_elem_a:
  Antisymmetric edges only.
  Calculate the complete set of edgeweights for an element.
  
  Last update:
  ------------
  11Mar01: antisymmetric only. conceived.
  
  Input:
  ------
  pllEdge      list of edges
  ppEdgeNorm   list of edge weights.
  pElem        the element

  Changes To:
  -----------
  pllEdge      add the edges of the element, if not present
  ppEdgeNorm:  possible realloc, accumulate the weights of the edges.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int ewts_elem ( llEdge_s *pllEdge, double **ppEdgeWt,
                             elem_struct *pElem ) {
  
  static const elemType_struct *pElT ;
  static const faceOfElem_struct *pFoE ;
  static const edgeOfElem_struct *pEoE ;
  static const int *kVxEdge ;
  static const vrtx_struct *pVx[MAX_EDGES_ELEM][2], *pVx0, *pVx1,
    *pVxEl[MAX_VX_ELEM], *pVxFc[MAX_VX_FACE] ;
  static int kEdge, nElEg[MAX_EDGES_ELEM], side[MAX_EDGES_ELEM],
    kFace, mDim, iFc, mVxEl, mVxFc[MAX_FACES_ELEM+1], newEg ;
  static double surfNorm[2][MAX_DIM], elemGC[MAX_DIM], faceGC[MAX_DIM], edgeGC[MAX_DIM],
    elem2EdgeGC[MAX_DIM], elem2FaceGC[MAX_FACES_ELEM+1][MAX_DIM],
    *pEN ;

  pElT = elemType + pElem->elType ;
  mDim = pElT->mDim ;


  /* Find/add all edges of the element. */
  for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
    if ( !( nElEg[kEdge] =
            add_elem_edge ( pllEdge, (void**) ppEdgeWt, pElem, kEdge,
                            pVx[kEdge], pVx[kEdge]+1, side+kEdge, &newEg ) ) ) {
      printf ( " FATAL: could not add edge in get_ewts_elem.\n" ) ;
      return ( 0 ) ; }

  
  if ( mDim == 2 ) {
    /* Basic median dual. */
    elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;
    for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
      kVxEdge = pElT->edgeOfElem[kEdge].kVxEdge ;
      pEN = *ppEdgeWt + 2*sizeof( double )*nElEg[kEdge] ;
      /* Have surfNorm point away from the 0th vertex on the geometric edge. */
      med_normal_edge_2D ( pElem, elemGC, kEdge, surfNorm[0] ) ;

      if ( side[kEdge] )
        vec_add_mult_dbl ( pEN, -.5, surfNorm[0], 2, pEN ) ;
      else
        vec_add_mult_dbl ( pEN,  .5, surfNorm[0], 2, pEN ) ;
    }
  }

  else {
    /* 3D: Loop over all faces, calculate the face gravity center and
       the median dual surface attached to each edge of the face. */
    
    elem_grav_ctr ( pElem, elemGC, &pElT, &mVxEl, pVxEl ) ;
    for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
      face_grav_ctr ( pElem, kFace, faceGC, &pFoE, mVxFc+kFace, pVxFc ) ;
      vec_diff_dbl ( faceGC, elemGC, 3, elem2FaceGC[kFace] ) ;
    }

    /* Loop over all edges. */
    for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ ) {
      pEoE = pElT->edgeOfElem + kEdge ;
      kVxEdge = pEoE->kVxEdge ;
      pVx0 = pElem->PPvrtx[ kVxEdge[0] ] ;
      pVx1 = pElem->PPvrtx[ kVxEdge[1] ] ;
      
      if ( pVx0 != pVx1 ) {
        /* Non-collapsed edge. */
        edge_grav_ctr ( pElem, kEdge, edgeGC ) ;
        /* Vector from the center of the edge to the gravity center of the element. */
        vec_diff_dbl ( edgeGC, elemGC, 3, elem2EdgeGC ) ;

        /* Calculate the areas of the two triangular facets of the median dual. */
        for ( iFc = 0 ; iFc < 2 ; iFc++ ) {
          kFace = pEoE->kFcEdge[iFc] ;
          if ( mVxFc[kFace] > 2 )
            /* Non-collapsed face. */
            cross_prod_dbl ( elem2FaceGC[kFace], elem2EdgeGC, 3, surfNorm[iFc] ) ;
          else
            vec_ini_dbl ( 0., 3, surfNorm[iFc] ) ;
        }
        /* The two parts have opposite directions. Add them reversing one. */
        vec_diff_dbl ( surfNorm[0], surfNorm[1], 3, surfNorm[0] ) ;


        if ( side[kEdge] )
          vec_add_mult_dbl ( pEN, -.25, surfNorm[0], 3, pEN ) ;
        else
          vec_add_mult_dbl ( pEN,  .25, surfNorm[0], 3, pEN ) ;
      }
    }
  }
  
  return ( 1 ) ;
}

/******************************************************************************

  make_edge_normals_a:
  Simplified version w/o symmetric edges and w/o boundary edges. But does
  include the modifications for elements w/ hanging nodes.
  Make a list of all the edges in a grid. In the course, a list of edges is
  generated and possibly reused later on. Thus, it is not deallocated.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  4Apr13; modified interface to loop_elems.
  11Mar01: conceived.
  
  Input:
  ------
  pUns
  ppEdgeWt:  


  Changes To:
  -----------
  ppEdgeWt: a list of 2*mDim weights for each edge, starting at 1. First mDim
              weights for the first vx of the edge by the second, and then vice
	      versa.
  pmEdges:    the size of the list of edges.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_edge_weights ( uns_s *pUns, double **ppEdgeWt, int *pmEdges ) {
  
  const int dataSize = pUns->mDim*sizeof(double) ;
  const int mDim = pUns->mDim ;
  int mMakesNegVol, doesAddCtrVx ;

  llEdge_s *pllEdge ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElemBeg, *pElemEnd ;

  int mVxHg, kVxHg[MAX_ADDED_VERTS] ;
  vrtx_struct *pVxElem[MAX_REFINED_VERTS] ;
  surfTri_s *pSurfTri ;
  childSpc_s childSpc ;

  
  /* Make some initial storage for the list. */
  free_llEdge ( &(pUns->pllEdge) ) ;
  if ( !( pUns->pllEdge = pllEdge =
          make_llEdge ( pUns, CP_NULL, 0, dataSize, NULL, (void **) ppEdgeWt ) ) ) {
    hip_err ( fatal, 0, "could not make an edge list in make_edge_normals." ) ;
  }

  
  /* Loop over all elements and add their edges to the list. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number ) {

#ifdef ADAPT_HIERARCHIC
        if ( pUns->pllAdEdge )
          pSurfTri = make_surfTri ( pUns, pElem, &mVxHg, kVxHg, pVxElem ) ;
        else
#endif
          mVxHg = 0 ;

        /* Is this a derived one? */
#ifdef ADAPT_HIERARCHIC
        if ( mVxHg ) {
	  if ( mDim == 2 )
	    buffer_2D_elem ( pElem, pUns, &childSpc ) ;
	  else
	    buffer_3D_elem ( pUns, pElem, mVxHg, kVxHg, pVxElem, pSurfTri, &childSpc,
                             0, &mMakesNegVol, &doesAddCtrVx) ;
        }
        else
#endif
          ewts_elem ( pllEdge, ppEdgeWt, pElem ) ;
      }

  /* Combine antisymmetric edges along periodic boundaries.
  comb_ewts_a ( pUns, nLstEg, *ppEdgeWt ) ; */
  
  return ( 1 ) ;
}

