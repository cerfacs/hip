/*
  write_uns_cut.c:
  Write a cut of an unstructured mesh.
   
  Last update:
  7Jul98; use get_elem_edge.
  
  Contains:
  ---------
  write_mb_faces:
*/

#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

const extern int verbosity ;
extern char hip_msg[] ;
const extern elemType_struct elemType[] ;

static int write_uns_cut_faces ( uns_s *pUns, char *ProotFile );
static int write_uns_cut_edges ( uns_s *pUns, char *rootFile );

/******************************************************************************

  write_uns_cut:
  .
  
  Last update:
  ------------
  6Apr13; promote possibly large int to type ulong_t.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_uns_cut ( uns_s *pUns, char *rootFile )
{

  if ( pUns->mDim == 3 )
    return ( write_uns_cut_faces ( pUns, rootFile ) ) ;
  else if ( pUns->mDim == 2 )
    return ( write_uns_cut_edges ( pUns, rootFile ) ) ;
  else {
    printf ( " FATAL: wrong dimension %d in write_uns_cut.\n", pUns->mDim ) ;
    return ( 0 ) ;
  }
}



/******************************************************************************
  
  write_uns_cut_edges:
  Write the lists of edges that are open to a cut in a .pts format.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems.
  25May09; use full char length of LINE_LEN in r1_fopen.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_uns_cut_edges ( uns_s *pUns, char *rootFile )
{
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxEdge ;
  
  typedef struct {
    bc_struct *pBc ;
    int mUsed ;
    int dir ;     /* 0 if the domain is to the left of the edge. */
  } cutEg_s ;

  char ptsFile[TEXT_LEN] ;
  FILE *fPts ;
  llEdge_s *pllEdge ;
  cutEg_s *pCutEg ;
  chunk_struct *pChunk ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem, *pElemBeg, *pElemEnd ;
  int kEdge, side, nEg, nVxEg, mEgVx, nEgBeg=0 ;
  ulong_t nLstEdge ;
  int nBnd, n1, nNxtEg=0, mBnd=0, nxtBnd, prvBnd, doPrint, mBndWritten, 
    mSegments, setBeg, newEg ;
  const vrtx_struct *pcVxEg[2] ;
  vrtx_struct *pVxEg[2], **ppVx, *pVxNxt ;
  bc_struct *pBc ;


  sprintf ( ptsFile, "%s.pts", rootFile ) ;
  if ( !( fPts = r1_fopen ( prepend_path ( ptsFile ), LINE_LEN, "w" ) ) ) {
    printf ( " FATAL: could not open %s in write_uns_cut_edges.\n", ptsFile ) ;
    return ( 0 ) ; }
  

  /* Make a root of a list of edges. */
  if ( !( pllEdge = make_llEdge ( pUns, CP_NULL, 0, sizeof( cutEg_s ),
                                  NULL, (void**) &pCutEg ) ) ) {
    printf ( " FATAL: could not alloc a list root write_uns_cut_edges.\n" ) ;
    return ( 0 ) ; }
  
  /* Loop over all boundaries and make a list of its edges. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
      if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
	pElem = pBndFc->Pelem ;
	ppVx = pElem->PPvrtx ;
	pElT = elemType + pElem->elType ;
	pFoE = pElT->faceOfElem + pBndFc->nFace ;
	kEdge = pFoE->kFcEdge[0] ;
	kVxEdge = pElT->edgeOfElem[kEdge].kVxEdge ;

	/* The two forming nodes of the edge. */
	pcVxEg[0] = ppVx[ kVxEdge[0] ] ;
	pcVxEg[1] = ppVx[ kVxEdge[1] ] ;

	/* Add the edge to the list. */
	nEg = add_edge_vrtx ( pllEdge, (void**) &pCutEg, pcVxEg, pcVxEg+1,
                              &side, &newEg ) ;
	/* If edgeDir == 1, the elem is to the left of the edge. */
	pCutEg[nEg].dir = 1-pFoE->edgeDir[0] ;
	/* If side == 1, the edge is reversed in the list. */
	pCutEg[nEg].dir = ( side ? 1-pCutEg[nEg].dir : pCutEg[nEg].dir ) ;
	pCutEg[nEg].pBc = pBndFc->Pbc ;
      }

  /* Reset the count of elements formed with each edge. */
  get_number_of_edges ( pllEdge, &nLstEdge ) ;
  for ( nEg = 1 ; nEg <= nLstEdge ; nEg++ )
    pCutEg[nEg].mUsed = 0 ;

  /* Count the number of elements that are formed by an edge. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElemBeg, &pElemEnd ) )
    for ( pElem = pElemBeg ; pElem <= pElemEnd ; pElem++ )
      if ( pElem->number ) {
	pElT = elemType + pElem->elType ;
	ppVx = pElem->PPvrtx ;
	
	/* Loop over all edges of the element. */
	for ( kEdge = 0 ; kEdge < pElT->mEdges ; kEdge++ )
	  if ( ( nEg = get_elem_edge ( pllEdge, pElem, kEdge,
                                       pcVxEg, pcVxEg+1, &side ) ) ) {
	    /* This edge exists. */
	    pCutEg[nEg].mUsed++ ;
	  }
      }
  

  /* Loop while there are edges left over. Whenever the bc changes in a
     string of edges, start a new segment for the .pts file. */
  nEg = mBndWritten = mSegments = 0 ;
  while ( 1 ) {

    if ( !nEg ) {
      /* Find a new start for a loop. */

      /* Loop over all edges in the list, find an unused edge. */
      for ( nEg = 1 ; nEg <= nLstEdge ; nEg++ )
	if ( pCutEg[nEg].mUsed == 1 )
	  break ;

      if ( nEg > nLstEdge )
	/* No more single-sided edges. */
	break ;
    }


    nEgBeg = nEg ;
    for ( setBeg = 0, doPrint = 0, doPrint = 0 ; doPrint < 2 ; doPrint++ ) {
      /* Loop over the string of edges. In the first loop just count the number
	 of segments of the loop. */
      nEg = nEgBeg ;
      nBnd = mBndWritten ;

      if ( doPrint )
	/* We have a valid nEgBeg to start the loop with. Trigger a new segment. */
	pBc = NULL ;
      else
	pBc = pCutEg[nEg].pBc ;
      
      while ( nEg ) {
	
	side = pCutEg[nEg].dir ;
	show_edge ( pllEdge, nEg, pVxEg, pVxEg+1 ) ;

	if ( pBc != pCutEg[nEg].pBc ) {
	  /* Open a new segment. */
	  ++nBnd ;
	  pBc = pCutEg[nEg].pBc ;

	  if ( doPrint ) {
	    /* Mark this edge as written to file. */
	    pCutEg[nEg].mUsed = -1 ;

	    nxtBnd = ( nBnd == mBnd ? mBndWritten+1 : nBnd+1 ) ;
	    prvBnd = ( nBnd == mBndWritten+1 ? mBnd : nBnd-1 ) ;
	    fprintf ( fPts, 
		      "NEWBND \nNAMEBN \n%d %s\nNFRSBN \n%d \nNLSTBN \n%d \n"
		      "ITYPBN \n%d \nBNDEXY\n",
		      nBnd, pBc->text, prvBnd, nxtBnd, 1 ) ;

	    if ( verbosity > 4 )
	      printf ( "   INFO: found cut boundary %s\n", pBc->text ) ;
	  }
	  else if ( !setBeg ) {
	    /* First sweep, and we found a switch in bc. Start the loop. */
	    setBeg = 1 ;
	    nEgBeg = nEg ;
	  }

	  
	  /* List the starting vertex. */
	  show_edge ( pllEdge, nEg, pVxEg, pVxEg+1 ) ;
	  side = pCutEg[nEg].dir ;
	  if ( doPrint ) 
	    fprintf ( fPts, "%g %g\n", pVxEg[side]->Pcoor[0], pVxEg[side]->Pcoor[1] ) ;
	}

	/* The next vertex in the loop. */
	pVxNxt = pVxEg[1-side] ;
	if ( doPrint ) {
	  pCutEg[nEg].mUsed = -1 ;
	  fprintf ( fPts, "%g %g\n", pVxNxt->Pcoor[0], pVxNxt->Pcoor[1] ) ;
	}

	/* Find the next edge in the loop. */
	nVxEg = mEgVx = 0 ;
	while ( loop_edge_vx ( pllEdge, pVxNxt, &n1, &nVxEg, &side ) )
	  if ( nVxEg != nEg ) {
	    nNxtEg = nVxEg ;
	    mEgVx++ ;
	  }

	if ( mEgVx != 1 ) {
	  fclose ( fPts ) ;
	  sprintf ( hip_msg, "found %d edges for vertex %"FMT_ULG" in write_uns_cut_edges.\n",
		   mEgVx, pVxNxt->number ) ;
          hip_err ( fatal, 0, hip_msg ) ;
	  return ( 0 ) ; }
      
	else if ( nNxtEg == nEgBeg )
	  /* This loop of edges is done. */
	  nEg = 0 ;

	else {
	  nEg = nNxtEg ;
	}
      }

      /* Segment loop closed. */
      if ( !setBeg && !doPrint )
	/* We have not yet switched the boundary, only one type in this loop. */
	nBnd++ ;
      mBnd = nBnd ;
      if ( doPrint ) {
	mBndWritten += mBnd ;
	mSegments++ ;
      }
    }
  }

  if ( verbosity > 2 )
    printf ( "   INFO: found %d segments.\n", mSegments ) ;
  
  fclose ( fPts ) ;
  return ( 1 ) ;
}


/******************************************************************************

  write_uns_cut_faces:
  Write all the cut faces of a 3D unstructured mesh.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pmb:
  FfaceOut

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_uns_cut_faces ( uns_s *pUns, char *ProotFile )
{
  char fileName[TEXT_LEN] ;
  FILE *FpointsOut, *FfacesOut ;
  chunk_struct *Pchunk ;
  int mMarkedVerts = 0, mTriFaces = 0, mQuadFaces = 0, fcType ;
  intFc_struct *PintFc ;
  vrtx_struct *Pvrtx, ***PPPvrtx, **PPvxFc[MAX_VX_FACE] ;

  sprintf ( fileName, "%s.points", ProotFile ) ;
  FpointsOut = fopen ( prepend_path ( fileName ), "w" ) ;
  sprintf ( fileName, "%s.faces", ProotFile ) ;
  FfacesOut = fopen ( prepend_path ( fileName ), "w" ) ;
  if ( !FpointsOut || !FfacesOut ) {
    printf ( " FATAL: could not open files for the cut.\n" ) ;
    return ( 0 ) ; }
  
  /* Fix this. */
  printf ( " WARNING: At this moment, 7Feb98, I cannot see any use for this\n"
	   "          function. What is the format actually? If you use it, you\n"
	   "          better tell me <muller@comlab.ox.ac.uk> before 6Mar98,\n"
	   "          otherwise the cleaner in me will remove this feature.\n" ) ;
  
  /* Loop over all chunks. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  { /* Reset the vertex marks. */
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
	  Pvrtx <= Pchunk->Pvrtx+Pchunk->mVerts ; Pvrtx++  )
      Pvrtx->mark = 0 ;

    /* Mark the vertices in the faces. */
    for ( PintFc = Pchunk->PintFc + 1 ;
	  PintFc <= Pchunk->PintFc + Pchunk->mIntFaces ; PintFc++ )
      if ( PintFc->Pelem && PintFc->Pelem->number && PintFc->nFace )
      { get_uns_face ( PintFc->Pelem, PintFc->nFace, PPvxFc, &fcType ) ;
	for ( PPPvrtx = PPvxFc ;
	      PPPvrtx < PPvxFc + fcType ; PPPvrtx++ )
	  (**PPPvrtx)->mark = 1 ;

	/* Count the faces. */
	if ( fcType == 3 )
	  ++mTriFaces ;
	else if  ( fcType == 4 )
	  ++mQuadFaces ;
	else
	  printf ( " WARNING: unknown face type %d in write_uns_cut.\n",
		   fcType ) ;
      }

    /* Count and number the vertices. */
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
	  Pvrtx <= Pchunk->Pvrtx+Pchunk->mVerts ; Pvrtx++  )
      if ( Pvrtx->mark )
	Pvrtx->mark = ++mMarkedVerts ;
  }    

  /* Write the vertices. */
  rewind ( FpointsOut ) ;
  fprintf ( FpointsOut, "%d\n", mMarkedVerts ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
	  Pvrtx <= Pchunk->Pvrtx+Pchunk->mVerts ; Pvrtx++  )
      if ( Pvrtx->mark )
	fprintf ( FpointsOut, " %g %g %g %d\n",
		  Pvrtx->Pcoor[0], Pvrtx->Pcoor[1], Pvrtx->Pcoor[2],
		  Pchunk->nr ) ;

  /* Write the faces. */
  rewind ( FfacesOut ) ;
  fprintf ( FfacesOut, " %d 0\n", mTriFaces + 2*mQuadFaces ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( PintFc = Pchunk->PintFc + 1 ;
	  PintFc <= Pchunk->PintFc + Pchunk->mIntFaces ; PintFc++ )
      if ( PintFc->Pelem && PintFc->Pelem->number && PintFc->nFace )
      { get_uns_face ( PintFc->Pelem, PintFc->nFace, PPvxFc, &fcType ) ;
	if ( fcType == 3 )
	{ /* Triangle. */
	  fprintf ( FfacesOut, "3 %d %d %d %d 0 0 0\n",
		    (*PPvxFc[0])->mark, (*PPvxFc[1])->mark, (*PPvxFc[2])->mark,
		    Pchunk->nr ) ;
	}
	
	else if ( fcType == 4 )
	{ /* Quad, list two triangles. */
	  fprintf ( FfacesOut, "3 %d %d %d %d 0 0 0\n",
		    (*PPvxFc[0])->mark, (*PPvxFc[1])->mark, (*PPvxFc[2])->mark,
		    Pchunk->nr ) ;
	  fprintf ( FfacesOut, "3 %d %d %d %d 0 0 0\n",
		    (*PPvxFc[0])->mark, (*PPvxFc[2])->mark, (*PPvxFc[3])->mark,
		    Pchunk->nr ) ;
	}
      }
  return ( 1 ) ;
}
