/*
  adapt.c
  Call to adaption from the main. Thus, must pretend to know about mb.
  Contains all functions that need to know about sens_s.

  Last update:
  ------------
  7Jan05; replace heap_cmp_double with cmp_double.
  12Jan: cleanup. Rewrite.

  This file contains:
  -------------------
  cmp_sensor
  order_sensor
  adapt

*/
  
#include "cpre.h"
#include "proto.h"
#include "proto_mb.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern const unsigned int bitEdge[] ;
extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern char hip_msg[] ;

typedef struct {
  elem_struct *pElem ;
  double val ;
} sens_s ;

int cmp_sensor ( const void *p0, const void *p1 ) {

  const sens_s *pS0 = ( const sens_s * ) p0, *pS1 = ( const sens_s * ) p1 ;

  if ( pS0->val > pS1->val )
    return ( 1 ) ;
  else if  ( pS0->val < pS1->val )
    return ( -1 ) ;
  else
    return ( 0 ) ;
}


/******************************************************************************
  order_sensor:
  Calculate the maximum values of the scalar in each element and sort them.

  Last update:
  ------------
  16Dec11; rename hrbMark to boxMark.

  
  Input:
  ------
  pUns:
  x_scalar: function to extract the scalar
  kVar:     which scalar to extract.
  pSensor:  the list of sensors

  
  Changes To:
  -----------  
  pSensor

  Returns:
  --------
  mEl:     the number of elements in the hrb.
  
*/

static int order_sensor ( uns_s *pUns, 
                          int (*x_scalar) ( const elem_struct*, const int,
                                            double* , double* , double* ),
                          const int kVar, sens_s *pSensor,
                          double *pAvg, double *pDev ) {
  
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElB, *pElE ;
  double maxVal, elAvg, EdgeScal[MAX_EDGES_ELEM] ;
  int mEl ;
  sens_s *pS = pSensor ;

  *pAvg = 0. ;
  
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElB, &pElE ) )
    for ( pElem = pElB ; pElem <= pElE ; pElem++ )
      if ( pElem->term &&
           //pElem->boxMark
           elem_has_mark ( pElem, BOX_MARK )
           ) {
          /* Calculate Scal of current Elem and hold min and max.   */
          (*x_scalar) ( pElem, kVar, &maxVal, &elAvg, EdgeScal ) ;
          pS->pElem = pElem ;
          pS->val = maxVal ;
          *pAvg += ABS( maxVal ) ;
          pS++ ;
      }

  mEl = pS - pSensor ;

  /* Calculate an average. */
  *pAvg /= mEl ;

  /* And an average deviation. */
  for ( *pDev = 0., pS = pSensor ; pS < pSensor + mEl ; pS++ ) {
    *pDev += ABS( pS->val - *pAvg ) ;
  }
  *pDev /= mEl ;


  qsort ( pSensor, mEl, sizeof ( sens_s ), cmp_sensor ) ;



  if ( verbosity > 4 ) {
    FILE *dpl ;
    char dplName[LINE_LEN] ;
    int i ;
    double *vcp = arr_malloc ( "vcp", NULL, mEl, sizeof( double ) ),
      avg = *pAvg, dev = *pDev ;

    /* Copy the list and sort the copy. */
    
    for ( i = 0 ; i < mEl ; i++ )
      vcp[i] = pSensor[i].val ;
    qsort ( vcp, mEl, sizeof( double ), cmp_double ) ;

    
    printf ( " writing vf distribution to vf.ldpl.\n" ) ;
    
    strcpy ( dplName, "vf.ldpl" ) ;
    prepend_path ( dplName ) ;
    dpl = fopen ( dplName, "w" ) ;

    fprintf ( dpl, "vf distribution\n" ) ;

    fprintf ( dpl, "%d %g\n", mEl-1, avg-dev ) ;
    fprintf ( dpl, "%d %g\n", 0, avg-dev ) ;
    fprintf ( dpl, "%d %g\n", 0, avg ) ;
    fprintf ( dpl, "%d %g\n", mEl-1, avg ) ;
    fprintf ( dpl, "%d %g\n", mEl-1, avg+dev ) ;
    fprintf ( dpl, "%d %g\n", 0, avg+dev ) ;

    
    for ( i = mEl-1 ; i >= 0 ; i-- )
      fprintf ( dpl, "%d %g\n", mEl-1-i, vcp[i] ) ;


    
    fclose ( dpl ) ;
    arr_free ( vcp ) ;
  }

  
  
  return ( mEl ) ;
}

/**************************************************************************************
  adapt:
  Adapt a grid.
  
  Last update:
  ------------
  16Dec11; rename hrbMark to boxMark.
  4Apr09; replace varTypeS with varList.
  12Jan01; no more tillier.
  7Sep00; clean up. Throw out entropy and dissipation sensors, ditch kword2funk.
  : conceived.
  
  Input:
  ------
  keyword:  one of max-div-rot or diff. 
  deref:    fraction of the grid - according to the number of cells - to be Derefine
  ref:      fraction  if the grid to be Refine . ref = 1 - refFct (see calc_ref_deref )
  anisotropy: if 0, do everything isotropically, including propagation.
 
  Changes To:
  -----------
  
  Returns:
  --------
  0 on failure, 1 on success.

*/

int adapt_hierarchical_sensor ( char keyword[], const double deref, const double ref, const int iso ) {
  
  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  chunk_struct *pChunk ;
  int kVar=0, mEl, mDeref, mRef, mLvl, nB, nE ;
  double valDeref, valRef, avg, dev ;
  elem_struct *pElem, *pElB, *pElE ;
  int (*x_scalar)( const elem_struct*, const int,
		   double*, double*, double*) ;
  sens_s *pSensor, *pS ;

  /* find the adapt_functions */
  if ( Grids.PcurrentGrid->uns.type != uns ) {
    printf ( " FATAL: only unstructured grids can be refined.\n" ) ;
    return ( 0 ) ; }
  if ( pUns->varList.varType == noVar ) {
    /* No unknowns, use vertex numbers. Useful for global de/refinement.
    kVar = -1 ;
    x_scalar = calc_diff ; */
    printf ( " FATAL: no solution, no adaption.\n" ) ;
    return ( 0 ) ; }


  
  
  /* Select an adaptation sensor. */
  if (!strncmp( keyword, "div-rot", 5 ))
    x_scalar = calc_M_R_D ;
  else if (!strncmp( keyword, "diff", 2 ))
    x_scalar = calc_diff ;
  else if (!strncmp( keyword, "abs", 2 ))
    x_scalar = calc_abs ;
  else {
    printf ( " FATAL: cannot perform adaption on %s in adapt.\n", keyword ) ;
    return ( 0 ) ;
  }

  if ( x_scalar == calc_diff || x_scalar == calc_abs ) {
    /* Diff on what? */
    r1_stripsep ( keyword, LINE_LEN ) ;
    if ( isdigit ( keyword[0] ) )
      kVar = atoi ( keyword )-1 ;
    else {
      switch ( keyword[0] ) {
      case 'r' :
        /* rho: specific mass */
        kVar = 0 ;
        break ;
      case 'q' :
        /* q: Velocity vector norm */
        conv_uns_var ( pUns, prim ) ;
        kVar = 101 ;
        break ;
      case 'p' :
        /* p: pressure */
        conv_uns_var ( pUns, prim ) ;
        kVar = ( pUns->varList.mUnknFlow == 5 ? 4 : pUns->mDim+1 ) ;
        break ;
      case 't':
        /* t: temperature */
        conv_uns_var ( pUns, prim ) ;
        kVar = ( pUns->varList.mUnknFlow == 5 ? 104 : 100+pUns->mDim+1  ) ;
        break ;
      default  :
        kVar = 0 ;
        break ;
      }
    }
  }


  
  /* Use the element mark for all elements to be worked upon. */
  reset_all_elem_all_mark ( pUns ) ;

  pSensor = arr_malloc ( "sensor", pUns->pFam, pUns->mElemsNumbered, sizeof ( sens_s )) ;

  /* Calculate the maximum scalar value per element and order. Lowest value first.
     Only pElem->term && pElem->boxMark are included. */
  mEl = order_sensor ( pUns, x_scalar, kVar, pSensor, &avg, &dev ) ;

  if ( ref >= 0. ) {
    /* Fixed fractions. */
    /* Derefinement from the bottom. */
    mDeref = mEl*deref ;
    valDeref = pSensor[mDeref].val ;
    if ( mDeref > 0. ) 
      /* Do complete derefinement of large areas of uniform flow, even if
         there are more than mDeref elements in these areas. Add, say, 1%
         of the max-min sensor. */
      valDeref += .01*( pSensor[mEl-1].val - pSensor[0].val ) ;
    else
      /* No Derefinement. */
      valDeref = valDeref - ABS( valDeref ) ;
    
    for ( pS = pSensor ; pS < pSensor + mDeref ; pS++ ) {
      pElem = pS->pElem ;
      pElem->mark = 1 ;
      debuffer_elem ( pElem ) ;
    }

    /* Refinement from the top. Disregard all elements marked that already have
       the maximum refinement level. mRef is the position of the lowest to be refined. */
    mRef = mEl*( 1.-ref ) ;
    mRef = MAX( 0,   mRef ) ;
    mRef = MIN( mEl, mRef ) ;
    valRef = pSensor[mRef].val ;
    if ( ref >= 1. )
      /* Full refinement. */
      valRef += ABS( valRef ) ;
    
    for ( pS = pSensor+mRef ; pS < pSensor + mEl ; pS++ ) {
      pElem = pS->pElem ;
      mLvl = adaptLvl_elem ( pElem ) ;
      if ( mLvl < Grids.adapt.maxLevels ) {
        pElem->mark = 1 ;
        debuffer_elem ( pElem ) ;
      }
    }

    
    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "refining above %g = avg+%g*dev.",
                valRef, ( valRef - avg )/dev ) ;
      hip_err ( info, 1, hip_msg ) ;
      sprintf ( hip_msg, "derefining below %g = avg-%g*dev.\n",
                valDeref, ( avg-valDeref )/dev ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
  }
  
  else {
    /* Mean deviation. */
    valDeref = avg - ABS( deref )*dev ;
    valRef   = avg + ABS( ref )*dev ;

    /* Mark for derefinement. */
    for ( mDeref = 0, pS = pSensor ; pS < pSensor + mEl ; pS++ )
      if ( pS->val < valDeref ) {
        pS->pElem->mark = 1 ;
        debuffer_elem ( pS->pElem ) ;
        mDeref++ ;
      }
      else
        break ;


    /* Mark for refinement. */
    for ( pS = pSensor+mEl-1, mRef = 0 ; pS > pSensor + mDeref ; pS-- ) {
      pElem = pS->pElem ;
      mLvl = adaptLvl_elem ( pElem ) ;
      if ( mLvl < Grids.adapt.maxLevels && pS->val > valRef ) {
        pElem->mark = 1 ;
        debuffer_elem ( pElem ) ;
        mRef++ ;
      }
    }
    
    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "refining above %g = %3.2f%%.\n",
                valRef, 1.*mRef/mEl ) ;
      hip_err ( info, 1, hip_msg ) ;
      sprintf ( hip_msg, "derefining below %g = %3.2f%%.\n",
                valDeref, 1.*mDeref/mEl ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
  }

  arr_free ( pSensor ) ;
  
  
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElB, &pElE ) )
    for ( pElem = pElB ; pElem <= pElE ; pElem++ )
      if ( pElem->mark )
        /* Either refine or derefine, anyhow change this element. */
        mark_edges ( pElem, x_scalar, kVar, valDeref, valRef ) ;
  

  return ( adapt_uns_hierarchical ( pUns, iso ) ) ;
}
