/*
  uns_surfTri:
  Make or maintain the surface triangulation of a dynamic element.
  
  Last update:
  
  This file contains:
  -------------------
  make_surfTri
  surfTri_mFacets
  surfTri_mVerts
  surfTri_n_edgedVx
  surfTri_diffVx
  surfTri_ngh_facet
  surfTri_nxt_facet
  
  printSt
  printStvx

  surfTri_order_fcOfVx
  surfTri_swap_fcOfVx
  surfTri_update
  surfTri_add_facet
  surfTri_match_facet
  
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;

typedef struct _fcOfVx_s fcOfVx_s ;
typedef struct _facet_s facet_s ;
typedef struct _face_s face_s ;

/* A list of faces formed with each vertex. There can be only three
   facets per face, three faces per vertex? */
# define MAX_FACETS_VERT (3*3)
struct _fcOfVx_s {
  int mFacets ;                      /* Number of facets formed with one vert. */
  int kFacet[MAX_FACETS_VERT] ;      /* The facet in that face as by surfTri. */
  int kVxInFacet[MAX_FACETS_VERT] ;  /* The vert in that facet. */
} ;

/* A list of facets. At most quadrilateral. Can there be at most 8*6 facets? */
#define MAX_FACETS (8*6)
struct _facet_s {
  int mVerts ;
  int kFace ;
  int kVxFacet[4] ;
  int kNghFacet[4] ; /* The neighbors are ccw after the vertex, ie neighbor 0
			is between vertices 0 and 1. */
  int active ;
} ;

/* A list of facets per face. */
struct _face_s {
  int mFacets ;
  int kFacet[MAX_CHILDS_FACE] ;
} ;

struct _surfTri_s {
  int mFacets ;
  int mActiveFacets ;
  facet_s facet[MAX_FACETS] ;
  int mVerts ;
  fcOfVx_s fcOfVx[MAX_REFINED_VERTS] ;
  face_s face[MAX_FACES_ELEM] ;
} ;


static int surfTri_order_fcOfVx ( surfTri_s *PsurfTri, const int kVx );
static void surfTri_swap_fcOfVx ( surfTri_s *PsurfTri, const int kVx,
			   const int iFacet, const int jFacet );
static int surfTri_update ( surfTri_s *PsurfTri,
			    const int kVxElem[], const elType_e elType );
static int surfTri_add_facet ( surfTri_s *PsurfTri, const int kFacet );
static int surfTri_match_facet ( const surfTri_s *PsurfTri, 
			  const int kVx2Match[], const int mVx2Match,
			  int *PkFacet, int *PiFacet );


/* Public parts: */

/******************************************************************************

  make_fcOfVx
  Make an exterior surface polygonization.
  
  Last update:
  ------------
  12Nov19; add doc
  : conceived.
  
  Input:
  ------
  pUns: grid
  pElem: element
  


  Changes To:
  -----------

  Output:
  -----------
  *PmVxHg: the number of hanging vertices.
  kVxHg[]: the positions of the hanging vertices. [mVerts...mVerts+mEdges-1] are on
           edges, [mVerts+mEdges+1 .... mVerts+mEdges+mFaces] are on faces.
  PvxPrt[]: the corner and hanging vertices, listed in order of occurence, 
            that is 0 .. mVxElem-1 for parent, mVxElem .. mVxElem+*PmVxHg-1 for hanging.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

surfTri_s *make_surfTri ( const uns_s *pUns, const elem_struct *Pelem,
			  int *PmVxHg, int kVxHg[], vrtx_struct *PvxPrt[] ) {
  
  const elemType_struct *pElT = elemType + Pelem->elType ;
  int kFace, kFacet, kVx, mVerts, mFacets, iVx, mFctVx, 
    mFacetsFace[MAX_FACES_ELEM+1], 
    mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1] ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;

  static surfTri_s surfTri ;
  fcOfVx_s *const fcOfVx = surfTri.fcOfVx ;
  facet_s *const facet = surfTri.facet ;
  face_s *const face = surfTri.face ;

  /* Find all hanging vertices. */
  get_drvElem_aE ( pUns, Pelem, PmVxHg, kVxHg, PhgVx, fixDiag, diagDir ) ;
  surfTri.mVerts = mVerts = pElT->mVerts + *PmVxHg ;
  /* Get the surface triangulation. */
  get_surfTri_drvElem ( Pelem, *PmVxHg, kVxHg, fixDiag, diagDir,
			mFacetsFace, mFacetVerts, kFacetVx ) ;

  /* List all the parent vertices. */
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
    PvxPrt[kVx] = Pelem->PPvrtx[kVx] ;
  for ( kVx = 0 ; kVx < *PmVxHg ; kVx++ )
    PvxPrt[ kVx+pElT->mVerts ] = PhgVx[kVx] ;

  /* Reset facets, vertex lists. */
  mFacets = 0 ;
  for ( kVx = 0 ; kVx < MAX_REFINED_VERTS ; kVx++ )
    fcOfVx[kVx].mFacets = 0 ;
  
  /* Make an unordered list of facets per vert. */
  for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
    face[kFace].mFacets = 0 ;
    for ( kFacet = 0 ; kFacet < mFacetsFace[kFace] ; kFacet++ ) {
      /* Make a facet. */
#     ifdef CHECK_BOUNDS
        if ( mFacets > MAX_FACETS-2 ) {
          printf ( " FATAL: not enough facets in make_surfTri.\n" ) ;
	  return ( NULL ) ; }
#     endif
      facet[mFacets].mVerts = mFacetVerts[kFace][kFacet] ;
      facet[mFacets].kFace = kFace ;
      face[kFace].kFacet[ face[kFace].mFacets ] = kFacet ;
      face[kFace].mFacets++ ;
      facet[mFacets].active = 1 ;

      /* List all the vertices of this facet. */
      for ( iVx = 0 ; iVx < mFacetVerts[kFace][kFacet] ; iVx++ ) {
        kVx = facet[mFacets].kVxFacet[iVx] = kFacetVx[kFace][kFacet][iVx] ;
	mFctVx = fcOfVx[kVx].mFacets ;
#       ifdef CHECK_BOUNDS
	  if ( mFctVx > MAX_FACETS_VERT - 1 ) {
            printf ( " FATAL: not enough facets per vert in make_surfTri.\n" ) ;
	    return ( NULL ) ; }
#       endif
	fcOfVx[kVx].kFacet[mFctVx] = mFacets ;
	fcOfVx[kVx].kVxInFacet[mFctVx] = iVx ;
	fcOfVx[kVx].mFacets++ ;
      }
      mFacets++ ;
    }
  }
  surfTri.mActiveFacets = surfTri.mFacets = mFacets ;

  /* Order the list of facets per vert. */
  for ( kVx = 0 ; kVx < mVerts ; kVx++ )
    surfTri_order_fcOfVx ( &surfTri, kVx ) ;
  
  /* Make a face connectivity entry. */
  for ( kFacet = 0 ; kFacet < mFacets ; kFacet++ )
    surfTri_ngh_facet ( &surfTri, kFacet ) ;

  return ( &surfTri ) ;
}


/******************************************************************************

  surfTri_mFacets:
  Return the number of active facets.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int surfTri_mFacets ( const surfTri_s *PsurfTri )
{
  if ( PsurfTri )
    return ( PsurfTri->mFacets ) ;
  else      
    return ( 0 ) ;
}


/******************************************************************************

  surfTri_mFacets:
  Return the number of active facets.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int surfTri_mVerts ( const surfTri_s *PsurfTri )
{
  if ( PsurfTri )
    return ( PsurfTri->mVerts ) ;
  else      
    return ( 0 ) ;
}



/******************************************************************************

  surfTri_n_edgedVx:
  Return a vertex that forms three edges/facets.
  
  Last update:
  ------------
  2May19; increment value of *PiVx, not the pointer.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int surfTri_n_edgedVx ( const surfTri_s *PsurfTri, int *PiVx, const int mFacets ) {
  
  const facet_s *facet = PsurfTri->facet ;
  const fcOfVx_s *fcOfVx = PsurfTri->fcOfVx ;
  int iFacet, kFacet, sumFacetVerts = 0 ;

  /* Start the loop from the given iVx. */
  for (  ; *PiVx < PsurfTri->mVerts ; (*PiVx)++ )
    if ( fcOfVx[*PiVx].mFacets == mFacets ) {
      /* Sum the vertices of all formed facets. */
      for ( iFacet = 0 ; iFacet < mFacets ; iFacet++ ) {
        kFacet = fcOfVx[*PiVx].kFacet[iFacet] ;
	sumFacetVerts += facet[kFacet].mVerts ;
      }
      return ( sumFacetVerts ) ;
    }

  return ( -1 ) ;
}

/******************************************************************************

  surfTri_diffVx:
  Find the vertex on a facet that is ccw diff to the one given.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  fcOfVx: The list of facets per vertex,
  kVx:    The center vertex in fcOfVx,
  iFacet: The number of the facet formed with kVx,
  nDiff:  The difference in vertex positions in that facet that is
          sought.

  Returns:
  --------
  kVxDiff: the vertex number that is ccw nDiff away in iFacet.
  
*/

int surfTri_diffVx ( const surfTri_s *PsurfTri, const int kVx,
		     const int iFacet, const int nDiff )
{
  const fcOfVx_s *PfcOfVx = PsurfTri->fcOfVx + kVx ;
  int kFacet = PfcOfVx->kFacet[iFacet], 
      mVertsFacet = PsurfTri->facet[kFacet].mVerts,
      kVxInFacet = PfcOfVx->kVxInFacet[iFacet], kVxDiff ;
  const int *kVxFacet = PsurfTri->facet[kFacet].kVxFacet ;

  kVxDiff = kVxFacet[ ( kVxInFacet+mVertsFacet+nDiff )%mVertsFacet ] ;
	    
  return ( kVxDiff ) ;
}


/******************************************************************************

  surfTri_ngh_facet:
  Given a facet, update the connectivity of neighboring facets.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int surfTri_ngh_facet ( surfTri_s *PsurfTri, int kFacet )
{
  facet_s *facet = PsurfTri->facet, *Pfacet = facet+kFacet, *PnghFacet ;
  static const fcOfVx_s *PfcOfVx ;
  static int iVx, kVx, kVxInNghFacet, iFacet, iNghFacet, kNghFacet ;

  /* Make a face connectivity entry. */
  for ( iVx = 0 ; iVx < Pfacet->mVerts ; iVx++ ) {
    kVx = Pfacet->kVxFacet[iVx] ;
    PfcOfVx = PsurfTri->fcOfVx + kVx ;
    
    /* Find kFacet among the facets of kVx. */
    for ( iFacet = 0 ; iFacet < PfcOfVx->mFacets ; iFacet++ )
      if ( PfcOfVx->kFacet[iFacet] == kFacet )
	break ;

    /* Fix the neighboring entries between iFacet and iFacet+1, that is
       across the edge of iVx and iVx-1 in kFacet.
       Note that the neighbors are ccw after the vertex, ie neighbor 0
       is between vertices 0 and 1. */
    iNghFacet = (iFacet+1)%PfcOfVx->mFacets ;
    kNghFacet = PfcOfVx->kFacet[iNghFacet] ;
    PnghFacet = PsurfTri->facet + kNghFacet ;
    kVxInNghFacet = PfcOfVx->kVxInFacet[iNghFacet] ;
    
    Pfacet->kNghFacet[ ( iVx+Pfacet->mVerts-1)%Pfacet->mVerts ] = kNghFacet ;
    PnghFacet->kNghFacet[ kVxInNghFacet ] = kFacet ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  surfTri_nxt_facet:
  Step through the list of facets, returning one at a time.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  PsurfTri
  PkFacet:   the facet to read.


  Changes To:
  -----------
  PkFacet:      Incremented.
  PkFace:       The face this facet is on.
  PmVertsFacet: The number of verts on this facet.
  kVxFacet:     The list of vertices forming this facet.
  
  Returns:
  --------
  0 if there are no more facets, 1 otherwise.
  
*/

int surfTri_nxt_facet ( const surfTri_s *PsurfTri, int *PkFacet,
		        int *PkFace, int *PmVertsFacet, int kVxFacet[] )
{
  int iVx ;
  const facet_s *facet = PsurfTri->facet+(*PkFacet) ;

  if ( ++(*PkFacet) > PsurfTri->mFacets )
    return ( 0 ) ;

  *PkFace = facet->kFace ;
  *PmVertsFacet = facet->mVerts ;
  for ( iVx = 0 ; iVx < facet->mVerts ; iVx++ )
    kVxFacet[iVx] = facet->kVxFacet[iVx] ;

  return ( 1 ) ;
}


/***************************************************************************
  Debugging utilities.
  */

#ifdef DEBUG

void printSt ( const surfTri_s *PsurfTri )
{
  const facet_s *Pfct ;
  const fcOfVx_s *Pfvx ;
  int kFacet, iVx, iFct ;

  printf ( " %d facets:\n", PsurfTri->mFacets ) ;
  for ( kFacet = 0 ; kFacet < PsurfTri->mFacets ; kFacet++ ) {
    Pfct = PsurfTri->facet + kFacet ;
    printf ( "  facet %3d, face %2d, kVx:", kFacet, Pfct->kFace ) ;
    for ( iVx = 0 ; iVx < Pfct->mVerts ; iVx++ )
      printf ( " %2d,", Pfct->kVxFacet[iVx] ) ;
    if ( Pfct->mVerts == 3 )
      printf ( "    " ) ;
    printf ( " kNgh:" ) ;
    for ( iVx = 0 ; iVx < Pfct->mVerts ; iVx++ )
      printf ( " %d,", Pfct->kNghFacet[iVx] ) ;
    printf ( "\n" ) ;
  }

  printf ( "\n  verts: %d\n", PsurfTri->mVerts ) ;
  for ( iVx = 0 ; iVx < PsurfTri->mVerts ; iVx++ ) {
    Pfvx = PsurfTri->fcOfVx + iVx ;
    printf ( " vert %3d, facets:", iVx ) ;
    for ( iFct = 0 ; iFct < Pfvx->mFacets ; iFct++ )
      printf ( " %2d,", Pfvx->kFacet[iFct] ) ;
    for ( iFct = Pfvx->mFacets ; iFct < 6 ; iFct++ )
      printf ( "    " ) ;
    printf ( " vxInFacet:" ) ;
    for ( iFct = 0 ; iFct < Pfvx->mFacets ; iFct++ )
      printf ( " %2d,", Pfvx->kVxInFacet[iFct] ) ;
    printf ( "\n" ) ;
  }

  return ;
}

void printStvx ( const surfTri_s *PsurfTri, const vrtx_struct *PprtVx[] )
{
  const facet_s *Pfct ;
  const fcOfVx_s *Pfvx ;
  int kFacet, iVx, iFct ;

  printf ( " %d facets:\n", PsurfTri->mFacets ) ;
  for ( kFacet = 0 ; kFacet < PsurfTri->mFacets ; kFacet++ ) {
    Pfct = PsurfTri->facet + kFacet ;
    printf ( "  facet %3d, face %2d, kVx:", kFacet, Pfct->kFace ) ;
    for ( iVx = 0 ; iVx < Pfct->mVerts ; iVx++ )
      printf ( " %2d,", Pfct->kVxFacet[iVx] ) ;
    if ( Pfct->mVerts == 3 )
      printf ( "    " ) ;
    printf ( " kNgh:" ) ;
    for ( iVx = 0 ; iVx < Pfct->mVerts ; iVx++ )
      printf ( " %d,", Pfct->kNghFacet[iVx] ) ;
    printf ( "\n" ) ;
  }

  printf ( "\n  verts: %d\n", PsurfTri->mVerts ) ;
  for ( iVx = 0 ; iVx < PsurfTri->mVerts ; iVx++ ) {
    Pfvx = PsurfTri->fcOfVx + iVx ;
    printf ( " vert %3d, nr %7d, facets:", iVx, PprtVx[iVx]->number ) ;
    for ( iFct = 0 ; iFct < Pfvx->mFacets ; iFct++ )
      printf ( " %2d,", Pfvx->kFacet[iFct] ) ;
    for ( iFct = Pfvx->mFacets ; iFct < 6 ; iFct++ )
      printf ( "    " ) ;
    printf ( " vxInFacet:" ) ;
    for ( iFct = 0 ; iFct < Pfvx->mFacets ; iFct++ )
      printf ( " %2d,", Pfvx->kVxInFacet[iFct] ) ;
    printf ( "\n" ) ;
  }

  return ;
}

#endif

/* Private parts. */

/******************************************************************************

  surfTri_order_fcOfVx:
  Order a list of facets for one vertex.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int surfTri_order_fcOfVx ( surfTri_s *PsurfTri, const int kVx )
{
  fcOfVx_s *PfcOfVx = PsurfTri->fcOfVx+kVx ;
  int iFacet, kVxPrv, nxtFacet, kVxNxt ;
  
  for ( iFacet = 0 ; iFacet < PfcOfVx->mFacets-1 ; iFacet++ ) {
    /* Find the backward edge of this vertex in this facet.  */
    kVxPrv = surfTri_diffVx( PsurfTri, kVx, iFacet, -1 ) ;
    
    /* Loop over all remaining facets to find the one that shares
       both vertices. */
    for ( nxtFacet = iFacet+1 ; nxtFacet < PfcOfVx->mFacets ; nxtFacet++ ) {
      /* Find the forward edge of this vertex in this facet.  */
      kVxNxt = surfTri_diffVx( PsurfTri, kVx, nxtFacet, 1 ) ;
      
      if ( kVxNxt == kVxPrv ) {
        /* This is the match. */
	if ( nxtFacet - iFacet - 1 )
	  /* Switch iFacet and nxtFacet */
	  surfTri_swap_fcOfVx ( PsurfTri, kVx, iFacet+1, nxtFacet ) ;
	break ;
      }
    }
    if ( nxtFacet == PfcOfVx->mFacets ) {
      printf ( " FATAL: no match edge around %d to %d in surfTri_order_fcOfVx.\n",
	       kVx, kVxPrv ) ;
      return ( 0 ) ;
    }
  }

  return ( 1 ) ;
}

/******************************************************************************

  surfTri_swap_fcOfVx:
  Swap two facets of a vertex.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void surfTri_swap_fcOfVx ( surfTri_s *PsurfTri, const int kVx,
				  const int iFacet, const int jFacet )
{
  int kFacet, kVxInFacet ;
  fcOfVx_s *PfcOfVx = PsurfTri->fcOfVx+kVx ;
  
  kFacet                      = PfcOfVx->kFacet[jFacet] ;
  kVxInFacet                  = PfcOfVx->kVxInFacet[jFacet] ;
  
  PfcOfVx->kFacet[jFacet]     = PfcOfVx->kFacet[iFacet] ;
  PfcOfVx->kVxInFacet[jFacet] = PfcOfVx->kVxInFacet[iFacet] ;
  
  PfcOfVx->kFacet[iFacet]     = kFacet ;
  PfcOfVx->kVxInFacet[iFacet] = kVxInFacet ;

  return ;
}


/******************************************************************************

  surfTri_update:
  Mark all facets that have become inactive on the outside of an element, form
  the new inside face (there can only be one?), update the connectivity.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  facet, fcOfVx, PmFacets: the surface triangulation.
  kVxElem: the numbers of the forming vertices of the element that was stripped away
           int the list of the original element to be buffered.
  elType: the type of element that was stripped away.

  Changes To:
  -----------
  facet, fcOfVx, PmFacets: the surface triangulation.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int surfTri_update ( surfTri_s *PsurfTri,
			    const int kVxElem[], const elType_e elType ) {

  facet_s *facet = PsurfTri->facet ;
  fcOfVx_s *fcOfVx = PsurfTri->fcOfVx ;
  const elemType_struct *pElT = elemType + elType ;
  const faceOfElem_struct *pFoE ;
  int *kVxFace, kVx, iVx, kFace, mVerts = PsurfTri->mVerts, mFacets = PsurfTri->mFacets,
      mNewFacets = 0, kFacet, iFacet, change ;

  /* Loop over all faces in the element. */
  for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
    pFoE = pElT->faceOfElem + kFace ;

#   ifdef CHECK_BOUNDS
      /* Note that kVxFace is stored as the kVxFacets of the next facet. */
      if ( PsurfTri->mFacets > MAX_FACETS-2 ) {
        printf ( " FATAL: need more facets in surfTri_update.\n" ) ;
	return ( 0 ) ; }
#   endif
    
    /* Extract all its vertices. */
    kVxFace = facet[mFacets+1].kVxFacet ;
    for ( iVx = 0 ; iVx < pFoE->mVertsFace ; iVx++ )
      kVxFace[iVx] = kVxElem[ pFoE->kVxFace[iVx] ] ;

    /* Find the face in facet via fcOfVx. */
    if ( surfTri_match_facet ( PsurfTri, kVxFace, pFoE->mVertsFace,
			       &kFacet, &iFacet) ) {
      /* Match. Remove this face. */
      facet[kFacet].active = 0 ;
    }
    else {
      /* No match, Make this face of the element as a facet. */
      mFacets = (PsurfTri->mFacets)++ ; mNewFacets++ ;
      facet[mFacets].mVerts = pFoE->mVertsFace ;
      /* Mark interior faces as 0. Do we really use this? */
      facet[mFacets].kFace = 0 ;
      facet[mFacets].active = 1 ;
      /* Add it to the vertices' list. */
      surfTri_add_facet ( PsurfTri, mFacets ) ;
      surfTri_ngh_facet ( PsurfTri, mFacets ) ;
    }
  }

  /* Loop over all facets of all vertices and fill empty slots from the end. */
  for ( kVx = 0 ; kVx < mVerts ; kVx++ ) {
    for ( change = iFacet = 0 ; iFacet < fcOfVx[kVx].mFacets ; iFacet++ )
      if ( !facet[ fcOfVx[kVx].kFacet[iFacet] ].active ) {
        surfTri_swap_fcOfVx ( PsurfTri, kVx, iFacet--, fcOfVx[kVx].mFacets-- ) ;
	change = 1 ;
      }
    if ( change && fcOfVx[kVx].mFacets )
      /* There has been a change in the list. Reorder. */
      surfTri_order_fcOfVx ( PsurfTri, kVx ) ;
  }

  if ( mNewFacets > 1 ) {
    printf ( " FATAL: trying to add too many new facets with one child"
	     " surfTri_update.\n" ) ;
    return ( 0 ) ; }
  else
    return ( 1 ) ;
}


/******************************************************************************

  surfTri_add_facet:
  Add a facet to all of its forming vertices.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int surfTri_add_facet ( surfTri_s *PsurfTri, const int kFacet )
{
  facet_s *Pfacet = PsurfTri->facet+kFacet ;
  fcOfVx_s *PfcOfVx ;
  int iVx, kVx, mFc ;

  for ( iVx = 0 ; iVx < Pfacet->mVerts ; iVx++ ) {
    kVx = Pfacet->kVxFacet[iVx] ;
    PfcOfVx = PsurfTri->fcOfVx + kVx ;
    mFc = PfcOfVx->mFacets ;
#   ifdef CHECK_BOUNDS
      if ( PfcOfVx->mFacets >= MAX_FACETS_VERT ) {
        printf ( " FATAL: too many facets in surfTri_add_facet.\n" ) ;
        return ( 0 ) ; }
#   endif
    
    PfcOfVx->kFacet[mFc] = kFacet ;
    PfcOfVx->kVxInFacet[mFc] = iVx ;
    ++PfcOfVx->mFacets ;
  }
  return ( 1 ) ;
}

/******************************************************************************

  surfTri_match_facet:
  Check a facet for a match with a list of vertices.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  facet, fcOfVx: the surface triangulation.
  kVx:   vertex in a facet to be matched, entry to fcOfVx,
  kVx2Match: a list of vertices to match.
  mVx2Match: # of vx to match.

  Changes To:
  -----------
  *PkFacet: The matching facet in facet
  *PiFacet: The matching facet in fcOfVx[kVx].

  
  Returns:
  --------
  0 on miss, 1 on match.
  
*/

static int surfTri_match_facet ( const surfTri_s *PsurfTri, 
				 const int kVx2Match[], const int mVx2Match,
				 int *PkFacet, int *PiFacet )
{
  const facet_s *facet = PsurfTri->facet, *Pfacet ;
  const fcOfVx_s *PfcOfVx = PsurfTri->fcOfVx + kVx2Match[0] ;
  int kVxInFacet, iVx, match, iFacet, kFacet ;

  /* Loop over all facets formed with kVx2Match[0]. */
  for ( iFacet = 0 ; iFacet < PfcOfVx->mFacets ; iFacet++ ) {
    kFacet = PfcOfVx->kFacet[iFacet] ;
    Pfacet = facet + (kFacet) ;

    if ( Pfacet->mVerts == mVx2Match ) {
      kVxInFacet = PfcOfVx->kVxInFacet[iFacet] ;

      /* Loop over all vertices in this facet. */
      for ( match = 1, iVx = 0 ; iVx < mVx2Match ; iVx++ )
	if ( Pfacet->kVxFacet[ (iVx+kVxInFacet)%mVx2Match ] != kVx2Match[iVx] )	{
          match = 0 ;
	  break ;
	}
      
      if ( match ) {
        *PiFacet = iFacet ;
	*PkFacet = kFacet ;
	return ( 1 ) ;
      }
    }
  }
  return ( 0 ) ;
}
