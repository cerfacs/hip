/*
   write_n3snatur.c
   Write an unstructured, multichunked grid as single block into an
   n3s format. Tet only.
 
   Last update:
   ------------
   3Jul10; edit redundant FATAL out of fatal msg. 
   15Oct07; intro n3s_write_sol.
   1Apr05; reverse boundary face orientation in 3D as compared to AVBP.
   10Jan05; extend to hybrid elements.
   5Jan05; modify conn. table back to AVBP convention for 2D.
   8Jul04; modify conn. table for tets in write_conn.
   5Feb04; conceived.

   Contains:
   ---------
   write_n3s:
   n3s_write_conn:
   n3s_write_coor:
   n3s_write_sol:
   
*/

#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const char version[] ;
extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const int perBc_in_exBound ;
extern const Grids_struct Grids ;

/* Specified in read_n3s. */
extern const int av2n3s[MAX_ELEM_TYPES][MAX_VX_ELEM] ;

static void bwrite_1int ( FILE* file, int intVal ) {

  int recLen = sizeof ( int ) ;

  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;
  FWRITE ( &intVal,  sizeof ( int ), 1,    file ) ;
  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;

  return ;
}
static void bwrite_int ( FILE* file, int mInt, int *intVal ) {

  int recLen = mInt*sizeof ( int ) ;

  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;
  FWRITE ( intVal,  sizeof ( int ), mInt, file ) ;
  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;

  return ;
}
static void bwrite_1dbl ( FILE* file, double dblVal ) {

  int recLen = sizeof ( double ) ;

  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;
  FWRITE ( &dblVal,  sizeof ( double ), 1, file ) ;
  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;

  return ;
}
static void bwrite_dbl ( FILE* file, int mDbl, double *dblVal ) {

  int recLen = mDbl*sizeof ( double ) ;

  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;
  FWRITE ( dblVal,  sizeof ( double ), mDbl, file ) ;
  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;

  return ;
}
static void bwrite_char ( FILE* file, int mChar, char *pString ) {

  int recLen = mChar*sizeof ( char ) ;

  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;
  FWRITE ( pString,  sizeof ( char ), mChar, file ) ;
  FWRITE ( &recLen, sizeof ( int ), 1,    file ) ;

  return ;
}

/****************************************************************

  n3s_write_shdr:
  Header for geom file.

  Last update:
  5Feb04: conceived..

  Input:
  ------
  pUns:
  file:
 
  Changes to:
  -----------

*/

static int n3s_write_ghdr ( uns_s *pUns, FILE *file ) {

  char stringH[] = "CRUBRIQUE=EN-TETE      " ;
  int iVal[2] ;

  if ( verbosity > 2 )
    printf ( "      header\n" ) ;

  /* Binary indicator 1 */
  bwrite_1int ( file, 1 ) ;

  bwrite_char ( file, 22, stringH ) ;

  /* NDIM */
  bwrite_1int ( file, pUns->mDim ) ;
  /* NTYP: */
  bwrite_1int ( file, 1 ) ;
  /* NUMNP1, NUMNP2 */
  iVal[0] = iVal[1] = pUns->mVertsNumbered ;
  bwrite_int ( file, 2, iVal ) ;
  /* NELEM */
  bwrite_1int ( file, pUns->mElemsNumbered ) ;
  /* NELEMB */
  bwrite_1int ( file, pUns->mFaceAllBc ) ;


  /* IPR_TAB_SURF */
  bwrite_1int ( file, 0 ) ;
  /* IPR_TAB_LIN */
  bwrite_1int ( file, 0 ) ;


  /* NB_RUB_OPT Rubriques optionelles, e.g. perodicite, couleur. */
  bwrite_1int ( file, 0 ) ;
  /* IPR_RUB_PERIO: presence de rubrique periodique. */
  bwrite_1int ( file, 0 ) ;

  /* NB_EL_SURF */
  bwrite_1int ( file, 0 ) ;
  /* NB_EL_LIN */
  bwrite_1int ( file, 0 ) ;
  /* NB_ENR_RUB */
  iVal[0] = pUns->mBc ;
  iVal[1] = 0 ;
  bwrite_int ( file, 2, iVal ) ;
  /* NB_ENR_PER */
  bwrite_1int ( file, 0 ) ;

  return ( 1 ) ;
}
/******************************************************************************

  n3s_write_coor:
  Write vertex coordinates, hybrid elements.
  
  Last update:
  ------------
  1Apr05; or therabouts, conceived.
  
  Input:
  ------
  pUns
  file

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int n3s_write_coor ( uns_s *pUns, FILE *file ) {

  char stringN[] = "CRUBRIQUE=NOEUDS      " ;
  int nBeg, nEnd ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;

  bwrite_char ( file, 22, stringN ) ;

  /* One stream of coordinates,  x,y,z triplets. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number )
        bwrite_dbl ( file, pUns->mDim, pVrtx->Pcoor ) ;

  return ( 1 ) ;
}

/******************************************************************************

  n3s_write_conn:
  Write element connectivity, hybrid elements.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
          make all large counters ulong_t
  10Dec07; change definition of prism av2n3s.
  1Apr05; fix bugs in writing out.
  10Jan05; extend to hybrid elements
  14Sep04: reverse orientation of the elements.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int n3s_write_conn ( uns_s *pUns, FILE *file ) {

  const int typeCode[] = {332,442,443,553,653,863,000} ;
  const elemType_struct *pElT ;

  char stringE[] = "CRUBRIQUE=ELEMENT V     " ;
  int iVal[MAX_VX_ELEM+1], kVx ;
  ulong_t mVx ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  vrtx_struct **ppVx ;

  bwrite_char ( file, 22, stringE ) ;

  /* One record for each element. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        ppVx = pElem->PPvrtx ;
        pElT = elemType + pElem->elType ;
        mVx = pElT->mVerts ;
        
        iVal[0] = typeCode[pElem->elType] ;
        for ( kVx = 0 ; kVx < mVx ; kVx++ )
          iVal[ av2n3s[ pElem->elType][kVx]+1 ] = ppVx[ kVx ]->number ;

        bwrite_int ( file, mVx+1, iVal ) ; 
      }

  return ( 1 ) ;
}

/******************************************************************************

  n3s_write_bnd:
  Write boundary faces.
  
  Last update:
  ------------
  10Dec07; fix bug and lengthen iVal to iVal[6].
  1Apr05; reverse orientation of bnd fc as compared to AVBP.
  14Sep04: reverse orientation of the boundary faces.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int n3s_write_bnd ( uns_s *pUns, FILE *file ) {

  const int mDim = pUns->mDim ;
  const int typeCode = ( mDim == 2 ? 332 : 443 ) ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  const elem_struct *pElem ;
  vrtx_struct **ppVx ;

  char stringE[] = "CRUBRIQUE=ELEMENT B     ", stringC[] = "CRUBRIQUE=COULEUR     " ;
  int iVal[6], kVx, nBc, mFc, nFc, mFcW, mVxFc ;
  bndPatch_struct *pBndPatchBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;


  bwrite_char ( file, 22, stringE ) ;

  /* One record for each face. Linears and Triangles only. */
  for ( nBc = 0 ; pBndPatchBc = NULL, nBc < pUns->mBc ; nBc ++ )
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatchBc, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
          pElem = pBndFc->Pelem ;
          pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
          mVxFc = pFoE->mVertsFace ;
          kVxFc = pFoE->kVxFace ;
          ppVx = pElem->PPvrtx ;


          iVal[0] = pElem->number ;
          if ( mDim == 2 ) {
            iVal[1] = 221 ;
            iVal[2] =  ppVx[ pFoE->kVxFace[0] ]->number ;
            iVal[3] =  ppVx[ pFoE->kVxFace[1] ]->number ;
            bwrite_int ( file, 4, iVal ) ; 
          }
          else {
            iVal[1] = ( mVxFc == 3 ? 332 : 442 ) ;
            
            /* List faces with the other sens. */
            for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
              iVal[kVx+2] = ppVx[ pFoE->kVxFace[mVxFc-kVx-1] ]->number ;
            bwrite_int ( file, mVxFc+2, iVal ) ; 
          }
        }



  /* Then the 'colour' info indicating bcNr. */
  bwrite_char ( file, 22, stringC ) ;


  /* One record for each colour. */
  mFcW = 0 ;
  for ( nBc = 0 ; pBndPatchBc = NULL, nBc < pUns->mBc ; nBc ++ ) {
    mFc = ( mDim == 2 ? pUns->pmBiBc[nBc] : pUns->pmFaceBc[nBc] ) ;

    /* Length of the record: 1int+12char+2int. */
    iVal[0] = 3*sizeof(int) + 12 ;
    FWRITE ( iVal, sizeof( int ), 1, file ) ;

    /* Number of the sub-rubrique: nBc. */
    iVal[0] = nBc+1 ;
    FWRITE ( iVal, sizeof( int ), 1, file ) ;

    /* Label. */
    FWRITE ( pUns->ppBc[nBc]->text, sizeof( char), 12, file ) ;

    /* Colours refer to faces, id 3. */
    iVal[0] = 3 ;
    /* How many. */
    iVal[1] = mFc ;
    FWRITE ( iVal, sizeof( int ), 2, file ) ;

    /* Closing record length. */
    iVal[0] = 3*sizeof(int) + 12 ;
    FWRITE ( iVal, sizeof( int ), 1, file ) ;



    /* Length of the record of colours. */
    iVal[0] = mFc*sizeof ( int ) ;
    FWRITE ( iVal, sizeof( int ), 1, file ) ;

    /* List the face numbers. Easy, since they have been written in order. */
    for ( nFc = mFcW+1 ; nFc <= mFcW+mFc ; nFc++ )
      FWRITE ( &nFc, sizeof( int ), 1, file ) ;

    /* Closing record length. */
    FWRITE ( iVal, sizeof( int ), 1, file ) ;

    mFcW += mFc ;
  }

  

  return ( 1 ) ;
}


/****************************************************************

  n3s_write_sol:
  Write solution to n3s solution file. Navier-Stokes only, so far.

  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  11Dec07; write doubles rather than floats.
           fix organisation of records.
           switch from rho to using T as req. variable
  15Oct07; conceived.

  Input:
  ------
  pUns:
  file:
 
  Changes to:
  -----------

*/

static int n3s_write_sol ( uns_s *pUns, FILE *fl ) {

  int iBuf[101] = {0} ;
  double rBuf[101] = {0.} ;
  int i ;
  char cBuf[14] = "             " ;
  int iPresence[100]={0}, iType[100]={0}, iVarDim[100]={0}, iVarLen[100]={0} ;
  char varName[100][12] ;
  const varList_s *pVL = &(pUns->varList) ;
  const int mVerts = pUns->mVertsNumbered ;
  const int mElems = pUns->mElemsNumbered ;
  const int mDim = pUns->mDim ;
  int nVar, kVar ;
  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  

  conv_uns_var ( pUns, primT ) ;

  if ( verbosity > 2 )
    printf ( "      header\n" ) ;

  /*
  write (ISUITE) TITRE, TIME,Crank,Cfl,&
       isol,jdefor,nstart,&
       numnp1,nelem,idim,nb_var,NB_VAR_POST,&
       (RBIDON(I), I=1,100),(IBIDON(I), I=1,100),(CBIDON(I), I=1,100)
  */

  /* Hardwire the length. */
  iBuf[0] = 2536 ;   FWRITE ( iBuf, sizeof( int ), 1, fl ) ;        /* Record length */
                     FWRITE ( &pUns->pGrid->uns.name, sizeof( char ), 80, fl); /* Title */
  rBuf[0] = 0. ;     FWRITE ( rBuf, sizeof( double ), 1, fl);        /* Time */
                     FWRITE ( rBuf, sizeof( double ), 1, fl);        /* Crank */
                     FWRITE ( rBuf, sizeof( double ), 1, fl);        /* CFL */
  iBuf[0] = 0 ;      FWRITE ( iBuf, sizeof( int ), 1, fl);          /* Isol */
                     FWRITE ( rBuf, sizeof( int ), 1, fl);          /* JDefor */
                     FWRITE ( rBuf, sizeof( int ), 1, fl);          /* nStart */
                     FWRITE ( &mVerts, sizeof( int ), 1, fl);       /* numnp1 */
                     FWRITE ( &mElems, sizeof( int ), 1, fl);       /* nelem */
                     FWRITE ( &mDim, sizeof( int ), 1, fl);         /* nDim */
  iBuf[0] = mDim+2 ; FWRITE ( iBuf, sizeof( int ), 1, fl);          /* nb_var */
  iBuf[0] = 0 ;      FWRITE ( iBuf, sizeof( int ), 1, fl);          /* nb_var_post */

                     FWRITE ( iBuf, sizeof( int ), 100, fl);        /* iBidon */
                     FWRITE ( rBuf, sizeof( double ), 100, fl);      /* rBidon */
  for ( i = 0 ; i < 100 ; i++ )
                     FWRITE ( cBuf, sizeof( char ), 12, fl);        /* cBidon */

  iBuf[0] = 2536 ;   FWRITE ( iBuf, sizeof( int ), 1, fl);          /* Record length */

  /*
  write (ISUITE)&
       (TAB(I,1), I=1,100),(TAB(I,2), I=1,100),(TAB(I,3), I=1,100),(TAB(I,4), I=1,100),&
       (NOMVAR(I), I=1,100)
  */

  /* Initialise the header tables. */

  /* Navier-Stokes. */
  for ( kVar = 0 ; kVar < mDim+2 ; kVar++ ) {
    iPresence[kVar] = 1 ;
    iType[kVar] = 1 ;
    iVarDim[kVar] = 1 ;
    iVarLen[kVar] = mVerts ;
    strncpy ( varName[kVar], pVL->var[kVar].name, 12 ) ;
  }


  /* Write the tables to file, single record */
  iBuf[0] = 4*100*sizeof(int) + 12*100*sizeof(char) ;
  FWRITE ( iBuf, sizeof(int), 1, fl);

  FWRITE ( iPresence,  sizeof( int ),  100, fl ) ;
  FWRITE ( iType,      sizeof( int ),  100, fl ) ;
  FWRITE ( iVarDim,    sizeof( int ),  100, fl ) ;
  FWRITE ( iVarLen,    sizeof( int ),  100, fl ) ;
  fwrite ( varName, 12*sizeof( char ), 100, fl ) ;

  FWRITE ( iBuf, sizeof(int), 1, fl);


  /* Write the N-S solution. */
  for ( kVar = 0 ; kVar < mDim+2 ; kVar++ ) {
    iBuf[0] = mVerts*sizeof( double ) ;
    FWRITE ( iBuf, sizeof(int), 1, fl);

    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ ) {
        FWRITE ( pVx->Punknown+kVar, sizeof( double ), 1, fl);
      } 
    FWRITE ( iBuf, sizeof(int), 1, fl);
  }
  

  conv_uns_var ( pUns, cons ) ;
  return ( 1 ) ;
}



/****************************************************************

  n3s_write_tail:
  Tail.

  Last update:
  20Apr04: conceived..

  Input:
  ------
  pUns:
  file:
 
  Changes to:
  -----------

*/

static void n3s_write_tail ( FILE *file ) {

  char stringH[] = "CRUBRIQUE=FIN              " ;

  if ( verbosity > 2 )
    printf ( "      done\n" ) ;

  bwrite_char ( file, 22, stringH ) ;

  return ;
}

/******************************************************************************

  write_n3s:
  Write a hybrid mesh to n3s format.
  
  Last update:
  ------------
  18dec24; move fclose(flGeom) after last use of flGeom
  22Apr04; learn more about the format.
  5Feb04: conceived.
  
  Input:
  ------
  pFlGeom: the geom file to write
  
  Returns:
  --------
  1
  
*/

int write_n3s ( char *pFlGeom, char *pFlSol ) {
  
  FILE *flGeom = NULL, *flSol = NULL ;
  uns_s *pUns ;

  if ( Grids.PcurrentGrid->uns.type != uns ) {
    sprintf ( hip_msg, "you can only write unstructured grids to n3s.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  pUns = Grids.PcurrentGrid->uns.pUns ;


  if ( !pUns->validGrid ) {
    sprintf ( hip_msg, "you were told that this grid is invalid, weren't you?.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  else if ( (check_bnd_setup ( pUns )).status != success ) {
    sprintf ( hip_msg, "cannot write grid without proper boundary setup.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  



  prepend_path ( pFlGeom ) ;
  if ( verbosity > 0 ) 
    printf ( "   Writing finest grid to n3snatur as %s\n", pFlGeom ) ;

  if ( ( flGeom = fopen ( pFlGeom, "w" ) ) == NULL ) {
    sprintf ( hip_msg, "grid file: %s could not be opened in write_n3s.c.\n",  pFlGeom ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  
  
  /* Number the leaf elements only. Force a recount */
  pUns->numberedType = invNum ;
  number_uns_elem_leafs ( pUns ) ;

  if ( !special_verts ( pUns ) ) {
    sprintf ( hip_msg, "failed to match periodic vertices in write_n3s.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
     for the current numbering of elements. */
  count_uns_bndFaces ( pUns ) ;
  /* Remove all periodic bc, if they are not supposed to be listed. */
  if ( !perBc_in_exBound ) rm_perBc ( pUns ) ;

  /* Make pairs of periodic nodes and faces. */
  if ( ! match_per_faces ( pUns ) ) {
    sprintf ( hip_msg, "failed to establish periodicity in write_n3s.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  n3s_write_ghdr   ( pUns, flGeom ) ;
  n3s_write_coor  ( pUns, flGeom ) ;
  n3s_write_conn  ( pUns, flGeom ) ;
  n3s_write_bnd   ( pUns, flGeom ) ;

  n3s_write_tail   ( flGeom ) ;






  if ( pFlSol[0] != '\0' ) {
    prepend_path ( pFlSol ) ;

    if ( ( flSol = fopen ( pFlSol, "w" ) ) == NULL ) {
      sprintf ( hip_msg, 
      "sol file: %s could not be opened in write_n3s.c.\n",  pFlSol ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  }
  

  if ( pUns->varList.varType != noVar && flSol ) {
    n3s_write_sol ( pUns, flSol ) ;
    n3s_write_tail ( flGeom ) ;
    fclose ( flSol ) ;
  }

  /* Clean up periodic setup. */
  pUns->mPerVxPairs = 0 ;
  arr_free ( pUns->pPerVxPair ) ;
  pUns->pPerVxPair = NULL ;

  /* Turn periodic boundaries back on. */
  count_uns_bndFaces ( pUns ) ;
  fclose ( flGeom ) ;


  return ( 1 ) ;
}

