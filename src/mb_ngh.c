/*
   mb_ngh.c
   Structured grid neighbor operations.
 
   Last update:
   ------------

   Contains:
   ---------
   get_mb_nghCell:
   get_mb_nghNode:
   find_mb_subface_cell:
   find_mb_subface_node:
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

extern int verbosity ;
extern Grids_struct Grids ;


/******************************************************************************

  get_mb_ngh{Cell,Node}:
  Find the next cell,node in direction dir for a given index ijk
  possibly across multi-block interfaces.
  
  Last update:
  ------------
  : derived from get_mb_nghCell.
  
  Input:
  ------
  PPbl: pointer to the current block,
  ijk:  current indices,
  dir:  direction of the neighbor given as +/-1 for only one coordinate,
  mDim.

  Changes To:
  -----------
  *PPbl
  ijk[],
  dir[].
  
  Returns:
  --------
  0 if there is no neighbor, ie. boundary,
  1 on finding the next cell/node,
  2 on hitting a degenerate subface. 
  
*/


#if defined (NODE)
#  define SHIFTOP vertShift
#  define OFFSET 0
  int get_mb_ngh_node ( block_struct **PPbl, int ijk[], int dir[],
	  	        const int mDim )
#elif defined (CELL)
#  define SHIFTOP elemShift
#  define OFFSET 1
  int get_mb_ngh_cell ( block_struct **PPbl, int ijk[], int dir[],
		        const int mDim )
#endif
{
  static int ijkO[MAX_DIM], ijkDir[MAX_DIM], nDim, runningDim, runningDir ;
  static subFace_struct *Psf ;
  static block_struct *Pbl ;
  Pbl = *PPbl ;

  /* Find the running index. */
  for ( runningDim = 0 ; runningDim < mDim ; runningDim++ ) 
    if ( dir[runningDim] )
      break ;
  runningDir = dir[runningDim] ;

  if ( ( runningDir == 1 && ijk[runningDim] >= Pbl->mVert[runningDim]-OFFSET ) ||
       ( runningDir == -1  &&  ijk[runningDim] <= 1 ) ) {
    /* Hit the limit. Find the subface for this cell. */
#   if defined (NODE)
      if ( !find_mb_subFc_node ( Pbl, ijk, mDim, runningDim, runningDir, &Psf ) ) {
        printf ( " FATAL: could not find the containing subface in"
		 " get_mb_nghNode.\n" ) ;
	return ( 0 ) ; }
#   elif defined (CELL)
      if ( !find_mb_subFc_cell ( Pbl, ijk, mDim, runningDim, runningDir, &Psf ) ) {
        printf ( " FATAL: could not find the containing subface in"
		 " get_mb_nghCell.\n" ) ;
	return ( 0 ) ; }
#   endif

    if ( !( Psf->PrBlock || Psf->Pbc ) )
      /* This is a degenerate subface. */
      return ( 2 ) ;
    else if ( Psf->Pbc )
      /* This is a boundary. No neighbor. */
      return ( 0 ) ;
    else if ( Psf->PlBlock == Pbl ) {
      /* The other block is on the right. */
      trans_l2r ( ijk, Psf->ProtL2R->rotMatrix, Psf->SHIFTOP, ijkO ) ;
      /* Transform the directions. */
      ijk[runningDim] += dir[runningDim] ;
      trans_l2r ( ijk, Psf->ProtL2R->rotMatrix, Psf->SHIFTOP, ijkDir ) ;
	
      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
	dir[nDim] = ijkDir[nDim] - ijkO[nDim] ;
	/* Copy the new indices. */
	ijk[nDim] = ijkO[nDim] ; }
      /* The new block. */
      *PPbl = Psf->PrBlock ;
    }
    else {
      /* The other block is on the left. */
      trans_r2l ( ijk, Psf->ProtL2R->rotMatrix, Psf->SHIFTOP, ijkO ) ;
      /* Transform the directions. */
      ijk[runningDim] += dir[runningDim] ;
      trans_r2l ( ijk, Psf->ProtL2R->rotMatrix, Psf->SHIFTOP, ijkDir ) ;

      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
        dir[nDim] = ijkDir[nDim] - ijkO[nDim] ;
	/* Copy the new indices. */
	ijk[nDim] = ijkO[nDim] ; }
      /* The new block. */
      *PPbl = Psf->PlBlock ;
    }
  }
  else {
    /* Just increment within the same block. */
    ijk[runningDim] += dir[runningDim] ;
  }

  return ( 1 ) ;
}

/******************************************************************************

  find_mb_subface:
  Find the subface that is bordered a cell ijk given the direction.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pbl:       block
  ijk[]:     cell indices.
  mDim:
  staticDim: static dimension of this surface.
  staticDir: orientation of the face sought: +1:, +ijk, -1: -ijk.

  Returns:
  --------
  NULL on failure, the subface pointer on success.
  
*/

#if defined (NODE)
  int find_mb_subFc_node ( const block_struct *Pbl, const int ijk[],
			   const int mDim, const int staticDim, const int staticDir,
			   subFace_struct **PPsubFc )
#elif defined (CELL)
  int find_mb_subFc_cell ( const block_struct *Pbl, const int ijk[],
			   const int mDim, const int staticDim, const int staticDir, 
			   subFace_struct **PPsubFc )
#endif
{
  subFace_struct **PPsf, *Psf ;
  int contained, nDim ;
  
  for ( PPsf = Pbl->PPsubFaces ;
        PPsf < Pbl->PPsubFaces + Pbl->mSubFaces ; PPsf++ )
  { Psf = *PPsf ;
    contained = 0 ;
	
    if ( Psf->PlBlock == Pbl )
    { /* Left side. */
#     if defined (NODE)
        if ( Psf->llLBlock[staticDim] == Psf->urLBlock[staticDim] &&
	     ijk[staticDim] == Psf->llLBlock[staticDim] )
	  /* This subface has the proper static index. Check the running ones. */
	  for ( contained = 1, nDim = 0 ; contained && nDim < mDim ; nDim++ )
	    if ( nDim != staticDim )
	      if ( Psf->llLBlock[nDim] > ijk[nDim] ||
		   Psf->urLBlock[nDim] < ijk[nDim] )
		contained = 0 ;

#     elif defined (CELL)
        if ( Psf->llLBlock[staticDim] == Psf->urLBlock[staticDim] &&
	     ijk[staticDim] + ( staticDir+1 )/2 == Psf->llLBlock[staticDim] )
	  /* This subface has the proper static index.  Check the running ones. */
	  for ( contained = 1, nDim = 0 ; contained && nDim < mDim ; nDim++ )
	    if ( nDim != staticDim )
	      if ( Psf->llLBlock[nDim] > ijk[nDim] ||
		   Psf->urLBlock[nDim] <= ijk[nDim] )
		contained = 0 ;
#     endif
    }
    else
    { /* Right side. */
#     if defined (NODE)
        if ( Psf->llRBlock[staticDim] == Psf->urRBlock[staticDim] &&
	     ijk[staticDim] == Psf->llRBlock[staticDim] )
	  /* This subface has the proper static index. Check the running ones. */
	  for ( contained = 1, nDim = 0 ; contained && nDim < mDim ; nDim++ )
	    if ( nDim != staticDim )
	      if ( Psf->llRBlock[nDim] > ijk[nDim] ||
		   Psf->urRBlock[nDim] < ijk[nDim] )
		contained = 0 ;

#     elif defined (CELL)
        if ( Psf->llRBlock[staticDim] == Psf->urRBlock[staticDim] &&
	     ijk[staticDim] + ( staticDir+1 )/2 == Psf->llRBlock[staticDim] )
	  /* This subface has the proper static index.  Check the running ones. */
	  for ( contained = 1, nDim = 0 ; contained && nDim < mDim ; nDim++ )
	    if ( nDim != staticDim )
	      if ( Psf->llRBlock[nDim] > ijk[nDim] ||
		   Psf->urRBlock[nDim] <= ijk[nDim] )
		contained = 0 ;
#     endif
    }
	
    if ( contained )
    { /* This is the proper subface. Note that *Psf may be NULL since ijk
	 may reside on a degenerate face. */
      *PPsubFc = Psf ;
      return ( 1 ) ;
    }
  }

  /* No match found. */
  *PPsubFc = NULL ;
  return ( 0 ) ;
}
