/*
   adEdge.c:
   

   Last update:
   ------------
   3Apr00; intro get_fcvx_aE.


   This file contains:
   -------------------
   get_child_aE
   get_face_aE
   add_adEdge_vrtx
   add_adEdge_elem
   add_elem_aE
   add_quadFc_aE:
   add_quadFace_aE:

   match_per_aE:
   clean_uns_adEdge:
   match_perVx_mark:
   count_newVx_llAe:

   mark_uns_vertFromAdEdge:
   list_adEdge:
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

#define VX vrtx_struct
#define CV const vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;

static int add_quadFace_aE ( uns_s *pUns, vrtx_struct *pVxCrnr[MAX_VX_FACE],
                             vrtx_struct *pVxMid[MAX_VX_FACE], vrtx_struct **ppVxCtr,
                             //int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                             int *nFcAe, int *nCrossAe,
                             int *pnFixAe, int *pfixDiagDir,
                             chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                             double **ppLstCoor, double **ppLstUnknown );

/******************************************************************************

  get_child_aE:
  Given an adapted edge, find out whether there are children to this edge.
  
  Last update:
  ------------
  Jul98: conceived.
  
  Input:
  ------
  pllAdEdge: The list of adapted edges
  pAdEdge:   and the adaption info to the edge,
  nAe:       the edge for which the children are sought.

  Changes To:
  -----------
  nChAe[]:   the children of the edge,
  pVx[]:     the vertices on the edge.
  
  Returns:
  --------
  The number of children.
  
*/

int get_child_aE ( const uns_s *pUns, const llEdge_s *pllAdEdge, adEdge_s *pAdEdge,
                   int nAe,
                   int nChAe[2], const vrtx_struct *pVx[3] ) {
  
  int nDir ;
  const adEdge_s *pAe = pAdEdge+nAe ;
  const vrtx_struct *pVx0, *pVx1 ;
  
  if ( !show_edge ( pllAdEdge, nAe,
                    ( vrtx_struct ** ) pVx, ( vrtx_struct ** ) pVx+2 ) ) {
    /* No such edge. Cannot have any children. */
    nChAe[0] = nChAe[1] = 0 ;
    pVx[0] = pVx[1] = pVx[2] = NULL ;
    return ( 0 ) ;
  }

  if ( pAe->cpVxMid.nr ) {
    /* There is a mid vertex on the edge. */
    pVx[1] = de_cptVx( pUns, pAe->cpVxMid ) ;

    /* Make a copy of the end vertices, to avoid having pVx switched. */
    pVx0 = pVx[0] ; pVx1 = pVx[1] ;
    nChAe[0] = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &nDir ) ;
    pVx0 = pVx[2] ; pVx1 = pVx[1] ;
    nChAe[1] = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &nDir ) ;
  }
  else {
    /* No midvertex. Thus there cannot be any children. */
    nChAe[0] = nChAe[1] = 0 ;
    pVx[1] = NULL ;
  }

  if ( nChAe[0] && nChAe[1] )
    return ( 2 ) ;
  else if ( nChAe[0] || nChAe[1] )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

/******************************************************************************

  get_face_aE:
  Given a face of an element, find the adaptive edges on that face. These edges
  can define
  - a full refinement of a quad face with a center node,
  - a split refinement of a quad or tri face,
  - a cross refinement in two directions of a quad face when both formed cells
    are directionally refined,
  - a hanging edge situation where two tri faces match one quad face.
  
  Last update:
  ------------
  3Apr00; make a new interface for just the vertices of a face. 
  Aug98; also look for a vertex on a quad face in case of a fixed diagonal.
  Jul98: conceived.
  
  Input:
  ------
  pllAdEdge: The list of adapted edges
  pAdEdge:   and the adaption info to the edge,
  pElem:     The element and
  kFace:     its face.

  Changes To:
  -----------
  pmVxFc:    The number and 
  pVxFc:     list of vertices on the face.
  nFcAe:     The list of adapted edges around the face.
  nCrossAe:  The list of adapted edges across the face.
  pnFixAe:   A fixed diagonal on a quad face.
  pfixDiagDir: 0/1 if a fixed diagonal runs from/opposite the lowest indexed node.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_face_aE ( const uns_s *pUns,
                  const elem_struct *pElem, const int kFace, int *pmVxBase,
                  int *pmVxFc, const vrtx_struct *pVxFc[2*MAX_VX_FACE+1],
                  int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                  int *pnFixAe, int *pfixDiagDir ) {
  
  const faceOfElem_struct *pFoE = elemType[pElem->elType].faceOfElem + kFace ;
  int kVx, mVxBase = pFoE->mVertsFace ;
  vrtx_struct *pVxCrnr[MAX_VX_FACE]  ;

  *pmVxBase = mVxBase ;
  for ( kVx = 0 ; kVx < mVxBase ; kVx++ )
    pVxCrnr[kVx] = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;

  return ( get_fcvx_aE ( pUns, pVxCrnr, mVxBase, pmVxFc, pVxFc,
                         nFcAe, nCrossAe, pnFixAe, pfixDiagDir ) ) ;
}

/* The same, just identify the face via its four corner vertices. */

int get_fcvx_aE ( const uns_s *pUns,
                  vrtx_struct *pVxCrnr[MAX_VX_FACE], int mVxBase,
                  int *pmVxFc, const vrtx_struct *pVxFc[2*MAX_VX_FACE+1],
                  int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                  int *pnFixAe, int *pfixDiagDir ) {
  
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  adEdge_s *pAdEdge = pUns->pAdEdge ;
  int kVx, dir ;
  const vrtx_struct *pVx0, *pVx1 ;

  /* Find the vertices around the face. Reset vertices at midedge and the center one. */
  for ( kVx = 0 ; kVx < 2*MAX_VX_FACE+1 ; kVx++ )
    pVxFc[kVx] = NULL ;
  
  *pmVxFc = mVxBase ;
  for ( kVx = 0 ; kVx < mVxBase ; kVx++ )
    pVxFc[2*kVx] = pVxCrnr[kVx] ;



  /* Find the edges and mid-vertices around the face. */
  for ( kVx = 0 ; kVx < mVxBase ; kVx++ ) {
    pVx0 = pVxFc[2*kVx] ;
    pVx1 = pVxFc[(2*kVx+2)%(2*mVxBase)] ;
    if ( ( nFcAe[kVx] = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &dir ) ) ) {
      /* There is an edge. Look for a mid-vertex. */
      pVxFc[2*kVx+1] = de_cptVx( pUns, pAdEdge[ nFcAe[kVx] ].cpVxMid ) ;
      ++(*pmVxFc) ;
    }
    else
      pVxFc[2*kVx+1] = NULL ;
  }

  if ( mVxBase == 3 ) {
    /* Triangular faces can carry edges across the face from each corner
       vertex to the midvertex of the opposite edge. */
    for ( kVx = 0 ; kVx < 3 ; kVx++ )
      if ( ( pVx1 = pVxFc[2*kVx+1] ) ) {
        pVx0 = pVxFc[2*kVx] ;
        nCrossAe[kVx] = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &dir ) ;
      }
      else
        nCrossAe[kVx] = 0 ;
    *pnFixAe = 0 ;
  }
  else if ( mVxBase == 4 ) {
    /* Quad faces can carry two cross edges. */
    for ( kVx = 0 ; kVx < 2 ; kVx++ )
      if ( ( pVx0 = pVxFc[2*kVx+1] ) && ( pVx1 = pVxFc[2*kVx+5] ) )
        nCrossAe[kVx] = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &dir ) ;
      else
        nCrossAe[kVx] = 0 ;
    nCrossAe[2] = 0 ;

    /* Quad faces can carry one fixed diagonal. */
    for ( *pnFixAe = kVx = 0 ; kVx < 2 ; kVx++ ) {
      pVx0 = pVxFc[2*kVx] ;
      pVx1 = pVxFc[2*kVx+4] ;
      if ( ( *pnFixAe = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &dir ) ) ) {
        *pfixDiagDir = kVx ;
        break ;
      }
    }
    
    /* If there is a cross edge or a fixed diagonal, look for a mid-vertex. */
    if ( nCrossAe[0] && pAdEdge[ nCrossAe[0] ].cpVxMid.nr ) {
      pVxFc[8] = de_cptVx ( pUns, pAdEdge[ nCrossAe[0] ].cpVxMid ) ;
      ++(*pmVxFc) ;
    }
    else if ( nCrossAe[1] && pAdEdge[ nCrossAe[1] ].cpVxMid.nr ) {
      pVxFc[8] = de_cptVx ( pUns, pAdEdge[ nCrossAe[1] ].cpVxMid ) ;
      ++(*pmVxFc) ;
    }
    else if ( *pnFixAe ) {
      pVxFc[8] = de_cptVx ( pUns, pAdEdge[ *pnFixAe ].cpVxMid ) ;
      ++(*pmVxFc) ;
    }
  }
  
  return ( 1 ) ;
}
/******************************************************************************

  get_face_half_aE:
  Get all the half edges around a face.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  mVxBase:   the number of base vx around the face.
  pVxFc[8]:  the canonically ordered vertices around the face. 

  Changes To:
  -----------
  nHFcAe:    the half edges, if present,
  pVxHFc:    the vertices on those edges, if present.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_face_half_aE ( const uns_s *pUns,
                       int mVxBase, const vrtx_struct *pVxFc[2*MAX_VX_FACE+1],
                       int nHFcAe[2*MAX_VX_FACE],
                       const vrtx_struct *pVxHFc[2*MAX_VX_FACE] ) {
  
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  adEdge_s *pAdEdge = pUns->pAdEdge ;
  int kVx, dir ;
  const vrtx_struct *pVx0, *pVx1 ;

  for ( kVx = 0 ; kVx < 2*mVxBase ; kVx++ ) {
    pVx0 = pVxFc[kVx] ;
    pVx1 = pVxFc[(kVx+1)%(2*mVxBase)] ;
    if ( ( nHFcAe[kVx] = get_edge_vrtx ( pllAdEdge, &pVx0, &pVx1, &dir ) ) )
      /* There is an edge. Look for a mid-vertex. */
      pVxHFc[kVx] = de_cptVx( pUns, pAdEdge[ nHFcAe[kVx] ].cpVxMid ) ;
    else
      pVxHFc[kVx] = NULL ;
  }

      
  return ( 1 ) ;
}


/******************************************************************************

  rm_adEdgeVx_elem:
  Given an element, remove any mid-edge vertices.
  
  Last update:
  ------------
  16Sep19: conceived.
  
  Input:
  ------
  pUns: grid with list of adapted edges.
  pElem: defines the number of edges, faces, edges on faces.
  nAe: list of element edges
  nCrossAe: list of cross-face edges.

  
*/

void rm_adEdgeVx_elem ( uns_s *pUns, const elem_struct *pElem, 
                      int nAe[MAX_EDGES_ELEM],
                      int nCrossAe[MAX_FACES_ELEM+1][MAX_VX_FACE] ) {
  
  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mEdges = pElT->mEdges ;
  const cpt_s cptVxNull = {0,0} ;

  int kEdge ;
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  adEdge_s *pAe ;
  for ( kEdge = 0 ; kEdge < mEdges ; kEdge++ )
    if ( nAe[kEdge] ) {
      pAe = pUns->pAdEdge + nAe[kEdge] ;
      pAe->cpVxMid = cptVxNull ;
    }

  const int mFaces = pElT->mFaces ;
  int kFace, kVert ;
  const faceOfElem_struct *pFoE ;
  for ( kFace = 1 ; kFace <= mFaces ; kFace++ ) {
    pFoE = pElT->faceOfElem + kFace ;
    for ( kVert = 0 ; kVert < pFoE->mVertsFace ; kVert++ )
      if ( nCrossAe[kFace][kVert] ) {
        del_edge( pllAdEdge, nCrossAe[kFace][kVert] ) ;
        //pAe = pUns->pAdEdge + nAe[kEdge] ;
        //pAe->cpVxMid = cptVxNull ;
      }
  }

  return ;
}

/******************************************************************************

  add_adEdge_vrtx:
  A binder for add_edge_vrtx that does periodicity.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns:
  pVrtx0/1: the two vertices that are spanned by the edge. No switching here.


  Changes To:
  -----------
  *pnPerEg: the number of the periodic sibling edge, if there is one,
  *pNewEg:  1 if the edge had to be added.

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_adEdge_vrtx ( uns_s *pUns, vrtx_struct *pVrtx0, vrtx_struct *pVrtx1,
                      int *pnPerEg, int *pNewEg ) {

  const vrtx_struct *pVx[2], *pVxPer[2] ;
  perVxPair_s vxPair, *pVxPair ;
  int dir, nEg, nPerEg, newEg ;
  
  *pnPerEg = 0 ;
  pVx[0] = pVrtx0 ;
  pVx[1] = pVrtx1 ;
  
  if ( !( nEg = add_edge_vrtx ( pUns->pllAdEdge, (void**) &pUns->pAdEdge,
                                pVx, pVx+1, &dir, &newEg ) ) )
    hip_err ( fatal, 0, "could not add edge in add_adEdge_vrtx." ) ;
  if ( newEg ) {
    *pNewEg = 1 ;
    pUns->pAdEdge[nEg].cpVxMid = CP_NULL ;
  }

  /* Is there periodicity to be reckoned with. */
  if ( pUns->mPerVxPairs ) {
    vxPair.In = pVrtx0 ;
    pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                        sizeof ( perVxPair_s ), cmp_perVxPair ) ;
    pVxPer[0] = ( pVxPair ? pVxPair->Out : NULL ) ;
    
    if ( pVxPer[0] ) {
      vxPair.In = pVrtx1 ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[1] = ( pVxPair ? pVxPair->Out : NULL ) ;
      if ( pVxPer[1] ) {
        /* There is a sibling. */
        if ( !( nPerEg = add_edge_vrtx ( pUns->pllAdEdge, (void**) &pUns->pAdEdge,
                                         pVxPer, pVxPer+1, &dir, &newEg ) ) )
          hip_err ( fatal, 0, 
                    "could not add periodic edge in add_adEdge_vrtx." ) ;
        *pnPerEg = nPerEg ;
        if ( newEg ) {
          *pNewEg = 1 ;
          pUns->pAdEdge[nPerEg].cpVxMid = CP_NULL ;
        }
      }
    }
  }
  
  return ( nEg ) ;
}

/******************************************************************************

  add_elem_edge:
  Add one edge of an element to the list, taking periodicity into account.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pllEdge     The list of edges.
  pElem       The element and its edge to insert.
  kEdge

  Changes To:
  -----------
  *pnPerEg: the number of the periodic sibling edge, if there is one,
  *pNewEg:  1 if the edge had to be added.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_adEdge_elem ( uns_s *pUns, const elem_struct *pElem, const int kEdge,
                      int *pnPerEg, int *pNewEg ) {
  
  static const int *kVxEdge ;
  vrtx_struct *pVx0, *pVx1 ;

  kVxEdge = elemType[ pElem->elType ].edgeOfElem[kEdge].kVxEdge ;
  
  /* The two vertices of the edge. */
  pVx0 = pElem->PPvrtx[ kVxEdge[0] ] ;
  pVx1 = pElem->PPvrtx[ kVxEdge[1] ] ;

  /* Find the edge. */
  return ( add_adEdge_vrtx ( pUns, pVx0, pVx1, pnPerEg, pNewEg ) ) ;
}


/******************************************************************************
  add_quadFace_aE:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


static int add_quadFace_aE ( uns_s *pUns, vrtx_struct *pVxCrnr[MAX_VX_FACE],
                             vrtx_struct *pVxMid[MAX_VX_FACE], vrtx_struct **ppVxCtr,
                             //int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                             int *nFcAe, int *nCrossAe,
                             int *pnFixAe, int *pfixDiagDir,
                             chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                             double **ppLstCoor, double **ppLstUnknown ) {
  
  int nAeFix[2], dir, nAfP, newEg ;
  cpt_s cp[4], cpRef ;
  const vrtx_struct *pVrtx[2] ;

  nFcAe[0] = add_adEdge_vrtx ( pUns, pVxCrnr[0], pVxCrnr[1], &nAfP, &newEg ) ;
  nFcAe[1] = add_adEdge_vrtx ( pUns, pVxCrnr[1], pVxCrnr[2], &nAfP, &newEg ) ;
  nFcAe[2] = add_adEdge_vrtx ( pUns, pVxCrnr[2], pVxCrnr[3], &nAfP, &newEg ) ;
  nFcAe[3] = add_adEdge_vrtx ( pUns, pVxCrnr[3], pVxCrnr[0], &nAfP, &newEg ) ;
  if ( !nFcAe[0] || !nFcAe[0] || !nFcAe[0] || !nFcAe[0] ) {
    /* Legal behaviour. */
    /* printf ( " FATAL: missing required face edge in add_quadFc_aE\n" ) ; */
    return ( 1 ) ; }

  /* Find midvertices. */
  pVxMid[0] = de_cptVx( pUns, pUns->pAdEdge[ nFcAe[0] ].cpVxMid ) ;
  pVxMid[1] = de_cptVx( pUns, pUns->pAdEdge[ nFcAe[1] ].cpVxMid ) ;
  pVxMid[2] = de_cptVx( pUns, pUns->pAdEdge[ nFcAe[2] ].cpVxMid ) ;
  pVxMid[3] = de_cptVx( pUns, pUns->pAdEdge[ nFcAe[3] ].cpVxMid ) ;
  if ( !pVxMid[0] || !pVxMid[0] || !pVxMid[0] || !pVxMid[0] ) {
    hip_err ( fatal, 0, "missing required mid edge vertex in add_quadFc_aE." ) ;
    return ( 0 ) ; }

  
  /* Find all edges that cross the face. */
  nCrossAe[0] = add_adEdge_vrtx ( pUns, pVxMid[0], pVxMid[2], &nAfP, &newEg ) ;
  nCrossAe[1] = add_adEdge_vrtx ( pUns, pVxMid[1], pVxMid[3], &nAfP, &newEg ) ;

  /* Maybe the vertex exists already on a fixed diagonal? */
  pVrtx[0] = pVxCrnr[0] ;
  pVrtx[1] = pVxCrnr[2] ;
  nAeFix[0] = get_edge_vrtx ( pUns->pllAdEdge, pVrtx, pVrtx+1, &dir ) ;
  pVrtx[0] = pVxCrnr[1] ;
  pVrtx[1] = pVxCrnr[3] ;
  nAeFix[1] = get_edge_vrtx ( pUns->pllAdEdge, pVrtx, pVrtx+1, &dir ) ;
            

  /* Find all existing vertices on the face, on cross edges and fixed diags. */
  for ( dir = 0 ; dir < 2 ; dir++ ) {
    cp[dir] = pUns->pAdEdge[ nCrossAe[dir] ].cpVxMid ;
    if ( nAeFix[dir] ) {
      *pnFixAe = nAeFix[dir] ;
      *pfixDiagDir = dir ;
      cp[dir+2] = pUns->pAdEdge[ nAeFix[dir] ].cpVxMid ;
    }
    else
      cp[dir+2] = CP_NULL ;
  }
  
  /* Do all the vertices found coincide? */
  for ( cpRef = CP_NULL, dir = 0 ; dir < 4 ; dir++ )
    if ( cp[dir].nr ) {
      if ( !cpRef.nr )
        cpRef = cp[dir] ;
      else if ( cpRef.nr != cp[dir].nr || cpRef.nCh != cp[dir].nCh ) {
        hip_err ( fatal, 0, "mismatch of face vertex in add_elem_aE." ) ;
        return ( 0 ) ; }
    }
  
  
  /* Add a center vertex to the face. */
  if ( !cpRef.nr && pRefChunk ) {
    /* Make a new one. */
    *ppVxCtr = adapt_uh_place_vx ( pUns, pRefChunk, ppLstVx,
                                   ppLstCoor, ppLstUnknown,
                          pVxCrnr, 4 ) ;
    cpRef = (*ppVxCtr)->vxCpt ;
  }
  else 
    *ppVxCtr = de_cptVx ( pUns, cpRef ) ;
  
  /* Put the vertex on the edges on the face. */
  pUns->pAdEdge[ nCrossAe[0] ].cpVxMid = cpRef ;
  pUns->pAdEdge[ nCrossAe[1] ].cpVxMid = cpRef ;
  if ( nAeFix[0] )
    pUns->pAdEdge[ nAeFix[0] ].cpVxMid = cpRef ;
  else if ( nAeFix[1] )
    pUns->pAdEdge[ nAeFix[1] ].cpVxMid = cpRef ;

  return ( 1 ) ;
}

/******************************************************************************

  add_quadFc_aE:
  Make a consistent edge pattern on a quad face, find or place the center vertex.
  Take periodicity into account.
  
  Last update:
  ------------
  11Nov19; intro doBuf 
  : conceived.
  
  Input:
  ------
  pUns
  pVxCrnr:     the four vertices around the face.
  doBuf:       fill for [test-]buffering, omit periodic vertices.

  Changes To:
  -----------
  pRefChunk:   the vertex space 
  ppLstVx:
  ppLstCoor:
  ppLstUnknown:
  pVxMid[]:    the midvertices around the face
  ppVxCtr:     the added center vertex.
  nFcAe[]:     the four perimeter edges
  nCrossAe[]:  the two cross edges.
  pnFixAe:     is there a fixed diagonal
  pfixDiagDir: in what direction?
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_quadFc_aE ( uns_s *pUns, vrtx_struct *pVxCrnr[MAX_VX_FACE],
                    const int doBuf,
                    vrtx_struct **ppVxCtr,
                    // gcc 11.3 doesn't like this.
                    // int nFcAe[MAX_VX_FACE], int nCrossAe[MAX_VX_FACE-1],
                    int *nFcAe, int *nCrossAe,
                    int *pnFixAe, int *pfixDiagDir,
                    chunk_struct *pRefChunk, vrtx_struct **ppLstVx, double **ppLstCoor,
                    double **ppLstUnknown ) {

  perVxPair_s vxPair, *pVxPair ;
  vrtx_struct *pVxPer[4], *pVxMidP[4], *pVxCtrP ;
  int nPerFix, perFixDir, nFcAeP[4], nCrAeP[2] ;
  vrtx_struct *pVxMid[MAX_VX_FACE] ;  
  if ( !add_quadFace_aE ( pUns, pVxCrnr, pVxMid, ppVxCtr,
                          nFcAe, nCrossAe, pnFixAe, pfixDiagDir,
                          pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown ) ) {
    hip_err ( fatal,0,"failed to add face in add_quadFc_aE." ) ;
    return ( 0 ) ; }
    

  if ( !doBuf ) {
    /* Add the sibling face. */
    if ( pVxCrnr[0]->per && pVxCrnr[1]->per &&
         pVxCrnr[2]->per && pVxCrnr[3]->per ) {
      /* All vertices on this face are periodic. 
         pPerVxPair is one-sided, points from lower to upper. If an periodic
         edge is on the upper per surface, it will be picked up when visiting
         the lower sibling in the next sweep. */
      vxPair.In = pVxCrnr[0] ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[0] = ( pVxPair ? pVxPair->Out : NULL ) ;
    
      vxPair.In = pVxCrnr[1] ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[1] = ( pVxPair ? pVxPair->Out : NULL ) ;
    
      vxPair.In = pVxCrnr[2] ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[2] = ( pVxPair ? pVxPair->Out : NULL ) ;
    
      vxPair.In = pVxCrnr[3] ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[3] = ( pVxPair ? pVxPair->Out : NULL ) ;
    
      if ( !add_quadFace_aE ( pUns, pVxPer, pVxMidP, &pVxCtrP,
                              nFcAeP, nCrAeP, &nPerFix, &perFixDir,
                              pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown ) ) {
        hip_err ( fatal,0,"failed to add sibling face in add_quadFc_aE" ) ;
        return ( 0 ) ; }
    }
  }

  return  ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  get_buf_aE:
*/
/*! given a element not marked for refinement, find all edges that are refined by 
 * its neighbours.
 */

/*
  
  Last update:
  ------------
  8Nov19: extracted from add_elem_aE
  

  Input:
  ------
  pUns   grid
  pElem: element

  Changes To:
  -----------

  Output:
  -------
  nAe[] : number of the adapted edge in position kEdge, zero if unadapted.
  kVertEg[]: counter of the added edge in position kEdge.
    
  Returns:
  --------
  number of adapted edges.
  
*/

int get_bufEdges_elem ( uns_s *pUns, const elem_struct *pElem,
                        int nAe[MAX_EDGES_ELEM], int kVertEg[MAX_EDGES_ELEM] ) {

  int kEdge, mAdEdges=0 ;
  const int mEdges = elemType[pElem->elType].mEdges ;
  const vrtx_struct *pVrtx[2] ;
  int dirSwitch ;
  for ( kEdge = 0 ; kEdge < mEdges ; kEdge++ ) {
    nAe[kEdge] = get_elem_edge (  pUns->pllAdEdge, pElem, kEdge,
                                  pVrtx, pVrtx+1, &dirSwitch ) ;
    if ( nAe[kEdge] ) 
      kVertEg[kEdge] = mAdEdges++ ;
  }

  return ( mAdEdges ) ;
}

  
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  add_adEdges_elem:
*/
/*! given an element marked for refinement, generate all refined element edges
 *  (i.e. corner edges, not cross edges over faces.)
 *
 */

/*
  
  Last update:
  ------------
  8Nov19: extracted from add_elem_aE
  

  Input:
  ------
  pUns   grid
  pElem: element

  Changes To:
  -----------

  Output:
  -------
  nAe[] : number of the adapted edge in position kEdge, zero if unadapted.
  nAeP[] : number of parallel sibling to 
           the adapted edge in position kEdge, zero if nonexistent.
  kVertEg[]: counter of the added edge in position kEdge.
    
  Returns:
  --------
  number of adapted edges.
  
*/

int add_adEdges_elem ( uns_s *pUns, const elem_struct *pElem,
                       int nAe[MAX_EDGES_ELEM],
                       int nAeP[MAX_EDGES_ELEM],
                       int kVertEg[MAX_EDGES_ELEM]  ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;
  const refType_struct *pRefT = pElem->PrefType ;

  int mAdEdges = 0 ;
  int iEdge, kEdge, kVert, newEg ;
  /* Loop over all the refined edges. */
  for ( iEdge = 0 ; iEdge < pRefT->mEdgeVerts ; iEdge++ ) {
    kEdge = pRefT->edgeVert[iEdge].kEdge ;
    kVert = pRefT->edgeVert[iEdge].kVert ;
    kVertEg[kEdge] = kVert ;
    mAdEdges++ ;

    /* All edges exist in the case of refinement. In the case of derefinement,
       they might have to be created. */
    if ( !( nAe[kEdge] = add_adEdge_elem ( pUns, pElem, kEdge,
                                           nAeP+kEdge, &newEg ) ) ) {
      sprintf ( hip_msg, 
                " required edge %d in elem %"FMT_ULG" could not be added"
                " in add_adEdges_elem.\n",
                kEdge, pElem->number ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  
  return ( mAdEdges ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  add_midVx_elem:
*/
/*! Given an element with a set of corner edges marked for refinement,
 *  add the mid-vertices.
 *
 */

/*
  
  Last update:
  ------------
  8Nov19: extracted from add_elem_aE 
  

  Input:
  ------
  pUns: grid
  pElem: element
  nAe[] : number of the adapted edge in position kEdge, zero if unadapted.
  nAeP[] : number of parallel sibling to 
           the adapted edge in position kEdge, zero if nonexistent.
  kVertEg[]: counter of the added edge in position kEdge.
  pRefChunk:    the new chunk containing the new vertices,
  doBuf:        buffering, only central vx added, no periodicity.

  Changes To:
  -----------
  ppVrtx:       A list of all vertices on the element in their canonical positions.
  ppLstVx:      a pointer to the last vertex used,
  ppLstCoor:    a pointer to the last coordinate used,
  ppLstUnknown: a pointer to the last unknown used,

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int add_midEgVx_elem ( uns_s *pUns, const elem_struct *pElem,
                       int nAe[MAX_EDGES_ELEM],
                       int nAeP[MAX_EDGES_ELEM],
                       int kVertEg[MAX_EDGES_ELEM],
                       const int doBuf,
                       vrtx_struct *ppVrtx[MAX_VX_ELEM+MAX_ADDED_VERTS],
                       chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                       double **ppLstCoor, double **ppLstUnknown ) {
  
  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mEdges = pElT->mEdges ;
  const int *kVxEdge ;
  const refType_struct *pRefT = pElem->PrefType ;

  /* Find all corner edges that need a mid-vertex. */
  int mAdEdges = 0 ;
  if ( doBuf ) {
    /* Buffering, find existing hanging edges. */
    mAdEdges = get_bufEdges_elem ( pUns, pElem, nAe, kVertEg ) ;
  }
  else {
    /* Refinement, identify the edges required by the refinement pattern. */
    mAdEdges = add_adEdges_elem ( pUns, pElem, nAe, nAeP, kVertEg ) ;
  }

  
  int kEdge ;
  adEdge_s *pAe ;
  vrtx_struct *pVrtxMid ;
  const vrtx_struct *pVrtx[2] ;
  vrtx_struct *pPerVxMid ;
  vrtx_struct *pPerVx[2] ;
  const edgeVert_s *pEV ;
  perVxPair_s vxPair, *pVxPair ;
  for ( kEdge = 0 ; kEdge < mEdges ; kEdge++ ) {
    pAe = pUns->pAdEdge + nAe[kEdge] ;
    if ( nAe[kEdge] ) {
      /* Edge with hanging node. */
      if ( ( pVrtxMid = de_cptVx( pUns, pAe->cpVxMid ) ) ) {
        /* This edge already has a vertex on its middle. Thus, it exists fine
           on the periodic side. */
        ppVrtx[ kVertEg[kEdge] ] = pVrtxMid ;
      }
        
      else if ( pRefChunk ) {
        /* Place a new vertex at mid-edge. */
        kVxEdge = pElT->edgeOfElem[kEdge].kVxEdge ;
        pVrtx[0] = pElem->PPvrtx[ kVxEdge[0] ] ;
        pVrtx[1] = pElem->PPvrtx[ kVxEdge[1] ] ;
        ppVrtx[ kVertEg[kEdge] ] =
          adapt_uh_place_vx ( pUns, pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown,
                     (vrtx_struct **) pVrtx, 2 ) ;
        pAe->cpVxMid = ppVrtx[ kVertEg[kEdge] ]->vxCpt ;
        if ( nAeP[kEdge] && !doBuf ) {
          /* Place a vertex with the sibling. */
          show_edge ( pUns->pllAdEdge, nAeP[kEdge], pPerVx, pPerVx+1 ) ; 
          pPerVxMid = adapt_uh_place_vx ( pUns, pRefChunk, ppLstVx,
                                          ppLstCoor, ppLstUnknown, 
                                 (vrtx_struct **) pPerVx, 2 ) ;
          pUns->pAdEdge[ nAeP[kEdge] ].cpVxMid = pPerVxMid->vxCpt ;
        } 
      }
        
      else if ( pElem->PPchild ) {
        /* Derefinement. This element has children. Find the vertex in the child. */
        pEV = pRefT->edgeVert + kEdge ;
        pAe->cpVxMid = pElem->PPchild[ pEV->kChild ]->PPvrtx[ pEV->kChildVx ]->vxCpt ;

        if ( nAeP[kEdge] ) {
          pAe = pUns->pAdEdge + nAeP[kEdge] ;
          if ( !de_cptVx( pUns, pAe->cpVxMid ) ) {
            /* Problem here is: we don't know the matching periodic element. But
               all the vertices involved have to exist and we can use the periodic
               vertex tables to match them. */
            vxPair.In = de_cptVx( pUns, pAe->cpVxMid ) ;
            pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                                sizeof ( perVxPair_s ), cmp_perVxPair ) ;
            pPerVxMid = ( pVxPair ? pVxPair->Out : NULL ) ;
            pUns->pAdEdge[ nAeP[kEdge] ].cpVxMid = pPerVxMid->vxCpt ;
          }
        }
      }
        
      else {
        hip_err ( fatal, 0, "cannot place new edge vertex in add_elem_aE." ) ;
      }
    }
  }

  
  return ( mAdEdges ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  add_midFcVx_elem:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  20Mar20: enable for use with buffering, when no pRefT is defined.
  : conceived.
  

  Input:
  ------
  pUns:         The grid,
  pElem:        the element,
  pRefChunk:    the new chunk containing the new vertices,
  doBuf:        if non-zero, buffering: add only element ctr vx, temporarily face vx, no periodicity.
  nAe:          list of adapted edges on this element.

  Changes To:
  -----------
  ppLstVx:      a pointer to the last vertex used,
  ppLstCoor:    a pointer to the last coordinate used,
  ppLstUnknown: a pointer to the last unknown used,
  ppVrtx:       A list of all vertices on the element in their canonical positions.

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int  add_midFcVx_elem ( uns_s *pUns, elem_struct *pElem,
                        int nAe[MAX_EDGES_ELEM],
                        int nCrossAe[MAX_FACES_ELEM+1][MAX_VX_FACE],
                        int doBuf,
                        vrtx_struct *ppVrtx[MAX_VX_ELEM+MAX_ADDED_VERTS ],
                        chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                        double **ppLstCoor, double **ppLstUnknown ) {

  int mFcVxAdded = 0 ;
  
  int kFace ;
  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mFaces = pElT->mFaces ;
  const int mVxFace ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *kVxFace ;
  vrtx_struct *pVxCrnr[MAX_VX_FACE] ;
  int nFixAe, fixDir, nFcAe[MAX_VX_FACE] ;
  int mVxCtr = 0 ;
  int kVert = 0 ;
  int kFcQuadded[MAX_FACES_ELEM+1] ;
    /* Reset the flags that indicates full refinement of each face. */
  for ( kFace = 1 ; kFace <= mFaces ; kFace++ )
    kFcQuadded[kFace] = 0 ;


  /* Add mid-fc vx needed by the Refinement pattern. */
  int iFace ;
  const refType_struct *pRefT = pElem->PrefType ;
  int kVertFc[MAX_FACES_ELEM+1] ;
  int kFcVxCtr[MAX_FACES_ELEM+1] ;
  int kVx ;
  vrtx_struct *pVxCtr ;
  for ( iFace = 0 ; iFace < (pRefT ? pRefT->mFaceVerts : 0 ) ; iFace++ ) {
    kFace = pRefT->faceVert[iFace].kFace ;
    kFcVxCtr[mVxCtr] = kFace ;
    kVertFc[kFace] = pRefT->faceVert[iFace].kVert ;

    /* All faces that carry a face vertex must be quad. */
    pFoE = pElT->faceOfElem + kFace ;
    for ( kVx = 0; kVx < 4 ; kVx++ )
      pVxCrnr[kVx] = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;


    if ( !add_quadFc_aE ( pUns, pVxCrnr, doBuf, &pVxCtr,
                          nFcAe, nCrossAe[kFace], &nFixAe, &fixDir,
                          pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown ) ) {
      hip_err ( fatal, 0, "Failed to add quad face 1 in add_elem_aE_vx." ) ;
    }
    ppVrtx[ kVertFc[kFace] ] = pVxCtr ;
    kFcQuadded[kFace] = 1 ;
    mVxCtr++ ;
  }




      
  /* Do full refinement of all faces that have refinement all around the
     perimeter, either for buffering or anisotropic ref. 
     Case covered already under doBuf, and all vx listed in ppVrtx.
  */
  for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
    pFoE = pElT->faceOfElem + kFace ;
    if ( pFoE->mVertsFace == 4 && !kFcQuadded[kFace] &&
         nAe[ pFoE->kFcEdge[0] ] &&
         nAe[ pFoE->kFcEdge[1] ] &&
         nAe[ pFoE->kFcEdge[2] ] &&
         nAe[ pFoE->kFcEdge[3] ] ) { 
      /* This quad face has all edges existing around its perimeter,
         refine it fully. */
      for ( kVx = 0; kVx < 4 ; kVx++ )
        pVxCrnr[kVx] = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;

      if ( !add_quadFc_aE ( pUns, pVxCrnr, doBuf, &pVxCtr,
                            nFcAe, nCrossAe[kFace], &nFixAe, &fixDir,
                            pRefChunk, ppLstVx, ppLstCoor, ppLstUnknown ) ) {
        hip_err ( fatal, 0, "Failed to add quad face 2 in add_elem_aE_vx." ) ;
      }
      /* Use of ppVrtx for added mid-edge/face needs a fixed posiiton
         pattern index, see e.g. get_drvElem_aE:
         kVxHg[]: the positions of the hanging vertices. [mVerts...mVerts+mEdges-1] are on
         edges, [mVerts+mEdges+1 .... mVerts+mEdges+mFaces] are on faces.
         PhgVx[]: the hanging vertices, listed in order of occurence, that is 0 .. *PmV

     The face vx is not listed with ppVrtx as anisotropic
     refinement may not prescribe this ctr vx, hence does not record a
     kVert position for it. Neither do we have a kVert pos for buffering.

     ppVrtx[ kVertFc[kFace] ] = pVxCtr ; */
      mVxCtr++ ;
    }
  }


  return ( mVxCtr ) ;
}


/******************************************************************************

  add_elem_aE:
  Given an element and a refinement type, add all necessary adapted edges. This
  function is called for refinement and derefinement.
  Given an element and a set of adapted edges, fill the vertices at mid-edge
  and mid-face.
  
  Last update:
  ------------
  30Mar20; zero nCrossAe.
  30Oct19; fix copy&paste bug with kFcEdge for face ctr refinement.
  15Sep19; support vx fill for buffering, in particular called from buf2ref.
  9Sep98; don't add cross edges.
  : conceived.
  
  Input:
  ------
  pUns:         The grid,
  pElem:        the element,
  pRefChunk:    the new chunk containing the new vertices,
  doFaces:      add mid-face vertices, cross edges.
  doBuf:        if non-zero buffering: only temporary edge vx for edges, no periodicity, .
                
  Changes To:   
  -----------   
  ppLstVx:      a pointer to the last vertex used,
  ppLstCoor:    a pointer to the last coordinate used,
  ppLstUnknown: a pointer to the last unknown used,

  Output:
  -------
  ppVrtx:       A list of all vertices on the element in their canonical positions.
  nAe:          list of adapted edges on this element.
  nCrossAe:     list of adapted edges across faces.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_elem_aE_vx ( uns_s *pUns, elem_struct *pElem,
                     const int doFaces, const int doBuf,
                     vrtx_struct *ppVrtx[MAX_VX_ELEM+MAX_ADDED_VERTS ],
                     chunk_struct *pRefChunk, vrtx_struct **ppLstVx,
                     double **ppLstCoor, double **ppLstUnknown,
                     int nAe[MAX_EDGES_ELEM],
                     int nCrossAe[MAX_FACES_ELEM+1][MAX_VX_FACE] ) {
  
  /* Reset the list of adapted elemental edges. */
  int kEdge ;
  for ( kEdge = 0 ; kEdge < MAX_EDGES_ELEM ; kEdge++ )
    nAe[kEdge] = 0 ;
  int kFace, kVert ;
  for ( kFace = 1 ; kFace < MAX_FACES_ELEM+1 ; kFace++ )
    for ( kVert = 0 ; kVert < MAX_VX_FACE ; kVert++ )
      nCrossAe[kFace][kVert] = 0 ;
  
  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;
  const int mVerts = pElT->mVerts, mEdges = pElT->mEdges, *kVxEdge ;
  const edgeVert_s *pEV ;
  const vrtx_struct *pVrtx[2] ;
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  adEdge_s *pAe ;
  
  int kVertEg[MAX_EDGES_ELEM],
    iEdge, fixDir, newEg ;
  vrtx_struct *pVrtxMid, *pPerVxMid, *pVxMid[MAX_VX_FACE] ;
  perVxPair_s vxPair, *pVxPair ;
  int dirSwitch ;
       

  /* Fill the list of parent verts. */
  pElem->mark = 1 ;
  for ( kVert = 0 ; kVert < mVerts ; kVert++ )
    ppVrtx[kVert] = pElem->PPvrtx[kVert] ;


  

  /* Make a list of adapted edges, add the mid-vertices on corner edges. */
  int nAeP[MAX_EDGES_ELEM] ;
  int mAdEdges = add_midEgVx_elem ( pUns, pElem,
                                    nAe, nAeP, kVertEg, doBuf,
                                    ppVrtx, pRefChunk, ppLstVx,
                                    ppLstCoor, ppLstUnknown ) ;

  int mFcVxAdded ;
  if ( mAdEdges && doFaces ) {
    /* Once edges around a face are synchronised between periodic siblings,
       the face state (midVx or not) on either periodic side is fully determined,
       no need to propagate periodic matches here. */
    mFcVxAdded = add_midFcVx_elem ( pUns, pElem,
                                    nAe, nCrossAe, doBuf,
                                    ppVrtx, pRefChunk, ppLstVx,
                                    ppLstCoor, ppLstUnknown ) ;
  }
 
  
  return ( 1 ) ;
}

/******************************************************************************
  match_per_aE: 
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns: grid


  Changes To:
  -----------
  pUns->
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int match_per_aE ( uns_s *pUns ) {

  const vrtx_struct *pVxPer[2] ;
  vrtx_struct *pVx0, *pVx1, *pVxMid, *pVxMidP ;
  int nAe, nAeP ;
  ulong_t mEg ;
  int dir, newEg ;
  perVxPair_s vxPair, *pVxPair ;

  if ( !pUns->pllAdEdge )
    /* There is no list of edges to match. */
    return ( 1 ) ;

  get_number_of_edges ( pUns->pllAdEdge, &mEg ) ;
  
  /* Pair the marks for midedge vertices that are on periodic edges. */
  for ( nAe = 1 ; nAe <= mEg ; nAe++ ) {
    if ( show_edge ( pUns->pllAdEdge, nAe, &pVx0, &pVx1 ) &&
         pVx0->per && pVx1->per &&
         pUns->pAdEdge[nAe].cpVxMid.nr ) {
      /* This edge is periodic and carries a vertex. */
      pVxMid = de_cptVx ( pUns, pUns->pAdEdge[nAe].cpVxMid ) ;

      /* Find the partner edge. */
      vxPair.In = pVx0 ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[0] = ( pVxPair ? pVxPair->Out : NULL ) ;
      
      vxPair.In = pVx1 ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxPer[1] = ( pVxPair ? pVxPair->Out : NULL ) ;
      
      nAeP = add_edge_vrtx ( pUns->pllAdEdge, ( void **) &pUns->pAdEdge,
                             pVxPer, pVxPer+1, &dir, &newEg ) ;
      if ( !nAeP )
        hip_err ( fatal, 0, "could not add equivalent periodic edge"
                  " in match_per_aE.\n" ) ;

      /* Find the equivalent midvertex. */
      vxPair.In = pVxMid ;
      pVxPair = bsearch ( &vxPair, pUns->pPerVxPair, pUns->mPerVxPairs,
                          sizeof ( perVxPair_s ), cmp_perVxPair ) ;
      pVxMidP = ( pVxPair ? pVxPair->Out : NULL ) ;
      if ( !pVxMidP )
        hip_err ( fatal, 0, "could not find equivalent periodic vertex"
                 " in match_per_aE.\n" ) ;

      pUns->pAdEdge[nAeP].cpVxMid = pVxMidP->vxCpt ;
    }
  }

  return ( 1 ) ;
}



/******************************************************************************

  clean_uns_adEdge:
  Remove all unused edges. Used edges fall into two categories:
  - any edge that carries a mid-vertex which is a corner vertex in a terminal
    element is used.
  - any edge that carries a center vertex of a quad face and which is
    referenced as an elemental edge of a terminal element.
  
  Last update:
  ------------
  30Apr19; new interface to reset_vx_mark.
  21Dec14; use new interface to loop_elems.
  4Jan01; delete also the contents of pAdEdge, not just pllEdge when
          returning an edge.
  17Mar00; use the new criterion.
  2Sep98; mark all fixed diagonals as used.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int clean_uns_adEdge ( uns_s *pUns, llEdge_s *pllAdEdge, adEdge_s *pAdEdge ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kFcEdge ;
  vrtx_struct *pVx[4] ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  int nBeg, nEnd, dir, kEg, nAe0, nAeX[2], nAe[MAX_EDGES_ELEM], kFc, kVx, nP ;
  ulong_t mEg ;
  vrtx_struct *pVxMid ;

  /* Reset the usage mark on all vertices. */
  reset_all_vx_mark_k ( pUns, 1 ) ;

  
  /* Reset all edge usage marks. */
  get_number_of_edges ( pllAdEdge, &mEg ) ;
  for ( nAe0 = 1 ; nAe0 <= mEg ; nAe0++ )
    pAdEdge[nAe0].mark = pAdEdge[nAe0].used = 0 ;


  
  /* Mark all elemental edges in terminal elements. Those elemental edges
     that carry a midface vertex will also be needed. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->term ) {
        pElT = elemType + pElem->elType ;

        for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ ) {
          nAe0 = get_elem_edge ( pllAdEdge, pElem, kEg,
                                 (CV**) pVx, (CV**) pVx+1, &dir ) ;
          if ( nAe0 && pAdEdge[nAe0].cpVxMid.nr )
            pAdEdge[nAe0].mark = 1 ;
        }
      }



  
  
  /* Loop over all terminal elements. Mark all corner vertices. We need
     all elemental edges that carry a terminally used vertex. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->term ) {
        pElT = elemType + pElem->elType ;
        /* This is a terminal element. Mark all forming vertices. */
        for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
          pElem->PPvrtx[kVx]->mark = 1 ;
      }


  
  /* Loop over all valid elements. Mark all center vertices on quad faces if
     one of the cross edges is elemental in a terminal element. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid ) {
        pElT = elemType + pElem->elType ;

        /* Find all edges. */
        for ( kEg = 0 ; kEg < pElT->mEdges ; kEg++ )
          nAe[kEg] = get_elem_edge ( pllAdEdge, pElem, kEg,
                                     (CV**) pVx, (CV**) pVx+1, &dir ) ;

          
        /* Loop over all quad faces and mark cross edges that hang. */
        for ( kFc = 1 ; kFc <= pElT->mFaces ; kFc++ ) {
          pFoE = pElT->faceOfElem + kFc ;
          if ( pFoE->mVertsFace == 4 ) {
            kFcEdge = pFoE->kFcEdge ;

            /* Do all four edges around the face exist? */
            if ( nAe[ kFcEdge[0] ] &&
                 nAe[ kFcEdge[1] ] &&
                 nAe[ kFcEdge[2] ] &&
                 nAe[ kFcEdge[3] ] &&
                 pAdEdge[nAe[ kFcEdge[0] ]].cpVxMid.nr &&
                 pAdEdge[nAe[ kFcEdge[1] ]].cpVxMid.nr &&
                 pAdEdge[nAe[ kFcEdge[2] ]].cpVxMid.nr &&
                 pAdEdge[nAe[ kFcEdge[3] ]].cpVxMid.nr ) {
              /* All edges exist and carry a vertex. Look for the cross-edges
                 and mark the center vertex. */
              pVx[0] = de_cptVx ( pUns, pAdEdge[ nAe[ kFcEdge[0] ] ].cpVxMid ) ;
              pVx[1] = de_cptVx ( pUns, pAdEdge[ nAe[ kFcEdge[1] ] ].cpVxMid ) ;
              pVx[2] = de_cptVx ( pUns, pAdEdge[ nAe[ kFcEdge[2] ] ].cpVxMid ) ;
              pVx[3] = de_cptVx ( pUns, pAdEdge[ nAe[ kFcEdge[3] ] ].cpVxMid ) ;
              nAeX[0] = get_edge_vrtx ( pllAdEdge, (CV**) pVx+0, (CV**) pVx+2, &dir ) ;
              nAeX[1] = get_edge_vrtx ( pllAdEdge, (CV**) pVx+1, (CV**) pVx+3, &dir ) ;
              pVxMid = de_cptVx ( pUns, pAdEdge[ nAeX[0] ].cpVxMid ) ;

              if ( pVxMid && ( pAdEdge[ nAeX[0] ].mark || pAdEdge[ nAeX[1] ].mark ) ) {
                /* This is a midvertex on an edge in a terminal element. Needed. */
                pVxMid->mark = 1 ;
                /* Also make sure that the four perimeter vertices are marked. */
                pVx[0]->mark = 1 ;
                pVx[1]->mark = 1 ;
                pVx[2]->mark = 1 ;
                pVx[3]->mark = 1 ;
              }
            }
          }
        }
      }
  
  
  /* Match all the marks between periodic boundaries. This requires the list
     of periodic pairs redone including the newly added ones. */
  for ( nP = 0 ; nP < pUns->mPerVxPairs ; nP++ )
    if ( pUns->pPerVxPair[nP].In->mark )
      pUns->pPerVxPair[nP].Out->mark = 1 ;
    else if ( pUns->pPerVxPair[nP].Out->mark )
      pUns->pPerVxPair[nP].In->mark = 1 ;

  
  /* Return all edges that do not have a marked midvertex. */
  for ( nAe0 = 1 ; nAe0 <= mEg ; nAe0++ )
    if ( show_edge ( pllAdEdge, nAe0, (VX**) pVx, (VX**) pVx+1 ) ) {
      pVxMid = de_cptVx ( pUns, pAdEdge[nAe0].cpVxMid ) ;
      if ( !pVxMid->mark ) {
        del_edge ( pllAdEdge, nAe0 ) ;
        pAdEdge[nAe0].cpVxMid.nCh = 0 ;
        pAdEdge[nAe0].cpVxMid.nr  = 0 ;
      }
    }

  
  return ( 1 ) ;
}

/******************************************************************************

  count_newVx_llAe:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int count_newVx_llAe ( const llEdge_s *pllAdEdge, const adEdge_s *pAdEdge ) {
  
  int mNewVx = 0 ; // To do: convert return type to ulong_t.
  ulong_t nLstEdge, nEg ;
  vrtx_struct *pVx1, *pVx2 ;

  get_number_of_edges ( pllAdEdge, &nLstEdge ) ;

  for ( nEg = 1 ; nEg <= nLstEdge ; nEg++ )
    if ( show_edge ( pllAdEdge, nEg, &pVx1, &pVx2 ) && !pAdEdge[nEg].cpVxMid.nr )
      /* There is an edge without a mid vertex. */
      mNewVx++ ;

  return ( mNewVx ) ;
}

/******************************************************************************

  mark_uns_vertFromAdEdge:
  Loop over all adapted edges and mark all used vertices. This function is
  to be called after mark_uns_vertFromElem, which resets the vertex marks.
  
  Last update:
  4May96: conceived.
  
  Input:
  ------
  pUns
  
  Changes to:
  ----------
  pChunk->Pvrtx->nr
  
*/

void mark_uns_vertFromAdEdge ( uns_s *pUns ) {
  
  int nLstEdge ;
  const adEdge_s *pAdEdge = pUns->pAdEdge, *pAe ;

  if ( !pUns->pllAdEdge )
    return ;

  /* There is a list of adapted edges. f*/
  nLstEdge = get_sizeof_llEdge ( pUns->pllAdEdge ) ; 
  pAdEdge = pUns->pAdEdge ;

  /* Loop over all edges. Mark all mid vertices. */
  for ( pAe = pAdEdge+1 ; pAe <= pAdEdge+nLstEdge ; pAe++ )
    if ( pAe->cpVxMid.nr )
      /* There is a vertex on this edge. */
      ( de_cptVx( pUns, pAe->cpVxMid ) )->number = 1 ;
  
  return ;
}

  
  


/******************************************************************************

  list_adEdge:
  A print function to be passed into the list_edge family.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns: grid with edge data
  pData: non-type pointer to the adaptive edge to list


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void list_adEdge ( const uns_s *pUns, void const * const pData ) {
  
  vrtx_struct *pVx ;
  adEdge_s const * const pAdEdge = pData ;

  if ( pAdEdge->cpVxMid.nr ) {
    pVx = de_cptVx ( pUns, pAdEdge->cpVxMid ) ;
    printf ( " vxMid: %"FMT_ULG" (%"FMT_ULG" in %d ),", pVx->number, pAdEdge->cpVxMid.nr,
             pAdEdge->cpVxMid.nCh ) ;
  }
  else
    printf ( " no vxMid," ) ;

  printf ( " used: %d", pAdEdge->used ) ;
  
  return ;
}

