// Program for licence
// Author: G. Staffelbach  01/09/2011
//

// to compile: 
// /bin/bash gnu*.sh

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cpre.h"
#include "proto.h"


int debug = 0;   // Debug verbose
int end_mon;
int end_year;

// JDM: there is no executable code here, so void statement.
// void flush_buffer ();

ret_s license () {
  ret_s ret = ret_success () ;
  printf("\n       License expires on:  %02d/%04d \n\n", end_mon, end_year); 
  // JDM: is a specific flushing of the output buffer needed?
  // if yes, needs importing the prototypes from read1 lib here.
  // flush_buffer () ;
  return ( ret ) ;
}

ret_s check_valid_license ( void ) {
  ret_s ret = ret_success () ;
  
   time_t tval;
   struct tm *now; 

/* Get current date and time */
   tval = time(NULL);

// Get local time 
   now = localtime(&tval);
   now->tm_year =  now->tm_year + 1900 ; 
   now->tm_mon =  now->tm_mon + 1; 

// End date is in 18 months
   end_mon = now->tm_mon + 6;
   end_year= now->tm_year + 1; 

// CHECK YEAR
   if ( now->tm_year > end_year
        || (( now->tm_mon > end_mon ) &&  ( now->tm_year == end_year )) ) {
     printf("       This exec has expired \n" );
     license();
     printf("       Please contact CERFACS or update from https://inle.cerfacs.fr/projects/hip/files\n\n"); 
     exit ( EXIT_FAILURE ) ; 
   }
   else if ( ( now->tm_mon == end_mon ) &&  ( now->tm_year == end_year ) ) {
     printf("       This exec will expire soon\n" );
     license(); 
     printf("       Please contact CERFACS or update from https://inle.cerfacs.fr/projects/hip/files\n\n"); 
   }
   return ( ret ) ;
 }
