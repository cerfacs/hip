/*
  proto_mb.h:
  All headers of structured methods for prototyping.

*/



/* cut_mb.c. */
int mark_mb_all ( mb_struct *Pmb ) ;
int cut_mb_iso ( mb_struct *Pmb, const double isoVal, const isotype_enum isoType ) ;
int cut_mb_dist ( mb_struct *Pmb, const double cutDist, const int cutType ) ;

int mb_iso ( mb_struct *Pmb, const isotype_enum isoType, const double isoVal ) ;
int aspect_ratio_mb ( const block_struct *Pbl, const int ijk[],
		      const int dir[], const int mDim, 
		      const isotype_enum isoType, const double isoVal,
		      double *PprevAR ) ;

int mb_distance ( int mBlock, block_struct *blockS, const int mDim ) ;

int mb_markDist ( const int mBlocks, block_struct *blockS,
		  const double cutDist ) ;
int mb_markElem ( const int mBlocks, block_struct *blockS, const int mDim, 
		  const int cutType ) ;
int mb_markVert ( mb_struct *Pmb ) ;




/* fix_mb_degenFc.c: */
int mb_degen_subfc ( mb_struct *Pmb ) ;
int is_degen_subfc ( const block_struct *PBL, const subFace_struct *PSF,
		     const int mDim );




/* mb_meth.c. */
block_struct *make_blocks ( int mBlocks  ) ;
int mb_apply_skip ( const char *fcName,
                    const int fullRg[MAX_DIM],
                    const int kDimLo, const int kDimHi,
                    const int skip,
                    int skipRg[MAX_DIM] ) ;
int get_mb_subface ( const block_struct *Pbl, const subFace_struct *Psf,
		     const int mDim, int ll[], int ur[],
		     int *Pindex1, int *PmultVert1, int *PmultCell1,
		     int *Pindex2, int *PmultVert2, int *PmultCell2,
		     int *PoffsetVert, int *PoffsetCell, int *PindexStatic,
		     int *Pdll, int *Pdlr, int *Pdur, int *Pdul ) ;
int get_mb_boundVert ( int index1, int multVert1,
		       int index2, int multVert2, int offsetVert ) ;
int get_mb_boundCell ( int index1, int multCell1,
		       int index2, int multCell2, int offsetCell ) ;
int get_mb_boundFace ( int n1, int mult1,
		       int n2, int mult2, int offset,
		       int dll, int dlr, int dur, int dul,
		       int *pnFcVert ) ;

int get_faceNr_ijk ( int indexStatic, int indexValue, int mDim ) ;

void get_ijk_nElem ( const int mVert[], int mCellsBlock,
		     int nCell, const int mDim, int ijk[] ) ;
void get_ijk_nVert ( int mVert[], int mVertTotal, int nVert, int mDim, int ijk[] ) ;

int get_nElem_ijk ( const int mDim, const int *Pijk, const int *PIJK ) ;
int get_nVert_ijk ( const int mDim, const int *Pijk, const int *PIJK ) ;

int check_nElem_ijk ( const int mDim, const int ijk[], const int IJK[] ) ;
int check_nVert_ijk ( const int mDim, const int ijk[], const int IJK[] ) ;

void free_mb ( mb_struct **PPmb ) ;

int cell_loop_subfc ( const int ll[], const int ur[], const int mDim,
		      int *Pn1, const int index1, const int multCell1,
		      int *Pn2, const int index2, const int multCell2,
		      const int offsetCell ) ;
int face_loop_block ( const int mDim, const int mVert[],
		      int ijk[], int *Pindex1, int *Pindex2, int *PnDim, int *Pdcell ) ;

int put_mb_subFc ( block_struct *Pbl, subFace_struct *Psf ) ;

int mb_count ( mb_struct *Pmb ) ;
int mb_size ( mb_struct *Pmb ) ;
int mb_bb ( mb_struct *Pmb ) ;

int get_static_subface ( const subFace_struct *Psf, const int mDim, const int side, 
			 int *PstaticDim, int *PstaticDir, int *PfreeDir1, int *PfreeDir2 ) ;

int mb_fix_degen_subfc ( subFace_struct *Psf, const int mDim,
			 int freeDir1, double hMin1, double hMax1,
			 int freeDir2, double hMin2, double hMax2 ) ;

double get_block_hMinSq ( block_struct *pBl, const int mDim ) ;
double get_mb_hMinSq  ( mb_struct *pMb ) ;


/*mb_ngh.c:*/
  int get_mb_ngh_node ( block_struct **PPbl, int ijk[], int dir[],
	  	        const int mDim ) ;
  int get_mb_ngh_cell ( block_struct **PPbl, int ijk[], int dir[],
		        const int mDim ) ;

  int find_mb_subFc_node ( const block_struct *Pbl, const int ijk[],
			   const int mDim, const int staticDim, const int staticDir, 
			   subFace_struct **PPsubFc ) ;
  int find_mb_subFc_cell ( const block_struct *Pbl, const int ijk[],
			   const int mDim, const int staticDim, const int staticDir, 
			   subFace_struct **PPsubFc ) ;

/* mb_rot.c. */
void init_mb () ;
rotation_struct *find_rot_123 ( const int *blTr, int mDim ) ;     

rotation_struct *find_rot( char rotChar[], int mDim,
			   rotation_struct *rotationS ) ;
rotation_struct *find_rot_ijk ( subFace_struct *Psf, int mDim ) ;

void trans_l2r( const int ijkL[MAX_DIM],
	        int rotMatrix[MAX_DIM][MAX_DIM], const int offset[MAX_DIM],
	        int ijkR[MAX_DIM] ) ;
void trans_r2l( const int ijkR[MAX_DIM],
	        int rotMatrix[MAX_DIM][MAX_DIM], const int offset[MAX_DIM],
	        int ijkL[MAX_DIM] ) ;
void get_mb_elemShift ( subFace_struct *PSF, const int mDim ) ;
void get_mb_vertShift ( subFace_struct *PSF, const int mDim ) ;



/* mb_bc.c: */
void mb_bcSubFc ( block_struct *blockS, int mBlocks ) ;
void mb_bcBox ( const int mDim ) ;
void list_mb_bc ( const grid_struct *Pgrid ) ;




/* write_mb_avbp.c. */
int write_mb_avbp ( const mb_struct *Pmb, char *ProotFile ) ;

int write_mb_coor ( const int mBlocks, block_struct *blockS,
		    const int mDim, const int mEqu, char *PmeshFile ) ;
int write_mb_conn ( const int mBlocks, block_struct *blockS,
		    const int mDim, char *PconnFile ) ;
int write_mb_bound ( const int mBlocks, block_struct *blockS,
		     const int mDim, char *PboundFile ) ;
int write_mb_bound_cond ( const int mBlocks, block_struct *blockS,
			  char *PboundFile ) ;
int write_mb_int_bound ( const int mBlocks, char *PboundFile ) ;
int write_mb_sol ( const int mBlocks, block_struct *blockS,
	           const int mEqu, char *PsolFile ) ;

/* write_mb_cut.c */
int write_mb_pts ( const mb_struct *Pmb, FILE *FptsOut, const int writeKeepNotCut ) ;


/* read_mb_cfdrc.c */
int read_mb_cfdrc ( char *PibcFile, char *PcoorFile, char coorType,
                    const int skip, const int withBlanking ) ;
int read_mb_plot3d ( mb_struct *Pmb, char* PcoorFile, const char coorType,
		     const int skip, const int withBlanking ) ;

/* 
Moved to proto_mb_uns.h
read_mb_cgns.c
int read_mb_cgns ( char *gridFile, char *solFile, const int skip, const int kBlock ) ;
*/

/* read_mb_damas.c: */
int read_mb_damas ( const char *PdmsFile, const int nSkip ) ;
int read_mb_doc_damas ( mb_struct *Pmb, const int unitDms ) ;
int read_mb_coor_damas ( mb_struct *Pmb, const int nSkip, const int unitDms ) ;

int get_llur_damas ( int kFace, int ijFree[], int ijkOrg[], int mVertFace[],
		     int mDim, int ll[], int ur[] ) ;
int get_rotChar_damas ( int kFaceL, int ijFreeL[], int kFaceR, int ijFreeR[], int mDim,
		        char *ProtChar ) ;
int mb_skip ( mb_struct *Pmb, const int nSkip ) ;


/* read_mb_flower.c */
int read_mb_flower ( char *PlogicFile, char *PcoorFile, char coorType, int skip ) ;



/* read_str_dpl.c */
int read_str_dpl ( FILE *FdplIn ) ;


/* write_mb_cut.c: */
int write_str_pts ( const mb_struct *Pmb, FILE *FptsOut ) ;

