/*
   write_avbp.c
   Write an unstructured, multichunked grid as single block into an
   avbp format. The routines writing the boundary files are in
   write_uns_avbp_bound.c.
 
   Last update:
   ------------
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   15Mar06; switch to writing of tpf 2.0 file formats.
   1Apr05; intro elGraph, change length of ident string.
   19Sep2; intro 5.1.
   9Jan01; write separate record for each nadd.
   30Oct00; proper ini for restart, use all variable fields.
   22Nov96; cut out boundary routines.
   4May96; conceived.

   Contains:
   ---------
   write_uns_avbp:
   write_avbp_conn:
   write_avbp_coor:
   write_avbp_sol:
   
*/

#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const char version[] ;
extern char hip_msg[] ;
extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const int perBc_in_exBound ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;

/****************************************************************

  write_avbp_elGraph:
  Write a CSR style element graph to file for METIS paritioning.

  Last update:
  ------------
  6Apr13; promote possibly large int to type ulong_t.
  3Feb06; change output lists.
  1Apr05; conceived.

  Input:
  ------
  pUns:
  PconnFile:
 
  Changes to:
  -----------
  *PconnFile:

*/

static int write_avbp_elGraph ( uns_s *pUns, const char *pGraphFile ){

  ulong_t mXAdj, *pXAdj, mAdjncy, *pAdjncy, buffer[4], i ;
  FILE *graphFile ;

  /* Open input file. */
  if ( ( graphFile = fopen ( pGraphFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", pGraphFile ) ;
    return ( 0 ) ; }


  if ( verbosity > 2 )
    printf ( "      element graph to %s\n", pGraphFile ) ;

  make_elGraph ( pUns, &mXAdj, &pXAdj, &mAdjncy, &pAdjncy ) ;

  /* Counters. */
  buffer[0] = 2*sizeof( int ) ;
  buffer[1] = mXAdj ;
  buffer[2] = mAdjncy ;
  buffer[3] = buffer[0] ;
  FWRITE ( buffer, sizeof( int ), 4, graphFile ) ;


  /* Index: xXAdj. Note: there is one extra entry for the end.*/
  buffer[0] = (mXAdj+1)*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, graphFile ) ;
  FWRITE ( pXAdj, sizeof( int ), mXAdj+1, graphFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, graphFile ) ;


  /* Graph: adjncy. */
  buffer[0] = mAdjncy*sizeof( int ) ;
  FWRITE ( buffer, sizeof( int ), 1, graphFile ) ;
  for ( i = 1 ; i < 2*mAdjncy ; i+=2 )
    FWRITE ( pAdjncy+i, sizeof( int ), 1, graphFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, graphFile ) ;
  
  fclose ( graphFile ) ;

  arr_free ( pXAdj ) ;
  arr_free ( pAdjncy ) ;


  return ( 1 ) ;
}

/****************************************************************

  write_avbp_conn:
  Write the mesh connectivity of a chunked unstructured mesh to AVBP.
  The elements are renumbered in the sequence with a lexicographic
  ordering of element types.

  Last update:
  ------------
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  3Dec09; write record lengths as unsigned int.
  30Jul98; cleanup, suppress buffered output.
  5May96: conceived..

  Input:
  ------
  pUns:
  PconnFile:
 
  Changes to:
  -----------
  *pUns->Pelem->number:
  *PconnFile:

*/

int write_avbp_conn ( uns_s *pUns, const char *PconnFile ){

  FILE *connFile ;
  unsigned int rLen ;
  int buffer[MAX_VX_ELEM], mElems, mVerts, mElemsWritten,
      mElemType[MAX_ELEM_TYPES] = {0}, mTypes, nVert, mElemsChunk ;
  elType_e elType ;
  chunk_struct *Pchunk ;
  elem_struct *pElem ;

  if ( verbosity > 2 )
    printf ( "      connectivity to %s\n", PconnFile ) ;

  /* Open input file. */
  if ( ( connFile = fopen ( PconnFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PconnFile ) ;
    return ( 0 ) ; }
  
  /* Count the total number of elements and the number of
     the different type of elements. */
  mElems = 0 ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
    mElems += Pchunk->mElemsNumbered ;
    mElemsChunk = 0 ;
    for ( pElem = Pchunk->Pelem+1 ;
	  pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
      if ( pElem->number ) {
	mElemType[ pElem->elType ]++ ;
	mElemsChunk++ ; }
    if ( mElemsChunk != Pchunk->mElemsNumbered ) {
      sprintf ( hip_msg, "invalid element count in chunk %d: %d found %"FMT_ULG" expected.\n",
	       Pchunk->nr, mElemsChunk, Pchunk->mElemsNumbered ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  /* Count the number of element types. */
  mTypes = 0 ;
  for ( elType = tri ; elType <= hex ; elType++ )
    if ( mElemType[elType] )
      mTypes++ ;

  /* Number of blocks, cells, layers of dummy cells (0). */
  buffer[1] = 1 ;
  buffer[2] = mElems ;
  buffer[3] = 0 ;
  rLen = 3*sizeof( int ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;
  FWRITE ( buffer+1, sizeof( int ), 3, connFile ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;


  /* k=1 loop, interior cells. */
  /* Number of dummy cells, number of different element types. */
  /* number of dummy cells. */
  buffer[1] = 0 ;
  /* number of different element types. */
  buffer[2] = mTypes ;
  rLen = 2*sizeof( int ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;
  FWRITE ( buffer+1, sizeof( int ), 2, connFile ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;

  /* Loop over all element types. Renumber them in the sequence written
     to find back the proper face to element pointers in write_avbp_bound.*/
  mElemsWritten = 0 ;
  for ( elType = tri ; elType <= hex ; elType++ )
    if ( mElemType[elType] ) {
      mVerts = elemType[elType].mVerts ;
      
      /* Number of vertices per element. */
      buffer[1] = mVerts ;
      buffer[2] = mElemsWritten + 1 ;
      buffer[3] = mElemType[elType] ;
      rLen = 3*sizeof( int ) ;
      FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;
      FWRITE ( buffer+1, sizeof( int ), 3, connFile ) ;
      FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;

      /* Start the next record. */
      rLen = elemType[elType].mVerts*mElemType[elType]*sizeof( int ) ;
      FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;

      for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
	for ( pElem = Pchunk->Pelem+1 ;
	      pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ )
	  if ( elemType[pElem->elType].mVerts == mVerts && pElem->number ) {
            /* This element is marked and of the type sought. */
	    pElem->number = ++mElemsWritten ;
	    
	    for ( nVert = 0 ; nVert < mVerts ; nVert++ )
	      buffer[nVert] = pElem->PPvrtx[nVert]->number ;
            FWRITE ( buffer, sizeof( int ), mVerts, connFile ) ;
	  }

      /* Trailing length of the record. */
      FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;
    }

  if ( mElemsWritten != mElems )
    printf ( " FATAL: %d total elements expected, %d found.\n",
	     mElems, mElemsWritten ) ;
    
  /* k=2 loop, dummy cells. */
  /* Number of dummy cells, number of different element types = 0. */
  buffer[1] = 0 ;
  buffer[2] = 0 ;
  buffer[3] = 0 ;
  rLen = 3*sizeof( int ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;
  FWRITE ( buffer+1, sizeof( int ), 3, connFile ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;


  /* Zero length record of empty cells. */
  rLen = 0 ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;
  FWRITE ( &rLen, sizeof( unsigned int ), 1, connFile ) ;

  fclose ( connFile ) ;
  return ( 1 ) ;
}

/****************************************************************

  write_avbp_coor:
  Write the mesh coordinates of a chunked unstructured mesh to AVBP.

  Last update:
  5May96: conceived..

  Input:
  ------
  pUns:
  PcoorFile:
 
  Changes to:
  -----------
  *PcoorFile:

*/

int write_avbp_coor ( uns_s *pUns, const char *PcoorFile ) {
  
  FILE *coorFile ;
  int buffer[IO_STREAM_LEN/4], buffFill, mVertsNumbered, nDim ;
  chunk_struct *Pchunk ;
  double dbuffer[IO_STREAM_LEN/8] ;
  volatile double *Pdbuffer ;
  vrtx_struct *Pvrtx ;

  if ( verbosity > 2 )
    printf ( "      coordinates to %s\n", PcoorFile ) ;

  if ( ( coorFile = fopen ( PcoorFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PcoorFile ) ;
    return ( 0 ) ; }

  /* Dimensions, equations. */
  buffer[0] = 2*sizeof( int ) ;
  buffer[1] = pUns->mDim ;
  /* The number of flow variables. Legacy item. */
  buffer[2] = pUns->mDim+2 ;
  buffer[3] = buffer[0] ;

  buffer[4] = 2*sizeof( int ) ;
  /* Number of blocks. */
  buffer[5] = 1 ;
  /* Number of vertices. */
  mVertsNumbered = 0 ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    mVertsNumbered += Pchunk->mVertsNumbered ;
  buffer[6] = mVertsNumbered ;
  buffer[7] = buffer[4] ;

  buffer[8] = 3*sizeof( int ) ;
  buffer[9] = 1 ;
  buffer[10] = mVertsNumbered ;
  buffer[11] = 0 ;
  buffer[12] = buffer[8] ;

  /* Length of the record of coordinates. */
  buffer[13] = mVertsNumbered * pUns->mDim * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 14, coorFile ) ;
  
  Pdbuffer = dbuffer ;
  /* One stream of coordinates, first x, then y, z. */
  for ( nDim = 0 ; nDim < pUns->mDim ; nDim++ )
    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
      for ( Pvrtx = Pchunk->Pvrtx + 1 ;
	    Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++)
	if ( Pvrtx->number ) {
	  *(++Pdbuffer) = Pvrtx->Pcoor[nDim] ;
      
	  if ( ( buffFill = Pdbuffer - dbuffer ) > IO_STREAM_LEN/8 - 2 ) {
	    FWRITE ( dbuffer+1, sizeof( double ), buffFill, coorFile ) ;
	    Pdbuffer = dbuffer ; }
	}
  
  /* Remaining coordinates. */
  if ( ( buffFill = Pdbuffer - dbuffer) > 0 )
    FWRITE ( dbuffer+1, sizeof( double ), buffFill, coorFile ) ;
  
  /* Trailing recordlength. */
  buffer[13] = mVertsNumbered * pUns->mDim * sizeof( double ) ;
  FWRITE ( buffer+13, sizeof( int ), 1, coorFile ) ;

  fclose ( coorFile ) ;
  return ( 1 ) ;
}

/****************************************************************

  write_avbp_meansol:
  Write the averaged solution of an unstructured chunked mesh to an
  AVBP format.


  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  2Feb04; conceived
  
  Input:
  ------
  mEqu:       number of equations to write, filled with 1s.
  PsolFile:   
  
  Changes to:
  -----------
  *PsolFile
  
*/

static int write_avbp_meansol ( uns_s *pUns, const char *PsolFile, 
                                const avbpFmt_e avbpFmt ) {
  
  FILE *solFile ;
  const int mEq = pUns->varList.mUnknowns, mVx = pUns->mVertsNumbered, lLen = 80 ;

  int buffer[3], nEq, nBeg, nEnd ;
  double dbuffer[3] ;
  char string[TEXT_LEN] ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;

  
  if ( pUns->varList.varType == noVar )
    /* No solution. Don't write the file. */
    return ( 1 ) ;


  if ( verbosity > 2 )
    printf ( "      averaged solution to %s\n", PsolFile ) ;

  if ( ( solFile = fopen ( PsolFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened in write_avbp_meansol.c.\n",
	     PsolFile ) ;
    return ( 0 ) ; }


  /* Identification string. */
  sprintf ( string, " AVBP %s   ", avbpFmtStr[avbpFmt] ) ;

  /* Skip the trailing \0. */
  buffer[0] = buffer[1] = strlen( string ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
  FWRITE ( string, 1, buffer[0], solFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;


  

  /* niter, mVx, dtsum. */
  buffer[0] = 2*sizeof( int ) + sizeof( double ) ;
  buffer[1] = pUns->restart.avbp.itno ;
  buffer[2] = mVx ;
  FWRITE ( buffer, sizeof( int ), 3, solFile ) ;
  dbuffer[0] = pUns->restart.avbp.dtsum ;
  FWRITE ( dbuffer, sizeof( double ), 1, solFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
  
  /* mEq */
  buffer[0] = buffer[2] = sizeof ( int ) ;
  buffer[1] = mEq ;
  FWRITE ( buffer, sizeof( int ), 3, solFile ) ;

  /* dt_avg. */
  buffer[0] = sizeof ( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
  dbuffer[0] = pUns->restart.avbp.dt_av ; 
  FWRITE( dbuffer, sizeof( double ), 1, solFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;





  /* Names for each unknown. */
  buffer[0] = lLen*sizeof ( char ) ;
  for ( nEq = 0 ; nEq < mEq ; nEq++ ) {
     strncpy(  string, pUns->varList.var[nEq].name, lLen ) ;
     fwrite_string ( solFile, string, lLen ) ;
  }


  
  /* Opening length of the record of solution values. Flow variables first. */
  buffer[0] = mVx * mEq * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;

  for ( nEq = 0 ; nEq < mEq ; nEq++ ) {
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( pVrtx->number )
          FWRITE ( pVrtx->Punknown+nEq, sizeof( double ), 1, solFile ) ;
  }

  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;

  
  fclose ( solFile ) ;
  return ( 1 ) ;
}



/****************************************************************

  write_avbp_sol.c:
  Write the solution of an unstructured chunked mesh to an
  AVBP format.

  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  14Sep06; recover variable categories from non-avbp solutions.
  15Mar06; switch to writing of tpf 2.0 file formats.
  22Apr04; add t5.1
  2Feb04; add v5.3.
  18Jun03; set neq2pf to zero for non-AVBP.
  7Sep00; write all the headers for a restart from pUns->restart.
  1Dec99; new 4.2 solution format.
  8May96: derived from mb.
  
  Input:
  ------
  mEqu:       number of equations to write, filled with 1s.
  PsolFile:   
  
  Changes to:
  -----------
  *PsolFile
  
*/

int write_avbp_sol ( uns_s *pUns, const char *PsolFile, avbpFmt_e avbpFmt ) {
  
  const int mDim = pUns->mDim, mEqu = pUns->varList.mUnknowns ;
  FILE *solFile ;
  int buffer[99], mVx = pUns->mVertsNumbered, nEqu, nBeg, nEnd,
    mEqF, mEqS, nreac, neqt, nadd, neq2pf, nadd_tpf=0, neqfic, k, 
    mEqK[SZ_mEqK], mK, mEqX, i, nPb ;
  double dbuffer[9] ;
  chunk_struct *pChunk ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  varList_s *pVL = &(pUns->varList) ;
  char versionString[TEXT_LEN] ;

  
  if ( pUns->varList.varType == noVar )
    /* No solution. Don't write the file. */
    return ( 1 ) ;
  else if ( mEqu < mDim+ 2 ) {
    /* Need at least a set of flow variables. */
    printf ( " FATAL: the avbp solution format requires at least %d variables"
             " in %d dim, rather than %d.\n", mDim+2, mDim, mEqu ) ;
    return ( 0 ) ;
  }

  if ( pUns->restart.avbp.iniSrc == 3 ) {
    /* This is an averaged solution. */
    return ( write_avbp_meansol ( pUns, PsolFile, avbpFmt ) ) ;
  }
  else if ( pUns->restart.avbp.iniSrc == 4 ||
            pUns->restart.avbp.iniSrc == 5 ) {
    /* Two phase flow. Also write input t5.1 as t2.0 */
    avbpFmt = t2_0 ;  
  }

  if ( pUns->restart.avbp.iniSrc ) {
    nreac    = pUns->restart.avbp.nreac ;
    neqfic   = pUns->restart.avbp.neqfic ;
    neqt     = pUns->restart.avbp.neqt ;
    nadd     = pUns->restart.avbp.nadd ;
    neq2pf   = pUns->restart.avbp.neq2pf ;
    nadd_tpf = pUns->restart.avbp.nadd_tpf ;

    mEqF = mDim + 2 ;
    mEqS = MAX( 0, mEqu - mEqF - neqfic - 2*nreac - neqt - nadd - neq2pf ) ;
  }
  else {
    mEqF   = mEqS = 0 ;
    nreac  = 0 ;
    neqfic = 0 ;
    neqt   = 0 ;
    nadd   = 0 ;
    neq2pf = 0 ;
    nPb    = 0 ;
    /* Loop over all equ, count types and assume they are in order. */
    for ( k = 0 ; k < mEqu ; k++ )
      switch ( pVL->var[k].cat ) {
      case ns : mEqF++ ; break ;
      case species : mEqS++ ; break ;
      case rrates : nreac++ ; break ;
      case tpf : neq2pf++ ; break ;
      case rans : neqt++ ; break ;
      case add : nadd++ ; break ;
      case mean : nPb++ ; break ;
      case fictive : neqfic++ ; break ;
      case add_tpf : nPb++ ; break ;
      case noCat : nPb++ ; break ;
      default : hip_err ( fatal, 0, "this shouldn't have happened in write_avbp_sol" ) ;
    }

    if ( nPb == mDim+2 ) {
      /* Assume all are NS. */
      mEqF = nPb ;
      nPb = 0 ;
    }

    if ( mEqF != mDim+2 ) {
      sprintf ( hip_msg, "looking for %d flow vars, found %d.\n", mDim+2, mEqF ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    else if ( nPb ) {
      sprintf ( hip_msg, "found %d unwriteable variables (mean, add_tpf, noCat).\n", 
                nPb ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  }
    

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "solution to %s\n", PsolFile ) ;
    hip_err ( info, 3, hip_msg ) ;
  }

  if ( ( solFile = fopen ( PsolFile, "w" ) ) == NULL ) {
    sprintf ( hip_msg, "file: %s could not be opened in write_avbp_sol.c.\n",
	      PsolFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;}

  /* We want conservative variables. */
  conv_uns_var ( pUns, cons ) ;
  

  /* Identification string. */
  sprintf ( versionString, " AVBP %s    ", avbpFmtStr[avbpFmt] ) ;

  /* Skip the trailing \0. */
  buffer[0] = buffer[1] = strlen( versionString ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
  FWRITE ( versionString, 1, buffer[0], solFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;


  

  /* niter, mVx, dtsum. */
  buffer[0] = 2*sizeof( int ) + sizeof( double ) ;
  buffer[1] = ( pUns->restart.avbp.iniSrc ? pUns->restart.avbp.itno : 0 ) ;
  buffer[2] = pUns->mVertsNumbered ;
  FWRITE ( buffer, sizeof( int ), 3, solFile ) ;
  dbuffer[0] = ( pUns->restart.avbp.iniSrc ? pUns->restart.avbp.dtsum : 0. ) ;
  FWRITE ( dbuffer, sizeof( double ), 1, solFile ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
  

  if ( avbpFmt == v5_1 ) {
    /* neq, neqs, nreac, neqt, nadd, neq2pf. */
    buffer[0] = buffer[7] = 6*sizeof( int ) ;
    buffer[1] = mEqF ;
    buffer[2] = mEqS ;
    buffer[3] = nreac ;
    buffer[4] = neqt  ;
    buffer[5] = nadd  ;
    buffer[6] = neq2pf  ;
    FWRITE ( buffer, sizeof( int ), 8, solFile ) ;
  }
  else if ( avbpFmt == t2_0 ) {
    /* neq, neqs, neq2pf. */
    i = 1 ;
    buffer[i++] = mEqF   ;
    buffer[i++] = mEqS   ;
    buffer[i++] = neqfic ;
    buffer[i++] = neq2pf ;
    buffer[i++] = nreac  ;
    buffer[i++] = neqt   ;
    buffer[i++] = nadd   ;
    buffer[i++] = pUns->restart.avbp.ithick         ;
    buffer[i++] = pUns->restart.avbp.iles           ;
    buffer[i++] = pUns->restart.avbp.ichem          ;
    buffer[i++] = pUns->restart.avbp.iavisc         ;
    buffer[i++] = pUns->restart.avbp.ipenalty       ;
    buffer[i++] = pUns->restart.avbp.iwfles         ;
    buffer[i++] = pUns->restart.avbp.istoreadd      ;
    buffer[i++] = pUns->restart.avbp.nadd_tpf       ;
    buffer[i++] = pUns->restart.avbp.istoreadd_tpf  ;
    buffer[i++] = pUns->restart.avbp.iavisc_tpf     ;
    buffer[i++] = pUns->restart.avbp.ipenalty_tpf   ;
    buffer[i++] = pUns->restart.avbp.nvar_evap      ;
    buffer[i++] = pUns->restart.avbp.nvar_sigma     ;
    buffer[i++] = pUns->restart.avbp.nvar_qb        ;

    buffer[0] = buffer[i] = (i-1)*sizeof( int ) ;
    FWRITE ( buffer, sizeof( int ), i+1, solFile ) ;
  }
  else if ( avbpFmt == t5_1 ) {
    /* Should be obsolete. neq, neqs, neq2pf. */
    buffer[0] = buffer[17] = 16*sizeof( int ) ;
    buffer[1] = mEqF ;
    buffer[2] = mEqS ;
    buffer[3] = neq2pf  ;

    for ( k = 4 ; k < 17 ; k++ )
      buffer[k] = 0 ;

    FWRITE ( buffer, sizeof( int ), 18, solFile ) ;
  }
  else  {
    /* neq, neqs, neqfic, nreac, neqt, nadd. */
    buffer[0] = buffer[7] = 6*sizeof( int ) ;
    buffer[1] = mEqF ;
    buffer[2] = mEqS ;
    buffer[3] = neqfic ;
    buffer[4] = nreac ;
    buffer[5] = neqt  ;
    buffer[6] = nadd  ;
    FWRITE ( buffer, sizeof( int ), 8, solFile ) ;
  }


  
  /* mach, gamma-1, Freestream variables. */
  buffer[0] = ( 2 + mDim+2 ) * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
  
  if ( pUns->restart.avbp.iniSrc ) {
    dbuffer[0] = pUns->restart.avbp.ma_cp ;
    dbuffer[1] = pUns->restart.avbp.gmm1 ; 
  } else {
    dbuffer[0] = 0. ;
    dbuffer[1] = GammaM1 ; 
  }
  for ( nEqu = 0 ; nEqu < mDim+2 ; nEqu++ )
    dbuffer[nEqu+2] = pUns->varList.freeStreamVar[nEqu] ;
  FWRITE ( dbuffer, sizeof( double ), mDim+4, solFile ) ;
  
  buffer[0] = ( 2 + mDim+2 ) * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;

  

  
  /* Opening length of the record of solution values. Flow variables first. */
  buffer[0] = (mDim+2) * mVx * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;

  for ( nEqu = 0 ; nEqu < mDim+2 ; nEqu++ ) {
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( pVrtx->number )
          FWRITE ( pVrtx->Punknown+nEqu, sizeof( double ), 1, solFile ) ;
  }

  buffer[0] = (mDim+2) * mVx * sizeof( double ) ;
  FWRITE ( buffer, sizeof( int ), 1, solFile ) ;


  /* Additional variables. */
  if ( avbpFmt == v5_3 ) {
    /* 5.1 */
    mEqX = 0 ;
    mEqK[mEqX++] = mEqS ;
    mEqK[mEqX++] = neqt ;
    mEqK[mEqX++] = neqfic ;
    mEqK[mEqX++] = nreac ;   /* Reaction rates are forward and backward. */
    mEqK[mEqX++] = nreac ;
    for ( i = 0 ; i < nadd ; i++ )
      mEqK[mEqX++] = 1 ;
  }
  else if ( avbpFmt == t2_0 ) {
    /* tpf 2.0 */
    mEqX = 0 ;
    mEqK[mEqX++] = mEqS ;
    mEqK[mEqX++] = neqfic ;
    mEqK[mEqX++] = neq2pf ;
    mEqK[mEqX++] = neqt ;
    for ( i = 0 ; i < nadd ; i++ )
       /* Additional vars written in a record each. */
      mEqK[mEqX++] = 1 ;
    for ( i = 0 ; i < nadd_tpf ; i++ )
       /* Additional vars written in a record each. */
      mEqK[mEqX++] = 1 ;
  }
  else if ( avbpFmt == t5_1 ) {
    /* t5.1 */
    mEqX = 2 ;
    mEqK[0] = mEqS ;
    mEqK[1] = neq2pf ;
  }
  else {
    /* 4.7. */
    mEqX = 3 ;
    mEqK[0] = mEqS ;
    mEqK[1] = nreac ;
    mEqK[2] = neqt ;
  }
  
  mK = mEqF ;
  for ( k = 0 ; k < mEqX ; k++ )
    if ( mEqK[k] ) {
      buffer[0] = mEqK[k]*mVx*sizeof( double ) ;
      FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
        
      for ( nEqu = mK ; nEqu < mK+mEqK[k] ; nEqu++ ) {
        pChunk = NULL ;
        while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
          for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
            if ( pVrtx->number )
              FWRITE ( pVrtx->Punknown+nEqu, sizeof( double ), 1, solFile ) ;
      }
    
      FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
      mK += mEqK[k] ;
    }
    
  /* Now dealt with in the loop above. 
  for ( k = 0 ; k < nadd ; k++ ) {
    buffer[0] = mVx*sizeof( double ) ;
    FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
        
    pChunk = NULL ;
    while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
      for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
        if ( pVrtx->number )
          FWRITE ( pVrtx->Punknown+mK, sizeof( double ), 1, solFile ) ;
    
    FWRITE ( buffer, sizeof( int ), 1, solFile ) ;
    mK ++ ;
    } */

  
  fclose ( solFile ) ;
  return ( 1 ) ;
}

/******************************************************************************

  write_uns_avbp:
  Write the un-chunked consistent mesh to avbp files. Format: AVBP 4.2.
  
  Last update:
  4May96: conceived.
  
  Input:
  ------
  pUns:
  mDim:         
  
  Returns:
  --------
  1
  
*/

int write_avbp ( uns_s *pUns, char *pRootFile, const char keyword[]  ) {
  
  char outFile[MAX_BC_CHAR+15] ;
  FILE *visFile = NULL ;
  avbpFmt_e avbpFmt ;
  int doGraph = 0 ;

  
  if ( !strncmp( keyword, "avad", 5 ) )
    return ( write_avbp4 ( pUns, pRootFile ) ) ;

  else if ( !strncmp( keyword, "avbp4.2", 7 ) )
    avbpFmt =  v4_2 ;

  else if ( !strncmp( keyword, "avbp4.7", 7 ) )
    avbpFmt =  v4_7 ;

  else if ( !strncmp( keyword, "avbp5.1", 7 ) )
    avbpFmt =  v5_1 ;

  else if ( !strncmp( keyword, "avbp5.3eg", 9 ) ) {
    /* Write also the element graph. */
    avbpFmt =  v5_3 ;
    doGraph = 1 ;
  }
  else if ( !strncmp( keyword, "avbp5.3", 7 ) )
    avbpFmt =  v5_3 ;

  else if ( !strncmp( keyword, "avh", 3 ) )
    avbpFmt =  v6_0 ;



  else
    avbpFmt =  DEFAULT_avbpFmt ; ;



  if ( verbosity > 0 )
    printf ( " in AVBP %s format to %s\n", avbpFmtStr[avbpFmt], pRootFile ) ;




  if ( !pUns->validGrid ) {
    printf ( " FATAL: you were told that this grid is invalid, weren't you?.\n" ) ;
    /* return ( 0 ) ; */ }
  if ( ( check_bnd_setup ( pUns ).status != success ) ) {
    printf ( " FATAL: cannot write grid without proper boundary setup.\n" ) ;
    return ( 0 ) ; }

  
  /* In any-D, write a 'master-file' for visual. */
  strcpy ( outFile, pRootFile ) ;
  strcat ( outFile, ".visual" ) ;
  if ( !( visFile = fopen ( outFile, "w" ) ) ) {
    printf ( " FATAL: file: %s could not be opened.\n", outFile ) ;
    return ( 0 ) ; }
  else
    fprintf ( visFile,
	      "' Masterfile for AVBP %s by hip version %3s.'\n",
              avbpFmtStr[avbpFmt], version ) ;
  
  /* Number the leaf elements only. Force a recount */
  pUns->numberedType = invNum ;
  number_uns_elem_leafs ( pUns ) ;

  if ( !special_verts ( pUns ) ) {
    printf ( " FATAL: failed to match periodic vertices in write_avbp.\n" ) ;
    return ( 0 ) ; }

  /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
     for the current numbering of elements. */
  count_uns_bndFaces ( pUns ) ;
  /* Remove all periodic bc, if they are not supposed to be listed. */
  if ( !perBc_in_exBound ) rm_perBc ( pUns ) ;

  /* Make pairs of periodic nodes and faces. */
  if ( ! match_per_faces ( pUns ) ) {
    printf ( " FATAL: failed to establish periodicity in write_avbp.\n" ) ;
    return ( 0 ) ; }

  
  strcpy ( outFile, pRootFile ) ;
  if ( avbpFmt == v6_0 ) {
    /* hdf5 solution file. */
    strcat ( outFile, ".sol.h5" ) ;
    fprintf ( visFile, "'%s'\n", outFile ) ;
    /* Write standard format only. */
    write_hdf5_sol ( pUns, outFile ) ;
  }
  else {
    strcat ( outFile, ".sol" ) ;
    fprintf ( visFile, "'%s'\n", outFile ) ;
    write_avbp_sol ( pUns, outFile, avbpFmt ) ;
  }

  strcpy ( outFile, pRootFile ) ;
  strcat ( outFile, ".coor" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_coor ( pUns, outFile ) ;

  strcpy ( outFile, pRootFile ) ;
  strcat ( outFile, ".conn" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_conn ( pUns, outFile ) ;
  
  strcpy ( outFile, pRootFile ) ;
  strcat ( outFile, ".exBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_exBound ( pUns, outFile ) ;
  
  strcpy ( outFile, pRootFile ) ;
  strcat ( outFile, ".inBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_inBound ( pUns, outFile ) ;

  
  strcpy ( outFile, pRootFile ) ;
  strcat ( outFile, ".asciiBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  if ( avbpFmt == v4_2 )
    write_avbp_asciiBound_4p2 ( pUns, outFile ) ;
  else
    write_avbp_asciiBound_4p7 ( pUns, outFile ) ;


  if ( doGraph ) {
    strcpy ( outFile, pRootFile ) ;
    strcat ( outFile, ".elGraph" ) ;
    write_avbp_elGraph ( pUns, outFile ) ;
  }

  fclose ( visFile ) ;

  /* Clean up periodic setup. */
  pUns->mPerVxPairs = 0 ;
  arr_free ( pUns->pPerVxPair ) ;
  pUns->pPerVxPair = NULL ;

  /* Turn periodic boundaries back on. */
  count_uns_bndFaces ( pUns ) ;
  return ( 1 ) ;
}

