/*
   nr_chol.c:
   Cholesky deco and solve from the Num. Rec.

   Last update:
   ------------
   18Dec2; collated.

   This file contains:
   -------------------
   choldc
   cholsl
*/

#include <math.h>
#include "nr.h"

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#define SGN(a) ( (a) > 0 ? 1 : (-1) )
#define ABS(a) ( (a) > 0 ? (a) : (-(a)) )


double pythag(double a, double b)
{
  double absa,absb;
  absa=fabs(a);
  absb=fabs(b);
  if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
  else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
}


/******************************************************************************

  choldc:
  Cholesky Deco.
  
  Last update:
  ------------
  22Dec02: modified return values from NR util.
  
  Input:
  ------
  a[1..n][1..n]: matrix in NR format.
  n:             size

  Changes To:
  -----------
  p: pivot vector?
  
  Returns:
  --------
  0 on success, the number of the degen column on failure.
  
*/

int choldc(double **a, int n, double p[], double tol ) {
  int i,j,k;
  double sum;

  for (i=1;i<=n;i++) {
    for (j=i;j<=n;j++) {
      for (sum=a[i][j],k=i-1; k>=1; k--)
        sum -= a[i][k]*a[j][k];
      if (i == j) {
        if (sum <= tol )
          /*  nrerror("choldc failed"); */
          return (i) ;
        p[i]=sqrt(sum);
      }
      else
        a[j][i]=sum/p[i];
    }
  }
  return (0) ;
}

void cholsl( double **a, int n, double p[], double b[], double x[] ){
  
  int i,k;
  double sum;

  for ( i=1; i<=n; i++ ) {
    sum=b[i] ;
    for ( k=i-1; k>=1; k-- )
      sum -= a[i][k]*x[k];
    x[i]=sum/p[i];
  }

  for ( i=n; i>=1; i-- ) {
    sum=x[i] ;
    for ( k=i+1; k<=n; k++ )
      sum -= a[k][i]*x[k];
    x[i]=sum/p[i];
  }
}

/******************************************************************************

  qrdcmp:
  QR decomposition.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/


void qrdcmp( double **a, int n, double *c, double *d, int *sing ) {
  
  int i,j,k;
  double scale,sigma,sum,tau;

  *sing=0;
  
  for ( k = 1 ; k < n ; k++ ) {
    scale = 0.0 ;
    for ( i = k ; i <= n ; i++ )
      scale = FMAX( scale, fabs( a[i][k] ) ) ;
    
    if ( scale == 0.0) {
      *sing = 1 ;
      c[k] = d[k] = 0.0 ; }
    else {
      for ( i = k ; i <= n ; i++ )
        a[i][k] /=  scale ;
      
      for ( sum = 0., i = k ; i <= n ; i++ )
        sum += SQR( a[i][k] ) ;

      sigma = SIGN( sqrt(sum), a[k][k] ) ;
      a[k][k] += sigma ;
      c[k] = sigma*a[k][k] ;
      d[k] = -scale*sigma ;
      for ( j = k+1 ; j <= n ; j++ ) {
        for ( sum = 0., i = k ; i <= n ; i++ )
          sum += a[i][k]*a[i][j] ;
        tau = sum/c[k] ;

        for ( i = k ; i <= n ; i++ )
          a[i][j] -= tau*a[i][k] ;
      }
    }
  }
  
  d[n] = a[n][n] ;
  if ( d[n] == 0.0 )
    *sing = 1 ;
  
  return ;
}

void rsolv( double **a, int n, double d[], double b[] ) {
  int i,j ;
  double sum ;

  b[n] /= d[n] ;
  for ( i = n-1 ; i >= 1 ; i-- ) {
    for ( sum = 0.0, j = i+1 ; j <= n ; j++ )
      sum += a[i][j]*b[j] ;

    b[i] = ( b[i]-sum )/d[i] ;
  }
  return ;
}


void qrsolv( double **a, int n, double c[], double d[], double b[] ) {
  
  int i,j ;
  double sum,tau ;

  for ( j = 1 ; j < n ; j++ ) {
    for ( sum = 0.0, i = j ; i <= n ; i++ )
      sum +=  a[i][j]*b[i] ;
    tau = sum/c[j] ;

    for ( i = j ; i <= n ; i++ )
      b[i] -= tau*a[i][j] ;
  }

  rsolv( a, n, d, b ) ;
  
  return ;
}


/******************************************************************************

  sv*:
  SV decomposition.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void svdcmp(double **a, int m, int n, double w[], double **v)
{
  double pythag(double a, double b);
  int flag,i,its,j,jj,k,l,nm=0;
  double anorm,c,f,g,h,s,scale,x,y,z,*rv1;

  rv1=vector(1,n);
  g=scale=anorm=0.0;
  for (i=1;i<=n;i++) {
    l=i+1;
    rv1[i]=scale*g;
    g=s=scale=0.0;
    if (i <= m) {
      for (k=i;k<=m;k++) scale += fabs(a[k][i]);
      if (scale) {
        for (k=i;k<=m;k++) {
          a[k][i] /= scale;
          s += a[k][i]*a[k][i];
        }
        f=a[i][i];
        g = -SIGN(sqrt(s),f);
        h=f*g-s;
        a[i][i]=f-g;
        for (j=l;j<=n;j++) {
          for (s=0.0,k=i;k<=m;k++) s += a[k][i]*a[k][j];
          f=s/h;
          for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
        }
        for (k=i;k<=m;k++) a[k][i] *= scale;
      }
    }
    w[i]=scale *g;
    g=s=scale=0.0;
    if (i <= m && i != n) {
      for (k=l;k<=n;k++) scale += fabs(a[i][k]);
      if (scale) {
        for (k=l;k<=n;k++) {
          a[i][k] /= scale;
          s += a[i][k]*a[i][k];
        }
        f=a[i][l];
        g = -SIGN(sqrt(s),f);
        h=f*g-s;
        a[i][l]=f-g;
        for (k=l;k<=n;k++) rv1[k]=a[i][k]/h;
        for (j=l;j<=m;j++) {
          for (s=0.0,k=l;k<=n;k++) s += a[j][k]*a[i][k];
          for (k=l;k<=n;k++) a[j][k] += s*rv1[k];
        }
        for (k=l;k<=n;k++) a[i][k] *= scale;
      }
    }
    anorm=FMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
  }
  for (i=n;i>=1;i--) {
    if (i < n) {
      if (g) {
        for (j=l;j<=n;j++)
          v[j][i]=(a[i][j]/a[i][l])/g;
        for (j=l;j<=n;j++) {
          for (s=0.0,k=l;k<=n;k++) s += a[i][k]*v[k][j];
          for (k=l;k<=n;k++) v[k][j] += s*v[k][i];
        }
      }
      for (j=l;j<=n;j++) v[i][j]=v[j][i]=0.0;
    }
    v[i][i]=1.0;
    g=rv1[i];
    l=i;
  }
  for (i=MIN(m,n);i>=1;i--) {
    l=i+1;
    g=w[i];
    for (j=l;j<=n;j++) a[i][j]=0.0;
    if (g) {
      g=1.0/g;
      for (j=l;j<=n;j++) {
        for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
        f=(s/a[i][i])*g;
        for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
      }
      for (j=i;j<=m;j++) a[j][i] *= g;
    } else for (j=i;j<=m;j++) a[j][i]=0.0;
    ++a[i][i];
  }
  for (k=n;k>=1;k--) {
    for (its=1;its<=30;its++) {
      flag=1;
      for (l=k;l>=1;l--) {
        nm=l-1;
        if ((double)(fabs(rv1[l])+anorm) == anorm) {
          flag=0;
          break;
        }
        if ((double)(fabs(w[nm])+anorm) == anorm) break;
      }
      if (flag) {
        c=0.0;
        s=1.0;
        for (i=l;i<=k;i++) {
          f=s*rv1[i];
          rv1[i]=c*rv1[i];
          if ((double)(fabs(f)+anorm) == anorm) break;
          g=w[i];
          h=pythag(f,g);
          w[i]=h;
          h=1.0/h;
          c=g*h;
          s = -f*h;
          for (j=1;j<=m;j++) {
            y=a[j][nm];
            z=a[j][i];
            a[j][nm]=y*c+z*s;
            a[j][i]=z*c-y*s;
          }
        }
      }
      z=w[k];
      if (l == k) {
        if (z < 0.0) {
          w[k] = -z;
          for (j=1;j<=n;j++) v[j][k] = -v[j][k];
        }
        break;
      }
      if (its == 30) nrerror("no convergence in 30 svdcmp iterations");
      x=w[l];
      nm=k-1;
      y=w[nm];
      g=rv1[nm];
      h=rv1[k];
      f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
      g=pythag(f,1.0);
      f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
      c=s=1.0;
      for (j=l;j<=nm;j++) {
        i=j+1;
        g=rv1[i];
        y=w[i];
        h=s*g;
        g=c*g;
        z=pythag(f,h);
        rv1[j]=z;
        c=f/z;
        s=h/z;
        f=x*c+g*s;
        g = g*c-x*s;
        h=y*s;
        y *= c;
        for (jj=1;jj<=n;jj++) {
          x=v[jj][j];
          z=v[jj][i];
          v[jj][j]=x*c+z*s;
          v[jj][i]=z*c-x*s;
        }
        z=pythag(f,h);
        w[j]=z;
        if (z) {
          z=1.0/z;
          c=f*z;
          s=h*z;
        }
        f=c*g+s*y;
        x=c*y-s*g;
        for (jj=1;jj<=m;jj++) {
          y=a[jj][j];
          z=a[jj][i];
          a[jj][j]=y*c+z*s;
          a[jj][i]=z*c-y*s;
        }
      }
      rv1[l]=0.0;
      rv1[k]=f;
      w[k]=x;
    }
  }
  free_vector(rv1,1,n);
}


void svbksb(double **u, double w[], double **v, int m, int n, double b[], double x[])
{
  int jj,j,i;
  double s,*tmp;

  tmp=vector(1,n);
  for (j=1;j<=n;j++) {
    s=0.0;
    if (w[j]) {
      for (i=1;i<=m;i++) s += u[i][j]*b[i];
      s /= w[j];
    }
    tmp[j]=s;
  }
  for (j=1;j<=n;j++) {
    s=0.0;
    for (jj=1;jj<=n;jj++) s += v[j][jj]*tmp[jj];
    x[j]=s;
  }
  free_vector(tmp,1,n);
}

#define TOL 1.0e-5

void svdfit(double x[], double y[], double sig[], int ndata, double a[], int ma,
            double **u, double **v, double w[], double *chisq,
            void (*funcs)(double, double [], int))
{
  void svbksb(double **u, double w[], double **v, int m, int n, double b[],
              double x[]);
  void svdcmp(double **a, int m, int n, double w[], double **v);
  int j,i;
  double wmax,tmp,thresh,sum,*b,*afunc;

  b=vector(1,ndata);
  afunc=vector(1,ma);
  for (i=1;i<=ndata;i++) {
    (*funcs)(x[i],afunc,ma);
    tmp=1.0/sig[i];
    for (j=1;j<=ma;j++) u[i][j]=afunc[j]*tmp;
    b[i]=y[i]*tmp;
  }
  svdcmp(u,ndata,ma,w,v);
  wmax=0.0;
  for (j=1;j<=ma;j++)
    if (w[j] > wmax) wmax=w[j];
  thresh=TOL*wmax;
  for (j=1;j<=ma;j++)
    if (w[j] < thresh) w[j]=0.0;
  svbksb(u,w,v,ndata,ma,b,a);
  *chisq=0.0;
  for (i=1;i<=ndata;i++) {
    (*funcs)(x[i],afunc,ma);
    for (sum=0.0,j=1;j<=ma;j++) sum += a[j]*afunc[j];
    *chisq += (tmp=(y[i]-sum)/sig[i],tmp*tmp);
  }
  free_vector(afunc,1,ma);
  free_vector(b,1,ndata);
}
