/*
  uns_meth.c
  Unstructured grid methods.
 
  Last update:
  ------------
  3Apr19; and also init_uns_var.
  2Apr19; move init_uns to, well, of course, init_uns.c.
  7Sep17: intro face_all_numbered_vx.
  8Apr13; switch all arr_[re]alloc to deference the pointer to get the allocation size.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  3Jul10; handle FATAL errors with hip_err.
  13May07; append_chunk initialises Pvrtx pointers. 
  07Jan05; intro bcNrCompare, make bcOrderCompare public.
  11Apr03; intro uns_compress_bc.
  22Apr02; remove set_uns_defVar.
  10Apr00; fix cmp_vx to work with **ppVx.
  11Nov00; add cmp_vx.
  11Apr00; intro reset_elem, reset_vrtx. 
  22Oct99; intro find_face_elem.
  15Oct99; convert freestream variables as well in conv_uns_var.
  24Jun97; allow for an empty pUns in append_chunk.
  19Oct97; copy PPbc in append_chunk from the root chunk.
  6May96: move make_chunk here from mb_2uns.
  2May96; conceived.

  Contains:
  ---------
  bcNrCompare
  bcOrderCompare

  make_uns:
  make_chunk:
  append_chunk:

  free_uns:
  free_chunk:

  reset_elem:
  reset_vrtx:
  
  conv_uns_var:
  init_uns_var:
  set_uns_freestream:

  make_uns_ppBc:
  make_uns_ppChunk:
  link_uns_bcpatch:

  make_vxToElem

  find_face_elem

  de_cptVx
  min_cpt
  max_cpt
  cmp_cpt
*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

#include "fnmatch.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;

extern const char varCatNames[][LEN_VAR_C] ;
extern const char topoString[][MINITXT_LEN] ;

extern double reallocFactor  ;
extern double minArrSize ;

int bfBcNrCompare ( const void *pBf1, const void *pBf2 ) {
  const bndFc_struct *pB1 = ( bndFc_struct * ) pBf1 ;
  const bndFc_struct *pB2 = ( bndFc_struct * ) pBf2 ;

  if ( pB1->invalid )
    return ( 1 ) ;
  else if ( pB2->invalid )
    return ( -1 ) ;
  else
    return ( pB1->Pbc->nr - pB2->Pbc->nr ) ;
}



/* Compare the order of boundary conditions pBc */
int bcOrderCompare ( const void *vppBc0, const void *vppBc1 ) {
  const bc_struct *pBc0 = *(const bc_struct **)vppBc0,
    *pBc1 = *(const bc_struct **)vppBc1 ;

  return ( pBc0->order - pBc1->order ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_uns_expr:
*/
/*! find an unstructured grid matching the given expression.
 *
 *
 */

/*
  
  Last update:
  ------------
  3Apr20; rename to find_uns_expr
  30Jun17: conceived.
  

  Input:
  ------
  nr: grid number

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

uns_s *find_uns_expr ( const char *expr ) {

  grid_struct *pGrid = find_grid ( expr, uns ) ;

  if ( pGrid )
    return ( pGrid->uns.pUns ) ;
  else
    return ( NULL ) ;
}



uns_s *find_uns_numbered ( int nr ) {

  grid_struct *pGrid = find_grid_numbered ( nr, uns ) ;

  if ( pGrid )
    return ( pGrid->uns.pUns ) ;
  else
    return ( NULL ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  get_next_slot_array:
*/
/*! Given a linear array, point to the next free slot, re-allocating if needed.
 *
 *
 */

/*
  
  Last update:
  ------------
  5Jul17: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void *get_next_slot_array ( char **ppData, ulong_t *pAllocSize, ulong_t *pUseSize,
                            const size_t dataSize ) {

  hip_err ( fatal, 0, "use make_arr instead." ) ;

  return ( NULL ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  make_slidingPlaneSide:
*/
/*! allocate, initalise a sliding plane/non-conformal interface, associate it with a grid. 
 *
 */

/*
  
  Last update:
  ------------
  5Apr20: conceived.
  

  Input:
  ------
  pUns: grid to add this interface to.
    
  Returns:
  --------
  pointer to the sliding plane side
  
*/

slidingPlaneSide_s * make_slidingPlaneSide ( uns_s *pUns,
                                             const int isMaster, 
                                             const char ifcName[LINE_LEN] ) {

  static int mSlidingPlaneMasters = 0 ;
  if ( isMaster )
    mSlidingPlaneMasters++ ;

  /* Sliding plane data. */
  slidingPlaneSide_s *pSpS = 
    arr_malloc ( "one slidingPlaneSide in make_slidingPlaneSide", pUns->pFam,
                 1, sizeof( *pSpS ) ) ;

  init_slidingPlaneSide ( pSpS ) ;
  if ( ifcName[0] != '\0' ) {
    strncpy ( pSpS->name, ifcName, LINE_LEN ) ;
  }
  else {
    /* No name given, build one from grid name and ifc number. */
    char someName[LINE_LEN] ;
    sprintf ( someName, "ifc%d", mSlidingPlaneMasters ) ;
    strncpy ( pSpS->name, someName, LINE_LEN ) ;
  }
  pSpS->nr = mSlidingPlaneMasters ;
  pSpS->isMaster = isMaster ;

  pSpS->pUns = pUns ;


  /* List of sliding plane pointers with the uns grid. */
  pUns->ppSlidingPlaneSide =
    arr_realloc ("slidingPlaneSide list in make_slidingPlaneSide", pUns->pFam,
                 pUns->ppSlidingPlaneSide, 1, sizeof ( *(pUns->ppSlidingPlaneSide) ) ) ;

  pUns->ppSlidingPlaneSide[pUns->mSlidingPlaneSides++] = pSpS ;
    
  return ( pSpS ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  move_slidingPlaneSides:
*/
/*! move all sliding plane from one uns mesh to another, e.g. after attaching.
 *
 */

/*
  
  Last update:
  ------------
  6Apr20: conceived.
  

  Input:
  ------
  pUnsTo: to move to
  pUnsFrom: to move from

  Returns:
  --------
  total number of sliding planes 
  
*/

int move_slidingPlaneSides ( uns_s *pUnsTo, uns_s* pUnsFrom ) {

  int mSpSFrom = pUnsFrom->mSlidingPlaneSides ; 
  int mSpSTo = pUnsTo->mSlidingPlaneSides ;
  int mSpS = mSpSTo + mSpSFrom ;
  if ( !mSpSFrom )
    /* Nothing to move. */
    return ( mSpS ) ;

  slidingPlaneSide_s **ppSpS = pUnsTo->ppSlidingPlaneSide = 
    arr_realloc ("slidingPlaneSide list in make_slidingPlaneSide", pUnsTo->pFam,
                 pUnsTo->ppSlidingPlaneSide, mSpS, sizeof ( *ppSpS ) ) ;

  int kS ;
  for ( kS = 0 ; kS < mSpSFrom ; kS++ ) {
    ppSpS[ mSpSTo+kS ] = pUnsFrom->ppSlidingPlaneSide[kS] ;
    ppSpS[ mSpSTo+kS ]->pUns = pUnsTo ;
  }
  pUnsTo->mSlidingPlaneSides = mSpS ;

  pUnsFrom->mSlidingPlaneSides = 0 ;
  arr_free ( pUnsFrom->ppSlidingPlaneSide ) ;

  return ( mSpS ) ;
}



/******************************************************************************

  make_uns:
  Create a new unstructured grid root.
  
  Last update:
  ------------
  16Apr20; init_uns first, so name can be construced in pGrid.
  1Apr19; extract content of init_uns.
  19Dec17; add pGrid as input arg. Connect the uns grid to the container.
  8Sep17; remove init of rotAngle, no longer stored in pUns.
  5Jul17; init stackFc.
  18Feb12; maxConn and piConn moved from uns_s to become global in read_cgns.
  18Dec11; init default pUns->name.
  17Dec11; init vxMarkActivity
  9Sep11; init mZones to zero.
  29Aug11; init isVec value in var.
  5Apr11; initialise maxConn, piConn.
  17Sep09; init flag.
  4Apr09; replace varTypeS with varList.
  18Jun06; intro pUns->epsOverlap.
  26Apr02; initialise pNxtVar in varType_s.
  10Mar01; intro pVxCpt.
  7Sep00; intro mUnknFlow.
  24Feb00; intro pPerEdge.
  15Jul99; intro pBndFcVx.
  19Jun98; reset mPerBcPairs.
  : conceived.
  
  Input:
  ------
  pGrid: in non-NULL, add this uns grid to the grid container.
  
  Returns:
  --------
  pUns: created uns grid.
  
*/

uns_s *make_uns ( grid_struct *pGrid ) {

  char gridName[LINE_LEN];
  sprintf ( gridName, "uns grid nr. %d", ++Grids.mUnsGrids ) ;
  arrFam_s *pFam = make_arrFam ( gridName ) ;
  
  uns_s *pUns = arr_malloc ( "pUns in make_uns", pFam, 1, sizeof( *pUns ) ) ;
  init_uns ( pUns, pGrid ) ;

  pUns->nr = Grids.mUnsGrids ;
  if ( pGrid ) 
    sprintf ( pUns->pGrid->uns.name, "uns_grid_%d", Grids.mUnsGrids ) ;
  pUns->pFam = pFam ;

  // Any changed defaults to apply?
  set_specialTopo ( pUns, topoString[Grids.nxtSpecialTopo] ) ;
  Grids.nxtSpecialTopo = noTopo ;

  return ( pUns ) ;
}


/******************************************************************************

  make_uns_grid:
  Wrap make_grid and make_uns and append_chunk into one. 
  
  Last update:
  ------------
  19Dec17; new interface to make_uns.
  7Jul3; allow pre-allocated pUns, pass it as an argument.
  Apr13; intro ulong_t types.
  14Sep10; add mUnknown to interface. 
  4Apr09; GS: drop incrementation of Grids.mGrids, this is done in make_grid.
          JDM: replace varTypeS with varList.
  28Jun07; set pUns->mBc.
  13May07: conceived.
  
  Input:
  ------
  mDim 
  mEl
  mConn
  mEl2ChP
  mVx
  mUnknown
  mBndFc
  mBc


  Changes To:
  -----------
  ppUns

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

grid_struct *make_uns_grid ( uns_s **ppUns, int mDim,
                             ulong_t mEl, ulong_t mConn, ulong_t mEl2ChP,
                             ulong_t mVx, ulong_t mUnknown,
                             ulong_t mBndFc, int mBc ) {

  grid_struct *pGrid ;
  uns_s *pUns ;
  chunk_struct *pChunk ;

  /* Alloc. */
  if ( !*ppUns )  *ppUns = make_uns ( NULL ) ;
  pUns = *ppUns ;
  pUns->mBc = mBc ;
  pUns->varList.mUnknowns = mUnknown ;
  
  if ( !( pChunk = append_chunk( pUns, mDim, mEl, mConn, mEl2ChP, 
                                 mVx, mBndFc, mBc ) ) ) {
    sprintf ( hip_msg, "failed to alloc for grid in make_uns_grid.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

 
  /* Unstructured. Make a new grid. */
  pGrid = make_grid () ;

  /* Put the chunk into Grids. */
  pGrid->uns.type = uns ;
  pGrid->uns.pUns = pUns ;
  pGrid->uns.mDim = mDim ;
  pGrid->uns.pVarList = &(pUns->varList) ;

  pUns->nr = pGrid->uns.nr = Grids.mGrids ;
  pUns->pGrid = pGrid ;
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;


  return ( pGrid ) ;
}


/************************************************************************

  make_chunk:
  Allocate and initialize a chunk for the linked list of chunks.

  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  10Jun19; zero Used counters.
  20Dec16; init Pchunk->pNr2, ->pVrtxVol ;
  4Nov10; track pUns with pChunk.
  25Apr96: cleanup.

  Returns:
  --------
  NULL:   on failure,
  Pchunk: the allocated chunk,

*/

chunk_struct* make_chunk ( uns_s *pUns ) {
  
  chunk_struct *Pchunk ;

  Pchunk = arr_malloc ( "Pchunk in make_chunk", pUns->pFam, 1, sizeof( *Pchunk ) ) ;

  Pchunk->pUns = pUns ;
  Pchunk->nr = Pchunk->chunkMark = 0 ;
  Pchunk->PprvChunk = Pchunk->PnxtChunk = NULL ;
  //Pchunk->hMin = TOO_MUCH ;
  //Pchunk->hMax = -TOO_MUCH ;

  Pchunk->mVerts = Pchunk->mVertsMarked = Pchunk->mVertsNumbered = Pchunk->mVertsUsed = 0 ;
  Pchunk->nLstVxPrvChk = 0 ;
  Pchunk->Pvrtx = NULL ; 
  Pchunk->Pcoor = NULL ;
  Pchunk->Punknown = NULL ;
  Pchunk->sizeDblMark = 0 ;
  Pchunk->PdblMark = NULL ; 

  Pchunk->pVxCpt = NULL ;

  Pchunk->pVrtxNr2 = NULL ; 
  Pchunk->pVrtxVol = NULL ; 

  
  Pchunk->mElems = Pchunk->mElemsNumbered = Pchunk->mElemsMarked =  Pchunk->mElemsUsed = 0 ;
  Pchunk->Pelem = NULL ;
  Pchunk->mElem2VertP = Pchunk->mElem2VertPUsed = 0 ;
  Pchunk->PPvrtx = NULL ; 
  Pchunk->PelemMark = NULL ;


#ifdef ADAPT_HIERARCHIC
  Pchunk->mElem2ChildP = Pchunk->mElem2ChildPUsed = 0 ;
  Pchunk->PPchild = NULL ;
#endif


  
  Pchunk->mBndPatches =  Pchunk->mBndPatchesUsed = 0 ;
  Pchunk->PbndPatch = NULL ;
  Pchunk->mDegenFaces = 0 ;
  Pchunk->mBndFaces = Pchunk->mBndFacesMarked = Pchunk->mBndFacesUsed = 0 ;
  Pchunk->PbndFc = NULL ; 

  Pchunk->mMatchFaces = 0 ;
  Pchunk->PmatchFc = NULL ; 

  Pchunk->mIntFaces = 0 ;
  Pchunk->PintFc = NULL ;

  /* If derived from a mb grid, and mapping is requested, track the block. */
  Pchunk->nBlock = 0 ;
  Pchunk->blockDim[0] = 0 ;
  Pchunk->ppIJK2Vx = NULL ;

  return ( Pchunk ) ;
}
/******************************************************************************
  realloc_unknowns:   */

/*! Reallocate the field of unknowns over all chunks, update vertex ->pUn pointers.
 *
 * 
 *
 */

/*
  
  Last update:
  ------------
  3Jul12; fix bug with setting pointer to pUnknown.
  9May11; fix bug in alloc, alloc for doubles, rather than pointers to doubles.
  6May11; fix bug with recursive use of loop_chunks.
  21Dec10: conceived.
  

  Input:
  ------
  pUns
  mUn0:  this is the current size of unknowns/vx.
  mUn2: allocate to this size per vertex.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

void realloc_unknowns ( uns_s *pUns, int mUn0, int mUn2 ) {

  chunk_struct *pChunk ;
  ulong_t mV ;
  vrtx_struct *pVxBeg, *pVxEnd, *pVx ;
  double *pUn0, *pUn2 ;

  pUns->varList.mUnknowns = mUn2 ;

  pChunk = NULL ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    mV = pChunk->mVerts ;

    /* Index unknowns from 1, as are vertices. */
    pChunk->Punknown = arr_realloc ( "pUnknown in realloc_unknown", pUns->pFam, 
                                     pChunk->Punknown, (mV+1)*mUn2, 
                                     sizeof( *pChunk->Punknown ) ) ;

    /* Set pointers to the unknowns. Can't use loop_verts here, as it resets pChunk. */
    pVxBeg = pChunk->Pvrtx+1 ;
    pVxEnd = pChunk->Pvrtx + mV ;
    if ( mUn0 ) {
      /* There is an existing rcpt solution. Run backwards through the
         expanded array, copy the exisiting blocks into their new
         positions. */
      pUn0 = pChunk->Punknown + mV*mUn0 ;
      pUn2 = pChunk->Punknown + mV*mUn2 ;
      for ( pVx = pVxEnd ; pVx >= pVxBeg ; pVx-- ) {
        /* Set a pointer to the unknown, all vx, not just numbered ones. */
        pVx->Punknown = pUn2 ;
        memcpy ( pUn2, pUn0, mUn0*sizeof( double ) ) ;
        pUn2 -= mUn2 ;
        pUn0 -= mUn0 ;
      }
    }
    else {
      /* No solution on the rcpt grid that needs respecting. Just set the pointers. */
      pUn2 = pChunk->Punknown + mUn2 ;
      for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ ) {
        pVx->Punknown = pUn2 ;
        pUn2 += mUn2 ;
      }
    }
  }

  return ;
}


/******************************************************************************
  append_bndFc:   */

/*! Append boundary face space to a chunk.
 */

/*
  
  Last update:
  ------------
  16Dec16; init also pBndPatch[0] to zero.
  16Dec16; init mBndEg to zero for each patch.
  4Apr13; make large counters ulong_t
  29Jun12; reset pBndFc.
  2Apr12; move chunk size updates here from append_chunk.
  5Feb12: cut out of append_chunk.
  

  Input:
  ------
  pChunk: 
  mBndPatches: number of boundary conditions
  mBndFaces: number of boundary faces.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void append_bndFc ( chunk_struct *pChunk, ulong_t mBndPatches, ulong_t mBndFaces ) {

  bndFc_struct *pBf ;

  if ( !mBndFaces )
    pChunk->PbndFc = NULL ;
  else {
    pChunk->PbndFc = arr_malloc ( "PbndFc in append_chunk", pChunk->pUns->pFam, 
                                  mBndFaces+1, sizeof( *pChunk->PbndFc ) ) ;
    for ( pBf = pChunk->PbndFc+1 ; pBf <= pChunk->PbndFc+mBndFaces ; pBf++) {
      init_bndFc ( pBf ) ;
    }
  }


  bndPatch_struct *pBp ;

  if ( !mBndPatches )
    pChunk->PbndPatch = NULL ;
  else {
    pChunk->PbndPatch = arr_malloc ( "PbndPatch in append_chunk", pChunk->pUns->pFam, 
                                     mBndPatches+1, sizeof( *pChunk->PbndPatch ) ) ;
    for ( pBp = pChunk->PbndPatch ; 
          pBp <= pChunk->PbndPatch+mBndPatches ; pBp++) {
      pBp->Pbc = NULL ;
      pBp->PbndFc = NULL ;
      pBp->mBndFc = pBp->mBndEg = 0 ;
    }

  }

  /* Set the sizes. */
  pChunk->mBndFaces = mBndFaces ;
  pChunk->mBndPatches = mBndPatches ;

  return ;
}


/******************************************************************************
  append_vrtx:   */

/*! Append vrtx space to a chunk.
 */

/*
  
  Last update:
  ------------
  4Apr13; make large counters ulong_t
  initialise pUnk before using it.
  2Apr12; move chunk size updates here from append_chunk.
  5Feb12: cut out of append_chunk.
  

  Input:
  ------
  pChunk: 
  mVerts: number of vertices
  mDim: number of dimensions
  mEq: number of equations/unknowns.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void append_vrtx ( chunk_struct *pChunk, ulong_t mVerts, int mDim, int mEq ) {

  vrtx_struct *Pvrtx ;
  double *pUnk, *pCoor ;
  ulong_t n ;

  if ( !mVerts ) {
    /* Don't alloc for vertices. */
    pChunk->Pvrtx = NULL ;
  }
  else {
    /* Alloc vertex related spaces. */
    pChunk->Pvrtx = arr_malloc ( "Pvrtx in append_vrtx", pChunk->pUns->pFam, 
                                 mVerts+1, sizeof( *pChunk->Pvrtx ) ) ;
  }


  if ( mVerts*mDim == 0 ) {
    /* Don't alloc for coor. Used when coor fields are already allocated
       by setting mDim to 0. */
    pChunk->Pcoor = NULL ;
  }
  else {
    /* Alloc vertex related spaces. */
    pChunk->Pcoor = arr_malloc ( "Pcoor in append_vrtx", pChunk->pUns->pFam, 
                                 mDim*( mVerts+1 ), sizeof( *pChunk->Pcoor ) ) ;

    /* Reset the new vertices. */
    pCoor = pChunk->Pcoor ;
    for ( Pvrtx = pChunk->Pvrtx+1, n=1 ; n <= mVerts ; n++, Pvrtx++ ) {
      Pvrtx->number = Pvrtx->mark = 0 ;
      Pvrtx->Pcoor = pCoor + n*mDim ;
      Pvrtx->Punknown = NULL ;
    }
  }


  if ( mVerts*mEq == 0 )
    pChunk->Punknown = NULL ;
  else {
    pChunk->Punknown = arr_malloc ( "Punknown in append_vrtx", pChunk->pUns->pFam, 
                                    mEq*(mVerts+1), sizeof( *pChunk->Punknown ) ) ;

    /* Reset the new vertices. */
    pUnk = pChunk->Punknown ;
    for ( Pvrtx = pChunk->Pvrtx+1, n=0 ; n < mVerts ; n++, Pvrtx++ ) {
      Pvrtx->Punknown = pUnk + n*mEq ;
    }
  }

  /* Set the sizes. */
  pChunk->mVerts = mVerts ;

  return ;
}

/******************************************************************************
  append_elem:   */

/*! Append element space to a chunk.
 */

/*
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  5Jan15; carry mElem2ChildP again.
  4Apr13; make large counters ulong_t
  2Apr12; move chunk size updates here from append_chunk.
  5Feb12: cut out of append_chunk.
  

  Input:
  ------
  pChunk: 
  mElems: number of elements
  mElem2VertP: number of connectivity entries.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void append_elem ( chunk_struct *pChunk, ulong_t mElems, 
                   ulong_t mElem2VertP, ulong_t mElem2ChildP ) {

  elem_struct *Pelem ;

  if ( !mElems ) {
    /* Don't alloc for elems. */
    pChunk->Pelem = NULL ;
    pChunk->PPvrtx = NULL ;
#ifdef ADAPT_HIERARCHIC
    pChunk->PPchild = NULL ;
#endif
  }
  else {
    pChunk->Pelem = arr_malloc ( "Pelem in append_elem", pChunk->pUns->pFam, 
                                 mElems+1, sizeof( *pChunk->Pelem ) ) ;
    if ( mElem2VertP )
      pChunk->PPvrtx = arr_malloc ( "PPvrtx in append_elem", pChunk->pUns->pFam,
                                    mElem2VertP, sizeof( *pChunk->PPvrtx ) ) ;
    else
      pChunk->PPvrtx = NULL ;

#ifdef ADAPT_HIERARCHIC
    /* Use arr_calloc for PPchild, because buffered elements use a zero pointer as
       a termination of the list of children. Allocate an extra entry for that. */
    pChunk->PPchild = arr_calloc ( "PPchild in append_elem", pChunk->pUns->pFam,
                                   mElem2ChildP+1, sizeof( *pChunk->PPchild ) ) ;
#endif
  
    /* Reset the new elems. */
    for ( Pelem = pChunk->Pelem ; Pelem <= pChunk->Pelem+mElems ; Pelem++ ) {
      init_elem ( Pelem, noEl, 0, NULL ) ;
    }
  }

  /* Set the sizes. */
  pChunk->mElems = mElems ;
  pChunk->mElem2VertP = mElem2VertP ;

#ifdef ADAPT_HIERARCHIC
  pChunk->mElem2ChildP = mElem2ChildP ;
#endif

  return ;
}


/******************************************************************************
  extend_bndFc:   */

/*! Extend boundary face space in the root chunk.
 */

/*
  
  Last update:
  ------------
  9Jul15; initialise pBndPatch to suppress compiler warning.
  9Jan15; derived from append_bndFc

  Input:
  ------
  pUns: 
  mBndPatches: number of boundary conditions
  mBndFaces: number of boundary faces.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void extend_bndFc ( uns_s *pUns, ulong_t mBndPatches, ulong_t mBndFaces ) {


  chunk_struct *pChunk = pUns->pRootChunk ;
  pChunk->mBndFacesUsed = pChunk->mBndFaces ;
  bndPatch_struct *pOldPbndPatch = pChunk->PbndPatch ;
  pChunk->mBndPatchesUsed = pChunk->mBndPatches ;

  if ( !mBndFaces && !mBndPatches ) {
    // nothing else to be done. 
    return ;
  }

  bndFc_struct *pOldPbndFc = pChunk->PbndFc ;
  bndFc_struct *pBndFc ;
  if ( mBndFaces ) {
    pChunk->mBndFaces += mBndFaces ;
    pBndFc = pChunk->PbndFc = 
      arr_realloc ( "PbndFc in extend_bndFc", pChunk->pUns->pFam,
                    pChunk->PbndFc, mBndFaces+1, sizeof( *pChunk->PbndFc ) ) ;
  }

 
  bndPatch_struct *pBndPatch = NULL ;
  if ( mBndPatches ) {
    pChunk->mBndPatches += mBndPatches ;
    pBndPatch = pChunk->PbndPatch = 
      arr_realloc ( "PbndPatch in extend_bndFc", pChunk->pUns->pFam, 
                    pChunk->PbndPatch, pChunk->mBndPatches+1, 
                    sizeof( *pChunk->PbndPatch ) ) ;
  }



  /* Nothing needs doing with reallocated faces. Reset new faces. */
  bndFc_struct *pBf ;
  for ( pBf = pChunk->PbndFc + pChunk->mBndFacesUsed + 1 ; 
        pBf <= pChunk->PbndFc+mBndFaces ; pBf++) {
    init_bndFc ( pBf ) ;
  }


  bndPatch_struct *pBp ;
  if ( mBndFaces ) {
    // Update face pointers with the patches. 
    for ( pBp = pBndPatch+1 ; 
          pBp <= pBndPatch+pChunk->mBndPatchesUsed ; pBp++)
      pBp->PbndFc = pBndFc + ( pBp->PbndFc - pOldPbndFc ) ;
  }

  // Reset any new patches. 
  for ( pBp = pBndPatch + pChunk->mBndPatchesUsed+1 ; 
        pBp <= pBndPatch+mBndPatches ; pBp++) {
    pBp->Pbc = NULL ;
    pBp->PbndFc = NULL ;
    pBp->mBndFc = 0 ;
  }

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  copy_chunk_pVrtx:
*/
/*! copy vrtx spaces from one chunk to the root. Spaces may not overlap.
 *! i.e. can't copy the root into itselv, that is done by extend_vrtx.
 */

/*
  
  Last update:
  ------------
  13jul20: extracted from extend_vrtx.
  

  Input:
  ------
  pUns: grid with linked list of chunks
  pCh2: chunk with new vrtx spaces.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s copy_chunk_pVrtx ( uns_s *pUns, chunk_struct *pCh2 ) {
#undef FUNLOC
#define FUNLOC "in copy_chunk_pVrtx"
  
  ret_s ret = ret_success () ;


  const int mDim = pUns->mDim ;
  const int mEq = pUns->varList.mUnknowns ;
  vrtx_struct *pVrtx = pCh2->Pvrtx ;
  double *pCoor = pCh2->Pcoor ;
  double *pUnknown = pCh2->Punknown ;

  chunk_struct *pCh = NULL ;
  while ( loop_chunks ( pUns, &pCh ) ) {

    /* Check sizes. */
    ulong_t mVxUsed = pCh2->mVertsUsed ;
    ulong_t mChVerts = pCh->mVertsUsed ;
    if ( mVxUsed + mChVerts > pCh2->mVerts ) {
      hip_err ( fatal, 0, "not enough space allocated to combine vertex fields\n"
                "          with the root chunk "FUNLOC"." ) ;
    }
    
    vrtx_struct *pVrtxCh = pCh->Pvrtx ;
    vrtx_struct *pVxChMin = pVrtxCh + 1 ;

    double *pCoorCh = pCh->Pcoor ;
    double *pCoChMin = pCoorCh + mDim ;

    double *pUnknownCh = pCh->Punknown ;
    double *pUnChMin = pUnknownCh + mEq ;

    /* Copy. */
    memcpy ( pVrtx + mVxUsed+1, pVxChMin,
             mChVerts*sizeof ( *pVrtx) ) ;
    memcpy ( pCoor + mDim*(mVxUsed+1), pCoChMin,
             mDim*mChVerts*sizeof ( *pCoor ) ) ;
    memcpy ( pUnknown + mEq*(mVxUsed+1), pUnChMin,
             mEq*mChVerts*sizeof( *pUnknown ) ) ;


    /* Update pointers into vrtx, coor, unkowns. Need to check all
       chunks. */
    vrtx_struct **ppVx ;
    vrtx_struct *pVxChMax = pVrtxCh + mChVerts+1 ;
    double *pCoChMax = pCoorCh + mDim*(mChVerts+1) ;
    double *pUnChMax = pUnknownCh + mEq*(mChVerts+1) ;

    /* Update elem 2 vrtx pointers. */
    for ( ppVx = pCh->PPvrtx ; 
          ppVx < pCh->PPvrtx + pCh->mElem2VertP ; ppVx++ ) {
      if ( *ppVx && *ppVx >= pVxChMin && *ppVx <= pVxChMax  )
        // valid vertex pointer pointing into the moved section.
        *ppVx = pVrtx + ( *ppVx - pVrtxCh ) ;
    }

    /* Update coor pointers in Pvrtx, Punkown. */
    vrtx_struct *pVx ;
    double *pCo ;
    double *pUn ;
    for ( pVx = pCh->Pvrtx + 1, 
            pCo = pCoor + mDim, pUn = pUnknown+mEq ;
          pVx <= pCh->Pvrtx + pCh->mVertsUsed ; 
          pVx++, pCo += mDim, pUn += mEq ) {
      if ( pVx->Pcoor &&
           pVx->Pcoor  >= pCoChMin && pVx->Pcoor <= pCoChMax )
        pVx->Pcoor = pCoor + ( pVx->Pcoor - pCoorCh ) ;
      if ( pVx->Punknown &&
           pVx->Punknown >= pUnChMin && pVx->Punknown <= pUnChMax )
        pVx->Punknown = pUnknown + ( pVx->Punknown - pUnknownCh ) ;
    }


    pCh->mVerts = pCh->mVertsNumbered = pCh->mVertsMarked = pCh->mVertsUsed = 0 ;
    arr_free ( pCh->Pvrtx ) ;
    pCh->Pvrtx = NULL ;
    arr_free ( pCh->Pcoor ) ;
    pCh->Pcoor = NULL ;
    arr_free ( pCh->Punknown ) ;
    pCh->Punknown = NULL ;

    pCh2->mVertsUsed += mChVerts ;
  }


  /* Reset any unsed vertices. */
  vrtx_struct *pVx ;
  double *pCo ;
  double *pUn ;
  for ( pVx = pCh2->Pvrtx + pCh2->mVertsUsed+1, pCo = pCoor + mDim, pUn = pUnknown+mEq ;
        pVx <= pCh2->Pvrtx + pCh2->mVerts ; 
        pVx++, pCo += mDim, pUn +=mEq ) {
    pVx->number = pVx->mark = 0 ;
    pVx->Pcoor = pCo ;
    if ( mEq )
      pVx->Punknown = pUn ;
    else
      pVx->Punknown = NULL ;
  }
  
  

  
  return ( ret ) ;
}



/******************************************************************************
  extend_vrtx:   */

/*! Extend vrtx space in a chunk.
 */

/*
  
  Last update:
  ------------
  13jul20; extract mDim and mEq args from pUns, rather than dummy args.
  9Jul15; initialise pUnknown to suppress compiler warning.
  9Jan15; derived from append_vrtx

  Input:
  ------
  pUns: 
  mVerts: number of vertices
  mDim: number of dimensions
  mEq: number of equations/unknowns.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void extend_vrtx ( uns_s *pUns, ulong_t mVerts ) {
#undef FUNLOC
#define FUNLOC "in extend_vrtx"

  vrtx_struct *Pvrtx ;
  const int mDim = pUns->mDim ;
  const int mEq = pUns->varList.mUnknowns ;

  if ( !mVerts ) {
    /* Nothing to be done. */
    return ;
  }
  else if ( !mDim )
    hip_err ( fatal, 0, "extend_vrtx can't handle mDim=0" ) ;
  else if ( pUns->pRootChunk->PnxtChunk )
    hip_err ( fatal, 0, "can only handle single chunks "FUNLOC"." ) ;

  chunk_struct chunk2, *pCh2 = &chunk2 ;

  /* Alloc vertex related spaces. */
  chunk_struct *pChunk = pUns->pRootChunk ;
  pChunk->mVertsUsed = pChunk->mVerts ;
  pChunk->mVerts += mVerts ; 
  pChunk->Pvrtx = 
    arr_malloc ( "Pvrtx in extend_vrtx", pChunk->pUns->pFam, 
                  pChunk->mVerts+1, sizeof( *pChunk->Pvrtx ) ) ;


 
  /* Alloc coordinates. Always allocate, leave the case
     of mDim =0 (pre-existing coor fields e.g. from other grid) to
     append_chunk. */
  pChunk->Pcoor =
    arr_malloc ( "Pcoor in extend_vrtx", pChunk->pUns->pFam, 
                  mDim*( mVerts+1 ), sizeof( *pChunk->Pcoor ) ) ;


  /* Unknowns. */
  pChunk->Punknown = NULL ;
  if ( mEq ) {  
    pChunk->Punknown = 
      arr_malloc ( "Punknown in extend_vrtx", pChunk->pUns->pFam,
                    mEq*( mVerts+1 ), sizeof ( *pChunk->Punknown ) ) ;
  }


  copy_chunk_pVrtx ( pUns, pCh2 ) ;


  /* Move newly filled vertex space to the root chunk. */
  pUns->pRootChunk->Pvrtx = pCh2->Pvrtx  ;
  pUns->pRootChunk->Pcoor = pCh2->Pcoor  ;
  pUns->pRootChunk->Punknown = pCh2->Punknown  ;

  return ;
}

/******************************************************************************
  extend_elem:   */

/*! Extend element space in a chunk.
 */

/*
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  9Jan15; derived from append_elem.

  Input:
  ------
  pUns: 
  mElems: number of elements
  mElem2VertP: number of connectivity entries.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void extend_elem ( uns_s *pUns, ulong_t mElems, 
                   ulong_t mElem2VertP, ulong_t mElem2ChildP ) {


  if ( !mElems ) {
    /* Nothing to be done. */
    return ;
  }

  // Reallocate
  chunk_struct *pChunk = pUns->pRootChunk ;
  pChunk->mElemsUsed = pChunk->mElems ;
  elem_struct *pOldPelem = pChunk->Pelem ;
  pChunk->mElems += mElems ;
  elem_struct *pElem = pChunk->Pelem = 
    arr_realloc ( "Pelem in extend_elem", pChunk->pUns->pFam, 
                  pChunk->Pelem, pChunk->mElems+1, 
                  sizeof( *pChunk->Pelem ) ) ;

  pChunk->mElem2VertPUsed = pChunk->mElem2VertP ;
  vrtx_struct **ppOldPPvrtx = pChunk->PPvrtx, **ppVrtx ;
  if ( mElem2VertP ) {
    pChunk->mElem2VertP += mElem2VertP ;
    ppVrtx = pChunk->PPvrtx = 
      arr_realloc ( "PPvrtx in extend_elem", pChunk->pUns->pFam,
                    pChunk->PPvrtx, pChunk->mElem2VertP, 
                    sizeof( *pChunk->PPvrtx ) ) ;
  }

#ifdef ADAPT_HIERARCHIC
  pChunk->mElem2ChildPUsed = pChunk->mElem2ChildP ;
  elem_struct **ppOldPPchild = pChunk->PPchild, **ppChild ;
  if ( mElem2ChildP ) {
    pChunk->mElem2ChildP += mElem2ChildP ;
    ppChild = pChunk->PPchild = 
      arr_realloc ( "PPchild in extend_elem", pChunk->pUns->pFam,
                    pChunk->PPchild, mElem2ChildP+1, 
                    sizeof( *pChunk->PPchild ) ) ;
  }
#endif
  
  // Loop over all boundary faces and reset element pointers.
  // None of these loops need while ( loop_xx ) since there is only 
  // a single chunk.
  bndFc_struct *pBf ;
  elem_struct *pEl ;
  if ( mElems ) {
    for ( pBf = pChunk->PbndFc ; 
          pBf < pChunk->PbndFc + pChunk->mBndFaces ; pBf++ ) {
      if ( pBf->Pelem )
        pBf->Pelem = pElem + ( pBf->Pelem - pOldPelem ) ;
    }


    /* Reset the new elems. */
    for ( pEl = pElem + pChunk->mElemsUsed+1 ; 
          pEl <= pChunk->Pelem + pChunk->mElems ; pEl++ ) {
      init_elem ( pEl, noEl, 0, NULL ) ;
    }

  }

  // Loop over all elements and update PPvrtx pointers.
  if ( mElem2VertP ) {
    for ( pEl = pElem+1 ; pEl <= pElem + pChunk->mElemsUsed ;  pEl++ ) {
      if ( pEl->PPvrtx )
        pEl->PPvrtx = ppVrtx + ( pEl->PPvrtx - ppOldPPvrtx ) ;
    }
  }


#ifdef ADAPT_HIERARCHIC
  // Loop over all elements and update PPvrtx pointers.
  if ( mElem2ChildP ) {
    for ( pEl = pElem+1 ; pEl <= pElem + pChunk->mElemsUsed ;  pEl++ ) {
      if ( pEl->PPchild )
        pEl->PPchild = ppChild + ( pEl->PPchild - ppOldPPchild ) ;
    }
  }
#endif


  return ;
}


/******************************************************************************

  extend_chunk:
  Extend an existing chunk, re-allocate fields for elements, elem2verts 
  and vertices. This variant allows to stick to a single list of
  entities, suitable for using as library inside codes with monolithic
  storage layout.
  
  Last update:
  ------------
  9Jan15; derived from append_chunk.
  
  Input:
  ------
  pUns
  mDim: only carried for consistency with append_chunk. Not used.
  mElems:       number of elems to add
  mElem2VertP             element to vertex pointers (PPvrtx) 
  mElem2ChildP            element to child pointers (refinement)
  mVerts                  vertices
  mBndFaces               boundary faces
  mBndPatches             boundary patches to add.

  Changes To:
  -----------
  pUns
  
  Returns:
  --------
  NULL on failure, a pointer to the modified chunk PnewChunk on success.
  
*/

chunk_struct *extend_chunk ( uns_s *pUns, int mDim,
			     ulong_t mElems,
			     ulong_t mElem2VertP, ulong_t mElem2ChildP,
			     ulong_t mVerts,
			     ulong_t mBndFaces, ulong_t mBndPatches ) {
  

  /* Find the last chunk and check whether it contains anything.
     If there are neither vertices nor elements, it is considered
     empty. */
  if ( !pUns->pRootChunk ) {
    hip_err ( fatal, 0, "no chunk in this grid in extend_chunk." ) ;
  }
  else if ( pUns->pRootChunk->PnxtChunk) {
    hip_err ( fatal, 0, "extend_chunk needs single chunk grids." ) ;
  }
  
    

  /* Vertices, coordinates, unknowns. */
  extend_vrtx ( pUns, mVerts ) ;

  /* Alloc for boundary patches, faces. */
  extend_bndFc ( pUns, mBndPatches, mBndFaces ) ;

  /* Alloc for elems. */
  extend_elem ( pUns, mElems, mElem2VertP, mElem2ChildP ) ;

  
  /* Relink boundary conditions. */
  make_uns_ppChunk ( pUns ) ;
  
  return ( pUns->pRootChunk ) ;
}




/******************************************************************************

  append_chunk:
  Make a new chunk, allocate fields for elements, elem2verts and vertices,
  and append it to the linked list of chunks.
  
  Last update:
  ------------
  5Jan15; pass mElem2ChildP to append_elem again.
  4Apr13; make large counters ulong_t
  2Apr12; move chunk size updates to append_vrtx/elem/bndFc.
  5Feb12; farm out append_elem/vrtx/bndFc.
  9Jul11; use init_elem.
  4Apr09; replace varTypeS with varList.
  28Jun07; don't alloc default size for PPvrtx if no mElem2Verts given.
  13May07; alloc only for mEq unknowns, set Pcoor and Punknown pointers for
           each vertex.
  1Sep5; reset pBndPatch after alloc.
  6Jul4; fix pb with !a*b construct in if. Use a*b == instead.
  2Feb4; fix bug with bndPatch allocation.
  5Sep3; allow zero sizes for most components. 
  19Jul99; fix allocation bug: allocated for chunk_struct, rather than vrtx_struct.
           Ouf, costly one.
  24Jun98; allow for an empty pUns. 
  19Oct97; copy the list of bc into each appended chunk.
  : conceived.
  
  Input:
  ------
  obvious

  Changes To:
  -----------
  none
  
  Returns:
  --------
  NULL on failure, a pointer to the new chunk PnewChunk on success.
  
*/

chunk_struct *append_chunk ( uns_s *pUns, int mDim,
			     ulong_t mElems,
			     ulong_t mElem2VertP, ulong_t mElem2ChildP,
			     ulong_t mVerts,
			     ulong_t mBndFaces, ulong_t mBndPatches ) {
  
  chunk_struct *PnewChunk, *PlastChunk = pUns->pRootChunk ;
  int mEq = pUns->varList.mUnknowns ;

  
  /* Set dim. mEq should be set in the same way, fix this. */
  pUns->mDim = mDim ;


  /* Alloc. */
  PnewChunk = make_chunk ( pUns ) ;

  /* Find the last chunk and check whether it contains anything.
     If there are neither vertices nor elements, it is considered
     empty. */
  if ( PlastChunk ) {
    /* There is already a chunk with pUns. */
    while ( PlastChunk->PnxtChunk )
      PlastChunk = PlastChunk->PnxtChunk ;
    
    PnewChunk->PprvChunk = PlastChunk ;
    PnewChunk->nr = PlastChunk->nr + 1 ;
    PlastChunk->PnxtChunk = PnewChunk ;
  }
  else {
    /* Empty pUns. */
    pUns->mChunks = 1 ;
    pUns->pRootChunk = PnewChunk ;
    PnewChunk->PprvChunk = NULL ;
    /* Start chunk numbering at 0. */
    PnewChunk->nr =  0 ;
  }
    

  /* Alloc for elems. */
  append_elem ( PnewChunk, mElems, mElem2VertP, mElem2ChildP ) ;

  /* Vertices, coordinates, unknowns. */
  append_vrtx ( PnewChunk, mVerts, mDim, mEq ) ;

  /* Alloc for boundary patches, faces. */
  append_bndFc ( PnewChunk, mBndPatches, mBndFaces ) ;

  
  /* Relink boundary conditions. */
  make_uns_ppChunk ( pUns ) ;
  
  return ( PnewChunk ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  make_single_pVrtx:
*/
/*! combine the vrtx, coor, unknown fields from all chunks. 
 *
 */

/*
  
  Last update:
  ------------
  13jul20: conceived.
  

  Input:
  ------
  pUns
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s make_single_pVrtx ( uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in make_single_pVrtx"
  
  ret_s ret = ret_success () ;

  if ( !pUns->pRootChunk )
    /* No other chunks, done. */
    return ( ret ) ;

  /* Count. */
  ulong_t mVxAllChunk = 0 ;
  chunk_struct *pChunk = NULL ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    mVxAllChunk += pChunk->mVertsUsed ;
  }

  if ( mVxAllChunk == pUns->pRootChunk->mVerts )
    /* No pVrts with other chunks, done. */
    return ( ret ) ;


  extend_vrtx ( pUns, mVxAllChunk ) ;
  
  return ( ret ) ;
}



/******************************************************************************

  free_uns:
  Remove an unstructured grid.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void free_uns ( uns_s **ppUns ) {
  
  chunk_struct *pChunk ;
  
  for ( pChunk = (*ppUns)->pRootChunk->PnxtChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    free_chunk ( *ppUns, &pChunk ) ;

  /* Free the rootChunk, since free_chunk refuses to. */
  arr_free ( (*ppUns)->pRootChunk ) ;
  arr_free ( (*ppUns)->ppBc ) ;
  arr_free ( (*ppUns)->ppChunk ) ;


  arr_free ( (*ppUns)->pmVertBc ) ;
  arr_free ( (*ppUns)->pmBiBc ) ;
  arr_free ( (*ppUns)->pmTriBc ) ;
  arr_free ( (*ppUns)->pmQuadBc ) ;
  arr_free ( (*ppUns)->pmFaceBc ) ;
  
  free_llEdge ( ( llEdge_s ** ) &((*ppUns)->pllEdge) ) ;

  /* Interfaces. */
  int kS ;
  for ( kS = 0 ; kS < (*ppUns)->mSlidingPlaneSides ; kS++ )
    arr_free ( (*ppUns)->ppSlidingPlaneSide[kS] ) ;

  arr_free ( (*ppUns)->pSlidingPlanePair ) ;


  /* How about removing pllAdEdge? Fix this. */
  
  arr_free ( (*ppUns) ) ;
  *ppUns = NULL ;

  return ;
}

/************************************************************************

  free_chunk:
  Deallocate all the memory linked in a chunk. Close the linked list.

  Last update:
  14Aug98; properly deal with the linked list of chunks.
  25Jul96; conceived.

  Input:
  --------
  pUns
  pChunk

*/

void free_chunk ( uns_s *pUns, chunk_struct **ppChunk ) {
  
  chunk_struct *pChunk ;
  
  if ( !*ppChunk )
    return ;
  pChunk = *ppChunk ;
  
  arr_free ( pChunk->Pelem ) ;
  arr_free ( pChunk->PPvrtx ) ;
  arr_free ( pChunk->Pvrtx ) ;
  arr_free ( pChunk->Pcoor ) ;
  arr_free ( pChunk->PbndFc ) ;
  arr_free ( pChunk->PbndPatch ) ;

  if ( !pChunk->PprvChunk ) {
    /* This is the rootchunk. Keep it, but reset the counters. */
    pChunk->mElems = pChunk->mVerts = 0 ;
    pChunk->mElemsNumbered = pChunk->mElemsMarked = pChunk->mVertsNumbered = 0 ;
    pChunk->mBndPatches = 0 ;
    pChunk->mIntFaces = pChunk->mBndFaces =
      pChunk->mMatchFaces = pChunk->mDegenFaces = 0 ;
  }
  else {
    if ( !pChunk->PnxtChunk ) {
      /* This chunk is at the end. Truncate. */
      pChunk->PprvChunk->PnxtChunk = NULL ;
    }
    else {
      /* This chunk is intermediate. */
      pChunk->PprvChunk->PnxtChunk = pChunk->PnxtChunk ;
      pChunk->PnxtChunk->PprvChunk = pChunk->PprvChunk ;
    }
    arr_free ( pChunk ) ;
    *ppChunk = NULL ;
  }

  if ( pUns )
    /* There is a grid given. Redo the list of chunks. */
    make_uns_ppChunk ( pUns ) ;
  
  return ;
}


/******************************************************************************

  reset_elem:
  constructor for an empty element.
  
  Last update:
  ------------
  11Jul11; use init_elem.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void reset_elems ( elem_struct *pElem, ulong_t mElems ) {

  elem_struct *pEl ;

  for ( pEl = pElem ; pEl < pElem + mElems ; pEl++ ) {
    init_elem ( pEl, noEl, 0, NULL ) ;
  }

  return ;
}



/******************************************************************************

  reset_vrtx:
  constructor for an empty vrtxent.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void reset_verts ( vrtx_struct *pVrtx, ulong_t mVerts ) {

  vrtx_struct *pVx ;

  for ( pVx = pVrtx ; pVx < pVrtx + mVerts ; pVx++ ) {
    pVx->number = 0 ;
    pVx->mark2 = pVx->mark = pVx->singular = pVx->per = 0 ;
    pVx->vxCpt = CP_NULL ;
    
    pVx->Pcoor = pVx->Punknown = NULL ;
  }
  return ;
}

/******************************************************************************
  reset_bndFcVx:   */

/*! Reset list of boundary faces defined by forming vertices.
 */

/*
  
  Last update:
  ------------
  26Jan12: conceived.
  

  Input:
  ------
  pUns->pBndFcVx: pointer to first face to reset
  mBndFcVx: number of faces to reset.

*/

void reset_bndFcVx ( bndFcVx_s *pBndFcVx, ulong_t mBndFcVx ) {

  bndFcVx_s *pBv ;
  for ( pBv = pBndFcVx ; pBv < pBndFcVx + mBndFcVx ; pBv++ ) {
    /* Use the node counter for a test whether filled or not. */
    pBv->mVx = 0 ;
    /* Just for prettyness. */
    pBv->ppVx[0] = NULL ;
    pBv->pBc = NULL ;
  }

  return ;
}


/******************************************************************************

  conv_uns_var:
  Convert variables in unstructured grids.
  
  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  11Dec07; intro primT.
  15Dec06; trace varnames when changing vartypes
  18Jun03; consider anyVar as nothing to do.
  15Oct99; convert freestream variables as well.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int conv_uns_var ( uns_s *pUns, varType_e newVarType ) {
  
  chunk_struct *Pchunk ;
  vrtx_struct *Pvrtx ;
  void (*convFun) ( double varIn[], double varOut[], const int mDim ) ;
  int mDim ;
  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;

  if ( newVarType == pUns->varList.varType )
    /* No conversion necessary. */
    return ( 1 ) ;
  else if ( pUns->varList.varType == noVar )
    /* There are no variables. */
    return ( 1 ) ;
  else if ( pUns->varList.varType == noType )
    /* No conversion defined. */
    return ( 1 ) ;
  
  if ( !var2var( pUns->varList.varType, newVarType, &convFun ) ) {
    sprintf ( hip_msg, "could not convert to this variable type.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  

  pVL->varType = newVarType ;
    

  /* Hydra solution files list a w velocity in 2D. */
  if ( pVL->mUnknFlow == 5 )
    mDim = 3 ;
  else
    mDim = pUns->mDim ;

  /* Convert variable names. */
  if ( newVarType == cons ) {
    strncpy ( pVar[mDim+1].name,"rhoE", LEN_VARNAME ) ;
    strncpy ( pVar[mDim].name,  "rhow", LEN_VARNAME ) ;
    strncpy ( pVar[2].name,     "rhov", LEN_VARNAME ) ;
    strncpy ( pVar[1].name,     "rhou", LEN_VARNAME ) ;
    strncpy ( pVar[0].name,     "rho",  LEN_VARNAME ) ;
  } else if ( newVarType == prim ) {
    strncpy ( pVar[mDim+1].name,"p",    LEN_VARNAME ) ;
    strncpy ( pVar[mDim].name,  "w",    LEN_VARNAME ) ;
    strncpy ( pVar[2].name,     "v",    LEN_VARNAME ) ;
    strncpy ( pVar[1].name,     "u",    LEN_VARNAME ) ;
    strncpy ( pVar[0].name,     "rho",  LEN_VARNAME ) ;
  } else if ( newVarType == primT ) {
    strncpy ( pVar[mDim+1].name,"Temperature",    LEN_VARNAME ) ;
    strncpy ( pVar[mDim].name,  "pressure",    LEN_VARNAME ) ;
    strncpy ( pVar[mDim-1].name,"w-velocity",    LEN_VARNAME ) ;
    strncpy ( pVar[1].name,     "v-velocity",    LEN_VARNAME ) ;
    strncpy ( pVar[0].name,     "u-velocity",  LEN_VARNAME ) ;
  } 
  
  /* Loop over all chunks. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
	  Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
      if ( Pvrtx->number )
	convFun ( Pvrtx->Punknown, Pvrtx->Punknown, mDim ) ;

  /* Freestream variables. */
  convFun ( pVL->freeStreamVar, pVL->freeStreamVar, pUns->mDim ) ;

  return ( 1 ) ;
}


/******************************************************************************

  set_uns_freestream:
  Set freestream values.
  
  Last update:
  ------------
  16Feb17; allow setting of freestream for grids without vars.
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void set_uns_freestream ( uns_s *pUns, double freeStreamVar[] ) {
  
  int nEq, mEq ;

  mEq = MAX ( pUns->mDim+2, pUns->varList.mUnknowns ) ;
  
  for ( nEq = 0 ; nEq < mEq ; nEq++ )
    pUns->varList.freeStreamVar[nEq] = freeStreamVar[nEq] ;

  return ;
}


/******************************************************************************

  traceMinMax:
  Maintain and update bounds of a variable with a descriptive index, e.g. a
  node number.
  
  Last update:
  ------------
  15Oct06: extracted from read_avbp.
  
  Input:
  ------
  pUn         = the value at n
  n           = the current number

  Changes To:
  -----------
  pvalMin/Max = the min and max value so far.
  pnMinMax    = the number where min and max were found
  
*/

void traceMinMax ( const double *pUn, ulong_t n,
                   double *pvalMin, ulong_t *pnMin, double *pvalMax, ulong_t *pnMax ) {

  if ( *pvalMin > *pUn ) {
    *pvalMin = *pUn ;
    *pnMin = n ;
  }
  if ( *pvalMax < *pUn ) {
    *pvalMax = *pUn ;
    *pnMax = n ;
  }
  return ;
}


/******************************************************************************

  min_max_var:
  Find and list minina, maxima of variables, their type and names.
  
  Last update:
  ------------
  17Sep09; print flag, reformat table.
  4Apr09; rename global varCat into varCatNames.
          list group name.
  12Apr3; modify the writeout of varnames on avbp.initialised == 2.
  : conceived.
  
  Input:
  ------
  pUns:
  
*/

void min_max_var ( uns_s *pUns ) {

  const varList_s *pVL = &pUns->varList ;
  const var_s *pVar = pVL->var ;
  const int mVar = pVL->mUnknowns ;
  const char noChar[] = "", *pVarC[MAX_UNKNOWNS], *pVarN[MAX_UNKNOWNS] ;

  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxB, *pVxE, *pVxMin[MAX_UNKNOWNS], *pVxMax[MAX_UNKNOWNS] ;
  double minV[MAX_UNKNOWNS], maxV[MAX_UNKNOWNS], *pU ;
  int i, nB, nE ;

  for ( i = 0 ; i < mVar ; i++ ) {
    minV[i] =  TOO_MUCH ;
    maxV[i] = -TOO_MUCH ;
    pVarC[i] = varCatNames[1] ;
    pVarN[i] = noChar ;
  }


  /* Loop over all chunks. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxB, &nB, &pVxE, &nE ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
      if ( pVx->number && ( pU = pVx->Punknown ) )
        for ( i = 0 ; i < mVar ; i++ ) {
          if ( maxV[i] < pU[i] ) {
            maxV[i] = pU[i] ;
            pVxMax[i] = pVx ;
          }
          if ( minV[i] > pU[i] ) {
            minV[i] = pU[i] ;
            pVxMin[i] = pVx ;
          }
        }


  
  printf ( "  #  name         cat group          vec flag,   min   at    node,"
           "    max   at   node:\n" ) ;
  for ( i = 0 ; i < mVar ; i++ )
    printf ( "%3i: %-12.12s %-3.3s %-12.12s %4i,%4i, %10.3e %8"FMT_ULG", %10.3e %8"FMT_ULG"\n",
             i+1, pVar[i].name, varCatNames[pVar[i].cat], pVar[i].grp, 
             pVar[i].isVec, pVar[i].flag, 
             minV[i], pVxMin[i]->number,  maxV[i], pVxMax[i]->number ) ;

  printf ( "\n") ;
  return ;
}



/******************************************************************************

  ini_default_freestream:
  Initialize freestream variables. This should only be needed for dpl formats,
  so just add 1. after the fourth variable.
  
  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  3Dec99; allow freestream variables for flow variables only.
  15Oct99: conceived.
  
  Input:
  ------


  Changes To:
  -----------
*/

void ini_default_freestream ( uns_s *pUns ) {

  double *fs = pUns->varList.freeStreamVar, fscons[MAX_DIM+2] ;
  int kU ;
  void (*convFun)(double*, double*, const int) ;

  /* Initialise to a freestream Mach number of 99. */
  get_freestream_mach ( fscons, pUns->mDim, 99., 0., 0. ) ; 

  /* Convert to the required variable type. */
  if ( pUns->varList.varType != cons && pUns->varList.varType != noVar ) {
    /* Find the conversion function. */
    var2var( cons, pUns->varList.varType, &convFun ) ;
    convFun ( fscons, fscons, pUns->mDim ) ;
  }

  for ( kU = 0 ; kU < pUns->mDim+2 ; kU++ )
    fs[kU] = fscons[kU] ;
  
  return ;
}


/******************************************************************************

  make_uns_sol:
  Add a solution field to a grid.
  
  Last update:
  ------------
  10Nov17; assign all variable names.
  26Apr10; specify variable names.
  4Apr09; replace varTypeS with varList.
  2Sep03; initialise category.
  21Dec02: conceived.
  
  Input:
  ------
  pUns
  mUn: number of unknowns.
  solType: type.

  Changes To:
  -----------
  pUns
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_uns_sol ( uns_s *pUns, int mUn, const char *solType ) {

  char varNmCons[][5] = { "rho", "rhou", "rhov", "rhow", "rhoE" } ;
  char varNmPrim[][5] = { "rho", "u", "v", "w", "p" } ;

  chunk_struct *pChunk = pUns->pRootChunk ;
  vrtx_struct *pVx ;
  double *pUn ;
  int n, mVx = pUns->mVertsNumbered ;
  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;
  
  pVL->mUnknowns = pVL->mUnknFlow = mUn ;

  if ( !strncmp( solType, "prim", 4 ) ) {
    pVL->varType = prim ;
  }
  else if ( !strncmp( solType, "cons", 4 ) ) {
    pVL->varType = cons ;
  }
  else {
    sprintf ( hip_msg, "variable type %s undefined, using conservative.\n",
             solType ) ;
    hip_err ( warning, 1, hip_msg ) ;
    pVL->varType = cons ;
  }
  /*  pVL->category = ns ;*/

  for ( n = 0 ; n < mUn ; n++ ) {
    pVar[n].cat = ns ;
    strncpy ( pVar[n].grp, "GaseousPhase", LEN_VARNAME ) ;
    pVar[n].flag = 1 ;
    if ( !strncmp( solType, "prim", 4 ) )
      strcpy( pVar[n].name, varNmPrim[n] ) ;
    else
      strcpy( pVar[n].name, varNmCons[n] ) ;
  }


  pUn = pChunk->Punknown = arr_malloc ( "pChunk->Pvrtx in make_uns_sol", pUns->pFam,
                                        (mVx+1)*mUn, sizeof( *pChunk->Punknown ) ) ;
  
  
  /* Loop over all chunks. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    for ( pVx = pChunk->Pvrtx+1 ;
	  pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++ )
      if ( pVx->number ) {
        pUn += mUn ;
        pVx->Punknown = pUn ;
        for ( n = 0 ; n < mUn ; n++ )
          pUn[n] = 1. ;
      }

  return ( 1 ) ;
}


/******************************************************************************

  del_uns_sol:
  Remove a solution from an unstructured grid.
  
  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void delete_uns_sol ( uns_s *pUns ) {

  chunk_struct *Pchunk ;
  vrtx_struct *Pvrtx ;
  varList_s *pVL = &(pUns->varList) ;

  pVL->mUnknowns = 0 ;
  pVL->varType = noVar ;


  /* Loop over all chunks. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
    
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
	  Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ )
      if ( Pvrtx->number )
	Pvrtx->Punknown= NULL ;

    arr_free ( Pchunk->Punknown ) ;
    Pchunk->Punknown = NULL ;
  }
  
  return ;
}


/******************************************************************************

  make_uns_ppBc:
  Make a table of all bc with an uns grid.
  
  Last update:
  ------------
  8Jul18; test for pBndFc->invalid flag, when counting active bnd faces.
  19Jul17; replace coarse grid copy of ppBc with memcpy.
  29jun17; order ppBc by geoType: first bnd, then interface ....
  7Apr13; allocate for ulong_t ints by using the actual pointer.
  3Sep12; take pBc form pBndFc, not pBndPatch.
  17Nov98; allow the user to choose the ordering of patches.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_uns_ppBc ( uns_s *pUns ) {
  
  chunk_struct *pChunk ;
  int iBc, mBc = 0, activeBc ;
  bcGeoType_e geoTypeLoop ;
  bc_struct *pBc ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc ;

  if ( pUns == pUns->pUnsFinest ) {
    /* Finest grid. Count. */
    pUns->mBc = pUns->mBcBnd = 0 ;
    arr_free ( pUns->ppBc ) ;
    pUns->ppBc = NULL ;
    arr_free ( pUns->ppRootPatchBc ) ;
    pUns->ppRootPatchBc = NULL ;
    
    /* Loop over all patches in this grid. Number bnd patches first, then
       interfaces patches. Then the others, but there shouldn't be any matching
       ones, as they have been removed. */
    for ( geoTypeLoop = bnd ; geoTypeLoop < noBcGeoType ; geoTypeLoop++ ) {
      for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
        for ( pBndPatch = pChunk->PbndPatch+1 ;
              pBndPatch <= pChunk->PbndPatch + pChunk->mBndPatches ; pBndPatch++ ) {

          pBc = pBndPatch->Pbc ; activeBc = 0 ;
          if ( pBc->geoType == geoTypeLoop ) { 
            /* Next bc of the desired geoType. Is this bc active? */
            for ( pBndFc = pBndPatch->PbndFc ;
                  pBndFc < pBndPatch->PbndFc + pBndPatch->mBndFc ; pBndFc++ )
              if ( !pBndFc->invalid && pBndFc->nFace && pBndFc->Pelem && 
                   pBndFc->Pelem->number && !pBndFc->Pelem->invalid ) {
                activeBc = 1 ;
                // pBc = pBndFc->Pbc ; Patches have the same bc for all faces? 
                break ;
              }
          }
        
          if ( activeBc ) {
            /* Find this bc in pUns. */
            for ( iBc = 0 ; iBc < pUns->mBc ; iBc++ )
              if ( pBc == pUns->ppBc[iBc] )
                /* This bc exists already in pUnsTo. */
                break ;
            if ( iBc == pUns->mBc ) {
              /* Finest grid. Add this bc to pUns. */
              mBc = ++pUns->mBc ;
              pUns->ppBc = arr_realloc ( "ppBc in make_uns_ppBc", pUns->pFam, pUns->ppBc,
                                         mBc, sizeof( *pUns->ppBc ) ) ;
              pUns->ppRootPatchBc = arr_realloc ( "ppRootPatchBc in make_uns_ppBc",
                                                  pUns->pFam, pUns->ppRootPatchBc,
                                                  mBc, sizeof( *pUns->ppRootPatchBc ) ) ;
            
              pUns->ppBc[ mBc-1 ] = pBc ;
              pUns->ppRootPatchBc[ mBc-1 ] = pBndPatch ;
            }
          }
        }
      }

      /* Number of patches with geoType bnd. Only those are actual patches with bcs. */
      if ( geoTypeLoop == bnd )
        pUns->mBcBnd = mBc ;
    }


    /* Sort the bnd patches (only those, not inter) for proper ordering and relink the patches. */
    qsort ( pUns->ppBc, pUns->mBcBnd, sizeof( bc_struct * ), bcOrderCompare ) ;    
  }
  
  else {
    /* Pick up the list from the finest grid. */
    if ( pUns->mBc != pUns->pUnsFinest->mBc || !pUns->ppBc ) {
      /* Resize, re-alloc */
      mBc = pUns->mBc = pUns->pUnsFinest->mBc ;
      pUns->ppBc = arr_realloc ( "ppBc in make_uns_ppBc", pUns->pFam, pUns->ppBc,
                                 mBc, sizeof( *(pUns->ppBc) ) ) ;
    }
    /* Make a physical copy, rather than a pointer alias, as ppBc on the 
       finest grid may be rebuilt during renumbering. */
    memcpy( pUns->ppBc, pUns->pUnsFinest->ppBc, mBc*sizeof( *(pUns->ppBc) ) ) ;

    /* Find a root-patch for each bc. */
    pUns->ppRootPatchBc =
      arr_realloc ( "ppRootPatchBc in make_uns_ppBc", pUns->pFam,  pUns->ppRootPatchBc,
                    mBc, sizeof( *pUns->ppRootPatchBc ) ) ;
  }
  
  /* Realloc boundary node and face counters. */
  pUns->pmVertBc  = arr_realloc ( "pmVertBc in make_uns_ppBc", pUns->pFam,
                                  pUns->pmVertBc, mBc, sizeof( *pUns->pmVertBc ) ) ;
  pUns->pmBiBc    = arr_realloc ( "pmBiBc in make_uns_ppBc", pUns->pFam,
                                  pUns->pmBiBc, mBc, sizeof( *pUns->pmBiBc ) ) ;
  pUns->pmTriBc   = arr_realloc ( "pmTriBc in make_uns_ppBc", pUns->pFam,
                                  pUns->pmTriBc, mBc, sizeof( *pUns->pmTriBc ) ) ;
  pUns->pmQuadBc  = arr_realloc ( "pmQuadBc in make_uns_ppBc", pUns->pFam,
                                  pUns->pmQuadBc, mBc, sizeof( *pUns->pmQuadBc ) ) ;
  pUns->pmFaceBc  = arr_realloc ( "pmFaceBc in make_uns_ppBc",  pUns->pFam,
                                  pUns->pmFaceBc,mBc, sizeof( *pUns->pmFaceBc ) ) ;

  
  link_uns_bcpatch ( pUns ) ;

  return ( 1 ) ;
}



/******************************************************************************

  uns_compress_bc:
  Collapse all bc patches with the same text label into one.
  
  Last update:
  ------------
  5Sep17; treat inactive boundary conditions.
  8Jul13; fix bug #563 with bc-compress following multiple merges.
  11Apr3: conceived.
  
  Input:
  ------


  Changes To:
  -----------
  
*/

void uns_compress_bc ( uns_s *pUns ) {

  bc_struct **pBc1, *pBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  bndPatch_struct *pBndPatch, *pBndPatchBeg, *pBndPatchEnd ;
  chunk_struct *pChunk ;
  int mBc, nBc ;

  /* Find the maximum pBc->nr. */
  mBc = 0 ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    mBc = MAX( mBc, pUns->ppBc[nBc]->nr ) ;
  
  
  /* Find the first occurence of each bc-text. */
  pBc1 = arr_calloc ( "pBc1 in uns_compress_bc", pUns->pFam,
                      mBc+1, sizeof( *pBc1 ) ) ;

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBc = pUns->ppBc[nBc] ;
    pBc1[pBc->nr] = find_bc ( pBc->text, 2 ) ;
  }

  /* Reset all face bc pointers. */
  pChunk = NULL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
    for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
      if ( pBc1[ pBndFc->Pbc->nr ] )
        pBndFc->Pbc = pBc1[ pBndFc->Pbc->nr ] ;
    }

  
  /* Reset all patch bc pointers. */
  pChunk = NULL ;
  while ( loop_bndPatches ( pUns, &pChunk, &pBndPatchBeg, &pBndPatchEnd ) )
    for ( pBndPatch = pBndPatchBeg ; pBndPatch <= pBndPatchEnd ; pBndPatch++ ) {
      if ( pBc1[ pBndPatch->Pbc->nr ] )
        pBndPatch->Pbc = pBc1[ pBndPatch->Pbc->nr ] ;
    }

  arr_free ( pBc1 ) ;
  
  /* Redo the list of bcs active in this grid. */
  make_uns_ppBc ( pUns ) ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  is_fc_below_hyperplane:
*/
/*! Are all nodes of a face below a given hyperplane or inside a given hypervolume?
 *
 */

/*
  
  Last update:
  ------------
  14May20: conceived.
  

  Input:
  ------
  pElem: the element
  nFace: the face.
  geo: the hyperplane/volume.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 if below/inside, 0 otherwise
  
*/

int is_fc_below_hyperplane ( const elem_struct *pElem, const int nFace, const geo_s *pGeo ) {
#undef FUNLOC
#define FUNLOC "in is_fc_below_hyperplane"
  
  const faceOfElem_struct *pFoE = elemType[ pElem->elType].faceOfElem + nFace ;
  const int mDim = elemType[ pElem->elType].mDim ;
  const int mVxFc = pFoE->mVertsFace ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  int k ;
  vrtx_struct *pVx ;
  for ( k = 0 ; k < mVxFc ; k++ ) {
    pVx = ppVx[ pFoE->kVxFace[k] ] ;

    if ( !( is_in_geo ( pVx->Pcoor, mDim, pGeo ) ) )
      return ( 0 ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  split_bc_patch:
*/
/*! Given a geometric hyperplane, split a bc patch in to faces either side of it.
 *
 */

/*
  
  Last update:
  ------------
  14May20: conceived.
  

  Input:
  ------
  pUns:
  exprBc0: split patches with a bc that matches this expr.
  bcSplitText: bc label of new bc
  pGeo: put all bnd faces inside this hyperplane/vol into the new bc.
    
  Returns:
  --------
  pointer to the new bc if successfull, NULL otherwise.
  
*/

ret_s split_uns_bc_patch ( uns_s *pUns, const char *exprBc0, const geo_s *pGeo,
                            char *bcSplitAppend ) {
#undef FUNLOC
#define FUNLOC "in split_bc_patch"
  ret_s  ret = ret_success () ;

  if ( bcSplitAppend[0] == '\0' ) {
    ret = hip_err ( warning, 1, "bc label appendix for split bc "FUNLOC" must be non-null.\n"
              "            Split command ignored." ) ;
    return ( ret ) ;
  }

  char bcSplitText[LINE_LEN] ;

  bc_struct *pBc, *pBc2 ;
  int foundNewBc = 0 ;
  for ( pBc = NULL ; loop_bc_expr( &pBc, exprBc0 ) ; ) {
  
    specchar2underscore ( bcSplitAppend ) ;
    snprintf ( bcSplitText, LINE_LEN-1, "%s%s", pBc->text, bcSplitAppend ) ; 
  
    if ( ( pBc2 = find_bc ( bcSplitText, 2 ) ) ) {
      ret = hip_err ( warning, 1, "bc label for split bc "FUNLOC" must not exist."
                      " Split command ignored." ) ;
      return ( ret ) ;
    }
    if ( !( pBc2 = find_bc ( bcSplitText, 1 ) ) ) {
      ret = hip_err ( warning, 1, "could not create bc with new label for split bc "FUNLOC"."
                      " Split command ignored." ) ;
      return ( ret ) ;
    }
  


    chunk_struct *pCh = NULL ;
    bndPatch_struct *pBndPatch ;
    bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
    while ( loop_bndFaces ( pUns, &pCh, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        if ( pBndFc->Pbc == pBc ) {
          // face on the bc to be split.
          if ( is_fc_below_hyperplane ( pBndFc->Pelem, pBndFc->nFace, pGeo ) ) {
            foundNewBc = 1 ;
            pBndFc->Pbc = pBc2 ;
          }
        }
    }

  }

  if ( foundNewBc ) {
    /* Rebuild patch/face lists. */
    (pUns->mBc)++ ;
    make_uns_bndPatch ( pUns ) ;
    make_uns_ppBc ( pUns ) ;
  }

  return ( ret ) ;
}

ret_s split_uns_bcPatch_arg () {
#undef FUNLOC
#define FUNLOC "split_uns_bcPatch_arg"

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  if ( !pUns )
    return ( hip_err ( warning, 1, "bc patch splitting can only be done for unstructured grids,"
                       "           but there isn't any. No split done." ) ) ;

  if ( pUns->mHyVol == 0 )
    return ( hip_err ( warning, 1, "bc patch splitting needs a def of hyperplane/vol on this grid,"
                       "           but there isn't any. No split done." ) ) ;
   
  
  char exprBc0[LINE_LEN] ;
  if ( !eo_buffer() )
    read1string ( exprBc0 ) ;
  else
    return ( hip_err ( warning, 1, "no bc to split is speciied. No split done." ) ) ;

  char bcSplitAppend[LINE_LEN] ;
  sprintf ( bcSplitAppend, "_split" ) ;
  if ( !eo_buffer() )
    read1string ( bcSplitAppend ) ;

  return ( split_uns_bc_patch ( pUns, exprBc0, pUns->pHyVol, bcSplitAppend ) ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  create_interFc_zones:
*/
/*! Create an interface boundary between two sets of zones.
 *
 */

/*
  
  Last update:
  ------------
  20Dec17: conceived.
  

  Input:
  ------
  pUns: grid
  mZonesL: number of 'left' zones.
  iZoneL: list of 'left' zones.
  mZonesC: number of 'right', or other side zones.
  iZoneC: list of 'right' zones.

    
  Returns:
  --------
  pointer to the new bc.
  
*/

bc_struct *create_interFc_zones (  uns_s *pUns,
                                   const int mZonesL, const int iZoneL[mZonesL],
                                   const int mZonesC, const int iZoneC[mZonesL],
                                   int *mBndFcZn, int *mBcZn,
                                   char bcLbl[MAX_BC_CHAR] ) {
#undef FUNLOC
#define FUNLOC "create_interFc_zones"

  hip_err ( fatal, 0, "needs completion of code "FUNLOC"." ) ;

  /* Make a unique label for this bc. */
  char someStr[LINE_LEN] ;
  if ( !find_bc ( bcLbl, 2 ) ) {
    /* Boundary exists, add a specialising postfix. */
    if ( strlen( bcLbl) < LINE_LEN-3 ) {
      sprintf ( someStr,"_%2d", pUns->nr ) ;
      strcat ( bcLbl, someStr ) ;
      if ( !find_bc ( bcLbl, 2 ) ) {
        hip_err ( fatal, 0, "can't find a inique bcLabel "FUNLOC"." ) ;
      }
    }
    else {
      hip_err ( fatal, 0, "can't append to bcLabel "FUNLOC"." ) ;
    }
  }
  bc_struct *pBc = find_bc ( bcLbl, 1 ) ;

  /* Count the number of new faces. */

  /* Allocate as a new chunk with bndfc only. */

  /* Fill */

  /* Rebuild array lists. */
  make_uns_ppChunk ( pUns ) ;
  make_uns_ppBc ( pUns ) ;
  
  
  return ( pBc ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  make_uns_mbMap:
*/
/*! make a map how vx of block struct grid map onto the uns vx.
 *
 */

/*
  
  Last update:
  ------------
  12Mae18: conceived.
  

  Input:
  ------
  pUns: uns grid.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int make_uns_mbMap ( uns_s *pUns ) {

  chunk_struct *pChunk = NULL ;
  int nCh ;
  int mVx ;
  vrtx_struct **ppIJK ;
  vrtx_struct *pVx, *pVrtx ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    
    /* Check whether sizes match. */
    if ( !pChunk->nBlock || !pChunk->blockDim[0] )
      hip_err ( fatal, 0,
                "block not listed with chunk, can't do make_uns_mbMap." ) ;

    mVx = pChunk->blockDim[0]*pChunk->blockDim[1]*pChunk->blockDim[2] ;
    if ( mVx != pChunk->mVerts )
      hip_err ( fatal, 0,
                "block and chunk dim mismatch, can't do make_uns_mbMap." ) ;

    /* Alloc. */
    pChunk->ppIJK2Vx = arr_malloc ( "pChunk->pIJK2Vx in get_mbVerts",
                                    pChunk->pUns->pFam,
                                    mVx+1, sizeof( *pChunk->ppIJK2Vx ) ) ;
    

    nCh = pChunk->nr ;
    pVrtx = pChunk->Pvrtx ;
    for ( pVx = pVrtx+1, ppIJK = pChunk->ppIJK2Vx+1 ;
          pVx <= pVrtx+mVx ;
          pVx++, ppIJK++ ) {
      if ( pVx->vxCpt.nCh == nCh ) {
        /* Same chunk. Just recompute pointer offset, to catch
           in-chunk replacements (block wrapped onto itself. */
        *ppIJK = pVrtx+  pVx->vxCpt.nr ;
      }
      else {
        *ppIJK = de_cptVx ( pUns, pVx->vxCpt ) ;
      }
    }
  }

  return ( 0 ) ;
}


/******************************************************************************

  make_uns_ppChunk:
  Make a table with all chunks of an uns grid. Renumber all chunks in the grid
  consecutively and number all vertices globally.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_uns_ppChunk ( uns_s *pUns ) {
  
  chunk_struct *pChunk, **ppChunk ;
  int mVerts = 0, mElems = 0, nVx ;

  pUns->mChunks = 0 ;
  arr_free ( pUns->ppChunk ) ;

  /* Count all chunks in this grid. */
  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk )
    pUns->mChunks++ ;

  ppChunk = pUns->ppChunk = arr_malloc ( "ppChunk in make_uns_ppChunk", pUns->pFam,
                                         pUns->mChunks, sizeof( *pUns->ppChunk ) ) ;

  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {

    if ( pChunk->nr > MAX_CPT_CHUNKS ) {
      sprintf ( hip_msg, "%s\n%s%d\n%s%d\n%s\n",
                " too many chunks for this grid in make_uns_ppChunk:",
                "        you want ", pChunk->nr,
                "        you can have ", MAX_CPT_CHUNKS,
                "        you can change the definition of cpt_s in cpre_uns.h" ) ;

      hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( pChunk->mVerts > MAX_CPT_VXNR ) {
      sprintf ( hip_msg, "%s\n%s%"FMT_ULG"\n%s%d\n%s\n%s\n",
                " too many nodes for this chunk in make_uns_ppChunk:",
                "        you want ", pChunk->mVerts,
                "        you can have ", MAX_CPT_VXNR,
                "        you can split your chunk or",
                "        you can change the definition of cpt_s in cpre_uns.h" ) ;

      hip_err ( fatal, 0, hip_msg ) ;
    }


    /* Number chunks consecutively starting with 0. */
    pChunk->nr = ppChunk - pUns->ppChunk ;
    *(ppChunk++) = pChunk ;
    pChunk->nLstVxPrvChk = mVerts ;

    /* Number all vertices globally. */
    for ( nVx = 1 ; nVx <= pChunk->mVerts ; nVx++ ) {
      pChunk->Pvrtx[nVx].vxCpt.nCh = pChunk->nr ;
      pChunk->Pvrtx[nVx].vxCpt.nr = nVx ;
    }

    mVerts += pChunk->mVerts ;
    mElems += pChunk->mElems ;
  }

  pUns->mVertsAlloc = mVerts ;
  pUns->mElemsAlloc = mElems ;
  
  return ( 1 ) ;
}

/******************************************************************************

  make_uns_bndPatch:
  Given a possibly unordered list of boundary faces, order them for bc
  and generate the bndPatch index.
  
  Last update:
  ------------
  14Mar12; filter out invalid/removed boundary faces.
  29Jun07: extracted from read_uns_n3s.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns->p*Chunk->PbndPatch
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_uns_bndPatch ( uns_s *pUns ) {

  int mBc, mBndFc, k ;
  bndPatch_struct *pBP = NULL ;
  bc_struct *pBc, bcNULL ;
  chunk_struct *pChunk ;
  bndFc_struct *pBf ;

  /* Set a high number for a NULL bc, so faces referencing it sort to the end. */
  bcNULL.nr = pUns->mBc+999 ;

  for ( pChunk = pUns->pRootChunk ; pChunk ; pChunk = pChunk->PnxtChunk ) {
    mBndFc = pChunk->mBndFaces ;

    /* Filter out all removed boundary faces. */
    for ( k = 1 ; k <= mBndFc ; k++ ) {
      pBf = pChunk->PbndFc+k ;
      if ( pBf->invalid || pBf->Pelem == NULL ) {
        pBf->Pbc = &bcNULL ;
        pBf->invalid = 1 ;
      }
    }


    /* Order faces for boundary conditions. */
    qsort ( pChunk->PbndFc+1, mBndFc, sizeof ( bndFc_struct ), bfBcNrCompare ) ;

    /* Allocate a list of boundary patches, can be at most all present in the grid. */
    pChunk->PbndPatch = arr_realloc ( "pChunk->PbndPatch in make_uns_bndPatch",
                                      pUns->pFam, pChunk->PbndPatch,
                                      pUns->mBc+1, sizeof( *pChunk->PbndPatch ) ) ;

    /* Reset the list. */
    int nBc ;
    for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
      pChunk->PbndPatch->mBndFc = 0 ;
      pChunk->PbndPatch->PnxtBcPatch = NULL ;
    }

    pBc = NULL ;
    mBc = 0 ;
    for ( k = 1 ; k <= mBndFc ; k++ ) {
      pBf = pChunk->PbndFc+k ;
      if ( !pBf->invalid ) {      
        if ( pBc != pChunk->PbndFc[k].Pbc ) {
          /* New Patch. */
          pChunk->mBndPatches = ++mBc ;
          pBP = pChunk->PbndPatch+mBc ;
          
          pBP->Pchunk = pChunk ;
          pBP->Pbc = pBc = pChunk->PbndFc[k].Pbc ;
          pBP->PbndFc = pChunk->PbndFc + k ;
          pBP->mBndFc = 1 ;
        }
        else
          /* One more face. */
          pBP->mBndFc++ ;
      }
    }  


    if ( mBc > pUns->mBc ) {
      sprintf ( hip_msg, "expected %d, found %d bc in make_uns_bndPatch.\n", 
                pUns->mBc, mBc ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  make_uns_ppBc ( pUns ) ;
  link_uns_bcpatch ( pUns ) ;


  return ( 1 ) ;
}



/******************************************************************************

  link_uns_bcpatch:
  Establish the linked list pointers between all patches of the same
  boundary condition over all chunks.
  
  Last update:
  4May96: conceived.
  
  Input:
  ------
  pUns
  
  Changes to:
  ----------
  Pchunk->PbndPatch
  
*/

void link_uns_bcpatch ( uns_s *pUns ) {
  
  chunk_struct *Pchunk ;
  bndPatch_struct *PbndPatch, *PlastPatch = NULL ;
  bc_struct *Pbc ;
  int nBc ;

  /* Reset the root patch pointers. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    Pbc = pUns->ppBc[nBc] ;
    pUns->ppRootPatchBc[nBc] = NULL ;

    for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
      /* There are boundary patches in this chunk. */
      for ( PbndPatch = Pchunk->PbndPatch + 1 ;
	    PbndPatch <= Pchunk->PbndPatch + Pchunk->mBndPatches; PbndPatch++ )
	if ( PbndPatch->Pbc == Pbc ) {
	  if ( pUns->ppRootPatchBc[nBc] ) {
	    /* Append this patch. */
	    PlastPatch->PnxtBcPatch = PbndPatch ;
	    PlastPatch = PbndPatch ;
	  }
	  else
	    /* First patch. */
	    pUns->ppRootPatchBc[nBc] = PlastPatch = PbndPatch ;
	  PbndPatch->PnxtBcPatch = NULL ;
	}
  }
  
  return ;
}


/******************************************************************************

  make_vx2elem:
  Create a list of vertex to element pointers.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  12May11, check/abort on successful nitial alloc.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

llToElem_s *make_vxToElem ( uns_s *pUns ) {
  
  const elemType_struct *pElT ;
  int kVx ;
  llToElem_s **ppllVxToElem = &(pUns->pllVxToElem) ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
				
  free_toElem ( ppllVxToElem ) ;
  make_toElem ( ppllVxToElem, pUns->pFam, pUns->mVertsAlloc ) ;

  if ( !*ppllVxToElem ) {
    hip_err ( fatal, 0, "could not allocate vertex to elem list in make_vxToElem.\n" ) ;
  }

  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid ) {
	pElT = elemType + pElem->elType ;
	
	for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
	  if ( !add_toElem ( ppllVxToElem, pElem->PPvrtx[kVx]->number, pElem ) ) {
	    sprintf ( hip_msg, 
                      "failed to add elem %"FMT_ULG" for vertex %"FMT_ULG" in make_vxToElem.\n",
		      pElem->number, pElem->PPvrtx[kVx]->number ) ;
	    hip_err ( fatal, 0, hip_msg ) ; }
      }

  return ( *ppllVxToElem ) ;
}




/******************************************************************************

  find_el_walk:
  Find an element containing a location using an initial guess for a closest
  vertex and walking on the mesh from there using vertex to element pointers.
  
  Last update:
  ------------
  13Feb19; promote n from int to ulong_t.
  2Jul12; recalculate kFc when exiting the loop if mSteps is exceeded.
  17Dec11: nasty fix, just eject from the contains_co loop after fixed number
           to work over non-convex geometries that lead to endless loops.
  28Jun06; catch endless loop due to round-off.
  14Sep06; return pkFc.
  12Feb04: conceived.
  
  Input:
  ------
  pUns:  the mesh with filled pllVxToElem
  pCo: the coordinates to contain
  nVx:   the number of the vertex to start with.
  
  Changes to:
  -----------
  pkFc: 0 if returned elem contains pCo, the outward face to pCo otherwise.

  Returns:
  --------
  a pointer to the containing element on success
  
*/


elem_struct *find_el_walk ( uns_s *pUns, const double *pCo, const vrtx_struct *pVx,
                            int *pkFc ) {

  if ( !pUns->pllVxToElem )
    hip_err ( fatal, 0, "find_el_walk needs the vertex to element list pllVxToElem. This shouldn't have happened." ) ; 

  const int mDim = pUns->mDim ;

  elem_struct *pElem = NULL, *pElem2, *pElem1=NULL, *pElBest ;
  ulong_t n ;
  int kFc, kFcBest ;

  *pkFc = 0 ;

  /* Pick the first element listed with pVx to start with. */
  n = 0 ;
  loop_toElem ( pUns->pllVxToElem, pVx->number, &n, &pElem ) ;

  /* Loop over each coarse grid pivot vertex in the walk. */
  kFc = -1 ;
  int mSteps ;
#define MSTEPS (100)
  for ( mSteps = 0 ; kFc && mSteps < MSTEPS ; mSteps++ ) {
    kFc = elem_contains_co ( pElem, pCo, -1 ) ;

    if ( kFc ) {
      /* The search location is outside this element. */
      /* Proceed with the element on the other side of this face. */
      pElem2 = find_el_face ( pUns, mDim, pElem, kFc ) ;
      if ( !pElem2 ) {
        /* No element found that shares this face. Could be a boundary
           face on the other side but first try whether there is another face with
           a neighbour in the search direction. */

        /* using the newly introduced search for the best face with kFc=-1 as
           initial face for elem_contains_co, this bit of code won't work
           any longer. 
           We may not need it with a better normal.
           if ( 0 ( kFc = elem_contains_co ( pElem, pCo, kFc ) ) &&
           ( pElem2 = find_el_face ( pUns, mDim, pElem, kFc ) ) )
           pElem = pElem2 ;
           else
        */
        /* No containing element found in the walk. */
        break ;
      }
      else if ( pElem1 == pElem2 ) {
        /* Due to round-off we can have both sc prods on either face be
           negative. Pick one side if we're bouncing back and forth around
           the same face. */
        kFc = 0 ;
      }
      else {
        pElem1 = pElem ;
        pElem = pElem2 ;
      }
    }
  }

  if ( mSteps >= MSTEPS ) {
    /* endless loop. Should only happen on very distorted meshes due to
       finite precision. Maybe a bug? Recalculate kFc. */
    kFc = elem_contains_co ( pElem, pCo, 0 ) ;
  }


  if ( kFc ) {
    /* No containing element found in the walk. Memorise the best elem in the walk
       and then try all connected elems. */
    pElBest = pElem ;
    kFcBest = kFc ;

    while ( kFc && loop_toElem ( pUns->pllVxToElem, pVx->number, &n, &pElem2 ) ) {
      kFc = elem_contains_co ( pElem2, pCo, 0 ) ;
    }
    
    if ( !kFc )
      /* Containing cell found. */
      pElem = pElem2 ;
    else {
      /* In case we want to check fine grid nodes outside the c.g. domain explicitly.
         pElem = NULL ;
         *pkFc = 0 ; */
      pElem = pElBest ;
      *pkFc = kFcBest ;
    }
  }

  return ( pElem ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_el_tree_walk:
*/
/*! Find an element containing a coordinate by tree and walk searches. 
 *
 */

/*
  
  Last update:
  ------------
  14Dec17; rename intPol_dist_inside to intFullTol to clarify.
  10Nov17; adjust logic to follow doc for global check for dist < intFcTol*hEdge
  13Jul17: extracted from uns_interpolate.
  

  Input:
  ------
  pVxTo: target vertex that should be contained in the element
  pUnsFrom: grid to search on
  pTree: tree structure for nodes of the grid to search on
  intPolRim: the allowable distance to interpolate outside the domain is 
             intPolRim*hEdge where hEdge is the max edge-length of the nearest face.
  intFcTol: When to consider a vertex to be inside an element: 
            the smallest face distance is closer than this many multiples of hElem,
            the typical size of the element.
            (see set.c)
  intPol_dist_inside: if within this multiple of hElem from a node, launch a global
                      search and compute face distances.

  Changes to:
  ----------
  *pmOut: Points 
  *pmReallyOut:

  Output:
  -------
  *pmOut: number of non-contained points.
  *pmReallyOut: number of points also outside of the 'tolerance' zone.
  *pdomainOverlap: is the target within the zone considered overlapped by the 
                   domain to be searched?
    
  Returns:
  --------
  containing elemnt if found, NULL otherwise.
  
*/

elem_struct* find_el_tree_walk ( const vrtx_struct *pVxTo,
                                 uns_s *pUnsFrom, kdroot_struct *pTree,
                                 const double intPolRim, const double intFcTol,
                                 const double intFullTol,
                                 int *pmOut, int *pmReallyOut, int *pdomainOverlap ) {

  double dist ;
  vrtx_struct *pVxNrst = kd_nearest_data ( pTree, pVxTo, &dist ) ;
  /* Walk from there to the containing element. */
  int kFc ;
  elem_struct *pEl = find_el_walk ( pUnsFrom, pVxTo->Pcoor, pVxNrst, &kFc ) ;
  elem_struct *pElWalk = pEl ;

  *pdomainOverlap = 1 ;
  
  double hEdge ;
  if ( kFc ) {
    /* No outright contaiment, */
    (*pmOut)++ ;
    
    /* but maybe good enough? First try a more accurate distance calculation. */
    dist = point_dist_face ( pEl, kFc, pVxTo->Pcoor, &hEdge ) ; 


    /* Note: intFcTol is the multiple of the local edge length that
       a point can be away from an element and still be considered to
       be covered by that element. E.g. refined grids on convex
       boundaries. */
    if (  dist < intFullTol*hEdge ) {
      /* Only do more detailed searches if within the tolerance for
         full search from the domain.  */
      min_dist_face_elems ( pUnsFrom, pVxTo->Pcoor, &dist,
                            (const elem_struct **) &pEl, &kFc ) ;
    }
    
    if ( dist > intFcTol*hEdge )
      /* Still no containment, and too far away from nearest face. */
      (*pmReallyOut)++ ;

    if ( dist > MAX(intPolRim,intFcTol)*hEdge ) {
      /* So far outside the donor domain that no further seach is needed. */ 
      *pdomainOverlap = 0 ; 
    }

    
  }

  return ( pEl ) ;
}


/******************************************************************************

  find_el_face:
  Find an element on the other side of the face given the element on this
  side and the number of the face.
  
  Last update:
  ------------
  13Feb19; promote possibly large int to ulong_t. Intro pack_ulg.
  18Dec16; remove duplicates and order vertex list to allow for collapsed elems.
  21Feb16; support degenerate elements.
  12Feb04: conceived.
  
  Input:
  ------
  pUns:
  mDim:
  pElem: the element on this side of the face.
  kFc:   the number of the face. Note: face numbers run 1 .. mFaces.
  
  Returns:
  --------
  NULL on failure, a pointer to the containing element on success
  
*/


elem_struct *find_el_face ( uns_s *pUns, const int mDim, 
                            const elem_struct *pElem, const int kFc ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE = pElT->faceOfElem + kFc ;
  int mVxFc = pFoE->mVertsFace ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  ulong_t nVxFc[MAX_VX_FACE], nElV[MAX_VX_FACE]={0}, matchEl ;
  int found=0, k ;
  elem_struct *pEl[MAX_VX_FACE] = {NULL} ;


  /* Find the element that shares side kFc. It is not sufficient to check
     mDim shared vertices, for degenerate elements all face vx need to 
     match. */
  for ( k = 0 ; k < mVxFc ; k++ )
    nVxFc[k] = ppVx[ pFoE->kVxFace[k] ]->number ;
  mVxFc = pack_ulg_list ( nVxFc, mVxFc ) ;
    
  /* Find the other element that is formed with these three. Note that
     the vx2el lists are ordered lexicographically. Start with trying to
     match the first element listed with nVxFc[0]. */
  while ( !found ) {

    /* Pick the next element of the list of nVxFc[0] that is not pElem. */
    for ( matchEl = pElem->number ; matchEl == pElem->number ; ) {
      if ( !loop_toElem ( pUns->pllVxToElem, nVxFc[0], nElV+0, pEl+0 ) )
        /* List exhausted, no match possible. */
        return ( NULL ) ;
      matchEl = pEl[0]->number ;
    }


    /* Loop over any other forming vertices. */
    for ( found = k = 1 ; k < mVxFc ; k++ ) {

      while ( !pEl[k] || pEl[k]->number < matchEl )
        /* Pick the next element in the list for that vx to see whether it could match
           matchEl until the the element is too big. */
        if ( !loop_toElem ( pUns->pllVxToElem, nVxFc[k], nElV+k, pEl+k ) )
          /* List exhausted, no match possible. */
          return ( NULL ) ;

      if ( pEl[k]->number > matchEl ) {
        /* No match found. */
        found = 0 ;
        break ;
      }
    }
  }

  return ( pEl[0] ) ;
}

/******************************************************************************

  find_face_elem:
  Given a set of vertex pointers forming a face, find the face in an element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  mVx     the number of vertices on the face
  ppVxFc  a list of vertex pointers on the face
  pElem   the element whose faces are to be scanned.
  
  Returns:
  --------
  0 on failure, the matching face number on success.
  
*/

int find_face_elem ( int mVx, const vrtx_struct *ppVxFc[], const elem_struct *pElem ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  const vrtx_struct **ppVx, *pVx ;
  
  int kFace, kVx, matched[MAX_VX_FACE], iVx ;
  
  
  pElT = elemType + pElem->elType ;
  /* Loop over all faces/edges of this element. */
  for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
    pFoE = pElT->faceOfElem + kFace ;
    kVxFace = pFoE->kVxFace ;
    ppVx = ( const vrtx_struct ** ) pElem->PPvrtx ;

    /* Reset the list of flags whether each of nVxFace has been matched. */
    for ( kVx = 0 ; kVx < MAX_VX_FACE ; kVx++ )
      matched[kVx] = 0 ;

    /* Try to find each of the vertices of this face in nVxFace. */
    for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
      pVx = ppVx[ kVxFace[kVx] ] ;
      for ( iVx = 0 ; iVx < mVx ; iVx++ )
        if ( pVx == ppVxFc[iVx] ) {
          matched[iVx] = 1 ;
          break ;
        }
      if ( iVx == mVx )
        /* Failure, nVx is not in nVxFace. */
        break ;
    }
      
    if ( kVx == pFoE->mVertsFace ) {
      /* All of the vertices of kFace in pElem are in ppVxFc. Now check whether all of
         ppVxFc have been matched in pFoE. */
	
      for ( iVx = 0 ; iVx < mVx ; iVx++ )
        if ( !matched[iVx] )
          break ;
      
      if ( iVx == mVx ) {
        /* Match. */
        return ( kFace ) ;
      }
    }
  }

  /* No match found. */
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  face_all_marked_vx:
*/
/*! check whether all the vertices of a face are numbered, count, mark.
 *
 *
 */

/*
  
  Last update:
  ------------
  4Sep18; complete doc, treat non-tri elems.
  7Sep17: conceived.
  

  Input:
  ------
  pElem = element 
  pElT = its type
  kFace = the face to check
  doMark = if true, mark all these vertices.
  *pmVxmarked = number of marked vertices.

  Returns:
  --------
  1 if all numbered, 0 if mixed, -1 if all unmarked.
  
*/

int face_all_numbered_vx ( const elem_struct *pElem, const elemType_struct *pElT,
                           const int kFace, const int doMark, int *pmVxMarked ) {
  
  const faceOfElem_struct *pFoE = pElT->faceOfElem + kFace ;
  int allNumbered = 1 ;
  int allUnNumbered = 1 ;
  vrtx_struct *pVx ;
  int kVx ;
  for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
    pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ; 
    if ( pVx->number ) {
      allUnNumbered = 0 ;
      if ( doMark && !pVx->mark ) {
        pVx->mark = 1 ;
        (*pmVxMarked)++ ;
      }
    }
    else
      allNumbered = 0 ;
  }

  if ( allNumbered )     
    return ( 1 ) ;
  else if ( allUnNumbered )     
    return ( -1 ) ;
  else
    return ( 0 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  face_all_mark3_vx:
*/
/*! check whether all the vertices of a face are marked3
 */

/*
  
  Last update:
  ------------
  4Sep18: derived from face_all_numbered_vx.
  

  Input:
  ------
  pElem = element 
  pElT = its type
  kFace = the face to check
  doMark = if true, mark all these vertices.
  *pmVxmarked = number of marked vertices.

  Returns:
  --------
  1 if all marked, 0 if mixed, -1 if all unmarked.
  
*/

int face_all_mark3_vx ( const elem_struct *pElem, const elemType_struct *pElT,
                        const int kFace, const int doMark, int *pmVxMarked ) {
  
  const faceOfElem_struct *pFoE = pElT->faceOfElem + kFace ;
  int allMarked3 = 1 ;
  int allUnMarked3 = 1 ;
  vrtx_struct *pVx ;
  int kVx ;
  for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
    pVx = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ; 
    if ( pVx->mark3 ) {
      allUnMarked3 = 0 ;
      if ( doMark && !pVx->mark ) {
        pVx->mark = 1 ;
        (*pmVxMarked)++ ;
      }
    }
    else
      allMarked3 = 0 ;
  }

  if ( allMarked3 )     
    return ( 1 ) ;
  else if ( allUnMarked3 )     
    return ( -1 ) ;
  else
    return ( 0 ) ;
}






/******************************************************************************

  elem_contains_co: Check whether an element contains a coordinate inside.
  .
  
  Last update:
  ------------
  9Sep16; fix bug with tracking scMin/kSideMin when using checkAllFc
  30Mar16; return zero if among a partial check no outward pointing faces
    were found.
  10Jul15; add doc on kSide0=-1.
  6Apr13; tidy up dangling braces in if/else.
  3Sep12; introduce kSide0<0, to trigger complete checking of all faces.
  6May11 ; use unit normals for directional decisions.
  23Mar06; fix bug in returing outside status when kSide0 != 0.
  19Mar4; 
  12Feb4; changed input params to pCo, return kSide on failure.
  : conceived.
  
  Input:
  ------
  pElem
  pCo
  kSide0: start looping over the remaining faces after kSide. 
          if set to -1, don't stop after the first outward
          pointing face but check all faces.
  
  Returns:
  --------
  0 if the element contains pCo, 
    otherwise the checked face number (1..mFc) that points to the coor.
  
*/

int elem_contains_co ( const elem_struct *pElem, const double *pCo, int kSide0 ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;

  double faceGC[MAX_DIM], vx2face[MAX_DIM], fcNorm[MAX_DIM], sc ;
  int kSide, mVxFc, mTimesNormal ;
  int checkAllFc = ( kSide0 < 1 ? 1 : 0 ) ;
  double scMin = TOO_MUCH ;
  int kSideMin = 0 ;
  const vrtx_struct *pVxFc[MAX_VX_FACE] ;

  pElT = elemType + pElem->elType ;
  
  kSide0 = MAX( 0,kSide0 ) ;
  for ( kSide = kSide0+1 ; kSide <= pElT->mSides ; kSide++ ) {
    /* Vector from vertex to the face gravity center. */
    face_grav_ctr ( pElem, kSide, faceGC, &pFoE, &mVxFc, pVxFc ) ;
    vec_diff_dbl ( faceGC, pCo, pElT->mDim, vx2face ) ;
    vec_norm_dbl ( vx2face, pElT->mDim ) ;

    /* Outward face normal. */
    uns_face_normal ( pElem, kSide, fcNorm, &mTimesNormal ) ;
    vec_norm_dbl ( fcNorm, pElT->mDim ) ;
    sc = scal_prod_dbl ( vx2face, fcNorm, pElT->mDim ) ;
    
    if ( sc < -1.e-10 ) {
      /* Outside. */
      if ( checkAllFc ) { if (sc < scMin ) {
        scMin = sc ;
        kSideMin = kSide ;
      } }
      else      
        return ( kSide ) ;
    }
  }

  if ( checkAllFc )
    return ( kSideMin ) ;
  else {
    if ( kSide0 )
      /* Not all edges were tested, but no other outside face found. */
      /* This leads to an endless loop when embedded in while.
         return ( kSide0 ) ;*/
      return ( 0 ) ;
    else
      /* Match. All scalar products positive. */
      return ( 0 ) ;
  }
}


/******************************************************************************

  varType_avbp2adf:
  .
  
  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  3Apr06; might be broken after removing references to pNxtVar.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void varType_avbp2adf ( uns_s *pUns ) {

  varList_s *pVL = &pUns->varList ;
  var_s *pVar = pVL->var ;
  int mV, iV ; 

  /* N-S is always present. Default: cons.*/
  for ( iV = 0 ; iV < pUns->mDim+2 ; iV++ )
    pVar[iV].cat = ns ;

  strncpy ( pVar[0].name, "rho",   LEN_VARNAME-1 ) ;
  strncpy ( pVar[1].name, "rho*u", LEN_VARNAME-1 ) ;
  strncpy ( pVar[2].name, "rho*v", LEN_VARNAME-1 ) ;
  if ( pUns->mDim == 2 )
    strncpy ( pVar[3].name, "rho*E", LEN_VARNAME-1 ) ;
  else {
    strncpy ( pVar[3].name, "rho*w", LEN_VARNAME-1 ) ;
    strncpy ( pVar[4].name, "rho*E", LEN_VARNAME-1 ) ;
  }
  
  if ( ( mV = pUns->restart.avbp.neqt ) ) {
    pVL->mUnknFlow = mV ;

    for ( iV = 0 ; iV < mV ; iV ++ ) {
      pVar[iV].cat = rans ;
      pVar[iV].name[0] = '\0' ;
    }
  }
    
  if ( ( mV = pUns->restart.avbp.neqs ) ) {
    pVL->mUnknFlow = mV ;
    for ( iV = 0 ; iV < mV ; iV ++ ) {
      pVar[iV].cat = species ;
      pVar[iV].name[0] = '\0' ;
    }
  }
    
  if ( ( mV = pUns->restart.avbp.nreac ) ) {
    pVL->mUnknFlow = mV ;
    for ( iV = 0 ; iV < mV ; iV ++ ) {
      pVar[iV].cat = rrates ;
      pVar[iV].name[0] = '\0' ;
    }
  }
    
  if ( ( mV = pUns->restart.avbp.nadd ) ) {
    pVL->mUnknFlow = mV ;
    for ( iV = 0 ; iV < mV ; iV ++ ) {
      pVar[iV].cat = add ;
      pVar[iV].name[0] = '\0' ;
    }
  }

  return ;
}

  

/******************************************************************************

  de_cpvx:
  Turn a compound vertex pointer giving chunk and number in chunk into a
  vrtx_struct *.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

vrtx_struct *de_cptVx ( const uns_s *pUns, const cpt_s cpVx ) {

  if ( cpVx.nr )
    return ( pUns->ppChunk[ cpVx.nCh ]->Pvrtx + cpVx.nr ) ;
  else
    return ( NULL ) ;
}

/******************************************************************************

  min_cpt: Sort two compound pointers lexicographically.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

cpt_s min_cpt ( cpt_s cpt0, cpt_s cpt1 )
{
  if ( cpt0.nCh < cpt1.nCh )
    return ( cpt0 ) ;
  else if ( cpt0.nCh > cpt1.nCh )
    return ( cpt1 ) ;
  else if ( cpt0.nr < cpt1.nr )
    return ( cpt0 ) ;
  else
    return ( cpt1 ) ;
}

/******************************************************************************

  max_cpt: Sort two compound pointers lexicographically.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

cpt_s max_cpt ( cpt_s cpt0, cpt_s cpt1 ) {
  if ( cpt0.nCh < cpt1.nCh )
    return ( cpt1 ) ;
  else if ( cpt0.nCh > cpt1.nCh )
    return ( cpt0 ) ;
  else if ( cpt0.nr < cpt1.nr )
    return ( cpt1 ) ;
  else
    return ( cpt0 ) ;
}

/******************************************************************************

  de_cpvx: Returns the equivalent of sign( cpt0-cpt1 ), that is,
  -1 if cp0 < cpt1, 0 if cpt0 == cpt1, 1 otherwise.
  
  Last update:
  ------------
  31May22; intro cmp_elem_data
  28Apr20; intro  cmp_perVxPair_bc
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int cmp_cpt ( cpt_s cpt0, cpt_s cpt1 ) {
  
  if ( cpt0.nCh < cpt1.nCh )
    return ( -1 ) ;
  else if ( cpt0.nCh > cpt1.nCh )
    return ( 1 ) ;
  else if ( cpt0.nr < cpt1.nr )
    return ( -1 ) ;
  else if ( cpt0.nr > cpt1.nr )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

int cmp_perVxPair ( const void* pPerVx0, const void* pPerVx1 ) {
  return ( (int) ( (( perVxPair_s * ) pPerVx0)->In -
                   (( perVxPair_s * ) pPerVx1)->In ) ) ;
}

int cmp_perVxPair_bc ( const void* pPerVx0, const void* pPerVx1 ) {
  
  const perVxPair_s *pPV0 = pPerVx0 ;
  const bc_struct *pBc0 = pPV0->pPerBc->pBc[ pPV0->revDir] ;
  int iBc0 = pBc0->order ;
  

  const perVxPair_s *pPV1 = pPerVx1 ;
  const bc_struct *pBc1 = pPV1->pPerBc->pBc[ pPV1->revDir] ;
  int iBc1 = pBc1->order ;

  return ( iBc0-iBc1 ) ;
}

int cmp_vx ( const void* vpVx0, const void* vpVx1 ) {
  const vrtx_struct
    *pVx0 = *( ( vrtx_struct **) vpVx0 ),
    *pVx1 = *( ( vrtx_struct **) vpVx1 ) ;
  /*  printf ( "cmp_vx: %d - %d = %d\n",
      pVx0->number, pVx1->number, pVx0->number - pVx1->number ) ;*/
  return ( pVx0->number - pVx1->number ) ;
}


int cmp_pvx ( const void* vpVx0, const void* vpVx1 ) {
  const vrtx_struct
    *pVx0 = *( ( vrtx_struct **) vpVx0 ),
    *pVx1 = *( ( vrtx_struct **) vpVx1 ) ;
  /*  printf ( "cmp_vx: %d - %d = %d\n",
      pVx0->number, pVx1->number, pVx0->number - pVx1->number ) ;*/
  return ( pVx0 - pVx1 ) ;
}

int cmp_elem_data ( const void* vpED0, const void* vpED1 ) {
  const elem_data_s
    *pED0 = ( ( elem_data_s *) vpED0 ),
    *pED1 = ( ( elem_data_s *) vpED1 ) ;

  if ( !pED0->pElem && !pED1->pElem )
    return (0);
  else if ( !pED0->pElem )
    return (-1);
  else if ( !pED1->pElem )
    return (1);
  else
    // Both elements exist, compare their data.
    return ( pED0->dblData - pED1->dblData ) ;
}


/******************************************************************************
  add_elem_list:   
*/  

/*! Given a list of integers, e.g. node or elem numbers, sort ascending and remove duplicates.
 *
 * This is meant for short lists, e.g. 4 face node numbers.
 *
 */

/*
  
  Last update:
  ------------
  16Dec16: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void add_elem_list ( elem_data_s *pListElemData, const int mList,
                     elem_struct *pElem, const double elData ) {


  /* Compare to first element in the list, qsort sorts ascending. */
  if ( !pListElemData->pElem || pListElemData->dblData < elData ) {
    // replace
    pListElemData->pElem = pElem ;
    pListElemData->dblData = elData ;
  }

  // Order.
  qsort ( pListElemData, mList, sizeof ( *pListElemData ), cmp_elem_data ) ;

  return ;
}



/*********************************************************

Functions for solution variables.

*********************************************************/


/******************************************************************************

  :
  Take a continous array defined over nodes and scatter to the elem.
  Reweight by area.
  
  Last update:
  ------------
  7may24; derived from scatter_vx2ele_nr
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void scatter_vx2ele_area ( uns_s *pUns, int mEq, 
                                  double vxQ[], double vxVol[],
                                  double elQ[], double elVol[] ) {
#undef FUNLOC
#define FUNLOC "in  scatter_vx2ele_area"

  const elemType_struct *pElT ;
  const int mEl = pUns->mElemsNumbered ;

  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  int nVx, mVxEl, kVx, nEl, nE ;
  double  *peQ, *pvR, fac ;


  /* Qet elQ for elements with marked vertices only. */
  for ( nEl = 0 ; nEl <= mEl ; nEl++ )
    if ( !elVol || elVol[nEl] > 0. ) {
      peQ = elQ + mEq*nEl ;
      for ( nE = 0 ; nE < mEq ; nE++ )
        peQ[nE] = 0. ;
    }
  
  
  /* Loop over all elements. */
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( ( nEl = pElem->number ) ) {
        pElT = elemType + pElem->elType ;
        peQ = elQ + mEq*nEl ;

        mVxEl = pElT->mVerts ;
        for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
          nVx = pElem->PPvrtx[kVx]->number ;
          pvR = vxQ + mEq*nVx ;

          /* If no elVol is given, use centroid interpolation. */
          fac = ( elVol ? elVol[nEl]/vxVol[nVx] : 1./mVxEl ) ;
          for ( nE = 0 ; nE < mEq ; nE++ )
            peQ[nE] += fac*pvR[nE] ;
        }
      }
    
  return ;
}
/******************************************************************************

  :
  Take a continous array defined over elements and scatter to the nodes.
  Reweight by number of contributions, i.e. cells formed by a node.
  
  Last update:
  ------------
  7may24; rename _nr to distinguish from _area.
  : conceived.
  
  Input:
  ------

  Changes To:
  -----------
  pUns:    mesh, with possibly added element/vertex areas
  vxQ[]:   unknown[s] at the vertices
  vxVol[]: control volume area of the vertex
  elVol[]: control volume area of the element
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void scatter_ele2vx_area ( uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in  scatter_ele2vx_area"

  const elemType_struct *pElT ;
  const int mEq = pUns->varList.mUnknowns ;
  
  /* Loop over all elements. */
  double elemVol ;
  cpt_s cpVx ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd, *pEl ;
  vrtx_struct *pVx ;
  double *pVxVol ; // nodal volume, sum of the element contributions.
  int nBeg, nEnd, nVx, mVxEl, kVx, nEl, nE, mVxMrk ;
  double  *peQ, fac ;
  pChunk = NULL ;
  while ( loop_elems( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg; pElem <= pElEnd ; pElem++ )
      if ( ( nEl = pElem->number ) ) {

        pElT = elemType + pElem->elType ;
        // Elem sol is a temp field, single field with root indexed by ->nr
        peQ = pUns->pRootChunk->pElemVol + mEq*nEl ;

        /* Cell volume. */
        mVxEl = pElT->mVerts ;
        elemVol = 0. ;
        for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
          pVx = pElem->PPvrtx[kVx] ;
          cpVx = pVx->vxCpt ;
          pVxVol = pUns->ppChunk[cpVx.nCh]->pVrtxVol + cpVx.nr ;
          // Vx vol is a temp field, single field with root indexed by ->nr
          elemVol += *pVxVol ;
        }

        /* Scatter to vertices. */
        for ( kVx = 0 ; kVx < mVxEl ; kVx++ ) {
          pVx = pElem->PPvrtx[kVx] ;
          nVx = pElem->PPvrtx[kVx]->number ;
          fac = pUns->pRootChunk->pVrtxVol[nVx]/elemVol ;
          for ( nE = 0 ; nE < mEq ; nE++ )
            pVx->Punknown[nE] += fac*peQ[nE] ;
        }
      }
    
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  unslice_scalar_var:
*/
/*! Given a linear array of a scalar variable, unslice/send to all nodes in a chunk.
 *
 *
 */

/*
  
  Last update:
  ------------
  8may24; rename to unslice, to not confuse with gather/scatter routines
  10Jan19; add doc
  18Dec19: extracted from read_hdf5_sol.
  

  Input:
  ------
  pChunk: chunk for nodes and unknowns
  mVx; number of vertices
  kEq: position (from 0) of the variable in pUnknown.
  dBuf: linear array of the vector over nodes of the scalar

  Changes To:
  -----------
  pChunk->pVrtx[]->Punknown

  Output:
  -------
  pValMin: minimal scalar value encountered
  pnMIn:   node of the min value
  pValMin: maximal scalar value encountered
  pnMIn:   node of the max value
    
  
*/

void unslice_scalar_var ( chunk_struct *pChunk, const ulong_t mVx,
                          const int kEq, const double *dBuf,
                          double *pValMin, ulong_t *pnMin,
                          double *pValMax, ulong_t *pnMax ) {
#undef FUNLOC
#define FUNLOC "in unslice_scalar_var"

  *pValMin = TOO_MUCH ;
  *pValMax = -TOO_MUCH ;
  const double *pDbl ;
  vrtx_struct *pVx ;
  for ( pDbl = dBuf, pVx = pChunk->Pvrtx+1 ; 
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pDbl++ ) {
    pVx->Punknown[kEq] = *pDbl ;
    
    traceMinMax ( pDbl, pVx - pChunk->Pvrtx, pValMin, pnMin, pValMax, pnMax ) ;
  }

  return ;
}
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_var_name:
*/
/*! Find the index of a variable in a varlist, given the variable name.
 *
 *
 */

/*
  
  Last update:
  ------------
  7Dec20: conceived.
  

  Input:
  ------
  pVarList: the variable list
  varName: the name to match, can be an expression.
  pVar: the previously matched variable from pVarList.

  Returns:
  --------
  NULL on failure, pVar for the next match on success
  
*/

var_s *find_var_name ( varList_s *pVL, var_s *pVar, const char *pVarName ) {
#undef FUNLOC
#define FUNLOC "in find_var_name"
  
  if ( !pVar )
    pVar = pVL->var ;
  else
    pVar++ ;

  for ( ; pVar < pVL->var + pVL->mUnknowns ; pVar++ ) {
    if ( !fnmatch( pVarName, pVar->name, 0 ) )
      return ( pVar ) ;
  }

  return ( NULL ) ;
}




/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mult_uns_var_var:
*/
/*! Multiply two variables.
 *
 */

/*
  
  Last update:
  ------------
  13Jul19; rename to mult_uns, as this is specific of uns_s.
           return type set to void
  25Feb19: derived from mmg_egLen_from_var
  

  Input:
  ------
  pUns: grid
  kVar0, kVar1: variables to multiply
  kVar2: var to store the product (safe overwrite) of kVar0,1.

  Output:
   --------
  *pValMin: min val of updated variable kVar2
  *pValMax: max val of updated variable kVar2
 
*/

void mult_uns_var_var ( uns_s *pUns,
                        const int kVar0, const int kVar1, const int kVar2,
                         double *pValMin, double *pValMax ) {
  
  double valMin = TOO_MUCH ;
  double valMax = -TOO_MUCH ;
  
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double *pUnk, prod ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number ) {
        pUnk = pVrtx->Punknown ;
        prod = pUnk[kVar0]*pUnk[kVar1] ;
        pUnk[kVar2] = prod ;
        valMin = MIN( prod, valMin ) ;
        valMax = MAX( prod, valMin ) ; 
     }

  *pValMin = valMin ;
  *pValMax = valMax ;
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mult_uns_var_scal:
*/
/*! Multiply a variable with a scalar.
 *
 */

/*
  
  Last update:
  ------------
  13Jul19; rename to mult_uns, as this is specific of uns_s.
           return type changed to void.
  25Feb19: derived from mmg_egLen_from_var
  

  Input:
  ------
  pUns: grid
  kVar0, variables to multiply
  scal: scalar multiplier.
  kVar2: var to store the product (safe overwrite) of kVar0,1.
    
  Output:
  --------
  *pValMin: min val of updated variable kVar2
  *pValMax: max val of updated variable kVar2
  
*/

void mult_uns_var_scal ( uns_s *pUns,
                         const int kVar0, double const scal, const int kVar2,
                         double *pValMin, double *pValMax ) {

  double valMin = TOO_MUCH ;
  double valMax = -TOO_MUCH ;
  
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double *pUnk, prod ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number ) {
        pUnk = pVrtx->Punknown ;
        prod = pUnk[kVar0]*scal ;
        pUnk[kVar2] = prod ;
        valMin = MIN( prod, valMin ) ; 
        valMax = MAX( prod, valMin ) ; 
      }

  *pValMin = valMin ;
  *pValMax = valMax ;
  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_uns_var:
*/
/*! Set a variable to a const double.
 *
 */

/*
  
  Last update:
  ------------
  13Jul19; derived from mult_uns_var
  

  Input:
  ------
  pUns: grid
  kVar, variables to multiply
  val: value to set to
    
  
*/

void set_uns_var ( uns_s *pUns, const int kVar, double const val ) {
  
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double *pUnk, prod ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number ) {
        pUnk = pVrtx->Punknown ;
        pUnk[kVar] = val ;
      }

  return ;
}

/**************************************************

 Matching functions. 

*****************************************************/

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  count_elem_matches:
*/
/*! Count the number of possible zone, mark matches for an element.
 *  Count any zones, count the number of true bits in kMark2Match.
 *
 */

/*
  
  Last update:
  ------------
  7May20: conceived.
  

  Input:
  ------
  pMatch
    
  Returns:
  --------
  number of matches.
  
*/

int count_elem_matches ( const match_s *pMatch ) {
#undef FUNLOC
#define FUNLOC "in count_elem_matches"
  
  int mMatches = 0 ;

  if ( pMatch->matchZone )
    mMatches += pMatch->mZones ;

  
  if ( pMatch->matchMarks ) {
    int kM ;
    unsigned int kM2M = pMatch->kMark2Match ;
    for ( kM = 0 ; kM < SZ_ELEM_MARK ; kM++ ) {
      if ( kM2M & 1 )
        mMatches++ ;
      kM2M >>= 1 ;
    }
  }

  return ( mMatches ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  make_elem_match_k:
*/
/*! restrict a match with multiple zone/mark matches to the kth instance.
 *
 */

/*
  
  Last update:
  ------------
  7May20: conceived.
  

  Input:
  ------
  pMultMatch: a match structure with possible options
  kMath: position of the sought match
  doReset: reset pMatch to zero, ie only the added match.

  Output:
  -------
  pMatch: the k-th unique match.
    
  
*/

void make_elem_match_k ( const match_s *pAllMatch,
                         const int kMatch, const int doReset,
                         match_s *pMatch ) {
#undef FUNLOC
#define FUNLOC "in make_elem_match_k"

  if ( doReset )
    init_match ( pMatch ) ;
  

  int mMatches = 0 ;

  if ( pMatch->matchZone )
    mMatches += pMatch->mZones ;

  if ( kMatch < mMatches ) {
    // match kMatch is a zone.
    pMatch->matchZone = 1 ;
    pMatch->iZone[0] = pAllMatch->iZone[kMatch] ;
    return ;
  }

  /* Has to be a mark match, if we get here. */
  if ( pMatch->matchMarks ) {
    int kMark ;
    unsigned int kM2M = pAllMatch->kMark2Match ;
    /* left-shift a non-zero byte, switch on if position matches kMatch. */
    pMatch->kMark2Match &= 1 ;
    for ( kMark = 0 ; kMark < SZ_ELEM_MARK ; kMark++ ) {
      if ( kM2M & 1 ) {
        mMatches++ ;
        if ( kMatch+1 ==  mMatches ) {
          /* Switch on. */
          pMatch->matchMarks = 1 ;
          return ;
        }
      }
      kM2M >>= 1 ;
      pMatch->kMark2Match <<= 1 ;
    }
  }

  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  elem_matches:
*/
/*! Does an element match a particular condition?
 *
 */

/*
  
  Last update:
  ------------
  1Oct23; fix logic condition to match any one mark, not all.
  6May20: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int elem_matches ( const elem_struct *pEl, const match_s *pMatch ) {
#undef FUNLOC
#define FUNLOC "in elem_matches"

  if ( pEl->invalid )
    return ( 0 ) ;

  int someMatchTested = 0 ;

  if ( pMatch->hasNumber ) {
    someMatchTested = 1 ;
    if ( !pEl->number )
      return ( 0 ) ;
  }

  if ( pMatch->matchZone ) {
    someMatchTested = 1 ;
    int kZ = 0 ;
    for ( kZ = 0; kZ < pMatch->mZones ; kZ++ ) {
      if ( pEl->iZone == pMatch->iZone[kZ] )
        break ;
    }
    if ( kZ == pMatch->mZones )
      return ( 0 ) ;
  }

  if ( pMatch->matchMarks ) {
    someMatchTested = 1 ;
    // Assume here that kMark2Match/NotMatch are correctly masked to SZ_MARK_ELEM-1.
    // This requires to match all
    // if ( !( ( pEl->mark & pMatch->kMark2Match ) == pMatch->kMark2Match ) )
    // But we need to match any one.
    if ( !(pEl->mark & pMatch->kMark2Match ) )
      return ( 0 ) ;
    // if ( !( ~(pEl->mark & pMatch->kMark2NotMatch) == pMatch->kMark2NotMatch ) )
    if ( pEl->mark & pMatch->kMark2NotMatch )
      return ( 0 ) ;
  }
  
  if ( pMatch->matchElType ) {
    someMatchTested = 1 ;
    if ( pEl->elType < pMatch->elTypeBeg )
      return ( 0 ) ;
    if ( pEl->elType > pMatch->elTypeEnd )
      return ( 0 ) ;
  }

  //typedef enum { noNum, root, leaf, term, parent, leafNprt, elTypes,
  //             vxBc, vxBndFirst, invNum } numberedType_e ;
#ifdef ADAPT_HIERARCHIC
  if ( pMatch->matchAdaptType ) {
    someMatchTested = 1 ;
    if ( !pEl->root && pMatch->adaptType == root )
      return ( 0 ) ;
    if ( !pEl->leaf && pMatch->adaptType == leaf )
      return ( 0 ) ;
    if ( !pEl->term && pMatch->adaptType == term )
      return ( 0 ) ;
    if ( !pEl->PPchild && pMatch->adaptType == parent )
      return ( 0 ) ;
  }
#endif

  if ( !someMatchTested )
    hip_err ( fatal, 0, "zero test conditions specified "FUNLOC"." ) ;

  return ( 1 ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  elem_matches:
*/
/*! Does an element match a particular condition?
 *
 */

/*
  
  Last update:
  ------------
  6May20: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int vx_matches ( const vrtx_struct *pVx, const match_s *pMatch ) {
#undef FUNLOC
#define FUNLOC "in elem_matches"

  if ( pVx->invalid )
    return ( 0 ) ;

  int someMatchTested = 0 ;

  if ( pMatch->hasNumber ) {
    someMatchTested = 1 ;
    if ( !pVx->number )
      return ( 0 ) ;
  }

  /* No zones for vertices.*/

  if ( pMatch->matchMarks ) {
    someMatchTested = 1 ;
    /* For now, vertices have three separate marks. Pack them into a bitfield. */
    unsigned int mark = 0 ;
    mark &= pVx->mark ; mark <<= 1 ;
    mark &= pVx->mark2 ; mark <<= 1 ;
    mark &= pVx->mark3 ;
    
    // Assume here that kMark2Match/NotMatch are correctly masked to SZ_MARK_ELEM-1.
    if ( ( mark & pMatch->kMark2Match ) != pMatch->kMark2Match )
      return ( 0 ) ;
    if ( ~(mark & pMatch->kMark2NotMatch) != ~pMatch->kMark2NotMatch )
      return ( 0 ) ;
  }
  
  if ( pMatch->matchVxPer ) {
    someMatchTested = 1 ;
    if ( pVx->per == pMatch->isPer )
      return ( 0 ) ;
  }

  if ( !someMatchTested )
    hip_err ( fatal, 0, "zero test conditions specified "FUNLOC"." ) ;

  return ( 1 ) ;
}
