/*
   read_uns_fluent.c:
   Read an unstructured fluent format.

   Last update:
   ------------
   13Nov15; increase MAX_BC to 1024.
   8Jul15; increase MAX_BC to 512.
   27Aug14; track domain zones, augment zoneBc_s.
   18Feb13; switch format specifiers to %zu for ulong_t types.
   1Sep12; up MAX_BC to 200.
   2Apr11; read fluent bc tags and use as bcText.
   6Apr09; use fread based on counterexample.
   12Apr08; use FREAD, assuming binary files are big-endian.
   14Oct07; intro binary read, intro reading solution.
   10Aug01; add zone number to boundary labe.
   3Oct00; fill mElem2VertP in pChunk.
   June98, conception.

   This file contains:
   -------------------
   read_uns_fluent
   fl_read_hdr_x
   fl_decode_bc

*/
#include <string.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

#include "r1map.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;

/* A patch of faces with zone id, boundary id, and global face indices. */
typedef struct {
  int nZone ;
  int flType ; /* 2 is interior. All others bc, done by text. */
  int nBeg ;
  int nEnd ;
} flBc_s ;

typedef enum { fl_ascii, fl_sgl, fl_dbl } flTp_e ;

const elType_e flElType[] = { noEl, tri, tet, qua, hex, pyr, pri } ;

/* A temporary list of bc as given at the end of the file. */
typedef enum { fl_fc_undefined, fl_fc_domain, fl_fc_interior, fl_fc_bnd } flFaceType_e ;
typedef struct {
  int nZone ;
  flFaceType_e fcType ;
  char type[TEXT_LEN] ;
  char name[TEXT_LEN] ;
  bc_struct *pBc ;
} zoneBc_s ;

#define MAX_BC 1024
int mZoneBc ;
zoneBc_s zoneBc[MAX_BC] ;

/******************************************************************************
  fl_bc:   */

/*! match a fluent zone id to a hip bc.
 *
 *
 */

/*
  
  Last update:
  ------------
  2Apr11: conceived.
  

  Input:
  ------
  int nZone
    
  Returns:
  --------
  *pBc: pointer to the bc, NULL if there is no match.

  
*/

bc_struct *fl_bc ( const int nZone ) {
  int i ;

  for ( i = 0 ; i < mZoneBc ; i++ ) 
    if ( nZone == zoneBc[i].nZone )
      return ( zoneBc[i].pBc ) ;

  return ( NULL ) ;
}


/******************************************************************************

  fl_key:
  Modulate keys according to different precisions.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  key: the key to prepend
  flTp: the filetype selector.
  
  Returns:
  --------
  precKey: a pointer to the modified key string.
  
*/

static char* fl_key ( char key[], flTp_e flTp ) {

  const int flTpPrefix[3] = { 0, 2000, 3000 } ;
  int iKey = atoi( key ) ;
  static char precKey[5] ;

  sprintf ( precKey, "%d", flTpPrefix[flTp]+iKey ) ; 

  return ( precKey ) ;
}



/******************************************************************************

  fl_read_hdr_x:
  fl_read_hdr_d:
  fl_read_hdr:
  .
  
  Last update:
  ------------
  19Apr18; zone-ids are hexa, as given in large example.
  18Apr18; intro ulong_t.
  14Oct07; intro _d to deal with decimal headers.
  28Jun07; skip leading blanks before open parenthesis.
  : conceived.
  
  Input:
  ------
  tag:     the tag after ( to be found

  Changes to:
  -----------
  *pnZone:   if the zone was arbitrary, the one found is returned.
  *pnBeg,    *pnEnd: the index limits found,
  *ppflFile: the file pointer positioned after nEnd.
  *pnZone: the zone id. 
  
  Returns:
  --------
  the number of items in the zone.
  
*/
int fl_read_hdr_x ( char *tag, int nKey,
                    int *pnZone, ulong_t *pnBeg, ulong_t *pnEnd, FILE **ppflFile ) {
  //                  const char *fmt ) {
  
  int nFile = 0 ;
  *pnZone = -1 ;
  *pnBeg = *pnEnd = -1 ;

  if ( !r1map_pos_keyword ( tag, &nFile, &nKey, ppflFile ) ) {
    /* No such tag. */
    *pnZone = *pnBeg = *pnEnd = -1 ;
    return ( -1 ) ;
  }

  /* Skip up to the next open parenthesis. */
  fscanf ( *ppflFile, "%*[^(]" ) ; 
  /* Why does this read fail for very large meshes? Read as size_t rather than int.*/
  //if ( !fscanf ( *ppflFile, "(%x ", pnZone ) )
  size_t nZnLong ;
  char hexStr[LINE_LEN], *pStr ;
  if ( fscanf ( *ppflFile, "(%s ", hexStr ) ) {
    nZnLong = strtoul ( hexStr, &pStr, 16 ) ;
    *pnZone = (int) nZnLong ;
  }
  else
   /* No zone. */
    return (0) ;

#ifdef HIP_USE_ULONG
  /* Need to convert hexa to ulong_t */
  if ( fscanf ( *ppflFile, "%s ", hexStr ) )
    *pnBeg = strtoul ( hexStr, &pStr, 16 ) ;
  else
    return ( 0 ) ;

  if ( fscanf ( *ppflFile, "%s ", hexStr ) )
    *pnEnd = strtoul ( hexStr, &pStr, 16 ) ;
  else
    return ( 0 ) ;

#else
  /* short int. Use std x conversion. */
  if ( fscanf ( *ppflFile, "%x %x", pnBeg, pnEnd ) != 2 )
    return ( 0 ) ;
#endif
  /* Found. */
  return ( *pnEnd - *pnBeg + 1 ) ;
}


// Only hdr_x calls hdr, so rename hdr to hdr_x and bypass this wrapper. 
/* int fl_read_hdr_x ( char *tag, int nKey, */
/*                     int *pnZone, ulong_t *pnBeg, ulong_t *pnEnd, FILE **ppflFile ) { */
/*   return ( fl_read_hdr ( tag, nKey, pnZone, pnBeg, pnEnd, ppflFile, "(%x %x %x" ) ) ; */
/* } */



/* int fl_read_hdr_d ( char *tag, int nKey, */
/*                     int *pnZone, ulong_t *pnBeg, ulong_t *pnEnd, FILE **ppflFile ) { */
/*   return ( fl_read_hdr ( tag, nKey, pnZone, pnBeg, pnEnd, ppflFile, "(%d %d %d" ) ) ; */
/* } */

int fl_read_hdr5 ( char *tag, int nKey,
                   int *pnZone, int *pnV, int *pSz, ulong_t *pnBeg, ulong_t *pnEnd, 
                   FILE **ppflFile ) {
  
  int nFile = 0 ;
  *pnZone = -1 ;
  *pnBeg = *pnEnd = -1 ;

  if ( !r1map_pos_keyword ( tag, &nFile, &nKey, ppflFile ) ) {
    /* No such tag. */
    return ( -1 ) ;
  }

  /* Skip up to the next open parenthesis. */
  fscanf ( *ppflFile, "%*[^(]" ) ;
  if ( fscanf ( *ppflFile, "(%x %x %x", pnZone, pnV, pSz ) != 3 )
    /* Failed read. */
    return (0) ;
  
#ifdef HIP_USE_ULONG
  /* Need to convert hexa to ulong_t */
  char hexStr[LINE_LEN], *pStr ;
  if ( fscanf ( *ppflFile, "%s ", hexStr ) )
    *pnBeg = strtoul ( hexStr, &pStr, 16 ) ;
  else
    return ( 0 ) ;

  if ( fscanf ( *ppflFile, "%s ", hexStr ) )
    *pnEnd = strtoul ( hexStr, &pStr, 16 ) ;
  else
    return ( 0 ) ;

#else
  /* short int. Use std x conversion. */
  if ( fscanf ( *ppflFile, "%x %x", pnBeg, pnEnd ) != 2 )
    return ( 0 ) ;
#endif
  /* Found. */
  return ( *pnEnd - *pnBeg + 1 ) ;
}



/******************************************************************************
  fl_bcType:   */

/*! Convert fluent bc type tags to hip types.
 *
 * 
 *
 */

/*
  
  Last update:
  ------------
  9Jul13; fix bug with wrong subscript for label.
  2Apr11: conceived.
  

  Input:
  ------
  flBcType: string with Fluents bc tag
    
  Returns:
  --------
  bcType: hip's corresponding type
  
*/

char fl_bcType ( const char flBcType[] ) {
  /* 
     static const char * fl_decode_bc ( int nBc ) {

     static char *label[] = { "interior", "wall", "pressure-inlet", "pressure-outlet",
     "symmetry", "periodic-shadow", "pressure-far-field",
     "velocity-inlet", "periodic", "fan", "mass-flow-inlet",
     "interface", "parent", "outflow", "axis" } ;
     static char noLabel[80] ; 
     int nrBc[] = { 2,3,4,5,7,8,9,10,12,14,20,24,31,36,37 }, mBc = 15, iBc ;
  */

  const char label[9][2][MAX_BC_CHAR] = 
    { {"wall", "w",              },
      {"pressure-outlet", "o",   },
      {"symmetry", "s",          },
      {"periodic-shadow", "u",   },
      {"pressure-far-field", "f",},
      {"velocity-inlet", "f",    },
      {"periodic", "l",          },
      {"mass-flow-inlet", "f",   },
      {"outflow", "o"            }} ;

  int i ;


  for ( i = 0 ; i < 9 ; i++ )
    if ( !strcmp( label[i][1], flBcType ) )
      /* Match. */
      return ( label[i][1][0] ) ;

  /* No match found. */
  return ( 'n' ) ;
}

/******************************************************************************

  fl_read_int:
  wrapper to transparently read ascii, binary sgl and binary dbl Fluent files.
  Fluent's manual is not clear on what binary integers are written on "20" or "30",
  assume it is 4byte words.
  
  Last update:
  ------------
  18Apr18: use ulong_t.
  6Apr09; use fread based on counterexample, intro bSwap.
  12Apr08; use FREAD, although the Fluent manual is stumm about endian-ness.
  8Dec07; assume that fl_dbl still writes 4byte ints.
  13Oct07: conceived.
  
  Input:
  ------
  flFile: positioned file
  flTp: file type
  bSwap: if true, perform a binary swap little/big-Endian.
  mInt: number of ints to read
  pInt: pointer to data

  Changes To:
  -----------
  *pInt
  
  Returns:
  --------
  the number of ints read.
  
*/

static ulong_t fl_read_int ( FILE *flFile, flTp_e flTp, const int bSwap, ulong_t mInt, 
                             int *pInt ) {

  ulong_t i ;
  if ( flTp == fl_ascii ) {
    for ( i=0; i<mInt ; i++ ) 
      if ( !fscanf ( flFile, "%x", pInt+i ) )
        return ( i ) ;
  }
  else if ( flTp == fl_sgl || flTp == fl_dbl ) {
    if ( bSwap ) 
      return ( FREAD ( pInt, sizeof( int ), mInt, flFile ) ) ;
    else 
      return ( fread ( pInt, sizeof( int ), mInt, flFile ) ) ;
  }
  else {
    sprintf ( hip_msg, "unknown file type %d in fl_read_int.", flTp ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( mInt ) ;
}

/******************************************************************************

  fl_read_ulg:
  wrapper to transparently read ascii, binary sgl and binary dbl Fluent files.
  Fluent's manual is not clear on what binary integers are written on "20" or "30",
  assume it is 4byte words.
  
  Last update:
  ------------
  18Apr18: derived from fl_read_ulg.
  
  Input:
  ------
  flFile: positioned file
  flTp: file type
  bSwap: if true, perform a binary swap little/big-Endian.
  mUlg: number of ULGs to read
  pUlg: pointer to data

  Changes To:
  -----------
  *pUlg
  
  Returns:
  --------
  the number of ints read.
  
*/

static ulong_t fl_read_ulg ( FILE *flFile, flTp_e flTp, const int bSwap, ulong_t mUlg, 
                             ulong_t *pUlg ) {

  ulong_t i ;
  char hexStr[LINE_LEN], *pStr ;
  int someInt ;
  if ( flTp == fl_ascii ) {
    for ( i=0; i<mUlg ; i++ ) {

#ifdef HIP_USE_ULONG
      if ( !fscanf ( flFile, "%s", hexStr ) )
        return ( i ) ;
      // Explicit conversion from short hexa to long unsigned int.
      pUlg[i] = strtoul ( hexStr, &pStr, 16 ) ;
#else
      if ( !fscanf ( flFile, "%x", pUlg+i ) )
        return ( i ) ;
#endif
      
    }
  }
  else if ( flTp == fl_sgl || flTp == fl_dbl ) {
    for ( i=0; i<mUlg ; i++ ) {

      if ( bSwap )
        // Note: we assume here that the int written by Fluent is a short int.
        FREAD ( &someInt, sizeof( int ), 1, flFile ) ;
      else 
        fread ( &someInt, sizeof( int ), 1, flFile ) ;

      pUlg[i] = ( ulong_t ) someInt ;
    }
  }
  else {
    sprintf ( hip_msg, "unknown file type %d in fl_read_ulg.", flTp ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( mUlg ) ;
}



/******************************************************************************

  fl_read_real:
  wrapper to transparently read ascii, binary sgl and binary dbl Fluent files.
  Single precision real numbers are promoted to dbl.
  
  Last update:
  ------------
  6Apr09; use fread, based on counterexample, into bSwap.
  12Apr08; use FREAD, although the Fluent manual is stumm about endian-ness.
  8Dec07; rename to fl_read_real, to avoid confusion between sgl and dbl.
  13Oct07: conceived.
  
  Input:
  ------
  flFile: positioned file
  flTp: file type
  bSwap: if true, perform a binary swap little/big-Endian.
  mReal: number of ints to read
  pDbl: pointer to data

  Changes To:
  -----------
  *prData
  
  Returns:
  --------
  the number of reals read.
  
*/

static ulong_t fl_read_real ( FILE *flFile, flTp_e flTp, const int bSwap, ulong_t mDbl, 
                              double *pDbl ) {
  ulong_t i, m ;
  float rBuf[MAX_DIM] ;

  if ( flTp == fl_ascii ) {
    for ( i = 0 ; i < mDbl ; i++ ) 
      if ( !fscanf ( flFile, "%lf", pDbl+i ) )
        return ( i ) ;
  }

  else if ( flTp == fl_sgl ) {
    if ( bSwap ) 
      m = FREAD ( rBuf, sizeof( float ), mDbl, flFile ) ;
    else
      m = fread ( rBuf, sizeof( float ), mDbl, flFile ) ;

    for ( i = 0 ; i < m ; i++ ) 
      pDbl[i] = (double) rBuf[i] ;
    return ( m ) ;
  }

  else if ( flTp == fl_dbl ) {
    if ( bSwap ) 
      return ( FREAD ( pDbl, sizeof( double ), mDbl, flFile ) ) ;
    else
      return ( fread ( pDbl, sizeof( double ), mDbl, flFile ) ) ;
  }
  else {
    sprintf ( hip_msg, "unknown file type %d in fl_read_real.", flTp ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( mDbl ) ;
}


/******************************************************************************

  flr_err:
  Catch fluent read errors.
  
  Last update:
  ------------
  18Apr18; use ulong_t.
  6Apr09; removed: exit( EXIT_FAILURE ), now done in hip_err.
  13Oct07: conceived.
  
*/

static void flr_face_failure ( ulong_t nFc ) {
  sprintf ( hip_msg, "could not read face %"FMT_ULG" from fluent file.", nFc ) ;
  hip_err ( fatal, 0, hip_msg ) ;
}

static void flr_elem_failure ( ulong_t nFc, int nZone, int bcNr ) {
  sprintf ( hip_msg, 
            "could not read elem %"FMT_ULG", zone %d, bc %d in fl_elem_failure.",
            nFc, nZone, bcNr ) ;
  hip_err ( fatal, 0, hip_msg ) ;
}



/******************************************************************************

  fl_make_bndFc:
  Given fluent's faces, construct a list of boundary faces.
  
  Last update:
  ------------
  11May18; gracefully treat faces that reference non-existent zone numbers.
  20Apr18; ignore faces with no element pointers either side.
  19Apr18; identify and discard internal faces.
  14Oct07: extracted from read_fluent.
  
  Input:
  ------        
  pUns: the grid.
  pChunk: the chunk that stores this part of the grid.
  pFace: list of faces
  mFlBnd: number of boundaries to read
  pFlBnd: list of boundaries

  Changes To:
  -----------
  pUns
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fl_make_bndFc ( uns_s *pUns, chunk_struct *pChunk, 
                    flFc_s *pFace, int mFlBnd, flBc_s *pFlBnd ) {

  bndFc_struct *pBndFc = pChunk->PbndFc, *pBF = pBndFc ;
  bndPatch_struct *pBndPatch = pChunk->PbndPatch, *pBP = pBndPatch ;
  char bcLabel[TEXT_LEN] ;
  bc_struct *pBc ;
  elem_struct *pElem = pChunk->Pelem ;
  flBc_s *pFB = pFlBnd ;
  flFc_s *pFc ;
  elem_struct *pEl=NULL ;
  int kFc ;
  ulong_t mFcInternal = 0, mFcNoEl = 0 ;

  for ( pFB = pFlBnd ; pFB < pFlBnd + mFlBnd ; pFB++ )
    if ( pFB->flType != 2 ) {
      /* This is a boundary patch. */
      pBP++ ;
      pBP->PbndFc = pBF+1 ;
      pBP->mBndFc = pFB->nEnd - pFB->nBeg + 1 ;
      pBP->Pchunk = pChunk ;
      pBc = fl_bc( pFB->nZone ) ;
      if ( !pBc ) {
        /* This zone label is not present in the grid. Make one, or ref it if it already exists. */
        sprintf ( bcLabel, "hip_bc_%d", pFB->nZone ) ;
        pBc = find_bc ( bcLabel, 2 ) ;
        if ( !pBc ) {
          /* Doesn't exist yet. */
          sprintf ( hip_msg, "zone %d referenced, but not defined. Added bc `hip_bc_%d'", pFB->nZone, pFB->nZone ) ;
          hip_err ( warning, 1, hip_msg ) ;
          pBc = find_bc ( bcLabel, 1 ) ;
        }
      }
      pBP->Pbc = pBc ;

      for ( pFc = pFace+pFB->nBeg ; pFc <= pFace+pFB->nEnd ; pFc++ ) {
        if ( pFc->nEl[0] && !pFc->nEl[1] )
          /* The first slot is a valid element. */
          pEl = pElem + pFc->nEl[0] ;
        else if ( pFc->nEl[1] && !pFc->nEl[0] )
          /* The second slot is a valid element. */
          pEl = pElem + pFc->nEl[1] ;
        else if (pFc->nEl[0] && pFc->nEl[1] ) {
          /* Internal boundary face. Retain, but only one instance pointing to
             nEl[0]. */
          pEl = pElem + pFc->nEl[0] ;
          mFcInternal++ ;
          if ( verbosity > 4 ) {
            sprintf ( hip_msg,
                      "internal boundary between elems %"FMT_ULG",%"FMT_ULG", retaining "
                      "only ref to first one.",pFc->nEl[0], pFc->nEl[1] ) ;
            hip_err ( info, 5, hip_msg ) ;
          }
        }
        else{
          /* Both element pointers are zero, no ref to any elem. Ignore. */
          mFcNoEl++ ;
          pEl = NULL ;
        }
          

        if ( pEl ) {
          pBF++ ;
          if ( ( kFc = face_in_elem ( pEl, pFc->mVxFc, pFc->nVx ) ) ) {
            pBF->Pelem = pEl ;
            pBF->nFace = kFc ;
            pBF->Pbc = pBc ;
          }
          else {
            sprintf ( hip_msg, 
                      "face %zu not found in element %"FMT_ULG" in fl_make_bndFc.",
                      pFc - pFace, pFc->nEl[0] ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }
        }
      }
    }

  if ( mFcInternal ) {
    sprintf ( hip_msg, "found %"FMT_ULG" internal boundary faces,"
              "listed with ref to first element.", mFcInternal ) ;
    hip_err ( info, 2, hip_msg ) ;
  }

  if ( mFcNoEl ) {
    sprintf ( hip_msg, "found %"FMT_ULG" faces without element reference,"
              " ignored.", mFcNoEl ) ;
    hip_err ( info, 2, hip_msg ) ;
  }
  
  return ( 1 ) ;
}




/******************************************************************************

  fl_read_sizes:
  Read the headers for allocation.
  
  Last update:
  ------------
  18Apr18; use ulong_t.
  19Dec17; new interface to make_uns.
  14Oct07: extracted from read_fluent.
  
  Changes To:
  -----------
  pUns: allocated grid.
  *ppChunk: the allocated chunk.
  *pmFlFc: the number of fluent faces.
  *pmFlBnd: the number of boundaries (= number of zones -1)
  
  Returns:
  --------
  NULL on failure, pUns on success.
  
*/

static uns_s *fl_read_sizes ( chunk_struct **ppChunk, ulong_t *pmFlFc, int *pmFlBnd ) {

  int nKey, nFile ;
  FILE *flFile ;
  int mDim ;
  ulong_t mVerts, mElems, mFlFc ;
  int mBnd ;
  ulong_t mBndFc, mFlBnd ;
  int nZone ;
  ulong_t nBeg, nEnd ;
  flTp_e flTp ;
  char *pKey ;
  int bcNr, mVxFace ;
  ulong_t mZoneFc ;
  uns_s *pUns ;
  chunk_struct *pChunk ;

  /* Dimensions. */
  nKey = 1 ; nFile = 0 ;
  if ( !r1map_pos_keyword ( "2", &nFile, &nKey, &flFile ) )
    hip_err ( fatal, 0, "no dimension statement (2 in fl_read_sizes." ) ;
  fscanf ( flFile, "%d", &mDim ) ;

  /* Number of Nodes. We need the entry with the 0 zone index. */
  mVerts = mElems = mFlFc = -1 ;
  mBnd = mBndFc = mFlBnd = 0 ;
  for ( nKey = 1 ;
        ( mVerts = fl_read_hdr_x ( "10", nKey, &nZone, &nBeg, &nEnd, &flFile)) != -1 ;
        nKey++ )
    if ( !nZone ) break ;

  /* Number of elements. We need the entry with the 0 zone index. */
  for ( nKey = 1 ;
        ( mElems = fl_read_hdr_x ( "12", nKey, &nZone, &nBeg, &nEnd, &flFile)) != -1 ;
        nKey++ )
    if ( !nZone ) break ;
  
  /* Number of faces and number of boundary patches. */
  for ( flTp = fl_ascii ; flTp <= fl_dbl ; flTp++ ) {
    pKey = fl_key ( "13", flTp ) ;

    for ( nKey = 1 ;
          ( mZoneFc = fl_read_hdr_x ( pKey, nKey, &nZone, &nBeg,&nEnd,&flFile))!=-1 ;
          nKey++ ) {
      if ( nZone ) {
        if ( fscanf ( flFile, "%x %x", &bcNr, &mVxFace ) != 2 ) {
          sprintf ( hip_msg, 
                    "failed to read bc nr in zone %d in fl_read_sizes.", nZone ) ;
          hip_err ( fatal, 0, hip_msg ) ; 
        }
        if ( bcNr != 2 ) {
          mBnd++ ;
          mBndFc += mZoneFc ;
        }
        mFlBnd++ ;
      }
      else
        /* Zone 0. Global counter. */
        mFlFc = mZoneFc ;
    }
  }

  if ( mVerts == -1 || mElems == -1 || mFlFc == -1 ) {
    sprintf ( hip_msg, "failed to read number of verts/elems/faces"
              " in fl_read_sizes." ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  else if ( verbosity > 2 ) {
    sprintf ( hip_msg, "found %"FMT_ULG" vertices, %"FMT_ULG" elements, %"FMT_ULG" faces.",
              mVerts, mElems, mFlFc ) ;
    hip_err ( info, 2, hip_msg ) ;
  }

  pUns = make_uns ( NULL ) ;
  pUns->mDim = mDim ;
  /* Don't allocate for PPvrtx, yet. */
  pChunk = append_chunk ( pUns, pUns->mDim, mElems, 0, 0, mVerts, mBndFc, mBnd ) ;
  if ( !pUns || !pChunk ) {
    sprintf ( hip_msg, " FATAL: failed to do major alloc in fl_read_sizes." ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }


  *ppChunk = pChunk ;
  *pmFlFc = mFlFc ;
  *pmFlBnd = mFlBnd ;
  return ( pUns ) ;
}

/******************************************************************************

  fl_read_coor:
  Read grid coordinates from fluent case or mesh file.
  
  Last update:
  ------------
  18Apr18; use ulong_t.
  6Apr09; pass bSwap.
  14Oct07; extracted from read_fluent.
  
  Input:
  ------
  pCunk: the allocated part of the grid.
  mDim: the spatial dimension.

  Changes To:
  -----------
  *pUns: filled coordinates.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int fl_read_coor ( chunk_struct *pChunk, const int mDim, const int bSwap ) {

  flTp_e flTp ;
  char *pKey ;
  int nKey, nZone ;
  ulong_t nBeg, nEnd ;
  FILE *flFile ;
  const ulong_t mVerts = pChunk->mVerts ;
  ulong_t nVx ;
  double *pCo, *pCoor = pChunk->Pcoor ;
  vrtx_struct *pVx, *pVrtx = pChunk->Pvrtx ;


  for ( flTp = fl_ascii ; flTp <= fl_dbl ; flTp++ ) {
    pKey = fl_key ( "10", flTp ) ; 

    for ( nKey = 1 ;
          fl_read_hdr_x ( pKey, nKey, &nZone, &nBeg, &nEnd, &flFile) != -1 ;
          nKey++ )

      if ( nZone ) {
        fscanf ( flFile, "%*[^(]" ) ;  fscanf ( flFile, "(" ) ;
        if ( nEnd > mVerts ) {
          sprintf ( hip_msg, 
                    "found %"FMT_ULG" nodes with only %"FMT_ULG" declared"
                    " in fl_read_coor.", nEnd, mVerts ) ;
          hip_err ( fatal, 0, hip_msg ) ; }

        for ( nVx = nBeg ; nVx <= nEnd ; nVx++ ) {
          pCo = pCoor + nVx*mDim ;
          pVx = pVrtx + nVx ;
          pVx->number = nVx ;
          pVx->Pcoor = pCo ;

          if ( fl_read_real ( flFile, flTp, bSwap, mDim, pCo ) != mDim ) {
            sprintf ( hip_msg, "failed to read coor (%"FMT_ULG")"
                      " in fl_read_coor.", nVx ) ;
            hip_err ( fatal, 0, hip_msg ) ; 
          }
        }
      }
  }

  return ( 1 ) ;
}

/******************************************************************************

  fl_read_elemType:
  Read the element types and allocate for elem->vertex pointers.
  
  Last update:
  ------------
  18Apr18; use ulong_t.
  6Apr09; pass bSwap.
  7Dec07; fix bug with reseting mEl2Vx, resulting in zero allocation.
  14Oct07: extracted from read_fluent.
  
  Input:
  ------
  pUns: the grid.
  pChunk: this part of the grid.
  
  Changes To:
  -----------
  pUns:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int fl_read_elemType ( uns_s *pUns, chunk_struct *pChunk, const int bSwap ) {

  ulong_t nEl ;
  ulong_t mElemsType[MAX_ELEM_TYPES] ;
  flTp_e flTp ;
  char *pKey ;
  ulong_t mEl2Vx = 0 ;
  int nKey, nZone ;
  ulong_t nBeg, nEnd ;
  FILE *flFile ;
  const ulong_t mElems = pChunk->mElems ;
  int bcNr ;
  int nrElType ;
  const int mDim  = pUns->mDim ;
  elem_struct *pElem = pChunk->Pelem ;
  vrtx_struct **ppVrtx ;
  elem_struct *pEl ;
  vrtx_struct **ppVx ;

  for ( nEl = 0 ; nEl < MAX_ELEM_TYPES ; nEl++ )
    mElemsType[nEl] = 0 ;
  
  /* The fluent manual 6.1 gives examples for sgl and dbl, but does not
     specify how it deals with binary integers. Assume std 4word ints upon "20". */
  for ( flTp = fl_ascii ; flTp <= fl_dbl ; flTp++ ) {
    pKey = fl_key ( "12", flTp ) ; 

    for ( nKey = 1 ; 
          fl_read_hdr_x ( pKey, nKey, &nZone, &nBeg, &nEnd, &flFile) != -1 ;
          nKey++ )
      if ( nZone ) {
        if ( nEnd > mElems ) {
          sprintf ( hip_msg, 
                    "found %"FMT_ULG" elems with only %"FMT_ULG" declared"
                    " in fl_read_elemType.", nEnd, mElems) ;
          hip_err ( fatal, 0, hip_msg ) ; 
        }


        /* Bc number, elem type. */
        if ( !fscanf ( flFile, "%x", &bcNr ) ) {
          sprintf ( hip_msg,
                    "failed to read elem bc for zone %d in fl_read_elemType.",
                    nZone ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        } 

        if ( !fscanf ( flFile, "%x", &nrElType ) ) {
          /* No element type given. Use a default????. */
          if ( verbosity > 2 )
            sprintf ( hip_msg, 
                      "failed to read elem type for zone %d, using %s.\n",
                      nZone, ( mDim == 2? "tri" : "tet" ) ) ;
          hip_err ( fatal, 0, hip_msg ) ;
          nrElType = ( mDim == 2? 1 : 2 ) ;
        }
        fscanf ( flFile, "%*[^(]" ) ;  fscanf ( flFile, "(" ) ;
    


        if ( nrElType == 0 )
          /* Mixed. */
          for ( nEl = nBeg ; nEl <= nEnd ; nEl++ ) {
            if ( !fl_read_int ( flFile, flTp, bSwap, 1, &nrElType ) ) {
              sprintf ( hip_msg, "failed to read elem type list for zone %d"
                        "in fl_read_elemType.", nZone ) ;
              hip_err ( fatal, 0, hip_msg ) ;
            }

            pElem[nEl].number = 0 ;
            pElem[nEl].elType = flElType[ nrElType ] ;
            mEl2Vx += elemType[ flElType[ nrElType ] ].mVerts ;
            mElemsType[ flElType[ nrElType ] ]++ ;
          }
        else
          /* One type. */
          for ( nEl = nBeg ; nEl <= nEnd ; nEl++ ) {
            pElem[nEl].number = 0 ;
            pElem[nEl].elType = flElType[ nrElType ] ;
            mEl2Vx += elemType[ flElType[ nrElType ] ].mVerts ;
            mElemsType[ flElType[ nrElType ] ]++ ;
          }
      }
  }



  sprintf ( hip_msg, "found %"FMT_ULG" tris, %"FMT_ULG" quads,"
            " %"FMT_ULG" tets, %"FMT_ULG" pyrs, %"FMT_ULG" prisms, %"FMT_ULG" hexes,\n"
            "           allocating %"FMT_ULG" element to vertex pointers.",
            mElemsType[0], mElemsType[1], 
            mElemsType[2], mElemsType[3], mElemsType[4], mElemsType[5],
            mEl2Vx ) ;
  hip_err ( info, 3, hip_msg ) ;
  


  /* Malloc for element to vertex pointers. */
  pChunk->PPvrtx = ppVrtx = arr_calloc ( "pChunk->PPvrtx in fl_read_elemType.", pUns->pFam,
                                         mEl2Vx, sizeof( vrtx_struct * ) ) ;
  pChunk->mElem2VertP = mEl2Vx ;



  /* Set the vertex pointers for each element. */
  for ( pEl = pElem+1, ppVx = ppVrtx ; pEl <= pElem + mElems ; pEl++ ) {
    pEl->PPvrtx = ppVx ;
    ppVx += elemType[ pEl->elType ].mVerts ;
  }

  return ( 1 ) ;
}

/******************************************************************************
  fl_face_type:   */

/*! return the type this zone number: domain, bnd, interior, undefined? 
 */

/*
  
  Last update:
  ------------
  27Aug14: conceived.
  

  Input:
  ------
  nZone: zone to test
    
  Returns:
  --------
  1 if zone is domain, -1 if boundary, 0 if nonexistent
  
*/

flFaceType_e fl_face_type ( const int nZone ) {

  int k ;
  for ( k = 0 ; k < mZoneBc ; k++ )
    if ( zoneBc[k].nZone == nZone ) {
      return ( zoneBc[k].fcType ) ;
    }

  return ( fl_fc_undefined ) ;
}
/******************************************************************************
  fl_face_type:   */

/*! return the name of this zone number? 
 */

/*
  
  Last update:
  ------------
  28Aug14: derived from fl_face_type.
  

  Input:
  ------
  nZone: zone to test
    
  Returns:
  --------
  1 if zone is domain, -1 if boundary, 0 if nonexistent
  
*/

const char* fl_face_name ( const int nZone ) {

  int k ;
  for ( k = 0 ; k < mZoneBc ; k++ )
    if ( zoneBc[k].nZone == nZone ) {
      return ( zoneBc[k].name ) ;
    }

  return ( NULL ) ;
}


/******************************************************************************

  fl_read_faces:
  Read fluent-style faces.
  
  Last update:
  ------------
  18Aug18; use ulong_t.
  28Aug14; use zoneBc to ignore internal faces.
  6Apr09; pass bSwap.
  7Dec07; also check for 3000 codes for fl_dbl.
  14Oct07: extracted from read_fluent.
  
  Input:
  ------
  mFlFc: number of faces to read
  pFace: list of faces
  mFlBnd: number of boundaries to read
  pFlBnd: list of boundaries
  bSwap: perform endian swap if true.

  Changes To:
  -----------
  pFace:
  pFlBnd:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int fl_read_faces ( ulong_t mFlFc, flFc_s *pFace, int mFlBnd, flBc_s *pFlBnd,
                           const int bSwap ) {
  
  flTp_e flTp ;
  char *pKey ;
  int nKey, nZone ;
  ulong_t nBeg, nEnd ;
  ulong_t mZoneFc ;
  FILE *flFile ;
  flBc_s *pFB = pFlBnd-1 ;
  int flType ;
  ulong_t nFc, mVxFace ;
  flFc_s *pFc ;


  /* The fluent manual 6.1 gives examples for sgl and dbl, but does not
     specify how it deals with binary integers. Assume std 4word ints 
     upon "20". */
  for ( flTp = fl_ascii ; flTp <= fl_dbl ; flTp++ ) {
    pKey = fl_key ( "13", flTp ) ; 

    for ( nKey = 1 ;
          ( mZoneFc = 
            fl_read_hdr_x ( pKey, nKey, &nZone, &nBeg, &nEnd, &flFile)) !=-1 ;
          nKey++ )
      if ( nZone ) {
        if ( 0 && fl_face_type( nZone ) == fl_fc_interior ) {
          sprintf ( hip_msg, "ignoring internal interface %s with %"FMT_ULG" faces.",
                    fl_face_name ( nZone ), nEnd-nBeg+1 ) ;
          hip_err ( info, 1, hip_msg ) ;
        }
        else {
          if ( nEnd > mFlFc ) {
            sprintf ( hip_msg,
                      "found %"FMT_ULG" faces, only %"FMT_ULG" declared in fl_read_faces.",
                      nEnd, mFlFc ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }

          /* Bc number, face type. */
          if ( fscanf ( flFile, "%x %"FMT_ULG"", &flType, &mVxFace ) != 2 ) {
            sprintf ( hip_msg,
                      "failed to read type for zone %d in read_uns_fluent.\n",
                      nZone ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }
          fscanf ( flFile, "%*[^(]" ) ; fscanf ( flFile, "(" ) ;

          /* Patch index. */
          pFB++ ;
          pFB->flType = flType ;
          pFB->nZone = nZone ;
          pFB->nBeg = nBeg ;
          pFB->nEnd = nEnd ;

          if ( mVxFace == 0 )
            /* Mixed type. */
            for ( nFc = nBeg ; nFc <= nEnd ; nFc++ ) {
              pFc = pFace + nFc ;
              pFc->pNxtFc[0] = pFc->pNxtFc[1] = NULL ;
              if ( !fl_read_int ( flFile, flTp, bSwap, 1, &pFc->mVxFc ) )
                flr_face_failure ( nFc ) ;
              if ( fl_read_ulg ( flFile, flTp, bSwap, pFc->mVxFc, pFc->nVx ) 
                   != pFc->mVxFc )
                flr_face_failure ( nFc ) ;
              if ( fl_read_ulg ( flFile, flTp, bSwap, 2, pFc->nEl ) != 2 )
                flr_face_failure ( nFc ) ;

              /* if ( pFc->nEl[0] == 7 || pFc->nEl[1] == 7 ) 
                 printf ( " reading faces, found face %d ref'ing ele %d, %d\n", 
                 nFc, pFc->nEl[0], pFc->nEl[1] ) ; */

              /* fscanf ( flFile, "%*[^\n]" ) ; fscanf ( flFile, "\n" ) ; */
            }
          else
            /* Fixed type. */
            for ( nFc = nBeg ; nFc <= nEnd ; nFc++ ) {
              pFc = pFace + nFc ;
              pFc->mVxFc = mVxFace ;
              pFc->pNxtFc[0] = pFc->pNxtFc[1] = NULL ;
              if ( fl_read_ulg ( flFile, flTp, bSwap, mVxFace, pFc->nVx ) 
                   != mVxFace )
                flr_face_failure ( nFc ) ;
              if ( fl_read_ulg ( flFile, flTp, bSwap, 2, pFc->nEl ) != 2 )
                flr_face_failure ( nFc ) ;
              /* fscanf ( flFile, "\n" ) ;*/

              /* if ( pFc->nEl[0] == 7 || pFc->nEl[1] == 7 ) 
                 printf ( " reading faces, found %d-noded face %d ref'ing ele %d, %d\n", 
                 nFc, mVxFace, pFc->nEl[0], pFc->nEl[1] ) ; */

              /*{ int k, matchVx[3] = {0,0,0} ;
                int nVxToMatch[3] = {160561, 160560, 161120} ;
                for ( k = 0 ; k < 3 ; k++ ) {
                if ( pFc->nVx[k] == nVxToMatch[0] ) matchVx[0] = 1 ;
                if ( pFc->nVx[k] == nVxToMatch[1] ) matchVx[1] = 1 ;
                if ( pFc->nVx[k] == nVxToMatch[2] ) matchVx[2] = 1 ;
                }
                if ( matchVx[0] && matchVx[1] && matchVx[2] )
                printf ( "matching face for %d,%d,%d, is %d\n",
                nVxToMatch[0], nVxToMatch[1], nVxToMatch[2], nFc) ;
                }*/

            }
        }
      }
  }

  return ( 1 ) ;
}


/******************************************************************************

  fl_read_sol:
  Read a fluent solution. This solution is cell-based - hip ignores what
  is written for faces - and is then scattered to the nodes. Note that
  the .dat file seems to use decimal counters as opposed to the grid sections
  of the case file which uses hexadecimal.
  
  Last update:
  ------------
  18Apr18: use ulong_t.
           fix bug with arg order calling hdr5.
  27Aug14; allow domain zones numbered other than just 2.
  18Feb13; switch format specifiers to %zu for ulong_t types.
  6Apr09; pass bSwap.
  4Apr09; replace varTypeS with varList.
  14Apr08; supply variable names.
  8Dec07; fix bug with size test on elemental variables.
  14Oct07: conceived.
  
  Input:
  ------
  pUns: the grid
  pChunk: this part of the grid

  Changes To:
  -----------
  pChunk->Punknown
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int fl_read_sol ( uns_s *pUns, chunk_struct *pChunk, const int bSwap ) {

  int nFile, nKey ;
  FILE *flFile ;
  ulong_t mEl, mFc, mVx ;
  int mEq ;
  const int mDim = pUns->mDim ;
  vrtx_struct *pVx ;
  double *pUn ;
  double *pCUnknown ;
  flTp_e flTp ;


  /* from (this is as good as the info gets:)
     /usr/local/Fluent.Inc/fluent6.2.16/client/src/xfile.h

     XF_RF_DATA_PRESSURE=1,
     XF_RF_DATA_TEMPERATURE=3,
     XF_RF_DATA_ENTHALPY=4,
     XF_RF_DATA_SPECIES=7,
     XF_RF_DATA_G=8,               ? What is G? Gamma?

     XF_RF_DATA_DENSITY=101,
     XF_RF_DATA_MU_LAM=102,
     XF_RF_DATA_MU_TURB=103,
     XF_RF_DATA_CP=104,
     XF_RF_DATA_KTC=105,

     XF_RF_DATA_X_VELOCITY=111,
     XF_RF_DATA_Y_VELOCITY=112,
     XF_RF_DATA_Z_VELOCITY=113,

  */

  int idKey[] = {101,111,112,113,1,3} ; /* rho, u,v,w, p, t */
  char idNm[][6] = { "rho", "u", "v", "w", "p","t", } ;

  int kVar, nVar, iVar,solVar;
  char *pKey ; 
  int nZone, sz ;
  ulong_t nBeg, nEnd ;
  ulong_t mRead ;
  varList_s *pVL = &pUns->varList ;
  ulong_t nVx, nEl ;
  double *pCUn ;
  vrtx_struct *pVrtx = pChunk->Pvrtx ;
  elem_struct *pElem = pChunk->Pelem, *pEl ;
  ulong_t mV ;
  vrtx_struct **ppVx ;
  int kVx ;
  double fac ;


  /* Do case and data file match? */
  nKey = 1 ; nFile = 0 ;
  if ( !r1map_pos_keyword ( "33", &nFile, &nKey, &flFile ) )
    hip_err ( fatal, 0, "no dimension statement (33 in fl_read_sol.\n" ) ;
  fscanf ( flFile, "%*[^(]" ) ;
  fscanf ( flFile, "(%"FMT_ULG" %"FMT_ULG" %"FMT_ULG"", &mEl, &mFc, &mVx ) ;

  if ( mEl != pChunk->mElems || mVx != pChunk->mVerts ) {
    sprintf ( hip_msg," mismatch between case and data:\n"
              "          %"FMT_ULG" vs %"FMT_ULG" elements,\n"
              "          %"FMT_ULG" vs %"FMT_ULG" nodes.\n"
              "          disregarding solution.\n",
              pChunk->mElems, mEl, pChunk->mVerts, mVx ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }



  /* Alloc for unknowns. For the time being, cold flow only. */
  mEq = mDim + 3 ;
  pChunk->Punknown = arr_malloc ( "pChunk->Punknown in fl_read_sol", pUns->pFam,
                                  ( mVx+1 )*mEq, sizeof( double ) ) ;

  /* Set the vrtx pointer. */ 
  for ( pVx = pChunk->Pvrtx+1, pUn = pChunk->Punknown + mEq ;
        pVx <= pChunk->Pvrtx + pChunk->mVerts ; pVx++, pUn += mEq )
    pVx->Punknown = pUn ;

  /* Alloc for a temporary list of cell-based variables. */
  pCUnknown = arr_malloc ( "pCunknown in fl_read_sol", pUns->pFam,
                           ( mEl+1 )*mEq, sizeof( double ) ) ;





  solVar = 0 ; 
  for ( nVar = 0, kVar = 0 ; nVar <= mEq ; nVar++, kVar++ ) {
    /* Skip nVar = 3, the w component for 2D. */
    if ( mDim == 2 && nVar == 3 ) nVar++ ;
    mRead = 0 ;

    /* Read cell-based variables. */
    for ( flTp = fl_ascii ; flTp <= fl_dbl ; flTp++ ) {

      pKey = fl_key ( "300", flTp ) ; 

      for ( nKey = 1 ;
            fl_read_hdr5 ( pKey, nKey, &nZone, &iVar, &sz, &nBeg, &nEnd, &flFile ) != -1 ;
            nKey++ ) {

        if ( iVar == idKey[nVar] && fl_face_type( nZone ) == fl_fc_domain ) {
          /* Only read cells of the volume grid, not bnd faces. */
          strncpy ( pVL->var[solVar].name, idNm[nVar], LEN_VARNAME ) ;

          fscanf ( flFile, "%*[^(]" ) ;  fscanf ( flFile, "(" ) ;
          if ( nEnd > mEl ) {
            sprintf ( hip_msg, 
                      "found %"FMT_ULG" elems, only %"FMT_ULG" expected in fl_read_sol.\n",
                      nEnd, mEl ) ;
            hip_err ( fatal, 0, hip_msg ) ; 
          }

          mRead += nEnd-nBeg+1 ;
          pCUn = pCUnknown +kVar ;
          for ( nEl = nBeg ; nEl <= nEnd ; nEl++ ) {
            pCUn += mEq ;

            if ( fl_read_real ( flFile, flTp, bSwap, 1, pCUn ) != 1 ) {
              sprintf ( hip_msg, 
                        "failed to read cell unknown (%d) in fl_read_sol.\n",
                        nVar+1 ) ;
              hip_err ( fatal, 0, hip_msg ) ; 
            }
          }
         solVar ++;
        }
      }
    }

    /* Enough unknowns read for this var? */
    if ( mRead != mEl ) {
      sprintf ( hip_msg, 
                "found only %"FMT_ULG" cell unknowns for var %s in fl_read_sol.",
                mRead, idNm[kVar] ) ;
      hip_err ( info, 0, hip_msg ) ;
      solVar--; 
    }
  }

  mEq = solVar; 
  /* These are primitive variables. */
  pUns->varList.mUnknowns = pUns->varList.mUnknFlow = mEq ;
  pUns->varList.varType = prim ;
  for ( kVar = 0 ; kVar <  mEq ; kVar++ ) {
      pVL->var[kVar].cat = add ;
      strcpy ( pVL->var[kVar].grp,"Additionals");
    }


  /* Scatter those variables to the nodes. */
  /* First reset node counters. Use nr for that. */
  for ( pVx = pVrtx+1 ; pVx <= pVrtx+mVx ; pVx++ ) {
    pVx->number = 0 ;
    pUn = pVx->Punknown ;
    for ( kVar = 0 ; kVar < mEq ; kVar++ )
      pUn[kVar] = 0. ;
  }

  /* Scatter and count the number of donors. */
  for ( nEl = 1 ; nEl <= mEl ; nEl++ ) {
    pEl = pElem + nEl ;
    mV = elemType[ pEl->elType ].mVerts ; 
    ppVx = pEl->PPvrtx ;

    for ( kVx = 0 ; kVx < mV ; kVx++ ) {
      pVx = ppVx[kVx] ;
      pVx->number++ ;
      for ( kVar = 0, pCUn = pCUnknown + nEl*mEq ; kVar < mEq ; kVar++, pCUn++ )
        pVx->Punknown[kVar] += *pCUn ;
    }
  }

  /* Average. */
  for ( pVx = pVrtx+1 ; pVx <= pVrtx+mVx ; pVx++ ) {
    fac = 1./pVx->number ;
    pUn = pVx->Punknown ;
    for ( kVar = 0 ; kVar < mEq ; kVar++ )
      pUn[kVar] *= fac ;
  }

  arr_free ( pCUnknown ) ;    
  return ( 1 ) ;
}

/******************************************************************************

  fl_isKey:
  Is this a valid fluent key? Has to be a number between 0 and 4000, well
  for future expansion let's allow 4 digit ints. 
  Provided as a call-back function to r1map.
  
  Last update:
  ------------
  13Oct07: conceived.
  
  Input:
  ------
  key: the key
  
  Returns:
  --------
  0 if it is not, 1 if it is.
  
*/

int fl_isKey ( const char *key ) {

  int len = strlen(key), i, someInt ;

  if ( len < 1 || len > 4 )
    return ( 0 ) ;

  for ( i = 0 ; i < len ; i++ )
    if ( !isdigit( key[i] ) )
      return ( 0 ) ;
  
  someInt = atoi ( key ) ;
  if ( someInt < 0 )
    return ( 0 ) ;
  else if ( someInt > 9999 )
    return ( 0 ) ;
  else 
    return ( 1 ) ;
}

/******************************************************************************

  fl_endKey:
  Provide a binary endkey string to r1map.
  Provided as a call-back function to r1map.
  
  Last update:
  ------------
  13Oct07: conceived.
  
  Input:
  ------
  key: the key to close

  Returns:
  --------
  "\0" for ascii, "End of Binary Section" for binary.
  
*/

const char * fl_endKey ( const char* key ) {

  static char binEnd[] = "Binary", asciiEnd[] = "\0" ;
  int iKey ;

  if ( !fl_isKey ( key ) )
    return ( asciiEnd ) ;

  iKey = atoi ( key ) ;

  if ( iKey > 1999 )
    return ( binEnd ) ;
  else
    return ( asciiEnd ) ;
}

/******************************************************************************
  fl_read_zones:   */

/*! Read the zone names and types and create a bc entry for each.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  8Jul11; scan for both zone tags, 39 and 45.
  2Apr11: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

void fl_read_zones () {

  flTp_e flTp ;
  char *pKey, key[LINE_LEN], someStr[LINE_LEN] ;
  int nKey, nZone ;
  FILE *flFile ;
  int nFile = 0 ;

  char bcType[LINE_LEN], bcName[LINE_LEN] ;
  
  mZoneBc = 0 ;
  zoneBc_s *pZn ;

  int kid ;
  for ( kid = 0 ; kid < 2 ; kid++ ) {
    if( kid ) sprintf ( key, "39" ) ;
    else      sprintf ( key, "45" ) ;


    for ( flTp = fl_ascii ; flTp <= fl_dbl ; flTp++ ) {
      pKey = fl_key ( key, flTp ) ;

  
      for ( nKey = 1 ;
            r1map_pos_keyword ( pKey, &nFile, &nKey, &flFile) ;
            nKey++ ) {

        fscanf ( flFile, "%*[^(]" ) ;
        fscanf ( flFile, "(" ) ;
        /* Read the string up to the closing parenthesis. */
        fscanf( flFile, "%[^)]", someStr ) ;

        if ( sscanf ( someStr, "%d %s %s", &nZone, bcType, bcName ) !=3 ) {
          sprintf ( hip_msg,
                    "failed to read bc name and type in fl_read_zones.\n" ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }
        
        if ( verbosity > 5 )
          printf ( "found bc %d for zone %d, bcType %s, bcName %s\n", 
                   mZoneBc, nZone, bcType, bcName ) ;
                   
        pZn = zoneBc+mZoneBc ;
        mZoneBc++ ;

        pZn->nZone = nZone ;
        if ( !strncmp( bcType, "fluid", sizeof( "fluid" ) ) ) {
          /* domain zone. */
          pZn->fcType = fl_fc_domain ;
          pZn->pBc = NULL ;
          strncpy ( pZn->type, bcType, TEXT_LEN-1 ) ;
          strncpy ( pZn->name, bcName, TEXT_LEN-1 ) ;
        }
        else if ( !strncmp( bcType, "interior", sizeof( "interior" ) ) ) {
          /* interior walls. */
          pZn->fcType = fl_fc_interior ;
          pZn->pBc = NULL ;
          strncpy ( pZn->type, bcType, TEXT_LEN-1 ) ;
          strncpy ( pZn->name, bcName, TEXT_LEN-1 ) ;
        }
        else {
          /* boundary patch. */
          pZn->fcType = fl_fc_bnd ;
          pZn->pBc = find_bc ( bcName, 1 ) ;
          pZn->pBc->type[0] = fl_bcType( bcType ) ;
          pZn->pBc->type[1] = '\0' ;
          strncpy ( pZn->name, bcName, TEXT_LEN-1 ) ;
        }

        if ( mZoneBc >= MAX_BC )
          hip_err ( fatal, 0,"too many boundaries in fl_read_zones,\n"
                    "                increase MAX_BC in read_fluent.\n" );
      }
    }
  }
  return ;
}

/******************************************************************************

  read_fluent:
  Read a grid and solution from fluent file.
  
  Last update:
  ------------
  18Apr18; use ulong_t.
           record pGrid in pUns.
  1Jul16; new interface to check_uns.
  5Apr09; call check_var_name.
  7Dec07; fix bug with reading solution when no sol file given.
  14Oct07; intro reading of binary files, split into more manageable functions.
  27Jun07; extract elem2vx_from_fc, change in r1map_reset interface.
  14Apr03; remove keyword listing.
  26Jun98: conceived.
  
  Input:
  ------
  flFileName:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_fluent ( const char *keyword, char *flCase, char* flDat ) {
  
  int bSwap = 0 ;
  unsigned int mDim ;
  ulong_t mFlFc, mElemsType[MAX_ELEM_TYPES] ;
  int mFlBnd, kVx, keySize, minALen ;
  char magicKey[] = "(", closeFiles[] = "t", key[5] ;

  grid_struct *pGrid ;
  uns_s *pUns ;
  chunk_struct *pChunk ;
  flFc_s *pFace ;
  flBc_s *pFlBnd ;


  if ( !strncmp ( keyword, "flbswap", 4 ) )
    bSwap = 1 ;
  
  /* Map the file. Fluent files are always ascii, bogus magicBKey. */
  keySize = sizeof( magicKey ) ;
  minALen = 1 ;
  r1map_reset ( magicKey, &minALen, magicKey, &keySize, 1, closeFiles, 
                fl_isKey, fl_endKey ) ;
  prepend_path ( flCase ) ;
  r1map_open_file ( flCase, "a" ) ;

  if ( flDat[0] != '\0' ) {
    prepend_path ( flDat ) ;
    r1map_open_file ( flDat, "a" ) ;
  }

  if ( verbosity > 5 ) R1MAP_LIST_KEYWORDS() ;

  /* Find the zone names and types. */
  fl_read_zones ( ) ;


  /* Read headers with grid sizes and allocate. */
  pUns = fl_read_sizes ( &pChunk, &mFlFc, &mFlBnd ) ;


  /* Temporary face storage of all faces. Fluent numbers faces from 1. */
  pFace = arr_malloc ( "pFace in read_uns_fluent",
                       pUns->pFam,mFlFc+1, sizeof( flFc_s ) ) ;
  pFlBnd = arr_malloc ( "pFlBnd in read_uns_fluent",
                        pUns->pFam,mFlBnd, sizeof( flBc_s ) ) ;
  
  /* Read coordinates. */
  fl_read_coor ( pChunk, pUns->mDim, bSwap ) ;



  /* Read the element types and allocate for elem->vx pointers. */
  fl_read_elemType ( pUns, pChunk, bSwap ) ;
  
  /* Read the fluent style faces. */
  fl_read_faces ( mFlFc, pFace, mFlBnd, pFlBnd, bSwap ) ;

  /* Assemble the element to vertex pointers from the list of faces. */
  elem2vx_from_fc ( mFlFc, pFace, pChunk->mElems, pChunk->Pelem, pChunk->Pvrtx ) ;

  /* Boundary faces. */
  fl_make_bndFc ( pUns, pChunk, pFace, mFlBnd, pFlBnd ) ;

  /* Dealloc temporary spaces. */
  arr_free ( pFace ) ;
  arr_free ( pFlBnd ) ;



  /* Read solution, if there is any. */
  if ( flDat[0] != '\0' ) fl_read_sol ( pUns, pChunk, bSwap ) ;

  /* Done with reading files. */
  r1map_close_allfiles () ;




  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 



  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;

  Grids.epsOverlap = .9*pUns->hMin ;
  Grids.epsOverlapSq = Grids.epsOverlap* Grids.epsOverlap ;
  
  /* Make a new grid. */
  if ( !( pGrid = make_grid () ) ) {
    free_chunk ( pUns, &pChunk ) ;
    hip_err ( fatal, 0, "failed to link list of grids failed in read_fluent." ) ;
  }
  else {
    /* Put the chunk into Grids. */
    pGrid->uns.type = uns ;
    pGrid->uns.pUns = pUns ;
    pGrid->uns.mDim = pUns->mDim ;
    pGrid->uns.pVarList = &(pUns->varList) ;
    pUns->nr = pGrid->uns.nr ;
    pUns->pGrid = pGrid ;
  }
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  
  return ( 1 ) ;
}


