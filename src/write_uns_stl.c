/*
  write_uns_stl.c:
*/

/*! write a 3d surface mesh in stl format.
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  26Aug24; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
//#include "proto_adapt.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern const Grids_struct Grids ;
extern const elemType_struct elemType[] ;


/***************************************************************************

  PRIVATE

**************************************************************************/
/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  ustl_write_face:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s ustl_write_face ( FILE *fStl,
                        const vrtx_struct *pVx0,
                        const vrtx_struct *pVx1,
                        const vrtx_struct *pVx2
                        ) {
#undef FUNLOC
#define FUNLOC "in ustl_write_face"
  
  ret_s ret = ret_success () ;

  // Calc face normal
  double fcNrm[MAX_DIM] ;
  const double *pCoVx[MAX_VX_FACE] ;
  int mTimesNormal ;
  pCoVx[0] =pVx0->Pcoor ; 
  pCoVx[1] =pVx1->Pcoor ; 
  pCoVx[2] =pVx2->Pcoor ; 
  uns_face_normal_co ( MAX_DIM, MAX_DIM, pCoVx, fcNrm, &mTimesNormal ) ;
  vec_mult_dbl ( fcNrm, 1.*mTimesNormal, MAX_DIM ) ;


  // Write face.
  #define NPR "21.14"
  // #define NPR "15.8"
  fprintf ( fStl, "facet normal %g %g %g\n", fcNrm[0], fcNrm[1], fcNrm[2]) ;
  fprintf ( fStl, "    outer loop\n") ;
  fprintf ( fStl, "      vertex %"NPR"e %"NPR"e %"NPR"e\n", pCoVx[0][0], pCoVx[0][1], pCoVx[0][2] ) ;
  fprintf ( fStl, "      vertex %"NPR"e %"NPR"e %"NPR"e\n", pCoVx[1][0], pCoVx[1][1], pCoVx[1][2] ) ;
  fprintf ( fStl, "      vertex %"NPR"e %"NPR"e %"NPR"e\n", pCoVx[2][0], pCoVx[2][1], pCoVx[2][2] ) ;
  fprintf ( fStl, "    endloop\n") ;
  fprintf ( fStl, "endfacet\n") ;
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  ustl_write_bc:
*/
/*! write an stl for one boundary patch.
 *
 */

/*
  
  Last update:
  ------------
  Aug24: conceived.
  

  Input:
  ------
  pUns: grid
  stlNamePrefix: file name root 
  writeManyFiles: if nonzero, write a separate a file for each bc.
                otherwise write a single STL with all bnd, 

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s ustl_write_bc ( const uns_s *pUns, const char* stlNamePrefix0,
                      const int writeManyFiles ) {
#undef FUNLOC
#define FUNLOC "in ustl_write_bc"
  
  ret_s ret = ret_success () ;


  // loop over all bc, for now all types
  int nBc ;
  char flNm[LINE_LEN] ;
  FILE *fStl = NULL ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  vrtx_struct **ppVx,  *pVxBeg, *pVxEnd, *pVx ;
  const faceOfElem_struct *pFoE ;
  ulong_t mVerts ;
  const int *kVxFc ;
  int non34facet_warning_given = 0, doCloseFile=0 ;
#define MAX_BC_TAG (11)
  char shortBcText[MAX_BC_TAG] ;

  char stlNamePrefix[LINE_LEN] ;
  if ( !strlen( stlNamePrefix0 ) )
    snprintf ( stlNamePrefix, LINE_LEN, "surface_of_%s", pUns->pGrid->uns.name ) ;
  else
    strncpy ( stlNamePrefix, stlNamePrefix0, LINE_LEN ) ;
    


  // Open a .geo.
  FILE *fGeo = NULL ;
  snprintf ( flNm, LINE_LEN, "%s.geo", stlNamePrefix ) ;
  if ( !( fGeo = fopen ( flNm, "w" ) ) ) {
    sprintf ( hip_msg, "could not open file %s "FUNLOC".", flNm ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  fprintf ( fGeo, "// geo file written by hip, version %s\n", version ) ;
  
  
  if ( !writeManyFiles ) {
    // Open a single STL.
    snprintf ( flNm, LINE_LEN, "%s.stl", stlNamePrefix ) ;
    if ( !( fStl = fopen ( flNm, "w" ) ) ) {
      sprintf ( hip_msg, "could not open file %s "FUNLOC".", flNm ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    fprintf ( fGeo, "Merge \"%s\";\n", flNm ) ;
  }

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {

    if ( writeManyFiles ) {
      // Create the filename for this bc and open
      snprintf ( shortBcText, MAX_BC_TAG, "%s", pUns->ppBc[nBc]->text ) ;
      snprintf ( flNm, LINE_LEN, "%s_bnd_%03d_%s.stl",
                stlNamePrefix, nBc, shortBcText ) ;
      if ( !( fStl = fopen ( flNm, "w" ) ) ) {
        sprintf ( hip_msg, "could not open file %s "FUNLOC".", flNm ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      fprintf ( fGeo, "Merge \"%s\";\n", flNm ) ;
    }

    // Add entry to .geo
    fprintf ( fGeo, "//+\n" ) ;
    fprintf ( fGeo, "Physical Surface(\"%s\", %d) = {%d};\n",
              pUns->ppBc[nBc]->text, pUns->mBc+nBc+1, nBc+1) ; 
    fprintf ( fGeo, "Surface Loop(%d) = {%d};\n",
              nBc+1,nBc+1) ;


    // Add header to stl.
    fprintf ( fStl, "solid boundary %d named %s\n",
              nBc,pUns->ppBc[nBc]->text ) ;  

    // loop over all faces of this bc
    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pElem = pBndFc->Pelem ;
        if ( pElem && pElem->number && pBndFc->nFace ) {
          pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
          mVerts = pFoE->mVertsFace ;
          kVxFc = pFoE->kVxFace ;
          ppVx = pElem->PPvrtx ;
    

          // if face is tri, add 1 tri fc.
          if ( mVerts == 3 )
            ustl_write_face ( fStl, ppVx[kVxFc[0]], ppVx[kVxFc[1]], ppVx[kVxFc[2]] ) ;

          // if face is quad add 1 tri fc 123, 1 tri fc 134
          else if ( mVerts == 4 ) {
            ustl_write_face ( fStl, ppVx[kVxFc[0]], ppVx[kVxFc[1]], ppVx[kVxFc[2]] ) ;
            ustl_write_face ( fStl, ppVx[kVxFc[0]], ppVx[kVxFc[2]], ppVx[kVxFc[3]] ) ;
          }

          else if ( !non34facet_warning_given ) {
            // Can't treat this type of face. Warn only once.
            sprintf ( hip_msg, "found face with %"FMT_ULG" vertices, ignored "FUNLOC",",
                      mVerts ) ;
            hip_err ( warning, 1, hip_msg)  ;
            non34facet_warning_given = 1 ;
            ret = ret_failure( hip_msg ) ;
          }
        }
      }
    }

    // Add footer
    fprintf ( fStl, "endsolid boundary %d named %s\n",
              nBc,pUns->ppBc[nBc]->text ) ;  

    if ( writeManyFiles )
      // close bc file
      fclose (fStl ) ;
  }
      
  if ( !writeManyFiles )
    // close file with all bc
    fclose (fStl ) ;

  // Form volume in .geo
  fprintf ( fGeo, "//+\n" ) ;
  fprintf ( fGeo, "Volume(1) ={%d:%d};\n", 1, pUns->mBc ) ;
  fprintf ( fGeo, "//+\n" ) ;
  fprintf ( fGeo, "Physical Volume(\"vol\",%d) = {1};\n",  2*pUns->mBc+1 ) ;
  fclose ( fGeo ) ;

  return ( ret ) ;
}


/***************************************************************************

  PUBLIC

**************************************************************************/


/******************************************************************************
  write_stl:   */

/*! write a 3d surface mesh in stl format.
 *
 */

/*
  
  Last update:
  ------------
  26aug24: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s write_stl ( grid_struct *pGrid, const char* stlNamePrefix ) {
#undef FUNLOC
#define FUNLOC "in write_stl"
  ret_s ret = ret_success () ;

  // is the grid uns?
  if ( pGrid->uns.type != uns ) {
    sprintf ( hip_msg, "grid %d named %s is not unstructured"FUNLOC".\n",
              pGrid->uns.nr, pGrid->uns.name ) ;
    hip_err ( warning, 0, hip_msg ) ;
    ret = ret_failure( hip_msg ) ;
    return ( ret ) ;
  }
  uns_s *pUns = pGrid->uns.pUns ;

  // must be 3d
  if ( pUns->mDim != 3 ) {
    sprintf ( hip_msg, "grid %d named %s is %d-dim,"
              " only 3d supported "FUNLOC".\n",
              pUns->nr, pUns->pGrid->uns.name, pUns->mDim ) ;
    hip_err ( warning, 0, hip_msg ) ;
    ret = ret_failure( hip_msg ) ;
    return ( ret ) ;
  }

  // is it w/o boundaries?, then exit
  if ( !pUns->mBc ) {
    sprintf ( hip_msg, "grid %d named %s has no boundaries"FUNLOC".\n",
              pUns->nr, pUns->pGrid->uns.name ) ;
    hip_err ( warning, 0, hip_msg ) ;
    ret = ret_failure( hip_msg ) ;
    return ( ret ) ;
  }
   

  // is it a surf only grid?
  // Should be transparent?
      //if ( !pUns->specialTopo = surf )
      //return ( ustl_write_surf ( pUns, stlNamePrefix ) ) ;

  // extract and write the bnd patches of the vol grid
  // else
  // So far only a single file. Expose this option later.
  const int dontWriteManyFiles = 0; 
  return ( ustl_write_bc ( pUns, stlNamePrefix, dontWriteManyFiles ) ) ;
}
