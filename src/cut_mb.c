/*
 cut_mb.c:

 Last update:
 ------------
 6Jan15; rename pMbFam to more descriptive pArrFamMb
 22Jul96; new parameters. use externally marked surfaces.
 25Apr96: cleanup.
 15Feb96; conceived.

 Contains:
 ---------
 mark_mb_all:
 cut_mb_iso:
 cut_mb_dist:

 mb_iso:
 aspect_ratio_mb:
 
 mb_distance:
 mb_markDist:
 mb_markElem:
 mb_markVert:
*/


#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

extern arrFam_s *pArrFamMb ;

/******************************************************************************

  mark_mb_all:
  Mark the entire multiblock mesh.
  
  Last update:
  ------------
  
  Input:
  ------
  Pmb: a multiblock root.
  cutDist: distance for the cut from the marked surfaces.
  cutType: 0/1 for one/all vertices within the distance.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mark_mb_all ( mb_struct *Pmb )
{
  block_struct *Pbl ;
  int mCellsBlock=0, mCellsGrid = 0, nDim, *elemMark, nCell ;
  
  /* Loop over all blocks. */
  for ( Pbl = Pmb->PblockS+1 ; Pbl <= Pmb->PblockS + Pmb->mBlocks ; Pbl++ )
  { if ( !Pbl->PelemMark )
    { /* There is no marker field. Count the cells in the block. */
      for ( mCellsBlock = 1, nDim = 0 ; nDim < Pmb->mDim ; nDim++ )
	mCellsBlock *= Pbl->mVert[nDim] - 1 ;
      
      /* Alloc a cell marker for the block. */
      Pbl->PelemMark = arr_malloc ( "Pbl->PelemMark in mark_mb_all", pArrFamMb,
                                    mCellsBlock+1, sizeof( int ) ) ;
      if ( !Pbl->PelemMark )
      { printf ( " FATAL: could not allocate the element marker" ) ;
	printf ( " in mb_iso.\n" ) ;
	return ( 0 ) ;
      }
    }
    elemMark = Pbl->PelemMark ;
    /* Set all marks. */
    for ( nCell = 1 ; nCell <= mCellsBlock ; nCell++ )
      elemMark[nCell] = nCell + mCellsGrid ;
    Pbl->mElemsMarked = mCellsBlock ;
    mCellsGrid += mCellsBlock ;
  }

  if ( mb_markVert ( Pmb ) )
    return ( 1 ) ;
  else
    return ( 0 ) ;
}


/******************************************************************************

  cut_mb_iso:
  Cut a multiblock mesh according to isotropy.
  
  Last update:
  ------------
  23Jul96: conceived.
  
  Input:
  ------
  Pmb: a multiblock root.
  cutDist: distance for the cut from the marked surfaces.
  cutType: 0/1 for one/all vertices within the distance.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int cut_mb_iso ( mb_struct *Pmb, const double isoVal, const isotype_enum isoType )
{
  if ( mb_iso ( Pmb, isoType, isoVal ) && mb_markVert ( Pmb ) )
      return ( 1 ) ;
  else
    return ( 0 ) ;
}


/******************************************************************************

  cut_mb_dist:
  Calculate the distance of vertices in a multi-block with respect to
  logical combinations of the subfaces. Mark all vertices within a
  certain distance.
  
  Last update:
  ------------
  23Jul96: conceived.
  
  Input:
  ------
  Pmb: a multiblock root.
  cutDist: distance for the cut from the marked surfaces.
  cutType: 0/1 for one/all vertices within the distance.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int cut_mb_dist ( mb_struct *Pmb, const double cutDist, const int cutType )
{
  if ( mb_distance ( Pmb->mBlocks, Pmb->PblockS, Pmb->mDim ) )
    if ( mb_markDist ( Pmb->mBlocks, Pmb->PblockS, cutDist ) )
      if ( mb_markElem ( Pmb->mBlocks, Pmb->PblockS, Pmb->mDim, cutType ) ) {
	Pmb->mUnknowns = 1 ;
	return ( 1 ) ;
      }
  return ( 0 ) ;
}


/**********************************************************************

  mb_iso.c:
  Calculate a choice of isotropicity for each cell and mark all those
  with a value inferior to isoVal.

  Input:
  ------
  Pmb:     multiblock grid
  isoType: type of isotropicity.
  isoVal:  cutoff value.
 
  Changes to:
  -----------
  Pmb:

  Returns:
  --------
  0: on failure, 1: on success.
 
*/

int mb_iso ( mb_struct *Pmb, const isotype_enum isoType, const double isoVal )
{
  int nDim, mDim, nBlock, nVert, mVerts, *elemMark, nCell, found, nElem ;
  block_struct *Pbl, *PblC ;
  subFace_struct *PSF ;
  int index1, multVert1, multCell1, index2, multVert2, multCell2,
      offsetVert, offsetCell, indexStatic, dll, dlr, dur, dul, n1, n2,
      ijk[MAX_DIM], ll[MAX_DIM], ur[MAX_DIM] ;
  const bc_struct *Pbc ;
  int dir[MAX_DIM], dirC[MAX_DIM] ;
  double prevAR, *PdblMark ;

  mDim = Pmb->mDim ;
  
  /* Loop over all blocks and make sure there is an element marker field. */
  for ( nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ )
  { Pbl = Pmb->PblockS+nBlock ;
    
    if ( !Pbl->PelemMark )
    { /* Alloc a cell marker for the block. */
      Pbl->PelemMark = arr_malloc ( "Pbl->PelemMark in mb_iso", pArrFamMb,
                                    Pbl->mElemsBlock+1, sizeof( int ) ) ;
      if ( !Pbl->PelemMark )
      { printf ( " FATAL: could not allocate the element marker" ) ;
	printf ( " in mb_iso.\n" ) ;
	return ( nBlock-1 ) ;
      }
    }

    /* Reset the mark. */
    elemMark = Pbl->PelemMark ;
    for ( nCell = 1 ; nCell <= Pbl->mElemsBlock ; nCell++ )
      elemMark[nCell] = 0 ;

    mVerts = Pbl->mVertsBlock ;
    if ( !Pbl->PdblMark )
    { /* Allocate a marker field for each block. Note that this does
	 not take changes in mVertsBlock into account, which shouldn't occur. */
      Pbl->PdblMark = arr_malloc ( "Pbl->PdblMark in mb_iso", pArrFamMb,
                                   mVerts+1, sizeof ( double ) ) ;
      PdblMark = Pbl->PdblMark ;
      if ( !PdblMark )
      { printf ( " FATAL: allocation for a marker field" ) ;
	printf ( " failed in mb_iso.\n" ) ;
	return ( nBlock-1 ) ;
      }
    }
      
    /* Reset the distances of all vertices to a NULL value. */
    /*for ( nVert = 1 ; nVert <= mVerts ; nVert++ )*/
    for ( nVert = 1 ; nVert <= 0 ; nVert++ )
      PdblMark[nVert] = -TOO_MUCH ;
  }
  /* The first unknown will be isotropy. */
  Pmb->mUnknowns = 1 ;

  /* Loop over all marked boundary surfaces. */
  for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc  )
    if ( Pbc->mark )
      /* Loop over all boundary patches of this surface. */
      for ( PSF = Pbc->ProotSubFc ; PSF ; PSF = PSF->PnxtBcSubFc )
      { Pbl = PSF->PlBlock ;
	/* Mark the static side of the block. Note that PSF is always
	   a boundary side, thus Pbl is on the left. */
	for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  if ( PSF->llLBlock[nDim] == PSF->urLBlock[nDim] )
	    if ( PSF->llLBlock[nDim] == 1 )
	      dir[nDim] = 1 ;
	    else
	      dir[nDim] = -1 ;
	  else
	    dir[nDim] = 0 ;

	/* Loop over all the elements on this subface and mark
	   the good ones. This initializes the stacks. */
	get_mb_subface ( Pbl, PSF, mDim, ll, ur,
			 &index1, &multVert1, &multCell1,
			 &index2, &multVert2, &multCell2,
			 &offsetVert, &offsetCell, &indexStatic,
			 &dll, &dlr, &dur, &dul ) ;

	/* Loop over the subface. */
	n2 = ll[index2] ;
	n1 = ll[index1] - 1 ; 
	while ( ( nCell = cell_loop_subfc ( ll, ur, mDim,
					  &n1, index1, multCell1,
					  &n2, index2, multCell2,
                                            offsetCell ) ) ) {
#         ifdef CHECK_BOUNDS
	    /* Count the cells in the block. */
	    if ( nCell > Pbl->mElemsBlock )
	    { printf ( " FATAL: bounds of element Marker exceeded"
		       " in mb_iso.\n" ) ;
	      exit ( EXIT_FAILURE ) ;
	    }
#         endif

	    
	  /* Indices of this cell. */
	  ijk[index1] = n1 ;
	  ijk[index2] = n2 ;
	  /* Note the offset for the cell at the upper bound. */
	  ijk[indexStatic] = ll[indexStatic] + (dir[indexStatic]-1)/2 ;
	  prevAR = 0. ;

	  if ( Pbl->PelemMark[nCell] )
	    /* This cell is marked already. Forget it. */
	    ;
	  else if ( aspect_ratio_mb ( Pbl, ijk, dir, mDim,
				      isoType, isoVal, &prevAR ) )
	  { /* Follow this stack. Mark the bottom cell. */
	    Pbl->PelemMark[nCell] = 1 ;

	    /* Orientation of this block. */
	    PblC = Pbl ;
	    for ( nDim = 0 ; nDim < mDim ; nDim++ )
	      dirC[nDim] = dir[nDim] ;
	    
	    /* Follow this stack of cells until the aspect_ratio exceeds
	       the threshold. */
	    found = 1 ;
	    while ( found )
	    { /* Get the next cell. */
	      if ( !get_mb_ngh_cell ( &PblC, ijk, dirC, mDim ) )
		/* The stack is done. Hit a boundary. */
		break ;

	      /* Cell number of this ijk. */
	      nCell = get_nElem_ijk ( mDim, ijk, PblC->mVert ) ;

#             ifdef CHECK_BOUNDS
	        /* Count the cells in the block. */
	        if ( nCell > PblC->mElemsBlock )
		{ printf ( " FATAL: beyound bounds of element Marker"
			   " in mb_iso.\n" ) ;
		  exit ( EXIT_FAILURE ) ;
		}
#             endif

	      /* Check its aspect ratio. */
	      if ( PblC->PelemMark[nCell] )
		/* This cell is marked already. Done. */
		found = 0 ;
	      else if ( aspect_ratio_mb ( PblC, ijk, dirC, mDim,
					  isoType, isoVal, &prevAR ) )
		/* Mark this cell. */
		PblC->PelemMark[nCell] = 1 ;
	      else
		/* This stack is finished. */
		found = 0 ;
	    }
	  }
	  else
	    /* Always mark the first cell. This cell is needed to
	       define the boundary. */
	    Pbl->PelemMark[nCell] = 1 ;
	    
	}
      }

  /* Loop over all blocks and count the marked elements. */
  for ( nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ )
  { Pbl = Pmb->PblockS+nBlock ;
    for ( Pbl->mElemsMarked = 0, nElem = 1 ;
	  nElem <= Pbl->mElemsBlock ; nElem++ )
      if ( Pbl->PelemMark[nElem] )
	Pbl->mElemsMarked++ ;
  }


  return ( 1 ) ;
}

/******************************************************************************

  aspect_ratio_mb:
  Calculate the aspect ratio of a given cell.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pbl:     Block.
  ijk[]:   Cell indices.
  dir:     Direction for the aspect ratio.
  mDim:    dimensions.
  isoType: Type of aspect ratio. One of minmax/avg_all/dir.
  isoVal:  cutoff value.
  *PprevAR: latest aspectratio value.

  Changes to:
  -----------
  *PprevAR: aspect ratio.

  Returns:
  --------
  0, 1 on no matching or matching the criterion.
  
*/

int aspect_ratio_mb ( const block_struct *Pbl, const int ijk[],
		      const int dir[], const int mDim,
		      const isotype_enum isoType, const double isoVal,
		      double *PprevAR )
{
  int vxCell[2][2][2], n[MAX_DIM], k0, k1, k2, nDim,
      nllf, nulf, nlrf, nurf, mEdgesPerDim, nVx1, nVx2, returnVal=0 ;
  double dist, maxLen[MAX_DIM], minLen[MAX_DIM], maxLenTang, minLenTang,
         maxLenNorm=1.0, *Pcoor, *Pcoor1, *Pcoor2, AR=1.0 ;
  Pcoor = Pbl->Pcoor ;

  /* Reset the values. */
  for ( nDim = 0 ; nDim <= mDim ; nDim++ )
  { maxLen[nDim] = -TOO_MUCH ;
    minLen[nDim] = TOO_MUCH ;
  }
  
  /* Get the forming nodes. */
  if ( mDim == 2 )
  { nllf = ( ijk[1]-1 )*Pbl->mVert[0] + ijk[0] ;
    nulf = nllf + Pbl->mVert[0] ;
    vxCell[0][0][0] = nllf ;
    vxCell[0][0][1] = nllf+1 ;
    vxCell[0][1][0] = nulf ;
    vxCell[0][1][1] = nulf+1 ;
    mEdgesPerDim = 2 ;
  }
  else
  { nllf = ( ( ijk[2]-1 )*Pbl->mVert[1] + ( ijk[1]-1) )*Pbl->mVert[0] + ijk[0] ;
    nulf = nllf + Pbl->mVert[0] ;
    nlrf = nllf + Pbl->mVert[0]*Pbl->mVert[1] ;
    nurf = nlrf + Pbl->mVert[0] ;
    vxCell[0][0][0] = nllf ;
    vxCell[0][0][1] = nllf+1 ;
    vxCell[0][1][0] = nulf ;
    vxCell[0][1][1] = nulf+1 ;
    vxCell[1][0][0] = nlrf ;
    vxCell[1][0][1] = nlrf+1 ;
    vxCell[1][1][0] = nurf ;
    vxCell[1][1][1] = nurf+1 ;
    mEdgesPerDim = 4 ;
  }
  
  if ( isoType == minmax_all || isoType == minmax_dir )
    /* Calculate maximum edge lengths. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
    { maxLen[nDim] = -TOO_MUCH ;
      minLen[nDim] = TOO_MUCH ;

      k0 = ( mDim-nDim )%mDim ;
      k1 = ( mDim-nDim+1 )%mDim ;
      k2 = ( mDim == 2 ? 2 : ( mDim-nDim+2 )%mDim ) ;
      
      for ( n[1] = 0 ; n[1] < 2 ; n[1]++ )
	/* Trip the outermost index once for 2-D. The 'mDim-1' works
	   for 2-D and 3-D only. */
	for ( n[2] = 0 ; n[2] < mDim-1 ; n[2]++ )
	{ n[0] = 0 ;
	  nVx1 = vxCell[n[k2]][n[k1]][n[k0]] ;
	  Pcoor1 = Pcoor + mDim*nVx1 ;
	  n[0] = 1 ;
	  nVx2 = vxCell[n[k2]][n[k1]][n[k0]] ;
	  Pcoor2 = Pcoor + mDim*nVx2 ;
	    
	  dist = sq_distance_dbl ( Pcoor1, Pcoor2, mDim ) ;
	  maxLen[nDim] = MAX( maxLen[nDim], dist ) ;
	  minLen[nDim] = MIN( minLen[nDim], dist ) ;
	}
      minLen[nDim] = sqrt( minLen[nDim] ) ;
      maxLen[nDim] = sqrt( maxLen[nDim] ) ;
    }
  else if ( isoType == avg_all || isoType == avg_dir )
    /* Calculate average edge lengths. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
    { maxLen[nDim] = 0. ;
      minLen[nDim] = 0. ;
	
      k0 = ( mDim-nDim)%mDim ;
      k1 = ( mDim-nDim+1 )%mDim ;
      k2 = ( mDim == 2 ? 2 : ( mDim-nDim+2 )%mDim ) ;
      
      for ( n[1] = 0 ; n[1] < 2 ; n[1]++ )
	for ( n[2] = 0 ; n[2] < mDim-1 ; n[2]++ )
	{ n[0] = 0 ;
	  nVx1 = vxCell[n[k2]][n[k1]][n[k0]] ;
	  Pcoor1 = Pcoor + mDim*nVx1 ;
	  n[0] = 1 ;
	  nVx2 = vxCell[n[k2]][n[k1]][n[k0]] ;
	  Pcoor2 = Pcoor + mDim*nVx2 ;
	    
	  dist = sqrt( sq_distance_dbl ( Pcoor1, Pcoor2, mDim ) ) ;
	  maxLen[nDim] += dist ;
	  minLen[nDim] += dist ;
	}
      maxLen[nDim] /= mEdgesPerDim ;
      minLen[nDim] /= mEdgesPerDim ;
    }
  
  if ( isoType == minmax_all )
  { /* Collapse the three directions. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
    { maxLen[0] = MAX( maxLen[0], maxLen[nDim] ) ;
      minLen[0] = MIN( minLen[0], minLen[nDim] ) ;
    }
    AR = minLen[0]/maxLen[0] ;
    if ( AR < *PprevAR )
      /* When comparing all directions, search for the minimum of the
	 aspect ratio to cut off. */
      returnVal = 0 ;
    else
    { *PprevAR = AR ;
      returnVal = 1 ;
    }
  }
  else if ( isoType == avg_all )
  { /* Collapse the three directions. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
    { maxLen[0] += maxLen[nDim] ;
      minLen[0] += minLen[nDim] ;
    }
    AR = minLen[0]/maxLen[0] ;
    if ( AR < *PprevAR )
      /* When comparing all directions, search for the minimum of the
	 aspect ratio to cut off. */
      returnVal = 0 ;
    else
    { *PprevAR = AR ;
      returnVal = 1 ;
    }
  }
  else if ( isoType == minmax_dir )
  { /* Directional max case. Collapse the two other directions. */
    maxLenTang = -TOO_MUCH ;
    minLenTang = TOO_MUCH ;

    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      if ( dir[nDim] != 0 )
       	/* This is the tangential, "horizontal" direction. */
	minLenTang = MIN( minLenTang, minLen[nDim] ) ;
      else
	maxLenNorm = maxLen[nDim] ; 

    AR = minLenTang/maxLenNorm ;
    if ( AR > isoVal )
      /* When comparing to a specific directions, check against
	 isoVal. */
      returnVal = 0 ;
    else
    { *PprevAR = AR ;
      returnVal = 1 ;
    }
  }
  else if ( isoType == avg_dir )
  { /* Directional avg case. Average the two other directions. */
    maxLenTang = 0. ;
    minLenTang = 0. ;

    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      if ( dir[nDim] )
      	/* This is the tangential, "horizontal" direction. */
	minLenTang += minLen[nDim] ;
      else
      	/* This is the tangential, "horizontal" direction. */
	maxLenTang += maxLen[nDim] ;

    if ( mDim == 2 )
      AR = minLenTang/maxLenTang ;
    else
      /* There are twice as many vertical edges. */
      AR = 2.*minLenTang/maxLenTang ;

    if ( AR > isoVal )
      /* When comparing to a specific directions, check against
	 isoVal. */
      returnVal = 0 ;
    else
    { *PprevAR = AR ;
      returnVal = 1 ;
    }
  }

  if ( Pbl->PdblMark )
    /* Store the max of the aspect ratios of all formed cells with
       the vertices. */
    for ( n[2] = 0 ; n[2] < mDim-1 ; n[2]++ )
      for ( n[1] = 0 ; n[1] < 2 ; n[1]++ )
	for ( n[0] = 0 ; n[0] < 2 ; n[0]++ )
	{ nVx1 = vxCell[n[2]][n[1]][n[0]] ;
	  Pbl->PdblMark[nVx1] = MAX( Pbl->PdblMark[nVx1], AR ) ;
	}

  return ( returnVal ) ;
  
}

/**********************************************************************
 mb_distance.c:
 Calculate the distance of vertices in a multi-block with respect to
 marked surfaces.
 
 Input:
 ------
 mBlock/blockS[]: number of and list of blocks,
 mBc/bcS[]:       number of and list of boundary conditions
 mDim:            dimensions,
 
 Changes to:
 -----------
 blockS[]:

 Returns:
 --------
 0: on failure,
 1: on success.
 
*/

int mb_distance ( int mBlock, block_struct blockS[], const int mDim )
{
  int nDim, nBlock, nVert, mVert,  marked = 0 ;
  block_struct *Pbl ;
  subFace_struct *PSF ;
  root_struct *Ptree ;
  double llBB[MAX_DIM], urBB[MAX_DIM], size, *PdblMark = NULL, 
      *Pcoor, nearestDist ;
  const double *Pnearest ;
  int ll[MAX_DIM], ur[MAX_DIM] ;
  int index1, multVert1, multCell1, index2, multVert2, multCell2,
      offsetVert, offsetCell, indexStatic, dll, dlr, dur, dul, n1, n2 ;
  const bc_struct *Pbc ;
  
  /* Initialize the bounding box. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
  { llBB[nDim] = TOO_MUCH ;
    urBB[nDim] = - TOO_MUCH ;
  }

  /* Loop over all blocks. */
  for ( nBlock = 1 ; nBlock <= mBlock ; nBlock++ )
  { Pbl = blockS+nBlock ;
    
    mVert = Pbl->mVertsBlock ;
    if ( !Pbl->PdblMark )
    { /* Allocate a marker field for each block. Note that this does
	 not take changes in mVertBlock into account, which shouldn't occur. */
      Pbl->PdblMark = arr_malloc ( "Pbl->PdblMark in mb_distance", pArrFamMb,
                                   mVert+1, sizeof ( double ) ) ;
      PdblMark = Pbl->PdblMark ;
      if ( !PdblMark )
      { printf ( " FATAL: allocation for a marker field" ) ;
	printf ( " failed in mb_distance.\n" ) ;
	exit ( EXIT_FAILURE ) ;
      }
    }
      
    /* Reset the distances of all vertices to a NULL value. */
    for ( nVert = 1 ; nVert <= mVert ; nVert++ )
      PdblMark[nVert] = NO_DBL_MARK ;

    /* Find a bounding box for all blocks. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
    { llBB[nDim] = MIN( llBB[nDim], Pbl->llBox[nDim] ) ;
      urBB[nDim] = MAX( urBB[nDim], Pbl->urBox[nDim] ) ;
    }
  }

  /* Initialize the quadtree. Make it larger, such that the borders of
     the bounding box fit inside and not just on the edges. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
  { size = .1*( urBB[nDim] - llBB[nDim] ) ;
    llBB[nDim] -= size ;
    urBB[nDim] += size ;
  }
  Ptree = ini_tree ( pArrFamMb, "mb_distance", mDim, llBB, urBB, coor2coor ) ;

  for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc  )
    if ( Pbc->mark )
    { /* Mark all vertices with this bc once. */
      for ( PSF = Pbc->ProotSubFc ; PSF ; PSF = PSF->PnxtBcSubFc )
      {
	Pbl = PSF->PlBlock ;	  
	get_mb_subface ( Pbl, PSF, mDim, ll, ur,
			 &index1, &multVert1, &multCell1,
			 &index2, &multVert2, &multCell2,
			 &offsetVert, &offsetCell, &indexStatic,
			 &dll, &dlr, &dur, &dul ) ;

	/* Loop over the subface. */
	for ( n2 = ll[index2] ; n2 <= ur[index2] ; n2++ )
	  for ( n1 = ll[index1] ; n1 <= ur[index1] ; n1++ ) {
	    nVert = get_mb_boundVert ( n1, multVert1, n2, multVert2, offsetVert ) ;
	    add_data ( Ptree, ( DATA_TYPE *) ( Pbl->Pcoor+mDim*nVert ) ) ;
	    ++marked ;
	  }
      }
    }

  /* Calculate the distance. */
  if ( marked )
  { printf ( "   Calculating distances for block     " ) ;
    for ( nBlock = 1 ; nBlock <= mBlock ; nBlock++ )
    { Pbl = blockS + nBlock ;
      printf ( "\b\b\b\b%4d", nBlock ) ;
      fflush ( stdout ) ;
      
      for ( nVert = 1 ; nVert <= Pbl->mVertsBlock ; nVert++ )
      { Pcoor = Pbl->Pcoor + mDim*nVert ;
	Pnearest = nearest_data ( Ptree, ( DATA_TYPE *) Pcoor, &nearestDist ) ;
	if ( !Pnearest )
	{ printf ( " FATAL: could not contain vertex %d of block %d in tree.\n",
		  nVert, nBlock ) ;
	  return ( 0 ) ;
	}
	(Pbl->PdblMark)[nVert] = nearestDist ;
      }
    }
    printf ( "\n" ) ;
  }
  else
    printf ( " WARNING: no vertices found that match the marked surfaces.\n" ) ;
  
  /* Remove the tree. */
  del_tree ( &Ptree ) ;

  return ( 1 ) ;
}

/*********************************************************************************

  mb_markDist.c:
  Mark all vertices within a certain distance. 

  Last update:
  25Apr96: cleanup.
  
  Input:
  ------
  mBlocks/blockS[]: number of and list of blocks.
  cutDist:          mark all vertices <= distance,
  
  Changes to:
  -----------
  blockS[].PintMrk:

  Returns:
  --------
  0: on failure,
  1: on success.

*/

int mb_markDist ( const int mBlocks, block_struct blockS[],
		  const double cutDist )
{
  int nBlock, *PintMark, nVert ;
  block_struct *PBL ;
  double *PdblMark ;

  for ( nBlock = 1 ; nBlock <= mBlocks ; nBlock++ )
  { PBL = blockS + nBlock ;
    
    if ( !PBL->PintMark )
    { /* Allocate an integer mark for each block. */
      PBL->PintMark =
	( int * ) arr_malloc ( "PBL->PintMark in mb_markDist", pArrFamMb,
                               PBL->mVertsBlock + 1, sizeof( int ) ) ;
      if ( !PBL->PintMark )
      { printf ( " FATAL: malloc failed for PintMark in mb_markDist.\n" ) ;
	return ( 0 ) ;
      }
    }

    /* Mark all vertices within the distance, unmark all others. */
    PdblMark = PBL->PdblMark ;
    PintMark = PBL->PintMark ;
    for ( nVert = 1 ; nVert <= PBL->mVertsBlock ; nVert++ )
      if ( PdblMark[nVert] <= cutDist )
	PintMark[nVert] = 1 ;
      else
	PintMark[nVert] = 0 ;
  }
  return ( 1 ) ;
}
      

/*********************************************************************************

  mb_markElem.c:
  Allocate a cell marker field for each block. 
  Mark all elements depending on how their vertices are marked. Mark
  either if only one or only if all vertices of the element are marked. 

  Last update:
  ------------
  15jul17; avoid loop over nDim when init mVert to silence -O3 compiler warning.
  25Apr96: cleanup.
  
  Input:
  ------
  mBlocks/blockS[]: number of and list of blocks.
  cutType:          1: all marked, 0: only one marked.
  
  Changes to:
  -----------
  blockS[].PelemMrk:

  Returns:
  --------
  0: on failure,
  1: on success.

*/

int mb_markElem ( const int mBlocks, block_struct blockS[], const int mDim, 
		  const int cutType ) {
  int nBlock, *elemMark, mVert[MAX_DIM], nDim, *vxMark, mCellsBlock, mCells ;
  block_struct *PBL ;
  int nK, nJ, nI, nllf, nulf, nlrf, nurf, nCell ;


  for ( nBlock = 1 ; nBlock <= mBlocks ; nBlock++ ) {
    PBL = blockS + nBlock ;
    vxMark = PBL->PintMark ;
    mVert[0] = PBL->mVert[0] ;
    mVert[1] = PBL->mVert[1] ;
    /* Default value for 2D. */
    mVert[2] = ( mDim == 3 ? PBL->mVert[2] : 1 ) ;

    if ( !PBL->PelemMark ) {
      /* Count the cells in the block. */
      for ( mCellsBlock = 1, nDim = 0 ; nDim < mDim ; nDim++ )
	mCellsBlock *= mVert[nDim] - 1 ;
      
      /* Alloc a cell marker for the block. */
      PBL->PelemMark = ( int * )
	arr_malloc ( "PBL->PelemMark in mb_markElem", pArrFamMb,
                     mCellsBlock+1, sizeof( int ) ) ;
    }
    elemMark = PBL->PelemMark ;

    /* Count the number of marked cells. */
    mCells = 0 ;
    if ( mDim == 2 )
      for ( nJ = 1 ; nJ <= mVert[1]-1 ; nJ++ )
	for ( nI = 1 ; nI <= mVert[0]-1 ; nI++ ) {
	  /* Corner vertices of the cell: */
	  nllf = ( nJ-1)*mVert[0] + nI;
	  nulf = nllf + mVert[0] ;
	  nlrf = nllf + 1 ;
	  nurf = nulf + 1 ;
	    
	  /* Cell number. */
	  nCell =  ( nJ-1)*( mVert[0]-1 ) + nI ;
	  
	  if ( cutType &&
	       vxMark[nllf] && vxMark[nulf] && vxMark[nlrf] && vxMark[nurf] )
	    /* All vertices of this cell have been marked. */
	    elemMark[nCell] = ++mCells ;
	  else if ( !cutType && ( vxMark[nllf] || vxMark[nulf] ||
				  vxMark[nlrf] || vxMark[nurf] ) )
	    /* One vertex of this cell has been marked. */
	    elemMark[nCell] = ++mCells ;
	  else
	    elemMark[nCell] = 0 ;
	}

    else
      for ( nK = 1 ; nK <= ( mDim == 2 ? 1 : mVert[2]-1 ) ; nK++ )
	for ( nJ = 1 ; nJ <= mVert[1]-1 ; nJ++ )
	  for ( nI = 1 ; nI <= mVert[0]-1 ; nI++ )
	  {
	    /* Corner vertices of the cell: */
	    nllf = ( ( nK-1 )*mVert[1] + ( nJ-1) )*mVert[0] + nI;
	    nulf = nllf + mVert[0] ;
	    nlrf = nllf + mVert[0]*mVert[1] ;
	    nurf = nlrf + mVert[0] ;
	    
	    /* Cell number. */
	    nCell = ( ( nK-1 )*( mVert[1]-1 ) + ( nJ-1) )*( mVert[0]-1 ) + nI ;
	    
	    if ( cutType &&
		 vxMark[nllf] && vxMark[nllf+1] && vxMark[nulf+1] && vxMark[nulf] &&
	         vxMark[nlrf] && vxMark[nlrf+1] && vxMark[nurf+1] && vxMark[nurf] )
	      /* All vertices of this cell have been marked. */
	      elemMark[nCell] = ++mCells ;
	    else if ( !cutType && ( vxMark[nllf] || vxMark[nllf+1] ||
				    vxMark[nulf+1] || vxMark[nulf] ||
				    vxMark[nlrf] || vxMark[nlrf+1] ||
				    vxMark[nurf+1] || vxMark[nurf] ) )
	      /* One vertex of this cell has been marked. */
	      elemMark[nCell] = ++mCells ;
	    else
	      elemMark[nCell] = 0 ;
	  }
    PBL->mElemsMarked = mCells ;
  }
  return ( 1 ) ;
}

/******************************************************************************

  coor2coor:
  From data to comparable contiguous values for the tree.

  Last update:
  ------------
  3Jun96; conceived.

  Input:
  ------
  Pdata: data pointer.

  Returns:
  --------
  Pdata->Pcoor:

*/

VALU_TYPE *coor2coor( const DATA_TYPE *Pcoor )
{
  /* Suppress the compiler warning. */
  VALU_TYPE *Pvalu ;
  Pvalu = ( VALU_TYPE * ) Pcoor ;
  return ( Pvalu ) ;
}


/******************************************************************************

  mb_markVert:
  Mark all vertices of marked cells in a multiblock grid.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  Pmb:

  Changes To:
  -----------
  Pmb->PintMark
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mb_markVert ( mb_struct *Pmb )
{
  block_struct *Pbl ;
  int *intMark, *elemMark, nllf, nulf, nlrf, nurf, ijk[MAX_DIM] ;

  for ( Pbl = Pmb->PblockS +1 ; Pbl <= Pmb->PblockS + Pmb->mBlocks ; Pbl++ )
  {
    if ( !Pbl->PintMark )
    { /* Allocate an integer mark for each block. */
      Pbl->PintMark = arr_malloc ( "Pbl->PintMark in mb_markVert", pArrFamMb,
                                   Pbl->mVertsBlock + 1, sizeof( int ) ) ;
    }
    intMark = Pbl->PintMark ;
    elemMark = Pbl->PelemMark ;

    /* Loop over all cells. */
    if ( Pmb->mDim == 2 )
    { for ( ijk[1] = 1 ; ijk[1] < Pbl->mVert[1] ; ijk[1]++ )
	for ( ijk[0] = 1 ; ijk[0] < Pbl->mVert[0] ; ijk[0]++ )
	  if ( elemMark[ get_nElem_ijk ( Pmb->mDim, ijk, Pbl->mVert ) ] )
	  { /* This cell is marked. Mark it's vertices. */
	    nllf = ( ijk[1]-1 )*Pbl->mVert[0] + ijk[0] ;
	    nulf = nllf + Pbl->mVert[0] ;
	    intMark[nllf] = 1 ;
	    intMark[nllf+1] = 1 ;
	    intMark[nulf] = 1 ;
	    intMark[nulf+1] = 1 ;
	  }
    }
    else
    { for ( ijk[2] = 1 ; ijk[2] < Pbl->mVert[2] ; ijk[2]++ )
	for ( ijk[1] = 1 ; ijk[1] < Pbl->mVert[1] ; ijk[1]++ )
	  for ( ijk[0] = 1 ; ijk[0] < Pbl->mVert[0] ; ijk[0]++ )
	    if ( elemMark[ get_nElem_ijk ( Pmb->mDim, ijk, Pbl->mVert ) ] )
	    { nllf = ( ( ijk[2]-1 )*Pbl->mVert[1] + ( ijk[1]-1) )*Pbl->mVert[0] + ijk[0] ;
	      nulf = nllf + Pbl->mVert[0] ;
	      nlrf = nllf + Pbl->mVert[0]*Pbl->mVert[1] ;
	      nurf = nlrf + Pbl->mVert[0] ;
	      intMark[nllf] = 1 ;
	      intMark[nllf+1] = 1 ;
	      intMark[nulf] = 1 ;
	      intMark[nulf+1] = 1 ;
	      intMark[nlrf] = 1 ;
	      intMark[nlrf+1] = 1 ;
	      intMark[nurf] = 1 ;
	      intMark[nurf+1] = 1 ;
	    }
    }
  }
  return ( 1 ) ;
}
