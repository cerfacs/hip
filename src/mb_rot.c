/*
   mb_rot.c
   Integer rotations for structured grid methods.
 
   Last update:
   ------------
   30May96; cut out of mb_meth.c

   Contains:
   ---------
   make_rotationS:
   find_rot:
   find_rot_ijk:
   trans_l2r:
   trans_r2l:
   get_mb_elemShift:
   get_mb_vertShift:
 
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

extern Grids_struct Grids ;
rotation_struct *pRotations ;
int mRot = 0 ;
extern arrFam_s *pArrFamMbInit ;
extern char hip_msg[] ;

/******************************************************************************

  make_rotationS:
  Malloc an array of rotation matrices. Hide it from the main.
  
  Last update:
  ------------
  6Jan15; rename pMbInit to more descriptive pArrFamMbInit
  9May96: cut out of cpre.c.
  
  Changes to:
  -----------
  *PProtationS:

  Returns:
  --------
  0: on a malloc failure,
  1: otherwise.
  
*/

void init_mb ( ) {
  
  pArrFamMbInit = make_arrFam ( "mb_init" ) ;
  pRotations = arr_malloc ( "pRotations in init_mb", pArrFamMbInit,
                            MAX_ROT_STRUCT, sizeof( rotation_struct ) ) ;
  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  find_rot_ijk:
*/
/*! build a rotation matrix from numeric ijk transform table.
 *
 */

/*
  
  Last update:
  ------------
  8Mar18: conceived.
  

  Input:
  ------
  blTr: signed index each coor of the left maps onto on the right
        running from 1=i.
  mDim:


  Changes:
  -----------
    rotationS[]: array of rotation_structs.

    
  Returns:
  --------
  pointer to rot struct.
  
*/

rotation_struct *find_rot_123 ( const int *blTr, int mDim ) {       

  char rotChar[2*MAX_DIM+1], *pRc = rotChar ;
  char dirChar[4] = {'0', 'i','j','k'} ;

  hip_err ( fatal, 0, "find_rot_123 is missing code for matchFc." ) ;

  int k ;
  for ( k = 0 ; k < mDim ; k++ ) {
    if ( blTr[k] < 0 ) {
      *pRc++ = '-' ;
      *pRc++ = dirChar[-blTr[k]] ;
    }
    else {
      *pRc++ = ' ' ;
      *pRc++ = dirChar[blTr[k]] ;
    }
  }                       
  
  return ( find_rot ( rotChar, mDim, pRotations ) ) ;
}

  
/************************************************************************

   find_rot:
   Find the rotation matrix from ijk to rotChar. If it doesn't exist, make it.
   The definition of the matrix as given in cpre.h:
   Rotation matrix, left to right. The rotation matrix operates Ax=y,
   with x on the left and y on the right, thus read A as A[from][to], and
   the transformed left system vectors are the columns of A.

   Last update:
   ------------
   9Mar18; reformat braces, use hip_err.
   30May96; moved from read_doc_as.c to mb_meth.c.
   20Apr96: conceived.
   
   Input:
   ------
   rotChar:     the string indicating the transformation from i j k to
                e.g. -j i-k.
   mDim:        number of spatial dimensions.
   rotationS[]: array of rotation_structs.
   
   Changes to:
   -----------
   rotationS[]: array of rotation_structs.

*/


rotation_struct *find_rot ( char rotChar[], int mDim,
			    rotation_struct rotationS[] ) {       
  int nRot, nDim, iDim, nrI, nrJ, nrK ;
  // static int mRot = 0 ; now global in this class.
  rotation_struct *pRot ;
  
  /* Check whether the given rotation matrix exists. */
  for ( nRot = 0 ; nRot < mRot ; nRot++ )
    if ( !strncmp( rotChar, rotationS[nRot].rotChar, 6 ) )
      return ( rotationS+nRot ) ;

  /* The new rotation matrix will be here. */
  pRot = rotationS + mRot ;
  mRot++ ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    for ( iDim = 0 ; iDim < mDim ; iDim++ )
      pRot->rotMatrix[nDim][iDim] = 0 ;
  
  /* Make the missing rotation matrix. Check whether the given string
     is valid. There must be one i,k,j in the 1,3,5 position. */
  for ( nrI=nrJ=nrK=0, nDim = 0 ; nDim < mDim ; nDim++ )
    if ( rotChar[2*nDim+1] == 'i' || rotChar[2*nDim+1] == 'I' ) {
      nrI++ ;
      if ( rotChar[2*nDim] == ' ' )
	pRot->rotMatrix[0][nDim] = 1 ;
      else if ( rotChar[2*nDim] == '-' )
	pRot->rotMatrix[0][nDim] = -1 ;
      else
	nrI = -99 ;
    }
    else if ( rotChar[2*nDim+1] == 'j' || rotChar[2*nDim+1] == 'J' ) {
      nrJ++ ;
      if ( rotChar[2*nDim] == ' ' )
	pRot->rotMatrix[1][nDim] = 1 ;
      else if ( rotChar[2*nDim] == '-' )
	pRot->rotMatrix[1][nDim] = -1 ;
      else
	nrJ = -99 ;
    }
    else if ( rotChar[2*nDim+1] == 'k' || rotChar[2*nDim+1] == 'K' ) {
      nrK++ ;
      if ( rotChar[2*nDim] == ' ' )
	pRot->rotMatrix[2][nDim] = 1 ;
      else if ( rotChar[2*nDim] == '-' )
	pRot->rotMatrix[2][nDim] = -1 ;
      else
	nrK = -99 ;
    }
    else {
      sprintf ( hip_msg, "could not parse rotation string \'%s\'.",
                rotChar ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

  if ( mDim == 2 )
    /* Add a bogus k. */
    nrK++ ;
  
  if ( nrI != 1 || nrJ != 1 || nrK != 1 ) {
    sprintf ( hip_msg, "invalid rotation string %s in find_rot.\n",
              rotChar ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }	
  else {
    strcpy ( pRot->rotChar, rotChar ) ;
    return ( pRot ) ;
  }
  /* hip_err will prevent control reaching here, but compiler doesn't understand. */
  return ( NULL ) ;
}

/************************************************************************

   find_rot_ijk:
   Find the rotation matrix given an ijk window on either side. If it
   doesn't exist, make it.
   The definition of the matrix as given in cpre.h:
   Rotation matrix, left to right. The rotation matrix operates Ax=y,
   with x on the left and y on the right, thus read A as A[from][to], and
   the transformed left system vectors are the columns of A.

   Last update:
   ------------
   29Aug97; copied from find_rot.
   
   Input:
   ------
   Psf: a subface with ijk specified on either side.
   mDim: the number of dimensions.
   
   Changes to:
   -----------
   Psf->ProtL2R ;
   rotationS[]: array of rotation_structs.

*/


rotation_struct *find_rot_ijk ( subFace_struct *Psf, int mDim ) {
  
  /* Two maps to convert face orientations. Use as jkMap[sameEnd][jDir[kDir][ij].  */
  static const int jkMap[2][2][2][2] = { { { {1,2}, {-2,1} }, { {2,-1}, {-1,-2} } },
					 { { {2,1}, {1,-2} }, { {-1,2}, {-2,-1} } } } ;
  static char rotChar[7], dimChar[3] = {"ijk"} ;
  static int nDim, statDimL, statDimR, nDimL, nDimR, ijkR[MAX_DIM],
             sameEnd, found, runDir, jk, jDir, kDir ;
  static double *PlCoor, *PrCoor2D[2], *PrCoor ;
  static block_struct *PlBl, *PrBl ;

  strcpy ( rotChar, "     k" ) ;
  PlBl = Psf->PlBlock ;
  PrBl = Psf->PrBlock ;

  /* Find the static directions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    if ( Psf->llLBlock[nDim] == Psf->urLBlock[nDim] ) 
      statDimL = nDim ;
    if ( Psf->llRBlock[nDim] == Psf->urRBlock[nDim] )
      statDimR = nDim ; }
  sameEnd = ( Psf->llLBlock[statDimL] == 1 ? -1 : 1 ) *
            ( Psf->llRBlock[statDimR] == 1 ? -1 : 1 ) ;
  sameEnd = ( sameEnd == 1 ? 1 : 0 ) ;
  
  rotChar[2*statDimL+1] = dimChar[statDimR] ;
  rotChar[2*statDimL] = ( sameEnd ? '-' : ' ' ) ;

  if ( mDim == 2 ) {
    /* That leaves two possible rotations in 2D. Get the corner coordinates. */
    PlCoor = PlBl->Pcoor + 2*get_nVert_ijk ( 2, Psf->llLBlock, PlBl->mVert ) ;
    PrCoor2D[0] = PrBl->Pcoor + 2*get_nVert_ijk ( 2, Psf->llRBlock, PrBl->mVert ) ;
    PrCoor2D[1] = PrBl->Pcoor + 2*get_nVert_ijk ( 2, Psf->urRBlock, PrBl->mVert ) ;

    /* Pick the llL node as anchor. */
    for ( nDim = 0 ; nDim < 2 ; nDim++ ) {
      Psf->matchVertLBlock[nDim] = Psf->llLBlock[nDim] ; }

    /* Find the matching rhs. */
    if ( sq_distance_dbl ( PlCoor, PrCoor2D[0], 2 ) < Grids.epsOverlapSq ) {
      /* Match ll to ll. */
      for ( nDim = 0 ; nDim < 2 ; nDim++ ) {
	Psf->matchVertRBlock[nDim] = Psf->llRBlock[nDim] ; }
      rotChar[2*((statDimL+1)%2)+1] = dimChar[(statDimR+1)%2] ;
      rotChar[2*((statDimL+1)%2)] = ' ' ; }
    else if ( sq_distance_dbl ( PlCoor, PrCoor2D[1], 2 ) < Grids.epsOverlapSq ) {
      /* Match ll to ur. */
      for ( nDim = 0 ; nDim < 2 ; nDim++ ) {
	Psf->matchVertRBlock[nDim] = Psf->urRBlock[nDim] ; }
      rotChar[2*((statDimL+1)%2)+1] = dimChar[(statDimR+1)%2] ;
      rotChar[2*((statDimL+1)%2)] = '-' ; }
    else {
      sprintf ( hip_msg,
                "no match found for 2d subface %d in find_rot_ijk.\n", Psf->nr ) ;
      /*  JDM 19Dec19, no need for correct setup if all we want is
          to interpolate the solution.
          hip_err ( fatal, 0, hip_msg ) ; }  */
      hip_err ( warning, 1, hip_msg ) ; }
  }
  else { // 3D
    /* That leaves four possible rotations in 3D. First the ll and ur corners. */
    PlCoor = PlBl->Pcoor + 3*get_nVert_ijk ( 3, Psf->llLBlock, PlBl->mVert ) ;

    /* Pick the llL node as anchor. */
    for ( nDim = 0 ; nDim < 3 ; nDim++ ) {
      Psf->matchVertLBlock[nDim] = Psf->llLBlock[nDim] ; }

    /* Find the indices of the four corners of the subface viewed from the right. */
    for ( found = jDir = 0 ; jDir < 2 && !found ; jDir++ )
      for ( kDir = 0 ; kDir < 2 && !found ; kDir++ ) {
	/* The static dimension remains. */
	ijkR[statDimR] = Psf->llRBlock[statDimR] ;
	nDim = (statDimR+1)%3 ;
	ijkR[nDim] = ( jDir? Psf->urRBlock[nDim] : Psf->llRBlock[nDim] ) ;
	nDim = (statDimR+2)%3 ;
	ijkR[nDim] = ( kDir? Psf->urRBlock[nDim] : Psf->llRBlock[nDim] ) ;

	PrCoor = PrBl->Pcoor + 3*get_nVert_ijk ( 3, ijkR, PrBl->mVert ) ;

	if ( sq_distance_dbl ( PlCoor, PrCoor, 3 ) < Grids.epsOverlapSq ) {
	  /* Match. */
	  for ( nDim = 0 ; nDim < 3 ; nDim++ ) {
	    Psf->matchVertRBlock[nDim] = ijkR[nDim] ; }

	  for ( runDir = 0 ; runDir < 2 ; runDir++ ) {
	    nDimL = (statDimL+runDir+1)%3 ;
	    jk = jkMap[sameEnd][jDir][kDir][runDir] ;
	    nDimR = (statDimR+ ABS(jk) )%3 ;
	    rotChar[ 2*nDimL+1 ] = dimChar[nDimR] ;
	    rotChar[ 2*nDimL ] = ( jk > 0 ? ' ' : '-' ) ;
	  }
	  found = 1 ;
	}
      }
    if ( !found ) {
      sprintf ( hip_msg, "no match found for 3D subface %d"
	       " in find_rot_ijk.\n", Psf->nr ) ;
     /*  JDM 19Dec19, no need for correct setup if all we want is
          to interpolate the solution.
          hip_err ( fatal, 0, hip_msg ) ; }  */
      hip_err ( warning, 1, hip_msg ) ; }
  }
  
  Psf->ProtL2R = find_rot ( rotChar, mDim, pRotations ) ;
  return ( Psf->ProtL2R ) ;
}


/************************************************************************

   trans_l2r:
   Given a left to right rotation matrix, translate the ijk
   from left to right.

   Last update:
   30May96; moved from read_doc_as.c to mb_meth.c.
   20Apr96: conceived.
   
   Input:
   ------
   ijkL: logical coordinates of the left point.
   rotMatrix: rotation matrix.
   offset: offsets in all three dimensions. 

   Changes to:
   -----------
   ijkR:

*/


void trans_l2r( const int ijkL[MAX_DIM],
	        int rotMatrix[MAX_DIM][MAX_DIM], const int offset[MAX_DIM],
	        int ijkR[MAX_DIM] ) {
 /* Take the integer coordinates of a vertex and transform them
     to a new block via A ijkL+b = ijkR. */
  
  int nRow, nCol ;
  
  /* Initialize with the translation and add the rotation. */
  for ( nRow = 0 ; nRow < MAX_DIM ; nRow++ ) {
    ijkR[nRow] = offset[nRow] ;
    for ( nCol = 0 ; nCol < MAX_DIM ; nCol++ )
      ijkR[nRow] += rotMatrix[nRow][nCol] * ijkL[nCol] ;
  }
}
  
/************************************************************************

   trans_l2r:
   Given a left to right rotation matrix, translate the ijk
   from right to left.

   Last update:
   30May96; moved from read_doc_as.c to mb_meth.c.
   20Apr96: conceived.
   
   Input:
   ------
   ijkR: logical coordinates of the right point.
   rotMatrix: rotation matrix.
   offset: offsets in all three dimensions. 

   Changes to:
   -----------
   ijkL:

*/

void trans_r2l( const int ijkR[MAX_DIM],
	        int rotMatrix[MAX_DIM][MAX_DIM], const int offset[MAX_DIM],
	        int ijkL[MAX_DIM] ) {
 /* Take the integer coordinates of a vertex and transform them
     to a new block via A^T ( ijkRight-vShift ) = ijkLeft. */
  
  int nRow, nCol ;
  
  /* Initialize with the translation and add the rotation. */
  for ( nRow = 0 ; nRow < MAX_DIM ; nRow++ ) {
    ijkL[nRow] = 0 ;
    for ( nCol = 0 ; nCol < MAX_DIM ; nCol++ )
      ijkL[nRow] += rotMatrix[nCol][nRow] * ( ijkR[nCol] - offset[nCol] ) ;
  }
}



/************************************************************************

   get_mb_elemShift:
   Given the rotation matrix and the indices of the matching subwindows
   in each block, calculate a elem offset that is added just like
   the vertex offset after the rotation matrix is applied to the indices.
   
   Last update:
   ------------
   27Jan23; pacify compiler and zero llT at entry.
   9Mar18, reformatting braces, use hip_err.
   30May96; conceived.
   
   Input:
   ------
   PSF:         subface between the blocks.
   mDim:        number of spatial dimensions.
   
   Changes to:
   -----------
   PSF:

*/


void get_mb_elemShift ( subFace_struct *PSF, const int mDim ) {

  int llL[MAX_DIM]={0}, llR[MAX_DIM]={0}, nDim, llT[MAX_DIM]={0} ;

# ifdef CHECK_MATCH
  int urL[MAX_DIM], urR[MAX_DIM], urT[MAX_DIM], misMatch ;
# endif
  
  /* Calculate the indices of the ll and ur cell (as opposed
     to vertex!) of each subwindow. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    PSF->elemShift[nDim] = 0 ;

    /* Left side. */
    if ( PSF->llLBlock[nDim] == PSF->urLBlock[nDim] ) {
      /* This is the static index. */
      if ( PSF->llLBlock[nDim] == 1 ) {
        /* -ijk face. */
        llL[nDim] = PSF->llLBlock[nDim] ;
#       ifdef CHECK_MATCH
        urL[nDim] = PSF->llLBlock[nDim] ;
#       endif
      }
      else {
        /* +ijk face. */
        llL[nDim] = PSF->llLBlock[nDim]-1 ;
#       ifdef CHECK_MATCH
        urL[nDim] = PSF->llLBlock[nDim]-1 ;
#       endif
      }
    }
    else {
      /* Running index. */
      llL[nDim] = PSF->llLBlock[nDim] ;
#     ifdef CHECK_MATCH
      urL[nDim] = PSF->urLBlock[nDim]-1 ;
#     endif
    }

    /* Right side. */
    if ( PSF->llRBlock[nDim] == PSF->urRBlock[nDim] ) {
      /* This is the static index. */
      if ( PSF->llRBlock[nDim] == 1 ) {
        /* -ijk face. */
        llR[nDim] = PSF->llRBlock[nDim] ;
#       ifdef CHECK_MATCH
        urR[nDim] = PSF->llRBlock[nDim] ;
#       endif
      }
      else {
        /* +ijk face. */
        llR[nDim] = PSF->llRBlock[nDim]-1 ;
#       ifdef CHECK_MATCH
        urR[nDim] = PSF->llRBlock[nDim]-1 ;
#       endif
      }
    }
    else  {
      /* Running index. If that index is reversed in the
         transformation, switch the bounds. */
      if ( PSF->ProtL2R->rotMatrix[nDim][0] == -1 ||
           PSF->ProtL2R->rotMatrix[nDim][1] == -1 ||
           PSF->ProtL2R->rotMatrix[nDim][2] == -1 ) {
        /* This index is reversed. */
        llR[nDim] = PSF->urRBlock[nDim]-1 ;
#       ifdef CHECK_MATCH
        urR[nDim] = PSF->llRBlock[nDim] ;
#       endif
      }
      else  {
        llR[nDim] = PSF->llRBlock[nDim] ;
#       ifdef CHECK_MATCH
        urR[nDim] = PSF->urRBlock[nDim]-1 ;
#       endif
      }
    }
  }

  /* Transform the llL to the right block. Note that elemShift
     is set to 0. */
  trans_l2r ( llL, PSF->ProtL2R->rotMatrix, PSF->elemShift, llT ) ;
  /* Calculate the cellShift. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    PSF->elemShift[nDim] = llR[nDim] - llT[nDim] ;
    
# ifdef CHECK_MATCH_FIXED
  /* Check the match of the ur's. Note that elemShift should be
     correct now. */
  trans_l2r ( urL, PSF->ProtL2R->rotMatrix, PSF->elemShift, urT ) ;
  for ( misMatch = 0, nDim = 0 ; nDim < mDim ; nDim++ )
    if ( urR[nDim] != urT[nDim] )
      misMatch = 1 ;

  if ( misMatch ) {
    sprintf ( hip_msg, "upper right corner mismatch between\n"
              "        block %d subface ( %d,%d,%d ) to ( %d,%d,%d ) and\n"
              "        block %d subface ( %d,%d,%d ) to ( %d,%d,%d ).",
              PSF->PlBlock->nr,
              PSF->llLBlock[0], PSF->llLBlock[1], PSF->llLBlock[2],
              PSF->urLBlock[0], PSF->urLBlock[1], PSF->urLBlock[2],
              PSF->PrBlock->nr,
              PSF->llRBlock[0], PSF->llRBlock[1], PSF->llRBlock[2],
              PSF->urRBlock[0], PSF->urRBlock[1], PSF->urRBlock[2] ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
# endif

  return ;
}
  
/******************************************************************************

  get_mb_vertShift:
  Calculate the vertex shift given a rotation matrix and two orgins.
  
  Last update:
  ------------
  9Mar18; reformatting braces, use hip_err.
  : conceived.
  
  Input:
  ------
  Psf: subface.
  mDim:

  Changes To:
  -----------
  Psf->vertShift[]
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void get_mb_vertShift ( subFace_struct *PSF, const int mDim ) {
  
  int nDim, rotVert[MAX_DIM] ;
  rotation_struct *Prot = PSF->ProtL2R ;
# ifdef CHECK_MATCH
    int llT[MAX_DIM],urT[MAX_DIM], ll, ur, misMatch ;
# endif
  
  /* Calculate the vertex shift using the pair of matching vertices. */
  for ( nDim = 0 ; nDim < MAX_DIM ; nDim++ )
    PSF->vertShift[nDim] = 0 ;
  trans_l2r ( PSF->matchVertLBlock, Prot->rotMatrix, PSF->vertShift,
	      rotVert ) ;
  for ( nDim = 0 ; nDim < MAX_DIM ; nDim++ )
    PSF->vertShift[nDim] = PSF->matchVertRBlock[nDim] - rotVert[nDim] ;

# ifdef CHECK_MATCH_FIXED
  /* JDM: ll in one block may not mach onto ll in the other block. 
     Logic needs fixing. */


  
    /* Check the match. Transform the ll and ur corners of the left block. */
    trans_l2r ( PSF->llLBlock, PSF->ProtL2R->rotMatrix, PSF->vertShift, llT ) ;
    trans_l2r ( PSF->urLBlock, PSF->ProtL2R->rotMatrix, PSF->vertShift, urT ) ;
    /* Find the bounding box of indices. JDM 2018: why?
    for ( nDim = 0 ; nDim < MAX_DIM ; nDim++ )
    { ll = MIN( llT[nDim], urT[nDim] ) ;
      ur = MAX( llT[nDim], urT[nDim] ) ;
      llT[nDim] = ll ;
      urT[nDim] = ur ;
    } */

    /* Compare to the right side. */
    for ( misMatch = 0, nDim = 0 ; nDim < mDim ; nDim++ )
      if ( llT[nDim] != PSF->llRBlock[nDim] ||
	   urT[nDim] != PSF->urRBlock[nDim] )
	misMatch = 1 ;

    if ( misMatch ) {
      sprintf ( hip_msg, "vertex mismatch in get_mb_vertShift from\n"
                "        block %d subface ( %d,%d,%d ) to ( %d,%d,%d ) to\n"
                "        block %d subface ( %d,%d,%d ) to ( %d,%d,%d )\n"
                "        via %s.\n",
                PSF->PlBlock->nr,
                PSF->llLBlock[0], PSF->llLBlock[1], PSF->llLBlock[2],
                PSF->urLBlock[0], PSF->urLBlock[1], PSF->urLBlock[2],
                PSF->PrBlock->nr,
                PSF->llRBlock[0], PSF->llRBlock[1], PSF->llRBlock[2],
                PSF->urRBlock[0], PSF->urRBlock[1], PSF->urRBlock[2],
                Prot->rotChar ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
# endif

  return ;
}
