/*
  refbuf_bound.c:
  Make boundary faces for refined or buffered grids.

  Last update:
  ------------
  10January15; reorder functions to avoid prototyping.
               make_2D_bufFc routine seems obsolete.
  8Apr13; use dereferenced pointer in sizeof when allocating.
  3Feb98; collect the existing routines, create make_refbuf_bndfc.


  This file contains:
  -------------------
  make_refbuf_bndfc
  make_2D_childFc
  make_3D_childFc
  make_refFc

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"


extern const int verbosity ;
extern const elemType_struct elemType[] ;


/******************************************************************************

  make_2D_bufFc_obsolete:

  Obsolete? Only 3D version seems used.

  Given a parent element, and two forming vertices, find a child with the same
  face. If found, append a new boundary face. Note that in 2D, there can only
  be one buffered bndFc per term bndFc.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  **PPlstFc: the last boundary face written.


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int make_2D_bufFc_obsolete ( bndFc_struct *pBndFc, uns_s *pUns, 
			   bndFc_struct **ppBndFc, int *pnLstFc, int *pmBndFc ) {

  const elem_struct *Pprt = pBndFc->Pelem ;
  const elemType_struct *PelT = elemType + Pprt->elType ;
  const faceOfElem_struct *PfoE = PelT->faceOfElem + pBndFc->nFace  ;
  elem_struct **PPch, *Pch ;
  int kFace ;
  const vrtx_struct *PchVx1, *PchVx2 ;
  /* The edge number that corresponds to this face. */
  const vrtx_struct *Pvx1 = Pprt->PPvrtx[ PfoE->kVxFace[0] ],
                    *Pvx2 = Pprt->PPvrtx[ PfoE->kVxFace[1] ] ;
  bndFc_struct *PnewFc ;
  
  /* Make sure there are at least MAX_CHILDS_FACE spaces left. */
  if ( *pmBndFc - *pnLstFc < 2 ) {
    *pmBndFc = *pmBndFc*REALLOC_FACTOR + 2 ;
    if ( !( *ppBndFc = arr_realloc ( "ppBndFc in make_2D_bufFc", pUns->pFam, *ppBndFc,
                                     *pmBndFc+1, sizeof( **ppBndFc ) ) ) ) {
      printf ( " FATAL: failed to realloc boundary spaces in make_2D_bufFc.\n" ) ;
      return ( 0 ) ; }
  }

  /* Loop over all children of this element. */
  for ( PPch = Pprt->PPchild ; (Pch = *PPch) ; PPch++ ) {
    if ( Pch->Pparent == Pprt ) {
      PelT = elemType+ Pch->elType ;
      /* Loop over all faces/edges of this element. */
      for ( kFace = 1 ; kFace <= PelT->mSides ; kFace++ ) {
        PfoE = PelT->faceOfElem + kFace ;
        PchVx1 = Pch->PPvrtx[PfoE->kVxFace[0]] ;
        PchVx2 = Pch->PPvrtx[PfoE->kVxFace[1]] ;
        
        if ( PchVx1 == Pvx1 && PchVx2 == Pvx2 ) {
          /* Match. */
          PnewFc = *ppBndFc + (++(*pnLstFc)) ;
          PnewFc->Pelem = Pch ;
          PnewFc->nFace = kFace ;
          PnewFc->Pbc = pBndFc->Pbc ;
          return ( 1 ) ;
        }
      }
    }
    else
      break ;
  }

  return ( 0 ) ;
}




/******************************************************************************

  make_bufFc:
  Given a parent element, and two forming vertices, find a child with the same
  face. If found, append a new boundary face.
  
  Last update:
  ------------
  10Jan15; use hip_err.
           rename to make_bufFc, as 2D version is no longer used.
  13Jun97: derived from make_child_fc_2D.
  
  Input:
  ------
  pBndFc
  **PPlstFc: the last boundary face written.


  Changes To:
  -----------
  PPlstFc
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int make_bufFc ( bndFc_struct *pBndFc, const uns_s *pUns,
                        bndFc_struct **ppBndFc, int *pnLstFc, int *pmBndFc ) {

  const elem_struct *Pprt = pBndFc->Pelem ;
  const elemType_struct *PprtT = elemType + Pprt->elType, *PchT ;
  const int mPrtV = PprtT->mVerts ;
  const faceOfElem_struct *PfoE = PprtT->faceOfElem + pBndFc->nFace  ;
  elem_struct **PPch, *Pch ;
  int kFace, mVxHg, kVxHg[MAX_ADDED_VERTS], mFacets[MAX_FACES_ELEM+1],
    mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1], 
    mFcts, *kVxFct, kVx, iVx, iiVx, iFacet, match ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;
  bndFc_struct *PnewFc ;
# define PVXPRT(kV) ( kV < mPrtV ? Pprt->PPvrtx[kV] : PhgVx[ kV-mPrtV ] )
  
  /* Find the facets to be formed. */
  if ( !( get_drvElem_aE ( pUns, Pprt, &mVxHg, kVxHg, PhgVx, 
                           fixDiag, diagDir ) &&
	  get_surfTri_drvElem ( Pprt, mVxHg, kVxHg, fixDiag, diagDir,
				mFacets, mFacetVerts, kFacetVx ) ) )
    hip_err ( fatal, 0, "could not get the surface triangulation"
             " in make_bufFc.\n" ) ;
  mFcts = mFacets[pBndFc->nFace] ;
  
  /* Make sure there are at least MAX_CHILDS_FACE spaces left. */
  if ( *pmBndFc - *pnLstFc < MAX_CHILDS_FACE ) {
    *pmBndFc = *pmBndFc*REALLOC_FACTOR + MAX_CHILDS_FACE ;
    *ppBndFc = arr_realloc ( "ppBndFc in make_3D_bufFc", pUns->pFam, *ppBndFc,
                             *pmBndFc+1, sizeof( **ppBndFc ) ) ;
  }

  /* Loop over all facets of this face. */
  for ( iFacet = 0 ; iFacet < mFcts ; iFacet++ ) {
    /* The vertices of that facet. */
    kVxFct = kFacetVx[pBndFc->nFace][iFacet] ;
    match = 0 ;
    
    /* Loop over all children of this element. */
    for ( PPch = Pprt->PPchild ;
	  *PPch && (*PPch)->Pparent && (*PPch)->Pparent == Pprt && !match ; 
          PPch++ ) {
      Pch = *PPch ;
      PchT = elemType + Pch->elType ;

      /* Loop over all faces/edges of the child. */
      for ( kFace = 1 ; kFace <= PchT->mSides && !match ; kFace++ ) {
	PfoE = PchT->faceOfElem + kFace ;

	/* Loop over all vertices of the child's face to find the a match with
	   the first vertex of the facet. */
	for ( iVx = 0 ; iVx < PfoE->mVertsFace && !match ; iVx++ ) {
	  kVx = PfoE->kVxFace[iVx] ;
	  if ( Pch->PPvrtx[kVx] == PVXPRT( kVxFct[0] ) )
	    /* Match. Try to match the remaining ones. Note that they have
	       to be listed in the same order. */
	    for ( match = iiVx = 1 ; iiVx < PfoE->mVertsFace ; iiVx++ ) {
	      kVx = PfoE->kVxFace[ (iVx+iiVx)%PfoE->mVertsFace ] ;
	      if ( Pch->PPvrtx[kVx] != PVXPRT( kVxFct[iiVx] ) ) {
		match = 0 ;
		break ; }
	    }
	}
	
	if ( match ) {
	  /* Match. */
	  PnewFc = *ppBndFc + (++(*pnLstFc)) ;
	  PnewFc->Pelem = Pch ;
	  PnewFc->nFace = kFace ;
	  PnewFc->Pbc = pBndFc->Pbc ;
          PnewFc->invalid = 0 ;
	  break ;
	}
      }
    }
    if ( !match )
      hip_err (fatal,0,"could not match buffered face in make_bufFc.\n" ) ;
  }
  return ( 1 ) ;
}

/******************************************************************************

  make_refFc:
  Given a boundary face on a refined element, create all boundary faces for the
  children.
  
  Last update:
  ------------
  10jun19; unset ->invalid.
  : conceived.
  
  Input:
  ------
  pBf:     the parent's boundary face.
  ppBndFc: a field of new boundary faces
  pnLstFc: with the index of the last face used
  pmBndFc: and the size of the field, [1...mBndFc].

  Changes To:
  -----------
  ppBndFc:                realloc
  ppBndFc[*pnLstFc+1...]: the newly created boundary faces
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int make_refFc ( bndFc_struct *pBf, uns_s *pUns, 
			bndFc_struct **ppBndFc, int *pnLstFc, int *pmBndFc ) {

  const parentFc_struct *pPrtFc = pBf->Pelem->PrefType->prntFc + pBf->nFace ;
  int iChild, kChild ;
  bndFc_struct *PlstFc ;
  
  /* Make sure there are at least MAX_CHILDS_FACE spaces left. */
  if (*pmBndFc - *pnLstFc  < MAX_CHILDS_FACE ) {
    *pmBndFc = *pmBndFc*REALLOC_FACTOR + MAX_CHILDS_FACE ;
    *ppBndFc = arr_realloc ( "ppBndFc in make_refFc",  pUns->pFam, *ppBndFc,
                             *pmBndFc+1, sizeof( **ppBndFc ) ) ;
  }

  for ( iChild = 0 ; iChild < pPrtFc->mChildsFc ; iChild++ ) {
    PlstFc = *ppBndFc + (++(*pnLstFc)) ;
    kChild = pPrtFc->refFc[iChild].kChild ;
    PlstFc->Pelem = pBf->Pelem->PPchild[kChild] ;
    PlstFc->nFace = pPrtFc->refFc[iChild].kChildFc ;
    PlstFc->Pbc = pBf->Pbc ;
    PlstFc->invalid = 0 ;
  }

  return ( 1 ) ;
}



/******************************************************************************

  make_refbuf_bndfc:
  Given a new chunk with elements derived from buffering or refinement, create
  the necessary boundary faces and patches for that chunk.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns: grid
  pNewChunk: chunk to add to, could be either existing or empty.

  Changes To:
  -----------
  pNewChunk[0]
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_refbuf_bndfc ( uns_s *pUns, chunk_struct *pNewChunk ) {

  const int mDim = pUns->mDim ;
  
  int mBndFc, mBndPatch, nBc, nLstFc ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc = NULL, *pBf, *pBfBeg, *pBfEnd ;


  if ( pUns->pRootChunk == pNewChunk && 
       pNewChunk->PbndPatch && pNewChunk->PbndFc ) {
    /* rootChunk has been extended, rather than allocating a separate
       chunk for the refined elements. Add to the faces in the rootChunk. */
    pBndPatch = pNewChunk->PbndPatch ;
    pBndFc = pNewChunk->PbndFc ;
    mBndFc = pNewChunk->mBndFaces ;
    nLstFc = pNewChunk->mBndFacesUsed ;
  }
  else {
    /* Alloc for at most mBc boundary patches, and a bunch, say a 100 boundary
       faces. */
    pBndPatch = arr_malloc ( "pBndPatch in make_refbuf_bndfc", pUns->pFam,
                             pUns->mBc+1, sizeof( *pBndPatch ) ) ;
    // Start with, an estimate of boundary faces. the factor 0.2 stems
    // from a rectangular grid with AR=4 for one side. 
    mBndFc = 0.2*pow( pNewChunk->mVerts, 2./3 ) ;
    mBndFc = MAX( 100, mBndFc ) ; // ah well, at least a hundred.
    nLstFc = 0 ;
    pBndFc = arr_malloc ( "pBndFc in make_refbuf_bndfc", pUns->pFam,
                          mBndFc+1, sizeof( *pBndFc ) ) ;
  }
  int nLstFcOld = nLstFc ;



  /* Loop over all sides and create the child boundary faces.  This
     loop uses un-updated patch information, so will only loop over old
     faces as intended. */
  chunk_struct *pCh = NULL ;
  bndPatch_struct *pBp = NULL ;
  while ( loop_bndFaces ( pUns, &pCh, &pBp, &pBfBeg, &pBfEnd ) ) {
    for ( pBf = pBfBeg ; pBf <= pBfEnd ; pBf++ )
      if ( pBf->nFace  && !pBf->Pelem->leaf && pBf->Pelem->term ) {
        /* This is a boundary face of a buffered element. */
        if ( !make_bufFc ( pBf, pUns, &pBndFc, &nLstFc, &mBndFc ) )
          hip_err ( fatal, 0, 
                    "no buffer face in child in make_refbuf_bndFc.\n" ) ;
      }
      else if ( pBf->nFace  && pBf->Pelem->mark ) {
        /* This is a bnd face of a refined element, refine_uns sets the mark. */
        if ( !make_refFc ( pBf, pUns, &pBndFc, &nLstFc, &mBndFc ) )
          hip_err ( fatal, 0, 
                    "no refined face in child in refbuf_bndFc.\n" ) ;
      }
  }


  /* Realloc boundary faces to the final size. */
  if ( nLstFc - nLstFcOld )
    // There are new faces.
    pBndFc = arr_realloc ( "pBndFc in make_refbuf_bndfc", pUns->pFam, pBndFc,
                           nLstFc+1, sizeof( *pBndFc ) ) ;

  pNewChunk->mBndFaces = nLstFc ;
  pNewChunk->mBndFacesUsed = nLstFc ;
  pNewChunk->PbndFc = pBndFc ;

  /* Sort faces and list patches. */
  make_uns_bndPatch ( pUns ) ;

  return ( 1 ) ;
}


