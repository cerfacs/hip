/*
  buffer_uns_D:
  Buffer 2-D elements.
  
  Last update:
  
  This file contains:
  ---------
   18Dec10; new pBc->type
  count_buf_elems_2D:
  buffer_elems_2D:
  add_child_2D:
  
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const int bitEdge[MAX_EDGES_ELEM] ;
extern const refType_struct bufferType[MAX_FACES_ELEM*MAX_CHILDS_FACE+1] ;

/******************************************************************************

  count_buf_elems_2D:
  Calculate the number of elements, element2vertex pointers and vertices
  needed to buffer a grid in 2D. It sets Pelem->mark to bitmark the refined
  sides such that the boundary faces can be found.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void count_buf_elems_2D ( const uns_s *pUns, int *PmNewElems, int *PmNewElem2VertP ) {
  
  const chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  const vrtx_struct *Pvrtx1, *Pvrtx2 ;
  const elemType_struct *PelT ;
  int nAe, mRefdFaces, kFace, kEdge, kRefdFace[MAX_EDGES_FACE], dir ;
  const llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    /* Loop over all cells. Find the refined faces and assume the worst
       case of adding an interior vertex to all of them. */
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf ) {
        PelT = elemType + Pelem->elType ;
	Pelem->markdEdges = mRefdFaces = 0 ;
	/* Find all faces of this element. */
	for ( kFace = 1 ; kFace <= PelT->mSides ; kFace++ ) {
          kEdge = PelT->faceOfElem[kFace].kFcEdge[0] ;
          nAe = get_elem_edge ( pllAdEdge, Pelem, kEdge, &Pvrtx1, &Pvrtx2, &dir ) ;
	  if ( nAe && pUns->pAdEdge[nAe].cpVxMid.nr ) {
            /* This is a hanging face from the coarse side. */
	    kRefdFace[mRefdFaces] = kFace ;
	    mRefdFaces++ ;
	    Pelem->markdEdges |= bitEdge[kFace] ;
	  }
	}

	if ( PelT->elType == tri && mRefdFaces ) {
          /* Only tris as buffer elems. */
	  *PmNewElems += mRefdFaces+1 ;
	  *PmNewElem2VertP += ( mRefdFaces+1 )*3 ;
	}
	else if ( PelT->elType == qua && mRefdFaces ) {
          /* Quad. */
	  if ( mRefdFaces == 4 ) {
            /* 4 tris in the corners and one large quad in the center. */
	    *PmNewElems += 5 ;
	    *PmNewElem2VertP += 4*3 + 1*4 ;
	  }
	  else if ( mRefdFaces == 3 ) {
            /* 3 tris and one quad. */
	    *PmNewElems += 4 ;
	    *PmNewElem2VertP += 3*3 + 1*4 ;
	  }
	  else if ( mRefdFaces == 1 ) {
            /* 3 tris. */
	    *PmNewElems += 3 ;
	    *PmNewElem2VertP += 3*3 ;
	  }
	  else if ( kRefdFace[1] - kRefdFace[0] == 2 ) {
            /* Two opposite faces are hanging, two quads. */
	    *PmNewElems += 2 ;
	    *PmNewElem2VertP += 2*4 ;
	  }
	  else if ( kRefdFace[1] - kRefdFace[0] == 1 ||
		    kRefdFace[1] - kRefdFace[0] == 3 ) {
            /* Two adjacent faces are hanging, four tris. */
	    *PmNewElems += 4 ;
	    *PmNewElem2VertP += 4*3 ;
	  }
	  else
	    printf ( " STRANGE: can't deal with this hanging pattern in"
		     " count_buf_elems_2D.\n" ) ;
	}
      }

  /* Ok this is really nasty. Fix this later. */
  *PmNewElems *= 1.200 ;
  *PmNewElem2VertP *= 1.200 ;


  return ;
}
/******************************************************************************

  buffer_2D_elem:
  Buffer a 2D element. This function finds the hanging edges of the element
  again. Switch it to surfTri, some time.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int buffer_2D_elem ( elem_struct *Pelem, const uns_s *pUns,
		     childSpc_s *PchildSpc )
{
  const elemType_struct *PelT = elemType + Pelem->elType ;
  const llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  const vrtx_struct *Pvrtx1, *Pvrtx2 ;
  vrtx_struct *PvxPrt[2*MAX_VX_ELEM], *PvxSide[MAX_EDGES_FACE+1] ;
  int nAe, kRefdSide[MAX_EDGES_FACE], kSide, kEdge, kHgVx, kNonVx,
    mRefdSides, dir ;
  cpt_s cpVx ;

  mRefdSides = 0 ;
  /* Find all edges of this element. */
  for ( kSide = 1 ; kSide <= PelT->mSides ; kSide++ ) {
    kEdge = PelT->faceOfElem[kSide].kFcEdge[0] ;
    nAe = get_elem_edge ( pllAdEdge, Pelem, kEdge, &Pvrtx1, &Pvrtx2, &dir ) ;
    cpVx = pUns->pAdEdge[nAe].cpVxMid ;
    if ( nAe && cpVx.nr ) {
      /* This is a hanging edge from the coarse side. */
      kRefdSide[mRefdSides] = kSide ;
      mRefdSides++ ;
      PvxSide[kSide] = de_cptVx( pUns, cpVx ) ;
    }
    else
      PvxSide[kSide] = NULL ;
  }

  if ( !mRefdSides )
    /* Nothing to be done. */
    return ( 1 ) ;

  else if ( PelT->elType == tri && mRefdSides ) {
    /* This element needs buffering. In case there is an old invalid
       PPchild, remove it. */
    Pelem->PPchild = NULL ;
    
    /* There are hanging edges. A list of vertices around the element. */
    for ( kSide = 1 ; kSide <= PelT->mSides ; kSide++ )
    { PvxPrt[2*kSide-2] = Pelem->PPvrtx[kSide-1] ;
      /* In triangles, the side 1 is opposite node 0. */
      PvxPrt[ (2*kSide+1)%6 ] = PvxSide[kSide] ;
    }
    
    if ( mRefdSides == 1 )
    { /* Two tris. Find the position of the hanging vertex.*/
      kHgVx = (2*kRefdSide[0]+1)%6 ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		     kHgVx, kHgVx+3, kHgVx+5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		     kHgVx, kHgVx+1, kHgVx+3, 0 ) ;
      Pelem->PrefType = bufferType + 2 ;
    }
    else if ( mRefdSides == 2 )
    { /* Three tris. Find the position of the unrefined vertex.*/
      for ( kNonVx = 1 ; kNonVx <=5 ; kNonVx += 2 )
	if ( !PvxPrt[kNonVx] )
	  break ;
      
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		     kNonVx+5, kNonVx+1, kNonVx+2, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		     kNonVx+5, kNonVx+2, kNonVx+4, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		     kNonVx+4, kNonVx+2, kNonVx+3, 0 ) ;
      Pelem->PrefType = bufferType + 3 ;
    }
    else if ( mRefdSides == 3 )
    {				/* Four tris. */
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 0, 1, 5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 5, 1, 3, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 1, 2, 3, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 5, 3, 4, 0 ) ;
      Pelem->PrefType = bufferType + 4 ;
    }
    else
    { printf ( " FATAL: can't deal with this hung tri in"
	      " buffer_elems_2D.\n" ) ;
      return ( 0 ) ;
    }
  }

  else if ( PelT->elType == qua )
  { /* This element needs buffering. In case there is an old invalid
       PPchild, remove it. */
    Pelem->PPchild = NULL ;
    
    /* There are hanging edges. A list of vertices around the element. */
    for ( kSide = 1 ; kSide <= PelT->mSides ; kSide++ )
    { PvxPrt[2*kSide-2] = Pelem->PPvrtx[kSide-1] ;
      /* In Quads, side 1 is ccw after node 0. */
      PvxPrt[2*kSide-1] = PvxSide[kSide] ;
    }
    
    if ( mRefdSides == 1 )
    { /* 3 tris. Find the position of the hanging vertex.*/
      kHgVx = 2*kRefdSide[0]-1 ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx+7, kHgVx, kHgVx+5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx, kHgVx+1, kHgVx+3, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx+5, kHgVx, kHgVx+3, 0 ) ;
      Pelem->PrefType = bufferType + 3 ;
    }
    else if ( mRefdSides == 3 )
    { /* 3 tris and one quad. Find the position of the unrefined vertex.*/
      for ( kNonVx = 1 ; kNonVx <=7 ; kNonVx += 2 )
	if ( !PvxPrt[kNonVx] )
	  break ;
	    
      add_child_2D ( PchildSpc, Pelem, qua, PvxPrt,
		    kNonVx+7, kNonVx+1, kNonVx+2, kNonVx+6 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kNonVx+6, kNonVx+2, kNonVx+4, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kNonVx+6, kNonVx+4, kNonVx+5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kNonVx+4, kNonVx+2, kNonVx+3, 0 ) ;
      Pelem->PrefType = bufferType + 4 ;
    }
    else if ( mRefdSides == 4 )
    { /* 4 tris in the corners and one large quad in the center. Number the
	 children with first the tris in the sequence of their corner vertices,
	 at the end the quad.*/
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 0, 1, 7, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 2, 3, 1, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 4, 5, 3, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt, 6, 7, 5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, qua, PvxPrt, 1, 3, 5, 7 ) ;
      Pelem->PrefType = bufferType + 5 ;
    }
    else if ( kRefdSide[1] - kRefdSide[0] == 2 )
    { /* Two opposite edges are hanging, two quads. */
      kHgVx = 2*kRefdSide[0]-1 ;
      add_child_2D ( PchildSpc, Pelem, qua, PvxPrt,
		    kHgVx+0, kHgVx+4, kHgVx+5, kHgVx+7 ) ;
      add_child_2D ( PchildSpc, Pelem, qua, PvxPrt,
		    kHgVx+0, kHgVx+1, kHgVx+3, kHgVx+4 ) ;
      Pelem->PrefType = bufferType + 2 ;
    }
    else if ( kRefdSide[1] - kRefdSide[0] == 1 ||
	     kRefdSide[1] - kRefdSide[0] == 3 )
    { /* Two adjacent edges are hanging, four tris. */
      if ( kRefdSide[1] - kRefdSide[0] == 1 )
	kHgVx = 2*kRefdSide[0]-1 ;
      else
	kHgVx = 2*kRefdSide[1]-1 ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx+7, kHgVx+0, kHgVx+5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx+0, kHgVx+1, kHgVx+2, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx+0, kHgVx+2, kHgVx+5, 0 ) ;
      add_child_2D ( PchildSpc, Pelem, tri, PvxPrt,
		    kHgVx+5, kHgVx+2, kHgVx+3, 0 ) ;
      Pelem->PrefType = bufferType + 4 ;
    }
    else
    { printf ( " FATAL: can't deal with this hung quad in"
	      " buffer_elems_2D.\n" ) ;
      return ( 0 ) ;
    }
  }

  Pelem->leaf = 0 ;
  Pelem->number = 0 ;
  return ( 1 ) ;
}
/******************************************************************************

  add_child_2D:
  Add a child to a parent element. Write the child and the pointer to the child
  at the next incremented location of the given pointers.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  PPlstElem:   The last element written in the the new chunk.
  PPPlstVx:    The last element to vertex pointer written.
  PPPlstChild: The last element to child pointer written.

  Pelem:       The parent element.
  chType:      The type of the child.
  PvxPrt:      The three forming and hanging vertices of the parent.
  kVxPrt[0..4]:The position of the forming vertices of the child in the parent.

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_child_2D ( childSpc_s *PchildSpc,
		   elem_struct *Pelem, elType_e chType, vrtx_struct *PvxPrt[],
		   int kVxPrt0, int kVxPrt1, int kVxPrt2, int kVxPrt3 ) {

  elem_struct *Pchild = ++(PchildSpc->PlstElem) ;
  int mAllVxPrt ;
  
  *(++(PchildSpc->PPlstChild)) = Pchild ;
  if ( !Pelem->PPchild )
    /* This is the first child for Pelem. */
    Pelem->PPchild = PchildSpc->PPlstChild ;

# ifdef CHECK_BOUNDS
    if ( Pchild > PchildSpc->Pelem_max ||
	 PchildSpc->PPlstChild > PchildSpc->PPchild_max ||
	 PchildSpc->PPlstVx + elemType[chType].mVerts > PchildSpc->PPvrtx_max )
    { printf ( " FATAL: space for children exhausted in add_child_2D.\n" ) ;
      return ( 0 ) ;
    }
# endif
  
  /* The number of all vertex position around the fully refined parent. */
  if ( Pelem->elType == tri )
    mAllVxPrt = 6 ;
  else if ( Pelem->elType == qua )
    mAllVxPrt = 8 ;
  else
  { printf ( " FATAL: can't deal with parent of type %d in add_child_elem.\n",
	     Pelem->elType ) ;
    return ( 0 ) ;
  }
  
  /* Validate the child. */
  Pchild->number = Pchild-PchildSpc->PnewChunk->Pelem +
	           PchildSpc->pUns->nHighestElemNumber ;
  Pchild->elType = chType ;
  Pchild->PPchild = NULL ;
  Pchild->leaf = 1 ;
  /* The terminal for a buffered branch is the parent of the buffer cell. */
  Pchild->term = 0 ;
  Pchild->Pparent = Pelem ;
  Pchild->root = Pchild->invalid = 0 ;

  /* List its vertices. */
  Pchild->PPvrtx = (PchildSpc->PPlstVx)+1 ;
  Pchild->PPvrtx[0] = PvxPrt[ kVxPrt0%mAllVxPrt ] ;
  Pchild->PPvrtx[1] = PvxPrt[ kVxPrt1%mAllVxPrt ] ;
  Pchild->PPvrtx[2] = PvxPrt[ kVxPrt2%mAllVxPrt ] ;

  if ( chType == tri )
    PchildSpc->PPlstVx += 3 ;
  else if ( chType == qua ) {
    Pchild->PPvrtx[3] = PvxPrt[ kVxPrt3%mAllVxPrt ] ;
    PchildSpc->PPlstVx += 4 ;
  }
  else {
    printf ( " FATAL: can't deal with child of type %d in add_child_elem.\n",
	     chType ) ;
    return ( 0 ) ;
  }

# ifdef CHECK_BOUNDS
  {
    int kVx ;
    /* Check whether the vertex pointers of the child are valid. */
    for ( kVx = 0 ; kVx < elemType[chType].mVerts ; kVx++ )
      if ( !Pchild->PPvrtx[kVx] )
      { printf ( " FATAL: invalid vertex pointer in add_child_2D.\n" ) ;
	return ( 0 ) ;
      }
  }
# endif

  return ( 1 ) ;
}

