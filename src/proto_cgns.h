/*
  proto_cgns.h:
*/

/*! prototypes for cgns utilities.
 *
 */


/* 
  Last update:
  ------------
  20mar18; conceived

  
  
  This file contains:
  -------------------
 

*/


/* hcg_util.h */
int hcg_bcTypeDecode ( BCType_t cg_BCType, char *bcType ) ;
int hcg_open ( const char *fileName, int mode ) ;

