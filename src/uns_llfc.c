/*
   uns_llfc.c:
   The list of faces of a grid.

   Last update:
   ------------
   14Mar12; remove internal/thin boundary faces if doRemove.intFc is set.
   8Nov10; fix bug in match_llFc arisen from bad nesting when switching to hip_err.
   3Jul10; handle all FATAL errors with hip_err.
   1Apr05; make make_llFc static.
   19Jul99; intro bndFcVx, match_bndFcVx.

   This file contains:
   -------------------
   add_elem2fc:
   add_bnd2fc:
   bcNrCompare:
   match_bndFcVx:
   make_llFc:
   check_conn:

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

#include "cpre_adapt.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;


extern const elemType_struct elemType[] ;
extern const doFc_struct doWarn, doRemove ;


/* face_u, what is on a side of a face. */
typedef enum { noFace, el, bndFace } faceType_e ;
typedef union {
  struct {
    elem_struct *pElem ;                 /* Pointer to the interior element. */
    faceType_e  type:3 ;                   /* one of: noFc, elem, bndFc*/
    unsigned int drvFc:1 ;               /* Is this face derived? */
    unsigned int kFacet:4 ;              /* And if yes, which facet on the face? */
    unsigned int kFace:4 ;               /* Number of the face in the int. elem. */
  } elem ;

  struct {
    bndFc_struct *pBndFc ;
    faceType_e  type:3 ;                   /* one of: noFc, elem, bndFc*/
    unsigned int drvFc:1 ;               /* Is this face derived? */
    unsigned int kFacet:4 ;              /* And if yes, which facet on the face? */
  } bndFc ;
} face_u ;

/* fc2el_s: Each face has two sides.*/
struct _fc2el_s {
  face_u side[2] ;
} ;


/* This macro conveniently returns the proper vertex pointer of a derived element
   out of PPvrtx or PhgVx. */ 
#define CPVX(kV,mV,pEl,pHgV) ( const vrtx_struct * ) \
                             ( kV < mV ? pEl->PPvrtx[kV] : pHgV[kV-mV] )


/******************************************************************************
  show_fc2el:   */

/*! public interface to extract the content of fc2el for int. faces between 2 elements
 */

/*
  
  Last update:
  ------------
  1Apr12: conceived.
  

  Input:
  ------
  pfc2el: pointer to the face

  Output:
  -------
  ppElem0: pointer to element 0
  pkFc0: canonical face number in element 0
  ppElem1: pointer to element 1
  pkFc1: canonical face number in element 1
    
  Returns:
  --------
  0 on success, 1 if face one is not element, 2 if face 2 is not element, 3 if neitheris.
  
*/

int show_fc2el_elel ( const fc2el_s *pfc2el, const int nFc, 
                      elem_struct **ppElem0, int *pkFc0, 
                      elem_struct **ppElem1, int *pkFc1 ) {
  int retVal = 0 ;

  const fc2el_s *pFc = pfc2el + nFc ;

  if ( pFc->side[0].elem.type ) {
    *ppElem0 = pFc->side[0].elem.pElem ;
    *pkFc0   = pFc->side[0].elem.kFace ;
  }
  else {
    /* This side is either not filled, or not an element. */
    *ppElem0 = NULL ;
    retVal += 1 ;
  }

  if ( pFc->side[1].elem.type ) {
    *ppElem1 = pFc->side[1].elem.pElem ;
    *pkFc1   = pFc->side[1].elem.kFace ;
  }
  else {
    /* This side is either not filled, or not an element. */
    *ppElem1 = NULL ;
    retVal += 2 ;
  }


  return ( retVal ) ;
}

/******************************************************************************
  llFc_list_faces_elem:   */

/*! list all the faces referencing a given element number.
 *
 */

/*
  
  Last update:
  ------------
  1Sep12: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int llFc_list_faces_elem ( int mFlFc, flFc_s *pFace, const int nEl ) {

  sprintf ( hip_msg, "missing faces for element %d, found only", nEl ) ;
  hip_err ( warning, 1, hip_msg ) ;

  int nFc, kVx, iSide ;
  flFc_s *pFc ;
  for ( nFc = 1 ; nFc <= mFlFc ; nFc++ )
    for ( pFc = pFace + nFc, iSide = 0 ; iSide < 2 ; iSide++ ) { 
      if ( nEl == pFc->nEl[iSide] ) {
        printf ( "        face %d, ", nFc ) ;
        for ( kVx = 0 ; kVx < pFc->mVxFc ; kVx++ )
          printf ( " %"FMT_ULG",", pFc->nVx[kVx] ) ;
        printf ( " side %d\n", iSide ) ;
      }
    }

  return ( 0 ) ;
}


/******************************************************************************

  add_elem2fc:
  Given a fc2el and a side, add an element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void add_elem2fc ( fc2el_s *pFc, int side,
                          elem_struct *pElem, int kFace, int kFct, int drvFc ) {
  
  side = ( side ? 1 : 0 ) ;
  
  pFc->side[side].elem.type = el   ;
  pFc->side[side].elem.pElem = pElem ;
  pFc->side[side].elem.kFace = kFace ;
  if ( drvFc ) {
    /* Derived face. */
    pFc->side[side].elem.drvFc = 1 ;
    pFc->side[side].elem.kFacet = kFct ;
  }
  else
    /* Base element. */
    pFc->side[side].elem.drvFc = 0 ;

  return ;
}

/******************************************************************************

  add_bnd2fc:
  Given a fc2el and a side, add an element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void add_bnd2fc ( fc2el_s *pFc, bndFc_struct *pBndFc, int kFct,
                         int drvFc ) {
  
  pFc->side[1].elem.type = bndFace ;
  pFc->side[1].bndFc.pBndFc = pBndFc ;
  if ( drvFc ) {
    /* Derived face. */
    pFc->side[1].elem.drvFc = 1 ;
    pFc->side[1].elem.kFacet = kFct ;
  }
  else
    /* Base element. */
    pFc->side[1].elem.drvFc = 0 ;

  return ;
}


/******************************************************************************

  vxNrCompare:
  Comparison function for qsort.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int bndFcVx_bcNrCompare ( const void *pBf1, const void *pBf2 ) {
  return ( ( ( bndFcVx_s * ) pBf1 )->pBc->nr - ( ( bndFcVx_s * ) pBf2 )->pBc->nr ) ;
}


/******************************************************************************

  elem2vx_from_fc:
  assemble element to vx pointers from a list of fc2vx and fc2elem pointers.
  
  Last update:
  ------------
  10Nov16; change warnings for face orientation to infos.
  15Dec11; reduce volume of warnings on wrong-handedness, pass mFcWrongHand by arg.
  9Jul11; bug-fixes, check for face orientation and use init_elem.
  27Jun07; extracted from read_uns_fluent.
  
  Input:
  ------
  mFlFc  = number of faces
  pFace  = list of faces, starting with the element numbered 1
  mElems = number of elements
  pElem  = list of elements, starting from 1
  pVrtx  = base pointer to the linear list of vertices indexed by pFace->nVx

  Changes To:
  -----------
  pElem->PPvrtx
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int elem2vx_from_fc ( int mFlFc, flFc_s *pFace, int mElems, elem_struct *pElem, 
                      vrtx_struct *pVrtx ) {

  const elemType_struct *pElT ;
  const int mDim = (elemType + pElem[1].elType)->mDim ;

  int nFc, nEl, kVx, nVxFc[MAX_FACES_ELEM][MAX_VX_FACE], iSide, kFc, kkVx, kFc4, nVx, 
    nVx2, found ;
  elem_struct *pEl ;
  flFc_s *pFc, *pNxtFc, *pElFc[MAX_FACES_ELEM] ;

  /* Make a linked list of faces for each element. Use the element number as entry. */
  for ( nFc = 1 ; nFc <= mFlFc ; nFc++ )
    for ( pFc = pFace + nFc, iSide = 0 ; iSide < 2 ; iSide++ ) { 
      nEl = pFc->nEl[iSide] ;

      if ( nEl ) {
        /* Physical cell. */
        pEl = pElem + nEl ;

        if ( !pEl->number ) {
          /* New Root. */
          pEl->number = nFc ;
        }
        else {
          /* There is a root face with this element. Follow the list to the end. */
          for ( pNxtFc = pFace + pEl->number ; 1 ; )
            /* Which side of the face are we on with pEl? */
            if ( pNxtFc->nEl[0] == nEl ) {
              if ( pNxtFc->pNxtFc[0] )
                pNxtFc = pNxtFc->pNxtFc[0] ;
              else {
                /* Append. */
                pNxtFc->pNxtFc[0] = pFace+nFc ;
                break ;
              }
            }
            else {
              if ( pNxtFc->pNxtFc[1] )
                pNxtFc = pNxtFc->pNxtFc[1] ;
              else {
                /* Append. */
                pNxtFc->pNxtFc[1] = pFace+nFc ;
                break ;
              }
            }
        }
      }
    }


  
  /*****
    Collect the elements from their faces.
    *****/
  int mFcWrongHand = 0 ;
  for ( nEl = 1 ; nEl <= mElems ; nEl++ ) {
    pEl = pElem + nEl ;
    pElT = elemType + pEl->elType ;


    /* Make a local unordered list of faces. */
    for ( pNxtFc = pFace + pEl->number, kFc = 0 ; kFc < pElT->mSides ; kFc++ ) {
      if ( !pNxtFc ) {
        if ( verbosity > 1 ) llFc_list_faces_elem ( mFlFc, pFace, nEl ) ;
        sprintf ( hip_msg, "not enough faces for ele %d in elem2vx_from_fc.", nEl ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else if ( pNxtFc->nEl[0] == nEl ) {
        pElFc[kFc] = pNxtFc ;
        /* Make a properly right-hand-rule ordered list of vertices. */
        for ( kVx = 0 ; kVx < pNxtFc->mVxFc ; kVx++ )
          nVxFc[kFc][kVx] = pNxtFc->nVx[kVx] ;
        pNxtFc = pNxtFc->pNxtFc[0] ;
      }
      else if ( pNxtFc->nEl[1] == nEl ) {
        pElFc[kFc] = pNxtFc ;
        /* Make a properly right-hand-rule ordered list of vertices. The element
           is to the left of the face. */
        for ( kVx = 0 ; kVx < pNxtFc->mVxFc ; kVx++ )
          nVxFc[kFc][ pNxtFc->mVxFc-kVx-1 ] = pNxtFc->nVx[kVx] ;
        pNxtFc = pNxtFc->pNxtFc[1] ;
      }
      else {
        sprintf ( hip_msg, "this shouldn't have happened, inconsistent linked list in elem2vx_from_fc." ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    if ( pNxtFc ) {
      sprintf ( hip_msg, "too many faces for element %d in elem2vx_from_fc.", nEl ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }





    /* Check for correct orientation of the faces, require that the
       face normal points away from the gravity centre. 
       Quads/Hexes bent into a chevron could violate that, so let's
       only issue a warning here. */
    double gcEl[MAX_DIM], gcFc[MAX_FACES_ELEM][MAX_DIM] ;
    vec_ini_dbl ( 0., MAX_DIM, gcEl ) ;
    int mVxFc ;
    int mVxEl = 0 ;
    const double *pCo, *pCoVxFc[MAX_FACES_ELEM][MAX_VX_FACE] ;
    for ( kFc = 1; kFc < pElT->mSides ; kFc++ ) {
      mVxFc = pElFc[kFc]->mVxFc ;
      vec_ini_dbl ( 0., MAX_DIM, gcFc[kFc] ) ;

      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
        mVxEl++ ;
        pCoVxFc[kFc][kVx] = pCo = pVrtx[ nVxFc[kFc][kVx] ].Pcoor ;
        vec_add_dbl ( gcEl, pCo, mDim, gcEl ) ;
        vec_add_dbl ( gcFc[kFc], pCo, mDim, gcFc[kFc] ) ;
      }
      vec_mult_dbl ( gcFc[kFc], 1./mVxFc, mDim ) ;
    }
    vec_mult_dbl ( gcEl, 1./mVxEl, mDim ) ;
    
    double fcNrm[MAX_DIM] ;
    double fcVec[MAX_DIM] ;
    double scProd ;
    int mTmsNrm ;
    for ( kFc = 1 ; kFc < pElT->mSides ; kFc++ ) {
      uns_face_normal_co ( mDim,  pElFc[kFc]->mVxFc, pCoVxFc[kFc], 
                           fcNrm, &mTmsNrm ) ;   
      /* Vector from el grav ctr to face grav ctr. */
      vec_diff_dbl ( gcFc[kFc], gcEl, mDim, fcVec ) ;
      scProd = scal_prod_dbl ( fcVec, fcNrm, mDim ) ;

      if ( scProd < 0. ) {
        mFcWrongHand++ ;
        if ( verbosity >= 4 ) {
          sprintf ( hip_msg, "face %td has wrong handedness for element %d, reversed.", 
                    pElFc[kFc] - pFace, nEl ) ;
          hip_err ( info, 4, hip_msg ) ;
        } 
      }
    }

    if ( pElT->mDim == 2 ) {
      /* 2D should be easy. Just concatenate. */
      pEl->PPvrtx[0] = pVrtx + nVxFc[0][0] ;
      pEl->PPvrtx[1] = pVrtx + nVxFc[0][1] ;

      if ( pEl->elType == tri ) {
        if ( nVxFc[1][0] == nVxFc[0][1] )
          pEl->PPvrtx[2] = pVrtx + nVxFc[1][1] ;
        else
          pEl->PPvrtx[2] = pVrtx + nVxFc[2][1] ;
      }
      else {
        /* Find the edge that is opposite. */
        for ( kFc = 1 ; kFc < pElT->mSides ; kFc++ )
          if ( nVxFc[0][0] != nVxFc[kFc][1] &&
               nVxFc[0][1] != nVxFc[kFc][0] ) {
            pEl->PPvrtx[2] = pVrtx + nVxFc[kFc][0] ;
            pEl->PPvrtx[3] = pVrtx + nVxFc[kFc][1] ;
          }
      }
    }

    else if ( pEl->elType == tet ) {
      /* Pick a face. */
      pEl->PPvrtx[0] = pVrtx + nVxFc[0][0] ;
      pEl->PPvrtx[1] = pVrtx + nVxFc[0][2] ;
      pEl->PPvrtx[2] = pVrtx + nVxFc[0][1] ;
      /* Find the non-listed vertex from the next face. */
      for ( kVx = 0 ; kVx < 3 ; kVx++ ) {
        nVx = nVxFc[1][kVx] ;
        for ( kkVx = 0 ; kkVx < 3 ; kkVx++ )
          if ( nVxFc[0][kkVx] == nVx )
            break ;
        if ( kkVx == 3 )
          pEl->PPvrtx[3] = pVrtx + nVx ;
      }
    }

    else if ( pEl->elType == pyr ) {
      /* Take the quad face. */
      for ( kFc = 0 ; kFc < pElT->mSides ; kFc++ )
        if ( pElFc[kFc]->mVxFc == 4 ) {
          pEl->PPvrtx[0] = pVrtx + nVxFc[kFc][0] ;
          pEl->PPvrtx[1] = pVrtx + nVxFc[kFc][1] ;
          pEl->PPvrtx[2] = pVrtx + nVxFc[kFc][2] ;
          pEl->PPvrtx[3] = pVrtx + nVxFc[kFc][3] ;
          break ;
        }
      
      /* Find the non-listed vertex from any other face, all tris. */
      nFc = ( kFc == 0 ? 1 : 0 ) ;
      for ( kVx = 0 ; kVx < 3 ; kVx++ ) {
        nVx = nVxFc[nFc][kVx] ;
        for ( kkVx = 0 ; kkVx < 4 ; kkVx++ )
          if ( nVxFc[kFc][kkVx] == nVx )
            break ;
        if ( kkVx == 4 )
          pEl->PPvrtx[4] = pVrtx + nVx ;
      }
    }

    else if ( pEl->elType == pri ) {
      /* Take the first quad face as 3, ie. 0321. */
      for ( kFc4 = 0 ; kFc4 < pElT->mSides ; kFc4++ )
        if ( pElFc[kFc4]->mVxFc == 4 ) {
          pEl->PPvrtx[0] = pVrtx + nVxFc[kFc4][0] ;
          pEl->PPvrtx[3] = pVrtx + nVxFc[kFc4][3] ;
          pEl->PPvrtx[2] = pVrtx + nVxFc[kFc4][2] ;
          pEl->PPvrtx[1] = pVrtx + nVxFc[kFc4][1] ;
          break ;
        }

      for ( found = 0 ; found < 2 ; found++ ) {
        /* Find the quad face adjacent at kVx=3,2 wrt the face in the element */
        nVx  = pEl->PPvrtx[3] - pVrtx ; 
        nVx2 = pEl->PPvrtx[2] - pVrtx ; 
        for ( kFc = kFc4+1 ; kFc < pElT->mSides ; kFc++ )
          if ( pElFc[kFc]->mVxFc == 4 ) {
            for ( kVx = 0 ; kVx < 4 ; kVx++ )
              if ( nVxFc[kFc][  kVx      ] == nVx &&
                   nVxFc[kFc][ (kVx+1)%4 ] == nVx2 ) {
                /* Match. */
                found = 1 ;
                pEl->PPvrtx[4] = pVrtx + nVxFc[kFc][ (kVx+2)%4 ] ;
                pEl->PPvrtx[5] = pVrtx + nVxFc[kFc][ (kVx+3)%4 ] ;
                break ;
              }
          }

        if ( !found ) {
          /* No match found. nVx and nVx2 are adjacent to a tri face. Rotate the
             quad face by one vertex. */
          pEl->PPvrtx[4] = pEl->PPvrtx[3] ;
          pEl->PPvrtx[3] = pEl->PPvrtx[2] ;
          pEl->PPvrtx[2] = pEl->PPvrtx[1] ;
          pEl->PPvrtx[1] = pEl->PPvrtx[0] ;
          pEl->PPvrtx[0] = pEl->PPvrtx[4] ;
          pEl->PPvrtx[4] = NULL ;
        }
      }
    }

    


    else if ( pEl->elType == hex ) {
      /* Take the first face as 5, ie. 0321. */
      pEl->PPvrtx[0] = pVrtx + nVxFc[0][0] ;
      pEl->PPvrtx[1] = pVrtx + nVxFc[0][1] ;
      pEl->PPvrtx[2] = pVrtx + nVxFc[0][2] ;
      pEl->PPvrtx[3] = pVrtx + nVxFc[0][3] ;
      
      /* Find the quad face adjacent at kVx=3,2. */
      nVx  = nVxFc[0][3] ;
      nVx2 = nVxFc[0][2] ;
      for ( kFc = 1, found = 0 ; kFc < pElT->mSides && !found ; kFc++ )
        if ( pElFc[kFc]->mVxFc == 4 ) {
          for ( kVx = 0 ; kVx < 4 ; kVx++ )
            if ( nVxFc[kFc][kVx] == nVx &&
                 nVxFc[kFc][ (kVx+1)%4 ] == nVx2 ) {
              /* Match. */
              nVx  = nVxFc[kFc][ (kVx+2)%4 ] ;
              nVx2 = nVxFc[kFc][ (kVx+3)%4 ] ;
              pEl->PPvrtx[6] = pVrtx + nVx ;
              pEl->PPvrtx[7] = pVrtx + nVx2 ;
              found = 1 ;
              break ;
            }
        }
      
      /* Find the quad face adjacent at kVx=6,7. */
      for ( kFc = 1 ; kFc < pElT->mSides ; kFc++ )
        if ( pElFc[kFc]->mVxFc == 4 ) {
          for ( kVx = 0 ; kVx < 4 ; kVx++ )
            if ( nVxFc[kFc][kVx] == nVx2 &&
                 nVxFc[kFc][ (kVx+1)%4 ] == nVx ) {
              /* Match. */
              pEl->PPvrtx[5] = pVrtx + nVxFc[kFc][ (kVx+2)%4 ] ;
              pEl->PPvrtx[4] = pVrtx + nVxFc[kFc][ (kVx+3)%4 ] ;
              break ;
            }
        }
    }

    /* Validate the element for empty vertex pointers. */
    for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
      if ( !pEl->PPvrtx[kVx] ) {
        sprintf ( hip_msg, "could not reconstruct el. %d in elem2vx_from_fc.", nEl ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
        
  }

  if ( mFcWrongHand ) {
    sprintf ( hip_msg, "found %d faces with wrong handedness in elem2vx_from_fc, fixed.", 
              mFcWrongHand ) ;
    hip_err ( info, 1, hip_msg ) ;
  }



  return ( 1 ) ;

}


/******************************************************************************
  fc_is_collapsed:   */

/*! Given nodes in sequence around a face, check whether it is collapsed.
 *
 * more detailed desc for doxygen
 */

/*
  
  Last update:
  ------------
  25Feb18; extend size of pVxFc.
  18Dec11: conceived.

  Input:
  ------
  mVxFc: number of verts around the face
  ppVxFc: list of vertices around the face.
    
  Returns:
  --------
  0 if collapsed, number of remaining nodes otherwise
  
*/

int fc_not_collapsed ( int mVxFc, vrtx_struct **ppVxFc ) {

  /* extend vertex list cyclically */
  vrtx_struct *pVxFc[MAX_VX_FACE+2] ;
  if ( mVxFc > MAX_VX_FACE ) {
    hip_err ( fatal, 0, "too many vertices on a face in fc_not_collapsed." ) ;
    return (0) ;
  }
  memcpy ( pVxFc, ppVxFc, mVxFc*sizeof( vrtx_struct* ) ) ;
  pVxFc[mVxFc] = ppVxFc[0] ;

  int k, kk ;
  for ( k = 0 ; k < mVxFc ; k++ ) {
    if ( pVxFc[k] == pVxFc[k+1] ) {
      if ( mVxFc == 3 )
        /* That's it, nothing left. */
        return ( 0 ) ;
      else {
        /* Copy all remaining one down. */
        /* JDM, Feb18: that should be kk < mVxFc-1? */
        for ( kk = k+1 ; kk < mVxFc ; kk++ )
          pVxFc[kk] = pVxFc[kk+1] ;
        /* Check this one against the new next one. */
        mVxFc-- ;
        k-- ;
      }
    }
  }

  if ( mVxFc == 4 ) {
    /* Check collapse across the diagonal of a quad. */
    if ( pVxFc[0] == pVxFc[2] || pVxFc[1] == pVxFc[3] )
      return ( 0 ) ;
  }

  /* Copy modified list back. */
  memcpy ( ppVxFc, pVxFc, mVxFc*sizeof( vrtx_struct* ) ) ;

  return ( mVxFc ) ;
}






/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  llFc_add_elem_pvxfct:
*/
/*! add one face of an element to the list.
 *
 */

/*
  
  Last update:
  ------------
  1May20: extracted from llFc_add_facets.
  

  Input:
  ------
  pElem: elem that forms this face
  kFace: face of this elem that the facets are on

  Output:
  -------
    
  Returns:
  --------
  number of the offending entity on failure, 0 on success
  
*/

int llFc_add_elem_pvxfct ( fc2el_s **ppfc2el, llVxEnt_s *pllVxFc, 
                            elem_struct *pElem, int kFace,
                           int mVxFct, vrtx_struct *pVxFct[MAX_VX_FACE],
                           int drvFc, int kFct, bndFc_struct *pBndFc,
                           const int use_side1 ) {
#define FUNLOC "in llFc_add_elem_pvxfct"

  int nEnt ;
  int kMin ;
  fc2el_s *pFc ;
  int kVx ;
  int mC ;
  if ( ( nEnt =
         get_ent_vrtx ( pllVxFc, mVxFct,
                        ( const vrtx_struct **) pVxFct, &kMin ) ) ) {
    /* The face exists. Fill the other side. */
    pFc = (*ppfc2el)+nEnt ;
    if ( pFc->side[1].elem.type ) {
      /* Side 1 already filled, above my paygrade, escalate. */
      return ( nEnt ) ;
    }
    else if ( pBndFc ) {
      // Add a boundary face. 
      add_bnd2fc ( pFc, pBndFc, kFct, drvFc ) ;
      return ( 0 ) ;
    }
    else if ( use_side1 ) {
      // Add pElem to the other side.
      add_elem2fc ( pFc, 1, pElem, kFace, kFct, drvFc ) ;
      return ( 0 ) ;
    }
    else
      // use_side1 == 0, not allowed to add to empty side 1.
      return ( nEnt ) ;
  }

  /* Add the face. */
  else if ( ( nEnt =
              add_ent_vrtx ( pllVxFc, mVxFct, ( const vrtx_struct **) pVxFct, 
                             &kMin ) ) ) {
    pFc = (*ppfc2el)+nEnt ;
    if ( pBndFc ) {
      // Add a boundary face.
      // JDM: Does this work without having the element on side 0? 
      add_bnd2fc ( pFc, pBndFc, kFct, drvFc ) ;
      return ( 0 ) ;
    }
    else 
      // Add pElem
      add_elem2fc ( pFc, 0, pElem, kFace, kFct, drvFc ) ;
    return ( 0 ) ;
  }
  else {
    sprintf ( hip_msg, "could not add element "FUNLOC"." ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( -1 ) ;
}










/******************************************************************************
  llFc_add_facets:   */


/*! Add the facets of a face to one of the face litst.
 *
 */

/*
  
  Last update:
  ------------
  22Nov20; for faces that have become internal, set nFace=0 if doRemove.
  1May20; use llFc_add_elem_pvxfct
  8Jun18; re-implement special treatment of boundary faces.
  25May18; print out warning only if verbosity >=3.
  29jul17; count doubly matched faces even when doWarn and doRemove are zero.
  16May17; fix bug in logic path to triplicate faces
  use doRemove by setting geoType to match. 
  10Sep16: extracted from make_llFc.
  

  Input:
  ------
  pElem: elem that forms this face
  kFace: face of this elem that the facets are on
  pBndFc: pointer to bndFc if this is a bnd face
  mFacets: the number of facets on this face
  mFacetVerts: the number of vertices of each facet
  pFaceVx: the vertices of each facet

  Changes To:
  -----------
  ppfc2el: the list of faces so far
  pmFctDupl: Counter incremented if facet duplication happened.


  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int llFc_add_facets ( fc2el_s **ppfc2el, llVxEnt_s *pllVxFc, 
                      elem_struct *pElem, int kFace, bndFc_struct *pBndFc,
                      int mFacets, int mFacetVerts[], vrtx_struct *pFacetVx[][MAX_VX_FACE],
                      ulong_t *pmFctBecomeInt, ulong_t *pmFctDupl, ulong_t *pmFctOtherDupl,
                      const int doWarn, const int doRemove ) {

  int kFct ;
  int mVxFct ;
  vrtx_struct **ppVxFct ;
  int drvFc = ( mFacets > 1 ? 1 : 0 ) ; // is a derived face if more than 1 facet
  int nEnt ;
  const int use_side1 = 1 ;
  fc2el_s *pFc ;
  int kVx ;
  for ( kFct = 0 ; kFct < mFacets ; kFct++ ) {
    /* Make a list of vertices that form this facet. */
    mVxFct = mFacetVerts[kFct] ;
    ppVxFct = pFacetVx[kFct] ;

    /* Disregard collapsed faces. */
    if ( ( mVxFct = fc_not_collapsed ( mVxFct, ppVxFct ) ) ) {

      const int use_s1=1 ; // add elems silently to side 1. 
      nEnt = llFc_add_elem_pvxfct ( ppfc2el, pllVxFc, 
                                    pElem, kFace, mVxFct, ppVxFct,
                                    drvFc, kFct, pBndFc, use_side1 ) ;

      if ( nEnt ) {
        /* Face exists, and adding elem to s1 failed. */
        pFc = (*ppfc2el)+nEnt ;
        if ( pFc->side[1].elem.type ) {

          if ( ( pFc->side[1].elem.type == el && !pBndFc ) ||
               ( pFc->side[1].elem.type == bndFace && pBndFc ) )
            /* Duplicated element face, or duplicated boundary face. */
            (*pmFctDupl)++ ;
          else if  ( pFc->side[1].elem.type == el && pBndFc &&
                     !(pBndFc->Pbc->geoType == match) )
            /* Boundary face of a non-matching bc between elements. */
            (*pmFctBecomeInt)++ ;
          else
            /* Let's see what this could be. */
            (*pmFctOtherDupl)++ ;
            

          if ( doWarn || doRemove ) {
            if ( pBndFc ) {
              // Boundary face that already exists between two elements
              if ( doWarn && verbosity > 3 ) {
                int mC = sprintf ( hip_msg, "boundary face/edge between" ) ;
                for ( kVx = 0 ; kVx < mVxFct ; kVx++ )
                  mC += sprintf ( hip_msg+mC, " %"FMT_ULG"",
                                  ppVxFct[kVx]->number ) ;
                sprintf ( hip_msg+mC, ", has become interior." ) ;
                hip_err ( info, 4, hip_msg ) ;
              }
              if ( doRemove ) {
                pBndFc->invalid = 1 ;
                pBndFc->nFace = 0 ; // write_hdf check on nFace, where was that set zero?
              }
              else {
                pBndFc->geoType = inter ;
              }
            }
            else { 
              sprintf ( hip_msg,
                        "found triplicate element face in make_llFc." ) ;
              hip_err ( warning, 1, hip_msg ) ;
            }
          }
        }
        
      } // if ( nEnt )
      

#ifdef DONTUSE      
      /* Add the face to the list. */
      if ( ( nEnt =
             get_ent_vrtx ( pllVxFc, mVxFct,
                            ( const vrtx_struct **) ppVxFct, &kMin ) ) ) {
        /* The face exists. Fill the other side. */
        pFc = (*ppfc2el)+nEnt ;
        if ( pFc->side[1].elem.type ) {

          if ( ( pFc->side[1].elem.type == el && !pBndFc ) ||
               ( pFc->side[1].elem.type == bndFace && pBndFc ) )
            /* Duplicated element face, or duplicated boundary face. */
            (*pmFctDupl)++ ;
          else if  ( pFc->side[1].elem.type == el && pBndFc &&
                     !(pBndFc->Pbc->geoType == match) )
            /* Boundary face of a non-matching bc between elements. */
            (*pmFctBecomeInt)++ ;
          else
            /* Let's see what this could be. */
            (*pmFctOtherDupl)++ ;
            

          if ( doWarn || doRemove ) {
            if ( pBndFc ) {
              // Boundary face that already exists between two elements
              if ( doWarn ) {
                mC = sprintf ( hip_msg, "boundary face/edge between" ) ;
                for ( kVx = 0 ; kVx < mVxFct ; kVx++ )
                  mC += sprintf ( hip_msg+mC, " %"FMT_ULG"",
                                  ppVxFct[kVx]->number ) ;
                sprintf ( hip_msg+mC, ", has become interior." ) ;
                hip_err ( info, 4, hip_msg ) ;
              }
              if ( doRemove ) {
                pBndFc->invalid = 1 ;
              }
              else {
                pBndFc->geoType = inter ;
              }
            }
            else { 
              sprintf ( hip_msg,
                        "found triplicate element face in make_llFc." ) ;
              hip_err ( warning, 1, hip_msg ) ;
            }
          }
        }
        else if ( pBndFc )
          // Add a boundary face. 
          add_bnd2fc ( pFc, pBndFc, kFct, drvFc ) ;
        else
          // Add an internal face.
          add_elem2fc ( pFc, 1, pElem, kFace, kFct, mFacets ) ;
      } // if ( ( nEnt =
      else if ( ( nEnt = add_ent_vrtx ( pllVxFc, mVxFct,
                                        ( const vrtx_struct **) ppVxFct,
                                        &kMin ) ) ) {
        pFc = (*ppfc2el)+nEnt ;
        add_elem2fc ( pFc, 0, pElem, kFace, kFct, mFacets-1 ) ;
      }
      else {
        sprintf ( hip_msg, "could not add element in make_llFc." ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
#endif
    }
  }

  
  return ( 0 ) ;
}


/******************************************************************************

  make_llFc:
  Make a complete list of faces.
  
  Last update:
  ------------
  25Feb20; remove mFcUnmatched, mFcRemoved args, since not updated, unused.
  9Jul19; rename to ADAPT_HIERARCHIC
  4jun19; remove term cond in bnd loop, consider only base elems if grid is buffered.
  3Jun19; remove term condition in element loop, consider only number condition.
  8Jun18; new interface to llFc_add_facets
  5Jul17: only work on numbered elements, rename to _llFc from llfc.
  12May17; intro geoType arg to only treat actual boundary faces with geoType==bnd.
  3Mar16; remove ancillary line feeds from warnings.
  6Apr13; extend ADAPT_REF to conditionally define vars unused otherwise.
  6Apr13; promote possibly large int to type ulong_t.
          add correct extra parenthesis in if test with mVxFct.
  4Apr13; modified interface to loop_elems.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  20Dec11; don't add collapsed faces to the list.
  1Apr05; make static, add doListBnd.
  15Jul99; intro bndFcVx.
  16Dec98; Fix bug with passing ppfc2el.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

llVxEnt_s *make_llFc ( uns_s *pUns, bcGeoType_e geoType,
                       fc2el_s **ppfc2el,
                       int doWarn, int doRemove, int doListBnd,
                       ulong_t *pmBndFcBecomeInt, ulong_t *pmIntFcDupl, ulong_t *pmBndFcDupl ) {
  
  const elemType_struct *pElT ;
  vrtx_struct **ppVxFct ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  llVxEnt_s *pllVxFc ;
  fc2el_s *pFc ;
  int mVxHg, kFace, kFct, kVx, kMin, mVxFct, nBc, nEnt ;
  //#ifdef ADAPT_HIERARCHIC
  int kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS] ;
  //#endif
  int mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE] ;
  vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  pllVxFc = make_llEnt( NULL, pUns, ( char ** ) ppfc2el, CP_NULL, 99,
                        MAX_VX_FACE, sizeof( fc2el_s ) ) ;


  /* Reset counters. */
  *pmIntFcDupl = *pmBndFcDupl =*pmBndFcBecomeInt = 0 ;
  ulong_t mFcOtherDupl = 0 ;
  ulong_t mIntFcBecomeInt = 0 ; // There shouldn't be any of these, but this is a dummy arg.
  
  /* Loop over all elements, add all faces to the list. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ ) {
      if ( pElem->number ) {
        /* Get the surface triangulation. */
#ifdef ADAPT_HIERARCHIC
        if ( pUns->pllAdEdge && !pUns->isBuffered ) {
          get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
          get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                                mFacets, mFacetVerts, kFacetVx ) ;
          get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                               pFacetVx ) ;
        }
        else
#endif
          get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;

        /* Loop over all facets. */
        pElT = elemType + pElem->elType ;
        for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ )
          llFc_add_facets ( ppfc2el, pllVxFc, pElem, kFace, NULL,
                            mFacets[kFace], mFacetVerts[kFace], pFacetVx[kFace],
                            &mIntFcBecomeInt, pmIntFcDupl, &mFcOtherDupl,
                            doWarn, doRemove ) ;
      } //if ( pElem->number ) {
    } //for ( pElem = pElBeg
  } //   while ( loop_elems 


  if ( !doListBnd)   
    return ( pllVxFc ) ;




  
  /* Loop over all boundary faces and find the matching fc2el. If fc2el
     is double-sided, ie. has two neighbors, toss the boundary face. If
     not, check the match in element pointers and mark fc2el. */
  ulong_t mFctBc, mFctDuplBc ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBndPatch = NULL ;
    mFctDuplBc = mFctBc = 0 ;
    if ( geoType == noBcGeoType || pUns->ppBc[nBc]->geoType == geoType ) {
      /* Only consider boundary faces, ignore interfaces, matching
         faces have been tested separately at declaration. */
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
      
        /* Match these boundary faces to the list. */
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pElem = pBndFc->Pelem ;
          kFace = pBndFc->nFace ;
          if ( pElem && pElem->number && kFace ) {
            /* Is this face derived? For the time being we have no support to
               triangulate just one face. Redo the entire element. Fix this. */
#ifdef ADAPT_HIERARCHIC
            if ( pUns->pllAdEdge && !pUns->isBuffered ) {
              get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
              get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                                    mFacets, mFacetVerts, kFacetVx ) ;
              get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                                   pFacetVx ) ;
            }
            else
#endif
              get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;
            
            mFctBc += mFacets[kFace] ;
            llFc_add_facets ( ppfc2el, pllVxFc, pElem, kFace, pBndFc,
                              mFacets[kFace], mFacetVerts[kFace], pFacetVx[kFace],
                              pmBndFcBecomeInt, &mFctDuplBc, &mFcOtherDupl,
                              doWarn, doRemove ) ;
          } // if ( pElem && pElem->number && kF
        }
      }
    }

    (*pmBndFcDupl) += mFctDuplBc ;

    if ( mFctDuplBc == mFctBc ) {
      /* All faces of this boundary are duplicated. Issue warning, if doRemove=1
         the faces have already been invalidated and the recount will discard the bc. */
      sprintf ( hip_msg, "bc %s\n"
                "            is redundant as all its %"FMT_ULG" faces are duplicated.",
                pUns->ppBc[nBc]->text, mFctDuplBc ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }

  if ( *pmIntFcDupl ) {
    sprintf ( hip_msg, "found %"FMT_ULG" duplicated internal faces/edges.\n", *pmIntFcDupl ) ;
    hip_err ( warning, 2, hip_msg ) ;
  }
  if ( *pmBndFcDupl ) {
    sprintf ( hip_msg, "found %"FMT_ULG" duplicated boundary faces/edges.\n", *pmBndFcDupl ) ;
    hip_err ( warning, 2, hip_msg ) ;
  }
  if ( *pmBndFcBecomeInt ) {
    sprintf ( hip_msg, "found %"FMT_ULG" boundary faces that have become interior.\n",
              *pmBndFcBecomeInt ) ;
    hip_err ( info, 1, hip_msg ) ;
  }
  
  return ( pllVxFc ) ;
}



/******************************************************************************

  make_llMatchfc:
  Make a list of faces to be matched, 'zero' each of the
  faces by setting pBndFc->nFace=0.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC, interDuplicate to duplicateInter.
  11May17; repace matchBc with geoType in bc_s.
  6Apr13; extend ADAPT_REF to conditionally define vars unused otherwise.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  30Jun05; derived from make_llFc
  
  Input:
  ------
  pUns

  Changes to:
  -----------
  pBndFc->nFace: set this index to zero to flag as invalid face. 

  Output:
  -----------
  **ppfc2el = list of faces to element pointers.
  *pmFcRemoved = number of removed faces
  
  Returns:
  --------
  pllVxFc: list of faces by node.
  
*/

llVxEnt_s *make_llMatchfc ( uns_s *pUns, bcGeoType_e geoType,
                            fc2el_s **ppfc2el, int *pmFcRemoved ) {
  
  vrtx_struct **ppVxFct ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem;
  llVxEnt_s *pllVxFc ;
  fc2el_s *pFc ;
  //#ifdef ADAPT_HIERARCHIC
  int mVxHg, 
    kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS] ;
  //#endif
  int kFace, kFct, kVx, kMin, mVxFct, nBc, nEnt, 
    mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE] ;
  vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  pllVxFc = make_llEnt( NULL, pUns, ( char ** ) ppfc2el, CP_NULL, 99,
                        MAX_VX_FACE, sizeof( fc2el_s ) ) ;

  int mC ;
  /* Reset counters. */
  *pmFcRemoved = 0 ;

  /* interfaces are treated with inter and duplicateInter faces
     at the same time. */
  bcGeoType_e geoType2 = ( geoType == inter ? duplicateInter : geoType ) ; 
  bcGeoType_e geoTypeThisBc ;

  
  /* Loop over all boundary faces and find the matching fc2el. If fc2el
     is double-sided, ie. has two neighbors, toss the boundary face. If
     not, check the match in element pointers and mark fc2el. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBndPatch = NULL ;
    geoTypeThisBc = pUns->ppBc[nBc]->geoType ;
    if ( geoTypeThisBc == geoType || geoTypeThisBc == geoType2 ) {
      /* This is a bc to be matched. */

      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
        /* Match these boundary faces to the list. */
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
          pElem = pBndFc->Pelem ;
          kFace = pBndFc->nFace ;
          if ( pElem && pElem->term && kFace ) {

            /* Treat face: remove matching and duplicateInters. This
               applies to all faces of this group, so can be done here. */
            if ( geoTypeThisBc == match || geoTypeThisBc == duplicateInter ) {
              (*pmFcRemoved)++ ;
              pBndFc->nFace= 0 ;
            }
 
            /* Is this face derived? For the time being we have no support to
               triangulate just one face. Redo the entire element. Fix this. */
#ifdef ADAPT_HIERARCHIC
            if ( pUns->pllAdEdge ) {
              get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
              get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                                    mFacets, mFacetVerts, kFacetVx ) ;
              get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                                   pFacetVx ) ;
            }
            else
#endif
              get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;


            /* Add all facets to the list. */
            for ( kFct = 0 ; kFct < mFacets[kFace] ; kFct++ ) {
              /* Make a list of vertices that form this facet. */
              mVxFct = mFacetVerts[kFace][kFct] ;
              ppVxFct = pFacetVx[kFace][kFct] ;

              /* Add the face to the list. */
              if ( ( nEnt = get_ent_vrtx ( pllVxFc, mVxFct,
                                           ( const vrtx_struct **) ppVxFct, &kMin ) ) ) {
                /* The face exists. Fill the other side. */
                pFc = (*ppfc2el)+nEnt ;

                if ( pFc->side[1].elem.type ) {
                  /* There is no missing boundary face. The face is either matched
                     by another boundary face or another element. */
                  if ( verbosity > 4 ) {
                    mC = sprintf ( hip_msg, "boundary face/edge between" ) ;
                    for ( kVx = 0 ; kVx < mVxFct ; kVx++ )
                      mC += sprintf ( hip_msg+mC, " %"FMT_ULG"", ppVxFct[kVx]->number ) ;
                    sprintf ( hip_msg+mC, ", exists in triplicate." ) ;
                    hip_err ( warning, 1, hip_msg ) ;
                  }
                  /*(*pmFcRemoved)++ ;
                    pBndFc->nFace= 0 ;*/
                }
                else {
                  add_elem2fc ( pFc, 1, pElem, kFace, kFct, mFacets[kFace]-1 ) ;
                  /*(*pmFcRemoved)++ ;
                    pBndFc->nFace= 0 ;*/
                }
              }

              else if ( ( nEnt = add_ent_vrtx ( pllVxFc, mVxFct,
                                                ( const vrtx_struct **) ppVxFct,
                                                &kMin ) ) ) {
                /* First reference. Add to the list. */
                pFc = (*ppfc2el)+nEnt ;
                add_elem2fc ( pFc, 0, pElem, kFace, kFct, mFacets[kFace]-1 ) ;
                /*(*pmFcRemoved)++ ;
                  pBndFc->nFace= 0 ;*/
              }
              else {
                sprintf ( hip_msg, "could not add element in make_llMatchfc." ) ;
                hip_err ( fatal, 0, hip_msg ) ;
              }
            }
          }
        }
      }
    }
  }
  
  return ( pllVxFc ) ;
}

/******************************************************************************

  make_llHybTriFc
  Make a list of faces between tet and pri/pyr.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  6Sep18; rename reset_vx_mark.
  8Jun18; new interface to llFc_add_facets
  12May17; intro geoType arg to only treat bnd faces.
  1Oct16; need only 3 nodes to identify a face.
  10Sep16; derived from make_llMatchFc
  
  Input:
  ------
  pUns

  The following args mean not much here, all these tests will have been done
  in check_conn. But keep them here to keep arg lists between llfc and llHybTrifc
  the same
  doWarn: warn about dubplicate faces
  doRemove: remove duplicate faces
  doListBnd: ?

  Changes To:
  -----------
  pVrtx->mark: all tet are flagged.

  Output:
  -------
  ppfc2el: list of face to el pointers
  pmFcBecomeInt: number of bnd faces that became interior (now elemnts either side)
  pmFcDupl: list of duplicate elem faces (e.g. more than two elem sharing)
  pmFcRemoved: number of removed boundary faces
  pmFcUnMatched: number of unmatched faces.
  
  Returns:
  --------
  llVxEnt: llVx index of faces
  
*/

llVxEnt_s *make_llHybTriFc ( uns_s *pUns,
                             fc2el_s **ppfc2el,
                             int doWarn, int doRemove, int doListBnd,
                             ulong_t *pmFcBecomeInt, ulong_t *pmFcDupl,
                             ulong_t *pmFcRemoved, ulong_t *pmFcUnMatched ) {
  
  vrtx_struct **ppVxFct ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  llVxEnt_s *pllVxFc ;
  fc2el_s *pFc ;
  //#ifdef ADAPT_HIERARCHIC
  int mVxHg, 
    kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS] ;
  //#endif
  int kFace, kFct, kVx, kMin, mVxFct, nBc, nEnt, 
    mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE] ;
  vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  // At most three nodes to consider, no need for MAX_VX_FACE. 
  int maxVxFace = 4 ;
  pllVxFc = make_llEnt( NULL, pUns, ( char ** ) ppfc2el, CP_NULL, 99,
                        3, sizeof( fc2el_s ) ) ;


  // Tag all vx forming tets
  reset_vx_mark ( pUns ) ;
  
  vrtx_struct ** ppVx ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid && pElem->elType == tet ) {
        ppVx = pElem->PPvrtx ;
        // Only tet now.
        for ( kVx = 0 ; kVx < 4 ; kVx++ ) {
          ppVx[kVx]->mark = 1 ;
        }
      }
  }// while ( loop_elems


  
  // Loop over all pri/pyr, add all tri faces to the list that have all
  // nodes forming tets
  const elemType_struct *pElT;
  const faceOfElem_struct *pFoE ;
  int allMarked ;
  ulong_t mFcOtherDupl = 0 ;
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid && ( pElem->elType == pyr || pElem->elType == pri ) ) {
        pElT = elemType + pElem->elType ;
        ppVx = pElem->PPvrtx ;
        for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
          pFoE = pElT->faceOfElem + kFace ;

          if ( pFoE->mVertsFace == 3 ) {
            for ( kVx = 0, allMarked = 1 ; kVx < 3 ; kVx++ ) {
              if ( !ppVx[ pFoE->kVxFace[kVx] ]->mark )
                allMarked = 0 ;
            }

            if ( allMarked ) {
              // Face with all vx used by tets, add it. 
#ifdef ADAPT_HIERARCHIC
              if ( pUns->pllAdEdge ) {
                get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
                get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                                      mFacets, mFacetVerts, kFacetVx ) ;
                get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                                     pFacetVx ) ;
              }
              else
#endif
                get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;


              /* Loop over all facets. */
              llFc_add_facets ( ppfc2el, pllVxFc, pElem, kFace, NULL,
                                mFacets[kFace], mFacetVerts[kFace], pFacetVx[kFace],
                                pmFcBecomeInt, pmFcDupl, &mFcOtherDupl,
                                doWarn, doRemove ) ;
            }
          }
        }
      }
  } // while ( loop_elems

  
  // not needed, as those faces are available with the bnd fc structure.
#ifdef NOT_NEEDED
  // Loop over all bnd, add all faces formed by tri to the list.
  /* Loop over all boundary faces and find the matching fc2el. If fc2el
     is double-sided, ie. has two neighbors, toss the boundary face. If
     not, check the match in element pointers and mark fc2el. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) ) {
      
      /* Match these boundary faces to the list. */
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pElem = pBndFc->Pelem ;
        kFace = pBndFc->nFace ;
        if ( pElem && pElem->term && pElem->elType == tet && kFace ) {
          /* Is this face derived? For the time being we have no support to
             triangulate just one face. Redo the entire element. Fix this. */
          // #ifdef ADAPT_HIERARCHIC
          if ( pUns->pllAdEdge ) {
            get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
            get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                                  mFacets, mFacetVerts, kFacetVx ) ;
            get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                                 pFacetVx ) ;
          }
          //#else
          else {
            get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;
          }
          //#endif

          llFc_add_facets ( ppfc2el, pllVxFc, pElem, kFace, pBndFc,
                            mFacets[kFace], mFacetVerts[kFace], pFacetVx[kFace],
                            pmBndFcBecomeInt, pmBndFcDupl, pmBndFcOtherDupl,
                            doWarn, doRemove ) ;
        }
      }
    }
  } // for ( nBc
#endif
  
  return ( pllVxFc ) ;
}


/******************************************************************************

  make_llMatchFc_vxMark
  Make a list of matching faces internal to a grid based on vertex marks
  arising e.g. from one-sided internal boundaries where the other side is
  sought.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  17Dec18; derived from make_llMatchFc_zoneVxMark
  
  Input:
  ------
  pUns
  kMark: which marker flag to use: can be 0/1, 2 or 3.

  Output:
  -------
  ppfc2el: list of face to el pointers
  pmIntZnFc: number of faces between those zone sets.
  
  Returns:
  --------
  llVxEnt: llVx index of faces
  
*/

llVxEnt_s * make_llInterFc_vxMark ( uns_s *pUns, 
                                    fc2el_s **ppfc2el,
                                    const int kMark ) {
  
  llVxEnt_s *pllVxFc ;
  fc2el_s *pFc ;
  //#ifdef ADAPT_HIERARCHIC
  int mVxHg, 
    kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS] ;
  //#endif
  int kFace, kFct, kVx, kMin, mVxFct, nBc, nEnt, 
    mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE] ;
  vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  // At most three nodes to consider, no need for MAX_VX_FACE. 
  int maxVxFace = 4 ;
  pllVxFc = make_llEnt( NULL, pUns, ( char ** ) ppfc2el, CP_NULL, 99,
                        3, sizeof( fc2el_s ) ) ;


  
  // Loop over all pri/pyr, add all tri faces to the list that have all
  // nodes forming tets
  const elemType_struct *pElT;
  const faceOfElem_struct *pFoE ;
  int allMark, allMark2, allMark3 ;
  vrtx_struct ** ppVx ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  ulong_t mFcDupl, mFcBecomeInt, mFcOtherDupl ;
  int doWarn=0, doRemove=0, doListBnd=0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid ) {
        pElT = elemType + pElem->elType ;
        ppVx = pElem->PPvrtx ;
        for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
          pFoE = pElT->faceOfElem + kFace ;

          for ( kVx = 0, allMark=1, allMark2=1, allMark3 = 1 ; kVx < 3 ; kVx++ ) {
            if ( !vx_has_markN ( ppVx[ pFoE->kVxFace[kVx] ], kMark ) )
              allMark = 0 ;
          }

          if ( allMark ) {
            // All vx of this face also have the same other mark. Add it. 
            //#ifdef ADAPT_HIERARCHIC
            if ( pUns->pllAdEdge )  {
              hip_err ( fatal, 0, "implement facet extraction for hierarchic adaptation"
                        " in make_llInterFc_vxMark.") ;
              mFacets[kFace] = 0 ; // to pacify compiler warning.
              /*
                get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
                get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                mFacets, mFacetVerts, kFacetVx ) ;
                get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                pFacetVx ) ; */
            }
            //#else
            else {
              /* JDM: add a variant that computes only the one face needed. */
              get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;
            }
            //#endif
        
            llFc_add_facets ( ppfc2el, pllVxFc, pElem, kFace, NULL,
                              mFacets[kFace], mFacetVerts[kFace], pFacetVx[kFace],
                              &mFcBecomeInt, &mFcDupl, &mFcOtherDupl,
                              doWarn, doRemove ) ;
            
          }
        }
      }
  } // while ( loop_elems

  
  return ( pllVxFc ) ;
}


/******************************************************************************

  make_llMatchFc_vxMark
  Make a list of matching faces internal to a grid based on vertex marks arising
  from nodes being shared by two or more zones.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  17Dec18; renme to zoneVxMark.
  8Jun18; new interface to llFc_add_facets
  20Jan18; derived from make_llHybTriFc
  
  Input:
  ------
  pUns
  mZone0, iZone0[]: number and list of zones on side 0
  mrkPos0: 1,3 or 3 for using mrk, mrk2 or mrk3
  mZone1, iZone1[]: number and list of zones on side 1
  mrkPos1: 1,3 or 3 for using mrk, mrk2 or mrk3

  Changes To:
  -----------
  pVrtx->mark: all tet are flagged.

  Output:
  -------
  ppfc2el: list of face to el pointers
  pmIntZnFc: number of faces between those zone sets.
  
  Returns:
  --------
  llVxEnt: llVx index of faces
  
*/

llVxEnt_s * make_llInterFc_zoneVxMark ( uns_s *pUns, 
                                        fc2el_s **ppfc2el,
                                        const int useMark3 ) {
  
  llVxEnt_s *pllVxFc ;
  fc2el_s *pFc ;
  //#ifdef ADAPT_HIERARCHIC
  int mVxHg, 
    kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS] ;
  //#endif
  int kFace, kFct, kVx, kMin, mVxFct, nBc, nEnt, 
    mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE] ;
  vrtx_struct *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  // At most three nodes to consider, no need for MAX_VX_FACE. 
  int maxVxFace = 4 ;
  pllVxFc = make_llEnt( NULL, pUns, ( char ** ) ppfc2el, CP_NULL, 99,
                        3, sizeof( fc2el_s ) ) ;


  
  // Loop over all pri/pyr, add all tri faces to the list that have all
  // nodes forming tets
  const elemType_struct *pElT;
  const faceOfElem_struct *pFoE ;
  int allMark, allMark2, allMark3 ;
  vrtx_struct ** ppVx ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  ulong_t mFcDupl, mFcBecomeInt, mFcOtherDupl ;
  int doWarn=0, doRemove=0, doListBnd=0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid ) {
        pElT = elemType + pElem->elType ;
        ppVx = pElem->PPvrtx ;
        for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
          pFoE = pElT->faceOfElem + kFace ;

          for ( kVx = 0, allMark=1, allMark2=1, allMark3 = 1 ; kVx < 3 ; kVx++ ) {
            if ( !vx_has_markN ( ppVx[ pFoE->kVxFace[kVx] ], 0 ) )
              allMark = 0 ;
            if ( !vx_has_markN ( ppVx[ pFoE->kVxFace[kVx] ], 2 ) )
              allMark2 = 0 ;
            if ( !vx_has_markN ( ppVx[ pFoE->kVxFace[kVx] ], 3 ) )
              allMark3 = 0 ;
          }
          /* At least mark and mark2 need to be used. Blank out allMark3 if not used. */
          if ( !useMark3 ) allMark3 = 0 ;

          if ( allMark + allMark2 + allMark3 > 1 ) {
            // All vx of this face also have the same other mark. Add it. 
            //#ifdef ADAPT_HIERARCHIC
            if ( pUns->pllAdEdge ) {
              hip_err ( fatal, 0, "implement facet extraction for hierarchic adaptation"
                        " in make_llInterFc_zoneVxMark.") ;
              mFacets[kFace]=0; // to pacify compiler warning.
              /*
                get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
                get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                mFacets, mFacetVerts, kFacetVx ) ;
                get_surfVx_drvElem ( pElem, pHgVx, mFacets, mFacetVerts, kFacetVx,
                pFacetVx ) ; */
            }
            //#else
            else {
              /* JDM: add a variant that computes only the one face needed. */
              get_elem_facets( pElem, mFacets, mFacetVerts, pFacetVx ) ;
            }
            //#endif
        
            llFc_add_facets ( ppfc2el, pllVxFc, pElem, kFace, NULL,
                              mFacets[kFace], mFacetVerts[kFace], pFacetVx[kFace],
                              &mFcBecomeInt, &mFcDupl, &mFcOtherDupl,
                              doWarn, doRemove ) ;
          }
        }
      }
  } // while ( loop_elems

  
  return ( pllVxFc ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  create_matchFc_llVx:
*/
/*! given an llVxFc list of matching internal faces, create a list of matchFc_s.
 * 
 *
 */

/*
  
  Last update:
  ------------
  21Apr20, is this ever called?
  6Feb18; consistently catch useMark3
  21Jan18: conceived.
  

  Input:
  ------
  pUns
  useMark3: if non-0, work with three sets/zones, marked with mark, mark2, mark3.
  Otherwwise consider only two sets/zones mark, mark2.
  bcLbl_XY: label the interface between mark X and mark Y with this bc label.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int create_matchFc_llVx ( uns_s *pUns, const int useMark3,
                          const char bcLbl_12[MAX_BC_CHAR],
                          const char bcLbl_13[MAX_BC_CHAR],
                          const char bcLbl_23[MAX_BC_CHAR]
                          ) {


  /* Create a list of matching faces between based on up to 3 vx->marks. */
  fc2el_s *pFc2El ;
  llVxEnt_s *pllVxMatchFc = make_llInterFc_zoneVxMark ( pUns, &pFc2El, useMark3 ) ; 
  int mMatchFc = get_used_sizeof_llEnt ( pllVxMatchFc ) ;

  /* Copy that list into matchFc. */
  chunk_struct *pRCh = pUns->pRootChunk ;
  if ( pRCh->mMatchFaces )
    hip_err ( fatal, 0, "ask your friendly developer to implement incrementing"
              " matching faces in create_matchFc_llVx." ) ;

  pRCh->PmatchFc = arr_malloc ( "PmatchFc in create_matchFc_llVx", pUns->pFam,
                                mMatchFc+1, sizeof ( *pRCh->PmatchFc ) ) ;
  pRCh->mMatchFaces = mMatchFc ;


  /* Create bcs for L, U, C interfaces. */
  char bcLabel[MAX_BC_CHAR] ;
  bc_struct *pBc_12, *pBc_13, *pBc_23 ;
  snprintf ( bcLabel, MAX_BC_CHAR, "hip_matchIF_%s", bcLbl_12 ) ;  
  if ( !find_bc ( bcLabel, 2 ) ) {
    sprintf ( hip_msg,   "reserved bc %s exists already.", bcLabel ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  pBc_12 = find_bc ( bcLabel, 1 ) ;
  pBc_12->geoType = match ;
             
  if ( useMark3 ) {
    snprintf ( bcLabel, MAX_BC_CHAR, "hip_matchIF_%s", bcLbl_13 ) ;  
    if ( !find_bc ( bcLabel, 2 ) ) {
      sprintf ( hip_msg,   "reserved bc %s exists already.", bcLabel ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    pBc_13 = find_bc ( bcLabel, 1 ) ;  
    pBc_13->geoType = match ;

    snprintf ( bcLabel, MAX_BC_CHAR, "hip_matchIF_%s", bcLbl_23 ) ;  
    if ( !find_bc ( bcLabel, 2 ) ) {
      sprintf ( hip_msg,   "reserved bc %s exists already.", bcLabel ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    pBc_23 = find_bc ( bcLabel, 1 ) ;
    pBc_23->geoType = match ;
  }


             
  /* Pass the fc2el data to matchFc_s */
  matchFc_struct *pMFc = pRCh->PmatchFc+1 ;
  fc2el_s *pF2E ;
  elem_struct *pEl0, *pEl1 ;
  for ( pF2E = pFc2El ; pF2E < pFc2El + mMatchFc ; pF2E++ ) {
#ifdef DEBUG
    if( !pF2E->side[0].elem.Pelem || !pF2E->side[1].elem.Pelem )
      hip_err ( fatal, 0, "empty element slot in create_matchFc_llVx." ) ;
#endif

    pMFc->pElem0 = pEl0 = pF2E->side[0].elem.pElem ;
    pMFc->nFace0 = pF2E->side[0].elem.kFace ;
    pMFc->pElem1 = pEl1 = pF2E->side[1].elem.pElem ;
    pMFc->nFace1 = pF2E->side[1].elem.kFace ;

    /* JDM: when converting to mark, mark2, mark3 to the bitfield mark, 
       keep mark as 0, mark2 as 2, mark3 as 3. 1 unused here. */
    if ( elem_has_mark ( pEl1, 0 ) ) {
      //pEl0->mark ) {
      if ( elem_has_mark ( pEl1, 2 ) )
        //pEl1->mark2 )
        pMFc->pBc = pBc_12 ;
      else if ( useMark3 )
        pMFc->pBc = pBc_13 ;
    }
    else if ( elem_has_mark ( pEl0, 2 ) ) {
      //pEl0->mark2 ) {
      if ( elem_has_mark ( pEl1, 0 ) )
        //pEl1->mark )
        pMFc->pBc = pBc_12 ;
      else if (useMark3)
        pMFc->pBc = pBc_23 ;
    }
    else if ( useMark3 ) { // mark3 has mark3
      if ( elem_has_mark ( pEl1, 0 ) )
        // pEl1->mark )
        pMFc->pBc = pBc_13 ;
      else
        pMFc->pBc = pBc_23 ;
    }
  }


  arr_free ( pllVxMatchFc ) ;
  arr_free ( pFc2El ) ;
 
  
  return ( mMatchFc ) ;
}



/******************************************************************************

  rm_special_fc:
  Remove faces that are declared to match or are duplicates of an 
  interface.
  
  Last update:
  ------------
  18dec24; pacify compiler by initialising pChFcType.
  12May17; rename from rm_matchFc, treat also interfaces.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int rm_special_fc ( uns_s *pUns, bcGeoType_e geoType ) {
  
  llVxEnt_s *pllVxFc ;
  fc2el_s *pfc2el, *pFc ;
  int mFcRemoved, mFcUnMtch = 0, mFc, nEnt, kVx, kFace ;
  vrtx_struct *pVrtx[MAX_VX_FACE] ;
  elem_struct *pElem ;
  char chMatchingFc[] = "matching faces", chInterFc[] = "interfaces" ;
  char *pChFcType = chMatchingFc ;

  if ( geoType == match )
    pChFcType = chMatchingFc ;
  else if ( geoType == inter )
    pChFcType = chInterFc ;
  else 
    hip_err ( fatal, 0, "unknown geoType oin rm_special_fc, "
              "this shouldn't have happened." ) ;
  
  sprintf ( hip_msg, "Removing %s.", pChFcType ) ;
  hip_err ( blank, 4, hip_msg ) ;


  /* Make a list of matching faces so we can check the match,
     flag the faces as invalid using pBndFc->nFace=0. */
  pllVxFc = make_llMatchfc ( pUns, geoType, &pfc2el, &mFcRemoved ) ;

  int mC ;
  /* Loop over all entries, list holes. */
  mFc = get_sizeof_llEnt ( pllVxFc ) ;
  for ( nEnt = 1 ; nEnt <= mFc ; nEnt++ ) {
    pFc = pfc2el+nEnt ;
    if ( pFc->side[0].elem.type && !pFc->side[1].elem.type ) {
      /* Hole. */
      if ( verbosity > 4 ) {
	mC = sprintf ( hip_msg, "unmatched edge/face at" ) ;
        show_ent ( pllVxFc, nEnt, pVrtx ) ;
        pElem = pFc->side[0].elem.pElem ;
        kFace = pFc->side[0].elem.kFace ;
	for ( kVx = 0 ; kVx < MAX_VX_FACE ; kVx++ )
          if ( pVrtx[kVx] )
            mC+=sprintf ( hip_msg+mC," %"FMT_ULG",", pVrtx[kVx]->number ) ;
	sprintf ( hip_msg+mC," from element %"FMT_ULG", face %d.\n", pElem->number, kFace ) ;
        hip_err ( warning, 1, hip_msg ) ;
	if ( verbosity > 5 )
          printfcco( pElem, kFace ) ;
      }
      mFcUnMtch++ ;
    }
  }

  free_llEnt ( &pllVxFc ) ;
  arr_free ( pfc2el ) ;

  sprintf ( hip_msg, "removed %d %s.", mFcRemoved, pChFcType ) ;
  hip_err ( info, 1, hip_msg ) ;

  if ( mFcUnMtch ) {
    sprintf ( hip_msg, "found %d %s that are unmatched, but should have been matched.",
              mFcUnMtch, pChFcType ) ;
    hip_err ( fatal, 0, hip_msg ) ;
    pUns->validGrid = 0 ;
  }

  return ( 1 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  rm_special_faces:
*/
/*! Wrapper routine to remove matching and duplcate internal faces.
 *
 *
 */

/*
  
  Last update:
  ------------
  12May17: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int rm_special_faces ( uns_s *pUns ) {

  int mFcRemoved ;
  mFcRemoved  = rm_special_fc ( pUns, match ) ;
  mFcRemoved += rm_special_fc ( pUns, inter ) ;
  
  return ( mFcRemoved ) ;
}



/******************************************************************************

  match_bndFcVx:
  match a list of boundary faces given by vertex numbers to the elements.
  For the time being, primitives only. The patch and face spaces have to
  be properly allocated.
  
  Last update:
  ------------
  11Feb25; fix bug with applying q2t to second mesh, reset invalid face flag
  3Mar16; compress output, remove ancillary line feeds.
  21Dec14; move below make_llFc for prototyping.
  4Apr13; modified interface to loop_elems.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  8Nov10; fix bug in element matching loop arisen with switch to hip_err.
  1Sep05; adjust mBc in pUns.
  19Jul99: derived from make_llFc.
  
  Input:
  ------
  pUns
  reDo: if set to 1, just redo the face number calculation, e.g. if a
        degenerate element has been demoted.


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int match_bndFcVx ( uns_s *pUns ) {

  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace  ;
  
  int nBf, mBc, kMin, nEnt, kVx, kFace, nBp,
    mFcDupl = 0, mFcUnMatched = 0, mBFcInt = 0 ;
  bndFcVx_s *pBf ;
  bndFc_struct *pBndFc ;
  bndPatch_struct *pBp ;
  bc_struct *pBc ;
  chunk_struct *pChunk ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  vrtx_struct *pVxFc[MAX_VX_FACE] ;
  char str[LINE_LEN] ;

  if ( !pUns->mBndFcVx )
    /* No faces to match. */
    return ( 1 ) ;
  

  fc2el_s *pFc2El, *pFc ;
  llVxEnt_s *pllVxFc = make_llEnt( NULL, pUns, ( char ** ) &pFc2El, CP_NULL,
                                   pUns->mBndFcVx, MAX_VX_FACE, sizeof( fc2el_s ) ) ;


  /* Sort pBndFcVx for bc number. */
  qsort ( pUns->pBndFcVx, pUns->mBndFcVx, sizeof ( bndFcVx_s ), bndFcVx_bcNrCompare ) ;


  /* Loop over all faces. */
  pBc = NULL ;
  mBc = 0 ;
  pBp = pUns->pRootChunk->PbndPatch ;
  for ( nBf = 0, pBf = pUns->pBndFcVx, pBndFc = pUns->pRootChunk->PbndFc+1 ;
        nBf < pUns->mBndFcVx ; nBf++, pBf++, pBndFc++ ) {

    if ( pBf->pBc != pBc ) {
      /* Use a new patch. */
      if ( ++mBc > pUns->mBc ) {
        sprintf ( hip_msg, "too many bc's with the faces in match_bndFcVx." ) ;
        hip_err ( fatal, 0, hip_msg ) ; 
      }
      pBp++ ;
      pBp->Pchunk = pUns->pRootChunk ;
      pBp->PnxtBcPatch = NULL ;
      pBp->Pbc = pBc = pBf->pBc ;
      pBp->PbndFc = pBndFc ;
      pBp->mBndFc = 0 ;
    }

    /* Count the faces. */
    pBp->mBndFc++ ;
      
    /* Add this face, given by its vertices (not yet as bndFc_struct) to 
       the list of llVxEnt. */
    nEnt = add_ent_vrtx ( pllVxFc, pBf->mVx,
                          ( const vrtx_struct **) pBf->ppVx, &kMin ) ;
    
    pFc = pFc2El+nEnt ;
    if ( pFc->side[1].elem.type ) {
      /* There is no missing boundary face. The face is either matched
         by another boundary face or another element. */
      if ( verbosity > 3 ) {
        sprintf ( hip_msg, "boundary face/edge between" ) ;
        for ( kVx = 0 ; kVx < pBf->mVx ; kVx++ ) {
          sprintf ( str," %"FMT_ULG"", pBf->ppVx[kVx]->number ) ;
          strcat( hip_msg, str ) ;
        }
        strcat ( hip_msg, ", has become interior in match_bndFcVx." ) ;
        hip_err ( warning, 4, hip_msg ) ;
      }
      mFcDupl++ ;

      pBndFc->Pelem = NULL ;
      pBndFc->nFace = 0 ;
      pBndFc->invalid = 1 ;
    }
    else {
      /* Add the face. */
      pBndFc->Pbc = pBc ;
      pBndFc->invalid = 0 ;
      add_bnd2fc ( pFc, pBndFc, pFc->side[0].elem.kFacet, pFc->side[0].elem.drvFc ) ;

      /* For error tracing, encode nBf into nFace. */
      pBndFc->nFace = nBf ;
    }
  }

  if ( mFcDupl ) {
    sprintf ( hip_msg, "found %d duplicated boundary faces.", mFcDupl ) ;
    hip_err ( warning, 1, hip_msg ) ;
  } 



  /* Adjust bc count. */
  pUns->mBc = mBc ;
  pUns->pRootChunk->mBndPatches = mBc ;


  
  /* Loop over all elements, add those faces to the list that are
     matched by boundary faces. */
  pChunk = NULL ;
  int mElDupl = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid ) {
        pElT = elemType + pElem->elType ;

        for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
          pFoE = pElT->faceOfElem + kFace ;
          kVxFace = pFoE->kVxFace ;

          /* Make a list of vertices that form this facet. */
          for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ )
            pVxFc[kVx] = pElem->PPvrtx[ kVxFace[kVx] ] ;

          /* Add the face to the list. */
          if ( ( nEnt = get_ent_vrtx ( pllVxFc, pFoE->mVertsFace,
                                       ( const vrtx_struct **) pVxFc, &kMin ) ) ) {
            /* The face exists. Fill the other side. */
            pFc = pFc2El+nEnt ;

            if ( pFc->side[0].elem.type ) {
              /* The element side is already filled. */
              if ( pFc->side[1].elem.type == el ) {
                mElDupl++ ;
                if ( doWarn.intFc ) {
                  sprintf ( hip_msg, "found duplicate face between elements in match_bndFcVx." ) ;
                  hip_err ( warning, 4, hip_msg ) ;
                  if ( verbosity > 4 ) 
                    printfc ( pElem, kFace ) ;
                }
              }
              else if ( pFc->side[1].elem.type == bndFace ) {
                mBFcInt++ ;
                if ( doWarn.intFc ) {
                  sprintf ( hip_msg, "found internal bnd face in match_bndFcVx." ) ;
                  hip_err ( warning, 4, hip_msg ) ;
                  if ( verbosity > 4 ) 
                    printfc ( pElem, kFace ) ;
                }
                if ( doRemove.intFc ) {
                  /* This is an interior boundry face. Remove it. */
                  pBndFc = pFc->side[1].bndFc.pBndFc ;
                  pBndFc->invalid = 1 ;
                  pBndFc->Pelem = NULL ;
                  pBndFc->nFace = 0 ;
                  /* Replace the face with the interior element. */
                  add_elem2fc( pFc, 1, pElem, kFace, 1, 0 ) ;
                }
              }
            }

            else {
              add_elem2fc ( pFc, 0, pElem, kFace, 1, 0 ) ;

              pBndFc = pFc->side[1].bndFc.pBndFc ;
              pBndFc->Pelem = pElem ;
              pBndFc->nFace = kFace ;
            }
          }
        }
      }

  if ( mElDupl ) {
    sprintf ( hip_msg, "found %d duplicated faces between elements (not boundaries).", mElDupl ) ;
    hip_err ( warning, 1, hip_msg ) ;
  } 
  if ( mBFcInt ) { 
    if ( doRemove.intFc )
      sprintf ( hip_msg, "found and removed %d bnd faces that became internal.\n"
                "            Use set fc-remove to alter hip's behaviour.", mBFcInt ) ;
    else
      sprintf ( hip_msg, "found, but retained %d bnd faces that became internal.\n"
                "            Use set fc-remove to alter hip's behaviour.", mBFcInt ) ;
    hip_err ( warning, 1, hip_msg ) ;
  } 



  /* Loop over all boundary faces and check for proper matches. Hand-code the
     loop rather than using loop_bndFc, since pUns->ppBc is not yet built. */
  pChunk = pUns->pRootChunk ;
  for ( nBp = 1 ; nBp <= pChunk->mBndPatches ; nBp++ ) {
    pBp = pChunk->PbndPatch + nBp ;

    for ( pBndFc = pBp->PbndFc ; pBndFc < pBp->PbndFc+pBp->mBndFc ; pBndFc++ )
      if ( !pBndFc->invalid && !pBndFc->Pelem ) {
        sprintf ( hip_msg, "found unmatched boundary face in match_bndFcVx" ) ;
        hip_err ( warning, 4, hip_msg ) ;
        if ( verbosity > 4 ) {
          printf ( "         formed by" ) ;
          show_ent ( pllVxFc, pBndFc->nFace, pVxFc ) ;
          for ( kVx = 0 ; kVx < MAX_VX_FACE ; kVx++ )
            if ( pVxFc[kVx] )
              printf ( " %"FMT_ULG",", pVxFc[kVx]->number ) ;
          printf ( "\b in match_bndFcVx.\n" ) ;
        }
        mFcUnMatched++ ;
      }
  }
  
  
  arr_free ( pUns->pBndFcVx ) ;
  pUns->mBndFcVx = 0 ;
  pUns->pBndFcVx = NULL ;

  arr_free ( pllVxFc ) ;
  arr_free ( pFc2El ) ;

  if ( mFcUnMatched ) {
    sprintf ( hip_msg, "found %d unmatched boundary faces.\n", mFcUnMatched ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 1 ) ;
}


/******************************************************************************

  match_bvx2vx:
  match a list of boundary nodes and bc info to the elements.
  For the time being, primitives only. 
  
  Last update:
  ------------
  7jul20; fix bug with if block nesting that prevented filling of interior sides.
  29Apr20: derived from match_bndFc2vx.
  
  Input:
  ------
  pUns

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int match_bvx2vx ( uns_s *pUns ) {
#undef FUNLOC
#define FUNLOC "in match_bvx2vx" 


  if ( !pUns->mBvx2Vx )
    /* No faces to match. */
    return ( 1 ) ;

  const int kMark = 0 ;
  reserve_vx_markN ( pUns, kMark, FUNLOC ) ;
  reset_vx_markN ( pUns, kMark ) ;


  /* Renumbering vertices breaks the list of boundary vertices,
     store the vertex pointer for each bnd vx. */
  vrtx_struct **ppBndVrtx = arr_malloc ( "ppbndVrtx "FUNLOC".", pUns->pFam,
                                       pUns->mBvx2Vx, sizeof( *ppBndVrtx ) ) ;
  
  
  /* Sort the Bx2Vx list within each bc, mark all bnd vx. */
  int kBc ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  vrtx_struct **ppBvx = ppBndVrtx ;
  int *pnBv, *pnBvBeg, *pnBvNxt ;
  chunk_struct *pCh ;
  for ( kBc = 0 ; kBc < pUns->mBc ; kBc++ ) {
    pCh = NULL ;
    pVx = NULL ;
    pnBvBeg = pUns->pnBvx2Vx + pUns->pnBvx2Vx_fidx[kBc]-1 ;
    pnBvNxt = pUns->pnBvx2Vx + pUns->pnBvx2Vx_fidx[kBc+1]-1 ;
    qsort ( pnBvBeg, pnBvNxt - pnBvBeg, sizeof( *pUns->pnBvx2Vx ), cmp_int ) ;

    /* Flag all vx on this bc. */
    for ( pnBv = pnBvBeg ; pnBv < pnBvNxt ; pnBv++, ppBvx++ ) {
      *ppBvx = NULL ;
      while ( !*ppBvx && loop_verts_cont ( pUns, &pCh, &pVxBeg, pVx, &pVxEnd ) )
        for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
          if ( pVx->number == *pnBv ) {
            *ppBvx = pVx ;
            break ;
          }
              
      if ( *ppBvx ) {
        set_vrtx_mark_k ( *ppBvx, kMark ) ;
      }
      else
        hip_err ( fatal, 0, "could not find referenced boundary vx "FUNLOC"." ) ;
    }
  } //  for ( kBc = 0 ; kBc < pUns->mBc ; kBc++ )
   

  /* Renumber only with those vx. The node numbers in pnBvx2Vx no longer
     match the vertex numbers. */
  number_uns_vx_markN ( pUns, kMark ) ;



  
  /* Start a face list. */
  fc2el_s *pFc2El = NULL ;
  llVxEnt_s *pllVxFc = make_llEnt( NULL, pUns, ( char ** ) &pFc2El, CP_NULL,
                                   pUns->mVertsNumbered,
                                   MAX_VX_FACE, sizeof( fc2el_s ) ) ;


  
  /* Loop over all elements, add those faces to the list that are
     matched by boundary faces. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  const elemType_struct *pElT ;
  int nFace ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  int kVx ;
  vrtx_struct *pVxFc[MAX_VX_FACE] ;
  int nEnt, kMin ;
  fc2el_s *pFc ;
  ulong_t mElDupl = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( !pElem->invalid ) {
        pElT = elemType + pElem->elType ;

        for ( nFace = 1 ; nFace <= pElT->mSides ; nFace++ ) {
          pFoE = pElT->faceOfElem + nFace ;
          kVxFace = pFoE->kVxFace ;

          /* Make a list of vertices that form this facet. */
          for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
            pVxFc[kVx] = pElem->PPvrtx[ kVxFace[kVx] ] ;
            if ( !vx_has_markN ( pVxFc[kVx], kMark ) )
              /* Not a bnd vx, can't be a bnd face. */
              break ;
          }

          if ( kVx == pFoE->mVertsFace ) {
            /* All vx on this face are marked, i.e. are boundary nodes. */
            /* Add this potential bnd face to the list. */
            if ( ( nEnt = get_ent_vrtx ( pllVxFc, pFoE->mVertsFace,
                                         ( const vrtx_struct **) pVxFc, &kMin ) ) ) {
              /* The face exists. Fill the other side. */
              pFc = pFc2El+nEnt ;

              if ( pFc->side[1].elem.type == el ) {
                /* The element side is already filled. */
                mElDupl++ ;
                if ( doWarn.intFc ) {
                  sprintf ( hip_msg, "found duplicate face between elements "
                            FUNLOC"." ) ;
                  hip_err ( warning, 4, hip_msg ) ;
                  if ( verbosity > 4 ) 
                    printfc ( pElem, nFace ) ;
                }
              } // if ( pFc->side[1].elem.type ) {
              else {
                /* Fill the other side. */
                pFc->side[1].elem.pElem = pElem ;
                pFc->side[1].elem.kFace = nFace ;
                pFc->side[1].elem.type = el ;
              }
            }

            else {
              /* Empty. Add this face. */
              const int use_side1 = 1, drvFc = 0, kFct=0 ;
              if ( ( nEnt =
                     llFc_add_elem_pvxfct
                     ( &pFc2El, pllVxFc, 
                       pElem, nFace, pFoE->mVertsFace, pVxFc,
                       drvFc, kFct, NULL, use_side1 ) ) ) {
              }
            }
          }  // if ( kVx == pFoE->mVertsFace ) {
        }  // for ( nFace = 1 ; nFace <= pElT->mSides ; nFace++ ) {
      }  //  if ( !pElem->invalid ) {

  
  if ( mElDupl ) {
    sprintf ( hip_msg, "found %"FMT_ULG" duplicated faces between elements.",
              mElDupl ) ;
    hip_err ( warning, 1, hip_msg ) ;
  } 

  

  /* All unmatched sides are future boundary faces. Count, alloc. */
  ulong_t mFc = get_sizeof_llEnt ( pllVxFc ) ; // Allocated size, includes empty slots.
  ulong_t mBndFc = 0 ;
  for ( pFc = pFc2El+1 ; pFc <= pFc2El + mFc ; pFc++ ) {
    if ( pFc->side[0].elem.type && !pFc->side[1].elem.type )
      mBndFc++ ;
  }

  append_bndFc ( pUns->pRootChunk, pUns->mBc, mBndFc ) ;
  bndFc_struct *pBndFc = pUns->pRootChunk->PbndFc ;
  bndFc_struct *pBf = pBndFc ;




  /* Loop over all entries, fill empty sides with boundary faces. */
  int mVxFc ;
  vrtx_struct *pVxFace[MAX_VX_FACE], *pVxFound[MAX_VX_FACE] ;
  int foundFirstMatch, kBcFirstMatch = 0 ;
  for ( pFc = pFc2El+1 ; pFc <= pFc2El + mFc ; pFc++ ) {
    if ( pFc->side[0].elem.type && !pFc->side[1].elem.type ) {
      /* Add a bnd face. */
      pElem = pFc->side[0].elem.pElem ;
      nFace = pFc->side[0].elem.kFace ;

      pFoE = elemType[pElem->elType].faceOfElem + nFace ;
      kVxFace = pFoE->kVxFace ;

      /* Make a list of vertices that form this face. */
      mVxFc = pFoE->mVertsFace ;
      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
        pVxFace[kVx] = pElem->PPvrtx[ kVxFace[kVx] ] ;
      }

      /* Find the first section in the lidx that contains all vx. */
      foundFirstMatch = 0 ;
      for ( kBc = 0 ; kBc < pUns->mBc ; kBc++ ) {
        if ( find_npVx_list ( pVxFace, mVxFc,
                              ppBndVrtx + pUns->pnBvx2Vx_fidx[kBc]-1,
                              pUns->pnBvx2Vx_fidx[kBc+1] - pUns->pnBvx2Vx_fidx[kBc],
                              pVxFound ) ) {
          /* All nodes of this face are in this section of bnd nodes, 
             make a bnd face with this bc. pBndFc indexes from 1. */
          if ( foundFirstMatch ) {
            sprintf ( hip_msg, "found a match for face formed by" ) ;
            for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
              sprintf ( hip_msg + strlen(hip_msg), "%zu, ", pVxFound - ppBndVrtx ) ;
            sprintf ( hip_msg+ strlen(hip_msg),
                      "with bnd %d and %d "FUNLOC".", kBcFirstMatch, kBc ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }
          
          pBf++ ;
          pBf->Pbc = pUns->ppBc[kBc] ;
          pBf->Pelem = pElem ;
          pBf->nFace = nFace ;
          pBf->invalid = 0 ;

          add_bnd2fc ( pFc, pBf, nFace, 0 ) ;
          kBcFirstMatch = kBc ;
        }
      }
    }
  } //  for ( pFc = pfc2el+1 ; pFc <= pfc2el + mFc ; pFc++ ) {





  /* Loop over all entries, there must not remain any unmatched faces. */
  for ( pFc = pFc2El+1 ; pFc <= pFc2El + mFc ; pFc++ ) {
    if ( pFc->side[0].elem.type && !pFc->side[1].elem.type ) {
      sprintf ( hip_msg, "found unmatched boundary face of element %"FMT_ULG","
                " face %d "FUNLOC".",
                pFc->side[0].elem.pElem->number,
                pFc->side[0].elem.kFace ) ;
      if ( !doRemove.listUnMatchedFc )
        hip_err ( fatal, 0, hip_msg ) ;
      else 
        hip_err ( warning, 2, hip_msg ) ;
    }
  }


  arr_free ( pllVxFc ) ;
  arr_free ( pFc2El ) ;

  arr_free ( ppBndVrtx ) ;

  release_vx_markN ( pUns, 0 ) ;


  return ( 1 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  llFc_list_unmatched:
*/
/*! make a bc of unmatched faces.
 *
 *
 */

/*
  
  Last update:
  ------------
  15Apr18: conceived.
  

  Input:
  ------
  pUns
  pllVxFc: list of faces.
  mOpenFc: number of open faces.

  Returns:
  --------
  1 on failure, 0 on success
  
*/

void llFc_list_unmatched ( uns_s *pUns, llVxEnt_s *pllVxFc, fc2el_s *pfc2el, int mOpenFc ) {

  if (!mOpenFc )
    return ;

  //make_uns_bndPatch ( pUns ) ;



  sprintf ( hip_msg, "adding %d unmatched faces to new bc ''hip_unmatched''", mOpenFc ) ;
  hip_err ( info, 1, hip_msg ) ;

  /* Add a bc. */
  bc_struct *pBc = find_bc ( "hip_unmatched", 1 ) ;
  pUns->mBc++ ;

  /* Reallocate root chunk bnd faces. */
  chunk_struct *pChunk = pUns->pRootChunk ;
  ulong_t mBndFaces = pChunk->mBndFaces + mOpenFc ;
  bndFc_struct *pBndFc = pChunk->PbndFc
    = arr_realloc ( "pBndFc in llFc_list_unmatched", pUns->pFam,
                    pChunk->PbndFc,mBndFaces+1, sizeof (*pBndFc) ) ;
  bndFc_struct *pBF = pBndFc + pChunk->mBndFaces ;
  pChunk->mBndFaces = mBndFaces ;

  /* Fill. */
  /* Loop over all entries, list holes. */
  ulong_t mFc = get_sizeof_llEnt ( pllVxFc ) ;
  ulong_t nEnt ;
  fc2el_s *pFc ;
  for ( nEnt = 1 ; nEnt <= mFc ; nEnt++ ) {
    pFc = pfc2el+nEnt ;
    if ( pFc->side[0].elem.type && !pFc->side[1].elem.type ) {
      /* Hole. */
      pBF++ ;
      if ( pBF > pChunk->PbndFc + mBndFaces )
        hip_err ( fatal, 0,
                  "exceeded expected number of unmatched faces in llFc_list_unmatched." ) ;
      pBF->Pelem = pFc->side[0].elem.pElem ;
      pBF->nFace = pFc->side[0].elem.kFace ;
      pBF->Pbc = pBc ;
      pBF->invalid = 0 ;
    }
  }


  /* Reconstruct index structures. */
  make_uns_bndPatch ( pUns ) ;
  
  return ;
}


/******************************************************************************

  check_conn:
  Check one-to-one connectivity of element faces.
  
  Last update:
  ------------
  25Feb20; fix bug with test for unmatched faces not executed. 
           New interface to make_llfc
  4Sep19; use viz_elems_vtk
  8Jun18; only give exit zero for duplication if duplicates are not removed.
  21Mar18; use doWarn.abortFc.
  17May16; track geoType changes with *pBcGeoType_change arg.
  19Dec16; track pUns->numberedType.
  16Dec16; exclude surface meshes from connectivity check.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  1Apr12; move warnings into make_llFc.
  29Jun07; fix bug with pVrtx in printout.
  
  Input:
  ------
  pUns: grid
  
  Output:
  *pBcGeoType_dhange = non-zero if any faces were found to be duplicates.

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int check_conn ( uns_s *pUns, int *pBcGeoType_change ) {
  

  *pBcGeoType_change = 0 ;

  if ( pUns->specialTopo == surf ) {
    /* no elements to be checked, all fine. */
    return ( 1 ) ;
  }

  
  if ( verbosity > 4 )
    hip_err ( info, 1, "Checking unstructured grid for matching connectivity." ) ;


  fc2el_s *pfc2el, *pFc ;
  ulong_t mFcBecomeInt, mIntFcDupl, mBndFcDupl, mFcRemoved ;
  ulong_t mFcUnMtch ;
  /* Warnings following non-zero &mFcBecomeInt, &mFcDupl, &mFcRemoved, &mFcUnMtch
     now issued from within make_llFc. */
  llVxEnt_s *pllVxFc = make_llFc ( pUns, bnd, &pfc2el, doWarn.bndFc, doRemove.bndFc, 1,
                                   &mFcBecomeInt, &mIntFcDupl, &mBndFcDupl ) ;

  ulong_t mFc, mOpenFc = 0, nEnt ;
  int kVx, kFace ;
  vrtx_struct *pVrtx[MAX_VX_FACE] ;
  elem_struct *pElem ;
  ulong_t mElViz = 0 ;
  chunk_struct *pChunk = NULL ;
  bndPatch_struct *pBP = NULL ;
  bndFc_struct *pBF, *pBndFcF, *pBndFcL ;
  const elem_struct **ppElViz = NULL ;
  if ( mFcBecomeInt || mIntFcDupl || mBndFcDupl )
    *pBcGeoType_change = 1  ;

  /* Check whether entire boundaries have become internal, flag those. 
     We have a valid pUns->mBc, but not a valid linked list of bndPatches for
     each bc, so can't use loop_bndFaces_bc. */
  int bc_hasOneFc_bnd = 0 ;
  int bc_hasOneFc_int = 0 ;
  int kBc ;
  bc_struct *pBc ;
  for ( kBc = 0; kBc < pUns->mBc ; kBc++ ) {
    pBc = pUns->ppBc[kBc] ;
      
    pChunk = NULL ;
    bc_hasOneFc_bnd = 0 ;
    bc_hasOneFc_int = 0 ;
    while ( loop_bndFaces ( pUns, &pChunk, &pBP, &pBndFcF, &pBndFcL ) ) {
      for ( pBF = pBndFcF ; pBF <= pBndFcL ; pBF++ )
        if ( pBF->Pbc == pBc ) {
          if ( pBF->geoType == bnd )
            bc_hasOneFc_bnd = 1 ;
          else if ( pBF->geoType == inter )
            bc_hasOneFc_int = 1 ;
        }
    }

    if ( bc_hasOneFc_int && !bc_hasOneFc_bnd ) {
      /* All faces of the bc are/have become interior. */
      pBc->geoType = inter ;
    }
  }


  /* Loop over all entries, list holes. */
  mFc = get_sizeof_llEnt ( pllVxFc ) ;
  for ( nEnt = 1 ; nEnt <= mFc ; nEnt++ ) {
    pFc = pfc2el+nEnt ;
    if ( pFc->side[0].elem.type && !pFc->side[1].elem.type ) {
      /* Hole. */
      if ( verbosity > 4 ) {
        sprintf ( hip_msg, "unmatched edge/face between" ) ;
        show_ent ( pllVxFc, nEnt, pVrtx ) ;
        pElem = pFc->side[0].elem.pElem ;
        kFace = pFc->side[0].elem.kFace ;
        for ( kVx = 0 ; kVx < MAX_VX_FACE ; kVx++ )
          if ( pVrtx[kVx] )
            sprintf ( strchr(hip_msg, '\0' ), " %"FMT_ULG",", pVrtx[kVx]->number ) ;
        sprintf ( strchr(hip_msg, '\0' ), " from element %"FMT_ULG", face %d.\n", pElem->number, kFace ) ;
        hip_err ( warning, 1, hip_msg ) ;
        if ( verbosity > 5 )
          printfcco( pElem, kFace ) ;
#ifdef DEBUG
        if ( verbosity > 6 )
          add_viz_el ( pElem, &ppElViz, &mElViz ) ;
#endif
      }
      mOpenFc++ ;
    }
  }


  if ( mOpenFc ) {
    sprintf ( hip_msg, "found %"FMT_ULG" unmatched faces/edges.", mOpenFc ) ;
    if ( doWarn.abortFc ) {
      hip_err ( fatal, 0, hip_msg ) ; }
    else {
      hip_err ( warning, 1, hip_msg ) ;}

    if ( doRemove.listUnMatchedFc )
      llFc_list_unmatched ( pUns, pllVxFc, pfc2el, mOpenFc ) ;

#ifdef DEBUG
    if ( verbosity > 6 ) {
      viz_elems_vtk ( "unmatched_fc_elems.vtk", mElViz, ppElViz, NULL ) ;
      arr_free ( ppElViz ) ;
    }
#endif
      
  }


  free_llEnt ( &pllVxFc ) ;
  arr_free ( pfc2el ) ;

  if ( mOpenFc
       || ( !doRemove.intFc && mIntFcDupl )
       || ( !doRemove.bndFc && mBndFcDupl ) ) {
    pUns->numberedType = invNum ;
    return ( 0 ) ;
  }
  else
    return ( 1 ) ;
}


/******************************************************************************

  make_elGraph:
  Build a graph from the element to element connectivity.
  
  Last update:
  ------------
  25Feb20; new interface to make_llfc
  3Oct19; make work for ulong_t
  18May08; add description of arguments.
  1Apr05: conceived.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  *pmXAdj   = number of elements in the graph
  *ppXAdj   = index of the first graph edge with each element
  *pmAdjncy = number of edges in the graph
  *ppAdjncy = list of edges in the graph

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int elEg_cmp0 ( const void *p1, const void *p2 ) {
  return ( ( ( int * ) p1 )[0] - ( ( int * ) p2 )[0] ) ;
}

int elEg_cmp1 ( const void *p1, const void *p2 ) {
  return ( ( ( int * ) p1 )[1] - ( ( int * ) p2 )[1] ) ;
}


int make_elGraph ( uns_s *pUns, ulong_t *pmXAdj, ulong_t **ppXAdj, 
                   ulong_t *pmAdjncy, ulong_t **ppAdjncy ) {
  
  fc2el_s *pfc2el, *pFc ;
  ulong_t mFcBecomeInt=0, mIntFcDupl=0, mBndFcDupl=0, mFcRemoved=0, mFc, nEnt, mFcUnMtch, *pAdj, 
    mEl=pUns->mElemsNumbered, mEg, nEl0, *pAdj0, nEl, mEl0 ;
  ulong_t *adjncy, *xAdj, *pAdjEnd, *pxAdj ;
  elem_struct elMax ;

  elMax.number = pUns->mVertsNumbered+1 ;

  llVxEnt_s *pllVxFc = make_llFc ( pUns, bnd, &pfc2el, doWarn.bndFc, doRemove.bndFc, 0,
                                   &mFcBecomeInt, &mIntFcDupl, &mBndFcDupl ) ;



  /* Make a list of "edges"  from el to el. List each edge twice, once
     in either direction.  Add an extra element as
     the sorting loop will read one element beyond validity.*/
  mFc = get_sizeof_llEnt ( pllVxFc ) ;
  *ppAdjncy = adjncy = arr_malloc ( "adjncy in make_elGraph", 
                                    pUns->pFam, 4*mFc+1, sizeof( **ppAdjncy  ) ) ;


  /* List all valid edges. */
  mEg = 0 ;
  mFc = get_sizeof_llEnt ( pllVxFc ) ;
  for ( pAdj = adjncy, nEnt = 1 ; nEnt <= mFc ; nEnt++ ) {
    pFc = pfc2el+nEnt ;
    if ( pFc->side[0].elem.type && pFc->side[1].elem.type ) {
      mEg++ ;

      *pAdj++ = pFc->side[0].elem.pElem->number - 1 ;
      *pAdj++ = pFc->side[1].elem.pElem->number - 1 ;
      *pAdj++ = pFc->side[1].elem.pElem->number - 1 ;
      *pAdj++ = pFc->side[0].elem.pElem->number - 1 ;
    }
  }


  free_llEnt ( &pllVxFc ) ;
  arr_free ( pfc2el ) ;
  



  /* Sort the list for first element number. */
  qsort ( adjncy, 2*mEg, 2*sizeof( *pAdj ), elEg_cmp0 ) ;


  /* Allocate for pointers for each element into adjncy. */
  *ppXAdj = xAdj = arr_malloc ( "xAdj in make_elGraph", 
                                pUns->pFam, mEl+1, sizeof( **ppXAdj ) ) ;


  xAdj[0] = 0 ;

  nEl0 = 0 ;
  pAdj0 = adjncy ;
  pAdjEnd = adjncy + 4*mEg ;
  for ( pAdj = adjncy ; pAdj <= pAdjEnd ; pAdj+=2 ) {

    if (  pAdj == pAdjEnd ) 
      nEl = mEl ;
    else
      nEl = pAdj[0] ;

    if ( nEl != nEl0 ) {
      /* First element has changed. List the pointer for the previous element. */
      xAdj[nEl] = (pAdj-adjncy)/2 ;

      /* Sort the second element entries for the previous first element. */
      mEl0 = (pAdj-pAdj0)/2 ;
      qsort ( pAdj0, mEl0, 2*sizeof( *pAdj ), elEg_cmp1 ) ;

      nEl0 = nEl ;
      pAdj0 = pAdj ;
    }
  }


  *pmXAdj = mEl ;
  *pmAdjncy = 2*mEg ;

  /* offset indices and adjacency entries to count from 1. */
  for ( pxAdj = xAdj ; pxAdj < xAdj+*pmXAdj+1 ; pxAdj++ )
    (*pxAdj)++ ;
  for ( pAdj = adjncy ; pAdj < pAdjEnd ; pAdj++ )
    (*pAdj)++ ;

    

  return ( 1 ) ;
}
