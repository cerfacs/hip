/*
   write_avbp3_adapt.c:
   Write a set of files in AVBP 4.2 format. Adaptive hierarchy.


   Last update:
   ------------


   This file contains:
   -------------------

*/
#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern const char version[] ;
extern const int perBc_in_exBound ;

extern char hip_msg[] ;


/******************************************************************************

  write_uns_avbp3:
  Write the un-chunked consistent mesh to avbp files. In addition to
  the standard files for the finest level, write files for the parents.
  Format: AVBP 4.2.
  
  Last update:
  4May96: conceived.
  
  Input:
  ------
  pUns:
  mDim:         
  
  Returns:
  --------
  1
  
*/

int write_avbp3_adapt ( uns_s *pUns, char *ProotFile )
{
  char outFile[MAX_BC_CHAR+15], prtFile[MAX_BC_CHAR+4] ;
  int mEqu, mPrtElems ;
  FILE *adpFile = NULL, *visFile ;

  if ( !pUns->validGrid ) {
    printf ( " FATAL: you were told that this grid is invalid, weren't you?.\n" ) ;
    return ( 0 ) ; }
  else if ( (check_bnd_setup ( pUns )).status != success ) {
    printf ( " FATAL: cannot write grid without proper boundary setup.\n" ) ;
    return ( 0 ) ; }

  
  prepend_path ( ProotFile ) ;
  if ( verbosity > 0 )
    printf ( "   Writing finest adapted grid to AVBP 4.2 as %s\n", ProotFile ) ;
  
  /* mEqu = pUns->mUnknowns ; */
  if ( pUns->mDim == 3 )
    mEqu = 5 ;
  else if ( pUns->mDim == 2 ) {
    mEqu = 4 ;
    /* In 2-D, write a 'master-file' for dplot. */
    strcpy ( outFile, ProotFile ) ;
    strcat ( outFile, ".adp" ) ;
    if ( !( adpFile = fopen ( outFile, "w" ) ) ) {
      printf ( " FATAL: file: %s could not be opened.\n", outFile ) ;
      return ( 0 ) ; }
    else
      fprintf ( adpFile, "avbp 4.2\n" ) ;
  }

  /* In any-D, write a 'master-file' for visual. */
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".visual" ) ;
  if ( !( visFile = fopen ( outFile, "w" ) ) ) {
    printf ( " FATAL: file: %s could not be opened.\n", outFile ) ;
    return ( 0 ) ; }
  else
    fprintf ( visFile,
	      "' Masterfile for visual3/AVBP 4.2 by hip version %s.'\n", version ) ;

  
  /* Number the leaf elements only. */
  number_uns_elem_leafs ( pUns ) ;
  /* Count the number of active boundary patches. This sets PbndPatch->mBndFcMarked
     for the current numbering of elements. */
  count_uns_bndFaces ( pUns ) ;
  /* Remove all periodic bc, if they are not supposed to be listed. */
  if ( !perBc_in_exBound ) rm_perBc ( pUns ) ;

  /* Make pairs of periodic nodes and faces. */
  if ( ! match_per_faces ( pUns ) ) {
    printf ( " FATAL: failed to establish periodicity in write_avbp3.\n" ) ;
    return ( 0 ) ; }
  if ( !special_verts ( pUns ) ) {
    printf ( " FATAL: failed to match periodic vertices in write_avbp3_inBound.\n" ) ;
    return ( 0 ) ; }

  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".sol" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_sol ( pUns, outFile, v4_7 ) ;
    
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".coor" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_coor ( pUns, outFile ) ;

  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".conn" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_conn ( pUns, outFile ) ;
  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".exBound" ) ;
  if ( adpFile ) fprintf ( adpFile, "%s\n", outFile ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_exBound ( pUns, outFile ) ;
  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".inBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_inBound ( pUns, outFile ) ;
  
  strcpy ( outFile, ProotFile ) ;
  strcat ( outFile, ".asciiBound" ) ;
  fprintf ( visFile, "'%s'\n", outFile ) ;
  write_avbp_asciiBound_4p7 ( pUns, outFile ) ;

  if ( adpFile )
    fclose ( adpFile ) ;

  /* Write the parents. */
  strcpy ( prtFile, ProotFile ) ;
  strcat ( prtFile, ".prt" ) ;
  if ( verbosity > 0 ) {
    sprintf ( hip_msg, "    Writing coarser grids to AVBP 4.2 as %s\n", prtFile ) ;
    hip_err ( blank, 1, hip_msg ) ;
  }

  /* Number children and parents. Write the parents to file, if there are any. */
  write_avbp3_prt ( pUns, prtFile, &mPrtElems ) ;

  if ( mPrtElems ) {
    fprintf ( visFile, "'%s'\n", prtFile ) ;
    /* Number and count the parent elements only.
    was:
    number_uns_elemFromVerts ( pUns, parent ) ;  */
    number_uns_elems_by_type ( pUns, parent, tri, hex, 1 ) ;
    count_uns_bndFaces ( pUns ) ;
    if ( !perBc_in_exBound ) rm_perBc ( pUns ) ;

    /* Make pairs of periodic nodes and faces. */
    if ( ! match_per_faces ( pUns ) ) {
      printf ( " FATAL: failed to establish periodicity in write_avbp3.\n" ) ;
      return ( 0 ) ; }
    if ( !special_verts ( pUns ) ) {
      printf ( " FATAL: failed to match periodic vertices in write_avbp3_inBound.\n" ) ;
      return ( 0 ) ; }
    

    strcpy ( outFile, prtFile ) ;
    strcat ( outFile, ".exBound" ) ;
    fprintf ( visFile, "'%s'\n", outFile ) ;
    write_avbp_exBound ( pUns, outFile ) ;
  
    strcpy ( outFile, prtFile ) ;
    strcat ( outFile, ".inBound" ) ;
    fprintf ( visFile, "'%s'\n", outFile ) ;
    write_avbp_inBound ( pUns, outFile ) ;
  }
  
  fclose ( visFile ) ;

  /* Turn periodic boundaries back on. */
  number_uns_elem_leafs ( pUns ) ;
  count_uns_bndFaces ( pUns ) ;
  return ( 1 ) ;
}



/******************************************************************************

  write_avbp3_prt:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_avbp3_prt ( uns_s *pUns, char *PparentFile, int *pmPrtElems )
{
  FILE *Fparent ;
  const chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  const int justZeros[2] = {0}, justOnes[2] = {1} ;
  int mLeafElems, mPrtElem2VertP, mPrtElem2ChildP,
      mPrtBndPatches, mPrtBndFaces, mChildren, mParentWritten, nRefType, kVx, kCh,
      number[MAX_VX_ELEM], mPrtElemsPerType[MAX_ELEM_TYPES];

  /* Count the and renumber the leaf and the parent elements. */
  number_uns_elem_leafNprt ( pUns, &mLeafElems, pmPrtElems, mPrtElemsPerType,
			     &mPrtElem2VertP, &mPrtElem2ChildP,
			     &mPrtBndPatches, &mPrtBndFaces ) ;
  if ( !*pmPrtElems )
    /* There are no parents to write. */
    return ( 1 ) ;
  
  /* Open parent file. */
  if ( verbosity > 2 )
    printf ( "      parents to %s\n", PparentFile ) ;

  if ( ( Fparent = fopen ( PparentFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PparentFile ) ;
    return ( 0 ) ; }

  FWRITE ( pmPrtElems, sizeof( int ), 1, Fparent ) ;
  FWRITE ( &mLeafElems, sizeof( int ), 1, Fparent ) ;
  FWRITE ( &mPrtElem2VertP, sizeof( int ), 1, Fparent ) ;
  FWRITE ( &mPrtElem2ChildP, sizeof( int ), 1, Fparent ) ;
  FWRITE ( &mPrtBndPatches, sizeof( int ), 1, Fparent ) ;
  FWRITE ( &mPrtBndFaces, sizeof( int ), 1, Fparent ) ;

  /* List the parents. */
  for ( mParentWritten = 0, Pchunk = pUns->pRootChunk ;
	Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( !Pelem->leaf && !Pelem->invalid ) {
	mParentWritten++ ;
	/* This is a parent element that is not a leaf.
	   Find the number of children, the type of refinement (ref or buf).*/
	if ( Pelem->PrefType->refOrBuf == ref )	{
	  nRefType = Pelem->PrefType->nr ;
	  mChildren = Pelem->PrefType->mChildren ;
	}
	else if ( Pelem->PrefType->refOrBuf == buf ) {
	  nRefType = -1 ;
	  for ( mChildren = 0 ;
	        Pelem->PPchild[mChildren] &&
	        Pelem->PPchild[mChildren]->Pparent == Pelem ; mChildren++ )
	    ;
	}
	else
	  printf ( " FATAL: found a parent that is neither buffered, nor refined"
		   " in write_avbp3_prt.\n" ) ;

	/* The element type. Write to number to force the cast to a full int. */
	number[0] = Pelem->elType ;
	FWRITE ( number, sizeof( int ), 1, Fparent ) ;
	/* Is this parent a root? */
	if ( Pelem->root )
	  FWRITE ( justOnes, sizeof ( int ), 1, Fparent ) ;
	else
	  FWRITE ( justZeros, sizeof( int ), 1, Fparent ) ;

	/* The parent, if there is one. */
	if ( Pelem->Pparent ) {
	  number[0] = Pelem->Pparent->number ;
	  FWRITE ( number, sizeof ( int ), 1, Fparent ) ;
	}
	else
	  FWRITE ( justZeros, sizeof( int ), 1, Fparent ) ;

	/* The refinement type, -1 for buffering. */
	FWRITE ( &nRefType, sizeof( int ), 1, Fparent ) ;
	FWRITE ( &mChildren, sizeof( int ), 1, Fparent ) ;

	/* The forming vertices. */
	for ( kVx = 0 ; kVx < elemType[Pelem->elType].mVerts ; kVx++ )
	  number[kVx] = Pelem->PPvrtx[kVx]->number ;
	FWRITE ( number, sizeof( int ), elemType[Pelem->elType].mVerts, Fparent ) ;

	/* List the children. */
	for ( kCh = 0 ; kCh < mChildren ; kCh++ ) {
	  number[0] = Pelem->PPchild[kCh]->number ;
	  number[1] = Pelem->PPchild[kCh]->leaf ;
	  FWRITE ( number, sizeof ( int ), 2, Fparent ) ;
	}
      }

  if ( mParentWritten != *pmPrtElems ) {
    printf ( " FATAL: miscount in the parent elements"
	     " %d written, %d expected, in write_avbp3_prt.\n",
	     mParentWritten, *pmPrtElems ) ;
    fclose ( Fparent ) ;
    return ( 0 ) ; }

  fclose ( Fparent ) ;
  return ( 1 ) ;
}

