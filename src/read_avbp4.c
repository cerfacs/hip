 /*
   read_uns_avbp4.c:
   Read a set of files in AVBP 4.0 format.


   Last update:
   ------------
   18Dec10; new pBc->type
   13Feb99 ; pack elMark.

   This file contains:
   -------------------

*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern double Gamma, GammaM1 ;
extern const int bitEdge[MAX_EDGES_ELEM] ;
extern const int dg_fix_lrgAngles ;

/******************************************************************************

  read_uns_avbp4:
  Read the master file.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  6Sep18; new arg doCheck for merge_uns.
  4Sep18; new interface to merge_uns.
  21Mar18; fix scope of ADAPT_REF, use hip_err.
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
  17Dec10; fix bug with TEXT_LEN in r1_fopen
  15Sep10; new interface to merge_uns.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_uns_avbp4 ( char masterFile[TEXT_LEN] ) {

  FILE *Fmaster, *Fcoor=NULL, *Fsol=NULL, *Fconn=NULL, 
       *FinBound=NULL, *FexBound=NULL, *FasciiBound=NULL,
       *Fparent, *Fopened, *FelMark=NULL, *FadEdge=NULL ;
  grid_struct *Pgrid ;
  chunk_struct *PcoarseChunk = NULL, *PfineChunk ;
  char fileName[TEXT_LEN] ;
  int partNumber, found ;
  uns_s *pUns ;

  prepend_path ( masterFile ) ;
  if ( !( Fmaster = fopen ( masterFile, "r" ) ) ) {
    printf ( " FATAL: file named:%s not found in read_uns_avbp4.\n", masterFile ) ;
    return ( 0 ) ; }
  fgets( fileName, TEXT_LEN, Fmaster ) ;
  if ( !strstr ( fileName, "AVBP 4-" ) ) {
    printf ( " SORRY: read_uns_avbp4 does only AVBP 4- files.\n" ) ;
    return ( 0 ) ; }

  /* This will be the next grid. */
  partNumber = Grids.mGrids + 1 ;
  printf ( "  Reading unstructured avbp as part %d.\n", partNumber ) ;

  /* Read the filenames for the finest level from the master file. */
  found = 0 ; Fopened = NULL ;
  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
    if ( ( Fopened = Fsol = r1_fopen (prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
      if ( fgets( fileName, TEXT_LEN, Fmaster ) )
	if ( ( Fopened = Fcoor =
               r1_fopen (prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
	  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
	    if ( ( Fopened = Fconn =
                   r1_fopen (prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
	      if ( fgets( fileName, TEXT_LEN, Fmaster ) )
		if ( ( Fopened = FexBound =
                       r1_fopen (prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
		  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
		    if ( ( Fopened = FinBound =
                           r1_fopen (prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
		      if ( fgets( fileName, TEXT_LEN, Fmaster ) )
			if ( ( Fopened = FasciiBound =
                               r1_fopen (prepend_path(fileName ), TEXT_LEN, "r" ) ) )
			  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
			    if ( ( Fopened = FelMark = 
				   r1_fopen (prepend_path(fileName),TEXT_LEN,"r") ) )
			      if ( fgets( fileName, TEXT_LEN, Fmaster ) )
                                if ( ( Fopened = FadEdge = 
                                       r1_fopen ( prepend_path(fileName),
                                                  TEXT_LEN,"r") ) )
                                  found = 1 ;
  if ( !Fopened ) {
    fclose ( Fsol ) ; fclose ( Fcoor ) ; fclose ( Fconn ) ;
    fclose ( FinBound ) ; fclose ( FexBound ) ; fclose ( FasciiBound ) ;
    fclose ( FadEdge ) ;

    sprintf ( hip_msg, 
              "could not open the fine grid files in read_uns_avbp4." ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( !found ) {
    fclose ( Fsol ) ; fclose ( Fcoor ) ; fclose ( Fconn ), fclose ( FelMark ) ;
    fclose ( FinBound ) ; fclose ( FexBound ) ; fclose ( FasciiBound ) ;
    fclose ( FadEdge ) ;

    hip_err ( fatal, 0, "could not read the master file in read_uns_avbp4." ) ;
  }



  /* Allocate a chunk. */
  if ( !( pUns = make_uns (NULL) ) || !( PfineChunk = make_chunk ( pUns ) ) ) {
    printf ( " FATAL: failed to alloc a new unstructured chunk in read_uns_avbp4.\n" ) ;
    fclose ( Fsol ) ; fclose ( Fcoor ) ; fclose ( Fconn ) ;
    fclose ( FinBound ) ; fclose ( FexBound ) ; fclose ( FasciiBound ) ;
    return ( 0 ) ; }
  pUns->pRootChunk = PfineChunk ;
  PfineChunk->nr = 1 ;

  /* read_adEdge uses the llEdge class, which needs an initialized pUns. */
  make_uns_ppChunk ( pUns ) ;

  /* Read coordinates, solution and connectivity of the finest level. */
  found = 0 ;
  if ( read_avbp_coor ( Fcoor, pUns, PfineChunk ) )
    if ( read_avbp_sol ( Fsol, pUns, PfineChunk ) )
#ifdef ADAPT_HIERARCHIC
      if ( read_adEdge ( pUns, FadEdge ) )
#endif
        if ( read_avbp4_conn ( Fconn, FelMark, pUns, PfineChunk->Pvrtx ) )
          /* Read the boundary information of the finest level. */
          if ( read_avbp_asciiBound ( FasciiBound, pUns ) )
            if ( read_avbp_exBound ( FexBound, pUns, PfineChunk ) )
              if ( read_avbp_inBound ( FinBound, pUns, PfineChunk ) )
                found = 1 ;
  if ( !found ) {
    printf ( " FATAL: could not read the finest level in read_uns_avbp4.\n" ) ;
    return ( 0 ) ; }
  fclose ( Fsol ) ; fclose ( Fcoor ) ; fclose ( Fconn ) ;
  fclose ( FinBound ) ; fclose ( FexBound ) ; fclose ( FasciiBound ) ;

  /* Read the filenames for the coarser levels from the master file. */
  found = 0 ; Fopened = NULL ;
  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
    if ( ( Fopened = Fparent =
	   r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
      if ( fgets( fileName, TEXT_LEN, Fmaster ) )
	if ( ( Fopened = FexBound =
	       r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
	  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
	    if ( ( Fopened = FinBound =
		   r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) )
              found = 1 ;
  if ( found ) {
    if ( !Fopened ) {
      printf ( " FATAL: could not open the coarse grid files in read_uns_avbp4.\n" ) ;
      fclose( Fparent ) ; fclose ( FinBound ) ; fclose ( FexBound ) ; 
      return ( 0 ) ;
    }

    /* Read the parent elements. Note that the parent's vertices are
       stored with the finest level. */
    found = 0 ;
    /* Set the parent and children pointers, set all parent element numbers
       to zero. */
#ifdef ADAPT_HIERARCHIC
    if ( read_avbp3_prt ( Fparent, pUns, &PcoarseChunk ) )
#endif
      /* Read the boundary information of the coarser level. */
      if ( read_avbp_exBound ( FexBound, pUns, PcoarseChunk ) )
	if ( read_avbp_inBound ( FinBound, pUns, PcoarseChunk ) )
          found = 1 ;
    if ( !found ) {
      printf ( " FATAL: could not read the coarser level in read_uns_avbp4.\n" ) ;
      return ( 0 ) ; }
    fclose ( Fparent ) ; fclose ( FinBound ) ; fclose ( FexBound ) ; 
  }

  /* Make sure there are no coincident vertices. */
  set_degenVx ( pUns ) ;
  
  /* Unstructured. Make a new grid. */
  if ( !( Pgrid = make_grid () ) )
    hip_err ( fatal, 0, " malloc for the linked list of grids"
	     " failed in read_uns_avpb." ) ;

  /* Put the chunk into Grids. */
  Pgrid->uns.type = uns ;
  Pgrid->uns.pUns = pUns ;
  Pgrid->uns.mDim = pUns->mDim ;
  Pgrid->uns.pVarList =  &(pUns->varList) ;
  pUns->nr = Pgrid->uns.nr ;
  pUns->pGrid = Pgrid ;
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = Pgrid ;
  

  if ( dg_fix_lrgAngles ) {
    /* Check for large hex angles, split into prisms. */
    if ( !merge_uns ( pUns, 0, 1 ) ) {
      hip_err ( fatal, 0, "merging of unstructured grids in read_uns_avbp4 failed." ) ;
    }
  }
  else
    /* Validate the grid. */
    check_uns ( pUns, check_lvl ) ;
      
  return ( 1 ) ;
}

/******************************************************************************

  read_avbp4_conn:
  Read the connectivity.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  30Apr19; revive ADAPT_REF
  6Apr13; promote possibly large int to type ulong_t.
  10Jul11; avoid drvElem without ADAPT_REF.
  13Feb99; pack elMark. 
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp4_conn ( FILE *Fconn, FILE *FelMark, uns_s *pUns, vrtx_struct *PbaseVx ) {
  
  const elemType_struct *pElT ;
  const faceOfElem_struct *pFoE ;
  const vrtx_struct *pVxEg[MAX_EDGES_ELEM], **ppVx, *pVrtx1, *pVrtx2 ;
  
  llEdge_s *pllAdEdge = pUns->pllAdEdge ;
  int someInt[MAX_REFINED_VERTS], mElems, mElemTypes, mVertsElem, mNewE2V, nElT,
    mElemsThisType, kVx, drvElem = 0, mVertsBaseElem, nLstE2V = -1,
    dir, kEg, kFc, mVxHg, nAe, nAeFix=0, found ;
#ifdef ADAPT_HIERARCHIC
  unsigned int elInt[2] ;
#endif
  elem_struct *pElem, *PlstElemPrvType ;
  vrtx_struct **PPfrstE2V ;
  elType_e elType ;
  elMark_s elMark ;
  chunk_struct *Pchunk = pUns->pRootChunk ;
  double volume ;

  if ( verbosity > 1 )
    hip_err ( blank, 1, "   Reading mesh connectivity.\n" ) ;

  if ( sizeof( elMark_s ) != 2*sizeof( int ) ) {
    hip_err ( fatal, 0, "sizeof( elMark_s ) != 2*sizeof( int ) in read_avbp4_conn.\n" ) ;
  }

  /* Try to read the elMark file header. */
  if ( FREAD ( someInt, sizeof ( int ), 4, FelMark ) != 4 ||
       someInt[0] != 1*sizeof( int ) || someInt[3] != someInt[1]*2*sizeof( int ) )
    /* Invalid elMark file. */
    FelMark = NULL ;

  /* We need to have an array of chunks and compound pointers for the edge list. */
  make_uns_ppChunk ( pUns ) ;

  /* mBlocks, mTElems, mDummies. */
  if ( FREAD ( someInt, sizeof( int ), 5, Fconn ) !=5 ||
       someInt[0] != 3*sizeof ( int ) ) {
    hip_err ( fatal, 0, "error reading mBlocks, mTElems, mDummies in read_avbp4_conn." ) ;
   }
  else if ( someInt[1] != 1 ) {
    hip_err ( warning, 1, "read_avbp4_conn can only deal with 1-block avbp files." ) ;
    return ( 0 ) ;
  }

  /* Malloc for the elements. */
  Pchunk->mElems = mElems = someInt[2] ;
  Pchunk->Pelem = arr_malloc ( "Pchunk->Pelem", pUns->pFam,  mElems+1, sizeof( elem_struct ) ) ;
  if ( !Pchunk->Pelem ) {
    printf ( " FATAL: could not allocate the for the %d elements"
	     " in read_avbp4_coor.\n", mElems ) ;
    return ( 0 ) ; }
  Pchunk->mElem2VertP = 0 ;
  Pchunk->PPvrtx = NULL ;

  /* mDummies, mElemTypes. */
  if ( FREAD ( someInt, sizeof( int ), 4, Fconn ) !=4 ||
       someInt[0] != 2*sizeof ( int ) ) {
    hip_err ( fatal, 0, "error reading mDummies, mElemTypes in read_avbp4_conn.\n" ) ;
    return ( 0 ) ; }

  if ( someInt[2] == 0 )
    mElemTypes = 30 ;
  else {
    hip_err ( fatal, 0, "not the right number of elements in read_avbp4_conn.\n" ) ;
    return ( 0 ) ; }
  
  /* Loop over all element types. */
  for ( PlstElemPrvType = Pchunk->Pelem, nElT = 0 ; nElT < mElemTypes ; nElT++ ) {
    /* elT, begin, mElems. */
    if ( FREAD ( someInt, sizeof( int ), 5, Fconn ) !=5 ||
	 someInt[0] != 3*sizeof ( int ) ) {
      sprintf ( hip_msg, "error reading elType, begin, mElemsThisType for elType %d"
	       " in read_avbp4_conn.\n", nElT ) ;
      hip_err ( fatal, 0, hip_msg ) ;
      return ( 0 ) ; }
    mVertsElem = mVertsBaseElem = someInt[1] ;
    mElemsThisType = someInt[3] ;

    if ( mElemsThisType ) {
      /* Find the corresponding element type. */
      if ( nElT <= hex ) {
	elType = ( elType_e ) nElT ;
	mVertsElem = elemType[nElT].mVerts ;
      }
      else {
	/* Derived element. */
	drvElem = 1 ;
	elType = noEl ;
	mVertsElem = nElT - MAX_ELEM_TYPES + 4 ;
      }
      
      /* Realloc for the new element to vert pointers. There may be at most
	 MAX_VX_ELEM verts per element*/
      mNewE2V = nLstE2V + 1 + MAX_VX_ELEM*mElemsThisType ;
      Pchunk->PPvrtx = arr_realloc ( "Pchunk->PPvrtx in read_avbp4_coor", pUns->pFam,
                                     Pchunk->PPvrtx, mNewE2V, sizeof( vrtx_struct * ) ) ;
      Pchunk->mElem2VertP = mNewE2V ;
      
#     ifdef CHECK_BOUNDS
        if ( PlstElemPrvType + mElemsThisType > Pchunk->Pelem + Pchunk->mElems ||
	     nLstE2V + mElemsThisType*MAX_VX_ELEM >= Pchunk->mElem2VertP )
	  hip_err ( fatal, 0, "beyond elem bounds in read_avbp4_conn." ) ;
#     endif

      /* Check the size of the record of this element type. */
      if ( FREAD ( someInt, sizeof( int ), 1, Fconn ) != 1 ||
	   someInt[0] != mVertsElem * mElemsThisType * sizeof ( int ) ) {
	sprintf ( hip_msg, "wrong size in connectivity of elements of type %d"
		 " in read_avbp4_conn.\n", nElT ) ;
	hip_err ( fatal, 0, hip_msg ) ; }
      
      /* Read all elements of this type. */
      for ( pElem = PlstElemPrvType+1 ;
	    pElem <= PlstElemPrvType + mElemsThisType ; pElem++ ) {
#ifdef ADAPT_HIERARCHIC
	if ( drvElem ){
	  /* This is a derived element. Read elMark. */
          if ( !FelMark ) {
            hip_err ( fatal, 0, "invalid elMark file in read_avbp4_conn." ) ;
          }
	  else if ( FREAD ( elInt, sizeof( int ), 2, FelMark ) != 2 ) {
	    sprintf ( hip_msg, "error reading elMark of %d-noded elems"
		     " in read_avbp4_conn.\n", mVertsElem ) ;
	    hip_err ( fatal, 0, hip_msg ) ; }
          elMark = int2elMark ( elInt ) ;
	  elType = elMark.elType ;
	  mVertsBaseElem = elemType[elType].mVerts ;
	}
#endif
        init_elem ( pElem, elType, (int)(pElem - Pchunk->Pelem), NULL ) ;

	if ( FREAD ( someInt, sizeof( int ), mVertsElem, Fconn ) != mVertsElem ) {
	  sprintf ( hip_msg, "error reading connectivity of %d-noded elems"
		   " in read_avbp4_conn.\n", mVertsElem ) ;
	  hip_err ( fatal, 0, hip_msg ) ; }
	
	/* Convert the integers to pointers. */
	for ( kVx = 0 ; kVx < mVertsBaseElem ; kVx++ )
	  Pchunk->PPvrtx[++nLstE2V] = PbaseVx + someInt[kVx] ;

#ifdef ADAPT_HIERARCHIC
        /* Add all hanging edges. */
        if ( drvElem ) {
          /* Base type. */
          pElT = elemType + elType ;
          pElem->PPvrtx =  Pchunk->PPvrtx + nLstE2V - mVertsBaseElem + 1 ;

          /* Reset markers for added vertices on edges. */
          for ( ppVx = pVxEg ; ppVx < pVxEg + MAX_EDGES_ELEM ; ppVx++ )
            *ppVx = NULL ;
          
          /* Find the hanging vertices using elMark. */
          for ( mVxHg = kEg = 0 ; kEg < pElT->mEdges ; kEg++ )
            if ( elMark.hgEdge & bitEdge[kEg] ) {
              pVxEg[kEg] = PbaseVx + someInt[pElT->mVerts+mVxHg] ;
              ++mVxHg ;
              if ( !( nAe = get_elem_edge ( pllAdEdge, pElem, kEg,
                                            &pVrtx1, &pVrtx2, &dir ) ) )
                hip_err ( fatal, 0, "could not find hanging vx in read_avbp4_conn" ) ;
              pUns->pAdEdge[nAe].cpVxMid = pVxEg[kEg]->vxCpt ;
            }

          /* Special faces. */
          for ( kFc = 1 ; kFc <= pElT->mFaces ; kFc++ ) {
            if ( elMark.fixDiag & bitEdge[kFc] ) {
              /* There is a hanging edge. */
              pFoE = pElT->faceOfElem + kFc ;
              kVx = ( elMark.diagDir & bitEdge[kFc] ? 1 : 0 ) ;
              pVrtx1 = pElem->PPvrtx[ pFoE->kVxFace[kVx] ] ;
              pVrtx2 = pElem->PPvrtx[ pFoE->kVxFace[kVx+2] ] ;
              if ( !( nAeFix = get_edge_vrtx ( pllAdEdge, &pVrtx1, &pVrtx2, &dir ) ) )
                hip_err ( fatal, 0, "could not find hanging edge in read_avbp4_conn" ) ;
            }
            
            if ( elMark.hgFace & bitEdge[kFc] ) {
              /* There is a hanging vertex on that face. Look for cross edges. */
              ++mVxHg ;
              pFoE = pElT->faceOfElem + kFc ;
              for ( found = kEg = 0 ; kEg < 2 ; kEg++ )
                if ( ( pVrtx1 = pVxEg[ pFoE->kFcEdge[kEg] ] ) &&
                     ( pVrtx2 = pVxEg[ pFoE->kFcEdge[kEg+2] ] ) )
                  if ( get_edge_vrtx ( pllAdEdge, &pVrtx1, &pVrtx2, &dir ) )
                    found = 1 ;

              /* At least one of the cross edges or a fixe diagonal have to be present
                 if there is a center vertex. */
              if ( !found && !nAeFix ) {
                hip_err ( fatal, 0, "missing cross edge, shouldn't have happened"
                          " in read_avbp4_conn.\n" ) ;
              }
            }
          }

          if ( pElT->mVerts+mVxHg != mVertsElem ) {
            sprintf ( hip_msg, "%d verts expected, %d found in elem %"FMT_ULG""
                     " in read_avbp4_conn.\n",
                     mVertsElem, pElT->mVerts+mVxHg, pElem->number ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }

	  /* Check the convexity of the derived elements. */
#ifdef CHECK_CONVEX
          /* Calulate face normals */
#ifdef ADAPT_HIERARCHIC
          volume = drvElem_volume ( pUns, pElem ) ;
#else
          volume = get_elem_vol ( pElem ) ;
#endif
          if ( volume <= 0. )
            printf ( " FATAL: non-positive volume %g in %s type %d"
                     " in read_avbp4_conn.\n", volume, pElT->name, nElT ) ;
#endif
        }
#endif
      }
      PlstElemPrvType += mElemsThisType ;

      /* Trailing record length. */
      FREAD ( someInt, sizeof( int ), 1, Fconn ) ;    }
  }

  /* Final realloc of PPvrtx to the needed size. */
  mNewE2V = nLstE2V + 1 ;
  Pchunk->PPvrtx = arr_realloc ( "Pchunk->PPvrtx", pUns->pFam, Pchunk->PPvrtx,
                                 mNewE2V, sizeof( vrtx_struct * ) ) ;
  if ( !Pchunk->PPvrtx )
    hip_err  ( fatal, 0, "could not reallocate the for final size of PPvrtx"
	      " in read_avbp4_coor.\n" ) ;
  Pchunk->mElem2VertP = mNewE2V ;

  /* Loop over all elements and set PPvrtx after the final realloc. */
  PPfrstE2V = Pchunk->PPvrtx ;
  for ( pElem = Pchunk->Pelem+1 ;
        pElem <= Pchunk->Pelem + Pchunk->mElems ; pElem++ ) {
    pElem->PPvrtx = PPfrstE2V ;
    PPfrstE2V += elemType[ pElem->elType ].mVerts ;
  }
  
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "      Found %d elements of %d types, %d ele2vert pointers.\n",
	     mElems, mElemTypes, mNewE2V ) ;
    hip_err ( blank, 3, hip_msg ) ;
  }
  
  /* Skip the dummy layers. */
  return ( 1 ) ;
}

