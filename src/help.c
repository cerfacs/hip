/******************************************************************************

  help_menu:
  .
  
  Last update:
  ------------
  13Jan`9; add help_interface
  22Sep19; document ad-per.
  9Jul19; rename to ADAPT_HIERARCHIC
  4Apr19; update help for se bc-ma.
  16Apr18; update help for li, ch, create a path for ch.
  1Nov17; update help text for in-fc-tol.
  20Dec16; update help for write ensight, decimate. 
  3Oct16; clarify that isoFac < 1 is refinement for mmg3d.
  5Jul16; update help for ensight, intro decimate.
  7Mar16; intro hlp_mm.
  30Jun12; write the help entry on partial interpolation more precisely.
  1Apr12; update CGNS, command line args for hdf.
  1Feb12; intro read_ensight.
  Dec11; update zone syntax.
  1Apr11; intro per_thresh_is_rot
  17Dec10; correct uns copy description.
  16Oct10; document se bc-ma
  4Jul10; updates for hdfa/parameter fields and bc-text: hip_bnde_mp
  18Sep09; add doc for hdfa.
  17Sep09; document var flag, write ensight.
  25May09; use space as separator between command line arguments.
  4Apr09; update 'va na'.
  11Dec07; intro paraT variable type.
  17Oct06; intro avh, remove refs to adf, cgns, asmb.
  1Apr05; belated intro reading n3s, writing hybrid n3s into help.
          intro avbp5.3eg.
  2Feb04; intro writing to 5.3. 
  9Apr03; intro set no-single, transform, patch merge, chk_lvl.
          rewrite parts on in-line, reading avbp averaged solution.
  18Sep02; add avbp 5.1 format.
  26Apr02; remove var default.
  15Jul96: conceived.
  
*/

#include <string.h>
#include "cpre.h"
#include "proto.h"

void hlp_re ( ) {

  printf ( " read script < InFile >:\n"
           "      read the commands from InFile. The script filename can\n"
           "      also be specified on the commandline.\n" ) ;

  printf ( "\n Unstructured formats:\n" ) ;
  printf ( " read avbp < masterFile >\n"
           "      read a set of AVBP X.X files listed in a master file. E.g.:\n"
           "        ' Masterfile for visual3/AVBP X.X by hip version 1.5.1'\n"
           "        '/home/muller2/grids/3D/per/box.sol'\n"
           "        '/home/muller2/grids/3D/per/box.coor'\n"
           "        '/home/muller2/grids/3D/per/box.conn'\n"
           "        '/home/muller2/grids/3D/per/box.exBound'\n"
           "        '/home/muller2/grids/3D/per/box.inBound'\n"
           "        '/home/muller2/grids/3D/per/box.asciiBound'\n"
           "      Quotes are optional, but needed by visual.\n"
           "      If the flag periodic is set (see set periodic, also write avbp),\n"
           "      it is assumed that all periodic boundaries appear in the .exBound\n"
           "      file and the .inBound file is ignored.\n"
           "      You can supply a file with the averaged solution for the .sol\n"
           "      hip will look at the structure of the file and determine what\n"
           "      you are supplying. The pertinent solution then is the averaged\n"
           "      one and will be the one that is written, interpolated, etc.\n" ) ;
  printf ( " read avh < masterFile >.\n"
           "      Read a set of AVBP 6.X files with the solution in hdf.\n"
           "      All files are given in a .visual master file as per avbp above.\n") ;
  printf ( " read bae < file.fro file.gri file.bco >.\n"
           "      read a tetrahedral grid in BAe's/Swansea's flite format.\n" ) ;
  printf ( " read cdre < File >.\n"
           "      read an unstructured grid in cedre format.\n" ) ;
  printf ( " read centaur < File >.\n"
           "      read an unstructured grid in Centaur Soft's format.\n" ) ;
  printf ( " read cfdrc-mfg < mfgFile {flip} >.\n"
           "      read an unstructured 2-D/3-D grid in CFDRC's mixed format.\n"
           "      CFDRC lists boundary faces for the hybrid interface. These\n"
           "      must be unlabeled or labeled 'Default' if hip is to detect\n"
           "      the match between two triangular and a quadrilateral face\n"
           "      properly.\n"
           "      Currently there is a bug in CFDRC mixed grids with the quads\n"
           "      being listed clockwise. You can turn on 'flip', 2 characters\n"
           "      significant, to have all negative quads flipped.\n" ) ;
#ifdef CGNS
  printf ( " read cgns < File >.\n"
           "      read an unstructured grid in CGNS format.\n"
           "      Currently hip reads the grid only, no solution.\n" ) ;
  printf ( "   Options to read hdf:\n" );
  printf ( "   -n   provide a mesh name to identify the grid.\n" ) ;
#endif
  printf ( " read dpl < dplFile >.\n"
           "      read a 2-D/3-D grid in dplot format.\n" ) ;
  printf ( " read ensight < caseFile >.\n"
           "      read an unstructured 3-D grid in ensight format.\n"
           "      this is currently limited to primitive elements, polyhedral\n"
           "      elements are not supported.\n" ) ;
  printf ( " read fluent < caseFile { datFile } >.\n"
           "      read an unstructured grid in Fluent's ascii (.msh) or\n"
           "      binary (.cas) format. If given, a solution in .dat format\n"
           "      is read.\n" ) ;
  printf ( " read flbswap < caseFile { datFile } >.\n"
           "      same as 'read fluent', but performing an little/big-endian\n"
           "      binary swap. 4 significant letters (flbs) are required.\n" ) ;
  printf ( " read gmsh < {options} gmshFile >.\n"
           "      read a 2-D/3-D grid in gmsh 2.0 ascii format.\n"
           "      Variable fields must be given in separate files. Required\n"
           "      components are (in this order) density, velocity, pressure\n"
           "      Options:\n"
           "      The following options are only relevant to gmsh 2 formats:\n"
           "      -bstringTag: prepend bc numbers with stringTag_ as bc name \n"
           "      -tbcTagPos: read the number of the bnd entity as the \n"
           "       bcTagPos'th tag. Default is 1, the first tag, which should\n"
           "       be the 'physical entity' associated with that element.\n"
           ) ;
  printf ( " read hdf < meshFile asciiBndFile {solFile}  >.\n"
           "      read an unstructured grid in AVBP's hdf format.\n" ) ;
  printf ( " read hdf  < meshFile > -s solFile.\n"
           "      read an unstructured grid in AVBP's hdf format reading\n"
           "      patch labels and periodicity from the hdf file.\n" ) ;
  printf ( "   Options to read hdf:\n"
           "      Usage: read hdf -[asz] < meshFile {asciiBndFile {solFile}}  >:\n") ;
  printf ( "   -a   read the full set of variables.\n"
           "      A 'GaseousPhase' is always required.\n"
           "      Under hdf -a, hip will retain in the group Parameters all\n"
           "      scalars and in all groups all node-based vectors. \n" ) ;
  printf ( "   -i   scan the mesh file and write out numbers of vertices, elements, etc.\n" ) ;
  printf ( "   -n   provide a mesh name to identify the grid.\n" ) ;
  printf ( "   -s solfile    When not using an asciiBound file, specify\n"
           "      the solution file using -s. The option can be placed at\n"
           "      the end after the meshfile.\n" ) ;
  printf ( "   -z0  do not read the zones in the mesh. Default state is -z,\n"
           "       which means do read the zones.\n" ) ;
  printf ( " read hyd  < meshFile > -c caseFile -s solFile -a adjFile.\n"
           "      read an unstructured grid in hydra's hdf format reading\n"
           "      patch labels from the input.hyd case file, if available.\n" ) ;
  printf ( "   Options to read hdf:\n"
           "      Usage: read hdf < meshFile {-c caseile -s solFile -a adjFile}  >:\n") ;
  printf ( "   -a  read adjoint flow, turbulence variables and normal sensitivity from adj file.\n" ) ;
  printf ( "   -c  read bc labels from case file input.hyd.\n" ) ;
  printf ( "   -s  read flow, turbulence variables from flow file.\n" ) ;
  printf ( " read n3s < gridFile { solFile } >.\n"
           "      read an unstructured grid in n3s format, optionally with.\n"
           "      solution.\n"
           "      The solution can either be single-species, containing\n"
           "      the u,v,w velocities, pressure and density, or it can be\n"
           "      multi-species, in which in addition the species, cp, T and\n"
           "      gamma are required.\n"
           "      As of 1.28.2, in multi-species cases the primitive variables will\n"
           "      be written, mono-species cases will converted to conservative\n" ) ;
  printf ( " read n3sa < gridFile { solFile } >.\n"
           "      same as n3s, except that the entire field of variables is read.\n" );
  printf ( " read saturne < GridFile MasterFile saveNr >.\n"
           "      read an unstructured grid from Saturne. The GridFile is\n"
           "      in I-DEAS unv format, the solution files are given in\n"
           "      a master file for ensight gold. That master file describes\n"
           "      a number of saves, so a save nr must be given.\n" ) ;

  printf ( "\n Structured formats:\n" ) ;
  printf ( " read cstruct < ibcFile plot3dFile { plot3dType[ascii,bin] skip } >.\n"
           "      read a multiblock grid in cfdrc structured format. The plot3d\n"
           "      file must be with blanking information for the nodes and can be\n"
           "      formatted or unformatted.\n"
           "      Default file type for the plot3d file is ascii. In the unformatted\n"
           "      case, hip automatically determines whether the file is single or\n"
           "      double precision.\n"
           "      Skip indicates the next node to be retained in each direction, ie.\n"
           "      skip=2 means every other node is skipped.\n" ) ;
#ifdef DAMAS
  printf ( " read damas < name {skip} >.\n"
           "      read a damas database. Skip indicates the\n"
           "      the next node to be read in each direction, ie skip=2 means\n"
           "      every other node is skipped.\n" ) ;
#endif
  printf ( " read dlr-flower < logicFile gridFile { gridFileType[a,b]} skip } >.\n"
           "      read a multiblock grid in dlr flower format.\n" ) ;
  printf ( " read mcgns < cgnsFile { skip, blockNo } >.\n"
           "      read a multiblock cgns file. Connectivity has to be given as.\n"
           "      1to1. If given, periodicity is read from the interface definition.\n"
           "      Boundary conditions are read directly, or via families.\n"
           "      If a skip is specified, e.g. 2 or 4, then only every n-th node\n"
           "      is retained.\n"
           "      If a blockNo is given, then only that block will be written,\n"
           "      all its internal interfaces will be converted to boundaries.\n"
           "      This can be used to debug multi-block connectivity.\n"
           "      Currently (version 18.03) the solution is not read.\n" ) ; 
  printf ( "\n" ) ;
  return ;
}

void hlp_ge ( ) {

  printf ( " generate < { llx lly urx ury mX mY } >\n"
           "     generate a rectangular mesh of quadrilateral elements with\n"
           "     llx, lly as the lower left, urx, ury as the upper right\n"
           "     coordinates and with mX nodes in x, mY nodes in y.\n"
           "     The mesh is unstructured.\n\n" ) ;
  return ;
}

void hlp_fl ( ) {

  printf ( " flag < { nodes geometry parameters .... } >\n"
           "     flag entities of a mesh (currently only nodes are supported)\n"
           "     using a specific type of geometry and its parameters.\n"
           "     Invoking flag with nonzero arguments sets the status of the grid\n"
           "     to 'flagged', see e.g. zone creation for what this implies.\n"
           "     Possible geometries and their parameters are:\n"
           "       Flag all nodes inside a box:\n"
           "         flag nodes box llx lly [llz] urx ury [urz]\n"
           "       Flag all nodes above a plane: \n"
           "         flag nodes plane orx ory [orz] normx normy [normz]\n"
           "         ('above' means that the scalar product of norm and vector\n"
           "          from origin or to vertex is positive)\n"
           "       Flag all nodes within a sphere:\n"
           "         flag nodes sphere orx ory [orz] rad\n"
           "       Flag all nodes within a cylinder with axis axyz through orxyz, radius r:\n"
           "          flag nodes  orx ory [orz] axx axy [axz] r\n"
           "       Flag all nodes within a sector defined by two limiting points a,b and\n"
           "          a rotation axis of x,y,z \n"
           "          flag nodes sector  ax ay [az]  bx by [bz]  axis\n"
           "     Invoking flag without arguments clears all flags. Flags are\n"
           "     cumulative, so several flag operations can be sequenced.\n"
           "     Flag operations are used e.g. in associating elements to zones.\n"
           "\n" ) ;
  return ;
}

void hlp_tr ( ) {
  
  printf ( " transform translate < { dx dy dz } >\n"
           "     Translate the entire mesh by dx, dy, dz.\n"
           " transform scale < { sx sy sz } >\n"
           "     Scale the mesh by sx, sy, sz in the x,y,z direction.\n"
           " transform rotate < { axis alpha } >\n"
           "     Rotate the mesh around the axis x,y, or z with angle alpha \n"
           "     in degrees. Right hand rule applies.\n"
           " transform reflect < { axis } >\n"
           "     Change the sign of the coordinates of axis x,y, or z.\n"
           " transform per\n"
           "     Use the setup of the first periodic pair, rotation or translation.\n"
           "     If periodicity is split into more than one pair around the same axis\n"
           "     or the same direction, make sure they all have the same sign.\n"
           "     If there is multiple periodicity in different direction, you\n"
           "     probably want to specify the operation manually rather than relying\n"
           "     on the first one encountered.\n"
           "     \n\n" ) ;
  return ;
}

void hlp_va ( ) {

  printf ( " var\n"
           "     show number, type of variables and freestream.\n"
           " var cat nr-range var_cat\n"
           "     Set the category of variables in nr-range to be var_cat.\n"
           "     Recognised categ. are: ns, species, rrates, tpf, rans, add, mean.\n"
           " var convert < cons prim primT para symm >\n"
           "     convert the state variables to one of the above. Default: cons.\n"
           " var flag value nr-range\n"
           "     Set the flag of variables in nr-range to be value.\n"
           "     Currently used by write_ensight and write_hdf,\n"
           "      any non-zero value is flagged 'on'.\n"
           " var freestream < w(1) ... w(mEq) >\n"
           "     set freestream variables.\n"
           " var gamma < { gamma } >\n"
           "     Set a new value vor gamma. Default is 1.4.\n" 
           " var init < keyword, {value} >\n"
           "     initialise the solution. For the time being you can choose from:\n"
           "     var_name_expr value: set all variables matching the expr to value.\n"
           "       (value only needs to be provided for this option.)\n"
           "     0: set all vars to 0.\n"
           "     1: set all vars to 1.\n"
           "     e: set rho*e (cons) or p (prim) to 100.\n"
           "     f: initialise with freestream variables\n"
           "     r: set rho to 1.\n"
           "     s: some solution with a shock in a unit square\n"
           "     x: set rho=1., u=x, v=y, w=y, p= xy+yz+zx\n"
           " var name\n"
           "     List a table of variables and their names.\n" 
           " var name nr var_name\n"
           "     Set the name of variable nr to be var_name.\n"
           "     Note that the hdf format uses the variable name as an idenifier\n"
           "       and hence requires that they are distinct.\n" 
           " var r < r >\n"
           "     Set a new value vor r. Default is 287.\n" 
           " var scale < nVar, scale >\n"
           "     Scale varialble nVar with scale.\n" 
           " var vec < nr > < vecPos >\n"
           "     Modify the vector status of variable nr,\n"
           "     vecPos=0 is a scalar, 1,2,3 is x,y,z.\n" 
           "\n" ) ;
  return ;
}


void hlp_vi ( ) {

  printf ( " vis\n"
           "    Visualise grid entities below/above certain thresholds. Currently supported:\n"
           " vis elems\n"
           "    -p property: one of hMin, volMin, angMax\n"
           "       with hMin the shortest length of the element, volMin its volume,\n"
           "       angMax the maximal face or dihedral angle (whichever is larger.)\n"
           "       Default is volMin.\n"
           "    -m factor: threshold as a multiplicative factor of the smallest (min) or\n"
           "       1/factor in case of the largest (max)."
           "       Multiplicative is default with a factor of 1.1.\n"
           "    -t threshold: threshold given directly, with visualised entities below\n"
           "       (min) or above (max) the threshold.\n"
           "    -f fileName: vtk filename to write to. Default is elems_prop.vtk \n"
           "     \n\n" ) ;
  return ;
}




void hlp_li ( ) {
  printf ( " list grids.\n"
           "      list all grids stored.\n"
           " list current.\n"
           "      list the current grid being worked upon.\n"
           " list surfaces < { all, area } >.\n"
           "      list the surfaces in the current grid, or all grid[s]\n"
           "      if 'all' is specified.\n"
           "      If area is given, patch areas are computed and displayed.\n"
           " list periodic.\n"
           "      list all periodic surfaces with transformation operations.\n"
           " list storage.\n"
           "      list all allocated arrays to see how storage is used.\n"
           "\n" ) ;
  return ;
}

void hlp_ma ( ) {
  printf ( " mark surf < { surf1 surf2  ...} >\n"
           "      mark the numbered boundary surfaces. \n"
           "      No argument clears all markings. \n"
           " mark match. < bcNr1 bcNr2 ... >.\n"
           "      The boundaries bcNr1, bcNr2, ... collapse onto each other\n"
           "      when attaching grids to each other.\n"
           "      With no argument given, the list of matching pairs is erased.\n"
           "\n"
           " mark interface  < bcNr1 bcNr2 >\n"
           "      identifies the two boundaries as a matching pair. The first one is\n"
           "      retained and written out as an interface, the second one is deleted\n"
           "      as a duplicate. If the second boundary number is not provided,\n"
           "      the first boundary is retained as interface and no boundary is\n"
           "      deleted. If no arguments are given,\n"
           " mark interface\n"
           "      all interface flags are removed.\n"
           "\n" ) ;
  return ;
}

void hlp_cu ( ) {
  printf ( " cut distance < distance { cutType[0,1] } >.\n" ) ;
  printf ( "     cut a grid retaining only elements with [1,all] vertices \n" ) ;
  printf ( "         within distance from the currently marked surfaces.\n" ) ;
  printf ( "         Default cuttype is 0.\n" ) ;
  printf ( " cut iso < { [minmax avg] [all dir] isoValue } >.\n"
           "     cut a grid to retain only anisotropic elements. Isotropy is\n"
           "         defined as a minimum over a maximum length, ie tends to\n"
           "         zero for increasing stretching.\n"
           "         Cutting is controlled by a calculation type and a cutoff\n"
           "         value.\n"
           "         Four calculation types are possible:\n"
           "            minmax all, avg all, minmax dir, avg dir:\n"
           "            minmax compares the smallest and longest dimensions, while\n"
           "            avg compares the averages of all dimensions.\n"
           "            all considers all directions irrespective of orientation,\n"
           "            dir only considers stretching normal to the marked surface.\n"
           "         The cutoff value is used with the directional types.\n"
           "         If no preferential direction is fixed, 'all', hip truncates\n"
           "         after finding a maximum in the stretching. This behaviour\n"
           "         makes the iso option usable in 3D, where isotropy=1. is not\n"
           "         necessarily reached.\n"
           "         Default type is minmax, all, default isoVal is 1.\n"
           "         Example: cut iso min all\n"
           "                  follows the stacks of cells on all marked boundary\n"
           "                  surfaces, until a local minimum of the ratio of\n"
           "                  minimum over maximum edge length in the element is\n"
           "                  reached that exceeds the default cutoff isoval = 1..\n"
           "         Example: cut iso avg dir .8\n"
           "                  follows the stacks of cells on all marked boundary\n"
           "                  surfaces, until the ratio of the average length of\n"
           "                  all edges normal to the face to the one of all\n"
           "                  edges forming the face exceeds .8.\n" ) ;
  printf ( "\n" ) ;
  return ;
}

void hlp_co ( ) {
  printf ( " copy uns < mCopies translate-operation < { translate-values } > .\n"
           "      Duplicate the current grid sector mCopies times and apply.\n"
           "      a transformation operation to each sector, see translate for\n"
           "      details on these operations and their values.\n"
           "      E.g. 'copy uns 3 rot x 90' would complete an annulus from a\n"
           "      quarter sector of a mesh (note, you have to make 3 copies to\n"
           "      have 4 parts in total.)\n"
           "      If a translation operation is specified, hip will merge the\n" 
           "      resulting mesh into one. \n"
           "      By default, h!p will not modify the boundary patches where the\n"
           "      copies join. They will remain in the mesh and then cause problems\n"
           "      when writing out and reading back in.\n"
           "      There are a few options to remove these duplicate patches.\n"
           "      a) declare periodicity. The 'u' boundary of the original mesh\n"
           "         will be matched onto the 'l' boundary of the first copy, and so on.\n"
           "         If the annulus is not complete, the outermost 'l' and 'u' boundaries\n"
           "         remain.\n"
           "      b) for non-periodic cases, declare the boundar[y,ies] that match each \n"
           "         other using the 'mark match bcNr' command. This could e.g. be a single\n"
           "         boundary if a mesh is duplicated around a symmmetry bc.\n"
           "      c) the lazy option: first instruct hip to remove all duplicate faces using\n"
           "         set fc-remove matcFc cutFc bndFc, where the value for bndFc needs to be 1\n" 
           "         Then use set check 5 to trigger the connectivity check which identifies\n"
           "         duplicate faces. Having changed the removal instructions, those will then\n"
           "         be removed. Note that using this approach, your're out of control!\n"
           "      If you don't want a merged mesh, then specify the transformation\n"
           "      operation in a separate step.\n"
           "      The copy will become the current grid.\n" ) ;
  printf ( " copy 2uns < { arg } >.\n"
           "      Copy the current multi-block grid to an unstructured format.\n"
           "      The copy will become the current grid.\n"
           "      with the additional argument 'map' a vertex mapping from linear\n"
           "      block ijk to unstructured node number is produced, which can be\n"
           "      written to hdf using 'write mhdf'.\n") ;
  printf ( " copy 2tets.\n"
           "      cut all elements to tets. Currently only hexes supported.\n" ) ;
  printf ( " copy 3D < zBeg zEnd mSlices axis >.\n"
           "      Copy an unstructured 2D grid to an unstructured 3D one.\n"
           "      The z coordinate will range from zBeg to zEnd in mSlices of\n"
           "      elements. Default values are 0., 1., 1, z\n"
           "      Hip will create a new pair of boundaries named\n"
           "      'first slice at <zBeg>' and 'last slice at <zEnd>'.\n"
           "      The optional axis argument allows to extrude along either x, y, z\n"
           "      or around the x-axis using a. In the case of axi-extrusion the\n"
           "      zBeg/zEnd arguments give the two bounding angles in degrees.\n"
           "      See also write avbp and set textbc.\n" ) ;
  printf ( " copy q2t < { splitType } >\n"
           "      If the current mesh is 2D, all the quads in it will be split into\n"
           "      two triangles. splitType can either be\n"
           "        maxAngle: the split will cut the maximum angle,\n"
           "        ll2ur:    from lower left to upper right\n" 
           "        ul2lr:    from upper left to lower right\n"
           "        1-3:      from the node in the 1st to the 3rd node\n"
           "        2-4:      from the node in the 2nd to the 4th node\n"
           "      The default is maxAngle.\n" ) ;
  printf ( " copy x-zy\n"
           "      Do a rotation of the coordinates of 90 degrees around the x-axis,\n"
           "      that is, x, y, z, go over into x, -z, y.\n" ) ;
  printf ( "\n" ) ;
  return ;
}

void hlp_at ( ) {
  printf ( " attach [options] < nr.1 { nr.2 .... nr.n } >.\n"
           "        Attach the grids nr.1 ... nr.n to the current grid. \n"
           "        There are two ways to remove the boundaries that have.\n"
           "        become interior:\n"
           "        a) Set the default to remove duplicate faces to 1 with the command\n"
           "           set fc-remove matchFc cutFc bndFc, where the value for\n"
           "           for bndFc needs to be 1.\n"
           "           Then run with check level 5 (default, changed with\n"
           "           se ch), h!p will run a complete connectivity test which\n"
           "           will detect the duplicated faces and remove them.\n"
           "           However, in this case hip has no way of knowing what\n"
           "           faces should be matched and cannot warn you if some of\n"
           "           them don't.\n"
           "        b) a better way is to use the 'mark' command and mark the\n"
           "           boundaries to match with  'mark match bcNr1 bcNr2 ...'.\n"
           "           hip will remove all marked faces and warn you if there\n"
           "           were faces that did not match.\n"
           "           This option also avoids the heavy storage requirement\n"
           "           of the full connectivity check.\n"
           "        CAVEAT: a typical application is to rotate a grid by the\n"
           "           periodicity angle and glue together. If you do not change\n"
           "           the names of the periodic patches in the copied mesh, hip\n"
           "           will merge the _inlet and _outlet patches as they have the\n"
           "           same name. However, only half of this patch will be matched,\n"
           "           the other half is still periodic. Hence, take care to ensure\n"
           "           that patches are named uniquely and match fully.\n"
           "         options:\n"
           "         -m0: do not merge the grids, i.e. do not coalesce coincident\n"
           "           vertices.\n"
           "         -texpr: attach the other mesh[es] to the mesh specified by expr,\n"
           "           rather than attaching to the current mesh.\n"
           ) ;
  return ;
}

void hlp_bc () {
  printf ( " bc command: replaces the deprecated set bc-{text,type,order ...}"
           " bc compress\n"
           "     B.c. are considered to be identical if they carry the same text\n"
           "     label. Hence one way of merging patches is to set the text to be\n"
           "     the same and use bc-compress to force the merging of the bc.\n"
          " bc mark < bc_expr mark >:\n"
           "     set the mark for a range of boundaries, e.g. for duplication\n"
           "     of bcs in copy_uns, or fixing boundary patches in mmg mesh\n"
           "     adaptation. No arguments resets the marks for all bcs.\n"
           " bc order < bc_expr rank >:\n"
           "     set the rank of bc bc_expr in output. Duplicate ranks give hip a\n"
           "     choice to list either first. No argument will reset the list to\n"
           "     chronological order.\n"
           " bc split < bc_expr bc_text_appendage >\n"
           "     Use a previously defined hypervolume (see help on set hypervolume)\n"
           "     to split all boundaries matching bc_expr in parts in and outside the\n"
           "     hypervolume. New bcs are formed with the parts inside the hypervolume\n"
           "     they are given a text label of old bc text + bc_text_appendage.\n"
           " bc text < bc_expr text >:\n"
           "     set the text of a bc matching bc_expr to text.\n"
           "     bc-expr can be just a single number, but can also be a collection\n"
           "     of ranges of numbers separated by commata, e.g. 1-4,6,9-58.\n"
           "     Note that either you type no blanks inside the list, or you\n"
           "     have to place the list in quotes.\n"
           "     Alternatively, if the supplied bc nr contains either any\n"
           "     alphabetic characters or the wildcards ? or *, the supplied\n"
           "     pattern is matched against the existing bc text. E.g. *-?\n"
           "     matches the boundaries named 1: -x, --y or 10:-z.\n"
           "     A few caveats:\n"
           "       avoid names starting with numbers, as the bc-expr 2*\n"
           "       would be intepreted as a numeric expression.\n"
           "     - Names starting with hip_ are reserved.\n" 
           "     - Any pair of boundaries with hip_per_inlet_#, hip_per_outlet_#,\n"
           "       where # stands for any text label, are interpreted as a pair\n"
           "       of periodic boundaries. If you don't want to modify the bnd\n"
           "       text, you can also use the type l,u with the same tag, see type.\n"
           /* Dropped as of 1.29.3, 
              "     - any boundary labelled hip_bnde_mp_# will be included in\n"
              "       and the listing of boundary vertices referenced by more\n"
              "       than a single patch ('multi-patch boundary nodes').\n"
           */
           "     - Any patch labelled hip_mb_degen is considered a degenerate\n"
           "       block face in a multiblock grid.\n"
           "     - Any patch labeled hip_match_bc is considered a patch between\n"
           "       different parts of the grid that have to match.\n"
           " bc type < bc_expr type >:\n"
           "     set the type of boundaries matching bc_expr.\n"
           "     bc_nr can be a wildcard expression as\n"
           "     described for bc-text. Type is a single character value. hip\n"
           "     recognizes special meanings of the following ones:\n"
           "     e:   & entry          \n"
           "     f:   & far field      \n"
           "     i:   & inviscid wall  \n"
           "     l:   & lower (shadow) periodic \n"
           "     o:   & outlet         \n"
           "     p:   & any periodic   \n"
           "     n:   & none           \n"
           "     s:   & symmetry plane \n"
           "     u:   & upper (master) periodic \n"
           "     v:   & viscous wall   \n"
           "     w:   & any wall       \n"
           ) ;
  printf (
           "     Note that the boundary vertex no-angle fix works\n"
           "     only on wall patches. Thus before writing the file the boundary\n"
           "     type of the relevant patches must be set to 'w','i' or 'v'.\n"
           "     In hdf a list of nodes belonging to multiple patches is written\n"
           "     for all bc with type 'w'.\n"
           "     Symmetry nodes are specially treated and listed if the boundary\n"
           "     patch is declared as a symmetry one.\n"
           "     Periodicity can be declared with the boundary text, as discussed\n"
           "     under 'text', but also with the boundary types 'l,u' and an\n"
           "     appended tag, e.g. 'l99'. This tag works in exactly the same way\n"
           "     as for the boundary text. If no label is given, hip internally\n"
           "     supplies the tag 00. The periodic pair tag specified with 'lu'\n"
           "     takes precedence over the tag specified with hip_per.\n"
           "     You still have to describe the geometry transformation\n"
           "     with set pe-corner or set pe-rotation. Unfortunately, this\n"
           "     pairing is currently not carried forward in the hdf/asciiBound\n"
           "     output files.\n"
           ) ;
  return ;
}

void hlp_se ( ) {

  char keyword[LINE_LEN] ;
  /* The help menu has become too big. Subdivide. */  
  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  if ( !strncmp ( keyword, "", 1 ) )
    printf ( " set lists or changes set-able parameters. Query on a subject as\n"
             " general, edge, bc, mg, mb, normals, periodicity, fc, dg, ad, in.\n" ) ;
  else if ( !strncmp ( keyword, "ge", 2 ) )
    printf ( " set:\n"
             "     list all set-able parameters.\n"
             " set verbosity < verbosity[0,5] >:\n"
             "     Have you ever met Eddie?\n"
             "     If set to 0, hip will assume it is run by machine, hence\n"
             "     will abort on warnings.\n"
             " set current < grid nr. >:\n"
             "     set the current grid to grid nr.\n"
             " set path < path >:\n"
             "     prepend a path to all relative filenames. With no argument, the\n"
             "     current path is cleared.\n"
             " set gridname < expr> < name >:\n"
             "     set the name of grid matching expr to name. USE IT!\n"
             " set gridname < name >:\n"
             "     with only a single argument, change the name of the current grid.\n" 
             " set epsoverlap < {epsOverlap} >:\n"
             "     set the disc for overlap. If no argument is given, it is 10.E-7.\n"

             " set vo-abort < [0,1] >:\n"
             "     if set to 1, hip aborts if negative volumes are encountered.\n"
             "     Default is 1.\n"
             " set vo-swap < [0,1] >:\n"
             "     if set to 1, hip flips elements if all hex are reversed.\n"
             "     Currently only applied to reading ensight meshes.\n"
             "     Default is 1.\n"

             " set topology < {noTopo axiX annular cascade} >\n"
             "     Treat the grid as a special topology. hip knows about:\n"
             "     axiX: an axisymmetric case with the singularity on the x-Axis,\n"
             "       and periodicity on the two side-walls.\n"
             "     similarly, axiY/Z: an axisymmetric case with the singularity on\n"
             "       the y/z-Axis, and periodicity on the two side-walls.\n"
             "     In AVBP- and hdf formats, the points on the axis will not appear \n"
             "       in the list of periodic nodes but be listed separately in hdf \n"
             "       or at the end of the .exBound file.\n"
             " set symmetry-coordinate. Define the symmetry plane. Default is y=0,\n"
             "     i.e. 'y'\n"
             " set check < [0..5] >:\n"
             "     Sets the level of checking on the mesh.\n"
             "     If set to 5, the connectivity is verified for consistency, which\n"
             "     requires a lot of storage. \n"
             "     The default setting of doRemove removes all boundary and cut\n"
             "     faces that have collapsed onto each other. In order to control.\n"
             "     whether all matching pairs have been sucessfully matched, it is\n"
             "     highly recommended to explicitely mark all pairings.\n"
             "     In the case of internal boundary conditions, doRemove has to be\n"
             "     set to 0 for boundary faces and all internal boundarys have to\n"
             "     be marked as match, except for those to be retained.\n"
             "     Set to >=4 the cells are checked for positive volumes.\n"
             "     Default value is 5.\n"
             " set chord <{ chordLen }>\n"
             "     set the chord length for the surface reconstruction of a\n"
             "     NACA0012 profile.\n"
             " set hypervol <{ options }>\n"
             "     Add a hypervolume to the current uns grid\n"
             "     e.g.to split a boundary patch in/out.\n"
             "     Any dimensions of vectors, coordinates depend on the number of\n"
             "     dimensions of the grid the hypervol is attached to\n."
             " set hypervolume\n"
             "     Without arguments the current hypervolume (if any) is removed.\n"
             " set hypervolume box llx lly [llz] urx ury [urz]\n"
             "     Define a Cartesian box from llxyz to urxyz.\n"
             "     Default values are llxyz = 0,0,0 and urxyz = 1,1,1.\n"
             " set hypervolume plane nx ny [nz] x0 y0 [z0]\n"
             "     Define a plane through xyz0 with normal nxyz. 'inside' the hypervolume\n"
             "     is interpreted here as 'below' the plane with the normal pointing \n"
             "     to the upper half.\n"
             "     Default values are nxyz = 1,0,0 and xyz0 = 0,0,0.\n"
             " set hypervolume cylinder rad nx ny [nz] x0 y0 [z0]\n"
             "     Define an infinite cylinder of radius rad with axis nx, ny, [nz]\n"
             "     through point xyz0\n"
             "     Default values are rad=1, nxyz = 1,0,0, xyz0 = 0,0,0.\n"
             " set hypervolume sphere rad x0 y0 [z0]\n"
             "     Define a sphere of radius rad with origin xyz0.\n"
             "     Default values are rad=1, xyz0 = 0,0,0.\n"
             ) ;
  else if ( !strncmp ( keyword, "ed", 2 ) )
    printf ( " Edge weight lp options, two characters after lp- are sufficient.\n"
             " set lp-tolerance < {lp_tolerance} >:\n"
             "     set a percentage for lp_tolerance. If no argument is given, it\n"
             "     is reset to 1.e25. On writing the weights to file, hip checks\n"
             "     whether all calculated gradients are within this tolerance.\n"
             " set lp-sweeps < {lp_sweeps} >:\n"
             "     if lp-sweeps is non-zero, all elements will be fixed up to be\n"
             "     lp. Default is 0.\n"
             " set edge-weight-cutoff < {cutoff} >:\n"
             "     edge weights are listed only if the norm is above this level.\n"
             "     Default is 1.e-15\n" ) ;
  else if ( !strncmp ( keyword, "bc", 2 ) )
    printf ( " Boundary condition options, two characters after bc- are sufficient.\n"
             " the 'set bc-{text,type,order ...}' commands are deprecated, use\n"
             " 'bc {text,type,order ...}' instead.\n"
             " See 'help bc' for details\n"
             ) ;
  else if ( !strncmp ( keyword, "mg", 2 ) )
    printf ( " Multigrid coarsening options, two characters after mg- needed.\n"
             " set mg-length < {mg-length} >:\n"
             "     set a maximum length increase for an edge during coarsening.\n"
             "     If no argument is given, it is reset to 2.2.\n"
             " set mg-angle < {mg-angle} >:\n"
             "     set a maximum cosine for the largest angle during coarsening.\n"
             "     If no argument is given, it is reset to -.99.\n"
             " set mg-twist < {mg-twist} >:\n"
             "     set a minimum permissible twist of quad faces. Twist is expressed\n"
             "     as the scalar product of the two normals of a triangulation of\n"
             "     the quad face. The smaller value of the two possible choices of\n"
             "     diagonal is taken. Thus, 1. is a perfectly linear face, -1. is a\n"
             "     face that is completely folder onto itself. Default: .2\n"
             " set mg-volAspect < {mg-volAspect.} >:\n"
             "     hip uses a volumetric aspect ratio to determine whether a\n"
             "     partly collapsed element is valid. This is aspect ratio is\n"
             "     defined as the element volume divided by a reference volume.\n"
             "     The reference is the volume of the smallest isotropic simplex\n"
             "     built with the shortest edge of the element. mg-volAspect is the\n"
             "     cutoff in fraction of this smallest volume. Default is .9\n"
             " set mg-aspectRatio < {mg-aspectRatio} >:\n"
             "     set a minimum aspect ratio that is to be considered stretched.\n"
             "     Semi-coarsening is only applied in regions streched more than.\n"
             "     this value. Default is 2.\n"
             " set mg-ramp < {mgRamp} >:\n"
             "     A factor to ramp the cutoff aspect-ratio and maximum angle with.\n"
             "     This is done by multiplying the cutoff values with this factor.\n"
             "     The Angle still is limited by 175 degrees.\n"
             "     Default is 1., i.e. values remain constant.\n"
             "\n") ;
    else if ( !strncmp ( keyword, "mb", 2 ) )
      printf ( " Multi-block options, two characters after mb- needed.\n"
               " set mb-degen-find < {mb-degen-find} >:\n"
               "     to find degenerate block faces, hip usually relies on either\n"
               "     - no neighboring block and no boundary condition given or\n"
               "     - a boundary tag labeled hip_mb_degen.\n"
               "     If mb-degen-find is switched on, any block face with edges\n"
               "     shorter than epsOverlap is considered degenerate.\n\n"
               ) ;
    else if ( !strncmp ( keyword, "no", 2 ) )
      printf ( " Geometry normals, no-\n"
               " set no-angle < {nodeAngleCut} >:\n"
               "     nodeAngleCut is the cosine of the maximum angle that face\n"
               "     normals around are allowed to vary. If the angle is larger, the\n"
               "     node normal is set to zero. Default is .9\n"
               " set no-single < {singleNormal} >:\n"
               "     This is an option for avbp files, AVBP recalculates the\n"
               "     components of all normals, but hip provides the list of\n"
               "     normals. Hence these options do affect the boundary definition.\n"
               "       If singleNormal is set to 1, a boundary node which\n"
               "     belongs to several patches/groups will only be listed with\n"
               "     the first group encountered. You will want to use set bc-nr\n"
               "     to order the boundary groups such that the first one encountered\n"
               "     is the one you want.\n"
               "       If set to 2, only one normal for all wall patches will be\n"
               "     written, but all other types of patches (including undefined\n"
               "     types) receive their own normal. Hence, to use this you must\n"
               "     declare the walls using set bc-type.\n"
               "     Default is 0, boundary nodes are listed again with each patch\n"
               "     they touch.\n"
               ) ;
    else if ( !strncmp ( keyword, "pe", 2 ) )
      printf ( " Periodicity options, two characters after pe- are sufficient.\n"
               "     Periodicity is declared for two matching patches at a time.\n"
               "     Such pairs are either specified by hip_per_in/outlet_# labels,\n"
               "     see 'help set bc' on this, or in the case of just one pair by\n"
               "     specifying types 'l' and 'u' ( help set bc ).\n"
               "     Note that for the time being, hip is set up to expect exactly\n"
               "     one patch per side in each pair.\n"
               " set pe-rotation < label axis angle >\n"
               "     Declare a periodic pair by giving an axis as in <x,y,z> and a\n"
               "     rotation angle (right hand rule!) in degrees, e.g.\n"
               "       se pe-ro somePatch x 90\n"
               "     If you want your rotation angle listed with the adf file, you\n"
               "     must use set pe-ro rather than se pe-co.\n"
               "     'somePatch' refers to the # label in case hip_per ... is used,\n"
               "     in case a pair of 'l','u' patches it is just 'lu'.\n"
               " set pe-corner < label xy[z]_in0 xy[z]_in1[ ..2],\n"
               "                        xy[z]_out0 ..1[ ..2] >:\n"
               "     To find matching periodic pairs a set of 2/3 matching coordinates\n"
               "     has to be given in 2/3D on each matching patch. 2D coordinates \n"
               "     are given as x,y, 3D as x,y,z. First the point on the _inlet,\n"
               "     patch then the matching point on the _oulet.\n"
               "     hip then calculates a local orthonormal coordinate system with\n"
               "     the 2/3 points in each patch. These local coordinates have to.\n"
               "     match.\n"
               "     Thus: the coordinate system that the points span in each patch\n"
               "     may NOT be linearly degenerate.\n"
               "     Hint: the 2/3 points that define the local systems do not have to\n"
               "     be points of the patch, e.g. a rotation of -90 degrees around the\n"
               "     x-Axis for a patch labelled '1' can always be specified as:\n"
               "       se pe co 1   0. 0. 0.   1. 0. 0.   0. 0. 1.\n"
               "                    0. 0. 0.   1. 0. 0.   0. 1. 0.\n"
               "     One more hint, since the number of arguments is fixed, hip scans\n"
               "     the input file until enough parameters have been read. Thus, you\n"
               "     can format the list of points in a nice tabular way as shown.\n\n"
               " set pe-thresh-rot < value >:\n"
               "     hip looks at the sum of the outward normals of matching periodic\n"
               "     patch pairs to determine what the required periodicity operation\n"
               "     is.\n"
               "     In the case of translation, the sum of normals is zero as they\n"
               "     are perfectly opposed. Rotations are expected along major axes\n"
               "     hence the sum of normals is zero along the rotation axis\n"
               "     while the other components are non-zero.\n"
               "     pe-thresh-rot sets the magnitude below which a vector\n"
               "     component is considered zero. Default is 1e-2.\n"
               " set pe-write < [0,1] >:\n"
               "     List [1] or don't list [0] periodic boundaries in the AVBP\n"
               "     exBound file. It will always be listed in the inBound file.\n"
               "     Default is 1.\n"
               " set pe-fix< [0,1] >:\n"
               "     Fix [1] or don't fix [0] mismatching periodic vertices.\n"
               "     Default is 1.\n"
               ) ;
    else if ( !strncmp ( keyword, "fc", 2 ) )
      printf ( " Face options, two characters after fc- are sufficient.\n"
               " set fc-warnings < matching cut boundary >\n"
               "     Warn for every {matching, cut, or boundary} face that is interior\n"
               "     if set to 1. Default is 1 1 1.\n"
               "     A fourth numeric argument allows to control aborting or not\n"
               "     if unmatched faces are found: 0 to continue, 1 to abort (default).\n" 
               " set fc-remove < matching cut boundary >\n"
               "     Remove every {matching, cut, or boundary} face that is interior\n"
               "     if set to 1. Default is 1 1 1.\n"
               "     A fourth numeric argument allows to visualise unmatched faces.\n"
               "     If set to 1, a boundary 'hip_unmatched' will be added listing\n"
               "     all unmatched faces. Default is 0, no extra boundary.\n"
               "     Have a look at 'mark interface' to control how internal faces\n"
               "     (none, one side, both sides) are written to file.\n"
               " Note that a check level of 5 is needed to trigger face checks.\n" ) ;
    else if ( !strncmp ( keyword, "dg", 2 ) )
      printf ( " Degeneracy options, two characters after dg- are sufficient.\n"
               " set dg-coll < 1,0 >\n"
               "     Attempt to fix elements out of a multiblock grid that have\n" 
               "     collapsed edges/faces. Default is 0, dont do it.\n"
               " set dg-lrgAngle < 1,0 >\n"
               "     Try to cut cells in two to halve too large angles. Default is 0.\n"
               " set dg-angle < maxAngle >\n"
               "     Cosine of the threshold angle to cut, default is -.75.\n" ) ;
    else if (!strncmp ( keyword, "ad", 2 ) )
      printf ( " Adaptation options.\n"
               " set ad-lv < maxLevel >\n"
               "     Set the maximum refinement depth to maxLevel.\n"
               " set ad-up < upRef >\n"
               "     Allow hip to upgrade to the next adaption pattern if upRef.\n"
               "     edges already are refined. This helps to smoothen the refined\n"
               "     grid. Default is 1., no upgrade.\n"
               " set ad-per\n"
               "     If non-zero, adapt periodic boundaries. Otherwise\n"
               "     freeze the periodic surfaces (no adaptation).\n"
               "     Default is 0, no adaptation of periodic surfaces.\n" ) ;
    
    else if (!strncmp ( keyword, "in", 2 ) )
      printf ( " Interpolation options.\n"
               " set in-recoType < recoType >\n"
               "     Set the type of reconstruction. Currently available are\n"
               "       'el': linear discontinuous reconstruction within an element,\n"
               "       '1','2': first and second order least-squares reconstruction.\n"
               "       'minnorm': minimum norm solution reconstruction.\n"
               "       'flag': not really an interpolation: find the value at\n"
               "          the nearest point. If it is non-zero use that, subject to\n"
               "          the same rim behaviour as interpolation.\n"
               "     Default is `minnorm`.\n"
               "     \n"
               " Parameters for element-based interpolation:\n"
               " set in-rim < rimFrac >\n"
               "     If both the providing and receiving grid have a variable of the\n"
               "     same name, hip can do a partial interpolation for this where\n"
               "     only the part covered by the providing domain will be updated.\n"
               "     This includes a rim around the providing domain of rimFrac*hEdge.\n"
               "     where hEdge is the longest edge length of the nearest face in the\n"
               "     providing mesh.\n"
               "     The default is rimFrac=infinity, i.e. extrapolation to everywhere.\n"
               "     Note however that the extraplated value is limited to not exceed the\n"
               "     values found in the nearest cell. In practice this results in using\n"
               "     the value of the nearest point.\n"
               " set in-fc-tol < fcTol >\n"
               "     When searching for the containg cell to interpolate on, this face\n"
               "     tolerance controls how far a node can be away from the closest face\n"
               "     and still be considered inside the cell.\n"
               "     The factor fcTol multiplies a typical length scale of the element,\n"
               "     e.g. hElem which is the longest edge around the face.\n"
               "     The default is fcTol=0.1.\n"
               " set in-full-search_dist <distFac>\n"
               "     h!p searches for the containing element by walking on the donor\n"
               "     grid from element to element, starting from the nearest point.\n"
               "     On distorted meshes, or thin-walled geometries, this walk may get\n"
               "     stuck before reaching a containing element.\n"
               "     If the final element does not actually contain the target point,\n"
               "     but is within a distance of distFac*hElem of any of its forming\n"
               "     nodes, then hip will perform a global search and compute the\n"
               "     exact distance to the nearest face, not just the node.\n"
               "     This can get very expensive, especially if the domains do not\n"
               "     overlap.\n"
               "     The default value is zero, no refined global search is done.\n"
               "     The value of dist_{full}=0 is augmented by += 2.0$ when computing\n"
               "     inter-grid interpolation coefficients for multi-grid, as higher\n"
               "     accuracy is required there.\n"
               "\n"
               " Parameters for least-squares interpolation:\n"
               " set in-nr < factor >\n"
               "     Sets the factor of redundancy of the least-squares stencil.\n"
               "     Default is 1.5, ie for a 2-D quadratic reco 6 coefficients need\n"
               "     to be determined and 1.5*6=9 nodes are used.\n"
               " set in-lambda < lambda >\n"
               "     A factor that scales the distance used for the weighting.\n"
               "     Default is 1., larger values make the weighting more even.\n"
               "     At lambda > 1.e25 all weights are set to unity.\n"
               " set in-distance-inside <distFac>\n"
               "     Similar to in-rim this sets the factor for the overlap for least-\n"
               "     squares interpolation. In this case, interpolation is based on\n"
               "     point clouds rather than element containment, hence the width of\n"
               "     the overlap rim is computed as this factor multiplied with the\n"
               "     global epsOverlap. \n"
               "     The default value for this factor is infinity, so extrapolation to\n"
               "     the entire domain. Note however that, as opposed to element-based\n"
               "     interpolation, this is indeed extrapolation, there is no limiting\n"
               "     of the values.\n"
                "\n" ) ;
  return ;
}

void hlp_ch ( ) {
  printf ( " check:\n"
           "     validate the current grid.\n"
           " check grid:\n"
           "     validate the current grid, default.\n"
           " check bnd:\n"
           "     check the periodic boundary setup, find transformations.\n"
           " check per:\n"
           "     find transformations, check matching of periodic vertices.\n"
           " check quality < -b mBuckets> < -w mWorst > :\n"
           "     Computes and displays statistics on equi-angle skewness.\n"
           "     equivolume skewness, element squish index and element\n"
           "     volume smoothness.\n"
           "     The optional -b argument controls the granularity of\n"
           "     of the distribution statistics, default is 10.\n"
           "     The optional -v argument controls how many of the worst\n"
           "     elements are written to vtk for visualisation, default is 0.\n"
           "     The optional -w argument controls how many of the worst\n"
           "     elements are printed to the screen, default is 0\n"
           "     If verbosity is increased to 5, also the node coordinates\n"
           "     of those volumes are shown.\n"
           ) ;
  return ;
}

#ifdef ADAPT_HIERARCHIC
void hlp_ad ( ) {
  printf ( " adapt:\n"
           "       adapt the current grid. You will want to specify a choice of\n"
           "       sensor to use with a percentage of how much to derefine and\n"
           "       to refine, as well as a flag for isotropy.\n"
           "       Default arguments are <div-rot .1 .3 1>\n"
           "       In addition, you can limit the adaption to certain regions of\n"
           "       the mesh, a list of cylinder-like 'haribos' which are specified\n"
           "       by beginning and end of a line and a maximum distance to it.\n"
           "       Default is the entire domain.\n"
           "       Note also the commands set ad-lvl and set ad-up in the\n"
           "       set adapt section.\n"
           /*" adapt reset:\n"
             "       reset all refinenement marks on the edges.\n"
             "       This will actually have an effect from the next release on.\n"
             "       For the time being, every call to adapt resets all marks.\n" */
           " adapt hrb-reset:\n"
           "       Reset the list of haribos to the entire domain.\n"
           " adapt hrb-add < x,y,{z}Begin x,y,{z}End radius >\n"
           "       Add a haribo to the list. E.g.\n"
           "          ad hrb-ad    0. 0. 0.    1.5 0. 0.    1.5\n\n"

           " adapt diff-< state variable deref ref iso >:\n"
           "       adapt a grid based on differences of the state variable ( one of\n"
           "       rho, q, p, t, #), where # is the number of a state variable\n"
           "       starting from 1.\n"
           "       Derefining for the deref lowest cells, refining for the ref \n"
           "       highest cells, using isotropic adaption only if iso=1.\n"
           "       Example: diff-q .1 .3 1 refines on differences of the norm of\n"
           "                the velocities, derefining the 10%% lowest, refining the\n"
           "                30%% highest in an isotropic way.\n"
           "       If the refinement percentage is given negative, hip refines\n"
           "       and derefines the ABS(ref)*mean deviation from the average.\n"
           " adapt avg-< state variable deref ref iso >:\n"
           "       the same as diff-, except that the average value along an edge\n"
           "       is taken. Useful e.g. to adapt a flame to the reaction rate.\n"
           " adapt divrot < deref ref >:\n"
           "       adapt a grid using a div-rot sensor, derefining the deref cells\n"
           "       with the lowest values, refining all the ref highest cells.\n"
           " adapt nr < nEl0 eg0 nEl1 eg1 ...> \n"
           "       adapt on specific elements a bitmask of edges. Only one line.\n"
           "       This, of course, is for debugging only.\n"
           " adapt file type fileName\n"
           "       adapt specific elements as given in na list. \n"
           "       Currently supported is type 'ascii', with comment lines preceded.\n"
           "       by %%, hash or !, then for each element a pair of `elno, bitmark', where\n"
           "       bitmark is nonzero for each edge that needs adapting, or a negative\n"
           "       value for derefinement of that element.\n"
           "       Elements and bit pattern are read as one pair per line, an omitted\n"
           "       bit pattern (i.e. only element no) marks all edges of that element.\n"
           " adapt vf adjoint deref ref iso smooth\n"
           "       adapt using residual sensors and an adjoint solution.\n"
           ) ;
  printf ( "\n" ) ;
  return ;
}
#endif

#ifdef ADAPT_HIERARCHIC
void hlp_bu ( ) {
  printf ( " buffer: \n"
           "        Buffer an adapted unstructured grid to remove hanging nodes.\n"
           " buffer un: \n"
           "        Unbuffer an adapted unstructured grid.\n" ) ;
  return ;
}
#endif

void hlp_mm () {
    printf ( " As of version 19.01, periodicity is recognised if declared.\n"
             " Use 'set mm-layer' to influence how many layers off periodic\n"
             " bcs are included in the first refinement.\n"
             "\n"
             " mmg keyword <options>: Remeshing optons with mmg3d.\n"
             " mmg isoFactor <-f factor>: Set a constant factor to multiply edge lengths\n"
             "     with to <factor>, default is factor = 1.0, values below 1 refine.\n"
             " mmg isoVar <>: Use a variable field as a refinement scaling factor,\n"
             "     e.g. a value of 0.5 halves the mesh width at that point.\n" 
             "     Options:\n"
             "  -g hGrad: maximal spacing gradient, default: hGrad=1.4\n"
             "  -h hausdorff: surface fidelity, distance to initial mesh,\n"
             "     smaller values force higher accuracy. Default: hausdorff=0.01\n"
             "  -i do not interpolate the solution to the new grid\n"
             "  -l set a lower bound for mesh width hMin,\n"
             "  -p fix the number of layers of elements for the first refinement\n"
             "     sweep around periodic boundaries. Default is 10\n"
             "  -s save meshes before and after refinement in medit .mesh format.\n"
             "  -u set an upper bound for mesh width hMax,\n"
             "  -v nVar: nVar=1=rho is the default.\n"
             " mmg isoMap: Replace sensor variable with metric value for plot.\n"
             "     Options:\n"
             "  -l set a lower bound for mesh width hMin,\n"
             "  -u set an upper bound for mesh width hMax,\n"
             "  -v nVar: nVar=1=rho is the default.\n"
             "  Note that with periodic meshes this becomes obsolete: a separate\n"
             "  variable with the local lengthscale is created and can be written\n"
             "  to file.\n"
             "  For 2D and non-periodic 3D cases the surface mesh on selected\n"
             "  boundaries can be frozen during adaptation using set bc-mark.\n"
             )  ;
    printf ( " decimate <options>: strongly coarsen a 3D surface mesh with mmgs.\n"
             "   The decimated mesh can be used for user interaction with GUIs.\n"
             "   It contains only triangles, but also lists its perimeter and\n"
             "   feature edges. Currently (as of hip 1.50), only the ensight file\n"
             "   file format supports writing edges in 3D.\n"
             "   -a: set the threshhold value for the scalar product between\n"
             "       unit normals of faces forming an edge. Feature edges are\n"
             "       listed if the scalar product falls below that value.\n"
             "       Default value is 0.8\n"
             "   -f: factor multiplying the edge width. For coarsening, the default\n"
             "       value is 10e2.\n"
             "   -g: allowable gradient of mesh spacing, A larger value allows\n"
             "       stronger gradation in mesh width. Default is -1, ignore.\n"
             "   -h: Hausdorff distance to original geometry. Lower values force.\n"
             "       closer fit to the initial grid, but result in denser meshes.\n"
             "       Default value for decimation is 0.001 (compared to refinement 0.01).\n"
             "   -l: lower bound of mesh spacing hMin.\n"    
             "   -u: upper bound of mesh spacing hMin.\n" 
             "   -s: save the meshes before and after decimation in medit .mesh files.\n" 
             ) ;

  return ;
}

void hlp_mg ( ) {
  printf ( " mg < { mLevels } >: \n"
           " mg -l mLevels\n"
           "    coarsen an unstructured grid by edge-collapsing,\n"
           "    creating mLevels coarser levels. Default is 1. See <help set>\n"
           "    for parameters to influence the collapsing.\n"
           "    The operation creates a new mesh for each level. Currently\n"
           "    the hdf format can only write independent mesh levels using\n"
           "    the -l command. Writing the sequnce of grids with inter-grid\n"
           "    connectivity is work in progress.\n") ;
  printf ( " mg volMin< threshold >: \n"
           " mg -v threshold\n"
           "    remove elements bwith a volume below\n"
           "    threshold by edge-collapsing, threshold is a required\n"
           "    parameter, no default value is provided. The mesh is\n"
           "    coarsened in situ, ie no new mesh is created.\n" ) ;
  printf ( " mg sequence gr1 gr2 [ ...]\n"
           " mg -s gr1 gr2 [ ...]\n"
           "    connect independently generated grids as multi-grid sequence\n"
           "    and compute inter-grid connectivity and transfer operators\n" ) ;
  printf ( " mg test gr1 gr2\n"
           "    test restriction (if gr1<gr2) or prolongation operators\n"
           "    using a test solution of rho=1, u/v/w = x/y/z, p=xy+yz+zx.\n"
           "    Solutions at the grid levels can then be writting into gmsh.\n" ) ;
  return ;
}



void hlp_interface ( ) {
  printf ( " iface [options] < {mixing, sliding} gridNr1 bc1 gridNr2 bc2 ifc_type >: \n"
           "       compute a non-conformal interface (mixing or sliding plane) \n"
           "       between boundary bc1 of grid matching gridExpr1 and\n"
           "       bc2 of gridExpr2. \n"
           "       Supported types of interface ifc_type are\n"
           "       `r' for radial interface with const. r around z-axis,\n"
           "       'x' for axial interface with x=const, or \n"
           "       'z' for axial interface with z=const. \n"
           "     optional arguments:\n"
           "     -nIfcName: assign IfcName as a name to this interface,\n"
           "       name is written to hdf.\n"
           );
  return ;
}

void hlp_interpolate ( ) {
  printf ( " interpolate grid < { gridNr axi } >: interpolate a solution for the \n"
           "       current grid from grid numbered gridNr. Default is the first\n"
           "       mesh read, numbered 1. Use list grids to find out about the grid\n"
           "       numbers.\n"
           "       The interpolation algorithm is controlled with set in-reco-type\n"
           "       and can be either minnorm, 1st O. least squares, 2nd O. least squares,\n"
           "       element.\n"
           "       \n"
           "       \n"
           "     - If axi is specified, hip will do a 2D interpolation along x,r\n"
           "       and apply the appropriate rotation. \n"
           "     - If the mesh to interpolate from is 2D, a zero tangential velocity\n"
           "       component is supplied.\n"
           "     - If the the target mesh is not fully covered by the donor mesh, hip\n"
           "       will extrapolate the solution within a specified rim beyond\n"
           "       the overlap for those variables that exist in both grids. Outside\n"
           "       this rim the variable in the recipient grid is retained. The\n"
           "       width of that rim is calculated as a factor times the the maximal\n"
           "       edge lengths of the boundary face nearest to the outlying point\n"
           "       The factor can be set with set in-rim, default value\n"
           "       is infinity, hence full extrapolation of the donor solution.\n"
           "     - See the manual on where the solution is taken from if donor and\n"
           "       target variable sets differ. If the variable exists only in the\n"
           "       target mesh, then it is retained as is. If the variable exists\n"
           "       only with the donor, in case of overlap that variable is used,\n"
           "       otherwise the corresponding component of the freestream vector on\n"
           "       the donor mesh is used. The freestream vector can be set with\n"
           "       'set var freestream'.\n"
           " interpolate line < var xB yB zB  xE yE zE file >\n"
           "       Interpolate variable var along a line running from xyzB to xyzE,\n"
           "       write it ASCII to file.\n"
           "       var can be a number 1 .. mUnknowns, the number of unknowns. Then\n"
           "       that variable is picked from the solution vector.\n"
           "       var can also be one of r,u,v,w,q,p,t.\n"
           "       In the file hip lists a comment line indicated by a leading hash\n"
           "       restating the coordinates of the end points of the line.\n"
           "       Each line then lists the coordinates of each point where the\n"
           "       line intersects a face of an element, the distance of that point\n"
           "       to the beginning of the line, the interpolated quantity and\n"
           "       the accrued integral.\n"
           " intergrate rectangle < var swf sef nwf swr mE mN file >\n"
           "       computes the integral of var along a number of lines traced\n"
           "       between two opposite rectangles of a box.\n"
           "       swf ... nwf are triplets of coordinates for the corners\n"
           "       of the \"front\"-plane definining the start points as x/y/zB\n"
           "       in interpolate line, swf defining the South-West-Front point,\n"
           "       nef defining the North-East-front point, etc.\n"
           "       swr defines the South-West-rear point on the \"rear\" plane.\n"
           "       mE is the number of lines in the East-West, mN in the North-South\n"
           "       direction.\n"
           "       The variable var is integrated and the data written to file.\n"
           "       The file lists a hash-commented header and then for each line\n"
           "       the coordinates of the beginning xyzB and the integral of the\n"
           "       variable along the line.\n"
           " interpolate plane < x1 y1 z1 .... z3 >\n"
           "       Interpolate the solution onto a planar cut through the mesh\n"
           "       given by three points x1 ...z3. The resulting mesh is a disjoint\n"
           "       collection of faces (duplicated corner nodes) without boundaries\n"
           "       suitable for plotting but not for calculation.\n"
           " interpolate bnd <zSlice> \n"
           "       Applied to a 3D grid this command will extract a 2D grid at\n"
           "       the boundary surface z=zSlice. This would typically be used\n"
           "       for 2D plotting of solutions on grids extruded in z.\n"
           ) ;
}

void hlp_wr ( ) {
  printf ( " write avbp < { rootFile level } > \n"
           "       write the finest level of the current grid to an avbp-format.\n"
           "       If no filename is specified, the root defaults to grid.\n"
           "       level indicates the level of coarsened grid to be written\n"
           "       default is 0, the finest grid.\n"
           "       The AVBP format treats periodic boundaries similarly to \n"
           "       partition boundaries and lists them in the .inBound file. If\n"
           "       the flag periodic is set (see help on set), periodic boundaries\n"
           "       are also listed in the exBound file.\n"
           "       Three periodic pairs are possible labeled with \n"
           "       hip_per_inlet_[123], resp. outlet with appended text specifying\n"
           "       the coordinate value of each periodic plane. (See eg. the help\n"
           "       on copy 3D.) All periodic patches on the same periodic plane of\n"
           "       the  various chunks must point to the same boundary condition,\n"
           "       not just have the same text. Note that when written and reread,\n"
           "       the same text in a bc will collapse to the same bc.\n" ) ;
  printf ( " write avh < rootFile >.\n"
           "      Write a set of AVBP 6.X files with the solution in hdf,\n"
           "      otherwise same as per avbp above.\n") ;
  printf ( " write avbp4.2 < { rootFile } > \n"
           "       Deprecated variant, the format of the asciiBound file changed\n"
           "       with version 4.7.\n" ) ;
  printf ( " write avbp4.7 < { rootFile } > \n"
           "       The current default (hence equivalent to wr av).\n" ) ;
  printf ( " write avbp5.1 < { rootFile } > \n"
           "       Additional variables in the solution file.\n" ) ;
  printf ( " write avbp5.3 < { rootFile } > \n"
           "       The latest and default version of the  solution file.\n" ) ;
  printf ( " write avbp5.3eg < { rootFile } > \n"
           "       The latest version of the solution file with an added\n"
           "       file for the graph between elements in serial CSR format\n"
           "       for METIS partitioning.\n" ) ;
  printf ( " write avad < { rootFile } > \n"
           "       write the current grid to an avbp-format. All levels, parents,\n"
           "       children, adapted edges, the whole thing. This information is\n"
           "       needed for derefinement. Reading is done with read avbp.\n" ) ;
  printf ( " write avbp4 < { rootFile } > \n"
           "       write the current grid to a pre avbp 4.0-format, including\n"
           "       parents, children, adapted edges, element marks. This\n"
           "       information is needed for running AVBP on hanging nodes.\n"
           "       Reading is done with read avbp4.\n" ) ;
#ifdef CGNS
  printf ( " write cgns < { fileName } >\n"
           "       write to CGNS format, Hip currently only writes the grid only\n" ) ;
#endif
  printf ( " write dpl < { dplFile } > \n"
           "       write the current 2-D grid to a dpl-format. If no filename is\n"
           "             specified, the file defaults to hip.dpl.\n" ) ;
  printf ( " write dplad < { dplFile } > \n"
           "       write an adaptation map of the current 2-D grid to a dpl.\n"
           "       Elements are flat shaded on density marking: 0: no refinement\n"
           "       2: derefinement, 4: buffering, 6.5/8.5: directional refinement,\n"
           "       11/13: isotropic refinement.\n" ) ;
  printf ( " write ensight < { fileName } >\n"
           "       write to ensight gold binary format. 3D only. Ensight needs to\n"
           "       identify the flow vector which is done by variable name.\n"
           "       hip looks for u,v,w (primitive) or rhou,rhov,rhow (cons.)\n"
           "       Rename your variables with 'var name', if necessary.\n"
           "       The flow variables (cat 'ns') will always be written, but\n"
           "       from the others only the flagged variable fields. Use the\n"
           "       'var flag' command for that.\n"
           "    -n Version 1.35 supports writing and reading node ids given the\n"
           "       option -n.\n" 
           "       These are not necessary for simple plotting, but are needed to piece\n"
           "       together boundary parts (patches) and volume mesh for some\n"
           "       operations. \n"
           "       By default node id's are off, You can turn node ids on by\n"
           "       supplying the '-n on' or '-n 1' option.\n" 
           "    -2 Paraview only plots 3D. By default, hip will extrude a 2D mesh\n"
           "    -3 to 3D when writing to ensight. This can be turned off by the\n"
           "       option -2, turned back on by -3.\n"
           "    -a Writing of ascii files is triggered with the option -a.\n" 
           "    -s -s off or -s0 suppresses writing of boundary faces. Default.\n"
           "       is 'on'.\n"
  );
  printf ( " write fieldview < { fileName } >\n"
           "       write to fieldview binary format. No wall info for cells,\n"
           "       at the moment only flow variables. A 2D grid is copied to 3D.\n" );

  printf ( " write gmsh < { fileName } >\n"
           "       write to gmsh 2.0 ascii format, Hip currently only writes\n"
           "       the N-S variables as density, velocity, pressure.\n" );

  printf ( " write hdf < {fileNameRoot}  >.\n"
           "      write an unstructured grid in AVBP's hdf format.\n"
           "      hip will write produce a mesh file, a 6.X asciiBound file and\n"
           "       if it exists a solution file\n" ) ;
  printf ( "   Options to write hdf -[abeflnsz67]:\n" ) ;
  printf ( "   -6:  Write a 6.X asciiBound file with the mesh (default)\n");
  printf ( "   -7:  Write a 7.X asciiBound file with the mesh\n");
  printf ( "   -a0:  don't write all vars, but only write the standard set.\n"
           "      The default value is -a, i.e. write all variables.\n" 
           "      See help on 'var flag' to exclude some vars\n"
           "      from being written.\n" ) ;
  printf ( "   -b: Write only the boundary faces and a renumbered, shortend\n"
           "      list of boundary nodes and, if present, their solution.\n"
           "   -b0: write no bnd grid. Default: write vol and bnd grids.\n" ) ;
  printf ( "   -e:  include a METIS style elGraph in the mesh file.\n" ) ;
  printf ( "   -f:  include the /Faces chapter with all internal\n"
           "      faces in the mesh file.\n" ) ;
  printf ( "   -h: write parent/child hierarchy for hierarchically adapted\n"
           "       grids.\n" ) ;
  printf ( "   -l:  write a coarser grid level to file. Level 0 is the.\n"
           "      finest level, the default. Level 1 is the next coarser, etc.\n" ) ;
  printf ( "   -n:  do not write volume nodes and connectivity.\n"
           "      This option implies the -b option.\n" ) ;
  printf ( "   -p: allow writing of primitive flow variables to hdf, rather than\n"
           "      automatically promoting flow variables to conservative.\n"
           "      Note: as of 18.03, the xdmf file still only supports\n"
           "      conservative variables.\n" ) ; 
  printf ( "   -s:  only write an updated solution file, but no mesh.\n" ) ;
  printf ( "   -v:  include the node to element connectivity.\n" ) ;
  printf ( "   -z0:  do not write the zones. The default value is -z, i.e.\n"
           "      write the zones.\n" ) ;

  printf ( " write mhdf < { hdfFile } >\n"
           "       write a node mapping from structured ijk-chronological order\n"
           "       to the number of the unstructured grid. This correctly maps\n"
           "       merged nodes at block interfaces.\n"
           "       Output format is hdf, but only the mapping is written,\n"
           "       not the grid.\n"
           "       Note that this requires the mapping to be established\n"
           "       when the mesh is copied to uns, i.e. 'copy 2uns map'." ) ;

  printf ( " write pts < { ptsFile } { keepState } > \n"
           "       write the cut interface of the grid. In 2-D a .pts file,\n"
           "       in 3-D a .points and a .faces file are produced.\n"
           "       If no filename is specified, the file defaults to hip.\n"
           "       keepState on 1 (default) writes the cut around the kept part,\n"
           "       0 writes the cut around the ommitted part.\n" ) ;
  printf ( " write n3s < { gridFile } { solFile } >\n"
           "       write to n3s binary format. N-S solution only, no periodicity,\n"
           "       at the moment. hip will write the extended hybrid n3s format.\n" );
  printf ( " write screen\n"
           "       dump the current grid to the screen.\n" ) ;
  printf ( "\n" ) ;
  return ;
}

void hlp_zo ( ) {
  printf ( " zone < { list } >\n"
           "       List all zone names present in the current grid. Same as `list zone'.\n" ) ;
  printf ( " zone list all\n"
           "       List all zone names present in all grids. Same as `list zone all'.\n" ) ;
  printf ( " zone list {expr}\n"
           "       List details of the zones in the current grid matching `expr'.\n"
           "       Note that using `al' or `all' as an expression will actually be\n"
           "       intepreted as the `zone list all' command described above.\n" ) ;

  printf ( " zone add < label >\n"
           "       Add a zone with the given label to the current grid.\n"
           /* "       If no flags are active, a zone grabs all elements in the grid.\n" */
           /* "       If vertex flags are on, only those elements with at least one\n" */
           /* "       flagged vertex are associated with the zone.\n" */
           /* "       Adding elements to a zone nullifies all vertex flags.\n" */
           ) ;

  printf ( " zone  < expr > modify < label >\n"
           "       Modify the label of any zone matching expr.\n"
           "       Note: zones are identified by their chronological number.\n"
           "       or by their name/label:\n"
           "       - any expression starting with a - or a number is parsed as a\n"
           "         numerical expresion.\n"
           "       - a negative zone number is an alias for the most recent zone.\n"
           "       - any other expression is interpreted as a string match, wildcards\n"
           "         such as ?wall* behave the same as in Unix filename matches.\n"
           "       - names starting with 'add', 'list', 'mod', 'del' or 'param'\n"
           "         should be avoided ad they result in ambiguous syntax of zone\n"
           "         commands.\n"
            ) ;

  printf ( " zone < number > del\n"
           "       delete the zones in the number range, including their parameters.\n"
           "       Set zone pointers for all elements in this zone to zero.\n"
           "       Ranges are numbers separated by commata, e.g. 1-4,6,9-58\n" ) ;

  printf ( " zone < number > element <add,del> selection \n"
           "       add or delete  the selected elements to/from the zone .\n"
           "       Selection can be 'nodes' if based on flagged nodes (see 'flag'),\n"
           "       'all': all elements, 'remaining': all untagged elements.\n" ) ;

  printf ( " zone < number > {solparam,param} add < name int iVal >\n" ) ;
  printf ( " zone < number > {solparam,param} add < name dbl dVal >\n" ) ;
  printf ( " zone < number > {solparam,param} add < name vec dVal_x dVal_y {dVal_z} >\n"
           "       Add to zone 'number' a new scalar int/dbl/vec labelled 'name'\n"
           "       with values 'iVal,dVal', etc.\n"
           "       Invoked with 'solparam', this will be a variable solution parameter\n"
           "       written to the solution file, with 'param' this will be a mesh\n"
           "       parameter written to the mesh file.\n" ) ;

  printf ( " zone < number > {solparam,param} mod < name int iVal >\n" ) ;
  printf ( " zone < number > {solparam,param} mod < name dbl dVal >\n" ) ;
  printf ( " zone < number > {solparam,param} mod < name vec dVal_x dVal_y {dVal_z} >\n"
           "       Modify the parameter of an existing scalar int/dbl/vec labelled name\n"
           "       Note: parameters of a zone are identified by name.\n" ) ;

  printf ( " zone < number > {solparam,param} add < name iArr > dim iVal\n" ) ;
  printf ( " zone < number > {solparam,param} add < name dbl  > dVal\n"
           "       Add a new int/dbl array of dim labelled name to zone number\n" ) ;

  printf ( " zone < number > {solparam,param} mod < name int > iVal\n" ) ;
  printf ( " zone < number > {solparam,param} mod < name dbl > dVal\n"
           "       Modify the values of an existing array int/dbl labelled name\n"
           "       at zone number\n" ) ;

  printf ( " zone < number > {solparam,param} del < name >\n"
           "       Delete parameter name from zone number.\n" ) ;

  return ;
}



/* hip_cython */
ret_s help_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN] ;

  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;


  if ( !strncmp ( keyword, "me", 2 ) )  {
    printf ( "   Do you really think that hip can help you? hip is very powerful,\n"
	     "   but your problems might be beyond its means.\n\n" ) ;
  }

#ifdef ADAPT_HIERARCHIC
  else if ( !strncmp ( keyword, "adapt", 2 ) ) hlp_ad () ;
#endif
  else if ( !strncmp ( keyword, "attach", 2 ) ) hlp_at () ;
  else if ( !strncmp ( keyword, "bc", 2 ) ) hlp_bc () ;
#ifdef ADAPT_HIERARCHIC
  else if ( !strncmp ( keyword, "buffer", 2 ) ) hlp_bu () ;
#endif
  else if ( !strncmp ( keyword, "check", 2 ) ) hlp_ch () ;
  else if ( !strncmp ( keyword, "copy", 2 ) ) hlp_co () ;
  else if ( !strncmp ( keyword, "dec", 2 ) ) hlp_mm () ;
  else if ( !strncmp ( keyword, "cut", 2 ) ) hlp_cu () ;
  else if ( !strncmp ( keyword, "flag", 2 ) ) hlp_fl () ;
  else if ( !strncmp ( keyword, "generate", 2 ) ) hlp_ge () ;
  else if ( !strncmp ( keyword, "iface", 2 ) ) hlp_interface () ;
  else if ( !strncmp ( keyword, "interpolate", 2 ) ) hlp_interpolate () ;
  else if ( !strncmp ( keyword, "list", 2 ) ) hlp_li () ;
  else if ( !strncmp ( keyword, "mark", 2 ) ) hlp_ma () ;
  else if ( !strncmp ( keyword, "mm", 2 ) ) hlp_mm () ;
  else if ( !strncmp ( keyword, "mg", 2 ) ) hlp_mg () ;
  else if ( !strncmp ( keyword, "read", 2 ) ) hlp_re () ;
  else if ( !strncmp ( keyword, "set", 2 ) ) hlp_se () ;
  else if ( !strncmp ( keyword, "translate", 2 ) ) hlp_tr () ;
  else if ( !strncmp ( keyword, "variable", 2 ) ) hlp_va () ;
  else if ( !strncmp ( keyword, "vis", 2 ) ) hlp_vi () ;
  else if ( !strncmp ( keyword, "writ", 2 ) ) hlp_wr () ;
  else if ( !strncmp ( keyword, "zone", 2 ) ) hlp_zo () ;


  else if ( !strncmp ( keyword, "hip", 2 ) ) {
    printf ( "   hip initially meant ,,Hybrid Interaction Protocol''. But actually,\n"
             "   it is a triply recursive acronym (you all use GNU software, don't\n"
             "   you?), ,,hip is pretty hip''. But seriously, ,,hip is not square''.\n"
	     "\n" ) ;
  }
  else if ( !strncmp ( keyword, "domino", 2 ) ) {
    printf ( "   Domino software's answer to all of the grid generation problems\n"
	     "   you never had. \n\n" ) ;
  }
  else if ( !strncmp ( keyword, "disclaimer", 2 ) ) {
    printf ( "   \"We don't claim Interactive EasyFlow is good for anything --- if you\n"
             "   think it is, great. But it's up to you to decide.  If Interactive\n"
             "   EasyFlow doesn't work: tough. If you lose a million because\n"
             "   Interactive EasyFlow messes up it's you that's out the million not\n"
             "   us. If you don't like this disclaimer: tough. We reserve the right to\n"
             "   do the absolute minimum provided by law, up to and including nothing.\n"
             "   This is basically the same disclaimer that comes with all software\n"
             "   packages, but ours is in plain English and theirs is in legalese.\"\n\n"

             "   [John Shore: Why I never met a programmer I could trust."
	     " CACM 31/4, p. 372]\n\n" ) ;
  }
  else {
    printf ( "   The following commands are recognized:\n     "
#ifdef ADAPT_HIERARCHIC
             "adapt, "
#endif
             "attach, "
             "bc (boundary condition), "
#ifdef ADAPT_HIERARCHIC
             "buffer, "
#endif
             "check, "
             "copy, "
             "cut, "
             "decimate,\n "
             "disclaimer,"
             "domino,"
             "generate, "
             "hip,"
             "iface (interface), "
             "interpolate, "
             "list, "
             "mark, "
             "mg, "
             "mm, "
             "read, "
             "set,\n     "
             "translate, "
             "variable, "
             "visualise, "
             "write, "
             "zone.\n\n"

	     "     In general, all commands can be abbreviated to two letters,\n"
	     "     but often specialised subversions require 4 or 5, as indicated.\n\n"
	    
	     "     Type help < command > for details of this command or on\n"
             "     'me', 'hip', 'disclaimer'.\n"
             "     hip -v provides the version number.\n\n"

	     "     In the help pages necessary parameters are identified as < nec_param >,\n"
	     "     A choice of parameters are given as [ choice1 choice2 .. ],\n"
	     "     a separating space works on all platforms, but tabs or commata\n"
	     "     in general work as well.\n"
	     "     Optional parameters are given as { optParam }.\n"
	     "     ( Note: naturally you do not type the <{[;-)]}> ).\n\n" ) ;
  }
  flush_buffer () ;
  return (ret) ;
}
