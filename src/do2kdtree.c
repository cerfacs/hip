
/*
  do2kdtree.c:
*/

/*! wrapper for lib_do/tree calls to kdtree.
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  17Jun11; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "kdtree.h"

/* All of this for sq_distance_dbl. */
#include "cpre.h"
#include "proto.h"

#define MAX_DIM 3
#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )



extern const int verbosity ;
extern char tree_msg[] ;

extern const char version[] ;


struct _kdroot_struct {
  int mDim ;                  /* That many dimensions. */
  double minDist ;            /* Min size of range. */
  void *pKdt ;  /* handle for the kdtree. */

  /* extraction function, needs to return a double here. */
  double *(*data2valu) ( const void *) ; /* Extraction function. */
  void *pFam ;
  double llRoot[MAX_DIM] ; /* Bounding box of the root */
  double urRoot[MAX_DIM] ;
} ;


/****************************************************************************
private
****************************************************************************/


/******************************************************************************

  dkd_add2list:
  Add an entry to the sorted list of data and distances. Drop duplicates.
  
  Last update:
  ------------
  14Dec19; make private, rename dkd_
  17Jun11; copied to do2kdtree.
  12May11; implement pointer artihmetic on void*.
  8Sep10: conceived.
  
  Input:
  ------
  mData: length of pNearestData and pNearestDist
  pOtherData: data to add
  otherDist: distance of other data

  In/Out:
  -------
  *pmD:  number uf current entries
  *pNearestData: list of nearest entries
  *pNearestDist: distance of nearest entries
  
  */

void dkd_add2list ( int mData, int *pmD, 
                    const void *pNearestData[], ulong_t dataSize,
                    double nearestDist[], 
                    const void *pOtherData, double otherDist ) {

  int n0, n1, n ;

  if ( *pmD == 0 ) {
    /* Empty list. */
    pNearestData[0] = pOtherData ;
    *nearestDist = otherDist ;
    *pmD = 1 ;
    return ;
  }

  /* Find the slot n1 in the list where this data should be written into. 
     First bracket the search interval. */

  else if ( otherDist >= nearestDist[*pmD-1] ) {
    /* Append at the end of the list, but compare to the final listed elem
       for duplication . */
    n1 = *pmD ;
  }

  else if ( otherDist <= nearestDist[0] ) {
    /* Insert at the top of the list. */
    if ( pOtherData == pNearestData[0] )
      /* Duplicate. Drop it. */
      return ;
    else
      n1 = 0 ;
  }

  else {
    /* Find the slot in the sorted list where otherData fits. 
       The slot will be between n0,n1. */
    for ( n0 = 0, n1 = *pmD-1 ; n1-n0 > 1 ;  ) {
      n = (n1+n0)/2 ;
      if ( otherDist < nearestDist[n] )
        n1 = n ;
      else 
        n0 = n ;
    }
  }


  if ( n1 ) {
    /* There may be duplicate data items with the samve value ahead
       in the list. Filter them out. */
    for ( n = n1-1 ; n >=0 && otherDist == nearestDist[n] ; n-- ) {
      if ( pOtherData ==  pNearestData[n] )
        /* Duplicate. Drop it. */  
        return ;
    }
  }



  if ( n1 == *pmD && *pmD == mData )
    /* Full list, and otherData further than the mData closest, drop it. */
    return ;
  else if ( *pmD == mData ) 
    /* The list is full, drop the last item off. */
    (*pmD)-- ;

  /* Copy the bottom list down. */
  for ( n = *pmD ; n > n1 ; n-- ) {
    pNearestData[n] = pNearestData[n-1] ;
    nearestDist[n] = nearestDist[n-1] ;
  }

  pNearestData[n1] = pOtherData ;
  nearestDist[n1] = otherDist ;
  (*pmD)++ ;

  return ;
}




/****************************************************************************
private
****************************************************************************/



/******************************************************************************

  kdtree_err:
  Print an error message and terminate, if appropriate.
  
  Last update:
  ------------
  12Aug10: derived from hairdo's error handler hdo_err.
  
  Input:
  ------
  
  
  
*/

void kdtree_err  ( hip_stat_e errStat, const int verbLvl, const char* msg ) {

  FILE *fl ;
  const char eLabel[][20] = { "          ", "FATAL", "WARNING", "INFO" } ;

  if ( verbosity >= verbLvl )
    printf ( " %s: %s\n", eLabel[errStat], msg ) ;


  if ( errStat == fatal || ( verbLvl == 0 && errStat == warning ) ) {
    /* Either fatal error or warning under machine-run script, terminate. */
    fl = fopen ( "kdtree_error.log", "w" ) ;
    fprintf ( fl, " %s: %s\n", eLabel[errStat], msg ) ;
    fclose ( fl ) ;
    exit ( EXIT_FAILURE ) ; 
  }

  return ;
}

/******************************************************************************

  ini_tree:
  Initialize a tree.
  
  Last update:
  ------------
  7Apr00; make a copy of *data2valu, so that data2valu can store a modified
          set of values.
  : conceived.
  
  Input:
  ------
  mDim:      number of dimensions.
  minDist:   default radius to search for, e.g. epsOverlap. 
  Pll/Pur:   lower left/ upper right inclusive bounding box. (All data items must
             be > resp. < than the bounds. ( not<= ).
  data2valu: a function extracting the VALU_TYPE to compare from void.

  Returns:
  --------
  the pointer to the tree root, Proot.
  
*/

kdroot_struct *kd_ini_tree ( void *do2kd_pFam, char name[], 
                             const int mDim, const double minDist,
                             const double *pll, const double *pur,
                             double *(*do2kd_data2valu) ( const void *) ) {
  /* Initialize the tree, give it a bounding box. */
  if ( mDim > MAX_DIM ) kdtree_err ( fatal, 0, "make MAX_DIM larger in do2kdtree.h" ) ;


  kdroot_struct *pRoot = malloc ( sizeof( kdroot_struct ) ) ;
  if ( !pRoot ) kdtree_err ( fatal, 0, "could not allocate the tree in ini_tree.\n" ) ;

  pRoot->pKdt = kd_create( mDim );
  if ( !pRoot->pKdt ) kdtree_err ( fatal, 0, "failed to allocate kdtree in ini_tree" ) ;


  pRoot->mDim = mDim ;
  pRoot->minDist = minDist ;
  pRoot->data2valu = do2kd_data2valu ;
  pRoot->pFam = do2kd_pFam ;
  
  int k ;
  for ( k = 0 ; k < mDim ; k++ ) {
    pRoot->llRoot[k] = pll[k] ;
    pRoot->urRoot[k] = pur[k] ;
  }

  return ( pRoot ) ;
}


void kd_del_tree ( kdroot_struct **ppRoot ) {
  kd_free( (*ppRoot)->pKdt ) ;
  free ( *ppRoot ) ;
  *ppRoot = NULL ;

  return ;
}


/********************************************************************************

  add_data:
  Add an entry pointed to by Pdata into the tree PRoot. Note that data2valu(Pdata)
  must be distinct. Otherwise adding more than mDim of the same will result in an
  endless loop.

  Last update:
  ------------
  4May98; remove onceness.
  3Jun96; new tree.

  Input:
  ------
  Proot:
  Pdata:

  Changes to:
  -----------
  Proot->...

  Returns:
  --------
  NULL on failure, Pdata on success, the pointer to the duplicate in the tree
  if Pdata has not been added due to duplication.

*/

void kd_add_data ( kdroot_struct *pRoot, const void *pData )  {

  const double *pos = (pRoot->data2valu)(pData) ;
  int iPos ;

  iPos = kd_insert( pRoot->pKdt, pos, (void*) pData);

  if ( iPos ) kdtree_err ( fatal, 0, "failed to insert data in kdtree's add_data" ) ;

  return ;
}


/************************************************************************************

  nearest_data:
  Find the nearest data to a given data in the tree.

  Last update:
  ------------
  6Feb12; deal with empty list.
  4Jun96; return nearestDist.

  Input:
  ------
  Proot:
  Pdata:

  Changes to:
  -----------
  nearestDist: Nearest distance, root taken.

  Returns:
  --------
  a pointer to the nearest data.

*/

void *kd_nearest_data ( const kdroot_struct *pRoot, const void *Pdata,
			  double *pNearestDist ) {

  const double *pPos = (pRoot->data2valu)(Pdata) ;
  struct kdres* pKdr = kd_nearest( pRoot->pKdt, pPos ) ;
  double coor[MAX_DIM] ;

  if ( !pKdr ) {
    *pNearestDist = 0. ;
    return ( NULL ) ;
  }

  /* Start the list with the nearest item. */
  void * pNearestData = kd_res_item ( pKdr, coor ) ;
  kd_res_free( pKdr );

  *pNearestDist = sqrt( sq_distance_dbl ( pPos, coor, pRoot->mDim ) ) ;

  return ( pNearestData ) ;
}


/************************************************************************************

  nearest_datas:
  List the mData nearest data entries to a given data in the tree. 

  Last update:
  ------------
  14Dec19; rename to m_nearest to clarify use.
  17Jun11; derived from nearest_datas in lib_do/tree

  Input:
  ------
  pRoot:
  pData:
  mData: number of data entries to list 

  Changes to:
  -----------
  pNearestData: List of nearest data entries.
  pNearestDist: List of nearest distances squared.

  Returns:
  --------
  the number of nearest data entries.

*/

int kd_m_nearest_datas ( const kdroot_struct *pRoot, const void *pData, int mData,
                         const void *pNearestData[], const ulong_t dataSize,
                         double nearestDistSq[], int *pmRgTot ) {
                    
  
  double nrstDst = 1.e25 ;

  /* First check if the root is valid. */
  if ( !pRoot ) kdtree_err ( fatal, 0, "Empty tree in nearest_data.\n" ) ;

  /* Start the list with the nearest item. */
  double dist ;
  kd_nearest_data ( pRoot, pData, &dist ) ;
  dist *= 2 ;
  dist = 8*MAX( dist, pRoot->minDist ) ;


  /* Do a number of range searches with incrementally enlarged boxes,
     estimate a range using the distance to the nearest and epso. */
  int filling = 1 ;
  const double *pPos = (pRoot->data2valu)(pData) ;
  struct kdres* pKdr ;
  void *pOthDat ;
  double othDist, othPos[MAX_DIM] ;
  int mD ;
  while ( filling ) {
  
    /* Search the range. */
    pKdr = kd_nearest_range( pRoot->pKdt, pPos, dist);

    /* Range search this box. */
    mD = 0 ;
    while ( !kd_res_end( pKdr ) ) {
      (*pmRgTot)++ ;
      pOthDat = (void*) kd_res_item( pKdr, othPos );

      othDist = sq_distance_dbl ( pPos, othPos, pRoot->mDim ) ;
      dkd_add2list ( mData, &mD, pNearestData, dataSize, nearestDistSq, 
                    pOthDat, othDist ) ;
      kd_res_next( pKdr ) ;
    }

    if ( mD >= mData )
      filling = 0 ;
    else {
      /* Enlarge the range for the search. */
      dist *=2 ;
    }
    
    kd_res_free( pKdr );
  }


  return ( mD ) ;
}
