/*
   uns_color.c:
   Colorize an unstructured mesh.

   Last update:
   ------------


   This file contains:
   -------------------
   color_vx:

*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern const elemType_struct elemType[] ;

/******************************************************************************

  lowest_color:
  Given a vertex, scan the list of edges attached to that node. Find the
  first free color among all its neighbors.
  
  Last update:
  ------------
  17Nov16; move to top to avoid prototyping.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int lowest_color ( llEdge_s *pllEdge, color_s vxColor[],
			  vrtx_struct *pVx ) {
  
  const int nVx = pVx->number ;
  int freeColor[MAX_COLORS+1], nEg = 0, n1stEdge, side, nCol ;
  vrtx_struct *pVxEg[2] ;
  

  if ( vxColor[nVx].color )
    /* This vertex already has a color assigned to it. */
    return ( vxColor[nVx].color ) ;

  /* Reset the list of available colors to all available. */
  for ( nCol = 1 ; nCol <= MAX_COLORS ; nCol++ )
    freeColor[nCol] = 1 ;
  
  /* Loop over all neighboring nodes. */
  while ( loop_edge_vx ( pllEdge, pVx, &n1stEdge, &nEg, &side ) ) {
    show_edge ( pllEdge, nEg, pVxEg, pVxEg+1 ) ;

    /* Remove the color of this neighbor from the list of free colors. */
    freeColor[ vxColor[ pVxEg[ 1-side ]->number ].color ] = 0 ;
  }

  /* Loop over all the colors to find the first free one. */
  for ( nCol = 1 ; nCol <= MAX_COLORS ; nCol++ )
    if ( freeColor[nCol] ) {
      /* This one's available. */
      vxColor[nVx].color = nCol ;
      return ( nCol ) ;
    }

  /* Failure. Colors exhausted. */
  hip_err ( fatal, 0, "list of colors exhausted in lowest_color.\n" ) ;
  return ( 0 ) ;
}

/******************************************************************************

  color_vx:
  Colorize vertices and count the number of boundary patches for each boundary
  vertex..
  
  Last update:
  ------------
  4Nov99; don't make an edge list if there isn't one.
  
  Input:
  ------
  pUns
  pllEdge:  If there is no edge list, don't color.

  Changes To:
  -----------
  pUns->pVxColor: A list of colors for the vertices in the mesh.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

color_s *color_vx ( uns_s *pUns, llEdge_s *pllEdge ) {
  
  const faceOfElem_struct *pFoE ;
  const int *kVxFace ;
  
  color_s *pVxColor ;
  int mVx = pUns->mVertsAlloc, nBc, nVx, kVx, nVxBeg, nVxEnd ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  chunk_struct *pChunk ;

  
  pVxColor = arr_malloc ( "pVxColor in color_vx", pUns->pFam, mVx+1, sizeof( color_s ) );
  pUns->pVxColor = pVxColor ;

  
  /* Reset the boundary counters for each vertex. */
  for ( nVx = 1 ; nVx <= mVx ; nVx++ )
    pVxColor[nVx].mBnd = 0 ;

  
  /* Loop over all boundaries. Count the number of boundaries a node is
     situated on. */
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {

    for ( nVx = 1 ; nVx <= mVx ; nVx++ )
      pVxColor[nVx].mark = 0 ;

    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
        /* Do we insist that elements be properly numbered? */
        if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
          pElem = pBndFc->Pelem ;
          pFoE = elemType[ pElem->elType ].faceOfElem + pBndFc->nFace ;
          kVxFace = pFoE->kVxFace ;
          for ( kVx = 0 ; kVx < pFoE->mVertsFace ; kVx++ ) {
            pVx = pElem->PPvrtx[ kVxFace[kVx] ] ;
            nVx = pVx->number ;
            pVxColor[nVx].mark = 1 ;
            if ( pllEdge )
              pVxColor[nVx].color = lowest_color ( pllEdge, pVxColor, pVx ) ;
          }
        }
    
    for ( nVx = 1 ; nVx <= mVx ; nVx++ )
      if ( pVxColor[nVx].mark )
	++pVxColor[nVx].mBnd ;
  }


  /* JDM: how can pllEdge not exist? This seems not possible. */
  if ( !pllEdge )
    return ( pVxColor ) ;

  
  /* Loop over all nodes and color the remaining interior nodes. */
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nVxBeg, &pVxEnd, &nVxEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      pVxColor[ pVx->number ].color = lowest_color ( pllEdge, pVxColor, pVx ) ;

  return ( pVxColor ) ;
}

