/*
   read_uns_avbp.c:
   Read a set of files in AVBP format.


   Last update:
   ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
   2Feb04; intro new solution elements with 5.3.
   22Dec02; accept 5.0 as a valid 5.1 string for the solution file.
   17Sep2; intro solution format of 5.1, Drop the 3 in the filename.
   16May01; fix bug in initialising mK in read_avbp3_flowsol. intro read_avbp3_meanval.
   9Jan01; read separate record for each nadd.
   30Oct00; proper ini for restart, use all variable fields.
   10Dec97; intro perBc_in_exBound.

   This file contains:
   -------------------
   read_uns_avbp3
   read_avbp3_coor
   read_avbp3_sol
   read_avbp3_conn

*/

#include <strings.h>

#include "cpre.h"
#include "cpre_uns.h"
/* Needed for prototype definitions in proto_adapt.h. The surface triangulation
   is a three-dimensional field. Fix this by cutting private and public apart. */
#include "cpre_adapt.h" 

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern double Gamma, GammaM1 ;
extern double R ;
extern int perBc_in_exBound ;
extern const int dg_fix_lrgAngles ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;
/******************************************************************************

  read_avbp_coor:
  Coordinates. Also reads the number of equations.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp_coor ( FILE *Fcoor, uns_s *pUns, chunk_struct *Pchunk ) {
  
  int someInt[9], nDim, mDim, mEqu, mVerts ;
  double *Pcoor ;
  vrtx_struct *Pvrtx ;
  char dirChar[3] = {'x','y','z'} ;
  
  if ( verbosity > 1 )
    printf ( "   Reading vertex coordinates.\n" ) ;

  /* Dimension, # of equations. */
  if ( FREAD ( someInt, sizeof( int ), 4, Fcoor ) !=4 ||
       someInt[0] != 2*sizeof ( int ) ) {
    printf ( " FATAL: error reading mDim, mEqu in read_avbp_coor.\n" ) ;
    return ( 0 ) ; }
  mDim = pUns->mDim = someInt[1] ;
  mEqu = someInt[2] ;
  if ( mEqu > MAX_UNKNOWNS ) {
    printf ( " SORRY: change MAX_UNKNOWNS in cpre.h from %d to %d.\n",
	     MAX_UNKNOWNS, mEqu ) ;
    return ( 0 ) ; }

  /* Number of blocks, vertices. */
  if ( FREAD ( someInt, sizeof( int ), 4, Fcoor ) !=4 ||
       someInt[0] != 2*sizeof ( int ) ) {
    printf ( " FATAL: error reading mBlocks, mVerts in read_avbp_coor.\n" ) ;
    return ( 0 ) ; }
  if ( someInt[1] != 1 ) {
    printf ( " SORRY: read_avbp cannot do prepartitioned meshes.\n" ) ;
    return ( 0 ) ; }
  mVerts = Pchunk->mVerts = someInt[2] ;

  /* Skip three ints. */
  if ( FREAD ( someInt, sizeof( int ), 5, Fcoor ) != 5 ||
       someInt[0] != 3*sizeof ( int ) ) {
    printf ( " FATAL: error reading block length in read_avbp_coor.\n" ) ;
    return ( 0 ) ; }
  
  /* Malloc for the coordinates and vertices. */
  Pchunk->Pvrtx = arr_malloc ( "Pchunk->Pvrtx in read_avbp_coor", pUns->pFam,
                               mVerts + 1, sizeof( *Pchunk->Pvrtx ) ) ;
  Pchunk->Pcoor = arr_malloc ( "Pchunk->Pcoor in read_avbp_coor", pUns->pFam,
                               ( mVerts + 1 )*mDim, sizeof( *Pchunk->Pcoor ) ) ;

  /* Length of the record of coordinates. */
  if ( FREAD ( someInt, sizeof( int ), 1, Fcoor ) != 1 ||
       someInt[0] != mDim*mVerts*sizeof ( double ) ) {
    printf ( " FATAL: error reading length of coordinates in read_avbp_coor.\n" ) ;
    return ( 0 ) ; }

  /* Read the coordinates, first all x, make a vrtx. */ 
  for ( Pvrtx = Pchunk->Pvrtx+1, Pcoor = Pchunk->Pcoor + mDim ;
        Pvrtx <= Pchunk->Pvrtx + mVerts ; Pvrtx++, Pcoor += mDim )
    if ( FREAD ( Pcoor, sizeof( double ), 1, Fcoor ) != 1 ) {
      printf ( " FATAL: error reading x-coordinates in read_avbp_coor.\n" ) ;
      return ( 0 ) ; }
    else {
      Pvrtx->Pcoor = Pcoor ;
      Pvrtx->number = Pvrtx - Pchunk->Pvrtx ;
      Pvrtx->Punknown = NULL ;
      Pvrtx->vxCpt.nCh = 0 ; Pvrtx->vxCpt.nr = Pvrtx-Pchunk->Pvrtx ;
    }

  /* Read the remaining coordinates. */
  for ( nDim = 1 ; nDim < mDim ; nDim++ )
    for ( Pcoor = Pchunk->Pcoor + mDim + nDim ;
	  Pcoor <= Pchunk->Pcoor + mDim*mVerts + nDim ; Pcoor += mDim )
      if ( FREAD ( Pcoor, sizeof( double ), 1, Fcoor ) != 1 ) {
	printf ( " FATAL: error reading %c-coordinates in read_avbp_coor.\n",
		 dirChar[nDim] ) ;
	return ( 0 ) ; }

  if ( verbosity > 3 )
    printf ( "      Found %d vertices.\n", mVerts ) ;
  
  return ( 1 ) ;
}
/******************************************************************************

  reset_avbp_ini:
  Rest all avbp descriptors for the solution:
  
  Last update:
  ------------
  15Mar06: conceived.
  
  Input:
  ------
  pUns

  Changes To:
  -----------
  pUns->restart.avbp
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void reset_avbp_ini ( uns_s *pUns ) {

  pUns->restart.avbp.neqs   = 0 ;
  pUns->restart.avbp.neqfic = 0 ;
  pUns->restart.avbp.nreac  = 0 ;
  pUns->restart.avbp.neqt   = 0 ;
  pUns->restart.avbp.nadd   = 0 ;
  pUns->restart.avbp.neq2pf = 0 ;

  pUns->restart.avbp.ithick        = 0 ;
  pUns->restart.avbp.iles          = 0 ;
  pUns->restart.avbp.ichem         = 0 ;
  pUns->restart.avbp.iavisc        = 0 ;
  pUns->restart.avbp.ipenalty      = 0 ;
  pUns->restart.avbp.iwfles        = 0 ;
  pUns->restart.avbp.istoreadd     = 0 ;
  pUns->restart.avbp.nadd_tpf      = 0 ;
  pUns->restart.avbp.istoreadd_tpf = 0 ;
  pUns->restart.avbp.iavisc_tpf    = 0 ;
  pUns->restart.avbp.ipenalty_tpf  = 0 ;
  pUns->restart.avbp.nvar_evap     = 0 ;
  pUns->restart.avbp.nvar_sigma    = 0 ;
  pUns->restart.avbp.nvar_qb       = 0 ;

  return ;
}



/******************************************************************************

  read_avbp_sol:
  Read the solution. The number of equations is given in the coor
  file, which has to be read before. Ignore the turbulent quantities.
  
  Last update:
  ------------
  16Dec16; fix bug with varNm index exceeding 2. 
           use hip_err.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  12Sep06; fix bug with alloc for double rather vrtx_s in unknown alloc.
  4Apr06; intro AVSP
  15Mar06; support reading tpf 5.1 and tpf 2.0 file formats.
  3Feb06; alter TPF header string
  1Jul05; fix bug with freestream vars not initialised to zero under 5.3.
  22Sep03; be more flexible on V5. AVBP header string.
  16Apr03; catch unreoverable error of meanval.
  11Apr03; update read_avbp_meanval for 5.0 format.
  16May01; fix bug in initialising mK. intro read_avbp_meanval.
  30Sep00; read all sets of variables.
  7Sep00; read and store all the headers for a restart in pUns->restart.
  1Dec99; new 4.2 solution format.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int read_avbp_flowsol ( FILE *Fsol, uns_s *pUns, chunk_struct *Pchunk ) {
  
  const int mDim = pUns->mDim ;

  avbpFmt_e avbpFmt = DEFAULT_avbpFmt ;
  
  int someInt[9] ;
  ulong_t mVerts ;
  int nEq, minSize, readFail = 0, mEq, mEqF, mEqS, neqfic,
    itno=0, nreac, neqt, nadd, neq2pf, nadd_tpf ;
  ulong_t nMin, nMax ;
  int k, mEqK[SZ_mEqK], mK, len, mEqX=0, i, m ;
  double *Punknown, dtsum=0.0, ma_cp=1.0, gmm1=0.4,
    valMin = TOO_MUCH, valMax = -TOO_MUCH, *pUn ;
  vrtx_struct *Pvrtx ;
  char someChar[LINE_LEN], *varNm[] = { "species", "reac. rate", "turb. var.", "other" } ;
  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;

  /* Rewind. */
  fseek ( Fsol, 0, 0 ) ;
  

  /* all counters for the diff. sol fields to 0. */
  reset_avbp_ini ( pUns ) ;
  mEqS = neqfic = nreac = neqt = nadd = neq2pf = nadd_tpf = 0 ;

  /* Text label. */
  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
  len = MIN( LINE_LEN-1, someInt[0] ) ;
  FREAD ( someChar, 1, len, Fsol ) ;
  fseek ( Fsol, someInt[0] - len + sizeof( int ), SEEK_CUR ) ;
  someChar[len] = '\0' ;

  if ( !strncmp ( someChar, " AVBP", 5 ) ) {

    if ( !strncmp ( someChar+6, "Version V5.", 11 ) ) {
      if ( !strncmp ( someChar+6, "Version V5.1", 12 ) ||
           !strncmp ( someChar+6, "Version V5.2", 12 ) )
        avbpFmt = v5_1 ;
      else
        avbpFmt = v5_3 ;
      pUns->restart.avbp.iniSrc = 1 ;
    }
    else if ( !strncmp ( someChar+6, "TPF Version V5", 14 ) ) {
      /* Two phase flow, v5.1. */
      avbpFmt = t5_1 ;
      pUns->restart.avbp.iniSrc = 4 ;
    }
    else if ( !strncmp ( someChar+6, "TPF Version V2", 14 ) ) {
      /* Two phase flow, v2.0 */
      avbpFmt = t2_0 ;
      pUns->restart.avbp.iniSrc = 5 ;
    }
    else {
      /* Default for older versions. None prior to 4.7 is supported any longer. */
      avbpFmt = v4_7 ;
      pUns->restart.avbp.iniSrc = 1 ;
    }
    
    if ( verbosity > 1 ) {
      sprintf ( hip_msg, "   Reading flow solution for AVBP in %s format.",avbpFmtStr[avbpFmt]) ;
      hip_err ( blank, 1, hip_msg ) ;
    }
  }

  else if ( !strncmp ( someChar, " AVSP", 5 ) ) {
    /* Only one AVSP format at the moment. */
      avbpFmt = s3_0 ;
      pUns->restart.avbp.iniSrc = 6 ;


      if ( verbosity > 1 ) {
        sprintf ( hip_msg, "   Reading flow solution for AVSP in %s format.",avbpFmtStr[avbpFmt]) ;
        hip_err ( blank, 1, hip_msg ) ;
      }
  }

  else {
    sprintf ( hip_msg, "unrecognised header in read_avbp_flowsol: %s", someChar ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }




  /* AVBP files are in conservative variables. */
  pUns->varList.varType = cons ;
  
  /* # of iter, mVerts, dtsum. */
  minSize = 2*sizeof( int ) + sizeof( double ) ;
  if ( !FREAD ( someInt, sizeof( int ),    1, Fsol ) || someInt[0] < minSize ||
       !FREAD ( &itno,   sizeof( int ),    1, Fsol ) ||
       !FREAD ( &mVerts, sizeof( int ),    1, Fsol ) ||
       !FREAD ( &dtsum,  sizeof( double ), 1, Fsol ) ||
       fseek ( Fsol, someInt[0] - minSize + sizeof( int ), SEEK_CUR ) )
    readFail = 1 ;
  if ( readFail ) {
    sprintf ( hip_msg, "error reading mIter, mVerts, dtSum in read_avbp_sol." ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  else if ( mVerts != Pchunk->mVerts ) {
    sprintf ( hip_msg, "wrong number of vertices (%"FMT_ULG" vs. %"FMT_ULG") in read_avbp_sol.",
	      mVerts, Pchunk->mVerts ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  

  /* Store the solution parameters for a restart. */
  pUns->restart.avbp.itno  = itno ;
  pUns->restart.avbp.dtsum = dtsum ;
  pUns->restart.avbp.dt_av = 0. ;


  if ( avbpFmt == v5_1 ) {
    /* Versions 5.1 and 5.2. */
    /* # neq, neqs,nreac,neqt,nadd, neq2pf. */
    if ( !FREAD ( someInt, sizeof( int ), 1, Fsol ) || someInt[0] < sizeof( int ) ||
         !FREAD ( &mEqF,   sizeof( int ), 1, Fsol ) ) {
      hip_err ( fatal, 0, "error reading neq for V5.1 in read_avbp_sol.\n" ) ; }

    minSize = 5*sizeof( int ) ;
    if ( someInt[0] > 1*sizeof( int ) ) FREAD ( &mEqS,   sizeof( int ), 1, Fsol ) ;
    if ( someInt[0] > 2*sizeof( int ) ) FREAD ( &nreac,  sizeof( int ), 1, Fsol ) ;
    if ( someInt[0] > 3*sizeof( int ) ) FREAD ( &neqt,   sizeof( int ), 1, Fsol ) ;
    if ( someInt[0] > 4*sizeof( int ) ) FREAD ( &nadd,   sizeof( int ), 1, Fsol ) ;
    if ( someInt[0] > 5*sizeof( int ) ) FREAD ( &neq2pf, sizeof( int ), 1, Fsol ) ;
    fseek ( Fsol, MAX( 0, someInt[0] - minSize ) + sizeof( int ), SEEK_CUR ) ;
  }
  else if ( avbpFmt == t5_1 ) {

    minSize = 3*sizeof( int ) ;
    if ( !FREAD ( someInt, sizeof( int ), 1, Fsol ) || someInt[0] < minSize ) {
      hip_err ( fatal, 0, "error reading neq for TPF V5.1 in read_avbp_sol.\n" ) ;}

    FREAD ( &mEqF,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &mEqS,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &neq2pf, sizeof( int ), 1, Fsol ) ;

    fseek ( Fsol, someInt[0] - 3*sizeof( int ) + sizeof( int ), SEEK_CUR ) ;

  }
  else if ( avbpFmt == t2_0 ) {
    /* Two phase flow. */

    minSize = 4*sizeof( int ) ;
    if ( !FREAD ( someInt, sizeof( int ), 1, Fsol ) || someInt[0] < minSize ) {
      hip_err ( fatal, 0, "error reading neq for TPF V2.0 in read_avbp_sol.\n" ) ;}

    FREAD ( &mEqF,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &mEqS,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &neqfic, sizeof( int ), 1, Fsol ) ;
    FREAD ( &neq2pf, sizeof( int ), 1, Fsol ) ;

    fseek ( Fsol, someInt[0] - minSize + sizeof( int ), SEEK_CUR ) ;
  }
  else if ( avbpFmt == v5_3 || avbpFmt == s3_0 ) {
    /* Version 5.3 and higher or AVSP 3.0 and higher. */
    /* # neq, neqs, neqfic, nreac,neqt,nadd. */
    minSize = 6*sizeof( int ) ;
    if ( !FREAD ( someInt, sizeof( int ), 1, Fsol ) || someInt[0] < minSize ) {
      hip_err ( fatal, 0, "error reading neq for V5.3 in read_avbp_sol.\n" ) ; }

    FREAD ( &mEqF,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &mEqS,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &neqfic, sizeof( int ), 1, Fsol ) ;
    FREAD ( &nreac,  sizeof( int ), 1, Fsol ) ;
    FREAD ( &neqt,   sizeof( int ), 1, Fsol ) ;
    FREAD ( &nadd,   sizeof( int ), 1, Fsol ) ;
    fseek ( Fsol, someInt[0] - minSize + sizeof( int ), SEEK_CUR ) ;
  }

  if ( feof( Fsol ) )
    hip_err ( fatal, 0, "error reading neqs,nreac,neqt,nadd,neq2pf in read_avbp_sol." ) ;
  
  else if ( mEqF != mDim+2 ) {
    sprintf ( hip_msg, "expecting %d flow variables for a %d-dim case, found %d.",
              mDim+2, mDim, mEqF ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  /* Store the solution parameters for a restart. */
  pUns->restart.avbp.neqs   = mEqS ;
  pUns->restart.avbp.neqfic = neqfic ;
  pUns->restart.avbp.nreac  = nreac ;
  pUns->restart.avbp.neqt   = neqt  ;
  pUns->restart.avbp.nadd   = nadd  ;
  pUns->restart.avbp.neq2pf = neq2pf  ;

  nadd_tpf = pUns->restart.avbp.nadd_tpf ;
  pUns->varList.mUnknowns = mEq = mEqF+mEqS+neqfic+2*nreac+neqt+nadd+neq2pf+nadd_tpf ;
  pUns->varList.mUnknFlow = mEqF ;

  
  /* Set the individual variable categories. */
  if ( avbpFmt == v5_1 ) {
    /* Versions 5.1 and 5.2. */
    for ( m = i = 0 ; i <  mEqF ; i++ )
      pVar[m++].cat = ns ;
    for ( i = 0 ; i < neq2pf ; i++ )
      pVar[m++].cat = species ;
    for ( i = 0 ; i < mEqS ; i++ )
      pVar[m++].cat = species ;
    /* Reaction rates exist forward and backward. */
    for ( i = 0 ; i < nreac ; i++ )
      pVar[m++].cat = rrates ;
    for ( i = 0 ; i < nreac ; i++ )
      pVar[m++].cat = rrates ;
    for ( i = 0 ; i < neqt ; i++ )
      pVar[m++].cat = rans ;
    for ( i = 0 ; i < nadd ; i++ )
      pVar[m++].cat = add ;
  }
  else if ( avbpFmt == t5_1 ) {
    /* Two phase flow versions 5.x. */
    for ( m = i = 0 ; i <  mEqF ; i++ )
      pVar[m++].cat = ns ;
    for ( i = 0 ; i < mEqS ; i++ )
      pVar[m++].cat = species ;
    for ( i = 0 ; i < neq2pf ; i++ )
      pVar[m++].cat = tpf ;
  }
  else if ( avbpFmt == t2_0 ) {
    /* Two phase flow versions 2.0 */
    for ( m = i = 0 ; i <  mEqF ; i++ )
      pVar[m++].cat = ns ;
    for ( i = 0 ; i < mEqS ; i++ )
      pVar[m++].cat = species ;
    for ( i = 0 ; i < neqfic ; i++ )
      pVar[m++].cat = fictive ;
    for ( i = 0 ; i < neq2pf ; i++ )
      pVar[m++].cat = tpf ;
    for ( i = 0 ; i < neqt ; i++ )
      pVar[m++].cat = rans ;
    for ( i = 0 ; i < nadd ; i++ )
      pVar[m++].cat = add ;
    for ( i = 0 ; i < nadd_tpf ; i++ )
      pVar[m++].cat = add_tpf ;
  }
  else if ( avbpFmt == v5_3 || avbpFmt == s3_0 ) {
    /* Versions 5.3 to ... */
    for ( m = i = 0 ; i <  mEqF ; i++ )
      pVar[m++].cat = ns ;
    for ( i = 0 ; i < mEqS ; i++ )
      pVar[m++].cat = species ;
    for ( i = 0 ; i < neqt ; i++ )
      pVar[m++].cat = rans ;
    for ( i = 0 ; i < neqfic ; i++ )
      pVar[m++].cat = fictive ;
    /* Reaction rates exist forward and backward. */
    for ( i = 0 ; i < nreac ; i++ )
      pVar[m++].cat = rrates ;
    for ( i = 0 ; i < nreac ; i++ )
      pVar[m++].cat = rrates ;
    for ( i = 0 ; i < nadd ; i++ )
      pVar[m++].cat = add ;
  }



  if ( avbpFmt < v5_1 ) {
    /* Mach/cpbar, Gamma-1, w_inf. freestream variables. */
    minSize = ( 2+mEqF )*sizeof( double ) ;
    if ( !FREAD ( someInt, sizeof( int ),    1, Fsol ) || someInt[0] < minSize ||
         !FREAD ( &ma_cp,  sizeof( double ), 1, Fsol ) ||
         !FREAD ( &gmm1,   sizeof( double ), 1, Fsol ) ||
         !(FREAD ( pUns->varList.freeStreamVar, sizeof( double ), mEqF, Fsol ) == mEqF ) ||
         fseek ( Fsol, someInt[0] - minSize + sizeof( int ), SEEK_CUR ) )
      readFail = 1 ;
    if ( readFail )
      hip_err ( warning, 1, "could not read freestream variables in  read_avbp_sol.\n"
                "          use 'variables freestream'." ) ;
    /* Check gamma. */
    if ( ABS( gmm1 - GammaM1 ) > 1.e-3 ) {
      if ( gmm1 < 0. || gmm1 > 3. ) {
        sprintf ( hip_msg, "invalid value for gamma: %g, ignored. Use var gamma.",
                  gmm1+1. ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else {
        GammaM1 = gmm1 ;
        Gamma = GammaM1 + 1. ;
        if ( verbosity > 2 ) {
          sprintf ( hip_msg, "new value for gamma: %g.", Gamma ) ;
          hip_err ( info, 1, hip_msg ) ;
        }
      }
    }

    /* Store the solution parameters for a restart. */
    pUns->restart.avbp.ma_cp = ma_cp ;
    pUns->restart.avbp.gmm1  = gmm1 ;
    /* Set new R. */
    if ( ma_cp > 5. ) {
      /* Must be cp. */
      R = ma_cp*GammaM1/Gamma ;
      if ( verbosity > 2 ) {
        sprintf ( hip_msg, "new value for R: %g", R ) ;
        hip_err ( info, 1, hip_msg ) ;
      }
    }
  }
  else {
    /* Skip the line. */
    FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
    fseek ( Fsol, someInt[0] + sizeof( int ), SEEK_CUR ) ;
    pUns->restart.avbp.ma_cp = 0. ;
    pUns->restart.avbp.gmm1  = 0. ;
    for ( i=0 ; i<mEqF ; i++ ) 
      pUns->varList.freeStreamVar[i] = 0. ;
  }
  
  /* Malloc for the unknowns. */
  Pchunk->Punknown = arr_malloc ( "Pchunk->Punknown in read_avbp_sol", pUns->pFam,
                                  ( mVerts + 1 )*mEq, sizeof( *Pchunk->Punknown ) ) ;


  
  /* Length of the record of flow variables. */
  if ( FREAD ( someInt, sizeof( int ), 1, Fsol ) != 1 ||
       someInt[0] != mEqF*mVerts*sizeof ( double ) ) {
    hip_err ( fatal, 0, "error reading length of flow variables in read_avbp_sol.\n" ) ;
  }

  /* Set the vrtx pointer. */ 
  for ( Pvrtx = Pchunk->Pvrtx+1, Punknown = Pchunk->Punknown + mEq ;
        Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++, Punknown += mEq )
    Pvrtx->Punknown = Punknown ;

  /* Read the unknowns, First all rho, set the vrtx pointer. */ 
  for ( nEq = 0 ; nEq < mEqF ; nEq++ ) {
    valMin = TOO_MUCH, valMax = -TOO_MUCH ;
    
    for ( Pvrtx = Pchunk->Pvrtx+1 ; Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ ) {
      pUn = Pvrtx->Punknown+nEq ;
      if ( FREAD ( pUn, sizeof( double ), 1, Fsol ) != 1 )
        hip_err ( fatal, 0, "error reading flow variables in read_avbp_sol." ) ;

      traceMinMax ( pUn, Pvrtx - Pchunk->Pvrtx, &valMin, &nMin, &valMax, &nMax ) ; 
    }

    if ( verbosity > 3 ) {
      sprintf ( hip_msg, "Found flow var %d, min %g at %"FMT_ULG", max %g at %"FMT_ULG".",
               nEq+1, valMin, nMin, valMax, nMax ) ;
      hip_err ( info, 3, hip_msg ) ;
    }
  }

  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;


  
  /* Remaining stuff. */
  if ( avbpFmt == v5_3 || avbpFmt == s3_0 ) {
    /* 5.3 */
    mEqX = 0 ; 
    mEqK[mEqX++] = mEqS ;
    mEqK[mEqX++] = neqt ;
    mEqK[mEqX++] = neqfic ;
    mEqK[mEqX++] = nreac ;
    mEqK[mEqX++] = nreac ;

    if ( mEqX + nadd >= SZ_mEqK ) {
      sprintf ( hip_msg, "too many variable fields (each add counts for one).\n"
                "        recompile with SZ_mEqK > %d in cpre_uns.h.", mEqX + nadd ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    for ( i = 0 ; i < nadd ; i++ )
       /* Additional vars written in a record each. */
      mEqK[mEqX++] = 1 ;
  }
  else if ( avbpFmt == t5_1 ) {
    /* t5.1 */
    mEqX = 2 ;
    mEqK[0] = mEqS ;
    mEqK[1] = neq2pf ;
  }
  else if ( avbpFmt == t2_0 ) {
    /* t5.1 */
    mEqX = 0 ;
    mEqK[mEqX++] = mEqS ;
    mEqK[mEqX++] = neqfic ;
    mEqK[mEqX++] = neq2pf ;
    mEqK[mEqX++] = neqt ;
    for ( i = 0 ; i < nadd ; i++ )
       /* Additional vars written in a record each. */
      mEqK[mEqX++] = 1 ;
    for ( i = 0 ; i < nadd_tpf ; i++ )
       /* Additional vars written in a record each. */
      mEqK[mEqX++] = 1 ;
  }
  else if ( avbpFmt == v5_1 ) {
    /* 5.1 */
    mEqX = 0 ;
    mEqK[mEqX++] = neq2pf ;
    mEqK[mEqX++] = neqt ;
    mEqK[mEqX++] = mEqS ;
    mEqK[mEqX++] = nreac ;
    mEqK[mEqX++] = nreac ;
    for ( i = 0 ; i < nadd ; i++ )
       /* Additional vars written in a record each. */
      mEqK[mEqX++] = 1 ;
  }
  else if ( avbpFmt == v4_7 ) {
    /* 4.7. */
    mEqX = 3 ;
    mEqK[0] = mEqS ;
    mEqK[1] = nreac ;
    mEqK[2] = neqt ;
  }
  
    
  /* Loop over all extra sets of variables. */
  for ( k = 0, mK = mEqF ; k < mEqX ; k++ )
    if ( mEqK[k] ) {
      if ( FREAD ( someInt, sizeof( int ), 1, Fsol ) != 1 ||
           someInt[0] != mEqK[k]*mVerts*sizeof ( double ) ) {
        sprintf ( hip_msg, "error reading length of scalar unknowns for set %d"
                 " in read_avbp_sol.", k+1 ) ;
        hip_err ( fatal, 0, hip_msg ) ; }
        
      for ( nEq = mK ; nEq < mK+mEqK[k] ; nEq++ ) {
        valMin = TOO_MUCH, valMax = -TOO_MUCH ;
          
        for ( Pvrtx = Pchunk->Pvrtx+1 ;
              Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ ) {
          pUn =  Pvrtx->Punknown+nEq ;
          if ( FREAD ( pUn, sizeof( double ), 1, Fsol ) != 1 ) {
            sprintf ( hip_msg, "error reading flow variables in read_avbp_sol." ) ;
            hip_err ( fatal, 0, hip_msg ) ; }
          traceMinMax ( pUn, Pvrtx - Pchunk->Pvrtx, &valMin, &nMin, &valMax, &nMax ) ; 
        }

        if ( verbosity > 3 ) {
          sprintf ( hip_msg, "      Found %s var %d, min %g at %"FMT_ULG", max %g at %"FMT_ULG".",
                    varNm[ MIN(3,k) ], nEq+1, valMin, nMin, valMax, nMax ) ;
          hip_err ( blank, 3, hip_msg ) ;
        }
      }
        
      FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
      mK += mEqK[k] ;
    }

  /* All nAdd have one record each.
  for ( k = 0 ; k < nadd ; k++ ) {
    valMin = TOO_MUCH, valMax = -TOO_MUCH ;

    if ( FREAD ( someInt, sizeof( int ), 1, Fsol ) != 1 ||
         someInt[0] != mVerts*sizeof ( double ) ) {
      printf ( " FATAL: error reading length of added unknowns nr. %d"
               " in read_avbp_sol.\n", k+1 ) ;
      return ( 0 ) ; }
      
    for ( Pvrtx = Pchunk->Pvrtx+1 ;
          Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ ) {
      pUn =  Pvrtx->Punknown+mK ;
      if ( FREAD ( pUn, sizeof( double ), 1, Fsol ) != 1 ) {
        printf ( " FATAL: error reading flow variables in read_avbp_sol.\n" ) ;
        return ( 0 ) ; }
      traceMinMax ( pUn, Pvrtx - Pchunk->Pvrtx, &valMin, &nMin, &valMax, &nMax ) ;
    }
        
    if ( verbosity > 3 )
      printf ( "      Found added var %d, min %g at %d, max %g at %d.\n",
               k+1, valMin, nMin, valMax, nMax ) ;
        
    FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
    mK++ ;
  } */
  


  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 


  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "      Found %d unknowns for %"FMT_ULG" vertices.\n"
             "      Found %d ns, %d tpf, %d species, %d rrates,"
             " %d turb, %d add unknowns.\n",
             mEq, mVerts, mEqF, neq2pf, mEqS, nreac, neqt, nadd ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }
    
  return ( 1 ) ;
}
/******************************************************************************

  read_avbp_meanval:
  read avbp soultion with averaged values..
  
  Last update:
  ------------
  4Apr13; make all large counters ulong_t.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/


static int read_avbp_meanval ( FILE *Fsol, uns_s *pUns, chunk_struct *Pchunk ) {

  /* Find out whether this is a proper fluid flow field or selected
     averaged quantities in an averaged mean solution file. */

  int someInt[9], nEq, mEq, isMean = 1, c, len ;
  ulong_t mVx=0, nMin, nMax ;
  int  i, minSize, itno=0, readFail = 0 ;
  char someChar[LINE_LEN], header[LINE_LEN] ;
  vrtx_struct *Pvrtx ;
  double *pUn, valMin, valMax, dtsum=0.0 ;
  varList_s *pVL = &(pUns->varList) ;
  var_s *pVar = pVL->var ;
  
  /* Text label. */
  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
  len = MIN( LINE_LEN-1, someInt[0] ) ;
  FREAD ( someChar, 1, len, Fsol ) ;
  fseek ( Fsol, someInt[0] - len + sizeof( int ), SEEK_CUR ) ;
  someChar[len] = '\0' ;
  strncpy ( header, someChar, len+1 ) ;

  
  if ( strncmp( header, " AVBP", 5 ) ) {
    /* printf ( " FATAL: could not recognise solution file header: %s\n", header ) ;*/
    return ( 0 ) ; }

  /* # of iter, mVerts, dtsum. */
  minSize = 2*sizeof( int ) + sizeof( double ) ;
  if ( !FREAD ( someInt, sizeof( int ),    1, Fsol ) || someInt[0] < minSize ||
       !FREAD ( &itno,   sizeof( int ),    1, Fsol ) ||
       !FREAD ( &mVx, sizeof( int ),    1, Fsol ) ||
       !FREAD ( &dtsum,  sizeof( double ), 1, Fsol ) ||
       fseek ( Fsol, someInt[0] - minSize + sizeof( int ), SEEK_CUR ) )
    readFail = 1 ;
  if ( readFail ) {
    printf ( hip_msg, "error reading mIter, mVerts, dtSum in read_avbp_sol.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  else if ( mVx != Pchunk->mVerts ) {
    printf ( hip_msg, "wrong number of vertices (%d vs. %"FMT_ULG") in read_avbp_sol.\n",
	     mVx, Pchunk->mVerts ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }

  /* Store the solution parameters for a restart. */
  pUns->restart.avbp.iniSrc = 3 ;
  pUns->restart.avbp.itno  = itno ;
  pUns->restart.avbp.dtsum = dtsum ;


  if ( mVx != Pchunk->mVerts ) {
    printf ( hip_msg, "wrong number of vertices (%d vs. %"FMT_ULG") in read_avbp_meanval.\n",
	     mVx, Pchunk->mVerts ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  

  if ( strncmp( header, " AVBP Version V5", 16 ) ) {
    /* Line not present anymore in 5.0. */
    /* cpbar, gmm1, u_inf in the case of a mean val file, nVar ... for flowfile. */
    FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
    fseek ( Fsol, someInt[0] + sizeof( int ), SEEK_CUR ) ;
  }

  
  /* mEq? */
  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
  isMean = FREAD ( &mEq, sizeof( int ), 1, Fsol ) ;
  fseek ( Fsol, someInt[0], SEEK_CUR ) ;

  /* Averaged solutions should only have a single int on this record. */
  if ( someInt[0] != sizeof (int) ) isMean = 0 ;

  if ( !isMean )
    return ( 0 ) ;
  else if ( mEq < 0 )
    return ( 0 ) ;
  else if ( mEq > MAX_UNKNOWNS ) {
    printf ( " FATAL: requested %d unknowns, only %d compiled.", mEq, MAX_UNKNOWNS ) ;
    return ( -1 ) ;
  }


  
  /* tAvgVar? */
  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
  FREAD ( &dtsum,  sizeof( double ), 1, Fsol ) ;
  fseek ( Fsol, someInt[0] - sizeof(double) + sizeof( int ), SEEK_CUR ) ;
  pUns->restart.avbp.dt_av = dtsum ;


  /* there should be a character string for each variable. */
  for ( nEq = 0 ; nEq < mEq ; nEq++ ) {
    FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
    len = MIN( LINE_LEN, someInt[0] ) ;
    if ( FREAD ( someChar, 1, len, Fsol ) != len )
      return ( 0 ) ;
    fseek ( Fsol, someInt[0] - len + sizeof( int ), SEEK_CUR ) ;
    /* is it a character string? */
    for ( i = 0 ; i < len ; i++ ) {
      c = someChar[i] ;
      if ( !isprint( c ) )
        return ( -1 ) ;
    }
    r1_endstring ( someChar, len ) ;
    strncpy(  pVar[nEq].name, someChar, LEN_VARNAME ) ;
  }
  
  /* Length of the avg. Var. */
  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;
  if ( someInt[0] != mVx*mEq*sizeof( double ) )
    return ( 0 ) ;

  /* OK, seems to be a mean val file. */
  if ( verbosity > 1 )
    printf ( "   Reading averaged values for %s.\n", header ) ;

  pUns->varList.varType   = noType ;
  for ( nEq = 0 ; nEq < mEq ; nEq++ )
    pVar[nEq].cat  = mean ;
  pVL->mUnknowns = mEq ;
  pUns->varList.mUnknFlow = 0 ;
    
  /* Malloc for the unknowns. */
  Pchunk->Punknown = arr_malloc ( "Pchunk->Punknown in read_avbp_sol", pUns->pFam,
                                  ( mVx+1 )*mEq, sizeof( *Pchunk->Punknown ) ) ;

  /* Set the pointers to the unknowns. */
  for ( Pvrtx = Pchunk->Pvrtx+1, pUn = Pchunk->Punknown + mEq ;
        Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++, pUn += mEq )
    Pvrtx->Punknown = pUn ;


  /* Read the unknowns. */
  for ( nEq = 0 ; nEq < mEq ; nEq++ ) {
    valMin = TOO_MUCH, valMax = -TOO_MUCH ;
    
    for ( Pvrtx = Pchunk->Pvrtx+1 ; Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++ ) {
      pUn = Pvrtx->Punknown+nEq ;
      if ( FREAD ( pUn, sizeof( double ), 1, Fsol ) != 1 ) {
        printf ( " FATAL: error reading flow variables in read_avbp_sol.\n" ) ;
        return ( -1 ) ; }
      traceMinMax ( pUn, Pvrtx - Pchunk->Pvrtx, &valMin, &nMin, &valMax, &nMax ) ; 
    }

    if ( verbosity > 3 )
      printf ( "      Found var %d: %-15s, min %g at %"FMT_ULG", max %g at %"FMT_ULG".\n",
               nEq+1, pVar[nEq].name, valMin, nMin, valMax, nMax ) ;
  }

  FREAD ( someInt, sizeof( int ), 1, Fsol ) ;



  /* Set default variable names. */
  check_var_name ( &(pUns->varList), &pUns->restart, pUns->mDim ) ; 


  return ( 1 ) ;
}

int read_avbp_sol ( FILE *Fsol, uns_s *pUns, chunk_struct *Pchunk ) {
    
  /* Is it an averaged flow file. */
  int retVal = read_avbp_meanval ( Fsol, pUns, Pchunk ) ;

  if ( retVal > 0 )
    return ( 1 ) ;
  else if ( retVal < 0 )
    /* unrecoverable error. */
    return ( 0 ) ;
  else
    /* Try reading unsteady flow solution. */
    return ( read_avbp_flowsol ( Fsol, pUns, Pchunk ) ) ;
}
  
/******************************************************************************

  read_avbp_conn:
  Read the connectivity.
  
  Last update:
  ------------
  12Dec13; fix bug with wrong loop scope when converting node integers
    to pointers, introduced with switch from printf to sprintf.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  9Jul11; call init_elem ;
  15Nov09; use LGINT, intro someUInt for reading large meshes.
  9Apr06; use EXIT_FAILURE on exit
  17May04; check that the elements only reference existing nodes.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp_conn ( FILE *Fconn,
		      uns_s *pUns, chunk_struct *Pchunk, vrtx_struct *PbaseVx ) {
  
  const int mDim = pUns->mDim ;
  /* For very large meshes, use long ints. */
  ulong_t mNewE2V=0 ;
  unsigned int someUInt[MAX_VX_ELEM] ;
  int mElems, mElemTypes, mVertsElem, nElT,
      mElemsThisType, kVx ;
  elem_struct *Pelem, *PlstElemPrvType ;
  vrtx_struct **PPlstE2V ;
  elType_e elType ;

  if ( verbosity > 1 )
    printf ( "   Reading mesh connectivity.\n" ) ;

  /* mBlocks, mTElems, mDummies. */
  if ( FREAD ( someUInt, sizeof( int ), 5, Fconn ) !=5 ||
       someUInt[0] != 3*sizeof ( int ) ) {
    printf ( " FATAL: error reading mBlocks, mTElems, mDummies in read_avbp_conn.\n" ) ;
    return ( 0 ) ; }
  else if ( someUInt[1] != 1 ) {
    printf ( " SORRY: read_avbp_conn can only deal with 1-block avbp files.\n" ) ;
    return ( 0 ) ; }
  Pchunk->mElems = mElems = someUInt[2] ;
  Pchunk->mElem2VertP = 0 ;
  Pchunk->PPvrtx = NULL ;

  /* Malloc for the elements. */
  Pchunk->Pelem = arr_malloc ( " Pchunk->Pelem in read_avbp_coor", pUns->pFam,
                               mElems+1, sizeof( *Pchunk->Pelem ) ) ;

  /* mElems, mElemTypes. */
  if ( FREAD ( someUInt, sizeof( int ), 4, Fconn ) !=4 ||
       someUInt[0] != 2*sizeof ( int ) ) {
    printf ( " FATAL: error reading mElems, mElemTypes in read_avbp_conn.\n" ) ;
    return ( 0 ) ; }
  mElemTypes = someUInt[2] ;
  
  /* Loop over all element types. */
  for ( PlstElemPrvType = Pchunk->Pelem, nElT = 0 ; nElT < mElemTypes ; nElT++ ) {
    /* elT, begin, mElems. */
    if ( FREAD ( someUInt, sizeof( int ), 5, Fconn ) !=5 ||
	 someUInt[0] != 3*sizeof ( int ) ) {
      printf ( " FATAL: error reading elType, begin, mElems"
	       " in read_avbp_conn.\n" ) ;
      return ( 0 ) ; }
    mVertsElem = someUInt[1] ;
    mElemsThisType = someUInt[3] ;

    /* Find the corresponding element type. */
    for ( elType = tri ; elType <= hex ; elType++ )
      if ( elemType[elType].mVerts == mVertsElem &&
	   elemType[elType].mDim == mDim )
	break ;
    if ( elType > hex ) {
      printf ( " FATAL: no such element type with %d verts in %d-D in"
	       " read_avbp_conn.\n",
	       mVertsElem, pUns->mDim ) ;
      return ( 0 ) ; }

    /* Realloc for the new element 2 vert pointers. */
    mNewE2V = Pchunk->mElem2VertP + mVertsElem * (ulong_t) mElemsThisType ;
    Pchunk->PPvrtx = arr_realloc ( "Pchunk->PPvrtx in read_avbp_coor", pUns->pFam,
                                   Pchunk->PPvrtx, mNewE2V, sizeof( *Pchunk->PPvrtx ) ) ;
    PPlstE2V = Pchunk->PPvrtx + Pchunk->mElem2VertP - 1 ;
    Pchunk->mElem2VertP = mNewE2V ;

#   ifdef CHECK_BOUNDS
      if ( PlstElemPrvType + mElemsThisType > Pchunk->Pelem + Pchunk->mElems ||
	   PPlstE2V + mElemsThisType*mVertsElem >= Pchunk->PPvrtx + Pchunk->mElem2VertP )
	hip_err ( fatal, 0, "beyond elem bounds in read_avbp_conn.\n" ) ;
#   endif

    /* Check the size of the record. */
    if ( FREAD ( someUInt, sizeof( int ), 1, Fconn ) != 1 ||
	 someUInt[0] != mVertsElem * mElemsThisType * sizeof ( int ) ) {
      printf ( " FATAL: wrong size in connectivity of %d-noded elems"
	       " in read_avbp_conn.\n", mVertsElem ) ;
      return ( 0 ) ; }
    
    /* Read all elements of this type. */
    for ( Pelem = PlstElemPrvType+1 ;
	  Pelem <= PlstElemPrvType + mElemsThisType ; Pelem++ ) {
      init_elem ( Pelem, elType, (int)( Pelem - Pchunk->Pelem ), NULL ) ;
      
      if ( FREAD ( someUInt, sizeof( int ), mVertsElem, Fconn ) != mVertsElem ) {
	printf ( " FATAL: error reading connectivity of %d-noded elems"
		 " in read_avbp_conn.\n", mVertsElem ) ;
	return ( 0 ) ; }

      /* Convert the integers to pointers. */
      for ( kVx = 0 ; kVx < mVertsElem ; kVx++ ) {
        if ( someUInt[kVx] > Pchunk->mVerts ) {
          sprintf ( hip_msg, "connectivity error in read_avbp_conn:\n"
                   "        element %"FMT_ULG" is formed with node %d as %dth node\n"
                   "        but there are only %"FMT_ULG" nodes in the grid.",
                   Pelem->number, someUInt[kVx], kVx+1, Pchunk->mVerts ) ;
          hip_err ( fatal, 0, hip_msg ) ; 
        }
        *(++PPlstE2V) = PbaseVx + someUInt[kVx] ;
      }
    }

    /* Trailing record length. */
    FREAD ( someUInt, sizeof( int ), 1, Fconn ) ;
    PlstElemPrvType += mElemsThisType ;
  }

  /* Loop over all elements and set PPvrtx after the final realloc. */
  PPlstE2V = Pchunk->PPvrtx - 1 ;
  for ( Pelem = Pchunk->Pelem+1 ;
        Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ ) {
    Pelem->PPvrtx = PPlstE2V + 1 ;
    PPlstE2V +=  elemType[ Pelem->elType ].mVerts ;
  }
  
  if ( verbosity > 3 ) {
    sprintf ( hip_msg, "Found %d elements of %d types, %"FMT_ULG" ele2vert"
              " pointers.\n", mElems, mElemTypes, mNewE2V ) ;
    hip_err ( info, 4, hip_msg ) ;
  }
  
  /* Skip the dummy layers. */
  return ( 1 ) ;
}




/******************************************************************************

  read_uns_avbp:
  Read the master file.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  6Sep18; new arg doCheck for merge_uns.
  4Sep18; new interface to merge_uns.
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
  1Apr12; new interface to write_hdf_sol.
  15Sep10; new inteface to merge_uns.
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_uns_avbp ( char masterFile[TEXT_LEN], int hdfSol ) {
  
  FILE *Fmaster = NULL, *Fcoor = NULL, *Fsol = NULL, *Fconn = NULL, *FinBound = NULL,
    *FexBound = NULL, *FasciiBound = NULL, *Fparent = NULL, *Fopened = NULL ;
  grid_struct *Pgrid ;
  chunk_struct *PcoarseChunk = NULL, *pFineChunk ;
  char fileName[TEXT_LEN], solFileName[TEXT_LEN] ;
  int partNumber, found ;
  uns_s *pUns ;

  prepend_path ( masterFile ) ;
  if ( !( Fmaster = fopen ( masterFile, "r" ) ) ) {
    printf ( " FATAL: master file named:%s not found in read_uns_avbp.\n",
	     masterFile ) ;
    return ( 0 ) ; }

  fgets( fileName, TEXT_LEN, Fmaster ) ;

  /* This will be the next grid. */
  partNumber = Grids.mGrids + 1 ;
  printf ( "  Reading unstructured avbp as grid nr. %d.\n", partNumber ) ;

  /* Read the filenames for the finest level from the master file. */
  found = 1 ;
  if ( !fgets( solFileName, TEXT_LEN, Fmaster ) ) {
    printf ( " FATAL: could not read solution file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }


  /* coor */
  if ( !fgets( fileName, TEXT_LEN, Fmaster ) ) {
    printf ( " FATAL: could not read coor file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }
  else if ( !( Fcoor = r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open coor file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }


  /* conn */
  if ( !fgets( fileName, TEXT_LEN, Fmaster ) ) {
    printf ( " FATAL: could not read conn file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }
  else if ( !( Fconn = r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open conn file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }



  /* exBound */
  if ( !fgets( fileName, TEXT_LEN, Fmaster ) ) {
    printf ( " FATAL: could not read exBound file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }
  else if ( !( FexBound = r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open exBound file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }


  /* inBound */
  if ( !fgets( fileName, TEXT_LEN, Fmaster ) ) {
    printf ( " FATAL: could not read inBound file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }
  else if ( !( FinBound = r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open inBound file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }


  /* asciiBound */
  if ( !fgets( fileName, TEXT_LEN, Fmaster ) ) {
    printf ( " FATAL: could not read asciiBound file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }
  else if ( !( FasciiBound = r1_fopen ( prepend_path ( fileName ), TEXT_LEN, "r" ) ) ) {
    printf ( " FATAL: could not open asciiBound file name in read_uns_avbp.\n" ) ;
    found = 0 ;
  }

  if ( !found ) {
    printf ( " FATAL: could not open/find the fine grid files in read_uns_avbp.\n" ) ;
    if ( Fcoor       ) fclose ( Fcoor ) ; 
    if ( Fconn       ) fclose ( Fconn ) ;
    if ( FinBound    ) fclose ( FinBound ) ; 
    if ( FexBound    ) fclose ( FexBound ) ; 
    if ( FasciiBound ) fclose ( FasciiBound ) ;
    return ( 0 ) ; }

  
  /* Allocate a chunk. */
  if ( !( pUns = make_uns ( NULL ) ) || !( pFineChunk = make_chunk ( pUns ) ) ) {
    printf ( " FATAL: failed to alloc a new unstructured chunk in read_uns_avbp.\n" ) ;
    fclose ( Fsol ) ; fclose ( Fcoor ) ; fclose ( Fconn ) ;
    fclose ( FinBound ) ; fclose ( FexBound ) ; fclose ( FasciiBound ) ;
    return ( 0 ) ; }
  pUns->pRootChunk = pFineChunk ;
  pFineChunk->nr = 1 ;



  
  /* Read coordinates, solution and connectivity of the finest level. */
  found = read_avbp_coor ( Fcoor, pUns, pFineChunk ) ;
  fclose ( Fcoor ) ; 

  if ( !found ) {
    printf ( "   FATAL: could not read coor file.\n" ) ;
    return ( 0 ) ;
  }




  /* Is there a solution? */
  r1_stripquote ( solFileName, TEXT_LEN ) ;
  if ( solFileName[0] != '\0' ) {
    Fsol = r1_fopen ( prepend_path ( solFileName ), TEXT_LEN, "r" ) ;
    if ( !Fsol )
      printf ( "   WARNING: could not open solution file: %s\n"
               "            Reading grid only.\n", solFileName ) ;
    else {
      if ( !hdfSol ) {
        read_avbp_sol ( Fsol, pUns, pFineChunk ) ;
        fclose ( Fsol )  ;
      }
      else {
        fclose ( Fsol ) ;
        /* Supply a standard 'hdf' as keyword, only std reading with avbp. */
        read_hdf5_sol ( pUns, solFileName ) ; 
      }
    }
  }

  found = 0 ;
  if ( read_avbp_conn ( Fconn, pUns, pFineChunk, pFineChunk->Pvrtx ) )
    /* Read the boundary information of the finest level. */
    if ( read_avbp_asciiBound ( FasciiBound, pUns ) )
      if ( read_avbp_exBound ( FexBound, pUns, pFineChunk ) )
        if ( read_avbp_inBound ( FinBound, pUns, pFineChunk ) )
          found = 1 ;
  
  fclose ( Fconn ) ;
  fclose ( FinBound ) ; fclose ( FexBound ) ; fclose ( FasciiBound ) ;

  if ( !found ) {
    printf ( " FATAL: could not read the finest level in read_uns_avbp.\n" ) ;
    return ( 0 ) ; }


  
  /* Read the filenames for the coarser levels from the master file. */
  found = 0 ; Fopened = NULL ;
  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
    if ( ( Fopened = Fparent = r1_fopen (prepend_path ( fileName ),
					 TEXT_LEN, "r" ) ) )
      if ( fgets( fileName, TEXT_LEN, Fmaster ) )
	if ( ( Fopened = FexBound = r1_fopen (prepend_path ( fileName ),
					      TEXT_LEN, "r" ) ) )
	  if ( fgets( fileName, TEXT_LEN, Fmaster ) )
	    if ( ( Fopened = FinBound = r1_fopen (prepend_path ( fileName ),
						  TEXT_LEN, "r" ) ) )
              found = 1 ;
  if ( found ) {
    if ( !Fopened ) {
      printf ( " FATAL: could not open the coarse grid files in read_uns_avbp.\n" ) ;
      fclose( Fparent ) ; fclose ( Fconn ) ; fclose ( FinBound ) ; fclose ( FexBound ) ; 
      return ( 0 ) ; }


    
    /* Read the parent elements. Note that the parent's vertices are
       stored with the finest level. */
    found = 0 ;
#ifdef ADAPT_HIERARCHIC
    /* Set the parent and children pointers, set all parent element numbers
       to zero. */
    if ( read_avbp3_prt ( Fparent, pUns, &PcoarseChunk ) )
#endif
      /* Read the boundary information of the coarser level. */
      if ( read_avbp_exBound ( FexBound, pUns, PcoarseChunk ) )
	if ( perBc_in_exBound || read_avbp_inBound ( FinBound, pUns, PcoarseChunk ) )
          found = 1 ;
    if ( !found ) {
      printf ( " FATAL: could not read the coarser level in read_uns_avbp.\n" ) ;
      return ( 0 ) ; }
    fclose ( Fparent ) ; fclose ( Fconn ) ; fclose ( FinBound ) ; fclose ( FexBound ) ; 
  }
  
  /* Make sure there are no coincident vertices. */
  set_degenVx ( pUns ) ;

  /* Unstructured. Make a new grid. */
  if ( !( Pgrid = make_grid () ) ) {
    printf ( " FATAL: malloc for the linked list of grids"
	     " failed in read_uns_avpb.\n" ) ;
    free_chunk ( pUns, &PcoarseChunk ) ;
    free_chunk ( pUns, &pFineChunk ) ;
    free_uns ( &pUns ) ;
    return ( 0 ) ; }

  /* Put the chunk into Grids. */
  Pgrid->uns.type = uns ;
  Pgrid->uns.pUns = pUns ;
  Pgrid->uns.mDim = pUns->mDim ;
  Pgrid->uns.pVarList =  &(pUns->varList) ;
  pUns->nr = Pgrid->uns.nr ;
  pUns->pGrid = Pgrid ;
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = Pgrid ;


  /* This is only here to run the ALVAST mesh. */
  if ( dg_fix_lrgAngles ) {
    if ( !merge_uns ( pUns, 0, 1 ) ) {
      hip_err ( fatal, 0, "merging of unstructured grids in read_uns_avbp failed." ) ;
      return ( 0 ) ; }
  }
  else
    /* Validate the grid. */ 
    check_uns ( pUns, check_lvl ) ;
      
  return ( 1 ) ;
}

