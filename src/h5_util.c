/*
  h5_util.c:
*/

/*! collection of utility macro functions for hdf5 access.
 */


/* 
  Last update:
  ------------
  4Apr12; introduce compression with h5_zip.
  9Feb11; add v2 specifier to H5Gopen, H5Dopen, H5Dcreate.
  3Jul10; extracted from read_hdf5 and write_hdf5.

  
  
  This file contains:
  -------------------
  h5_nxt_obj
  h5_nxt_dset
  h5_nxt_grp 

  h5_obj_exists
  h5_dset_exists
  h5_grp_exists 
  h5_open_group 

  h5_read_vec
  h5_read_dbl
  h5_read_int
  h5_read_char

  h5_write_vec_strd
  h5_write_dbl_stride
  h5_write_int_stride

  h5_write_vec
  h5_write_dbl
  h5_write_int
  h5_write_char
  h5_write_fxStr
  h5_write_one_fxStr
  h5_read_fxStr_len
  h5_read_fxStr
  h5_read_one_fxStr
 
*/
#include <strings.h>


#include "cpre.h"
#include "proto.h"
#include "hdf5.h"
#include "proto_hdf.h"


extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

static int h5_zip_value ;
static hid_t DEFAULT_pList = H5P_DEFAULT ;



/******************************************************************************
  h5_zip_pList:   */

/*! generate a Dataset access property list identifier for compression.
 */

/*
  
  Last update:
  ------------
  3Apr12: conceived.
  

  Input:
  ------
  mData: number of data elements
  compress: do compress if non-zero
  mtyp_id: hdf5 id of data type.
  
*/

hid_t h5_zip_pList ( int mData, int compress, hid_t mtyp_id ) {

  ulong_t dataSize=0 ;
  hsize_t cdims ;
  int mBytes ;
  hid_t pList ;
  hid_t status ;

  if ( h5_zip_value && compress ) {
    /* HH_INT is a macro, not a fixed value it seems, so case statment fails. */
    if      ( mtyp_id == HH_INT ) dataSize = sizeof( int ) ; 
    else if ( mtyp_id == HH_DBL ) dataSize = sizeof( double ) ; 
    else if ( mtyp_id == HH_STR ) dataSize = sizeof( char ) ; 
    else      hip_err ( fatal, 0, "unrecognized hdf data class type in h5_zip." ) ;

    /* First heuristics, feel free to tune. */
    mBytes = mData*dataSize ; 

    if ( mBytes < 1000 ) {
      /* Not worth compressing, */
      pList = DEFAULT_pList ;
    }
    else {
      /* Heuristically estimate chunking sizes. */
      cdims = MAX( 2000, mBytes/10 ) ;
      cdims = MIN( cdims, mBytes ) ;
 

      pList  = H5Pcreate(H5P_DATASET_CREATE);
      status = H5Pset_chunk(pList, 1, &cdims);
      if ( status ) {
        H5Eget_msg( status, H5E_MAJOR, hip_msg, LINE_LEN ) ;
        hip_err ( fatal, 0, hip_msg ) ; 
      } 
      status = H5Pset_deflate( pList, h5_zip_value );
    }

  }
  else {
    /* No compression. */
    pList = DEFAULT_pList ;
  } 

  return ( pList ) ;
}


/******************************************************************************
  h5_dclass2parType:   */

/*! Convert the recognised HDF5 data class types to 
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

parType_e h5_dclass2parType ( hid_t dclass_id ) {

  /* can't use a switch statement here as HH_INT, which is aliased to
     H5T_NATIVE_INT is a macro, not an integer. */
  if ( dclass_id == HH_INT ) 
    return ( parInt ) ;
  else if ( dclass_id == HH_DBL ) 
    return ( parDbl ) ;
  else 
    return ( noPar ) ;
}



/******************************************************************************

  h5_nxt_dset:
  Return the next dataset in a group.
  
  Last update:
  ------------
  13Apr08; converted to hdf5-1.8, drop deprecated functions and use H5L.
  14Oct06: conceived.
  
  Input:
  ------
  grp_id
  *idx: pointer to index, 0 <= idx < mObjs where mObjs is the num of objs in grp_id.

  Changes To:
  -----------
  *idx: pointer to index, 0 <= idx < mObjs where mObjs is the num of objs in grp_id.
  obj_name: string of at least LINE_LEN containg the name of the dataset, if succesful.

  Returns:
  --------
  0 on failure, 1 if a next dataset was found.
  
*/

int h5_nxt_obj ( hid_t loc_id, int objType, int *idx, char *obj_name ) {

  int mObjs, thisObjType ;
  hsize_t h_mObjs ;
  hid_t obj_id ;

  H5Gget_num_objs ( loc_id, &h_mObjs ) ;
  mObjs = h_mObjs ;

  if ( *idx <= 0 ) {
    /* Init. */
    *idx = 0 ;
  }


  while ( *idx < mObjs ) {
    H5Lget_name_by_idx( loc_id, ".", H5_INDEX_NAME, H5_ITER_INC, *idx, 
                        obj_name, TEXT_LEN, H5P_DEFAULT  ) ;
    obj_id = H5Oopen( loc_id, obj_name, H5P_DEFAULT ) ;
    thisObjType = H5Iget_type( obj_id  ) ;
    H5Oclose( obj_id ) ;

    (*idx)++ ;

    if ( thisObjType == objType ) {
      /* This matches. */
      return ( 1 ) ;
    }
  }

  obj_name[0] = '\0' ;
  return ( 0 ) ;
}

int h5_nxt_dset ( hid_t grp_id, int *idx, char *dset_name ) {
  return ( h5_nxt_obj ( grp_id, H5I_DATASET, idx, dset_name ) ) ;
}


int h5_nxt_grp ( hid_t loc_id, int *idx, char *grp_name ) {
  return ( h5_nxt_obj ( loc_id, H5I_GROUP, idx, grp_name ) ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  21Feb17: conceived.
  

  Input:
  ------
  grp_id: id of opened group to read in.
  mIgnoreDset: number of data ets to disregard.
  ignoreNm: names of data sets to disregard. 
  grpName: name of group.
    
  Returns:
  --------
  1 if found, 0 if empty.
  
*/

int h5_list_unread_dset ( hid_t grp_id, char grpName[],
                          int mIgnoreDset, char ignoreNm[][LINE_LEN] ) {

  char dset_name[LINE_LEN] ;
  int idx=0, k, match = 0, mIgnored = 0 ;
  while ( h5_nxt_dset ( grp_id, &idx, dset_name ) ) {

    /* Is this group among those to be ignored? */
    for ( k = 0 ; k < mIgnoreDset ; k++ ) {
      if ( !strcmp( dset_name, ignoreNm[k] ) ) {
        break ;
      }
    }
      
      
    if ( k == mIgnoreDset ) {
      /* Not in ignore list. Show. */
      mIgnored++ ;
      if ( verbosity > 2 ) {
        sprintf ( hip_msg, "ignored data set %20s in group %s.",
                  dset_name, grpName ) ;
        hip_err ( info, 1, hip_msg ) ;
      }
    }
  }

  if ( verbosity == 1 ) {
    /* Just a summary, still per group. */
    sprintf ( hip_msg, "ignored %d data sets in group %s.", mIgnored, grpName ) ;
  }

  
  return ( mIgnoreDset ) ;
}



/******************************************************************************

  h5_obj_exists:
  Find out whether a named object in a group exists and return its type.
  
  Last update:
  ------------
  13Apr08: conceived.
  
  Input:
  ------
  grp_id:
  name:

  Changes To:
  -----------
  type:
  
  Returns:
  --------
  H5I_BADID = -1  on failure, object type on success.
  
*/

int h5_obj_exists ( hid_t grp_id, const char* name ) {

  hid_t obj_id ;
  H5I_type_t obj_tp ;

  if ( H5Lexists( grp_id, name, H5P_DEFAULT ) ) {
    obj_id = H5Oopen( grp_id, name, H5P_DEFAULT ) ;
    obj_tp = H5Iget_type( obj_id  ) ;
    H5Oclose( obj_id ) ;

    return ( obj_tp ) ;
  }
  else 
    return ( H5I_BADID ) ;
}



/******************************************************************************

  h5_dset_exists:
  In hdf-1.6 there was an ugly non-suppresible error msg if one tried to
  open an non-existent data-set. Hence the loop with h5_nxt was built to
  step through and find a match before opening. That does not seem to work
  any longer in hdf-1.8
  Use the LT function.
  
  Last update:
  ------------
  13Apr08; switch to hdf5-1.8, drop use of h5_nxt, add doc.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int h5_dset_exists ( hid_t grp_id, const char *dset_name ) {

  if ( h5_obj_exists ( grp_id, dset_name ) == H5I_DATASET )
    return ( 1 ) ;
  else 
    return ( 0 ) ;

  /*
  char nextDsetName[TEXT_LEN] ;
  int idx = 0 ;

  while ( h5_nxt_dset ( grp_id, &idx, nextDsetName ) ) {
    if ( !strcmp ( nextDsetName, dset_name ) ) {
      return ( 1 ) ;
    }
  }
  */

  return ( 0 ) ;
}



int h5_grp_exists ( hid_t loc_id, const char *grp_name ) {

  if ( h5_obj_exists ( loc_id, grp_name ) == H5I_GROUP )
    return ( 1 ) ;
  else 
    return ( 0 ) ;
}


hid_t h5_open_group ( hid_t file_id, char *grpName ) {

  hid_t grp_id ;

  if ( h5_grp_exists ( file_id, grpName ) ) {
    grp_id = H5Gopen(file_id, grpName, H5P_DEFAULT ) ;
    if ( grp_id > 0 )
      return ( grp_id ) ;
    else {
      sprintf ( hip_msg, "could not open grp %s in h5_open_group.\n", grpName ) ;
      hip_err ( fatal, 0, hip_msg ) ;
      return ( 0 ) ;
    }
  }
  else {
    sprintf ( hip_msg, "group %s does not exist in h5_open_group.\n", grpName ) ;
    hip_err ( fatal, 0, hip_msg ) ;
    return ( 0 ) ;
  }
}


/******************************************************************************

  h5_read_arr:
  Read a 2d array or its size from hdf5.
  
  Last update:
  ------------
  1Apr16; derived from h5_read_vec.
  
  Input:
  ------
  grp_id:  group id
  class_id: one of H5F_NO_CLASS, H5T_INTEGER, H5T_FLOAT, H5T_STRING
  pmtyp_id: pointer to memory type
  set_nm: set name
  mRows: vector length, 1st dim in row major
  mCols: number of colums, 2nd dim in row major 
  data:   pointer to data storage, no read if NULL
  
  Returns:
  --------
  0 on failure, the number of elements in the array otherwise.
  
*/
ulong_t h5_read_arr ( hid_t grp_id, hid_t class_id, hid_t *pmtyp_id, 
                      const char *set_nm, size_t mRows, size_t mCols, 
                      void *data ) {

  hid_t dspc_id, dset_id, dtype_id, mtyp_id = *pmtyp_id ;
  herr_t status ;
  hsize_t hsz_dim[2] = {0}, hsz_maxDim[2] ;

  int mDim ;

  /* Does the object exist? */
  if ( !h5_dset_exists ( grp_id, set_nm ) ) {
    return ( 0 ) ;
  }

  /* Dataset */
  dset_id =  H5Dopen(grp_id, set_nm, H5P_DEFAULT ) ;



  /* Type */
  dtype_id = H5Dget_type(dset_id);
  hid_t thisClass_id = H5Tget_class( dtype_id ) ;
  if ( class_id == H5T_NO_CLASS ) {
    switch ( thisClass_id ) {
    case ( H5T_INTEGER ) : *pmtyp_id = HH_INT ; break ;
    case ( H5T_FLOAT  ) : *pmtyp_id = HH_DBL ; break ;
    case ( H5T_STRING  ) : *pmtyp_id = HH_STR ; break ;
    default :
      sprintf ( hip_msg, "unrecognized hdf data class type in h5_read_vec for dset %s",
                set_nm ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    mtyp_id = *pmtyp_id ;
  }
  else {
    if ( thisClass_id !=  class_id ) {
      /* Type doesn't match, exit quietly. */
      H5Tclose( dtype_id ) ;
      return ( 0 ) ;
    }
  }
  H5Tclose( dtype_id ) ;
  


  /* Dataspace */
  dspc_id = H5Dget_space(dset_id);

  /* Dims */
  mDim = H5Sget_simple_extent_dims( dspc_id, NULL, NULL ) ;

  if ( mDim == 2 ) {
    H5Sget_simple_extent_dims( dspc_id, hsz_dim, hsz_maxDim ) ;
  }
  else {
    H5Sclose ( dspc_id ) ;
    H5Dclose ( dset_id ) ;
    sprintf ( hip_msg, 
              "found %d dimensions for vector %s in h5_read_vec.\n", mDim,set_nm ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  /* Read data. */
  if ( data ) {
    if ( mRows < hsz_dim[0] ) {
      printf ( "too many rowsin h5_read_arr: expected %zu, found %zu\n", 
               mRows, (size_t ) hsz_dim[0] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( mCols < hsz_dim[1] ) {
      printf ( "too many columns in h5_read_arr: expected %zu, found %zu\n", 
               mCols, (size_t ) hsz_dim[1] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else 
      /* Read */
      status = H5Dread( dset_id, mtyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data );
  }



  /* conclude. */
  H5Sclose ( dspc_id ) ;
  H5Dclose ( dset_id ) ;

  size_t len_szt0  =  hsz_dim[0] ;
  size_t len_szt1  =  hsz_dim[1] ;
  if ( len_szt0 -  hsz_dim[0] || len_szt1 -  hsz_dim[1] ) {
    hip_err ( fatal, 0, 
              "differing vector length after conversion to ulong_t in h5_read_arr\n"
              "      check length of ulong_t against hdf's hsize_t of H5public.h\n" ) ;
  }
  return ( len_szt0*len_szt1 ) ;
}


ulong_t h5_read_darr ( hid_t grp_id, char *set_nm, 
                       size_t mRows, size_t mCols, double *dBuf ) {
  hid_t mtyp_id = HH_DBL ;
  return ( h5_read_arr ( grp_id, H5T_FLOAT, &mtyp_id, set_nm, 
                         mRows, mCols, dBuf ) ) ; 
}

ulong_t h5_read_iarr ( hid_t grp_id, char *set_nm, 
                       size_t mRows, size_t mCols, int *iBuf ) {
  hid_t mtyp_id = HH_INT ;
  ulong_t m = h5_read_arr ( grp_id, H5T_INTEGER, &mtyp_id, set_nm, 
                            mRows, mCols, iBuf ) ;  
  return ( m ) ;
}

ulong_t h5_read_uarr ( hid_t grp_id, char *set_nm, 
                       size_t mRows, size_t mCols, ulong_t *ulgBuf ) {
  hid_t mtyp_id = HH_ULG, unsigned_int_id = H5T_NATIVE_UINT ;
  ulong_t m = h5_read_arr ( grp_id, H5T_INTEGER, &mtyp_id, set_nm, 
                            mRows, mCols, ulgBuf ) ;  
  return ( m ) ;
}



/******************************************************************************

  h5_read_vec:
  Read a vector or its size from hdf5.
  
  Last update:
  ------------
  27Jul11; intro h5_read_dat.
  1Jul10; check for matching datatype.
  15Sep06: conceived.
  
  Input:
  ------
  grp_id:  group id
  class_id: one of H5F_NO_CLASS, H5T_INTEGER, H5T_FLOAT, H5T_STRING
  pmtyp_id: pointer to memory type
  set_nm: set name
  len: vector length
  data:   pointer to data storage, no read if NULL
  
  Returns:
  --------
  0 on failure, the number of elements in the vec otherwise.
  
*/
ulong_t h5_read_vec ( hid_t grp_id, hid_t class_id, hid_t *pmtyp_id, 
                     const char *set_nm, size_t len, void *data ) {

  hid_t dspc_id, dset_id, dtype_id, mtyp_id = *pmtyp_id ;
  herr_t status ;
  hsize_t hsz_dim[1] = {0} ;

  int mDim ;

  /* Does the object exist? */
  if ( !h5_dset_exists ( grp_id, set_nm ) ) {
    return ( 0 ) ;
  }

  /* Dataset */
  dset_id =  H5Dopen(grp_id, set_nm, H5P_DEFAULT ) ;



  /* Type */
  dtype_id = H5Dget_type(dset_id);
  hid_t thisClass_id = H5Tget_class( dtype_id ) ;
  if ( class_id == H5T_NO_CLASS ) {
    switch ( thisClass_id ) {
    case ( H5T_INTEGER ) : *pmtyp_id = HH_INT ; break ;
    case ( H5T_FLOAT  ) : *pmtyp_id = HH_DBL ; break ;
    case ( H5T_STRING  ) : *pmtyp_id = HH_STR ; break ;
    default :
      sprintf ( hip_msg, "unrecognized hdf data class type in h5_read_vec for dset %s",
                set_nm ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    mtyp_id = *pmtyp_id ;
  }
  else {
    if ( thisClass_id !=  class_id ) {
      /* Type doesn't match, exit quietly. */
      H5Tclose( dtype_id ) ;
      return ( 0 ) ;
    }
  }
  H5Tclose( dtype_id ) ;
  


  /* Dataspace */
  dspc_id = H5Dget_space(dset_id);

  /* Dims */
  mDim = H5Sget_simple_extent_dims( dspc_id, NULL, NULL ) ;

  if ( mDim == 1 ) 
    H5Sget_simple_extent_dims( dspc_id, hsz_dim, NULL ) ;
  else {
    H5Sclose ( dspc_id ) ;
    H5Dclose ( dset_id ) ;
    sprintf ( hip_msg, 
              "found %d dimensions for vector %s in h5_read_vec.\n", mDim,set_nm ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  /* Read data. */
  if ( data ) {
    if ( len < hsz_dim[0] ) {
      sprintf ( hip_msg, "too much data in h5_read_vec: expected %zu, found %zu\n", 
               len, (size_t ) hsz_dim[0] ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else 
      /* Read */
         status = H5Dread( dset_id, mtyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data );
  }



  /* conclude. */
  H5Sclose ( dspc_id ) ;
  H5Dclose ( dset_id ) ;

  size_t len_szt  =  hsz_dim[0] ;
  if ( len_szt -  hsz_dim[0] ) {
    hip_err ( fatal, 0, 
              "differing vector length after conversion to ulong_t in h5_read_vec\n"
              "      check length of ulong_t against hdf's hsize_t of H5public.h\n" ) ;
  }
  return ( len_szt ) ;
}


/******************************************************************************

  h5_read_dat/dbl/int/char:
  Read a vector of dat/dbl/int/char or its size from hdf5.
  
  Last update:
  ------------
  18Feb12; comments added.
  
  Input:
  ------
  grp_id:   group id
  set_nm:   set name
  class_id: one of H5F_NO_CLASS, H5T_INTEGER, H5T_FLOAT, H5T_STRING
  pmtyp_id: pointer to memory type
  len:      vector length
  pDat:     pointer to data storage, no read if NULL
  
  Returns:
  --------
  0 on failure, the number of elements in the vec otherwise.
  
*/

ulong_t h5_read_dat ( hid_t grp_id, char *set_nm, hid_t *pmtyp_id, size_t len, void *pDat ) {
  return ( h5_read_vec ( grp_id, H5T_NO_CLASS, pmtyp_id, set_nm, len, pDat ) ) ; 
}

ulong_t h5_read_dbl ( hid_t grp_id, char *set_nm, size_t len, double *dBuf ) {
  hid_t mtyp_id = HH_DBL ;
  return ( h5_read_vec ( grp_id, H5T_FLOAT, &mtyp_id, set_nm, len, dBuf ) ) ; 
}

ulong_t h5_read_int ( hid_t grp_id, char *set_nm, size_t len, int *iBuf ) {
  hid_t mtyp_id = HH_INT ;
  ulong_t m = h5_read_vec ( grp_id, H5T_INTEGER, &mtyp_id, set_nm, len, iBuf ) ;  
  return ( m ) ;
}

ulong_t h5_read_ulg ( hid_t grp_id, char *set_nm, size_t len, ulong_t *ulgBuf ) {
  hid_t mtyp_id = HH_ULG, unsigned_int_id = H5T_NATIVE_UINT ;
  ulong_t m = h5_read_vec ( grp_id, H5T_INTEGER, &mtyp_id, set_nm, len, ulgBuf ) ;  
  return ( m ) ;
}

ulong_t h5_read_char ( hid_t grp_id, char *set_nm, size_t len, char *string ) {
  hid_t mtyp_id = HH_STR ;
  return ( h5_read_vec ( grp_id, H5T_STRING, &mtyp_id, set_nm, len, string ) ) ; 
}


/******************************************************************************

  h5_write_vec_strd:
  Write an vector to hdf5, but allow to pick elements in stride.
  
  Last update:
  ------------
  18Apr08; derived from h5_write_vec.
  
  Input:
  ------
  gp_id:  group id
  set_nm: set name
  dim:    length of the vector
  iBuf:   data
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int h5_write_vec_strd ( hid_t grp_id, const int compress, hid_t mtyp_id, 
                        char *set_nm, int dim, int stride, void *data ) {

  hid_t dspc_id, dset_id ;
  herr_t status ;
  hsize_t hsz_dim[1], strd[1] ;
  const hsize_t zero= 0 ;
  const hsize_t mBlock = 1 ; 

  /* hdf5 does not allow to write zero length arrays. */
  if      ( dim <  0 ) return ( 0 ) ;
  else if ( dim == 0 ) return ( 1 ) ;
  


  /* Would this work, too? There does not seem to be ref. manual for H5LT.
  status = H5LTmake_dataset(gp_id, set_nm,2,dims,H5T_STD_F64BE, dBuf); */

  /* Dataspace. */
  hsz_dim[0] = dim ;
  dspc_id = H5Screate_simple( 1, hsz_dim, NULL);  /* Dataset */
  hid_t pList = h5_zip_pList ( dim, compress, mtyp_id ) ;
  dset_id = H5Dcreate(grp_id, set_nm, mtyp_id, dspc_id, 
                      H5P_DEFAULT, pList, H5P_DEFAULT ) ;

  /* Only allow a stride starting from the first element.
     start = 0
     stride = as chosen
     count = dim
     block = 1
  */
  strd[0] = stride ;
  H5Sselect_hyperslab(grp_id, H5S_SELECT_SET, &zero, strd, hsz_dim, &mBlock ) ;

  /* Write */
  status = H5Dwrite(dset_id, mtyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);

  /* conclude. */
  H5Sclose ( dspc_id ) ;
  H5Dclose ( dset_id ) ;


  return ( 1 ) ;
}


int h5_write_dbl_stride ( hid_t grp_id, const int compress, char *set_nm, 
                          int dim, int stride, double *dBuf ) {
  return ( h5_write_vec_strd ( grp_id, compress, HH_DBL, set_nm, 
                               dim, stride, dBuf ) ) ; 
}

int h5_write_int_stride ( hid_t grp_id, const int compress, char *set_nm, 
                          int dim, int stride, int *iBuf ) {
  return ( h5_write_vec_strd ( grp_id, compress, HH_INT, set_nm, 
                               dim, stride, iBuf ) ) ; 
}

/******************************************************************************

  h5_write_vec:
  Write a vector to hdf5.
  
  Last update:
  -----------
  25Feb18; make data args const.
  16Dec13; intro h5_compr_e
  3Jul10; allow overwrite of existing datasets.
  15Sep06: conceived.
  
  Input:
  ------
  grp_id:    group id
  mType_id:  data type
  dset_name: set name
  compress:  if nonzero, then write this data set compressed.
  dim:       length of the vector
  iBuf:      data
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int h5_write_vec ( hid_t grp_id, const int compress, hid_t mtyp_id,
                  const char *dset_name, size_t dim, const void *data ) {

  hid_t dspc_id=0, dset_id ;
  herr_t status ;
  hsize_t hsz_dim[1] ;
  hid_t pList ;


  /* hdf5 does not allow to write zero length arrays. */
  if ( dim <= 0 ) return ( 1 ) ;
  
  /* Does this dataset exist? */
  /* This requires linking to H5LT 
     status  = H5LTfind_dataset ( grp_id, dset_name ) ; */
  status = h5_dset_exists( grp_id, dset_name ) ;

  if ( status )
    /* Open an existing dataset. 
       We tacitly assume from now on, that it has the right type and dim.*/
    dset_id = H5Dopen(grp_id, dset_name, H5P_DEFAULT);
  else {
    /* Create Dataspace. */
    hsz_dim[0] = dim ;
    dspc_id = H5Screate_simple( 1, hsz_dim, NULL);

    /* Dataset */
    pList = h5_zip_pList ( dim, compress, mtyp_id ) ;
    dset_id = H5Dcreate(grp_id, dset_name, mtyp_id, dspc_id, 
                        H5P_DEFAULT, pList, H5P_DEFAULT );
  }

  /* Write */
  status = H5Dwrite(dset_id, mtyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);

  /* conclude. */
  if ( dspc_id ) H5Sclose ( dspc_id ) ;
  H5Dclose ( dset_id ) ;

  return ( 1 ) ;
}


int h5_write_dbl ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const double *dBuf ) {
  return ( h5_write_vec ( grp_id, compress, HH_DBL, dset_name, dim, dBuf ) ) ; 
}

int h5_write_int ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const int *iBuf ) {
  return ( h5_write_vec ( grp_id, compress, HH_INT, dset_name, dim, iBuf ) ) ; 
}

int h5_write_ulg ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const ulong_t *ulgBuf ) {
#ifdef HIP_USE_ULONG
  return ( h5_write_vec ( grp_id, compress, HH_ULG, dset_name, 
           dim, ulgBuf ) ) ; 
#else
  return ( h5_write_vec ( grp_id, compress, HH_INT, dset_name, 
           dim, ulgBuf ) ) ; 
#endif
}

int h5_write_char ( hid_t grp_id, const int compress,  
                  const char *dset_name, size_t dim, const char *string ) {
  /* This creates an array of single characters. */  
  return ( h5_write_vec ( grp_id, compress, H5T_C_S1, dset_name, 
                          dim, string ) ) ; 
}




/******************************************************************************

  h5_write_fxStr:
  Write a string as an 80 char chain, not an array of 80 chars.
  
  Last update:
  ------------
  15Dec13; intro fxStr type.
  14Dec13: derived from the renamed h5_write_one_str80
  
  Input:
  ------
  grp_id: opened group
  dset_name: label
  string: pointer to first string in array of strings to write
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int h5_write_fxStr ( hid_t grp_id, const char *dset_name, 
                      size_t dim, fxStr_e fxStr, void *string ) {


  hid_t dspc_id, dset_id, typ_id ;
  herr_t status ;
  hsize_t hsz_dim[1] = {0} ;

  /* Cast the input string into its possible types. */
  string80 *str80 = string ;
  string240 *str240 = string ;

  /* Fixed string length: */
  hsize_t hsz ;
  switch ( fxStr ) {
  case fxStr80 :
    hsz = 80 ;
    break ;
  case fxStr240 :
    hsz = 240 ;
    break ;
  default :
    hip_err ( fatal, 0, "h5_write_fxStr: invalid fixed string type." ) ;
    return (0) ; // just to pacify the compiler for using hsz possibly non-init.
  }

  typ_id = H5Tcopy( H5T_C_S1 ) ;  /* instead of 'H5T_NATIVE_CHAR' */
  H5Tset_size( typ_id, hsz ) ;
  hsz_dim[0] = dim ;
  dspc_id = H5Screate_simple( 1, hsz_dim, NULL);


  //hid_t pList = h5_zip_pList ( hsz, typ_id ) ;
  dset_id = H5Dcreate( grp_id, dset_name, typ_id, dspc_id, 
                       H5P_DEFAULT, DEFAULT_pList, H5P_DEFAULT ) ;

  switch ( fxStr ) {
  case fxStr80 :
    status = H5Dwrite( dset_id, typ_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, str80[0] );
    break ;
  case fxStr240 :
    status = H5Dwrite( dset_id, typ_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, str240[0] );
    break ;
  default :
    hip_err ( fatal, 0, "h5_write_fxStr: invalid fixed string type." ) ;
  }

  /* conclude. */
  H5Sclose ( dspc_id ) ;
  H5Dclose ( dset_id ) ;

  return ( 1 ) ;
}

/******************************************************************************

  h5_write_one_fxStr:
  Write a string as an 80 char chain, not an array of 80 chars.
  
  Last update:
  ------------
  15Dec13; intro fxStr type.
  14Dec13; renamed from h5_write_str80
  18Sep09: conceived.
  
  Input:
  ------
  grp_id: opened group
  dset_name: label
  string: to write
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int dont_use_h5_write_one_fxStr ( hid_t grp_id, const char *dset_name, 
                          fxStr_e fxStr, void *string ) {

  /* Comment JDM, 15Dec13: once h5_write_fxStr is validated, turn this one
     into a call of that with dim=1. */

  hid_t dspc_id, dset_id, typ_id ;
  herr_t status ;

  /* Cast the input string into its possible types.
  string80 str80 = string ;
  string240 str240 = string ; */


  /* Fixed string length: */
  hsize_t hsz ;
  switch ( fxStr ) {
  case fxStr80 :
    hsz = 80 ;
    break ;
  case fxStr240 :
    hsz = 240 ;
    break ;
  default :
    hip_err ( fatal, 0, "h5_write_one_fxStr: invalid fixed string type." ) ;
    return ( 0 ) ; // just to pacify the compiler, possibly using hsz non-init.
  }

  typ_id = H5Tcopy( H5T_C_S1 ) ;  /* instead of 'H5T_NATIVE_CHAR' */
  H5Tset_size( typ_id, hsz ) ;
  dspc_id = H5Screate( H5S_SCALAR ) ;

  //hid_t pList = h5_zip_pList ( hsz, typ_id ) ;
  dset_id = H5Dcreate( grp_id, dset_name, typ_id, dspc_id, 
                       H5P_DEFAULT, DEFAULT_pList, H5P_DEFAULT ) ;

  switch ( fxStr ) {
  case fxStr80 :
    status = H5Dwrite( dset_id, typ_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, string );
    break ;
  case fxStr240 :
    status = H5Dwrite( dset_id, typ_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, string );
    break ;
  default :
    hip_err ( fatal, 0, "h5_write_one_fxStr: invalid fixed string type." ) ;
  }

  /* conclude. */
  H5Sclose ( dspc_id ) ;
  H5Dclose ( dset_id ) ;

  return ( 1 ) ;
}


/******************************************************************************

  h5_read_fxStr_len:
  Read the length of a fxStr. Typically 80 or 240.
  
  Last update:
  ------------
  5may24; query string padding type, increment sdim if no padding
  17Dec14; derived from h5_read_fxStr.
  
  Input:
  ------
  grp_id: opened group
  dset_name: label
  
  Returns:
  --------
  length of the fxStr.
  
*/


int h5_read_fxStr_len ( hid_t grp_id, const char *dset_name ) {

  /* Does the object exist? */
  if ( !h5_dset_exists ( grp_id, dset_name ) ) {
    return ( 0 ) ;
  }

  /* Get the datatype and its size. */
  hid_t dset_id =  H5Dopen( grp_id, dset_name, H5P_DEFAULT ) ;
  hid_t dtype_id = H5Dget_type ( dset_id ) ;
  int sdim = H5Tget_size ( dtype_id ) ;
  
  H5T_str_t padType = H5Tget_strpad (dtype_id ) ;
  if ( padType == H5T_STR_NULLPAD )
    // String written with no padding, Make room for null terminator 
    sdim++; 

  /* Done.    */
  H5Dclose (dset_id);

  return ( sdim ) ;
}


/******************************************************************************

  h5_read_fxStr:
  Read a string as an 80 char chain, not an array of 80 chars.
  
  Last update:
  ------------
  4may24; test for padding or not of strings
  15Dec13; renamed from h5_read_str80, allow variable length.
  27Aug11: derived from h5_write_str80.
  
  Input:
  ------
  grp_id: opened group
  dset_name: label
  fxStr: type of string (fx80 or fx240)
  string: ptr to allocated field
  
  Returns:
  --------
  number of elements of fxStr present (if string==NULL), or # read.
  
*/

int h5_read_fxStr ( hid_t grp_id, const char *dset_name, 
                     size_t dim, fxStr_e fxStr, char *string ) {

  /* Does the object exist? */
  if ( !h5_dset_exists ( grp_id, dset_name ) ) {
    return ( 0 ) ;
  }



  /* Get the datatype and its size.
http://www.hdfgroup.org/ftp/HDF5/examples/examples-by-api/hdf5-examples/1_8/C/H5T/h5ex_t_string.c
   */
  hid_t dset_id =  H5Dopen( grp_id, dset_name, H5P_DEFAULT ) ;
  hid_t dtype_id = H5Dget_type ( dset_id ) ;
  ulong_t sdim = H5Tget_size ( dtype_id ) ;

  // See:
  // http://web.mit.edu/fwtools_v3.1.0/www/Datatypes.html
  H5T_str_t padType = H5Tget_strpad (dtype_id ) ;
  if ( padType == H5T_STR_NULLPAD )
    // String written with no padding, Make room for null terminator 
    sdim++; 


  int max_len ;
  switch ( fxStr ) {
  case fxStr80 :
    max_len = 80 ; break ;
  case fxStr240 :
    max_len = 240 ; break ;
  default :
    max_len = 0 ;
    hip_err ( fatal, 0, "h5_read_fxStr: invalid fixed string type." ) ;
  }

  if ( sdim > max_len  ) {
    sprintf ( hip_msg, 
              "fxStr in h5_read_fxStr can be only 80, 240. Found %"FMT_ULG"\n", sdim ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Create the memory datatype. */
  hid_t mTyp_id = H5Tcopy (H5T_C_S1);
  H5Tset_size ( mTyp_id, sdim ) ;



  /*
   * Get dataspace and allocate memory for read buffer.  This is a
   * two dimensional dataset so the dynamic allocation must be done
   * in steps.
   */
  hid_t dspc_id = H5Dget_space ( dset_id ) ;
  int mDim = H5Sget_simple_extent_dims ( dspc_id, NULL, NULL ) ;

  /* Treat only a scalar fxStr or a 1d array with fxStr */
  hid_t status ;
  hsize_t hsz_dim[1] = {0};
  if ( mDim == 1 ) {
    H5Sget_simple_extent_dims( dspc_id, hsz_dim, NULL ) ;

    if ( string ) {
      /* Get data. */
      if ( dim < hsz_dim[0] ){
        printf ( "too much data in h5_read_fxStr: expected %zu, found %zu\n", 
                 dim, (size_t ) hsz_dim[0] ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else
        /* Read */
        status = H5Dread( dset_id, mTyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                          string);
    }
  }
  else {
    hip_err ( fatal, 0, 
              "expecting only a scalar string or a 1d array"
              " with only one element in h5_read_fxStr.\n" ) ;
  }

    

  /* Done.    */
  status = H5Dclose (dset_id);
  status = H5Sclose (dspc_id);
  status = H5Tclose (dtype_id);
  status = H5Tclose (mTyp_id);

  return ( hsz_dim[0] ) ;
}



/******************************************************************************

  h5_read_one_fxStr:
  Read a string as an 80 char chain, not an array of 80 chars.
  
  Last update:
  ------------
  4may24; test for padding or not of strings
  23jul20; fix ptr arg to strcpy of str80, str240.
  15Dec13; renamed from h5_read_str80, allow variable length.
  27Aug11: derived from h5_write_str80.
  
  Input:
  ------
  grp_id: opened group
  dset_name: label
  string: to write
  
  Returns:
  --------
  0 on failure, 1 on success
  
*/

int h5_read_one_fxStr ( hid_t grp_id, const char *dset_name, 
                         fxStr_e fxStr, char *string ) {

  /* Does the object exist? */
  if ( !h5_dset_exists ( grp_id, dset_name ) ) {
    return ( 0 ) ;
  }



  /* Get the datatype and its size.
http://www.hdfgroup.org/ftp/HDF5/examples/examples-by-api/hdf5-examples/1_8/C/H5T/h5ex_t_string.c
   */
  hid_t dset_id =  H5Dopen( grp_id, dset_name, H5P_DEFAULT ) ;
  hid_t dtype_id = H5Dget_type ( dset_id ) ;
  ulong_t sdim = H5Tget_size ( dtype_id ) ;
  H5T_str_t padType = H5Tget_strpad (dtype_id ) ;
  if ( padType == H5T_STR_NULLPAD )
    // String written with no padding, Make room for null terminator 
    sdim++;

  /*
   * Get dataspace and allocate memory for read buffer.  This is a
   * two dimensional dataset so the dynamic allocation must be done
   * in steps.
   */
  hid_t dspc_id = H5Dget_space ( dset_id ) ;
  int mDim = H5Sget_simple_extent_dims ( dspc_id, NULL, NULL ) ;
  /* Treat only a scalar str80 or a 1d array with only one str80 */
  if ( mDim != 0 ) {
    if ( mDim == 1 ) {
      hsize_t hsz_dim[1] = {0};
      H5Sget_simple_extent_dims( dspc_id, hsz_dim, NULL ) ;
      /* */
      if ( hsz_dim[0] != 1 ) {
	hip_err ( fatal, 0, "expecting only a scalar string or a 1d array with only one element in h5_read_one_fxStr.\n" ) ;
      }
    }
    else {
      hip_err ( fatal, 0, "expecting only a scalar string or a 1d array with only one element in h5_read_one_fxStr.\n" ) ;
    }
  }
    

  /* Create the memory datatype. */
  hid_t mTyp_id = H5Tcopy (H5T_C_S1);
  H5Tset_size ( mTyp_id, sdim ) ;

  /* Define tmp containers. */
  string80 str80 ;
  string240 str240 ;
  /* Read the data. */
  hid_t status ;
  switch ( fxStr ) {
  case fxStr80 :
    status = H5Dread ( dset_id, mTyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, &str80 );
    // Compiler warning. should be pointer as 2nd arg?
    //strncpy( string, str80, sizeof(str80) ) ;
    strncpy( string, str80, 80 ) ;
    break ;
  case fxStr240 :
    status = H5Dread ( dset_id, mTyp_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, &str240 );
    // strncpy( string, str240, sizeof(str240) ) ;
    strncpy( string, str240, 240 ) ;
    break ;
  default :
    hip_err ( fatal, 0, "h5_read_fxStr: invalid fixed string type." ) ;
  }
 
  /* Done.    */
  status = H5Dclose (dset_id);
  status = H5Sclose (dspc_id);
  status = H5Tclose (dtype_id);
  status = H5Tclose (mTyp_id);

  return ( 1 ) ;
}




/******************************************************************************
  h5_zip:   */

/*! set compression level for hdf write utilities.
 */

/*
  
  Last update:
  ------------
  3Apr12: conceived.
  

  Input:
  ------
  h5w_zip: the compression level, 0 being none.
  
*/

void h5_set_zip ( int h5w_zip ) {

  h5_zip_value = h5w_zip ;
  return ;
}

size_t h5_get_dset_size(hid_t parent,const char* name)
{
    hid_t dset=H5Dopen2(parent,name,H5P_DEFAULT);
    hid_t space=H5Dget_space(dset);
    size_t pcount=H5Sget_simple_extent_npoints(space);

    H5Sclose(space);
    H5Dclose(dset);

    return pcount;
}


void h5_load_int_hyperslab(hid_t parent,const char* name,int* buffer,size_t offset,size_t count)
{
    int status;

    //open data set
    hid_t dset=H5Dopen2(parent,name,H5P_DEFAULT);
    //get file data space
    hid_t filespace=H5Dget_space(dset);
    //create memory data space
    hsize_t dims=count;
    hid_t memspace = H5Screate_simple (1, &dims, NULL);

    //select hyperslab for file data space
    hsize_t h_offset = offset;
    status = H5Sselect_hyperslab (filespace, H5S_SELECT_SET, &h_offset, NULL,
                                  &dims, NULL);
    //select hyperslab for memory data space
    h_offset=0;
    status = H5Sselect_hyperslab (memspace, H5S_SELECT_SET, &h_offset, NULL,
                                  &dims, NULL);
    //read data set hyperslab
    status=H5Dread(dset, H5T_NATIVE_INT,
        memspace, filespace, H5P_DEFAULT, buffer );

    //close hdf5 objects
    H5Sclose(memspace);
    H5Sclose(filespace);
    H5Dclose(dset);
}


void h5_load_double_hyperslab(hid_t parent,const char* name,double* buffer,size_t offset,size_t count)
{
    int status;

    //open data set
    hid_t dset=H5Dopen2(parent,name,H5P_DEFAULT);
    //get file data space
    hid_t filespace=H5Dget_space(dset);

    //create memory data space
    hsize_t dims=count;
    hid_t memspace = H5Screate_simple (1, &dims, NULL);

    //select hyperslab for file data space
    hsize_t h_offset = offset;
    status = H5Sselect_hyperslab (filespace, H5S_SELECT_SET, &h_offset, NULL,
                                  &dims, NULL);
    //select hyperslab for memory data space
    h_offset=0;
    status = H5Sselect_hyperslab (memspace, H5S_SELECT_SET, &h_offset, NULL,
                                  &dims, NULL);
    //read data set hyperslab
    status=H5Dread(dset, H5T_NATIVE_DOUBLE,
        memspace, filespace, H5P_DEFAULT, buffer );

    //close hdf5 objects
    H5Sclose(memspace);
    H5Sclose(filespace);
    H5Dclose(dset);
}




