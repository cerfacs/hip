/*
   read_avbp_bound.c:
   Read a set of files in AVBP 4.2 format. Boundaries.


   Last update:
   ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
   18Feb13; switch format specifiers to %zu for ulong_t types.
   04Jun12; trim line before scanning to avoid redundant spaces in BCtext when writing the asciiBound
   30Dec11; GS: compatibility with boundary names with multiple spaces within name
   18Dec10; new pBc->type
   10Dec97; intro perBc_in_exBound.


   This file contains:
   -------------------
   read_avbp_asciiBound
   read_avbp_exBound 
   read_avbp_inBound 

*/

#include "cpre.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto.h"
#include "proto_uns.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;

extern const elemType_struct elemType[] ;
extern int perBc_in_exBound ;

/******************************************************************************

  read_avbp_asciibound_4p7:
  Ascii boundary patches and conditions file.
  
  Last update:
  ------------
  9Apr13; switch default for "WALL" bc to v, so as not to collide with
          AVBP's use of 'w' for forced corrners (mpVx).
  3Sep12; use hip_err.
  4Jul10; be less strict in comparison, look at the stems only.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void avbp4p7_bc_deco ( FILE *fAscii, const char *bcText, 
                       const char *bcTpStr, char *bcType ) {

  int sign ;
  char perLabel[MAX_BC_CHAR] ;
  
  /* Most of the types are single characters, set the terminator. */
  *(bcType+1) = '\0' ;
 
  if ( !strncmp ( bcTpStr, "PERIODIC_AXI"     , 12 ) ) {
    /* Read the next line. if -1: l, +1: u */
    fscanf ( fAscii, "%*[\n]" ) ;
    fscanf ( fAscii, "%d", &sign ) ;

    /* Find the periodic label/tag from bcText. */
    if ( !strncmp( bcText, "hip_per_inlet", 13 ) ) {
      strcpy ( perLabel, bcText+14 ) ;
    }
    else if ( !strncmp( bcText, "hip_per_outlet", 14 ) ) {
      strcpy ( perLabel, bcText+15 ) ;
    }

    if ( sign == -1 )
      sprintf ( bcType, "l%s", perLabel ) ;
    else
      sprintf ( bcType, "u%s", perLabel ) ;
  }
  /* All other types are single character. */
  else if ( !strncmp ( bcTpStr, "INLET_FREESTREAM" , 16 ) ) *bcType = 'f' ;
  else if ( !strncmp ( bcTpStr, "INLET"            ,  5 ) ) *bcType = 'e' ;
  else if ( !strncmp ( bcTpStr, "OUTLET"           ,  6 ) ) *bcType = 'o' ;
  else if ( !strncmp ( bcTpStr, "SYMMETRY"         ,  8 ) ) *bcType = 's' ;
  else if ( !strncmp ( bcTpStr, "WALL_SLIP"        ,  9 ) ) *bcType = 'i' ;
  else if ( !strncmp ( bcTpStr, "WALL_NOSLIP"      , 11 ) ) *bcType = 'v' ;
  else if ( !strncmp ( bcTpStr, "WALL"             ,  4 ) ) *bcType = 'v' ;
  else                                                      *bcType = 'n' ;

  return ;
}

int read_avbp_asciiBound_4p7 ( FILE *fAscii, uns_s *pUns ) {
  
  int nBc, iBc, mBndPatches ;
  char line[MAX_BC_CHAR] ;
  char bcText[MAX_BC_CHAR], bcType[MAX_BC_CHAR] ;
  bc_struct *Pbc ;

  hip_err ( blank, 1, "   Reading 4.7-5.3 ascii boundary information." ) ;

  rewind ( fAscii ) ;
  
  
  /* Skip the first line with comments. */
  fscanf ( fAscii, " %*[^\n]" ) ; fscanf ( fAscii, "%*[\n]" ) ; 

  /* Number of boundary patches. */
  fscanf ( fAscii, "%d%*[^\n]", &mBndPatches ) ; fscanf ( fAscii, "%*[\n]" ) ;
  pUns->mBc = mBndPatches ;

  /* A list of bcs for this rootchunk only. */
  pUns->ppBc = arr_malloc ( "pUns->ppBc in read_avbp_asciiBound", pUns->pFam,
                            mBndPatches, sizeof ( *pUns->ppBc ) ) ;

  for ( nBc = 0 ; nBc < mBndPatches ; nBc++ ) {

    /* Skip to the next line with ------. */
    while ( 1 ) {
      fscanf ( fAscii, "%5s", bcText ) ;
      fscanf ( fAscii, "%*[^\n]" ) ; fscanf ( fAscii, "%*[\n]" ) ;
      if ( !strcmp ( bcText, "-----" ) )
        break ;
    }
    /* Skip the comment line. */
    fscanf ( fAscii, "%*[^\n]" ) ; fscanf ( fAscii, "%*[\n]" ) ;

    
    /* Read the label, but avoid overflows and strip trailing blanks. */
    fgets( line, MAX_BC_CHAR, fAscii ) ; fscanf ( fAscii, "%*[\n]" ) ;
    trim(line);
    sscanf ( line, "%[^\n]s", bcText ) ;
    /* Type. */
    fgets( line, MAX_BC_CHAR, fAscii ) ; fscanf ( fAscii, "%*[\n]" ) ;    
    sscanf ( line, "%s", bcType ) ; 

    
    sprintf ( hip_msg, "      Found boundary %s, type %s.", bcText, bcType ) ;
    hip_err ( blank, 4, hip_msg ) ;

    if ( !( Pbc = find_bc ( bcText, 1 ) ) ) {
      hip_err ( fatal, 0, "could not add for boundary cond in read_avbp_asciibound." ) ;
    }
    
    /* Decode the asciiBound tags. */
    avbp4p7_bc_deco( fAscii, bcText, bcType, Pbc->type ) ;
    pUns->ppBc[nBc] = Pbc ;

    for ( iBc = 0 ; iBc < nBc ; iBc++ )
      if ( Pbc == pUns->ppBc[iBc]  ) {
	/* This one exists already. */
	sprintf ( hip_msg, "bc labeled '%s' already assigned. Bc's will coalesce.",
		 bcText ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
  }

  return ( 1 ) ;
}


/******************************************************************************

  read_avbp_asciibound_4p2:
  Ascii boundary patches and conditions file.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int read_avbp_asciiBound_4p2 ( FILE *fAscii, uns_s *pUns ) {
  
  int nBc, iBc, bcType, mBndPatches ;
  char bcText[MAX_BC_CHAR] ;
  bc_struct *Pbc ;

  if ( verbosity > 1 )
    printf ( "   Reading 4.2 ascii boundary information.\n" ) ;

  rewind ( fAscii ) ;
  
  /* Skip the first line with comments. */
  fscanf ( fAscii, " %*[^\n]" ) ; fscanf ( fAscii, "%*[\n]" ) ; 

  /* Number of boundary patches. */
  fscanf ( fAscii, "%d%*[^\n]", &mBndPatches ) ; fscanf ( fAscii, "%*[\n]" ) ;
  pUns->mBc = mBndPatches ;

  /* A list of bcs for this rootchunk only. */
  pUns->ppBc = arr_malloc ( "pUns->ppBc in read_avbp_asciiBound", pUns->pFam,
                            mBndPatches, sizeof ( *pUns->ppBc ) ) ;

  for ( nBc = 0 ; nBc < mBndPatches ; nBc++ ) {
    /* Skip two lines. */
    fscanf ( fAscii, "%*[^\n]%*[\n]%*[^\n]" ) ; fscanf ( fAscii, "%*[\n]" ) ; 
    /* Read the text. */
    fgets( bcText, MAX_BC_CHAR, fAscii ) ; fscanf ( fAscii, "%*[\n]" ) ;
    r1_beginstring ( bcText, MAX_BC_CHAR ) ;
    /* Type. */
    fscanf ( fAscii, "%d%*[^\n]", &bcType ) ; fscanf ( fAscii, "%*[\n]" ) ;
    
    if ( verbosity > 3 )
      printf ( "      Found boundary %s, type %d.\n", bcText, bcType ) ;

    if ( !( Pbc = find_bc ( bcText, 1 ) ) ) {
      printf ( " FATAL: could not add for boundary cond in read_avbp_asciibound.\n" ) ;
      return ( 0 ) ;
    }
    /* Can't be bothered to put in backward compatibility for this one. */
    Pbc->type[0] = 'o' ; Pbc->type[1] = '\0' ;
    pUns->ppBc[nBc] = Pbc ;

    for ( iBc = 0 ; iBc < nBc ; iBc++ )
      if ( Pbc == pUns->ppBc[iBc]  )
	/* This one exists already. */
	printf ( " WARNING: bc labeled '%s' already assigned. Bc's will coalesce.\n",
		 bcText ) ;

    /* Skip the two lines of integer and real values. */
    fscanf ( fAscii, "%*[^\n]%*[\n]%*[^\n]" ) ; fscanf ( fAscii, "%*[\n]" ) ; 
  }

  return ( 1 ) ;
}


/******************************************************************************

  read_avbp_asciibound:
  Wrapper to read Ascii boundary patches and conditions file. Find out
  whether it is 4.7 or 4.2
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp_asciiBound ( FILE *fAscii, uns_s *pUns ) {

  int k ;
  char keyWord[LINE_LEN] ;
  
  rewind ( fAscii ) ;

  /* Skip two leading lines, than the 3 lines for the first patch and
     read the bc info. If it is numeric, 4.2, if it is uppercase alpha 4.7 */
  for ( k = 0 ; k < 5 ; k++ ) {
    fscanf ( fAscii, " %*[^\n]" ) ;
    fscanf ( fAscii, "%*[\n]" ) ; 
  }

  fscanf ( fAscii, "%20s", keyWord ) ;
  if ( r1_isalpha( keyWord, 20 ) )
    return ( read_avbp_asciiBound_4p7 ( fAscii, pUns ) ) ;

  else
    return ( read_avbp_asciiBound_4p2 ( fAscii, pUns ) ) ;
}


/******************************************************************************

  read_avbp_exBound:
  External boundary faces.
  
  Last update:
  ------------
  6Apr09; use EXIT_FAILURE in exit.
  18Jun06; allow for the 5th int (mMPVx) on the header line. 
  17May04; check that the boundary faces only reference existing elements.
  : conceived.
  
  Input:
  ------
  FexBound: the opened file.
  pUns:   the chunk with the elements and faces
  pChunk:   the chunk the element numbers in PbndFc are referring to.

  Changes To:
  -----------
  pUns:
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp_exBound ( FILE *FexBound, uns_s *pUns, chunk_struct *pChunk ) {
  
  int someInt[9], readOk, mBndPatches, mBndFaces, *mBndFacesPatch, nBndPatch,
    mBndFacesThisPatch, mBndVerts, iSkip ;
  bndFc_struct *PbndFc, *PlstBndFc ;
  bndPatch_struct *PbndPatch ;
  
  if ( verbosity > 1 )
    printf ( "   Reading external boundaries.\n" ) ;
  
  /* mBlocks, mBndPatches, mBndVerts, mBndFaces. */
  if ( FREAD ( someInt, sizeof( int ), 6, FexBound ) != 6 ||
       someInt[0] < 4*sizeof ( int ) ) {
    printf ( " FATAL: error reading mBlocks ... in read_avbp_exBound.\n" ) ;
    return ( 0 ) ; }
  if ( someInt[1] != 1 ) {
    printf ( " SORRY: read_avbp_exBound can only do 1-block meshes.\n" ) ;
    return ( 0 ) ; }
  /* Skip to end. */
  iSkip = someInt[0]-6*sizeof( int )+2*sizeof( int ) ;
  fseek ( FexBound, iSkip, SEEK_CUR ) ;
  
  /* For the first and only block: mBndPatches, dummy, mBndVerts, dummy, mBndFaces. */
  if ( FREAD ( someInt, sizeof( int ), 7, FexBound ) != 7 ||
       someInt[0] != 5*sizeof ( int ) ) {
    printf ( " FATAL: error reading mDim, mEqu in read_avbp_exBound.\n" ) ;
    return ( 0 ) ; }
  mBndPatches = someInt[1] ;
  mBndVerts = someInt[3] ;
  mBndFaces = someInt[5] ;

  if ( mBndPatches != pUns->mBc ) {
    printf ( " WARNING: Mismatch in number of boundary patches:\n"
	     "          %d expected from asciiBound, %d found in exBound.\n",
	     pUns->mBc, mBndPatches ) ;
    /* return ( 0 ) ; */ }

  /* Alloc. */
  if ( mBndFaces && pChunk ) {
    pChunk->PbndFc = arr_malloc ( "pChunk->PbndFc in read_avbp_exBound", pUns->pFam,
                                  mBndFaces+1, sizeof( *pChunk->PbndFc ) ) ;
    pChunk->PbndPatch = arr_malloc ( "pChunk->PbndPatc in read_avbp_exBound", pUns->pFam,
                                     mBndPatches+1, sizeof( *pChunk->PbndPatch ) ) ;
    mBndFacesPatch = arr_malloc ( "mBndFacesPatch in read_avbp_exBound", pUns->pFam,
                                  mBndPatches*4, sizeof( *mBndFacesPatch ) ) ;
    pChunk->mBndFaces = mBndFaces ;
    pChunk->mBndPatches = mBndPatches ;
  }
  else if ( mBndFaces ) {
    printf ( " FATAL: found %d boundary faces, but no chunk in read_avbp_exBound.\n",
	     mBndFaces ) ;
    return ( 0 ) ; }
  else {
    /* There are no boundary faces. */
    if ( pChunk ) pChunk->mBndFaces = 0 ;
    return ( 1 ) ;
  }

# ifdef CHECK_BOUND_VERTS
    /* Read the boundary vertices, compare them to the faces. */
    nBndVert = arr_malloc ( "nBndVert", pUns->pFam, mBndVerts+1, sizeof( *nBndVert ) ) ;
    mBndVertsPatch = arr_malloc ( "mBndVertsPatch in read_avbp_exBound", pUns->pFam,
                                  2*mBndPatches, sizeof( *mBndVertsPatch ) ) ;
    
    readOk = 0 ;
    if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 &&
	 someInt[0] == 2*mBndPatches*sizeof( int ) )
      /* Number of boundary vertices per patch, start + length. */
      if ( FREAD ( mBndVertsPatch, sizeof( int ), 2*mBndPatches, FexBound ) ==
	   2*mBndPatches )
	if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
	  if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 &&
	       someInt[0] == mBndVerts*sizeof( int ) )
	    /* List of indices of boundary vertices. */
	    if ( FREAD ( nBndVert, sizeof( int ), mBndVerts, FexBound ) == mBndVerts )
	      if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
		if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
		  /* Skip the normals. */
		  if ( !fseek ( FexBound, someInt[0]+sizeof( int ), SEEK_CUR ) )
		    readOk = 1 ;
# else
    /* Skip the boundary vertices: index, vertex numbers, normals, ie. 3 records. */
    readOk = 0 ;
    if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
      if ( !fseek ( FexBound, someInt[0]+sizeof( int ), SEEK_CUR ) )
	if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
	  if ( !fseek ( FexBound, someInt[0]+sizeof( int ), SEEK_CUR ) )
	    if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
	      if ( !fseek ( FexBound, someInt[0]+sizeof( int ), SEEK_CUR ) )
		readOk = 1 ;
# endif
  if ( !readOk ) {
    printf ( " FATAL: failed to skip the boundary vertices in  read_avbp_exBound.\n" ) ;
    return ( 0 ) ; }

  /* For each bndPatch: dummy, mTriFaces, dummy, mQuadFaces. */
  readOk = 0 ;
  if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 ||
       someInt[0] != 4*mBndPatches*sizeof ( int ) )
    if ( FREAD ( mBndFacesPatch, sizeof( int ), 4*mBndPatches, FexBound ) ==
	 4*mBndPatches )
      if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) == 1 )
	readOk = 1 ;
  if ( !readOk ) {
    printf ( " FATAL: error reading bndFace indices in read_avbp_exBound.\n" ) ;
    return ( 0 ) ; }

  /* Check the length of the record of boundary faces. */
  if ( FREAD ( someInt, sizeof( int ), 1, FexBound ) != 1 ||
       someInt[0] != 2*mBndFaces*sizeof ( int ) ) {
    printf ( " FATAL: wrong number of boundary faces in read_avbp_exBound.\n" ) ;
    return ( 0 ) ; }

  /* Read the boundary faces. */
  for ( PlstBndFc = pChunk->PbndFc, PbndPatch = pChunk->PbndPatch+1 ;
        PbndPatch <= pChunk->PbndPatch + mBndPatches ; PbndPatch++ ) {
    /* Number of faces for this patch. */
    nBndPatch = PbndPatch - pChunk->PbndPatch - 1 ;
    /* Sum the tri and quad faces. */
    mBndFacesThisPatch = mBndFacesPatch[ 4*nBndPatch+1 ] +
      mBndFacesPatch[ 4*nBndPatch+3 ] ;

#   ifdef CHECK_BOUNDS
    if ( PlstBndFc + mBndFacesThisPatch > pChunk->PbndFc + pChunk->mBndFaces ) {
      printf ( " FATAL: beyond bounds in boundary faces in read_avbp_exBound.\n" ) ;
      return ( 0 ) ; }
#   endif

    /* The patch. */
    PbndPatch->Pchunk = pChunk ;
    PbndPatch->PbndFc = PlstBndFc + 1 ;
    PbndPatch->mBndFc = mBndFacesThisPatch ;
    PbndPatch->Pbc = pUns->ppBc[nBndPatch] ;

#   ifdef CHECK_BOUND_VERTS
      nBndVertBeg = nBndVertEnd + 1 ;
      nBndVertEnd += mBndVertsPatch[2*nBndPatch+1] ;

      /* Reset the marks of the boundary vertices in this patch. */
      for ( iVx = nBndVertBeg ; iVx <= nBndVertEnd ; iVx++ )
	pChunk->Pvrtx[ nBndVert[iVx] ].mark = 0 ;
#   endif
    
    for ( PbndFc = PlstBndFc+1 ;
	  PbndFc <= PlstBndFc + mBndFacesThisPatch ; PbndFc++ ) {
      if ( FREAD ( someInt, sizeof( int ), 2, FexBound ) != 2 ) {
	printf ( " FATAL: failure while reading boundary faces"
		 " in read_avbp_exBound.\n" ) ;
	return ( 0 ) ; }

      if ( someInt[0] > pChunk->mElems ) {
        sprintf ( hip_msg, "connectivity error in read_avbp_exBound:\n"
                 "        boundary face %td on patch %d is formed"
                 " with element %d\n"
                 "        but there are only %"FMT_ULG" elements in the grid.",
                 (PbndFc - pChunk->PbndFc), PbndPatch->Pbc->nr, 
                   someInt[0], pChunk->mElems ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      
      PbndFc->Pelem = pChunk->Pelem + someInt[0] ;
      PbndFc->nFace = someInt[1] ;
      PbndFc->Pbc = PbndPatch->Pbc ;

#     ifdef CHECK_BOUND_VERTS
        /* Find the forming vertices of the face in the list of boundary verts. */
        get_uns_face ( PbndFc->Pelem, PbndFc->nFace, PPvxFc, &mVertsFace ) ;
        for ( kVx = 0 ; kVx < mVertsFace ; kVx++ ) {
	  for ( iVx = nBndVertBeg ; iVx <= nBndVertEnd ; iVx++ )
	  if ( nBndVert[iVx] == (*PPvxFc[kVx])->number ) {
	    (*PPvxFc[kVx])->mark = 1 ;
	    break ; }
	  if ( iVx > nBndVertEnd )
	    printf ( " WARINING: could not find boundary face vertex %d"
		     " in face %d in patch %s in read_avbp_exBound.\n",
		     (*PPvxFc[kVx])->number, PbndFc-PlstBndFc, PbndFc->Pbc->text ) ;
	}
#     endif
    }
    PlstBndFc = PbndFc-1 ;

#   ifdef CHECK_BOUND_VERTS
      /* Check whether all vertices of this patch are in a face. */
      for ( iVx = nBndVertBeg ; iVx <= nBndVertEnd ; iVx++ )
	if ( !Pvrtx[ nBndVert[iVx] ].mark )
	  printf ( " FATAL: vertex %d not used with any face in patch %d in"
		   " read_avbp_exBound.\n",  nBndVert[iVx], nBndPatch ) ;
	/* else
	  printvxco ( pChunk->Pvrtx + nBndVert[iVx] ) ; */
#   endif
  }

# ifdef CHECK_BOUND_VERTS
    arr_free ( nBndVert ) ;
    arr_free ( mBndVertsPatch ) ;
# endif
  
  return ( 1 ) ;
}

/******************************************************************************

  read_avbp_inBound:
  Internal, ie periodic boundary faces.
  
  Last update:
  ------------
  13Dec08; treat older write bug that had 0 <= nPerBc < mPerBc.
  14Apr08; reread old AVBP3 (hip version <= 1.14.0 ) files.
  18Jul05; same again.
  2Jul05; fix bug with reading boundary numbers in inBound from 1 rather than 0.
  : conceived.
  
  Input:
  ------
  FinBound: file name
  pUns:     the unstructured grid root
  pChunk:   the chunk the element numbers in PbndFc are referring to.

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_avbp_inBound ( FILE *FinBound, uns_s *pUns, chunk_struct *pChunk ) {

  const int one = 1 ;
  
  int someInt[9], readOk, mPerBc, mPerFaces, mOldBndPatches, iPerPP, mNewBndFaces,
    mNewBndPatches, iSide, makeNewBc, k, iBc, 
    nPerBc[2*MAX_PER_PATCH_PAIRS], mPerFacesPatch[MAX_PER_PATCH_PAIRS] = {0} ;
  unsigned int recLen ;
  bndFc_struct *PbndFc, *POldBndFc ;
  bndPatch_struct *PbndPatch ;
  bc_struct *Pbc, *pBcIn, *pBcOut ;
  char bcTextIn[MAX_BC_CHAR], bcTextOut[MAX_BC_CHAR] ;

  if ( verbosity > 1 )
    printf ( "   Reading internal/periodic boundaries.\n" ) ;

  /* Skip the first four records dealing with periodic boundary nodes. */
  for ( k = 0 ; k < 4 ; k++ ) {
    readOk = 0 ;
    if ( FREAD ( someInt, sizeof( int ), 1, FinBound ) == 1 )
      if ( !fseek ( FinBound, someInt[0]+sizeof(int), SEEK_CUR ) )
        readOk = 1 ;
    
    if ( !readOk ) {
      printf ( " FATAL: error on record %d in read_avbp_inBound.\n", k+1 ) ;
      return ( 0 ) ; }
  }



  
  /* For each bndPatch: mFaces. */
  readOk = 0 ;
  if ( FREAD ( someInt, sizeof( int ), 1, FinBound ) ) {
    int rec5[3*MAX_PER_PATCH_PAIRS+1] ;
    recLen = someInt[0]/sizeof( int ) ;

    if ( recLen == 0 ) {
      /* No faces listed. */
      return ( 1 ) ;
    }

    /* In order to figure out whether we have a <=1.14.1 file with only
       face counters or a more recent >=1.14.2 file with also bc numbers,
       read the entire thing. */
    if ( recLen > 3*MAX_PER_PATCH_PAIRS+1 ) {
      printf ( " FATAL: this version is compiled for only %d periodic patch pairs.\n"
               "        recompile setting MAX_PER_PATCH_PAIRS in cpre_uns.h to %d\n",
               MAX_PER_PATCH_PAIRS, recLen/3+1 ) ;
      return ( 0 ) ; }

    /* A 1.14.2 file has 3*mPerBc ints and a -9999 in as the mPerBc'th int. */
    if ( ( recLen-1 )%3 ) {
      /* old: no bc counters. 1.14.0 */
      makeNewBc = 1 ;
      mPerBc = recLen ;
      if ( FREAD ( rec5, sizeof( int ), recLen+1, FinBound ) != recLen+1 ) {
        printf ( " FATAL: could not read the 14.0 style index of periodic faces.\n" ) ;
        return ( 0 ) ; }
      /* Face counters only. */
      makeNewBc = 1 ;
      mPerBc = recLen ;
      if ( mPerBc > MAX_PER_PATCH_PAIRS ) {
        printf ( " FATAL: this version is compiled for only %d periodic patch pairs.\n"
                 "        recompile setting MAX_PER_PATCH_PAIRS in cpre_uns.h to %d\n",
                 MAX_PER_PATCH_PAIRS, mPerBc ) ;
        return ( 0 ) ; }
      memcpy ( mPerFacesPatch, rec5, mPerBc*sizeof ( int ) ) ;
    }

    else {
      if ( FREAD ( rec5, sizeof( int ), recLen+1, FinBound ) != recLen+1 ) {
        printf ( " FATAL: could not read the 14.1 style index of periodic faces.\n" ) ;
        return ( 0 ) ; }
    
      mPerBc = ( recLen-1 )/3 ;
      if ( rec5[mPerBc] == -9999 ) {
        /* There are bc numbers. */
        makeNewBc = 0 ;
        memcpy ( nPerBc, rec5+mPerBc+1, 2*mPerBc*sizeof ( int ) ) ;

        /* Check whether nPerBc covers the proper range from 1-mBc. 
           If it covers 0 to mBc-1, assume a bug translate by 1 and flash 
           a warning. */
        { int hasZero = 0, hasMBc = 0, nBc ;
          for ( nBc = 0 ; nBc < mPerBc ; nBc ++ ) {
            if ( nPerBc[nBc] <= 0 ) hasZero = 1 ;
            else if  ( nPerBc[nBc] >= mPerBc ) hasMBc = 1 ;
          }
          if ( hasZero == 1 && hasMBc == 1 ) {
            /* Major trouble. Not possible to fix. */
            hip_err ( fatal, 0, "Periodic bc nos in .inBound range 0 to mPerBc.\n       hip cannot read this mesh.\n" ) ;
          }
          else if ( hasZero ) {
            /* Shift by one. */
            hip_err ( warning, 1, "Periodic bc nos in .inBound range 0 to mPerBc-1.\n         hip will translate them to a range 1 to mPerBc\n" ) ;
            for ( nBc = 0 ; nBc < 2*mPerBc ; nBc ++ ) {
              nPerBc[nBc]++ ;
            }
          }
        }
      }
      else {
        /* Face counters only. */
        makeNewBc = 1 ;
        mPerBc = recLen ;
        if ( mPerBc > MAX_PER_PATCH_PAIRS ) {
          printf ( " FATAL: this version is compiled for only %d periodic patch pairs.\n"
                   "        recompile setting MAX_PER_PATCH_PAIRS in cpre_uns.h to %d\n",
                   MAX_PER_PATCH_PAIRS, mPerBc ) ;
          return ( 0 ) ; }
      }

      memcpy ( mPerFacesPatch, rec5, mPerBc*sizeof ( int ) ) ;
    }
  }
  else
    /* No info. */
    return ( 1 ) ;



  
  for ( mPerFaces = 0, iPerPP = 0 ; iPerPP < mPerBc ; iPerPP++ )
    mPerFaces += mPerFacesPatch[iPerPP] ;

  /* Check the length of the record of boundary faces. */
  if ( FREAD ( someInt, sizeof( int ), 1, FinBound ) != 1 ||
       someInt[0] != 2*mPerFaces*sizeof ( int ) ) {
    printf ( " FATAL: wrong number of boundary faces in read_avbp_inBound.\n" ) ;
    return ( 0 ) ; }

  /* Realloc. */
  mNewBndFaces = pChunk->mBndFaces + mPerFaces ;
  POldBndFc = pChunk->PbndFc ;
  pChunk->PbndFc = arr_realloc ( "pChunk->PbndFc in read_avbp_inBound",
                                 pUns->pFam, pChunk->PbndFc,
                                 mNewBndFaces+1, sizeof( *pChunk->PbndFc ) ) ;
  mNewBndPatches  = pChunk->mBndPatches + 2*mPerBc ;
  pChunk->PbndPatch = arr_realloc ( "pChunk->PbndPatch in read_avbp_inBound",
                                    pUns->pFam, pChunk->PbndPatch,
                                    mNewBndPatches+1, sizeof( *pChunk->PbndPatch ) ) ;

  if ( pChunk->PbndFc - POldBndFc )
    /* PbndFc has moved. Update the boundary patches. */
    for ( PbndPatch = pChunk->PbndPatch+1 ; 
	  PbndPatch <= pChunk->PbndPatch + pChunk->mBndPatches ; PbndPatch++ )
      PbndPatch->PbndFc = pChunk->PbndFc + ( PbndPatch->PbndFc - POldBndFc ) ;
  
  mOldBndPatches = pChunk->mBndPatches ;
  PbndPatch = pChunk->PbndPatch + mOldBndPatches ;
  pChunk->mBndFaces = mNewBndFaces ;
  pChunk->mBndPatches = mNewBndPatches ;



  
  /* Loop over all pairs. */
  iBc = 0 ;
  for ( iPerPP = 0 ; iPerPP < mPerBc ; iPerPP++ ) {

    /* Find a suitable label for the periodic pair. */
    if ( makeNewBc ) {
      /* Make a new label that does not exist, yet. */
      while ( one ) {
        iBc++ ;
          
        /* Don't overwrite an existing one. */
        sprintf ( bcTextIn,  "hip_per_inlet_%d",  iBc ) ;
        sprintf ( bcTextOut, "hip_per_outlet_%d", iBc ) ;
        if ( !find_bc ( bcTextIn,  2 ) &&
             !find_bc ( bcTextOut, 2 ) ) {
          /* This pair doesn't exist, yet. */
          if ( !( pBcIn  = find_bc ( bcTextIn,  1 ) ) ||
               !( pBcOut = find_bc ( bcTextOut, 1 ) ) ) {
            printf ( " FATAL: could not add periodic b.c. in read_avbp_inBound.\n" ) ;
            return ( 0 ) ; }
          else
            break ;
        }
      }
    }
    
    else {
      /* Pick the existing one. nPerBc lists boundary numbers from 1-mBc,
         while ppBC is indexed from 0. */
      pBcIn  = pUns->ppBc[ nPerBc[2*iPerPP]-1  ] ;
      pBcOut = pUns->ppBc[ nPerBc[2*iPerPP+1]-1] ;
    }



    /* Read the faces. */
    for ( iSide = 0 ; iSide < 2 ; iSide++ ) {
      Pbc = ( iSide ? pBcOut : pBcIn ) ;
                 
      /* New patch. */
      PbndPatch++ ;
      PbndPatch->Pchunk = pChunk ;
      PbndPatch->Pbc = Pbc ;
      PbndPatch->mBndFc = mPerFacesPatch[iPerPP]/2 ;
      if ( PbndPatch - pChunk->PbndPatch > 1 )
	/* Increment from the previous bound patch. */
	PbndPatch->PbndFc = (PbndPatch-1)->PbndFc + (PbndPatch-1)->mBndFc ;
      else
	/* This is the first patch. */
	PbndPatch->PbndFc = pChunk->PbndFc ;

#     ifdef CHECK_BOUNDS
        if ( PbndPatch->PbndFc + PbndPatch->mBndFc - 1 >
	     pChunk->PbndFc + pChunk->mBndFaces ) {
          printf ( " FATAL: beyond bounds in boundary faces in read_avbp_inBound.\n" ) ;
	  return ( 0 ) ; }
#     endif

      /* Read the elements and faces. */
      for ( PbndFc = PbndPatch->PbndFc ;
	    PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ ) {
	if ( FREAD ( someInt, sizeof( int ), 2, FinBound ) != 2 ) {
	printf ( " FATAL: failure while reading boundary faces"
		   " in read_avbp_inBound.\n" ) ;
	  return ( 0 ) ; }
	PbndFc->Pelem = pChunk->Pelem + someInt[0] ;
	PbndFc->nFace = someInt[1] ;
	PbndFc->Pbc = Pbc ;
      }
    }
  }
  return ( 1 ) ;
}
