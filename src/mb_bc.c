/*
   mb_bc.c:
   Boundary conditions and multiblock grids.

   Last update:
   ------------
   1Jun96; conceived.

   
   Contains:
   ---------
   mb_bcSubFc:
   mb_bcBox:
   list_mb_bc:
*/


#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

/********************************************************************
  mb_bcSubFc:
  Establish a linked list of subfaces with the same boundary condition.

  Last update:
  ------------
  1Jun96; conceived.
  
  Input:
  ------
  bcS:
  mBc:
  blockS:
  mBlocks:
  
  Changes to:
  -----------
  bcS:
  blockS:

*/

void mb_bcSubFc ( block_struct blockS[], int mBlocks )
{
  bc_struct *Pbc ;
  block_struct *PBL ;
  subFace_struct *PprvSubFc, **PPSF, *PSF ;

  /* Get the root bc. */
  Pbc = find_bc ( "", 0 ) ;
  
  /* Loop over all boundary conditions. */
  for ( ; Pbc ; Pbc = Pbc->PnxtBc )
  { Pbc->ProotSubFc = NULL ;
    PprvSubFc = NULL ;
    
    /* Loop over all blocks. */
    for ( PBL = blockS+1 ; PBL <= blockS+mBlocks ; PBL++ )
      /* Loop over all subfaces. */
      for ( PPSF = PBL->PPsubFaces ;
	    PPSF < PBL->PPsubFaces + PBL->mSubFaces ; PPSF++ )
      {
	PSF = *PPSF ;
	if ( PSF->Pbc == Pbc )
	{ /* This subfaces has the sought b.c. */

	  if ( Pbc->ProotSubFc )
	  { /* Other subfaces with this bc have been found. Append. */
	    PSF->PprvBcSubFc = PprvSubFc ;
	    PprvSubFc->PnxtBcSubFc = PSF ;
	    PprvSubFc = PSF ;
	    PSF->PnxtBcSubFc = NULL ;
	  }
	  else
	  { /* This is the first of this type. */
	    Pbc->ProotSubFc = PprvSubFc = PSF ;
	    PSF->PprvBcSubFc = PSF->PnxtBcSubFc = NULL ;
	  }
	}
      }
  }
}

/********************************************************************
  
  mb_bcBox:
  Calculate the bounding box for all the subfaces with the same bc.

  Last update:
  ------------
  1Jun96; conceived.
  
  Input:
  ------
  bcS:
  mBc:
  blockS:
  mBlocks:
  
  Changes to:
  -----------
  bcS:
  blockS:

*/

void mb_bcBox ( const int mDim )
{
  static bc_struct *Pbc ;
  static int nDim, nVert ;
  static subFace_struct *PSF ;
  static block_struct *PBL ;
  static int ll[MAX_DIM], ur[MAX_DIM], index1, multVert1, multCell1, index2,
             multVert2, multCell2, offsetVert, offsetCell, indexStatic,
             dll, dlr, dur, dul, n1, n2 ;
  static double *Pcoor ;

  /* Loop over all boundary conditions. */
  for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc )
  {
    /* Reset the bounding box. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
    { Pbc->llBox[nDim] = TOO_MUCH ;
      Pbc->urBox[nDim] = -TOO_MUCH ;
    }

    /* Scan the linked list of subfaces. */
    for ( PSF = Pbc->ProotSubFc ; PSF ; PSF = PSF->PnxtBcSubFc ) {
      PBL = PSF->PlBlock ;
      get_mb_subface ( PBL, PSF, mDim, ll, ur,
		       &index1, &multVert1, &multCell1,
		       &index2, &multVert2, &multCell2,
		       &offsetVert, &offsetCell, &indexStatic,
		       &dll, &dlr, &dur, &dul ) ;
      
      /* Loop over the subface. */
      for ( n2 = ll[index2] ; n2 <= ur[index2] ; n2++ )
	for ( n1 = ll[index1] ; n1 <= ur[index1] ; n1++ )
	{ nVert = get_mb_boundVert ( n1, multVert1, n2, multVert2, offsetVert ) ;
	  Pcoor = PBL->Pcoor + nVert*mDim ;
	    
	  for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  { Pbc->llBox[nDim] = MIN( Pbc->llBox[nDim], Pcoor[nDim] ) ;  
	    Pbc->urBox[nDim] = MAX( Pbc->urBox[nDim], Pcoor[nDim] ) ;  
	  }
	}
    }
  }
}

      

/******************************************************************************

  list_mb_bc:
  List all active boundary conditions of a multiblock grid.
  
  Last update:
  ------------
  16Feb18; new interface to print_bc.
  : conceived.
  
  Input:
  ------
  Pgrid:

*/

void list_mb_bc ( const grid_struct *Pgrid ) {
  
  const mb_struct *Pmb ;
  const subFace_struct *Psf ;
  const bc_struct *Pbc ;

  Pmb = Pgrid->mb.Pmb ;

  /* Print the header. */
  print_bc ( NULL, NULL ) ;

  for ( Pbc = find_bc ( "", 0 ) ; Pbc ; Pbc = Pbc->PnxtBc )
    /* Loop over all subfaces of this bc until one with the current
       grid is found. */
    for ( Psf = Pbc->ProotSubFc ; Psf ; Psf = Psf->PnxtBcSubFc )
      if ( Psf->PlBlock->PmbRoot == Pmb ) {
	print_bc ( Pbc, NULL ) ;
	break ; }

  printf (   "\n" ) ;

}
