/*
  write_mb_cut.c:
  Write a cut in a multi-block mesh.
   
  Last update:
  ------------
  6Jan15; rename pFamMb to more descriptive pArrFamMb
  4Nov98; fix write_mb_pts.
  
  Contains:
  ---------
  write_mb_pts:
  mb_next_open_cell:
*/

#include "cpre.h"
#include "cpre_mb.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_mb.h"
#include "proto_uns.h"
#include "proto_mb_uns.h"

#define BOUND_FACE 1
#define CUT_FACE 0

extern const int verbosity ;
extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;
extern arrFam_s *pArrFamMb ;

/* A local data structure to link edges. */
typedef struct _cutEdge_s cutEdge_s ;
struct _cutEdge_s
{ double *Pcoor[2] ;     /* The two forming vertices, in order. */
  bc_struct *Pbc ;       /* The bc, or NULL if internal. */

  double egCoor[2] ;     /* A unique location of an edge near vertex 0. */
  
  cutEdge_s *PnxtCutEg ; /* A linked list. */
} ;

/****************************************************************

  write_mb_pts:
  Write a cut in a multi-block mesh to pts. 2D only.

  Input:
  ------
 
  Changes to:
  -----------


  Returns:
  --------
  0,1 on success, failure.
  
*/

int write_mb_pts ( const mb_struct *Pmb, FILE *FptsOut, const int writeKeepNotCut ) {
  
  const faceOfElem_struct * const PFoE = elemType[qua].faceOfElem ;
  const int mDim = 2 ;
  block_struct *Pbl ;
  int nBlock, ijk[MAX_DIM], mElemsMarked, nDim, mCutEg, nCell, 
    vxOff[4], nVert, mBounds, mLoopSegs, mEgFollowed, mCuts=0, nSeg ;
  chunk_struct *ProotChunk, *Pchunk, **ppChunk ;
  double llBox[MAX_DIM], urBox[MAX_DIM], llRange[MAX_DIM], urRange[MAX_DIM],
    size, *Pcoor, vec0[2], vec2[2], vec3[2], cross2[2], cross3[2], alpha2, alpha3 ;
  bc_struct *pBc, *pBcCut ;
  bndFc_struct *PbndFc ;
  intFc_struct *PintFc ;
  /* Derived types only defined within this file. */
  cutEdge_s *cutEdge, *pCutEg, *pCutEg2, *pCutEg3, *pCutBegin, *pCutBeginLoop = 0 ;
  /* Quadtree and iterator. */
  root_struct *Ptree ;
  tree_pos_struct *Piter ;
  
  if ( Pmb->mDim != 2 ) {
    printf ( " FATAL: write_mb_pts does only 2D.\n" ) ;
    return ( 0 ) ; }

  /* Alloc some bogus chunks to be able to call std functions. */
  if ( !( ppChunk = arr_malloc ( "ppChunk in write_mb_pts", pArrFamMb,
                                 Pmb->mBlocks+1, sizeof( chunk_struct * ) ) ) ) {
    printf ( " FATAL: could not malloc list of chunks in write_mb_pts.\n" ) ;
    return ( 0 ) ; }

  /* Make all chunks and link them. */
  for ( nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ )
    if ( !( ppChunk[nBlock] = make_chunk( NULL ) ) ) {
      printf ( " FATAL: could not malloc temporary chunks in write_mb_pts.\n" ) ;
      return ( 0 ) ; }
    else {
      if ( nBlock > 1 )
        ppChunk[nBlock]->PprvChunk =  ppChunk[nBlock-1]->PnxtChunk ;
      else
        ppChunk[nBlock]->PprvChunk = NULL ;
      if ( nBlock == Pmb->mBlocks )
        ppChunk[nBlock-1]->PnxtChunk = NULL ;
      else
        ppChunk[nBlock-1]->PnxtChunk = ppChunk[nBlock] ;
    }
  ProotChunk = ppChunk[1] ;

  /* Make a hip_cut bc entry. */
  pBcCut = find_bc ( "hip_cut", 1 ) ;


  
  /* Renumber all marked elements with their original block number, not the
     incremental 'unstructured' one. */
  for ( nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ ) {
    Pbl = Pmb->PblockS + nBlock ;
    if ( Pbl->PelemMark ) 
      for ( ijk[2] = ijk[1] = 1 ; ijk[1] < Pbl->mVert[1] ; ijk[1]++ )
	for ( ijk[0] = 1 ; ijk[0] < Pbl->mVert[0] ; ijk[0]++ ) {
	  nCell = get_nElem_ijk ( mDim, ijk, Pbl->mVert ) ;
	  if ( writeKeepNotCut ) {
	    /* Write the kept part. Renumber elemMark to the cell number
               in the block. */
	    if ( Pbl->PelemMark[nCell] )
	      Pbl->PelemMark[nCell] = nCell ;
	    else
	      Pbl->PelemMark[nCell] = 0 ; }
	  else {
	    /* Write the cut part, not the kept part. Flip all elemMarks. */
	    if ( Pbl->PelemMark[nCell] )
	      Pbl->PelemMark[nCell] = 0 ;
	    else
	      Pbl->PelemMark[nCell] = nCell ; }
	}
  }

  /* Loop over all blocks. Make a list of faces. */
  for ( mCutEg = 0, nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ ) {
    Pbl = Pmb->PblockS + nBlock ;
    Pchunk = ProotChunk + nBlock ;
    /* free_chunk needs a linked list of blocks. */
  
    /* Get external boundary faces. */
    if ( !get_mbBndFc ( Pbl, mDim, Pchunk ) ||
	 /* Get cut faces. */
	 !get_mbIntFc ( Pbl, mDim, Pchunk ) ) {
      printf ( " FATAL: could not get faces for block %d in write_mb_pts.\n", nBlock ) ;
      return ( 0 ) ; }

    mCutEg += Pchunk->mBndFaces + Pchunk->mIntFaces ;
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      llBox[nDim] = MIN( llBox[nDim], Pbl->llBox[nDim] ) ;
      urBox[nDim] = MAX( urBox[nDim], Pbl->urBox[nDim] ) ; }
  }

  /* Alloc a list of cut edges, boundary and interior. */
  if ( !( cutEdge = arr_malloc ( "cutEdge in write_mb_pts", pArrFamMb,
                                 mCutEg, sizeof( cutEdge_s ) ) ) ) {
    printf ( " FATAL: could not malloc temporary cutEdge in write_mb_pts.\n" ) ;
    return ( 0 ) ; }
  
  
  /* Initialize the quadtree. Make it larger, such that the borders of
     the bounding box fit inside and not just on the edges. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    size = .1*( urBox[nDim] - llBox[nDim] ) ;
    llBox[nDim] -= size ;
    urBox[nDim] += size ; }
  if ( !( Ptree = ini_tree ( pArrFamMb, "write_mb_pts", mDim, llBox, urBox, cutEg2coor ) ) ) {
    printf ( " FATAL: could allocate tree root in write_mb_pts.\n" ) ;
    return ( 0 ) ; }
  if ( !( Piter = ini_traverse ( Ptree ) ) ) {
    printf ( " FATAL: could allocate iterator in write_mb_pts.\n" ) ;
    return ( 0 ) ; }



  for ( mCutEg = 0, nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ ) {
    Pbl = Pmb->PblockS + nBlock ;
    Pchunk = ProotChunk + nBlock ;

    /* Make a map of vertex offsets for each 'unstructured' cell,
       ll-lr-ur-ul. */
    vxOff[0] = 0 ;
    vxOff[1] = 1 ;
    vxOff[3] = Pbl->mVert[0] ;
    vxOff[2] = vxOff[3] + 1 ;

    /* List all boundary faces with cutEg and introduce them into the tree
       via the lower numbered node. */
    for ( PbndFc = Pchunk->PbndFc + 1 ;
	  PbndFc <= Pchunk->PbndFc + Pchunk->mBndFaces ; PbndFc++ ) {
      /* Get the vertex pointers. */
      get_ijk_nElem ( Pbl->mVert, Pbl->mElemsBlock,
		      PbndFc->Pelem - Pchunk->Pelem, mDim, ijk ) ;
      nVert = get_nVert_ijk ( mDim, ijk, Pbl->mVert ) ;
      Pcoor = Pbl->Pcoor + mDim*nVert ;

      pCutEg = cutEdge + (mCutEg++) ;
      pCutEg->Pcoor[0] = Pcoor + mDim*vxOff[ PFoE[PbndFc->nFace].kVxFace[0] ] ;
      pCutEg->Pcoor[1] = Pcoor + mDim*vxOff[ PFoE[PbndFc->nFace].kVxFace[1] ] ;
      pCutEg->Pbc = PbndFc->Pbc ;
      pCutEg->PnxtCutEg = NULL ;

      /* Make a unique edge coordinate close to vertex 0 on the edge. */
      vec_diff_dbl ( pCutEg->Pcoor[1], pCutEg->Pcoor[0], 2, pCutEg->egCoor ) ;
      vec_norm_dbl ( pCutEg->egCoor, 2 ) ;
      vec_mult_dbl ( pCutEg->egCoor, .5*Grids.epsOverlap, 2 ) ;
      vec_add_dbl  ( pCutEg->egCoor, pCutEg->Pcoor[0], 2, pCutEg->egCoor ) ;
      
      add_data ( Ptree, ( DATA_TYPE *) pCutEg ) ;
    }

    /* Internal, ie. cut faces. */
    for ( PintFc = Pchunk->PintFc + 1 ;
	  PintFc <= Pchunk->PintFc + Pchunk->mIntFaces ; PintFc++ ) {
      /* Get the vertex pointers. */
      get_ijk_nElem ( Pbl->mVert, Pbl->mElemsBlock,
		      PintFc->Pelem - Pchunk->Pelem, mDim, ijk ) ;
      nVert = get_nVert_ijk ( mDim, ijk, Pbl->mVert ) ;
      Pcoor = Pbl->Pcoor + mDim*nVert ;

      pCutEg = cutEdge + (mCutEg++) ;
      pCutEg->Pcoor[0] = Pcoor + mDim*vxOff[ PFoE[PintFc->nFace].kVxFace[0] ] ;
      pCutEg->Pcoor[1] = Pcoor + mDim*vxOff[ PFoE[PintFc->nFace].kVxFace[1] ] ;
      pCutEg->Pbc = pBcCut ;
      pCutEg->PnxtCutEg = NULL ;

      /* Make a unique edge coordinate close to vertex 0 on the edge. */
      vec_diff_dbl ( pCutEg->Pcoor[1], pCutEg->Pcoor[0], 2, pCutEg->egCoor ) ;
      vec_norm_dbl ( pCutEg->egCoor, 2 ) ;
      vec_mult_dbl ( pCutEg->egCoor, .5*Grids.epsOverlap, 2 ) ;
      vec_add_dbl  ( pCutEg->egCoor, pCutEg->Pcoor[0], 2, pCutEg->egCoor ) ;

      add_data ( Ptree, ( DATA_TYPE *) pCutEg ) ;
    }
  }

  /*{
    int m ;
    
    for ( pCutBegin = cutEdge ; pCutBegin < cutEdge+mCutEg ; pCutBegin++ ) {
      Pcoor = pCutBegin->Pcoor[1] ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
        llRange[nDim] = Pcoor[nDim] - Grids.epsOverlap ;
        urRange[nDim] = Pcoor[nDim] + Grids.epsOverlap ; }

      m = 0 ;
      reset_range ( Piter, llRange, urRange ) ;
      while ( ( pCutEg2 = range_search ( Piter, llRange, urRange ) ) )
        m++ ;

      if ( m != 1 ) {
        reset_range ( Piter, llRange, urRange ) ;
        printf ( " mult %4d: %5d- %5d, %15s\n", pCutBegin-cutEdge, 
                 pCutBegin->Pcoor[0] - Pmb->PblockS[1].Pcoor,
                 pCutBegin->Pcoor[1] - Pmb->PblockS[1].Pcoor, pCutBegin->Pbc->text ) ;
        
        while ( ( pCutEg2 = range_search ( Piter, llRange, urRange ) ) )
          printf ( "   by %4d: %5d- %5d, %15s\n", pCutEg2-cutEdge,
                   pCutEg2->Pcoor[0] - Pmb->PblockS[1].Pcoor,
                   pCutEg2->Pcoor[1] - Pmb->PblockS[1].Pcoor, pCutEg2->Pbc->text ) ;
      }
    }
  }*/


      
  /* Loop over all edges. */
  for ( mLoopSegs = mBounds = 0, pBc = NULL, pCutBegin = cutEdge ;
        pCutBegin < cutEdge+mCutEg ; pCutBegin++ ) 
    if ( !pCutBegin->PnxtCutEg ) {
      /* This is an edge that is not in any loop, yet. */
      mEgFollowed = 0 ;

      /* Loop until the segment is closed. The segment starts and pCutBegin
	 is initialized as soon as the next bounday type on the loop
	 is encountered.  */
      pCutEg = pCutBegin ;
      while ( mEgFollowed <= mCutEg+1 ) {
	/* Find the edge continuing with pCutEg->Pcoor[1] via a range search of
	   a box +/- epsOverlap. */
	Pcoor = pCutEg->Pcoor[1] ;
	for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
	  llRange[nDim] = Pcoor[nDim] - Grids.epsOverlap ;
	  urRange[nDim] = Pcoor[nDim] + Grids.epsOverlap ; }

	/* The iterator has to be reset to contain the range. */
	reset_range ( Piter, llRange, urRange ) ;

	/* Find a first match. */
	while ( ( pCutEg2 = range_search ( Piter, llRange, urRange ) ) )
          if ( !( pCutEg2->PnxtCutEg && pCutEg2 != pCutBegin ) )
            break ;
          else
            pCutEg2 = NULL ;
        
        if ( !pCutEg2 ) {
	  printf ( " FATAL: could not find an edge to match in write_mb_pts.\n" ) ;
	  return ( 0 ) ; }

	/* Loop over other possible matches. */
	while ( ( pCutEg3 = range_search ( Piter, llRange, urRange ) ) )
	  /* Retain the one with the lowest counterclockwise angle to pCutEg. */
          if ( !pCutEg3->PnxtCutEg || pCutEg3 == pCutBegin ) {
            vec_diff_dbl ( pCutEg ->Pcoor[0], pCutEg ->Pcoor[1], 2, vec0 ) ;
            vec_diff_dbl ( pCutEg2->Pcoor[1], pCutEg2->Pcoor[0], 2, vec2 ) ;
            vec_diff_dbl ( pCutEg3->Pcoor[1], pCutEg3->Pcoor[0], 2, vec3 ) ;
            vec_norm_dbl ( vec0, 2 ) ;
            vec_norm_dbl ( vec2, 2 ) ;
            vec_norm_dbl ( vec3, 2 ) ;

            alpha2 = scal_prod_dbl ( vec0, vec2, 2 ) ;
            alpha3 = scal_prod_dbl ( vec0, vec3, 2 ) ;

            /* Note that cross_prod_dbl returns the cross product in the x-component
               in 2D. */
            cross_prod_dbl ( vec2, vec0, 2, cross2 ) ;
            cross_prod_dbl ( vec3, vec0, 2, cross3 ) ;
            if ( cross2[0] <= 0 ) alpha2 = -1.-alpha2 ;
            if ( cross3[0] <= 0 ) alpha3 = -1.-alpha3 ;
            
            pCutEg2 = ( alpha2 > alpha3 ? pCutEg2 : pCutEg3 ) ;
          }
        
        /* Link the two edges. */
        mEgFollowed++ ;
        pCutEg->PnxtCutEg = pCutEg2 ;
        pCutEg = pCutEg2 ;

        if ( pCutEg == pCutBegin )
          /* Done. The loop is finished. */
          break ;

        /* Is this a new segment? */
        if ( pCutEg->Pbc != pBc ) {
          mLoopSegs++ ;
          pBc = pCutEg->Pbc ;
          pCutBeginLoop = pCutEg ;
        }
      }
      
      if ( mEgFollowed > mCutEg+1 ) {
        printf ( " FATAL: mislink of cut edges in write_mb_pts.\n" ) ;
        return ( 0 ) ; }


      /* Dump the loop to file. Dont increment the pointer in the loop, do
         that at the end to be able to void the poiners for checking. */
      for ( pBc = NULL, nSeg = 0, pCutEg = pCutBeginLoop ;
             ; pCutEg = pCutEg->PnxtCutEg ) {
      
        if ( pCutEg->Pbc != pBc ) {
          /* New segment. */
          pBc = pCutEg->Pbc ;
          fprintf ( FptsOut, "NEWBND\n" ) ;
          if ( verbosity > 5 )
            printf ( "NEWBND\n" ) ;
          if ( pCutEg->Pbc == pBcCut )
            fprintf ( FptsOut, "%% Cut Nr. %d\n", ++mCuts ) ;
          else
            fprintf ( FptsOut, "%% %s\n", pCutEg->Pbc->text ) ;

          /* Segment connectivity. Use 0 <= nSeg < mLoopSegs. Makes cycling easier. */
          fprintf ( FptsOut,
                    "NAMEBN\n %d\n NFRSBN\n %d\n NLSTBN\n %d\n ITYPBN\n -1\n BNDEXY\n",
                    mBounds + 1 + nSeg,
                    mBounds + 1 + (nSeg+mLoopSegs-1)%mLoopSegs,
                    mBounds + 1 + (nSeg+1)%mLoopSegs ) ;
          nSeg++ ;

          /* List the first vertex. */
          Pcoor = pCutEg->Pcoor[0] ;
          fprintf ( FptsOut, "%17.9e %17.9e\n", Pcoor[0], Pcoor[1] ) ;
          if ( verbosity > 5 )
            printf ( "%17.9e %17.9e %td\n", Pcoor[0], Pcoor[1], pCutEg - cutEdge ) ;
        }
        
        /* List the loop. */
        Pcoor = pCutEg->Pcoor[1] ;
        fprintf ( FptsOut, "%17.9e %17.9e\n", Pcoor[0], Pcoor[1] ) ;
        if ( verbosity > 5 )
          printf ( "%17.9e %17.9e %td-%td\n", Pcoor[0], Pcoor[1],
                   pCutEg - cutEdge, pCutEg->PnxtCutEg - cutEdge ) ;

        /* Since on entry we satisfy the exit condition, exit here. */
        if ( pCutEg->PnxtCutEg == pCutBeginLoop )
          break ;
      }
      mBounds += mLoopSegs ;

    }
  fprintf ( FptsOut, "ENDDAT\n" ) ;
  
  /* Release storage. */
  del_traverse ( &Piter ) ;
  del_tree ( &Ptree ) ;
  for ( nBlock = Pmb->mBlocks ; nBlock > 0 ; nBlock-- )
    free_chunk ( NULL, ppChunk+nBlock ) ;
  arr_free ( ProotChunk ) ;
  arr_free ( cutEdge ) ;

  /* Renumber all marked elements with their incremental 'unstructured' number. */
  for ( nBlock = 1 ; nBlock <= Pmb->mBlocks ; nBlock++ ) {
    Pbl = Pmb->PblockS + nBlock ;
    mElemsMarked = 0 ;
    if ( Pbl->PelemMark ) 
      for ( ijk[2] = ijk[1] = 1 ; ijk[1] < Pbl->mVert[1] ; ijk[1]++ )
	for ( ijk[0] = 1 ; ijk[0] < Pbl->mVert[0] ; ijk[0]++ ) {
	  nCell = get_nElem_ijk ( mDim, ijk, Pbl->mVert ) ;
	  if ( writeKeepNotCut ) {
	    /* Write the kept part. Renumber elemMark to the cell number in the
               block. */
	    if ( Pbl->PelemMark[nCell] )
	      Pbl->PelemMark[nCell] = ++mElemsMarked ;
	    else
	      Pbl->PelemMark[nCell] = 0 ; }
	  else {
	    /* Write the cut part, not the kept part. Flip all elemMarks. */
	    if ( Pbl->PelemMark[nCell] )
	      Pbl->PelemMark[nCell] = 0 ;
	    else
	      Pbl->PelemMark[nCell] = ++mElemsMarked ; }
	}
  }
  return ( 1 ) ;
}

/******************************************************************************

  cutEg2coor:
  Return a unique coordinate value on a 2D edge. In 2D, each edge can at most
  appear twice (branch cuts) and then in opposite directions. Take the unique
  location on the edge as 1/2 epsOverlap from vertex 0 to vertex 1.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

VALU_TYPE *cutEg2coor ( const DATA_TYPE *Pdata ) {
  return ( ( ( cutEdge_s * ) Pdata )->egCoor ) ;
}
