/*
   uns_drvElem.c:
   All kinds of functionalities connected to derived elements, ie.
   elements with hanging nodes derived from standard simplices.

   Last update:
   ------------
   15Oct99; fix bug in loop control for hanging vertices in drvElem_volume.
   7Jun99; deal with 2D in get_surfVx_drvElem.
   5Jun99; change return type of get_drvElem_aE, return 1 on derived element.
   21May99; fix bug in int2elMark due to improper masking.
   13Feb99; intro elMark2int and int2elMark to pack elMark.
   14Jan08; intro fixed diagonals on quad faces, move mark_uns_elemFromVerts here.
   8Aug97; intro Side functions and get_face_norm. Normals now inward and
           multiplied with the dimension as done in AVBP.
   14Jun97; fix bug in get_surfTri_drvElem in the case of a quad with
            two adjacent refined edges.

   This file contains:
   -------------------
   drvSide_volume
   get_drvSide_aE
   drvSide_normls
   drvElem_volume
   get_drvElem_aE
   drvElem_normls
   get_face_norm:
   get_surfTri_drvElem
   add_facet
   get_elMark_aE
   mark_uns_elemFromVerts

   printDrvFc

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

extern char hip_msg[] ;

extern const int verbosity ;
extern const elemType_struct elemType[] ;
extern const int bitEdge[MAX_EDGES_ELEM] ;

/* This macro conveniently returns the proper vertex pointer of a derived element
   out of PPvrtx or PhgVx. */ 
#define PCOOR(kV) ( kV < mV ? pElem->PPvrtx[kV]->Pcoor : PhgVx[ kV-mV ]->Pcoor )

/* Find the appropriate vertex pointer. */
#define PVX(kV,mV,pEl,pHgV) ( ( vrtx_struct * ) \
                              ( kV < mV ? pEl->PPvrtx[kV] : pHgV[kV-mV] ) )


/******************************************************************************

  drvSide_volume:
  Calculate the volume of the side of a derived element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double drvSide_volume ( const uns_s *pUns,
		        const elem_struct *pElem, const int nSide,
		        double volVec[] )
{
  const elemType_struct *PelT ;
  int mV, mDim, nDim, kVxHg[MAX_ADDED_VERTS], kVx, mVxHg,
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1] ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;
  double nodeNorm[MAX_REFINED_VERTS][MAX_DIM], volume, comp ;

  PelT = elemType + pElem->elType ;
  mV = PelT->mVerts ;
  mDim = PelT->mDim ;
  volume = 0. ;

  /* Calulate face normals */
  get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, PhgVx, fixDiag, diagDir ) ;
  drvSide_normls ( pElem, 1, &nSide, mVxHg, kVxHg, PhgVx, fixDiag, diagDir, nodeNorm ) ;
  
  /* Calculate the surface or volume of elem. The length/area is the
     sum of the norms of the surface normals. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    for ( comp = 0., kVx = 0 ; kVx < mV + mVxHg ; kVx++ )
      comp += nodeNorm[kVx][nDim] ;
    volVec[nDim] = comp /= mDim ;
    volume += comp*comp ;
  }
  volume = sqrt( volume )/mDim/mDim ;

  return ( volume ) ;
}

/******************************************************************************

  get_drvSide_aE:
  Given a side of an element, produce an unordered list of vertices around or on that
  using the adaptive edge list.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pUns: needed for the base pointer to the list of adapted edges.
  pElem:
  nFace:

  Changes To:
  -----------
  *PmVxSide: the number of all vertices on the face.
  PvxSide[]; the list of vertices around.
  pFixDiag:  1, if the face carries a fixed diagonal
  pDiagDir: 1, if the diagonal runs opposite to the lowest indexed node on the face.
  
  Returns:x
  --------
  0 on base face, 1 on a derived face.
  
*/

int get_drvSide_aE ( const uns_s *pUns,
		     const elem_struct *pElem, const int nSide,
                     // this upsets gcc 11.3
		     // int *PmVxSide, vrtx_struct *PvxSide[2*MAX_VX_FACE+1],
		     int *PmVxSide, vrtx_struct **PvxSide,
		     int *pFixDiag, int *pDiagDir ) {
  
  int nFcAe[MAX_VX_FACE], nCrossAe[MAX_VX_FACE+1], nFixAe, mVxBase ;
  
  get_face_aE ( pUns, pElem, nSide, &mVxBase, 
                PmVxSide, ( const vrtx_struct ** ) PvxSide,
                nFcAe, nCrossAe, &nFixAe, pDiagDir ) ;
  *pFixDiag = ( nFixAe ? 1 : 0 ) ;

  if ( !pUns->pllAdEdge )
    /* There are no derived elements. */
    return ( 0 ) ;
  else if ( *PmVxSide == mVxBase && !(*pFixDiag) )
    /* Base face. */
    return ( 0 ) ;
  else
    /* Hanging face. */
    return ( 1 ) ;
}


/******************************************************************************

  get_face_norm:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_face_norm ( const elem_struct *pElem, vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		    const int mFacets, const int mFacetVerts[MAX_CHILDS_FACE],
		    int kFacetVx[MAX_CHILDS_FACE][MAX_VX_FACE],
		    double nodeNorm[][MAX_DIM] )
{
  const elemType_struct *PelT = elemType + pElem->elType ;
  /* Note that mV is used in the PCOOR macro. */
  const int mV = PelT->mVerts, mDim = PelT->mDim ;
  int iVert, nDim, kFacet ;
  double fcNorm[MAX_DIM], sfNorm[4][MAX_DIM], *Pcoor[MAX_VX_FACE],
    vecFwrd[MAX_DIM], vecBckw[MAX_DIM], vecDiag[MAX_DIM] ;

  for ( kFacet = 0; kFacet < mFacets ; kFacet++ ) {
    /* Find the coordinates of that facet. */
    for ( iVert = 0 ; iVert < mFacetVerts[kFacet] ; iVert++ )
      Pcoor[iVert] = PCOOR( kFacetVx[kFacet][iVert] ) ;
    
    if ( mDim == 2 ) {
      /* A 2D side is an edge. */
      fcNorm[0] = Pcoor[0][1] - Pcoor[1][1] ;
      fcNorm[1] = Pcoor[1][0] - Pcoor[0][0] ;
      
      /* Scatter the face normal to the forming vertices. */
      for ( nDim  = 0 ; nDim < 2 ; nDim++ )
	for ( iVert = 0 ; iVert < mFacetVerts[kFacet] ; iVert++ )
	  nodeNorm[ kFacetVx[kFacet][iVert] ][nDim] += fcNorm[nDim] ;
    }
    else if ( mFacetVerts[kFacet] == 3 ) {
      /* A triangular face. Calculate the cross product around Pcoor[0].*/
      for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
	vecFwrd[nDim] = Pcoor[1][nDim] - Pcoor[0][nDim] ;
	vecBckw[nDim] = Pcoor[2][nDim] - Pcoor[0][nDim] ;
      }
      cross_prod_dbl ( vecFwrd, vecBckw, 3, fcNorm ) ;
      
      /* Scatter the face normal to the forming vertices. */
      for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
	/* The cross-product gives twice the area of the triangle. */
	fcNorm[nDim] /= -2 ;
	for ( iVert = 0 ; iVert < mFacetVerts[kFacet] ; iVert++ )
	  nodeNorm[ kFacetVx[kFacet][iVert] ][nDim] += fcNorm[nDim] ;
      }
    }
    else if ( mFacetVerts[kFacet] == 4 ) {
      /* A quad face 0,1,2,3. Cut into two triangular faces and average
	 both possible diagonals. */
      
      /*Diagonal 0,2. */
      for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
	vecFwrd[nDim] = Pcoor[1][nDim] - Pcoor[0][nDim] ;
	vecDiag[nDim] = Pcoor[2][nDim] - Pcoor[0][nDim] ;
	vecBckw[nDim] = Pcoor[3][nDim] - Pcoor[0][nDim] ;
      }
      cross_prod_dbl ( vecFwrd, vecDiag, 3, sfNorm[0] ) ;
      cross_prod_dbl ( vecDiag, vecBckw, 3, sfNorm[1] ) ;
      
      /*Diagonal 1,3. */
      for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
	vecFwrd[nDim] = Pcoor[2][nDim] - Pcoor[1][nDim] ;
	vecDiag[nDim] = Pcoor[3][nDim] - Pcoor[1][nDim] ;
	vecBckw[nDim] = Pcoor[0][nDim] - Pcoor[1][nDim] ;
      }
      cross_prod_dbl ( vecFwrd, vecDiag, 3, sfNorm[2] ) ;
      cross_prod_dbl ( vecDiag, vecBckw, 3, sfNorm[3] ) ;
      
      for ( nDim  = 0 ; nDim < 3 ; nDim++ ) {
	/* .5 due to the averaging of the two quads and
	   .5 due to the triangle, which is has half the area
	   of the parallelogram of the cross_product. */
	fcNorm[nDim] = -.25*3/4*( sfNorm[0][nDim] + sfNorm[1][nDim] +
				  sfNorm[2][nDim] + sfNorm[3][nDim] ) ;
	
	/* Scatter the face normal to the forming vertices. */
	for ( iVert = 0 ; iVert < mFacetVerts[kFacet] ; iVert++ )
	  nodeNorm[ kFacetVx[kFacet][iVert] ][nDim] += fcNorm[nDim] ;
      }
    }
    else {
      printf ( " FATAL: cannot deal with a %d-noded face in"
               " get_face_norm.\n", mFacetVerts[kFacet] ) ;
      return ( 0 ) ;
    }
  }
  return ( 1 ) ;
}





/******************************************************************************

  drvSide_normls.c:
  Calculate the averaged node-normals for a number of faces of an element.
  Any derived element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem: the element with the corner vertices etc.
  mVxHg: the number of hanging vertices on the element.
  kVxHg: the positions of each hanging vertex.
  PhgVx: the hanging vertices.

  Changes To:
  -----------
  nodeNorm[][]: the node normals, in the sequence:
                first the corner vertices of the element,
		then the list of vertices as given in PhgVx.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int drvSide_normls ( const elem_struct *pElem, int mSides, const int kSide[],
                     // this upsets gcc 11.3
		     //const int mVxHg, const int kVxHg[MAX_ADDED_VERTS],
		     const int mVxHg, const int *kVxHg,
		     vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		     // int fixDiag[MAX_FACES_ELEM+1], int diagDir[MAX_FACES_ELEM+1],
		     int *fixDiag, int *diagDir,
		     double nodeNorm[][MAX_DIM] ) {
  
  int iSd, kSd, kVert, nDim ;
  const elemType_struct *PelT ;
  int mV ; 
  int mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
             kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  PelT = elemType + pElem->elType ;
  mV = PelT->mVerts ;
  
  /* Get the surface triangulation of that element. */
  if ( !get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
			      mFacets, mFacetVerts, kFacetVx ) ) {
    printf ( " FATAL: could not get the surface triangulation in drvElem_normls.\n" ) ;
    return ( 0 ) ; }

  /* Reset the normals. */
  for ( nDim = 0 ; nDim < PelT->mDim ; nDim++ )
    for ( kVert = 0 ; kVert < mV + mVxHg ; kVert++ )
      nodeNorm[kVert][nDim] = 0. ;
  
  for ( iSd = 0 ; iSd < mSides ; iSd++ ) {
    kSd = kSide[iSd] ;
    get_face_norm ( pElem, PhgVx, mFacets[kSd], mFacetVerts[kSd], kFacetVx[kSd],
		    nodeNorm ) ; }

  return ( 1 ) ;
}



/******************************************************************************

  drvElem_volume:
  Calculate the volume of the derived element.
  
  Last update:
  ------------
  15Oct99; fix bug in loop control for hanging vertices.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

double drvElem_volume ( const uns_s *pUns, const elem_struct *pElem ) {
  
  const elemType_struct *PelT ;
  int mV, mDim, nDim, kVxHg[MAX_ADDED_VERTS], kVx, mVxHg,
    fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1] ;
  vrtx_struct *PhgVx[MAX_ADDED_VERTS] ;
  double nodeNorm[MAX_REFINED_VERTS][MAX_DIM], volume ;

  PelT = elemType + pElem->elType ;
  mV = PelT->mVerts ;
  mDim = PelT->mDim ;
  volume = 0. ;

  /* Calulate face normals */
  get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, PhgVx, fixDiag, diagDir ) ;
  drvElem_normls ( pElem, mVxHg, kVxHg, PhgVx, fixDiag, diagDir, nodeNorm ) ;
  
  /* Calculate the surface or volume of elem. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {

    for ( kVx = 0 ; kVx < mV ; kVx++ )
      volume += pElem->PPvrtx[kVx]->Pcoor[nDim]*nodeNorm[kVx][nDim] ;

    for ( kVx = 0 ; kVx < mVxHg ; kVx++ )
      volume += PhgVx[kVx]->Pcoor[nDim]*nodeNorm[kVx+mV][nDim] ;
  }
  
  return ( -volume/mDim/mDim ) ;
}

/******************************************************************************

  get_drvElem_aE:
  Given an element, produce a canonical list of hanging vertices on that
  element using the adaptive edge list.
  
  Last update:
  ------------
  5Jun99; return 0 for base, 1 for derived element.
  : conceived.
  
  Input:
  ------
  pUns: needed for the base pointer to the list of adapted edges.
  pElem:

  Output:
  -----------
  *PmVxHg: the number of hanging vertices.
  kVxHg[]: the positions of the hanging vertices. [mVerts...mVerts+mEdges-1] are on
           edges, [mVerts+mEdges+1 .... mVerts+mEdges+mFaces] are on faces.
  PhgVx[]: the hanging vertices, listed in order of occurence, that is 0 .. *PmVxHg-1.
  
  Returns:
  --------
  0 on base element, 1 on element with derived content.
  
*/

int get_drvElem_aE ( const uns_s *pUns, const elem_struct *pElem,
		     int *PmVxHg,
                     //		     int kVxHg[MAX_ADDED_VERTS], vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		     //int fixDiag[MAX_FACES_ELEM+1], int diagDir[MAX_FACES_ELEM+1]
		     int *kVxHg, vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		     int *fixDiag, int *diagDir
                     ) {
  
  const elemType_struct *PelT = elemType + pElem->elType ;
  const llEdge_s *pllAe ;
  int nAe, kEdge, kFace, mV, mE, mVxBase, mVxFc, dir,
    nFcAe[MAX_VX_FACE], nCrossAe[MAX_VX_FACE-1], nFixAe, isDerived = 0 ;
  const vrtx_struct *Pvx[2*MAX_VX_FACE+1] ;

  /* Reset hanging edges. */
  for ( kFace = 1 ; kFace <= PelT->mFaces ; kFace++ )
    fixDiag[kFace] = diagDir[kFace] = 0 ;

  if ( !( pllAe = pUns->pllAdEdge ) ) {
    /* There are no derived elements. */
    *PmVxHg = 0 ;
    return ( 0 ) ; }
    
  mV = PelT->mVerts ;
  mE = PelT->mEdges ;
  
  /* Loop over all edges. */
  for ( *PmVxHg = kEdge = 0 ; kEdge < mE ; kEdge++ )
    if ( ( nAe = get_elem_edge ( pllAe, pElem, kEdge, Pvx, Pvx+2, &dir ) ) &&
         ( Pvx[1] = de_cptVx( pUns, pUns->pAdEdge[nAe].cpVxMid ) ) ) {
      /* There is a vertex at midedge. */
      PhgVx[*PmVxHg] = ( vrtx_struct * ) Pvx[1] ;
      kVxHg[*PmVxHg] = mV + kEdge ;
      ++(*PmVxHg) ;
      isDerived = 1 ;
    }
  
  /* Loop over all faces for 3D. */
  for ( kFace = 1 ; kFace <= PelT->mFaces ; kFace++ )
    if ( PelT->faceOfElem[kFace].mVertsFace == 4 ) {
      /* Only quad faces can carry vertices. */
      get_face_aE ( pUns, pElem, kFace,
                    &mVxBase, &mVxFc, Pvx, nFcAe, nCrossAe, &nFixAe, diagDir+kFace ) ;
      
      if ( Pvx[8] ) {
        /* There is a center vertex on the face. */
        PhgVx[*PmVxHg] = ( vrtx_struct * ) Pvx[8] ;
	kVxHg[*PmVxHg] = mV + mE + kFace ;
	++(*PmVxHg) ;
        isDerived = 1 ;
      }

      if ( nFixAe ) { 
        fixDiag[kFace] = 1 ;
        isDerived = 1 ;
      }
    }
  
  return ( isDerived ) ;
}


/******************************************************************************

  drvElem_normls.c:
  Calculate the averaged node-normals for an element. Any derived element.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pElem: the element with the corner vertices etc.
  mVxHg: the number of hanging vertices on the element.
  kVxHg: the positions of each hanging vertex.
  PhgVx: the hanging vertices.
  fixDiag: 1 if the corresponding face has a fixed diagonal.
  diagDir: 1 if the fixed diagonal runs from 1-3 in the listing of pFoE.

  Changes To:
  -----------
  nodeNorm[][]: the node normals, in the sequence:
                first the corner vertices of the element,
		then the list of vertices as given in PhgVx.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int drvElem_normls ( const elem_struct *pElem, const int mVxHg,
		     //const int kVxHg[MAX_ADDED_VERTS],
		     const int *kVxHg,
		     vrtx_struct *PhgVx[MAX_ADDED_VERTS],
		     // int fixDiag[MAX_FACES_ELEM+1], int diagDir[MAX_FACES_ELEM+1],
		     int *fixDiag, int *diagDir,
		     double nodeNorm[][MAX_DIM] ) {
  
  int kSide, kVert, nDim ;
  const elemType_struct *PelT ;
  int mV ; 
  
  int mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ;

  PelT = elemType + pElem->elType ;
  mV = PelT->mVerts ;
  
  /* Get the surface triangulation of that element. */
  if ( !get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
			      mFacets, mFacetVerts, kFacetVx ) ) {
    printf ( " FATAL: could not get the surface triangulation in drvElem_normls.\n" ) ;
    return ( 0 ) ; }

  /* Reset the normals. */
  for ( nDim = 0 ; nDim < PelT->mDim ; nDim++ )
    for ( kVert = 0 ; kVert < mV + mVxHg ; kVert++ )
      nodeNorm[kVert][nDim] = 0. ;
  
  for ( kSide = 1 ; kSide <= PelT->mSides ; kSide++ )
    get_face_norm ( pElem, PhgVx, mFacets[kSide], mFacetVerts[kSide], kFacetVx[kSide],
		    nodeNorm ) ;
  return ( 1 ) ;
}


/******************************************************************************

  add_facet:
  Add a facet to a list of facets composing a face.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  kFace:     the number of the face in the element.
  fcType:    tri or quadr
  kVxFace[]: a consecutive list of vertices ccw around the face including hanging ones.
  kvx0..4:   the position of the three/four forming vertices of the facet in that list.
  *PmFacets: the number of facets for this face so far.

  Changes To:
  -----------
  *PmFacets: incremented.
  mFacetVerts: the number of vertices of the facet.
  kFacetVx:  the forming vertices of the facet.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_facet ( const int mVertsAround, const int mVertsFacet,
                const int kVxFace[MAX_VX_FACE], 
                const int kVx0, const int kVx1, const int kVx2, const int kVx3,
                int *PmFacets, int mFacetVerts[MAX_CHILDS_FACE],
                int kFacetVx[MAX_CHILDS_FACE][MAX_VX_FACE] ) {
  
# ifdef CHECK_BOUNDS
    if ( *PmFacets > MAX_CHILDS_FACE - 1 )
    { printf ( " FATAL: too many facets (%d)in add_facet.\n", *PmFacets ) ;
      return ( 0 ) ;
    }
# endif

  mFacetVerts[ *PmFacets ] = mVertsFacet ;
    
  kFacetVx[ *PmFacets ][0] = kVxFace[ kVx0%mVertsAround ] ;
  kFacetVx[ *PmFacets ][1] = kVxFace[ kVx1%mVertsAround ] ;
  kFacetVx[ *PmFacets ][2] = kVxFace[ kVx2%mVertsAround ] ;

  if ( mVertsFacet == 4 ) {
    /* Quad face. */
    kFacetVx[ *PmFacets ][3] = kVxFace[ kVx3%mVertsAround ] ;
  }
  ++(*PmFacets) ;
  return ( 1 ) ;
}



/******************************************************************************

  get_surfTri_drvElem:
  Find the surface 'triangulation', ie. tris and quads, for a derived element,
  given a list of added vertices.
  
  Last update:
  ------------
  12Nov19; use hip_err.
  13Jan98; add fixed diagonals.
  : conceived.
  
  Input:
  ------
  pElem:   the element with the corner vertices etc.
  mVxHg:   the number of hanging vertices on the element.
  kVxHg:   the positions of each hanging vertex.
  PhgVx:   the hanging vertices.
  fixDiag: a flag for each face if it carries a fixed diagonal.
  diagDir: the direction of the fixed diagonal: 0 meaning that it runs from the
           first vertex listed for the face in faceOfElem.

  Output:
  -----------
  mFacets[]: the number of facets for each face.
  mFacetVerts[][]: the number of vertices for each face, for each facet.
  PvxFacet[][][]: the list of vertices for each face, for each facet.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_surfTri_drvElem ( const elem_struct *pElem, const int mVxHg,
			  const int kVxHg[MAX_ADDED_VERTS],
			  const int fixDiag[MAX_FACES_ELEM+1],
			  const int diagDir[MAX_FACES_ELEM+1],
			  int mFacets[MAX_FACES_ELEM+1],
			  int mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
			  //int kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE]
			  int kFacetVx[][MAX_CHILDS_FACE][MAX_VX_FACE]
                          ){
  
  const elemType_struct *pElT ;
  int mV, mE, mF, iHalf, mHalves ;
  const edgeOfElem_struct *pEoE ;
  const faceOfElem_struct *pFoE ;
  int kFace, iVx, kEdge, iEdge, mRefdEdges, kRefdEdge[MAX_EDGES_FACE], kEg,
             mVxAround, kVxL, kVxR, kVxFace[2*MAX_VX_FACE+1], mVertsFace, kVxDiag ;
  /* The two lists kHgVxEg/Fc list for each edge/face the index of the hanging vertex
     on that face edge, starting from mVerts. */
  int kHgVxEg[MAX_EDGES_ELEM] = {0}, kHgVxFc[MAX_FACES_ELEM+1] = {0} ;
  pElT = elemType + pElem->elType ;
  mV = pElT->mVerts ;
  mE = pElT->mEdges ;
  mF = pElT->mFaces ;


  /* Fill a table of hanging vertices per face and edge. */
  for ( iVx = 0 ; iVx < mVxHg ; iVx++ )
    if ( kVxHg[iVx] < mV ) {
      sprintf ( hip_msg, "hanging node %d in a corner location on a %s"
	       " in get_surfTri_drvElem.", kVxHg[iVx], pElT->name ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    else if ( kVxHg[iVx] < mV + mE )
      /* This is a edge with a hanging vertex. */
      kHgVxEg[ kVxHg[iVx] - mV ] = iVx + mV ;
    else if ( kVxHg[iVx] <= mV + mE + mF )
      /* Note that pElT->mFaces = 0 for 2-D. */
      kHgVxFc[ kVxHg[iVx] - mV - mE ] = iVx + mV ;
    else {
      sprintf ( hip_msg, "impossible location for a hanging node %d in a %s"
	       " in get_surfTri_drvElem.", kVxHg[iVx], pElT->name ) ;
      hip_err ( fatal, 0, hip_msg ) ; }

  if ( pElT->mDim == 2 )
    /* Tris and Quads. Loop over all faces/sides. Sides are non-0 in 2-D.*/
    for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
      /* Number of the edge that forms this face. Note that edges are numbered
	 0...mEdges-1, while faces,sides, alas, are still numbered 1..mFaces. */
      kEdge = pElT->faceOfElem[kFace].kFcEdge[0] ;
      pEoE =  pElT->edgeOfElem + kEdge ;
      if ( pElT->faceOfElem[kFace].edgeDir[0] == 1 ) {
	/* The edge runs in the same direction as the face. */
	kVxL = pEoE->kVxEdge[0] ;
	kVxR = pEoE->kVxEdge[1] ; }
      else {
	/* The edge runs in the other direction as the face. */
	kVxL = pEoE->kVxEdge[1] ;
	kVxR = pEoE->kVxEdge[0] ; }
	
      if ( kHgVxEg[kEdge] ) {
	/* This is a refined edge. */
	mFacets[kFace] = 2 ;
	mFacetVerts[kFace][0] = mFacetVerts[kFace][1] = 2 ;
	kFacetVx[kFace][0][0] = kVxL ;
	kFacetVx[kFace][0][1] = kFacetVx[kFace][1][0] = kHgVxEg[kEdge] ;
	kFacetVx[kFace][1][1] = kVxR ;
      }
      else {
	/* This edge is unrefined. */
	mFacets[kFace] = 1 ;
	mFacetVerts[kFace][0] = 2 ;
	kFacetVx[kFace][0][0] = kVxL ;
	kFacetVx[kFace][0][1] = kVxR ;
      }
    }
  
  else if ( pElT->mDim == 3 )
    for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ ) {
      pFoE = pElT->faceOfElem + kFace ;
      mVxAround = 2*pFoE->mFcEdges ;
      mFacets[kFace] = 0 ;

      /* A quad face can be split into two triangular facets with a fixed diagonal. */
      mHalves = ( pFoE->mVertsFace == 4 && fixDiag[kFace] ? 2 : 1 ) ;
      for ( iHalf = 0 ; iHalf < mHalves ; iHalf++ ) {

	/* Make a consecutive list of vertices ccw around the face, including 
	   hanging ones. Corner vertices are at even numbers, mid-edge vertices
	   at odd ones. */
	mVertsFace = pFoE->mVertsFace ;
	for ( mRefdEdges = iEdge = 0 ; iEdge < pFoE->mFcEdges ; iEdge++ ) {
	  kEdge = pFoE->kFcEdge[iEdge] ;
	  pEoE = pElT->edgeOfElem + kEdge ;
	  if ( pFoE->edgeDir[iEdge] == 1 )
	    /* Edge runs ccw around the face, viewed from outside. */
	    kVxFace[ (2*iEdge+2)%mVxAround ] = pEoE->kVxEdge[1] ;
	  else
	    kVxFace[ (2*iEdge+2)%mVxAround ] = pEoE->kVxEdge[0] ;
	  if ( kHgVxEg[kEdge] ){
	    /* This edge has a hanging node on it. Note that 2*iEdge+1 cannot
	       wrap around 0...7. */
	    kVxFace[ 2*iEdge+1 ] = kHgVxEg[kEdge] ;
	    kRefdEdge[mRefdEdges] = iEdge ;
	    mRefdEdges++ ;
	  }
	  else
	    kVxFace[ 2*iEdge+1 ] = 0 ;
	}
        

	if ( mVertsFace == 4 && fixDiag[kFace] ) {
	  /* Split quad. Repack kVxFace. Find out how the face is split. */
	  kVxDiag = ( diagDir[kFace] ? pFoE->kVxFace[1] : pFoE->kVxFace[0] ) ;
	  if ( kVxDiag == kVxFace[0] || kVxDiag == kVxFace[4] ) {
	    /* Split runs from 0-4. */
	    if ( iHalf ) {
	      /* Consider triangle 0-4-6. */
	      for ( iVx = 2 ; iVx < 6 ; iVx++ )
		kVxFace[iVx] = kVxFace[iVx+2] ;
	      kVxFace[1] = kHgVxFc[kFace] ;
	    }
	    else {
	      /* 0-2-4. */
	      kVxFace[5] = kHgVxFc[kFace] ;
	    }
	  }
	  else {
	    /* Split runs from 2-6. */
	    if ( iHalf ) {
	      /* Consider triangle 2-4-6. */
	      for ( iVx = 0 ; iVx < 5 ; iVx++ )
		kVxFace[iVx] = kVxFace[iVx+2] ;
	      kVxFace[5] = kHgVxFc[kFace] ;
	    }
	    else {
	      /* 0-2-6. */
	      for ( iVx = 4 ; iVx < 6 ; iVx++ )
		kVxFace[iVx] = kVxFace[iVx+2] ;
	      kVxFace[3] = kHgVxFc[kFace] ;
	    }
	  }

          
	  mVertsFace = 3 ;
	  /* Count the number of refined edges. */
	  for ( mRefdEdges = 0, iVx = 1 ; iVx < 7 ; iVx += 2 )
	    if ( kVxFace[iVx] )
	      kRefdEdge[mRefdEdges++] = iVx/2 ;
	}


        /* Now go about and just view a simple tri or quad face. */
	if ( mVertsFace == 3 ) {
	  if ( mRefdEdges == 0 ) {
	    /* Just the original triangular face. */
	    add_facet ( 6, 3, kVxFace, 0,2,4,0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 1 ) {
	    /* Two tris. Find the position of the hanging vertex.*/
	    kEg = 2*kRefdEdge[0]+1 ;
	    add_facet ( 6, 3, kVxFace, kEg, kEg+3, kEg+5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 6, 3, kVxFace, kEg, kEg+1, kEg+3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 2 ) {
	    /* A tri and a quad. Find the position of the unrefined vertex.*/
	    for ( kEg = 1 ; kEg <=5 ; kEg += 2 )
	      if ( !kVxFace[kEg] )
		break ;
	  
	    add_facet ( 6, 4, kVxFace, kEg+5, kEg+1, kEg+2, kEg+4,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 6, 3, kVxFace, kEg+4, kEg+2, kEg+3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 3 ) {
	    /* Four tris. */
	    add_facet ( 6, 3, kVxFace, 0, 1, 5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 6, 3, kVxFace, 5, 1, 3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 6, 3, kVxFace, 1, 2, 3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 6, 3, kVxFace, 5, 3, 4, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else {
	    printf ( " FATAL: can't deal with %d hanging edges on a triangular face"
		     " in get_surfTri_drvElem.\n", mRefdEdges ) ;
	    return ( 0 ) ; }
	}
      
	else if ( mVertsFace == 4 ) {
	  if ( mRefdEdges == 0 ) {
	    /* Just the original quad face. */
	    add_facet ( 8, 4, kVxFace, 0, 2, 4, 6,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 1 && !kHgVxFc[kFace] ) {
	    /* 3 tris. Find the position of the hanging vertex.*/
	    kEg = 2*kRefdEdge[0]+1 ;
	    add_facet ( 8, 3, kVxFace, kEg+7, kEg, kEg+5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg, kEg+1, kEg+3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg+5, kEg, kEg+3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 3 && kHgVxFc[kFace] ) {
	    /* Three tris and two quads. Note that due to the center vertex the
	       list is not circular. Find the position of the unrefined vertex. */
	    kVxFace[8] = kHgVxFc[kFace] ;
	    for ( kEg = 1 ; kEg <=7 ; kEg += 2 )
	      if ( !kVxFace[kEg] )
		break ;

	    add_facet ( 9, 3, kVxFace, (kEg+6)%8, (kEg+7)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+7)%8, (kEg+1)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+1)%8, (kEg+2)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;

	    add_facet ( 9, 4, kVxFace, (kEg+2)%8, (kEg+3)%8, (kEg+4)%8, 8,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 4, kVxFace, (kEg+4)%8, (kEg+5)%8, (kEg+6)%8, 8,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 3 ) {
	    /* 3 tris and one quad. Find the position of the unrefined vertex. */
	    for ( kEg = 1 ; kEg <=7 ; kEg += 2 )
	      if ( !kVxFace[kEg] )
		break ;
	  
	    add_facet ( 8, 4, kVxFace, kEg+7, kEg+1, kEg+2, kEg+6,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg+6, kEg+2, kEg+4, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg+6, kEg+4, kEg+5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg+4, kEg+2, kEg+3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 4 && kHgVxFc[kFace] ) {
	    /* Fully refined quad. Append the center vertex as a ninth one to the list.*/
	    kVxFace[8] = kHgVxFc[kFace] ;
	    add_facet ( 9, 4, kVxFace, 0, 1, 8, 7,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 4, kVxFace, 1, 2, 3, 8,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 4, kVxFace, 8, 3, 4, 5,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 4, kVxFace, 7, 8, 5, 6,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( mRefdEdges == 4 ) {
	    /* 4 tris in the corners and one large quad in the center. Number the
	       children with first the tris in the sequence of their corner vertices,
	       at the end the quad.*/
	    add_facet ( 8, 3, kVxFace, 0, 1, 7, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, 2, 3, 1, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, 4, 5, 3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, 6, 7, 5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 4, kVxFace, 1, 3, 5, 7,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( kRefdEdge[1] - kRefdEdge[0] == 2 && kHgVxFc[kFace] ) {
	    /* Two opposite edges are hanging, and there is a center
	       vertex. Six tris. Note that due to the center vertex the
	       list is not circular. */
	    kVxFace[8] = kHgVxFc[kFace] ;
	    kEg = 2*kRefdEdge[0]+1 ;
	    add_facet ( 9, 3, kVxFace, (kEg+0)%8, (kEg+1)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+1)%8, (kEg+3)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+3)%8, (kEg+4)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+4)%8, (kEg+5)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+5)%8, (kEg+7)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 9, 3, kVxFace, (kEg+7)%8, (kEg+0)%8, 8, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( kRefdEdge[1] - kRefdEdge[0] == 2 ) {
	    /* Two opposite edges are hanging, two quads. */
	    kEg = 2*kRefdEdge[0]+1 ;
	    add_facet ( 8, 4, kVxFace, kEg+0, kEg+4, kEg+5, kEg+7,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 4, kVxFace, kEg+0, kEg+1, kEg+3, kEg+4,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else if ( ( kRefdEdge[1] - kRefdEdge[0] == 1                    ) ||
		    ( kRefdEdge[1] - kRefdEdge[0] == 3 && !kHgVxFc[kFace] ) ) {
	    /* Two adjacent edges are hanging, four tris. */
	    if ( kRefdEdge[1] - kRefdEdge[0] == 1 )
	      kEg = 2*kRefdEdge[0]+1 ;
	    else
	      kEg = 2*kRefdEdge[1]+1 ;
	    add_facet ( 8, 3, kVxFace, kEg+7, kEg+0, kEg+5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg, kEg+1, kEg+2, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg, kEg+2, kEg+5, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	    add_facet ( 8, 3, kVxFace, kEg+5, kEg+2, kEg+3, 0,
			mFacets+kFace, mFacetVerts[kFace], kFacetVx[kFace] ) ;
	  }
	  else {
	    printf ( " FATAL: can't deal with %d hanging edges on a quad face"
		     " in get_surfTri_drvElem.\n", mRefdEdges ) ;
	    return ( 0 ) ; }
	}
      }
    }
  return ( 1 ) ;
}


/******************************************************************************

  get_surfTri_drvElem:
  Given a canonical surface 'triangulation', ie. tris and quads, for a derived element,
  find the list of vertex pointers on the surface. Take care of collapses. If
  collapses occur, the number of vertices on a face can decrease. If facets collapse
  to a line, the list of facets per face is rearranged and the count mFacets adjusted.
  
  Last update:
  ------------
  7Jun99; deal with 2D degeneracies properly.
  20Nov98; derived from get_surfTri_drvElem.
  
  Input:
  ------
  pElem:           the element with the corner vertices etc.
  pHgVx:           the list of hanging vertices on the face.
  mFacets[]:       the number of facets for each face.
  mFacetVerts[][]: the number of vertices for each face, for each facet.
  kFacetVx[][][]:  the list of canonical vertex positions for each face, for each facet.

  Changes To:
  -----------
  mFacets[]:       the number of facets for each face.
  mFacetVerts[][]: the number of vertices for each face, for each facet.
  kFacetVx[][][]:  the list of canonical vertex positions for each face, for each facet.
  pFacetVx[][][]:  the list of vertices for each face, for each facet.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_surfVx_drvElem ( const elem_struct *pElem, vrtx_struct *pHgVx[],
                         int mFacets[MAX_FACES_ELEM+1],
                         int mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
                         int kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE],
                         vrtx_struct
                         *pFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE] ) {
  
  const elemType_struct *pElT = elemType + pElem->elType ;
  const int mVx = pElT->mVerts, mDim = pElT->mDim ;
  int kFace, kFct, kkFct, kVx, kkVx, mVxFct ;
  vrtx_struct **ppVxFct, **ppVxFct2 ;
  

  /* Loop over all facets. */
  for ( kFace = 1 ; kFace <= pElT->mSides ; kFace++ ) {
    for ( kFct = 0 ; kFct < mFacets[kFace] ; kFct++ ) {
      ppVxFct = pFacetVx[kFace][kFct] ;

      /* Make a list of vertices that form this facet. */
      mVxFct = mFacetVerts[kFace][kFct] ;
      for ( kVx = 0 ; kVx < mVxFct ; kVx++ )
        ppVxFct[kVx] = PVX( kFacetVx[kFace][kFct][kVx], mVx, pElem, pHgVx ) ;

      /* Find duplicates. */
      for ( kVx = 0 ; kVx < mVxFct ; kVx++ )
        if ( ppVxFct[kVx] == ppVxFct[ (kVx+1)%mFacetVerts[kFace][kFct] ] ) {
          /* There is a duplicate. Copy the remaining vertices one down. */
          for ( kkVx = kVx+1 ; kkVx < mVxFct-1 ; kkVx++ )
            ppVxFct[kkVx] = ppVxFct[kkVx+1] ;
          mVxFct = --mFacetVerts[kFace][kFct] ;
          kVx-- ;
        }
    }

    /* If facets have disappeared, i.e. less than two/three forming vertices,
       remove them from the list and repack. */
    for ( kFct = 0 ; kFct < mFacets[kFace] ; kFct++ )
      if ( mFacetVerts[kFace][kFct] < mDim ) {
        /* Copy all remaining facets one position down. */
        for ( kkFct = kFct ; kkFct < mFacets[kFace]-1 ; kkFct++ ) {
          ppVxFct = pFacetVx[kFace][kkFct] ;
          ppVxFct2 = pFacetVx[kFace][kkFct+1] ;
          mVxFct = mFacetVerts[kFace][kkFct+1] ;
          for ( kVx = 0 ; kVx < mVxFct ; kVx++ )
            ppVxFct[kVx] = ppVxFct2[kVx] ;
        }
        mFacets[kFace]-- ;
        kFct-- ;
      }
  }

  return ( 1 ) ;
}
/******************************************************************************

  get_elMark_aE:
  Convert a list of vertices on edges and faces to a binary elem Mark.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------

  Changes To:
  -----------
  
  Returns:
  --------
  PelMark:
  
*/

elMark_s get_elMark_aE ( const uns_s *pUns, const elem_struct *pElem,
			 int *PmVxHg ) {
  
  const elemType_struct *PelT = elemType + pElem->elType ;
  const llEdge_s *pllAe = pUns->pllAdEdge ;
  const vrtx_struct *Pvx[2*MAX_VX_FACE+1], *Pvx0, *Pvx1 ;
  int nAe, dir, kEdge, kFace, mVxBase, mVxFc, diagDir, nFcAe[MAX_VX_FACE],
    nCrossAe[MAX_VX_FACE-1], nFixAe ;
  elMark_s elMark ;

  elMark.elType = pElem->elType ;
  elMark.hgEdge = elMark.collEdge = elMark.hgFace = elMark.fixDiag = elMark.diagDir = 0 ;
  *PmVxHg = 0 ;
  
  /* Loop over all edges. */
  for ( kEdge = 0 ; kEdge < PelT->mEdges ; kEdge++ )
    if ( ( nAe = get_elem_edge ( pllAe, pElem, kEdge, &Pvx0, &Pvx1, &dir ) ) )
      if ( pUns->pAdEdge[nAe].cpVxMid.nr ) {
        elMark.hgEdge |= bitEdge[kEdge] ;
	(*PmVxHg)++ ; }

  /* Loop over all faces for 3D. */
  for ( kFace = 1 ; kFace <= PelT->mFaces ; kFace++ )
    if  ( PelT->faceOfElem[kFace].mVertsFace == 4 ) {
      /* Only quad faces can carry a hanging vertex. */
      get_face_aE ( pUns, pElem, kFace,
                    &mVxBase, &mVxFc, Pvx, nFcAe, nCrossAe, &nFixAe, &diagDir ) ;
      if ( Pvx[8] ) {
	elMark.hgFace |= bitEdge[kFace] ;
	(*PmVxHg)++ ; }

      else if ( nFixAe ) {
	elMark.fixDiag |= bitEdge[kFace] ;
	elMark.diagDir |= ( diagDir? bitEdge[kFace] : 0 ) ;
      }
    }
      
  return ( elMark ) ;
}

/******************************************************************************

  elMark2int:
  Given an elMark, pack it into an int[2]. Treat each word separately, fill
  the highest bits with the rightmost parts of elMark.
  
  Last update:
  ------------
  21May99; fix bug in int2elMark due to improper masking.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void elMark2int ( const elMark_s elMark, unsigned int elInt[2] ) {
  
  unsigned int *pElIntHi = elInt, *pElIntLo = elInt+1 ;

  /* First 4 bytes. */
  *pElIntHi  = 0 ;             
  *pElIntHi <<= 12 ; *pElIntHi |= elMark.collEdge ;
  *pElIntHi <<= 12 ; *pElIntHi |= elMark.hgEdge ;
  *pElIntHi <<= 4  ; *pElIntHi |= elMark.elType ; 

  /* Lower 4 bytes. */
  *pElIntLo  = 0 ; 
  *pElIntLo <<= 7 ; *pElIntLo |= elMark.diagDir ;
  *pElIntLo <<= 7 ; *pElIntLo |= elMark.fixDiag ;
  *pElIntLo <<= 7 ; *pElIntLo |= elMark.hgFace  ; 
    
  return ;
}

elMark_s int2elMark ( unsigned int elInt[2] ) {

  elMark_s elMark ;
  unsigned int elIntHi = elInt[0], elIntLo = elInt[1] ;
  
  /* First 4 bytes. */
  elMark.elType   = ( elType_e )
                  ( elIntHi & 15 ) ; elIntHi >>= 4 ;
  elMark.hgEdge   = elIntHi & 8191 ; elIntHi >>= 12 ;
  elMark.collEdge = elIntHi & 8191 ; elIntHi >>= 12 ;
  elMark.dummy0   = 0 ;

  /* Lower 4 bytes. */
  elMark.hgFace   = elIntLo & 255  ; elIntLo >>= 7 ;
  elMark.fixDiag  = elIntLo & 255  ; elIntLo >>= 7 ;
  elMark.diagDir  = elIntLo & 255  ; elIntLo >>= 7 ;
  elMark.dummy1   = 0 ;
    
  return ( elMark ) ;
}


/******************************************************************************/
#ifdef DEBUG

void printDrvFc ( const uns_s *pUns, const elem_struct *pElem, int kFace )
{
  int mVxHg, kFct, kVx, mVx, mVxFct,
    kVxHg[MAX_ADDED_VERTS], fixDiag[MAX_FACES_ELEM+1], diagDir[MAX_FACES_ELEM+1],
    mFacets[MAX_FACES_ELEM+1], mFacetVerts[MAX_FACES_ELEM+1][MAX_CHILDS_FACE],
    kFacetVx[MAX_FACES_ELEM+1][MAX_CHILDS_FACE][MAX_VX_FACE], nr ;
  vrtx_struct *pHgVx[MAX_ADDED_VERTS] ;

    /* Get the surface triangulation. */
  get_drvElem_aE ( pUns, pElem, &mVxHg, kVxHg, pHgVx, fixDiag, diagDir ) ;
  get_surfTri_drvElem ( pElem, mVxHg, kVxHg, fixDiag, diagDir,
                        mFacets, mFacetVerts, kFacetVx ) ;

  mVx = elemType[pElem->elType].mVerts ;
  for ( kFct = 0 ; kFct < mFacets[kFace] ; kFct++ ) {
    /* Make a list of vertices that form this facet. */
    mVxFct = mFacetVerts[kFace][kFct] ;
    printf ( " facet %d/%d, mVx %d:", kFct, kFace, mVxFct ) ;
    for ( kVx = 0 ; kVx < mVxFct ; kVx++ ) {
      nr = PVX( kFacetVx[kFace][kFct][kVx], mVx, pElem, pHgVx )->number ;
      printf ( " %d,", nr ) ;
    }
    printf ( "\n" ) ;
  }
  
  return ;
}

#endif
