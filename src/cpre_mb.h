/*
   cpre_mb.h:
   Structured grid stuff. 
   
   Last update:
   4Apr09; use varList instead of varTypeS.
   24Apr98; move typedefs here.
   6May96: cut out of cpre.h.
*/


struct _subFace_struct
{
  int nr ;
  char name[TEXT_LEN] ;                     /* cgns identifies by label. */

  struct _block_struct *PlBlock ;           /* Left block number. */
  int llLBlockFile[MAX_DIM] ;               /* Indices of the subfaces in the left */
  int urLBlockFile[MAX_DIM] ;               /* block, 1:1, as given by the .doc file. */
  int llLBlock[MAX_DIM] ;                   /* Indices of the subfaces in the left */
  int urLBlock[MAX_DIM] ;                   /* block, coarsened. */

  struct _block_struct *PrBlock ;           /* Right block number. */
  int llRBlockFile[MAX_DIM] ;               /* Indices of the subfaces in the right */
  int urRBlockFile[MAX_DIM] ;               /* block. */
  int llRBlock[MAX_DIM] ;                   /* Indices of the subfaces in the right */
  int urRBlock[MAX_DIM] ;                   /* block, coarsened. */

  int matchVertRBlock[MAX_DIM] ;            /* Matching point in the right block. */
  int matchVertLBlock[MAX_DIM] ;            /* Matching point in the left block. */
  struct _rotation_struct *ProtL2R ;        
  int vertShift[MAX_DIM] ;                  /* Index shift for vertices. */
  int elemShift[MAX_DIM] ;                  /* Index shift for cells. */

  struct _bc_struct *Pbc ;                  /* Boundary condition, if any. */
  
  struct _subFace_struct *PprvBcSubFc ;     /* Linked list of boundary patches. */
  struct _subFace_struct *PnxtBcSubFc ;     /* of the same boundary condition. */
} ;

struct _rotation_struct
{
  char rotChar[2*MAX_DIM + 1] ;             /* String indicating transformation
					       from ijk to e.g. " i-k j\n" */
  int rotMatrix[MAX_DIM][MAX_DIM] ;         /* Rotation matrix, left to right.
					       The rotation matrix operates Ax=y,
					       with x on the left and y on the right,
					       thus read A as A[to][from], and
					       the transformed left system vectors
					       are the columns of A. */
} ;


/* Sweep direction in a block. */
typedef enum { noDir, up, down } direction_enum ;
struct _blkDir_struct
{
  int nDim ;
  direction_enum dir ;
} ;



struct _block_struct {
  int nr ;                                 /* A number to identify this block. */
  char name[TEXT_LEN] ;                    /* cgns identifies blocks by name. */
  mb_struct *PmbRoot ;                     /* Identifity the grid. */
  
  int mVertFile[MAX_DIM] ;                 /* Original number of vertices in each
					      direction, as given by the file and as
					      referred to by the subface indices. */
  int mVert[MAX_DIM] ;                     /* Actual number of vertices in each
					      direction, after coarsening. */
  int skip ;                               /* Coarsening ratio, take every skip
					      vertex in each direction. */
  
  int mVertsBlock ;                        /* Total number of vertices in the block. */
  double *Pcoor ;                          /* Coordinates. */

  int mVertsMarked ;                       /* Number of marked vertices. */
  double *PdblMark ;                       /* Pointer to a double marker field for
					      the vertices. */
  int *PintMark ;                          /* Pointer to a integer marker field. */
  double *Punknown ;

  int mElemsBlock ;
  int mElemsMarked ;                       /* Number of marked elements. */
  int *PelemMark ;                         /* Pointer to an integer marker field for
					      the elements. */
  
  int mSubFaces ;                          /* Number of subfaces. */
  struct _subFace_struct **PPsubFaces ;    /* Subfaces. */
  
  double llBox[MAX_DIM] ;                  /* Bounding box of the block. */
  double urBox[MAX_DIM] ;

  double hMin ;
  double hMax ;
} ;



struct _mb_struct {
  int mBlocks ;                            /* Number of blocks. */
  block_struct *PblockS ;                  /* Array of block_structs. */
  int mDim ;                               /* Spatial dimension of this block. */
  int mUnknowns ;                          /* Number of unknowns of each vertex. */

  int mVerts ;
  int mElems ;
  
  int mSubFaces ;                          /* Number of subfaces. */
  subFace_struct *subFaceS ;               /* Storage for the subfaces. */

  varList_s varList ;                      /* # of unknowns, type, freestream values. */
} ;
