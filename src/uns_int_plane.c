/*
  uns_cut_line.c:
  Find the intersections of a given line with all the faces of the grid
  and interpolat the values on these faces.

  Last update:
  ------------
  8Apr13; use dereferenced pointer in sizeof when allocating.
  Oct00; conceived.

  This file contains:
  -------------------

*/

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"

extern const int verbosity ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;

/* List of elem to cut vertex pointers. */
int mCutElemsAlloc ;
int mCutElems ;
int mConnEntries ;
int *pCutEl ;
/* List of coordinates, unknowns per cut vertex. */
int mCutVxAlloc ;
int mCutVx ;
double *pCutCo ;
/* Solution. */
int mUnknowns ;
double *pCutUn ;

/******************************************************************************

  make_cut:
  Alloc a list of cells and vertices to be converted to a boundary-less
  grid later.
  
  Last update:
  ------------
  4Apr09; use varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int make_cut ( uns_s *pUns) {

  mCutElemsAlloc = 100 ;
  mCutVxAlloc = 4*mCutElemsAlloc ;

  mCutElems = mCutVx = 0 ;
  mUnknowns = pUns->varList.mUnknowns ;

  pCutEl = arr_calloc ( "make_cut: pCutEl", pUns->pFam, 
                        4*mCutElemsAlloc, sizeof( *pCutEl ) ) ;
  pCutCo = arr_malloc ( "make_cut: pCutCo", pUns->pFam, 
                        3*mCutVxAlloc, sizeof( *pCutCo ) ) ;
  pCutUn = arr_malloc ( "make_cut: pCutUn", pUns->pFam, 
                        mUnknowns*mCutVxAlloc, sizeof( *pCutUn ) ) ;

  return ( 1 ) ;
}

/******************************************************************************

  duplicate_plane:
  Make a copy of the intersection plane and shift it slightly in the normal
  direction.
  
  Last update:
  ------------
  16Sep04: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int duplicate_plane ( uns_s *pUns, const double z[] ) {

  const int mVx3 = 3*mCutVx, mVxU = mUnknowns*mCutVx ; 

  int n, k ;
  double dN[MAX_DIM], *pCF, *pCR, *pUF, *pUR ;

  pCutCo = arr_realloc ( "make_cut: pCutCo", pUns->pFam, 
                         pCutCo, 3*(2*mCutVx+1), sizeof( *pCutCo ) ) ;
  pCutUn = arr_realloc ( "make_cut: pCutUn", pUns->pFam, 
                         pCutUn, mUnknowns*(2*mCutVx+1), sizeof( *pCutUn ) ) ;

  /* Displacement. */
  dN[0] = .1*z[0]*Grids.epsOverlap ;
  dN[1] = .1*z[1]*Grids.epsOverlap ;
  dN[2] = .1*z[2]*Grids.epsOverlap ;


  /* Duplicate nodes. */
  for ( n = 1 ; n <= mCutVx ; n++ ) {
    pCF = pCutCo + n*3 ;
    pCR = pCF + mVx3 ;

    pUF = pCutUn + n*mUnknowns ;
    pUR = pUF + mVxU ;

    pCR[0] = pCF[0] + dN[0] ;
    pCR[1] = pCF[1] + dN[1] ;
    pCR[2] = pCF[2] + dN[2] ;

    for ( k = 0 ; k < mUnknowns ; k++ )
      pUR[k] = pUF[k] ;
  }

  return ( 1 ) ;
}



/******************************************************************************

  make_cut_grid:
  Take the cut and turn it into a grid w/o boundary info.
  
  Last update:
  ------------
  4Apr09; replace varTypeS with varList.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int make_cut_grid ( uns_s *pUns, const double z[] ) {

  grid_struct *pGrid ;
  uns_s *pUnsC ;
  chunk_struct *pChunk ;
  elem_struct *pElem ;
  vrtx_struct *pVrtx, **ppVrtx, *pVx ;
  int nElem, mVerts, *pCEl, nVrtx ;
  double *pCoor, *pUnknown ;

  /* Allocate a chunk. */
  if ( !( pUnsC = make_uns ( NULL ) ) )
    hip_err ( fatal, 0, "failed to alloc a new unstructured grid"
              " in make_cut_grid." ) ;

  /* Copy solution info across. */
  pUnsC->restart = pUns->restart ;


  /* Copy the plane and shift it slightly for 3D elements. */
  duplicate_plane ( pUnsC, z ) ;


  /* Set mDim to 0 to avoid allocation of coordinates. */
  pUnsC->mDim = 0 ;
  if ( !( pChunk = append_chunk( pUnsC, pUnsC->mDim, mCutElems, 2*mConnEntries, 0,
                                 2*mCutVx, 0, 0 ) ) ) {
    printf ( " FATAL in make_cut_grid: could not allocate the  connectivity, vertex,"
	     " coordinate or boundary space.\n" ) ;
    return ( 0 ) ; }
    

  pUnsC->mDim = 3 ;
  pUnsC->varList = pUns->varList ;
  pUnsC->specialTopo = noBc ;


  
  /* Vertices. */
  if ( verbosity > 2 )
    printf ( "   Number of vertices in the cut plane:           %-d\n", mCutVx ) ;
  pChunk->mVerts = pChunk->mVertsNumbered = 2*mCutVx ;


  /* Vertices. Note: Pcoor starts
     with index 1, ie. the first set of nDim coordinates skipped. */	
  pCoor = pChunk->Pcoor = pCutCo ;
  pUnknown = pChunk->Punknown = pCutUn ; 
  pVx = pChunk->Pvrtx ;
  for ( nVrtx = 1 ; nVrtx <= 2*mCutVx ; nVrtx++ ) {
    pVx++ ;
    pCoor += 3 ;
    pUnknown += mUnknowns ;

    pVx->Pcoor = pCoor ;
    pVx->Punknown = pUnknown ;
    pVx->number = nVrtx ;
  }


  /* Copy elems into the main datastructure. */
  if ( verbosity > 2 )
    printf ( "   Number of elements in the cut plane:           %-d\n", mCutElems ) ;
  


  pElem = pChunk->Pelem ;
  ppVrtx = pChunk->PPvrtx ;
  pVrtx = pChunk->Pvrtx ;

  reset_elems ( pElem+1, mCutElems ) ;

  for ( nElem = 1 ; nElem <= mCutElems ; nElem++ ) {

    pElem++ ;
    pElem->PPvrtx = ppVrtx ;
    pElem->number = nElem ;

    pCEl = pCutEl + 4*nElem ;
    if ( pCEl[3] == -1 ) {
      pElem->elType = pri ;
      mVerts = 6 ;
      ppVrtx[0] = pVrtx + pCEl[0] ;
      ppVrtx[3] = pVrtx + pCEl[1] ;
      ppVrtx[5] = pVrtx + pCEl[2] ;
      ppVrtx[1] = pVrtx + pCEl[0]+mCutVx ;
      ppVrtx[2] = pVrtx + pCEl[1]+mCutVx ;
      ppVrtx[4] = pVrtx + pCEl[2]+mCutVx ;

      if ( get_elem_vol ( pElem ) < 0. ) {
        /* Negative volume, swap the connectivity. */
        ppVrtx[5] = pVrtx + pCEl[1] ;
        ppVrtx[3] = pVrtx + pCEl[2] ;
        ppVrtx[4] = pVrtx + pCEl[1]+mCutVx ;
        ppVrtx[2] = pVrtx + pCEl[2]+mCutVx ;
      }        
    }
    else {
      pElem->elType = hex ;
      mVerts = 8 ;
      ppVrtx[0] = pVrtx + pCEl[0] ;
      ppVrtx[1] = pVrtx + pCEl[1] ;
      ppVrtx[2] = pVrtx + pCEl[2] ;
      ppVrtx[3] = pVrtx + pCEl[3] ;
      ppVrtx[4] = pVrtx + pCEl[0]+mCutVx ;
      ppVrtx[5] = pVrtx + pCEl[1]+mCutVx ;
      ppVrtx[6] = pVrtx + pCEl[2]+mCutVx ;
      ppVrtx[7] = pVrtx + pCEl[3]+mCutVx ;

      if ( get_elem_vol ( pElem ) < 0. ) {
        /* Negative volume, swap the connectivity. */
        ppVrtx[3] = pVrtx + pCEl[1] ;
        ppVrtx[1] = pVrtx + pCEl[3] ;
        ppVrtx[7] = pVrtx + pCEl[1]+mCutVx ;
        ppVrtx[5] = pVrtx + pCEl[3]+mCutVx ;
      }        
    }
    ppVrtx += mVerts ;
  }

  pChunk->mElems =  pChunk->mElemsNumbered = mCutElems ;
  pChunk->mElem2VertP = mConnEntries ; 










  
  /* Unstructured. Make a new grid. */
  pGrid = make_grid () ;

  /* Put the chunk into Grids. */
  pGrid->uns.type = uns ;
  pGrid->uns.pUns = pUnsC ;
  pGrid->uns.mDim = 3 ;
  pGrid->uns.pVarList = &(pUns->varList) ;
  pUns->nr = pGrid->uns.nr ;
  pUns->pGrid = pGrid ;
  
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  



  /* Get the containing rectangle for the grid. */
  get_uns_box ( pUnsC ) ;
  /* Count all boundary faces, link all boundary patches,
     since check_conn might have removed some. */
  pUnsC->numberedType = invNum ;
  number_uns_grid ( pUnsC ) ;


  Grids.epsOverlap = 0. ;
  Grids.epsOverlapSq = 0. ;

  return ( 1 ) ;
}


/******************************************************************************

  add_cut_vx: 
  Add all vertices on cut edges and nodal intersections with the plane for an element.
  
  Last update:
  ------------
  14Sep04; recode, cleanup.
  3Sep3: conceived.
  
  Input:
  ------
  pUns
  pElem
  dist = signed distance for each elemental vertex to the plane
  adist = abs value of dist, set to zero for all vertices considered in the plane.

  Changes To:
  -----------
  mCutVx/mCutVxAlloc
  pCutCo
  pCutUn

  nVxEg[]: for each edge the number of the new intersection vertex on it, 0 otherw.
  nVxVx[]: for each vertex the number of the corresp. intersection vertex, 0 otherw.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int add_cut_vx ( const uns_s *pUns, const elem_struct *pElem, 
                        const double dist[], const double adist[], 
                        int *pmVxAll, int nVxAll[],
                        int *pmVxEg, int nVxEg[], int *pmVxVx, int nVxVx[], 
                        int *pnVxCtr ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;
  const edgeOfElem_struct *pEoE ;
  const vrtx_struct *pVx0, *pVx1, **ppVx = (const vrtx_struct **) pElem->PPvrtx ;
  const double *pCo0, *pCo1, *pUn0, *pUn1 ;

  double wt0, wt1, *pCCo, *pCUn, coCtr[MAX_DIM], 
    unCtr[MAX_UNKNOWNS], d0, d1 ;
  int kVx, k, m, mEg, kEg, kVx0, kVx1, mInt, kFc,
    kFcInPlane, mVxFc, maxNewVx ;

  maxNewVx = MAX_EDGES_ELEM+MAX_VX_ELEM+1 ;
  if ( mCutVx + maxNewVx >= mCutVxAlloc ) {
    /* Realloc Coor. For compatibility with Pcoor start numbering at 1. 
       Make sure there is extra room for a possible center node. */
    m = mCutVxAlloc ;
    m *= 1.3 ;
    m++ ;
    mCutVxAlloc = MAX( mCutVx + maxNewVx, m ) ;

    pCutCo = arr_realloc ( "make_cut: pCutCo", pUns->pFam, 
                           pCutCo, 3*(mCutVxAlloc+1), sizeof( *pCutCo ) ) ;
    pCutUn = arr_realloc ( "make_cut: pCutUn", pUns->pFam, 
                           pCutUn, mUnknowns*(mCutVxAlloc+1), sizeof( *pCutUn ) ) ;
  }



  /* Reset the averaged values for a possible centre node. */
  for ( k = 0 ; k < 3 ; k++ )
    coCtr[k] = 0. ;
  for ( k = 0 ; k < mUnknowns ; k++ )
    unCtr[k] = 0. ;



  /* Add all elemental vertices in the plane. */
  *pmVxVx = 0 ;
  *pmVxAll = 0 ;
  for ( kVx = 0 ; kVx < pElT->mVerts ; kVx++ )
    if ( adist[kVx] == 0. ) {
      mCutVx++ ;
      (*pmVxVx)++ ;
      nVxVx[kVx] = mCutVx ;
      nVxAll[ (*pmVxAll)++ ] = mCutVx ;

      pVx0 = ppVx[kVx] ;

      /* Coordinates. */
      pCCo = pCutCo + 3*mCutVx ;
      pCo0  = pVx0->Pcoor ;
      for ( k = 0 ; k < 3 ; k++ ) {
        pCCo[k] = pCo0[k]  ;
        coCtr[k] += pCCo[k] ;
      }

      /* Unknowns. */
      pCUn = pCutUn + mUnknowns*mCutVx ;
      pUn0  = pVx0->Punknown ;
      for ( k = 0 ; k < mUnknowns ; k++ ) {
        pCUn[k] = pUn0[k] ;
        unCtr[k] += pCUn[k] ;
      }
    }
    else 
      nVxVx[kVx] = 0 ;


  /* Build facets of an internal cut from the edges. Loop
     over all edges and flag the cut ones. */
  mEg = pElT->mEdges ;
  *pmVxEg = 0 ;
  for ( kEg = 0 ; kEg < mEg ; kEg++ ) {
    pEoE = pElT->edgeOfElem + kEg ;
    kVx0 = pEoE->kVxEdge[0] ;
    kVx1 = pEoE->kVxEdge[1] ;

    nVxEg[kEg] = 0 ;

    if ( dist[kVx0]*dist[kVx1] <= 0 ) {
      d0 =  adist[kVx0] ;
      d1 =  adist[kVx1] ;

      if ( !( nVxVx[kVx0] || nVxVx[kVx1] ) ) {
        /* Edge intersects the plane but not at the endpoints. */
        mCutVx++ ;
        (*pmVxEg)++ ;
        nVxEg[kEg] = mCutVx ;
        nVxAll[ (*pmVxAll)++ ] = mCutVx ;

        wt0 = d1/(d0+d1) ;
        wt1 = 1.-wt0 ;

        pVx0 = ppVx[kVx0] ;
        pVx1 = ppVx[kVx1] ;

        /* Coordinates. */
        pCCo = pCutCo + 3*mCutVx ;
        pCo0  = pVx0->Pcoor ;
        pCo1  = pVx1->Pcoor ;
        for ( k = 0 ; k < 3 ; k++ ) {
          pCCo[k] = wt0*pCo0[k] + wt1*pCo1[k] ;
          coCtr[k] += pCCo[k] ;
        }

        /* Unknowns. */
        pCUn = pCutUn + mUnknowns*mCutVx ;
        pUn0  = pVx0->Punknown ;
        pUn1  = pVx1->Punknown ;
        for ( k = 0 ; k < mUnknowns ; k++ ) {
          pCUn[k] = wt0*pUn0[k] + wt1*pUn1[k] ;
          unCtr[k] += pCUn[k] ;
        }
      }
    }
  }



  kFcInPlane = 0 ;
  if ( *pmVxVx > 2 ) {
    /* Find out whether a face of the element lies in the plane. */
    for ( kFc = 1 ; kFc <= pElT->mFaces ; kFc++ ) {
      pFoE = pElT->faceOfElem + kFc ;
      mVxFc = pFoE->mVertsFace ;
      for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
        if ( !nVxVx[ pFoE->kVxFace[kVx] ] )
          break ;
      }

      if ( kVx == mVxFc ) {
        /* All of the vertices of this face are in the plane. Done. */
        kFcInPlane = kFc ;

        /* List the nodes in order into nVxAll. */
        for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
          nVxAll[kVx] = nVxVx[ pFoE->kVxFace[kVx] ] ;

        break ;
      }
    }
  }


  *pnVxCtr = 0 ;
  if ( kFcInPlane )
    /* There is an elemental face in the plane. Just list that one. 
       Nothing else to be done here. */
    ;

  else if ( *pmVxEg == 0 ) {
    /* No cut to be added, this case could arise with a plane
       touching a node or an edge. */
    *pmVxVx = *pmVxAll = 0 ;
  }

  else if ( *pmVxEg + *pmVxVx < 3 )
    printf ( "  WARNING in uns_int_plane: plane only cuts %d times.\n",
             *pmVxEg+*pmVxVx ) ; 

  else if (  *pmVxEg + *pmVxVx < 4 ) {
    /* Only 3 intersection points, Order is trivial. Pack all edge nodes
       into the vertex nodes. */
    if ( *pmVxEg ) {
      for ( kEg = 0 ; kEg < mEg ; kEg++ ) 
        if ( nVxEg[kEg] ) {
          nVxVx[ *pmVxVx ] = nVxEg[kEg] ;
          (*pmVxVx)++ ;
        }
      *pmVxEg = 0 ;
    }
  }
  else {
    /* A central node is needed for triangulation of the intersection. */
    mCutVx++ ;
    *pnVxCtr = mCutVx ;
    nVxAll[ (*pmVxAll)++ ] = mCutVx ;

    mInt = *pmVxEg + *pmVxVx ;

    /* Coordinates. */
    pCCo = pCutCo + 3*mCutVx ;
    for ( k = 0 ; k < 3 ; k++ )
      pCCo[k] = coCtr[k]/mInt ;


    /* Unknowns. */
    pCUn = pCutUn + mUnknowns*mCutVx ;
    for ( k = 0 ; k < mUnknowns ; k++ ) {
      pCUn[k] = unCtr[k]/mInt ;
    }
  }




  return ( 1 ) ;
}

/******************************************************************************

  add_cut_elem_vx: 
  Add an element in the plane formed from 3 or 4 vertices given in order.
  
  Last update:
  ------------
  14Sep04; change io.
  3Sep3: conceived.
  
  Input:
  ------
  mCutEg: number of cut edges,
  nVxCut: for each edge the new node on it.

  Changes To:
  -----------
  mCutElems/mCutElemsAlloc
  pCutEl
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int add_cut_elem_vx ( uns_s *pUns, int mVx, int nVx[] ) {

  int *pCEl, kVx ;


  mCutElems++ ;
  mConnEntries += mVx ;

  if ( mCutElems+1 >= mCutElemsAlloc ) {
    /* Realloc Elems. Elem indices start at 1*/
    mCutElemsAlloc *= 1.3 ;
    mCutElemsAlloc++ ;

    pCutEl = arr_realloc ( "make_cut: pCutEl", pUns->pFam, 
                           pCutEl, 4*mCutElemsAlloc, sizeof( *pCutEl ) ) ;
  }


  /* Add the element. */
  pCEl = pCutEl + 4*mCutElems ;
  
  for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
    /* Connectivity. */
    pCEl[kVx] = nVx[kVx] ;
  }

  if ( mVx == 3 )
    /* Mark it as a tri. */
    pCEl[3] = -1 ;

  return ( 1 ) ;
}



/******************************************************************************

  add_cut_elem_ctr: 
  Add a list of triangles formed with a center node. 
  
  Last update:
  ------------
  14Sep04; recode, cleanup.
  3Sep3: conceived.
  
  Input:
  ------
  pUns
  pElem
  mCutEg     = number of cut edges,
  cutEg      = for each canonical edge the number of the intersection node on it, 
               -1 if there is no intersection for that edge.
  nVxEg      = for each edge the new node on it.
  nVxEg      = for each elemetal node the new node on it.
  mEgInPlane = number of edges that lie in the plane.
  egInPlane  = for each edge, 1 if it is in the plane, 0 if it isn't.

  Changes To:
  -----------
  mCutElems/mCutElemsAlloc
  pCutEl
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int add_cut_elem_ctr ( uns_s *pUns, elem_struct *pElem,
                              int mVxEg, int nVxEg[], int mVxVx, int nVxVx[], 
                              int nVxCtr ) {

  const elemType_struct *pElT = elemType + pElem->elType ;
  const faceOfElem_struct *pFoE ;

  int kVx, mFc = pElT->mFaces, kFc, mFcEg, mIntFc, iEg, kEg, 
    nVxFc[4], mInt = mVxEg+mVxVx, mVxEgFc, mVxVxFc, nVx, iVx ;




  if ( mCutElems + mInt >= mCutElemsAlloc ) {
    /* Realloc Elems. */
    mCutElemsAlloc *= 1.3 ;

    pCutEl = arr_realloc ( "make_cut: pCutEl", pUns->pFam, 
                           pCutEl, 4*mCutElemsAlloc, sizeof( *pCutEl ) ) ;
  }







  /* Add the elements. For each face with two cut edges add a cut element. */
  for ( kFc = 1 ; kFc <= mFc ; kFc++ ) {
    pFoE = pElT->faceOfElem + kFc ;
    mFcEg = pFoE->mFcEdges ;
    mIntFc = 0 ;

    /* Count the number of edge intersections. */
    mVxEgFc = 0 ;
    for ( iEg = 0 ; iEg < mFcEg ; iEg++ ) {
      kEg = pFoE->kFcEdge[iEg] ;
      if ( ( nVx = nVxEg[ kEg ] ) ) {
        /* Cut Edge. */
        mVxEgFc++ ;
        nVxFc[  mIntFc++ ] = nVx ;
      }
    }

    /* Nodal intersection. */
    mVxVxFc = 0 ;
    for ( iVx = 0 ; iVx < pFoE->mVertsFace ; iVx++ ) {
      kVx = pFoE->kVxFace[iVx] ;
      if ( ( nVx = nVxVx[ kVx ] ) ) {
        mVxVxFc++ ;
        nVxFc[  mIntFc++ ] = nVx ;
      }
    }

    if ( mIntFc == 0 )
      /* No intersection. */
      ;

    else if ( mIntFc == 1 ) {
      /* One intersection. Possible if only a corner vertex of the face
         is in the plane. Nothing to do in that case. */
      if ( mVxVxFc == 0 )
        printf ( "   WARNING in add_cut_elem_ctr: single cut edge on a face.\n" ) ;
    }
    else if ( mIntFc == 2 ) {
      /* Add the cut face. */
      nVxFc[2] = nVxCtr ;
      add_cut_elem_vx ( pUns, 3, nVxFc ) ; 
    }
    else 
      printf ( "   WARNING in add_cut_elem_ctr: %d cut edges on a %d edged face.\n",
               mIntFc, mFcEg ) ;
  } 

  return ( 1 ) ;
}


/******************************************************************************

  uns_int_plane:
  Cutting plane in 3D.
  
  Last update:
  ------------
  4Apr13; modified interface to loop_elems
  06Jan04; fix bug with dimension of dx, leading to wrong cutting planes.
  14Sep04; recode, cleanup.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int uns_int_plane ( double xP[3][3] ) {

  const elemType_struct *pElT ;
  const double *pCo, dTol = 1.e-3*Grids.epsOverlap ;
  const vrtx_struct *pVx, **ppVx ;

  uns_s *pUns ;
  chunk_struct *pChunk ;
  double z[3], d, dist[MAX_VX_ELEM], adist[MAX_VX_ELEM], dx[3][3] ;
  int k, kVx, *iVxSide, sideEl, crossEl,  
    mVxEl, mVxEg, nVxEg[MAX_EDGES_ELEM], mVxVx, nVxVx[MAX_VX_ELEM], 
    nVxCtr, mVxAll, nVxAll[MAX_EDGES_ELEM+MAX_VX_ELEM+1] ;
  elem_struct *pElem, *pElBeg, *pElEnd ;
  vrtx_struct *pVxBeg, *pVxEnd ;

  if ( !Grids.PcurrentGrid || Grids.PcurrentGrid->uns.type != uns ) {
    printf ( " FATAL in uns_int_plane: works only for unstructured grids.\n" ) ;
    return ( 0 ) ;
  }
  pUns = Grids.PcurrentGrid->uns.pUns ;


  if ( pUns->mDim != 3 ) {
    printf ( " FATAL in uns_int_plane: Only works for 3D grids.\n" ) ;
    return ( 0 ) ;
  }

  /* Create a temporary list of elems and vertices. */
  make_cut ( pUns ) ;

  /* Calculate the unit normal z to the plane. */
  for ( k = 0 ; k < 3 ; k++ ) {
    dx[1][k] = xP[1][k] - xP[0][k] ;
    dx[2][k] = xP[2][k] - xP[0][k] ;
  }
  cross_prod_dbl ( dx[1], dx[2], 3, z ) ;
  vec_norm_dbl ( z, 3 ) ;

  /* Alloc a distance for each vertex. */
  iVxSide = arr_malloc ( "uns_int_plane: pVxDist", pUns->pFam, 
                         pUns->mVertsNumbered+1, sizeof ( *iVxSide ) ) ;

  /* For each vert calc the oriented distance to the plane. */
  pChunk = NULL ;
  int nBeg, nEnd ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {

        pCo = pVx->Pcoor ;
        for ( k = 0 ; k < 3 ; k++ )
          dx[0][k] = pCo[k] - xP[0][k] ;

        d = scal_prod_dbl ( dx[0], z, 3 ) ;
        if ( d > dTol )
          iVxSide[ pVx->number ] = 1 ;
        else if ( d < -dTol )
          iVxSide[ pVx->number ] = -1 ;
        else       
          iVxSide[ pVx->number ] = 0 ;
      }


  /* Loop over all elements. Cut elements have a change in sign in distance. */
  pChunk = NULL ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pElem = pElBeg ; pElem <= pElEnd ; pElem++ )
      if ( pElem->number ) {
        pElT = elemType + pElem->elType ;
        mVxEl = pElT->mVerts ;
        ppVx = (const vrtx_struct **) pElem->PPvrtx ;

        /* Does the element cross the plane? */
        sideEl = iVxSide[ ppVx[0]->number ] ;
        for ( crossEl = kVx = 0 ; kVx < mVxEl ; kVx++ )
          if ( sideEl*iVxSide[ ppVx[kVx]->number ] <= 0 ) {
            /* Nodes on either side of or on the plane. */
            crossEl = 1 ;
            break ;
          }

        if ( crossEl ) {
          /* Element crosses or touches the plane. Recalculate all vertex distances. */
          for ( kVx = 0, mVxVx = 0 ; kVx < mVxEl ; kVx++ ) {
            pVx = ppVx[kVx] ;
            pCo = pVx->Pcoor ;
            for ( k = 0 ; k < 3 ; k++ )
              dx[0][k] = pCo[k] - xP[0][k] ;

            dist[kVx] = scal_prod_dbl ( dx[0], z, 3 ) ;
            adist[kVx] = ABS( dist[kVx] ) ;

            /* Count the number of nodes in the plane. */
            if ( adist[kVx] < dTol ) {
              /* Set it to zero, to simplify further testing. */
              adist[kVx] = 0. ;
              mVxVx++ ;
            }
          }


          /* Add new intersection points on edges and vertices. */
          add_cut_vx ( pUns, pElem, dist, adist, 
                       &mVxAll, nVxAll, &mVxEg, nVxEg, &mVxVx, nVxVx, &nVxCtr ) ;


          if ( !mVxAll ) 
            /* No cuts. */
            ;
          else if ( !nVxCtr ) {
            /* List only one cut based on the ordered vertices. */
            add_cut_elem_vx ( pUns, mVxAll, nVxAll ) ; 
          }
          else {
            /* A cut which requires adding a central node. */
            add_cut_elem_ctr ( pUns, pElem, mVxEg, nVxEg, mVxVx, nVxVx, nVxCtr ) ;
          }
        }
      }

  arr_free ( iVxSide ) ;

  if ( mCutElems )
    make_cut_grid ( pUns, z ) ;
  else if ( verbosity > 1 )
    printf ( " WARNING: no elements found that intersect the plane.\n" ) ;
  

  arr_free ( pCutEl ) ;

  return ( 1 ) ;
}


/******************************************************************************

  uns_int_bnd:
  Given a 3D unstructured mesh, extract a 2D mesh from the boundary surface.
  
  Last update:
  ------------
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
  6Apr13; initialise mConn.
  9Jul11; call init_elem to initialise.
  4Apr09; replace varTypeS with varList.
  28Jul06: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int uns_int_bnd ( const double z ) {

  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;

  grid_struct *pGrid ;
  uns_s *pUns, *pUns2 ;
  chunk_struct *pChunk, *pChunk2 ;
  elem_struct *pElem, *pEl ;
  vrtx_struct *pVx, *pVrtx, *pVxBeg, *pVxEnd, **ppVx, **ppVrtx, *pVxZero[4] ;
  bndPatch_struct *pBndPatch ;
  bndFc_struct *pBndFcBeg, *pBndFcEnd, *pBndFc ;
  bndFcVx_s *pBv ;

  double *pUnknown, *pCoor ;
  int mVx, nBeg, nEnd, mEl, mFc, mBc, newBndPatch, nBc, mZeroVx, kVx, mVxFc, mConn=0, 
    nEl ;

  if ( !Grids.PcurrentGrid || Grids.PcurrentGrid->uns.type != uns ) {
    printf ( " FATAL in uns_int_bnd: works only for unstructured grids.\n" ) ;
    return ( 0 ) ;
  }
  pUns = Grids.PcurrentGrid->uns.pUns ;


  if ( pUns->mDim != 3 ) {
    printf ( " FATAL in uns_int_bnd: Only works for 3D grids.\n" ) ;
    return ( 0 ) ;
  }


  /* Count and mark all vx at z=0. */
  mVx = 0 ;
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {

        if ( ABS( pVx->Pcoor[2] - z ) < pUns->epsOverlap ) {
          pVx->mark = 1 ;
          pVx->number = ++mVx ;
        }
        else
          pVx->number = 0 ;
      }

  /* Count all bnd faces in the z=0 plane and those touching it. */
  mEl = 0 ;
  mFc = 0 ; 
  mBc = 0 ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] ) {
      pBndPatch = NULL ;
      newBndPatch = 0 ;
      
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
            pElem = pBndFc->Pelem ;
            pFoE = elemType[pElem->elType].faceOfElem + pBndFc->nFace ;
            kVxFc = pFoE->kVxFace ;
            ppVx = pElem->PPvrtx ;
            mVxFc = pFoE->mVertsFace ;
          
            mZeroVx = 0 ;
            for ( kVx = 0 ; kVx < mVxFc ; kVx++ )
              if ( ABS( ppVx[ kVxFc[kVx] ]->Pcoor[2] - z ) < pUns->epsOverlap )
                mZeroVx++ ;

            if ( mZeroVx == mVxFc ) {
              /* All forming vx in the plane, new element. */
              mEl++ ;
              mConn += mVxFc ;
            }
            else if ( mZeroVx == 2 ) {
              /* Shares an edge, new boundary face. */
              mFc++ ;
              newBndPatch = 1 ;
            }
          }

      if ( newBndPatch )
        mBc++ ;
    }
              
 

  /* Allocate a chunk. We don't know the number of boundary faces, yet. */
  if ( !( pUns2 = make_uns ( NULL ) ) )
    hip_err ( fatal, 0, "failed to alloc a new unstructured grid"
              " in uns_int_bnd." ) ;
  else {
    pUns2->mDim = 2 ;
    pUns2->varList.mUnknowns = pUns->varList.mUnknFlow = 4 ;
    pUns2->varList.varType = cons ;
  }

  if ( !( pChunk2 = append_chunk ( pUns2, pUns2->mDim, mEl, mConn,
                                  0, mVx, mFc, mBc ) ) ) {
    printf ( " FATAL: failed to alloc a new unstructured chunk in read_adf_level.\n" ) ;
    return ( 0 ) ; }

  pUns2->mBc = mBc ;
  pUns2->mBndFcVx = mFc ;
  pBv = pUns2->pBndFcVx = arr_malloc ( "pUns2->pBndFcVx in uns_int_bnd", pUns->pFam,
                                      mFc, sizeof( *pUns2->pBndFcVx ) ) ;




  /* Extract vertices. */
  pCoor    = pChunk2->Pcoor ;
  pUnknown = pChunk2->Punknown ;
  pVrtx    = pChunk2->Pvrtx ;

  pChunk   = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        /* This one is in the plane. */
        pVrtx++ ;
        pCoor += 2 ;
        /* Don't use the extra fields that append_chunk is still allocating. */
        pUnknown += 4 ;

        pCoor[0] = pVx->Pcoor[0] ;
        pCoor[1] = pVx->Pcoor[1] ;
        pVrtx->Pcoor = pCoor ;
                     
        pUnknown[0] = pVx->Punknown[0] ;
        pUnknown[1] = pVx->Punknown[1] ;
        pUnknown[2] = pVx->Punknown[2] ;
        pUnknown[3] = pVx->Punknown[4] ;
        pVrtx->Punknown = pUnknown ;

        pVrtx->number = pVx->number ;
      }





  /* Extract elements and boundary faces. */
  pElem = pChunk2->Pelem ; 
  ppVrtx = pChunk2->PPvrtx ; 
  pVrtx = pChunk2->Pvrtx ;
  nEl = 0 ;

  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmFaceBc[nBc] ) {
      pBndPatch = NULL ;
      newBndPatch = 0 ;
      
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
        for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ )
          if ( pBndFc->Pelem && pBndFc->Pelem->number && pBndFc->nFace ) {
            pEl = pBndFc->Pelem ;
            pFoE = elemType[pEl->elType].faceOfElem + pBndFc->nFace ;
            kVxFc = pFoE->kVxFace ;
            ppVx = pEl->PPvrtx ;
            mVxFc = pFoE->mVertsFace ;
          
            mZeroVx = 0 ;
            for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) {
              pVx = ppVx[ kVxFc[kVx] ] ;
              if ( ABS( pVx->Pcoor[2] - z ) < pUns->epsOverlap ) {
                pVxZero[mZeroVx] = pVx ;
                mZeroVx++ ;
              }
            }

            if ( mZeroVx == mVxFc ) {
              /* All forming vx in the plane, new element. */
              nEl++ ;

              pElem++ ;
              elType_e elType = ( mVxFc == 3 ? tri : qua ) ;
              init_elem ( pElem, elType, nEl, ppVrtx ) ;

              for ( kVx = 0 ; kVx < mVxFc ; kVx++ ) 
                *ppVrtx++ = pVrtx + pVxZero[mVxFc-kVx-1]->number ;

            }
            else if ( mZeroVx == 2 ) {
              /* Shares an edge, new boundary face. */
              pBv->pBc = pBndFc->Pbc ;
              pBv->mVx = 2 ;
              pBv->ppVx[0] = pVrtx + pVxZero[0]->number ;
              pBv->ppVx[1] = pVrtx + pVxZero[1]->number ;
              pBv++ ;
            }
          }
    }



  pChunk2->mElems = pChunk2->mElemsNumbered = mEl ;
  pChunk2->mElem2VertP = mConn ; 




  /* Match the bnd Fc to the elements. */
  if ( !match_bndFcVx ( pUns2 ) )
    hip_err ( fatal, 0, "could not match boundary faces"
              " in uns_int_bnd" ) ;


  
  /* Unstructured. Make a new grid. */
  pGrid = make_grid () ;

  /* Put the chunk into Grids. */
  pGrid->uns.type = uns ;
  pGrid->uns.pUns = pUns2 ;
  pGrid->uns.mDim = 2 ;
  pGrid->uns.pVarList = &(pUns->varList) ;
  pUns->nr = pGrid->uns.nr ;
  pUns->pGrid = pGrid ;

  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;
  


  /* Make a new grid. */
  number_uns_grid ( pUns2 ) ;
  check_uns ( pUns2, check_lvl ) ;

  return ( 1 ) ;
}

