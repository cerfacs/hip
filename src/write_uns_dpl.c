/*
   write_uns_dpl.c
   Write an unstructured, multichunked grid as single block
   to screen or a dpl format.
 
   Last update:
   ------------
   21Jan18; update matchFc_s to number 0,1, rather than 1,2.
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   13Jan01; change name to write_uns_dpl, take care of hydra-style 2D variables
            with a 0 w component.
   23Jan97; list cut faces as a boundary.
   4May96; conceived.

   Contains:
   ---------
   write_uns_ascii:
   write_uns_dpl:
   
*/

#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern const elemType_struct elemType[] ;

static int write_uns_level_dpl ( uns_s *pUns, char *PdplFile );

/******************************************************************************

  write_uns_ascii:
  Write the un-chunked consistent mesh to screen.
  
  Last update:
  ------------
  9Jul19; rename to ADAPT_HIERARCHIC
  21Jan18; update matchFc_s to number 0,1 rather than 1,2.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  4May96: conceived.
  
  Input:
  ------
  ProotChunk:
  mDim:         
  
  Returns:
  --------
  1
  
*/

int write_uns_ascii ( uns_s *pUns, const int mDim ) {
  
  int fcType, nDim ;
  vrtx_struct *Pvrtx, **PPvrtx, **PPvxFc[MAX_VX_FACE], ***PPPvrtx ;
  elem_struct *Pelem ;
  chunk_struct *Pchunk ;
  const bc_struct *Pbc ;
  bndFc_struct *PbndFc ;
  bndPatch_struct *PbndPatch ;
  intFc_struct *PintFc ;
  matchFc_struct *PmatchFc ;
  
  /* Number the leaf elements only. */
  number_uns_elem_leafs ( pUns ) ;

  /* Connectivity. */
  printf ( "\n Connectivity:\n" ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  { printf ( "  Chunk %3d\n", Pchunk->nr ) ;

    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem+Pchunk->mElems ; Pelem++ )
      if ( Pelem->number ) {
        printf ( "   %3"FMT_ULG": ", Pelem->number ) ;
	
	for ( PPvrtx = Pelem->PPvrtx ;
	      PPvrtx < Pelem->PPvrtx + elemType[Pelem->elType].mVerts ; PPvrtx++ )
	  printf ( " %3"FMT_ULG" ", (*PPvrtx)->number ) ;

        printf ( ", vol %g", get_elem_vol ( Pelem ) ) ;
	printf ( "\n" ) ;
      }
  }

  /* Coordinates. Solution*/
  printf ( "\n Coordinates:\n" ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  { printf ( "  Chunk %3d\n", Pchunk->nr ) ;

    for ( Pvrtx = Pchunk->Pvrtx+1 ;
	  Pvrtx <= Pchunk->Pvrtx+Pchunk->mVerts ; Pvrtx++ )
      if ( Pvrtx->number )
      { printf ( "   %3"FMT_ULG":", Pvrtx->number ) ;
	for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  printf ( " %f,", Pvrtx->Pcoor[nDim] ) ;
	if ( Pchunk->Punknown )
	  printf ( "  -  %f\n", Pvrtx->Punknown[0] ) ;
	else
	  printf ( "\n" ) ;
      }
  }

  /* Boundary faces. */
  printf ( "\n Boundary faces:\n" ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  { printf ( "  Chunk %3d\n", Pchunk->nr ) ;

    for ( PbndPatch = Pchunk->PbndPatch+1 ;
	  PbndPatch <= Pchunk->PbndPatch + Pchunk->mBndPatches ;
	  PbndPatch++ )
    { Pbc = PbndPatch->Pbc ;
      printf ( "   %3d, %s\n", Pbc->nr, Pbc->text ) ;

      for ( PbndFc = PbndPatch->PbndFc ;
	    PbndFc < PbndPatch->PbndFc + PbndPatch->mBndFc ; PbndFc++ )
	if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace )
	{ /* This is a non-degenerate physical face. */
	  get_uns_face ( PbndFc->Pelem, PbndFc->nFace, PPvxFc, &fcType ) ;

	  printf ( "      %3"FMT_ULG": ", PbndFc->Pelem->number ) ;
	  for ( PPPvrtx = PPvxFc ;
	        PPPvrtx < PPvxFc + fcType ; PPPvrtx++ )
	    printf ( "%3"FMT_ULG" ", (**PPPvrtx)->number ) ;
	  printf ( "\n" ) ;
	}
    }
  }

  /* Internal faces. There actually shouldn't be any. uns_matchFc transforms
     them into boundary faces. */
  printf ( "\n Internal faces:\n" ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
  { printf ( "  Chunk %3d\n", Pchunk->nr ) ;

    for ( PintFc = Pchunk->PintFc + 1 ;
	  PintFc <= Pchunk->PintFc + Pchunk->mIntFaces ; PintFc++ )
      if ( PintFc->Pelem && PintFc->Pelem->number && PintFc->nFace )
      { get_uns_face ( PintFc->Pelem, PintFc->nFace, PPvxFc, &fcType ) ;

	printf ( "      %3"FMT_ULG": ", PintFc->Pelem->number ) ;
	for ( PPPvrtx = PPvxFc ;
	      PPPvrtx < PPvxFc + fcType ; PPPvrtx++ )
	  printf ( "%3"FMT_ULG" ", (**PPPvrtx)->number ) ;
	printf ( "\n" ) ;
      }
  }

  /* Matching faces. */
  printf ( "\n Matching faces:\n" ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) {
    printf ( "  Chunk %3d\n", Pchunk->nr ) ;

    for ( PmatchFc = Pchunk->PmatchFc + 1 ;
	  PmatchFc <= Pchunk->PmatchFc + Pchunk->mMatchFaces ; PmatchFc++ )
      if ( PmatchFc->pElem0->number && PmatchFc->nFace0 ) {
	get_uns_face ( PmatchFc->pElem0, PmatchFc->nFace0, PPvxFc, &fcType ) ;
	printf ( "      %3"FMT_ULG": ", PmatchFc->pElem0->number ) ;
	for ( PPPvrtx = PPvxFc ;
	      PPPvrtx < PPvxFc + fcType ; PPPvrtx++ )
	  printf ( "%3"FMT_ULG" ", (**PPPvrtx)->number ) ;

	if ( !PmatchFc->pElem1->number )
	  printf ( " FATAL: invalid element on matching face in write_uns_ascii.\n" ) ;
	get_uns_face ( PmatchFc->pElem1, PmatchFc->nFace1, PPvxFc, &fcType ) ;
	printf ( "      %3"FMT_ULG": ", PmatchFc->pElem1->number ) ;
	for ( PPPvrtx = PPvxFc ;
	      PPPvrtx < PPvxFc + fcType ; PPPvrtx++ )
	  printf ( "%3"FMT_ULG" ", (**PPPvrtx)->number ) ;
	
	printf ( "\n" ) ;
      }
  }

#ifdef ADAPT_HIERARCHIC
  if ( pUns->pllAdEdge )
    listEdge ( pUns, pUns->pllAdEdge, list_adEdge ) ;
#endif

  return ( 1 ) ;
}

/******************************************************************************

  write_uns_dpl.c:
  Write an unstructured 2-D grid to a dpl format.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_uns_dpl ( uns_s *pUns, char *pDplFile ) {
  
  char fileName[TEXT_LEN] ;
  int nLevel = 0 ;

  if ( !pUns->pUnsCoarse ) {
    /* Only one finest level. Just write a .dpl. */
    sprintf ( fileName, "%s.%d", pDplFile, nLevel ) ;
    return ( write_uns_level_dpl ( pUns, pDplFile ) ) ;
  }
  else {
    
    while ( pUns ) {
      sprintf ( fileName, "%s.%d", pDplFile, nLevel ) ;
      if ( !write_uns_level_dpl ( pUns, fileName ) ) {
	printf ( " FATAL: failed to write level %d to dpl in write_uns_dpl.\n",
		 nLevel ) ;
	return ( 0 ) ; }
      else {
	nLevel++ ;
	pUns = pUns->pUnsCoarse ;
      }
    }
    return ( 1 ) ;
  }
}

/******************************************************************************

  write_uns_level_dpl.c:
  Write an unstructured 2-D grid to a dpl format.
  
  Last update:
  ------------
  13jul17, switch pElCollapseTo to ppElContain
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  1Jul05; invoke special_verts
  16Mar04; add iter info for eulsol
  12Feb04; add mg connectivity info.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int write_uns_level_dpl ( uns_s *pUns, char *PdplFile ) {
  
  const faceOfElem_struct *pFoE ;
# define M_UNKN_DPL 4

  FILE *dplFile ;
  int nVert, mVertsNumbered, nUnknown, mBndPatches, mUnknowns, mFacesFound, mVerts, nBc ;
  double *pUn ;
  chunk_struct *Pchunk ;
  elem_struct *Pelem ;
  vrtx_struct *Pvrtx ;
  bc_struct  *Pbc ;
  bndFc_struct *PbndFc, *pBndFcBeg, *pBndFcEnd ;
  bndPatch_struct *pBndPatch ;

  if ( pUns->mDim != 2 ) {
    printf ( " FATAL: only 2-D grids can be written to dpl.\n" ) ;
    return ( 0 ) ; }
  if ( verbosity > 2 )
    printf ( "   Writing grid in dpl format to %s\n", PdplFile ) ;

  /* We want no or conservative variables. */
  conv_uns_var ( pUns, cons ) ;

  if ( !special_verts ( pUns ) ) {
    printf ( " FATAL: failed to match periodic vertices in write_uns_level_dpl.\n" ) ;
    return ( 0 ) ; }

  
  /* Open input file. */
  prepend_path ( PdplFile ) ;
  if ( ( dplFile = fopen ( PdplFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PdplFile ) ;
    return ( 0 ) ; }

  /* Unstructured header. */
  fprintf ( dplFile, "unstr\n" ) ;

  /* Number of elements, mPrevIter, nVarRms. */
  number_uns_elem_leafs ( pUns ) ;
  fprintf ( dplFile, "%"FMT_ULG" 0 0\n", pUns->mElemsNumbered ) ;

  /* Loop over all elements. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->number ) {
        /* This element is marked. */
	mVerts = elemType[Pelem->elType].mVerts ;
	fprintf ( dplFile, " %d", mVerts ) ;
	
	for ( nVert = 0 ; nVert < mVerts ; nVert++ )
	  fprintf ( dplFile, " %"FMT_ULG"", Pelem->PPvrtx[nVert]->number ) ;

	fprintf ( dplFile, "    %"FMT_ULG"\n", Pelem->number ) ;
      }

  /* Number of vertices. */
  mVertsNumbered = 0 ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    mVertsNumbered += Pchunk->mVertsNumbered ;
  fprintf ( dplFile, "%d\n", mVertsNumbered ) ;

  /* Freestream states, rmsRef, resMaxRef. */
  if ( pUns->varList.varType != noVar )
    /* Use the freestream states given by the variable type. */
    for ( nUnknown = 0 ; nUnknown < M_UNKN_DPL ; nUnknown++ )
      fprintf ( dplFile, "%f ", pUns->varList.freeStreamVar[nUnknown] ) ;
  else
    /* Variable type and dpl format don't match. Make up a freestream set. */
    fprintf ( dplFile, "1. 7. 4. 86." ) ;
  fprintf ( dplFile, " 1. 1.\n" ) ;

  /* Loop over all vertices. Remember: dpl's are 2D only.*/
  mUnknowns = MIN( pUns->varList.mUnknowns, M_UNKN_DPL ) ;
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pvrtx = Pchunk->Pvrtx + 1 ;
	  Pvrtx <= Pchunk->Pvrtx + Pchunk->mVerts ; Pvrtx++)
      if ( Pvrtx->number ) { 
	fprintf ( dplFile, "%17.9e %17.9e", Pvrtx->Pcoor[0], Pvrtx->Pcoor[1] ) ;

	if ( ( pUn = Pvrtx->Punknown ) ) {
	  /* There is data. */
            /* Hydra solution files list a w velocity in 2D. */
          if ( pUns->varList.mUnknFlow == 4 )
            /* 2D flow data. */
	    fprintf ( dplFile, " %f %f %f %f", pUn[0], pUn[1], pUn[2], pUn[3] ) ;
          else if ( pUns->varList.mUnknFlow == 5 )
            /* 2D flow data with a zero w component. */
	    fprintf ( dplFile, " %f %f %f %f", pUn[0], pUn[1], pUn[2], pUn[4] ) ;
          else {
            /* Not enough data. */
            for ( nUnknown = 0 ; nUnknown < mUnknowns ; nUnknown++ )
              fprintf ( dplFile, " %f", pUn[nUnknown] ) ;
            /* Fill the remaining states. */
            for (  ; nUnknown < M_UNKN_DPL ; nUnknown++ )
              fprintf ( dplFile, " 1." ) ;
          }
        }
	else
	  /* There is only a grid. */
	  fprintf ( dplFile, " 1. 0. 0. 99." ) ;

	if ( pUns->pnVxCollapseTo )
	  /* List the coarse-grid vertex and coarse grid element. */
	  fprintf ( dplFile, " %"FMT_ULG" %"FMT_ULG" %"FMT_ULG"\n", Pvrtx->number,
		    pUns->pnVxCollapseTo[ Pvrtx->number ],
		    pUns->ppElContain[ Pvrtx->number ]->number ) ;
        else
          fprintf ( dplFile, "   %"FMT_ULG"\n", Pvrtx->number ) ;
      }

  /* Number of boundaries. Count the faces. */
  count_uns_bndFaces ( pUns ) ;

  /* Dpl files don't know the concept of boundary nodes. List a boundary if it
     has faces. */
  for ( mBndPatches = 0, nBc = 0 ; nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmBiBc[nBc] )
      mBndPatches++ ;
  
  /* Number of patches in the file. */
  fprintf ( dplFile, "%d\n", mBndPatches ) ;

  /* Loop over all boundaries */
  for ( nBc = 0 ; pBndPatch = NULL, nBc < pUns->mBc ; nBc++ )
    if ( pUns->pmBiBc[nBc] ) {
      /* There are faces with that bc in this grid. */
      Pbc = pUns->ppBc[nBc] ;
      fprintf ( dplFile, "%"FMT_ULG" %s\n", pUns->pmBiBc[nBc], Pbc->text ) ;
      mFacesFound = 0 ;
      while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch, &pBndFcBeg, &pBndFcEnd ) )
	for ( PbndFc = pBndFcBeg ; PbndFc <= pBndFcEnd ; PbndFc++ )
	  if ( PbndFc->Pelem && PbndFc->Pelem->number && PbndFc->nFace ) {
	    /* This face is active. */
	    Pelem = PbndFc->Pelem ;
	    pFoE = elemType[Pelem->elType].faceOfElem + PbndFc->nFace ;
	    fprintf ( dplFile, "%"FMT_ULG" %"FMT_ULG" %"FMT_ULG"\n",
		      Pelem->PPvrtx[pFoE->kVxFace[0]]->number,
		      Pelem->PPvrtx[pFoE->kVxFace[1]]->number,
		      PbndFc->Pelem->number ) ;
	    mFacesFound++ ;
	  }
      if ( mFacesFound != pUns->pmBiBc[nBc] ) {
	sprintf ( hip_msg, 
                  "boundary face miscount (%d/%"FMT_ULG") in write_uns_dpl for boundary\n"
		  "        %s\n",
		 mFacesFound, pUns->pmBiBc[nBc], Pbc->text ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
  
  /* Empty outer boundary. */
  fprintf ( dplFile, "0 0\n" ) ;

  /* Done. */
  fclose ( dplFile ) ;

  return ( 1 ) ;
}

#ifdef ADAPT_HIERARCHIC
/******************************************************************************

  write_uns_dpl_adapt.c:
  Write an unstructured 2-D grid to a dpl format.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int write_uns_dpl_adapt ( uns_s *pUns, char *PdplFile )
{
  static FILE *dplFile ;
  static int nVert, mVertsNumbered, mDim, mVerts, kCh, maxLvl = 0, elemLvl ;
  /* A colormap for the refinement status:
     0: unrefined,
     1: buffered,
     2: derefined,
     3,4: refined in one,
     5,6: refined in the other dir,
     7,8,9,10: isotropic.
  const double colMap[] = {0., 2., 4.,   6.,7.5, 8.,9., 12.,13.,12.,13.} ; */
  const double colMap[] = {0., 2., 4.,   6.5,8.5, 6.5,8.5, 12.,13.,12.,13.} ;
  static double elemValue ;
  static chunk_struct *Pchunk ;
  static elem_struct *Pelem, *Pprt ;
  static vrtx_struct *Pvrtx ;
  
  mDim = pUns->mDim ;
  if ( mDim != 2 )
  { printf ( " FATAL: only 2-D grids can be written to dpl.\n" ) ;
    return ( 0 ) ;
  }
  if ( verbosity > 2 )
    printf ( "   Writing adaptation in dpl format to %s\n", PdplFile ) ;

  /* List leaves only. */
  number_uns_elem_leafs ( pUns ) ;

  /* Find the maximum adaptation depth. */
  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk ) 
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf )
	maxLvl = MAX( maxLvl, adaptLvl_elem( Pelem ) ) ;
  
  /* Open input file. */
  prepend_path ( PdplFile ) ;
  if ( ( dplFile = fopen ( PdplFile, "w" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", PdplFile ) ;
    return ( 0 ) ; }

  /* Unstructured header. */
  fprintf ( dplFile, "unstr\n" ) ;

  /* Number of elements. */
  number_uns_elem_leafs ( pUns ) ;
  fprintf ( dplFile, "%"FMT_ULG"\n", pUns->mElemsNumbered ) ;

  /* Loop over all elements. */
  for ( mVertsNumbered = 0, Pchunk = pUns->pRootChunk ;
	Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf )
      { /* This element is marked. */
	mVerts = elemType[Pelem->elType].mVerts ;
	fprintf ( dplFile, " %d", mVerts ) ;
	
	for ( nVert = 0 ; nVert < mVerts ; nVert++ )
	  fprintf ( dplFile, " %d", ++mVertsNumbered ) ;

	fprintf ( dplFile, "    %"FMT_ULG"\n", Pelem->number ) ;
      }

  /* Number of vertices. */
  fprintf ( dplFile, "%d\n", mVertsNumbered ) ;

  /* Freestream states. */
  fprintf ( dplFile, "1. 1. 0. 99.\n" ) ;

  for ( Pchunk = pUns->pRootChunk ; Pchunk ; Pchunk = Pchunk->PnxtChunk )
    for ( Pelem = Pchunk->Pelem+1 ;
	  Pelem <= Pchunk->Pelem + Pchunk->mElems ; Pelem++ )
      if ( Pelem->leaf )
      { /* This element is a leaf. */
	mVerts = elemType[Pelem->elType].mVerts ;
	elemLvl = adaptLvl_elem ( Pelem ) ;
	if ( Pelem->Pparent && Pelem->Pparent->PrefType->refOrBuf == ref &&
	     elemLvl == maxLvl )
	{ /* This is a refined parent. Find which child Pelem is. */
	  for ( kCh = 0 ; kCh < Pelem->Pparent->PrefType->mChildren ; kCh++ )
	    if ( Pelem->Pparent->PPchild[kCh] == Pelem )
	      break ;

	  Pprt = Pelem->Pparent ;
	  if ( Pprt->PrefType->nr + Pprt->elType == 3 )
	    /* Full refinement. */
	    elemValue = colMap[7+kCh] ;
	  else
	    /* Directional. */
	    elemValue = colMap[3+kCh] ;
	}
	else if ( Pelem->Pparent && Pelem->Pparent->PrefType->refOrBuf == buf &&
		  elemLvl > maxLvl-2 )
	  /* This parent is buffered. */
	  elemValue = colMap[1] ;
	else if ( Pelem->leaf && Pelem->PPchild )
	  /* This is an ex-parent, derefined. */
	  elemValue = colMap[2] ;
	else
	  /* unrefined. */
	  elemValue = colMap[0] ;
	  
	for ( nVert = 0 ; nVert < mVerts ; nVert++ )
	{ Pvrtx = Pelem->PPvrtx[nVert] ;
	  fprintf ( dplFile, "%17.9e %17.9e %g %g %g. 99.\n",
		   Pvrtx->Pcoor[0], Pvrtx->Pcoor[1],
		   elemValue, (float)Pelem->number, (float)Pvrtx->number ) ;
	}
	  
      }

  fprintf ( dplFile, "0 0\n0 0\n" ) ;

  /* Done. */
  fclose ( dplFile ) ;

  return ( 1 ) ;
}
#endif
