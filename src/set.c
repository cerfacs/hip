/*
  set.c:
*/

/*! collect all set options and default parameters.
 *
 *   more details
 *
 */


/*
  Last update:
  ------------
  5Aug19; allow to set Grids.adapt.doPer.
  2Apr19; intro DEFAULT_mmg_mPerLayer. Remove setting mLayerBcPer.
  21Mar18; intro doWarn.abortFc, doRemove_listUnMatchedFc.
  10Nov17; edit comments about intPolRim, etc, for clarity
  16Feb17; intro mgVolMinSub.
  6Jan15; rename pMbFam to more descriptive pArrFamMb
  6Apr13; include proto for init_mb.
  14Mar12; initialise and set defaults to doWarn and doRemove.
  9Jul11; extracted from main.



  This file contains:
  -------------------
  init()
  set_menu ()
  var_menu ()

*/

#include "cpre.h"

#include <strings.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <unistd.h>


#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#include "proto_mb.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern int verbosity ;
#define DEFAULT_verbosity (3)
extern hip_output_e hip_output ;
#define DEFAULT_hip_output (term)

extern const char version[] ;

extern const char fatal_log_file[] ;
extern const char warning_log_file[] ;
extern const char recoString[][MINITXT_LEN] ;
extern const char varTypeNames[][LEN_VAR_C] ;



/* Parameters: */

extern double Gamma, GammaM1 ;
extern double R;


/* Low storage option. E.g., no face connectivity check. */
#define DEFAULT_check_lvl (5)
extern int check_lvl ;


/* Abort criteria. */
#define DEFAULT_negVol_abort (1)
extern int negVol_abort ;
#define DEFAULT_negVol_flip (1)
extern int negVol_flip ;


/* Symmetry plane is in which coordinate? Default y=1. */
#define DEFAULT_symmCoor (1)
extern int symmCoor ;



#define DEFAULT_doWarn_matchFc (1)
#define DEFAULT_doWarn_intFc   (1)
#define DEFAULT_doWarn_bndFc   (1)
#define DEFAULT_doWarn_abortFc   (1)

#define DEFAULT_doRemove_matchFc (1)
#define DEFAULT_doRemove_intFc   (1)
#define DEFAULT_doRemove_bndFc   (1)
#define DEFAULT_doRemove_listUnMatchedFc   (0)
extern doFc_struct doWarn, doRemove ;




/* What to do with periodic boundaries. Write them as external boundaries? */
#define DEFAULT_perBc_in_exBound (1)
extern int perBc_in_exBound ;

/* When to consider faces to be parallel (i.e. periodic translation
   or oblique (i.e. periodic rotation). Minor component of the
   difference of the unit normals on the matching patches. */
extern double per_thresh_is_rot ;
#define DEFAULT_per_thresh_is_rot (1.e-2)



/* By how much can an edge lengthen in the edge-collapsing? */
#define DEFAULT_mgLen (2.2)
extern double  mgLen ;
/* What is the cosine of the largest angle we want to tolerate in a coarser grid. */
#define DEFAULT_mgLrgstAngle (-.99)
extern double  mgLrgstAngle ;
/* Minimum face twist, 1. being linear, -1. being completely folded over. */
#define DEFAULT_mgTwistMin (.0)
extern double  mgTwistMin ;
/* Minimum volumetric aspect ratio. */
#define DEFAULT_mgVolAspect (.1)
extern double  mgVolAspect ;
/* Minimum volume of a subelement in convexity test as
   fraction of epsOver Cube. */
#define DEFAULT_mgVolMinSub (.01)
extern double  mgVolMinSub ;
/* What do we consider stretched in order to apply semi-coarsening? */
#define DEFAULT_mgArCutoff (2.)
#define DEFAULT_mgRamp (1.0)
extern double  mgArCutoff ;
extern double  mgArCutoff2 ;
extern double  mgRamp ;


/* MMG. */
int DEFAULT_mmg_mPerLayer = 10 ;

/* When calculating nodal normals, set these to zero if the face angles vary more
   than this cosine from the total node normal. */
#define DEFAULT_normAnglCut (.9)
extern double normAnglCut ;
#define DEFAULT_singleBndVxNormal (0)
extern int singleBndVxNormal ;



/* Least squares reconstruction for interpolation. */
#define DEFAULT_reco (reco_min_norm)
extern reco_enum reco ;
#define DEFAULT_mVxRecoFactor (1.5)
extern double mVxRecoFactor ;

extern double intPolRim ; /* the allowable distance to interpolate outside the domain is
   intPolRim*hEdge where hEdge is the max edge-length of the nearest face. */
#define DEFAULT_intPolRim (TOO_MUCH)

extern double intPol_dist_inside ; /* When to consider a location covered by a grid:
   the nearest vertex is closer than this many multiples of epsOverlap.
   For element-based interpolation: thickness of the layer to search globally.*/
#define DEFAULT_intPol_dist_inside (9999)

extern double intFcTol ; /* When to consider a vertex to be inside an element:
   the smallest face distance is closer than this many multiples of hElem,
   the typical size of the element. */
#define DEFAULT_intFcTol (.1)

extern double intFullTol ; /* If there is no containment in elmeent-based searches,
    search all of those cells where the donor is within  intFullTol*hElem. */
#define DEFAULT_intFullTol (0)




/* Weighting function. */
#define DEFAULT_choldc_tol (1.e-9)
extern double choldc_tol ;
#define DEFAULT_ls_lambda (1.)
extern double ls_lambda ;



/* edge weight Lp-ness. */
#define DEFAULT_lpTol (1.e25)
#define DEFAULT_lpSweeps  (0)
#define DEFAULT_egWtCutOff (1.e-15)

#define DEFAULT_fixPerVx (1)


/* Find degenerate multiblock faces by length comparisons? */
#define DEFAULT_find_mbDegenFaces (0)
extern int find_mbDegenFaces ;
/* How to treat elements with degenerate edges. */
#define DEFAULT_fix_degenElems (0)
extern int fix_degenElems ;
/* Fix elements with too large angles? And what is the cosine of the threshold? */
#define DEFAULT_dg_fix_lrgAngles (0)
extern int dg_fix_lrgAngles ;
#define DEFAULT_dg_lrgAngle (-.75)
extern double dg_lrgAngle ;

/* Chord length for the NACA0012 surface reco. */
#define DEFAULT_chord (1.)
extern double chord ;


/* Global variables. */
extern char hip_msg[] ;

extern Grids_struct Grids ;
extern arrFam_s *pArrFamMb ;


extern struct tms timings ;
extern clock_t nowTime, iniTime ;
extern int clk_ticks ;


/******************************************************************************

  hip_init:
  Initialise.

  Last update:
  ------------
  5Sep19; add check_valid, set_prompt from main.
  25Feb19; init mmg params.
  21Mar18; intro doWarn.abortFc, doRemove_listUnMatchedFc.
  11Nov17; intro intFullTol.
  6Jan15; rename pMbFam to more descriptive pArrFamMb
  3Sep12; fix bug with dg-coll setting,
          update parameter listing to include all setable params (except mb)
  11Oct07: extracted from main.

*/

void hip_init () {

  /* Parameters: */
  Gamma = 1.4, GammaM1 = .4 ;
  R = 287. ;



  /* Abort criteria. */
  negVol_abort = DEFAULT_negVol_abort ;
  negVol_flip = DEFAULT_negVol_flip ;



  /* What to do with periodic boundaries. Write them as external boundaries? */
  perBc_in_exBound = DEFAULT_perBc_in_exBound ;

  /* When to consider faces to be parallel (i.e. periodic translation
     or oblique (i.e. periodic rotation). Minor component of the
     difference of the unit normals on the matching patches. */
  per_thresh_is_rot = DEFAULT_per_thresh_is_rot ;



  /* By how much can an edge lengthen in the edge-collapsing? */
  mgLen = DEFAULT_mgLen ;
  /* What is the cosine of the largest angle we want to tolerate in a coarser grid. */
  mgLrgstAngle = DEFAULT_mgLrgstAngle ;
  /* Minimum face twist, 1. being linear, -1. being completely folded over. */
  mgTwistMin = DEFAULT_mgTwistMin ;
  /* Minimum volumetric aspect ratio. */
  mgVolAspect = DEFAULT_mgVolAspect ;
  /* Minimum volume of subtet in convexity test. */
  mgVolMinSub = DEFAULT_mgVolMinSub ;
  /* What do we consider stretched in order to apply semi-coarsening? */
  mgArCutoff = DEFAULT_mgArCutoff ;
  mgArCutoff2 = DEFAULT_mgArCutoff*DEFAULT_mgArCutoff ;
  mgRamp = DEFAULT_mgRamp ;


  /* When calculating nodal normals, set these to zero if the face angles vary more
     than this cosine from the total node normal. */
  normAnglCut = DEFAULT_normAnglCut ;
  singleBndVxNormal = DEFAULT_singleBndVxNormal ;

  /* Chord length for the NACA0012 surface reco. */
  chord = DEFAULT_chord ;

  /* Low storage option. E.g., no face connectivity check. */
  check_lvl = DEFAULT_check_lvl ;



  /* Least squares reconstruction for interpolation. */
  reco = DEFAULT_reco ;
  mVxRecoFactor = DEFAULT_mVxRecoFactor ;
  /* the allowable distance to interpolate outside the domain is
     intPolRim*hEdge where hEdge is the max edge-length of the nearest face. */
  intPolRim = DEFAULT_intPolRim ;
  /* When to consider a location covered by a grid:
     the nearest vertex is closer than this many multiples of epsOverlap. */
  intPol_dist_inside = 5. ;
  intFcTol = DEFAULT_intFcTol ;

  /* Weighting function. */
  choldc_tol = DEFAULT_choldc_tol ;
  ls_lambda = DEFAULT_ls_lambda ;


  /* Topology errors. */
  doWarn.matchFc = DEFAULT_doWarn_matchFc ;
  doWarn.intFc   = DEFAULT_doWarn_intFc   ;
  doWarn.bndFc   = DEFAULT_doWarn_bndFc   ;
  doWarn.abortFc   = DEFAULT_doWarn_abortFc   ;

  doRemove.matchFc = DEFAULT_doRemove_matchFc ;
  doRemove.intFc   = DEFAULT_doRemove_intFc   ;
  doRemove.bndFc   = DEFAULT_doRemove_bndFc   ;
  doRemove.listUnMatchedFc   = DEFAULT_doRemove_listUnMatchedFc ;


  Grids.mGrids = Grids.mUnsGrids = 0 ;

  Grids.epsOverlap = DEFAULT_epsOverlap ;
  Grids.epsOverlapSq = Grids.epsOverlap*Grids.epsOverlap ;

  Grids.lp_tolerance = DEFAULT_lpTol ;
  Grids.lp_sweeps = DEFAULT_lpSweeps ;
  Grids.egWtCutOff = DEFAULT_egWtCutOff ;

  Grids.fixPerVx = DEFAULT_fixPerVx ;

  Grids.hrbs.mHrbs = 0 ;
  Grids.hrbs.pHrb = NULL ;

  Grids.adapt.maxLevels = 999 ;
  Grids.adapt.upRef = 1. ;
  Grids.adapt.doPer = 1 ;


  pArrFamMb = make_arrFam ( "mbFam" ) ;

  /* Erase log files. */
  sprintf ( hip_msg, "touch %s ; /bin/rm %s", fatal_log_file, fatal_log_file ) ;
  system ( hip_msg ) ;
  sprintf ( hip_msg, "touch %s ; /bin/rm %s", warning_log_file, warning_log_file ) ;
  system ( hip_msg ) ;


  init_mb () ;
  init_uns_core_data () ;

  iniTime = times ( &timings ) ;
  clk_ticks = sysconf ( _SC_CLK_TCK ) ;

  // JDM: do we need to refer to the macro here? Points to just one function?
  //CHECK_VALID();
  check_valid_license () ;

  r1_set_prompt ( "hip[0/0]>" ) ;

  return ;
}





/******************************************************************************

  set_menu:
  Set set-able parameters.

  Last update:
  ------------
  7Feb16; remove periodic setup for per boundaries with name or type changes.
  13Jan15; replace all special chars in bc-text with underscores.
  15Dec13; intro axiY, axiZ topo.
  19Jul13; allow bc-order with tuples of expr and order.
  15Jul13; fix bug with uninitialised pBc in bc-order block.
  9Jul13; use loop_bc_expr matching for bc-options.
  8Dec07; reorder and tidy up into categories as per the help menu.
  17May06; allow wildcards in setting bc-type.
  15Jul96: conceived.

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_prompt:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  3Apr20: conceived.


  Input:
  ------
  pGrid: grid defining the prompt values

  Output:
  -------
  prompt: string with the prompt


*/
void set_prompt ( const grid_struct *pGrid ) {

  char prompt[TEXT_LEN] ;

  if ( pGrid->uns.name[0] != '\0' ) {
    /* There is a name, use it in the prompt. */
    snprintf ( prompt, 32, "hip[%s: %d/%d]>",
               pGrid->uns.name, pGrid->uns.nr, Grids.mGrids ) ;
  }
  else {
    /* No name, just use the assigned number. */
    sprintf ( prompt, "hip[%d/%d]>", pGrid->uns.nr, Grids.mGrids ) ;
  }

  r1_set_prompt ( prompt ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_current_grid:
*/
/*! Make the given grid the current one, update the prompt.
 */

/*

  Last update:
  ------------
  3Apr20; move here from meth.c, rename to set_current_pGrid.
  11Nov17: conceived.


  Input:
  ------
  pGrid: grid to make the current one.

*/

void set_current_pGrid ( grid_struct *pGrid ) {

  /* Make the copied to grid the current one. */
  Grids.PcurrentGrid = pGrid ;

  /* Set new prompt. */
  set_prompt ( pGrid ) ;

  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_current:
*/
/*! Set the current grid to the given number.
 *

 */

/*

  Last update:
  ------------
  3Apr20; match on expr, rename to set_current_grid
  14Mar18; move dir test from write_hdf5 to here.
  4Jul17: extracted from set.


  Input:
  ------
  gridNr: the number of the grid.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void set_current_grid_expr ( const char *expr ) {
  /* Find it. */
  grid_struct  *pGrid = find_grid ( expr, noGr ) ;
  if ( pGrid )
  set_current_pGrid ( pGrid ) ;
  else {
    sprintf ( hip_msg, "grid matching `%s' cannot be found, current grid unchanged.", expr ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_bc_text:
*/
/*! public function to change bc-text.
 *
 */

/*

  Last update:
  ------------
  6Sep19: extracted from set_menu.


  Input:
  ------
  expr: to match bcs to act on, reset all if empty
  bc_text: text to set matching bc to.

  Returns:
  --------
  ret_s: 1 on failure, 0 on success

*/

/* hip_cython */
ret_s set_bc_text ( char *expr, char *bc_text ) {
  ret_s ret = ret_success () ;

  bc_struct *pBc ;
  int found = 0 ;
  /* Find the bc. */
  for ( pBc = NULL, found = 0 ; loop_bc_expr( &pBc, expr ) ; ) {
    found = 1 ;
    specchar2underscore ( bc_text ) ;
    strncpy ( pBc->text, bc_text, MAX_BC_CHAR-1 ) ;
    pBc->text[MAX_BC_CHAR-1] = '\0' ;
    /* Update summary type. */
    set_bc_e ( pBc ) ;

    if ( pBc->pPerBc )
      /* This bc had periodic setup which may be invalidated. Reset. */
      unset_per ( pBc ) ;

  }

  if ( !found ) {
    sprintf ( hip_msg, "no matching boundary condition found.\n" ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }


  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_bc_text:
*/
/*! public function to change bc-text.
 *
 */

/*

  Last update:
  ------------
  6Sep19: extracted from set_menu.


  Input:
  ------
  expr: to match bcs to act on, reset all if empty
  bcTypeStr: type to set matching bc to, 'o' if empty.

  Returns:
  --------
  ret_s: 1 on failure, 0 on success

*/

/* hip_cython */
ret_s set_bc_type ( char *expr, char *bcTypeStr ) {
  ret_s ret = ret_success () ;

  bc_struct *pBc ;
  int found = 0 ;
  char bcType ;
  if ( !expr[0] ) {
    /* No argument. Erase all output boundary types. */
    for ( pBc = find_bc ( "", 0 ) ; pBc ; pBc = pBc->PnxtBc ) {
      pBc->type[0] = 'o' ;

      /* Update summary type. */
      set_bc_e ( pBc ) ;
      if ( pBc->pPerBc )
        /* This bc had periodic setup which may be invalidated. Reset. */
        unset_per ( pBc ) ;
    }
  }
  else {

    if ( !bcTypeStr[0] )
      bcType = 'o' ;
    else
      bcType = tolower ( bcTypeStr[0] ) ;
    /* Recognize 0,1,2 for backward compatibility. */
    if      ( bcType == '0' ) bcType = 'w' ;
    else if ( bcType == '1' ) bcType = 's' ;
    else if ( bcType == '2' ) bcType = 'f' ;


    /* Loop over the matching bcs. */
    for ( pBc = NULL, found = 0 ; loop_bc_expr( &pBc, expr ) ; ) {
      found = 1 ;

      if ( bcType == 'l' || bcType == 'u' ) {
        bcTypeStr[0] = tolower ( bcTypeStr[0] ) ;
        /* Supply a 00 tag if none is given. */
        if ( !strcmp( bcTypeStr, "u" ) ) strcpy ( bcTypeStr, "u00" ) ;
        if ( !strcmp( bcTypeStr, "l" ) ) strcpy ( bcTypeStr, "l00" ) ;
        /* Copy the full tag. */
        strncpy ( pBc->type, bcTypeStr, MAX_BC_CHAR ) ;
      }
      else {
        /* Retain first character only. */
        pBc->type[0] = bcType ; pBc->type[1] = '\0' ;
      }
      /* Update summary type. */
      set_bc_e ( pBc ) ;
      if ( pBc->pPerBc )
        /* This bc had periodic setup which may be invalidated. Reset. */
        unset_per ( pBc ) ;
    }

    if ( !found ) {
      sprintf ( hip_msg, "no matching boundary condition found.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }
  }

  return ( ret ) ;
}

/*

  Last update:
  ------------
  12Jan21: extracted from set_menu.


  Input:
  ------
  expr: to match bcs to act on, reset all if empty
  bcMarkInt: integer mark to set bnd

  Returns:
  --------
  ret_s: 1 on failure, 0 on success

*/

/* hip_cython */
ret_s set_bc_mark ( char *expr , int *bcMarkInt) {
  ret_s ret = ret_success () ;

  bc_struct *pBc ;
  int found = 0 ;
  if ( !expr[0] ) {
    /* No argument. Erase all output boundary types. */
    for ( pBc = find_bc ( "", 0 ) ; pBc ; pBc = pBc->PnxtBc ) {
      pBc->mark = 0 ; 
    }
  }
  else {

    /* Loop over the matching bcs. */
    for ( pBc = NULL, found = 0 ; loop_bc_expr( &pBc, expr ) ; ) {
      found = 1 ;
      pBc -> mark = *bcMarkInt ;
    }

    if ( !found ) {
      sprintf ( hip_msg, "no matching boundary condition found.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }
  }

  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_menu:
*/
/*! parse input and set parameters.
 *
 *
 */

/*

  Last update:
  ------------
  24Aug24; enable set_topo to other than current grids, including future reads. 
  21Mar18; intro doWarn.abort, doRemove_listUnMatchedFc.
  : conceived.

*/

/* hip_cython */
ret_s set_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  char keyword[LINE_LEN], prompt[LINE_LEN], bc_name[TEXT_LEN], axis[TEXT_LEN],
    bcType, expr[LINE_LEN] ;
  int gridNr, found = 0, bcNr, nCoor, mDim, mBc, bcOrder, iVal, isText=0 ;
  grid_struct *Pgrid ;
  bc_struct *pBc, *pBcNr ;
  double coor[6*MAX_DIM], rotAngle, dVal ;


  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  /* General options
     --------------------------------------------------------------- */

  if ( !strncmp ( keyword, "verb", 2 ) )
    /* Verbosity. */
    if ( eo_buffer () )
      /* No argument, reset. */
      verbosity = DEFAULT_verbosity ;
    else {
      read1int ( &verbosity ) ;
#     ifndef DEBUG
      verbosity = MIN( verbosity, 5 ) ;
#     endif
    }

  else if ( !strncmp ( keyword, "output", 2 ) )
    /* Verbosity. */
    if ( eo_buffer () )
      /* No argument, reset. */
      hip_output = DEFAULT_hip_output ;
    else {
      read1string ( keyword ) ;
      if ( strncmp ( keyword, "string", 2 ) )
        hip_output = string ;
      else if  ( strncmp ( keyword, "screen", 2 ) )
        hip_output = term ;
      else
        hip_err ( warning, 1, "unrecognised output type, ignored." ) ;
    }

  else if ( !strncmp ( keyword, "prom", 2 ) )
    /* Prompt */
    if ( eo_buffer () )
      /* No argument, reset. */
      r1_set_prompt ( "hip>" ) ;
    else {
      read1string ( prompt ) ;
      r1_set_prompt ( prompt ) ;
    }

  else if ( !strncmp ( keyword, "curr", 2 ) ) {
    /* Switch to a new current grid. */
    char expr[LINE_LEN] ;
    read1string ( expr ) ;

    set_current_grid_expr ( expr ) ;
  }

  else if ( !strncmp ( keyword, "path", 2 ) ) {
    /* Set a default path to be prepended to all non-absolute paths. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.path[0] = '\0' ;
    else {
      read1string ( Grids.path ) ;
      /* Make sure there is a trailing slash. */
      r1_beginstring ( Grids.path, TEXT_LEN-1 ) ;
      r1_endstring ( Grids.path, TEXT_LEN-1 ) ;
      if ( Grids.path[ strlen( Grids.path )-1 ] != '/' )
	strncat ( Grids.path, "/\0", TEXT_LEN-1 ) ;


      /* Check for existence of the directory. */
      char outFile[LINE_LEN] ;
      strcpy ( outFile, "." ) ;
      FILE* Ftest ;
      if ( !( Ftest = r1_fopen ( prepend_path ( outFile ), TEXT_LEN, "r" ) ) ) {
        //if ( !( Ftest = r1_fopen ( outFile, TEXT_LEN, "r" ) ) ) {
        hip_err ( warning, 1, "requested path does not exist, using './'" ) ;
        strcpy ( Grids.path, "./\0" ) ;
      }
      else
        fclose ( Ftest ) ;

    }
  }

  else if ( !strncmp ( keyword, "epso", 2 ) ) {
    /* Set a new epsOverlap value. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.epsOverlap = DEFAULT_epsOverlap ;
    else
      read1double ( &Grids.epsOverlap ) ;
    Grids.epsOverlapSq = Grids.epsOverlap*Grids.epsOverlap ;
  }

  else if ( !strncmp ( keyword, "gridname", 2 ) ) {
    /* Assign a name to a grid */
    char expr[LINE_LEN], name[LINE_LEN] ;
    read1string ( expr ) ;
    if ( !eo_buffer () ) {
      // Two args given, first is current name, second is name to change to
      read1string ( name ) ;
      Pgrid = find_grid ( expr, noGr ) ;
    }
    else {
      // One arg, rename current grid.
      strcpy( name, expr ) ;
      Pgrid = Grids.PcurrentGrid ;
    }
    
    if ( Pgrid ) {
      strncpy ( Pgrid->uns.name, name, TEXT_LEN-1 ) ;
      Pgrid->uns.name[TEXT_LEN-1] = '\0' ;
    }
    else {
      sprintf ( hip_msg, "no grid present which matches %s.\n", expr ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }

    /* Update the prompt, in case this was the current grid. */
    set_current_pGrid ( Grids.PcurrentGrid ) ;
  }

  else if ( !strncmp ( keyword, "vo-abort", 2 ) )
    /* Abort on negative volumes. */
    if ( eo_buffer () )
      /* No argument, reset. */
      negVol_abort = DEFAULT_negVol_abort ;
    else {
      read1int ( &iVal ) ;
      negVol_abort = ( iVal ? 1 : 0 ) ;
    }


  else if ( !strncmp ( keyword, "vo-swap", 2 ) )
    /* Swap negative volumes. */
    if ( eo_buffer () )
      /* No argument, reset. */
      negVol_flip = DEFAULT_negVol_flip ;
    else {
      read1int ( &iVal ) ;
      negVol_flip = ( iVal ? 1 : 0 ) ;
    }

  else if ( !strncmp ( keyword, "check", 2 ) ) {
    /* Low storage. */
    if ( eo_buffer () )
      /* Reset. */
      check_lvl = DEFAULT_check_lvl ;
    else {
      read1int ( &check_lvl ) ;
      check_lvl = MAX(0,check_lvl) ;
      check_lvl = MIN(5,check_lvl) ;
    }
  }

  else if ( !strncmp ( keyword, "symmetry", 2 ) ) {
    /* Symmetry plane direction. */
    if ( eo_buffer () )
      /* Reset. */
      symmCoor = DEFAULT_symmCoor ;
    else {
      char symmChar ;
      read1char ( &symmChar ) ;
      symmChar = tolower ( symmChar ) ;
      switch ( symmChar ) {
      case 'x':
        symmCoor = 0 ;
        break ;
      case 'y':
        symmCoor = 1 ;
        break ;
      case 'z':
        symmCoor = 2 ;
        break ;
      default :
        sprintf ( hip_msg, " WARNING: invalid value %d for the symmetry direction.\n"
                  "          reset to value 1, y=0.\n", symmCoor ) ;
        hip_err ( warning, 0, hip_msg ) ;
        symmCoor = 1 ;
      }
    }
  }

  else if ( !strncmp ( keyword, "topology", 2 ) ) {
    /* Special topologies. */
    char grid[LINE_LEN] ;
    grid_struct *pGrid = NULL ;
    printf ( "entering topo menu in set.c\n" ) ;
    if ( eo_buffer () )
      /* Reset. */
      set_specialTopo ( NULL, "noTopo" ) ;
    else {
      read1string ( prompt ) ;

      /* Apply to a grid other than current? */
      if ( !eo_buffer () ) {
        read1string ( grid ) ;
        
        if ( is_int(grid) && atoi(grid) < 1 ) {
          // Apply to next grid to be read using make_uns.
           printf ( "calling nxt in set.c\n" ) ;
           set_nxtSpecialTopo ( prompt ) ;
        }
        else if ( !(pGrid = find_grid ( grid, uns ) ) ) {
          // No other matching uns grid
          sprintf ( hip_msg, "could not find an uns grid matching %s,"
                    " command ignored", grid ) ;
          hip_err ( warning, 0, hip_msg ) ;
        }
        else {
          // Apply to pGrid.
          set_specialTopo ( pGrid->uns.pUns, prompt ) ;
        }
      }
    }
  }

  else if ( !strncmp ( keyword, "hyvol", 2 ) ) {
    ret = set_hyvol () ;
  }

  /* Boundary condition options, deprecated, now accessible directly via bc
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "bc-text", 5 ) ) {
    ret = set_bc_text_arg (  ) ;
  }

  else if ( !strncmp ( keyword, "bc-type", 5 ) ) {
    ret = set_bc_type_arg () ;
  }

  else if ( !strncmp ( keyword, "bc-mark", 5 ) ) {
    ret = set_bc_mark_arg () ;
  }

  else if ( !strncmp ( keyword, "bc-order", 5 ) ) {
    ret = set_bc_order_arg () ;
  }
  else if ( !strncmp ( keyword, "bc-compress", 5 ) ) {

    if ( Grids.PcurrentGrid->uns.type == uns )
      uns_compress_bc ( Grids.PcurrentGrid->uns.pUns ) ;
  }


  /* Edge options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "lp-tol", 5 ) ) {
    /* Set a new lp_tolerance value. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.lp_tolerance = DEFAULT_lpTol ;
    else
      read1double ( &Grids.lp_tolerance ) ;
  }

  else if ( !strncmp ( keyword, "lp-sweeps", 5 ) ) {
    /* Set a new lp_sweeps value. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.lp_sweeps = DEFAULT_lpSweeps ;
    else
      read1int ( &Grids.lp_sweeps ) ;
  }

  else if ( !strncmp ( keyword, "edge-weight-cutoff", 2 ) ) {
    /* Set a new lp_sweeps value. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.egWtCutOff = DEFAULT_egWtCutOff ;
    else
      read1double ( &Grids.egWtCutOff ) ;
  }


  /* Multi-grid options
     --------------------------------------------------------------- */


  else if ( !strncmp ( keyword, "mg-length", 5 ) ) {
    /* Set a new maximum permissible edge length increase. */
    if ( eo_buffer () )
      /* Reset. */
      mgLen = DEFAULT_mgLen ;
    else
      read1double ( &mgLen ) ;
  }

  else if ( !strncmp ( keyword, "mg-angle", 5 ) ) {
    /* Set a new maximum permissible cosine for coarsenend meshes. */
    if ( eo_buffer () )
      /* Reset. */
      mgLrgstAngle = DEFAULT_mgLrgstAngle ;
    else
      read1double ( &mgLrgstAngle ) ;
  }


  else if ( !strncmp ( keyword, "mg-twist", 5 ) ) {
    /* Set a new maximum permissible cosine for coarsenend meshes. */
    if ( eo_buffer () )
      /* Reset. */
      mgTwistMin = DEFAULT_mgTwistMin ;
    else
      read1double ( &mgTwistMin ) ;
  }

  else if ( !strncmp ( keyword, "mg-vol", 5 ) ) {
    /* Set a new minimum volumetric aspect ratio. */
    if ( eo_buffer () )
      /* Reset. */
      mgVolAspect = DEFAULT_mgVolAspect ;
    else
      read1double ( &mgVolAspect ) ;
  }

  else if ( !strncmp ( keyword, "mg-aspectRatio", 5 ) ) {
    /* What do we consider stretched in order to apply semi-coarsening? */
    if ( eo_buffer () )
      /* Reset. */
      mgArCutoff = DEFAULT_mgArCutoff ;
    else
      read1double ( &mgArCutoff ) ;
    mgArCutoff2 = mgArCutoff*mgArCutoff ;
  }

  else if ( !strncmp ( keyword, "mg-ramp", 5 ) ) {
    /* A factor to ramp lrgstAngle and aspect ratio. */
    if ( eo_buffer () )
      /* Reset. */
      mgRamp = DEFAULT_mgRamp ;
    else {
      read1double ( &mgRamp ) ;
    }
  }




  /* Multi-block options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "mb-degen", 5 ) ) {
    /* Find degenerate block faces? */
    if ( eo_buffer () )
      /* Reset. */
      find_mbDegenFaces = DEFAULT_find_mbDegenFaces ;
    else
      read1int ( &find_mbDegenFaces ) ;
  }


  /* Edge options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "no-angle", 5 ) ) {
    /* Cosine for setting node normals to zero. */
    if ( eo_buffer () )
      /* Reset. */
      normAnglCut = DEFAULT_normAnglCut ;
    else
      read1double ( &normAnglCut ) ;
  }


  else if ( !strncmp ( keyword, "no-single", 5 ) ) {
    /* Switch to list one normal for each boundary node. */
    if ( eo_buffer () )
      /* Reset. */
      singleBndVxNormal = DEFAULT_singleBndVxNormal ;
    else {
      read1int ( &singleBndVxNormal ) ;
      /* Legal range of values is currently 0,1,2. */
      singleBndVxNormal = MAX( 0, singleBndVxNormal ) ;
      singleBndVxNormal = MIN( 2, singleBndVxNormal ) ;
    }
  }

  /* Face options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "fc-warn", 5 ) )
    /* Duplicate face warnings. */
    if ( eo_buffer () ) {
      /* No argument, reset. */
      doWarn.matchFc = DEFAULT_doWarn_matchFc ;
      doWarn.intFc   = DEFAULT_doWarn_intFc   ;
      doWarn.bndFc   = DEFAULT_doWarn_bndFc   ;
      doWarn.abortFc   = DEFAULT_doWarn_abortFc   ;
    }
    else {
      read1int ( &doWarn.matchFc ) ;
      read1int ( &doWarn.intFc ) ;
      read1int ( &doWarn.bndFc ) ;
      if ( !eo_buffer () )
        read1int ( &doWarn.abortFc ) ;
    }

  else if ( !strncmp ( keyword, "fc-remove", 5 ) )
    /* Duplicate face removal. */
    if ( eo_buffer () ) {
      /* No argument, reset. */
      doRemove.matchFc = DEFAULT_doRemove_matchFc ;
      doRemove.intFc   = DEFAULT_doRemove_intFc   ;
      doRemove.bndFc   = DEFAULT_doRemove_bndFc   ;
      doRemove.listUnMatchedFc   = DEFAULT_doRemove_listUnMatchedFc   ;
    }
    else {
      read1int ( &doRemove.matchFc ) ;
      read1int ( &doRemove.intFc ) ;
      read1int ( &doRemove.bndFc ) ;
      if ( !eo_buffer () )
        read1int ( &doRemove.listUnMatchedFc ) ;
    }

  /* (Element) Degeneracy options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "dg-coll", 5 ) )
    /* Fix or leave degenerate elements with collapsed edges. */
    if ( eo_buffer () )
      /* No argument, reset. */
      fix_degenElems = DEFAULT_fix_degenElems ;
    else {
      read1int ( &fix_degenElems ) ;
      if ( fix_degenElems )
	fix_degenElems = 1 ;
    }


  else if ( !strncmp ( keyword, "dg-lrg", 5 ) )
    /* Fix or leave degenerate elements with excessive angles. */
    if ( eo_buffer () )
      /* No argument, reset. */
      dg_fix_lrgAngles = DEFAULT_dg_fix_lrgAngles ;
    else {
      read1int ( &dg_fix_lrgAngles ) ;
      if ( dg_fix_lrgAngles )
	dg_fix_lrgAngles = 1 ;
    }

  else if ( !strncmp ( keyword, "dg-angle", 5 ) )
    /* Fix or leave degenerate elements. */
    if ( eo_buffer () )
      /* No argument, reset. */
      dg_lrgAngle = DEFAULT_dg_lrgAngle ;
    else
      read1double ( &dg_lrgAngle ) ;


  /* Periodicity options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "pe-write", 5 ) )
    /* Write periodic boundaries to the exBound AVBP file?. */
    if ( eo_buffer () )
      /* No argument, reset. */
      perBc_in_exBound = DEFAULT_perBc_in_exBound ;
    else {
      read1int ( &perBc_in_exBound ) ;
      if ( perBc_in_exBound ) perBc_in_exBound = 1 ;
    }

  else if ( !strncmp ( keyword, "pe-corners", 5 ) ) {
    if ( !Grids.PcurrentGrid ) {
      sprintf ( hip_msg, "there is no grid to have periodic patches.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }

    else if ( Grids.PcurrentGrid->uns.type != uns ) {
      sprintf ( hip_msg, "only unstructured grids can have periodic patches.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }
    else {
      mDim = Grids.PcurrentGrid->uns.mDim ;

      /* Read a boundary name string. */
      read1string ( bc_name ) ;
      /* Read a list of 6 sets of coordinates that define how periodic patches match. */
      for ( nCoor = 0 ; nCoor < 2*mDim*mDim ; nCoor++ )
	read1double ( coor + nCoor ) ;

      /* supply a zero for reverse_rot, as this can't be detected
         when specifying local coor sys. by hand. */
      set_per_corners ( Grids.PcurrentGrid->uns.pUns, bc_name, coor, 0 ) ;
    }
  }

  else if ( !strncmp ( keyword, "pe-th", 5 ) ) {
    if ( eo_buffer () )
      /* Reset. */
      per_thresh_is_rot = DEFAULT_per_thresh_is_rot ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        per_thresh_is_rot = DEFAULT_per_thresh_is_rot ;
        sprintf ( hip_msg, " WARNING: invalid value %g, using default %g instead.\n",
                 per_thresh_is_rot, DEFAULT_per_thresh_is_rot ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        per_thresh_is_rot = dVal ;
    }
  }


  else if ( !strncmp ( keyword, "pe-rot", 5 ) ) {
    if ( !Grids.PcurrentGrid ) {
      sprintf ( hip_msg, "there is no grid to have periodic patches.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }
    else if ( Grids.PcurrentGrid->uns.type != uns ) {
      sprintf ( hip_msg, "only unstructured grids can have periodic patches.\n" ) ;
      hip_err ( warning, 0, hip_msg ) ;
    }
    else {
      mDim = Grids.PcurrentGrid->uns.mDim ;

      /* Read a boundary name string. */
      read1string ( bc_name ) ;
      /* Read a rotation axis string, x,y,z. */
      read1string ( axis ) ;
      /* Read a rotation angle in degrees. */
      read1double ( &rotAngle ) ;

      set_per_rotation ( Grids.PcurrentGrid->uns.pUns,
                         bc_name, axis, rotAngle ) ;
    }
  }

  else if ( !strncmp ( keyword, "pe-fix", 5 ) )
    /* Fix periodic vertices with a mismatch?. */
    if ( eo_buffer () )
      /* No argument, reset. */
      Grids.fixPerVx = DEFAULT_fixPerVx ;
    else {
      read1int ( &Grids.fixPerVx ) ;
      if (Grids.fixPerVx ) Grids.fixPerVx = 1 ;
    }


  /* Adaptivity options
     --------------------------------------------------------------- */


  else if  ( !strncmp ( keyword, "cr", 2 ) ) {
    /* Chord length. */
    if ( eo_buffer () )
      /* Reset. */
      chord = 1. ;
    else
      read1double ( &chord ) ;
  }

  else if ( !strncmp ( keyword, "ad-lv", 5 ) ) {
    /* Max depth of adaptation levels. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.adapt.maxLevels = 999 ;
    else {
      read1int ( &iVal ) ;
      Grids.adapt.maxLevels = MAX( 0, iVal ) ;
    }
  }


  else if ( !strncmp ( keyword, "ad-up", 5 ) ) {
    /* Upgrade to the next pattern if this fraction of edges is already
       marked.?. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.adapt.upRef = 1. ;
    else {
      read1double ( &dVal ) ;
      Grids.adapt.upRef = MAX( 0., dVal ) ;
    }
  }



  else if ( !strncmp ( keyword, "ad-per", 5 ) ) {
    /* Perform periodic adaptation. */
    if ( eo_buffer () )
      /* Reset. */
      Grids.adapt.doPer = 1 ;
    else {
      read1int ( &iVal ) ;
      Grids.adapt.doPer = iVal ;
    }
  }


  /* Interpolation options
     --------------------------------------------------------------- */

  else if ( !strncmp ( keyword, "in-recoType", 5 ) ) {
    /* Reco type. */
    if ( eo_buffer () )
      /* Reset. */
      reco = DEFAULT_reco ;
    else {
      read1string ( keyword ) ;
      if ( !strncmp ( keyword, "el", 2 ) )
        reco = reco_el ;
      else if ( !strncmp ( keyword, "minnorm", 2 ) )
        reco = reco_min_norm ;
      else if ( !strncmp ( keyword, "1", 1 ) )
        reco = reco_1 ;
      else if ( !strncmp ( keyword, "2", 1 ) )
        reco = reco_2 ;
       else if ( !strncmp ( keyword, "flag", 1 ) )
        reco = reco_flag ;
     else {
        sprintf ( hip_msg,
                 "unrecognised reconstruction type %s, using el instead.\n",
                 keyword ) ;
        hip_err ( warning, 1, hip_msg ) ;
        reco = DEFAULT_reco ;
      }
    }
  }


  else if ( !strncmp ( keyword, "in-nr", 5 ) ) {
    /* Nr of vx in reco stencil. */
    if ( eo_buffer () )
      /* Reset. */
      mVxRecoFactor = DEFAULT_mVxRecoFactor ;
    else {
      read1double ( &dVal ) ;
      if ( dVal > 3. ) {
        mVxRecoFactor = DEFAULT_mVxRecoFactor ;
        sprintf ( hip_msg, " WARNING: invalid value %g, using default %g instead.\n",
                 mVxRecoFactor, DEFAULT_mVxRecoFactor ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        mVxRecoFactor = dVal ;
    }
  }

  else if ( !strncmp ( keyword, "in-la", 5 ) ) {
    /* Weighting factor. */
    if ( eo_buffer () )
      /* Reset. */
      ls_lambda = DEFAULT_ls_lambda ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        ls_lambda = DEFAULT_ls_lambda ;
        sprintf ( hip_msg, " WARNING: invalid value %g, using default %g instead.\n",
                 ls_lambda, DEFAULT_ls_lambda ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        ls_lambda = dVal ;
    }
  }
  else if ( !strncmp ( keyword, "in-tol", 5 ) ) {
    /* Singular pivot detection. */
    if ( eo_buffer () )
      /* Reset. */
      choldc_tol = DEFAULT_choldc_tol ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        choldc_tol = DEFAULT_choldc_tol ;
        sprintf ( hip_msg, " WARNING: invalid value %g, using default %g instead.\n",
                 choldc_tol, DEFAULT_choldc_tol ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        choldc_tol = dVal ;
    }
  }
  else if ( !strncmp ( keyword, "in-rim", 5 ) ) {
    /* Declare the width of the rim for el interpolation outside the domain. */
    if ( eo_buffer () )
      /* Reset. */
      intPolRim = DEFAULT_intPolRim ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        intPolRim = DEFAULT_intPolRim ;
        sprintf ( hip_msg, " WARNING: invalid value %g, using default %g instead.\n",
                 dVal, intPolRim ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        intPolRim = dVal ;
    }
  }
  else if ( !strncmp ( keyword, "in-dist", 5 ) ) {
    /* Declare the width of the rim for l-s interpolation outside the domain. */
    if ( eo_buffer () )
      /* Reset. */
      intPol_dist_inside = DEFAULT_intPol_dist_inside ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        intPol_dist_inside = DEFAULT_intPol_dist_inside ;
        sprintf ( hip_msg, " WARNING: invalid value %g, using default %g instead.\n",
                 dVal, intPol_dist_inside ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        intPol_dist_inside = dVal ;
    }
  }
  else if ( !strncmp ( keyword, "in-fc-tol", 5 ) ) {
    /* Declare the width of the rim for el interpolation outside the domain. */
    if ( eo_buffer () )
      /* Reset. */
      intPolRim = DEFAULT_intFcTol ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        intPolRim = DEFAULT_intFcTol ;
        sprintf ( hip_msg, " invalid value %g, using default %g instead.\n",
                 dVal, intFcTol ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        intFcTol = dVal ;
    }
  }
  else if ( !strncmp ( keyword, "in-full", 5 ) ) {
    /* Declare the width of global searches outside the domain. */
    if ( eo_buffer () )
      /* Reset. */
      intFullTol = DEFAULT_intFullTol ;
    else {
      read1double ( &dVal ) ;
      if ( dVal < 0. ) {
        intFullTol = DEFAULT_intFullTol ;
        sprintf ( hip_msg, " invalid value %g, using default %g instead.\n",
                  dVal, intFullTol ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        intFullTol = dVal ;
    }
  }




  /* Listing
     --------------------------------------------------------------- */

  else if ( keyword[0] == '\0' && verbosity > 0 ) {
    /* no argument. */
    if ( Grids.PcurrentGrid )
      printf ( "  current grid:   %d\n", Grids.PcurrentGrid->uns.nr ) ;
    else
      printf ( "  no grids.\n" ) ;

    printf ( "  path:           %s\n", Grids.path ) ;
    printf ( "  verbosity:      %d\n",   verbosity ) ;
    printf ( "  epsOverlap:     %g\n",   Grids.epsOverlap ) ;
    printf ( "  symmetry (y=1): %d\n",   symmCoor  ) ;
    printf ( "  check level:    %d\n\n", check_lvl ) ;

    printf ( "  fc-warning: warn for duplicated matching/cut/boundary faces, abort:"
             " %d/%d/%d/%d\n", doWarn.matchFc, doWarn.intFc, doWarn.bndFc, doWarn.abortFc ) ;
    printf ( "  fc-remove:  remove duplicated matching/cut/boundary faces, list:"
             "   %d/%d/%d/%d\n", doRemove.matchFc, doRemove.intFc,
             doRemove.bndFc, doRemove.listUnMatchedFc ) ;
    printf ( "  el-degen:   fix degenerete elements:     %d\n\n", fix_degenElems ) ;
    printf ( "  vo-abort:  abort upon negative vol:     %d\n\n", negVol_abort ) ;
    printf ( "  vo-swap:   swap negative vol:     %d\n\n", negVol_flip ) ;

    printf ( "  dg-coll:    collapsed elements:   %d\n", fix_degenElems ) ;
    printf ( "  dg-fix: fix large angles          %d\n", dg_fix_lrgAngles ) ;
    printf ( "  dg-angle: lrg angle threshold:    %g\n\n", dg_lrgAngle ) ;

    printf ( "  mb-degenFace:   %d\n\n", find_mbDegenFaces ) ;

    printf ( "  adapt: maxLevel %d\n", Grids.adapt.maxLevels ) ;
    printf ( "  adapt: upRef    %g\n\n", Grids.adapt.upRef ) ;

    printf ( "  pe-write:       %d\n\n", perBc_in_exBound ) ;
    printf ( "  pe-threshold:   %g\n\n", per_thresh_is_rot ) ;
    printf ( "  pe-fix:         %d\n\n", Grids.fixPerVx ) ;

    printf ( "  mg-length:      %g\n",   mgLen ) ;
    printf ( "  mg-angle:       %g\n",   mgLrgstAngle ) ;
    printf ( "  mg-volAspect:   %g\n",   mgVolAspect ) ;
    printf ( "  mg-twist:       %g\n",   mgTwistMin ) ;
    printf ( "  mg-aspectRatio: %g\n\n",   mgArCutoff ) ;

    printf ( "  norm cutoff:    %g\n",   normAnglCut ) ;
    printf ( "  single normal:  %d\n\n", singleBndVxNormal ) ;

    /* printf ( "  lp_tolerance:   %g\n", Grids.lp_tolerance ) ;
       printf ( "  chord:          %g\n\n", chord ) ; */
    printf ( "  in-reco:        %s\n", recoString[reco] ) ;
    printf ( "  in-nr:          %g\n", mVxRecoFactor ) ;
    printf ( "  in-rim:         %g\n", intPolRim ) ;
    printf ( "  in-epso:        %g\n\n", intPol_dist_inside ) ;
    printf ( "  in-fc-tol:      %g\n", intFcTol ) ;
    printf ( "  in-full-tol:    %g\n", intFullTol ) ;
    printf ( "  in-tol:         %g\n\n", choldc_tol ) ;
    printf ( "  in-lambda:      %g\n\n", ls_lambda ) ;
  }

  else {
    sprintf ( hip_msg, " set needs either no argument, or an argument of\n"
	     "    [current, gridname, path, prompt, verbosity, epsOverlap, check,\n"
	     "     bc-type, bc-text, bc-compress\n"
	     "     mg-length, mg-angle, mg-aspectRatio, mg-dir, mg-semi,\n"
	     "     mb-degenFace,\n"
             "     no-cutoff, no-single\n"
             "     pe-write, pe-corner, fc-warning, fc-remove, el-degen,\n"
             "     dg-collapse, dg-lrg, dg-angle\n"
	     "     lp_tolerance,\n"
             "     in-recoType, in-nrInStencil, in-lambda].\n" ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }

  flush_buffer () ;
  return (ret) ;
}



/******************************************************************************

  var_menu:
  .

  Last update:
  ------------
  26Apr15; track bad keywords, intro setting type directly.
  6Apr13; fix bug with uinitialised mEq.
  22Mar10; allow setting of freestream vars w/o a solution for subsequ. init.
  4Apr09; allow number ranges for 'va cat', but use a single no for 'va na'.
  3Apr06; did va name ever work?
  29Apr02; add variable name manipulation.
  25Apr97: conceived.

  Changes To:
  -----------


  Returns:
  --------
  0 on failure, 1 on success.

*/

/* hip_cython */
ret_s var_menu ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  int found_op = 0 ;
  varList_s *pVarList ;
  grid_struct *Pgrid = Grids.PcurrentGrid ;
  uns_s *pUns ;
  char keyword[LINE_LEN], name[LINE_LEN], range[LINE_LEN] ;
  varType_e newVarType, oldVarType ;
  double freeStreamVar[MAX_UNKNOWNS], someDbl ;
  int mEq, nEq, nr ;

  if ( eo_buffer () && verbosity > 0 ) {
    found_op = 1 ;
    /* Default. Show variable type and freestream values. */
    hprintf ( "  gamma:          %g\n", Gamma ) ;
    hprintf ( "  R:              %g\n", R ) ;


    /* All other variable operations need an existing grid/solution. */
    if ( !( Pgrid  = Grids.PcurrentGrid ) ) {
      hip_err ( warning, 1, "  no grid, no variables.\n" ) ;
    }

    else {
      pVarList = Pgrid->uns.pVarList ;
      oldVarType = pVarList->varType ;
      mEq = pVarList->mUnknowns ;
      hprintf ( "  %d variables of type: %s\n",
                mEq, varTypeNames[oldVarType] ) ;

      if ( mEq ) {
        hprintf ("     reference/freestream values: " ) ;
        for ( nEq = 0 ; nEq < mEq ; nEq++ )
          hprintf ( " %g,", pVarList->freeStreamVar[nEq] ) ;
        hprintf ( "\n" ) ;


        if ( oldVarType != noVar && Pgrid->uns.type == uns )
          min_max_var ( Pgrid->uns.pUns ) ;
      }
    }

    flush_buffer() ;
    return (ret) ;
  }


  keyword[0] = '\0' ;
  read1lostring ( keyword ) ;

  /* These operations don't require a grid. */
  if ( !strncmp( keyword, "gamma", 2 ) ) {
    found_op = 1 ;
    /* Set a new gamma. */
    if ( !eo_buffer () ) {
      read1double ( &someDbl ) ;

      if ( someDbl < 1. || someDbl > 4. ) {
        sprintf ( hip_msg,
                  "invalid value for gamma: %g, ignored. 1<gamma<4.\n",
                  someDbl ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else {
        Gamma = someDbl ;
        GammaM1 = Gamma-1. ;
      }
    }
    else {
      /* Default. */
      Gamma = 1.4 ;
      GammaM1 = Gamma-1. ;
    }
    flush_buffer() ;
    return (ret) ;
  }


  else if ( !strncmp( keyword, "r", 1 ) ) {
    found_op = 1 ;
    /* Set a new R. */
    if ( !eo_buffer () ) {
      read1double ( &someDbl ) ;

      if ( someDbl < 10. || someDbl > 1000. ) {
        sprintf ( hip_msg,
                 "invalid value for R: %g, ignored. Use var r.", someDbl ) ;
        hip_err ( warning, 0, hip_msg ) ;
      }
      else
        R = someDbl ;
    }
    else
      /* Default. */
      R = 287. ;
    flush_buffer() ;
    return (ret) ;
  }

  else if ( !strncmp( keyword, "init", 2 ) ) {
    found_op = 1 ;
    /* Initialize. Eg. according to geometry. */
    strcpy ( keyword, "nil" ) ;
    if ( !eo_buffer () )
      read1lostring ( keyword ) ;

    
    someDbl = 1. ;
    if ( !eo_buffer () )
      read1double ( &someDbl ) ;


    init_uns_var ( Pgrid->uns.pUns, keyword, someDbl ) ;
    return (ret) ;
  }


  /* The following operations do require an unstructured grid. */
  if (!Pgrid ) {
    sprintf ( hip_msg, "there is no grid." ) ;
    hip_err ( warning, 0, hip_msg ) ;

    flush_buffer() ;
    return ( ret ) ;
  }
  else if ( Pgrid->uns.type != uns ) {
    sprintf ( hip_msg, "cannot transform variables on this type of grid." ) ;
    hip_err ( warning, 0, hip_msg ) ;
    flush_buffer() ;
    return (ret) ;
  }
  pUns = Pgrid->uns.pUns ;
  pVarList = &pUns->varList ;
  mEq = pVarList->mUnknowns ;

  if ( !strncmp( keyword, "freestream", 2 ) ) {
    found_op = 1 ;
    /* Read a new set of freestream values. */
    for ( nEq = 0 ; nEq < MIN( MAX( pUns->mDim+2, mEq ), MAX_UNKNOWNS ) ; nEq++ )
      read1double ( freeStreamVar+nEq ) ;
    set_uns_freestream ( Pgrid->uns.pUns, freeStreamVar ) ;
    flush_buffer () ;
    return (ret) ;
  }


  /* The following operations do require variables. */
  if ( !mEq ) {
    sprintf ( hip_msg, "This grid has no variables,"
              " variable command ignored." ) ;
    hip_err ( warning, 1, hip_msg ) ;
    flush_buffer () ;
    return (ret) ;
  }

  if ( !strncmp( keyword, "name", 2 ) || !strncmp( keyword, "cat", 2 ) ) {
    found_op = 1 ;
    read1string ( range ) ;
    read1string ( name ) ;
    set_var_name_cat_range ( pVarList, keyword, range, name ) ;
  }

  else if ( !strncmp( keyword, "type", 2 ) ) {
    found_op = 1 ;
    strcpy ( keyword, "noType" ) ;
    if ( !eo_buffer () )
      read1lostring ( keyword ) ;

    if ( !strncmp( keyword, "cons", 2 ) )
      pUns->varList.varType = cons ;
    else if ( !strncmp( keyword, "primT", 5 ) )
      pUns->varList.varType = primT ;
    else if ( !strncmp( keyword, "prim", 2 ) )
      pUns->varList.varType = prim ;
    else if ( !strncmp( keyword, "para", 2 ) )
      pUns->varList.varType = para ;
    else if ( !strncmp( keyword, "noType", 2 ) )
      pUns->varList.varType = noType ;
    else {
      sprintf ( hip_msg, "unknown variable type `%s', ignored.", keyword ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }

  else if ( !strncmp( keyword, "convert", 2 ) ) {
    found_op = 1 ;
    /* Convert variables to another set. Default: conservative. */
    strcpy ( keyword, "cons" ) ;
    if ( !eo_buffer () )
      read1lostring ( keyword ) ;

    if ( !strncmp( keyword, "cons", 2 ) )
      newVarType = cons ;
    else if ( !strncmp( keyword, "prim", 2 ) )
      newVarType = prim ;
    else if ( !strncmp( keyword, "para", 2 ) )
      newVarType = para ;
    else
      newVarType = noType ;

    conv_uns_var ( Pgrid->uns.pUns, newVarType ) ;
  }


  else if ( !strncmp( keyword, "flag", 2 ) ) {
    found_op = 1 ;
    /* Flag certain variables for specific i/o */
    nr = 1 ;
    if ( !eo_buffer () )
      read1int ( &nr ) ;

    range[0] = '\0' ;
    if ( !eo_buffer () )
      read1string ( range ) ;

    set_var_flag_range ( pVarList, nr, range ) ;
  }

  else if ( !strncmp( keyword, "vec", 2 ) ) {
    found_op = 1 ;
    /* Flag certain variables for specific i/o */
    nr = 1 ;
    int isVec = 0 ;
    if ( !eo_buffer () )
      read1int ( &nr ) ;

    if ( !eo_buffer () )
      read1int ( &isVec ) ;

    set_var_vec ( pVarList, nr, isVec ) ;
  }



  else if ( !strncmp( keyword, "name", 2 ) ) {
    found_op = 1 ;
    /* Variable names need to be distinct, so only allow a single number. */
    read1int ( &nr ) ;
    sprintf ( range, "%d", nr ) ;
    read1string ( name ) ;
    set_var_name_cat_range ( pVarList, keyword, range, name ) ;
  }



  else if ( !strncmp( keyword, "scale", 2 ) ) {
    found_op = 1 ;
    /* Variable names need to be distinct, so only allow a single number. */
    if ( !eo_buffer () ) {
      read1int ( &nr ) ;
      if ( !eo_buffer () ) {
        read1double ( &someDbl ) ;
        double valMin, valMax ;
        mult_uns_var_scal ( pUns, nr-1, someDbl, nr-1, &valMin, &valMax ) ;
      }
      else {
        hip_err ( warning, 1, "no scaling given, scaling command ignored." ) ;
      }
    }
    else {
      hip_err ( warning, 1, "no var no given, scaling command ignored." ) ;
    }
  }

  if ( !found_op ) {
    sprintf ( hip_msg, "keyword %s not known or not applicable"
              " to this mesh, ignored.", keyword ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }


  flush_buffer () ;
  return (ret) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_bc_*:
*/
/*! set bc attributes
 */

/*

  Last update:
  ------------
  14May20: extracted from set_menu.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/



ret_s set_bc_text_arg (  ) {
#undef FUNLOC
#define FUNLOC "in set_bc_text_arg"

  ret_s ret = ret_success () ;
  /* Assign a name to a bc.*/

  char expr[LINE_LEN ] ;
  read1string ( expr ) ;
  char bc_name[LINE_LEN ] ;
  read1string ( bc_name ) ;

  ret = set_bc_text ( expr, bc_name ) ;

  return ( ret ) ;
}


ret_s set_bc_type_arg (  ) {
#undef FUNLOC
#define FUNLOC "in set_bc_type_arg"

  ret_s ret = ret_success () ;
  /* Assign a type to a bc.*/
  char bcTypeStr[MAX_BC_CHAR] ;
  char expr[MAX_BC_CHAR] ;

  if ( eo_buffer () ) {
    /* No argument. Erase all output boundary types. */
    expr[0]='\0' ;
  }
  else
    read1string ( expr ) ;

  if ( eo_buffer () ) {
    /* No argument. Set default type. */
    bcTypeStr[0]='\0' ;
  }
  else
    read1string ( bcTypeStr ) ;


  ret = set_bc_type ( expr, bcTypeStr ) ;

  return ( ret ) ;
}

ret_s set_bc_mark_arg (  ) {
#undef FUNLOC
#define FUNLOC "in set_bc_mark_arg"

  ret_s ret = ret_success () ;
  /* Assign a type to a bc.*/
  char expr[MAX_BC_CHAR] ;
  int bcMarkInt ;

  if ( eo_buffer () ) {
    /* No argument. Erase all marks. */ 
    expr[0]='\0' ;
  }
  else
    read1string ( expr ) ;

  if ( eo_buffer () ) {
    /* No argument. Set default type. */
    bcMarkInt=1 ; 
  }
  else
    read1int ( &bcMarkInt ) ;


  ret = set_bc_mark ( expr, &bcMarkInt ) ;

  return ( ret ) ;
}


ret_s set_bc_order_arg (  ) {
#undef FUNLOC
#define FUNLOC "in set_bc_order_arg"

  ret_s ret = ret_success () ;

  int mBc ;
  bc_struct *pBc ;
  int found = 0 ;
  if ( eo_buffer () ) {
    /* No argument. Reset. */
    for ( mBc = 0, pBc = find_bc ( "", 0 ) ; pBc ; pBc = pBc->PnxtBc )
      pBc->order = ++mBc ;
    if ( Grids.PcurrentGrid->uns.type == uns )
      make_uns_ppBc( Grids.PcurrentGrid->uns.pUns ) ;
  }
  else {
    /* Read tuples of number and order.
       Note: doesn't make much sense to use ranges, but we
       want to allow text recognition. */
    while ( !eo_buffer () ) {
      char expr[LINE_LEN] ;
      read1string ( expr ) ;
      int isText = expr_is_text ( expr ) ;

      int bcOrder ;
      if ( !eo_buffer () )
        read1int ( &bcOrder ) ;
      else
        /* Hmm, what should be the default behaviour? */
        bcOrder = 1 ;

      /* Loop over the matching bcs. */
      for ( pBc = NULL, found = 0 ; loop_bc_expr( &pBc, expr ) ; ) {
        found = 1 ;

        pBc->order = bcOrder ;
      }
    }
  }

  if ( !found ) {
    sprintf ( hip_msg, "no matching boundary condition found." ) ;
    hip_err ( warning, 0, hip_msg ) ;
  }
  else if ( Grids.PcurrentGrid->uns.type == uns ) {
    make_uns_ppBc( Grids.PcurrentGrid->uns.pUns ) ;
  }

  return ( ret ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  set_hyvol:
*/
/*! define a hyperplane or hypervolume
 *
 */

/*

  Last update:
  ------------
  14May20: conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

ret_s set_hyvol (  ) {
#undef FUNLOC
#define FUNLOC "in set_hyvol"

  ret_s ret = ret_success () ;

  char keyword[LINE_LEN] ;
  if ( eo_buffer () )
    keyword[0] = '\0' ;
  else
    read1lostring ( keyword ) ;

  uns_s *pUns = Grids.PcurrentGrid->uns.pUns ;
  if ( !pUns )
    return ( hip_err ( warning, 1, "hyperplanes/volumes need to be attached to an unstructured grid,"
                       "           but there isn't any. No hyper added." ) ) ;


  geo_s *pHyVol = pUns->pHyVol ;
  const int mDim = pUns->mDim ;
  pUns->mHyVol = 1 ;
  if ( keyword[0] == '\0' ) {
    // empty command, reset.
    pUns->mHyVol = 0 ;
    pHyVol->noGeo.type = noGeo ;
    return ( ret ) ;
  }

  if ( keyword[0] == 'b' ) {
    // overwrite as box
    pHyVol->box.type = box ;
    pHyVol->box.ll[0] = 0. ;
    pHyVol->box.ll[1] = 0. ;
    pHyVol->box.ll[2] = 0. ;
    pHyVol->box.ur[0] = 1. ;
    pHyVol->box.ur[1] = 1. ;
    pHyVol->box.ur[2] = 1. ;

    int kDim, kul ;
    for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
      if ( eo_buffer () ) {
        break ;
      }
      read1double ( pHyVol->box.ll+kDim ) ;
    }

    if ( kDim == mDim ) {
      /* More to read for upper right. */
      for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
        if ( eo_buffer () ) {
          break ;
        }
        read1double ( pHyVol->box.ur+kDim ) ;
      }
    }

    return ( ret ) ;
  }

  if ( keyword[0] == 'p' ) {
    // overwrite as plane
    pHyVol->plane.type = plane ;
    pHyVol->plane.norm[0] = 1. ;
    pHyVol->plane.norm[1] = 1. ;
    pHyVol->plane.norm[2] = 1. ;
    pHyVol->plane.loc[0] = 0. ;
    pHyVol->plane.loc[1] = 0. ;
    pHyVol->plane.loc[2] = 0. ;

    int kDim, kul ;
    for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
      if ( eo_buffer () ) {
        break ;
      }
      read1double ( pHyVol->plane.norm+kDim ) ;
    }

    if ( kDim == mDim ) {
      /* More to read for upper right. */
      for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
        if ( eo_buffer () ) {
          break ;
        }
        read1double ( pHyVol->plane.loc+kDim ) ;
      }
    }

    return ( ret ) ;
  }

  if ( keyword[0] == 'c' ) {
    // overwrite as cylinder
    pHyVol->cyl.type = cyl ;
    pHyVol->cyl.rad = 1. ;
    pHyVol->cyl.axis[0] = 1. ;
    pHyVol->cyl.axis[1] = 0. ;
    pHyVol->cyl.axis[2] = 0. ;
    pHyVol->cyl.loc[0] = 0. ;
    pHyVol->cyl.loc[1] = 0. ;
    pHyVol->cyl.loc[2] = 0. ;

    if ( !eo_buffer () )
      read1double ( &pHyVol->cyl.rad ) ;
    int kDim ;
    for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
      if ( eo_buffer () ) {
        break ;
      }
      read1double ( pHyVol->cyl.axis+kDim ) ;
    }
    if ( kDim == mDim ) {
      /* More to read for origin. */
      for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
        if ( eo_buffer () ) {
          break ;
        }
        read1double ( pHyVol->cyl.loc+kDim ) ;
      }

      return ( ret ) ;
    }
  } // c


  if ( keyword[0] == 's' ) {
    // overwrite as sphere
    pHyVol->sph.type = sphere ;
    pHyVol->sph.rad = 1. ;
    pHyVol->sph.loc[0] = 0. ;
    pHyVol->sph.loc[1] = 0. ;
    pHyVol->sph.loc[2] = 0. ;

    if ( !eo_buffer () )
      read1double ( &pHyVol->sph.rad ) ;

    int kDim ;
    for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
      if ( eo_buffer () ) {
        break ;
      }
      read1double ( pHyVol->sph.loc+kDim ) ;
    }

    return ( ret ) ;
  }

  pUns->mHyVol = 0 ;
  sprintf ( hip_msg, "unrecognised hypervolume/plane shape %s "FUNLOC". No hyper added.",
            keyword) ;
  return ( hip_err ( warning, 1, hip_msg ) ) ;
}
