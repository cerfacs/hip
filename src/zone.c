/*
  zone.c:
*/

/*! utilities to manage zones on unstructured grids.
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  1Oct17; intro zone_elem_mod_bclayer, interface only so far.  
  8Sep17; intro zone_list_match.
  12Dec13; intro variable solParams to be written to the solution file.
  9Jul13; allow identifying zones by name
  17Dec11; intro zone_elem routines, zn_recount.
  18Jul11; v1.0 working.
  10Jul11; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#include "fnmatch.h"

#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;
extern const int  parTypeSize[] ;




/******************************************************************************
  zn_match_expr:   */

/*! match zones based on numerical range or name/label matching.
 */

/*
  
  Last update:
  ------------
  9Jul18; rename zn_match to zn_match_expr
  9Jul13: conceived.
  

  Input:
  ------
  pUns
  iZone: number of the zone 1 <= iZone <= mZones.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zn_match_expr( uns_s *pUns, int iZone, const char expr[] ) {

  if ( iZone == 0 || iZone > pUns->mZones )
    /* No such zone. */ 
    return ( 0 ) ;
  else if ( !pUns->pZones[iZone] ) 
    /* Deleted zone. */
    return ( 0 ) ;
  
  if ( expr[0] == '-'  ) {
    /* Epression starts with a -, such as a neg. number, matches most recent. */
    if ( iZone == pUns->mZones )
      return ( 1 ) ;
  }
  else if ( !fnmatch( expr, pUns->pZones[iZone]->name, 0 ) )
    /* text match. */
    return ( 1 ) ;
  else if ( num_match( iZone, expr ) )
    /* number match. */
    return ( 1 ) ;

  /* No match. */
  return ( 0 ) ;
}


/******************************************************************************
  zn_expr_to_iZone:   */

/*! given a numeric expr for a zone number, find the first match and check validity
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  6Apr20; correct error msg to list expr, not iZone.
  9Jul18; rename zn_match to zn_match_expr
  12Jul13; switch to 1 <= iZone <= mZones.
  17Dec11: cut out of zn_menu_elem.
  

  Input:
  ------
  pUns
  expr: 
    
  Returns:
  --------
  the number of the first matching zone if a valid one was found, 0 otherwise.
  
*/

int zn_expr_to_iZone ( uns_s *pUns, const char expr[] ) {

  /* Convert expr to a single zone. Use the first valid match. */
  int iZone ;
  for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )   
    if ( zn_match_expr ( pUns,  iZone, expr ) )
      break ;

  /* Is this a valid zone? */
  if ( !pUns->pZones[iZone] ) {
    sprintf ( hip_msg, "invalid zone matching `%s' requested.\n", expr ) ;
    hip_err ( warning, 0, hip_msg ) ;
    return ( 0 ) ;
  }

  return ( iZone ) ;
}


/******************************************************************************
  zn_tag_elems:   */

/*! tag/untag all elements in a grid with a zone indicator or w/in a geo box.
 *
 */

/*
  
  Last update:
  ------------  
  27Jun14; improve interface documentation.
           remove redundant error check statement, no logic path to it.
  4Apr13;  modified interface to loop_elems.
           make all large counters ulong_t.
  1Feb12;  rename uns_flag_vx to uns_flag_vx_geo.
  17Dec11; tidy up, make deletion clearer and limit tag modification 
             case to delete.
  10Jul11: conceived.
  

  Input:
  ------
  pUns     = unstructured grid
  geo      = geometric filter, only tag those elemenst that match the filter
  nSrcZone = zone filter, only tag those elements in this zone.
  iZone    = tag elements into this zone

  Changes To:
  -----------
  pUns-> ... Pelem->iZone  

    
  Returns:
  --------
  number of elements in zone.
  
*/

int zn_tag_elems ( uns_s *pUns, geo_s geo, const int nSrcZone, 
                   const int iZone ) {

  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  ulong_t mElZone = 0 ;


  if ( !nSrcZone ) {
    /* Active flags: tag elements from all zones, but only if "in the box",
       i.e. matching the filter. */
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( pEl->number ) {
          if (  geo.box.type == allGeo ){
            /* Tag all elems. */
            pEl->iZone = iZone ;
            mElZone++ ;
          }
          else if  ( geo.box.type == remaining && !pEl->iZone ) {
            /* Tag remaining untagged elements. */
            pEl->iZone = iZone ;
            mElZone++ ;
          }
          else if ( geo.box.type >= nodeFlagged && el_has_flag1_vx(pEl) ) {
            /* Base selection on flagged vx. */
            pEl->iZone = iZone ;
            mElZone++ ;
          }
        }
  }

  else {
    /* nSrcZone given.
     Alter tags for all elements in nSrcZone. E.g. delete all elems from
     nSrcZone if iZone = 0, or point to new zone if zone has moved number in
     a merge. */
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( pEl->number && pEl->iZone == nSrcZone ) {
          pEl->iZone = iZone ;
          mElZone-- ;
        }
  }


  /* Reset and free node flags. */
  unflag_vx ( pUns ) ;
  free_vx_flag ( pUns ) ;

  return ( mElZone ) ;
}



/******************************************************************************
  zn_ll_insert_param:   */

/*! add a parameter field into the linked list of a zone.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  16Feb17; align is_sol to be isSol.
  12Cec13; intro solParam variant.
  11Jul11: conceived.
  

  Input:
  ------
  pUns
  pZone: zone that hosts the parameter.
  isSol: 0 for params, nonzero for solParams.
    
  Returns:
  --------
  pointer to new parameter.
  
*/

param_s *zn_ll_insert_any_param ( uns_s *pUns, zone_s *pZ, int isSol ) {

  param_s *pPar ;
  if ( isSol )
    pPar = pZ->pSolParam ;
  else    
    pPar = pZ->pParam ;

  if ( pPar )
    while ( pPar->pNxtPar )
      pPar = pPar->pNxtPar ;

  param_s *pNewPar = arr_malloc ( "pParNew in zn_ll_insert_param", pUns->pFam, 
                                  sizeof( param_s ), 1 ) ;
  
  if ( pPar ) 
    pPar->pNxtPar = pNewPar ; 
  else {
    /* Set a new root. */
    if ( isSol )
      pZ->pSolParam = pNewPar ;
    else    
      pZ->pParam = pNewPar ;
  }

  pNewPar->pPrvPar = pPar ; 
  pNewPar->pNxtPar = NULL ; 
  pNewPar->pv = NULL ;
  pNewPar->flag = 0 ;

  return ( pNewPar ) ;
}

// is it a fixed mesh parameter
param_s *zn_ll_insert_param ( uns_s *pUns, zone_s *pZ ) {
  return ( zn_ll_insert_any_param ( pUns, pZ, 0 ) ) ;
}

// or is it a variable sol parameter?
param_s *zn_ll_insert_sol_param ( uns_s *pUns, zone_s *pZ ) {
  return ( zn_ll_insert_any_param ( pUns, pZ, 1 ) ) ;
}


/******************************************************************************
   zn_add_param_data:   */

/*! allocate and fill a data field for a parameter.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  17Jul11: conceived.
  

  Input:
  ------
  pUns: 
  pPar: parameter to add data to
  name: name of parameter
  parType: type
  dim: dimension, (parameters must be linear arrays.)
  pVal: pointer to the array of values.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void zn_add_param_data ( uns_s *pUns, param_s *pPar, const char name[], 
                         parType_e parType, int dim, void *pVal ) {

  if ( !pPar ) 
    hip_err ( fatal, 0, "invalid pointer to parameter in zn_add_param_data." ) ;

  if ( pPar->pv ) {
    /* Overwrite existing data, free all fields. */
    pPar->dim = 0 ;
    arr_free ( pPar->pv ) ;
  }

  pPar->pv = arr_malloc ( "pPar->pv in zn_add_param_data", pUns->pFam, 
                          parTypeSize[parType], dim ) ;

  memcpy ( pPar->pv, pVal,  parTypeSize[parType]*dim ) ;
  pPar->parType = parType ;
  pPar->dim = dim ;
  strncpy( pPar->name, name, LEN_VARNAME ) ;

  return ;
}

/******************************************************************************
  zn_list:   */

/*! list zone information.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  6Sep19; use hprintf.
  11Jul11: conceived.
  

  
*/

void zn_list_print_hdr ( ) {
  hprintf ( "             Zone Nr: zone name,           # of elems\n" ) ;
  return ;
}

void zn_list_print_zhdr ( uns_s *pUns, int iZone ) {
  zone_s *pZ = pUns->pZones[iZone] ;
  if ( !pZ ) 
    return ;

  hprintf ( "                  %3d %-20.20s %d\n", 
             iZone, pZ->name, pZ->mElemsZone ) ;
  //printf ( "\n" ) ;
  return ;
}

void zn_list_param ( param_s *pPar ) {
  /* defined in cpre.h:
  typedef enum { noPar, parInt, parDbl, parVec } parType_e ; */
  const char parTypeName[][4] = { "not\0", "int\0", "dbl\0", "vec\0" } ;
  int i ;

  /* zn_list_param is only called in \n_list_print_zall, after
     the output buffer has been already started. hence use hprintf.
  */
  hprintf ( "      Parameter: %s, type: %s, dim: %d, value: ", 
             pPar->name, parTypeName[pPar->parType], pPar->dim ) ;   
  
  if ( pPar->parType == parInt ) {
    for ( i = 0 ; i < pPar->dim ; i++ )
      hprintf ( " %i", ((int*)pPar->pv)[i] ) ;
    hprintf ( "\n" ) ;

  }
  else if ( pPar->parType == parDbl ||
            pPar->parType == parVec ) {
    for ( i = 0 ; i < pPar->dim ; i++ )
      hprintf ( " %g", ((double*)pPar->pv)[i] ) ;
    hprintf ( "\n" ) ;
  }
  else
    hip_err ( warning, 1, "unknown parameter type in zn_list_param." ) ;

  return ;
}

int zn_list_print_zall ( uns_s *pUns, int iZone ) {
  zone_s *pZ = pUns->pZones[iZone] ;

  if ( !pZ ) {
    hprintf ( "\n  Zone %d: deleted\n", iZone ) ;
    return ( 0 ) ;
  }
  else
    hprintf ( "\n  Zone %d: %-30s with %d elements:\n", 
              pZ->number, pZ->name, pZ->mElemsZone ) ;

  param_s *pPar = pZ->pParam ;
  hprintf ( "\n     fixed (mesh) parameters:\n" ) ;
  while ( pPar ) {
    zn_list_param ( pPar ) ;
    pPar = pPar->pNxtPar ;
  }

  pPar = pZ->pSolParam ;
  printf ( "\n     variable (solution) parameters:\n" ) ;
  while ( pPar ) {
    zn_list_param ( pPar ) ;
    pPar = pPar->pNxtPar ;
  }

  hprintf ( "\n" ) ;

  return (1) ;
}

/******************************************************************************
  zn_find_param:   */

/*! Find a parameter by name in the linked list of params of a zone.
 *
 */

/*
  
  Last update:
  ------------
  16Feb17; make isSol a fixed input param. Only look in either, not both.
  17Dec13; fix pb with logic.
  12Dec13; intro pSolParam, is_sol
  17Jul11: conceived.
  

  Input:
  ------
  pZ: zone to search the parameter in
  name: name of parameter to find.
  isSol: nonzero if to search only for solParam, zero for param, <0 for any.

  Output;
  -------
  pis_sol: nonzero if the param is a solParam, zero otherwise
    
  Returns:
  --------
  pointer to the parameter if found, NULL otherwise.
  
*/

param_s *zn_find_param ( zone_s *pZ, const char name[], const int isSol ) {

  param_s *pPar = NULL, *pParMatch = NULL ;

  if ( isSol == 0 ) {
    // Mesh parameters
    pPar = pZ->pParam ;
    while ( pPar ) {
      if ( !strcmp( pPar->name, name ) ) {
        //*pis_sol = 0 ;
        pParMatch = pPar ;
        break ;
      }
      else
        pPar = pPar->pNxtPar ;
    }
  }
  else {
    // Sol parameters
    pPar = pZ->pSolParam ;
    while ( pPar ) {
      if ( !strcmp( pPar->name, name ) ) {
        //*pis_sol = 1 ;
        pParMatch = pPar ;
        break ;
      }
      else
        pPar = pPar->pNxtPar ;
    }
  }

  return ( pPar ) ;
}

/******************************************************************************
  zn_str2parType:   */

/*! convert a string to parType enum
 *
 */

/*
  
  Last update:
  ------------
  27Jul11; intro inverste function parType2str
  18Jul11: conceived.
  

  Input:
  ------
  paramType: one of int, dbl, vec
    
  Returns:
  --------
  parType: the corresponding parType_e enum.
  
*/

parType_e zn_str2parType ( const char paramType[] ) {

  if ( !strncmp( paramType, "int", 3 ) ) {
    return ( parInt ) ;
  }
  else if ( !strncmp( paramType, "iarr", 3 ) ) {
    return ( parInt ) ;
  }
  else if ( !strncmp( paramType, "dbl", 3 ) ) {
    return ( parDbl ) ;
  }
  else if ( !strncmp( paramType, "darr", 3 ) ) {
    return ( parDbl ) ;
  }
  else if ( !strncmp( paramType, "vec", 3 ) ) {
    return ( parVec ) ;
  }
  else
    return ( noPar ) ;
}

/******************************************************************************
  fun_name:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  2jul20; remove duplicate case parDbl.
  7Apr14; fix too short string length in strncpy
  : conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void zn_parType2str ( parType_e parType, int isVec, char paramType[] ) {

  if ( parType == parInt )
    strncpy( paramType, "iarr", 5 ) ;
  else if ( parType == parVec )
    strncpy( paramType, "vec", 4 ) ;
  else if ( parType == parDbl && isVec )
    strncpy( paramType, "vec", 4 ) ;
  else if ( parType == parDbl )
    strncpy( paramType, "darr", 5 ) ;
  else
    hip_err ( fatal, 0, "unknown parType in zn_parType2str" ) ;

  return ;
}



/******************************************************************************
  zn_mod:   */

/*! add/modify a zone to an unstructured grid, currently the name. 
 */

/*
  
  Last update:
  ------------
  12Dec12; intro pSolParam
  7Sep12; fix bug with zero-valued iZone in the case of addition.
  17Dec11; no longer automatically associate elements with a modified zone.
  29Aug11; return pZone.
  : conceived.
  

  Input:
  ------
  pUns
  name: zone name
  iZone: zone number to be modified, 0 to add.
  

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

zone_s *zn_mod ( uns_s *pUns, const char name[], int iZone ) {

  zone_s *pZone=NULL ;

  if ( iZone < 0 )
    hip_err ( fatal, 0, "negative zone number in zn_mod\n" ) ;
  else if ( iZone >= MAX_ZONES )
    hip_err ( fatal, 0, "zone number too large in zn_mod\n" ) ;

  else if ( !iZone || !pUns->pZones[iZone] ) {
    /* Add a new zone. */
    if ( pUns->mZones > MAX_ZONES )
      hip_err ( fatal, 0, "too many zones in zn_mod,"
                " increase MAX_ZONES and recompile." ) ;

    if ( iZone )
      /* Allow for empty slots with specified iZone. */
      pUns->mZones = MAX( pUns->mZones, iZone ) ;
    else 
      iZone = ++pUns->mZones ;

    pZone = pUns->pZones[iZone] = arr_malloc ( "pZone in zn_mod", pUns->pFam, 
                                               sizeof( zone_s ), 1 ) ;
    pZone->iZone = pUns->mZones ;

    /* So far no elems, tag elements with a separate command. */
    pZone->mElemsZone = 0 ;
    pZone->pParam = NULL ;
    pZone->pSolParam = NULL ;
    pZone->number = iZone ;
  }

  else if ( iZone > pUns->mZones ) {
    sprintf ( hip_msg, "requested zone %d does not exist in zn_mod.", 
              pUns->mZones ) ;
    hip_err ( warning, 1, hip_msg ) ; 
  }
  else
    /* Return/modify this zone. */
    pZone = pUns->pZones[iZone] ;


  if ( !pZone ) {
    sprintf ( hip_msg, "requested zone %d has been deleted.", iZone ) ;
    hip_err ( warning, 1, hip_msg ) ;
    return ( NULL ) ;
  }

  strncpy ( pZone->name, name, MAX_BC_CHAR ) ;

  return ( pZone ) ;
}

/******************************************************************************
  zn_ll_param_del:   */

/*! remove a parameter field from the linked list of a zone.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  11Jul11: conceived.
  

  Input:
  ------
  pZone: zone that hosts the parameter.
  pPar: parameter to be removed from the linked list.
  is_sol: nonzero if the param is a solParam, zero otherwise.
    
  Returns:
  --------
  pointer to new parameter.
  
*/

void zn_ll_param_del ( zone_s *pZ, param_s *pPar, int is_sol ) {

  param_s *pPrvPar = pPar->pPrvPar ;
  param_s *pNxtPar = pPar->pNxtPar ;

  arr_free ( pPar->pv ) ;
  arr_free ( pPar ) ;  
  
  if ( pNxtPar ) pNxtPar->pPrvPar = pPrvPar ; 
  if ( pPrvPar ) pPrvPar->pNxtPar = pNxtPar ; 
  else {
    /* This was the root. */
    if ( is_sol) 
      pZ->pSolParam = pNxtPar ;
    else
      pZ->pParam = pNxtPar ;
  }
}

/******************************************************************************
  zn_param_del_expr:   */

/*! Delete a parameter from a expr of zones.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  9Jul18; rename zn_match to zn_match_expr
  12Dec13; intro is_sol and treat pSolParam.
  12Jul13; use expr instead of range.
  18Jul11: conceived.
  

  Input:
  ------
  pUns
  expr: of zones
  is_sol: non_zero if the parameter is a solParam.
  name: of parameter to delete.
  
*/

void zn_param_del_expr ( uns_s *pUns, const char expr[], 
                         int isSol, const char name[] ) {

  int iZone ;
  zone_s *pZ ;
  param_s *pPar ;
  int found = 0 ;

  for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )   
    if ( zn_match_expr ( pUns,  iZone, expr ) ) {
      pZ = pUns->pZones[iZone] ;
      if ( pZ ) {
        /* This is a non-deleted zone. 
           Search for any match, sol and mesh params.*/
        pPar = zn_find_param ( pZ, name, isSol ) ;

        if ( pPar ) {
          ++found ;
          zn_ll_param_del ( pZ, pPar, isSol ) ;
        }
      }
    }

  if ( !found )
    hip_err ( warning, 1, 
              "no matching parameter found to delete in zn_param_del." ) ;

  return ;
}



/******************************************************************************
  zn_del:   */

/*! Delete a zone.
 *
 */

/*
  
  Last update:
  ------------
  27Jun14; modified interface to zone_elem_mod_all
  17Dec11; adjust pUns->mZones after deletion to point to a valid zone.
  10Jul11: conceived.
  

  Input:
  ------
  pUns
  iZone: zone to delete, values 1 ... pUns->mZones.
  
*/

void zn_del ( uns_s *pUns, int iZone ) {

  zone_s *pZone=NULL ;

  if ( iZone < 1 )
    hip_err ( fatal, 0, "non-positive zone number in zn_del\n" ) ;

  else if ( iZone > pUns->mZones ) {
    sprintf ( hip_msg, "requested zone %d does not exist in zn_del.", 
              pUns->mZones ) ;
    hip_err ( warning, 1, hip_msg ) ; 
  }
  else
    pZone = pUns->pZones[iZone] ;

  if ( !pZone ) {
    sprintf ( hip_msg, "zone %d is already deleted.", iZone ) ;
    hip_err ( info, 3, hip_msg ) ;
    return ;
  }

  /* Free allocated parameters with the zone. */
  param_s *pPar, *pNxtPar ; 
  pPar = pZone->pParam ;
  while ( pPar ) {
    pNxtPar = pPar->pNxtPar ;
    zn_ll_param_del ( pZone, pPar, 0 ) ;
    pPar = pNxtPar ;
  }
  pPar = pZone->pSolParam ;
  while ( pPar ) {
    pNxtPar = pPar->pNxtPar ;
    zn_ll_param_del ( pZone, pPar, 1 ) ;
    pPar = pNxtPar ;
  }

  /* Untag elements of that zone.  */
  int mElemsZone = zone_elem_mod_all ( pUns, iZone, 0 ) ;
  pUns->pZones[iZone] = NULL ;

  /* Dealloc zone info, but leave zone counter as is, don't rearexpr zones
     in pUns->pZones.  */
  arr_free ( pUns->pZones[iZone] ) ;
  
  /* Make sure that pUns->mZones points to a valid zone. */
  while ( !pUns->pZones[ pUns->mZones] && pUns->mZones > 0 )
    pUns->mZones-- ;

  sprintf ( hip_msg, "%d elements disassociated from zone %d.", 
            mElemsZone, iZone ) ;
  hip_err ( info, 3, hip_msg ) ;

  return ;
}




/******************************************************************************
  zn_param_mod:   */

/*! load the parameter files of a zone with data.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  9Jul18; rename zn_match to zn_match_expr
  16Feb17; drop paramType and work directly with parType_e arg.
  pass isSol as arg, remove confusion with isVec.
  4Apr13; modified interface to loop_elems.
  11Jul11: conceived.
  

  Input:
  ------
  pUns
  expr:   expresion to define a expr of zones.
  parType: one of int, dbl, vec, iarr, darr
  name:   parameter name
  dim:     dimension of data, 1 vor int,dbl; mDim for vec, any for arr.
  pVal:    array of values

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void zn_param_mod ( uns_s *pUns, const char expr[], const int isSol, const char action[],
                    parType_e parType,
                    // why start with a parType_e, convert that to string, and then back?
                    // const char paramType[],
                    const char name[], 
                    int dim, void *pVal ) {
  int iZone ;
  zone_s *pZ ;
  param_s *pPar ;
  //parType_e parType = ( paramType ? zn_str2parType( paramType ) : noPar ) ;
  //int this_is_sol = is_sol ;

  for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )   
    if ( zn_match_expr ( pUns, iZone, expr ) ) {
      pZ = pUns->pZones[iZone] ;
      if ( pZ ) {
        /* This is a non-deleted zone. */
        pPar = zn_find_param ( pZ, name, isSol ) ;

        if ( pPar && !strncmp( "add", action, 2 ) ) {
          /* names have to be distinct, can't create an existing name. */
          hip_err ( warning, 1, "parameter names have to be distinct,"
                    " ignoring second assignment.") ;
          return ;
        }
        else if ( !pPar && ( !strncmp( "mod", action, 2 ) ||
                             !strncmp( "del", action, 2 ) ) ) {
          /* Name has to exist for mod or del. */
          sprintf ( hip_msg, "parameter name %s not found.", name ) ;
          hip_err ( warning, 1, hip_msg ) ;
          return ;
        }
        else if ( !pPar )
          /* Insert an empty param field into the linked list. */
          pPar = zn_ll_insert_any_param ( pUns, pZ, isSol ) ;

        /* Add name, type, data to param. */
        zn_add_param_data ( pUns, pPar, name, parType, dim, pVal ) ; 
      }
    }

  return ;
}

void zn_recount ( uns_s *pUns ) {

  int iZone ;
  zone_s *pZ ;

  /* Zero counters. */
  for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )   {
    pZ = pUns->pZones[iZone] ;
    if ( pZ ) pZ->mElemsZone = 0 ;
  }

  /* Loop over all elements and tally zone associations. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && pEl->iZone ) {
        pZ = pUns->pZones[ pEl->iZone ] ;
        if ( pZ ) 
          pZ->mElemsZone++ ;
        else
          /* Invalid zone, set element zone to zero. */
          pEl->iZone = 0 ;
      }

  return ;
}

/******************************************************************************
  zn_menu:   */

/*! Add, delete, modify zonal info.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  9Jul18; rename zn_match to zn_match_expr
  10Jul11: derived from var_menu.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/



void zn_menu_add ( uns_s *pUns, const char expr[] ) {

  char name[MAX_BC_CHAR] ;


  if ( !eo_buffer() ) 
    read1string ( name ) ;
  else
    sprintf ( name, "zn_hip_%d", pUns->mZones+1 ) ;


  int iZone ;

  if ( !expr ) {
    /* create a new zone. */
    zn_mod ( pUns, name, 0 ) ;
  }

  else {
    int found = 0 ;
    /* Of course it makes no sense to change the name for more than
       one zone, we expect the user specifies an expression that
       matches only one zone. */
    for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ ) 
      if ( zn_match_expr ( pUns, iZone, expr ) ) {
        found = 1 ;
        zn_mod ( pUns, name, iZone ) ;
      }

    if ( !found )
      hip_err ( blank, 1, "      no zone found that matches the expression." ) ;
  }

  return ;
}

void zn_menu_del ( uns_s *pUns, const char expr[] ) {

  int iZone ;
  for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )
    if ( zn_match_expr ( pUns, iZone, expr ) )
      zn_del ( pUns, iZone ) ;

  return  ;
}


void zn_menu_param_add ( uns_s *pUns, const char expr[], char keyword[],
                         int isSol ) {

  char name[MAX_BC_CHAR] ;
  char parType[LINE_LEN] ;
  int dim, k ;
  int iVal, *pIArr ;
  double dVal, vec[MAX_DIM], *pDArr ;
  void *pVal=NULL ;
  
  if ( eo_buffer() )  {
    hip_err ( warning, 1, 
              "no parameter name specified in zn_menu_param_add." ) ;
    return ;
  }
  else
    read1string ( name ) ;

  if ( eo_buffer() ) {
    hip_err ( warning, 1, 
              "no parameter datatype specified in zn_menu_param_add." ) ;
    return ;
  }
  else
    read1lostring ( parType ) ;



  if ( !strncmp( parType, "iarr", 2 ) || 
       !strncmp( parType, "darr", 2 ) ) {
    if ( eo_buffer() ) {
      hip_err ( warning, 1, "need a user-defined length for iArr or dArr" );
      return ;
    }
    else
      read1int ( &dim ) ;
  }

  if ( eo_buffer() ) {
    hip_err ( warning, 1, "not enough data in zn_menu_param_add" );
    return ;
  }
    
  if ( !strncmp( parType, "int", 2 ) ) {
    read1int ( &iVal ) ;
    dim = 1 ;
    pVal = (void*) &iVal ;
  }

  else if ( !strncmp( parType, "dbl", 2 ) ) {
    read1double ( &dVal ) ;
    dim = 1 ;
    pVal = (void*) &dVal ;
  }


  else if ( !strncmp( parType, "iarr", 2 ) ) {
    pIArr = arr_malloc ( "pIArr in zn_menu_param_add", 
                         pUns->pFam, sizeof(int),dim);
    for ( k = 0 ; k < dim ; k++ )
      if ( eo_buffer() ) {
        hip_err ( warning, 1, 
                  "not enough data for int array in zn_menu_param_add" );
        return ;
      }
      else {
        read1int ( pIArr+k ) ;
        pVal = (void*) pIArr ;
      }
  }

  else if ( !strncmp( parType, "vec", 2 ) ||
            !strncmp( parType, "darr", 2 ) ) {
    if ( !strncmp( parType, "vec", 2 ) ) {
      dim = pUns->mDim ;
      pDArr = vec ;
    }
    else {
      pDArr = arr_malloc ( "pDArr in zn_menu_param_add", 
                           pUns->pFam, sizeof(double), dim );
    }

    for ( k = 0 ; k < dim ; k++ )
      if ( eo_buffer() ) {
        hip_err( warning,1,
                 "not enough data for dbl array/vec in zn_menu_param_add");
        return ;
      }
      else {
        read1double ( pDArr+k ) ;
        pVal = (void*) pDArr ;
      }
  }

  zn_param_mod ( pUns, expr, isSol, keyword, zn_str2parType(parType), name, dim, pVal ) ;

  return ; 
}


void zn_menu_param_del ( uns_s *pUns, const char expr[], const char action[],
                         int is_sol ) {

  char name[MAX_BC_CHAR] ;

  if ( eo_buffer() )  {
    hip_err ( warning, 1, "no parameter name specified in zn_menu_param_del." ) ;
    return ;
  }
  else
    read1string ( name ) ;

  zn_param_del_expr ( pUns, expr, is_sol, name ) ;

  return ;
}


void zn_menu_mod ( uns_s *pUns, char keyword[] ) {

  char name[MAX_BC_CHAR], expr[LINE_LEN] ;
  sprintf ( name, "zn_hip_%d", pUns->mZones+1 ) ;


  if ( eo_buffer() ) {
    hip_err ( warning, 1, "zone modification needs a zone number argument." ) ;
    return ;
  }
  else
    read1string ( expr ) ;

  if ( !eo_buffer() ) 
    read1string ( name ) ;
  
  int iZone, found = 0 ;
  for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )
    if ( zn_match_expr ( pUns, iZone, expr ) ) {
      found = 1 ;
      zn_mod ( pUns, name, iZone ) ;
    }

  if ( !found ) hip_err ( blank, 1, "         no matching zone found." ) ;
  return ;
}


void zn_menu_param ( uns_s *pUns, const char expr[], int is_sol ) {

  char keyword[LINE_LEN] ;

  if ( eo_buffer() ) {
    hip_err ( warning, 1, "parameter operation needs an action argument." ) ;
    return ;
  }
  else
    read1string ( keyword ) ;

  if ( !strncmp( keyword, "add", 2 ) ||
       !strncmp( keyword, "mod", 2 ) ) {
    zn_menu_param_add ( pUns, expr, keyword, is_sol ) ;
  }

  else if  ( !strncmp( keyword, "del", 2 ) ) {
    zn_menu_param_del ( pUns, expr, keyword, is_sol ) ;
  }

  return ;
}



void zn_menu_elem ( uns_s *pUns, const char expr[] ) {

  int iZone = zn_expr_to_iZone ( pUns, expr ) ;
  
  /* Add or delete? */
  if ( eo_buffer() ) {
    hip_err ( warning, 0, "element/zone operation needs an action argument." ) ;
  }
  char action[LINE_LEN] ;
  read1string ( action ) ;

  /* Select elements based on what criterion? */
  if ( eo_buffer() ) {
    hip_err ( warning, 0, "element/zone operation needs a selection argument." ) ;
  }
  char select[LINE_LEN] ;
  read1string ( select ) ;

  geo_s geo ;
  if ( !strncmp( select, "nodes", 2 ) )
    geo.box.type = nodeFlagged ;
  else if ( !strncmp( select, "all", 2 ) )
    geo.box.type = allGeo ;
  else if ( !strncmp( select, "remaining", 2 ) )
    geo.box.type = remaining ;
  else
    hip_err ( warning, 0, "unrecognised element selection operation.\n" ) ; 

  if ( !strncmp( action, "add", 2 ) )
    zn_tag_elems ( pUns, geo, 0, iZone ) ;

  else if ( !strncmp( action, "del", 2 ) )
    zn_tag_elems ( pUns, geo, iZone, 0 ) ;


  else 
    hip_err ( warning, 0, "unknown zone element command.\n" ) ;

  return ;
}

/************************************************************************

  public

*/


/******************************************************************************
  zone_add:   */

/*! Add a zone to an unstructured grid. Called before reading elements.
 *
 */

/*
  
  Last update:
  ------------
  9Jul18; rename zn_match to zn_match_expr
  27Jun14; add warning for duplicated zone names.
  26Jul11: conceived.
  

  Input:
  ------
  pUns
  zn_nr: number this zone is referenced by. Note that this routine
  does not trigger an element retag. 
  zn_name: name of zone.
  iZone: id number of zone.
  
  Returns:
  --------
  the zone number in pUns->pZones.
  
*/

int zone_add ( uns_s *pUns, const char zn_name[], int iZone,
               const int doWarnDuplication ) {

  if ( !iZone ) {
    for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ ) 
      if ( zn_match_expr ( pUns, iZone, zn_name ) ) {

        if ( doWarnDuplication ) {
          sprintf ( hip_msg, "zone name `%s' already exists with zone nr. %d",
                    zn_name, iZone ) ;
          hip_err ( warning, 1, hip_msg ) ;
        }
        return ( iZone ) ;
      }

    // Trigger declaring a new zone. 
    iZone = 0 ;
  }

   
  zone_s *pZone = zn_mod ( pUns, zn_name, iZone ) ;
  
  return ( pZone->iZone ) ;
}

/******************************************************************************
  zone_add_param:   */

/*! Add a parameter to a zone. 
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  16Feb17; pass isSol as arg.
  12Dec13; intro variant for solParam.
  JDM, Dec 16: and what is the difference?
  12Jul13; replace kZ from 0 with iZone running from 1.
  17Dec11; interpret kZ < 0 as pUns->mZones-1.
  26Jul11: conceived.
  

  Input:
  ------
  pUns
  expr: for index of the zone for this parameter in pUns->pZones.
  parType: one of parInt, parDbl,
  par_name: name of parameter
  isVec: if non-zero, interpret a double as a vector.
  dim: vector dimension
  pv: data 

  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zone_add_param ( uns_s *pUns, char expr[], 
                     parType_e parType, const int isVec, 
                     const char par_name[], int dim, void *pv ) {

  //char paramType[LINE_LEN] ;
  // zn_parType2str( parType, isVec, paramType ) ; 

  int isSol = 0 ;
  zn_param_mod ( pUns, expr, isSol, "add", // is_vec,
                 parType, par_name, dim, pv ) ;

  return ( 0 ) ;
}

int zone_add_solParam ( uns_s *pUns, char expr[], 
                        parType_e parType, const int isVec, 
                        const char par_name[], int dim, void *pv ) {

  //char paramType[LINE_LEN] ;
  //zn_parType2str( parType, isVec, paramType ) ; 

  int isSol = 1 ;
  zn_param_mod ( pUns, expr, isSol, "add", //is_vec,
                 parType, par_name, dim, pv ) ;

  return ( 0 ) ;
}

/******************************************************************************
  fun_name:   */

/*! copy all parameters when duplicating a zone.
 *
 */

/*
  
  Last update:
  ------------
  12Dec13: cut out of zone_copy
  

  Input:
  ------
  pPar0: first param entry in the linked list to copy.

  Output:
  -------
  **pRootPar: anchor the copied linked list here.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zn_copy_all_param ( uns_s *pUns2, param_s *pPar0, param_s **pRootPar ) {

  param_s *pPrvPar2 = NULL, *pPar2 ;

  while ( pPar0 ) {
    pPar2 = arr_malloc ( "pParNew in zone_copy", pUns2->pFam, sizeof( param_s ), 1 ) ;
    *pPar2 = *pPar0 ;

    if ( pPar0->parType == parVec )
      /* Set vector length to new dimension. */
      pPar2->dim = pUns2->mDim ;

    /* Copy. */
    pPar2->pv = arr_malloc ( "pPar->pv in zone_copy", pUns2->pFam, 
                             parTypeSize[pPar0->parType], pPar2->dim ) ;
    memcpy ( pPar2->pv, pPar0->pv, parTypeSize[pPar0->parType]*pPar2->dim ) ;

    /* Fill in z-component for lengthened vectors. */
    if ( pPar0->parType == parVec && pUns2->mDim > pPar0->dim )
      ((double*)pPar2->pv)[ pUns2->mDim-1 ] = 0. ;


    /* Close the linked list. */
    pPar2->pPrvPar = pPrvPar2 ;
    if ( pPrvPar2 ) 
      pPrvPar2->pNxtPar = pPar2 ;
    else
      *pRootPar = pPar2 ;
    pPar2->pNxtPar = NULL ;
    pPrvPar2 = pPar2 ;

    pPar0 = pPar0->pNxtPar ;
  }

  return ( 0 ) ;
}











/*****************************************************************
Public
******************************************************************/

/******************************************************************************
  zone_elem_mod_all:   */

/*! Add/remove all elements of a grid to a zone.
 *
 */

/*
  
  Last update:
  ------------
  27Jun14; rename from _add to _mod, to more clearly express use.
  27Jun14; improve interface documentation.
  Reorder arguments.
  Fix iZone for pZone=NULL when deleting a zone.
  17Dec11: conceived.
  

  Input:
  ------
  pUns     = grid with elements to tag.
  pZoneSrc = zone where elements come from. 
  pZone    = zone to put elements into, or NULL for untagging.
    
  Returns:
  --------
  number of added elements.
  
*/

int zone_elem_mod_all ( uns_s *pUns, int iZoneSrc, zone_s *pZone ) {

  geo_s geo ;
  geo.box.type = allGeo ;

  int iZone =  ( pZone ? pZone->iZone : 0 ) ;

  int mElTagged =  zn_tag_elems ( pUns, geo, iZoneSrc, iZone ) ;

  /* This is messy. pZones is used from 0, so there should be a iZone-1 here. */
  if ( pZone )
    pZone->mElemsZone += mElTagged ;

  return ( mElTagged ) ;
}


/******************************************************************************
  zone_elem_mod_remaining:   */

/*! Add/remove all remaining, non-zoned elements of a grid to a zone.
 *
 */

/*
  
  Last update:
  ------------
  16Dec17; derived from zone_elem_mod_all.

  Input:
  ------
  pUns     = grid with elements to tag.
  pZone    = zone to put elements into, or NULL for untagging.
    
  Returns:
  --------
  number of added elements.
  
*/

int zone_elem_mod_remaining ( uns_s *pUns, const int iZone ) {

  geo_s geo ;
  geo.box.type = remaining ;

  if ( iZone > pUns->mZones )
    hip_err ( fatal, 0, "not that many zones in this grid"
              " in zone_elem_mod_remaining.\n" ) ;

  int mElTagged = zn_tag_elems ( pUns, geo, 0, iZone ) ;

  pUns->pZones[iZone]->mElemsZone += mElTagged ;

  return ( mElTagged ) ;
}

/******************************************************************************
  zone_elem_mod_range:   */

/*! Add a consecutive range of elements to a zone. 
 */

/*
  
  Last update:
  ------------
  22Sep14; promote args of zone_elem_mod_range to ulong_t.
  27Jun14; rename from _add to _mod, to more clearly express use.
  7Jul13: conceived.
  

  Input:
  ------
  pUns: 
  iZone: 
  nElBeg, nElEnd: First and last element to be added, expected to be 
  consecutive in the pRootChunk.

  Changes To:
  -----------
  *pUns

  Output:
  -------
  mElAdded: number of elements added to this zone.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zone_elem_mod_range ( uns_s *pUns, const int iZone, 
                          const ulong_t nElBeg, const ulong_t nElEnd ) {

  /* The elements are expected to be numbered consecutively in a single
     chunk w/o unnumbered holes, as typical when reading from file. */
  chunk_struct *pCh = pUns->pRootChunk ;
  ulong_t mElCh = pCh->mElems ;

  if ( nElBeg < 0 || nElEnd > mElCh ) {
    sprintf ( hip_msg, "in zone_elem_mod_expr,"
              " expr %"FMT_ULG"-%"FMT_ULG" does not lie in the root chunk"
              " with %"FMT_ULG" elems",
              nElBeg, nElEnd, mElCh ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Tag the elements in the expr with the zone number. */
  elem_struct *pEl ;
  for ( pEl = pCh->Pelem+nElBeg ; pEl <= pCh->Pelem+nElEnd ; pEl++ ) {
    pEl->iZone = iZone ;
  }

  /* Count the elements with the zone. */
  int mElAdded = nElEnd-nElBeg+1 ; 
  pUns->pZones[iZone]->mElemsZone += mElAdded ;

  return ( mElAdded ) ;
}



/******************************************************************************
  zone_elem_mod_type:   */

/*! Add all elements of a type range to a zone. 
 */

/*
  
  Last update:
  ------------
  9Sep17; derived from zone_elem_mod_range.
  

  Input:
  ------
  pUns: 
  iZone: 
  nElBeg, nElEnd: First and last element to be added, expected to be 
  consecutive in the pRootChunk.

  Changes To:
  -----------
  *pUns

  Output:
  -------
  mElAdded: number of elements added to this zone.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zone_elem_mod_type ( uns_s *pUns, const int iZone, 
                         const elType_e elTBeg, const elType_e elTEnd ) {


  /* Tag the elements in the expr with the zone number. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  int overwriteZone = 0 ;
  int mElemAdded = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) 
      if ( pEl->number && pEl->elType >= elTBeg && pEl->elType <= elTEnd ) {
        if ( pEl->iZone && pEl->iZone != iZone ) {
          --pUns->pZones[pEl->iZone]->mElemsZone ;
          ++overwriteZone ;
        }
        pEl->iZone = iZone ;
        ++pUns->pZones[iZone]->mElemsZone ;
        ++mElemAdded ;
      }

  if ( overwriteZone ) {
    sprintf ( hip_msg, "%d elements had their zone id changed.", overwriteZone ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }

  return ( mElemAdded ) ;
}





/* new */



/******************************************************************************
  zone_elem_del:   */

/*! Delete (invalidate) all elements and their bnd faces in a zone. 
 */

/*
  
  Last update:
  ------------
  15Sep18; derived from zone_elem_mod_type.
  

  Input:
  ------
  pUns: 
  iZone: 

  Output:
  -------
  mElDeleted: number of elements deleted.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zone_elem_invalidate ( uns_s *pUns, const int iZone ) {

  /* Set to 'invalid' all elements with zone number. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  int mElemDeleted = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) 
      if ( pEl->number && pEl->iZone && pEl->iZone == iZone ) {
        pEl->invalid = 1 ;
        ++mElemDeleted ;
      }

  pUns->pZones[iZone]->mElemsZone = 0 ;

  /* Invalidate all boundary faces pointing to invalid elems. */
  pChunk = NULL ;
  bndPatch_struct *pBP = NULL ;
  bndFc_struct *pBF, *pBndFcF, *pBndFcL ;
  while ( loop_bndFaces ( pUns, &pChunk, &pBP, &pBndFcF, &pBndFcL ) ) {
    for ( pBF = pBndFcF ; pBF <= pBndFcL ; pBF++ )
      if ( pBF->Pelem->invalid )
        pBF->invalid = 1 ;
  }

  return ( mElemDeleted ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  zone_elem_bc_layer:
*/
/*! Declare a zone for all elements within a distance from a bc.
 *
 */

/*
  
  Last update:
  ------------
  16Dec18; update number of elems in the zone.
  15Dec18; move node marking into the loop over layers.
  track use of vxMark.
  1Oct17: interface declared.
  

  Input:
  ------
  pUns
  iZone: number of zone for the elements to be added
  nBc: number of bc 
  mLayer: number of layers to add, starting from bc faces
  iZoneClash: warn if elements from this zone are rezoned. 

  Returns:
  --------
  number of elements in the zone.
  
*/

ulong_t zone_elem_mod_bclayer ( uns_s *pUns, int iZone,
                                const int nBc, const int mLayer,
                                const int iZoneClash, ulong_t *pmElemsClash ) {

  // Reserve the first vertex mark for l, second for u.
  const int kVxMrk0 = 0 ;
  const int kVxMrk2 = 1 ;
  reserve_vx_markN ( pUns, kVxMrk0, "zone_elem_mod_bclayer" ) ;
  reserve_vx_markN ( pUns, kVxMrk2, "zone_elem_mod_bclayer" ) ;

  int dontPer = 0, dontAxis=0, dontSglNrm=0, foundPer ;
  ulong_t mVxBc=0, mBiBc=0, mTriBc=0, mQuadBc=0 ;
  // This uses vx->mark, and mrk2. i.e. kVxMrk0, kVxMrk2
  mark_uns_vertBc ( pUns, nBc, dontPer, dontAxis, dontSglNrm, &foundPer, 
                    &mVxBc, &mBiBc, &mTriBc, &mQuadBc ) ;

  
  /* Repeatedly loop over all elements, propagate the flag and
     set the zone of any element with a flagged node. */
  int kL ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  ulong_t mVx, kVx ;
  ulong_t mElemsAdded = 0 ;
  for ( kL = 0 ; kL < mLayer ; kL++ ) {
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {

        mVx = elemType[ pEl->elType ].mVerts ;
        for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
          
          if ( pEl->PPvrtx[kVx]->mark ) {
            if ( !pEl->iZone ) {
              /* This element wasn't zoned, but now shares a node 
                 with a zoned elem. Add. */
              pEl->iZone = iZone ;
              mElemsAdded++ ;
              break ;
            }
            else if  ( pEl->iZone == iZoneClash ) {
              (*pmElemsClash)++ ;
            }
          }
        }
      }
    }

    /* Now mark all nodes formed with the marked elements. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {
        if ( pEl->iZone == iZone ) {
          mVx = elemType[ pEl->elType ].mVerts ;
          for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
            vx_set_markN( pEl->PPvrtx[kVx], kVxMrk0 ) ;
          }
        }
      }
    }
  }

  // This one is set.
  // release_vx_markN ( pUns, kVxMrk0 ) ;
  release_vx_markN ( pUns, kVxMrk2 ) ;

  pUns->pZones[iZone]->mElemsZone += mElemsAdded ;
  return ( mElemsAdded ) ;
}





/*new*/

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  zone_elem_bc_layer:
*/
/*! Declare a zone for all elements within a distance from a bc.
 *
 */

/*
  
  Last update:
  ------------
  4Sep19; rename to nrBcPer to clarify meaning, update doc.
  11Jul19; fix bug with setting vx mark during propagation.
  4Apr19; introduce elType conditions.
  3Apr19; derived from zone_elem_mod_bclayer.
  

  Input:
  ------
  pUns
  iZone[2] number of per l/u zone for the elements to be added
  markN[2]: id of vx mark used for l/u bcs/layers.
  mLayer: number of layers to add, starting from bc faces
  elTypeLo/Hi: only zone elements with elTypeLo <= elType <= elTypeHi
  *pmBcPer: pointer to number of periodic/internal bcs
  nrBcPer[]: list of bc.nr for internal/per bcs.

  Returns:
  --------
  number of elements in the zone.
  
*/

ulong_t zone_elem_mod_perBcLayer ( uns_s *pUns, int iZone[2],
                                   const int mLayer,
                                   const elType_e elTypeLo, const elType_e elTypeHi,
                                   int *pmBcPer, int nrBcPer[] ) {
#undef FUNLOC
#define FUNLOC "in zone_elem_mod_perBcLayer"

  // Reserve the first vertex mark for l, second for u.
  int markN[2] = {0,2} ;
  reserve_vx_markN ( pUns, markN[0], "zone_elem_mod_perBcLayer" ) ;
  reserve_vx_markN ( pUns, markN[1], "zone_elem_mod_perBcLayer" ) ;

  const int doReset = 1 ;
  mark_vx_per ( pUns, markN, pmBcPer, nrBcPer, doReset ) ;

  
  /* Repeatedly loop over all elements, propagate the flag and
     set the zone of any element with a flagged node. */
  int kL ;
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  ulong_t mVx ;
  int kVx ;
  vrtx_struct *pVx ;
  ulong_t mElemsAdded[2] = {0} ;
  int kDir, kOpp ;
  int elVx_markN[2] ;
  for ( kL = 0 ; kL < mLayer ; kL++ ) {
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( !pEl->iZone &&
             pEl->elType >= elTypeLo &&
             pEl->elType <= elTypeHi ) {

          mVx = elemType[ pEl->elType ].mVerts ;
          elVx_markN[0] = elVx_markN[1] = 0 ;
          for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
            pVx = pEl->PPvrtx[kVx] ;
            for ( kDir = 0 ; kDir < 2 ; kDir++ ) {
              if ( vx_has_markN ( pVx, markN[kDir] ) )
                elVx_markN[kDir] = 1 ;
            }
          }

          if ( elVx_markN[0] == elVx_markN[1] ) {
            /* If both zero: not yet in any layer, do nothing.
               If both non-zero: this yet unzoned element has vertices in 
               either layer, layers have hence grown together. 
               Stop here, do nothing. */
            ;
          }
          else if ( elVx_markN[0] ) {
            /* Add elem to L layer. */
            pEl->iZone = iZone[0] ;
            mElemsAdded[0]++ ;
          }
          else {
            /* Add to U layer. */
            pEl->iZone = iZone[1] ;
            mElemsAdded[1]++ ;
          }
        }

    /* Now mark all nodes formed with the marked elements. */
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        for ( kDir = 0 ; kDir < 2 ; kDir++ )
          if ( pEl->iZone == iZone[kDir] ) {
            mVx = elemType[ pEl->elType ].mVerts ;
            for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
              vx_set_markN ( pEl->PPvrtx[kVx], markN[kDir] ) ;
            }
          }
  }

  // But this one is still in use?
  // release_vx_markN ( pUns, markN[0] ) ;
  release_vx_markN ( pUns, markN[1] ) ;

  pUns->pZones[iZone[0]]->mElemsZone += mElemsAdded[0] ;
  pUns->pZones[iZone[1]]->mElemsZone += mElemsAdded[1] ;
  return ( mElemsAdded[0]+mElemsAdded[1] ) ;
}

/*wen*/


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  zone_match_list:
*/
/*! check whether a zone number is in a list of zones.
 *
 *
 */

/*
  
  Last update:
  ------------
  9Jul18; rename more consistently to zone_match_list, aligining with
  zn_match_expr.
  8Sep17: conceived.
  

  Input:
  ------
  mZones: number of zones, if negative: exclude, if zero: include all.
  iZone[]: list of zones
  zone: the zone number to check
    
  Returns:
  --------
  1 on match, 0 otherwise.
  
*/

int zone_match_list ( const int mZones, const int iZone[], int zone ) {

  int kZ = 0 ;
  if ( mZones > 0 ) {
    /* Check for inclusion. */
    for ( kZ = 0 ; kZ < mZones ; kZ++ )
      if ( iZone[kZ] == zone )
        return ( 1 ) ;
    return ( 0 ) ;
  }
  else if ( mZones < 0 ) {
    /* Check for exclusion. */
    for ( kZ = 0 ; kZ < -mZones ; kZ++ )
      if ( iZone[kZ] == zone )
        return ( 0 ) ;
    return ( 1 ) ;
  }
  else
    /* Always matches. */
    return ( 1 ) ;
}



/******************************************************************************
  zone_list_nodes:   */

/*! Produce a list of all nodes either in, or touching a zone.
 *
 */

/*
  
  Last update:
  ------------
  20Dec14: conceived.
  

  Input:
  ------
  pUns grid
  pZ:  zone

  Output:
  -------
  pZoneVx: allocated linear list with node numbers.
    
  Returns:
  --------
  number of nodes in the list.
  
*/

ulong_t zone_list_nodes ( uns_s *pUns, zone_s *pZ, ulong_t **ppZoneVx ) {

  ulong_t mZoneVx = 0 ;

  /* Reserve and reset the vx flag */
  use_vx_flag ( pUns, "zone_list_nodes" ) ;
  unflag_vx ( pUns ) ;

  /* Tag all nodes that form an element within this zone. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  vrtx_struct **ppVx ;
  int mVerts ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && pEl->iZone == pZ->number ) {
        mVerts = elemType[pEl->elType].mVerts ;
        for ( ppVx = pEl->PPvrtx ; ppVx < pEl->PPvrtx+mVerts ; ppVx++ )
          (*ppVx)->flag1 = 1 ;
      }

  /* Count. */ 
  pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->flag1 )
        mZoneVx++ ;

  /* Alloc. */
  *ppZoneVx = arr_malloc ( "ppZoneVx in zone_list_nodes", pUns->pFam, 
                           mZoneVx, sizeof (**ppZoneVx) ) ;

  /* Fill. */
  pChunk = NULL ;
  ulong_t *pZv = *ppZoneVx ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->flag1 )
        *pZv++ = pVrtx->number ;

  /* Check count. */
  if ( pZv - *ppZoneVx != mZoneVx ) {
    sprintf ( hip_msg, "expected %"FMT_ULG" nodes, but found %td for zone %d"
              " in zone_list_nodes.", mZoneVx, pZv - *ppZoneVx, pZ->number ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
 

  free_vx_flag ( pUns ) ;
  return ( mZoneVx ) ;
}


/******************************************************************************
  zone_list:   */

/*! List zones and content.
 */

/*
  
  Last update:
  ------------
  9Jul18; rename zn_match to zn_match_expr
  27Jul14; make global, rename as zone_list.
  12Jyl13: swittch to 1 <= iZone <= mZones
  : conceived.
  

  Input:
  ------
  pUns
  expr: alpha or numerica expression of zone to match.
  
*/

/* hip_cython */
ret_s zone_list ( uns_s *pUns, char expr[] ) {
  ret_s ret = ret_success () ;

  int iZone ;
  int mList = 0 ;

  if ( !pUns->mZones ) {
    ret = hip_err ( blank, 1, "no zones present for this grid in zn_list." ) ;
    return ( ret ) ;
  }

  /* Update all counters. */
  zn_recount ( pUns ) ;

  if ( !expr ) {
    /* List all. */
    for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )
      if ( pUns->pZones[iZone] )
        break ;

    if ( iZone >  pUns->mZones ) {
      if ( verbosity > 1 ) 
        hip_err ( blank, 1, "     no zones found in this grid.\n" ) ;
    }
    else {
      /* List the headers of all zones. */
      zn_list_print_hdr ( ) ;
      for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )   
        zn_list_print_zhdr ( pUns, iZone ) ;
    }
  }

  else {
    /* Full listing for the zones in the expr. */
    for ( iZone = 1 ; iZone <= pUns->mZones ; iZone++ )   
      if ( zn_match_expr( pUns, iZone, expr ) )
        mList += zn_list_print_zall ( pUns, iZone ) ;   

    if ( !mList )
      ret = hip_err ( blank, 1, "         no zones matching expression." ) ;
  }

  return ( ret ) ;
}

/******************************************************************************
  zone_list_all:   */

/*! List zones of all grids.
 *
 */

/*
  
  Last update:
  ------------
  6Sep19; use hprintf.
  27Jun14: conceived.
  

  Input:
  ------
  expr: 
  
*/

/* hip_cython */
ret_s zone_list_all ( ) {
  ret_s ret = ret_success () ;

  uns_s *pUns ;
  const grid_struct *pGrid ;
  hip_msg[0] = '\0' ;
  for ( pGrid = Grids.PfirstGrid ; pGrid ; pGrid = pGrid->uns.PnxtGrid ) {
    if ( pGrid->uns.type == uns ) {
      pUns = pGrid->uns.pUns ;

      if ( pUns->mZones ) {
        hprintf ( "           %d %12s\n", pUns->nr, pUns->pGrid->uns.name ) ;
        ret = zone_list ( pUns, NULL ) ;
      }
    }
  }

  return ( ret ) ;
}





/******************************************************************************
  zone_copy:   */

/*! copy the contents of a zone to another grid.
 *
 *
 */

/*
  
  Last update:
  ------------
  12Dec13; intro pSolParam, cut out copying parameters.
  17Dec11; adjust vector length and fill in zero for z when copying 2D to 3D.
  29Aug11: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

zone_s *zone_copy ( uns_s *pUns2, const zone_s *pZ0 ) {

  /* Copy the zone. */
  zone_s *pZ2 = zn_mod ( pUns2, pZ0->name, 0 ) ;

  /* modify the name. */
  char zn_name[LINE_LEN] ;
  sprintf ( zn_name, "%s_%d", pZ0->name, pUns2->nr ) ;
  strncpy ( pZ2->name, zn_name, MAX_BC_CHAR ) ;

  /* Copy the mesh params with the zone. */
  zn_copy_all_param ( pUns2, pZ0->pParam, &pZ2->pParam ) ;

  /* Copy the sol params with the zone. */
  zn_copy_all_param ( pUns2, pZ0->pSolParam, &pZ2->pSolParam ) ;


  return ( pZ2 ) ;
}

/******************************************************************************
  zone_copy_all:   */

/*! Copy all zones from one grid to another.
 */

/*
  
  Last update:
  ------------
  27June14; mofified interface to zone_elem_mod_all.
  15Jul13: cut out of uns_copy.
  

  Input:
  ------
  pUns0: source grid

  Changes To:
  -----------
  *pUns2: destination grid.

  */

void zone_copy_all  ( uns_s *pUns0, uns_s *pUns2 ) {


  pUns2->mZones = 0 ;
  int iZone ;
  zone_s *pZ ;
  for ( iZone = 1 ; iZone <= pUns0->mZones ; iZone++ ) {
    pZ = pUns0->pZones[iZone] ;
    zone_copy ( pUns2, pZ ) ;
    zone_elem_mod_all ( pUns2, 0, pZ ) ; 
  }   

  return ;
}

/******************************************************************************
  zone_get_active_number:   */

/*! Given the consecutive/chrono zone index, return its number.
 *  Zones are indexed chrono, those numbers never change. When writing
 *  they are renumbered by use, this number needs to be written to file.
 */

/*
  
  Last update:
  ------------
  15Jul13: conceived.
  

  Input:
  ------
  pUns
  iZone: chrono zone index
    
  Returns:
  --------
  nrZone: number of this zone among the active zones 1.. mActiveZones.
  
*/

int zone_get_active_number ( const uns_s *pUns, const int iZone ) {

  const zone_s *pZ = pUns->pZones[iZone] ;

  if ( iZone < 1 || iZone > pUns->mZones )
    /* no such zone. */
    return ( 0 ) ;
  else if ( !pZ )
    /* Deleted zone. */
    return ( 0 ) ;
  else
    return ( pZ->number ) ;
}


/******************************************************************************
  zone_name_sequence:   
  Remove odd suffixes from zone names if they originate from one stem.
*/

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  13Dec13: conceived.
  

  Input:
  ------
  pUns: grid to sequence zones in
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int zone_name_sequence  ( uns_s *pUns ) {

  if ( !pUns->mZones )
    return (0) ;
  
  /* Make a copy of the root zone name. */
  zone_s *pRootZn = pUns->pZones[1] ;
  int lenRZN = strlen ( pRootZn->name ) ;

  unsigned int kZn = 1 ;
  int found_root = 0 ;
  zone_s *pZ = pRootZn ;
  while ( ( zone_loop ( pUns, &pZ ) ) ) {
    /* If zone name root stems match, build a name. */
    kZn++ ;
    if ( !strncmp( pZ->name, pRootZn->name, lenRZN ) ) {
      found_root = 1 ;
      snprintf ( pZ->name, sizeof(pZ->name)-1, "%s_%d", pRootZn->name, kZn ) ;
    }
  }

  if ( found_root )
    /* Fix the root zone name. */
    strcat ( pRootZn->name, "_1" ) ;

  return ( 0 ) ;
}


/******************************************************************************
  zone_loop:   */

/*! loop over all zones.
 */

/*
  
  Last update:
  ------------
  15Jul13: conceived.
  

  Input:
  ------
  pUns: grid

  Changes To:
  -----------
  pZone: last zone visited.
    
  Returns:
  --------
  the number of the zone in pUns if there is another zone, 0 if all visited.
  
*/

int zone_loop ( uns_s *pUns, zone_s **ppZone  ) {

  int iZone = 0 ;

  if ( !pUns->mZones )
    /*  No zones. */
    return ( 0 ) ;

  if ( *ppZone ) {
    /* Find the matching zone. */ 
    for ( iZone = 1 ; iZone < pUns->mZones ; iZone++ )
      if ( pUns->pZones[iZone] == *ppZone )
        break ;

    if ( iZone > pUns->mZones ) 
      /* No match found, corrupted *ppZone? */
      return ( 0 ) ;
  }

  /* Find the next non-deleted zone. */
  for ( iZone++ ; iZone <= pUns->mZones ; iZone++ )
    if ( ( *ppZone = pUns->pZones[iZone] ) ) 
      return ( iZone ) ;

  return ( 0 ) ;
}


/******************************************************************************
  zone_del:   */

/*! delete a zone.
 */

/*
  
  Last update:
  ------------
  5Sep19; intro ret_s.
  12Dec13; intro pSolParam
  24/1/12: conceived.
  

  Input:
  ------
  pUns
  expr: regexp char string that identifies the zone.
  
*/

void zone_del  ( uns_s *pUns, char *expr ) {
  zn_menu_del ( pUns, expr ) ;
  return ;
}



/* hip_cython */
ret_s zone_menu  ( char line[LINE_LEN] ) {
  ret_s ret = ret_success () ;
  if ( line && line[0] != '\0' )
    r1_put_string ( line ) ;

  grid_struct *Pgrid = Grids.PcurrentGrid ;
  uns_s *pUns ;
  char keyword[LINE_LEN], expr[LINE_LEN] ;

  /* Need an existing grid/solution. */
  if ( !( Pgrid  = Grids.PcurrentGrid ) ) {
    sprintf ( hip_msg, "  no grid, no zones.\n" ) ;
    ret = hip_err ( warning, 1, hip_msg ) ;
    return ( ret ) ;
  }
  else if ( !( pUns = Pgrid->uns.pUns ) ) {
    sprintf ( hip_msg, "  there is no unstructured grid to carry zones.\n" ) ;
    ret = hip_err ( warning, 1, hip_msg ) ;
    return ( ret ) ;
  }


  if ( eo_buffer () && verbosity > 0 ) {
    /* List all zones. */
    zone_list ( pUns, NULL ) ;
    return ( ret ) ;
  }
  else
    read1string ( keyword ) ;
    


  if ( !strncmp( keyword, "list", 2 ) ) {
    /* More detailed listing of all parameters in all zones in expr. */
    if ( eo_buffer () )
      zone_list ( pUns, NULL ) ;
    else {
      read1string ( expr ) ;
      if ( !strncmp( expr, "all", 3) || !strncmp( expr, "all", 2 ) )
        zone_list_all ( ) ;
      else
        zone_list ( pUns, expr ) ;
    }
  }

  else if ( !strncmp( keyword, "add", 2 ) ) {
    zn_menu_add ( pUns, NULL ) ;
  }

  else {
    /* Interpret this argument as a expr of zones. */
    strcpy ( expr, keyword ) ;

    if ( eo_buffer() ) {
      ret = hip_err ( warning, 1, 
                      "zone iZone needs to be followed by an action cmd." ) ;
      return ( ret ) ;
    }
    else
      read1string ( keyword ) ;


    if (!strncmp( keyword, "list", 2 ) ) 
      zone_list ( pUns, expr ) ;

    else if  ( !strncmp( keyword, "del", 2 ) ) {
      zn_menu_del ( pUns, expr ) ;
    }

    else if ( !strncmp( keyword, "add", 2 ) ||
              !strncmp( keyword, "mod", 2 ) ) {
      zn_menu_add ( pUns, expr ) ;
    }

    else if  ( !strncmp( keyword, "param", 2 ) ) {
      zn_menu_param ( pUns, expr, 0 ) ;
    }
    else if  ( !strncmp( keyword, "solparam", 2 ) ) {
      zn_menu_param ( pUns, expr, 1 ) ;
    }
    else if  ( !strncmp( keyword, "elem", 2 ) ) {
      zn_menu_elem ( pUns, expr ) ;
    }
    else {
      sprintf ( hip_msg, "no command matches %s\n", keyword ) ;
      hip_err ( warning, 1, hip_msg ) ;
    }
  }
   
  flush_buffer () ; 
  return ( ret ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  merge_zones:
*/
/*! merge zoned parts of a mesh.
 *
 */

/*
  
  Last update:
  ------------
  21Sep18; block numbering in merge_uns using doCheck=0, renumber vx after.
  7Sep18: extracted from cp_per_part.
  

  Input:
  ------
  pUns: grid
  mZ: number of zones to retain in numbering
  iZones: list of zones
  doReset: if nonz=-zero, reset numbering.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


int zone_merge ( uns_s *pUns, const int mZ, const int iZones[], const int doReset ) {

  
  /* Renumber, considering only the mZ zones in iZoneos. */
  const int dontUseMark=0 ;
  number_uns_elems_in_regions ( pUns, leaf, mZ, iZones, doReset, dontUseMark ) ;

  
  /* Merge the grid. Include nodes on interfaces, carrying vxMark3.*/
  if ( !merge_uns ( pUns, 1, 0 ) ) {
    sprintf ( hip_msg,
              "merging of unstructured grids failed in zone_merge.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  merge_regions_zones:
*/
/*! merge parts of a mesh defined by retions (marks) and zones.
 *
 */

/*
  
  Last update:
  ------------
  1Mar22; derived from zone_merge
  

  Input:
  ------
  pUns: grid
  mReg, iReg: number and list of regions to retain in numbering.
  mZ: number of zones to retain in numbering
  iZones: list of zones
  doReset: if non-zero, reset numbering.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/


int zone_region_merge ( uns_s *pUns,
                        const int mReg, const int iReg[],
                        const int mZones, const int iZone[],
                        const int doReset ) {

  
  /* Renumber, considering only the elems in the given regions and zones. */
  match_s match ;
  init_match( &match ) ;
  int i ;
  if ( mReg ) {
    match.matchMarks = mReg ;
    for ( i = 0 ; i < mReg ; i++ )
      match.kMark2Match = match.kMark2Match | 1 << iReg[i] ;
    match.kMark2NotMatch = ~match.kMark2Match ;
  }

  if ( mZones ) {
    match.matchZone = 1 ;
    match.mZones = mZones ;
    for ( i = 0 ; i < mZones ; i++ )
      match.iZone[i] = iZone[i] ;
  }
  ulong_t mConn ;
  number_uns_elems_match ( pUns, &match, doReset, &mConn ) ;

  
  /* Merge the grid. Include nodes on interfaces, carrying vxMark3.*/
  if ( !merge_uns ( pUns, 1, 0 ) ) {
    sprintf ( hip_msg,
              "merging of unstructured grids failed in zone_merge.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( 0 ) ;
}
