/*
  read_uns_cgns.c:
*/

/*! Read a CGNS database using hdf primitives only (not the CGNS library.)
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  2Sep21; switch large ints in ucgsec to cgsize_t
  17Feb19; for consistency, rename h5r_ default vars to be ucg_
  18Feb13; make piConn a global variable here, used to be in uns_s.
  3Apr12; merge elsA and VW descriptions, allow familyBCs and mixed element sections.
  Mar12; extend to read alternative boundary descriptions (VW)
  4Apr11; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "cpre_adapt.h"

#include "proto_uns.h"
#include "proto_adapt.h"

#include "cgnslib.h"
#include "proto_cgns.h"

/* to call read_mb_cgns, until we have a combined cgns call in mb_or_uns.c */
#include "proto_mb_uns.h"


#define CV const vrtx_struct
#define VX vrtx_struct

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const char version[] ;

extern Grids_struct Grids ;
extern const char version[] ;
extern const elemType_struct elemType[] ;
extern const double Gamma, GammaM1 ;
extern const char avbpFmtStr[][MAX_BC_CHAR] ;
extern const char varCatNames[][LEN_VAR_C] ;
extern const char h5varCatNames[][LEN_GRPNAME] ;
extern const char topoString[][MINITXT_LEN] ;

/* In ucg_count_mixed_sec we need to allocate a list of bnd fc before we have
   an pUns->pFam to associate the array with. */
extern arrFam_s *pArrFamUnsInit ;


/* Global control variables, what is written to file. */
#define DEFAULT_ucg_flag_info 0 /* read all variables. */
static int ucg_flag_info  = DEFAULT_ucg_flag_info ;


#define DEFAULT_ucg_flag_zone 1 /* read or not read zones. */
static int ucg_flag_zone  = DEFAULT_ucg_flag_zone  ; 


/* Track the global element numbers across sections. */
typedef struct {
  int nZone ; /* what zone? */
  int nBeg ; /* first and */
  int nEnd ; /* last element in this section. */
  bndFcVx_s *pBFcBeg ;
  bndFcVx_s *pBFcEnd ;
  int isMixedEl ; /* 1 if section is MIXED. */
  elType_e elType ; /* Type of elem is only one type of vol elem. */
  cgsize_t mElems ; /* number of elems in this sec,
                  for non-MIXED: 1 if the section is only vol elems, mDim. */
  cgsize_t mBndFc ; /* number of bnd face in this section, 1 if only non-MIXED bnd. */
  cgsize_t *pnBndFc ; /* index of each bnd face in the list of entities. */
  cgsize_t mIgnore ; /* 1 if the section neither mDim, or mDim-1,
                   number of ignored entities for MIXED sections. */
} ucg_sec_s ;


/******************************************************************************
  cg2hip_elType:   */

/*! Convert CGNS ElementType_t to hip's elType_e.
 *
 */

/*
  
  Last update:
  ------------
  4Apr11: conceived.
  

  Input:
  ------
  cg_ElType: CGNS ElementType_t
    
  Returns:
  --------
  hip's elType_e.
  
*/

/* See CGNS SIDS document, section 3.3.3 */
const int cg2h[MAX_ELEM_TYPES][MAX_VX_ELEM] = 
{ {0,1,2},          /* tri */
  {0,1,2,3},        /* quad */
  {0,2,1,3},        /* tet */
  {0,1,2,3,4},      /* pyramids */
  {0,3,5,1,2,4},    /* prism */
  {0,1,2,3,4,5,6,7} /* hex */
} ;


elType_e cgh_cg2hip_elType (  ElementType_t cg_ElType ) {

  elType_e elT ;

  switch ( cg_ElType ) {
  case BAR_2   : elT = bi  ; break ;
  case TRI_3   : elT = tri ; break ;
  case QUAD_4  : elT = qua ; break ;
  case TETRA_4 : elT = tet ; break ;
  case PYRA_5  : elT = pyr ; break ;
  case PENTA_6 : elT = pri ; break ;
  case HEXA_8  : elT = hex ; break ;
  default      : elT = noEl ; 
  }

  return ( elT ) ;
}

void cgh_cg2str_elType (  ElementType_t cg_ElType, char str[] ) {

  switch ( cg_ElType ) {
  case BAR_2   : strcpy ( str, "BAR_2  " ) ; break ;
  case TRI_3   : strcpy ( str, "TRI_3  " ) ; break ;
  case QUAD_4  : strcpy ( str, "QUAD_4 " ) ; break ;
  case TETRA_4 : strcpy ( str, "TETRA_4" ) ; break ;
  case PYRA_5  : strcpy ( str, "PYRA_5 " ) ; break ;
  case PENTA_6 : strcpy ( str, "PENTA_6" ) ; break ;
  case HEXA_8  : strcpy ( str, "HEXA_8 " ) ; break ;
  default      : strcpy ( str, "unknown" ) ; 
  }

  return ;
}

/******************************************************************************
  ucg_find_bndFcVx:   */

/*! Given a global CGNS element pointer, find the corresp boundary face.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  22Apr20 ; Salome CGNS: they give 1D and 3D element pointers for bc.
  14Feb17; issue bad exit fatal error.
  3Apr12; work with ucgSec.
  5Apr11: conceived.
  

  Input:
  ------
  iFc: global (chronological) CGNS face/element number.
  elIndex: section index, built in read_ucg_bnd.
  mSec: number of sections in elIndex.
  pBndFcVx: list of boundary faces by vertex number.
    
  Returns:
  --------
  pointer to the matching boundary face.
  
*/

bndFcVx_s *ucg_find_bndFcVx ( int iFc, const ucg_sec_s ucgSec[], int mSec, 
                              const bndFcVx_s *pBndFcVx ) {

  int nSec ;
  int nFace ;

  for ( nSec = 1 ; nSec <= mSec ; nSec++ ) {
    if ( ucgSec[nSec].mBndFc &&
         iFc >= ucgSec[nSec].nBeg && iFc <= ucgSec[nSec].nEnd ) {
      if ( ucgSec[nSec].pnBndFc )
        /* A section that mixes volume, bnd and/or ignored faces, only
           bnd faces were read. */
        nFace = ucgSec[nSec].pnBndFc[iFc] ;
      else
        /* All of the section is stored, uniform mapping. */
        nFace = iFc ;
      
      return ( ucgSec[nSec].pBFcBeg + nFace -ucgSec[nSec].nBeg ) ;
    }
  }

  /* Legal behavour. We ignore any elements with dim != mDim -1. 
     No match.
  sprintf ( hip_msg, "could not find face no %d in %d mesh sections.",
            iFc, mSec ) ;
  hip_err ( fatal, 0, hip_msg ) ;
 */
  return ( NULL ) ;
}


/******************************************************************************
  fun_name:   */

/*! Read boundary condition tags and element lists.
 *
 */

/*
  
  Last update:
  ------------
  7ju724; alloc pnFc not to sizeof(int), but sizeof(*pnFc)
  2Sep21; switch type of mFcBc, cg_* from int t cgsize_t
  3Oct19; fix uninitialised mBndFc in alloc for PointList.
  13Aug19; support Point/ElementRange.
  19Mar18; rename ucg_bcTypeDecode to cg_bcTypeDecode after move to cg_util.
  3Apr12; rework to allow for mixed element sections and familyBCs.
  15Feb12: cut out of ucg_read_bnd.
  

  Input:
  ------
  file_id

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ucg_read_bc ( int file_id, int nBase, int nZone, uns_s *pUns, ucg_sec_s ucgSec[],
                   GridLocation_t bcLoc ) {



  /* Read the FamilySpecified BC.
  int mFam = 0 ;
  cg_nfamilies( file_id, nBase, &mFam ) ;
  not needed.  */

  int mSec ;
  cg_nsections( file_id, nBase, nZone, &mSec ) ;


  char bcText[MAX_BC_CHAR] ;
  int nFamBC, nGeo ;
  bc_struct *pBc, *pBcFam ;
  BCType_t cg_BCType ;
  char bcType[MAX_BC_CHAR] ;
  int nBc ;
  PointSetType_t cg_PtSetType ;
  int cg_NormalIndex ;
  cgsize_t cg_NormalListFlag ;
  DataType_t cg_NormalDataType ;
  int cg_mDataSets ;
  cgsize_t *pnFc = NULL, *pnF, *pnBndVx = NULL, *pnBV ;
  cgsize_t mEntBc = 0 ;
  cgsize_t iRange[2] ;
  char famName[LINE_LEN] ;
  bndFcVx_s *pBf ;
  vrtx_struct *pVx ;
  cgsize_t kVx ;
  for ( nBc = 1 ; nBc <= pUns->mBc ; nBc++ ) {

    /* Read the boundary conditions. */
    cg_boco_info( file_id, nBase, nZone, nBc, 
                  bcText, &cg_BCType, &cg_PtSetType, &mEntBc,
                  &cg_NormalIndex, &cg_NormalListFlag, &cg_NormalDataType, 
                  &cg_mDataSets ) ;



    /* Bc is grouped by family? */
    if ( cg_BCType == FamilySpecified ) {
      /* Extract the bc info from the family and override bc info. */
      cg_family_read( file_id, nBase, nBc, bcText, &nFamBC, &nGeo ) ;

      pBc = find_bc ( bcText, 1 ) ;

      /* Convert to hip. Note that this fambc returns the name
         of the FamilyBC_t node, which appears to be not the same as the name. */
      cg_fambc_read ( file_id, nBase, nBc, 1, bcText, &cg_BCType ) ;
      hcg_bcTypeDecode( cg_BCType, bcType ) ;
      strcpy ( pBc->type, bcType ) ;
  

      /* Code from GP translated to C. Awful design of CGNS, is this 
         really who we are supposed to access this? */
      if ( cg_goto( file_id, nBase, "Zone_t", nZone,"ZoneBC_t", 1, 
                    "BC_t", nBc ,"end" ) ) {
        hip_err ( fatal, 0, "failed goto in CGNS Family read in ucg_read_bc.\n" ) ; 
      }
  
      /* Implement families in bc_struct.  */
      cg_famname_read( famName ) ;

      /* JDM Apr 2020: should we use famName in find_bc? 
      pBc = find_bc ( bcText, 1 ) ;*/
    }
    else {
      /* Bc directly specified with the section. */
      pBc = find_bc ( bcText, 1 ) ;
    }


    

    if ( bcLoc == FaceCenter || bcLoc == CellCenter ) {
      // bc info points to the list of faces use directly.
      // Did we ever have CellCenter? How would that work?
      if ( cg_PtSetType == PointList ||
           cg_PtSetType == ElementList  ) {

        /* A list of bnd face to global element pointers. */
        pnFc = arr_malloc ( "pnFc in ucg_read_bc", NULL, mEntBc, sizeof(*pnFc) ) ;
        cg_boco_read( file_id, nBase, nZone, nBc, pnFc, NULL ) ;  

        /* Faster search for matching face when sorted. */
        qsort ( pnFc, mEntBc, sizeof(*pnFc), cmp_int ) ;
      }
      else if ( cg_PtSetType == PointRange || 
                cg_PtSetType == ElementRange ) {

        /* Range. */
        cg_boco_read( file_id, nBase, nZone, nBc, iRange, NULL ) ;  
        mEntBc = iRange[1]-iRange[0]+1 ;

        /* Convert to list. */
        pnFc = arr_malloc ( "pnFc in ucg_read_bc", NULL, mEntBc, sizeof(*pnFc) ) ;
        for ( pnFc[0] = iRange[0], pnF = pnFc+1 ; pnF < pnFc+mEntBc ; pnF++ )
          *pnF = pnF[-1]+1  ;
      }
      else {
        // Checked already in ucg_alloc, but hey....
        hip_err ( fatal, 0, 
                  "hip expects CGNS PointSetType as Point/ElementList"
                  " or Range in ucg_read_bc.");
      }
      

      for ( pnF = pnFc ; pnF < pnFc+mEntBc ; pnF++ ) {
        pBf = ucg_find_bndFcVx ( *pnF, ucgSec, mSec, pUns->pBndFcVx ) ;
        if ( pBf ) pBf->pBc = pBc ;
      }
      arr_free ( pnFc ) ;
    }


    
    else if ( bcLoc == Vertex ) {
      // Bc given by a ist of vertices. First id the faces formed with them.
      if ( cg_PtSetType == PointList ||
           cg_PtSetType == ElementList  ) {

        /* A list of bnd face to global element pointers. */
        pnBndVx = arr_malloc ( "pnFc in ucg_read_bc", NULL, mEntBc, sizeof(*pnFc) ) ;
        cg_boco_read( file_id, nBase, nZone, nBc, pnBndVx, NULL ) ;  
      }
      else if ( cg_PtSetType == PointRange || 
                cg_PtSetType == ElementRange ) {
        hip_err ( fatal, 0, " in ucg_read_bc: bc specified by Vertex"
                  " and Range not implemented." ) ;
      }

      // mark all vx in the list of bnd vx
      int kMark = 1 ;
      reserve_vx_markN( pUns, kMark, "ucg_read_bc" ) ;
      // There is only one chunk.
      reset_1chunk_vx_mark_k ( pUns->pRootChunk, kMark );
      for ( pnBV = pnBndVx ; pnBV < pnBndVx+mEntBc ; pnBV++ ) {
        pVx = pUns->pRootChunk->Pvrtx + *pnBV ;
        set_vrtx_mark_k ( pVx, kMark ) ;
      }

      // Find all the faces that have all marked vx, i.e. are on this bc
      for ( pBf = pUns->pBndFcVx ; pBf < pUns->pBndFcVx+pUns->mBndFcVx ; pBf++ ) {
        for ( kVx = 0 ; kVx < pBf->mVx ; kVx++ ) {
          pVx = pBf->ppVx[kVx] ;
          if ( !check_vrtx_mark_k ( pVx, kMark ) )
            break ;
        }
        if ( kVx == pBf->mVx )
          // All it's vx are on this bc
          pBf->pBc = pBc ;
      }
      arr_free ( pnBndVx ) ;
      release_vx_markN( pUns, kMark ) ;
    } // end else if ( bcLoc == Vertex ) {
  }

  
  // Check whether all faces have a pBc
  for ( pBf = pUns->pBndFcVx ; pBf < pUns->pBndFcVx+pUns->mBndFcVx ; pBf++ ) {
    if ( !pBf->pBc ) {
      vrtx_struct *pVx0 = pUns->pRootChunk->Pvrtx ;
      sprintf ( hip_msg, " no boundary assigned to face %li, formed by %li, %li, ..",
                pBf-pUns->pBndFcVx, pBf->ppVx[0]-pVx0, pBf->ppVx[1]-pVx0 ) ; 
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }

  return ;
}


/******************************************************************************
  ucg_open:   */

/*! test for existence, then open existing CGNS file for reading.
 *
 */

/*
  
  Last update:
  ------------
  4Apr11: conceived.
  

  Input:
  ------
  gridFile

  Returns:
  --------
  CGNS file id (int) on successful opening.
  
*/

int ucg_open ( char *gridFile ) {

  prepend_path ( gridFile ) ;

  /* Test whether the file is openable. Do this outside of hdf, as
     hdf throws up ugly pages of error msgs. */
  FILE* Ftest ;
  if ( !( Ftest = r1_fopen ( prepend_path ( gridFile ), TEXT_LEN, "r" ) ) ) {
    sprintf ( hip_msg, "could not find file %s in ucg_open.\n", gridFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else
    fclose ( Ftest ) ;


  /* Open the cgns file. */
  int file_id, ier ;
  if ( (ier = cg_open( gridFile, CG_MODE_READ, &file_id ) ) ) {
    sprintf ( hip_msg,"failed to open CGNS file %s in ucg_open\n", gridFile ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  return ( file_id ) ;
}


/******************************************************************************
  ucg_read_sol:   */

/*! Read a CGNS solution database.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  8may24; use renamed unslice_scalar_var
  2Sep21; change type of iSize from int to cgsize_t.
  17Feb20; remove unused keyword argument.
  18Dec19: conceived.
  

  Input:
  ------
  doReadData: if non-zero actually read the sol, 
              if not, then list and return mUnknowns
  file_id: opened cgns base
  pUns: grid
  pChunk: allocated chunk to add solution to
  mVx: number of vertices, (as pUns may not exist when !doReadData
    
  Returns:
  --------
  number of unknowns read.
  
*/

int ucg_read_sol ( const int doReadData,
                   int file_id,
                   uns_s *pUns, chunk_struct *pChunk, const ulong_t mVx ) {

  /* Hardwire single base, single zone for now. */
  int nBase = 1 ;
  int nZone = 1 ;

  
  /* How many flow sol nodes? */
  int mSols ;
  int ier ;
  ier = cg_nsols(file_id, nBase, nZone, &mSols);
  if ( ier ) {
    strncpy (hip_msg, cg_get_error(), LINE_LEN ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  if ( mSols > 1 ) {
    hip_err ( warning, 0, "reading multiple flowSolution nodes not implemented,"
              " reading only first one." ) ;
  }
  int nSol = 1 ;

  /* Check size against mVx. */
  char zoneName[LINE_LEN] ;
  cgsize_t iSize[MAX_DIM] = {0} ;  // uns CGNS: # of verts, # of elems, 0.
  ier = cg_zone_read( file_id, nBase, nZone, zoneName, iSize ) ;
  if ( iSize[0] != mVx ) {
    ulong_t uiSize = iSize[0] ;
    sprintf ( hip_msg, "expected %"FMT_ULG", found %"FMT_ULG" nodes in ucg_read_sol.",
              mVx, uiSize ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  
  /* Only one flow solution node so far. */
  if ( cg_goto( file_id, nBase, "Zone_t", nZone,
                "FlowSolution_t", nSol ,"end" ) ) {
    hip_err ( fatal, 0, "failed goto in CGNS sol read in ucg_read_sol." ) ; 
  }

  /* Where are unkowns stored? */
  char solname[LINE_LEN] ;
  GridLocation_t locUnknown ;
  ier = cg_sol_info( file_id, nBase, nZone, nSol, solname,
                     &locUnknown ) ;
  if ( locUnknown == CellCenter ) {
    // We need vx volumes for the scatter ele2vx
     compute_vrtxVol ( pUns, -1 ) ;  
  }
  else if ( locUnknown != Vertex ) {
    hip_err ( fatal, 0, "hip expects unknowns at vertices or cell centres." ) ;
  }


  /* Number of unknowns. It appears, no vectors. */
  int mUnknowns ;
  ier = cg_nfields ( file_id, nBase, nZone, nSol, &mUnknowns ) ;

  if ( verbosity > 1 ) {
    sprintf ( hip_msg, "found %d unknowns in solution %d of base %d, zone %d.",
              mUnknowns, nBase, nZone, nSol ) ;
    hip_err ( info, 1, hip_msg ) ;
  }



  
  double *dBuf = NULL, *dVBuf = NULL, *dEBuf = NULL ;
  if ( doReadData ) {
    /* All vars are written as scalars over nodes/cells, so allocate one for the vx. */
    dVBuf = arr_malloc ( "dBuf in ucg_read_sol", pUns->pFam, mVx, sizeof( double ) ) ;
    if ( locUnknown == CellCenter ) {
      // Also one vor elem data
      dEBuf = arr_malloc ( "dBuf in ucg_read_sol", pUns->pFam, mVx, sizeof( double ) ) ;
      dBuf = dEBuf ;
    }
    else {
      // Unknowns at vertices.
      dBuf = dVBuf ;
    }
      
    /* No recognised type of vars. */
    pUns->varList.varType = noType ;
  }

  int kUn ;
  char varName[LINE_LEN] ;
  DataType_t datatype ;
  cgsize_t rgMin[MAX_DIM] = {1}, rgMax[MAX_DIM] = {1} ;
  rgMax[0] = mVx ;
  double valMin, valMax ;
  ulong_t nMin, nMax ;
  for ( kUn = 0 ; kUn < mUnknowns ; kUn++ ) {
    /* CGNS numbers from 1. */
    ier = cg_field_info( file_id, nBase, nZone, nSol, kUn+1,
                         &datatype, varName);

    if ( doReadData ) {
      set_one_var_cat_name ( pUns->varList.var, kUn, varName, "other" ) ;
      ier = cg_field_read( file_id, nBase, nZone, nSol, varName,
                           RealDouble, rgMin, rgMax, dBuf );
      if ( locUnknown == CellCenter ) {
        // Currently uses the chunk-spread arrays.
        scatter_ele2vx_area ( pUns ) ;
      }
      unslice_scalar_var ( pChunk, pChunk->mVerts, kUn, dBuf,
                           &valMin, &nMin, &valMax, &nMax ) ;      
    }
    
    if ( verbosity > 3 && doReadData ) {
      sprintf ( hip_msg, "        var %d: %30s, min: %g, max: %g",
                kUn, varName, valMin, valMax ) ;
      hip_err ( blank, 2, hip_msg ) ;
    }
    else if ( verbosity > 2 ) {
      sprintf ( hip_msg, "        var %d: %30s", kUn, varName ) ;
      hip_err ( blank, 2, hip_msg ) ;
    }

  }
  
  
  if ( doReadData )
    arr_free ( dBuf ) ;
  
  return ( mUnknowns ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  ucg_count_mixed_sec:
*/
/*! count vol elems and bnd faces in a MIXED section, 
 * create a position list of bnd faces.
 *
 */

/*
  
  Last update:
  ------------
  23Apr20: conceived.
  

  Input:
  ------
  file_id: 
  nBase:
  nZone:
  nSec
  ucgSec: array with section stats, 
  pUns: for pFam.

  Changes To:
  -----------

  Output:
  -------
  *pmElems: number of volume elems
  *pmVolConn: number of connectivity entries for volume elements.
  *pmBndFc: number of boundary facs
  *pnBndFc: array of the position of bnd faces in the section.
  *pmIgnore: number of ignored entities.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

void ucg_count_mixed_sec ( int file_id, int nBase, int nZone, int nSec,
                           const int mDim,
                           cgsize_t *pmElems, cgsize_t *pmVolConn, cgsize_t *pmBndFc,
                           cgsize_t **ppnBndFc, cgsize_t *pmIgnore ) {

  cgsize_t nBeg, nEnd ;
  char someStr[LINE_LEN] ;
  ElementType_t cg_ElType ;
  int cg_nBndry, cg_prtFlg ;
  cg_section_read( file_id, nBase, nZone, nSec,
                   someStr, &cg_ElType, &nBeg, &nEnd, &cg_nBndry, &cg_prtFlg ) ;

  if ( cg_ElType != MIXED )
    hip_err ( fatal, 0, "ucg_count_mixed_sec works only on MIXED sections." ) ;

  cgsize_t mC ;
  cg_ElementDataSize( file_id, nBase, nZone, nSec, &mC ) ;
  /* Temporary connectivity list by node number, sized for the largest type. */
  cgsize_t mConn, *piConn, *piC ;
  piConn = arr_malloc ( "pUns->iConn in ucg_read_conn", 
                        pArrFamUnsInit, mC, sizeof( *piConn ) ) ;
  int mEnt = nEnd-nBeg+1 ; 
  *ppnBndFc = arr_malloc ( "pUns->iConn in ucg_read_conn", 
                         pArrFamUnsInit, mEnt+1, sizeof( **ppnBndFc ) ) ;
  cgsize_t *pnBF = *ppnBndFc ;


  cg_elements_read( file_id, nBase, nZone, nSec, piConn, NULL ) ;

  int nEnt ;
  elType_e elT ;
  const elemType_struct *pElT ;
  piC = piConn ;
  *pmElems = *pmVolConn = *pmBndFc = 0 ;
  for ( nEnt = 1 ; nEnt <= mEnt ; nEnt++ ) {
    elT = cgh_cg2hip_elType ( *piC++ ) ;
    if ( elT == noEl )
      hip_err ( fatal, 0, "unrecognised element type in ucg_count_mixed_sec" ) ;
    pElT = elemType + elT ;

    if ( pElT->mDim == mDim ) {
      (*pmElems)++  ;
      *pmVolConn += pElT->mVerts ;
      *pnBF++ = 0 ;
    }
    else if ( pElT->mDim == mDim-1 ) {
      /* The first face has index 1, to distinguish from elem=0 */
      *pnBF++ = (*pmBndFc)++ ;
    }
    else if ( pElT->mDim == mDim-1 ) {
      /* The first face has index 1, to distinguish from elem=0 */
      *pnBF++ = (*pmBndFc)++ ;
    }
    else {
      (*pmIgnore)++ ;
    }
    
    piC += pElT->mVerts ;
  }

  if ( ( *pmElems  > 0 && *pmBndFc == 0 && *pmIgnore == 0 ) ||
       ( *pmElems == 0 && *pmBndFc  > 0 && *pmIgnore == 0 ) ||
       ( *pmElems == 0 && *pmBndFc == 0 && *pmIgnore  > 0 ) ) {
    /* Mixed section has only one type of vol, bnd, ignore entities. 
       pnBndFc is uniform, not needed. */
    arr_free ( *ppnBndFc ) ;
    *ppnBndFc = NULL ;
  }

  arr_free ( piConn ) ;
  return ;
}



/******************************************************************************
  ucg_read_alloc:   */

/*! Get all dimensions necessary for allocation from a CGNS database.
 *
 */

/*
  
  Last update:
  ------------
  17Feb20; remove unused keyword argument.
  18Dec19; alloc for solution.
  13Aug19; support ElementRange
  6Jul13; new interface to make_uns_grid.
  18Feb13; alloc a global piConn of type cgsize_t = int.
  3Sep12;make mFcBc of type cgsize_t to pacify compiler.
  3Apr12; new definition of ucgSec with flags nBc and isBnd, 
          allow mixed element secs.
  19Jul11; fix bug in mBndFc count.
  Jul11: conceived.
  

  Input:
  ------
  file_id: opened CGNS database.
  solFile_id: opened CGNS database for solution.
  nBase: # of the base (needs to be 1 for now)
  nZone, # of the zone (=1 for now)
  nSol:  # of the solution (=1 for now)

  Changes To:
  -----------
  *ppUcgSec: parameters for each grid section
  *pBcLoc: where are bc specified? Vertex, Face, Cell?
    
  Returns:
  --------
  *pUns. 
  
  */

uns_s *ucg_alloc ( int file_id, int solFile_id, int nBase, int nZone,
                   ucg_sec_s **ppUcgSec, GridLocation_t *pBcLoc ) {


  /* How many bases? For the time being, read only the first base. */
  int mBases ;
  if ( cg_nbases( file_id, &mBases ) ) {
    sprintf ( hip_msg,
      "unable to read number of bases in CGNS file in ucg_alloc.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( mBases < nBase ) {
    sprintf ( hip_msg,"base no %d not in CGNS file in ucg_alloc.\n", nBase ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* Base properties. */
  char baseName[TEXT_LEN] ;
  int mCellDim, mDim ;
  cg_base_read( file_id, nBase, baseName, &mCellDim, &mDim ) ;

  int mZones ;
  cg_nzones( file_id, nBase, &mZones ) ;
  if ( mZones > 1 ) {
    /* Multiple zones. Track the cgns and hip zone numbers. */
    hip_err ( warning, 1, "implement multiple zone read in ucg_alloc, only zone 1 read." ) ;
  }
  

  char zoneName[TEXT_LEN] ;
  cgsize_t size[3] ;
  cg_zone_read( file_id, nBase, nZone, zoneName, size );
  ulong_t mVx = size[0] ;

  
  /* How many coordinate records are there? Should be mDim. */
  int mCoorRec ;
  cg_ncoords( file_id, nBase, nZone, &mCoorRec) ;
  if ( mCoorRec - mDim ) {
    sprintf ( hip_msg,"expected %d coor lists, found %d in ucg_read_alloc\n", 
              mDim, mCoorRec ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  int mSec ;
  cg_nsections( file_id, nBase, nZone, &mSec ) ;
  ucg_sec_s *ucgSec = arr_malloc ( "ucgSec in ucg_read_bnd", NULL, mSec+1, 
                                   sizeof( ucg_sec_s ) ) ;
  *ppUcgSec = ucgSec ;

  /* How many element sections? */
  int nSec ;
  char someStr[TEXT_LEN] ;
  ElementType_t cg_ElType ;
  cgsize_t nBeg, nEnd ;
  int nBndry, prtFlg ;
  elType_e elT ; 
  const elemType_struct *pElT ;

  /* Loop over all sections, make an index. */
  cgsize_t mC ;
  ulong_t mElems =  0;
  ulong_t mConn = 0 ;
  ulong_t mBndFc = 0 ;
  for ( nSec = 1 ; nSec <= mSec ; nSec++ ) {
    cg_section_read( file_id, nBase, nZone, nSec,
                     someStr, &cg_ElType, &nBeg, &nEnd, &nBndry, &prtFlg ) ;
    cg_ElementDataSize( file_id, nBase, nZone, nSec, &mC ) ;

    ucgSec[nSec].nZone = nZone ;
    ucgSec[nSec].nBeg = nBeg ;
    ucgSec[nSec].nEnd = nEnd ;
    ucgSec[nSec].mElems = 0 ;
    ucgSec[nSec].isMixedEl = 0 ;
    ucgSec[nSec].elType = elT = cgh_cg2hip_elType ( cg_ElType ) ;
    ucgSec[nSec].mBndFc = 0 ;
    ucgSec[nSec].pnBndFc = NULL ;
    ucgSec[nSec].mIgnore = 0 ;

    if ( cg_ElType == MIXED ) {
      ucgSec[nSec].isMixedEl = 1 ;
      ucg_count_mixed_sec ( file_id, nBase, nZone, nSec, mDim,
                            &ucgSec[nSec].mElems, &mC, &ucgSec[nSec].mBndFc,
                            &ucgSec[nSec].pnBndFc, &ucgSec[nSec].mIgnore ) ;
      mElems += ucgSec[nSec].mElems ;
      mConn += mC ;
      mBndFc += ucgSec[nSec].mBndFc ;
    }
    else if ( ucgSec[nSec].elType == noEl ) {
      sprintf ( hip_msg, " unrecognised element type in section %s", someStr ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    else if ( elemType[elT].mDim == mDim ) {
      /* Volume element section with one type. */
      ucgSec[nSec].mElems = nEnd-nBeg+1 ;
      mElems += ucgSec[nSec].mElems ;
      // mC is the total number of conn entries in this section.
      // includes an extra integer eltype for mixed sections.
      mConn += mC ;
    }
    else if ( elemType[elT].mDim == mDim-1 ) {
      /* Bnd Section with one type */
      ucgSec[nSec].mBndFc += nEnd-nBeg+1 ;
      mBndFc += ucgSec[nSec].mBndFc ;
    }
    else
      /* Entire section ignored. */
      ucgSec[nSec].mIgnore = 1 ;
  }



  /* Basic checks on bc setup, count number of bc. */
  int mBc = 0 ;
  cg_nbocos( file_id, nBase, nZone, &mBc ) ;
  int nBc ;
  cgsize_t mFcBc = 0;
  char bcText[LINE_LEN] ;
  BCType_t cg_BCType ;
  PointSetType_t cg_PtSetType ;
  int cg_NormalIndex ;
  cgsize_t cg_NormalListFlag ;
  DataType_t cg_NormalDataType ;
  int cg_mDataSets ;
  int *pnFc = NULL, *pnF = NULL ;
  GridLocation_t cg_PtLocation ;
  *pBcLoc = CG_Null ;
  for ( nBc = 1 ; nBc <= mBc ; nBc++ ) {
    cg_boco_info( file_id, nBase, nZone, nBc, 
                  bcText, &cg_BCType, &cg_PtSetType, &mFcBc,
                  &cg_NormalIndex, &cg_NormalListFlag, &cg_NormalDataType, 
                  &cg_mDataSets ) ;

    if ( cg_PtSetType == PointList ||
         cg_PtSetType == ElementList  ) {
      /* A list of bnd face to global element pointers. */
      ;
    }
    else if ( cg_PtSetType == PointRange || 
              cg_PtSetType == ElementRange ) {

      /* Range. */
      ;
    }
    else {
      hip_err ( fatal, 0, 
                "hip expects CGNS PointSetType as Point/ElementList or Range in ucg_alloc.");
    }


    /* location: we need FaceCenter, i.e. elements given. */
    cg_boco_gridlocation_read( file_id, nBase, nZone, nBc, &cg_PtLocation ) ;
    if ( *pBcLoc == CG_Null ) {
      // first boundary, intialise.
      *pBcLoc = cg_PtLocation ;
    }
    else if ( *pBcLoc != cg_PtLocation ) {
      // this bnd has a different loc type from preceding ones. Can't handle.
      hip_err ( fatal, 0, 
                "in ucg_alloc: GridLocation for Section %d is different from earlier ones.\n"
                "            hip expects all to be the same.\n" ) ;
    }
    if ( cg_PtLocation != Vertex && cg_PtLocation != CellCenter && cg_PtLocation != FaceCenter ) {
      hip_err ( fatal, 0,
                "in ucg_alloc: GridLocation for Section %d given as EdgeCenter.\n"
                "            hip expects Vertex,CellCenter,FaceCenter, please re-produce your"
                "            cgns grid with those, or contact your friendly hip developer.\n" ) ;
    }
     

  } //  for ( nBc = 1 ; nBc <= mBc ; nBc++ ) {




  /* Check solution. */
  int mUnknowns = 0 ;

  if ( solFile_id ) {
    const int dontReadData = 0 ;
    mUnknowns = ucg_read_sol ( dontReadData,
                               solFile_id, NULL, NULL, mVx )
      ;
  }
  



  /* Make, alloc grid. */
  uns_s *pUns = NULL ;
  if ( !make_uns_grid ( &pUns, mDim, mElems, mConn, 0, mVx, mUnknowns, mBndFc, mBc ) ) {
    sprintf ( hip_msg, "failed to alloc for grid in ucg_read_alloc.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }





  /* Alloc a temporary list of boundary faces to be sorted and a list of
     boundary conditions using the upper bound given in the file header. */
  bndFcVx_s *pBndFcVx ;
  pUns->pBndFcVx = pBndFcVx =
    arr_malloc ( "pUns->pBndFcVx in read_gmsh", pUns->pFam,
                 mBndFc, sizeof ( bndFcVx_s ) );
  pUns->mBndFcVx = mBndFc ;



  return ( pUns ) ;
}


/******************************************************************************
  ucg_read_coor:   */

/*! Read the coordinates from a CGNS database file.
 *
 */

/*
  
  Last update:
  ------------
  23Mar12; fix bug with float->double conversion of coordinates.
  26Aug11; fix bug with coor offset 0 vs 1.
  25Jul11; various bug fixes.
  5Apr11: conceived.
  

  Input:
  ------
  file_id: opened CGNS file
  pUns: allocated unstructured grid.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ucg_read_coor ( int file_id, int nBase, int nZone, uns_s *pUns ) {

  chunk_struct *pChunk = pUns->pRootChunk ;
  int mVx = pChunk->mVerts ;
  int mDim = pUns->mDim ;


  /* Malloc a buffer. */
  double *dBuf ;
  dBuf = arr_malloc ( "dBuf in read_cg_coor", pUns->pFam, mVx, sizeof(double));



  /* Coordinates: mDim, mVx. */
  char coorName[TEXT_LEN] ;
  DataType_t cg_prec ;
  int i ;
  cgsize_t rg_min[3] = {1,0,0}, rg_max[3] = {mVx,0,0} ;
  int n ;
  vrtx_struct *pVrtx = pChunk->Pvrtx ;
  for ( i = 1 ; i <= mDim ; i++ ) {
    cg_coord_info( file_id, nBase, nZone, i, &cg_prec, coorName) ;
    cg_coord_read( file_id, nBase, nZone, coorName, cg_prec, rg_min, rg_max, dBuf ) ;

    if ( cg_prec == RealSingle ) {
      float *fc ;
      double *dc ;
      /* Repack the single prec. values into doubles. */
      for ( dc = dBuf+(mVx-1), fc = ((float*)dBuf)+(mVx-1) ; dc >= dBuf ; dc--, fc-- )
        *dc = (double) *fc ;
    }

    /* Translate into vertices. */
    for ( n = 1 ; n <= mVx ; n++ ) {
      pVrtx[n].number = n ;
      pVrtx[n].Pcoor[i-1] = dBuf[n-1] ;
    }
  }

  arr_free ( dBuf ) ;

  if ( verbosity > 3 )
    printf ( "      Found %d coordinates for %d-D grid.\n", mVx, mDim ) ;

  return ( 0 ) ;
}

/******************************************************************************
  ucg_read_conn:   */

/*! Read volumetric connectivity (elements) from a CGNS database.   
 *
 */

/*
  
  Last update:
  ------------
  3Apr12; use ucgSec[].isBnd to exclude boundary sections.
  25Jul11; various bug fixes.
  9Jul11; use init_elem.
  5Apr11: conceived.
  

  Input:
  ------
  file_id: opened CGNS file.
  pUns: allocated unstructured grid.
  ucgSec: index of sections, boundary flag.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ucg_read_conn ( int file_id, int nBase, int nZone, uns_s *pUns, ucg_sec_s ucgSec[] ) {


  /* How many sections? */
  int mSecs ;
  cg_nsections( file_id, nBase, nZone, &mSecs ) ;


  elType_e elT ; 
  const elemType_struct *pElT = NULL ;
  int mVxEl, mEl ;
  int mElWritten = 0 ;
  chunk_struct *pChunk = pUns->pRootChunk ;
  elem_struct *pEl = pChunk->Pelem+1 ;
  vrtx_struct *pVrtx = pChunk->Pvrtx, **ppVx = pChunk->PPvrtx ;
  int nSec ;
  char someStr[LINE_LEN] ;
  ElementType_t cg_ElType ;
  cgsize_t nBeg, nEnd ;
  int cg_nBndry, cg_prtFlg ;
  int mElT ;
  int *iC, n, k ;
  cgsize_t mConn, *piConn, *piC ;
  const int *cg2hE ;
  for ( nSec = 1 ; nSec <= mSecs ; nSec++ ) {
    cg_section_read( file_id, nBase, nZone, nSec,
                     someStr, &cg_ElType, &nBeg, &nEnd, &cg_nBndry, &cg_prtFlg ) ;

    elT = cgh_cg2hip_elType ( cg_ElType ) ;
    if ( elT != noEl )
      pElT = elemType + elT ;


    if ( ( mElT = ucgSec[nSec].mElems ) > 0 ) {
      /* This section has volume elements. */

      cg_ElementDataSize( file_id, nBase, nZone, nSec, &mConn ) ;
      /* Temporary connectivity list by node number, sized for the largest type. */
      piConn = arr_malloc ( "pUns->iConn in ucg_read_conn", 
                            pUns->pFam, mConn, sizeof( *piConn ) ) ;


      
      cg_elements_read( file_id, nBase, nZone, nSec, piConn, NULL ) ;

      for ( piC = piConn, n = 0 ; n < mElT ; n++ ) {
        /* Fill the elements. */

        if ( cg_ElType == MIXED ) {
          elT = cgh_cg2hip_elType ( *piC++ ) ;
          pElT = elemType + elT ;
        }

        if ( pElT->mDim == pUns->mDim ) {
          mVxEl = pElT->mVerts ;
          cg2hE = cg2h[elT] ;

          init_elem ( pEl, elT, ++mElWritten, ppVx ) ;
          for ( k = 0 ; k < mVxEl ; k++ )
            ppVx[ cg2hE[k] ] = pVrtx + *piC++ ;

          pEl++ ;
          ppVx += mVxEl ;          
        }
      }


      if ( verbosity > 3 ) {
        mEl = pEl - pChunk->Pelem -1 ;
        printf ( "      Found %d %s in section %d.\n", mEl, elemType[elT].name, nSec ) ;
      }

      arr_free ( piConn ) ;
    } //     if ( ucgSec[nSec].isVol ) {
  } // for ( nSec = 1 ; nSec <= mSecs ; nSec++ ) {


  if ( ppVx > pChunk->PPvrtx + pChunk->mElem2VertP+1 )
    hip_err ( fatal, 0, "written beyond end of pChunk->PPvrtx in ucg_read_conn" ) ;
  if ( pEl > pChunk->Pelem + pChunk->mElems+1 )
    hip_err ( fatal, 0, "written beyond end of pChunk->Pelem in ucg_read_conn" ) ;
  
  
  return ( 0 ) ;
}

/******************************************************************************
  ucg_read_bnd_faces:   */

/*! Read the connectivity of boundary faces.
 *
 */

/*
  
  Last update:
  ------------
  6Apr13; promote possibly large int to type ulong_t.
  3Apr12; cut out of ucg_read_bnd, make work for mixed element sections.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ucg_read_bnd_faces ( int file_id, int nZone, int nBase, 
                         uns_s *pUns, ucg_sec_s ucgSec[] ) {

  int mSec ;
  cg_nsections( file_id, nBase, nZone, &mSec ) ;




  /* Read the boundary faces. */
  elType_e elT ; 
  const elemType_struct *pElT = NULL ;
  chunk_struct *pChunk = pUns->pRootChunk ;
  bndFcVx_s *pBF = pUns->pBndFcVx ;
  int nSec ;
  char someStr[LINE_LEN] ;
  ElementType_t cg_ElType ;
  cgsize_t nBeg, nEnd ;
  int cg_nBndry, cg_prtFlg ;
  int mElSec ;
  cgsize_t mConn, *piConn, *piC ; 
  int k ;
  int nFc ;
  bndFcVx_s *pBf = pUns->pBndFcVx ;
  for ( nSec = 1 ; nSec <= mSec ; nSec++ ) {
    cg_section_read( file_id, nBase, nZone, nSec,
                     someStr, &cg_ElType, &nBeg, &nEnd, &cg_nBndry, &cg_prtFlg ) ;

    elT = cgh_cg2hip_elType ( cg_ElType ) ;
    if ( elT != noEl )
      /* Valid, recognised element type, same for the entire section. */
      pElT = elemType + elT ;
    else if ( cg_ElType != MIXED ) {
      sprintf ( hip_msg, "unrecognised element type %d in ucg_read_bnd_faces.", cg_ElType ) ;
      hip_err ( 0, fatal, hip_msg ) ;
    }

    ucgSec[nSec].pBFcBeg = pBf ;
    if ( ucgSec[nSec].mBndFc > 0 ) {
      mElSec = nEnd-nBeg+1 ;

      cg_ElementDataSize( file_id, nBase, nZone, nSec, &mConn ) ;
      /* Temporary connectivity list by node number, sized for the largest type. */
      piConn = arr_malloc ( "pUns->iConn in ucg_read_conn", 
                            pUns->pFam, mConn, sizeof( *piConn ) ) ;


      if ( pBf + ucgSec[nSec].mBndFc > pUns->pBndFcVx+pUns->mBndFcVx ) 
        hip_err ( fatal, 0, "too many boundary faces in ucg_read_bnd_faces.\n" ) ;

      cg_elements_read( file_id, nBase, nZone, nSec, piConn, NULL ) ;


      for ( nFc = nBeg, piC = piConn ; nFc <= nEnd ; nFc++ ) {

        if ( cg_ElType == MIXED ) {
          /* MIXED lists first the type of entity. */
          elT = cgh_cg2hip_elType ( *piC++ ) ;
          if ( elT == noEl )
            hip_err ( fatal, 0, "unrecognised element type in ucg_read_bnd_faces" ) ;
          pElT = elemType + elT ;
        }

        if ( pElT->mDim == pUns->mDim-1 ) {
          /* This is a boundary face. */
          pBf->mVx = pElT->mVerts ;

          /* Forming vertices. . */
          for ( k = 0 ; k < pBf->mVx ; k++ ) {
            pBf->ppVx[k] = pChunk->Pvrtx + *piC++ ;
          }
        
          pBf++ ;
        }
        else {
          /* Skip. */
          piC +=  pElT->mVerts ;
        }
      } // for ( nFc = nBeg, iC = iConn ; nFc <= nEnd ; nFc++ ) {

      arr_free ( piConn ) ;
      
    } // if ( ucgSec[nSec].isBnd ) {
    ucgSec[nSec].pBFcEnd = pBf ;
  } // for ( nSec = 1 ; nSec <= mSec ; nSec++ ) {

  

  
  if ( (pBf-pUns->pBndFcVx) - pUns->mBndFcVx ) {
    sprintf ( hip_msg, "expected %"FMT_ULG" bnd faces, found %td in ucg_read_bnd_faces.\n",
              pUns->mBndFcVx, pBF-pUns->pBndFcVx ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  return ( 0 ) ;
}




/******************************************************************************
  ucg_read_bnd:   */

/*! Read boundary information from unstructured CGNS.
 *
 */

/*
  
  Last update:
  ------------
  3Apr12; split out ucg_read_bnd_faces, ucg_read_bc.
  25Jul11; various bug fixes.
  5Apr11: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int ucg_read_bnd ( int file_id, int nBase, int nZone, uns_s *pUns,
                   ucg_sec_s ucgSec[], GridLocation_t bcLoc ) {

  /* Read the faces, update face indices in ucgSec. */
  ucg_read_bnd_faces ( file_id, nBase, nZone, pUns, ucgSec ) ;

  /* Read bcs and families and associate each vertex-described boundary face with a bc.*/
  ucg_read_bc ( file_id, nBase, nZone, pUns, ucgSec, bcLoc ) ;

  
  /* Match boundary faces. */
  if ( !match_bndFcVx ( pUns ) ) {
    hip_err ( fatal, 0, "could not match boundary faces in ucg_read_bnd.\n" ) ;
  }
  arr_free ( pUns->pBndFcVx ) ; 


  return ( 0 ) ;
}

/******************************************************************************
  h5r_args:   */

/*! pull of arguments from the line with arguments.
 */

/*
  
  Last update:
  ------------
  17Feb19; remove asciiBound.
  25Feb18; prepend path to solfile before checking existence.
  15Dec16; intro -i flag for skeleton read
  18Dec14; intro -s argument for solfile, allow to leave out asciiBound.
  6Apr14; accept hdf5a and hdfa to trigger -a option.
  1Apr12: derived from h5w_args.

  Input:
  ------
  argLine[]: text line with all args, including hdf keyword
  
  Output:
  ------
  gridFile{}: 
  abndFile{}: asciBound 
  solFile{}: 

*/


void ucg_flag_reset () {
  int ucg_flag_info  = DEFAULT_ucg_flag_info ;
  int ucg_flag_zone  = DEFAULT_ucg_flag_zone ;
  return ;
}


void ucg_args  ( char argLine[], char gridName[LINE_LEN], char *gridFile, char *solFile ) {

  /* Reset. */
  ucg_flag_info = DEFAULT_ucg_flag_info ;

  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  /* Parse line of unix-style optional args. */
  char c ;
  strncpy ( solFile, gridFile, LINE_LEN ) ;
  while ((c = getopt_long ( mArgs, ppArgs, "in:s:z::",NULL,NULL)) != -1) {
    switch (c)  {
    case 'i': // info only, no storage of data.
      ucg_flag_info = 1;
      break;
    case 'n':
      strncpy ( gridName, optarg, LINE_LEN ) ;
      break;
    case 's': // solution
      if ( optarg && atoi( optarg ) == 0 ){
        /* arg 0 given: -s0, don't read a solution. */
        solFile[0] = '\0' ;
      }
      else if ( optarg ) {
        /* solfile is arg. */
        strcpy ( solFile, optarg ) ;
      }
      else 
        /* Default behaviour, sol in gridfile. */
        ;
      break;
    case 'z':  // read zone info, not used yet.
      if ( optarg && atoi( optarg ) == 0 )
        /* 0 arg given. use -z0 */
        ucg_flag_zone = 0;
      else 
        /* No arg given, or arg not 0, use -z1 */
        ucg_flag_zone = 1;
      break;
    case '?':
      if (isprint (optopt)) {
        sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
      else {
        sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }
  
  /* Check presence of non-opt (non -) args. */
  if ( optind < mArgs )
    strcpy ( gridFile, ppArgs[optind] ) ;
  else
    hip_err ( fatal, 0, "missing grid file name for read hdf\n" ) ;

  /* Cut & Paste error? There is no asciibound with cgns.  
     if ( optind+1 < mArgs )
     strcpy ( abndFile, ppArgs[optind+1] ) ;*/


  if ( optind+2 < mArgs )
    strcpy ( solFile, ppArgs[optind+2] ) ;

  return ;
}



/******************************************************************************
  read_uns_cgns:   */

/*! Read an unstructured CGNS database.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------ 
  7aug20; init gridFile, solFile to '\0'.
  25Jun20; 0 is EXIT_SUCCESS in return code of read_mb_cgns, not 1.
  24Apr20; call mcgns if grid is structured.
  11Mar19; add forgotten prepend_path to grid/solFile
  17Feb19; fix broken cut&paste of cg_args.
  18Dec19; read/alloc for solution.
  8Mar18; replace ucg_open with hcg_open. 
  1Jul16; new interface to check_uns.
  18Feb13; dealloc piConn here, after it became global.
  3Apr12; interface changes in ucg_read_*
  4Apr11: conceived.
  

  Input:
  ------
  keyword: optional arguments
  gridFile:
  solFile:
  abndFile: if bnd info in an AVBP-style .asciiBound file is given, this supersedes
  boundary condition definition in the CGNS gridfile.

  Changes To:
  -----------
  gridFile, solFile, abndFile: path pre-pended.
    
  Returns:
  --------
  0 on failure, 1 on success
  
*/

ret_s read_uns_cgns ( char* argLine ) {
  ret_s ret = ret_success () ;
  

  char gridNameArgs[LINE_LEN] ;
  gridNameArgs[0] = '\0' ;
  char gridFile[LINE_LEN], solFile[LINE_LEN] ;
  gridFile[0] = solFile[0] = '\0' ;
  ucg_args ( argLine, gridNameArgs, gridFile, solFile ) ;

  /* Open grid file. */
  cgsize_t file_id, solFile_id ;
  prepend_path ( gridFile ) ;
  file_id = hcg_open ( gridFile,  CG_MODE_READ ) ;

  /* Is it unstructure?. For the time being, only one zone. */
  cgsize_t nBase = 1, nZone = 1 ;
  ZoneType_t zoneType ;
  cg_zone_type( file_id, nBase, nZone, &zoneType ) ;
  if ( zoneType == Structured ) {
    cg_close( file_id ) ; 

    /* Call structured cgns read, bypassing mcgns keyword. */
    if ( read_mb_cgns ( gridFile, solFile, 1, 0 ) == EXIT_SUCCESS )
      ret.status = success ;
    else
      ret.status = fatal ;
    return ( ret) ;
  }
  else if ( zoneType != Unstructured ) {
    ulong_t unZone = nZone ;
    sprintf ( hip_msg,
              "zone no %"FMT_ULG" is neither structured nor unstructured in read_uns_cgns\n",
              unZone ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  
    
  if ( solFile[0] == '\0' ) {
    /* No solution. */
    solFile_id = 0 ;
  }
  else if ( strcmp ( gridFile, solFile ) ) {
    /* Different grid and sol files. Open sol file. */
    prepend_path ( solFile ) ;
    solFile_id = hcg_open ( solFile, CG_MODE_READ ) ;
  }
  else {
    solFile_id = file_id ;
  }
    

  /* Read sizes and alloc. */
  uns_s *pUns ;
  ucg_sec_s *ucgSec ;
  GridLocation_t bcLoc ;
  pUns = ucg_alloc ( file_id, solFile_id, nBase, nZone, &ucgSec, &bcLoc ) ; 
  if ( gridNameArgs[0] != '\0' )
    strncpy ( pUns->pGrid->uns.name, gridNameArgs, LINE_LEN ) ;
  else
    snprintf ( pUns->pGrid->uns.name, LINE_LEN, "grid_%d", pUns->nr ) ; 


  /* Read the grid. */
  ucg_read_coor ( file_id, nBase, nZone, pUns ) ;
  ucg_read_conn ( file_id, nBase, nZone, pUns, ucgSec ) ;
  ucg_read_bnd  ( file_id, nBase, nZone, pUns, ucgSec, bcLoc ) ;

  arr_free ( ucgSec ) ;

  /* Terminate access to the file. */
  cg_close( file_id ) ; 





  make_uns_bndPatch ( pUns ) ;

  /* number_uns_grid ( pUns ) ; Done in check_uns. */

  /* Validate the grid. */
  check_uns ( pUns, check_lvl ) ;




  /* Is there a solution to read? */
  if ( solFile[0] != '\0' ) {
    file_id = ucg_open ( gridFile ) ;

    const int doReadData = 1 ;
    ucg_read_sol ( doReadData, file_id,
                   pUns, pUns->pRootChunk, pUns->pRootChunk->mVerts ) ;
    cg_close( file_id ) ; 
  }

  return ( ret ) ;
}
