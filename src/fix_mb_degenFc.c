/*
   fix_mb_degenFc.c
   Check all degenerated subfaces whether they really fall onto a line or a point.
   Try to fix deficiencies. 
 
   Last update:
   ------------
   6Jan15; rename pFamMb to more descriptive pArrFamMb
   10Jul98; move checking of subface match from get_mbMatchFc to mb_degen_subFc.

   Contains:
   ---------
   mb_degen_subFc:  
   mb_measure_dgFc:
   mb_add_dgFc:
   mb_fix_dgFc:
   mb_free_dgFc:
   is_degen_subfc:
*/

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

const extern int verbosity ;
extern Grids_struct Grids ;
extern arrFam_s *pArrFamMb ;
extern char hip_msg[] ;

#define MIN_RATIO ( 1.e2 )

/* Define a linked list of degenerate faces. */
typedef struct _dgFace_s dgFace_s ;
struct _dgFace_s {
  const double *PcoBeg ;     /* Beginning and end of the axis. */
  const double *PcoEnd ;
  
  const subFace_struct *pSf ;/* The defining master subface with index of the face, */
  int axisDim ;              /* the axis and the collapsing/free direction. */
  int mAxisVerts ;           /* Number of vertices on the axis. */
  int degDim ;
  
  dgFace_s *PprvDgFc ;  /* linked list. */
  dgFace_s *PnxtDgFc ;

  int mRefs ;                /* reference counter. */
} ;



/* The root is common to the file. */
dgFace_s *ProotDgFc = NULL ;


static int mb_measure_dgFc ( subFace_struct *pSf, int *PdegenFc, 
			     int *PaxisDim, int *PdegDim, double *PhMin );
static int mb_add_dgFc ( const subFace_struct *pSf,
			 const int axisDim, const int degDim,
			 dgFace_s **PPdgFc, int *PsameDir );
static int mb_fix_dgFc ( subFace_struct *pSf,
			 const int axisDim, const int degDim,
			 const dgFace_s *PdgFc, const int sameDir );
static void mb_free_dgFc ();


/******************************************************************************

  mb_degen_subFc:
  Loop over all degenerate faces. Check whether the ends at least coincide, only
  then we have a chance at fixing it. If so, check in the linked list of degenerate
  subfaces whether a face with the same endpoints exists. If yes, move all points
  onto the prescribed axis. If not, make a new entry and fix an axis.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int mb_degen_subfc ( mb_struct *Pmb ) {

  const int mDim = Pmb->mDim ;

  int axisDim=0, degDim=0, sameDir=0, returnVal=1, degenFc=0, ijk[MAX_DIM],
    indexStatic, index1, index2, multVert1, multCell1, multVert2, multCell2,
    offsetVert, offsetCell, dll, dlr, dur, dul, ll[MAX_DIM], ur[MAX_DIM], ijkO[MAX_DIM] ;
  double hMin=TOO_MUCH, *pCoIjk, *pCoIjkO ;
  subFace_struct *pSf, **ppSf ;
  dgFace_s *PdgFc=NULL ;
  block_struct *pBl, *pBlO ;

  /*****
    Check whether block interfaces have matching coordinates.
    *****/
  for ( pBl = Pmb->PblockS+1 ; pBl <= Pmb->PblockS+ Pmb->mBlocks ; pBl++ )
    for ( ppSf = pBl->PPsubFaces ;
	  pSf = *ppSf, ppSf < pBl->PPsubFaces + pBl->mSubFaces ; ppSf++ ) {
      /* The other block. */
      pBlO = ( pSf->PlBlock == pBl ? pSf->PrBlock : pSf->PlBlock ) ;

      /* Check matching subfaces once for the lower numbered block. */
      if ( pBlO && pBl->nr <= pBlO->nr ) {
        get_mb_subface ( pBl, pSf, mDim, ll, ur,
                         &index1, &multVert1, &multCell1,
                         &index2, &multVert2, &multCell2,
                         &offsetVert, &offsetCell, &indexStatic,
                         &dll, &dlr, &dur, &dul ) ;
      
        /* Loop over the subface and check the nodes on both side for a match. */
        ijk[indexStatic] = pSf->llLBlock[indexStatic] ;
        for ( ijk[index2] = pSf->llLBlock[index2] ;
              ijk[index2] <= pSf->urLBlock[index2] ; ijk[index2]++ )
          for ( ijk[index1] = pSf->llLBlock[index1] ;
                ijk[index1] <= pSf->urLBlock[index1] ; ijk[index1]++ ) {
            trans_l2r ( ijk, pSf->ProtL2R->rotMatrix, pSf->vertShift, ijkO ) ;
            pCoIjk = pSf->PlBlock->Pcoor +
              mDim*get_nVert_ijk( mDim, ijk, pSf->PlBlock->mVert ) ;
            pCoIjkO = pSf->PrBlock->Pcoor +
              mDim*get_nVert_ijk( mDim, ijkO, pSf->PrBlock->mVert ) ;
            if ( sq_distance_dbl( pCoIjk, pCoIjkO, mDim ) >= Grids.epsOverlapSq ) {
              sprintf ( hip_msg, "node mismatch by %g between\n"
                       "        %3d %3d %3d in block %3d at %+g %+g %+g and\n"
                       "        %3d %3d %3d in block %3d at %+g %+g %+g.\n",
                       sqrt(sq_distance_dbl( pCoIjk, pCoIjkO, mDim )),
                       ijk[0],ijk[1],ijk[2],pSf->PlBlock->nr,
                       pCoIjk[0],pCoIjk[1],pCoIjk[2],
                       ijkO[0],ijkO[1],ijkO[2],pSf->PrBlock->nr,
                       pCoIjkO[0],pCoIjkO[1],pCoIjkO[2] ) ;
              hip_err ( warning, 2, hip_msg ) ;
              vec_avg_dbl ( pCoIjk, pCoIjkO, mDim, pCoIjk ) ;
              vec_copy_dbl ( pCoIjk, mDim, pCoIjkO ) ;
              sprintf ( hip_msg, "    hip will try to average to:     %+g %+g %+g,"
                       " no guarantees.\n", pCoIjk[0],pCoIjk[1],pCoIjk[2] ) ;
              hip_err ( blank, 2, hip_msg ) ;
            }
          }
      }
    }

  /*****
    Check for faulty degeneracies.
    *****/
  
  /* Faulty degeneracies in 2-D grids are your problem. */
  if ( mDim == 2 )
    return ( 1 ) ;
  
  for ( pBl = Pmb->PblockS+1 ; pBl <= Pmb->PblockS+ Pmb->mBlocks ; pBl++ )
    /* Loop over all the subfaces. */
    for ( ppSf = pBl->PPsubFaces ;
	  pSf = *ppSf, ppSf < pBl->PPsubFaces + pBl->mSubFaces ; ppSf++ )
      if ( !pSf->PrBlock && !pSf->Pbc ) {
        /* This is a degenerate subface. Get its sizes. */
	if ( !mb_measure_dgFc ( pSf, &degenFc, &axisDim, &degDim, &hMin ) ) {
	  printf ( " FATAL: untreatable degenerate subface in mb_degen_subfc.\n" ) ;
	  returnVal = 0 ; }

	if ( degenFc ) {
	  /* This face is degenerate. Match it against the list of degenerate faces. */
	  if ( !mb_add_dgFc ( pSf, axisDim, degDim, &PdgFc, &sameDir ) ) {
	    printf ( " FATAL: failure to add degenerate subface in mb_degen_subfc.\n" ) ;
	    returnVal = 0 ; }
	  
	  /* Fix it onto the matching face. Note that the matching face can be
	     itself. */
	  if ( hMin >= Grids.epsOverlap )
	    mb_fix_dgFc ( pSf, axisDim, degDim, PdgFc, sameDir ) ;
	  
#         ifdef CHECK_FACE
	  { /* Check the face. */
	    int nDim, iAxis, iDeg, ijkAxis[3], ijkMove[3] ;
	    double *PcoAxis, *PcoMove, h ;
	    
	    for ( nDim = 0 ; nDim < mDim ; nDim++ )
	      ijkAxis[nDim] = ijkMove[nDim] = pSf->llLBlock[nDim] ;
	    
	    for ( iAxis = pSf->llLBlock[axisDim] ;
		  iAxis < pSf->urLBlock[axisDim] ; iAxis++ ) {
	      ijkAxis[axisDim] = ijkMove[axisDim] = iAxis ;
	      PcoAxis = pSf->PlBlock->Pcoor +
		mDim*get_nVert_ijk( mDim, ijkAxis, pBl->mVert ) ;
	      for ( iDeg = pSf->llLBlock[degDim] + 1 ;
		    iDeg < pSf->urLBlock[degDim] ; iDeg++ ) {
		ijkMove[degDim] = iDeg ;
		PcoMove = pSf->PlBlock->Pcoor +
		  mDim*get_nVert_ijk( mDim, ijkMove, pBl->mVert ) ;
		h = sq_distance_dbl ( PcoAxis, PcoMove, mDim ) ;
		if ( h >= Grids.epsOverlapSq )
		  printf ( " FATAL: unfixed between %d %d %d and %d %d %d by %g.\n",
			   ijkAxis[0],ijkAxis[1],ijkAxis[2],
			   ijkMove[0],ijkMove[1],ijkMove[2], h ) ;
	      }
	    }
	  }
#       endif
	}
      }

  mb_free_dgFc () ;
  return ( returnVal ) ;
}



/******************************************************************************

  mb_measure_dgFc:
  Find the minimum and maximum mesh sizes on a degenerate face. Note that this
  is 3-D only.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int mb_measure_dgFc ( subFace_struct *pSf, int *PdegenFc, 
			     int *PaxisDim, int *PdegDim, double *PhMin )
{
  block_struct *pBl = pSf->PlBlock ;
  const int mDim = pBl->PmbRoot->mDim ;
  int nDim, vertDiff, freeDir1, freeDir2, ijkMin[MAX_DIM], ijkMax[MAX_DIM], degDim, 
      staticDim, staticDir, nVertMax1, nVertMax2, axisDim ;
  double hMin1, hMax1, hMin2, hMax2, h, *Pcoor, distBeg, distEnd, *PcoMin, *PcoMax ;

  /* Face info. */
  get_static_subface ( pSf, mDim, 0, &staticDim, &staticDir, &freeDir1, &freeDir2 ) ;

  /* Edges in freeDir1. */
  for ( vertDiff = 1, nDim = 0 ; nDim < freeDir1 ; nDim++ )
    vertDiff *= pBl->mVert[nDim] ;
  hMin1 = TOO_MUCH ;
  hMax1 = -TOO_MUCH ;

  ijkMin[staticDim] = pSf->llLBlock[staticDim] ;
  for ( ijkMin[freeDir1] = pSf->llLBlock[freeDir1] ;
        ijkMin[freeDir1] < pSf->urLBlock[freeDir1] ; ijkMin[freeDir1]++ )
    for ( ijkMin[freeDir2] = pSf->llLBlock[freeDir2] ;
	  ijkMin[freeDir2] <= pSf->urLBlock[freeDir2] ; ijkMin[freeDir2]++ )
    { Pcoor = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijkMin, pBl->mVert ) ;
      h = sq_distance_dbl( Pcoor, Pcoor + mDim*vertDiff, mDim ) ;
      if ( h > hMax1 )
	nVertMax1 = get_nVert_ijk( mDim, ijkMin, pBl->mVert )  ;
      hMin1 = MIN( hMin1, h ) ;
      hMax1 = MAX( hMax1, h ) ;
    }

  /* Edges in freeDir2. */
  for ( vertDiff = 1, nDim = 0 ; nDim < freeDir2 ; nDim++ )
    vertDiff *= pBl->mVert[nDim] ;
  hMin2 = TOO_MUCH ;
  hMax2 = -TOO_MUCH ;

  ijkMin[staticDim] = pSf->llLBlock[staticDim] ;
  for ( ijkMin[freeDir2] = pSf->llLBlock[freeDir2] ;
        ijkMin[freeDir2] < pSf->urLBlock[freeDir2] ; ijkMin[freeDir2]++ )
    for ( ijkMin[freeDir1] = pSf->llLBlock[freeDir1] ;
	  ijkMin[freeDir1] <= pSf->urLBlock[freeDir1] ; ijkMin[freeDir1]++ )
    { Pcoor = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijkMin, pBl->mVert ) ;
      h = sq_distance_dbl( Pcoor, Pcoor + mDim*vertDiff, mDim ) ;
      if ( h > hMax2 )
	nVertMax2 = get_nVert_ijk( mDim, ijkMin, pBl->mVert ) ;
      hMin2 = MIN( hMin2, h ) ;
      hMax2 = MAX( hMax2, h ) ;
    }

  hMin1 = sqrt( hMin1 ) ;
  hMax1 = sqrt( hMax1 ) ;
  hMin2 = sqrt( hMin2 ) ;
  hMax2 = sqrt( hMax2 ) ;

  if ( hMax1 >= Grids.epsOverlap && hMax2 >= Grids.epsOverlap ) {
    /* Non-degenerate face. */
    *PdegenFc = 0 ;
    return ( 1 ) ;
  }
  else if ( hMax1 < Grids.epsOverlap && hMax2 < Grids.epsOverlap ) {
    if ( verbosity > 4 )
      printf ( "   INFO: Degenerate subface %d of block %d collapsed into a point:\n"
	       "         degenerate: %d, hMin: %f hMax: %f\n"
	       "         degenerate: %d, hMin: %f hMax: %f\n",
	       pSf->nr, pSf->PlBlock->nr,
	       freeDir1, hMin1, hMax1, freeDir2, hMin2, hMax2 ) ;
    axisDim = freeDir1 ;
    degDim = freeDir2 ;
  }
  else if ( hMax1 < Grids.epsOverlap ) {
    if ( verbosity > 4 )
      printf ( "   INFO: Degenerate subface %d of block %d collapsed into a line:\n"
	       "         degenerate: %d, hMin: %f hMax: %f\n"
	       "         running: %d, hMin: %f hMax: %f\n",
	       pSf->nr, pSf->PlBlock->nr,
	       freeDir1, hMin1, hMax1, freeDir2, hMin2, hMax2 ) ;
    axisDim = freeDir2 ;
    degDim = freeDir1 ;
  }
  else if ( hMax2 < Grids.epsOverlap ) {
    if ( verbosity > 4 )
      printf ( "   INFO: Degenerate subface %d of block %d collapsed into a line:\n"
	       "         running: %d, hMin: %f hMax: %f\n"
	       "         degenerate: %d, hMin: %f hMax: %f\n",
	       pSf->nr, pSf->PlBlock->nr,
	       freeDir1, hMin1, hMax1, freeDir2, hMin2, hMax2 ) ;
    axisDim = freeDir1 ;
    degDim = freeDir2 ;
  }
  else {
    if ( verbosity > 3 )
      printf ( " INFO: trying to find a degenerate axis on subface %d of block %d:\n"
	       "       running: %d, hMin: %f hMax: %f at nVert %d\n"
	       "       running: %d, hMin: %f hMax: %f at nVert %d\n",
	       pSf->nr, pSf->PlBlock->nr,
	       freeDir1, hMin1, hMax1, nVertMax1, freeDir2, hMin2, hMax2, nVertMax2 ) ;

    if ( hMin1 < hMin2 && hMax1 < hMax2 &&
	 hMin2/hMin1 > MIN_RATIO && hMax2/hMax1 > MIN_RATIO ) {
      if ( verbosity > 3 )
	printf ( "       trying %d as static index.\n", freeDir1 ) ;
      axisDim = freeDir2 ;
      degDim = freeDir1 ;
    }
    else if ( hMin2 < hMin1 && hMax2 < hMax1 &&
	      hMin1/hMin2 > MIN_RATIO && hMax1/hMax2 > MIN_RATIO ) {
      if ( verbosity > 3 )
	printf ( "       trying %d as static index.\n", freeDir2 ) ;
      axisDim = freeDir1 ;
      degDim = freeDir2 ;
    }
    else {
      printf ( " FATAL: cannot assume a static direction for subface %d of block %d.\n",
	       pSf->nr, pBl->nr ) ;
      return ( 0 ) ;
    }
   
    /* Check whether the ends of the axis match. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      ijkMin[nDim] = ijkMax[nDim] = pSf->llLBlock[nDim] ;
    ijkMax[degDim] = pSf->urLBlock[degDim] ;
    
    /* Beginning. */
    PcoMin = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijkMin, pBl->mVert ) ;
    PcoMax = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijkMax, pBl->mVert ) ;
    distBeg = sq_distance_dbl( PcoMin, PcoMax, mDim ) ;
    /* End. */
    ijkMin[axisDim] = ijkMax[axisDim] =  pSf->urLBlock[axisDim] ;
    PcoMin = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijkMin, pBl->mVert ) ;
    PcoMax = pBl->Pcoor + mDim*get_nVert_ijk( mDim, ijkMax, pBl->mVert ) ;
    distEnd = sq_distance_dbl( PcoMin, PcoMax, mDim ) ;
    
    if ( distBeg  > Grids.epsOverlapSq || distEnd  > Grids.epsOverlapSq )
    { printf ( " FATAL: ends of the degenerate axis split:"
	       " %g, %g in mb_measure_dgFc.\n", sqrt(distBeg), sqrt(distEnd) ) ;
      return ( 0 ) ;
    }
  }
  
  *PhMin = MIN( hMax1, hMax2 ) ;
  *PaxisDim = axisDim ;
  *PdegDim = degDim ;
  *PdegenFc = 1 ;
  
  return ( 1 ) ;
}

/******************************************************************************

  mb_add_dgFc:
  Match this degenerate subface against the ones in the list. If it is not
  present, add it at the end.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int mb_add_dgFc ( const subFace_struct *pSf,
			 const int axisDim, const int degDim,
			 dgFace_s **PPdgFc, int *PsameDir )
{
  dgFace_s *PdgFc, *PnewDgFc, *PlstDgFc=NULL ;
  const block_struct *pBl = pSf->PlBlock ;
  const int mDim = pBl->PmbRoot->mDim ;
  
  /* The ends of this subface. */
  const double *PcoBeg = pBl->Pcoor +
    mDim*get_nVert_ijk ( mDim, pSf->llLBlock, pBl->mVert ) ;
  const double *PcoEnd = pBl->Pcoor +
    mDim*get_nVert_ijk ( mDim, pSf->urLBlock, pBl->mVert ) ;
  
  for ( PdgFc = ProotDgFc ; PdgFc ; PdgFc = PdgFc->PnxtDgFc ) { 
    PlstDgFc = PdgFc ;
    if ( pSf->urLBlock[axisDim] - pSf->llLBlock[axisDim] + 1 == PdgFc->mAxisVerts ){
      /* There is the proper number of nodes along the axis. */
      if ( sq_distance_dbl ( PcoBeg, PdgFc->PcoBeg, mDim ) < Grids.epsOverlapSq &&
	   sq_distance_dbl ( PcoEnd, PdgFc->PcoEnd, mDim ) < Grids.epsOverlapSq ) {
        /* Ends match. */
	*PsameDir = 1 ;
	*PPdgFc = PdgFc ;
	PdgFc->mRefs++ ;
	return ( 1 ) ;
      }
      else if ( sq_distance_dbl ( PcoEnd, PdgFc->PcoBeg, mDim ) < Grids.epsOverlapSq &&
	        sq_distance_dbl ( PcoBeg, PdgFc->PcoEnd, mDim ) < Grids.epsOverlapSq ) {
        /* Ends match inversely, beginning to end. */
	*PsameDir = 0 ;
	*PPdgFc = PdgFc ;
	PdgFc->mRefs++ ;
	return ( 1 ) ;
      }
    }
  }

  /* No match found. Alloc. */
  PnewDgFc = arr_malloc ( "PnewDgFc in mb_add_dgFc", pArrFamMb, 1, 
                          sizeof( dgFace_s ) ) ;
  if ( !PnewDgFc )
    hip_err ( fatal, 0, "could  not allocate a new list entry"
              " in mb_add_dgFc." ) ;

  if ( !ProotDgFc )
  { /* The list is empty. Make this the first entry. */
    ProotDgFc = PnewDgFc ;
    PnewDgFc->PprvDgFc = PnewDgFc->PnxtDgFc = NULL ;
  }
  else
  { /* Append it to the list. */
    PlstDgFc->PnxtDgFc = PnewDgFc ;
    PnewDgFc->PprvDgFc = PlstDgFc ;
    PnewDgFc->PnxtDgFc = NULL ;
  }

  /* Fix an axis. */
  PnewDgFc->PcoBeg = PcoBeg ;
  PnewDgFc->PcoEnd = PcoEnd ;
  PnewDgFc->pSf = pSf ;
  PnewDgFc->axisDim = axisDim ;
  PnewDgFc->mAxisVerts = pSf->urLBlock[axisDim] - pSf->llLBlock[axisDim] + 1 ;
  PnewDgFc->degDim = degDim ;
  PnewDgFc->mRefs = 1 ;

  *PPdgFc = PnewDgFc ;
  *PsameDir = 1 ;
  return ( 1 ) ;
}

/******************************************************************************

  mb_fix_dgFc:
  Move the coordinates of a degenerate face onto the fixed axis of the matched
  master face.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int mb_fix_dgFc ( subFace_struct *pSf,
			 const int axisDim, const int degDim,
			 const dgFace_s *PdgFc, const int sameDir ) {
  
  const int mDim = pSf->PlBlock->PmbRoot->mDim ;
  block_struct *pBlAxis = PdgFc->pSf->PlBlock, *pBlMove = pSf->PlBlock ;
  double *PcoAxis, *PcoMove ;
  int ijkAxis[MAX_DIM], ijkMove[MAX_DIM], nDim, kAxis, iMove, nAxis, nMove ;

  /* Initialize the axis coordinate to lower left. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    ijkAxis[nDim] = PdgFc->pSf->llLBlock[nDim] ;
  nAxis = ijkAxis[PdgFc->axisDim] ;

  /* Initialize the moved coordinate. */
  if ( sameDir == 1 )
    /* Same dir, lower left. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      ijkMove[nDim] = pSf->llLBlock[nDim] ;
  else
    /* Opposite dir, upper right. */
    for ( nDim = 0 ; nDim < mDim ; nDim++ )
      ijkMove[nDim] = pSf->urLBlock[nDim] ;
  nMove = ijkMove[axisDim] ;

  /* Loop over the layers normal to the degenerate axis in the subface.
     Note that the ends have to match. */
  for ( kAxis = 1 ; kAxis < PdgFc->mAxisVerts-1 ;  kAxis++ )
  { /* The fixed coordinate on the axis. */
    ijkAxis[PdgFc->axisDim] = nAxis + kAxis ;
    ijkMove[axisDim] = nMove + sameDir*kAxis ;
    PcoAxis = pBlAxis->Pcoor + mDim*get_nVert_ijk ( mDim, ijkAxis, pBlAxis->mVert ) ;
    
    /* Loop over all nodes inbetween and set them to ijkAxis. */
    for ( iMove = pSf->llLBlock[degDim] ; iMove <= pSf->urLBlock[degDim] ; iMove++ )
    { ijkMove[degDim] = iMove ;
      PcoMove = pBlMove->Pcoor + mDim*get_nVert_ijk ( mDim, ijkMove, pBlMove->mVert ) ;

      /* Overwrite the coordinates. */
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	PcoMove[nDim] = PcoAxis[nDim] ;
    }
  }

  return ( 1 ) ;
}

/******************************************************************************

  mb_free_dgFc:
  Free the memory of linked list of degenerate faces.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void mb_free_dgFc ()
{
  dgFace_s *PdgFc, *PprvDgFc ;

  if ( !ProotDgFc )
    /* Nothing to free. */
    return ;

  /* Walk to the end of the list. */
  for ( PdgFc = ProotDgFc ; PdgFc->PnxtDgFc ; PdgFc = PdgFc->PnxtDgFc )
    ;

  /* Walk backward and free. */
  while ( PdgFc )
  { PprvDgFc = PdgFc->PprvDgFc ;
    arr_free ( PdgFc ) ;
    PdgFc = PprvDgFc ;
  }

  return ;
}

/******************************************************************************

  is_degen_subfc:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int is_degen_subfc ( const block_struct *pBl, const subFace_struct *pSf, const int mDim )
{
  int mDegenFaces, ll[MAX_DIM], ur[MAX_DIM],
    nCell, index1, multVert1, multCell1, index2, multVert2, multCell2,
    offsetVert, offsetCell, indexStatic, dll, dlr, dur, dul, n1, n2,
    nFcVert[4], mCollV, kV, kkV ;
  double dist ;

  mDegenFaces = 0 ;
  
  /* Get the subface indices, etc. */
  get_mb_subface ( pBl, pSf, mDim, ll, ur,
		   &index1, &multVert1, &multCell1,
		   &index2, &multVert2, &multCell2,
		   &offsetVert, &offsetCell, &indexStatic,
		   &dll, &dlr, &dur, &dul ) ;
  
  /* Loop over the subface. */
  n2 = ll[index2] ;
  n1 = ll[index1] - 1 ; 
  while ( ( nCell = cell_loop_subfc ( ll, ur, mDim,
		  		    &n1, index1, multCell1, &n2, index2, multCell2,
                                      offsetCell ) ) )
    if ( !pBl->PelemMark || pBl->PelemMark[nCell] ) {
      get_mb_boundFace ( n1, multVert1, n2, multVert2, offsetVert,
			 dll, dlr, dur, dul, nFcVert ) ;

      /* Count the number of collapsed vertices. Check whether vertex kV is already
	 used as kkV. */
      for ( mCollV = 0, kV = 1 ; kV < ( mDim == 2 ? 2 : 4 ) ; kV++ )
	for ( kkV = 0 ; kkV < kV ; kkV++ ) {
	  dist = sq_distance_dbl ( pBl->Pcoor + mDim*nFcVert[kV],
				   pBl->Pcoor + mDim*nFcVert[kkV], mDim ) ;
	  if ( dist < Grids.epsOverlapSq ) {
	    mCollV++ ;
	    break ;
	  }
	}

      if ( mCollV > ( mDim == 2 ? 0 : 1 ) )
	mDegenFaces++ ;
    }
  
  return ( mDegenFaces ) ;
}

