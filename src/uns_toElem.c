/*
   uns_toElem.c:
   Make a list of something identified by a linear integer pointing to an element.

   Last update:
   ------------
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.


   This file contains:
   -------------------
   make_toElem
   free_toElem
   add_toElem
   get_toElem
   get_empty_toElem
   loop_toElem

   listToElem
   printToElem
   
*/

#include "cpre.h"
#include "cpre_uns.h"
#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;

/* Are typdef's really global? */
typedef struct {
  elem_struct *pElem ; /* The element. */
  ulong_t nNxt ;           /* The next item in the linked list. */
} toElem_s ;

struct _llToElem_s {
  ulong_t mEntries ;       /* The number of entry points, eg. mVerts for vx->elem. */
  ulong_t mToElem ;        /* The total size of the list, at least mEntries. */
  ulong_t nRootFree ;      /* The first free slot after mEntries. */
  toElem_s *pToElem ;   /* The list. */
} ;



/******************************************************************************

  make_toelem:
  Allocate a list of edge to element pointers. The initial allocation size will
  become the reserved entry space. Link empty slots.
  
  Last update:
  ------------
  12Feb19; promote int to ulong_t.
  27Aug14; tidy up to suppress compiler warning, use hip_err.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

llToElem_s *make_toElem ( llToElem_s **ppllToElem, arrFam_s *pFam, ulong_t mEnt ) {
  
  llToElem_s *pllToElem = NULL ;
  toElem_s *pToElem = NULL ;
  ulong_t n, nLast ;

  if ( mEnt < 1 ) {
    sprintf ( hip_msg, "invalid size %"FMT_ULG" in make_toElem.\n", mEnt ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( !(*ppllToElem) ) {
    /* No root, yet. */
    if ( !( pllToElem = arr_malloc ( "pllToElem in make_toElem", pFam, 
                                     1, sizeof ( *pllToElem ) ) ) ) {
      hip_err ( fatal, 0, "could not make llToElem root in make_toElem.\n" ) ;
    }
    else {
      pllToElem->mEntries = pllToElem->mToElem = mEnt ;
      pllToElem->nRootFree = mEnt+1 ;
      pToElem = pllToElem->pToElem = NULL ;
    }
  }
  else {
    pllToElem = (*ppllToElem) ;
    pToElem = pllToElem->pToElem ;
  }
    

  /* Realloc the list to mEnt. Don't shrink it further than mEntries. */
  mEnt = MAX( mEnt, pllToElem->mEntries ) ;
  if ( !( pToElem = arr_realloc ( "pToElem in make_toElem",  pFam, pToElem,
                                  mEnt+1, sizeof( toElem_s ) ) ) ) {
    hip_err ( fatal,0, "could not realloc toElem list in make_toElem.\n" ) ;
  }
  pllToElem->pToElem = pToElem ;

  if ( !*ppllToElem ) {
    /* New allocation. No free slots. Reset the fixed part of the list. */
    for ( n = 0 ; n <= mEnt ; n++ ) {
      pToElem[n].nNxt = 0 ;
      pToElem[n].pElem = NULL ;
    }
    pllToElem->nRootFree = 0 ;
  }
  else  if ( mEnt > pllToElem->mToElem ) {
    /* Link the List of appended free slots. */
    for ( n = pllToElem->mToElem+1 ; n <= mEnt ; n++ ) {
      pToElem[n].nNxt = n+1 ;
      pToElem[n].pElem = NULL ;
    }
    pToElem[mEnt].nNxt = 0 ;
    
    pllToElem->nRootFree = pllToElem->mToElem+1 ;
    pllToElem->mToElem = mEnt ;
  }
  else {
    /* The list shrank. Redo list of empty slots. */
    for ( pllToElem->nRootFree = nLast = 0, n = pllToElem->mEntries+1  ;
	  n <= mEnt ; n++ ) {
      if ( !pToElem[n].pElem ) {
	/* This slot is unused. */
	if ( !pllToElem->nRootFree )
	  /* New root. */
	  pllToElem->nRootFree = n ;
	else
	  /* Append. */
	  pToElem[nLast].nNxt = n ;
	nLast = n ;
      }
    }
  }

  *ppllToElem = pllToElem ;
  return ( pllToElem ) ;
}



/******************************************************************************

  get_emtpy_toElem:
  Get the next free slot. If nEntry is free, give that one out. Otherwise,
  pick the first free slot off the list after mEntries.
  
  Last update:
  ------------
  12Feb19; promote int to ulong_t.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

ulong_t get_emtpy_toElem ( llToElem_s **ppllToElem, const ulong_t nEntry ) {
  ulong_t newSize, nFree ;
  
  if ( nEntry > (*ppllToElem)->mEntries ) {
    sprintf ( hip_msg, "invalid entry number %"FMT_ULG" > %"FMT_ULG" in get_emtpy_toElem.\n",
	     nEntry, (*ppllToElem)->mEntries ) ;
    hip_err ( fatal, 0, hip_msg ) ;
    return ( 0 ) ;
  }
  else if ( !(*ppllToElem)->pToElem[nEntry].pElem )
    /* Free entry slot. */
    return ( nEntry ) ;
  
  else if ( !(*ppllToElem)->nRootFree ) {
    /* List full. Realloc. */
    newSize = REALLOC_FACTOR*(*ppllToElem)->mToElem + 1 ;
    if ( !make_toElem ( ppllToElem, NULL, newSize ) ) {
      sprintf ( hip_msg, "failed to reallocate toEdge list in get_emtpy_toElem.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
      return ( 0 ) ; }
  }

  nFree = (*ppllToElem)->nRootFree ;
  (*ppllToElem)->nRootFree = (*ppllToElem)->pToElem[nFree].nNxt ;
	   
  return ( nFree ) ;
}


/******************************************************************************

  _:
  .
  
  Last update:
  ------------
   12Feb19; promote int to ulong_t.
 : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void free_toElem ( llToElem_s **ppllToElem ) {
  
  if ( *ppllToElem ) {
    arr_free ( (*ppllToElem)->pToElem ) ;
    arr_free ( *ppllToElem ) ;
    *ppllToElem = NULL ;
  }
  return ;

}


/******************************************************************************

  add_toElem:
  Add an entry.
  
  Last update:
  ------------
  12Feb19; promote int to ulong_t.
  8Apr14; use hip_err instead of print.
          initialise nLastTe to suppress comp. warning.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_toElem ( llToElem_s **ppllToElem, const ulong_t nEntry, elem_struct *pElem ) {
  
  ulong_t nTE, nLastTE=0 ;
  toElem_s *pTE ;
  
  if ( get_toElem ( *ppllToElem, nEntry, pElem, &nLastTE ) )
    /* This entry exists. */
    return ( 1 ) ;
  else if ( !( nTE = get_emtpy_toElem ( ppllToElem, nEntry ) ) )
    hip_err ( fatal, 0, "could not get a new toEdge slot in add_toElem.\n" ) ;

  /* Append. */
  pTE = (*ppllToElem)->pToElem + nTE ;
  pTE->pElem = pElem ;
  pTE->nNxt = 0 ;

  if ( nLastTE )
    /* There is already a root. */
    (*ppllToElem)->pToElem[nLastTE].nNxt = nTE ;

  return ( 1 ) ;
}


/******************************************************************************

  get_toElem:
  Find an elem listing with an entry.
  
  Last update:
  ------------
  12Feb19; promote int to ulong_t.
  8Apr14; use hip_err instead of print.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int get_toElem ( const llToElem_s *pllToElem, const ulong_t nEntry, elem_struct *pElem,
		 ulong_t *pnLastTE ) {
  
  const toElem_s *pToElem = pllToElem->pToElem, *pTE = pToElem + nEntry ;

  
  if ( nEntry > pllToElem->mEntries ) {
    sprintf ( hip_msg, "invalid entry number %"FMT_ULG" > %"FMT_ULG" in get_toElem.\n",
	     nEntry, pllToElem->mEntries ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  else if ( !pTE->pElem ) {
    /* No element yet for this vx number. */
    *pnLastTE = 0 ;
    return ( 0 ) ;
  }

  // for ( ; pTE->nNxt ; pTE = pToElem + pTE->nNxt )
  //  if ( pTE->pElem == pElem ) {
  //    *pnLastTE = pTE - pToElem ;
  //    return ( *pnLastTE ) ;
  //  }

  while ( pTE != pToElem ) {
    *pnLastTE = pTE - pToElem ;
    
    if ( pTE->pElem == pElem ) {
      /* Match. */
      return ( *pnLastTE ) ;
    }
    else {
      pTE = pToElem + pTE->nNxt ;
    }
  }

  /* Nothing found. */
  return ( 0 ) ;
}

/******************************************************************************

  loop_toElem:
  Loop over all elements associated to an entry.
  
  Last update:
  ------------
  12Feb19; promote int to ulong_t.
  : conceived.
  
  Input:
  ------
  pllToElem = the list of entities pointing to the elements.
  mEntry = number of entity to match
  pnItem = iterator, NULL to initialise

  Changes To:
  -----------
  ppElem = matching element, if any.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int loop_toElem ( const llToElem_s *pllToElem, const ulong_t nEntry,
		  ulong_t *pnItem, elem_struct **ppElem ) {
  ulong_t nNxt ;
  
  if ( !*pnItem )
    /* Initialize the loop. */
    *pnItem = nEntry ;
  else if ( ( nNxt = pllToElem->pToElem[ *pnItem ].nNxt ) )
    /* Pick the next item. */
    *pnItem = nNxt ;
  else
    /* Exhausted list. Don't change pnItem or ppElem. */
    return ( 0 ) ;

  if ( ( *ppElem = pllToElem->pToElem[ *pnItem ].pElem ) )
    /* There is data. This case only filters out a completely empty linked list. */
    return ( 1 ) ;
  else
    return ( 0 ) ;
}

void listToElem ( const llToElem_s *pllToElem ) {
  ulong_t n ;

  for ( n = 1 ; n <= pllToElem->mEntries ; n++ )
    printToElem ( pllToElem, n ) ;

  return ;
}

void printToElem ( const llToElem_s *pllToElem, const ulong_t n ) {
  ulong_t ni = 0 ;
  elem_struct *pElem ;

  printf ( " entry %"FMT_ULG": ", n ) ;
  while ( loop_toElem ( pllToElem, n, &ni, &pElem ) )
    printf ( " %"FMT_ULG",", pElem->number ) ;
  printf ( "\n" ) ;

  return ;
}
