/*
  file_name.c:
*/

/*! short descr. for doxygen
 *
 *   more details
 *
 */


/* 
  Last update:
  ------------
  ; conceived

  
  
  This file contains:
  -------------------
 
*/
#include <strings.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"

#include "proto_mb.h"

#include "cgnslib.h"
#include "proto_cgns.h"

extern Grids_struct Grids ;

extern const int verbosity ;
extern char hip_msg[] ;

extern const char version[] ;

extern rotation_struct *pRotations ;
extern arrFam_s *pArrFamMb ; 


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  18DEec19: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

ret_s mcg_read_sol ( int file_id, int nBase, mb_struct *pMb,
                     const int skip, const int kBlock, const int doReadData ) {
  ret_s ret = ret_success () ;


  /* How many zones. */
  int mZonesFile = 0 ;
  if ( cg_nzones( file_id, nBase, &mZonesFile ) )
    hip_err ( fatal, 0, "failed to read number of zones in mcg_read_coor." ) ;

  int mZones = pMb->mBlocks ;
  
  int nZBeg = ( kBlock ? kBlock : 1 ) ;
  int nZEnd = ( kBlock ? kBlock : mZonesFile ) ;
  int mBlRead ;
  int nZ ;
  block_struct *blockS = pMb->PblockS, *pBl ;
  int ier ;
  cgsize_t znSize[MAX_DIM*MAX_DIM] ; //3*vx, 3*el, 3*?
  int kDim ;
  for ( mBlRead = 1, nZ = nZBeg ; nZ <= nZEnd ; nZ++, mBlRead++ ) {
    pBl = blockS+mBlRead ;

 
    /* Check size against block dims. */
    char zoneName[LINE_LEN] ;
    int iSize[MAX_DIM] = {0} ;  // uns CGNS: # of verts, # of elems, 0.
    ier = cg_zone_read( file_id, nBase, nZ, zoneName, znSize ) ;
    for ( kDim = 0 ; kDim < pMb->mDim ; kDim++ ) {
      if ( znSize[kDim] != pBl->mVertFile[kDim] ) {
        sprintf ( hip_msg, "block %d, dim %d: expected %d, found %d in ucg_read_sol.",
                  pBl->nr, kDim, pBl->mVertFile[0],  (int)znSize[kDim] ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }



    
    /* How many flow sol nodes? */
    int mSols ;
    int ier ;
    ier = cg_nsols(file_id, nBase, nZ, &mSols);
    if ( ier ) {
      strncpy (hip_msg, cg_get_error(), LINE_LEN ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    if ( mSols > 1 ) {
      hip_err ( warning, 0, "reading multiple flowSolution nodes not implemented,"
                " reading only first one." ) ;
    }
    int nSol = 1 ;
  
    /* Only one flow solution node so far. */
    if ( cg_goto( file_id, nBase, "Zone_t", nZ,
                  "FlowSolution_t", nSol ,"end" ) ) {
      hip_err ( fatal, 0, "failed goto in CGNS sol read in ucg_read_sol." ) ; 
    }

    /* Where are unkowns stored? */
    char solname[LINE_LEN] ;
    GridLocation_t locUnknown ;
    ier = cg_sol_info( file_id, nBase, nZ, nSol, solname,
                       &locUnknown ) ;
    if ( locUnknown != Vertex ) {
      hip_err ( fatal, 0, "hip expects unknowns at vertices." ) ;
    }



    

    /* Number of unknowns. It appears, no vectors. */
    int mUnknowns ;
    ier = cg_nfields ( file_id, nBase, nZ, nSol, &mUnknowns ) ;

    if ( verbosity > 1 ) {
      sprintf ( hip_msg, "found %d unknowns in solution %d of base %d, zone %d.",
                mUnknowns, nBase, nZ, nSol ) ;
      hip_err ( info, 1, hip_msg ) ;
    }





  
    double *dBuf = NULL ;
    if ( doReadData ) {
      /* All vars are written as vectors, so allocate one. */
      dBuf = arr_malloc ( "dBuf in mcg_read_sol", pArrFamMb, pBl->mVertsBlock+1, sizeof( double ) ) ;

      /* No recognised type of vars. */
      pMb->varList.varType = noType ;

 
      /* Alloc a field of unknowns with block. */
      pBl->Punknown = arr_malloc ( "pBl->Punknown in mcg_read_sol", pArrFamMb,
                                   mUnknowns*( pBl->mVertsBlock+1 ),
                                   sizeof( *(pBl->Pcoor) ) ) ;

      
    }


  }
  
  return ( ret ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_read_coor:
*/
/*! Read coordinates for all blocks of an mb grid in cgns.
 *
 */

/*
  
  Last update:
  ------------
  23Apr18; intro blockNo, allow to pick out a specific block.
  6Mar18: conceived.
  

  Input:
  ------
  file_id : id of cgns database.
  nBase: id of base.
  pMb: pointer to mb grid.
  skip: coarsening divisor.

  Changes To:
  -----------
  *pMb: mb grid.

    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mcg_read_coor ( int file_id, int nBase, mb_struct *pMb,
                    const int skip, const int kBlock ) {

  /* How many zones. */
  int mZonesFile = 0 ;
  if ( cg_nzones( file_id, nBase, &mZonesFile ) )
    hip_err ( fatal, 0, "failed to read number of zones in mcg_read_coor." ) ;

  int mZones = ( kBlock ? 1 : mZonesFile ) ;  
  /* Allocate for mBlocks. */
  if ( !( pMb->PblockS = make_blocks ( mZones+1 ) ) ) {
    hip_err ( fatal, 0, "mem alloc for blocks failed in mcg_read_coor." ) ;
  }
  pMb->mBlocks = mZones ;

  
  int nZ ;
  int mDim = 0, kDim ;
  int mVxFile ;
  ZoneType_t zoneType ;
  cgsize_t znSize[9] ;
  int iDimZ[MAX_DIM] ;
  block_struct *blockS = pMb->PblockS, *pBl ;
  char coorName[TEXT_LEN] ;
  DataType_t cg_prec ;
  int i ;
  cgsize_t rg_min[3] = {1,1,1}, rg_max[3] ;
  int n ;
  double *pCoTmp, *pCoT, *pCo ;
  int ijk[MAX_DIM], nVert ;
  int mBlRead ;
  int nZBeg, nZEnd ;
  nZBeg = ( kBlock ? kBlock : 1 ) ;
  nZEnd = ( kBlock ? kBlock : mZonesFile ) ;
  for ( mBlRead = 1, nZ = nZBeg ; nZ <= nZEnd ; nZ++, mBlRead++ ) {
    pBl = blockS+mBlRead ;

    cg_zone_type( file_id, nBase, nZ, &zoneType ) ;
    if ( zoneType != Structured ) {
      sprintf ( hip_msg,
                "zone no %d is not structured in mcg_read_alloc\n", nZ ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    /* Name and size. Note that cgns writes sizes per dim, hence 
       mI, mJ, mK nodes, mI, mJ, mK cells, 0, 0, 0 faces. */
    cg_zone_read( file_id, nBase, nZ, pBl->name, znSize ) ;
    pBl->mVertFile[0] = rg_max[0] = znSize[0] ;
    pBl->mVertFile[1] = rg_max[1] = znSize[1] ;
    pBl->mVertFile[2] = rg_max[2] = znSize[2] ;


    /* Check if 2-D by looking at the 3rd dimension. */
    mDim = pMb->mDim = ( pBl->mVertFile[2] == 1 ? 2 : 3 ) ;
    pBl->nr = pBl - blockS ;
    pBl->skip = skip ;
    pBl->mSubFaces = 0 ;
    pBl->PPsubFaces = NULL ;
    pBl->PmbRoot = pMb ;

    /* Check if skipping divides properly. */
    pBl->mVertsBlock = mVxFile = 1 ;
    for ( kDim = 0 ; kDim < 3 ; kDim++ ) {
      if ( ( pBl->mVertFile[kDim]-1 ) % skip ) {
        sprintf ( hip_msg, "block %td  (%d*%d*%d) doesn't divide by %d without remainder.",
                  pBl-blockS, pBl->mVertFile[0], pBl->mVertFile[1], pBl->mVertFile[2],
                  skip ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else {
        pBl->mVert[kDim] = ( pBl->mVertFile[kDim]-1 )/skip + 1 ;
        mVxFile *= pBl->mVertFile[kDim] ;
      }
      pBl->mVertsBlock *= pBl->mVert[kDim] ;
    }
  
    /* Alloc final coor field with block. */
    pBl->Pcoor = arr_malloc ( "pBl->Pcoor in mcg_read_coor", pArrFamMb,
                              mDim*( pBl->mVertsBlock+1 ),
                              sizeof( *(pBl->Pcoor) ) ) ;      


    /* Malloc a temporary coordinate space for all coordinates. */
    pCoTmp = arr_malloc ( "pCoTmp in read_mb_plot3d", pArrFamMb,
                          mVxFile+1, sizeof( *pCoTmp ) ) ;


    /* Read coor. */
    for ( kDim = 0 ; kDim < mDim ; kDim++ ) {
      cg_coord_info( file_id, nBase, nZ, kDim+1, &cg_prec, coorName) ;
      /* Not clear from the cgns doc, can we ask for a dataype? */
      if ( cg_coord_read( file_id, nBase, nZ, coorName,
                          RealDouble, rg_min, rg_max, pCoTmp+1 ) )
        hip_err ( fatal, 0, cg_get_error() ) ;

      // Otherwise, read the type in the database, convert if not what's needed. */
      //cg_coord_info( file_id, nBase, nZ, i, &cg_prec, coorName) ;
      //cg_coord_read( file_id, nBase, nZ, coorName,
      //               cg_prec, rg_min, rg_max, pCoTmp+1 ) ;

      // if ( cg_prec == RealSingle ) {
      //   float *fc ;
      //   double *dc ;
      //   /* Repack the single prec. values into doubles. */
      //   for ( dc = pCoTmp+(mVxFile-1), fc = ((float*)pCoTmp)+(mVxFile-1) ;
      //         dc >= pCoTmp ; dc--, fc-- )
      //     *dc = (double) *fc ;
      // }

      if ( skip != 1 ) {
        /* Skip all unused vertices. */
        pCo = pBl->Pcoor + mDim + kDim ;
        for ( ijk[2] = 1 ; ijk[2] <= pBl->mVertFile[2] ; ijk[2] += skip )
          for ( ijk[1] = 1 ; ijk[1] <= pBl->mVertFile[1] ; ijk[1] += skip )
            for ( ijk[0] = 1 ; ijk[0] <= pBl->mVertFile[0] ; ijk[0] += skip ) {
              nVert = get_nVert_ijk ( mDim, ijk, pBl->mVertFile ) ;
              *pCo = pCoTmp[nVert] ;
              pCo += mDim ;
            }
      }
      else {
        /* Copy the component into x,y,z. order. */
        for ( pCo = pBl->Pcoor+mDim+kDim, pCoT = pCoTmp+1 ;
              pCoT <= pCoTmp+mVxFile ; pCo+=mDim, pCoT++ ) {
          *pCo = *pCoT ;
        }
      }
    }

    arr_free ( pCoTmp ) ;
  }
  
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_find_block:
*/
/*! given a block name, find its number.
 *
 */

/*
  
  Last update:
  ------------
  8Mar18: conceived.
  

  Input:
  ------
  pMb: multiblock grid
  nghBlock: name of block to find.
    
  Returns:
  --------
  NULL on failure, pointer to block if found.
  
*/

block_struct *mcg_find_block ( const mb_struct *pMb, const char *nghBlock )  {

  block_struct *pBl ;
  for (  pBl = pMb->PblockS+1 ; pBl <= pMb->PblockS+pMb->mBlocks ; pBl++ )
    if ( !strcmp ( pBl->name, nghBlock ) )
      return ( pBl ) ;
  
  return ( NULL ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_find_lFc:
*/
/*! given a block number and window index, find a match as a left side of the face.
 *
 *
 */

/*
  
  Last update:
  ------------
  10Mar18: conceived.
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

subFace_struct *mcg_find_lFc ( const mb_struct *pMb, const block_struct *pLBl,
                               cgsize_t rg[2][MAX_DIM] ) {

  int ll[MAX_DIM], ur[MAX_DIM] ;
  ll[0] = MIN( rg[0][0], rg[1][0] ) ;
  ll[1] = MIN( rg[0][1], rg[1][1] ) ;
  ll[2] = MIN( rg[0][2], rg[1][2] ) ;
  ur[0] = MAX( rg[0][0], rg[1][0] ) ;
  ur[1] = MAX( rg[0][1], rg[1][1] ) ;
  ur[2] = MAX( rg[0][2], rg[1][2] ) ;

  subFace_struct *pSf ;
  int kDim ;
  for ( pSf = pMb->subFaceS+1 ; pSf <= pMb->subFaceS + pMb->mSubFaces ; pSf++ ) {
    if ( pSf->PlBlock == pLBl ) {
      for ( kDim = 0 ; kDim < pMb->mDim ; kDim++ ) {
        if ( pSf->llLBlockFile[kDim] != ll[kDim] ||
             pSf->urLBlockFile[kDim] != ur[kDim] ) {
          /* Mismatch. Looks like a good use of a goto, and the first in h!p! */
          goto nextFc ;
        }
      }
      /* Match. */
      return ( pSf ) ;
    }
  nextFc :
    ;
  }

  /* No match found. */
  return ( NULL ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_add_ifc:
*/
/*! add an internal block interface
 *
 */

/*
  
  Last update:
  ------------
  10Mar18: extracted from mcg_read_block_fc
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

subFace_struct *mcg_add_ifc ( subFace_struct *pSf, const char *iFcName,
                              cgsize_t thisRg[2][MAX_DIM], cgsize_t nghRg[2][MAX_DIM],
                              const int skip, 
                              mb_struct *pMb,
                              block_struct *pBl, block_struct *pRBl ) {


  pSf++ ;
  if ( pSf > pMb->subFaceS+pMb->mSubFaces ) {
    pMb->mSubFaces++ ;
    pMb->subFaceS = arr_realloc ( "pMb->pSubFaces in mcg_add_ifc", pArrFamMb,
                                  pMb->subFaceS,
                                  pMb->mSubFaces+1, sizeof ( *pMb->subFaceS ) ) ;
    pSf = pMb->subFaceS+pMb->mSubFaces ;
  }

  
  strncpy ( pSf->name, iFcName, TEXT_LEN-1 ) ;
  pSf->nr = pSf-pMb->subFaceS ;
  pSf->Pbc = NULL ;

  pBl->mSubFaces++ ;
  pSf->PlBlock = pBl ;

  pRBl->mSubFaces++ ;
  pSf->PrBlock = pRBl ;

  int kDim ;
  for ( kDim = 0 ; kDim < pMb->mDim ; kDim++ ) {
    /* List it. */
    pSf->llLBlockFile[kDim] = MIN( thisRg[0][kDim], thisRg[1][kDim] ) ;
    pSf->urLBlockFile[kDim] = MAX( thisRg[0][kDim], thisRg[1][kDim] ) ;
    /* Check whether the subface indices divide by skip. */
    mb_apply_skip ( pSf->name, pSf->llLBlockFile, kDim, kDim, skip, pSf->llLBlock ) ; 
    mb_apply_skip ( pSf->name, pSf->urLBlockFile, kDim, kDim, skip, pSf->urLBlock ) ; 
        
    pSf->llRBlockFile[kDim] = MIN( nghRg[0][kDim], nghRg[1][kDim] ) ;
    pSf->urRBlockFile[kDim] = MAX( nghRg[0][kDim], nghRg[1][kDim] ) ;
    /* Check whether the subface indices divide by skip. */
    mb_apply_skip ( pSf->name, pSf->llRBlockFile, kDim, kDim, skip, pSf->llRBlock ) ; 
    mb_apply_skip ( pSf->name, pSf->urRBlockFile, kDim, kDim, skip, pSf->urRBlock ) ; 
  }

  /* Find the translation operation between the two sides. */
  if ( !find_rot_ijk ( pSf, pMb->mDim ) ) {
    sprintf ( hip_msg, "could not match the subface named %d "
              " in mcg_read_block_fc.\n", pBl->nr ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  else {
    get_mb_elemShift ( pSf, pMb->mDim ) ;
    get_mb_vertShift ( pSf, pMb->mDim ) ;
  }

  if ( pMb->mDim == 2 ) {
    pSf->llLBlockFile[2] = pSf->urLBlockFile[2] = 1 ;
    pSf->llLBlock[2] = pSf->urLBlock[2] = 1 ;
  }

  
  return ( pSf ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_add_bc:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  10Mar18: extracted from mcg_read_block_fc
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

subFace_struct *mcg_add_bc ( subFace_struct *pSf, char *bcText,
                             cgsize_t thisRg[2][MAX_DIM], const int skip,
                             mb_struct *pMb, block_struct *pBl ) {

  pSf++ ;
  if ( pSf > pMb->subFaceS+pMb->mSubFaces ) {
    pMb->mSubFaces++ ;
    pMb->subFaceS = arr_realloc ( "pMb->pSubFaces in mcg_add_bc", pArrFamMb,
                                  pMb->subFaceS,
                                  pMb->mSubFaces+1, sizeof ( *pMb->subFaceS ) ) ;
    pSf = pMb->subFaceS+pMb->mSubFaces ;
  }


      
  pSf->Pbc = find_bc ( bcText, 1 ) ;
  strncpy ( pSf->name, bcText, LINE_LEN-1 ) ;
  pSf->nr = pSf-pMb->subFaceS ;
  pSf->PlBlock = pBl ;
  pBl->mSubFaces++ ;
  pSf->PrBlock = NULL ;

  int kDim ;
  for ( kDim = 0 ; kDim < pMb->mDim ; kDim++ ) {
    /* List it. */
    pSf->llLBlockFile[kDim] = MIN( thisRg[0][kDim], thisRg[1][kDim] ) ;
    pSf->urLBlockFile[kDim] = MAX( thisRg[0][kDim], thisRg[1][kDim] ) ;
    /* Apply skip. (*/
    mb_apply_skip ( pSf->name, pSf->llLBlockFile, kDim, kDim, skip, pSf->llLBlock ) ; 
    mb_apply_skip ( pSf->name, pSf->urLBlockFile, kDim, kDim, skip, pSf->urLBlock ) ; 
  }

  if ( pMb->mDim == 2 ) {
    pSf->llLBlockFile[2] = pSf->urLBlockFile[2] = 1 ;
    pSf->llLBlock[2] = pSf->urLBlock[2] = 1 ;
  }
  
      

  return ( pSf ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_add_per_bc:
*/
/*! add two periodic bcs for each periodic 1to1 interface.
 *
 */

/*
  
  Last update:
  ------------
  28Apr18; add block no to the iFc name.
  10mar18: conceived.
  

  Input:
  ------
  pSf = last used subFace_struct
  iFcName = name in cgns file for interface
  perDir = +1 if same, -1 if opposite direction to first pair
  thisRg = index range for interface in left block
  nghRg = index range for interface in right block
  skip = read every skip'th node
  pMb = mb grid
  pLBl = left block
  pRBl = right block

  Returns:
  --------
  pointer to added subFace.
  
*/

subFace_struct *mcg_add_per_bc ( subFace_struct *pSf, const char *iFcName,
                                 const int perDir,
                                 cgsize_t thisRg[2][MAX_DIM], cgsize_t nghRg[2][MAX_DIM],
                                 const int skip, 
                                 mb_struct *pMb,
                                 block_struct *pLBl, block_struct *pRBl ) {

  char iFcNameBl[LINE_LEN] ;
  char perBcText[LINE_LEN] ;


  /* Does this one exist already in the other direction? */
  subFace_struct *pSf2 ;
  if ( ( pSf2 = mcg_find_lFc ( pMb, pRBl, nghRg ) ) )
    return ( pSf ) ;

  /* Make a unique name out of the block names, as ifcName can repeat beteen blocks. */
  snprintf ( iFcNameBl, LINE_LEN-1, "bl%d_bl%d_%s", pLBl->nr, pRBl->nr, iFcName ) ;

  
  /* lower at pLBl. */
  if ( perDir == 1 )
    snprintf ( perBcText, LINE_LEN-1, "hip_per_inlet_%s", iFcNameBl ) ;
  else
    snprintf ( perBcText, LINE_LEN-1, "hip_per_outlet_%s", iFcNameBl ) ; 
  pSf = mcg_add_bc ( pSf, perBcText, thisRg, skip, pMb, pLBl ) ;


  /* Upper at pRBl. */
  if ( perDir == 1 )
    snprintf ( perBcText, LINE_LEN-1, "hip_per_outlet_%s", iFcNameBl ) ;
  else 
    snprintf ( perBcText, LINE_LEN-1, "hip_per_inlet_%s", iFcNameBl ) ;
  pSf = mcg_add_bc ( pSf, perBcText, nghRg, skip, pMb, pRBl ) ;
  
  return ( pSf ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_list_ppsubfc:
*/
/*!  Make a list of subfaces for each block.
 *
 */

/*
  
  Last update:
  ------------
  10Mar18: derived from  read_mb_ibc
  

  Input:
  ------

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mcg_list_ppSubFc ( mb_struct *pMb ) {

  block_struct *pBl ;
  for ( pBl = pMb->PblockS+1 ; pBl <= pMb->PblockS + pMb->mBlocks ; pBl++ )
    /* Malloc a list of subface pointers for each block. */
    pBl->PPsubFaces =  arr_calloc ( "pBl->PPsubFaces in mcg_list_ppsubfc",
                                    pArrFamMb, pBl->mSubFaces,
                                    sizeof( *pBl->PPsubFaces ) ) ;


  /* Fill the list of subface pointers with each block. */
  subFace_struct *pSf ;
  for ( pSf = pMb->subFaceS + 1 ; 
        pSf <= pMb->subFaceS + pMb->mSubFaces ; pSf++ ) {
    put_mb_subFc ( pSf->PlBlock, pSf ) ;
    if ( pSf->PrBlock )
      put_mb_subFc ( pSf->PrBlock, pSf ) ;
  }
  
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  fun_name:
*/
/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  28Apr18; doc i/o vars.
  : conceived.
  

  Input:
  ------
  mDim: spatial dim of grid.
  RotationAngle[3] = rotation angles around x,y,z given in hdf as float value
  Translation[3] = translation vec x,y,z given in hdf as float value
  iFcName = name of the interface

  Changes To:
  -----------
  pFoundPer = 1 if periodicity is found.
  pIsRot = 1 if per operation is rotation
  dblRotAngle0 = rotation angles around x,y,z as double of the first per oper found.
  dblTrans0[3] = translation vec x,y,z as double of the first per oper found.

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mcg_perDir ( int *pFoundPer, int *pIsRot, const int mDim,
                 double dblRotAngle0[MAX_DIM], double dblTrans0[MAX_DIM],
                 float RotationAngle[MAX_DIM], float Translation[MAX_DIM],
                 const char *iFcName ) { 

  float *pfVal ;
  double *pdVal0, dblVal[MAX_DIM] ;
  int perDir=0 ;
  

  if ( *pFoundPer ) {
    /* Make sure there is single periodicity and in the same direction. */
    if ( *pIsRot ) {
      pfVal = RotationAngle ;
      pdVal0 = dblRotAngle0 ;
    }
    else {
      pfVal = Translation ;
      pdVal0 = dblTrans0 ;
    }

    dblVal[0] = pfVal[0] ;        
    dblVal[1] = pfVal[1] ;        
    dblVal[2] = pfVal[2] ;
    /* We don't have a valid eps yet, so use 1e-10. */
    if ( sq_distance_dbl ( dblVal, pdVal0, mDim ) < 1.e-10 ) {
      /* Same translation/rotation as the others. */
      perDir = 1 ;
    }
    else {
      /* Make sure it is opposite. */
      vec_mult_dbl ( dblVal, -1.0, mDim ) ;
      if ( sq_distance_dbl ( dblVal, pdVal0, mDim ) > 1.e-10 ) {
        sprintf ( hip_msg,
                  "periodicity of %s is neither aligned nor opposite earlier ones.\n"
                  "        To obtain valid period. setup, make them the same.",
                  iFcName ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      perDir = -1 ;
    }
  }
  else {
    /* Declare main periodicity. */
    *pFoundPer = 1 ;
    dblRotAngle0[0] = RotationAngle[0] ;
    dblRotAngle0[1] = RotationAngle[1] ;
    dblRotAngle0[2] = RotationAngle[2] ;
    dblTrans0[0] = Translation[0] ;        
    dblTrans0[1] = Translation[1] ;        
    dblTrans0[2] = Translation[2] ;

    /* One deg of rot in rad would be 0.0086071 */
    if ( vec_len_dbl ( dblRotAngle0, 3 ) > 1.e-5 )
      *pIsRot = 1 ;
    else
      *pIsRot = 0 ;
  }
  

  return ( perDir ) ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mcg_read_block_fc:
*/
/*! read the block faces, boundary or interface, from struct cgns.
 *
 *
 */

/*
  
  Last update:
  ------------
  2Sep21; change type of fcDim from int to cgsize_t
  29Jun20; read general connectivity patches as bc patch.
  23Apr18; intro blockNo, allow to pick out a specific block.
  7Mar18: conceived.
  

  Input:
  ------
  file_id: id of cgns file.
  nBase: cgns base id.
  skip: coarsening divisor. 
  

  Changes To:
  -----------
  *pMb: mb grid
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int mcg_read_block_fc ( int file_id, int nBase, mb_struct *pMb,
                        const int skip, const int kBlock ) {

 
  const int mDim = pMb->mDim ;

  /* Number of block interfaces. Alloc for that size, then realloc for per bnd later. */
  int mIntFc ;
  int mBndFc=0, mBndFcZ ;
  cg_n1to1_global(file_id, nBase, &mIntFc );
  
  int mBlRead ;
  int nZBeg, nZEnd ;
  int nZ ;
  int ier ;
  int mGenConn ;
  nZBeg = ( kBlock ? kBlock : 1 ) ;
  nZEnd = ( kBlock ? kBlock : pMb->mBlocks ) ;
  for ( mBlRead = 1, nZ = nZBeg ; nZ <= nZEnd ; nZ++, mBlRead++ ) {
    ier = cg_nbocos(file_id, nBase, nZ, &mBndFcZ ) ;
    mBndFc += mBndFcZ ;

    /* General connectivity, e.g. mixing/sliding planes. 
       Record each side separately as a bc. */
    ier = cg_nconns(file_id, nBase, nZ, &mGenConn ) ;
    mBndFc += mGenConn ;
  }

  

  /* Alloc a list of subfaces. */
  pMb->mSubFaces = mBndFc+mIntFc ;
  pMb->subFaceS = arr_malloc ( "pMb->subFaceS in mcg_read_block_fc",pArrFamMb,
                               pMb->mSubFaces+1, sizeof ( *pMb->subFaceS ) ) ;


  

  int kFc ;
  block_struct *pBl, *pRBl = NULL ;
  int mIntFcZ ;
  char iFcName[TEXT_LEN], nghBlock[TEXT_LEN] ;
  cgsize_t thisRg[2][MAX_DIM], nghRg[2][MAX_DIM] ;
  int transform[3] ;
  float RotationCenter[MAX_DIM], RotationAngle[MAX_DIM], Translation[MAX_DIM] ;
  double dblRotAngle0[MAX_DIM], dblTrans0[MAX_DIM] ;
  int isRot ;
  int isPer, foundPer = 0 ;
  int perDir ;
  subFace_struct *pSf = pMb->subFaceS ;
  int mFam, kF ;
  char bcText[TEXT_LEN], bcText2[TEXT_LEN] ;
  BCType_t cg_BCType ;
  PointSetType_t cg_PtSetType ;
  char famName[LINE_LEN] ;
  cgsize_t fcDim[MAX_DIM] ;
  int cg_NormalIndex ;
  cgsize_t cg_NormalListFlag ;
  DataType_t cg_NormalDataType ;
  int cg_mDataSets ;
  int nFamBC, nGeo ;
  char bcType[MAX_BC_CHAR] ;
  bc_struct *pBc ;
  nZBeg = ( kBlock ? kBlock : 1 ) ;
  nZEnd = ( kBlock ? kBlock : pMb->mBlocks ) ;
  for ( mBlRead = 1, nZ = nZBeg ; nZ <= nZEnd ; nZ++, mBlRead++ ) {
    pBl = pMb->PblockS+mBlRead ;

    /* Read block interfaces. */
    /* Number of interfaces on this block. */
    cg_n1to1( file_id, nBase, nZ, &mIntFcZ);
    
    for ( kFc = 1 ; kFc <= mIntFcZ ; kFc++ ) {
      cg_1to1_read( file_id, nBase, nZ, kFc, iFcName, nghBlock,
                    (cgsize_t *) thisRg, (cgsize_t *) nghRg, transform ) ;

      if ( !kBlock ) {
        if ( !( pRBl = mcg_find_block ( pMb, nghBlock ) ) )
        hip_err ( fatal, 0,
                  "could not find ngh block in mcg_read_block_fc.") ;
      }
      
      isPer = !cg_1to1_periodic_read(file_id, nBase, nZ, kFc,
                                     RotationCenter, RotationAngle, Translation) ;
      
      if ( kBlock || nZ <= pRBl->nr ) {
        /* Add each interface only once. */
        if ( isPer ) {
          /* If needed, swap periodic interfaces to have all point the same way. 
             Single periodicity only for now. */
          perDir = mcg_perDir ( &foundPer, &isRot, mDim, dblRotAngle0, dblTrans0,
                                RotationAngle, Translation, iFcName ) ;
          pSf = mcg_add_per_bc ( pSf, iFcName, perDir, thisRg, nghRg, skip, pMb, pBl, pRBl ) ;
        }
        else if ( kBlock ) {
          /* Single block read for debug, turn the interface into a bc. */
          pSf = mcg_add_bc ( pSf, iFcName, thisRg, skip, pMb, pBl ) ;
        }
        else
          pSf = mcg_add_ifc ( pSf, iFcName, thisRg, nghRg, skip, pMb, pBl, pRBl ) ;
      }
    }


    /* Families. */
    cg_nfamilies( file_id, nBase, &mFam) ;
    for ( kF = 1 ; kF <= mFam ; kF++ ) {
      cg_family_read( file_id, nBase, kF, bcText, &nFamBC, &nGeo ) ;
      cg_fambc_read ( file_id, nBase, nFamBC, 1, bcText2, &cg_BCType ) ;
      
      /* Add to list of bcs. */
      pBc = find_bc ( bcText, 1 ) ;
      hcg_bcTypeDecode( cg_BCType, bcType ) ;
      strcpy ( pBc->type, bcType ) ;
    }



    /* Boundary faces. */
    ier = cg_nbocos( file_id, nBase, nZ, &mBndFcZ ) ;

    for ( kFc = 1 ; kFc <= mBndFcZ ; kFc++ ) {
      isPer = 0 ;

      cg_boco_info( file_id, nBase, nZ, kFc, 
                    bcText, &cg_BCType, &cg_PtSetType, fcDim,
                    &cg_NormalIndex, &cg_NormalListFlag, &cg_NormalDataType, 
                    &cg_mDataSets ) ;
    
      if ( cg_PtSetType != PointRange ) {
        hip_err(fatal,0,
                "PointSetType expected as PointRange in mcg_read_block_fc");
      }

      /* Is it periodic declared by bnd name? Then ignore family def. */
      if ( !strncmp ( "hip_per_inlet", bcText, strlen( "hip_per_inlet" ) ) ||
           !strncmp ( "hip_per_outlet", bcText, strlen( "hip_per_outlet" ) ) )
        isPer = 1 ;

      if ( !isPer && cg_BCType == FamilySpecified ) {
        /* Non-periodic bc is grouped by family. Find the matching family. */
        if ( ( ier = cg_goto( file_id, nBase, "Zone_t", nZ ,
                              "ZoneBC_t", 1, "BC_t", kFc, NULL ) ) ) {
          sprintf ( hip_msg, "bc is FamilySpecified, but has no FamilyName,\n"
                             "       using bc name." ) ;
          hip_err ( warning, 1, hip_msg ) ;
          strcpy ( famName, bcText ) ;
        }
        
        cg_famname_read ( famName ) ;
        if ( !(pBc = find_bc ( famName, 0 ) ) ) {
          sprintf ( hip_msg, "could not find family named %s,"
                    " creating bc with this name.", famName ) ;
          hip_err ( warning, 1, hip_msg ) ;
          pBc = find_bc ( famName, 1 ) ;
        }
        /* mcg_add_bc identifies bc by text. */
        strcpy ( bcText, famName ) ;
      }
      else {
        /* Bc directly specified with the section. */
        pBc = find_bc ( bcText, 1 ) ;
        hcg_bcTypeDecode( cg_BCType, bcType ) ;
        strcpy ( pBc->type, bcType ) ;
      }
      

      /* Don't want' normals, hence final arg is NULL. */
      cg_boco_read( file_id, nBase, nZ, kFc, (cgsize_t *) thisRg, NULL ) ;

      pSf = mcg_add_bc ( pSf, bcText, thisRg, skip, pMb, pBl ) ;
   
    }


    /* Averaging interfaces, mixing planes. */
    ier = cg_nconns(file_id, nBase, nZ, &mGenConn ) ;

    char connectname[LINE_LEN] ;
    GridLocation_t location ;
    GridConnectivityType_t connect_type ;
    PointSetType_t ptset_type ;
    cgsize_t npnts ;
    char donorname[LINE_LEN] ;
    ZoneType_t donor_zonetype ;
    PointSetType_t donor_ptset_type ;
    DataType_t donor_datatype ;
    cgsize_t ndata_donor ;
    AverageInterfaceType_t AverageInterfaceType ;
    cgsize_t *donor_data ;
    for ( kFc = 1 ; kFc <= mGenConn ; kFc++ ) {
    
      ier = cg_conn_info(file_id, nBase, nZ, kFc,
                         connectname,
                         &location, &connect_type,
                         &ptset_type, &npnts, donorname,
                         &donor_zonetype, &donor_ptset_type,
                         &donor_datatype, &ndata_donor);

      if ( ptset_type == PointRange ) {
        //donor_data = arr_malloc ("pMb->subFaceS in mcg_read_block_fc",pArrFamMb,
        //                         ndata_donor, sizeof (*donor_data)) ;
        ier = cg_conn_read(file_id, nBase, nZ, kFc,
                           (cgsize_t *) thisRg,
                           donor_datatype, (cgsize_t *) NULL ); //donor_data);
        //arr_free ( donor_data ) ;
      }
      else
        hip_err ( fatal, 0, "hip can only handle PointRange data"
                  "  to specifiy boundaries or interfaces for structured blocks" ) ;

      ier = cg_conn_average_read(file_id, nBase, nZ, kFc,
                           &AverageInterfaceType);

      pSf = mcg_add_bc ( pSf, connectname, thisRg, skip, pMb, pBl ) ;
    }
  }
  
  if ( kBlock )
    /* Count only bnd and inter fc of this block. */
    pMb->mSubFaces = mIntFcZ + mBndFcZ ;
  mcg_list_ppSubFc ( pMb ) ;

  
  return ( EXIT_SUCCESS ) ;
}


/***************************************************************************

  PUBLIC

**************************************************************************/


/******************************************************************************
  read_mb_cgns:   */

/*! Read a structured cgns file.
 *
 */

/*
  
  Last update:
  ------------
  25Jun20; move calculation of epsOverlap before face match, otherwise
           corner node comparison can fail unless exact.
  23Apr18; intro blockNo, allow to pick out a specific block.
  6Mar18: conceived.
  

  Input:
  ------
  gridFile: name of cgns file
  skip: if not 1, read every skip'th node.
  kBlock: if nonzero, read only block kBlock and its interfaces.
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int read_mb_cgns ( char *gridFile, char *solFile, const int skip, const int kBlock ) {

  /* Open grid file. Use hip version to test for existence first. */
  int file_id = hcg_open ( gridFile, CG_MODE_READ ) ;
  hip_err ( info, 1, "\n Reading structured cgns file." ) ;

 
   /* Allocate an instance of a multiblock root. */
  mb_struct *pMb = arr_calloc ( "pMb in read_mb_cfdrc",
                     pArrFamMb, 1, sizeof ( mb_struct ) ) ;


  /* Read the grid. */
  int nBase = 1 ;
  mcg_read_coor ( file_id, nBase, pMb, skip, kBlock ) ;


  /* initialise epsOverlap based on the non-degenerate minimal edge length. */
  double hMinMbSq ;
  if ( Grids.epsOverlap == DEFAULT_epsOverlap ) {
    /* Not changed yet. Adjust with actual value. */

    hMinMbSq = get_mb_hMinSq ( pMb ) ;
    Grids.epsOverlapSq = 0.9*0.9*hMinMbSq ;
    Grids.epsOverlap = sqrt( Grids.epsOverlapSq ) ;
  }


  
  mcg_read_block_fc  ( file_id, nBase, pMb, skip, kBlock ) ;


  int solFile_id = 0  ;
  int nZone = 0 ; // 0 = any
  int nSol = 0 ;
  if ( solFile[0] != '\0' ) {
    solFile_id = hcg_open ( solFile, CG_MODE_READ ) ;

    const int doReadData = 1 ;
    mcg_read_sol ( solFile_id, nBase, pMb, skip, kBlock, doReadData ) ;
  }
  

  /* Terminate access to the file. */
  cg_close( file_id ) ; 




  /* Count the number of elements and vertices in the entire block. */
  mb_count ( pMb ) ;
  mb_degen_subfc ( pMb ) ;
  mb_size ( pMb ) ;
  mb_bb ( pMb ) ;

  sprintf ( hip_msg, "        read structured grid with %d blocks, %d elements, %d nodes.",
            pMb->mBlocks, pMb->mElems, pMb->mVerts ) ;
  hip_err ( blank, 1, hip_msg ) ;

  /* Link bcs, get the block and bc bounding boxes. */
  mb_bcSubFc ( pMb->PblockS, pMb->mBlocks ) ;
  mb_bcBox ( pMb->mDim ) ;

  /* Done. */
  printf ( "\n" ) ;

  /* No unknowns. */
  pMb->varList.varType = noVar ;
  pMb->varList.mUnknowns = pMb->varList.mUnknFlow = 0 ;
  
  /* Make a new grid. */
  grid_struct *pGrid = make_grid () ;
  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid ;

  
  pGrid->mb.type = mb ;
  pGrid->mb.Pmb = pMb ;
  pGrid->mb.mDim = pMb->mDim ;
  pGrid->uns.pVarList = &(pMb->varList) ;
  

  return ( EXIT_SUCCESS ) ;
}

