/*
   uns2D_uns3D.c:
   Create a 3-D unstructured grid from translating 2-D slices.

   Last update:
   ------------
   18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
   8Feb99; intro of the axi-extrusion option.

   Contains:
   ---------

*/

#include "cpre.h"
#include "cpre_uns.h"

#include "proto.h"
#include "proto_uns.h"

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern Grids_struct Grids ;
extern const elemType_struct elemType[] ;


/******************************************************************************

  :
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static void x_zy ( double *pCo ) {

  double y = pCo[1] ;
  pCo[1] = - pCo[2] ;
  pCo[2] = y ;

  return ;
}

int rot_3d ( char rot2[] ) {

  uns_s *pUns ;
  chunk_struct *pChunk ;
  vrtx_struct *pVx, *pVxB, *pVxE ;
  int nB, nE ;
  void (*rotFun)( double *pCo ) ;
  
  
  if ( Grids.PcurrentGrid->uns.type != uns || Grids.PcurrentGrid->uns.mDim != 3 ) {
    /* 3-D unstructured only. */
    printf ( " FATAL: could not convert grid of this type.\n" ) ;
    return ( 0 ) ; }

  pUns = Grids.PcurrentGrid->uns.pUns ;

  if ( !strncmp ( rot2, "x-zy", 4 ) )
    rotFun = x_zy ;
  else {
    printf ( "  FATAL: no such coordinate rotation: %s\n", rot2 ) ;
    return ( 0 ) ; }



  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxB, &nB, &pVxE, &nE ) )
    for ( pVx = pVxB ; pVx <= pVxE ; pVx++ )
      if ( pVx->number ) {
        rotFun( pVx->Pcoor ) ;

        if ( pVx->Punknown )
          rotFun( pVx->Punknown+1 ) ;
      }
  
  return ( 1 ) ;
}


/******************************************************************************

  cp_uns2D_uns3D_:
  .
  
  Last update:
  ------------
  6Nov18; renumber to ensure contiguous numbering.
  19Dec17; new interface to make_uns.
  1Jul16; new interface to check_uns.
  8Jul15; set types 'l' and 'u' for annular extrusion.
  22Apr14; use hip_err for status printf of vertex numbers, etc.
  15Dec13; remove annCascade topo, intro axiY and axiZ.
  18Feb13; switch format specifiers to %"FMT_ULG" for ulong_t types.
  29Jun12; flag added w-component as 3rd vector component.
  17Dec11; carry over nodal flag1, carry over element zone pointer.
  27Jul11; modify to work on multigrid levels.
  9Jul11; call init_elem to initialise.
  24May09; fix bug with copying up solution fields to make space for w.
  6Apr09; fix bug with missing isPer in final ref to pBPIn.
          manage the names/cats in the new variable ordering.
  4Apr09; replace varTypeS with varList.
  22Apr04;  add warning about complex sol sets not being taken to 3D.
  13Apr00; fix bug with %mVerts2, intro periodic declaration.
  : conceived.
  
  Input:
  ------
  pUns2: the current level grid.
  zBeg, zEnd: the lowest and highest z plane coordinates.
  mSlices: the number of slices/layers of elements. That is, there
           are mSlices+1 layers of vertices.
  axis: x,y,z result in extrusion along x,y,z, a results in axisymmetric
        extrusion around the x-axis.

  Changes To:
  -----------
  Grids->PlastGrid: creates a new grid.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

uns_s *cp_uns2D_uns3D_lvl ( uns_s *pUns2, double zBeg, double zEnd, 
                            const int mSlices, char axis, double *phMin ) {

  const elemType_struct *pEl2T ;
  const faceOfElem_struct *pFoE ;

  chunk_struct *pChunk2, *pChunk3 ;
  ulong_t mVerts2, mElems2, mVerts3, mElems3, nVx2, nVx3 ;
  int nSlice ;
  ulong_t mConnEntries2, 
    mConnEntries3, nEl2, nVx0Slice, nVx0SliceP1 ;
  int kVx ;
  ulong_t mVxAxis, nrVx2[4] ;
  int mVxFc ;
  ulong_t mBndFaces2, mBndPatches2, mBndFaces3, mBndPatches3, nr0 ;
  int mVertsElem ; 
  int isPer = 1, mEq, k ;
  double deltaZ, *pCo2, *pCo3, *pCo4, *pCoor3, zSlice, alSlice, cosAl, sinAl,
    hMin= TOO_MUCH, dSinAl, dist ;
  vrtx_struct *pVrtx3, *pVx2, *pVx3, *pVx4, **ppVx3, **ppVx3_max ;
  elem_struct *pElem3, *pEl3, *pEl2, *pEl3_max, *pElemIn, *pElemOut ;
  bndFc_struct *pBndFc2, *pBndFc3, *pFc3, *pFcIn, *pFcOut, *pFc3_max ;
  bndPatch_struct *pBP2, *pBP3, *pBPIn, *pBPOut ;
  char bcText[80] ;
  varList_s *pVL ;
  var_s *pVar ;

  /* Ensure contiguous numbering. */
  pUns2->numberedType = invNum ;
  number_uns_elem_leafs ( pUns2 ) ;
  
  
  /* Allocate a chunk. */
  uns_s *pUns3 = make_uns ( NULL ) ;
  pUns3->mChunks = 1 ;
  pUns3->mDim = 3 ;

  if ( pUns2->varList.mUnknowns ) {
    if ( pUns2->varList.mUnknowns == MAX_UNKNOWNS )
      hip_err ( fatal, 0, "too many unknowns, can't add a w-velocity.\n" ) ;

    pUns3->varList = pUns2->varList ;
    pVL = &(pUns3->varList) ;
    pVar = pVL->var ;
    if ( pUns3->varList.mUnknFlow == 4 ) {
      /* An empty w will be supplemented. */
      ++pUns3->varList.mUnknFlow ;
      mEq = ++pUns3->varList.mUnknowns ;

      /* Make space for a w-component in 4th pos. */
      for ( k = mEq ; k > 3 ; k-- )
        pVar[k] = pVar[k-1] ;

      /* Insert w. */
      if ( pVL->varType == cons ) 
        sprintf ( pVar[3].name, "rhow" ) ;
      else
        sprintf ( pVar[3].name, "w" ) ;
      pVar[3].cat =  ns;
      pVar[3].isVec = 3 ;
    }
    else {
      mEq = pUns3->varList.mUnknowns ;
      if ( verbosity > 2 ) 
        hip_err ( warning, 0, 
                  " solution not augmented, no w velocity comp. supplied.\n" );
    } 
  }
  else
    mEq = 0 ;


            
  /* Extrude along which axis? */
  if ( axis == 'x' || axis == 'X' )
    axis = 'x' ;
  else if ( axis == 'y' || axis == 'Y' )
    axis = 'y' ;
  else if ( axis == 'z' || axis == 'Z' )
    axis = 'z' ;
  else if ( axis == 'a' || axis == 'A' ) {
    axis = 'a' ;
    pUns3->specialTopo = axiZ;
  }
  else {
    sprintf ( hip_msg, "unknown extrusion option %c.\n", axis ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }    

  
  if ( zBeg > zEnd ) {
    /* Swap the two to avoid negative volumes. */
    zSlice = zEnd ;
    zEnd = zBeg ;
    zBeg = zSlice ;
  }

  if ( axis == 'a' && ( zBeg == zEnd || ABS( zEnd - zBeg -360 ) < 1.e-10 ) )
    /* Full loop. Create one less vertex plane and don't create the periodic faces. */
    isPer = 0 ;


  
  /* Count the vertices and elements in the grid. */
  mVerts2 = mElems2 = mConnEntries2 = 0 ;
  for ( pChunk2 = pUns2->pRootChunk ; pChunk2 ; pChunk2 = pChunk2->PnxtChunk ) {
    mElems2 += pChunk2->mElemsNumbered ;
    mVerts2 += pChunk2->mVertsNumbered ;
    /* Grid connectivity entries. Note that mElem2VertP gives the size
       of PPvrtx, not the actual number of connectivity entries. Lets
       save the hassle of counting and possibly oversize. */
    mConnEntries2 += pChunk2->mElem2VertP ;
  }

  mElems3 = mSlices*mElems2 ;
  mVerts3 = ( mSlices+1 )*mVerts2 ;
  mConnEntries3 = 2*mSlices*mConnEntries2 ;
  hMin = pUns2->hMin ;


  /* Count the boundary patches and faces. Two patches will be formed
     from the cuts at zBeg and zEnd. */
  mBndPatches2 = mBndFaces2 = 0 ;
  for ( pChunk2 = pUns2->pRootChunk ; pChunk2 ; pChunk2 = pChunk2->PnxtChunk )
    for ( pBP2 = pChunk2->PbndPatch+1 ;
	  pBP2 <= pChunk2->PbndPatch + pChunk2->mBndPatches ;
	  pBP2++ )
      if ( pBP2->mBndFcMarked ) {
        mBndPatches2++ ;
	mBndFaces2 += pBP2->mBndFcMarked ;
      }
  mBndFaces3 = isPer*2*mElems2 + mSlices*mBndFaces2 ;
  mBndPatches3 = isPer*2 + mBndPatches2 ;


  /* Alloc for the chunk. */
  pChunk3 = append_chunk ( pUns3, pUns3->mDim, mElems3, mConnEntries3, 0,
                           mVerts3, mBndFaces3, mBndPatches3 ) ;

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "            Number of vertices:           %-"FMT_ULG"", 
              mVerts3 ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "            Number of elems:              %-"FMT_ULG"", 
              mElems3 ) ;
    hip_err ( blank, 2, hip_msg ) ;
    sprintf ( hip_msg, "            Number of bnd patches:        %-"FMT_ULG"", 
              mBndPatches3 ) ;
    hip_err ( blank, 2, hip_msg ) ;
  }




  
  /* Distance between the slices. */
  deltaZ = ( zEnd - zBeg )/mSlices ;
  dSinAl = sin( deltaZ/180.*PI ) ;
  if ( axis != 'a' )
    hMin = MIN( hMin, deltaZ ) ;

  /* Place the vertices for each slice. */
  pVrtx3 = pChunk3->Pvrtx ;
  pCoor3 = pChunk3->Pcoor ;
  for ( nVx3 = 0, pVx3 = pVrtx3, nSlice = 0 ;
        nSlice <= mSlices-(1-isPer) ; nSlice++ ) {
    zSlice = zBeg + nSlice*deltaZ ;
    alSlice = (zBeg + nSlice*deltaZ)/180.*PI ;
    cosAl = cos( alSlice ) ;
    sinAl = sin( alSlice ) ;

    for ( nVx2 = 0, pChunk2 = pUns2->pRootChunk ;
          pChunk2 ; pChunk2 = pChunk2->PnxtChunk )
      for ( pVx2 = pChunk2->Pvrtx+1 ;
            pVx2 <= pChunk2->Pvrtx + pChunk2->mVerts ; pVx2++ )
        if ( pVx2->number ) {
          nVx2++ ; nVx3++, pVx3++ ;
          if ( nVx2 != pVx2->number ) {
            sprintf ( hip_msg, "vertex %td numbered %"FMT_ULG","
                      " expected %"FMT_ULG" in cp_uns2D_uns3D.\n",
                      (pVx2-pChunk2->Pvrtx), pVx2->number, nVx2 ) ;
            hip_err ( fatal, 0, hip_msg ) ;
          }

#         ifdef CHECK_BOUNDS
          if ( nVx3 > mVerts3 )
            hip_err ( fatal, 0, "too many coordinates in cp_uns2D_uns3D." ) ;
#         endif

          pVx3->number = nVx3 ;
          pVx3->flag1 = pVx2->flag1 ;
          pCo2 = pVx2->Pcoor ;
          pCo3 = pVx3->Pcoor = pCoor3+3*nVx3 ;

          /* Unknowns. */
          if ( mEq  ) {
            pVx3->Punknown = pChunk3->Punknown + mEq*nVx3 ;

            if ( axis != 'a' ) {
              /* Copy the 2D unknowns. Set w=0 and shift the other state variables one
                 position up. */
              vec_copy_dbl ( pVx2->Punknown,       3, pVx3->Punknown ) ;
              vec_copy_dbl ( pVx2->Punknown+3, mEq-3, pVx3->Punknown+4 ) ;
              pVx3->Punknown[3] = 0. ;
            }
            else {
              /* Rotate the v,w vector into a radial one. */
              pVx3->Punknown[0] = pVx2->Punknown[0] ;
              pVx3->Punknown[1] = pVx2->Punknown[1] ;
              pVx3->Punknown[2] = cosAl*pVx2->Punknown[2] ;
              pVx3->Punknown[3] = sinAl*pVx2->Punknown[2] ;
              vec_copy_dbl ( pVx2->Punknown+3, mEq-3, pVx3->Punknown+4 ) ;
            }
          }
          else
            pVx3->Punknown = NULL ;
          
          
          if ( axis == 'x' ) {
            pCo3[1] = pCo2[0] ;
            pCo3[2] = pCo2[1] ;
            pCo3[0] = zSlice ;
          }
          else if ( axis == 'y' ) {
            pCo3[2] = pCo2[0] ;
            pCo3[0] = pCo2[1] ;
            pCo3[1] = zSlice ;
          }
          else if ( axis == 'z' ) {
            pCo3[0] = pCo2[0] ;
            pCo3[1] = pCo2[1] ;
            pCo3[2] = zSlice ;
          }
          else if ( axis == 'a' ) {
            /* Extrusion around the x-Axis. No negative y-values allowed. */
            pCo3[0] = pCo2[0] ;
            pCo3[1] = cosAl*pCo2[1] ;
            pCo3[2] = sinAl*pCo2[1] ;

            /* Keep track of the smallest vertex distance to the axis to
               fix epsOverlap. */
            if ( pCo2[1] < -Grids.epsOverlap )
              hip_err ( fatal, 0, "you cannot extrude a grid with negative y-coord.\n"
                        "         around the y-Axis. Do check your geometry." ) ;
            else if ( pCo2[1] > Grids.epsOverlap && !nSlice )
              hMin = MIN( hMin, dSinAl*pCo2[1] ) ;
          }
        }
  }




  
  if ( ABS( zEnd + zBeg ) < 1.e-10 )
    /* Force exact symmetry between planes. */
    for ( nSlice = 0 ; nSlice <= mSlices/2 ; nSlice++ ) {
      nVx0Slice   = mVerts2*nSlice ;
      nVx0SliceP1 = mVerts2*(mSlices-nSlice)  ;
      
      for ( nVx2 = 1 ; nVx2 <= mVerts2 ; nVx2++ ) {

        pVx3 = pVrtx3 + nVx2 + nVx0Slice ;
        pCo3 = pVx3->Pcoor ;
        
        pVx4 = pVrtx3 + nVx2 + nVx0SliceP1 ;
        pCo4 = pVx4->Pcoor ;
        
        if ( axis == 'x' ) {
          pCo4[1] =  pCo3[1] ;
          pCo4[2] =  pCo3[2] ;
          pCo4[0] = -pCo3[0] ;
        }
        else if ( axis == 'y' ) {
          pCo4[2] =  pCo3[2] ;
          pCo4[0] =  pCo3[0] ;
          pCo4[1] = -pCo3[1] ;
        }
        else if ( axis == 'z' ) {
          pCo4[0] =  pCo3[0] ;
          pCo4[1] =  pCo3[1] ;
          pCo4[2] = -pCo3[2] ;
        }
        else if ( axis == 'a' ) {
          pCo4[0] =  pCo3[0] ;
          pCo4[1] =  pCo3[1] ;
          pCo4[2] = -pCo3[2] ;
        }
      }
    }
    


  
  /* Create the 3D grid connectivity. */
  pEl3 = pElem3 = pChunk3->Pelem ;
  pEl3_max = pElem3 + mElems3 - 1 ;
  /* PPvrtx starts at PPvrtx[0]. */
  ppVx3 = pChunk3->PPvrtx ;
  ppVx3_max = ppVx3 + mConnEntries3 - 1 ;
  for ( nSlice = 0 ; nSlice < mSlices ; nSlice++ ) {
    /* Indices of vertex 0 in nSlice and nSlice+1. */
    nVx0Slice = nSlice * mVerts2 ;
    if ( nSlice == mSlices-1 && !isPer )
      /* Full circle, link up with the first slice. */
      nVx0SliceP1 = 0 ;
    else
      nVx0SliceP1 = nVx0Slice + mVerts2 ;
    
    for ( nEl2 = 0, pChunk2 = pUns2->pRootChunk ;
	  pChunk2 ; pChunk2 = pChunk2->PnxtChunk )
      for ( pEl2 = pChunk2->Pelem+1 ;
	    pEl2 <= pChunk2->Pelem + pChunk2->mElems ; pEl2++ )
	if ( pEl2->number ) {
#         ifdef CHECK_BOUNDS
	  if ( pEl2->number != ++nEl2 ) {
	    sprintf ( hip_msg, "element %td numbered %"FMT_ULG", expected %"FMT_ULG" in cp_uns2D_uns3D_lvl.\n",
		     (pEl2-pChunk2->Pelem), pEl2->number, nEl2 ) ;
	    hip_err ( fatal, 0, hip_msg ) ;
          }

          if ( ppVx3 + elemType[pEl2->elType].mVerts - 1 > ppVx3_max )
            hip_err ( fatal, 0, "more connectivity pointers found than counted." ) ;
          else if ( pEl3 > pEl3_max )
            hip_err ( fatal, 0, "more elements found than counted." ) ;
#         endif

          pEl2T = elemType + pEl2->elType ;
            
	  pEl3++ ;
          init_elem ( pEl3, noEl, (int)(pEl3 - pElem3), ppVx3 ) ;
          pEl3->iZone = pEl2->iZone ;
          
	  if ( pEl2->elType == tri ) {
	    /* Tri to prism. Vertices 0,1,2 go over into 3,5,0 at nSlice and
	       1,2,4 at nSlice+1. */
	    pEl3->elType = pri ;
	    pEl3->PPvrtx[0] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[0]->number ;
	    pEl3->PPvrtx[3] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[1]->number ;
	    pEl3->PPvrtx[5] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[2]->number ;
	    pEl3->PPvrtx[1] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[0]->number ;
	    pEl3->PPvrtx[2] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[1]->number ;
	    pEl3->PPvrtx[4] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[2]->number ;
	    ppVx3 += 6 ;
            mVertsElem = 6 ;
	  }
	  else {
	    /* Quad to hex. Vertices 0,1,2,3 go over into 0,1,2,3 at nSlice and
	       4,5,6,7 at nSlice+1. */
	    pEl3->elType = hex ;
	    pEl3->PPvrtx[0] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[0]->number ;
	    pEl3->PPvrtx[1] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[1]->number ;
	    pEl3->PPvrtx[2] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[2]->number ;
	    pEl3->PPvrtx[3] = pVrtx3 + nVx0Slice +   pEl2->PPvrtx[3]->number ;
	    pEl3->PPvrtx[4] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[0]->number ;
	    pEl3->PPvrtx[5] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[1]->number ;
	    pEl3->PPvrtx[6] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[2]->number ;
	    pEl3->PPvrtx[7] = pVrtx3 + nVx0SliceP1 + pEl2->PPvrtx[3]->number ;
	    ppVx3 += 8 ;
            mVertsElem = 8 ;
	  }

          
          if ( axis == 'a' ) {
            /* Find out how many vertices are on the axis. Axis vertices retain the
               number of slice 0 in all slices. */
            for ( mVxAxis = 0, kVx = 0 ; kVx < mVertsElem ; kVx++ ) {
              pCo3 = pEl3->PPvrtx[kVx]->Pcoor ;
              dist = pCo3[1]*pCo3[1] + pCo3[2]*pCo3[2] ;
              if ( dist < Grids.epsOverlapSq ) {
                mVxAxis++ ;
                /* Use the root slice vertex. */
                nr0 = (pEl3->PPvrtx[kVx]->number-1)%mVerts2+1 ;
                pEl3->PPvrtx[kVx] = pVrtx3 + nr0 ;
              }
            }

            if ( mVxAxis )
              simplify_one_elem ( pEl3, pEl3 ) ;
          }
        }
  }
  
  if ( pEl3 - pElem3 != mElems3 ) {
    sprintf ( hip_msg, "element miscount: %td found, %"FMT_ULG" expected in"
	     " cp_uns2D_uns3D.\n", (pEl3 - pElem3), mElems3 ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  pChunk3->mElems =  pChunk3->mElemsNumbered = mElems3 ;
  pChunk3->mElem2VertP = mConnEntries3 ; 



  
  /* List the boundary patches. */
  pBndFc3 = pChunk3->PbndFc ;
  pBP3 = pChunk3->PbndPatch ;
  /* Write to the allocated space starting with PbndPatch[1]. */
  pBP3->Pchunk = NULL ;
  pBP3->PnxtBcPatch = NULL ;
  pBP3->Pbc = NULL ;
  pBP3->PbndFc = pBndFc3 + 1 ;
  pBP3->mBndFc = 0 ;


  /* Boundary faces. */
  pFc3 = pBndFc3 ;
  for ( pChunk2 = pUns2->pRootChunk ; pChunk2 ; pChunk2 = pChunk2->PnxtChunk )
    for ( pBP2 = pChunk2->PbndPatch+1 ;
	  pBP2 <= pChunk2->PbndPatch + pChunk2->mBndPatches ;
	  pBP2++ )
      if ( pBP2->mBndFcMarked ) {
	/* Create a new patch. */
	++pBP3 ;
#       ifdef CHECK_BOUNDS
	  /* The last two patches are reserved for the in/out cut. */
	  if ( pBP3 > pChunk3->PbndPatch + mBndPatches3-2*isPer )
	    hip_err ( fatal, 0, "more boundary patches found than counted." ) ;
#       endif

	pBP3->Pbc = pBP2->Pbc ;
	pBP3->PbndFc = pFc3+1 ;
	pBP3->mBndFc = pBP3->mBndFcMarked = mSlices*pBP2->mBndFc ;
	pBP3->Pchunk = pChunk3 ;
	
	for ( nSlice = 0 ; nSlice < mSlices ; nSlice++ ) {
          nVx0Slice = nSlice * mVerts2 ;
          if ( nSlice == mSlices-1 && !isPer )
            /* Full circle, link up with the first slice. */
            nVx0SliceP1 = 0 ;
          else
            nVx0SliceP1 = nVx0Slice + mVerts2 ;
          
	  for ( pBndFc2 = pBP2->PbndFc ;
		pBndFc2 < pBP2->PbndFc + pBP2->mBndFc ; pBndFc2++ )
	    if ( pBndFc2->Pelem && pBndFc2->Pelem->number ) {
	      /* This is a non-degenerate physical face. */
              pEl2 = pBndFc2->Pelem ;
              pFoE = elemType[pEl2->elType].faceOfElem + pBndFc2->nFace ;
	      /* The pointer boundary face to interior cell. Make the index
		 of faces run first, then the slice index. */
	      pFc3++ ;
              pFc3->Pelem = pEl3 = pElem3 + nSlice*mElems2 + pBndFc2->Pelem->number ;
	      pFc3->Pbc = pBP3->Pbc ;

              /* Make a list of the vertices of the boundary face in 3D. Take care
                 of nodes on the axis. */
              mVxAxis = 0 ;
              nrVx2[0] = nrVx2[3] = pEl2->PPvrtx[ pFoE->kVxFace[0] ]->number ;
              if ( axis != 'a' ||
                   pEl2->PPvrtx[ pFoE->kVxFace[0] ]->Pcoor[1] > Grids.epsOverlap ) {
                nrVx2[0] += nVx0Slice ;
                nrVx2[3] += nVx0SliceP1 ;
              }
              else
                mVxAxis++ ;

              nrVx2[1] = nrVx2[2] = pEl2->PPvrtx[ pFoE->kVxFace[1] ]->number ;
              if ( axis != 'a' ||
                   pEl2->PPvrtx[ pFoE->kVxFace[1] ]->Pcoor[1] > Grids.epsOverlap ) {
                nrVx2[1] += nVx0Slice ;
                nrVx2[2] += nVx0SliceP1 ;
              }
              else
                mVxAxis++ ;

              if ( mVxAxis == 1 ) {
                /* Compress the list. */
                if ( nrVx2[1] == nrVx2[2] )
                  nrVx2[2] = nrVx2[3] ;
              }

              if ( ( mVxFc = 4-mVxAxis ) < 3 )
                /* No face left. */
                pFc3->nFace = 0 ;

              else {
                /* Find the face back in the element. */
                if ( !( pFc3->nFace = match_face_vxnr ( pEl3, nrVx2, mVxFc ) ) )
                  hip_err ( fatal, 0, "no match for boundary face in cp_uns2D_uns3D_lvl.") ;
              }
	    }
        }
      }
  
  if ( pFc3 - pBndFc3 != mSlices*mBndFaces2 ) {
    sprintf ( hip_msg, "boundary face miscount: %td found, %"FMT_ULG" expected in"
	     " cp_uns2D_uns3D.\n", (pFc3 - pBndFc3), mSlices*mBndFaces2  ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
    


  
  if ( isPer ) {
    /* List last the "inlet" at zBeg, then the "outlet" at zEnd. */
    pFcIn = pBndFc3 + mSlices*mBndFaces2 ;
    pFcOut = pFcIn + mElems2 ;
    pFc3_max = pBndFc3+mBndFaces3 ;
    pElemIn = pChunk3->Pelem ;
    pElemOut = pChunk3->Pelem + ( mSlices-1 )*mElems2 ;
    
    /* Inlet at zBeg. */
    pBPIn = pChunk3->PbndPatch + mBndPatches2 + 1 ;
    sprintf ( bcText, "first_slice_at_%c_eq_%g", axis, zBeg ) ;
    pBPIn->Pbc = find_bc ( bcText, 1 ) ;
// GS: Creates issue 1295 ( forced periodicity for non axi ) 
// Does not repeat issue 1247 therefore removed
//    pBPIn->Pbc->type[0] = 'l' ;
    /* Note that pFcIn will be incremented in the loop. */
    pBPIn->PbndFc = pFcIn+1 ;
    pBPIn->mBndFc = mElems2 ;
    pBPIn->Pchunk = pChunk3 ;
    
    /* Outlet at zEnd. */
    pBPOut = pBPIn + 1 ;
    sprintf ( bcText, "last_slice_at_%c_eq_%g", axis, zEnd ) ;
    pBPOut->Pbc = find_bc ( bcText, 1 ) ; 
// GS: Creates issue 1295 ( forced periodicity for non axi ) 
// Does not repeat issue 1247 therefore removed
//    pBPOut->Pbc->type[0] = 'u' ;
    pBPOut->PbndFc = pFcOut+1 ;
    pBPOut->mBndFc = mElems2 ;
    pBPOut->Pchunk = pChunk3 ;

    
    for ( pChunk2 = pUns2->pRootChunk ; pChunk2 ; pChunk2 = pChunk2->PnxtChunk )
      for ( pEl2 = pChunk2->Pelem+1 ;
            pEl2 <= pChunk2->Pelem + pChunk2->mElems ; pEl2++ )
        if ( pEl2->number ) { 
          pEl2T = elemType + pEl2->elType ;
          
#       ifdef CHECK_BOUNDS
          if ( pFcIn+1 > pFc3_max-mElems2 || pFcOut+1 > pFc3_max )
            hip_err ( fatal, 0, "more in/out faces found than counted." ) ;
#       endif
	  
          /* Inlet. */
          (++pFcIn)->Pelem = ++pElemIn ;
          pFcIn->Pbc = pBPIn->Pbc ;
          for ( kVx = 0 ; kVx < pEl2T->mVerts ; kVx++ ) {
            pVx2 = pEl2->PPvrtx[kVx] ;
            nrVx2[pEl2T->mVerts-kVx-1] = pVx2->number ;
          }
          if ( !( pFcIn->nFace =  match_face_vxnr ( pElemIn, nrVx2, pEl2T->mVerts ) ) )
            hip_err ( fatal, 0, "no match for inlet face in cp_uns2D_uns3D." ) ;
          
          /* Outlet. */
          (++pFcOut)->Pelem = ++pElemOut ;
          pFcOut->Pbc = pBPOut->Pbc ;
          for ( kVx = 0 ; kVx < pEl2T->mVerts ; kVx++ ) {
            pVx2 = pEl2->PPvrtx[kVx] ;
            nrVx2[pEl2T->mVerts-kVx-1] = pVx2->number ;
            if ( axis != 'a' || pVx2->Pcoor[1] > Grids.epsOverlap )
              nrVx2[pEl2T->mVerts-kVx-1] += mSlices*mVerts2 ;
          }
          if ( !( pFcOut->nFace =
                  match_face_vxnr ( pElemOut, nrVx2, pEl2T->mVerts ) ) )
            hip_err ( fatal, 0, "no match for outlet face in cp_uns2D_uns3D." ) ;
        }
  }



  /* Link all boundary patches. */
  make_uns_ppBc ( pUns3 ) ;

  /* Declare periodicity. Do this only here because we need the current grid to
     be 3D and the bcs linked. */
  if ( axis == 'a' && isPer ) {
    pBPIn->Pbc->type[0] = 'l' ; pBPIn->Pbc->type[1] = '\0' ;
    pBPOut->Pbc->type[0] = 'u' ; pBPOut->Pbc->type[1] = '\0' ;
    /* if ( !set_per_rotation ( "lu", "x", zEnd-zBeg ) ) {
      printf ( " FATAL: failed to declare periodicity in cp_uns2D_uns3D.\n" ) ;
      return ( 0 ) ; } */
  }
        

  /* Count the boundary faces. */
  count_uns_bndFc_chk ( pUns3 ) ;

  /* Validate the grid. */
  check_uns ( pUns3, check_lvl ) ;

  *phMin = hMin ;
  return ( pUns3 ) ;
}


/******************************************************************************
   cp_uns2D_uns3D_mgConn:   */

/*! Copy inter-grid connectivity to 3D.
 *
 * more detailed desc for doxygen
 *
 */

/*
  
  Last update:
  ------------
  13Jul17; rename pElCollapseTo to ppElContain.
  27Jul11: conceived.
  

  Input:
  ------
  pUns2: 2D fine grid
  pUns3: 3D fine grid
    
*/

void cp_uns2D_uns3D_mgConn ( uns_s *pUns2, uns_s *pUns3, int mSlices ) {

  int mVx3 = pUns3->mVertsNumbered ;
  int mEl3 = pUns3->mElemsNumbered ;

  pUns3->pnVxCollapseTo = 
    arr_malloc ( "pnVxCollapseTo in cp_uns2D_uns3D_mgConn", 
                 pUns3->pFam, mVx3+1, sizeof( *pUns3->pnVxCollapseTo ) ) ;
  pUns3->ppElContain = 
    arr_malloc ( "ppElContain in cp_uns2D_uns3D_mgConn", 
                 pUns3->pFam, mVx3+1, sizeof( *pUns3->ppElContain ) ) ;


  int k ;
  /* Vertices, there are mSlices+1 layers. */
  int mVx2 = pUns2->mVertsNumbered ;
  int mVx2C = pUns2->pUnsCoarse->mVertsNumbered ;
  int mEl2C = pUns2->pUnsCoarse->mElemsNumbered ;
  int nVx2, nVx3 = 1, mVxSlc = 0, mElSlc = 0 ;
  elem_struct *pElem3C = pUns3->pUnsCoarse->pRootChunk->Pelem ;
  for ( k = 0 ; k <= mSlices ; k++ ) {
    for ( nVx2 = 1 ; nVx2 <= mVx2 ; nVx2++, nVx3++ ) {
      pUns3->pnVxCollapseTo[nVx3] =  pUns2->pnVxCollapseTo[nVx2] + mVxSlc ;
      pUns3->ppElContain[nVx3] = 
        pElem3C + pUns2->ppElContain[nVx2]->number + mElSlc ;
    }
    mVxSlc += mVx2C ;
    if ( k < mSlices-1 ) {
      mElSlc += mEl2C ;
    }
  }

  return ;
}


/******************************************************************************
  cp_uns2D_uns3D:   */

/*! copy a 2D grid, possibly with multi-grid levels to 3D.
 */

/*
  
  Last update:
  ------------
  21Dec18; switch to useVxMark.
  19Dec17; keep pGrid pointer in pUns.
  11Nov17; update prompt using set_current_grid.
  17Dec11; carry over flag usage.
  27Jul11: conceived.
  

  Input:
  ------
  zBeg, zEnd: the lowest and highest z plane coordinates.
  mSlices: the number of slices/layers of elements. That is, there
           are mSlices+1 layers of vertices.
  axis: x,y,z result in extrusion along x,y,z, a results in axisymmetric
        extrusion around the x-axis.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int cp_uns2D_uns3D ( double zBeg, double zEnd, const int mSlices,
                     char axis ) {

  if ( !Grids.PcurrentGrid )
    hip_err ( warning, 0, "there is no grid to extrude." ) ;
  else if ( Grids.PcurrentGrid->uns.type != uns || Grids.PcurrentGrid->uns.mDim != 2 )
    /* 2-D unstructured only. */
    hip_err ( warning, 0, "cannot extrude a grid of this type in cp_uns2D_uns3D" ) ;

  uns_s *pUns2 = Grids.PcurrentGrid->uns.pUns ;


  /* Extrude the finest grid level. */
  double hMin ;
  uns_s *pUns3 = cp_uns2D_uns3D_lvl ( pUns2, zBeg, zEnd, mSlices, axis, &hMin ) ;



  /* Adjust the mesh size. */
  Grids.epsOverlap = .9*MIN( hMin, Grids.epsOverlap ) ;
  Grids.epsOverlapSq = Grids.epsOverlap*Grids.epsOverlap ;
  
  /* Unstructured. Make a new grid. */
  grid_struct *pGrid3 = make_grid () ;

  /* Put the chunk into Grids. */
  pGrid3->uns.type = uns ;
  pGrid3->uns.pUns = pUns3 ;
  pGrid3->uns.mDim = 3 ;
  pGrid3->uns.pVarList = &(pUns3->varList ) ;
  
  /* Retain restart info. */
  pUns3->restart = pUns2->restart ;

  pUns3->nr = pGrid3->uns.nr ;
  pUns3->pGrid = pGrid3 ;
  
  /* Make this grid the current one. */
  set_current_pGrid ( pGrid3 ) ;

  /* Copy flag usage. */
  pUns3->useVxMark = pUns2->useVxMark ;
  strcpy( pUns3->useVxMarkBy, pUns2->useVxMarkBy ) ;
  pUns3->useVxMark2 = pUns2->useVxMark2 ;
  strcpy( pUns3->useVxMark2By, pUns2->useVxMark2By ) ;
  pUns3->useVxMark3 = pUns2->useVxMark3 ;
  strcpy( pUns3->useVxMark3By, pUns2->useVxMark3By ) ;

  /* Copy zones. */
  pUns3->mZones = 0 ;
  int kZ ;
  for ( kZ = 0 ; kZ < pUns2->mZones ; kZ++ ) {
    zone_copy ( pUns3, pUns2->pZones[kZ] ) ;
  }   



  /* Extrude coarser grid levels. */
  while ( pUns2->pUnsCoarse ) {
    pUns3->pUnsCoarse =  cp_uns2D_uns3D_lvl ( pUns2->pUnsCoarse, 
                                              zBeg, zEnd, mSlices, axis, &hMin ) ;

    /* Copy inter-grid connectivity. */
    cp_uns2D_uns3D_mgConn ( pUns2, pUns3, mSlices ) ;

    pUns2 = pUns2->pUnsCoarse ;
    pUns3 = pUns3->pUnsCoarse ;  
  }


  return ( 0 ) ;
}
