/*********************************************************************
 read_mb_cfdrc.c:
 Reading a structured multiblock grid via a plot3d + ibc file. For the time
 being, use a CFDRC .ibc file for the block connectiivty and boundary
 conditions.

 Update:
 -------
 6Jan15; rename pFamMb to more descriptive pArrFamMb
         move read_mb_ibc and read_mb_phys up to avoid prototyping


 Contains:
 ---------
 read_mb_cfdrc:
 read_mb_plot3d:
 read_mb_ibc:
 read_mb_phys:

 
*/
#include <string.h>
#include <ctype.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_mb.h"
#include "proto_mb.h"

extern Grids_struct Grids ;
extern const int verbosity ;
extern rotation_struct *pRotations ;
extern arrFam_s *pArrFamMb;
extern char hip_msg[] ;


/*******************************************************************

 read_mb_ibc:
 Read structured multi-block mesh block connectivity and
 boundary condition information as given by CFDRC for the CFD-ACE solver.

 
 Last update:
 ------------
  9Jul15; initialise mSubFcUsed to suppress compiler warning.

 Input:
 ------
 
 Changes to:
 -----------

 */

int read_mb_ibc ( mb_struct *Pmb, char *PibcFile, int skip ) {
  
  FILE *ibcFile ;
  int mIntFc, mBndFc, nSubFc, mSubFcUsed=0, nDim, iSubFc, 
             ijkLo[MAX_DIM], ijkHi[MAX_DIM], nrSubFc, nBlLeft, nBlRight, misMatch ;
  block_struct *Pbl, *PblLeft, *PblRight ;
  char bndName[MAX_BC_CHAR+1], *pCh ;
  subFace_struct *Psf = NULL, **PPsf ;


  if ( verbosity > 2 )
    printf ( "       Reading %s as .ibc file.\n", PibcFile ) ;
  
  prepend_path ( PibcFile ) ;
  if ( !( ibcFile = fopen ( PibcFile, "r" ) ) ) { 
    sprintf ( hip_msg, "failed to open ibc file %s.\n", PibcFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }

  /* Skip two text lines. */
  fscanf ( ibcFile, "%*[^\n]" ) ; fscanf ( ibcFile, "\n" ) ;
  fscanf ( ibcFile, "%*[^\n]" ) ; fscanf ( ibcFile, "\n" ) ;

  /* Number of block interfaces. */
  if ( fscanf ( ibcFile, "%d%*[^\n]", &mIntFc ) != 1 )
    hip_err ( fatal, 0, "could not read number of block interfaces"
             " from .ibc file in read_mb_ibc." ) ;
  else if ( mIntFc%2 )
    hip_err ( warning, 1, "the number of block interfaces is uneven,"
              " thus inconsistent inin read_mb_ibc." ) ;
  fscanf ( ibcFile, "\n" ) ;

  /* Alloc a list of subfaces. */
  Pmb->subFaceS = arr_malloc ( "Pmb->subFaceS in read_mb_ibc",pArrFamMb,
                               mIntFc/2+1, sizeof ( subFace_struct ) ) ;

  /* Read the block interfaces. */
  for ( nSubFc = mSubFcUsed = 0 ; nSubFc < mIntFc ; nSubFc++ ) { 
    if ( fscanf ( ibcFile, "%d %d %d %d %d %d %d %d %d%*[^\n]", 
                  &nrSubFc, &nBlLeft, 
		  ijkLo, ijkHi, ijkLo+1, ijkHi+1, ijkLo+2, ijkHi+2, 
                  &nBlRight ) != 9 ) {
      sprintf ( hip_msg, "could not read internal blockface %d"
                " in read_mb_ibc\n", nSubFc ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    
    if ( nBlLeft < nBlRight ) {
      /* New subface. */
      if ( ++mSubFcUsed > mIntFc/2 ) {
	sprintf ( hip_msg, "too many block interfaces: %d from %d to %d"
                  " in read_mb_ibc\n", nSubFc, nBlLeft, nBlRight ) ;
        hip_err ( warning, 1, hip_msg ) ;

	/* Realloc, to be able to list unmatched block interfaces. */
	mIntFc += 2 ;
	Pmb->subFaceS = 
          arr_realloc ( "Pmb->subFaceS in read_mb_ibc",pArrFamMb,Pmb->subFaceS,
                       mIntFc/2+1, sizeof ( subFace_struct )) ;
      }
    
      /* Subface seems fine. List it. */
      Psf = Pmb->subFaceS + mSubFcUsed ;
      PblLeft = Pmb->PblockS + nBlLeft ;
      PblLeft->mSubFaces++ ;
      Psf->PlBlock = PblLeft ;
      Psf->nr = nrSubFc ;
      Psf->PrBlock = Pmb->PblockS + nBlRight ;
      Psf->Pbc = NULL ;
    
      for ( nDim = 0 ; nDim < Pmb->mDim ; nDim++ ) {
	/* Check whether the subface indices divide by skip. */
	if ( ( ijkHi[nDim] - ijkLo[nDim] ) % skip ) {
	  sprintf ( hip_msg, "subface %d: indices %d-%d dim %d don't"
                   " divide by %d.\n",
		   nSubFc, ijkLo[nDim],  ijkHi[nDim], nDim, skip ) ;
	  hip_err ( fatal, 0, hip_msg ) ; }

	/* List it. */
	Psf->llLBlockFile[nDim] = ijkLo[nDim] ;
	Psf->urLBlockFile[nDim] = ijkHi[nDim] ;
	Psf->llLBlock[nDim] = ( ijkLo[nDim]-1 )/skip + 1 ;
	Psf->urLBlock[nDim] = ( ijkHi[nDim]-1 )/skip + 1 ; }
    } 
    else {
      /* Old Subface. Find the already listed part. */
      PblLeft = Pmb->PblockS + nBlRight ;
      PblRight = Pmb->PblockS + nBlLeft ;
      PblRight->mSubFaces++ ;

      for ( iSubFc = 1 ; iSubFc <= mSubFcUsed ; iSubFc++ ) {
	Psf = Pmb->subFaceS + iSubFc ;
	if ( Psf->nr == nrSubFc && Psf->PlBlock == PblLeft )
	  break ; }
      if ( iSubFc > mSubFcUsed ) {
	sprintf ( hip_msg, "no matching subface found for %d,"
                  " named %d, from %d to %d.\n",
		  nSubFc, nrSubFc, nBlLeft, nBlRight ) ;
	hip_err ( fatal, 0, hip_msg ) ;
      }
      else {
	/* Fill the RHS of the subface. */
	for ( nDim = 0 ; nDim < Pmb->mDim ; nDim++ ) {
	  Psf->llRBlockFile[nDim] = ijkLo[nDim] ;
	  Psf->urRBlockFile[nDim] = ijkHi[nDim] ;
	  Psf->llRBlock[nDim] = ( ijkLo[nDim]-1 )/skip + 1 ;
	  Psf->urRBlock[nDim] = ( ijkHi[nDim]-1 )/skip + 1 ; }
	
	/* Find the translation operation between the two sides. */
	if ( !find_rot_ijk ( Psf, Pmb->mDim ) ) {
          sprintf ( hip_msg, "could not match the subface named %d "
                    " in read_mb_ibc.\n", nrSubFc ) ;
	  hip_err ( fatal, 0, hip_msg ) ; }
	else {
	  get_mb_elemShift ( Psf, Pmb->mDim ) ;
	  get_mb_vertShift ( Psf, Pmb->mDim ) ;
        }
      }
    }
  }

  /* Find out whether some faces have a mismatch. */
  for ( misMatch = 0, nSubFc = 1 ; nSubFc < (mIntFc+1)/2 ; nSubFc++ ) {
    Psf = Pmb->subFaceS + nSubFc ;
    if ( Psf->PlBlock && !Psf->PlBlock ) {
      sprintf ( hip_msg, "unmatched block interface labeled %d from"
              " block %d in read_mb_ibc.\n",
	       Psf->nr, Psf->PlBlock->nr ) ;
      hip_err ( warning, 0, hip_msg ) ;
      misMatch++ ; 
      }
  }
  if ( misMatch ) {
    sprintf ( hip_msg, "found %d mismatched block interfaces in read_mb_ibc.",
              misMatch ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  






  
  /* Number of boundary subfaces. */
  if ( fscanf ( ibcFile, "%d%*[^\n]", &mBndFc ) != 1 )
    hip_err ( fatal, 0, "could not read number of boundary subfaces"
              " from .ibc file in read_mb_ibc." ) ;

  Pmb->mSubFaces = mIntFc/2+mBndFc ;
  fscanf ( ibcFile, "\n" ) ;

  /* Realloc the list of subfaces. */
  if ( !( Pmb->subFaceS = 
          arr_realloc ( "Pmb->subFaceS in read_mb_ibc",
                        pArrFamMb,Pmb->subFaceS,
                        Pmb->mSubFaces+1, sizeof ( subFace_struct ))) )
    hip_err ( fatal, 0, "memory reallocation for the subfaces failed"
             " in read_mb_ibc.\n" ) ;

  /* Read the block boundary faces. */
  for ( nSubFc = 0 ; nSubFc < mBndFc ; nSubFc++ ) {
    Psf = Pmb->subFaceS + (++mSubFcUsed) ;
    if ( fscanf ( ibcFile, "%d %d %d %d %d %d %d %d", &nrSubFc, &nBlLeft, 
		  ijkLo, ijkHi, ijkLo+1, ijkHi+1, ijkLo+2, ijkHi+2 ) != 8 ||
	 !fgets ( bndName, MAX_BC_CHAR, ibcFile ) ) {
      sprintf ( hip_msg, "could not read boundary blockface %d in read_mb_ibc.",
	       nSubFc ) ;
      hip_err ( fatal, 0, hip_msg ) ; }

    /* Strip all leading whitespace and place a \0 after the first following
       whitespace. */
    r1_beginstring ( bndName, MAX_BC_CHAR ) ;
    /*r1_endstring ( bndName, MAX_BC_CHAR ) ;*/
    r1_stripquote ( bndName, MAX_BC_CHAR ) ;
    
    /* Truncate before the first whitespace. */
    for ( pCh = bndName ; pCh < bndName + MAX_BC_CHAR ; pCh++ )
      if ( isspace( *pCh ) ) {
        *pCh = '\0' ;
        break ; }

    /* Find a matching bc entry or make a new one. */
    Psf->Pbc = find_bc( bndName, 1 ) ;
    
    /* Subface seems fine. List it. */
    Psf->nr = nrSubFc ;
    PblLeft = Pmb->PblockS + nBlLeft ;
    PblLeft->mSubFaces++ ;
    Psf->PlBlock = PblLeft ;
    Psf->PrBlock = NULL ;

    for ( nDim = 0 ; nDim < Pmb->mDim ; nDim++ ) {
      /* Check whether the subface indices divide by skip. */
      if ( ( ijkHi[nDim] - ijkLo[nDim] ) % skip ) {
	sprintf ( hip_msg, "subface %d: indices %d-%d dim %d don't"
                " divide by %d in read_mb_ibc.",
		 nSubFc, ijkLo[nDim],  ijkHi[nDim], nDim, skip ) ;
	hip_err ( fatal, 0, hip_msg ) ; }

      /* List it. */
      Psf->llLBlockFile[nDim] = ijkLo[nDim] ;
      Psf->urLBlockFile[nDim] = ijkHi[nDim] ;
      Psf->llLBlock[nDim] = ( ijkLo[nDim]-1 )/skip + 1 ;
      Psf->urLBlock[nDim] = ( ijkHi[nDim]-1 )/skip + 1 ; }
  }



  
  /* Make a list of subfaces for each block. */
  for ( Pbl = Pmb->PblockS+1 ; Pbl <= Pmb->PblockS + Pmb->mBlocks ; Pbl++ ) {
    /* Malloc a list of subface pointers for each block. */
    Pbl->PPsubFaces =  arr_malloc ( "Pbl->PPsubFaces in read_mb_ibc",
                                    pArrFamMb,Pbl->mSubFaces,
                                    sizeof( subFace_struct * ) ) ;

    /* Reset. */
    for ( PPsf = Pbl->PPsubFaces ; 
          PPsf < Pbl->PPsubFaces + Pbl->mSubFaces ; PPsf++ )
      *PPsf = NULL ;
  }

  /* Fill the list of subface pointers with each block. */
  for ( Psf = Pmb->subFaceS + 1 ; 
        Psf <= Pmb->subFaceS + Pmb->mSubFaces ; Psf++ ) {
    if ( !put_mb_subFc ( Psf->PlBlock, Psf ) ) {
      sprintf ( hip_msg, "could not add subface %d to left block %d"
               " in read_mb_ibc.\n",
	       Psf->nr, Psf->PlBlock->nr ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
    if ( Psf->PrBlock && !put_mb_subFc ( Psf->PrBlock, Psf ) ) {
      sprintf ( hip_msg, "could not add subface %d to right block %d"
               " in read_mb_ibc.\n",
	       Psf->nr, Psf->PlBlock->nr ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  }


  /* Reset all k-indices for the subfaces to 1 in 2D. */
  if ( Pmb->mDim == 2 )
    for ( Psf = Pmb->subFaceS + 1 ; 
          Psf <= Pmb->subFaceS + Pmb->mSubFaces ; Psf++ ) {
      Psf->llLBlockFile[2] = Psf->urLBlockFile[2] = 1 ;
      Psf->llLBlock[2] = Psf->urLBlock[2] = 1 ;
    }
  

  /* Seems fine. */
  fclose ( ibcFile ) ;
  return ( 1 ) ;
}

/*******************************************************************

 read_mb_phys:
 Read structured multi-block mesh block connectivity and
 boundary condition information using a .phys file as given by ICEM
 for the CFD-ACE solver.

 
 Last update:
 ------------

 Input:
 ------
 
 Changes to:
 -----------

 */

int read_mb_phys ( mb_struct *Pmb, char *PphysFile, int skip ) {
  
  FILE *physFile ;
  int nSubFc, mSubFcUsed, nDim, iSubFc, failure,
    ijkLo[MAX_DIM], ijkHi[MAX_DIM], nrSubFc, misMatch,
    mBlocks, nBlock, mBndFcBlock, mDim, nDum, mValsRead, mIntFcBlock, intDummy ;
  block_struct *Pbl, *PblLeft, *PblRight ;
  char bndName[33], someLine[MAX_BC_CHAR+1], faceDir[12] ;
  subFace_struct *Psf = NULL, **PPsf ;
  const char upperDir[3][6] = { "EAST", "NORTH", "HIGH" } ;
  const char lowerDir[3][6] = { "WEST", "SOUTH", "LOW" } ;
  float floatDummy ;

  Pmb->mSubFaces = failure = mSubFcUsed = mDim = 0 ;
  Pmb->subFaceS = NULL ;

  if ( verbosity > 2 )
    printf ( "     Reading %s as .phys file.\n", PphysFile ) ;
  
  prepend_path ( PphysFile ) ;
  if ( !( physFile = fopen ( PphysFile, "r" ) ) ) { 
    printf ( " FATAL: failed to open phys file %s.\n", PphysFile ) ;
    goto failure ; }

  /* Skip one text line. */
  fscanf ( physFile, "%*[^\n]" ) ; fscanf ( physFile, "\n" ) ;

  /* Number of blocks. */
  if ( fscanf ( physFile, "%d%*[^\n]", &mBlocks ) != 1 ) {
    printf ( " FATAL: could not read number of blocks from in read_mb_phys.\n" ) ;
    goto failure ; }
  else if ( mBlocks != Pmb->mBlocks ) {
    printf ( " FATAL: wrong number of blocks ( %d rather than %d ) in read_mb_phys.\n",
	     mBlocks, Pmb->mBlocks ) ;
    goto failure ; }
  fscanf ( physFile, "\n" ) ;

  /* Loop over the blocks. */
  for ( nBlock = 1 ; nBlock <= mBlocks ; nBlock++ ) {

    /* Number of boundary subfaces for this block. */
    if ( fscanf ( physFile, "%d%*[^\n]", &mBndFcBlock ) != 1 ) {
      printf ( " FATAL: could not read number of boundary subfaces for block %d.\n",
	       nBlock ) ;
      goto failure ; }
    Pmb->mSubFaces += mBndFcBlock ;
    Pmb->PblockS[nBlock].mSubFaces += mBndFcBlock ;
    fscanf ( physFile, "\n" ) ;
    
    /* Realloc the list of subfaces. */
    if ( !( Pmb->subFaceS = arr_realloc ( "Pmb->subFaceS in read_mb_phys",
                                          pArrFamMb, Pmb->subFaceS,
                                          Pmb->mSubFaces+1, sizeof ( subFace_struct )))){
      printf ( "FATAL: memory reallocation for the block boundary faces failed"
	       " in read_mb_phys.\n" ) ;
      goto failure ; }

    /* Read the block boundary faces. */
    for ( nSubFc = 0 ; nSubFc < mBndFcBlock ; nSubFc++ ) {
      Psf = Pmb->subFaceS + (++mSubFcUsed) ;
      fgets ( someLine, MAX_BC_CHAR, physFile ) ;
      mValsRead = sscanf ( someLine, "%10s %10s %d %d %d %d %d %d", faceDir, bndName, 
			   ijkLo, ijkHi, ijkLo+1, ijkHi+1, ijkLo+2, ijkHi+2 ) ;
      if ( mValsRead == 8 && ( mDim == 0 || mDim == 3 ) ) {
	/* 3D. */
	mDim = 3 ; }
      else if ( mValsRead == 6&& ( mDim == 0 || mDim == 2 ) ) {
	/* 2D. */
	mDim = 2 ; }
      else {
	sprintf ( hip_msg, "not enough info for bnd face %d of block %d.\n",
		 nSubFc+1, nBlock ) ;
	hip_err ( fatal, 0, hip_msg ) ; }

      /* The .phys file lists indices for cells. Hip needs nodes. Increment
	 all upper limits. */
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	if ( ijkLo[nDim] == ijkHi[nDim] ) {
	  /* This might be a static direction. */
	  if ( !strncmp( faceDir, upperDir[nDim], 5 ) ) {
	    /* Static direction, upper limit. Increment both indices. */
	    ijkLo[nDim] = ++ijkHi[nDim] ; }
	  else if ( strncmp ( faceDir, lowerDir[nDim], 5 ) ) {
	    /* This is not the lower end of the static direction, increment the
	       upper limit. */
	    ++ijkHi[nDim] ; }
	}
	else {
	  /* This is a running index. Increment the upper limit. */
	  ++ijkHi[nDim] ; }

      /* Strip all leading whitespace and place a \0 after the first following
	 whitespace. */
      r1_beginstring ( bndName, MAX_BC_CHAR ) ;
      r1_stripsep ( bndName, MAX_BC_CHAR ) ;
      r1_stripquote ( bndName, MAX_BC_CHAR ) ;

      /* Find a matching bc entry or make a new one. */
      Psf->Pbc = find_bc( bndName, 1 ) ;
    
      /* Subface seems fine. List it. */
      Psf->nr = 0 ;
      PblLeft = Pmb->PblockS + nBlock ;
      Psf->PlBlock = PblLeft ;
      Psf->PrBlock = NULL ;

      for ( nDim = 0 ; nDim < Pmb->mDim ; nDim++ ) {
	/* Check whether the subface indices divide by skip. */
	if ( ( ijkHi[nDim] - ijkLo[nDim] ) % skip ) {
	  printf ( " FATAL: subface %d: indices %d-%d dim %d don't divide by %d.\n",
		   nSubFc, ijkLo[nDim],  ijkHi[nDim], nDim, skip ) ;
	  failure = 1 ; }

	/* List it. */
	Psf->llLBlockFile[nDim] = ijkLo[nDim] ;
	Psf->urLBlockFile[nDim] = ijkHi[nDim] ;
	Psf->llLBlock[nDim] = ( ijkLo[nDim]-1 )/skip + 1 ;
	Psf->urLBlock[nDim] = ( ijkHi[nDim]-1 )/skip + 1 ; }
    }
    if ( failure ) goto failure ;
  
    /* Read 24 dummies for each boundary subface. */
    for ( nSubFc = 0 ; nSubFc < mBndFcBlock ; nSubFc++ )
      for ( nDum = 0 ; nDum < 24 ; nDum++ )
	if ( fscanf ( physFile, "%g", &floatDummy ) != 1 ) {
	  printf ( "FATAL: not enough floatDummy variables for bndFc %d in block %d"
		   " in read_mb_phys.\n", nSubFc+1, nBlock ) ;
	  goto failure ; }
  }
  fscanf ( physFile, "%*[\n]" ) ;

  /* Scan for the domain conn section. */
  bndName[0] = '\0' ;
  while ( fgets ( bndName, 32, physFile ) ) {
    if ( !strncmp ( bndName, "DOMAIN CONNECTIVITY INFORMATION", 32 ) )
      break ;
    fscanf ( physFile, "%*[\n]" ) ; }
  if ( feof ( physFile ) ) {
    printf ( " FATAL: no DOMAIN CONNECTIVITY INFORMATION found in"
	     " in read_mb_phys.\n" ) ;
    goto failure ; }

  /* Loop over the blocks to read block interfaces. */
  for ( nBlock = 1 ; nBlock <= mBlocks ; nBlock++ ) {

    /* Number of block interfaces for this block. */
    if ( fscanf ( physFile, "%d%*[^\n]", &mIntFcBlock ) != 1 ) {
      printf ( " FATAL: could not read number of boundary subfaces for block %d.\n",
	       nBlock ) ;
      goto failure ; }
    Pmb->mSubFaces += mIntFcBlock ;
    Pmb->PblockS[nBlock].mSubFaces += mIntFcBlock ;
    fscanf ( physFile, "\n" ) ;
    
    /* Realloc the list of subfaces. */
    if ( !( Pmb->subFaceS = arr_realloc ( "Pmb->subFaceS in read_mb_phys",
                                          pArrFamMb, Pmb->subFaceS,
                                          Pmb->mSubFaces+1, sizeof ( subFace_struct )))){
      printf ( "FATAL: memory reallocation for block interfaces failed"
	       " in read_mb_phys.\n" ) ;
      goto failure ; }

    /* Read the block interfaces. */
    for ( nSubFc = 0 ; nSubFc < mIntFcBlock ; nSubFc++ ) { 
      if ( fscanf ( physFile, "%d %d %d %d %d %d %d %d%*[^\n]",
		    ijkLo, ijkHi, ijkLo+1, ijkHi+1, ijkLo+2, ijkHi+2,
		    &intDummy, &nrSubFc ) != 8 ) {
	printf ( " FATAL: could not read internal blockface %d in read_mb_phys\n",
		 nSubFc ) ;
	goto failure ; }

      /* Find a matching subface. */
      for ( iSubFc = 1 ; iSubFc <= mSubFcUsed ; iSubFc++ ) {
	Psf = Pmb->subFaceS + iSubFc ;
	if ( Psf->nr == nrSubFc )
	  break ; }
      
      if ( iSubFc > mSubFcUsed ) {
	/* New subface. */
	Psf = Pmb->subFaceS + (++mSubFcUsed) ;
	PblLeft = Pmb->PblockS + nBlock ;
	Psf->PlBlock = PblLeft ;
	Psf->nr = nrSubFc ;
	Psf->Pbc = NULL ;
	
	for ( nDim = 0 ; nDim < Pmb->mDim ; nDim++ ) {
	  /* Check whether the subface indices divide by skip. */
	  if ( ( ijkHi[nDim] - ijkLo[nDim] ) % skip ) {
	    printf ( " FATAL: subface %d: indices %d-%d dim %d don't divide by %d.\n",
		     nSubFc, ijkLo[nDim],  ijkHi[nDim], nDim, skip ) ;
	    failure = 1 ; }
	  
	  /* List it. */
	  Psf->llLBlockFile[nDim] = ijkLo[nDim] ;
	  Psf->urLBlockFile[nDim] = ijkHi[nDim] ;
	  Psf->llLBlock[nDim] = ( ijkLo[nDim]-1 )/skip + 1 ;
	  Psf->urLBlock[nDim] = ( ijkHi[nDim]-1 )/skip + 1 ; }
      } 
      else {
	/* Old Subface. Find the already listed part. */
	PblRight = Pmb->PblockS + nBlock ;
	Psf->PrBlock = PblRight ;
	
	/* Fill the RHS of the subface. */
	for ( nDim = 0 ; nDim < Pmb->mDim ; nDim++ ) {
	  Psf->llRBlockFile[nDim] = ijkLo[nDim] ;
	  Psf->urRBlockFile[nDim] = ijkHi[nDim] ;
	  Psf->llRBlock[nDim] = ( ijkLo[nDim]-1 )/skip + 1 ;
	  Psf->urRBlock[nDim] = ( ijkHi[nDim]-1 )/skip + 1 ; }
	
	/* Find the translation operation between the two sides. */
	if ( !find_rot_ijk ( Psf, Pmb->mDim ) ) {
	  printf ( " FATAL: could not match the subface named %d.\n", nrSubFc ) ;
	  failure = 1 ; }
	else {
	  get_mb_elemShift ( Psf, Pmb->mDim ) ;
	  get_mb_vertShift ( Psf, Pmb->mDim ) ; }
      }
    }
  }

  /* Find out whether some faces have a mismatch. */
  for ( misMatch = 0, nSubFc = 1 ; nSubFc <= mSubFcUsed ; nSubFc++ ) {
    Psf = Pmb->subFaceS + nSubFc ;
    if ( !Psf->PrBlock && !Psf->Pbc ) {
      printf ( " FATAL: unmatched block interface labeled %d from block %d.\n",
	       Psf->nr, Psf->PlBlock->nr ) ;
      misMatch++ ; }
  }
  if ( misMatch ) {
    printf ( " FATAL: found %d mismatched block interfaces in read_mb_phys.\n",
	     misMatch ) ;
    goto failure ; }

  
  /* Final realloc. */
  Pmb->mSubFaces = mSubFcUsed ;
  if ( !( Pmb->subFaceS = arr_realloc ( "Pmb->subFaceS in read_mb_phys", pArrFamMb, Pmb->subFaceS,
                                        Pmb->mSubFaces+1, sizeof ( subFace_struct )))){
    printf ( "FATAL: memory reallocation for the subfaces failed"
	     " in read_mb_phys.\n" ) ;
    goto failure ; }
  

  /* Increment the list of base pointers. */
  for ( Pbl = Pmb->PblockS+1 ; Pbl <= Pmb->PblockS + Pmb->mBlocks ; Pbl++ ) {
    /* Malloc a list of subface pointers for each block. */
    if ( !( Pbl->PPsubFaces = 
              arr_malloc ( " Pbl->PPsubFaces in read_mb_phys", pArrFamMb,
                            Pbl->mSubFaces, sizeof( subFace_struct*))) )
      hip_err ( fatal, 0, "malloc for the subface pointers failed"
                " in read_mb_phys.\n" ) ;

    /* Reset. */
    for ( PPsf = Pbl->PPsubFaces ; PPsf < Pbl->PPsubFaces + Pbl->mSubFaces ; 
          PPsf++ )
      *PPsf = NULL ;
  }

  /* Fill the list of subface pointers with each block. */
  for ( Psf = Pmb->subFaceS + 1 ; 
        Psf <= Pmb->subFaceS + Pmb->mSubFaces ; Psf++ ) {
    if ( !put_mb_subFc ( Psf->PlBlock, Psf ) ) {
      sprintf ( hip_msg, "could not add subface %d to left block %d"
                " in read_mb_phys.\n", Psf->nr, Psf->PlBlock->nr ) ;
      hip_err( fatal, 0, hip_msg ) ;
    }
    if ( Psf->PrBlock && !put_mb_subFc ( Psf->PrBlock, Psf ) ) {
      sprintf ( hip_msg, "could not add subface %d to right block %d"
                " in read_mb_phys.\n", Psf->nr, Psf->PlBlock->nr ) ;
      hip_err ( fatal, 0, hip_msg ) ; }
  }


  /* Reset all k-indices for the subfaces to 1 in 2D. */
  if ( Pmb->mDim == 2 )
    for ( Psf = Pmb->subFaceS + 1 ; 
          Psf <= Pmb->subFaceS + Pmb->mSubFaces ; Psf++ ) {
      Psf->llLBlockFile[2] = Psf->urLBlockFile[2] = 1 ;
      Psf->llLBlock[2] = Psf->urLBlock[2] = 1 ;
    }
  

  /* Seems fine. */
  fclose ( physFile ) ;
  return ( 1 ) ;

  failure:
  free_mb ( &Pmb ) ;
  fclose ( physFile ) ;
  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  p3d_next_ascii_dbl:
*/
/*! Pull the next float/double from the plot3d file, handle multipliers.
 *
 * Needs to treat commata, spaces, line feeds, but also multipilers such as 24*0.0
 *
 */

/*
  
  Last update:
  ------------
  19Feb18: conceived.
  

  Input:
  ------
  FlCoor: positioned coordinate file. 

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on success, 0 on failure
  
*/

int p3d_next_ascii_dbl ( FILE *FlCoor, double *pVal ) {

  static int mult ;
  static double dVal ;
  int fileStatus ;

  char valStr[LINE_LEN], *pStar ;

  if ( mult ) {
    /* Multiplicity not yet exhaused. */
    mult-- ;
    *pVal = dVal ;
    fileStatus = 1 ;
  }
  else {
    /* Read the separated value into a string, parse. */
    fscanf ( FlCoor, "%*[, \t\n\f\r\v]") ;
    fileStatus = fscanf ( FlCoor, "%99[^, \t\n\f\r\v\n]", valStr) ;

    if ( fileStatus ) {
      if ( ( pStar = strchr ( valStr, '*' ) ) ) {
        /* Multiplicity. */
        fileStatus = sscanf ( valStr, "%d", &mult ) ;
        if (fileStatus )
          fileStatus = sscanf ( pStar+1, "%lf", &dVal ) ;
        if ( fileStatus ) {
          mult-- ;
          *pVal = dVal ;
        }
      }
      else {
        /* Just read the value. */
	fileStatus = sscanf ( valStr, "%lf", pVal ) ;
      }
    }
  }
  
  return ( fileStatus ) ;
}


/******************************************************************************

  Read_mb_plot3d:
  .
  
  Last update:
  ------------
  19Feb18; treat comma-separated files.
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_mb_plot3d ( mb_struct *Pmb, char *PcoorFile, const char coorType,
		     const int skip, const int withBlanking ) {
  
  static FILE *coorFile ;
  static int fileStatus, recLen, nDim, mDim, mVertsFile, mCoor, mBlocks, nVert,
             ijk[MAX_DIM], failure ;
  static block_struct *Pbl, *blockS ;
  static double *Pcoor, someDbl, *PcoorTmp ;

  failure = 0 ;
  
  prepend_path ( PcoorFile ) ;
  if ( !( coorFile = fopen ( PcoorFile, "r" ) ) ) {
    sprintf ( hip_msg, "failed to open plot3d coor file named %s.\n", PcoorFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
    


  if ( coorType == 'b' ) {
    if ( verbosity > 2 )
      printf ( "       Reading binary plot3d file %s.\n", PcoorFile ) ;

    fileStatus = FREAD ( &recLen, sizeof( int ), 1, coorFile ) ;
    if ( fileStatus == 1 ) {
      if ( recLen !=4 ) { 
        sprintf ( hip_msg, "wrong rec length with numbers of blocks in plot3d.\n" ) ;
        hip_err ( fatal, 0, hip_msg ) ; }
      fileStatus = FREAD ( &mBlocks, sizeof( int ), 1, coorFile ) ;
      FREAD ( &recLen, sizeof( int ), 1, coorFile ) ; } }
  else {
    if ( verbosity > 2 )
      printf ( "       Reading ascii plot3d file %s.\n", PcoorFile ) ;

    fileStatus = fscanf ( coorFile, "%d", &mBlocks ) ; }
  if ( fileStatus != 1 ) {
    sprintf ( hip_msg, "could not read number of blocks from plot3d.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  Pmb->mBlocks = mBlocks ;




  /* Allocate for mBlocks. */
  if ( !( Pmb->PblockS = blockS = make_blocks ( mBlocks+1 ) ) ) {
    hip_err ( fatal, 0, "memory allocation for the blocks failed\n" ) ;
  }
  


  /* Read the block dimensions. */
  if ( coorType == 'b' ) {
    fileStatus = FREAD ( &recLen, sizeof( int ), 1, coorFile ) ;
    if ( fileStatus == 1 ) {
      if ( recLen != mBlocks*3*sizeof( int ) ) {
	hip_err ( fatal, 0, "wrong rec length with block sizes in plot3d.\n" ) ; ; }
      else
	for ( Pbl = blockS+1 ; Pbl <= blockS + mBlocks && fileStatus == 1 ; Pbl++ )
	  for ( nDim = 0 ; nDim < 3 && fileStatus ; nDim++ )
	    fileStatus = FREAD ( Pbl->mVertFile+nDim, sizeof( int ), 1, coorFile ) ;
      fileStatus = FREAD ( &recLen, sizeof( int ), 1, coorFile ) ; } }
  else
    for ( Pbl = blockS+1 ; Pbl <= blockS + mBlocks && fileStatus == 1 ; Pbl++ )
      for ( nDim = 0 ; nDim < 3 && fileStatus ; nDim++ ) {
	fileStatus = fscanf ( coorFile, "%d", Pbl->mVertFile+nDim ) ;
        fscanf ( coorFile, "," ) ;
      }
  if ( fileStatus != 1 ) {
    sprintf ( hip_msg, "could not read sizes of blocks from plot3d.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }



  /* Check if 2-D by looking at the 3rd dimension. */
  mDim = Pmb->mDim = ( blockS[1].mVertFile[2] == 1 ? 2 : 3 ) ;
  for ( Pbl = blockS+1 ; Pbl <= blockS + mBlocks ; Pbl++ ) {
    Pbl->nr = Pbl - blockS ;
    Pbl->skip = skip ;
    Pbl->mSubFaces = 0 ;
    Pbl->PPsubFaces = NULL ;
    Pbl->PmbRoot = Pmb ;

    /* Check if skipping divides properly. */
    Pbl->mVertsBlock = 1 ;
    for ( nDim = 0 ; nDim < 3 && fileStatus ; nDim++ ) {
      if ( ( Pbl->mVertFile[nDim]-1 ) % skip ) {
	sprintf ( hip_msg, "block %td  (%d*%d*%d) doesn't divide by %d without remainder.",
                  Pbl-blockS, Pbl->mVertFile[0], Pbl->mVertFile[1], Pbl->mVertFile[2],
                  skip ) ;
        hip_err ( fatal, 0, hip_msg ) ;
	failure = 1 ; }
      else
	Pbl->mVert[nDim] = ( Pbl->mVertFile[nDim]-1 )/skip + 1 ;
      Pbl->mVertsBlock *= Pbl->mVert[nDim] ; } }

  if ( failure ) hip_err ( fatal, 0, hip_msg ) ;
      


  /* Read the coordinates. */
  for ( Pbl = blockS+1 ; Pbl <= blockS + mBlocks && fileStatus == 1 ; Pbl++ ) {

    /* Malloc the actual coordinates needed after skipping. */
    if ( !( Pbl->Pcoor = arr_malloc ( "Pbl->Pcoor in read_mb_plot3d",pArrFamMb,
                                      mDim*( Pbl->mVertsBlock+1 ), sizeof( double ) ))){
      sprintf ( hip_msg, "memory allocation for %d coordinates failed.\n",
	       mDim*( Pbl->mVertsBlock+3 ) ) ;
      hip_err ( fatal, 0, hip_msg ) ; }

    /* Number of vertices for this block in the file. */
    for ( nDim = 0, mVertsFile = 1 ; nDim < mDim ; nDim++ )
      mVertsFile *= Pbl->mVertFile[nDim] ;
    mCoor = mVertsFile*mDim ;

    if ( skip != 1 ) {
      /* Malloc a temporary coordinate space for all coordinates. */
      if ( !( PcoorTmp = arr_malloc ( "PcoorTmp in read_mb_plot3d",pArrFamMb,
                                      mCoor+mDim, sizeof( double ) ) ) ) {
	sprintf ( hip_msg, "temporary memory allocation for %d coordinates failed.\n",
		 mCoor ) ;
	hip_err ( fatal, 0, hip_msg ) ; } }
    else
      PcoorTmp = Pbl->Pcoor ;
      
    if ( coorType == 'b' ) {
      fileStatus = FREAD ( &recLen, sizeof( int ), 1, coorFile ) ;
      if ( fileStatus == 1 ) {
	if ( recLen == 3*mVertsFile*sizeof( double ) + mVertsFile*sizeof(int) ) {
	  /* Double precision. */
	  if ( verbosity > 4 )
	    printf ( "       Reading coor. for %d vert. for block %td in dbl binary.\n",
		     mVertsFile, Pbl-blockS ) ; 
	  for ( nDim = 0 ; nDim < mDim && fileStatus ; nDim++ )
	    for ( Pcoor = PcoorTmp+mDim+nDim ;
		  Pcoor <= PcoorTmp+mCoor+nDim && fileStatus ; Pcoor += mDim )
	      fileStatus = FREAD ( Pcoor, sizeof( double ), 1, coorFile ) ;
	  if ( mDim == 2 )
	    /* Read a bogus third dimension into oblivion. */
	    fseek ( coorFile, mVertsFile*sizeof( double ), SEEK_CUR ) ;
	}
	else if (  recLen == 3*mVertsFile*sizeof( float ) + mVertsFile*sizeof(int) ) {
	  /* Single precision. */
	    printf ( "       Reading coor. for %d vert. for block %td in sgl binary.\n",
		     mVertsFile, Pbl-blockS ) ; 
	  for ( nDim = 0 ; nDim < mDim && fileStatus ; nDim++ )
	    for ( Pcoor = PcoorTmp+mDim+nDim ;
		  Pcoor <= PcoorTmp+mCoor+nDim && fileStatus ; Pcoor += mDim )
	      fileStatus = FREAD ( Pcoor, sizeof( float ), 1, coorFile ) ;
	  if ( mDim == 2 )
	    /* Read a bogus third dimension into oblivion. */
	    fseek ( coorFile, mVertsFile*sizeof( float ), SEEK_CUR ) ;
	}
	else {
	  sprintf ( hip_msg, "wrong record length for coordinates of block %d.\n",
		   Pbl->nr ) ;
	  hip_err ( fatal, 0, hip_msg ) ; }
	/* Skip the blanking info. */
	fseek ( coorFile, mVertsFile*sizeof(int), SEEK_CUR ) ;
	fileStatus = FREAD ( &recLen, sizeof( int ), 1, coorFile ) ; }
    }
    
    else {
      /* ascii */
      printf ( "       Reading coor. for %d vertices for block %td in ascii.\n",
	       mVertsFile, Pbl-blockS ) ;
      
      for ( nDim = 0 ; nDim < mDim && fileStatus ; nDim++ )
	for ( Pcoor = PcoorTmp+mDim+nDim ;
	      Pcoor <= PcoorTmp+mCoor+nDim && fileStatus ; Pcoor += mDim )
	  fileStatus = p3d_next_ascii_dbl ( coorFile, Pcoor ) ;

      if ( fileStatus && mDim == 2 ) {
	/* Read a bogus third dimension into oblivion. */
	for ( nVert = 0 ; nVert < mVertsFile ; nVert++ )
	  fileStatus =  p3d_next_ascii_dbl ( coorFile, &someDbl ) ;
      }
      
      /* Skip the blanking info. */
      for ( nVert = 0 ; withBlanking && nVert < mVertsFile && fileStatus ; nVert++ ) {
	fileStatus = fscanf ( coorFile, "%d", &recLen ) ;
        fscanf ( coorFile, "," ) ;
      }
    }
    
    if ( fileStatus != 1 ) {
      sprintf ( hip_msg, "could not read coordinates for block %d"
                " from plot3d.\n", Pbl->nr ) ;
      hip_err ( fatal, 0, hip_msg ) ; }

    if ( skip != 1 ) {
      /* Skip all unused vertices. */
      Pcoor = Pbl->Pcoor + mDim ;
      for ( ijk[2] = 1 ; ijk[2] <= Pbl->mVertFile[2] ; ijk[2] += skip )
	for ( ijk[1] = 1 ; ijk[1] <= Pbl->mVertFile[1] ; ijk[1] += skip )
	  for ( ijk[0] = 1 ; ijk[0] <= Pbl->mVertFile[0] ; ijk[0] += skip ) {
	    nVert = get_nVert_ijk ( mDim, ijk, Pbl->mVertFile ) ;
	    for ( nDim = 0 ; nDim < mDim ; nDim++ )
	      *Pcoor++ = PcoorTmp [ mDim*nVert + nDim ] ;
	  }
      arr_free ( PcoorTmp ) ;
    }
  }
  
  fclose ( coorFile ) ;
  return ( 1 ) ;
}


/******************************************************************************

  read_mb_cfdrc:
  Collector for the plot3d + ibc-file format mb grid functions.
  
  Last update:
  ------------
  25Feb18; initialise Grids.epsOverlap.
  6Jan15; use hip_err. 
  6Jan15; rename pFamMb to more descriptive pArrFamMb
  30Oct00; initialise PvarTypeS.
  16jul96: conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int read_mb_cfdrc ( char *PibcFile, char *PcoorFile, char coorType,
                    const int skip, const int withBlanking  ) {
  
  FILE *ibcFile ;
  grid_struct *Pgrid ;
  mb_struct *Pmb ;
  char someLine[MAX_BC_CHAR+1] ;

  hip_err ( info, 1, "\n Reading structured CFDRC file." ) ;
 
   /* Allocate an instance of a multiblock root. */
  Pmb = arr_calloc ( "Pmb in read_mb_cfdrc",
                     pArrFamMb, 1, sizeof ( mb_struct ) ) ;

  /* Read the plot3d file. */
  if ( !read_mb_plot3d ( Pmb, PcoorFile, coorType, skip, withBlanking ) )
    hip_err ( fatal, 0, "failed to read plot3d file in read_mb_cfdrc.\n" ) ;

  /* Find out whether PibcFile is out of CFD-GEOM or ICEM. */
  prepend_path ( PibcFile ) ;
  if ( !( ibcFile = fopen ( PibcFile, "r" ) ) ) { 
    sprintf ( hip_msg, "failed to open ibc file %s.", PibcFile ) ;
    hip_err ( fatal, 0, hip_msg ) ; 
  }
  fgets ( someLine, MAX_BC_CHAR, ibcFile ) ;
  fclose ( ibcFile ) ;
  if ( !strncmp ( someLine, "! Version 3", 11 ) ) {
    /* Read the ibc file. */
    if ( !read_mb_ibc ( Pmb, PibcFile, skip ) )
      hip_err ( fatal, 0, "failed to read ibc file in read_mb_cfdrc." ) ;
  }
  else if ( !strncmp ( someLine, "BOUNDARY CONDITION INFORMATION", 30 ) ) {
    /* Read the phys file. */
    if ( !read_mb_phys ( Pmb, PibcFile, skip ) )
      hip_err ( fatal, 0, "failed to read ibc file in read_mb_cfdrc." ) ;
  }
  else
    hip_err ( fatal, 0, "could not identify type of ibc/phys file"
              " in read_mb_cfdrc.\n" ) ;
    

  /* Count the number of elements and vertices in the entire block. */
  mb_count ( Pmb ) ;
  mb_degen_subfc ( Pmb ) ;
  mb_size ( Pmb ) ;
  mb_bb ( Pmb ) ;

  /* Link bcs, get the block and bc bounding boxes. */
  mb_bcSubFc ( Pmb->PblockS, Pmb->mBlocks ) ;
  mb_bcBox ( Pmb->mDim ) ;

  /* Done. */
  printf ( "\n" ) ;

  /* No unknowns. */
  Pmb->varList.varType = noVar ;
  Pmb->varList.mUnknowns = Pmb->varList.mUnknFlow = 0 ;
  
  /* Make a new grid. */
  Pgrid = make_grid () ;
  /* Make this grid the current one. */
  Grids.PcurrentGrid = Pgrid ;

  /* initialise epsOverlap based on the non-degenerate minimal edge length. */
  int nBlock ;
  double hMinMbSq ;
  if ( Grids.epsOverlap == DEFAULT_epsOverlap ) {
    /* Not changed yet. Adjust with actual value. */

    hMinMbSq = get_mb_hMinSq ( Pmb ) ;
    Grids.epsOverlapSq = 0.9*0.9*hMinMbSq ;
    Grids.epsOverlap = sqrt( Grids.epsOverlapSq ) ;
  }
  
  Pgrid->mb.type = mb ;
  Pgrid->mb.Pmb = Pmb ;
  Pgrid->mb.mDim = Pmb->mDim ;
  Pgrid->uns.pVarList = &(Pmb->varList) ;

  return ( 1 ) ;
}
