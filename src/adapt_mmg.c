#include "mmg/libmmg.h"

// defined in host/PCLINUX/makefile.h
#ifdef MMG_5p6p3
  // MMG 5.3.1
  #define MMG5_int int
  #define MMG5_INT_SZ 32
  #define MMG5_MAX_MVERTS 200e6
#else
  // MMG 5.7.1
  #define MMG5_INT_SZ 64
  #define MMG5_MAX_MVERTS 200e9
#endif

#include <time.h>

#include "cpre.h"
#include "proto.h"
#include "cpre_uns.h"
#include "proto_uns.h"
#include "cpre_adapt.h"
#include "proto_adapt.h"

// For debug, call _iso directly.
int MMG3D_doSol_iso(MMG5_pMesh mesh,MMG5_pSol met) ;
int MMG2D_doSol_iso(MMG5_pMesh mesh,MMG5_pSol met) ;


#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

extern const int verbosity ;
extern char hip_msg[] ;
extern int check_lvl ;

extern const elemType_struct elemType[] ;
extern Grids_struct Grids ;
extern const doFc_struct doWarn, doRemove ;


typedef enum {
  noMmgMeth, // non assigned.
  isoFac, // isotropic with a constant factor
  isoVar,  // isotropic from a variable.
  isoMap  // replace refinement sensor with a scale ratio.
} mmgMethod_e ;

const int kFcMMg2hip[] = {1,2,4,3} ; // same permuation as the vertices.
/* Read all faces. A bc no of mBc+1 means an internal face created
   at submission to mmg3d to fix faces with non-simplex elements.
   make_uns_bndPatch will order faces and create the list of bc
   in the new grid. */

extern int DEFAULT_mmg_mPerLayer;


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_write_mesh3d:
  mmg_write_mesh2d:
*/
/*! write the mmg mesh to medit file before translating to hip.
 */

/*

  Last update:
  ------------
  4Mar20; bugfix and cleaning of 2d Medit output.
  5Jul18; tidy up interface, build sol and mesh file names inside.
  14Dec17: conceived.


  Input:
  ------
  pMMesh: mmg mesh
  pMMet: mmg metric
  fileNameRoot: root of th file name to write to, path will be prepended

*/

void mmg_write_mesh3d ( MMG5_pMesh pMMesh, MMG5_pSol pMMet,
                       char fileNameRoot[LINE_LEN] ) {

  MMG3D_Set_outputSolName ( pMMesh, pMMet, fileNameRoot ) ;

  char flNm[LINE_LEN] ;
  snprintf ( flNm, LINE_LEN-1, "%s.mesh", fileNameRoot ) ;
  prepend_path ( flNm ) ;
  MMG3D_saveMesh ( pMMesh, flNm ) ;

  snprintf ( flNm, LINE_LEN-1, "%s.sol", fileNameRoot ) ;
  prepend_path ( flNm ) ;
  MMG3D_saveSol ( pMMesh, pMMet, flNm ) ;

  return ;
}

void mmg_write_mesh2d ( MMG5_pMesh pMMesh, MMG5_pSol pMMet,
                       char fileNameRoot[LINE_LEN] ) {

  MMG2D_Set_outputSolName ( pMMesh, pMMet, fileNameRoot ) ;

  char flNm[LINE_LEN] ;
  snprintf ( flNm, LINE_LEN-1, "%s.mesh", fileNameRoot ) ;
  prepend_path ( flNm ) ;
  MMG2D_saveMesh ( pMMesh, flNm ) ;

  snprintf ( flNm, LINE_LEN-1, "%s.sol", fileNameRoot ) ;
  prepend_path ( flNm ) ;
  MMG2D_saveSol ( pMMesh, pMMet, flNm ) ;

  return ;
}



/******************************************************************************
  mmg_check_metric:   */

/*! diagnostic for the metric.
 *
 */

/*

  last update:
  ------------
  dec15: conceived by gs.


  input:
  ------
  mesh: mmg mesh
  met:  mmg metric

*/

void mmg_check_metric ( MMG5_pMesh mesh, MMG5_pSol met ) {
  double min, max, sum, avg ;

  min = TOO_MUCH;
  max = -min;
  sum = 0. ;

  int k;
  for( k=1; k<=mesh->np ; k++ ) {
    max = MAX( max, met->m[k] ) ;
    min = MIN( min, met->m[k] ) ;
    sum += met->m[k];
  }

  avg = sum/mesh->np;
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "metric info: max = %lf \t min = %lf \t avg = %lf \n\n",
              pow(min,-0.5), pow(max,-0.5), pow( avg,-0.5));
    hip_err ( info, 1, hip_msg ) ;
  }
}


/******************************************************************************
  mmg_metric_from_const:   */

/*! Scale the mesh width with a constant isotropic factor.
 *
 */

/*

  Last update:
  ------------
  17Feb17; intro hMin, hMax.
  15Sep16; include 'from' in the name.
  Dec15: conceived.


  Input:
  ------
  mesh: mmg mesh
  isoFactor: the scaling factor

  Changes To:
  -----------
  met: the metric

*/

void mmg_metric_from_const ( MMG5_pMesh mesh, MMG5_pSol met,
                             const double isofactor,
                             const double hMin, const double hMax ) {
  int k;
  for (k=1; k<=mesh->np; k++) {
    met->m[k] = isofactor * met->m[k] ;
    if ( hMin != -TOO_MUCH )
      /* user-defined threshold, use it. */
      met->m[k] = MAX( met->m[k], hMin ) ;
    if ( hMax != TOO_MUCH )
      /* user-defined threshold, use it. */
      met->m[k] = MIN( met->m[k], hMax ) ;
  }

  mmg_check_metric ( mesh, met ) ;

  return ;
}




/******************************************************************************
  mmg_metric_from_var:   */

/*! Use a variable as the multiplier for the metric.
 *
 */

/*

  Last update:
  ------------
  11Jul19; rename nVar to kVarFactor, to clearly state that 0 <= kVarFactor < mUnknowns,
           and the var is a scaling factor, not the target edge length.
           include isoFactor in arg list, apply global scaling.
  17Feb17; intro hMin, hMax.
  15Dec16; fix bug with scaling metric also for non-tet vertices that are not known by mmg3d.
           add mTetVx to args.
  15Sep16; include 'from' in the name.
  6Mar16: conceived


  Input:
  ------
  mesh: mmg mesh
  kVarFactor: the index of the variable used as the scaling, 0 <= kVarFactor < mUnknowns

  Changes To:
  -----------
  met: the mmg metric

*/

int mmg_metric_from_var ( uns_s *pUns, ulong_t mTetVx, MMG5_pMesh pMMesh, MMG5_pSol pMMet,
                          const int kVarFactor, const double isoFactor, mmgMethod_e mmgMethod,
                          const double hMin, const double hMax ) {

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "refining isotropically with variable %s as scale.",
              pUns->varList.var[kVarFactor].name ) ;
    hip_err ( info, 3, hip_msg ) ;
  }

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double val ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number <= mTetVx ) {
        val = isoFactor*pVrtx->Punknown[kVarFactor] ;
        if ( val < 0. ) {
          sprintf ( hip_msg, "can't scale with value %g of node %"FMT_ULG".",
                    val, pVrtx->number ) ;
          hip_err ( warning, 1, hip_msg ) ;
          return ( 0 ) ;
        }
        pMMet->m[ pVrtx->number ] *= val ;
        if ( hMin != -TOO_MUCH )
          /* user-defined threshold, use it. */
           pMMet->m[ pVrtx->number ] = MAX( pMMet->m[ pVrtx->number ], hMin ) ;
        if ( hMax != TOO_MUCH )
          /* user-defined threshold, use it. */
          pMMet->m[ pVrtx->number ] = MIN( pMMet->m[ pVrtx->number ], hMax ) ;


        if ( mmgMethod == isoMap ) {
          pVrtx->Punknown[kVarFactor] = pMMet->m[ pVrtx->number ] ;
        }
      }

  mmg_check_metric ( pMMesh, pMMet ) ;
  return ( 1 ) ;
}


/******************************************************************************
  mmg_egLen_from_var:   */

/*! Supply a metric to mmg from edge lengths stored as a variable.
 *
 */

/*

  Last update:
  ------------
  13jul19l; remove kvarFactor and isoFactor args, scaling has been done
            rename kVarLen to kVarTrgtLen to make purpose clear.
  11Jul19; use isoFactor.
  22Feb19; derived from mmg_metric_from_var

  Input:
  ------
  pUns
  pMMesh: mmg mesh
  pMMet: mmg metric
  kVarTrgtLen: the index of the variable with the target eg len, 0< nVarLen < mUnknowns
  hMin/Max: edge length limits.

  Changes To:
  -----------
  met: the mmg metric

  Returns:
  -------
  the number of nodes set.

*/

int mmg_egLen_from_var ( uns_s *pUns, MMG5_pMesh pMMesh, MMG5_pSol pMMet,
                         const int kVarTrgtLen,
                         const double hMin, const double hMax ) {

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "refining isotropically with variable %s as target eg len.",
              pUns->varList.var[kVarTrgtLen].name ) ;
    hip_err ( info, 3, hip_msg ) ;
  }

  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVrtx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  double scal, len ;
  int mVxSet = 0 ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVrtx = pVxBeg ; pVrtx <= pVxEnd ; pVrtx++ )
      if ( pVrtx->number > 0 && pVrtx->number <= pMMesh->np ) {
        mVxSet++ ;
        len = pVrtx->Punknown[kVarTrgtLen] ;

        if ( hMin != -TOO_MUCH )
          /* user-defined threshold, use it. */
          len = MAX( len, hMin ) ;
        if ( hMax != TOO_MUCH )
          /* user-defined threshold, use it. */
          len = MIN( len, hMax ) ;

        pMMet->m[ pVrtx->number ] = len ;
      }

  mmg_check_metric ( pMMesh, pMMet ) ;
  return ( 1 ) ;
}


/******************************************************************************
  mmg_get_sizes:   */

/*! Obtain sizes of an mmg3d mesh.
 *
 */

/*

  Last update:
  ------------
  19Apri16; Added 2D options
  5Mar16; tidy up.
  Dec15: conceived by GS.


  Input:
  ------
  mesh: mmg3d mesh

  Changes To:
  -----------

  Output:
  -------
  *pmDim: spatial dimension
  *pmEl: number of Elements
  *pmConn: number of connectivity entries
  *pmVx: number of vertices.
  *pmBc: number of bcs.

*/

void mmg_get_sizes ( MMG5_pMesh mesh,
                     int *pmDim, ulong_t *pmEl, ulong_t *pmConn,
                     ulong_t *pmVx, ulong_t *pmBndFc, int *pmBc ) {

  ulong_t uBuf[6], i ;
  elType_e elType ;


  // Use number of elements in mesh as 3D/2D discriminator
  if ( mesh->ne != 0 ) {
    *pmDim = 3 ;
  }
  else {
    *pmDim = 2 ;
  }

  // Coordinates
  *pmVx =  mesh->np ;

  if ( mesh->ne != 0 ) {
    // Connectivity, tet only
    *pmEl = mesh->ne ;
    *pmConn = 4*(*pmEl) ;
  }
  else{
    // Connectivity, tri only
    *pmEl = mesh->nt ;
    *pmConn = 3*(*pmEl) ;
  }

  /* Number of bnd faces. */
  if ( mesh->ne != 0 ) {
    *pmBndFc = mesh->nt ;
  }
  else {
    *pmBndFc = mesh->na ;
  }

  /* Number of Boundaries. Take the highest index as number of bcs*/
  int mTwoFcTria=0, kFace[2] ;
  MMG5_int k, nTet[2] ;
  if ( mesh->nt != 0 ) {
    MMG5_pTria triMmg;
    *pmBc = 0 ;
    for ( k = 1 ; k <= mesh->nt ; k++ ) {
      triMmg = &mesh->tria[k];
      *pmBc = MAX( *pmBc, triMmg->ref ) ;
      // count bc in mmg_get_bnd_per
      MMG3D_Get_tetFromTria( mesh, k, nTet, kFace ) ;
      if ( nTet[1] ) {// two-sided internal tria, produce two faces.
      //  (*pmBndFc)++ ;
        mTwoFcTria++ ;
      }
    }
  }
  else {
    MMG5_pEdge EdgeMmg ;
    int k;
    *pmBc = 0 ;
    for ( k = 1 ; k <= mesh->na ; k++ ) {
      EdgeMmg = &mesh->edge[k];
      *pmBc = MAX( *pmBc, EdgeMmg->ref ) ;
    }
  }

  sprintf ( hip_msg, "found %d two-faced/internal triangles.", mTwoFcTria ) ;
  hip_err ( info, 2, hip_msg ) ;

  return ;
}

/******************************************************************************
  mmg_get_coor:   */

/*! extract coordinates from a mmg3d mesh.
 */

/*

  Last update:
  ------------
  19Apri16; Added 2D options
  5Mar16; tidied up.
  Dec15: conceived by GS.


  Input:
  ------
  mesh: mmg3d mesh

  Changes To:
  -----------
  pChunk:

  Output:
  -------

  Returns:
  --------
  number of coordinate vectors read.

*/

int mmg_get_coor ( MMG5_pMesh mesh, chunk_struct *pChunk ) {

  ulong_t k ;
  MMG5_pPoint	Vertex;
  vrtx_struct *pVx ;

  /* Coordinates: mDim, mVx. */
  for ( k = 1 ; k <= mesh->np ; k++ ) {
    Vertex = &mesh->point[k];
    pVx = pChunk->Pvrtx + k ;
    pVx->number = k ;
    pVx->Pcoor[0] = Vertex->c[0];
    pVx->Pcoor[1] = Vertex->c[1];
    // use element number as discriminator for 3D meshed
    if ( mesh->ne != 0 ) {
      pVx->Pcoor[2] = Vertex->c[2];
    }
  }

  return ( mesh->np ) ;
}


/******************************************************************************
  mmg_get_conn_mark:   */

/*! extract element connectivity from a mmg3d mesh, 
 *  interpret ref as elem mark
 */

/*

  Last update:
  ------------
  25sep23; derived from mmg_get_conn for zones. Set region via mark.


  Input:
  ------
  mesh: mmg3d mesh

  Changes To:
  -----------
  pChunk:

  Returns:
  --------
  number of connectivity entries transferred.

*/

ulong_t mmg_get_conn_mark ( MMG5_pMesh mesh, chunk_struct *pChunk ) {

  int nEl, kVx ;
  elem_struct *pEl = pChunk->Pelem ;
  vrtx_struct **ppVx = pChunk->PPvrtx ; // starts at zero.
  vrtx_struct * const pVxBase = pChunk->Pvrtx ;

  // use element number as discriminator for 3D meshed
  MMG5_int ref, nVx[4] ; // Tets only here, mmg only uses int.
  int req ;
  if ( mesh->ne != 0 ) {
     MMG5_pTetra tetMmg ;
     const int mVxEl = 4 ; // tets only.
     for ( nEl = 1 ; nEl <= mesh->ne ; nEl++ ) {
       pEl++ ;
       // Not really a good idea to poke in mmg's internals to get this data.
       /* tetMmg = mesh->tetra + nEl ; */
       /* nVx[0] =  tetMmg->v[0] ; */
       /* nVx[1] =  tetMmg->v[1] ; */
       /* nVx[2] =  tetMmg->v[3] ; */
       /* nVx[3] =  tetMmg->v[2] ; */
       // Use the API call, but how is the iterator set/reset to the first
       // tet? Example in libexamples/mmg3d/adaptation_example0/example0_b
       // doesn't do this.
       // But pay attention to different node ordering: 0,1,3,2.
       MMG3D_Get_tetrahedron(mesh, nVx,nVx+1,nVx+3,nVx+2, &ref,&req) ;
       init_elem ( pEl, tet, nEl, ppVx ) ;
       for ( kVx = 0 ; kVx < mVxEl ; kVx++ )
         *ppVx++ = pVxBase + nVx[kVx] ;
       reset_elem_all_mark ( pEl ) ;
       if ( ref )
         // Intepret reference for an element as a zone.
         // pEl->iZone = ref ;
         // Refs are non-zero, marks were incremented by 1
         elem_int2mark ( pEl, ref-1 ) ; // Unfinished: this needs unpacking zones as well.
     }
  }
  else {
     MMG5_pTria triMmg ;
     ulong_t nVx[3] ; // Tri only here.
     const int mVxEl = 3 ; // Tri only.
     for ( nEl = 1 ; nEl <= mesh->nt ; nEl++ ) {
       pEl++ ;
       triMmg = mesh->tria + nEl ;
       nVx[0] =  triMmg->v[0] ;
       nVx[1] =  triMmg->v[1] ;
       nVx[2] =  triMmg->v[2] ;
       init_elem ( pEl, tri, nEl, ppVx ) ;
       for ( kVx = 0 ; kVx < mVxEl ; kVx++ )
         *ppVx++ = pVxBase + nVx[kVx] ;
     }
  }

  ulong_t mConn = ppVx - pChunk->PPvrtx ;
  /* This test is not entirely safe, as it test for overflow after
     the oveflow has happened. But should be good enough. */
  if ( mConn > pChunk->mElem2VertP )
    hip_err ( fatal, 0, "overflow in elem2Vert pointers in mmg_conn." ) ;

  return ( mConn ) ;
}

/******************************************************************************
  mmg_get_conn_zone:   */

/*! extract element connectivity from a mmg3d mesh, set region via zone
 *
 */

/*

  Last update:
  ------------
  15Dec18; track ref/zone of elements.
  19Apri16; Added 2D options
  5Mar15; tidied up.
  Dec15: conceived by GS.


  Input:
  ------
  mesh: mmg3d mesh

  Changes To:
  -----------
  pChunk:

  Returns:
  --------
  number of connectivity entries transferred.

*/

ulong_t mmg_get_conn_zone ( MMG5_pMesh mesh, chunk_struct *pChunk ) {

  int nEl, kVx ;
  elem_struct *pEl = pChunk->Pelem ;
  vrtx_struct **ppVx = pChunk->PPvrtx ; // starts at zero.
  vrtx_struct * const pVxBase = pChunk->Pvrtx ;

  // use element number as discriminator for 3D meshed
  MMG5_int ref, nVx[4] ; // Tets only here, mmg only uses int.
  int req ;
  if ( mesh->ne != 0 ) {
     MMG5_pTetra tetMmg ;
     const int mVxEl = 4 ; // tets only.
     for ( nEl = 1 ; nEl <= mesh->ne ; nEl++ ) {
       pEl++ ;
       // Not really a good idea to poke in mmg's internals to get this data.
       /* tetMmg = mesh->tetra + nEl ; */
       /* nVx[0] =  tetMmg->v[0] ; */
       /* nVx[1] =  tetMmg->v[1] ; */
       /* nVx[2] =  tetMmg->v[3] ; */
       /* nVx[3] =  tetMmg->v[2] ; */
       // Use the API call, but how is the iterator set/reset to the first
       // tet? Example in libexamples/mmg3d/adaptation_example0/example0_b
       // doesn't do this.
       // But pay attention to different node ordering: 0,1,3,2.
       MMG3D_Get_tetrahedron(mesh, nVx,nVx+1,nVx+3,nVx+2, &ref,&req) ;
       init_elem ( pEl, tet, nEl, ppVx ) ;
       for ( kVx = 0 ; kVx < mVxEl ; kVx++ )
         *ppVx++ = pVxBase + nVx[kVx] ;
       if ( ref )
         // Intepret reference for an element as a zone.
         pEl->iZone = ref ;
     }
  }
  else {
     MMG5_pTria triMmg ;
     ulong_t nVx[3] ; // Tri only here.
     const int mVxEl = 3 ; // Tri only.
     for ( nEl = 1 ; nEl <= mesh->nt ; nEl++ ) {
       pEl++ ;
       triMmg = mesh->tria + nEl ;
       nVx[0] =  triMmg->v[0] ;
       nVx[1] =  triMmg->v[1] ;
       nVx[2] =  triMmg->v[2] ;
       init_elem ( pEl, tri, nEl, ppVx ) ;
       for ( kVx = 0 ; kVx < mVxEl ; kVx++ )
         *ppVx++ = pVxBase + nVx[kVx] ;
     }
  }

  ulong_t mConn = ppVx - pChunk->PPvrtx ;
  /* This test is not entirely safe, as it test for overflow after
     the oveflow has happened. But should be good enough. */
  if ( mConn > pChunk->mElem2VertP )
    hip_err ( fatal, 0, "overflow in elem2Vert pointers in mmg_conn." ) ;

  return ( mConn ) ;
}

/******************************************************************************
  mmg_get_bnd_per:   */

/*! Read boundary faces from mmg mesh.
 */

/*

  Last update:
  ------------
  25Se23; also support use of elem mark 
  4Sep19; initialise faces with invalid=0.
  2Apr19; catch zero face ref provided by mmg.
  8Mar19; catch pEl=NULL failure.
  14Dec18; new interface to  MMG3D_Get_tetFromTria.
  15Dec17; check valid element and face bounds of mmg output.
  19Apri16; Added 2D options
  5Mar16; conceived.


  Input:
  ------
  mesh: mmg3d mesh
  pChunk: chunk to write to
  mBc: number of bc in the base mesh
  ppBc: list of bc in the base mesh
  iZnL,U: zones at the l, u periodic boundaries.
  mBcInt: number of entries in list of internal bcs
  nBcInt: list of internal bc numbers.
  useElMark: if nonzero: id element region by mark, otherwise by zone.

  Changes To:
  -----------


  Returns:
  --------
  number of boundary faces transferred.

*/

int mmg_get_bnd_per ( MMG5_pMesh mesh, chunk_struct *pChunk,
                      const int mBc, bc_struct **ppBc,
                      const int iRegL, const int iRegU,
                      const int mBcInt, const int nBcInt[],
                      const int useElMark ) {

  if ( !pChunk->mBndFaces )
    /* No faces. Nothing to do. */
    return (0) ;

  // Reset vertex mark.
  uns_s *pUns = pChunk->pUns ;
  reserve_vx_markN ( pUns, 0, "mmg_get_bnd_per" ) ;
  reset_vx_markN ( pUns, 0 ) ;
  reserve_vx_markN ( pUns, 3, "mmg_get_bnd_per" ) ;
  reset_vx_markN ( pUns, 3 ) ;

  MMG5_int i ;
  int kSide, k, kMin ;
  int status ;
  int mFcRead = 0, mHybTri = 0, mInteriorFc=0 ;
  elem_struct *pElem = pChunk->Pelem ;
  MMG5_pTria triMmg ;
  MMG5_int nTet[2] ;
  int kFace[2] ;
  bndFc_struct bf ;
  // use the number of tetra as discriminator for 3D mesh
  if ( mesh->ne != 0 ) {
    /* 3D */
    for( i=1, triMmg=mesh->tria+1 ; i <= mesh->nt ; i++, triMmg++ ) {
      // we should really be using an API function, but there seems to
      // be no call to set/reset the iterator mesh->nti.
      if ( triMmg->ref < 1 ) {
        sprintf ( hip_msg, "found non-positive tri ref %"FMT_ULG" for tri %"FMT_ULG" "
                  "(%"FMT_ULG",%"FMT_ULG",%"FMT_ULG") in mmg_get_bnd_per."
                  " This should not have happened.",
                  triMmg->ref, i,triMmg->v[0],triMmg->v[1],triMmg->v[2] ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
      else if ( triMmg->ref <= mBc ) {
        mFcRead++ ;

        if ( match_int_list ( mBcInt, nBcInt, ppBc[triMmg->ref-1]->nr ) ) {
          /* This face is on an interior boundary to be split.
             Tag the forming nodes of this face. */
          for ( k = 0 ; k < 3 ; k ++ )
            pChunk->Pvrtx[ triMmg->v[k] ].mark = 1 ;
          ++mInteriorFc ;
        }
      }
    }

    // Re-alloc to add duplicate/interior faces.
    pChunk->mBndFaces += mInteriorFc ;
    pChunk->PbndFc = arr_realloc ( "PbndFc in mmg_get_bnd_per", pChunk->pUns->pFam,
                                   pChunk->PbndFc,  pChunk->mBndFaces+1,
                                   sizeof( *pChunk->PbndFc ) ) ;

    // Renumber vx based on tag.

    // vx 2 fc list considering only marked vx
    fc2el_s *pFc2El ;
    const int kMark= 1 ;
    llVxEnt_s *pllVxFc = make_llInterFc_vxMark ( pUns, &pFc2El, kMark ) ;
    int mFc2El = get_sizeof_llEnt ( pllVxFc ) ;



    // Add the duplicated internal, bnd fc and cut faces.
    int max_reg = MAX( iRegL, iRegU ) ;
    perBc_s *pPerBc ;
    int isL, kReg ;
    bc_struct *pBc ;
    int kMin ;
    const vrtx_struct *ppVxFc[3] ; // no more than tria here.
    elem_struct *pElFc[2], *pEl ;
    int nEnt, nFc[2] ;
    int status, mDbl=0, mSgl=0 ;
    bndFc_struct *pBf = pChunk->PbndFc+1 ;
    int mMissing_per_faces = 0 ;
    ulong_t mElViz = 0 ;
    const elem_struct **ppElViz = NULL ;
    for( i=1 ; i <= mesh->nt ; i++ ) {
      // we should really be using an API function, but there seems to
      // be no call to set/reset the iterator mesh->nti.
      triMmg = mesh->tria + i ;
      status = MMG3D_Get_tetFromTria( mesh, i, nTet, kFace ) ;

      mFcRead++ ;
      if ( !status )
        hip_err ( warning, 1,
                  "call to MMG3D_Get_tetFromTria failed in mmg_get_bnd_per." );
      else if ( nTet[0] > pChunk->mElems )
        hip_err ( fatal, 0,
                  "mmg returned out of bounds tet number in mmg_get_bnd_per." );
      else if ( kFace[0] > 4 )
        hip_err ( fatal, 0,
                  "mmg returned out of bounds face number in mmg_get_bnd_per." );
      else if ( triMmg->ref < 1 ) {
        sprintf ( hip_msg,
                  "mmg returned a non-positive bc ref %"FMT_ULG" in mmg_get_bnd_per.",
                  triMmg->ref );
        hip_err ( warning, 0, hip_msg ) ;
                 
      }

      /* Find the bc of this face, can be either side. */
      pBc = ppBc[triMmg->ref-1] ; // faces were submitted with nBc+1.
      
      if ( triMmg->ref > mBc ) {
        // cut face, fixed/frozen, will become internal upon re-glueing.
        bf.Pelem = pElem + nTet[0] ; // watch out, nTet is only int, not ulong!
        bf.nFace = kFcMMg2hip[kFace[0]] ;
        mHybTri++ ;

        // Tag all vx on cut faces with mark3.
        for ( k = 0 ; k < 3 ; k++ )
          pChunk->Pvrtx[triMmg->v[k]].mark3 = 1 ;
      }

      else if ( match_int_list ( mBcInt, nBcInt, ppBc[triMmg->ref-1]->nr ) ) {
        // Double-sided face. Check against the ones found using llVxFc.
        for ( k = 0 ; k < 3 ; k++ )
          ppVxFc[k] = pChunk->Pvrtx + triMmg->v[k] ;
        // No need to sort.
        // qsort ( ppVx, 3, sizeof ( vrtx_struct * ), cmp_vx ) ;
        //mDbl++ ;

        if ( !( nEnt =
                get_ent_vrtx ( pllVxFc, 3,( const vrtx_struct **) ppVxFc, &kMin ) ) ) {
          /* The face doesn't exist. Convert all to long, to cater for 32 and 64 mmg.*/
          long int nv[3] = { triMmg->v[0], triMmg->v[1], triMmg->v[2]};
          long int li = i ;
          sprintf ( hip_msg, "mmg face %ld, vx %ld, %ld, %ld, not present in mmg_get_bnd_per.",
                    //i, triMmg->v[0], triMmg->v[1], triMmg->v[2] ) ;
                    li, nv[0], nv[1], nv[2] ) ;
          hip_err ( fatal, 0, hip_msg ) ;
        }

        /* Extract the elements either side of the face. */
        show_fc2el_elel ( pFc2El, nEnt, pElFc, nFc, pElFc+1, nFc+1 ) ;

        if ( !(pPerBc = find_perBcPair ( pUns, pBc, &isL )) )
          hip_err ( fatal, 0, "failed to find per bc in mmg_get_bnd." ) ;

        for ( kSide = 0 ; kSide < 2 ; kSide++ ) {
          // Add bndFc.
          pBf->Pelem = pEl = pElFc[kSide] ;
          pBf->nFace = nFc[kSide] ;
          pBf->invalid = 0 ;

          if ( !pEl ) {
            sprintf ( hip_msg, "missing matching periodic bnd face to el %"FMT_ULG", in mmg_get_bnd_per.", pElFc[1-kSide]->number ) ;
            hip_err ( warning, 1, hip_msg ) ;
            mMissing_per_faces++ ;
          }
          else {
            if ( useElMark )
              // base on element marks. Then iRegL, iRegU are mark pos from zero
              kReg = elem_mark2int( pEl ) ;
            else
              // base on zones. Then iRegL,U are zones numered from 1.
              kReg = pEl->iZone ;

            if ( kReg < 0 && kReg >= max_reg )
              hip_err ( fatal, 0, "out of bounds zone number in mmg_get_bnd" ) ;
            else if ( kReg == iRegL )
              pBf->Pbc = pPerBc->pBc[0] ;
            else if ( kReg == iRegU )
              pBf->Pbc = pPerBc->pBc[1] ;
            else
              hip_err ( fatal, 0, "non periodic zone referenced"
                        " for internal face in mmg_get_bnd." ) ;
          }
          pBf++ ;
        }
      }
      else {
        // single-sided, external: use ref of face.
        pBf->Pelem = pElem + nTet[0] ; // watch out, nTet is only int
        pBf->nFace = kFcMMg2hip[kFace[0]] ;
        pBf->Pbc = pBc ;
        pBf->invalid = 0 ;
#ifdef DEBUG
        if ( verbosity > 6 )
          add_viz_el ( pBf->Pelem, &ppElViz, &mElViz ) ;
#endif
        pBf++ ; //mSgl++ ;
      }
    }

    release_vx_markN ( pUns, 0 ) ;

#ifdef DEBUG
    if ( verbosity > 6 && mElViz ) {
      viz_elems_vtk ( "postMmg_bnd_fc_elems.vtk", mElViz, ppElViz, NULL ) ;
      arr_free ( ppElViz ) ;
    }
#endif

    if ( mMissing_per_faces ) {
      sprintf ( hip_msg,
                "found %d missing matching periodic bnd faces in mmg_get_bnd_per.",
                mMissing_per_faces ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }

    // mmg3d count includes internal fixed tris, resize to final size.
    if ( pChunk->mBndFaces != pBf - pChunk->PbndFc-1 ) {
      pChunk->mBndFaces = pBf - pChunk->PbndFc-1 ;
      pChunk->PbndFc = arr_realloc ( "PbndFc in mmg_get_bnd", pChunk->pUns->pFam,
                                     pChunk->PbndFc,  pChunk->mBndFaces+1,
                                     sizeof( *pChunk->PbndFc ) ) ;
    }
  }
  else {
    /* 2D */
    MMG5_pEdge EdgeMmg ;
    MMG5_int nTri;
    int kEdge ;
    // Note: needs fixing for immersed bc faces.
    bndFc_struct *pBf = pChunk->PbndFc+1 ;

    const int kFcMMg2hip[] = {1,2,3} ; // same permuation as the vertices.
    for( i=1 ; i <= mesh->na ; i++ ) {
      EdgeMmg = mesh -> edge + i ;
      if ( EdgeMmg->ref-1 <= pUns->mBc ) {
        mFcRead ++;
        status = MMG2D_Get_triFromEdge( mesh, i, &nTri, &kEdge);

        if ( !status )
          hip_err ( fatal, 0,
                    "call to MMG2D_Get_TriFromEdge failed in mmg_2hip." );
        else if ( nTri > pChunk->mElems )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds tri number in mmg_2hip." );
        else if ( kEdge > 3 )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds edge number in mmg_2hip." );

        pBf->Pelem = pElem +  nTri;
        pBf->nFace = kFcMMg2hip[kEdge] ;
        pBf->Pbc   = pUns->ppBc[EdgeMmg->ref-1] ; // faces were submited with nBc+1.
        pBf++ ;
      }
    }
  }

  return ( mFcRead ) ;
}


/******************************************************************************
  mmg_get_bnd:   */

/*! Read boundary faces from mmg mesh.
 */

/*

  Last update:
  ------------
  15Dec17; check valid element and face bounds of mmg output.
  19Apri16; Added 2D options
  5Mar16; conceived.


  Input:
  ------
  mesh: mmg3d mesh
  mBc: number of bc in pBc
  pBc: list of bcs of the old mesh.

  Changes To:
  -----------
  pChunk

  Returns:
  --------
  number of boundary faces transferred.

*/

int mmg_get_bnd ( MMG5_pMesh mesh, chunk_struct *pChunk, const int mElems,
                  const int mBc, bc_struct **ppBc ) {

  if ( !pChunk->mBndFaces )
    /* No faces. Nothing to do. */
    return (0) ;

  int i ;
  int status ;
  int mFcRead = 0, mHybTri = 0 ;
  bndFc_struct *pBf = pChunk->PbndFc+1 ;
  elem_struct *pElem = pChunk->Pelem ;
  // use element number as discriminator for 3D mesh
  if ( mesh->ne != 0 ) {
    /* 3D */
    MMG5_pTria triMmg ;
    MMG5_int nTet ;
    int kFace ;
    for( i=1 ; i <= mesh->nt ; i++ ) {
      triMmg = mesh->tria + i ;
      status = MMG3D_Get_tetFromTria( mesh, i, &nTet, &kFace ) ;
      if ( triMmg->ref <= mBc ) {
        mFcRead++ ;
        if ( !status )
          hip_err ( warning, 1,
                    "call to MMG3D_Get_tetFromTria failed in mmg_2hip." );
        else if ( nTet > mElems )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds tet number in mmg_2hip." );
        else if ( kFace > 4 )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds face number in mmg_2hip." );

        pBf->Pelem = pElem + nTet ; // watch out, nTet is only int, not ulong!
        pBf->nFace = kFcMMg2hip[kFace] ;
        pBf->Pbc   = ppBc[triMmg->ref-1] ; // faces were submited with nBc+1.
        pBf++ ;
      }
      else {
        bndFc_struct bf ;
        bf.Pelem = pElem + nTet ; // watch out, nTet is only int, not ulong!
        bf.nFace = kFcMMg2hip[kFace] ;
        mHybTri++ ;
      }
    }
    // mmg3d count includes internal fixed tris, resize to final size.
    if ( pChunk->mBndFaces != pBf - pChunk->PbndFc-1 ) {
      pChunk->mBndFaces = pBf - pChunk->PbndFc-1 ;
      pChunk->PbndFc = arr_realloc ( "PbndFc in mmg_get_bnd", pChunk->pUns->pFam,
                                     pChunk->PbndFc,  pChunk->mBndFaces+1,
                                     sizeof( *pChunk->PbndFc ) ) ;
    }
  }
  else {
    /* 2D */
    MMG5_pEdge EdgeMmg ;
    MMG5_int nTri;
    int kEdge ;
    const int kFcMMg2hip[] = {1,2,3} ; // same permuation as the vertices.
    for( i=1 ; i <= mesh->na ; i++ ) {
      EdgeMmg = mesh -> edge + i ;
      if ( EdgeMmg->ref-1 <= mBc ) {
        mFcRead ++;
        status = MMG2D_Get_triFromEdge( mesh, i, &nTri, &kEdge);

        if ( !status )
          hip_err ( fatal, 0,
                    "call to MMG2D_Get_TriFromEdge failed in mmg_2hip." );
        else if ( nTri > mElems )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds tri number in mmg_2hip." );
        else if ( kEdge > 3 )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds edge number in mmg_2hip." );

        pBf->Pelem = pElem +  nTri;
        pBf->nFace = kFcMMg2hip[kEdge] ;
        pBf->Pbc   = ppBc[EdgeMmg->ref-1] ; // faces were submited with nBc+1.
        pBf++ ;
      }
    }
  }

  return ( mFcRead ) ;
}

/**************************************************************************
JDM: 17Dec18
This version attempts to use mmg's devel feature of listing both forming tets
of an immersed tria. But this does not work as advertised.

Furthermore, immersed tria can only carry one ref, we don't know which side
is which when we duplicate the single immersed face into two separate ones
after adaptation.
*/

int mmg_get_bnd_twofc ( MMG5_pMesh mesh, chunk_struct *pChunk, const int mElems,
                        const int mBc, uns_s *pUns, const int znIsL[] ) {

  if ( !pChunk->mBndFaces )
    /* No faces. Nothing to do. */
    return (0) ;

  MMG5_int i ;
  int kSide ;
  int status ;
  int mFcRead = 0, mHybTri = 0 ;
  bndFc_struct *pBf = pChunk->PbndFc+1 ;
  elem_struct *pElem = pChunk->Pelem ;
  MMG5_pTria triMmg ;
  MMG5_int nTet[2] ;
  int kFace[2] ;
  bndFc_struct bf ;
  // use element number as discriminator for 3D mesh
  if ( mesh->ne != 0 ) {
    /* 3D */
    for( i=1 ; i <= mesh->nt ; i++ ) {
      triMmg = mesh->tria + i ;
      status = MMG3D_Get_tetFromTria( mesh, i, nTet, kFace ) ;
      if ( triMmg->ref <= mBc ) {
        mFcRead++ ;
        if ( !status )
          hip_err ( warning, 1,
                    "call to MMG3D_Get_tetFromTria failed in mmg_2hip." );
        else if ( nTet[0] > mElems || nTet[1] > mElems  )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds tet number in mmg_2hip." );
        else if ( kFace[0] <0 || kFace[0] > 4 )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds face number in mmg_2hip." );

        for ( kSide = 0 ; kSide < 2 && nTet[kSide] ; kSide++ ) {
          if ( nTet[1] && kFace[1] > 0 && kFace[1] < 4 ) { // Possible bug in mmg? A single-sided face was listed with a non-zero nTet[1], but an out-of-range kFace[1].
            // double-sided, internal: look up zone and the bc associated with the zone.
            // We know the bc of one side of the pair, but not whether l or u.
            // Match l or u from the zone the element is in.
            pBf->Pelem = pElem + nTet[kSide] ; // watch out, int nTet not ulong!
            pBf->nFace = kFcMMg2hip[kFace[kSide]] ;
            pBf->Pbc = match_perPair_lu ( pUns->ppBc[triMmg->ref],
                                          pUns->mPerBcPairs, pUns->pPerBc,
                                          znIsL[pBf->Pelem->iZone] ) ;
            pBf++ ;
          }
          else if ( !kSide ) {
            // single-sided, external: use ref of face.
            pBf->Pelem = pElem + nTet[kSide] ; // watch out, nTet is only int
            pBf->nFace = kFcMMg2hip[kFace[0]] ;
            pBf->Pbc = pUns->ppBc[triMmg->ref-1] ; // faces were submitted with nBc+1.
            pBf++ ;
          }
        }
      }
      else {
        bf.Pelem = pElem + nTet[0] ; // watch out, nTet is only int, not ulong!
        bf.nFace = kFcMMg2hip[kFace[0]] ;
        mHybTri++ ;
      }
    }
    // mmg3d count includes internal fixed tris, resize to final size.
    if ( pChunk->mBndFaces != pBf - pChunk->PbndFc-1 ) {
      pChunk->mBndFaces = pBf - pChunk->PbndFc-1 ;
      pChunk->PbndFc = arr_realloc ( "PbndFc in mmg_get_bnd", pChunk->pUns->pFam,
                                     pChunk->PbndFc,  pChunk->mBndFaces+1,
                                     sizeof( *pChunk->PbndFc ) ) ;
    }
  }
  else {
    /* 2D */
    MMG5_pEdge EdgeMmg ;
    MMG5_int nTri;
    int kEdge ;
    const int kFcMMg2hip[] = {1,2,3} ; // same permuation as the vertices.
    for( i=1 ; i <= mesh->na ; i++ ) {
      EdgeMmg = mesh -> edge + i ;
      if ( EdgeMmg->ref-1 <= mBc ) {
        mFcRead ++;
        status = MMG2D_Get_triFromEdge( mesh, i, &nTri, &kEdge);

        if ( !status )
          hip_err ( fatal, 0,
                    "call to MMG2D_Get_TriFromEdge failed in mmg_2hip." );
        else if ( nTri > mElems )
          hip_err ( fatal, 0,
                    "mmg returned out of bounds tri number in mmg_2hip." );
        else if ( kEdge > 3 )
           hip_err ( fatal, 0,
                    "mmg returned out of bounds edge number in mmg_2hip." );

        pBf->Pelem = pElem +  nTri;
        pBf->nFace = kFcMMg2hip[kEdge] ;
        pBf->Pbc   = pUns->ppBc[EdgeMmg->ref-1] ; // faces were submited with nBc+1.
        pBf++ ;
      }
    }
  }

  return ( mFcRead ) ;
}



/******************************************************************************
  mmg_merge_hyb:   */

/*! Given a grid with hybrid and tet part, as well as a list of matching faces, unify duplicated vertices.
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  15Sep16: conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmg_merge_hyb ( uns_s *pUns, llVxEnt_s *pllVxTriFc, fc2el_s *pFc2El,
                    uns_s *pUnsMmg, MMG5_pMesh mesh ) {

  if ( !pllVxTriFc )
    // No list of hybrid faces, nothing to do.
    return ( 0 ) ;

  chunk_struct *pChTet = pUnsMmg->pRootChunk->PnxtChunk ;
  if ( !pChTet )
    // No second chunk for tets, i.e. no first chunk with hybrid elems.
    return ( 0 ) ;

  int mFc = pChTet->mBndFaces;
  bndFc_struct *pBndFc = pChTet->PbndFc, *pBF ;
  elem_struct *pTet ;
  int kFcTet ;
  elem_struct *pElem = pUnsMmg->pRootChunk->Pelem ;
  elem_struct *pElHyb0, *pElHyb, *pEl1 ;
  int nFcHyb ;
  int kFcHyb, kFc1 ;

  // Loop over all faces offered by mmg.
  MMG5_int i, nTet ;
  int kFace, status ;
  MMG5_pTria triMmg ;
  // Tets are numbered first when llFc was built, need to subtract from hybrid el number in pUnsMmg.
  ulong_t mTet = pUns->mElemsOfType[tet] ;
  for( i=1 ; i <= mesh->nt ; i++ ) {
    triMmg = mesh->tria + i ;
    nFcHyb = triMmg->ref-pUns->mBc ;
    if ( nFcHyb > 0 ) {
      // This is a hybrid face.
      status = MMG3D_Get_tetFromTria( mesh, i, &nTet, &kFace ) ;
      if ( !status )
        hip_err ( warning, 1,
                  "call to MMG3D_Get_tetFromTria failed in mmg_merge_hyb." );
      pTet = pChTet->Pelem + nTet ; // watch out, nTet is only int, not ulong!
      kFcTet = kFcMMg2hip[kFace] ;

      // Hybrid element is the first one.
      if ( !show_fc2el_elel ( pFc2El, nFcHyb, &pElHyb0, &kFcHyb, &pEl1, &kFc1 ) ) {
        sprintf ( hip_msg, "empty hybrid tri face %d in mmg_merge_hyb.", nFcHyb ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      else {
        // Hybrid elem in llFc is from pUns, we want its copy in the rootChunk of pUnsMmg.
        // Note that we renumbered with hybrid elems first, and copied into pUnsMmg in that order.
        pElHyb = pElem + pElHyb0->number - mTet ;
        merge_vx_face ( pUns->mDim, pElHyb, kFcHyb, pTet, kFcTet ) ;
      }
    }
  }

  /* Loop over all elements, find those that do not share a fixed face, but only an
     edge or a node. Find all pVx that have a vxCpt pointing elsewhere, update to end location.
  */
  vrtx_struct *pRootVrtx = pUnsMmg->pRootChunk->Pvrtx ;
  chunk_struct *pChunk = pUnsMmg->pRootChunk ; // No need to check the root chunk.
  elem_struct *pEl, *pElEnd, *pElBeg ;
  ulong_t mVx, kVx ;
  vrtx_struct *pVx, *pVxThChMin, *pVxThChMax ;
  int nThisChunk, nCh ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    nThisChunk = pChunk->nr ;
    pVxThChMin = pChunk->Pvrtx ;
    pVxThChMax = pVxThChMin + pChunk->mVerts+1 ; // add one to use < rather than <=
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ ) {
      mVx = elemType[ pEl->elType ].mVerts ;
      for ( kVx = 0 ; kVx < mVx ; kVx++ ) {
        pVx = pEl->PPvrtx[kVx] ;
        if ( pVx > pVxThChMin && pVx < pVxThChMax ) {
          // pointing to a vx in the second, tet chunk. Does it need repointing?
          nCh = pVx->vxCpt.nCh ;
          if ( nCh != nThisChunk ) {
            if ( nCh != 0 )
              hip_err ( fatal, 0, "in mmg_merge_hyb: expected pointer to root chunk." ) ;
            else {
              pEl->PPvrtx[kVx] = pRootVrtx + pVx->vxCpt.nr ;
            }
          }
        }
      }
    }
  }

  return ( 0 ) ;
}


/******************************************************************************
  mmg_get_mesh_3d_per:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  4Sep19; rename to to _get_mesh_ for naming consitency.
  3Apr19; add i/o doc.
  20Dec17; derived from mmg_2hip.


  Input:
  ------
  mesh: mmg3d mesh.
  pUns: original mesh with bcs
  pllVxTriFc, pFc2El: list of fixed/hybrid faces.
  mBc: number of bcs in ppBc
  ppBc: list of bcs
  iRegL/U: region/zone/mark+1 index of layers on lower/upper periodic bcs.
  mBcInt, nBcInt: number and list of internal (periodic) bcs.
  doCheck: if nonzero: perform a grid check.

  Changes To:
  -----------
  ppUnsMmg; a mesh with the modified tet part added to a possibly retained hybrid part

*/


uns_s *mmg_get_mesh_3d_per ( MMG5_pMesh mesh,
                             uns_s *pUns, llVxEnt_s *pllVxTriFc, fc2el_s *pFc2El,
                             const int mBc, bc_struct **ppBc,
                             const int iRegL, const int iRegU,
                             const int mBcPer, const int nBcPer[],
                             uns_s *pUnsMmg,
                             const int doCheck, const int useElMark ) {

  int mDim ;
  ulong_t mElem, mElemConn, mElemVx, mElemBndFc ;
  int maxRef ;


  /* Get sizes. */
  mmg_get_sizes ( mesh, &mDim, &mElem, &mElemConn, &mElemVx, &mElemBndFc, &maxRef ) ;

  if ( verbosity > 1 ) {
    sprintf( hip_msg, "MMG-adapted tet grid has "
             "%"FMT_ULG" elements, %"FMT_ULG" nodes, %"FMT_ULG" bnd faces.",
             mElem, mElemVx, mElemBndFc ) ;
    hip_err ( info, 1, hip_msg ) ;

    if ( pUnsMmg && pUnsMmg->pRootChunk ) {
      sprintf ( hip_msg, "Retaining %"FMT_ULG" non-tet elems and %"FMT_ULG" forming vertices.",
                pUnsMmg->pRootChunk->mElems,  pUnsMmg->pRootChunk->mVerts ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
  }

  chunk_struct *pChTriTet ;
  if ( !pUnsMmg )  {
    // No grid to add to. Make a grid for the tet only part.
    if ( !make_uns_grid ( &pUnsMmg, mDim, mElem, mElemConn, 0, mElemVx, 0,
                          mElemBndFc, pUns->mBc ) ) {
      sprintf ( hip_msg, "failed to alloc for grid in mmg2hip.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    // Chunk for the tets:
    pChTriTet = pUnsMmg->pRootChunk ;
  }
  else {
    pChTriTet = append_chunk ( pUnsMmg, mDim, mElem, 4*mElem, 0, mElemVx,
                               mElemBndFc, pUns->mBc ) ;
  }

  /* Fill tet part from mmg3d. */
  mmg_get_coor ( mesh, pChTriTet ) ;
  mmg_get_conn_mark ( mesh, pChTriTet ) ;
  mmg_get_bnd_per  ( mesh, pChTriTet, mBc, ppBc, iRegL, iRegU, mBcPer, nBcPer,
                     useElMark ) ;

  mmg_merge_hyb ( pUns, pllVxTriFc, pFc2El, pUnsMmg, mesh ) ;

  pUnsMmg->mBc = mBc ; // Make sure there is enough space for all bc in all chunks.
  make_uns_bndPatch ( pUnsMmg ) ;

  /* What if the mesh has been coarsened? Reset spacings. */
  pUnsMmg->epsOverlapSq = pUnsMmg->epsOverlap = -TOO_MUCH ;

  /* Reorder patch numbers in case there are duplicates */
  int iBc;
  bc_struct *pBc;
  for ( iBc = 0, pBc = find_bc ( "", 0 ) ; pBc ; pBc = pBc->PnxtBc )
    pBc->order = ++iBc ;
  make_uns_ppBc( pUnsMmg ) ;

  if ( doCheck )
    /* Validate the grid. */
    check_uns ( pUnsMmg, check_lvl ) ;

  return ( pUnsMmg ) ;
}




/******************************************************************************
  mmg_2hip:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  12Sep23; remove loop to reset bc order.
  3Apr19; add i/o var description.
  20Dec17; fix problems with ULG format mismatch.
  14Dec17; rename vars, use Elem, rather than Tet, as 2D is supported.
  15Sep16; add mmg_2hip
  1Jul16; new interface to check_uns.
  4Mar16; tidy up.
  Dec15: conceived. GS


  Input:
  ------
  mesh: mmg3d mesh.
  pUns: original mesh with bcs
  pllVxTriFc: list of hybrid fixed faces
  pFc2El: face to elem pointers for that list.
  doCheck: if nonzero: run a grid check

  Changes To:
  -----------
  ppUnsMmg; a mesh with the modified tet part added to a possibly retained hybrid part

*/

void mmg_2hip( MMG5_pMesh mesh, uns_s *pUns, llVxEnt_s *pllVxTriFc,
               fc2el_s *pFc2El, uns_s **ppUnsMmg, const int doCheck ) {

  int mDim ;
  ulong_t mElem, mElemConn, mElemVx, mElemBndFc ;
  int maxRef ;


  /* Get sizes. */
  mmg_get_sizes ( mesh, &mDim, &mElem, &mElemConn, &mElemVx,
                  &mElemBndFc, &maxRef ) ;

  if ( verbosity > 1 ) {
    sprintf( hip_msg, "MMG-adapted tet grid has "
             "%"FMT_ULG" elements, %"FMT_ULG" nodes, %"FMT_ULG" bnd faces.",
             mElem, mElemVx, mElemBndFc ) ;
    hip_err ( info, 1, hip_msg ) ;

    if ( *ppUnsMmg ) {
      // The hybrid chunk is not numbered yet, but allocated to the exact size.
      sprintf ( hip_msg, "Retaining %"FMT_ULG" non-tet elems and %"FMT_ULG" forming vertices.",
                (*ppUnsMmg)->pRootChunk->mElems,  (*ppUnsMmg)->pRootChunk->mVerts ) ;
      hip_err ( info, 1, hip_msg ) ;
    }
  }

  chunk_struct *pChTriTet ;
  if ( !(*ppUnsMmg) )  {
    // No hybrd part. Make a grid for the tet only part.
    if ( !make_uns_grid ( ppUnsMmg, mDim, mElem, mElemConn, 0, mElemVx, 0, mElemBndFc, pUns->mBc ) ) {
      sprintf ( hip_msg, "failed to alloc for grid in mmg2hip.\n" ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
    // Chunk for the tets:
    pChTriTet = (*ppUnsMmg)->pRootChunk ;
  }
  else {
    pChTriTet = append_chunk ( *ppUnsMmg, mDim, mElem, 4*mElem, 0, mElemVx, mElemBndFc, pUns->mBc ) ;
  }

  /* Fill tet part from mmg3d. */
  mmg_get_coor ( mesh, pChTriTet ) ;
  mmg_get_conn_zone ( mesh, pChTriTet ) ;
  mmg_get_bnd  ( mesh, pChTriTet, mElem, pUns->mBc, pUns->ppBc ) ;

  mmg_merge_hyb ( pUns, pllVxTriFc, pFc2El, *ppUnsMmg, mesh ) ;

  (*ppUnsMmg)->mBc = pUns->mBc ; // Make sure there is enough space for all bc in all chunks.
  make_uns_bndPatch ( *ppUnsMmg ) ;

  /* What if the mesh has been coarsened? Reset spacings. */
  (*ppUnsMmg)->epsOverlapSq = (*ppUnsMmg)->epsOverlap = -TOO_MUCH ;

  /* Reorder patch numbers in case there are duplicates */
  int mBc;
  bc_struct *pBc;
  /* This overrides a given order of bcs, and it doesn't affect
     duplication. 
  for ( mBc = 0, pBc = find_bc ( "", 0 ) ; pBc ; pBc = pBc->PnxtBc )
    pBc->order = ++mBc ;
  */
  make_uns_ppBc( *ppUnsMmg) ;

  if ( doCheck )
    /* Validate the grid. */
    check_uns ( *ppUnsMmg, check_lvl ) ;

  return ;
}




/******************************************************************************
  mmg_add_triFc:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  25Feb18; run PmatchFc from 1, not zero.
  15Sep16; rename to mmg_add_triFc.
  : conceived.


  Input:
  ------
  pElem: elem of face to add
  nFace: number of face in pElem
  iTag: integer tag, 0<iTag<mBc for boundaries, higher for matching faces
  doRequire: non-zero if face is required fix (matching or periodic)

  Changes To:
  -----------
  ppMesh: the mmg mesh
  pElem: element
  nFace: face of the element
  iTag: reference tag, e.g. bc number
  nTri: face number.
  doRequire: if non-zero flag as required/fixed.

*/

void mmg_add_triFc ( MMG5_pMesh pMMesh, elem_struct *pElem, int nFace, int iTag, int nTri, int doRequire ) {

  const faceOfElem_struct *pFoE = elemType[ pElem->elType ].faceOfElem + nFace ;
  const int *kVxFc = pFoE->kVxFace ;
  vrtx_struct **ppVx = pElem->PPvrtx ;

  MMG3D_Set_triangle( pMMesh,
                      ppVx[ kVxFc[0] ]->number,
                      ppVx[ kVxFc[1] ]->number,
                      ppVx[ kVxFc[2] ]->number, iTag, nTri ) ;

  if ( doRequire )
    MMG3D_Set_requiredTriangle ( pMMesh, nTri ) ;

  return  ;
}







/******************************************************************************
  mmg_put_mesh_3d:   */

/*! convert an uns mesh to mmg.
 */

/*

  Last update:
  ------------
  7may24; fix bug: include pyr in the list of hyb elems when numbering.
  2Jul20; Added angle detection parameter (default is 20.0)
  15Dec18; use new ucopy calls.
  25Feb18; run PmatchFc from 1, not zero.
  21Jan18; add matchFc.
  5Sep17; include internal faces.
  fix bug with writing mmg mesh when invalid vx (e.g.
  from a prior merge) are present.
  specify zones to be considered.
  21Feb17; read pBc->mark for required bcs whose fades are not allowed to change.
  17Feb17; intro mmg_hMin, mmg_hMax.
  9Sep16; strip out tet parts.
  9Sep16; indeed use 0 for successful return, as advertised.
  19Avril16: Renaming as 3D
  6Mar16: conceived.


  Input:
  ------
  hGrad: max spacing gradient.
  hausdorff: surface fidelity, lower values fit tighter to the initial mesh.
  isoFactor: refinement factor, multiplies h.
  mmg_hMin/hMax: if user-defined, use these as mesh size thresholds.
  pGrid: mesh to copy to mmg
  mZones: number of zones to include
  piZone: list of zones to consider.

  Changes:
  --------
  ppMesh: pointer to mmg mesh
  ppMet: pointer to mmg metric structure

  Output:
  -------
  ppllVxTriFc: vertex entry list to hybrid tri faces
  ppFc2El: list of hybrid tri faces.
  ppUnsMmg: new grid with hybrid elements copied over.
  pmTetVx: number of vertices forming tets.


  Returns:
  --------
  0 for success

*/

int mmg_put_mesh_3d ( MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet,
                      const double hGrad, const double hausdorff,
                      const double isofactor,
                      const double mmg_hMin, const double mmg_hMax,
                      grid_struct *pGrid,
                      llVxEnt_s **ppllVxTriFc, fc2el_s **ppFc2El,
                      uns_s **ppUnsMmg, ulong_t *pmTetVx ) {

  uns_s *pUns = pGrid->uns.pUns ;
  if ( !pUns->mElemsOfType[tet] ) {
    hip_err ( warning, 0, "this mesh has no tets, mmg3d can't help" ) ;
    return (0) ;
  }

  *ppMMesh = NULL ;
  *ppMMet = NULL ;
  *ppUnsMmg = NULL ;
  const double angledetection = 20.0 ;

  MMG3D_Init_mesh( MMG5_ARG_start,
                   MMG5_ARG_ppMesh, ppMMesh, MMG5_ARG_ppMet, ppMMet,
                   MMG5_ARG_end );

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "Initialisation of MMG3D");
    hip_err ( info, 1, hip_msg ) ;
  }

  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_verbose, 5);
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_noinsert, 0);
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_noswap, 0);
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_nomove, 0);
  if ( mmg_hMin != -TOO_MUCH )
    MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hmin, mmg_hMin ) ;
  if ( mmg_hMax != TOO_MUCH )
    MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hmax, mmg_hMax );

  MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hausd, hausdorff ) ;
  MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hgrad, hGrad );

  // Disable optimLES. It leads to small volumes at the moment
  // MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_optimLES, 1 );

  //  Adapt while keeping initial edge sizes
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_optim,0);

  // Turn off angle detection ?
  //MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_angle,0);

  // Avoid detecting live angles to reduce refinement in corners.
  MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_angleDetection,angledetection);

  // Check if tet only.
  elType_e elT ;
  int isHybTet = 0 ;
  for ( elT = pyr ; elT <= hex ; elT++ ) {
    if ( pUns->mElemsOfType[elT] ) {
      isHybTet = 1 ;
      break ;
    }
  }



  // Count the vertices only forming tets at the start of the list.
  // Easiest done by numbering.
  const int doUseNumber = 1 ;
  const int doReset = 1, doBound = 1 ;
  const int dontReset = 0, dontBound = 0 ;
  number_uns_grid_types ( pUns, tet, tet, doUseNumber, doReset, doBound ) ;
  int mTet = (int) pUns->mElemsNumbered ; // shipped to mmg, so not ulong_t.
  *pmTetVx  = (int) pUns->mVertsNumbered ; // shipped to mmg, so not ulong_t.
  int mBndTri = (int) pUns->mTriAllBc ;



  *ppllVxTriFc = NULL ;
  fc2el_s *pFc ;
  ulong_t mFcBecomeInt, mFcDupl, mFcRemoved, mFcUnMtch ;
  int mHybTri = 0 ; // shippped to mmg, so not ulong_t.
  grid_struct *pGrMmg ;
  int mHybVx ;
  if ( isHybTet ) {
    /* Renumber with non-tet and their forming vertices first.
       This allows to copy the non-tet part into a new mesh and retain
       the node pointers of the faces between tet and non-tet. */
    number_uns_grid_types ( pUns, pyr, hex, doUseNumber, doReset, doBound ) ;
    // Make a copy of the hybrid part.
    const int dontRenameDuplPerBc = 0 ;
    *ppUnsMmg = ucopy_oneUns ( pGrid, 1, &pGrMmg, &mHybVx, dontRenameDuplPerBc ) ;

    // Renumber tets first.
    number_uns_grid_types ( pUns, tet, tet, doUseNumber, doReset, dontBound ) ;
    number_uns_grid_types ( pUns, pyr, hex, doUseNumber, dontReset, doBound ) ;


    // create matching faces between tets and non-tets.
    // Note:
    // this flags all vx of tets, flags are not reset on exit of make_llHybTriFc,
    // re-used when writing vx to mmg.
    *ppllVxTriFc = make_llHybTriFc ( pUns, ppFc2El, doWarn.bndFc, doRemove.bndFc, 0,
                                     &mFcBecomeInt, &mFcDupl, &mFcRemoved, &mFcUnMtch ) ;

    // Number of tri faces between tets and pri/pyr.
    mHybTri = get_used_sizeof_llEnt ( *ppllVxTriFc ) ;
  }


  /* Count the number of tri boundary faces.
     mAllTriBc only contains geoType=bnd, we also need interfaces, so recount. */
  bndPatch_struct *pBndPatch ;
  int nBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pEl ;
  for ( nBc = 0, mBndTri = 0 ; nBc < pUns->mBc ; nBc++ ) {
    pBndPatch = NULL ;
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                               &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pEl = pBndFc->Pelem ;
        if ( pEl && pEl->number && pEl->elType == tet && pBndFc->nFace )
          mBndTri++ ;
      }
    }
  }

  /* Add matching faces. Search all chunks, even if mmg_adapt_per only
     populates the rootChunk with matching faces for the cuts. */
  matchFc_struct *pMFc ;
  elem_struct *pEl0, *pEl1 ;
  chunk_struct *pChunk = NULL ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    for ( pMFc = pChunk->PmatchFc+1 ; pMFc <= pChunk->PmatchFc+pChunk->mMatchFaces ; pMFc++ ){
      pEl0 = pMFc->pElem0 ;
      pEl1 = pMFc->pElem1 ;
      if ( ( pEl0 && pEl0->number && pEl0->elType == tet && pMFc->nFace0 ) ||
           ( pEl1 && pEl1->number && pEl1->elType == tet && pMFc->nFace1 ) )
        mBndTri++ ;
    }
  }




  /* Declare the sizes to mmg */
  /* JDM: watch out: these counters are ulong_t, but most likely
     mmg3d is expecting an int or unsigned int? This is likely to fail
     when ulong_t is compiled as 64bit int. */
  // version avril 2016
  //MMG3D_Set_meshSize( *ppMMesh, mPts, mTri, mTri, 0 ) ;
  // version juillet 2016
  int mPri=0, mQua=0, mEg=0 ;
  MMG3D_Set_meshSize( *ppMMesh, *pmTetVx, mTet, mPri, mBndTri+mHybTri, mQua, mEg ) ;


  /* Fill vertices that form tets. */
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  ulong_t mVx = 0 ;
  pChunk = NULL ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number && pVx->number <= *pmTetVx ) {// only numbered vx can be marked.
        MMG3D_Set_vertex( *ppMMesh,
                          pVx->Pcoor[0],
                          pVx->Pcoor[1],
                          pVx->Pcoor[2], 1, pVx->number) ;
        mVx++ ;
      }
  hip_check_count ( (int) *pmTetVx, (int) mVx,
                    "vertices", "mmg_put_mesh_3d" ) ;


  /* Fill tets. */
  pChunk = NULL ;
  elem_struct *pElEnd, *pElBeg ;
  mTet = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && pEl->elType == tet ) {
        MMG3D_Set_tetrahedron( *ppMMesh,
                               pEl->PPvrtx[0]->number,
                               pEl->PPvrtx[1]->number,
                               pEl->PPvrtx[3]->number,
                               pEl->PPvrtx[2]->number,
                               pEl->iZone, pEl->number );
        mTet++ ;
      }
  hip_check_count ( (int) mTet, (int) pUns->mElemsOfType[tet],
                    "tets", "mmg_put_mesh_3d" ) ;


  /* Fill moveable tri bnd faces. */
  pBndPatch = NULL ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int requiredBc ;
  int mHybBndFc = 0, mTri = 0 ;
  const int mBc = pUns->mBc ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    requiredBc = bc_is_per ( pUns->ppBc[nBc] ) || pUns->ppBc[nBc]->mark ;

    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                               &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pEl = pBndFc->Pelem ;
        if ( pEl && pEl->number && pBndFc->nFace ) {
          if ( pEl->elType == tet ) {
            mTri++ ;
            mmg_add_triFc ( *ppMMesh, pEl, pBndFc->nFace, nBc+1, mTri, requiredBc ) ;
          }
          else
            mHybBndFc++ ;
        }
      }
    }
  }
  hip_check_count ( (int) mBndTri, (int) mTri,
                    "bnd triangles", "mmg_put_mesh_3d" ) ;


  /* Add hybrid/fixed faces. */
  int nFc ;
  int kFc0, kFc1 ;
  const int doRequire = 1 ;
  for ( nFc = 1 ; nFc <= mHybTri ; nFc++ ) {
    show_fc2el_elel ( *ppFc2El, nFc, &pEl0, &kFc0, &pEl1, &kFc1 ) ;
    // Hybrid elems have been added first, so are in pEl0, tets in pEl1
    mTri++ ;
    mmg_add_triFc ( *ppMMesh, pEl0, kFc0, mBc+nFc, mTri, doRequire ) ;
  }



  /* Add matching faces. Search all chunks, even if mmg_adapt_per only
     populates the rootChunk with matching faces for the cuts. */
  pChunk = NULL ;
  while ( loop_chunks ( pUns, &pChunk ) ) {
    for ( pMFc = pChunk->PmatchFc+1 ; pMFc <= pChunk->PmatchFc+pChunk->mMatchFaces ; pMFc++ ){
      pEl0 = pMFc->pElem0 ;
      kFc0 = pMFc->nFace0 ;
      pEl1 = pMFc->pElem1 ;
      kFc1 = pMFc->nFace1 ;
      if ( pEl0 && pEl0->number && pEl0->elType == tet && kFc0 ) {
        mTri++ ;
        mmg_add_triFc ( *ppMMesh, pEl0, kFc0, mBc+mHybTri+nFc, mTri, doRequire ) ;

      }
      else if ( pEl1 && pEl1->number && pEl1->elType == tet && kFc1 ){
        mTri++ ;
        mmg_add_triFc ( *ppMMesh, pEl1, kFc1, mBc+mHybTri+nFc, mTri, doRequire ) ;
      }
    }
  }



  hip_check_count ( mHybTri,  (mTri-mBndTri),
                    "fixed/hyb triangles", "mmg_put_mesh_3d" ) ;

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_init:
*/
/*! initialise the mmg mesh.
 *
 *
 */

/*

  Last update:
  ------------
  : conceived.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void mmg_init ( uns_s *pUns,
                MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet,
                const double hGrad, const double hausdorff,
                const double isofactor,
                const double mmg_hMin, const double mmg_hMax ) {

  /* Initialise the mmg mesh. */
  *ppMMesh = NULL ;
  *ppMMet = NULL ;

  MMG3D_Init_mesh( MMG5_ARG_start,
                   MMG5_ARG_ppMesh, ppMMesh, MMG5_ARG_ppMet, ppMMet,
                   MMG5_ARG_end );

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "Initialisation of MMG3D");
    hip_err ( info, 1, hip_msg ) ;
  }

  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_verbose, 5);
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_noinsert, 0);
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_noswap, 0);
  MMG3D_Set_iparameter( *ppMMesh, *ppMMet, MMG3D_IPARAM_nomove, 0);
  if ( mmg_hMin != -TOO_MUCH )
    MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hmin, mmg_hMin ) ;
  if ( mmg_hMax != TOO_MUCH )
    MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hmax, mmg_hMax );

  MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hausd, hausdorff ) ;
  MMG3D_Set_dparameter( *ppMMesh, *ppMMet, MMG3D_DPARAM_hgrad, hGrad );

  return ;
}


/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_regions_zones_count:
*/
/*! renumber and count the grid elements in the required regions and zones.
 *
 *
 */

/*

  Last update:
  ------------
  27Feb22; dervived from mmg_zones_count.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmg_regions_zones_count ( uns_s *pUns,
                              const int mReg, const int iReg[],
                              const int mZones, const int iZone[],
                              int *pmTet, int *pmVxTet, int *pmTriBnd, int *pmTriCut ) {

  if ( !pUns->mElemsOfType[tet] ) {
    hip_err ( warning, 0, "this mesh has no tets, mmg3d can't help" ) ;
    return (0) ;
  }


  const int doUseNumber = 1 ;
  const int doReset = 1, dontReset = 0  ;
  // don't recompute ppBc, hence doBound=2.
  const int doBound = 2, dontBound = 0 ;
  ulong_t mConn ;
  if ( pUns->mZones != mZones ) {
    /* Number only the requested zones. */
    number_uns_grid_regions_zones
      ( pUns, mReg, iReg, mZones, iZone, doUseNumber, doReset, doBound, &mConn ) ;
  }
  else
    number_uns_grid_types ( pUns, tri, hex, doUseNumber, doReset, doBound ) ;


  /* For now, only tets. Make sure we have only tets. */
  if ( pUns->mElemsOfType[tet] != pUns->mElemsNumbered ) {
    sprintf ( hip_msg, "found %d numbered non-tet elems in  mmg_zones_count.",
              (int) ( pUns->mElemsNumbered-pUns->mElemsOfType[tet] ) ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  *pmTet = (int) pUns->mElemsNumbered ; // shipped to mmg, so not ulong_t.
  *pmVxTet  = (int) pUns->mVertsNumbered ; // shipped to mmg, so not ulong_t.
  *pmTriBnd = (int) pUns->mTriAllBc ;


  /* Use mark3 to count the vx on the zone interface. */
  // reset_vx_mark3 ( pUns ) ;  // JDM 11Jul19: mark is not used, so why reset?
  const int doMark = 0 ;
  *pmTriCut= 0 ;
  /* Count the number of cut faces around the zones. These are the faces
     of a numbered element, whith all nodes of the face marked 3. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  const elemType_struct *pElT ;
  int kFace ;
  int mVxCut = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number ) {
        pElT = elemType + pEl->elType ;
        for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
          if ( face_all_mark3_vx ( pEl, pElT, kFace, doMark, &mVxCut ) == 1 ) {
            (*pmTriCut)++ ;
          }
      }
  }

  return ( 0 ) ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_zones_count:
*/
/*! renumber and count the grid elements in the required zones.
 *
 *
 */

/*

  Last update:
  ------------
  6Sep18; rename reset_vx_mark.
  4Sep18; use number_uns_grid_types instead of number_uns_grid, following args change.
  8Sep17: extracted from mmg_put_mesh_3d.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmg_zones_count ( uns_s *pUns, const int mZones, const int iZone[],
                      int *pmTet, int *pmVxTet, int *pmTriBnd, int *pmTriCut ) {

  if ( !pUns->mElemsOfType[tet] ) {
    hip_err ( warning, 0, "this mesh has no tets, mmg3d can't help" ) ;
    return (0) ;
  }


  const int doUseNumber = 1 ;
  const int doReset = 1, dontReset = 0, dontUseMark = 0  ;
  // don't recompute ppBc, hence doBound=2.
  const int doBound = 2, dontBound = 0 ;
  if ( pUns->mZones != mZones ) {
    /* Number only the requested zones. */
    number_uns_grid_elem_regions
      ( pUns, mZones, iZone, doUseNumber, doReset, doBound, dontUseMark ) ;
  }
  else
    number_uns_grid_types ( pUns, tri, hex, doUseNumber, doReset, doBound ) ;


  /* For now, only tets. Make sure we have only tets. */
  if ( pUns->mElemsOfType[tet] != pUns->mElemsNumbered ) {
    sprintf ( hip_msg, "found %d numbered non-tet elems in  mmg_zones_count.",
              (int) ( pUns->mElemsNumbered-pUns->mElemsOfType[tet] ) ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  *pmTet = (int) pUns->mElemsNumbered ; // shipped to mmg, so not ulong_t.
  *pmVxTet  = (int) pUns->mVertsNumbered ; // shipped to mmg, so not ulong_t.
  *pmTriBnd = (int) pUns->mTriAllBc ;


  /* Use mark3 to count the vx on the zone interface. */
  // reset_vx_mark3 ( pUns ) ;  // JDM 11Jul19: mark is not used, so why reset?
  const int doMark = 0 ;
  *pmTriCut= 0 ;
  /* Count the number of cut faces around the zones. These are the faces
     of a numbered element, whith all nodes of the face marked 3. */
  chunk_struct *pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  const elemType_struct *pElT ;
  int kFace ;
  int mVxCut = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number ) {
        pElT = elemType + pEl->elType ;
        for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
          if ( face_all_mark3_vx ( pEl, pElT, kFace, doMark, &mVxCut ) == 1 ) {
            (*pmTriCut)++ ;
          }
      }
  }

  return ( 0 ) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_add_numbered_vx:
*/
/*! add the vertices in the specified zones to an mmg grid.
 *
 */

/*

  Last update:
  ------------
  18Sep23; renamed to numbered_vx
  5Sep18; don't list cut vertices.
  4Sep18; use mark3 for interface nodes.
  8Sep17: extracted from mmg_put_mesh_3d.


  Input:
  ------
  pUns: hip mesh
  pMMesh: mmg mesh
  mVxTet: number of tet vertices
  mVxCut: number of vertices on the cut around the zones

  Output:
  -------
  *ppVxCut: list of cut vertices.

  Returns:
  --------
  1 on failure, 0 on success

*/

void mmg_add_numbered_vx ( uns_s *pUns, MMG5_pMesh pMMesh,
                        int mVxTet
                        //, int mVxCut, vrtx_struct *ppVxCut[]
                        ) {


  /* Fill vertices that form tets. */
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  ulong_t mVx = 0, mVxCutWritten = 0 ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number ) {
        if ( pVx->mark3 ) {
          /* vertex on the cut, also add to pVxCut. */
          //ppVxCut[mVxCutWritten++] = pVx ;
          MMG3D_Set_vertex( pMMesh,
                            pVx->Pcoor[0],
                            pVx->Pcoor[1],
                            pVx->Pcoor[2], mVxCutWritten, pVx->number) ;
          /* Faces built with these nodes are required, so this may be
             redundant. */
          MMG3D_Set_requiredVertex(pMMesh, pVx->number ) ;
        }
        else {
          /* non-special vertex to be remeshed. */
          MMG3D_Set_vertex( pMMesh,
                            pVx->Pcoor[0],
                            pVx->Pcoor[1],
                            pVx->Pcoor[2], 0, pVx->number) ;
        }
        mVx++ ;
      }

  hip_check_count ( mVxTet, (int) mVx, "vertices", "mmg_add_numbered_vx" ) ;
  //hip_check_count ( mVxCut, mVxCutWritten, "cut vertices", "mmg_add_numbered_vx" ) ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_zones_add_tets:
*/
/*! add all tet to mmg using zones.
  18Sep23; this will become deprecated once per_mark works.
 *
 */

/*

  Last update:
  ------------
  18Sep23; this will become deprecated once per_mark works.
  22Dec18; intro freezeZoneInterFc.
  8Sep17: extracted from mmg_put_mesh_3d.


  Input:
  ------
  pUns: hip grid
  pMMesh: mmg mesh
  freezeZoneInterface: if nonzero: pass zone ids to mmg to retain interfaces.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void mmg_zones_add_tet ( uns_s *pUns, MMG5_pMesh pMMesh,
                         const int freezeZoneInterFc ) {

  chunk_struct *pChunk = NULL ;
  int mTet = 0 ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && pEl->elType == tet ) {
        MMG3D_Set_tetrahedron( pMMesh,
                               pEl->PPvrtx[0]->number,
                               pEl->PPvrtx[1]->number,
                               pEl->PPvrtx[3]->number,
                               pEl->PPvrtx[2]->number,
                               ( freezeZoneInterFc ? pEl->iZone : 0 ),
                               pEl->number );
        mTet++ ;
      }
  hip_check_count ( (int) mTet, (int) pUns->mElemsOfType[tet],
                    "tets", "mmg_zones_add_tet" ) ;

  return ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_zones_add_tets:
*/
/*! add all numbered tet to mmg using marks.
 *
 */

/*

  Last update:
  ------------
  18Sep23; derived from mmg_zones_add_tet

  Input:
  ------
  pUns: hip grid
  pMMesh: mmg mesh
  freezeZoneInterface: if nonzero: pass zone ids to mmg to retain interfaces.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void mmg_add_numbered_tet ( uns_s *pUns, MMG5_pMesh pMMesh,
                            const int freezeZoneInterFc ) {

  chunk_struct *pChunk = NULL ;
  int mTet = 0, kReg ;
  elem_struct *pEl, *pElBeg, *pElEnd ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && pEl->elType == tet ) {
        kReg = elem_mark2int ( pEl )+1 ;
        MMG3D_Set_tetrahedron( pMMesh,
                               pEl->PPvrtx[0]->number,
                               pEl->PPvrtx[1]->number,
                               pEl->PPvrtx[3]->number,
                               pEl->PPvrtx[2]->number,
                               ( freezeZoneInterFc ? kReg : 0 ),
                               pEl->number );
        mTet++ ;
      }
  hip_check_count ( (int) mTet, (int) pUns->mElemsOfType[tet],
                    "tets", "mmg_zones_add_tet" ) ;

  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_zones_add_tri:
*/
/*! add bnd tri and cut tri of selected zones to mmg mesh.
 *
 */

/*

  Last update:
  ------------
  4Sep19; track periodic bc numbering in a partial mesh with m/nBcPerMmg
  8Sep17: extracted from mmg_put_mesh_3d.


  Input:
  ------

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

void mmg_zones_add_tri ( uns_s *pUns, MMG5_pMesh pMMesh,
                         const int mTriBnd, const int mTriCut,
                         const int fixPer, const int fixZoneInterFc,
                         int *pmBcPerMmg, int nBcPerMmg[] ) {

  chunk_struct *pChunk = NULL ;

  /* Fill moveable tri bnd faces. */
  bndPatch_struct *pBndPatch = NULL ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int requiredBc ;
  int mTri = 0 ;
  int nBc ;
  const int mBc = pUns->mBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pEl ;
  int isPer ;
  *pmBcPerMmg = 0 ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    isPer = bc_is_per ( pUns->ppBc[nBc] ) ;
    if ( isPer )
      nBcPerMmg[ (*pmBcPerMmg)++ ] = nBc ;
    requiredBc = ( fixPer ?
                   isPer || pUns->ppBc[nBc]->mark :
                   0 );

    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                               &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pEl = pBndFc->Pelem ;
        if ( pEl && pEl->number && pBndFc->nFace ) {
          if ( pEl->elType == tet ) {
            mTri++ ;
            mmg_add_triFc ( pMMesh, pEl, pBndFc->nFace, nBc+1, mTri, requiredBc ) ;
          }
        }
      }
    }
  }
  hip_check_count ( mTriBnd, mTri, "bnd triangles", "mmg_put_mesh_3d" ) ;


  if ( fixZoneInterFc ) {
    /* Add hybrid/fixed faces. */
    const int doRequire = 1 ;
    const int dontMark = 0 ;
    int mTriCutWritten = 0 ;
    /* Count the number of cut faces around the zones. These are the faces
       with all nodes numbered of elements that are not numbered but valid. */
    elem_struct *pElEnd, *pElBeg ;
    const elemType_struct *pElT ;
    int kFace ;
    int mVxCut ;
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( pEl->number  ) {
          pElT = elemType + pEl->elType ;
          for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
            if ( face_all_mark3_vx ( pEl, pElT, kFace, dontMark, &mVxCut ) == 1 ) {
              mTri++ ;
              mmg_add_triFc ( pMMesh, pEl, kFace, mBc+pEl->iZone, mTri, doRequire ) ;
            }
        }
    }
    hip_check_count ( mTriCut, mTri-mTriBnd,
                      "fixed/cut triangles", "mmg_put_mesh_3d" ) ;
  }
  return ;
}



/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS,
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  mmg_zones_add_tri:
*/
/*! add bnd tri and cut tri of selected zones to mmg mesh.
 *
 */

/*

  Last update:
  ------------
  18Sep23; derived from mmg_zones_add_tri.


  Input:
  ------
  pUns: uns mesh to copy from
  pMMesh: mmg mesh to add to
  mTriBnd: number of bnd tri
  mTriCut: number of cut tri on exposed interfaces
  fixPer: if non-zero, declare periodic nodes frozen
  fixRegInterFc: if non-zero, declare interfaces between regions frozen

  Output:
  -------
  *pmBcPerMmg: number of periodic bcs?
   nBcPerMmg[]: list of periodic bcs?

*/

void mmg_add_tri ( uns_s *pUns, MMG5_pMesh pMMesh,
                   const int mTriBnd, const int mTriCut,
                   const int fixPer, const int fixRegionInterFc,
                   int *pmBcPerMmg, int nBcPerMmg[] ) {

  chunk_struct *pChunk = NULL ;

  /* Fill moveable tri bnd faces. */
  bndPatch_struct *pBndPatch = NULL ;
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int requiredBc ;
  int mTri = 0 ;
  int nBc ;
  const int mBc = pUns->mBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pEl ;
  int isPer ;
  *pmBcPerMmg = 0 ;
  for ( nBc = 0 ; nBc < mBc ; nBc++ ) {
    isPer = bc_is_per ( pUns->ppBc[nBc] ) ;
    if ( isPer )
      nBcPerMmg[ (*pmBcPerMmg)++ ] = nBc ;
    requiredBc = ( fixPer ?
                   isPer || pUns->ppBc[nBc]->mark :
                   0 );

    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                               &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pEl = pBndFc->Pelem ;
        if ( pEl && pEl->number && pBndFc->nFace ) {
          if ( pEl->elType == tet ) {
            mTri++ ;
            mmg_add_triFc ( pMMesh, pEl, pBndFc->nFace, nBc+1, mTri, requiredBc ) ;
          }
        }
      }
    }
  }
  hip_check_count ( mTriBnd, mTri, "bnd triangles", "mmg_put_mesh_3d" ) ;


  if ( fixRegionInterFc ) {
    /* Add hybrid/fixed faces. */
    const int doRequire = 1 ;
    const int dontMark = 0 ;
    int mTriCutWritten = 0 ;
    /* Count the number of cut faces around the regions. These are the faces
       with all nodes numbered of elements that are not numbered but valid. */
    elem_struct *pElEnd, *pElBeg ;
    const elemType_struct *pElT ;
    int kFace, kReg ;
    int mVxCut ;
    pChunk = NULL ;
    while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) ) {
      for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
        if ( pEl->number  ) {
          pElT = elemType + pEl->elType ;
          for ( kFace = 1 ; kFace <= pElT->mFaces ; kFace++ )
            if ( face_all_mark3_vx ( pEl, pElT, kFace, dontMark, &mVxCut ) == 1 ) {
              mTri++ ;
              kReg = elem_mark2int(pEl)+1 ;
              mmg_add_triFc ( pMMesh, pEl, kFace, mBc+kReg, mTri, doRequire ) ;
            }
        }
    }
    hip_check_count ( mTriCut, mTri-mTriBnd,
                      "fixed/cut triangles", "mmg_put_mesh_3d" ) ;
  }
  return ;
}


/******************************************************************************
  mmg_put_mesh_3d_regions_zones:   */

/*! convert part of an uns mesh identified by regions and/or zones to mmg.
 */

/*

  Last update:
  ------------
  27Feb22; derived from mmg_put_mesh_3d_zones.


  Input:
  ------
  hGrad: max spacing gradient.
  hausdorff: surface fidelity, lower values fit tighter to the initial mesh.
  isoFactor: refinement factor, multiplies h.
  mmg_hMin/hMax: if user-defined, use these as mesh size thresholds.
  pGrid: mesh to copy to mmg
  mReg: number of regions to include
  iReg: list of regions to include.
  mZones: number of zones to include
  iZone: list of zones to include.

  Changes:
  --------
  ppMesh: pointer to mmg mesh
  ppMet: pointer to mmg metric structure

  Output:
  -------
  pmBcPerMmg:
  nBcPerMmg:


  Returns:
  --------
  0 for success

*/

int mmg_put_mesh_3d_region_zones ( uns_s *pUns,
                            MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet,
                            const double hGrad, const double hausdorff,
                            const double isoFactor,
                            const double mmg_hMin, const double mmg_hMax,
                            grid_struct *pGrid,
                            const int freezePer, const int freezeZoneInterFc,
                            int mReg, int iReg[],
                            int mZones, int iZone[],
                            int *pmBcPerMmg, int nBcPerMmg[] ) {

  int mVxTet ;
  int mTet ;
  int mTriBnd, mTriCut ;

  //int mVxCut ;
  vrtx_struct **ppVxCut ;
  /* This also sets pUns->mBc, ppBc */
  mmg_regions_zones_count ( pUns, mReg, iReg, mZones, iZone,
                            &mTet, &mVxTet, &mTriBnd, &mTriCut ) ;

  /* JDM 27 Apr 23:
     running tekigo case of /home/jmuller/hip/bug/2023/adapt-per-apr/ISSUE_ADAPT
     comparing _per_mark to _per,
     mTet, mVxTet, mTriBnd match,
     but mTriCut doesn't.
`    _per_mark later fails with a negative tag ref from mmg.
  */



  /* Init the mmg mesh. */
  mmg_init ( pUns, ppMMesh, ppMMet, hGrad, hausdorff,
             isoFactor, mmg_hMin, mmg_hMax ) ;

  /* Declare the sizes to mmg */
  /* JDM: watch out: these counters are ulong_t, but most likely
     mmg3d is expecting an int or unsigned int? This is likely to fail
     when ulong_t is compiled as 64bit int. */
  int mPri=0, mQua=0, mEg=0 ;
  int mTri = ( freezeZoneInterFc ? mTriBnd+mTriCut : mTriBnd ) ;
  MMG3D_Set_meshSize( *ppMMesh, mVxTet, mTet, mPri, mTri, mQua, mEg ) ;


  /* Fill, including required vertices, faces. */

  /* Fill, including required vertices, faces. */
  mmg_add_numbered_vx ( pUns, *ppMMesh, mVxTet ) ;
  mmg_add_numbered_tet ( pUns, *ppMMesh, freezeZoneInterFc ) ;

  mmg_add_tri ( pUns, *ppMMesh, mTriBnd, mTriCut,
                freezePer, freezeZoneInterFc,
                pmBcPerMmg, nBcPerMmg ) ;

  return ( 0 ) ;
}


/******************************************************************************
  mmg_put_mesh_3d_zones:   */

/*! convert a number of zones of an uns mesh to mmg.
 */

/*

  Last update:
  ------------
  1tDec18; remove unused ppllVxTriFc and ppFc2El from arg list.
  21Sep18; fix bug with arg list in mmg_zones_count
  Sep17; derived from mmg_put_mesh3d.


  Input:
  ------
  hGrad: max spacing gradient.
  hausdorff: surface fidelity, lower values fit tighter to the initial mesh.
  isoFactor: refinement factor, multiplies h.
  mmg_hMin/hMax: if user-defined, use these as mesh size thresholds.
  pGrid: mesh to copy to mmg
  mZones: number of zones to include
  piZone: list of zones to consider.

  Changes:
  --------
  ppMesh: pointer to mmg mesh
  ppMet: pointer to mmg metric structure

  Output:
  -------
  ppUnsMmg: new grid of adapted zones.
  pmVxTet: number of vertices forming tets.


  Returns:
  --------
  0 for success

*/

int mmg_put_mesh_3d_zones ( uns_s *pUns,
                            MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet,
                            const double hGrad, const double hausdorff,
                            const double isoFactor,
                            const double mmg_hMin, const double mmg_hMax,
                            grid_struct *pGrid,
                            const int freezePer, const int freezeZoneInterFc,
                            int mZones, int iZone[],
                            int *pmBcPerMmg, int nBcPerMmg[] ) {

  int mVxTet ;
  int mTet ;
  int mTriBnd, mTriCut ;

  //int mVxCut ;
  vrtx_struct **ppVxCut ;
  /* This also sets pUns->mBc, ppBc */
  mmg_zones_count ( pUns, mZones, iZone, &mTet, &mVxTet, &mTriBnd, &mTriCut ) ;





  /* Init the mmg mesh. */
  mmg_init ( pUns, ppMMesh, ppMMet, hGrad, hausdorff,
             isoFactor, mmg_hMin, mmg_hMax ) ;

  /* Declare the sizes to mmg */
  /* JDM: watch out: these counters are ulong_t, but most likely
     mmg3d is expecting an int or unsigned int? This is likely to fail
     when ulong_t is compiled as 64bit int. */
  int mPri=0, mQua=0, mEg=0 ;
  int mTri = ( freezeZoneInterFc ? mTriBnd+mTriCut : mTriBnd ) ;
  MMG3D_Set_meshSize( *ppMMesh, mVxTet, mTet, mPri, mTri, mQua, mEg ) ;


  /* Fill, including required vertices, faces. */
  mmg_add_numbered_vx ( pUns, *ppMMesh, mVxTet ) ;
  mmg_zones_add_tet ( pUns, *ppMMesh, freezeZoneInterFc ) ;

  mmg_zones_add_tri ( pUns, *ppMMesh, mTriBnd, mTriCut,
                      freezePer, freezeZoneInterFc,
                      pmBcPerMmg, nBcPerMmg ) ;

  return ( 0 ) ;
}

/******************************************************************************
  mmg_put_mesh_2d:   */

/*! convert an uns 2D mesh to mmg.
 */

/*

  Last update:
  ------------
  4Apr19; fix bc with nonzero bc mark.
  17Feb17; intro mmg_hMin, mmg_hMax.
  14Avril16: conceived from mmg_put_mesh.


  Input:
  ------
  pMet: pointer to mmg metric structure
  hGrad: max spacing gradient.
  hausdorff: surface fidelity, lower values fit tighter to the initial mesh.
  isoFactor: refinement factor, multiplies h.
  mmg_hMin/hMax: if user-defined, use these as mesh size thresholds.
  pUns:

  Returns:
  --------
  pMMesh: pointer to mmg mesh

*/

int mmg_put_mesh_2d ( MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet,
                      const double hGrad, const double hausdorff,
                      const double isofactor,
                      const double mmg_hMin, const double mmg_hMax,
                      uns_s *pUns ) {


  // Check if mesh contains only triangles
  if ( pUns->mElemsOfType[qua] ) {
    sprintf ( hip_msg, "found %"FMT_ULG" %s elements, hybrid meshes are not supported in 2D yet.",
              pUns->mElemsOfType[qua], elemType[qua].name ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  *ppMMesh = NULL ;
  *ppMMet = NULL ;

  MMG2D_Init_mesh( MMG5_ARG_start,
                   MMG5_ARG_ppMesh, ppMMesh, MMG5_ARG_ppMet, ppMMet,
                   MMG5_ARG_end );

  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "Initialisation of MMG2D");
    hip_err ( info, 1, hip_msg ) ;
  }

  MMG2D_Set_iparameter( *ppMMesh, *ppMMet, MMG2D_IPARAM_verbose, 5);
  MMG2D_Set_iparameter( *ppMMesh, *ppMMet, MMG2D_IPARAM_noinsert, 0);
  MMG2D_Set_iparameter( *ppMMesh, *ppMMet, MMG2D_IPARAM_noswap, 0);
  MMG2D_Set_iparameter( *ppMMesh, *ppMMet, MMG2D_IPARAM_nomove, 0);
  if ( mmg_hMin != -TOO_MUCH )
    MMG2D_Set_dparameter( *ppMMesh, *ppMMet, MMG2D_DPARAM_hmin, mmg_hMin ) ;
  if ( mmg_hMax != TOO_MUCH )
    MMG2D_Set_dparameter( *ppMMesh, *ppMMet, MMG2D_DPARAM_hmax, mmg_hMax );

  MMG2D_Set_dparameter( *ppMMesh, *ppMMet, MMG2D_DPARAM_hausd, hausdorff ) ;
  MMG2D_Set_dparameter( *ppMMesh, *ppMMet, MMG2D_DPARAM_hgrad, hGrad );


  /* Count the number of boundary edges
     => Need to be more specific and count only tri edges
  */
  bndPatch_struct *pBndPatch = NULL ;
  ulong_t mBi = 0 ;
  int nBc ;
  bndFc_struct *pBndFc, *pBndFcBeg, *pBndFcEnd ;
  elem_struct *pElem ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                               &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pElem = pBndFc->Pelem ;
        if ( pElem && pElem->number && pBndFc->nFace ) mBi ++ ;
      }
    }
  }

  /* Declare the sizes to mmg */
  /* JDM: watch out: these counters are ulong_t, but most likely
     mmg3d is expecting an int or unsigned int? This is likely to fail
     when ulong_t is compiled as 64bit int. */
  ulong_t mPts = pUns->mVertsNumbered;
  ulong_t mTri = pUns->mElemsOfType[tri] ;
  /* 31/8/21: latest pull of mmg now needs the 0 quads. */
  MMG2D_Set_meshSize( *ppMMesh, mPts, mTri, 0, mBi ) ;

  /* Version for MMG develop branch
  MMG2D_Set_meshSize( *ppMMesh, mPts, mTri, 0, mBi ) ;
  */

  /* Fill vertices. */
  chunk_struct *pChunk = NULL ;
  vrtx_struct *pVx, *pVxBeg, *pVxEnd ;
  int nBeg, nEnd ;
  while ( loop_verts ( pUns, &pChunk, &pVxBeg, &nBeg, &pVxEnd, &nEnd ) )
    for ( pVx = pVxBeg ; pVx <= pVxEnd ; pVx++ )
      if ( pVx->number )
        MMG2D_Set_vertex( *ppMMesh,
                          pVx->Pcoor[0],
                          pVx->Pcoor[1],
                          1, pVx->number) ;


  /* Fill tets. */
  pChunk = NULL ;
  elem_struct *pEl, *pElEnd, *pElBeg ;
  ulong_t mTris = 0 ;
  while ( loop_elems ( pUns, &pChunk, &pElBeg, &pElEnd ) )
    for ( pEl = pElBeg ; pEl <= pElEnd ; pEl++ )
      if ( pEl->number && pEl->elType == tri ) {
        pEl->number = ++mTris ;
        MMG2D_Set_triangle ( *ppMMesh,
                             pEl->PPvrtx[0]->number,
                             pEl->PPvrtx[1]->number,
                             pEl->PPvrtx[2]->number, 1, mTris);
      }

  /* Fill moveable tri bnd faces. */
  pBndPatch = NULL ;
  ulong_t edge_number = 0 ; // keep same type as mBi
  const faceOfElem_struct *pFoE ;
  const int *kVxFc ;
  vrtx_struct **ppVx ;
  int isPer, isFixed ;
  for ( nBc = 0 ; nBc < pUns->mBc ; nBc++ ) {
    isPer = bc_is_per ( pUns->ppBc[nBc] ) ;
    isFixed = ( isPer ||  pUns->ppBc[nBc]->mark ) ;

    while ( loop_bndFaces_bc ( pUns, nBc, &pBndPatch,
                               &pBndFcBeg, &pBndFcEnd ) ) {
      for ( pBndFc = pBndFcBeg ; pBndFc <= pBndFcEnd ; pBndFc++ ) {
        pEl = pBndFc->Pelem ;
        if ( pEl && pEl->number && pBndFc->nFace ) {
          pFoE = elemType[ pEl->elType ].faceOfElem + pBndFc->nFace ;
          kVxFc = pFoE->kVxFace ;
          ppVx = pEl->PPvrtx ;
          edge_number++ ;
          MMG2D_Set_edge ( *ppMMesh,
                           ppVx[ kVxFc[0] ]->number,
                           ppVx[ kVxFc[1] ]->number,
                           nBc+1, edge_number);
          if ( isFixed ){
            MMG2D_Set_requiredEdge ( *ppMMesh, edge_number ) ;
          }
        }
      }
    }
  }
  if ( mBi != edge_number ) {
    sprintf ( hip_msg, "expected %"FMT_ULG", "
              "found %"FMT_ULG" Egdes in mmg_put_mesh_2d.",
              mBi, edge_number ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }

  /* For fixed tris, e.g. tet/pyr interfaces:
     MMG2D_Set_requiredEdge ( *ppMMesh, edge_number ) ;
  */

  return ( 1 ) ;
}

/******************************************************************************
  mmg_args:   */

/*! Filter arguments from the commandline for mmg
 *
 */

/*

  Last update:
  ------------
  9Jul20; use bounding box to determine default hausdorff
  10Jul19; support invoing mmg without iso keyword.
  2Apr19; intro mmg_mPerLayer arg.
  use find_nVar to address vars by name match.
  17Feb17; intro mmg_hMin/Max.
  20Dec16; allow to set hausdorff.
  26Apr16; allow the specific value of hGrad=-1.0 as a flag.
  6Mar16: conceived.


  Input:
  ------
  argLine: command line string.

  Changes To:
  -----------

  Output:
  -------

  Returns:
  --------
  1 on failure, 0 on success

*/

int mmg_args ( char argLine[], mmgMethod_e *pMmgMethod,
               double *pIsoFactor, int *pkVar, double *phGrad, double *phausdorff,
               int *pinterpolateSol, double *pmmg_hMin, double *pmmg_hMax,
               int *pmmg_mPerLayer, int *psavemsh, const uns_s *pUns,
               int *pmZones, int iZone[] ) {

  /* getopt needs an initial command. If deprecated iso* format was supplied,
     then we have the command keyword. If not, supply an default. */
  if ( strncmp ( argLine, "iso", 3 ) ) {
    char oldArgLine[TEXT_LEN] ;
    strncpy ( oldArgLine, argLine, TEXT_LEN-1 ) ;
    sprintf ( argLine, "iso " ) ;
    strncat ( argLine, oldArgLine, TEXT_LEN-1 ) ;
  }


  /* Pack into getopt form. */
  char **ppArgs = NULL ;
  int mArgs = r1_argfill ( argLine, &ppArgs ) ;

  /* Default values. */
  *pIsoFactor = 1.0 ;
  *phGrad = 1.4 ;
  *phausdorff = 1000.0 ;
  /* Use 0.05*bounding box length as default hausdorff as suggested by Algiane. */
  int ind;
  for ( ind = 0 ; ind < pUns->mDim; ind++){
    *phausdorff = MIN(0.05*(pUns->urBox[ind]-pUns->llBox[ind]),*phausdorff);
  }
  *pinterpolateSol = 1 ;
  *pkVar = 0 ;
  *psavemsh = 0 ;
  *pmmg_hMin = -TOO_MUCH ;
  *pmmg_hMax = TOO_MUCH ;

  /* Backward compatibility, but this will be overwritten by choice of opt args. */
  r1_str_tolower( ppArgs[0] ) ;
  if ( !strncmp ( ppArgs[0], "isofactor", 4 ) ) {
    *pMmgMethod = isoFac ;
  }
  else if  ( !strncmp ( ppArgs[0], "isovar", 4 ) ) {
    *pMmgMethod = isoVar ;
  }
  else if  ( !strncmp ( ppArgs[0], "isomap", 4 ) ) {
    *pMmgMethod = isoMap ;
  }

  /* Parse line of unix-style optional args. */
  char c ;
  double someDbl ;
  int someInt ;
  /* colon behind arg means argument to be read,
     double colon means optional argument, but needs to be joint: -a0, not -a 0. */
  while ((c = getopt_long ( mArgs, ppArgs, "f:v:g:h:il:mp:su:z:",NULL,NULL)) != -1) {
    switch (c)  {
    case 'f': // isoFactor
      someDbl = atof( optarg ) ;
      if ( someDbl < 0. ) {
        sprintf ( hip_msg, "requesting scalar multiplier for length or sensor"
                  " with factor %g < 0, ignored.", someDbl ) ;
        hip_err ( warning, 1, hip_msg ) ;
        return ( 0 ) ;
      }
      if ( *pMmgMethod != isoMap && *pMmgMethod != isoVar )
        *pMmgMethod = isoFac ;
      *pIsoFactor = someDbl ;
      break;
    case 'g': // spacing gradient
      someDbl = atof( optarg ) ;
      if ( *phGrad !=-1.0 && someDbl < 0. ) {
        sprintf ( hip_msg, "requesting spacing gradient hGrad"
                  " with value %g < 0, Graduation will be ignored.", someDbl ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      *phGrad = someDbl ;
      break;
    case 'h': // Hausodorff
      someDbl = atof( optarg ) ;
      if ( someDbl < 0. ) {
        sprintf ( hip_msg, "requesting hausdorff distance"
                  " with value %g < 0, ignored.", someDbl ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      *phausdorff = someDbl ;
      break;
    case 'i': // interpolate solution
      *pinterpolateSol = 0 ;
      if ( *pinterpolateSol !=1 ) {
        sprintf ( hip_msg, "requesting to not interpolate flow solution to new mesh");
        hip_err ( warning, 1, hip_msg ) ;
      }
      break;
    case 'l': // lower, hMin threshold
      someDbl = atof( optarg ) ;
      if ( someDbl < 0. ) {
        sprintf ( hip_msg, "requesting hMin threshold"
                  " with value %g < 0, ignored.", someDbl ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      *pmmg_hMin = someDbl ;
      break ;
    case 'm': // map only, replaces deprecated isoMap
      *pMmgMethod = isoMap ;
      if ( *pMmgMethod == isoMap ) {
        sprintf ( hip_msg, "requesting refinement map only.");
        hip_err ( warning, 1, hip_msg ) ;
      }
      break;
    case 'p': // number of layers on periodic boundaries
      someInt = atoi( optarg ) ;
      if ( someInt < 0 ) {
        sprintf ( hip_msg, "requesting number of perioic bc layers"
                  " with value %d < 0, ignored.", someInt ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      *pmmg_mPerLayer = someInt ;
      break;
    case 's': // save mesh as medit.
      *psavemsh = 1 ;
      if ( *psavemsh !=0 ) {
        sprintf ( hip_msg, "requesting to save msh Medit files");
        hip_err ( warning, 1, hip_msg ) ;
      }
      break;
    case 'u':
      someDbl = atof( optarg ) ;
      if ( someDbl < 0. ) {
        sprintf ( hip_msg, "requesting hMax threshold"
                  " with value %g < 0, ignored.", someDbl ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      *pmmg_hMax = someDbl ;
      break;
    case 'v': // select adaptation variable
      //*pkVar = atoi( optarg )-1 ; {
      *pkVar = find_kVar ( &pUns->varList, -1, optarg ) ;
      if ( *pkVar < 0 ) {
        sprintf ( hip_msg, "no variable matches %s.", optarg ) ;
        hip_err ( fatal, 0, hip_msg ) ;
        return ( 1 ) ;
      }
      else {
        if ( *pMmgMethod != isoMap )
          *pMmgMethod = isoVar ;
        sprintf ( hip_msg, "using metrics in var %d: %s.",
                  *pkVar+1,pUns->varList.var[*pkVar].name ) ;
        hip_err ( info, 2, hip_msg ) ;
      }
      break;
    case 'z': // select adaptation zone, not used yet.
      someInt = atoi( optarg )-1 ;
      if ( someInt < 0 || someInt >= pUns->mZones ) {
        sprintf ( hip_msg, "no zone numbered %d, ignored."
                  " use the command 'zo' to list all zones",
                  someInt ) ;
        hip_err ( warning, 1, hip_msg ) ;
      }
      iZone[*pmZones] = someInt ;
      (*pmZones)++ ;
      break;

    case '?':
      if ( optopt == 'l' )
        fprintf (stderr, "Option -%c requires an argument.\n", optopt );
      else if (isprint (optopt)) {
        sprintf ( hip_msg, "Unknown option `-%c'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
      else {
        sprintf ( hip_msg, "Unknown option character `\\x%x'.\n", optopt);
        hip_err ( warning, 1, hip_msg ) ;
        break ;
      }
    default:
      sprintf ( hip_msg, "getopt error `\\x%x'.\n", optopt);
      hip_err ( warning, 1, hip_msg ) ;
      break ;
    }
  }


  /* No specific zones for now.
   *pmZones = 0 ; */

  return ( 1 ) ;
}

/******************************************************************************
  mmg_free_all:   */

/*! liberate mmg memory.
 */

/*

  Last update:
  ------------
  6Mar16: conceived.

  Changes To:
  -----------
  ppMMesh: pointer to pointer to mesh
  ppMMet: pointer to pointer to metric

*/

void mmg_free_all (  MMG5_pMesh *ppMMesh, MMG5_pSol *ppMMet ) {

  MMG3D_Free_all( MMG5_ARG_start,
                  MMG5_ARG_ppMesh, ppMMesh,
                  MMG5_ARG_ppMet, ppMMet,
                  MMG5_ARG_end);

  return ;
}

/******************************************************************************
  adapt_mmg_2d:   */

/*! brief descr for doxygen
 *
 * more detailed desc for doxygen
 *
 */

/*

  Last update:
  ------------
  3Sep18; pass pUns, rather than pGrid.
  15Dec17; add -i arg to mmg_args.
  16April16; Conceived from adapt_mmg for distinction 2D/3D

  Input:
  ------
  pUns
  argLine

  Changes To:
  -----------

  Returns:
  --------
  1 on failure, 0 on success

*/
int adapt_mmg_2d ( uns_s *pUns, char* argLine ) {

  mmgMethod_e mmgMethod = noMmgMeth ;
  int status;
  double isoFactor ;
  int kVarFactor ;
  int savemsh ;
  double hGrad ;
  double hausdorff ;
  int interpolateSol ;
  double mmg_hMin, mmg_hMax ;
  int mmg_mPerLayer = DEFAULT_mmg_mPerLayer ;
  int mZones, iZone[MAX_ZONES] ;
  if ( !mmg_args ( argLine, &mmgMethod, &isoFactor, &kVarFactor, &hGrad, &hausdorff,
                   &interpolateSol, &mmg_hMin, &mmg_hMax,
                   &mmg_mPerLayer, &savemsh, pUns, &mZones, iZone ) )
    return (1) ;

  MMG5_pMesh pMMesh ;
  MMG5_pSol pMMet ;

  if ( !mmg_put_mesh_2d ( &pMMesh, &pMMet, hGrad, hausdorff,
                          isoFactor, mmg_hMin, mmg_hMax, pUns ) )
    hip_err ( fatal, 0, "failed to put mesh to mmg2d in adap_mmg." ) ;

  printf("here/n");


  if ( !MMG2D_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after MG2D_Set_solSize in adapt_mmg" ) ;

  if ( !MMG2D_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after MMG2D_Chk_meshData in adapt_mmg" ) ;

  status = MMG2D_doSol_iso ( pMMesh, pMMet );
  if ( !status ) hip_err ( fatal, 0, "call to MMG2_doSol");

  if ( mmgMethod == isoFac ) {
    // constant factor
    mmg_metric_from_const ( pMMesh, pMMet, isoFactor, mmg_hMin, mmg_hMax ) ;
  }
  else if ( mmgMethod == isoVar || mmgMethod == isoMap ) {
    // some variable.
    if ( !mmg_metric_from_var ( pUns, pMMesh->np, pMMesh, pMMet,
                                kVarFactor, isoFactor,
                                mmgMethod, mmg_hMin, mmg_hMax ) ) {
      mmg_free_all ( &pMMesh, &pMMet ) ;
      return (1) ;
    }
  }

  char flNm[LINE_LEN] ;
  if ( savemsh == 1 ){
    /* 2D only. */
    strcpy ( flNm, "orig_mesh2d" ) ;
    mmg_write_mesh2d ( pMMesh, pMMet, flNm ) ;
  }

  if  ( mmgMethod == isoMap ) {
    // Done.
    mmg_free_all ( &pMMesh, &pMMet ) ;
  }
  else {
    //  Adapt
    clock_t start_t  = clock () ;
    status = MMG2D_mmg2dlib ( pMMesh, pMMet ) ;
    if ( status ) hip_err ( fatal, 0, "2D Mesh adaptation failed");
    clock_t end_t = clock () ;
    clock_t total_t = (double)(end_t - start_t)  / CLOCKS_PER_SEC;
    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "MMG2D adaptation time %g s", total_t*1.);
      hip_err ( info, 1, hip_msg ) ;
    }

    if ( savemsh == 1 ) {
      /* 2D only. */
      strcpy ( flNm, "adapted_mesh2d" ) ;
      mmg_write_mesh2d ( pMMesh, pMMet, flNm ) ;
    }

    //  Convert to hip structures
    // For now: no support of hybrid meshes in 2D.
    uns_s *pUnsMmg = NULL ;
    mmg_2hip( pMMesh, pUns, NULL, NULL, &pUnsMmg, 1 );

    mmg_free_all ( &pMMesh, &pMMet ) ;

    if ( interpolateSol && pUns->varList.mUnknowns ){
      uns_interpolate ( pUns, pUnsMmg, 0 ) ;
    }
  }
  return (0) ;
}

/******************************************************************************
  adapt_mmg3d_nonPer:   */

/*! adapt the tet part of a grid with mmg3d.
 *
 */

/*

  Last update:
  ------------
  21Sep18; new arg doCheck to mmg-2hip.
  3Sep8; rename to mmg3d_noPer, call using wrapper from hip's mmg_menu.
  5Jul18; fix bug with segfauling mmg -s, use mmg_write_mesh3d throughout.
  15Dec17; add -i arg to mmg_args.
  4Jyl17; add doxygen short explanation.
  15Dec16; carry mTetVx to mmg_metric_from_var to fix hybrid case segfault.
  16Apr16; Renamed from adapt_mmg for distinction 2D/3D
  4Mar16; tidy up.
  Dec15: conceived by GS

da
  Input:
  ------
  pUns
  argLine

  Changes To:
  -----------

  Returns:
  --------
  1 on failure, 0 on success

*/
int adapt_mmg3d_nonPer ( uns_s *pUns, char *argLine ) {

  // What to do next?
  mmgMethod_e mmgMethod ;
  int status;
  double isoFactor ;
  int kVarFactor ;
  int savemsh ;
  double hGrad ;
  double hausdorff ;
  int interpolateSol ;
  double mmg_hMin, mmg_hMax ;
  int mmg_mPerLayer = DEFAULT_mmg_mPerLayer ;
  int mZones = 0;
  int iZone[MAX_ZONES] ;
  if ( !mmg_args ( argLine, &mmgMethod, &isoFactor, &kVarFactor, &hGrad,
                   &hausdorff, &interpolateSol, &mmg_hMin, &mmg_hMax,
                   &mmg_mPerLayer, &savemsh, pUns, &mZones, iZone ) )
    return (0) ;


  /* Copy the pUns to mmg. */
  MMG5_pMesh pMMesh ;
  MMG5_pSol pMMet ;
  // The new grid, initialise with the retained hybrid part.
  uns_s *pUnsMmg ;
  llVxEnt_s *pllVxTriFc ;
  fc2el_s *pFc2El ;
  ulong_t mTetVx ;
  if ( mmg_put_mesh_3d ( &pMMesh, &pMMet, hGrad, hausdorff,
                         isoFactor, mmg_hMin, mmg_hMax,
                         pUns->pGrid,
                         &pllVxTriFc, &pFc2El,
                         &pUnsMmg, &mTetVx ) )
    hip_err ( fatal, 0, "failed to put mesh to mmg3d in adapt_mmg3d." ) ;


  /* Define sizes for a solution/metric field and allocate. */
  if ( !MMG3D_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after MG3D_Set_solSize in adapt_mmg3d" ) ;
  if ( !MMG3D_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after MMG3D_Chk_meshData in adapt_mmg3d" ) ;

  hip_err ( info, 0, "before call MMG3D_doSol in adapt_mmg3d, setting mesh->info.optim=1" ) ;
  pMMesh->info.optim = 1 ;
  // Initialise metric with average neighbor cell edge length
  status = MMG3D_doSol_iso ( pMMesh, pMMet );
  if ( !status ) {
    sprintf ( hip_msg, "call to MMG3D_doSol with status %d", status ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }
  hip_err ( info, 0, "after call MMG3D_doSol in adapt_mmg3d, setting mesh->info.optim=0" ) ;
  pMMesh->info.optim = 0 ;


  if ( mmgMethod == isoFac ) {
    // constant factor
    mmg_metric_from_const ( pMMesh, pMMet, isoFactor, mmg_hMin, mmg_hMax ) ;
  }
  else if ( mmgMethod == isoVar || mmgMethod == isoMap ) {
    // some variable.
    if ( !mmg_metric_from_var ( pUns, mTetVx, pMMesh, pMMet,
                                kVarFactor, isoFactor, mmgMethod, mmg_hMin, mmg_hMax ) ) {
      /* Some failure. */
      mmg_free_all ( &pMMesh, &pMMet ) ;
      return (1) ;
    }
  }

  char flNm[LINE_LEN] ;
  if  ( savemsh == 1 ) {
    /* 3D only. */
    strcpy ( flNm, "orig_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }

  if  ( mmgMethod == isoMap ) {
    // Done.
    mmg_free_all ( &pMMesh, &pMMet ) ;
  }
  else {
    // 	Adapt
    /* Display MMG parameters */
    status = MMG3D_defaultValues( pMMesh) ;

    clock_t start_t  = clock () ;
    status = MMG3D_mmg3dlib ( pMMesh, pMMet ) ;
    if ( status ) hip_err ( fatal, 0, "3D Mesh adaptation failed");
    clock_t end_t = clock () ;
    clock_t total_t = (double)(end_t - start_t)  / CLOCKS_PER_SEC;
    if ( verbosity > 2 ) {
      sprintf ( hip_msg, "MMG3D adaptation time %g s", total_t*1.);
      hip_err ( info, 1, hip_msg ) ;
    }

    // Save MMG format
    if  ( savemsh == 1 ) {
      sprintf ( flNm, "adapted_mesh3d" ) ;
      mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
    }

    //  Convert to hip structures
    mmg_2hip( pMMesh, pUns, pllVxTriFc, pFc2El, &pUnsMmg, 1 );

    /* Free. */
    free_llEnt ( &pllVxTriFc ) ;

    mmg_free_all ( &pMMesh, &pMMet ) ;

    if ( interpolateSol && pUns->varList.mUnknowns )
      uns_interpolate ( pUns, pUnsMmg, 0 ) ;

    /* Copy zones and their parameters. */
    //GS for now do not copy zones since there is a bug and nodes are not tagged
    /*zone_copy_all ( pUns, pUnsMmg ) ;*/
  }
  return (0) ;
}

/******************************************************************************
  adapt_mmg3d_per:   */

/*! adapt the tet part of a grid with periodic boundaries using mmg3d.
 *
 */

/*

  Last update:
  ------------
  7Dec20; check for existence of hip_target_egLen, use and overwrite if it exists.
  17Oct20; initialise mDim with pUns2.
  17Apr20; call special_verts to ensure periodic metric.
  14Jul19; fix bug with feeding metric from pUns, rather than pUns2.
  13Jul19; remove duplicate saling in mult_var and mmg_egLen_from_var,
           rename kVarLen to kVarTrgtLen to indicute purpose.
  12Jul19; fix bug with retaining required flag for vertices on the U,L zone interfaces.
  10Jul19; allow scaling of sensor variable with isoFactor.
  12Apr19; toward hybrid and periodic adapt. Needs hyb faces. For now: stop!
  4Apr19; new interface to zone_elem_mod_perBcLayer, limit to tets.
  2Apr19; use mark3 to tag interfaces, fix bugs with merge after first adapt.
  first working version.
  8Mar19; copy epsOverlap, hMin, etc, to the copied grids.
  14Feb19; carry unknowns, including scaling field.
  3Sep18; complete draft.
  15Dec17; new interface to mmg_args
  5Sep17; derived from adapt_mmg_3d


  Input:
  ------
  pUns
  argLine

  Changes To:
  -----------

  Returns:
  --------
  1 on failure, 0 on success

*/
int adapt_mmg3d_per ( uns_s *pUns, char* argLine ) {

  /* Make sure the metric is periodic. */
  special_verts ( pUns ) ;


  /* Check whether there are no zones present.
  if ( pUns->mZones ) {
    hip_err ( warning, 1, "grids with existing zones can currently not be\n"
              "          periodically adapted. Either remove your zones,\n"
              "          or come back later.\n" ) ;
    return ( 1 ) ;
  } */

  if ( pUns->mElemsOfType[pyr] || pUns->mElemsOfType[pri] || pUns->mElemsOfType[hex] ) {
    hip_err ( warning, 1, "hybrid grids with periodicity can currently not be\n"
              "      periodically adapted. Come back later.\n" ) ;
    return ( 2 ) ;
  }

  // What to do next?
  mmgMethod_e mmgMethod ;
  int status;
  double isoFactor ;
  int kVarFactor ; // 0 ... mUnknowns-1
  int savemsh ;
  double hGrad ;
  double hausdorff ;
  int interpolateSol ;
  double mmg_hMin, mmg_hMax ;
  int mmg_mPerLayer = DEFAULT_mmg_mPerLayer ;
  int mZonesArg, iZoneArg[MAX_ZONES] ; // not used yet
  if ( !mmg_args ( argLine, &mmgMethod, &isoFactor, &kVarFactor, &hGrad,
                   &hausdorff, &interpolateSol, &mmg_hMin, &mmg_hMax,
                   &mmg_mPerLayer, &savemsh, pUns, &mZonesArg, iZoneArg ) )
    return (0) ;




  // add a var field for a size metric (not a scaling metric.)
  // store as an unkonwn, to use standard interpolation tools for 2nd pass.
  varList_s *pVL = &pUns->varList ;
  var_s *pVar = find_var_name ( pVL, NULL, "hip_target_egLen" ) ;
  int kVarTrgtLen = -1 ;

  /* Does hip_target_egLen exist?  */
  if ( pVar ) {
    // It does, reuse.
    hip_err ( warning, 2, "internal solution field `hip_target_egLen' exists already\n"
              "            will be reused and overwritten.\n" ) ;
    kVarTrgtLen = pVar - pVL->var ;
  }
  else {
    // It doesn't, add.
    int mUnSol = pVL->mUnknowns++ ;
    kVarTrgtLen = mUnSol ; // New variable to store the target edge length.
    realloc_unknowns ( pUns, mUnSol, mUnSol+1 ) ;
    if ( pUns->varList.varType == noVar ) pUns->varList.varType = noType ;
    pVar = pVL->var+kVarTrgtLen ;
    strncpy ( pVar->name, "hip_target_egLen", LEN_VARNAME ) ;;
  }
  pVar->cat = add ;  // JDM: = cat ; to prevent writing.
  pVar->flag = 1 ;  // JDM = 0 ; to avoid writing.
  pVar->isVec = 0 ;
  //strncpy ( pVar->grp, "Other", LEN_VARNAME ) ;
  strncpy ( pVar->grp, "Additionals", LEN_VARNAME ) ;

  // Calculate current metric, i.e. edge len average
  double domainVol =  compute_vrtxVol ( pUns, kVarTrgtLen ) ;
  edgeLen_from_vol ( pUns, kVarTrgtLen, pUns->mDim ) ;

  // Compute edge lengths directly, but loop over elements, hence
  // interior edges are added more often to the average than
  // boundary edges.
  calc_edgeLen ( pUns, kVarTrgtLen, "avg" ) ;

  double lenMin, lenMax ;
  if ( mmgMethod == isoFac ) {
    // Uniform refinement.
    mult_uns_var_scal ( pUns, kVarTrgtLen, isoFactor, kVarTrgtLen, &lenMin, &lenMax ) ;
  }
  else {
    // Refinement with field.
    mult_uns_var_var ( pUns, kVarTrgtLen, kVarFactor, kVarTrgtLen, &lenMin, &lenMax ) ;
  }
  //set_uns_var ( pUns, kVarTrgtLen, 0.01 ) ;  // JDM test




  /* Declare zones:
     iZnF: fixed, non-adapted, e.g. hybrid grid
     iZnL: L
     iZnC: central/core
     iZnU: U */
  int doWarnDupl = 1 ;
  int iZnF = zone_add ( pUns, "fixed", 0, doWarnDupl ) ;
  zone_elem_mod_type ( pUns, iZnF, pyr, hex ) ;


  int iZnL = zone_add ( pUns, "L", 0, doWarnDupl ) ;
  int iZnC = zone_add ( pUns, "C", 0, doWarnDupl ) ;
  int iZnU = zone_add ( pUns, "U", 0, doWarnDupl ) ;

  int iZnLU[2] ;
  iZnLU[0] = iZnL, iZnLU[1] = iZnU ;
  int mBcPer = 0 ; // JDM: bc numbering can change in partial grids of some zones. Replace with mBcPerMmg
  int nrBcPer[2*MAX_PER_PATCH_PAIRS] ; // is m/nrBcPer needed here/otherwise?
  zone_elem_mod_perBcLayer ( pUns, iZnLU, mmg_mPerLayer, tet, tet,
                             &mBcPer, nrBcPer ) ;

  /* Zone the remaining core. */
  zone_elem_mod_remaining ( pUns, iZnC ) ;

  /* Set vx->mark3 for all fixed nodes on a zone interface,
     assign elems to 4 zones, count. */
  int mZ = 0 ;
  int iZone[4] ; // We don't need more than 4?
  iZone[mZ++] = iZnF ;
  iZone[mZ++] = iZnL ;
  iZone[mZ++] = iZnC ;
  iZone[mZ++] = iZnU ;
  const int doReset = 1, dontPer = 0 ;
  mark3_vx_zones_per ( pUns, mZ, iZone, doReset, dontPer ) ;


  /* Copy parts into a new grid, such that old grid can be used for interpolation. */
  grid_struct *pGrid2 = make_grid () ;
  uns_s *pUns2 = make_uns ( pGrid2 ) ;

  /* Copy key parameters such as hMin. */
  pUns2->mDim = pUns->mDim ;
  pGrid2->uns.mDim = pUns->mDim ;
  pUns2->hMin = pUns->hMin ;
  pUns2->hMax = pUns->hMax ;
  pUns2->epsOverlap = pUns->epsOverlap ;
  pUns2->epsOverlapSq = pUns->epsOverlapSq ;
  pUns2->specialTopo = pUns->specialTopo ;
  snprintf ( pUns2->pGrid->uns.name, TEXT_LEN-1,"%s_lper_zone", pUns->pGrid->uns.name ) ;

  /* Copy boundary list. Keep same order as mmg uses integer refs.
     Note: renumbering overwrites pUns->ppBc, possibly with different
     numbering as not all bcs are present. Save a separate copy. */
  const int mBc = pUns->mBc ;
  bc_struct **ppBc = arr_malloc ( "ppBc in adapt_mmg3d_per", pUns2->pFam,
                                  mBc, sizeof(*ppBc) ) ;
  memcpy ( ppBc, pUns->ppBc, mBc*sizeof(*ppBc)) ;

  /* Copy periodic setup into pUns2. */
  pUns2->mPerBcPairs = pUns->mPerBcPairs ;
  pUns2->pPerBc = arr_malloc ( "pUns2->pPerBc in adapt_mmg3d_per", pUns2->pFam,
                               pUns->mPerBcPairs, sizeof(*(pUns2->pPerBc)) ) ;
  memcpy ( pUns2->pPerBc, pUns->pPerBc,  pUns->mPerBcPairs*sizeof(*(pUns->pPerBc)) )  ;

  /* Copy the list of zones.*/
  zone_copy_all ( pUns, pUns2 ) ;
  // Add a new zone identifier for the zone u to be rotated.
  int iZnUprime = zone_add ( pUns2, "Uprime", 0, doWarnDupl ) ;

  const int dontRenameDuplPerBc = 0, dontUnknown = 0, doMerge = 1, dontUseMark=0 ;
  pUns2 = ucopy_elem_region ( pUns, iZnL, pUns2, iZnL, dontRenameDuplPerBc, dontUnknown,
                              dontUseMark ) ;

  /* Duplicate u zone to become u', rotate, attach to l, retaining
     now interior faces. */
  ucopy_per_part ( pUns, iZnU, pUns2, iZnUprime, pUns->pPerBc, -1,
                   dontUnknown, dontUseMark ) ;



  /* We need to update epsOverlap, currently this also computes elem vols.
     To do: create a hMin only variant. */
  check_vol ( pUns2 ) ;
  pUns2->epsOverlap = .9*pUns2->hMin ;
  pUns2->epsOverlapSq = pUns2->epsOverlap*pUns2->epsOverlap ;

  mZ = 0 ;
  iZone[mZ++] = iZnL ;
  iZone[mZ++] = iZnUprime ;
  zone_merge ( pUns2, mZ, iZone, doReset ) ;


  if ( savemsh ) {
    /* if writing out, renumber vertices. Otherwise done in mmg_put_mesh_3d_zones. */
    validate_uns_vertFromElem ( pUns2, 1 ) ;
    increment_uns_vert_number ( pUns2, doReset ) ;
    //write_hdf5 ( "hdfd uprime" ) ;
    write_gmsh_uns ( pUns2, "UprimL", 0 ) ;
  }

  /* Init the mmg mesh. */
  MMG5_pMesh pMMesh ;
  MMG5_pSol pMMet ;
  // mmg_init ( pUns, &pMMesh, &pMMet, hGrad, hausdorff,
  //           isoFactor, mmg_hMin, mmg_hMax ) ;
  /* copy u', l zones to mmg.*/
  ulong_t mTetVx ;
  int freezePer = 0 ;
  int freezeZoneInterFc = 1 ;
  /* bc numbering can change in partial grids of some zones.
     Track what is used for this part. JDM: use bc->nr, needs updating ppBc, not this list.*/
  int mBcPerMmg = 0 ;
  int nrBcPerMmg[2*MAX_PER_PATCH_PAIRS] ;
  if ( mmg_put_mesh_3d_zones    // Copy for first adaptation. Fixed vx have mark3.
       ( pUns2, &pMMesh, &pMMet,
         hGrad, hausdorff, isoFactor, mmg_hMin, mmg_hMax,
         pUns->pGrid,
         freezePer, freezeZoneInterFc, mZ, iZone,
         &mBcPerMmg, nrBcPerMmg ) )
    hip_err ( fatal, 0, "failed to put u'l mesh to mmg3d in adapt_mmg3d_per." ) ;




  /* Define sizes for a solution/metric field and allocate. */
  if ( !MMG3D_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after MG3D_Set_solSize in adapt_mmg3d_per" ) ;
  if ( !MMG3D_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after MMG3D_Chk_meshData in adapt_mmg3d_per" ) ;

  // Initialise metric with average neighbor cell edge length
  //status = MMG3D_doSol ( pMMesh, pMMet );
  status = MMG3D_doSol_iso ( pMMesh, pMMet );
  if ( !status ) {
    sprintf ( hip_msg, "failed in adapt_mmg3d_per with call to MMG3D_doSol with status %d", status ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Refinement metrics. Note that users supply a scaling value to hip,
     while mmg stores the edge length.
     Eglen has been calculated for the original mesh, scaled stored in kVarTrgtLen
     for the second pass. */
  if ( !mmg_egLen_from_var ( pUns2, pMMesh, pMMet,
                             kVarTrgtLen,
                             mmg_hMin, mmg_hMax ) ) {
    mmg_free_all ( &pMMesh, &pMMet ) ;
    return (1) ;
  }




  char flNm[LINE_LEN] ;
  if  ( savemsh == 1 ) {
    /* 3D only. */
    strcpy ( flNm, "orig_upriml_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }

  /* Display MMG parameters */
  status = MMG3D_defaultValues( pMMesh) ;

  /* adapt mmg: first pass around periodic interface u'-l. */
  clock_t start_t  = clock () ;
  status = MMG3D_mmg3dlib ( pMMesh, pMMet ) ;
  if ( status ) hip_err ( fatal, 0, "3D periodic Mesh adaptation failed");
  clock_t end_t = clock () ;
  clock_t total_t = (double)(end_t - start_t)  / CLOCKS_PER_SEC;
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "MMG3D periodic adaptation time %g s\n", total_t*1.);
    hip_err ( info, 1, hip_msg ) ;
  }



  // Save MMG format
  if  ( savemsh == 1 ) {
    sprintf ( flNm, "adapt_upriml_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }


  // Erase old zone u from pUns, adapted grid re-uses/overwrites iZnL.
  zone_elem_invalidate ( pUns2, iZnUprime ) ;
  zone_elem_invalidate ( pUns2, iZnL ) ;


  /* Copy back to hip. Note, all u,l bcs are interior here.
     Note: bc numbering of partial grids can differ, use n/mBcPerMmg here. */
  const int dontCheck = 0, dontUseElMark = 0 ;
  llVxEnt_s *pllVxTriFc = NULL ;
  fc2el_s *pFc2El = NULL ;
  pUns2 = mmg_get_mesh_3d_per ( pMMesh, pUns2, pllVxTriFc, pFc2El,
                                pUns2->mBc, pUns2->ppBc,
                                iZnL, iZnUprime,
                                mBcPer, nrBcPer,
                                pUns2, dontCheck, dontUseElMark ) ;
  ulong_t mNegVols ;
  update_h_vol ( pUns2, &mNegVols ) ;
  pUns2->epsOverlap = .9*pUns2->hMin ;
  pUns2->epsOverlapSq = pUns2->epsOverlap*pUns2->epsOverlap ;


  if ( savemsh ) {
    check_uns ( pUns2, 5 ) ;
    write_gmsh_uns ( pUns2, "upriml_adapt", 0 ) ;
  }
  mmg_free_all ( &pMMesh, &pMMet ) ;


  // Copy  u' back to u. Adjust args for second pass. */
  ucopy_per_part ( pUns2, iZnUprime, pUns2, iZnU, pUns2->pPerBc, 1,
                   dontUnknown, dontUseMark ) ;
  zone_elem_invalidate ( pUns2, iZnUprime ) ;


  /* Add C */
  pUns2 = ucopy_elem_region ( pUns, iZnC, pUns2, iZnC,
                              dontRenameDuplPerBc, dontUnknown, dontUseMark ) ;
  number_uns_grid ( pUns2 ) ; // is this needed?

  mZ = 0 ;
  iZone[mZ++] = iZnL ;
  iZone[mZ++] = iZnC ;
  iZone[mZ++] = iZnU ;
  zone_merge ( pUns2, mZ, iZone, doReset ) ;
  //number_uns_grid ( pUns2 ) ;

  // Interpolate solution to pUns2, to retain scaling.
  int isAxi = ( axiX <= pUns->specialTopo && pUns->specialTopo <= axiZ ? 1 : 0 ) ;
  number_uns_grid ( pUns ) ; // Need all elems numbered in the initial grid for interpol.

  if ( savemsh ) {
    check_uns ( pUns2, 5 ) ;
    write_gmsh_uns ( pUns2, "u_adapt_backrot", 0 ) ;
  }

  uns_interpolate ( pUns, pUns2, isAxi ) ;



  /***************************************
   **************************************/
  // Start second adaptation.

  /* Mark3 ( i.e. fix ) all nodes on the interface to the hybrid patch,
     and all periodic bcs. */
  mZ = 0 ;
  iZone[mZ++] = iZnF ;
  const int doPer = 1 ;
  mark3_vx_zones_per ( pUns2, mZ, iZone, doReset, doPer ) ;
  //reset_vx_mark3 ( pUns2 ) ; // JDM Test.

  // Set the zones to adapt:
  mZ = 0 ;
  iZone[mZ++] = iZnU ;
  iZone[mZ++] = iZnC ;
  iZone[mZ++] = iZnL ;
  /* Copy the three tet zones from pUns to mmg,
     freezing the periodic and hybrid/fixed boundaries. */
  freezePer = 1 ;
  freezeZoneInterFc = 0 ;
  if ( mmg_put_mesh_3d_zones
       ( pUns2, &pMMesh, &pMMet,
         hGrad, hausdorff, isoFactor, mmg_hMin, mmg_hMax,
         pUns2->pGrid,
         freezePer, freezeZoneInterFc, mZ, iZone,
         &mBcPerMmg, nrBcPerMmg ) )
    hip_err ( fatal, 0, "failed to put luc mesh to mmg3d in adapt_mmg3d_per." ) ;
  release_vx_markN ( pUns2, 2 ) ;




  /* Define sizes for a solution/metric field and allocate. */
  if ( !MMG3D_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after second MG3D_Set_solSize in adapt_mmg3d_pre" ) ;
  if ( !MMG3D_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after second MMG3D_Chk_meshData in adapt_mmg3d_pre" ) ;

  // Initialise metric with average neighbor cell edge length
  // status = MMG3D_doSol ( pMMesh, pMMet );
  status = MMG3D_doSol_iso ( pMMesh, pMMet );
  if ( !status ) {
    sprintf ( hip_msg, "failed in second adapt_mmg3d_per with call to MMG3D_doSol with status %d", status ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  // Refinement metrics.
  if ( !mmg_egLen_from_var ( pUns2, pMMesh, pMMet,
                             kVarTrgtLen,
                             mmg_hMin, mmg_hMax ) ) {
    mmg_free_all ( &pMMesh, &pMMet ) ;
    return (3) ;
  }



  // Save MMG format
  if  ( savemsh == 1 ) {
    check_uns ( pUns2, 5 ) ;
    write_gmsh_uns ( pUns2, "adapt_pre_2nd_mesh3d", 0 ) ;


    sprintf ( flNm, "adapt_pre_2nd_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;

  }

  /* Display MMG parameters */
  status = MMG3D_defaultValues( pMMesh) ;

  start_t  = clock () ;
  status = MMG3D_mmg3dlib ( pMMesh, pMMet ) ;
  if ( status ) hip_err ( fatal, 0, "3D periodic Mesh adaptation failed");
  end_t = clock () ;
  total_t = (double)(end_t - start_t)  / CLOCKS_PER_SEC;
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "MMG3D periodic adaptation time %g s", total_t*1.);
    hip_err ( info, 1, hip_msg ) ;
  }


  // Save MMG format
  if  ( savemsh == 1 ) {
    sprintf ( flNm, "adapt_post_2nd_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }



  /* Copy parts into a new grid, such that old grid can be used for interpolation. */
  grid_struct *pGrid3 = make_grid () ;
  uns_s *pUns3 = make_uns ( pGrid3 ) ;
  pUns3->mDim = pUns->mDim ;
  pGrid3->uns.mDim = pUns->mDim ;
  pUns3->specialTopo = pUns->specialTopo ;
  snprintf ( pUns3->pGrid->uns.name, TEXT_LEN-1, "%s_adapted", pUns->pGrid->uns.name ) ;

  // Copy periodic setup. Keep same order as mmg uses integer refs.
  pUns3->mPerBcPairs = pUns->mPerBcPairs ;

  pUns3->pPerBc = arr_malloc ( "pUns3->pPerBc in adapt_mmg3d_per", pUns3->pFam,
                               pUns->mPerBcPairs, sizeof(*(pUns3->pPerBc)) ) ;
  memcpy ( pUns3->pPerBc, pUns->pPerBc,  pUns->mPerBcPairs*sizeof(*(pUns->pPerBc)) )  ;


  /* Copy adapted l,c,u mesh back to hip. */
  const int doCheck=1 ;
  pUns3 = mmg_get_mesh_3d_per ( pMMesh, pUns, pllVxTriFc, pFc2El,
                                mBc, ppBc, iZnL, iZnU, 0, nrBcPer, pUns3,
                                doCheck, dontUseElMark ) ;

  mmg_free_all ( &pMMesh, &pMMet ) ;


  /* Free. */
  free_llEnt ( &pllVxTriFc ) ;


  // Add the hyb part.


  if ( interpolateSol && pUns->varList.mUnknowns )
    uns_interpolate ( pUns, pUns3, 0 ) ;


  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid3 ;



  return (0) ;
}





/******************************************************************************
  adapt_mmg3d_per_mark:   */

/*! adapt the tet part of a grid with periodic boundaries using mmg3d.
 *  Tag regions to adapt with markdEdges, rather than iZone.
 */

/*

  Last update:
  ------------
  25apr23; move mgmt of vx->mark,mark2 from here into mark3_vx_elem_mark_per
  25Apr23; fix bug with numbering vx->mark3,
           marks number from 0, hence mark3=id2
  22Dec21; derived from adapt_mmg_3d_per


  Input:
  ------
  pUns
  argLine

  Changes To:
  -----------

  Returns:
  --------
  1 on failure, 0 on success

*/
int adapt_mmg3d_per_mark ( uns_s *pUns, char* argLine ) {
#undef FUNLOC
#define FUNLOC "in adapt_mmg3d_per_mark"

  /* Make sure the metric is periodic. */
  special_verts ( pUns ) ;


  if ( pUns->mElemsOfType[pyr] || pUns->mElemsOfType[pri] || pUns->mElemsOfType[hex] ) {
    hip_err ( warning, 1, "hybrid grids with periodicity can currently not be\n"
              "      periodically adapted. Come back later.\n" ) ;
    return ( 2 ) ;
  }

  // What to do next?
  mmgMethod_e mmgMethod ;
  int status;
  double isoFactor ;
  int kVarFactor ; // 0 ... mUnknowns-1
  int savemsh ;
  double hGrad ;
  double hausdorff ;
  int interpolateSol ;
  double mmg_hMin, mmg_hMax ;
  int mmg_mPerLayer = DEFAULT_mmg_mPerLayer ;
  int mZones=0, iZone[MAX_ZONES]={0} ; // not used yet
  if ( !mmg_args ( argLine, &mmgMethod, &isoFactor, &kVarFactor, &hGrad,
                   &hausdorff, &interpolateSol, &mmg_hMin, &mmg_hMax,
                   &mmg_mPerLayer, &savemsh, pUns, &mZones, iZone ) )
    return (0) ;




  // add a var field for a size metric (not a scaling metric.)
  // store as an unkonwn, to use standard interpolation tools for 2nd pass.
  varList_s *pVL = &pUns->varList ;
  var_s *pVar = find_var_name ( pVL, NULL, "hip_target_egLen" ) ;
  int kVarTrgtLen = -1 ;

  /* Does hip_target_egLen exist?  */
  if ( pVar ) {
    // It does, reuse.
    hip_err ( warning, 2, "internal solution field `hip_target_egLen' exists already\n"
              "            will be reused and overwritten.\n" ) ;
    kVarTrgtLen = pVar - pVL->var ;
  }
  else {
    // It doesn't, add.
    int mUnSol = pVL->mUnknowns++ ;
    kVarTrgtLen = mUnSol ; // New variable to store the target edge length.
    realloc_unknowns ( pUns, mUnSol, mUnSol+1 ) ;
    if ( pUns->varList.varType == noVar ) pUns->varList.varType = noType ;
    pVar = pVL->var+kVarTrgtLen ;
    strncpy ( pVar->name, "hip_target_egLen", LEN_VARNAME ) ;
  }
  pVar->cat = add ;  // JDM: = cat ; to prevent writing. But is now written anyway.
  pVar->flag = 1 ;  // JDM = 0 ; to avoid writing.
  pVar->isVec = 0 ;
  strncpy ( pVar->grp, "Additionals", LEN_VARNAME ) ;

  // Calculate current metric, i.e. edge len average
  double domainVol =  compute_vrtxVol ( pUns, kVarTrgtLen ) ;
  edgeLen_from_vol ( pUns, kVarTrgtLen, pUns->mDim ) ;

  // Compute edge lengths directly, but loop over elements, hence
  // interior edges are added more often to the average than
  // boundary edges.
  calc_edgeLen ( pUns, kVarTrgtLen, "avg" ) ;

  double lenMin, lenMax ;
  if ( mmgMethod == isoFac ) {
    // Uniform refinement.
    mult_uns_var_scal ( pUns, kVarTrgtLen, isoFactor, kVarTrgtLen, &lenMin, &lenMax ) ;
  }
  else {
    // Refinement with field.
    mult_uns_var_var ( pUns, kVarTrgtLen, kVarFactor, kVarTrgtLen, &lenMin, &lenMax ) ;
  }
  //set_uns_var ( pUns, kVarTrgtLen, 0.01 ) ;  // JDM test




  /* Declare regions for adaptation:
     iMarkF: fixed, non-adapted, e.g. hybrid grid
     iMarkL: L
     iMarkC: central/core
     iMarkU: U */
  int kMarkF, kMarkL, kMarkC, kMarkU ;
  kMarkF = reserve_next_elem_mark( pUns, FUNLOC ) ;
  kMarkL = reserve_next_elem_mark( pUns, FUNLOC ) ;
  kMarkC = reserve_next_elem_mark( pUns, FUNLOC ) ;
  kMarkU = reserve_next_elem_mark( pUns, FUNLOC ) ;

  // Mark all fixed elems, e.g. hex, pyr, pri.
  reset_all_elem_mark ( pUns, kMarkF ) ;
  mark_elem_type ( pUns, kMarkF, pyr, hex ) ;
  

  // iMarkLU[0] = iMarkL, iMarkLU[1] = iMarkU ;
  int kElMarkLU[2] = { kMarkL, kMarkU } ;
  int mBcPer = 0 ; // JDM: bc numbering can change in partial grids of some zones. Replace with mBcPerMmg
  int nrBcPer[2*MAX_PER_PATCH_PAIRS] ; // is m/nrBcPer needed here/otherwise?
  //zone_elem_mod_perBcLayer ( pUns, iMarkLU, mmg_mPerLayer, tet, tet,
  //                           &mBcPer, nrBcPer ) ;
  mark_elem_perBcLayer ( pUns, kElMarkLU, mmg_mPerLayer, tet, tet, &mBcPer, nrBcPer ) ;

  /* Zone the remaining core.
     zone_elem_mod_remaining ( pUns, iMarkC ) ; */
  // Create a mask of flags for zones F,U,L.
  int kMarksUsed[SZ_ELEM_MARK] = {0} ;
  kMarksUsed[kMarkF] = 1 ;
  kMarksUsed[kMarkL] = 1 ;
  kMarksUsed[kMarkU] = 1 ;
  unsigned int kMaskFalse = i32_packNi ( SZ_ELEM_MARK, kMarksUsed ) ;
  // Mark any non-marked element.
  unsigned int kMaskTrue = 0 ;
  mark_elem_remaining ( pUns, kMaskTrue, kMaskFalse, tet, tet, kMarkC ) ;


  /* Set vx->mark3 for all fixed nodes on a zone interface,
     Note: marks are mark, mark2, mark3, so mark3 = id 2.*/
  int kVx2Mark = 3 ;
  reserve_vx_markN ( pUns, kVx2Mark, "FUNLOC" ) ;

  int kMark[SZ_ELEM_MARK] = {0}, mMark=0 ;
  kMark[mMark++] = kMarkF ;
  kMark[mMark++] = kMarkL ;
  kMark[mMark++] = kMarkC ;
  kMark[mMark++] = kMarkU ;


  const int doReset = 1, dontPer = 0 ;
  mark3_vx_elem_mark_per ( pUns, mMark, kMark, doReset, dontPer ) ;


  /* Copy parts into a new grid, such that old grid can be used for interpolation. */
  grid_struct *pGrid2 = make_grid () ;
  uns_s *pUns2 = make_uns ( pGrid2 ) ;

  /* Copy key parameters such as hMin. */
  pUns2->hMin = pUns->hMin ;
  pUns2->hMax = pUns->hMax ;
  pUns2->epsOverlap = pUns->epsOverlap ;
  pUns2->epsOverlapSq = pUns->epsOverlapSq ;

  /* Copy boundary list. Keep same order as mmg uses integer refs.
     Note: renumbering overwrites pUns->ppBc, possibly with different
     numbering as not all bcs are present. Save a separate copy. */
  const int mBc = pUns->mBc ;
  bc_struct **ppBc = arr_malloc ( "ppBc in adapt_mmg3d_per_mark", pUns2->pFam,
                                  mBc, sizeof(*ppBc) ) ;
  memcpy ( ppBc, pUns->ppBc, mBc*sizeof(*ppBc)) ;

  /* Copy periodic setup into pUns2. */
  pUns2->mPerBcPairs = pUns->mPerBcPairs ;
  pUns2->pPerBc = arr_malloc ( "pUns2->pPerBc in adapt_mmg3d_per_mark", pUns2->pFam,
                               pUns->mPerBcPairs, sizeof(*(pUns2->pPerBc)) ) ;
  memcpy ( pUns2->pPerBc, pUns->pPerBc,  pUns->mPerBcPairs*sizeof(*(pUns->pPerBc)) )  ;

  /* Copy the list of zones. */
  zone_copy_all ( pUns, pUns2 ) ;


  const int dontRenameDuplPerBc = 0, dontUnknown = 0, doMerge = 1 ;
  const int useMark = 1 ;
  pUns2 = ucopy_elem_region ( pUns, kMarkL, pUns2, kMarkL, dontRenameDuplPerBc,
                              dontUnknown, useMark ) ;

  /* Duplicate u zone to become u', rotate, attach to l, retaining
     now interior faces. */
  int kMarkUprime = reserve_next_elem_mark( pUns, FUNLOC ) ;
  /* Copy elem mark use. */
  memcpy( pUns2->useElemMark, pUns->useElemMark, sizeof(pUns->useElemMark) ) ;

  ucopy_per_part ( pUns, kMarkU, pUns2, kMarkUprime, pUns->pPerBc, -1,
                   dontUnknown, useMark ) ;



  /* We need to update epsOverlap, currently this also computes elem vols.
     To do: create a hMin only variant. */
  check_vol ( pUns2 ) ;
  pUns2->epsOverlap = .9*pUns2->hMin ;
  pUns2->epsOverlapSq = pUns2->epsOverlap*pUns2->epsOverlap ;

  int mReg = 0 ;
  int iReg[2] ;
  iReg[mReg++] = kMarkL ;
  iReg[mReg++] = kMarkUprime ;
  merge_regions ( pUns2, mReg, iReg, doReset ) ;


  if ( savemsh ) {
    /* if writing out, renumber vertices. Otherwise done in mmg_put_mesh_3d_zones. */
    validate_uns_vertFromElem ( pUns2, 1 ) ;
    increment_uns_vert_number ( pUns2, doReset ) ;
    // write_hdf5 ( "hdfd uprime" ) ;
    write_gmsh_uns ( pUns2, "UprimL", 0 ) ;
  }

  /* Init the mmg mesh. */
  MMG5_pMesh pMMesh ;
  MMG5_pSol pMMet ;
  // mmg_init ( pUns, &pMMesh, &pMMet, hGrad, hausdorff,
  //           isoFactor, mmg_hMin, mmg_hMax ) ;
  /* copy u', l zones to mmg.*/
  ulong_t mTetVx ;
  int freezePer = 0 ;
  int freezeZoneInterFc = 1 ;
  /* bc numbering can change in partial grids of some zones.
     Track what is used for this part. JDM: use bc->nr, needs updating ppBc, not this list.*/
  int mBcPerMmg = 0 ;
  int nrBcPerMmg[2*MAX_PER_PATCH_PAIRS] ;
  if ( mmg_put_mesh_3d_region_zones    // Copy for first adaptation. Fixed vx have mark3.
       ( pUns2, &pMMesh, &pMMet,
         hGrad, hausdorff, isoFactor, mmg_hMin, mmg_hMax,
         pUns->pGrid,
         freezePer, freezeZoneInterFc, mReg, iReg, mZones, iZone,
         &mBcPerMmg, nrBcPerMmg ) )
    hip_err ( fatal, 0, "failed to put u'l mesh to mmg3d in adapt_mmg3d_per_mark." ) ;




  /* Define sizes for a solution/metric field and allocate. */
  if ( !MMG3D_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after MG3D_Set_solSize in adapt_mmg3d_pre" ) ;
  if ( !MMG3D_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after MMG3D_Chk_meshData in adapt_mmg3d_pre" ) ;

  // Initialise metric with average neighbor cell edge length
  // status = MMG3D_doSol ( pMMesh, pMMet );
  status = MMG3D_doSol_iso ( pMMesh, pMMet );
  if ( !status ) {
    sprintf ( hip_msg, "failed in adapt_mmg3d_per_mark with call to MMG3D_doSol with status %d", status ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }


  /* Refinement metrics. Note that users supply a scaling value to hip,
     while mmg stores the edge length.
     Eglen has been calculated for the original mesh, scaled stored in kVarTrgtLen
     for the second pass. */
  if ( !mmg_egLen_from_var ( pUns2, pMMesh, pMMet,
                             kVarTrgtLen,
                             mmg_hMin, mmg_hMax ) ) {
    mmg_free_all ( &pMMesh, &pMMet ) ;
    return (1) ;
  }




  char flNm[LINE_LEN] ;
  if  ( savemsh == 1 ) {
    /* 3D only. */
    strcpy ( flNm, "orig_upriml_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }

  /* Display MMG parameters */
  status = MMG3D_defaultValues( pMMesh) ;


  /* adapt mmg: first pass around periodic interface u'-l. */
  clock_t start_t  = clock () ;
  status = MMG3D_mmg3dlib ( pMMesh, pMMet ) ;
  if ( status ) hip_err ( fatal, 0, "3D periodic Mesh adaptation failed");
  clock_t end_t = clock () ;
  clock_t total_t = (double)(end_t - start_t)  / CLOCKS_PER_SEC;
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "MMG3D periodic adaptation time %g s\n", total_t*1.);
    hip_err ( info, 1, hip_msg ) ;
  }



  // Save MMG format
  if  ( savemsh == 1 ) {
    sprintf ( flNm, "adapt_upriml_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }


  // Erase old zone u from pUns, adapted grid re-uses/overwrites iMarkL.
  elem_invalidate_mark ( pUns2, kMarkUprime ) ;
  elem_invalidate_mark ( pUns2, kMarkL ) ;


  /* Copy back to hip. Note, all u,l bcs are interior here.
     Note: bc numbering of partial grids can differ, use n/mBcPerMmg here. */
  const int dontCheck = 0, doUseElMark=1 ;
  llVxEnt_s *pllVxTriFc = NULL ;
  fc2el_s *pFc2El = NULL ;
  pUns2 = mmg_get_mesh_3d_per ( pMMesh, pUns2, pllVxTriFc, pFc2El,
                                pUns2->mBc, pUns2->ppBc,
                                kMarkL, kMarkUprime,
                                mBcPer, nrBcPer,
                                pUns2, dontCheck, doUseElMark ) ;
  ulong_t mNegVols ;
  update_h_vol ( pUns2, &mNegVols ) ;
  pUns2->epsOverlap = .9*pUns2->hMin ;
  pUns2->epsOverlapSq = pUns2->epsOverlap*pUns2->epsOverlap ;


  if ( savemsh ) {
    check_uns ( pUns2, 5 ) ;
    write_gmsh_uns ( pUns2, "upriml_adapt", 0 ) ;
  }
  mmg_free_all ( &pMMesh, &pMMet ) ;


  // Copy  u' back to u. Adjust args for second pass. */
  ucopy_per_part ( pUns2, kMarkUprime, pUns2, kMarkU, pUns2->pPerBc, 1,
                   dontUnknown, useMark ) ;
  elem_invalidate_mark ( pUns2, kMarkUprime ) ;


  /* Add C */
  pUns2 = ucopy_elem_region ( pUns, kMarkC, pUns2, kMarkC,
                              dontRenameDuplPerBc, dontUnknown, useMark ) ;
  number_uns_grid ( pUns2 ) ; // is this needed?

  mMark = 0 ;
  kMark[mMark++] = kMarkL ;
  kMark[mMark++] = kMarkC ;
  kMark[mMark++] = kMarkU ;
  zone_region_merge ( pUns2, mMark, kMark, mZones, iZone, doReset ) ;
  //number_uns_grid ( pUns2 ) ;

  // Interpolate solution to pUns2, to retain scaling.
  int isAxi = ( axiX <= pUns->specialTopo && pUns->specialTopo <= axiZ ? 1 : 0 ) ;
  number_uns_grid ( pUns ) ; // Need all elems numbered in the initial grid for interpol.

  if ( savemsh ) {
    check_uns ( pUns2, 5 ) ;
    write_gmsh_uns ( pUns2, "u_adapt_backrot", 0 ) ;
    uns_s *pUnsC = Grids.PcurrentGrid->uns.pUns ;
    write_hdf5 ( "hdfd u_adapt_backrot" ) ;
    Grids.PcurrentGrid->uns.pUns  = pUnsC ;
  }

  uns_interpolate ( pUns, pUns2, isAxi ) ;



  /***************************************
   **************************************/
  // Start second adaptation.

  /* Mark3 ( i.e. fix ) all nodes on the interface to the hybrid patch,
     and all periodic bcs. */
  mMark = 0 ;
  iReg[mMark++] = kMarkF ;
  const int doPer = 1 ;
  mark3_vx_elem_mark_per ( pUns2, mMark, iReg, doReset, doPer ) ;
  //reset_vx_mark3 ( pUns2 ) ; // JDM Test.

  // Set the zones to adapt:
  mMark = 0 ;
  kMark[mMark++] = kMarkU ;
  kMark[mMark++] = kMarkC ;
  kMark[mMark++] = kMarkL ;
  /* Copy the three tet zones from pUns to mmg,
     freezing the periodic and hybrid/fixed boundaries. */
  freezePer = 1 ;
  freezeZoneInterFc = 0 ;
  if ( mmg_put_mesh_3d_region_zones
       ( pUns2, &pMMesh, &pMMet,
         hGrad, hausdorff, isoFactor, mmg_hMin, mmg_hMax,
         pUns2->pGrid,
         freezePer, freezeZoneInterFc, mMark, kMark, mZones, iZone,
         &mBcPerMmg, nrBcPerMmg ) )
    hip_err ( fatal, 0, "failed to put luc mesh to mmg3d in adapt_mmg3d_per_mark." ) ;
  release_vx_markN ( pUns2, 3 ) ;




  /* Define sizes for a solution/metric field and allocate. */
  if ( !MMG3D_Set_solSize( pMMesh, pMMet, MMG5_Vertex,
                           pMMesh->np, MMG5_Scalar ) )
    hip_err ( fatal, 0, "failed after second MG3D_Set_solSize in adapt_mmg3d_per_mark" ) ;
  if ( !MMG3D_Chk_meshData( pMMesh, pMMet ) )
    hip_err ( fatal, 0, "failed after second MMG3D_Chk_meshData in adapt_mmg3d_per_mark" ) ;

  // Initialise metric with average neighbor cell edge length
  // status = MMG3D_doSol ( pMMesh, pMMet );
  status = MMG3D_doSol_iso ( pMMesh, pMMet );
  if ( !status ) {
    sprintf ( hip_msg, "failed in second adapt_mmg3d_per_mark with call to MMG3D_doSol with status %d", status ) ;
    hip_err ( fatal, 0, hip_msg ) ;
  }



  // Refinement metrics.
  if ( !mmg_egLen_from_var ( pUns2, pMMesh, pMMet,
                             kVarTrgtLen,
                             mmg_hMin, mmg_hMax ) ) {
    mmg_free_all ( &pMMesh, &pMMet ) ;
    return (3) ;
  }



  // Save MMG format
  if  ( savemsh == 1 ) {
    check_uns ( pUns2, 5 ) ;
    write_gmsh_uns ( pUns2, "adapt_pre_2nd_mesh3d", 0 ) ;


    sprintf ( flNm, "adapt_pre_2nd_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;

  }

  /* Display MMG parameters */
  status = MMG3D_defaultValues( pMMesh) ;


  start_t  = clock () ;
  status = MMG3D_mmg3dlib ( pMMesh, pMMet ) ;
  if ( status ) hip_err ( fatal, 0, "3D periodic Mesh adaptation failed");
  end_t = clock () ;
  total_t = (double)(end_t - start_t)  / CLOCKS_PER_SEC;
  if ( verbosity > 2 ) {
    sprintf ( hip_msg, "MMG3D periodic adaptation time %g s", total_t*1.);
    hip_err ( info, 1, hip_msg ) ;
  }


  // Save MMG format
  if  ( savemsh == 1 ) {
    sprintf ( flNm, "adapt_post_2nd_mesh3d" ) ;
    mmg_write_mesh3d ( pMMesh, pMMet, flNm ) ;
  }



  /* Copy parts into a new grid, such that old grid can be used for interpolation. */
  grid_struct *pGrid3 = make_grid () ;
  uns_s *pUns3 = make_uns ( pGrid3 ) ;

  // Copy periodic setup. Keep same order as mmg uses integer refs.
  pUns3->mPerBcPairs = pUns->mPerBcPairs ;

  pUns3->pPerBc = arr_malloc ( "pUns3->pPerBc in adapt_mmg3d_per_mark", pUns3->pFam,
                               pUns->mPerBcPairs, sizeof(*(pUns3->pPerBc)) ) ;
  memcpy ( pUns3->pPerBc, pUns->pPerBc,  pUns->mPerBcPairs*sizeof(*(pUns->pPerBc)) )  ;


  /* Copy adapted l,c,u mesh back to hip. */
  const int doCheck=1 ;
  pUns3 = mmg_get_mesh_3d_per ( pMMesh, pUns, pllVxTriFc, pFc2El,
                                mBc, ppBc, kMarkL, kMarkU, 0, nrBcPer, pUns3,
                                doCheck, doUseElMark ) ;

  mmg_free_all ( &pMMesh, &pMMet ) ;


  /* Free. */
  free_llEnt ( &pllVxTriFc ) ;


  // Add the hyb part.


  if ( interpolateSol && pUns->varList.mUnknowns )
    uns_interpolate ( pUns, pUns3, 0 ) ;


  /* Make this grid the current one. */
  Grids.PcurrentGrid = pGrid3 ;



  return (0) ;
}



/******************************************************************************
  adapt_mmg_3d:   */

/*! adapt the tet part of a grid with mmg3d.
 *
 */

/*

  Last update:
  ------------
  5Aug19; use Grids.adapt.doPer.
  4Apr19; fix bug with periodic/non-per switch for 2D.
  4Sep18; new wrapper with old name.


  Input:
  ------
  pGrid
  argLine

  Returns:
  --------
  1 on failure, 0 on success

*/
void adapt_mmg ( grid_struct *pGrid, char *argLine ) {

  if ( !pGrid ) {
    /* No grid present. */
    sprintf ( hip_msg, "There is no grid to remesh." ) ;
    hip_err ( warning, 0, hip_msg ) ;
    return ;
  }

  else if ( pGrid->uns.type != uns ) {
    sprintf ( hip_msg,
              "Can only remesh unstructured grids. Copy to uns first." ) ;
    hip_err ( warning, 0, hip_msg ) ;
    return ;
  }

  uns_s *pUns = pGrid->uns.pUns ;

  if ( (check_bnd_setup ( pUns )).status != success ) {
    sprintf ( hip_msg, "grid has no proper boundary setup, adaptation may fail." ) ;
    hip_err ( warning, 1, hip_msg ) ;
  }

  if ( pUns->mVertsNumbered >MMG5_MAX_MVERTS  ) {
    sprintf ( hip_msg, "grid has %"FMT_ULG" nodes,"
              " the current %d-bit implementation of mmg"
              " only supports up to %g nodes.",
              pUns->mVertsNumbered, MMG5_INT_SZ, MMG5_MAX_MVERTS ) ;
    hip_err ( fatal, 1, hip_msg ) ;
  }


  if ( pUns->mDim == 3 ) {

    if ( pUns->pPerBc && Grids.adapt.doPer ) {
      // if ( adapt_mmg3d_per ( pUns, argLine ) ) {
      if ( adapt_mmg3d_per_mark ( pUns, argLine ) ) {
        sprintf ( hip_msg, "Failed to remesh this periodic 3D grid." ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
    }
    else if ( adapt_mmg3d_nonPer ( pUns, argLine ) ) {
        sprintf ( hip_msg, "Failed to remesh this non-periodic 3D grid." ) ;
        hip_err ( fatal, 0, hip_msg ) ;
      }
  }


  else if ( pGrid->uns.mDim == 2 ) {
    if ( pUns->pPerBc ) {
      hip_err ( info, 1, "periodic adaptation in 2D freezes per bcs. Contact your\n"
                "friendly hip developer if you need those adapted, as offered in 3D." ) ;
    }
    if ( adapt_mmg_2d ( pUns, argLine ) ) {
      sprintf ( hip_msg, "Failed to remesh this 2D grid." ) ;
      hip_err ( fatal, 0, hip_msg ) ;
    }
  }
  else
    hip_err ( warning, 1, "adapt_mmg expects 2D or 3D grids." ) ;

  return ;
}
