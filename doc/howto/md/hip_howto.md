# Overview

This is a document with some short examples of how to run typical workflows through hip. For more detail e.g. on parameter settings, the relevant sections in the hip manual are referenced.

For legibility, the scripts below use full command names and keywords such as 'read hdf' rather than 're hd'. All commands (and most keywords) can be abbreviated to the first two letters. 

## Examples
The examples are meant to be run on your files. Either run hip interactively and paste in the commands, or put the content from the colon up to/including exit into a file such as script.hip and run 
```
hip script.hip
```
The test files are [here](../files/).

# Reading other mesh formats

## Why use hip for this?
hip reads a wide variety of mesh formats.
hip reads the mesh and the solution for a number of formats, such as avbp v6,v7; CGNS, fluent v5,v6; and gmesh v2, v4. Most other tools such as the python library meshio do not support reading the solution.
hip allows to read part of a solution and manipulate it.


## Why not use hip for this?
If your format is not supported, let us know. If you're not the only one, we'll see what we can do.
If there is a format update of one of the main formats that is not supported, let us know and we'll include that.
hip stores unknowns at the vertices. We're considering adding some support for cell-centred solutions.
CGNS is not really a format, but rather a language how to express mesh connectivity. If the particular way your mesh is written is not understood by hip, let us know.

## Example 1, reading an unstructured grid

Find the files in the subfolder "read_unstructured_gmsh".
The following script reads an unstructured mesh and solution in gmsh, writes to AVBPs hdf format:

```
% read a grid and solution in gmsh
read gmsh ubend_gmsh.msh

% list the boundary or internal surfaces that are defined
list surfaces

% write mesh and solution to the fileset 'copiedGrid'
write hd copiedGrid

exit
```

### Output
The mesh is for a square channel with a u-bend.
![](../images/ubend.png)

```
INFO: found gmsh version 2.
```
means hip detected gmsh version 2. Make sure this matches the file format version you expect.

```
   WARNING: failed to find matching tag 7 dim 3 in gmr_find_entity.
   WARNING: failed to find matching tag 2 dim 2 in gmr_find_entity.
   WARNING: failed to find matching tag 3 dim 2 in gmr_find_entity.
   ...
```
means that your mesh file has volumetric elements ('dim 3') which reference a tag no 7, but no such tag is given. 
Similarly, tags 2-6 are missing which are referenced from surface elements, i.e. boundary faces. No need to worry, hip then supplies its own tags with those numbers. But better to include a list of tags with readable names, such as 'wall', or 'inlet.

```
Checking unstructured grid at check level 5, i.e. checking for
       consistent element connectivity (expensive!) and
       boundary setup.
```
means that hip performs all its mesh checks, in particular checking for correct connectivity. hip creates a list of all faces on elements and verifies that each face has only two neighbours, either two elements sharing that face, or an element and a boundary face.
You can influence the level of checking with 
```
set check lvl
```
where lvl is a number between 0 (no checks) and 5 (all checks, default).

If the check fails, i.e. the mesh connectivity is invalid, an error message is issued.

```    INFO: largest dihedral/face angle  91.9 deg., elem 0.
       INFO: grid seems valid.
```
shows you the largest dihedral angle, i.e. corner angle in 2D, or angle between faces sharing an edge in 3D. If the grid has valid connectivity and no negative volumes, the grid is 'valid'.

```
  Topology: noTopo
```
means that the grid has no particular topology. A particular topology would e.g. be 'annular cascade'. Some grid operations may specialise for particular topologies, you can set that after reading the mesh by 'set topology type'.

```
hip[grid_1: 1/1]> li su
   Nr: Mrk(#), geoType, bcType, order, text
    2: 0 ( 1),     bnd,      n,    2, tag2d_2
    3: 0 ( 1),     bnd,      n,    3, tag2d_3
    4: 0 ( 1),     bnd,      n,    4, tag2d_4
    5: 0 ( 1),     bnd,      n,    5, tag2d_5
    6: 0 ( 1),     bnd,      n,    6, tag2d_6
```
The list of surfaces in the mesh did not have tags provided, so hip created its own. You can rename using 'set bc text bcno tag'.

```
hip[grid_1: 1/1]> write hdf ubend-hdf
     Writing grid level 0 (0 being finest level)
      in hdf format to: ubend-hdf
             including separate boundary grid (-b)
             solution with all variables (-a)
             asciiBound in AVBP 7.X format

         writing grid in hdf5 to ubend-hdf.mesh.h5
       INFO: bc tags to ubend-hdf.asciiBound
```
writes a number of files with the root 'ubend-hdf'. Of particular interest are
```
ubend-hdf.mesh.h5
ubend-hdf.mesh.xmf
```
which are the mesh file in avbp's hdf5 format and an xdmf mapping file that can be read by paraview.


### Typical issues when reading an unstructured grid
If during the reading of your grid hip shows some of the following errors, then you will not be able to proceed:
```
WARNING: grid seems invalid due to unmatched or duplicated faces.
```
This means that the grid connectivity check has failed. There is at least one face that is unmatched (i.e. missing an element or a boundary face on the other side), or that there are more than two elements or boundary faces associated with it. You will not be able to compute on this mesh. 

You can instruct hip to convert the mismatched faces into a new boundary labelled 'hip_unmatched' by setting
```
set fc-remove 0 0 0 1
```
and then writing to your preferred mesh format.

```
WARNING: found negative volumes.
```
means what it says. These are negative cell volumes. If your solver uses the dual volume around a vertex, it may be that your nodal volume ends up positive, but this does not bode well for the stable calculation of convective or viscous fluxes. The diagnostic statement gives you minmal and maximal values for cell volumes. If you increase the verbosity of output to 5 with
```
set verbosity 5
```
hip will also tell you the nodal coordinates of that element.

## Example 2, reading a structured grid

Find the files in the subfolder "read_structured_cgns".
The following script reads a structured CGNS mesh taken from the 
[CGNS tutorials](https://cgns.github.io/CGNSFiles.html), converts from structured to unstrucured, writes to AVBPs hdf format:

The block structure is shown below:
![](../images/sqnz_s.png)

```
% read the block-structured mesh
% note that hip doesn't read adf, convert to hdf with adf2hdf if needed.
read mcgns sqnz_s.hdf.cgns

% convert the mesh to unstructured, removing duplicate nodes and#
% faces across the block interfaces
copy 2uns 

% write mesh and solution to the fileset 'copiedGrid'
write hdf copiedGrid

exit
```
### Output
```hip[0/0]> re mc sqnz_s.hdf.cgns
       INFO: 
 Reading structured cgns file.
   INFO: hMin:   0.01439, hMax:      0.15.
         read structured grid with 12 blocks, 1024 elements, 15228 nodes.
```
shows that the grid was read fine, 12 different blocks and 15228 nodes.

Conversion to unstructured means to create a single domain, removing the duplication
of nodes at block interfaces:
```
hip[0/0]> co 2uns
   Copying multiblock grid 1 to unstructured grid 2.

       INFO: removed 4220 duplicated vertices.
       INFO: removed 0 matching faces.
       INFO: removed 0 interfaces.
```
shows that the 4220 nodes were duplicate in this mesh and were removed (however as of hip 24.09, this count is not accurate, there is some double counting. 
hip retains the first instance, i.e. the sequential number with the lowest-numbered block, and throws away the rest. 

Duplicate nodes are found not by topological, but by geometric search. For this, hip uses the overarching parameter 'epsOverlap', which defines that any node falling into a sphere of radius epsOverlap is the same node. hip sets epsOverlap by default to be 80% of the smallest edge length in th mesh. 

This also shows that any degenerate elements have been correctly transformed into another primitive element, e.g. a hexahedron with a face collapesed into an edge was transformed to a prism.

```
   Checking unstructured grid at check level 5, i.e. checking for
       consistent element connectivity (expensive!) and
       boundary setup.

       INFO: largest dihedral/face angle 106.3 deg., elem 9104.
       INFO: grid seems valid.
       INFO: grid contains
          11264 cells, 
          13005 nodes, 
          3328 bnd. faces.

       INFO: hMin: 0.01439, hMax: 0.15.
       INFO: element volumes: min = 1.03536e-05, max = 0.001125:
       INFO: total grid volume: 1.82423

```
means the conversion to unstructured went fine and that the typical connectivity check for unstructured meshes was launched. That reported no issues, the resulting mesh metrics are shown.


### Typical issues when reading a structured grid


```
FATAL: no degenerate faces listed, but degenerate edges found in check_elems.
```
means that the block connectivity did not declare degeneracy, but edge lengths below epsOverlap were found.  Since there shouldn't be any, hip will stop.
Check whether the value for epsOverlap was what you expect, it could be that this was set to a value that was too large by a previous operation.

```
WARNING: new degenerate edges XXX.X (shorter than epsOverlap XXX.X) in
         the new elements to remove degeneracies in check_elems.
```
means that the fixes in dealing with degenerate edges produced new, additional degenerate edges.
Increase the verbosity to find out what the offending element is. 


## More information

at the hip prompt, run 
```
help read
```
for quick help.

See [hip manual](../../tex/hip.pdf) Sec 2 for parameters, 3.3 for reading structured grid files.


# Writing meshes to file
h!p can write your unstructured mesh in a number of file formats. 

## Why use hip for this?
Best supported is h!p's hdf format that is also used by AVBP, with extensive support for handling solutions. Next best is the unstructured CGNS format.

## Why not use hip for this?
h!p will not be able to help with your structured mesh.
CGNS support is rudimentary, and the tree structure h!p uses may not be what you expect. If you read a certain tree structure in CGNS, manipulate the mesh and/or solution, and write back to CGNS, your tree structure is likely to have been presented differently.

## Example
The sample files are in 'files/write_hdf'. Those are actually the same files as in the example for reading an unstructured grid. See there fore the meaning of the warnings.

Writing the mesh to hdf is done by
```
 <<hip[grid_1: 1/1]> write hdf copiedGrid
     Writing grid level 0 (0 being finest level)
      in hdf format to: copiedGrid
             including separate boundary grid (-b)
             solution with all variables (-a)
             asciiBound in AVBP 7.X format

         writing grid in hdf5 to copiedGrid.mesh.h5
       INFO: bc tags to copiedGrid.asciiBound
```
This produces the mesh in hdf format as 'copiedGrid.mesh.h5' and an xdmf mapping file 'copiedGrid.mesh.xmf' that can be read into paraview. 

## More information
All supported file formats with their restrictions are documented in the h!p manual under Sec. 7.
A short summary of available formats is given with the built-in help after 'help write'.


# Glueing meshes together 

## Why use hip for this?
Very few other tools offer combining different pieces of mesh.


## Why not use hip for this?
To have access to all/most tools offered by h!p, the different parts need to be conformal, i.e. each quadrilateral boundary face on the join needs to be matched by a quadrilateral faces in another part.
h!p does offer some specialist support for defining sliding plane interfaces for non-matching grids, see Sec. 7.7.5 of the manual.

## Example

The following script can be found in howto/files/merge/merge.hip.

```
% If you want your files read/written in another directory than the local one,
% adapt the path to what suits your file structure.
se pa /home/jmuller/hip/Release/doc/howto/files/merge/


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate square mesh, cut into tri, extrude to pri

% this is a unit square from 0,0 to 1,1 with 5x5 nodes
% change the 5 5 if you want a different mesh density.
ge  0 0 1 1 5 5
co q2t ll2ur

co 3d -1., 0., 4, z
set gridname prisms
li su

li gr

% write to file to visualise separately
wr hd pri555



%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate a cube hex mesh and split into tets
% this is a unit square from 0,0 to 1,1 with 5x5 nodes
% change the 5 5 if you want a different mesh density.
ge 0 0 1 1 5 5 

co 3D 0 1 4 z

co 2tet 
set gridname tets

li su

% write to file to visualise separately
wr hd box555tet



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% merge tet and prism grids.

% meshes are still in memory,
% but you could also read them back in
# re hd box555tet.mesh.h5 box555tet.asciiBound
% re hd pri555.mesh.h5 pri555.asciiBound

% set flag to remove matching faces 
set fc-remove 1 1 1


list surfaces all


% declare bcs 6 and 7 to be matching.
% chronological boundary number may be different when rereading meshes
% rather than storing.
mark match *z_eq_0


li su all

% When keeping all meshes in mem, the prism mesh is the 2nd
% when re-reading the mesh, it would be mesh 1
att 2
%att prisms

% declare periodicity on the sides
%
% se bc-ty 4 l01
% se bc-ty 2 u01
% 
% se bc-ty 1 l02
% se bc-ty 3 u02

se bc-ty left* l01
se bc-ty right* u01

se bc-ty bottom* l02
se bc-ty top* u02



li su

% se pa /home/jmuller/hip/newfeature/2016/mmg-hyb
wr hd tetpri555

ex

```
The first steps generate a 5x5 rectangular mesh in the unit square. Then the quadrilaterals in the mesh are cut into triangles with the diagnoal running from lower-left to upper-right. This mesh is then extruded with four additional slices in -1 <= z <= 0 containing only prismatic elements. That 3D grid is then given the name 'prisms'.

The final output of these steps is shown below. Note that the 2d grid is still present as 'grid_1', but no longer is of purpose here. The 3D grid is named 'prisms'. The 3D grid was the most recent to be worked upon, it has become the 'current' mesh to which all commands apply.
Default boundary names are provided by the generation and extrusion steps. The 'bottom', 'right', 'top', 'left' boundaries stem from the perimeter of the 2D mesh, the 'first', 'last' boundaries are the surfaces in z min/max from the extrusion. Note that the surface at z=0 is labelled 'last...'.
```
 <<hip[grid_2: 2/2]> set gridname prisms

 <<hip[prisms: 2/2]> li gr
        Nr Type Dim    Topo    Cells    Nodes  BndFc       Vars  Name:
         1  uns   2  noTopo       32       25     16  0*noVaTyp  grid_1
  Curr:  2  uns   3  noTopo      128      125    128  0*noVaTyp  prisms

 <<hip[prisms: 2/2]> li su
   Nr: Mrk(#), geoType, bcType, order, text
    1: 0 ( 1),     bnd,      n,    1, bottom_y_eq_0                           
    2: 0 ( 1),     bnd,      n,    2, right_x_eq_1                            
    3: 0 ( 1),     bnd,      n,    3, top_y_eq_1                              
    4: 0 ( 1),     bnd,      n,    4, left_x_eq_0                             
    5: 0 ( 1),     bnd,      n,    5, first_slice_at_z_eq_-1                  
    6: 0 ( 1),     bnd,      n,    6, last_slice_at_z_eq_0                    

 <<hip[prisms: 2/2]> 
```
The resulting prism mesh is shown below.
![](../images/pri555.png)


In the second steps another 5x5 mesh is generated for the unit square and extruded with 4 additional slices 0 <= z <= 1. The hexahedra in this mesh are then split into tetrahedra, this mesh is named 'tets'. 

The final output of this step shows that this mesh as 384 cells/elements, and is called 'tets'. As it is the most recent mesh produced, it has becomes by default the 'current' mesh, commands apply to this mesh.
The 2D mesh generation was identical, and has the identical boundary names. h!p shares boundary names globally, boundaries with the same name are considered shared. As a consequence those boundaries retain the numbers 1 to 4. This is the desired behaviour here. If boundaries are meant to remain distinct, then rename boundaries before reading a duplicate.
The boundaries that differ in name are the extrusion surfaces as their z coordinates differ. Note that the plane at z=0 is called 'first ...' in this mesh. Those boundaries are new, and hence number 7 and 8.

```

 <<hip[grid_4: 4/4]> set gridname tets
 <<hip[tets: 4/4]> li gr
        Nr Type Dim    Topo    Cells    Nodes  BndFc       Vars  Name:
         1  uns   2  noTopo       32       25     16  0*noVaTyp  grid_1
         2  uns   3  noTopo      128      125    128  0*noVaTyp  prisms
         3  uns   2  noTopo       16       25     16  0*noVaTyp  grid_3
  Curr:  4  uns   3  noTopo      384      125    192  0*noVaTyp  tets

 <<hip[tets: 4/4]> li su
   Nr: Mrk(#), geoType, bcType, order, text
    1: 0 ( 2),     bnd,      n,    1, bottom_y_eq_0        
    2: 0 ( 2),     bnd,      n,    2, right_x_eq_1         
    3: 0 ( 2),     bnd,      n,    3, top_y_eq_1           
    4: 0 ( 2),     bnd,      n,    4, left_x_eq_0          
    7: 0 ( 1),     bnd,      n,    7, first_slice_at_z_eq_0
    8: 0 ( 1),     bnd,      n,    8, last_slice_at_z_eq_1 
```
The resulting prism mesh is shown below.
![](../images/tet555.png)

The final instructions peform the merging of the two parts. The merging can either proceed with re-reading two meshes from file, see the commented statements in the script file. However, those two are already in memory, and the example proceeds directly.

The 'set fc-remove 1 1 1' statement instructs h!p to remove all matching, cut or boundary faces that become interior faces during the merge. This statement is actually redundant, as this is the default setting. Alternatively, if the user wants to retain those faces e.g. to compute some objective function over that surface, then a setting of 'set fc-remove 1 1 0' would retain those in the mesh.

The two meshes match at z=0. We could leave it to hip to find any matches and resolve duplication, matching is done by geometric proximity, please see the manual under Sec 1.3.3 on the parameter epsOverlap that governs the distance below which vertices are considered to be identical. However if there were some nodes that are part of a surface to match, but have a geometric mismatch, then those nodes would not be matched and the merged mesh would have some flat body between the matching surfaces. It is hence recommended to explicitly declare matching surfaces so h!p can verify that all vertices on those have successfully been matched. This is done with the 'mark match bcList' command, where bcList can be a list numbers and expressions. Numbers may change depending on the order in which meshes are read, so best to identify boundaries by name. In this case the expression '*z_wq_0' matches both and only those two boundaries that are to match.
```
mark match *z_eq_0
```
Listing all boundaries shows the declared match:
```
<<hip[tets: 4/4]> mark match *z_eq_0
 <<hip[tets: 4/4]> li su all
   Nr: Mrk(#), geoType, bcType, order, text
    1: 0 ( 2),     bnd,      n,    1, bottom_y_eq_0         
    2: 0 ( 2),     bnd,      n,    2, right_x_eq_1          
    3: 0 ( 2),     bnd,      n,    3, top_y_eq_1            
    4: 0 ( 2),     bnd,      n,    4, left_x_eq_0           
    5: 0 ( 1),     bnd,      n,    5, first_slice_at_z_eq_-1
    6: 0 ( 1),   match,      n,    6, last_slice_at_z_eq_0  
    7: 0 ( 1),   match,      n,    7, first_slice_at_z_eq_0 
    8: 0 ( 1),     bnd,      n,    8, last_slice_at_z_eq_1  
```
Now we can attach the prism part to the current mesh, which is the tet part. We could identify the mesh to attach by number, again this is not recommended as grid numbers may change depending on order. Earlier we gave the prism mesh the label 'prisms' so we can instruct
```
 <<hip[tets: 4/4]> attach prisms
 
  Adding grid 2 to grid 4.
       INFO: removed 25 duplicated vertices.
       INFO: removed 64 matching faces.
       INFO: removed 0 interfaces.
 
   Checking unstructured grid at check level 5, i.e. checking for
       consistent element connectivity (expensive!) and
       boundary setup.

       INFO: largest dihedral/face angle  90.0 deg., elem 1.
       INFO: grid seems valid.
       INFO: grid contains
          512 cells, 
          225 nodes, 
          256 bnd. faces.

       INFO: hMin: 0.25, hMax: 0.433013.
       INFO: element volumes: min = 0.00260417, max = 0.0078125:
       INFO: total grid volume: 2

 
   Topology: noTopo
 
   Domain
      min x,y,z:    0.000000000e+00,  0.000000000e+00, -1.000000000e+00
      max x,y,z:    1.000000000e+00,  1.000000000e+00,  1.000000000e+00
```
The diagnostic output shows that, as expected, 25 nodes have been removed, there are 64 shared triangular faces. The bounding box of the mesh runs '-1<=z<=1'. Inspecting the surfaces of this mesh with 'list surfaces' we find that the interior boundaries at z=0 have disappeared.
``` <<hip[tets: 4/4]> li su
   Nr: Mrk(#), geoType, bcType, order, text
    1: 0 ( 2),     bnd,      n,    1, bottom_y_eq_0          
    2: 0 ( 2),     bnd,      n,    2, right_x_eq_1           
    3: 0 ( 2),     bnd,      n,    3, top_y_eq_1             
    4: 0 ( 2),     bnd,      n,    4, left_x_eq_0            
    5: 0 ( 1),     bnd,      n,    5, first_slice_at_z_eq_-1 
    8: 0 ( 1),     bnd,      n,    8, last_slice_at_z_eq_1   
```
Finally we write the mesh in h!p hdf format
```
 <<hip[tets: 4/4]> wr hd tetpri555
```
This produces a hdf file tetpri555.mesh.h5 with the mesh, and an xdmf mapping file  tetpri555.mesh.xmf to load into e.g. paraview.
The resulting prism mesh is shown below.
![](../images/tetpri555.png)

## More information

See hip manual Sec 4.17, 'glueing together' for details.




# Interpolation
hip can interpolate the solution from one mesh to another.

## Why use hip for this?
hip's minnorm algorithm is very robust in that it gives the smoothest possible second order accurate solution when interpolating on a single element.
hip offers specific support for composite interpolation, e.g. the solution on your mesh comes from different sources.
hip enforces periodicity of the solution.

## Why not use hip for this?
hip's algorithm is sequential, if you have a parallel interpolation tool that works, you may prefer that one for large meshes.
hip offers algorithms that are second order accurate, but none that is conservative (that would be very expensive).  

## Example

The following script reads a source mesh (donor) with solution in hip format, reads a target mesh (target), interpolates using the default min-norm algorithm, writes mesh and solution back to hdf.

```
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% generate a 2d mesh for a cube
ge -1 -1 -0.1 0.1 3 3
% extrude to 3D, 
co 3d 0 1 2 z
% add a NS test solution that varies with x
va init x
% show the variables
va
% write to file for plotting
wr hd unitx

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% generate a larger rectangle mesh
ge -1 -1 4 1 24 12
% add a NS test solution based on radius from the orign
va init r
% extrude to 3D, this then becomes the current grid
co 3d -.5 1.5 13 z
va
wr hd rectr

% instruct to interpolate using the minnorm element-based interpolator
set in-recoType el

set in-fc 0
set in-ri 0
%set in-ri 0.5

% interpolate from the grid named cube to the current grid
interpolate grid 2
va

wr hd interp
ex
```

The first set of steps creates a 2D Cartesian mesh between the lower left point of x=-1,y=-1 and the upper right point of x= -0.1, y=0.1 with 3 nodes along x and y. This 2D mesh is then extruded to 3D between z=0 and z=1 with two additional slices, i.e. 3 nodes in z. Finally some sample flow solution is put on that mesh that varies in x. The figure below shows the resulting distribution of the w velocity component.
![](../images/unitx_w.png)

The second set of steps creates a larger rectangular bloc that encompasses the smaller one with a solution that varies with distance from the origin.The figure below shows the resulting distribution of the w velocity component. The inscribed  cube is shown with white lines.
![](../images/rectr_w.png)

The final set of instructions selects the default interpolation by element based on appling a linear gradient reconstruction within the element to the element average at the gravity centre. 
They also fix two tolerances to zero. The interpolate face tolerance "in-fc" determines the tolerance how far a node can be from the face of an element to still be considered inside the element, see Secs 2.8 and 4.19.2 for details. If a target node is inside the donor cell, standard interpolation rules apply, e.g. a gradient will be extrapolated. Default is 0.1, i.e. any node within 10% of the edge length of the face is considered contained, here we set this to zero. 
The second threshold is the rim by which to extend the solution of the nearest node, i.e. just picking that nodal value but no extrapolation. In the first instance we also set this to zero.

The resulting interpolated solution is shown below. We can see that only those nodes that are inside the smaller cube are interpolated, the other nodes retain their values. 
![](../images/interp_w_rim0.png)

Setting the "in-rim" tolerance to 0.5, i.e. any node within 0.5 of the edge length of the nearest element is considered to be inside the 'rim', produces a 'halo' of nodes around the smaller cube that also get interpolated, retaining the solution of the nearest node.
![](../images/interp_w_rim0p5.png)


## More information

See hip manual Sec 2.8 for parameters, or 4.19 for the description of the algorithms used.



# Mesh adaptation
hip can adapt the tetrahedral part of meshes using mmg.

## Why use hip for this?
hip's adaptation maintains periodicity.
hip handles a non-simplex part (e.g. prisms in 3D), but those are frozen, no adaptation. 


## Why not use hip for this?
hip's algorithm is sequential. You will need more patience than with a paralellised algorithm.

## Example 1

The following script adapts a mesh to reduce the average mesh width by half:

```
% read the initial mesh
read hdf level0.h5

% refine
mmg

exit
```

## Example 2

The following script adapts a 

```
exit
```

## More information

See hip manual Sec X for parameters, or X for the description of the algorithms used.


# Isotropic mesh refinement

## Why use hip for this?


## Why not use hip for this?


## Example

The following script 

```
exit
```

## More information

See hip manual Sec X for parameters, or X for the description of the algorithms used.



# Ab-initio mesh generation from STL

## Why use hip for this?


## Why not use hip for this?


## Example

The following script 

```
exit
```

## More information

See hip manual Sec X for parameters, or X for the description of the algorithms used.



# Last update:
13/12/24, JDM: conceived
