\contentsline {section}{\numberline {1}Introduction}{5}{section.1}
\contentsline {subsection}{\numberline {1.1}Scalar adaptation library in hip}{5}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Sequential refinement algorithm}{5}{subsection.1.2}
\contentsline {section}{\numberline {2}Data-structures}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Unstructured grids}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Periodic vertex pairs}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Linked lists of entities: llEnt}{8}{subsection.2.3}
\contentsline {section}{\numberline {3}Refinement}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Reftype}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Adaptive edge}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Periodic adapted edges}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Allocation}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}Parallelisation}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Questions}{9}{subsection.4.1}
