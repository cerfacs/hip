\section{Introduction}

{\hip} is a package for manipulating unstructured computational grids. 
It was born in 1997 at CERFACS in Toulouse to convert structured 
multiblock meshes into unstructured ones. (In the following, the
terms mesh and grid will be used synonymously, and if not specified
otherwise will always refer to the most general notion considered
here wich is a hybrid unstructured grid.) 
From that, {\hip} grew into a toolkit that
\begin{center}
  \begin{tabular}[c]{>{\bf}lp{.7\textwidth}}
    reads &  meshes in various formats,\\
    generates & simple rectangular 2D meshes,\\
    transforms & meshes by translation, scaling rotation or reflection,\\
    validates &  the mesh,\\
    removes &  certain mesh degeneracies,\\
    converts &  structured into unstructured grids,\\
    splits &  quadrilateral elements into  triangular ones,\\
    extrudes & 2D meshes axially and radially to 3D meshes,\\
    interpolates &  solutions between grids,\\
    cuts &  pieces of structured and unstructured meshes out,\\
    assembles &  meshes from pieces,\\
    coarsens &  unstructured grids for multigrid applications,\\
    adaptively &  refines grids,\\
    and writes & meshes in various formats.
  \end{tabular}
\end{center}
{\hip} is distributed under the Cecill B license http://www.cecill.info\\
Under this license, any work using any part of this software must acknowledge 
the work from the author and contributors (see src/hip.c)\\

\subsection{Command line interface}

The command
{\tt hip -v} prints a banner with a version stamp and compilation details
to the screen and then exits.

In more typical use, 
\index{command line}\index{interface|textbf}
{\hip} reads commands either from a script file, the preferred way for most
users, or directly from the prompt. 


%
Most commands can conveniently be
abbreviated to 2 characters. Some secondary commands, e.g.
{\tt set ab-vo} need two characters after the separation '-'.  
%
Filenames or a numerical range contains
non-standard characters such as blanks or '-' need to be
enclosed in single or double quotes. 

The following script \index{script} reads a file in a
dpl format, changes some boundary types and writes it out to an adf database:

\begin{verbatim}
  set verbosity 5
  set path /users/jdm/grids/2D/box/
  
  %%%%% hybrid box, i/o example.
  read dpl box-hyb.dpl

  se bc-ty "3-6,9,1" w
  
  write hdf box-hyb
  exit
\end{verbatim}

In the first line, the verbosity is increased to 5 from the default value
of 3. With a verbosity of 5, {\hip} will be quite chatty but this is a good
idea if you expect problems with your mesh. Error messages will be much
more detailed this way.

On the second line we define a file path. All filenames to be written or
read are pre-pended with this path, except absolute filenames that start
with a leading /.  

Line 4 has a comment which is echoed by {\hip} to the screen, and in line
5 we instruct {\hip} to read a dpl file named box-hyb.dpl which is to be
found, of course, in /users/jdm/grids/2D/box/. 

In line 7 we change the boundary conditions 1,3,4,5,6 and 9 to type
wall. Note how you can identify a particular boundary condition, grid, zone, variable:
\begin{description}
\item[by number:] specify the number of the entity. This number is assigned chronologically,
  the first boundary read from file is assigned 1. However, if the same mesh is read
  as a second mesh, with already 5 boundaries present, the first boundary of this mesh
  will be assigned 6.
\item[by name:] specify the name of the entity. Names are made unique as much as possible,
  given the information available in the particular mesh format that was presented to \hip.
  Some commands such as {\tt read gmsh} allow to add this information using UNIX-style
  command line arguments, the names of all entities can be changed using the {\tt set}
  command. Note that boundary names in \hip are global and coalesce: if you read another
  mesh with the same boundary names, they will be considered to be the same. Use {\tt set}
  to change them before you read the next mesh, if needed.
\item[by expression] specify an expression that matches one or a number of entities.
  Any expression starting with a number is considered a number range as in the example
  above. Any expressions starting with a character is compared to the names of entities.
  %Only simple matching is available: the wildcard '*' which matches any or no characters.
\end{description}

In line 9 we tell {\hip} to write this grid to an hdf database and perform
all the operations that go with that, such as variable type conversion.
This file format creates two files, one for the mesh and one for the
solution. File extensions will be added to the root name 'box-hyb'.

Finally, line 10 makes {\hip} exit. (More about reading and writing and a
description of some of the formats used can be found in the pertaining
sections.)

At the prompt, {\hip} will echo all commands read in from a script, 
including comments. The prompt shows you how many meshes are currently
held and which one you are working on. (Essential when glueing pieces
of mesh together.)

At the moment, {\hip} reads as many pieces of information from the input
as required, irrespective of line breaks. While this gives the user
some nice freedom to format his script file, it can lead to errors
with runaway arguments. In the near future, a UNIX-like continuation
character might be introduced if a command continues on the next line.

Starting with version 1.34.0, April 2012, \hip will start to use unix-style
optional arguments. Hence, to write all variables to an hdf file, 
instead of {\tt write hdfa} one can invoke {\tt write hdf -a}. This
is currently implemented for {\tt read/write hdf} and will gradually
be rolled out to all interfaces.  


\subsection{Getting help}

{\hip} has an \index{online help|textbf}\index{help}online 
help menu. On crying help at the prompt, all basic
commands are shown:
\begin{verbatim}
hip[0/0]> he 
 The following commands are recognized:
     read, list, mark, cut, attach, copy, set, variables,
     check, buffer, adapt, mg, write, exit.

     All commands can be abbreviated to two letters.

     Type help < command > for details of this command.
     Necessary parameters are given as < nec_param >,
     A choice of parameters are given as { choice1, choice2,.. },
     Optional parameters are given as [ optParam ].
     ( Note: naturally you do not type the <{[;-)]}> ).
\end{verbatim}
A {\tt he re} would give you a short description on all the input formats
{\hip} can understand. Failing that, you can s.o.s.~to j.mueller@qmul.ac.uk.

\subsection{Internal data structure}

{\hip} is not an environment to do number-crunching on your grid. Performance
is usually not an issue when designing modules for {\hip}, although I try
to have most operation costs scale optimally with the mesh size and to
minimise storage.

The most important issue is the flexibility of its data-structure. There
is a run-of-the-mill structured multiblock data-structure and a flexible
unstructured one with pointers between elements, vertices and boundary
faces.

\subsubsection{Structured grids}

\index{data structure!structured}
Structured grids are stored as a list of blocks with a list of ``patches''
that completely cover each block. These windows are either boundary
patches, internal patches connecting to another block or degenerate
patches where a block face collapses into a line or a point. 

When copying a an entire structured grid or part of it
to an unstructure one, the degeneracy information is deduced from
the degenerate patches. The vertices are then matched by geometric
comparison using a quadtree/octree data structure. While this is 
a simple and efficient way of doing it, it will fail if the 
structured grid is topologically closed but has geometrical gaps
between blocks that are larger than the smallest mesh size
(cf.~section \ref{sec:epsOverlap}).

\subsubsection{Unstructured grids}


The \index{data structure!unstructured} unstructured data structure
\index{data structure} is shown in Figure~\ref{fig:datastruct}.
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=.7\textwidth]{hip-data-struct}
    \caption{\protect\hip's backbone datastructure}
    \label{fig:datastruct}
  \end{center}
\end{figure}
Broadly speaking, for each grid it is working on,
{\hip} stores a list of elements with pointers from each element to a
vertex and a list of vertices. {\hip}  deals with hybrid grids which
in 3D can be composed of tetrahedra with four vertices or hexahedra
with 8. To economise storage, all element to vertex pointers are 
concatenated in one list and each element has a single pointer into
that list. The number of relevant pointers is implicitly known from
the element type.

Edge collapsing \index{edge!collapsing} (see section \ref{sec:mg})
and the removal of duplicate vertices such as in
multiblock mesh conversion needs to change the coordinates of
several vertices in a consistent fashion. This is easily
implemented by having one coordinate triplet and having
the vertices carry a pointer to the coordinates. {\hip} then just
changes the relevant coordinate which is pointed to by several
vertices and consistency is guaranteed.
%
Boundary faces are grouped and point into the element they belong
to.

What does this mean for the user:
\begin{itemize}
  \item  Any format that provides element to vertex pointers and 
         describes boundary faces by giving the element can be added
         very easily.
  \item  Formats that give face to vertex pointers for boundary faces
         are at a disadvantage, since {\hip} has to find the element that
         goes with it. An encapsulated object that accomplishes this
         is provided, see the source in read\_uns\_adf.c for examples.
  \item  {\hip} is not \index{parallel} parallel 
         and there are no plans to parallelise it any time
         soon. If you run out of memory with your problem in {\hip},
          switch off all check
          options that use a lot of storage. If that fails,
          ``you're gonna need a bigger boat''.
          %use a bigger machine.
\end{itemize}

All types of elements, vertices and boundary faces are kept in the same
storage space, be they valid, invalid, children, parents, cut off, glued on,
etc. This is what makes the implementation of grid manipulations rather
simple because we don't have to bother with shifting elements from one
dedicated space to another. 

The price that is paid for this is that writing a mesh is not a trivial task as
it is with most solver. {\hip} cannot take the contents of its own data-structure
and dump it straight to file. Any write operation starts with a numbering
step where all elements that are currently of interest are numbered, 
e.g.~leaf elements or parents. Certain formats then require that elements
and boundary faces are sorted for type, which is done by repeatedly looping
over the list and picking off the numbered entities with the proper type.
Care has to be taken here that the numbers are reset properly such that pointers
are correct when written to file. Think e.g.~about boundary faces pointing
into elements.

Additional complication is added by special treatment of inviscid boundary
normals, periodicity etc. Sections \ref{sec:periodicity} to \ref{sec:boundary-normals}
explain what modifications occur.

\subsubsection{Geometric accuracy: $\mathbf{\varepsilon_{Overlap}}$}  \label{sec:epsOverlap}

\index{epsOverlap}\index{epsOverlap@$\varepsilon$}
In {\hip}'s unstructured paradigm each vertex occupies a certain disc which
may not overlap with any other vertex's disc. E.g.~when duplicate vertices
of multiblock \index{multiblock}
 grids are merged, they are considered identical if their
distance is less than {\tt epsOverlap}, $\varepsilon_{Overlap}$. 

When {\hip} reads an unstructured grid from file or performs adaption or
coarsening,
$\varepsilon_{Overlap}$ is set to $\varepsilon_{Overlap} = .9*h_{Min}$, the smallest edge length found. This
value works quite well for glueing pieces of mesh together, but it is
rather tolerant. It also means that a mismatching periodic vertex might
be moved nearly a mesh width onto the nearest partner on the corresponding
patch (see section \ref{sec:periodicity}). 
Thus, it might be advisable to manually reduce $\varepsilon_{Overlap}$ once
the file has been read. This is done via the command {\tt set eps}. The
current value can be queried by issuing {\tt set} without any arguments.

\subsubsection{Canonical Elements}
\label{sec:canonical}

\index{canonical elements}
The definition of the elements, faces and edges is given in Figure \ref{fig:canon}.
\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=.8\textheight]{canon}
    \caption{\protect\hip's canonical elements.}
    \label{fig:canon}
  \end{center}
\end{figure}

The definition of the order of the vertices on an element has been adopted
from
\href
{http://raphael.mit.edu/visual3/visual3.html}
{Bob Haimes' \index{Visual3} Visual3} 
package.
The numbering of the faces has been
changed from Visual3 to conform with \index{AVBP} AVBP. However, the face numbering
is only relevant to input files that supply boundary faces by giving the
element and the canonical face number in the element as the adf and avbp
formats do.

Edges are numbered lexicograhically, e.g.~the edges of a triangle are
1: 1-2, 2: 1-3, 3: 2-3. Thus, in 2D edges and faces denote the same
entity, but with different numbering.

Derived elements that have hanging nodes or hanging edges are described
with the list of forming nodes and an ``elMark'' \index{elMark}
particular to {\hip}. This
elMark contains the base element type, the position of hanging nodes,
and the position of hanging edges, etc. {\hip} packs and unpacks this elMark 
into and out of a bitfield that is 8 bytes long. In all applications,
the content of this bitfield should not be visible to the solver on the
grid, but the geometry routines supplied with {\hip} should be invoked to
get surface triangulations etc. of these elements. Thus, a description
of the elMark is not given here.
 
\subsection{Standard boundary types}
  \label{sec:bc-types}

Each boundary patch or group carries a unique label and can be
assigned a boundary type. If a boundary label appears with more than
one patch the patches are concatenated. This is due to the usual
practice in multiblock grids to list each boundary `window' of a
block separately, although the windows on the different blocks form
a group together. Thus, if you want to keep patches separate, give
them different labels. If you'd like to condense several ones into
a larger one, change the labels to the same after reading the
mesh and compress the list or write it out again.

{\hip} recognizes the following generic boundary types:
\index{boundary!type}\index{surface type|see{boundary type}}
\begin{center}
  \begin{tabular}[c]{ l l l l l l }
   e:   & entry           &    o:   & outlet          & u:   & upper periodic  \\
   f:   & far field       &    p:   & any periodic    & v:   & viscous wall    \\
   i:   & inviscid wall   &    n:   & none            & w:   & any wall        \\
   l:   & lower periodic  &    s:   & symmetry plane  \\
  \end{tabular}  
\end{center}
Note that {\hip}'s dictionary most likely is richer than the one of your
solver at hand.

The boundary type \index{boundary!type}
 can be [re]set after the mesh has been read into {\hip}.
Whenever possible, {\hip} tries to read the boundary information from
file. Types have to be properly set before a read, a multigrid coarsening
or an adaptation is done, since the type has an influence on how boundaries
or periodic pairs are treated.

When calculating boundary node normals \index{boundary!normals}
on inviscid walls, {\hip} sets
normals at sharp corners to 0. such that a hard nodal boundary 
condition does not specify in a bluntly averaged direction. For these
surfaces groups the type has to be set to `i', inviscid wall
(see section \ref{sec:boundary-normals}).

Nodes in the symmetry plane \index{symmetry}
 are treated specially in certain formats.
E.g.~the adf format lists symmetry nodes with the periodic edges.
In this case, the symmetry boundary group has to be declared of type
`s'. 

Periodicity \index{periodic}
is usually set up by declaring pairs of hip\_per\_inlet
and hip\_per\_outlet labels on the respective surfaces
(see section \ref{sec:periodicity}). If the 
geometric transformation between the two
matching surfaces is simple, {\hip} will generate the necessary information.

The order in which boundary patches are listed can be influenced. By
default, {\hip} lists them in chronological order. See section \ref{sec:set-bc}
for details.

\subsection{Error messages}

Within its over 140000 lines of code, {\hip} has about a 900 instances of
tests issuing a ``FATAL'' error\index{error}, 
about a 500 that give you a ``WARNING''.

Only a few of those fatal ones actually need to stop the program, such
as e.g.~a malformed input file. Often a fatal message refers to the
current command being fatally affected, e.g.~changing the text for
a boundary condition that does not exist, but not to the overall
validity of the mesh.
%
Some of the warning messages on the other hand might be
fatal if not attended to, such as e.g.~the solution not
being read successfully. Hence the user is strongly advised
to carefully check the listing
for those. 

\hip will abort under any circumstance if a non-remediable
error has occurred. \hip will also abort after a warning
if the verbosity is set to 0, hence \hip considers to be
launched by software and manual remedy is not expected.
In those cases error logs are written to the current
directory.

If {\hip} is convinced that a faulty mesh connectivity or a
similarly grave problem has occurred, {\hip} will not write
the invalid grid to file.





% \subsection{Compiling and Installing {hip}}
% \index{installation|textbf}\index{compilation|textbf}
% 
% {\hip}\index{compilation} is written in ANSI-C and should
% compile on any UNIX system. It has a very basic visual
% facility for examining your mesh based on GLUT and can be
% interfaced with the adf/CGNS database management libraries.
% 
% The Makefile is build for
% GNU-make which usually is either the default make or can be
% found under /usr/local/bin/make. If in doubt, consult with
% your system administrators.
% 
% Obtain a license and the tar archive of the most recent
% version. At the moment, the master copy of {\hip} sits at
% CERFACS in /home/muller/hip/tar. Unpack the archive in
% a suitable location and have a look at the Makefile in
% hip/src.
% 
% The Makefile in the source distribution lists a number of platforms
% that {\hip} is regularly compiled for, among the ix86 32bit and i64\_86
% 64bit Linux, IRIX and IRIX64, Sun Solaris, OSF1.  If your system sets
% the HOSTTYPE environment variable correctly -- matching the entry in
% the Makefile -- you should be able to run the Makefile out of the box.
% 
% When introducing a new architecture add an if-block for your HOSTTYPE.
% Find out whether your system is little or
% big-endian\index{little-endian}\index{big-endian} and set the
% LITTLE\_ENDIAN variable in the Makefile accordingly. {\hip} writes binary
% AVBP files always as big-endian files and hence needs to be instructed
% to do byte-swapping for reading and writing binary files on
% little-endian systems.
% %
% By default {\hip} compiles with -g. Most likely you will want to compile
% supplying CFLAGS=-O instead.
% 
% To compile hip you need to run the following commands in hip/src:\\
% {\tt
% make cleano \\
% make CFLAGS=-O hip\\
% make install\\
% make cleano\\}
% 
% Installation will be done into the users home under
% $\sim$/bin/\$(MACH\_DEP)/hip-version where version refers to the numeric
% version identifier. A link to $\sim$/bin/\$(HOSTTYPE)/hip will also be
% created. 
% 
% 
% If you want to use {\hip}'s ``visual mesh debugger'', - I
% hope that rather sooner than later this label will be
% accurate and not just hype - you will have to compile
% ``glutGrid'' \index{glutGrid}\index{visual mesh debugger} in
% hip/viz\index{Vizard of Ox}.


\cleardoublepage
