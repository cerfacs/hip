\section{Mesh Adaption}
  \label{sec:adapt}


\subsection{Remeshing with mmg3d}
\index{mmg3d}
\index{remeshing}

As of version 1.48.0, March 2016, \hip can call the 
mmg3d\footnote{http://www.mmgtools.org/} 
library of INRIA Bordeaux. The mmg3d algorithm uses a metric
interpolated on an existing tetrahedral mesh to produce a new 
tetrahedral mesh. 
The tet mesh is fully copied to mmg3d, hence a substantial memory
increase can be expected.

If the source mesh is a hybrid 3-D mesh, \hip extracts the tetrahedral
part, submits it to mmg3d for remeshing with the interface triangles fixed, 
that is no changes are allowed for this interface.
After adaptation of the tetrahedral part by mmg3d,  both parts 
are glued back together. Currently (\hip 1.51), 
the non-tet part is copied as well, needing twice the memory for that part. Glueing
together makes use of a retained list of fixed faces and is low memory
compared to the `agnostic' glueing/merging described in Sec.~\ref{sec:glueing}.

If boundary faces are to be frozen and not adaptation of boundaries
can be prevented
by marking a boundary with {\tt mark surf nBc1}
where {\tt nBc} is the number of the boundary to freeze.

The
metric that controls the mesh sizes of the adapted mesh
can be at the simplest a constant isotropic edge length, or at
its most complex a tensor defining anisotropic meshes.
\hip currently offers two approaches to define the local mesh size
and one option to visualise the metric field.

\begin{enumerate}
\item 
An isotropic scaling relative to the existing mesh size can be prescribed with
\begin{verbatim}
   mmg isoFactor < options >
\end{verbatim}
The following options are supported:
\begin{description}
\item[{\tt -f factor}] allows to set the relative scaling, e.g. {\tt
    -f 0.5} prescribes all edge lengths to be half the size of the
  original grid. Values are interpolated linearly on tetrahedra
  between vertices, the current mesh size at a vertex is evaluated as
  the average length of all edges formed with that vertex. The default
  value for the factor is 1.0.
\end{description}

\item 
  Isotropic scaling relative to original mesh
  according to a scalar field variable
  such as e.g.~produced by an error sensor
is produced by \index{mmg3d!metric}
\begin{verbatim}
   mmg isoVariable < options >
\end{verbatim}
Note that the metric is a scalar multiplier of the existing edge
lengths, not the value of the desired edge length in the adapted
mesh. Hence a value of 0.5 prescribes half the mesh width compared
to the original mesh.

The supported options are
\begin{description}
\item[{\tt -g hGrad}] allows to limit the resulting gradient in mesh
  size after application of the metric. Allowing the gradient to be
  too severe will adversely affect mesh quality. The default value
  is $hGrad=1.4$.
\item[{\tt -h hausdorff}] specify the Hausdorff distance, i.e.~the
  distance between the surface reconstructed from the original mesh
  and the one of the adapted mesh.

  Smaller values force higher accuracy. Default: hausdorff=0.01. Note
  that refinement and decimation use different default values for
  the Hausdorff distance.
\item[{\tt -l, -u}] Specify a lower bound $h_{min}$ or an upper bound
  $h_{max}$ for the mesh spacing. The metric will be clipped
  to these values. The values will also be declared to mmg3d, however
  mmg3d only aims to adhere to these.
\item[{\tt -s}] Save meshed before and after refinement in medit .mesh
  format. This can be useful for debugging.
\item[{\tt -v nVar}] selecting the number of the field variable to
  adapt to. The prescribed local scale is the average edge length at a
  vertex multiplied with the field variable of the vertex.
\end{description}

\item 
To view the prescribed mesh widths the scalar isotropic metric 
can be plotted with
\begin{verbatim}
   mmg isoMap < options >
\end{verbatim}
with {\tt -v nVar} indicating the name of the metric field variable to adapt to, similar
to the {\tt isoVariable} option. The field variable will be
overwritten with the metric field on the original grid, but no
remeshing is actually performed.

The metric will be constructed with the same parameters as available
for the {\tt isoVar} option.
\end{enumerate}

If a remeshing is performed, an existing solution on the grid that
provides the metric will be interpolated to the new grid. mmg3d is
called here allowing tangential movement and face remeshing on
boundaries.

\subsubsection{Remeshing with periodicity}
If a 3-D mesh has a declared periodicity, as of \hip 19.04 the adaptation
will produce matching adapted surface meshes.
Currently (version 19.04), this is not supported for 2-D.

This is achieved in a two-step procedure:
\begin{enumerate}
\item Identify a portion of {\tt mPerLayers} of elements on either
  periodic boundary, cut the upper (shadow) periodic boundary out, rotate/translate,
  and merge with the lower (master) periodic boundary.

  Adapt this mesh, freezing the 'cut' boundaries, but allowing the
  periodic boundary inside to be adapted.
\item Separate the adapted upper (shadow) part of the mesh and translate/rotate
  back. Re-unite the mesh with unadapted core part, merge the previously
  cut boundaries.

  Adapt this mesh, allowing the internal boundaries to adapt, but freezing
  the already adapted periodic boundaries.
\end{enumerate}
The choice of the number of layers on each periodic boundary is important, the
default value is 10. Choosing too many layers will make the algorithm fail as
the periodic mesh parts on lower and upper periodic boundaries will touch and
no longer have an isolating core part between them. Choosing too few layers will
inhibit adaptation of the periodic boundaries and result in inferior mesh quality.
The number of layers is controlled with the argument
\begin{description}
\item[{\tt -p mPerLayer}] Number of layers of elements to be included in the
  first pass for periodic boundaries.
\end{description}
A few details are noteworthy:
with t
\begin{enumerate}
\item Just to repeat, currently only works in 3D. In 2D currently (19.04)
  periodic boundaries are fixed.
\item Multiple periodicity, where different periodic pairs may touch on
edges, is not supported.
\item Periodic patches of opposite ends ({\tt l,u}) may not touch. Make sure that
  all your periodic pairs have the same orientation, with all {\tt l} on one
  side, all {\tt u} on the other.
\item When using the {\tt isofactor} option, the two stage process requires
  to store the metric field on the original mesh, which will also be interpolated
  to the new mesh. Hence an additional field variable is produced.
\end{enumerate}

\subsubsection{Remeshing with fixed boundaries}

There may be cases where a fixed surface mesh is required on a
particular boundary patch. This can be achieved in 2D, and in
the non-periodic 3D case, by 'marking' the fixed boundary/ies
with
\begin{verbatim}
   set bc-mark <expression> 1
\end{verbatim}
where {\tt expression} identifies the range of fixed
boundaries.


\subsection{Decimation with mmg3d}

\index{decimation}
\index{mmg3d!decimation}
Some pre-processors or graphical utilities want to display a coarsened
surface mesh to help geometry inspection or graphically select boundaries.
\hip offers to write a decimated surface mesh which contains 
 only triangles, but also lists its perimeter and
  feature edges. Currently (as of \hip 1.50), only the ensight file
  file format supports writing edges in 3D.
\begin{verbatim}
   decimate < options >
\end{verbatim}
Options are
  \begin{description}
  \item [-a] set the threshhold value for the scalar product between
      unit normals of faces forming an edge. Feature edges are
      listed if the scalar product falls below that value.
      Default value is 0.8
    \item[-f] coarsening factor. Default value is 10e-2. Note that
      this value scales the average edge length, so a higher value
      produces stronger coarsening.
    \item[-g] gradient of mesh spacing, default value is 10. A larger
      value allows stronger gradation in mesh width.
    \item[{\tt -h hausdorff}] specify the Hausdorff distance, i.e.~the
      distance between the surface reconstructed from the original mesh
      and the one of the adapted mesh.

      Smaller values force higher accuracy. Default: hausdorff=0.001. Note
      that refinement and decimation use different default values for
      the Hausdorff distance.
    \item[-l] lower bound of mesh spacing hMin.
    \item[-u] upper bound of mesh spacing hMin.
    \item[{\tt -s}] Save meshed before and after refinement in medit .mesh
  format.
  \end{description}


\subsection{Hierarchical grid refinement}

{\hip} can adapt \index{adaption}
hybrid meshes based on a given flowfield. You can choose the
difference of a state quantity along an edge, the absolute
value of a state quantity averaged on an edge or a divergence-rotation
sensor. The thresholds can either be chosen as fixed percentiles
from top and bottom or as fractions of the mean deviation above
and below the average to refine and de-refine, respectively.
Finally it can be chosen whether the adaptation is to refine
isotropically (default) or directionally. Directional splits are
allowed for hexahedra and pyramids.


\subsubsection{Sensors}
  \label{sec:sensors}

A sensor can be an edge-wise first difference of a flow
quantity:
\begin{description}
\item[\tt  adapt diff-$<$state-variable deref ref {[isotropy]} $>$:]
       adapt a grid based on differences of the state variable ( one of
       rho, q, p, t, 1, 2, .. ), If the fractions are given positive,
       they are read as fixed fractions.
       Example: diff-q .1 .3 refines on differences of the norm of
       the velocities, derefining the 10\% lowest, refining the
       30\% highest.
       
       If the fractions are negative, they are read as fractions
       of mean deviation. Example: diff-p -1. -1. 0 refines on 
       pressure differences, derefining one mean deviation below
       and refining one mean deviation above the average using
       directional refinement.
\item[\tt adapt abs-$<$state-variable deref ref {[isotropy]} $>$:] 
       adapt a grid based on the absolute value of a state quantity.
       Example: ad abs-7 refines on the value of state quantity 7,
       a reaction rate e.g..
\end{description}


A flowfield sensor has proven very good in 2D Euler, and could also
be useful for 3D Navier-Stokes:
\begin{description}
\item[\tt  adapt divrot $<$ deref ref $>$ :]
       adapt a grid using a div-rot sensor, derefining the deref cells
       with the lowest values, refining all the ref highest cells. This
       is a cell based indicator and has no means of giving any indication
       on how to refine directionally. Default arguments are 
       {\tt $<$ div-rot .1 .7 $>$}.
\end{description}

Figure \ref{fig:naca-adapt} shows the adaptation on a NACA-0012 profile
in transonic flow. The adaptation parameters have been changed during
the process to allow for more adaptation at the first level and become
more restrictive on the fourth.
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=.32\textwidth]{naca-0-grid}
    \includegraphics[width=.32\textwidth]{naca-4-grid}
    \includegraphics[width=.32\textwidth]{naca-4-mach}
    \caption{A transonic flow over a Naca 0012 profile. Shown are the initial
             mesh, the mesh after fourth adaptations on the total velocity vector
             and the resultig Mach contours.}
    \label{fig:naca-adapt}
  \end{center}
\end{figure}

A major problem with these simple sensors is the resolution of a shock.
While all smooth features become more resolved, a difference of a 
state variable across a shocks remains unchanged, a gradient becomes
even steeper. After a few levels of adaptation all refinement will be
thrown at the shock.

One way of counteracting is to exclude the shock region from refinement
using a haribo (see below), albeit a rather crude one. Another possibility is to
refine a large percentage of cells initially, say around 50\%, to make sure
that smooth features e.g.~around the leading and trailing edges are 
resolved before refining the shocks. 
Alternatively the user can limit the depth of refinement 
(cf.~section~\ref{sec:ad-lv}).
Ultimately though we will want to
use adjoint error analysis to tell us exactly where errors matter and 
where they don't.

\subsubsection{Haribos}
  \label{sec:haribos}

You can also limit the region of adaption to certain regions of the
mesh, a list of cylinder-like `haribos'\footnote{Haribo macht Kinder
froh, und Erwachs'ne eben so!} \index{haribo} which are
specified by beginning and end of a line and a maximum distance to it.
Default is the entire domain.


\begin{description}
\item[\tt adapt hrb-reset:]
       Reset the list of haribos to the entire domain.
\item[\tt  adapt hrb-add $<$ x,y,{[z]}Begin, x,y,{[z]}End, radius $>$]
       Add a haribo to the list. E.g.\\
       adapt hrb-add \ \    0. 0. 0.  \ \  1.5 0. 0.  \ \  1.5\\
       adds a haribo running from the origin along the x-axis for
       1.5 units with a radius of 1.5.
\end{description}


\subsubsection{Refinement depth, smoothing}
  \label{sec:ref-depth-smooth}
  
For unsteady applications
it is of interest to limit the depth of adaptation in order to
maintain a reasonable timestep. This can be done by\\
\index{adaptation level}\index{level!adaptation} 
{\tt set ad-lvl $<$ {[maxLevel]} $>$.}\\
E.g.~maxLevel = 2 will only
permit refinement of cells that were once refined to be refined
one more time.

It is desirable to have a smooth adaptation that leaves the grid
as regular as possible and has a small number of interfaces at
different refinement levels. In the case of isotropic refinement,
the user can choose to have the refinement of an element 
upgraded to the next isotropic pattern if
already a certain fraction of the necessary edges exist. By
default this fraction is set to 1., it can be chosen using\\
\index{upgrade} {\tt set ad-upgrade frac}.\\
Useful values are around 2/3.

{\hip} can recover the surface geometry of a NACA 0012 \index{NACA 0012} airfoil
at the moment and move the inserted surface nodes on to the
actual geometry. For this it is necessary to label the
solid wall boundary `naca0012'. If the chord \index{chord length} is not 1., it
can be set to the appropriate value using\\
{\tt set crd value}.

Note that {\hip} does treat periodic pairs in the adaptation 
identically if periodicity is declared before adaptation.

\subsubsection{Buffering}
  \label{sec:buffering}

After adaptation, your grid in general contains elements with hanging
nodes on an edge and/or on a face. These ``derived'' elements 
\index{elements!derived} can be 
described as a base element with added entities described in an 
encoded ``elMark''\index{elMark}. For the time being there is no
public format for this elMark, but the {\hip} distribution contains a
set of functions to decode it. You can use either the AVBP4- \index{AVBP}
or the adf formats to write meshes with derived elements.

In case your solver cannot deal with derived elements,
{\hip} allows you to buffer\index{buffer}
the current grid using the command\\
{\tt buffer}.\\
Buffering means that the derived element is ``triangulated''
with non-derived primitives. Some derived elements can be
simply cut up into tetrahedra and pyramids, others need the
insertion of a central node as the apex of tetrahedra and
pyramids sitting on the surface of the derived element.

Buffering is a costly option. In general you will find an extra 50\%
of cells after buffering.
%
Buffering is removed before each adaption sweep or can be removed
manually by issuing a\\
{\tt unbuffer}\\
 command for the current \index{unbuffer} 
mesh.
\subsubsection{Writing adapted grids to file}
\label{sec:write_hierarchic_adapt}

If a hierarchically-adapted grid is buffered, it can be written to file
using hdf. Without any particular flags, this will strip out the
element hierarchy, including buffering.

To retain the hierarchy for further adaptation, some support is
available with the hdf format. See 
\secref{sec:write_hierarchic_adapt_hdf}
for details.

\cedp
