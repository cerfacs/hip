\section{Reading or generating meshes}

{\hip} can read files in various formats. Structured grids are read into
their own data-structure and have to be converted to unstructured before
performing operations on them. The formats currently supported are

\begin{itemize}
%  \item {adf databases,} 
  \item {various flavors of avbp files for AVBP,} 
  \item {unstructured Centaursoft files,} 
%  \item {unstructured 2-D/3-D grid in CFDRC's mixed format,} 
  \item {structured and unstructured dpl files} 
%  \item {unstructured British Aerospace flite files,} 
  \item {unstructured Fluent files,}
  \item unstructured gmsh ASCII 2.0 files,
  \item unstructured grids from hdf5 databases using AVBP definitions,
  \item {unstructured hybrid n3s files,} 
  \item {unstructured hybrid saturne files,} 
%  \item {various flavors of ox files for the Cindy and Hydra codes,} 
  \item {unstructured saturne files,} 
  \item {multiblock grids in plot3d, using CFDRC's structured topology format,} 
%  \item {multiblock grids in DLR's flower files.} 
%  \item {A\'erospatiale's DAMAS databases.} 
\end{itemize}

{\hip} can also generate simple rectangular meshes for test purposes.

\subsection{Files to control hip}

\subsubsection{read script inFile}

{\hip} reads the commands from inFile. The script\index{script}
 filename is usually
specified on the commandline, though. If the scriptfile does not
end with an {\tt exit}, {\hip} will return to the prompt for more
instructions.

\subsection{Unstructured grid files}

\subsubsection{generate llx lly urx ury mX mY}

\index{generation}
{\hip} can generate an unstructured rectangular mesh of quadrilateral elements
in the box bounded by llx, lly at the lower left, urx, ury at the upper
right. The mesh will have mX nodes along x, mY nodes along y. 

If you want a 3D mesh, you can extrude the 2D mesh using the copy 3D
option (cf.~section \ref{sec:2.5d}).

% \subsubsection{read adf gridFile \{solFile\}}
% 
% \index{unstructured!read|(}\index{adf!read}
% Reading and writing grids via the adf database is the preferred
% way. All information about the set of grids is conveniently packed into 
% into one file where bits and pieces can be picked off one at a
% time. The adf database is the kernel underlying the CGNS standard
% for describing structured multiblock grids, http://www.cgns.org. 
% For the time being no standard of defining unstructured grids has
% been agreed upon. See section \ref{sec:write.adf} for {\hip}'s definition
% of keywords.
% 
% The set of keywords that {\hip} defines for adf databases can be found in
% section \ref{sec:write.adf}. When reading an adf base, {\hip} only reads
% the mesh and the parents if it is a refined grid under /grid/parent.
% {\hip} is only interested in the coordinates, the element to node
% pointers, the boundary groups, their types and the boundary faces
% given by element pointers and faces in the elements. If parent to
% child information is given, {\hip} will recreate the complete grid
% hierarchy from it.  
% 
% If a solution file is specified, {\hip} will read the 
% solution, as well.
% If no node {\tt /sol/variable\_type} is given, the variable type
% \index{variable type} defaults to primitve. 

\subsubsection{read avbp inFile}

A number of versions of this format \index{AVBP} are
currently supported, {\hip} will automatically determine
what flavor you are presenting. If {\hip} fails to read an
older AVBP format, e.g.~4.7, you might be successful in
using an older hip version to translate the mesh and
solution to a more recent version. All 5.X formats are
supported, as are TPF and AVSP formats. Any problems with
reading them should be submitted as a bug.

AVBP formats come with a masterfile that usually carries the ending
.visual. In this you find the paths to files containing coordinates,
connectivity, boundary faces, etc.. Feel free to edit these paths
in the file. 

Note that {\hip} does read the boundary face \index{boundary!face}
to element pointers provided by
the fileformat, and not the face to node pointers. If {\hip} fails 
with a face-mismatch error message, this most likely means that your
file does not contain this information. If you are dealing with
periodic grids, some files do not list boundary faces in either the
external or the internal boundary file. In that case, {\hip} will fail
for the same reasons. If faces are given in the internal boundary
file, {\hip} will try to recreate the periodic patch labels for these
patches.

{\hip} also tries to understand the type of flowfield you are specifying.
AVBP writes the standard set of conservative flow variables, species,
fictive species,
reaction rates, turbulence variables and added variables in that
order. {\hip} concatenates them internally and they can be accessed for
interpolation and adaption by giving the variable number in the
order that they appear in the file. On writing, {\hip} remembers 
the types of the variables and other parameters, such as $\gamma$,
$c_p$, the simulation time $t$ and the number of iterations. These
are replaced if the solution is written to an avbp output. 

{\hip} can also read an \index{read!averaged solution} averaged
flowfield specified as the solution file. This fileformat
supplies variable names which can be queried using {\tt
va}. All operations on variables, notably interpolation, are
available with the exception of type conversions such as
conservative to primitive variables. Variables are addressed
by the number as they appear in the file.


\iffalse
\subsubsection{read bae flite.fro flite.gri flite.bco}

The flite \index{flite} format is used by Britsh Aerospace and Swansea University to
describe tetrahedral meshes. It consists of a set of three files, a
.fro file with boundary faces and singular lines, a .gri file with 
connectivity and coordinates and a .bco file that assigns a boundary
condition to the various surfaces. 

The .gri and .fro file are read as binary, the .bco is an ascii file.
The singular lines of the .fro file, trailing edges, e.g., are currently
not read.

\subsubsection{read cdre mesh.dat}

This unstructured face-based mesh format is used by the C\`edre code. Note
that the initial `e' is dropped as the abbreviation `ce' is used
for Centaur mesh files. 

The file is sectioned by headings as follows:
\begin{verbatim}
1.0. DONNEES GENERALES
1.1. SOMMETS DU MAILLAGE
1.2. FACES -> SOMMETS
1.3. FACES -> CELLULES
1.4. FACES MARQUEES-> FACE :
\end{verbatim}
\hip uses the initial {\tt 1.} as a marker key and the remaining label
such as {\tt .0. DONNEES GENERALES} as an identifier to parse the
file. 
\fi


\subsubsection{read centaursoft file}

Read a hybrid grid in Centaursofts \index{Centaursoft} own hybrid format. 
\index{unstructured!read|)}. Supported are file format versions 4 (single
record) and 5 (multiple record).
As of \hip version 1.39, internal interfaces are discarded when reading, rather
than being eliminated with a face check if check-level $>$ 1.
The same version also introduced reading zones from Centaur files.


% \subsubsection{read cfdrc mfgFile}

% This command reads a unstructured grid in CFDRC's mixed format. 
% CFDRC \index{CFDRC} lists boundary faces for the hybrid interface. These
% must be unlabeled or labeled 'Default' if {\hip} is to detect
% the match between two triangular and a quadrilateral faces
% properly. If your grid contains hanging edges, either the adf or
% the avbp4 formats are required on output.



\subsubsection{read cgns cgns.file}

\hip reads unstructured CGNS databases, currently grid only. The CGNS
standard is very flexible, (meaning: a nightmare to support in all its
variations.)  In particular the opaque MIXED element type poses
problems in separating volumetric elements and boundary faces. \hip
imposes two main restrictions on the grouping of elements in sections:
\begin{itemize}
\item you can freely mix and match boundary element types in a section, provided
  this section is exactly one boundary zone (one to one match). E.g.~
  you list together all boundary faces of all types of {\tt bc1\_top\_wall}.
\item you can mix faces from different boundaries, as long as they are
  all of the same type. E.g.~you can list all triangular boundary
  faces of all boundary zones.
\end{itemize}
\hip can read boundary conditions given in families, but is currently not
able to write this grouping back to file. The CGNS standard is currently 
evolving to allow recursive grouping with Families of Families, this hopefully
will be supported in the near future.

The following options are recognised with {\tt read cgns}:
\begin{description}
\item[-n] provide a mesh name to identify the grid.
\end{description}

\subsubsection{read dpl inFile}
  \label{sec:uns-dpl}

The dpl \index{dpl!read} format is a simple ASCII format 
which is the file format
of the triangular mesh generator delaundo%
\footnote{http://www.cerfacs.fr/$\sim$muller/delaundo.html\#delaundo}.
Similar to the gmsh ascii format which is also supported, 
it also is useful to
get any mesh format into {\hip} with very little coding effort. Just
write a little 20liner to convert your format into dpl and you
can work on your mesh with {\hip}.

In 2D, dpl knows a structured and an unstructured format which
differ by the connectivity table only. The structured 2D file
is as follows:
\begin{verbatim}
   write (1,"(a20)") "structured grid data"            
c..NOT "unstr" or "UNSTR" as first 5 characters        
   write (1,*) icells+1,jcells+1                       
   write (1,*) U1(inf),U2(inf),U3(inf),U4(inf)         
c..Freestream state values                             
   do i=0,icells                                       
      do j=0,jcells                                    
         write (1,*) X(i,j),Y(i,j),                    
  +           U1(i,j),U2(i,j),U3(i,j),U4(i,j)          
      end do                                           
   end do                                              
   write (1,*) nBodies                                 
   do j=1,nBodies                                      
     write (1,*) nBodyFaces(j)                         
     do i=1,nBodyFaces(j)                              
       write (1,*) node1(i),node2(i)                   
     end do                                            
   end do                                              
c..Two nodes for face on body numbered as above        
   write (1,*) nBoundaryFaces                          
   do i=1,nBoundaryFaces                               
     write (1,*) node1(i),node2(i)                     
   end do                                              
c..Two nodes for face on boundary numbered as above    
\end{verbatim}
The unstructured is:
\begin{verbatim}
   write (1,"(a22)") "unstructured grid data"          
c..Needs "unstr" or "UNSTR" as first five characters   
   write (1,*) nCells                                  
   do i=1,nCells                                       
      write (1,*) nCorners,(Corner(j,i),j=1,nCorners)  
   end do                                              
   write (1,*) nNodes                                  
   write (1,*) U1(inf),U2(inf),U3(inf),U4(inf)         
c..Freestream state values                             
   do i=1,nNodes                                       
      write (1,*) X(i),Y(i),U1(i),U2(i),U3(i),U4(i)    
   end do                                              
   write (1,*) nBodies                                 
   do j=1,nBodies                                      
     write (1,*) nBodyFaces(j), bodyLabel(j)                         
     do i=1,nBodyFaces(j)                              
       write (1,*) node1(i),node2(i)                   
     end do                                            
   end do                                              
c..Two nodes for face on body numbered as above, and the element       
   write (1,*) nBoundaryFaces                          
   do i=1,nBoundaryFaces                               
     write (1,*) node1(i),node2(i),nElem(i)                     
   end do                                              
c..Two nodes for face on boundary numbered as above    
\end{verbatim}
For reasons of compatibility {\hip} expects the text label of the ``body''
(boundary group, patch) to start with a number. The boundary faces
need the face to node connectivity as well as the internal element
given. The ``boundary'' is in general not used (dplot calculates lift
and drag values only over bodies).

A sample unstructured file for the box-hyb 2D testcase shown
in figure \ref{fig:box-hyb} is given below. 
%
The boundary faces are listed consecutively and grouped into 
4 boundary 'patches' in the following order: 
\begin{enumerate}
\item bottom: faces 1+2,
\item right: faces 3,4,
\item top: faces 5-7,
\item left: faces 8-9.
\end{enumerate}
\begin{verbatim}
unstr
7
 4 1 2 5 4
 4 2 3 6 5
 4 4 5 8 7 
 4 5 6 9 8
 3 7 8 10
 3 8 9 11
 3 11 10 8
11
1. 7. 4. 86.
0. 0. .67 0. 0. 1.
1. 0. 1. 0. 0. 1.
2. 0. 1.33 0. 0. 1.
0. 1. 1. 0. 0. 1.
1. 1. 1. 0. 0. 1.
2. 1. 1. 0. 0. 1.
0. 2. 1.2 0. 0. 1.
1. 2 1. 0. 0. 1.
2. 2. .8 0. 0. 1.
.5 3.  1. 0. 0. 1.
1.5 3.  1. 0. 0. 1.
4
2 1: bottom
2 1 1
3 2 2
2 2: right
6 3 2
9 6 4
3 3: top
11 9 6
10 11 7
7 10 5
2 4: left
4 7 3
1 4 1
0 0
\end{verbatim}
\begin{figure}[htb]
  \begin{center}
     \input{boxhyb-annot}
%    \includegraphics[width=.6\textwidth]{box-hyb}
    \caption{2D hybrid box testcase. Numbers at nodes, cells and faces are node, cell and face numbers.}
    \label{fig:box-hyb}
  \end{center}
\end{figure}

{\hip} defines a 3D unstructured extension to this format:
\begin{verbatim}
c..Needs "uns3d" as first five characters   
   write (1,"(a22)") "uns3d"          
c..Vertices                             
   write (1,*) nNodes                                  
   do i=1,nNodes                                       
      write (1,*) X(i),Y(i),Z(i)
   end do
c..Elements                                              
   write (1,*) nCells                                  
   do i=1,nCells                                       
      write (1,*) nCorners,(Corner(j,i),j=1,nCorners)  
   end do         
c..Surfaces                                     
   write (1,*) nBodies, nAllBodyFaces                                 
   do j=1,nBodies                                      
     write (1,*) nBodyFaces(j), bodyLabel(j)                         
     do i=1,nBodyFaces(j)                              
       write (1,*) elem(i),face_in_elem(i)                   
     end do                                            
   end do                                              
\end{verbatim}
The 3D extension lists the coordinates first, then the connectivity.. 
For easy allocation, the total number of connectivity entries 
and of boundary faces have to be given. Each boundary face is specified by the
number of the internal element and the canonical face number 
(cf.~fig.~\ref{fig:canon}) in that element.
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=.6\textwidth,angle=90,clip=]{box-hyb-3d}
    \caption{3D hybrid box testcase.}
    \label{fig:box-hyb-3d}
  \end{center}
\end{figure}
The 3D dpl file for the grid of figure \ref{fig:box-hyb-3d} goes
as follows:
\begin{verbatim}
uNs3d        ! header
30           ! 30 nodes
0. 0. 0. 1   ! 30 coordinates
1. 0. 0. 2
2. 0. 0. 3
0. 1. 0. 4
1. 1. 0. 5
2. 1. 0. 6
0. 2. 0. 7
1. 2. 0. 8
2. 2. 0. 9
0. 0. 1. 10
1. 0. 1. 11
2. 0. 1. 12
0. 1. 1. 13
1. 1. 1. 14
2. 1. 1. 15
0. 2. 1. 16
1. 2. 1. 17
2. 2. 1. 18
0. 0. 2. 19
1. 0. 2. 20
2. 0. 2. 21
0. 1. 2. 22
1. 1. 2. 23
2. 1. 2. 24
0. 2. 2. 25
1. 2. 2. 26
2.1 2. 2. 27
3. 0. 2. 28
3. 1. 2. 29
3. 2. 2. 30
11 79                        ! 11 elements with 79 element to node pointers 
8 1 2 5 4 10 11 14 13 1      ! the nodes for each element
8 2 3 6 5 11 12 15 14 2
8 4 5 8 7 13 14 17 16 3
8 5 6 9 8 14 15 18 17 4
8 10 11 14 13 19 20 23 22 5
8 11 12 15 14 20 21 24 23 6
8 13 14 17 16 22 23 26 25 7
8 14 15 18 17 23 24 27 26 8
5 12 15 24 21 28 9
4 15 29 28 24 10
6 29 30 18 15 27 24 11
6 30        ! 6 boundary patches with 30 faces
4 1: -x     ! 1st patch: 4 faces, labeled: `1: -x'
1 4         ! face 4 of element 1
3 4         ! face 4 of element 3
5 4
7 4
5 2: +x     ! 2nd patch: 5 faces, labeled: `2: +x'
2 2
4 2
9 2
10 4
11 3
5 3: -y
1 1 
2 1
5 1 
6 1
9 5
5 4: +y
3 3
4 3
7 3
8 3
11 5
4 5: -z
1 5
2 5
3 5
4 5
7 6: +z
5 6
6 6 
7 6 
8 6
9 4
10 1
11 2
\end{verbatim}
A solution can also be specified:
\begin{verbatim}
30 5 prim      ! 30 vertices, 5 primitve unknowns each.
1. 1. 1. 1. 2. ! unknowns ..
[...]
\end{verbatim}

\subsubsection{read ensight casefile.case}

Read an unstructured grid (no solution) in Ensight Gold format. If node id's are
provided, they are read and used to piece the parts together (typically the ensight
format lists volumetric grid and boundaries in separate parts with their own
connectivity).
If node ids are not provided, the parts are reassembled using a geometric 
coincidence of nodes: any two nodes within a distance of epsOverlap to each
other are considered to be identical. This search requires extra memory and
runtime.

\subsubsection{read fluent file.cas [file.dat]}
      
Read an unstructured grid and optionally a solution 
in Fluent's v5 and v6 \index{Fluent} ascii and binary formats.
Typically, a mesh produced e.g.~by gambit will carry a 
{\it .msh} extension, while the mesh and computational
settings output from Fluent will carry a {\it .cas}
extension. The solution is a file with the {\it .dat}
extension. The user needs to supply the full filenames
with extension.


The Fluent format recognises a specific number of boundary condition
types which are encoded with numbers from 2 (interior) to 37 (axis).
Starting with version 1.14.1 {\hip} constructs the boundary name 
from this type tag as well as the zone number. This
should make it easy to assign distinct boundary patch labels. 

As of version 1.21 \hip also reads the Navier-Stokes variables
$\rho, u, v, w, p$ from the file. However, Fluent uses a cell-centred
discretisation and lists the unknowns for each cell. \hip 
translates this into a vertex-centred description by scattering
the cell-centred solution to the nodes. This is done by simple
arithmetic averaging: 
\begin{equation}
  u_{i,v-ctr} = \frac{\sum_{i\in j} u_{j,c-ctr}}{\sum_{i\in j} 1}
\end{equation}
where $i\in j$ refers to all elements $j$ formed with node $i$.

\subsubsection{read flbswap file.cas [file.dat]}

If the keyword 'flbswap' is provided, \hip performs a binary
little/big-endian swap for the binary data.

\subsubsection{read gmsh file.msh [var1.msh ....]}
\label{sec:gmsh.read}\index{gmsh}\index{read!gmsh}

Gmsh is a general purpose pre-processor, solver and
post-processor environment, allowing you basic CAD
definition, CAD import, meshing, finite-element solvers
and basic post-processing. 

Gmsh supports binary and ASCII in 4.x and 2.x and a binary format in its latest
version 2.0. Currently as of \hip version 22.03 only the ASCII formats are supported, binary
in development.

A mesh file must be given,
and include the MeshFormat, Nodes and Elements sections. If the section
PhysicalNames is present, any labels given there are respected.
The mesh can be followed by up
to MAX\_UNKNOWNS variable files (256 in \hip 1.41). The
first three must be density, velocity and pressure. 
As of version 1.40.2, \hip can also interpret {\it \$NodeData\$}
tags in the mesh files to find the solution.

Some older gmsh 2 formats don't carry boundary names. You can use
the commandline option\\
{\tt -bstringTag} to pre-pend the boundary number with a string
to make the boundary names clearer.

Gmsh 2 formats give the association of an element (boundary face or volumetric
element) through a number of 'tags'. By default, the fist tag is
the `physical entity', the second one is the `elementary geometric
entity'. When generating your mesh, you should define physical
entities, however if you haven't, gmsh will associate your
element with the underlying geometry entity.
Using the {\tt -tbcTagPos} option allows you to choose any of
the tags to link to your list of boundary entities. Default is 0,
i.e.~the first tag, `physical entity'.

When producing 2-D meshes, gmsh views them as faces that 
form the skin on or between meshes. Hence for gmsh the
orientation is arbitrary. To achieve the correct orientation for
meshing, you have to specify the surrounding curves to the
2-D surface in a counter-clockwise sense.
\index{gmsh!orientation}

With the 4.x format \hip interprets any entity tags given to volumetric
elements (i.e.~tri and quad in 2D, tet-hex in 3D) as zone indicators.
If there is more than one zone, \hip generates zones with the given labels
and associates the elements with those.

\subsubsection{read hdf5 gridfile [asciiBndFile [solFile]]}
\label{sec:read.hdf5}\index{hdf5!mesh}

The hdf5 format is the most versatile and should be the preferred
format. \index{hdf5!read} \index{read!hdf5}. 
%
The structure of the file is described in
Sec.~\ref{sec:hdf-file-structure}.  The format allows to express much
more grid information than \hip actually reads, e.g.~writing of faces
and boundary connectivity is possible. \hip reads from the grid file
the coordinates and volumetric element connectivity.

Boundary faces
are read in two different ways, depending on what records are present.
The preferred route is to read the boundary face to element pointers,
as this is what \hip internally stores.
If boundary faces are only defined through nodes, \hip
reads
the boundary face node lists and recomputes the face to element
pointers.
Any boundary face connectivity is ignored 
by \hip, however this is typically read
by the flow solver.
Periodicity is recreated from the boundary setup through the asciiBound
text file.

\hip can also read meshes if only boundary
nodes are given, e.g.~via the {\tt bnode-$>$node} and {\tt bnode\_lidx}
records in the hdf file format. \hip expects that every boundary node
appears multiply for each boundary patch that it is present on.

If present, \hip will read the record ``Boundary/PatchLabels''. This will
then supersede any labels stated in the deprecated asciiBound file. Good practice is
to use the PatchLabels field.
HDF5 supports different ways to express the length of strings. \hip supports
a constant length string, maximal length is 240 characters.
Currently supported are two methods to specify the end of the string,
see the HDF documentation on types
\href
{https://docs.hdfgroup.org/hdf5/develop/group___a_t_o_m.html#ga564b21cc269467c39f59462feb0d5903}
{here}:
\begin{description}
\item[H5T\_STR\_NULLTERM] As typical for C-style strings, the string is ended with
  a NULL character. Hence the length of the string in the hdf file is increased by 1.
\item[H5T\_STR\_NULLPAD] The string is un-ended and not padded with blanks to length,
  the length of the string is the length of all characters.
\end{description}

The following options are recognised with {\tt read hdf}:
\begin{description}
\item[-a] 
By default \hip reads only the standard variables set out in
Sec.~\ref{sec:hdf-file-structure-sol}. Using the flag
\begin{verbatim}
    read hdf -a gridfile [asciiBndFile [solFile]]
\end{verbatim}
instructs \hip to read all components in
hdf solution files, including all scalars in the group
{\tt /Parameters}.
The names of the groups and the number of variables within the group
are arbitrary, but variable vectors have to be scalar (one component
at a time) and all variables have to have a length equivalent to the number
of nodes (cell-centred variables are not supported). .
However, \hip still expects group headings
at the top level, such as {\tt GaseousPhase}, and look for variables at
the top level in that group.

If you just want to obtain an overview of the sizes and variables 
in your grid, run 
\begin{verbatim}
    read hdf -i gridfile -s solfile
\end{verbatim}

%
The Navier-Stokes variables which, if
present, are expected as for the 
standard solution, namely $\rho, \rho u, \rho v, [\rho w,] \rho E$.

\item[-i] scan the mesh file and write out numbers of vertices, elements, etc.

\item[-n] provide a mesh name to identify the grid.

\item[-s solFile] If no asciiBound file is used, the solution file
must be specified with the -s option. This can be placed at the end
for legibility, e.g. {\tt read hdf5 meshFile -s solFile}. Note that
the use of the asciiBound file will become obsolete in the near future.

\item[-z0]  
\hip also reads zones in the hdf file, if present. If this is not
desired, this can be suppressed with
\begin{verbatim}
    read hdf -z0 gridfile asciiBndFile [solFile]
\end{verbatim}
Note that it has to be {\tt-z0} and not {\tt -z 0}, as the argument 
to the option is not required, and the item after the blank could
be the next non-optional argument.
Zones can also be deleted using the {\tt zone delete} command, see
Sec.~\ref{sec:zone}.

\end{description}



\subsubsection{read n3s gridfile \{solFile\}}

Read a hybrid grid in n3s \index{n3s} binary or ascii
format, optionally with solution. \index{unstructured!read|)} 

Two solution file formats are supported, an older one with fixed
variable indicator table $TAB(4,100)$, and one with a separate
size record $NV,ND$ specifying a variable size $TAB(NV,ND)$, written
as the transpose compared to the fixed size. \hip looks at the length
of the indicator line, if shorter than the minimal size for the
fixed size $TAB$, \hip assumes a variable size format.
Version string formation for this format is not documented and appears
rather random and variable, so \hip cannot test on this.

The solution can either be single-species, containing the
$u,v,w$ velocities, pressure $p$ and temperature $T$ and
density $\rho$. \hip
stores these variables in this order. By default \hip presents
them as type 'additionals', which suits the C3S post-processing
chain. You can use the {\tt var cat} command to set variables to
any type you so desire. 
Alternatively, it can be multi-species, in which case
additionally the species, but $c_p$ and $\gamma$ are
currently not read using {\tt n3s}, but all fields are read
using {\tt n3sa}, see below.

After reading the solution \hip can convert the provided set
of variables (u, v, w, p, T) into conservative ones using the
{\tt var conv} command, but in this case the variable types need to be 
converted from {\it add} to {\tt n-s}.

\subsubsection{read n3sa gridfile \{solFile\}}

The {\tt n3sa}\index{n3sa} is the same as {\tt read n3s}
except for the reading of the solution. In the {\tt n3sa}
variant all present solution fields are read. Any variable
fields additional to the ones described for {\tt n3s} above
are presented as 'additionals'. 
%
Currently only the hdf -a format is able to write out those
additional solution fields for further post-processing
of the solution.

%\subsubsection{read oxd inFile}
%
%{\hip} verifies whether inFile is in ox \index{ox!read} format and
%finds out what version it is. ox files have become obsolete and
%support for it might be removed at some point in the future.



\subsubsection{read saturne gridFile masterFile saveNr}

Read a grid in Saturne \index{Saturne} format. Saturne reads
the grid in an IDEAS format and the solution as a list of
Ensight data files given in a master file. The saveNr indicates
a instance of the convergence. 
\index{unstructured!read|)}

\subsubsection{Other Formats}

In older versions hip supported 'hydra' style .adf formats
using the ADF database format underlying CGNS. These formats
can be compiled in without any problems, if the the appropriate
CGNS and ADF libraries are provided. See the Makefile for
details.


An interface to DLR's database for the $\tau$-code also
exists, but needs to be compiled specially once the
proprietary database library has been put in place.

\subsection{Structured grid files}

\subsubsection{read cfdrc ibcFile, plot3dFile, \{ plot3dType, skip, blanking \} }

\index{structured!read|(}
This command reads a structured multiblock grid in cfdrc structured
format. The set of files is a file with the block coordinates in
plot3d format and an file describing the block connectivity.
This file can 
come in two variants, 
either a .ibc file written
by CFDRC \index{CFDRC} which describes how nodes on the block
surface match, 
or a .phys file written the ICEM \index{ICEM}
mesher which describes face matches. 
%
{\hip} properly detects which variant you
are offering.

The plot3d \index{Plot3D} file may have blanking information for the
nodes (default is no blanking) 
and can be formatted or unformatted.  The file type for the
plot3d file is ascii or binary, default being ascii. 
In the unformatted case, {\hip} automatically
determines whether the file is single or double precision.  Skip
indicates the next node to be retained in each direction, i.e.~skip=2
means every other node is skipped.


A sample 4-block 
M6 grid can be found on on the NASA wind website% 
\footnote{https://www.grc.nasa.gov/WWW/wind/valid/m6wing/m6wing01/m6wing01.html}.
It's blocking is 
\begin{table}
  \centering
  \begin{tabular}{|c|c|c|}
\hline
   block no & dimensions & total nodes \\
\hline
1 & 25 x 49 x 33 & 40425 \\
2 & 73 x 49 x 33 & 118041 \\
3 & 73 x 49 x 33 & 118041 \\
4 & 25 x 49 x 33 & 40425 \\
\hline
  \end{tabular}
  \caption{M6 grid blocking}
  \label{tab:mg-blocks}
\end{table}
A corresponding 
.ibc file would be 
\begin{verbatim}
! Version 3 % ibc file tag
! comment line
12 ! number of nblock interfaces, listed twice, once for each side.
 1 1   1 25  1 49 33 33 4 % kmax  ! nrSubFc, nBlLeft, ijkLo, ijkHi, ijkLo+1, ijkHi+1, ijkLo+2, ijkHi+2, nBlRight
 1 4   1 25  1 49 33 33 1 % kmax
 2 2   1 73  1 49 33 33 3 % kmax
 2 3   1 73  1 49 33 33 2 % kmax
 3 1   1 25  1  1  1 33 4 % j1
 3 4   1 25  1  1  1 33 1 % j1
 4 1  25 25  1 49  1 33 2 % imax
 4 2   1  1  1 49  1 33 1 % i1
 5 2  73 73  1 49  1 33 3 % imax
 5 3   1  1  1 49  1 33 2 % i1
 6 3  73 73  1 49  1 33 4 % imax
 6 4   1  1  1 49  1 33 3 % i1
12 % number of boundary subfaces \n     % nrSubFc, nBlLeft, ijkLo, ijkHi, ijkLo+1, ijkHi+1, ijkLo+2, ijkHi+2, bcLabel
 7 2  1 73  1  1  1 33 lower_wing
 8 3  1 73  1  1  1 33 upper_wing  
 9 1  1 25  1 49  1  1 symm
10 2  1 73  1 49  1  1 symm
11 3  1 73  1 49  1  1 symm
12 4  1 25  1 49  1  1 symm
13 1  1  1  1 49  1 33 outflow
14 4 25 25  1 49  1 33 outflow
15 1  1 25 49 49  1 33 far_field
16 2  1 73 49 49  1 33 far_field
17 3  1 73 49 49  1 33 far_field
18 4  1 25 49 49  1 33 far_field
\end{verbatim}



\iffalse
\subsubsection{read dlr-flower logicFile, gridFile, \{ gridFileType, skip\}}

Read a multiblock grid in DLR's Flower \index{DLR} \index{Flower}
format. The gridfile type can
be ascii or binary. Skip
indicates the next node to be retained in each direction, i.e.~skip=2
means every other node is skipped.
\fi
\subsubsection{read dpl inFile}

The structured variant of the 2D dpl \index{dpl!read} 
file is described in the unstructured
section \ref{sec:uns-dpl}.

% \subsubsection{read damas file \{skip\}}

% Read a multiblock grid from A\'erospatiale's DAMAS \index{DAMAS} database. Skip
% indicates the next node to be retained in each direction, i.e.~skip=2
% means every other node is skipped. Note that by default {\hip} is compiled
% with -UDAMAS. Edit your makefile and recompile if you happen to find
% a DAMAS database.

\subsubsection{read mcgns file \{skip, blockNo\}}

Read a multiblock cgns file. Connectivity has to be given as.
1to1. If given, periodicity is read from the interface definition.
Boundary conditions are read directly or via families.

If a skip is specified, e.g. 2 or 4, then only every n-th node
is retained.

If a blockNo is given, then only that block will be written,
all its internal interfaces will be converted to boundaries.
This can be used to debug multi-block connectivity.

Currently (version 18.05) the solution is not read.



\cedp
