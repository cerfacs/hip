\section{Setting parameters}
  \label{sec:set}

{\hip} is set up to allow you to work with minimal knowledge of the
things going on behind the screen. Default values were chosen for most
of the parameters that work well in general situations. Thus, a novice
user should not need to change any of these parameters, except a few
general ones. Feel free to gloss over any of the sections after
\ref{sec:set-bc}.

In general, the option label is significant up to the first two
letters after the first dash. If a set command is issued without
a parameter value, the corresponding parameter is reset to its default
value.

\subsection{General parameters}
  \label{sec:set-gen}

\begin{description}
  \item[\tt set:]
     list all set-able parameters and their current values
   \item[\tt set verbosity $<$ {[verbosity]} $>$:] Have you
     ever met Eddie? 0 is minimal, 5 is maximal output. From
     version 1.21 onwards \hip strives to give truly zero
     output under zero verbosity to allow running e.g.~under
     a GUI. This is currently implemented for the main body,
     setting parameters, reading and writing hdf as well as
     volume interpolation. \hip will abort under fatal
     errors and warnings, error log files are written to the
     current directory.
  \item[\tt set current $<$ grid-nr.~$>$:]
     make grid {\tt grid-nr.}~the current grid. A lot of operations apply to
     the current grid. A read or a copy automatically changes the
     current grid to the read/copied one.
  \item[\tt set path $<$ {[path]} $>$:]
     pre-pend a {\tt path} to all relative filenames. With no argument, the
     current path is cleared.
   \item[\tt set gridname $<$ expr, name $>$:]
     \index{gridName}\index{grid!name}
     set the name of grid matching {\tt expr}~to {\tt name}.
     Very useful if meshes are
     glued together or if the mesh format has a name label.\\
     If only a single argument is given,
     {\tt set gridname $<$ name $>$}, then the current grid is given the new {\tt name}.
  \item[\tt set epsoverlap $<$ {[$\varepsilon_{Overlap}$]} $>$:]
     set the disc for overlap. If no argument is given, it is set to
     $.9h_{Min},$ the smallest edge length of the current mesh. 
     \index{epsOverlap} \index{epsilon@$\varepsilon$} 
     Note that $\varepsilon_{Overlap}$ is assigned after the reading of a mesh to
     $.9h_{Min}$. This can lead to problems when reading a second mesh
     with a smaller $h_{Min}$. {\hip} then considers edges of that length
     to be a degenerate. In these cases $\varepsilon_{Overlap}$ needs to be reassigned
     a value less than $h_{Min}$ before reading the second mesh.
   \item[\tt set vo-abort $<$ 0,1 $>$:] 
     if set to 1, \hip will abort upon encountering negative volumes.
     Default is 1.
   \item[\tt set vo-flip $<$ 0,1 $>$:] if set to 1, \hip will flip
     elements of a particular type to reverse negative volumes if all
     elements of this type have a negative volume. Currently (\hip 1.48,
     March 2016), this is only applied when reading ensight
     meshes to work around a bug in the fluent-ensight translation chain.
     Default is 1.
   \item[\tt set topology
     $<$ {[\{noTopo, axiX, axiY, axiZ, noBc, surf \}]} $>$
     $<$ {[\{grid\}]} $>$:]
     Treat the grid as a special topology. {\hip} knows about:
     \begin{description}
       \item[\tt axiX,Y,Z:]{ an axisymmetric \index{axi-symmetric}
           case with rotational periodicity
           annular sector or complete cylinder around the
           $x,y,z$-Axis.
           In addition to a Cartesian bounding box,
           \hip will compute the bounding box
           also in the cylindrical coordinates around that axis. 
           In AVBP-\index{AVBP}
           %and hdf\index{hdf} formats, JDM: also in hdf?
           the points
         on the axis will not appear in the list of periodic nodes but be
         listed separately at the end of the .exBound file.}

         The topology is automatically set to axiX if periodicity with a rotation
         is found and there are vertices on the x-Axis. 
       \item[\tt noTopo:] default, no special topology.
       \item[\tt noBc:] The hull of boundaries is not complete,
         hence no connectivity tests are undertaken.
       \item[\tt surf:] The mesh is 3D, but only a surface mesh.
       \item[\tt grid:] If a boundary match is specified, i.e.~either
         a number or a text with wildcard, then the topology is
         applied to the first matching grid. Default, if no
         such argument is given, it is applied to the current grid.
         If the grid is specified as '0', i.e.~a non-existent
         grid number, it is applied to the next grid to be read.
         This is useful to inform the mesh checking that is
         automatically conducted at the end of each read.
     \end{description}
  \item[\tt set symmetry-coordinate $<$ {[coor-index]} $>$:]. 
    Define the symmetry plane. Default is y=0, i.e.~1. Specifying x, y or z
    should work as well.
  \item[\tt set checklvl $<$ {[ check\_level]} $>$:]
     If set to 5, all tests for the validity of a mesh are being conducted
     after each operation that could affect the connectivity.
     In particular, level 5 forces a  face connectivity 
     check which involves large memory and run-time. Level 4 forces a 
     test for positive element volumes.  Default value is 5. See section 
     \ref{sec:check} for more detail.
     \index{check}\index{validity tests}

 \end{description}

 \subsection{Hypervolumes}
 \label{sec:hypervolume}
 \index{hypervolume}\index{splitting!boundary patch}
   The set of commands
   {\tt set hypervolume $<$ {[ params ]} $>$:}
   adds a hypervolume
     to the current unstructured grid e.g.to split a boundary patch in
     two parts inside and outside of the hypervolume
     see e.g.~\secref{sec:split_bcPatch}.  

     In particular:
     \begin{description}
     \item[\tt set hypervolume:]
       Without arguments the current hypervolume (if any) is removed.
     \item[{\tt set hypervolume box llx lly [llz] urx ury [urz]:}]
       Define a Cartesian box from llxyz to urxyz. 
     \item[{\tt set hypervolume plane nx ny [nz] x0 y0 [z0]:}] Define
       a plane through xyz0 with normal nxyz. 'inside' the hypervolume
       is interpreted here as 'below' the plane with the normal
       pointing to the upper half
     \item[{\tt set hypervolume cylinder rad nx ny [nz] x0 y0 [z0]:}]
       Define an infinite cylinder of radius rad with axis nx, ny, [nz]
       through point xyz0.
     \item[{\tt set hypervolume sphere rad x0 y0 [z0]:}]
       Define a sphere of radius rad with origin xyz0.
     \end{description}
     
     Any dimensions of vectors, coordinates depend on the number of
     dimensions of the grid the hypervolume is attached to.
     
     Run {\tt help set general} to see the default values for the hyperplane/volume
     parameters.

\subsection{Boundary parameters}

Boundary conditions are global groups, i.e.~their attributes span
all grids. The key parameter is the label, or 'text', which
uniquely identifies it. 

Attributes of boundary conditions such as e.g.~their name/label or 
their order used to be modified with the command {\tt set bc-attrib}, 
where {\tt attrib} stands for the attribute as listed below. 
Two characters after {\tt bc-} are sufficient. 

This is now deprecated (as of \hip 20.4.1) and will be removed in
the future. Instead use the {\tt bc attribute} syntax explained
in \secref{sec:set-bc}
\subsection{Edge parameters}
  \label{sec:set-edge}

\begin{description}
 \item[\tt set lp-tolerance $<$ {[lp\_tolerance]} $>$:]
     set a percentage for {\tt lp\_tolerance}. LP \index{linearity preservation}
     stands for linearity preservation; any linear function is captured
     exactly on an arbitrary unstructured mesh. 
     
     The set of weights that {\hip} calculates for the {\tt adfh} and
     {\tt ox} formats can be made LP.  \index{adf!write} \index{ox!write}
      \index{write!adf} \index{write!ox} LP requires the addition of
     symmetric edges to the list of edges and is quite a costly
     option: currently an LP grid on a hexahedral mesh has about 3
     times the number of edges of a non-LP one. 
     
     If no argument is given, it is reset to 1.e25, thus LP is not
     enforced.  On writing the weights to file, {\hip} checks whether all
     calculated gradients are within this tolerance.
 \item[\tt set lp-sweeps $<$ {[lp\_sweeps]} $>$:]
     if {\tt lp-sweeps} is non-zero, all elements will be fixed up to be lp.
     Default is 0.
 \item[\tt set edge-weight-cutoff $<$ {[cutoff]} $>$:]
     edge weights are listed only if the norm is above {\tt cutoff}.
     Default is 1.e-15. Elemental edges, the ones that form the elements,
     will always be listed.
\end{description}

\subsection{Multigrid parameters}

See section \ref{sec:mg} for a list of the pertinent parameters.

\subsection{Multiblock parameters}

\begin{description}
  \item[\tt set mb-degen-find $<$ {[mb-degen-find]} $>$]:
     To find degenerate block faces, {\hip} usually relies on either
     no neighboring block and no boundary condition given or
     a boundary tag labeled hip\_mb\_degen.
     If {\tt mb-degen-find} is switched on, any block face with edges
     shorter than $\varepsilon_{Overlap}$ is considered degenerate. This 
     requires a-priori knowledge of the smallest mesh length
     and/or the largest roundoff error on degenerate faces.
     By default it is switched off, 0.
\end{description}

\subsection{Normals}

See \ref{sec:boundary-normals} for when normals are written
and how boundary normals are changed due to presence of
sharp corners or periodicity. Briefly the settable parameters
are

\begin{description}
  \item[\tt set no-angle $<$ {[nodeAngleCut]} $>$:]
     {\tt nodeAngleCut} is the cosine of the maximum angle that face
     normals around are allowed to vary. If the angle is larger, the
     node normal is set to zero. Default is .9.
   \item [\tt set no-single $<$ [0,1,2] $>$:] This parameter can
     take 3 values: 0, 1, or 2.\\ 
%     
     This is an option for AVBP and hdf files, AVBP recalculates the
     components of all normals, but hip provides the list of
     normals and their ordering by patch. Hence these
     options do affect the boundary definition.


     The standard behaviour and the default value is 0. In
     this case a boundary node in a corner hence which is
     referenced from different patches will appear with each
     of those patches. The normal in each patch will be averaged
     taking into account only the boundary faces on the patch. 

     When set to 1 only a single normal for each
     boundary node is written rather than one for each patch
     the node is referenced by. {\hip} assigns each boundary
     node to the first boundary encountered with it, the one
     with the lowest number. The order of the patches can
     be chosen using the command {\tt set bc-order} described
     in section \ref{sec:set-bc}.

     When set to 2, the first occurrence rule is only applied
     to patches that are given the type wall (either i,v,w)
     using the {\tt set bc-type} command described
     in section \ref{sec:set-bc}. E.g.~a boundary node
     belonging to the first patch (wall), the second patch (wall)
     and the third patch (say, outlet) will appear with the
     first and the third patch.

     Note that the setting of {\tt no-single} does not affect
     the listing of multiply-referenced boundary nodes in the
     AVBP .exBound file (see sec.~\ref{sec:boundary-normals}).
\end{description}

\subsection{Interpolation}
\index{interpolation!parameters}

The parameters are briefly described below, see \secref{sec:interpol} 
for further details about algorithms and parameters..
%
\hip offers a number of interpolation algorithms, the recommended
choice is `minnorm':
\begin{description}
\item [\tt set in-recoType $<$ recoType $>$]
  Set the type of reconstruction. Currently available are
  \begin{description}
  \item[\tt minnorm]: minimum norm solution reconstruction,
    \secref{sec:minnorm}.
  \item[\tt el]: linear discontinuous reconstruction within an element,
    \secref{sec:pcw-lin}
  \item[\tt 1,2]: first and second order least-squares reconstruction,
    \secref{sec:int-ls}.
  \item[\tt flag]: not really an interpolation: find the value at
    the nearest point. If it is non-zero use that, subject to
    the same rim behaviour as interpolation, see \secref{sec:int-flag}.
  \end{description}
  The  default value is `minnorm`.
\end{description}


\noindent
 Parameters for element-based interpolation such as piece-wise linear
or minnorm are:
 \begin{description}
 \item [\tt set in-rim $< f_{rim} >$]
     If both the providing and receiving grid have a variable of
     the same name, \hip can do a partial interpolation for this where
     only the part covered by the providing domain will be updated.
     This includes a rim around the providing domain of $f_{rim}*h_{Edge}$.
     where $h_{Edge}$ is the longest edge length of the nearest face in the
     providing mesh.\\
     The extraplated value is limited to not exceed the values
     found in the nearest cell. In practice this results in using the
     value of the nearest point.\\
     The default value is $f_{rim}=\infty$, i.e.~extrapolation to everywhere.
   \item[\tt  set in-fc-tol $< tol_{fc} >$]
     When searching for the containg cell to interpolate on, this face
     tolerance controls how far a node can be away from the closest face
     and still be considered inside the cell.
     The factor $tol_{fc}$ multiplies a typical length scale of the element,
     e.g. hElem which is the longest edge around the face.\\
     The Default value is $tol_{fc}=0.1$.
   \item[\tt set in-full-search\_dist $< dist_{full} >$]
     \hip searches for the containing element by walking on the donor
     grid from element to element, starting from the nearest point.
     On distorted meshes, or thin-walled geometries, this walk may get
     stuck before reaching a containing element.
     If the final element does not actually contain the target point,
     but is within a distance of $dist_{full}*h_{Elem}$ of any of its forming
     nodes, then \hip will perform a global search and compute the
     exact distance to the nearest face, not just the node.
     This can get very expensive, especially if the domains do not
     overlap. \\
     The default value is  $dist_{full}=0$, no refined global search is done.
     The value of $dist_{full}=0$ is augmented by $dist_{full} \mathrel{+}= 2.0$ when computing
     inter-grid interpolation coefficients for multi-grid, as higher
     accuracy is required there. 
\end{description}
     
\noindent
 Parameters for least-squares interpolation are:
 \begin{description}
 \item[\tt set in-nr $< f_{red} >$]
     Sets the factor of redundancy of the least-squares stencil.
     Default is $f_{red}=1.5$, ie for a 2-D quadratic reco 6 coefficients need
     to be determined and 1.5*6=9 nodes are used.
   \item[\tt set in-lambda $< lambda >$]
     A factor that scales the distance used for the weighting.
     Default is $\lambda=1$., larger values of $lambda$ make the weighting more even.
     At $\lambda \geq \meten{25}$ all weights are set to unity.
   \item[\tt  set in-distance-inside $<dist_{rim}>$]
     Similar to in-rim this sets the factor for the overlap for least-
     squares interpolation. In this case, interpolation is based on
     point clouds rather than element containment, hence the width of
     the overlap rim is computed as $dist_{rim}\epsilon_{overlap}$.\\
     The default value for this factor is $dist_{rim}=infinity$, so extrapolation to
     the entire domain. Note however that, as opposed to element-based
     interpolation, this is indeed extrapolation, there is no limiting
     of the values. 
 \end{description}


\subsection{Periodicity}
  \label{sec:set-per}

Normally {\hip} should be able to detect how the patches fit together that
have been declared periodic pairs. For troubleshooting, have a look
at sections \ref{sec:boundary-normals} and \ref{sec:periodicity} for 
details.

\begin{description}
  \item[\tt set pe-fix $<$ {[\{0,1\}]} $>$ ]allows you to turn on/off the 
    forced match between periodic patches. If turned on, default, {\hip}
    will move periodic nodes on the outlet, upper patch on to the rotated
    position of the corresponding inlet, lower node.
%
  \item[\tt set pe-thresh-rot]  $<$ {[threshVal]} $>$
    \hip looks at the sum of the outward
    normals of matching periodic patch pairs to determine what the
    required periodicity operation is.

    In the case of translation, all the components of the sum of the
    normals of matching 'u' and 'l' patches is zero as they are
    perfectly opposed. 

    Rotations are expected along major axes $x,y,z$ hence in the sum of
    normals one component (the one along the axis) has to be
    zero, while the other components must be non-zero for both normals.
    The default value is 1e-2, but can be adjusted with {\tt set pe-thresh-rot}.
%
  \item[\tt set pe-write $<$ {[\{0,1\}]} $>$] allows you to turn on/off
    the writing of periodic boundary faces and nodes to the exBound file
    in the avbp format. The periodic patches will be listed in the
    appropriate order in either case, but if set to 0, the
    periodic patch will be empty in the exBound file. The default value
    is 1.
\end{description}

\subsection{Faces}
  \label{sec:faces}

After reading a mesh {\hip} checks on proper connectivity. That is done
by generating a list of all faces in the mesh and making sure that
each one is covered on one side by an element and on the other by
either an element or a boundary face. In order to do checks and
consistent operations, all meshes have to be ``watertight''. Thus,
also faces on periodic surfaces have to be specified to {\hip}, even
if the solver at hand deals with periodicity on a nodal basis.

{\hip} always warns about\index{face!unmatched} 
unmatched faces. The warnings on face 
duplication can be turned on or off using \\
{\tt set fc-warnings $<$ {[matching, cut, boundary, abort]} $>$}.\\
 Matching faces are faces
of boundaries marked for a match when glued together with another mesh 
(cf.~sec.~\ref{sec:glueing}). Cut faces are faces that are still held
by {\hip} after a cutting operation has taken place (cf.~sec.~\ref{sec:cutting}).
The final value determines whether or not \hip will abort if unmatched
faces are found.
Set to 1, 
the warning/flag is on, default is 1,1,1,1.


Similarly, {\hip} can be instructed what to do about 
\index{face!duplication} face duplication.
If\\
{\tt set fc-remove $<${[ matching, cut, boundary, listUnMatched]} $>$}\\
 is set to 1,1,1.
all duplicated matching, cut or boundary faces are removed. This is
the default. You will want to turn automatic face removal off if you
want to keep those internal boundary faces 
\index{face!internal} \index{boundary!internal} to be able to specify an
internal boundary condition.
If the fourth parameter for {\tt listUnMatched} is set to 1, \hip will
pack the unmatched faces into a new boundary labelled {\it hip\_unmatched}.
Default setting is 0,0,0,0.

Internal faces, e.g.~arising from glueing meshes together, can
be double-sided. See the description of
{\tt mark interface}, \secref{sec:mark_interface}, for how
to control whether none, one or both sides are written to file.


\subsection{Element degeneracies}

{\hip} has some capability to deal with degenerate elements as they often
occur when reading structured meshes.

\begin{description}
 \item[\tt set dg-coll $<$ {[\{1,0\}]} $>$:]
     Attempt to fix elements out of a multiblock grid that have
     collapsed edges/faces. Default is 0, dont do it.
 \item[\tt set dg-lrgAngle $<$ {[\{1,0\}]} $>$:]
     Try to cut hexahedra in two prisms to halve too large angles. Default is 0.
     {\hip} takes hexahedra with a negative face angle and tries to cut them intro
     prisms. This might generate hanging edges that only can be dealt with
     formats able to carry the \index{elMark}
     ``elMark''. Make sure, your solver can, too.
 \item[\tt set dg-angle $<$ {[maxAngle]} $>$:]
     Cosine of the threshold angle to cut, default is -.75.
\end{description}

\subsection{Adaption}
  \label{sec:ad-lv}

{\hip} can adapt a mesh to a solution using gradient sensors. See 
section~\ref{sec:adapt} for more details.


\cedp
