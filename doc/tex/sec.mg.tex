\section{Coarsening Meshes for Multigrid and Other Applications}
  \label{sec:mg}

\subsection{Coarsening algorithm and parameters}

{\hip} can coarsen \index{coarsen} a hybrid mesh for you for multigrid 
\index{multigrid} convergence acceleration.
{\hip} uses an element-collapsing algorithm with directional coarsening in
regions of mesh stretching \cite{Muller_Copper99}.
The algorithm is designed to preserve as much
structure as possible if the mesh is regular. The user is
able to trade mesh quality against coarsening ratio with certain parameters.
In general, the mesh quality slowly degrades away from the finest mesh
with each coarsening. Thus,  
if your initial mesh is sufficiently smooth, you might be able to 
use the first or second coarser levels as finest grids. However, don't
complain if your results are poor, the algorithm is not really meant for
that. 

{\hip} offers two variants of the algorithm. One option is 
to coarsen grids globally: length-scales are increased in the entire
mesh. This is useful in multi-grid applications and a new grid
is created for each level. Alternatively specific cells can be
coarsened, e.g.~to remove a few small cells which are limiting
for explicit time-stepping. In this case the cells are removed
in-situ, i.e.~no new grid level is generated.

\subsection{Coarsening through element-collapsing}
\index{element collapse}\index{collapse!element}
\label{sec:ell-coll}

{\hip} uses an element-collapsing algorithm: on hybrid grids a
set of edges of an element is collapsed simultaneously in order
to guarantee the disappearance of an element. For simplex elements
such as triangles or tetrahedra this is equivalent to 
edge-collapsing algorithms which work on one edge of the grid
at a time. 

Element-collapse tries to remove an entire element at a time.
In triangular and tetrahedral grids the collapse of an edge
removes all the elements that share the edge. All elements
that are formed with one of the end-nodes of the collapsed
edge are enlarged.
\begin{figure}[htb]
  \begin{center}
       \includegraphics[height=.8\textwidth,angle=90]{hex-collapse}
    \caption{Partially collapsed hexahedral elements.}
    \label{fig:hex-coll}
  \end{center}
  \vspace{-6mm}
\end{figure}

In non-simplex grids the collapse of an element affects the
neighbouring elements differently as the collapse of a single edge is
not sufficient to make an element disappear. Different types of
degenerate elements result from a partial collapse, some of which are
shown in Fig.~\ref{fig:hex-coll}. If a degenerate
element\index{element!degenerate}\index{degenerate element}
corresponds to a lower-rank primitive hip converts it into that
primitive, e.g.~a hexahedron with one face collapsed into a single
vertex becomes a pyramid.

To preserve features in the geometry {\hip} counts how many boundaries
a vertex is part of. E.g.~if a boundary edge is formed from a node on
the interior of the boundary patch (belongs to a single boundary) and
one that is on the seam between two patches (part of 2 bnd.), {\hip}
will contract the edge on to the node with the higher valence and
preserve the seam. In the same way, {\hip} detects geometrically sharp
angles. If a boundary node is interior to a patch but located on a
geometry feature\index{sharp angle}\index{geometry!feature} with a
sharp angle, it receives higher priority.  The cut-off angle is
adjusted using {\tt set no-angle}.

If the nodes have the same priority, e.g.~two boundary nodes
interior to the patch or two interior nodes, the edge
collapses to the mid-point.

\subsection{Removing individual elements}

Mesh collapsing can also be used to remove indvidual offending
elements. Currently the removal of elements is supported which are
below a user-specified volume threshold\index{volume!threshold}. The
syntax for this
option is
\begin{verbatim}
  mg volMin < threshold >
\end{verbatim}
or
\begin{verbatim}
  mg -v < threshold >
\end{verbatim}
Note that {\hip} does not provide a default value for 
the threshold\index{volMin}\index{minimal volume}.

\begin{figure}[htb]
  \centering
     \mbox{
       \includegraphics[width=.32\textwidth]{tri}
       \hfill                                                             
       \includegraphics[width=.32\textwidth]{tbox1}
       \hfill                                                             
       \includegraphics[width=.32\textwidth]{tbox2}
       }
  \caption{Example of a simplex grid with an unacceptably small 
     volume in the lower left corner (left). Two different volume thresholds
     are applied to the smallest (middle) or the 11 smallest elements (right).}
  \label{fig:tri-coll}
\end{figure}

There are a few caveats to point out:
\begin{itemize}
\item This option should work well on simplex grids. If applied to
  grids with other element types, the removal of the smallest cell
  will remove that cell, but also reduce the volume of the
  neighbouring cells which share an edge with it as they become
  degenerate (cf.~Fig.~\ref{fig:hex-coll}). Hence, applying an
  element collapse in hybrid grids might not actually lead to
  an increase of the smallest volume.
\item Most of the time the smallest element will be located on
  the boundary and will involve the collapsing of boundary edges.
  {\hip} does not know what surface you had in mind, it is not
  linked to a CAD database. 

  A boundary edge internal to a patch is collapsed onto its midpoint
  (see Sec.~\ref{sec:ell-coll}). Hence there will be a slight
  alteration of the boundary shape\index{boundary!shape}. Similarly, a
  boundary edge along a seam between boundary patches or along a sharp
  geometrical feature will be collapsed onto its
  midpoint\index{collapse!midpoint}\index{midpoint}.
\item Mesh decimation does lead to a degradation of the mesh
  quality\index{mesh!quality} as it is a purely local operation. That
  is, no smoothing or edge-swapping is applied to improve the mesh
  quality after the collapse.
\end{itemize}
It is up to the user to verify that the grid remains suitable
for computation after the collapsing operation. It is recommended
to use this option with caution.


\subsection{Multi-grid}
The syntax for multi-level coarsening for multi-grid\index{multi grid} is
\begin{verbatim}
  mg < [mLevels] >
\end{verbatim}
or
\begin{verbatim}
  mg -c < [mLevels] >
\end{verbatim}
to coarsen an unstructured grid by element-collapsing,
creating mLevels coarser levels. Default is 1.

Figure~\ref{fig:rae.grids} shows a coarsening sequence for a hybrid
grid around a RAE airfoil. Where the grid is isotropic, the triangular
part and the outer layer of the structured grid, coarsening is
omnidirectional.  In the highly stretched boundary layer the
coarsening is
directional\index{coarsening!directional}\index{directional
  coarsening}.
\begin{figure}[htb]
  \begin{center}
     \mbox{
       \includegraphics[width=.32\textwidth,height=.32\textwidth]{rae-le-0}
       \hfill                                                             
       \includegraphics[width=.32\textwidth,height=.32\textwidth]{rae-le-1}
       \hfill                                                             
       \includegraphics[width=.32\textwidth,height=.32\textwidth]{rae-le-2}
       }
    \caption{Coarsening a hybrid grid around a RAE airfoil. The finest and
             two coarser levels are shown.}
    \label{fig:rae.grids}
  \end{center}
  \vspace{-6mm}
\end{figure}


Grid distortion, however, limits the isotropic coarsening in the
vicinity of the boundary layer.  This effect can be observed in the
fan blade grid shown below.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=.48\textwidth]{bnd1}
    \includegraphics[width=.48\textwidth]{bnd2}
  \end{center}                                
  \begin{center}                              
    \includegraphics[width=.48\textwidth]{bnd3}
    \includegraphics[width=.48\textwidth]{bnd4}
    \caption{Four multigrid levels for a viscous fan blade mesh. Shown are
             hub, blade surfaces and the inlet plane.}
    \label{fig:ja63}
  \end{center}
\end{figure}

In general, the coarsened meshes contain elements with degenerate edges
that run between the same duplicated node. Higher order primitives
that are collapsed into a lower order primitive are converted. E.g~a
hexahedron that has two opposite edges on a quad face collapsed is
turned into a prism.

 
The element collapsing is governed by a few parameters, all initialised
to sensible defaults that work well in most situations. If you feel the
need to change any of these, two characters after mg- needed:
\begin{description}
\item[\tt set mg-length $<$ {[mg-length]} $>$:]
     set a maximum length increase during coarsening.
     If no argument is given, it is reset to 2.2. This is the major
     parameter to control the coarsening: the higher you set it, the
     coarser your mesh. 

     The length criterion does not actually constrain the length of
     the edges, but limits the number of fine grid nodes collapsed into
     the same coarse grid one to the square or the cube of this value
     in 2D/3D.
\item[\tt set mg-angle $<$ {[mg-angle]} $>$:]
     set a maximum cosine for the largest angle during coarsening.
     The angles considered here are the angles between face normals,
     as well as the angles between edges around a face.
     If no argument is given, it is reset to -.99. On genuinely 
     unstructured meshes, this limit is effectively controlling the
     coarsening if set to values away from $180^\circ$.
\item[\tt set mg-twist $<$ {[mg-twist]} $>$:]
     set a minimum permissible twist of quad faces. Twist is expressed
     as the scalar product of the two normals of a triangulation of the
     quad face. The smaller value of the two possible choices of
     diagonal is taken. Thus, 1. is a perfectly linear face, -1. is a
     face that is completely folder onto itself. Default: .2. Not a
     major parameter, it seems.
\item[\tt set mg-volAspect $<$ {[mg-volAspect]} $>$:]
     {\hip} uses a volumetric aspect ratio to determine whether a
     partly collapsed element is valid. This is aspect ratio is
     defined as the element volume divided by a reference volume.
     The reference is the volume of the smallest isotropic simplex
     built with the shortest edge of the element. mg-volAspect is the
     cutoff in fraction of this smallest volume. Default is .9
\item[\tt set mg-aspectRatio $<$ {[mg-aspectRatio]} $>$:]
     set a minimum aspect ratio that is to be considered stretched.
     Semi-coarsening is only applied in regions stretched more than this.
     Default is 2. Unfortunately, I cannot offer any expertise on how
     to play this value, yet. My gut feeling is that you will be fine
     up to values of 5.
\item[\tt set mg-ramp $<$ {[mgRamp]} $>$:]
     A factor to ramp the cutoff aspect-ratio and maximum angle with.
     This is done by multiplying the cutoff values with this factor.
     The angle still is limited by $179^\circ$.
     Default is 1., i.e. values remain constant.
\end{description}

\subsection{Inter-grid transfer coefficients}
\label{sec:transfer-coeff}
\index{transfer operators}\index{inter grid!operators}

Inter-grid transfer operators are computed from fine to coarse grid
by a) finding a coarse grid element that contains the fine grid vertex,
and b) computing the minimum norm interpolation coefficients for
the coordinates. The minnorm interpolation is described in 
\secref{sec:minnorm}.

\index{multi grid!independently coarsened sequence}
{\hip} will compute inter-grid transfer coefficients for restriction
from fine to coarse or prolongation from coarse to fine, see 
\secref{sec:transfer-coeff}. These coefficients can also be
computed for independently generated grid sequences using 
\begin{verbatim}
  mg sequence $<$ grid_1 grid_2 ... $>$
\end{verbatim}
or 
\begin{verbatim}
  mg -s $<$ grid_1 grid_2 ... $>$
\end{verbatim}

where {\tt grid\_1} refers to the number of the finest grid,
 {\tt grid\_2} refers to the number of the next coarse grid, etc.

\subsection{Testing}
\index{mg!testing}
To check the interpolation coefficients, \\
{\tt mg t nLvl0 nLvl1}\\
equips nLvl0 with a sample solution of
\begin{align*}
  \rho = 1. \\
  u = x \\
  v = y \\
  w = z \\
  p = xy + yz + zx
\end{align*}
If $ n_{lvl,o} < n_{lvl,1}$ restriction, otherwise prolongation is tested. The
solution can then be written to file using the gmsh format.

\subsection{Writing coarsened grids to file}
\index{write!coarsened}\index{inter grid!connectivity}\index{multi grid!connectivity}
Coarsening a simplex mesh in-situ produces a single grid which can be written 
with any file format. Coarsening a hybrid mesh in-situ can produce degenerate elements,
so requires a solver that can tolerate degenerate elements, e.g.~a hexahedron 
with a collapsed edge. 

In some cases only a single level of the coarsened grid sequence
is desired. This can be written to AVPB format as \\
{\tt write avbp < rootFile, level >}, \\
or hdf as\\
{\tt write hdf -l nLvl < rootFile >}.


Not all file formats can accept a set of coarsened grids. Currently only
the dpl, gmsh and hdf formats support writing the inter-grid connectivity.
Only the hdf format supports writing out the coefficients of the 
transfer operators.


\cleardoublepage
