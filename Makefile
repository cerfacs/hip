#include ./host/$(HIP_HOSTTYPE)/makefile.h
HIP_DIR=./src
PYHIP_DIR=./pyhip
PREFIX=.

.PHONY: all

.DEFAULT: all

all: hip pyhip

pyhip: hip
	@echo "building pyHIP ..."
	@cd $(PYHIP_DIR) && python3 setup.py bdist_wheel && pip install -e .

wheel:
	@rm -rf $(PYHIP_DIR)/dist
	@rm -rf $(PYHIP_DIR)/build
	@cd $(PYHIP_DIR) && python3 setup.py bdist_wheel

upload:
	@cd $(PYHIP_DIR) && twine upload --repository testpypi --verbose dist/*.whl && twine upload --repository pypi --verbose dist/*.whl

test: pyhip
	@cd $(PYHIP_DIR) && PYTHONDONTWRITEBYTECODE=1 pytest -vv tests

.PHONY: init test

hip:
	@cd $(HIP_DIR) && make version install

clean_hip:
	@echo "Cleaning HIP.... "
	@cd $(HIP_DIR) && make clean
	@rm -rf $(PREFIX)/lib $(PREFIX)/bin

clean_hippy:
	@echo "Cleaning HIPPY.... "
	@rm -rf $(PYHIP_DIR)/dist
	@rm -rf $(PYHIP_DIR)/build
	@rm -rf $(PYHIP_DIR)/hip.c*
	@rm -rf $(PYHIP_DIR)/.eggs
	@rm -rf $(PYHIP_DIR)/src/hip.exe
	@echo "Removing $(PREFIX)/lib $(PREFIX)/bin"
	@rm -rf $(PREFIX)/lib $(PREFIX)/bin
	@echo " "

clean: clean_hip clean_hippy

install: hip hippy
