#include <getopt.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define LINUX
#ifdef LINUX
#  define FREAD fread_linux2
#  define FWRITE fwrite_linux2
#else
#  define FREAD fread
#  define FWRITE fwrite
#endif

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#define ABS(a)   ( (a) >  0  ? (a) : (-a) )

#define MAX_CHAR 256

const char version[] = "4.0, 4 April 2014" ;
/*
  Last update:
  -----------
  4 Apr 14; add byteSwap and %*

  const char version[] = "3.0, 20 April 2004" ;
*/
void print_version () {
  printf ( " version %s\n", version ) ;
  exit ( 0 ) ;
}

void usage () {

  printf ( " binread prints the values in a FORTRAN style binary file\n"
           " usage: [options] filename \n"
           " where: filename is the binary file to read\n"
           " Options are:\n"
           "  -b     byteSwap all numbers,\n"
           "  -f mF  is the number of ints to list at the beginning,\n"
           "  -l mL  is the number of ints to list at the end,\n"
           "  -d toD is a size of integer which is likely to be the\n"
           "            first 4 bytes of a double.\n"
           " -r recNo prints only the line (record) recNo.\n"
           " -s fmtString specifies a c-style format for that line,\n"
           "    e.g. -s%%'*'3i%%2i%%12c%%d%%f\n"
           "      skips 3 int, reads 2 int, a 12 char string, a double\n"
           "      and a float(real).\n"
           "    Note that the * has to be escaped on the cmd line as '*'\n"
           " -v prints the version number.\n"
           " -? prints this help.\n" ) ;
  exit ( 0 ) ;
}

void exit_feof ( ) {

  printf ( " (EOF)\n" ) ;
  exit ( 0 ) ;
}

/******************************************************************************

  fread_linux:
  Convert a high-endian read into a binary low-endian.

  Last update:
  ------------
  : conceived.

  Input:
  ------
  pTo        Just as in fread.
  size
  mItems
  binFile

  Changes To:
  -----------
  pTo
  binFile

  Returns:
  --------
  0 on failure, 1 on success.

*/

/* Use fread_linux out of lib_do. */
int fread_linux2 ( void* pTo, size_t size, int mItems, FILE *binFile ) {

  char swap, *pData ;
  int returnVal, k ;

  /* Read the data from the file. */
  returnVal = fread ( pTo, size, mItems, binFile ) ;

  if ( !returnVal )
    /* fread failed. */
    return ( returnVal ) ;
  if ( size%2 )
    /* Size is odd. No swapping. The only case I could see is 1 for char. */
    return ( returnVal ) ;
  else {
    /* Perform swapping. */
    for ( pData = ( char * ) pTo ;
          pData < ( char * ) pTo + size*mItems ; pData += size )
      for ( k = 0 ; k < size/2 ; k++ ) {
        swap = pData[size-k-1] ;
        pData[size-k-1] = pData[k] ;
        pData[k] = swap ;
      }
    return ( returnVal ) ;
  }
}

int fread_bswap ( int byteSwap,
                  void* pTo, size_t size, int mItems, FILE *binFile ) {

  if ( byteSwap )
    return ( fread_linux2 ( pTo, size, mItems, binFile ) ) ;
  else
    return (fread ( pTo, size, mItems, binFile ) ) ;
}

void bswap ( void *pTo, size_t size, int mItems ) {

  char swap, *pData ;
  int k ;

  for ( pData = ( char * ) pTo ;
        pData < ( char * ) pTo + size*mItems ; pData += size )
    for ( k = 0 ; k < size/2 ; k++ ) {
      swap = pData[size-k-1] ;
      pData[size-k-1] = pData[k] ;
      pData[k] = swap ;
    }

  return ;
}


int seems_dbl ( int twoInts[2], int toDbl ) {

  if ( ABS( twoInts[0] ) > toDbl || ABS( twoInts[1] ) > toDbl ) {
    return ( 1 ) ;
  }
  else
    return ( 0 ) ;
}


void printvals ( FILE* binFile, int byteSwap, const int toDbl ) {

  int twoInts[2] ;

  fread_bswap ( byteSwap,  twoInts, sizeof( double ), 1, binFile ) ;

  if ( feof ( binFile ) )
    exit_feof () ;

  if ( seems_dbl ( twoInts, toDbl ) ) {
  printf ( " %12.5e", *(( double *) twoInts) ) ;
    return ;
  }

  if ( byteSwap ) {
    /* fread_bswap converted a double, but it seems it isn't. Convert back
       to the file version, and swap two ints. */
    bswap ( twoInts, sizeof ( double ), 1 ) ;
    bswap ( twoInts, sizeof ( int ),    2 ) ;
  }

  printf ( " %d %d", twoInts[0], twoInts[1] ) ;
 return ;
}

void printValsFmt ( FILE* binFile, int byteSwap, const char *fmt ) {

  const char *pF = fmt ;
  int skip = 0 ;
  int mVals, i, iVal ;
  char tVal, cVal ;
  float fVal ;
  double dVal ;

  while ( *pF == '%' ) {

    if ( pF[1] == '*' ) {
      // read the specified elements, but don't print.
      skip = 1 ;
      pF++ ;
    }
    else
      skip = 0 ;


    /* What is the next format? */
    if ( sscanf ( pF+1, "%i%c", &mVals, &tVal ) != 2 ) {
      /* Try without quantifier. */
      mVals = 1 ;
      if ( !sscanf ( pF+1, "%c", &tVal ) )
        break ;
    }



    switch ( tVal ) {
    case 'c':
      /* Characters. */
      printf ( " " ) ;
      for ( i = 0 ; i < mVals ; i++ ) {
        fread_bswap ( byteSwap,  &cVal, sizeof( char ), 1, binFile ) ;
        if ( !skip ) printf ( "%c", cVal) ;
      }
      break ;
    case 'i':
      /* Integers. */
      for ( i = 0 ; i < mVals ; i++ ) {
        fread_bswap ( byteSwap,  &iVal, sizeof( int ), 1, binFile ) ;
        if ( !skip ) printf ( " %d", iVal) ;
      }
      break ;
    case 'f':
      /* Float. */
      for ( i = 0 ; i < mVals ; i++ ) {
        fread_bswap ( byteSwap,  &fVal, sizeof( float ), 1, binFile ) ;
        if ( !skip ) printf ( " % 12.5e", fVal) ;
      }
      break ;
    case 'd':
      /* Double */
      for ( i = 0 ; i < mVals ; i++ ) {
        fread_bswap ( byteSwap,  &dVal, sizeof( double ), 1, binFile ) ;
        if ( !skip ) printf ( " % 12.5e", dVal) ;
      }
      break ;
    }

    /* Skip to the next format group. */
    pF++ ;
    for ( pF++ ; pF-fmt < MAX_CHAR ; pF++ )
      if ( *pF == '%' )
        break ;
  }
  return ;
}

int is_all_char ( FILE* binFile, int byteSwap, int mBytes ) {

#define MAX_CHAR 256
  int n, c ;
  char someChar[MAX_CHAR+1] ;


  if ( mBytes > MAX_CHAR )
    /* Too long to be printed as char. */
    return ( 0 ) ;

  fread_bswap ( byteSwap,  someChar, 1, mBytes, binFile ) ;
  fseek ( binFile, -mBytes, SEEK_CUR ) ;

  for ( n = 0 ; n < mBytes ; n++ ) {
    c = someChar[n] ;
    if ( !isprint( c ) )
      return ( 0 ) ;
  }

  return ( 1 ) ;
}

void print_as_char ( FILE* binFile, int byteSwap, int mBytes ) {

  char someChar [MAX_CHAR+1] ;


  if ( mBytes > MAX_CHAR )
    /* Too long to be printed as char. */
    return ;

  fread_bswap ( byteSwap,  someChar, 1, mBytes, binFile ) ;
  if ( feof ( binFile ) )
    exit_feof () ;

  someChar[mBytes] = '\0' ;
  printf ( " '%s'", someChar ) ;

  return ;
}



int main( int argc, char *argv[] ) {

  extern char *optarg ;
  extern int optind ;

  const int intSize = sizeof( int ) ;

  int byteSwap=0, mF = 4, mL = 4, toDbl = 10000000, r = 0 ;
  int c, mBytes, mInt, nInt, intRead, mLines = 0, skip ;
  char fmt [MAX_CHAR+1] = "\0" ;

  FILE *binFile ;
  fpos_t fPos ;


  while ((c = getopt(argc, argv, "bf:l:d:r:s:vh")) != -1)
    switch (c) {
    case 'b':
      byteSwap = 1 ;
      break ;
    case 'f':
      mF = atoi ( optarg ) ;
      break ;
    case 'l':
      mL = atoi ( optarg ) ;
      break ;
    case 'd':
      toDbl = atoi ( optarg ) ;
      break ;
    case  'r':
      r = atoi ( optarg ) ;
      break ;
    case 's':
      snprintf( fmt, MAX_CHAR, "%s", optarg ) ;
      break ;
    case 'v':
      print_version () ;
    case 'h':
      usage () ;
    }

  if ( argc-optind < 1 )
    usage () ;

  if ( ( binFile = fopen ( argv[optind], "r" ) ) == NULL ) {
    printf ( " FATAL: file: %s could not be opened.\n", argv[optind] ) ;
    return ( 0 ) ; }



  /* Make sure mF, mE are even. */
  mF = 2*( mF/2 ) ;
  mL = 2*( mL/2 ) ;




  while ( !feof ( binFile ) ) {
    ++mLines ;

    fread_bswap ( byteSwap,  &mBytes, sizeof( int ), 1, binFile ) ;

    if ( mBytes < 0 ) {
      printf ( " found negative record length on line %d, will stop here.\n"
               " maybe try the -b argument to read byte-swapped?\n", mLines ) ;
      exit(0) ;
    }


    if ( !r || ( r == mLines && fmt[0] == '\0' ) ) {

      if ( feof ( binFile ) )
        return ( 0 ) ;

      printf ( "rec %3d:(%d)", mLines, mBytes ) ;

      if ( is_all_char ( binFile, byteSwap, mBytes ) )
        print_as_char ( binFile, byteSwap, mBytes ) ;

      else {

        /* As long as possible, treat groups of two ints to find doubles. */
        mInt = mBytes/intSize ;
        for ( nInt = 0 ; nInt < MIN( mF, mInt-1 ) ; nInt += 2 )
          printvals ( binFile, byteSwap, toDbl ) ;

        if ( ( skip = MAX( 0, mInt-mF-mL ) ) ) {
          printf ( " .... " ) ;
          fseek ( binFile, skip*sizeof( int ), SEEK_CUR ) ;
        }

        for ( nInt = MAX( nInt, mInt-mL ) ; nInt < mInt-1 ; nInt += 2 )
          printvals ( binFile, byteSwap, toDbl ) ;

        /* Remaining int. */
        if ( mBytes%sizeof(double) >= sizeof( int ) && nInt != mInt ) {
          fread_bswap ( byteSwap,  &intRead, sizeof( int ), 1, binFile ) ;
          if ( feof ( binFile ) )
            exit_feof () ;
          printf ( " %d", intRead ) ;
        }

        /* Skip remaining bytes. */
        if ( mBytes%sizeof(int) ) {
          fread_bswap ( byteSwap,  &intRead, 1, mBytes%sizeof(int), binFile ) ;
          if ( feof ( binFile ) )
            exit_feof () ;
        }

      }
    }
    else if ( r == mLines ) {
      /* Print this record using the specified format. */
      printf ( "rec %3d:(%d)", mLines, mBytes ) ;
      fgetpos ( binFile, &fPos ) ;
      printValsFmt ( binFile, byteSwap, fmt ) ;
      fsetpos ( binFile, &fPos ) ;
      fseek ( binFile, mBytes, SEEK_CUR ) ;
    }
    else {
      /* Skip the content. */
      fseek ( binFile, mBytes, SEEK_CUR ) ;
    }

    fread_bswap ( byteSwap,  &mBytes, sizeof( int ), 1, binFile ) ;
    if ( !r || r == mLines )
      printf ( " (%d)\n", mBytes ) ;

  }

  return ( 1 ) ;

}
