"""example/test hippy"""
import os
from pyhip import hip_cmd

def split_file_path(file_path):
    """ Returns the file basename and the parent
        directory given file path

        Parameters:
        ==========
        file_path : path to file (absolute or relative)

        Returns:
        =======
        parent : file parent directory
        filename : file name
    """
    abs_path = os.path.abspath(file_path)
    parent = os.path.dirname(abs_path)
    filename = os.path.basename(abs_path)
    return parent, filename


def file_base_name(filename):
    """ Removes the extension from the grid/solution
        file name

        Parameters:
        ==========
        filename : grid or solution file name

        Returns:
        =======
        basename : the basename of the grid/solution file
    """
    extensions = [".mesh.h5", ".sol.h5", ".grid.cgns",
                  ".sol.cgns"]
    basename = filename
    for ext in extensions:
        basename = basename.replace(ext, "")
    return basename


def convert_mesh_files(mesh_file, mesh_format, dest_format):
    """ Convert a mesh file from a format to another
        Parameters:
        ==========
        mesh_file : Mesh file path (absol or rel)
        mesh_format : Format of the mesh file (e.g hdf5)
        format_dest : The format to be converted into

        Returns:
        =======
        None
    """
    parent_dir, mesh = split_file_path(mesh_file)
    basename = file_base_name(mesh)
    lines = []
    lines.append("set path %s"%parent_dir)
    lines.append("read %s %s" %(mesh_format, mesh))
    lines.append("write %s %s" %(dest_format, basename))

    for line in lines:
        hip_cmd(line)

def convert_avbp_mesh_to_cgns(avbp_mesh_file):
    """ Convert an avbp mesh file into cgns file
        Parameters:
        ==========
        avbp_mesh_file : avbp mesh file path (absol or rel)

        Returns:
        =======
        None
    """
    mesh_format = "hdf"
    dest_format = "cgns"
    convert_mesh_files(avbp_mesh_file, mesh_format, dest_format)

def convert_cgns_mesh_to_avbp(cgns_file):
    """ Convert a cgns mesh file into avbp format
        Parameters:
        ==========
        cgns_file : cgns file path (absol or rel)

        Returns:
        =======
        None
    """
    mesh_format = "cgns"
    dest_format = "hdf"
    convert_mesh_files(cgns_file, mesh_format, dest_format)

def main():
    """ Testing"""
    avbp_mesh_file = "./examples/newtrappedvtx.mesh.h5"
    cgns_mesh_file = avbp_mesh_file.replace('.mesh.h5', '.grid.cgns')
    convert_avbp_mesh_to_cgns(avbp_mesh_file)
    convert_cgns_mesh_to_avbp(cgns_mesh_file)

if __name__ == "__main__":
    main()
