#/bin/sh 
set -e
echo "Deploying HIP in " $PWD
#Move new version from dev to release directory
echo "Create and move dev to new version directory"
VERSION=`ls -l VERSIONS/dev/hip-*-LINUX_x86 | grep -v '^d' | awk '{ print $9 }'|cut -d '-' -f 2`
echo "Releasing version" $VERSION
mkdir -p VERSIONS/$VERSION
cp  VERSIONS/dev/hip-*-LINUX_CI* VERSIONS/$VERSION
cp  VERSIONS/dev/libhip* VERSIONS/$VERSION
echo "Manual disable link latest for now"
#Backup previous version
#echo "Backup previous version"
#ls -alrt bin/
#cd bin
#mv hip-latest hip-previous
#mv hip-latest64 hip-previous64
#cd ..
#Link new version
#echo "Link latest"
#cd bin
#ln -s ../VERSIONS/$VERSION/hip-$VERSION-LINUX_x86 ./hip-latest
#ln -s ../VERSIONS/$VERSION/hip-$VERSION-LINUX_x86_Int64 ./hip-latest64
#cd ..
#ls -alrt bin/
echo "Done"
