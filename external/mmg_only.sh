#!/bin/sh
# Change this to point to the right MPI C compiler
CC=mpicc
INTEGER_SIZE=32
NUM_PROCS=2

##############################################
############  DO NOT MODIFY #################
##############################################
INSTALL_PATH=$PWD/DIST
ROOT_DIR=$PWD

#MMG
cd $ROOT_DIR
tar -zxf mmg.tgz
cd mmg  && \
rm -rf build  && mkdir build 
cd build  && \
cmake ../ -DUSE_ELAS=OFF \
          -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} 
make install -j $NUM_PROCS && cd $ROOT_DIR && rm -rf mmg
