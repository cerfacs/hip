#---------------------------------------------------------------------
# Build image based on Linux CentOS image and Python 3.6 distribution
#----------------------------------------------------------------------
FROM centos:7
FROM python:3.7-slim

#----------------------------------------------------------------------
# USER ENV VARIABLES AND LIBRARY VERSIONS
#----------------------------------------------------------------------
ENV LIBS_HOME /opt/LIBS
# HDF5 VERSION
ENV HDF_VERSION 1.8.18
# CGNS VERSION
ENV CGNS_VERSION 3.4.0
# SCOTCH VERSION
ENV SCOTCH_VERSION 6.1.3
# MMG_VERSION
ENV MMG_VERSION 5.7.1

#----------------------------------------------------------------------

#---------------------------------------------------------------------
# Install required system tools : gcc, cmake, make and python3-dev
#---------------------------------------------------------------------
RUN apt-get update
RUN apt update
RUN apt-get install -y git subversion gcc g++ gfortran cmake make python3-dev
RUN apt-get install -y zlib1g-dev rsync liblapack-dev liblapacke-dev libblas-dev

#---------------------------------------------------------------------
# Solve constructors/destructors bug occuring for cython compilation
#---------------------------------------------------------------------
#RUN cd /usr/lib/gcc/x86_64-linux-gnu/8 && \
#    cp crtbeginT.o crtbeginT.orig.o && \
#    cp crtbeginS.o crtbeginT.o

#---------------------------------------------------------------------
# Install/upgrade pip, and packages
#---------------------------------------------------------------------
#>>>>>>>>>>> Upgrade pip
RUN pip install pip --upgrade
#>>>>>>>>>>> Install testing and linting packages
RUN pip install pytest pytest-cov pylint
#>>>>>>>>>>> Install documentation packages
RUN pip install sphinx sphinx_rtd_theme
#>>>>>>>>>>> Install building and distributing packages
RUN pip install --upgrade Cython setuptools wheel twine
#>>>>>>>>>>> Install anybadge for gitlab badges
RUN pip install --upgrade anybadge

WORKDIR tmp
#---------------------------------------------------------------------
# Build and install Required library =====> HDF5
#---------------------------------------------------------------------
ENV HDF_REPO https://github.com/HDFGroup/hdf5.git 
RUN git clone --branch hdf5-$(echo ${HDF_VERSION} | tr . _) --depth 1 ${HDF_REPO}
RUN cd hdf5 && \
    ./configure --prefix=${LIBS_HOME}/ && \
    make -j 4 install && \
    cd ../..
ENV LD_LIBRARY_PATH="${LIBS_HOME}/lib:${LD_LIBRARY_PATH}"

#--------------------------------------------------------------------- # Build and install Required library =====> CGNS
#---------------------------------------------------------------------
ENV CGNS_REPO https://github.com/CGNS/CGNS.git
RUN git clone --branch v${CGNS_VERSION} --depth 1 ${CGNS_REPO}
RUN cd CGNS && \
    rm -rf build && \
    mkdir build && \
    cd build &&  \
    export FCFLAGS=-fPIC && export CFLAGS=-fPIC && \
    cmake ../ -DCGNS_ENABLE_HDF5=ON -DCGNS_BUILD_SHARED=OFF \
          -DHDF5_C_COMPILER_EXECUTABLE=${LIBS_HOME}/bin/h5cc\
          -DHDF5_INCLUDE_PATH=${LIBS_HOME}/include \
          -DHDF5_LIBRARY=${LIBS_HOME}/lib/libhdf5.a \
          -DCGNS_BUILD_CGNSTOOLS=OFF \
          -DCMAKE_MODULE_LINKER_FLAGS=Wl,-no-as-needed\
          -DCMAKE_INSTALL_PREFIX=${LIBS_HOME}/ && \
    make -j 4 install  && \
    cd ../..

#---------------------------------------------------------------------
# Build and install Required library =====> SCOTCH
#---------------------------------------------------------------------
ENV SCOTCH_REPO https://gitlab.inria.fr/scotch/scotch.git
RUN git clone ${SCOTCH_REPO} --branch v${SCOTCH_VERSION} --depth 1 scotch_${SCOTCH_VERSION}
RUN cd scotch_${SCOTCH_VERSION} && \
    cd src && \
    cp Make.inc/Makefile.inc.i686_pc_linux3 Makefile.inc &&\
    echo "CCP = gcc" >> Makefile.inc && \
    echo "CFLAGS = -O3 -Drestrict=__restrict \
         -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_RANDOM_FIXED_SEED \
         -DSCOTCH_RENAME -DSCOTCH_RENAME_PARSER" >> Makefile.inc && \
    make -j 4  && \
    cp -r ../include ../lib ${LIBS_HOME} &&\
    cd ../..

#---------------------------------------------------------------------
# Build and install Required library =====> MMG
#---------------------------------------------------------------------
WORKDIR tmp
ENV MMG_REPO https://github.com/MmgTools/mmg.git
ENV SCOTCH_LIB ${LIBS_HOME}
#RUN git clone --branch v${MMG_VERSION}  --depth 1 ${MMG_REPO}  && \
RUN git clone --branch feature/split-nonrid-edg-connecting-rid-pts --depth 1 ${MMG_REPO}  && \
    cd mmg  && \
    rm -rf build  && \
    mkdir build  && \
    cd build  && \
    cmake ../ -DUSE_ELAS=OFF -DUSE_SCOTCH=ON \
          -DSCOTCH_DIR=${SCOTCH_LIB} \
          -DCMAKE_INSTALL_PREFIX=${LIBS_HOME} && \
    make install -j 4

#---------------------------------------------------------------------
# Add hip root directory as an app
#---------------------------------------------------------------------
ADD . /app
WORKDIR /app
