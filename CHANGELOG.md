# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).

## [24.5.0] 2024-05 / Pyhip 1.1.0

## Added 

[-]

## Changed 

Update default versions in dockerfile to HDF5 1.14.3 and CGNS 4.4.0. Affects pyhip

## Fixed 

- Issue 2690: use full list of hyb elementsin mmg_put_mesh_3d renumbering, which had omitted listing pyramids
- Issue 2955: Support NULLTERM and NULLPAD strings in hdf read.

## [24.1.0] 2024-01

## Added 

[-]

## Changed

[-]

## Fixed

Fix reading bc label fromm hdf files. 
Fix memory allocation with writing to cgns
Fix adapt_mmg_per_mark for adaptation of Periodic cases

## [23.9.0] 2023-09

## Added 

[-]

## Changed

[-]

## Fixed

- remove loop to reset bc order in mmg_2hip.

## Changed

[-]

## [23.8.0] 2023-08

## Added 

[-]

## Changed

- Switch to mmg feature/split-nonrid-edg-connecting-rid-pts to split all edges bnd faces when using mmg.

## Fixed

[-]

## Changed

[-]

## [23.6.1] 2023-06

## Added

[-]

## Changed

[-]

## Fixed

- Hmin initialisation and 2D adaptation were broken

## [23.6.0] 2023-06

## Added

- introduced MMG_MAX_MVERTS to abort mmg usage if input mesh is to large.
- Using adapt_mmg3d_per to avoid periodic issues. \_per_mark for zones is in wip
- Add pyhip and test cmake options
- Added periodic adapt test case

## Changed

- Default internal boundaries treatment set to merge/remove by default ( set fc-remove 1 1 1 ). For previous behavior switch to se fc-remove 0 0 0

## Fixed

- Gmsh v2 and v4 format corrections ([Bug #2867](https://inle.cerfacs.fr/issues/2867)).

## [23.4.0] 2023-04

## Added 

- Compile time control of the lapack library for Apple (MAC) and for the MMG integer value (MMG_INT32)

## Changed 

- Remove compilation of lapack routines in hip. Lapack must be system provided now 
  apt-get install lapack-dev , and makefile.h modified accordingly
- Use only #include "mmg/libmmg.h" to include mmg now. 

## Fixed

- Refactoring of cmake to work now. 

### Deprecated

[ - ]

## [22.6.0] 2022-06

### Added

- augment syntax for set gridname, allow single arg to apply to current grid. Update doc.
- introduce check quality
- support for gmsh 4.x, read any lables given by gmsh, create zones based on volume labels
- CI: test cmake build

### Changed

- read Parameter section in hdf sol only if it exists.
- exclude reading nnode from hdf solution read.
- start work on mmg_adapt using element marks rather than zones
- removed automatic download of dependencies in cmake build 

### Fixed

- restore compilation with Lapack, instead of Clapack
- restore prepend-path in write_hdf_sol.
- fixed bug with hdf write segfault when joined meshes have interfaces

### Deprecated

[ - ]

## [21.9.0] 2021-09

### Added

- add var init var_name_expr value option to change var values
- vis elems -p: write all elements above/below some property threshold to a vtk file
- treat mismatches of sliding planes gracefully by snapping to the extrenal line in sp_calc_vx_weight_mixing_lines
- write interpolation weights for nodes on mixing planes.
- add list of mixing plane nodes and interpolation weights for mixing plane.
- write periodic faces to hdf.

### Changed

- revise edge length calculation for mmg_adapt_per

### Fixed

- fix bug to preserve grid refinement level
- fix bug with lidx for periodic faces.
