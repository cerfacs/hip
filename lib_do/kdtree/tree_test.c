#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* Timing stuff. Do we really need types.h? */
#include <sys/types.h>
#include <sys/times.h>
#include <unistd.h>

/* Needed for licence check. */
#include <time.h>
/* Needed for wildcard matching in se bc-text. */
#include <fnmatch.h>

#include "kdtree.h"
#include "hdf5.h"
#include "cpre.h"

#define MDIM 3
#define M_pts 320000

char hip_msg[10*128] ;
int verbosity = 3 ;
const char fatal_log_file[] = "hip-fatal.log" ;
const char warning_log_file[] = "hip-warning.log" ;



void hip_err ( hip_stat_e status, int printLvl, char *str ) {

  FILE *logFl ;
  const char errCat[][10] = { "ERROR", "FATAL", " WARNING", "    INFO"} ;

  if ( verbosity >= printLvl ) {
    printf ( "%s: %s\n", errCat[status], str ) ;
  }


  if ( status == fatal || ( status == warning && verbosity == 0 ) ) {

    /* Dump an error log. Treat warnings as fatal under verb=0. */
    logFl  = fopen ( fatal_log_file, "w" ) ;
    fprintf ( logFl, "%s: %s\n", errCat[status], str ) ;
    fprintf ( logFl, "Exiting via hip_err. Sorry.\n" ) ;
    fclose ( logFl ) ;

    /* Terminate */
    if ( verbosity >= printLvl ) {
      printf ( "Exiting via hip_err. Sorry.\n" ) ;
    }
    exit ( EXIT_FAILURE ) ;
  }

  else if ( status == warning ) {

    /* Dump an warning log. */
    logFl  = fopen ( warning_log_file, "a" ) ;
    fprintf ( logFl, "%s: %s\n", errCat[status], str ) ;
    fclose ( logFl ) ;
  }


  return ;
}



void fill_co_clu ( int m, double co[] ) {

  double *pCo = co, *pCoMax = co + MDIM*m, val ;
  int n ;

  for ( n = 0 ; n < MDIM ; n++ )
    for ( pCo = co+n ; pCo < pCoMax ; pCo+= MDIM ) {
      val = rand()/((double)RAND_MAX + 1) ;
      *pCo = pow( val, 4. ) ;
    }
}



void fill_co_ord ( int m, double co[] ) {

  /* Mimic a structured grid numbering with b.l. refinement:
     n = (ix*nx+ny)*iy +iz
   */

  int mx = pow((double)m,(1./3)), my = mx, mz = m/mx/my*1.1 ;
  int ix, iy, iz ;
  int n = 0 ;
  double *pCo = co ;

  for ( iz = 0 ; iz < mz ; iz++ ) {
    for ( iy = 0 ; iy < my ; iy++ ) {
      for ( ix = 0 ; ix < mx ; ix++ ) {

        pCo[0] = (double)ix/(double)mx ;
        pCo[1] = (double)iy/(double)my ;
        /*pCo[2] = pow((double)iz/mz,(double)2) ;*/
        pCo[2] = (double)iz/(double)mz ;

        n++ ;
        pCo += MDIM ;
        if ( n == m ) break ;
      }
      if ( n == m ) break ;
    }
    if ( n == m ) break ;
  }
  

}


void fill_co_ran ( int m, double co[] ) {

  double *pCo = co, *pCoMax = co + MDIM*m ;
  int n ;

  for ( n = 0 ; n < MDIM ; n++ )
    for ( pCo = co+n ; pCo < pCoMax ; pCo+= MDIM ) {
      *pCo = rand()/((double)RAND_MAX + 1) ;
    }
}

void unitcube ( int m, int lvl ) {

  int k ;
  for ( k = 0 ; k < lvl ; k++ ) {
    

    double *co = malloc ( 3*m * sizeof(double) ) ;
    double *po = malloc ( 3*m * sizeof(double) ) ;

    if ( !co ) {
      printf ( "failed malloc for co\n" ) ;
      exit ;
    }
    if ( !po ) {
      printf ( "failed malloc for co\n" ) ;
      exit ;
    }

    fill_co_ran ( m, co ) ;

    struct kdtree *pKdt = kd_create( MDIM );


    int n ;
    double *pCo, *pCoMax ;
    for ( pCo = co, pCoMax = co + 3*m ; pCo < pCoMax ; pCo+= MDIM ) {
      kd_insert ( pKdt, pCo, pCo ) ;
    }

    /*fill_co_ord ( m, po ) ;
    fill_co_ran ( m, po ) ;
    fill_co_clu ( m, po ) ;
    */
    fill_co_clu ( m, po ) ;

    /*
      printf ( " Starting to point search for %d points\n", m ) ;
    */
    clock_t c0 = clock () ;

    struct kdres* pKdr ;
    for ( pCo = po, pCoMax = po + 3*m ; pCo < pCoMax ; pCo+= MDIM ) {
      pKdr = kd_nearest( pKdt, pCo ) ;
      kd_res_free( pKdr );
    }

    clock_t c1 = clock () ;
    double dt[3] ;
    dt[0] = (float) (c1 - c0)/CLOCKS_PER_SEC ;
    /*
      printf ("\t%d points, elapsed CPU time:        %f\n", 
      m, (float) (c1 - c0)/CLOCKS_PER_SEC);
    */



    /*
      printf ( " Starting to range search for %d points\n", m ) ;
    */

    double mAvgNgh[3] ;
    int kk ;
    for ( kk = 1 ; kk < 3 ; kk++ ) { 
      c0 = clock () ;

      double dm = (double) m, e = 1./(double)MDIM, rg ;
      rg = 1./pow(dm,e) ;

      if ( kk>1 ) rg *= 1.5 ;

      int mNgh = 0 ;
      for ( pCo = po, pCoMax = po + 3*m ; pCo < pCoMax ; pCo+= MDIM ) {
        pKdr = kd_nearest_range( pKdt, pCo, rg ) ;

        while ( !kd_res_end( pKdr ) ) {
          mNgh++ ;
          kd_res_next( pKdr ) ;
        }
        kd_res_free( pKdr );

      }
  
      c1 = clock () ;
      dt[kk] = (float) (c1 - c0)/CLOCKS_PER_SEC ;
      mAvgNgh[kk] = (float) mNgh/m ;
      /*
        printf ("\t%d points, %g ngh, elapsed CPU time:        %f\n", 
        m, (float) mNgh/m, (float) (c3 - c2)/CLOCKS_PER_SEC);
      */
    }



    free( co ) ;
    free( po ) ;

    double t[3] ;
    double mlm = m*log(m) ;
    if ( k == 0 ) {
      t[0] = dt[0]/mlm ;
      t[1] = dt[1]/mlm ;
      t[2] = dt[2]/mlm ;

      printf ( "t0: %5.3f t1: %5.3f t2: %5.3f\n\n", dt[0], dt[1], dt[2] ) ;
      printf ( " m, dt[0], mAvgNgh[1], dt[1], mAvgNgh[2], dt[2]\n" );
    }

    mlm = m*log(m) ;
    printf ( " %8d %6.3f  %4.1f %6.3f   %4.1f %6.3f\n", 
             m, dt[0]/t[0]/mlm, mAvgNgh[1], dt[1]/t[0]/mlm, mAvgNgh[2], dt[2]/t[0]/mlm );

    m*= 2 ;
  }
  return ;
}

void readCoor ( const char fileName[], int *pmVx, double **ppCoor ) {

  hid_t file_id = H5Fopen( fileName, H5F_ACC_RDONLY, H5P_DEFAULT ) ;

  char grpName[128][2] ;
  strcpy ( grpName[0], "Coordinates" ) ;
  hid_t grp_id = h5_open_group ( file_id, grpName[0] ) ;

  int mVx = *pmVx = h5_read_dbl ( grp_id, "x", 0, NULL ) ;

  double *dBuf = malloc ( mVx*sizeof(double));
  int mDim = MDIM ;
  double *pCoor = *ppCoor =  malloc ( mDim*mVx*sizeof(double) );
  char coorName[3][2] = {"x", "y", "z"} ;
  int i ;
  for ( i = 0 ; i < mDim ; i++ ) {
    h5_read_dbl ( grp_id, coorName[i], mVx, dBuf ) ;

    int k ;
    for ( k = 0 ; k < mVx ; k++ )
      pCoor[mDim*k+i] = dBuf[k] ;
  }

  H5Gclose(grp_id);
  H5Fclose(file_id); 

  return ;
}

double sq_distance_dbl ( const double *Pcoor1, const double *Pcoor2, const int mDim ) {

  static int nDim ;
  static double diff, distance ;

  /* Loop over all dimensions. */
  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ ) {
    diff = Pcoor1[nDim] - Pcoor2[nDim] ;
    distance += diff*diff ;
  }
  return ( distance ) ;
}


void filesearch ( const char fileDonor[], const char fileRcpt[] ) {

  int mD ;
  double *co ;
  readCoor ( fileDonor, &mD, &co ) ;
     
  int mR ;
  double *po ;
  readCoor ( fileRcpt, &mR, &po ) ;

  struct kdtree *pKdt = kd_create( MDIM );

  printf ( " Starting to search for %d targets on %d donors\n", mR, mD ) ;


  int n ;
  double *pCo, *pCoMax ;
  for ( pCo = co, pCoMax = co + MDIM*mD ; pCo < pCoMax ; pCo+= MDIM ) {
    kd_insert ( pKdt, pCo, pCo ) ;
  }

  printf ( " Starting to point search\n" ) ;
  clock_t c0 = clock () ;

  struct kdres* pKdr ;
  for ( pCo = po, pCoMax = po + 3*mR ; pCo < pCoMax ; pCo+= MDIM ) {
    pKdr = kd_nearest( pKdt, pCo ) ;
    kd_res_free( pKdr );
  }

  clock_t c1 = clock () ;
  double dt[3] ;
  dt[0] = (float) (c1 - c0)/CLOCKS_PER_SEC ;
  printf (" point search: \t%d points, elapsed CPU time:        %f\n", 
          mR, dt[0] ) ;




  double mAvgNgh[3] ;
  int kk ;
  int mStencil ;

  int mNgh = 0 ;
  void * pNearestData ;

  int filling ;
  const double *pPos ;
  void *pOthDat ;
  double othDist, othPos[MDIM], coor[MDIM], dist ;
  int mS, mRgTot ;

  for ( kk = 1 ; kk < 3 ; kk++ ) { 
    c0 = clock () ;
    mRgTot = 0 ; 
    mStencil = ( kk == 1 ? 5 : 15 ) ;

    printf ( " Starting to range search for %d point stencil\n", mStencil ) ;


    for ( pCo = po, pCoMax = po + MDIM*mR ; pCo < pCoMax ; pCo+= MDIM ) {
      pKdr = kd_nearest( pKdt, pCo ) ;
      pNearestData = kd_res_item ( pKdr, coor ) ;
      dist = sqrt( sq_distance_dbl ( pPos, coor, MDIM ) ) ;
      kd_res_free( pKdr );

      /* Do a number of range searches with incrementally enlarged boxes,
         estimate a range using the distance to the nearest and epso. */
      for ( filling = 1 ;  filling ; ) {
  
        /* Search the range. */
        pKdr = kd_nearest_range( pKdt, pPos, dist);

        /* Range search this box. */
        mS = 0 ;
        while ( !kd_res_end( pKdr ) ) {
          (mRgTot)++ ;
          mS++ ;
          kd_res_next( pKdr ) ;
        }
        kd_res_free( pKdr );

        if ( mS >= mStencil )
          filling = 0 ;
        else {
          /* Enlarge the range for the search. */
          dist *=2 ;
        }
      }
    }
  
    c1 = clock () ;
    dt[kk] = (float) (c1 - c0)/CLOCKS_PER_SEC ;
    mAvgNgh[kk] = (float) mNgh/mR ;
    printf ("range search, %d stencil, \t%d points, %g ngh, elapsed CPU time:        %f\n", 
            mStencil, mR, (float) mNgh/mR, dt[kk] ) ;
  }
  
  free ( co ) ;
  free ( po ) ;
     
  return ;
}

void main( int argc, char *argv[] ) {

  int m, lvl ;
  char task[128], fileDonor[128], fileRcpt[128] ;

  if ( argc > 1 ) {
    strncpy ( task, argv[1], 128 ) ;
  }



  if ( !strcmp( task, "-c" ) ) {
    /* cube */
    if ( argc > 2 )
      m = atoi ( argv[1] ) ;
    else 
      m = M_pts ;

    if ( argc > 3 )
      lvl = atoi ( argv[2] ) ;
    else 
      lvl = 8 ;


    unitcube ( m, lvl ) ;
  }

  else if  ( !strcmp( task, "-f" ) ) {
    /* cube */
    if ( argc < 4 ) {
      printf ( "-f needs 2 filenames.\n" ) ;
      exit ;
    }

    strncpy ( fileDonor, argv[2], 128 ) ;
    strncpy ( fileRcpt,  argv[3], 128 ) ;


    filesearch ( fileDonor, fileRcpt ) ;
  }

  return ;
}
