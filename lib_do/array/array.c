/*
   array.c:
   Array class with realloc, repack, ....

   Last update:
   ------------
    21Apr11; print size_t with cast to lu
   15Nov09; use size_t for data counters.
   31Jan07; call check_array in arr_free.
   6Sep99; debug packing.


   This file contains:
   -------------------

*/
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include "array.h"

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

/* How many bytes to use to pad a pattern for array checking. */
#ifndef MBYTES_PAD
#  define MBYTES_PAD 8
#endif

/* Length of a namefield for each family and array. */
#ifndef NAME_LEN
#  define NAME_LEN 80
#endif

/* 64 bit SGI machines seem to have a problem with aligning to less than 64 bits.
   Allow 8 bytes of storage. */
#define ARR_S_PTRSZ 8

extern const int verbosity ;

/* A timeStamp like event counter. */
static int mEvents = 0 ;

/* A padding character set. */
char padChar[MBYTES_PAD+1] = "%%%%%%%%" ;

/* Track storage not handled by array but that comes with a query function for the size. */
typedef struct {
  const void *pData ;
  char name[NAME_LEN] ;
  /* Function returning the total allocated size. Returns the total allocated size, the overhead pOvh,
     the number of data items pmData and the size of one item pDataSize.  */
  size_t (*sizeFun) ( const void *pData,
                      size_t *pOvh, size_t *pmData, size_t *pDataSize ) ;
} nonArr_s ;

/* A family of arrays with explicit/automatic interdependency. Dependencies between
   arrays may only occur within a family. */
struct _arrFam_s {
  /* All the arrays in the family. Numbering of the arrays starts from 1 to
     allow for a NULL pointer. Leave the zeroth array pointer empty. */
  int mArr ;
  arr_s **ppArr ;

  /* All the trees, etc. Trees use standard malloc for their nodes. Too much overhead
     otherwise. */
  int mNonArr ;
  nonArr_s *pNonArr ;

  /* A name for the listing. */
  char name[NAME_LEN] ;
} ;


/* How does one array pTo depend on another pFrom?. */
typedef struct {
  /* The number of pFrom in the family. */
  int nArrFrom ;
  /* How many occurences of pTo can there be in pFrom? */
  int mToInFrom ;
  /* A function to extract each occurence of pTo in pFrom. */
  ap_s *(*xToInFrom) ( arr_s *pFrom, int nData, int nOcc ) ;
} arrDep_s ;


/* One instance of an array. */
struct _arr_s {
  /* The number of this array in the family. */
  arrFam_s *pFam ;
  int nrInFam ;

  /* A name for the listing. */
  char name[NAME_LEN] ;

  /* A timestamp. */
  int lastUpdate ;
  
  /* The size of each and the number of all items in the array. */
  size_t mData ;
  size_t dataSize ;
  /* The actual array of data. */
  char *pData ;
  /* The address where the calling function stores the pointer to the data-field.
     Note that on MBYTES_PAD, the data will be padded with control bits at the
     beginning and the end. Store this for backward compatibility with malloc. */
  char **ppData ;

  /* The first free data item. */
  size_t nFirstFreeData ;

  /* A list of other arrays that point into this one. */
  int mFrom ;
  arrDep_s *pArrDep ;
  
} ;


/* Memorize all families to track storage. Make a default family for all orphans. */
static int mFams = 0 ;
static arrFam_s **ppAllFam = NULL ;

static arrFam_s defFam = { 0, NULL, 0, NULL, "default" } ;

static void new_event () { mEvents++ ;  return ; }



/******************************************************************************

  make_arrFam:
  Create an array family.
  
  Last update:
  ------------
  20Sep99; track tree_structs.
  2Oct98: conceived.

  Input:
  ------
  name: a label for the array family.
  
  Returns:
  --------
  NULL on failure, a pointer to an array family on success.
  
*/

arrFam_s *make_arrFam ( const char *name ) {
  
  arrFam_s *pArrFam = NULL ;
  char def[] = "default" ;
  int nFam = 1 ;

  /* An empty string will request the default family. */
  if ( !name || name[0] == '\0' )
    name = def ;
  
  /* Does the family exist already? */
  if ( ppAllFam )
    for ( nFam = 0 ; nFam <= mFams ; nFam++ ) {
      pArrFam = ppAllFam[nFam] ;
      if ( !strcmp ( pArrFam->name, name ) )
        break ;
    }

  if ( nFam > mFams ) {
    /* No match. Make a new one. */
    new_event () ;

    if ( !( pArrFam = malloc ( sizeof( arrFam_s ) ) ) ) {
      printf ( " FATAL: could not allocate one arrFam_s in make_arrFam.\n" ) ;
      return ( NULL ) ; }
    
    pArrFam->mArr = 0 ;
    pArrFam->ppArr = NULL ;
    strncpy ( pArrFam->name, name, NAME_LEN ) ;

    pArrFam->mNonArr = 0 ;
    pArrFam->pNonArr = NULL ;

    /* Add the family to the list of families. */
    mFams++ ;
    if ( !( ppAllFam = realloc ( ppAllFam, (mFams+1)*sizeof( arrFam_s * ) ) ) ) {
      printf ( " FATAL: could not reallocate the list of families in make_arrFam.\n" ) ;
      return ( NULL ) ; }
    ppAllFam[mFams] = pArrFam ;
    ppAllFam[0] = &defFam ;
  }
  
  return ( pArrFam ) ;
}


/******************************************************************************

  make_array:
  Create an array. If pArray is given, the array is reallocated using
  the global REALLOC_FACTOR. If size is given, a list of that size is allocated.
  
  Last update:
  ------------
  20Dec23; zero the unused zero array in a family.
  21Apr11; print size_t with cast to lu
  15Nov09; expect mData as size_t.
  13Jul04; don't realloc on every call to make_array. Only realloc if mData requires it.
  2Oct98; derived from llVxEnt.
  
  Input:
  ------
  pArray:     the array in case of a realloc
  ppData:     the location where the calling function stores the data.
  mData:      If specified, that many data are allocated.
  dataSize:   Size of the datafield.
  pFam:       If non-NULL, and initial alloc, the array will be added to this family.
  name:       a label for the array family.
  

  Changes To:
  -----------
  pArray:     the array in case of a realloc
  ppData:     the location where the calling function stores the data.
  mData:      If specified, that many data are allocated.
  pFam:       If non-NULL, and initial alloc, the array will be added to this family.

  
  Returns:
  --------
  NULL on failure, a pointer to the array on success.
  
*/

arr_s *make_array ( arr_s *pArray, char **ppData, size_t mData, size_t dataSize,
                    arrFam_s *pFam, const char *name ) {
  
  size_t mOldData, nameLen ;
  int mArr ;
  char *padField, *pChar ;

  new_event () ;

  /*****
    Make a new list.
    *****/
  if ( !pArray ) {
    /* Malloc a root array and reset it. */
    if ( !( pArray = malloc ( sizeof( arr_s ) ) ) ) {
      printf ( " FATAL: could not allocate an array in make_array.\n" ) ;
      return ( NULL ) ; }
    
    pArray->mData = mOldData = 0 ;
    pArray->dataSize = dataSize ;
    pArray->nFirstFreeData = 0 ;
    pArray->pData = NULL ;
    pArray->ppData = ppData ;

    pArray->mFrom = 0 ;
    pArray->pArrDep = NULL ;

    if ( name )
      strncpy ( pArray->name, name, NAME_LEN-1 ) ;
    else
      /* SUN ANSI compilers don't know snprintf. Should not be a problem.
         snprintf ( pArray->name, NAME_LEN-1, "ev%d", mEvents ) ; */
      sprintf ( pArray->name, "ev%d", mEvents ) ;
  
    
    /* Add the array to the family. Use the default family 0 if there is none. */
    if ( !ppAllFam ) 
      /* Make a root family. */
      pFam = make_arrFam ( "default" ) ;
    else if ( !pFam )
      /* Use the default family. */
      pFam = ppAllFam[0] ;

    
    /* Add the array to the family. Note that array numbers start with one to
       allow for a NULL pointer with a zero array number. */
    mArr = ++pFam->mArr ;
    if ( !( pFam->ppArr = realloc ( pFam->ppArr, (mArr+1)*sizeof( arr_s * ) ) ) ) {
      printf ( " FATAL: failed to realloc list of arrays in make_array.\n" ) ;
      return ( NULL ) ; }
    pFam->ppArr[0] = NULL ;
    pFam->ppArr[ mArr ] = pArray ;
    pArray->pFam = pFam ;
    pArray->nrInFam = pFam->mArr ;

  }
  else {
    /* Realloc. */
    mOldData = pArray->mData ;
    if ( dataSize && dataSize != pArray->dataSize )
      printf ( " WARNING: illegal attempt to redefine datasize for"
               " array '%s' in family '%s' in make_array.\n",
               pArray->name, pArray->pFam->name ) ;
    dataSize = pArray->dataSize ;
  }


  
  /******
    Get the number of Data.
    *****/
  if ( !mData && !mOldData )
    /* Default size. */
    mData = DEFAULT_MDATA ;

  if ( mData ) {
    /* A specific size is given. Use it. */
    if ( pArray && mData < pArray->mData && verbosity > 5 )
      printf ( " INFO: shrinking the list of data from %lu to %lu  for"
               " array '%s' in family '%s' in make_array.\n",
               (unsigned long) pArray->mData, (unsigned long) mData, 
               pArray->name, pArray->pFam->name ) ;
  }
  else if ( mOldData ) {
    /* Resize. */
    mData = mOldData ;
    if ( verbosity > 5 )
      printf ( " INFO: realloc ent list in make_array to %lu data for"
               " array '%s' in family '%s' in make_array.\n",
               (unsigned long)mData, pArray->name, pArray->pFam->name ) ;
  }


  
  /*****
    Change the size.
    *****/
  if ( mOldData - mData ) {

#ifdef DEBUG
    if ( pArray->pData )
      /* Before reallocation, check for integrity. */
      check_array( pArray, ppData ) ;
#endif    
    
    pArray->pData = realloc ( pArray->pData, mData*dataSize +
                              2*MBYTES_PAD + ARR_S_PTRSZ ) ;
    if ( !pArray->pData ) {
      printf ( " FATAL: failed to realloc array to %lu for"
               " array '%s' in family '%s' in make_array.\n",
               (unsigned long) mData*dataSize, pArray->name, pArray->pFam->name ) ;
      show_arrUse ( NULL ) ;
      return ( 0 ) ; }
    
    /* The next free slot. */
    pArray->nFirstFreeData = mOldData ;
    pArray->mData = mData ;
    if ( pArray->ppData )
      *(pArray->ppData) = pArray->pData + MBYTES_PAD + ARR_S_PTRSZ ;
    
    /* Pad the data with marker bytes. */
    nameLen = MIN( MBYTES_PAD, strlen( pArray->name ) ) ;

    padField = pArray->pData + ARR_S_PTRSZ ;
    strncpy ( padField, pArray->name, nameLen ) ;
    /* strncat appends a \0 past MBYTES_PAD. DIY*/
    for ( pChar = padField+nameLen ; pChar < padField+MBYTES_PAD ; pChar++ )
      *pChar = padChar[0] ;

    padField = pArray->pData + ARR_S_PTRSZ + mData*dataSize + MBYTES_PAD ;
    strncpy ( padField, pArray->name, nameLen ) ;
    for ( pChar = padField+nameLen ; pChar < padField+MBYTES_PAD ; pChar++ )
      *pChar = padChar[0] ;

    *( ( arr_s ** ) pArray->pData ) = pArray ;
  }

  pArray->lastUpdate = mEvents ;
  return ( pArray ) ;
}


/******************************************************************************

  free_arrFam:
  Remove an entire family of arrays.
  
  Last update:
  ------------
  2Oct98: conceived.
  
  Input:
  ------
  ppArrFam: the family

  Changes To:
  -----------
  ppArrFam
  
*/

void free_arrFam ( arrFam_s **ppArrFam  ) {
  
  arrFam_s *pArrFam ;
  int nArr, nFam ;

  new_event () ;

  if ( !ppArrFam )
    /* No such list of Data. */
    return ;
  else if ( !( pArrFam = *ppArrFam ) )
    /* Empty list. */
    return ;

  /* Remove all arrays in this familiy. */
  for ( nArr = 1 ; nArr <= pArrFam->mArr ; nArr++ )
    free_array ( pArrFam->ppArr + nArr ) ;


  /* Remove the family from the global list of families. */
  for ( nFam = 0 ; nFam <= mFams ; nFam++ )
    if ( ppAllFam[nFam] == pArrFam ) {
      ppAllFam[nFam] = NULL ;
      break ;
    }
  
  
  free ( pArrFam->ppArr ) ;
  free ( pArrFam ) ;
  *ppArrFam = NULL ;

  /* Copy all remaining families one position down. */
  for ( ; nFam < mFams ; nFam++ )
    ppAllFam[nFam] = ppAllFam[nFam+1] ;
  
  ppAllFam = realloc ( ppAllFam, (mFams--)*sizeof( arrFam_s * ) ) ;
  
  return ;
}

/******************************************************************************

  free_array:
  Remove an array.
  
  Last update:
  ------------
  2Oct98: conceived.
  
  Input:
  ------
  ppArray: the array to be removed.


  Changes To:
  -----------
  ppArray
  
*/

void free_array ( arr_s **ppArray ) {
  
  arr_s *pArr ;
  arrFam_s *pFam ;

  new_event () ;

  if ( !ppArray )
    /* No such list of Data. */
    return ;
  else if ( !( pArr = *ppArray ) )
    /* Empty list. */
    return ;

  free ( pArr->pData ) ;
  free ( pArr->pArrDep ) ;

  pFam = pArr->pFam ;
  if ( pFam ) {
    if ( pFam->ppArr[ pArr->nrInFam ] == pArr )
      pFam->ppArr[ pArr->nrInFam ] = NULL ;
    else
      /* printf ( " WARNING: mismatch of arrays in family, no family update.\n" ) */ ;
  }

  free ( pArr ) ;
  *ppArray = NULL ;
  
  return ;
}


/******************************************************************************

  get_lastUpdate:
  Given an array, retrieve its timestamp.
  
  Last update:
  ------------
  2Oct98: conceived.
  
  Input:
  ------
  pArray
  
  Returns:
  --------
  the event timestamp.
  
*/

int get_lastUpdate ( const arr_s *pArray ) {

  if ( !pArray )
    return ( 0 ) ;
  else
    return ( pArray->lastUpdate ) ;
}

/******************************************************************************

  declare_arr_depend:
  Declare dependencies between arrays for automatic updates during a repack.
  Updates happen to a repacked array pTo that is being pointed to from data
  in pFrom.
  
  Last update:
  ------------
  2Oct98: conceived.
  
  Input:
  ------
  pArrTo:    The array that is being pointed to.
  pArrFrom:  The array that contains the pointers to pArrTo.
  mToInFrom: How many pointers to pArrTo are there in pArrFrom?
  xToInFrom: A function to extract all mToInFrom references to pArrTo in pArrFrom.

  Changes To:
  -----------
  pArrTo:    Add a dependency of pArrTo to its list.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int add_array_reference ( arr_s *pArrTo, arr_s *pArrFrom, int mToInFrom,
                          ap_s *(*xToInFrom) ( arr_s *pFrom, int nData, int mOcc ) ) {
  
  int mFrom ;
  arrDep_s *pArrDep ;

  new_event () ;
  
  if ( !pArrTo )
    return ( 0 ) ;

  if ( !pArrFrom )
    /* No array to depend from is listed. Just return the number of dependencies. */
    return ( pArrTo->mFrom ) ;

  if ( pArrTo->pFam != pArrFrom->pFam ) {
    printf ( " FATAL: dependencies must be in the same family.\n" ) ;
    return ( 0 ) ;
  }

  mFrom = ++pArrTo->mFrom ;
  if ( !( pArrTo->pArrDep = realloc ( pArrTo->pArrDep, mFrom*sizeof( arrDep_s )))) {
    printf ( " FATAL: could not reallocate dependencies in declare_array_depend.\n" ) ;
    return ( 0 ) ; }

  pArrDep = pArrTo->pArrDep + mFrom - 1 ;
  pArrDep->nArrFrom = pArrFrom->nrInFam ;
  pArrDep->mToInFrom = mToInFrom ;
  pArrDep->xToInFrom = xToInFrom ;
  
  return ( 1 ) ;
}

/******************************************************************************

  get_new_data:
  Get the next mNewData free slots from an array. If the array is exhausted, it is
  reallocated.
  
  Last update:
  ------------
  15Oct09; use size_t for mData, mNewData. Drop check for negative mNewData.
  2Oct98: conceived.
  
  Input:
  ------
  pArray:   the array.
  ppData:   the data field in the calling function.
  arrOp:    see array.h: arr_add (default), arr_total, probe ...
            arr_add: add another mNewData elems
            arr_probe: return the address to the next free (unreserved elem)
  dataSize: size of each entry.
  mNewData: the number of new slots needed.

  Changes To:
  -----------
  pArray:
  ppData:   filled with the allocated field.
  
  Returns:
  --------
  NULL on failure, the first vacant entity slot on arr_add and arr_sub,
  the root pointer on arr_total or arr_shrink, the last valid entry slot
  on add_probe.
  
*/

void *get_new_data ( arr_s *pArray, char **ppData,
                     arr_e arrOp, size_t dataSize, size_t mNewData ) {

  size_t mData, nFreeData ;

  new_event () ;

  if ( !pArray ) {
    /* pArray is invalid. */
    
    if ( ppData && *ppData )
      /* There is a datafield. Find the datafield in the default family. */
      pArray = *( ( arr_s ** ) ( *ppData - MBYTES_PAD - ARR_S_PTRSZ ) ) ;

    if ( !( ppData || *ppData ) || !pArray ) {
      /* No datafield given or no match found. */
      if ( dataSize ) {
        /* Alloc a new default field. */
        pArray = NULL ;
        if ( !( pArray =
                make_array ( pArray, ppData, mNewData, dataSize, NULL, NULL ) ) ) {
          printf ( " FATAL: failed to alloc a default array in get_new_data.\n" ) ;
          return ( NULL ) ; }
      }
      else {
        printf ( " FATAL: dataSize must be nonzero for new arrays in get_new_data.\n" ) ;
        return ( NULL ) ; }
    }
    
    else {
      /* Check the match. */
      if ( *ppData && pArray->pData + MBYTES_PAD + ARR_S_PTRSZ != *ppData ) {
        printf ( " FATAL: array mismatch in get_new_data.\n" ) ;
        return ( NULL ) ; }
      else if ( dataSize && pArray->dataSize != dataSize ) {
        printf ( " FATAL: dataSize mismatch in get_new_data.\n" ) ;
        return ( NULL ) ; }
    }
  }

  else if ( pArray && ( ppData && *ppData ) ) {
    if ( pArray->pData + MBYTES_PAD + ARR_S_PTRSZ != *ppData ) {
      printf ( " FATAL: data field mismatch in get_new_data.\n" ) ;
      return ( NULL ) ; }
  }

  else
    /* Find the data field with this array. */
    ppData = pArray->ppData ;



  
  /* Find the necessary size. */
  if ( arrOp == arr_probe )
    /* Return the used addresses. */
    return ( ( void * ) ( pArray->pData + ARR_S_PTRSZ + MBYTES_PAD +
                          pArray->nFirstFreeData*pArray->dataSize ) ) ;

  
  else if ( arrOp == arr_delete ) {
    /* Delete. */
    free_array ( &pArray ) ;
    return ( NULL ) ;
  }

  
  else if ( arrOp == arr_sub ) {
    /* Empty the last slot. */
    pArray->nFirstFreeData-- ;
    /* Return the base address. */
    return ( ( void * ) ( pArray->pData + ARR_S_PTRSZ + MBYTES_PAD ) ) ;
  }

  
  else if ( arrOp == arr_total || arrOp == arr_shrink ) {
    /* Use mNewData as total size. */
    if ( mNewData < pArray->mData && arrOp != arr_shrink ) {
      printf ( " FATAL: use arr_shrink to shrink from %lu to %lu"
               "        in array %s, family %s in get_new_data.\n",
               (unsigned long)pArray->mData, (unsigned long)mNewData, 
               pArray->name, pArray->pFam->name ) ;
      return ( NULL ) ;
    }
    else if ( !make_array ( pArray, NULL, mNewData, 0, NULL, NULL ) ) {
      printf ( " FATAL: failed to realloc the list of data in get_new_data.\n" ) ;
      return ( NULL ) ;
    }

    pArray->nFirstFreeData = mNewData ;
    pArray->lastUpdate = mEvents ;

    /* Return the base address. */
    return ( ( void * ) ( pArray->pData + ARR_S_PTRSZ + MBYTES_PAD ) ) ;
  }

  
  else {
    /* arr_add is the default option. */
    if ( pArray->nFirstFreeData + mNewData > pArray->mData ) {
      /* More space needed. */
      mData = MAX( pArray->mData + mNewData, REALLOC_FACTOR*pArray->mData + 1 ) ;
      if ( !make_array ( pArray, NULL, mData, 0, NULL, NULL ) ) {
        printf ( " FATAL: failed to realloc the list of data in get_new_data.\n" ) ;
        return ( NULL ) ;
      }
    }

    /* Update the counters in pArray. */
    nFreeData = pArray->nFirstFreeData ;
    pArray->nFirstFreeData += mNewData ;
    pArray->lastUpdate = mEvents ;

    /* Return the address of the first open slot. */
    return ( ( void * ) ( pArray->pData + ARR_S_PTRSZ + MBYTES_PAD +
                          nFreeData*pArray->dataSize ) ) ;
  }
}


/******************************************************************************

  sizeof_datafield:
  Given a data field either by array or data pointer, return its size,
  first and last pointers.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int sizeof_datafield ( arr_s *pArray, void *ppVData, void *ppVLstData ) {

  char **ppData = ( char** ) ppVData, **ppLstData = ( char** ) ppVLstData ;
  
  if ( pArray && *ppData ) {
    if ( pArray->pData + ARR_S_PTRSZ + MBYTES_PAD != *ppData ) {
      printf ( " FATAL: mismatch between array and data in get_size_data.\n" ) ;
      return ( 0 ) ; }
  }

  else if ( pArray )
    ppData = pArray->ppData ;

  else if ( *ppData )
    pArray = *( ( arr_s ** ) ( *ppData - MBYTES_PAD - ARR_S_PTRSZ ) ) ;

  else {
    printf ( " FATAL: either array or data must be given in get_size_data.\n" ) ;
    return ( 0 ) ;
  }
    
  *ppData = pArray->pData + ARR_S_PTRSZ + MBYTES_PAD ;
  *ppLstData = *ppData + pArray->mData*pArray->dataSize ;
  return ( pArray->mData ) ;
}


/******************************************************************************

  pack_array:
  Given an array and a field of integers, repack the array omitting all
  elements with zero in the number field and update the entries in all
  other arrays referencing this one.
  If no number field is given, the standard case, the number field is
  established from the referencing arrays. Only referenced elements are
  retained.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pArray:  the array.
  pNrUser: the number field supplied by the user.

  Changes To:
  -----------
  pArray:  the array and all referencing ones.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int pack_array ( arr_s *pArray, int *pNrUser ) {
  
  const arrFam_s *pFam ;
  const arrDep_s *pArrDep ;
  arr_s *pFrom ;
  int *pNr, nDep, nArr ;
  size_t nData, nOcc, nHole, nEl, mUsed ;
  ap_s *pDepData ;
  size_t dataSize ;
  char *pDataFrom, *pDataTo ;

  new_event () ;
  
  if ( !pArray ) {
    printf ( " FATAL: no such array in pack_array.\n" ) ;
    return ( 0 ) ; }

  if ( pNrUser )
    pNr = pNrUser ;
  else if ( !( pNr = calloc ( pArray->nFirstFreeData, sizeof( int ) ) ) ) {
    printf ( " FATAL: could not allocate number field in pack_array.\n" ) ;
    return ( 0 ) ; }
  
  pFam     = pArray->pFam ;
  nArr     = pArray->nrInFam ;
  dataSize = pArray->dataSize ;



  if ( !pNrUser )
    /* Mark all referenced elements of pArray. */
    for ( nDep = 0 ; nDep < pArray->mFrom ; nDep++ ) {
      pArrDep = pArray->pArrDep + nDep ;
      pFrom = pFam->ppArr[ pArrDep->nArrFrom ] ;
      
      /* Loop over all entries and all occurences. */
      for ( nData = 0 ; nData < pFrom->nFirstFreeData ; nData++ )
        for ( nOcc = 0 ; nOcc < pArrDep->mToInFrom ; nOcc++ ) {
          pDepData = pArrDep->xToInFrom ( pFrom, nData, nOcc ) ;
          if ( pDepData->nArr == nArr )
            /* This one points into pArray. Mark. */
            pNr[ pDepData->nElem ] = 1 ;
        }
    }
  

  /* Increment the numbering. This will be the future position of the element
     in the array offset by 1. */
  for ( mUsed = nEl = 0 ; nEl < pArray->nFirstFreeData ; nEl++ )
    if ( pNr[nEl] )
      pNr[nEl] = ++mUsed ;




  /* Loop over all arrays pointing into pArray and update pointers into pArray
     according to the number field in pArray. */
  for ( nDep = 0 ; nDep < pArray->mFrom ; nDep++ ) {
    pArrDep = pArray->pArrDep + nDep ;
    pFrom = pFam->ppArr[ pArrDep->nArrFrom ] ;

    /* Loop over all entries and all occurences. */
    for ( nData = 0 ; nData < pFrom->nFirstFreeData ; nData++ )
      for ( nOcc = 0 ; nOcc < pArrDep->mToInFrom ; nOcc++ ) {
        pDepData = pArrDep->xToInFrom ( pFrom, nData, nOcc ) ;
        if ( pDepData->nArr == nArr )
          /* Valid pointer. Rembember that pNr is offset by 1. */
          pDepData->nElem = pNr[ pDepData->nElem ] - 1 ;
      }
  }





  /* Repack the array in situ item by item. Find the first empty element. */
  for ( nHole = 0 ; nHole < pArray->nFirstFreeData ; nHole++ )
    if ( pNr[nHole] == 0 )
      break ;
  pDataTo = pArray->pData + ARR_S_PTRSZ + MBYTES_PAD + dataSize*nHole ;
  
  /* Start packing. Since we copy item by item, we can use memcpy. */
  pDataFrom = pDataTo ;
  for ( nData = nHole+1 ; nData < pArray->nFirstFreeData ; nData++ ) {
    pDataFrom += dataSize ;
    if ( pNr[nData] > 0 ) {
      /* This is the next filled element. */
      memcpy ( pDataTo, pDataFrom, dataSize ) ;
      pDataTo += dataSize ;
      nHole++ ;
    }
  }

  /* Reallocate the array. */
  get_new_data ( pArray, NULL, arr_shrink, 0, nHole ) ;

  
  /* All dependencies have been updated, remove the invalid number field. */
  if ( !pNrUser )
    free ( pNr ) ;
  pArray->lastUpdate = mEvents ;
  
  return ( 1 ) ;
}


/******************************************************************************

  show_arrUse:
  List the memory usage of the families. If no family is specified, all families
  are listed.
  
  Last update:
  ------------
  21Apr11; print size_t with cast to (unsigned long)
  15Nov09; make mData size_t.
  : conceived.
  
  Input:
  ------
  pFamList: the family to be listed.
  
*/

void show_arrUse ( arrFam_s *pFamList ) {

  const arrFam_s *pFam ;
  arrFam_s **ppFam ;
  const arr_s *pArr ;

  size_t mData, famSize, ovh, totalSize, globalSize = 0, dataSize ;
  int nArr ;
  const void *pData ;
  
  if ( !ppAllFam ) {
    printf ( " No storage allocated with array.c.\n" ) ;
    return ; }
    
  for ( ppFam = ppAllFam ; ppFam <= ppAllFam + mFams ; ppFam++ ) {
    pFam = *ppFam ;

    ovh = sizeof( arrFam_s) + pFam->mArr*sizeof( arr_s * ) ;
    famSize     = ovh ;
    globalSize += ovh ;

    if ( pFam == pFamList || !pFamList )
      printf ( "\n Family %.20s, %lu members, overhead %4lu.\n",
               pFam->name, (unsigned long)pFam->mArr, (unsigned long)ovh ) ;


    
    for ( nArr = 1 ; nArr <= pFam->mArr ; nArr++ )
      if ( ( pArr = pFam->ppArr[nArr] ) ) {
        ovh = sizeof( arr_s ) + pArr->mFrom*sizeof( arrDep_s ) ;
        totalSize = pArr->mData*pArr->dataSize + ovh ;
        famSize += totalSize ;
        globalSize += totalSize ;
        
        if ( pFam == pFamList || !pFamList )
          printf ( "   %3d: %-35.35s, %5lu + %9lu * %9lu b = %9lu b\n",
                   nArr, pArr->name, (unsigned long) ovh, (unsigned long) pArr->mData, 
                   (unsigned long) pArr->dataSize, (unsigned long) totalSize) ;
      }

    
    for ( nArr = 0 ; nArr < pFam->mNonArr ; nArr++ )
      if ( ( pData = pFam->pNonArr[nArr].pData ) ) {
        totalSize = pFam->pNonArr[nArr].sizeFun ( pData, &ovh, &mData, &dataSize ) ;
        printf ( "   %3d: %-35.35s, %5lu + %9lu * %5lu b = %9lu b.\n",
                 nArr, pFam->pNonArr[nArr].name, (unsigned long)ovh, 
                 (unsigned long)mData, (unsigned long)dataSize, 
                 (unsigned long)totalSize ) ;
        famSize += totalSize ;
        globalSize += totalSize ;
      }
    
    printf ( "\n Total Size in this family: %9lu b.\n", (unsigned long)famSize ) ;
  }
  
  printf ( "\n Total Size in all families:  %9lu b.\n", (unsigned long)globalSize ) ;
  return ;
}

/******************************************************************************

  show_arrDep:
  List the dependencies of the families. If no family is specified, all families
  are listed.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pFamList: the family to be listed.
  
*/

void show_arrDep ( arrFam_s *pFamList ) {

  const arrFam_s *pFam ;
  const arrDep_s *pAD ;
  arrFam_s **ppFam ;
  int nArr ;
  const arr_s *pArr ;
  int nDep, nFrom ;
  
  if ( !ppAllFam ) {
    printf ( " No storage allocated with array.c.\n" ) ;
    return ; }
  else
    printf ( "\n Inter-family dependencies between arrays:\n" ) ;
    
  for ( ppFam = ppAllFam ; ppFam <= ppAllFam + mFams ; ppFam++ ) {
    pFam = *ppFam ;

    if ( pFam == pFamList || !pFamList ) {
      printf ( "\n Family %.20s, %3d members.\n", pFam->name, pFam->mArr ) ;

      for ( nArr = 1 ; nArr <= pFam->mArr ; nArr++ )
        if ( ( pArr = pFam->ppArr[nArr] ) ) {
          printf ( "   %3d: %-40.40s\n", nArr, pArr->name ) ;
          
          if ( !pArr->mFrom )
            printf ( "        not referenced.\n" ) ;
          else
            for ( nDep = 0 ; nDep < pArr->mFrom ; nDep++ ) {
              pAD = pArr->pArrDep + nDep ;
              nFrom =  pAD->nArrFrom ;
              printf ( "        refd. %d times from  %3d: %-40.40s\n",
                       pAD->mToInFrom, nFrom, pFam->ppArr[nFrom]->name ) ;
            }
        }
    }
  }
  return ;
}
/******************************************************************************

  check_array:
  Check the integrity of an array by looking at the padding.
  If only pArr is given, the array is checked.#
  If only ppData is given, the corresponding array is checked.
  If both ppData and pArr are given, the array is checked and the match between
     the two is checked.
  If nothing is given, all arrays are checked.
  
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pArr:   the array to be checked.
  ppData: the datafield of the array.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int check_array ( arr_s *pArr, char **ppData ) {

  const char *pData ;
  arrFam_s *pFam, **ppFam ;
  arr_s **ppArr ;

  int returnVal = 1, nameLen ;
  
  if ( !ppAllFam ) {
    printf ( " Nothing to check in check_array.\n" ) ;
    return ( 0 ) ;
  }
  else if ( pArr && ppData &&
    /* Both array and data pointer given. Check the match. */
            pArr->ppData != ppData ) {
    printf ( " FATAL: mismatch in pointers to the data field for array %s.\n",
             pArr->name ) ;
    return ( 0 ) ;
  }
  else if ( !pArr && ppData )
    pArr = *( ( arr_s ** ) ( *ppData - MBYTES_PAD - ARR_S_PTRSZ ) ) ;

  else if ( !pArr && !ppData ) {
    /* Nothing given. Check all arrays of all families. */
    for ( ppFam = ppAllFam ; ppFam <= ppAllFam + mFams ; ppFam++ )
      if ( ( pFam = *ppFam ) )
        for ( ppArr = pFam->ppArr+1 ; ppArr <= pFam->ppArr + pFam->mArr ; ppArr++ )
          if ( *ppArr )
            returnVal &= check_array ( *ppArr, NULL ) ;
    
    return ( returnVal ) ;
  }
  

# if MBYTES_PAD == 0
    printf ( " No padding compiled.\n" ) ;
    return ( 0 ) ;
# endif

  /* Check the padding. */
  nameLen = MIN( MBYTES_PAD, strlen( pArr->name) ) ;
  pData = pArr->pData + ARR_S_PTRSZ ;

  if ( strncmp( pArr->name, pData, nameLen ) ||
       strncmp( padChar, pData+nameLen, MBYTES_PAD-nameLen) ) {
    printf ( " FATAL: corruption at the beginning of '%s' in family '%s'.\n",
             pArr->name, pArr->pFam->name ) ;
    returnVal = 0 ;
  }
    
  pData = pArr->pData + ARR_S_PTRSZ + MBYTES_PAD + pArr->mData*pArr->dataSize ;
  if ( strncmp( pArr->name, pData, nameLen ) ||
       strncmp( padChar, pData+nameLen, MBYTES_PAD-nameLen) ) {
    printf ( " FATAL: corruption at the end of '%s' in family '%s'.\n",
             pArr->name, pArr->pFam->name ) ;
    returnVal = 0 ;
  }
  
  return ( returnVal ) ;
}

/******************************************************************************

  en_ap:
  Encode an ap_s given number and array pointer.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pArr: the array
  nr:   the element in the array.
  
  Returns:
  --------
  AP_NULL on failure, the encoded pointer on success.
  
*/

ap_s en_ap ( arr_s *pArr, const int nr ) {

  ap_s ap ;

  if ( !pArr )
    return ( AP_NULL ) ;
  
# if CHECK_BOUNDS
  if ( nr >= pArr->mData ) {
    printf ( " FATAL: out of bounds 0 < %d < %d for '%s' in '%s' in en_ap.\n",
             nr, pArr->mData, pArr->name, pArr->pFam->name ) ;
    return ( AP_NULL ) ;
  }
# endif

  ap.nElem = nr ;
  ap.nArr  = pArr->nrInFam ; 
  return ( ap ) ;
}


/******************************************************************************

  en_ap:
  Encode an ap_s given a data pointer address.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppData: the datafield
  nr:     the element in the datafield.

  
  Returns:
  --------
  AP_NULL on failure, the encoded pointer on success.
  
*/

ap_s enp_ap ( char **ppData, const int nr ) {

  arr_s *pArr = *( ( arr_s ** ) ( *ppData - MBYTES_PAD - ARR_S_PTRSZ ) ) ;

  return ( en_ap ( pArr, nr ) ) ;
}


/******************************************************************************

  de_ap:
  Decode an ap_s.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ap:   The array pointer.
  pFam: The family of the pointer.

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void *de_ap ( const ap_s ap, const arrFam_s *pFam ) {

  arr_s *pArr ;
  int nArr, nr ;

  
  if ( ( nArr = ap.nArr ) == 0 )
    /* Invalid array number. */
    return ( NULL ) ;

  
  if ( !pFam )
    /* Use the default. */
    pFam = ppAllFam[0] ;

  if ( nArr > pFam->mArr ) {
    printf ( " FATAL: invalid array number %d for family %s in de_ap\n",
             nArr, pFam->name ) ;
    return ( NULL ) ;
  }
    
  pArr = pFam->ppArr[nArr] ;
  nr   = ap.nElem ;
    
# if CHECK_BOUNDS
  if ( nArr > pFam->mArr ) {
    printf ( " FATAL: array number %d out of bounds %d of family '%s' in de_ap.\n",
             nArr, pFam->mArr, pFam->name ) ;
    return ( NULL ) ;
  }
  else if ( nr < 0 || nr >= pArr->mData ) {
    printf ( " FATAL: out of bounds 0 <= %d < %d for '%s' in '%s' in de_ap.\n",
             ap.nElem, pArr->mData, pArr->name, pArr->pFam->name ) ;
    return ( NULL ) ;
  }
# endif


  return ( ( void * ) ( pArr->pData + ARR_S_PTRSZ +MBYTES_PAD +
                        nr*pArr->dataSize ) ) ;
}

/******************************************************************************

  Wrappers for the generic {m,c,re}alloc, free:
  .
  
  Last update:
  ------------
  21Apr11; print size_t with cast to lu
  15Nov09; make mData size_t. 
  : conceived.
  
  Input:
  ------
  pName,
  mData
  dataSize
  pData

  Changes To:
  -----------
  pData
  
  Returns:
  --------
  NULL on failure, pData on success.
  
*/

void *arr_malloc ( char *pName, arrFam_s *pFam, size_t mData, size_t dataSize ) {

  arr_s *pArr ;
  
  if ( !( pArr = make_array ( NULL, NULL, mData, dataSize, pFam, pName ) ) ) {
    printf ( " FATAL: could not allocate %lu bytes for %s in arr_malloc.\n",
             (unsigned long)mData*dataSize, pName ) ;
    show_arrUse ( NULL ) ;
    exit ( 0 ) ;
    /* The stupid SGI -n32 wants a return here or flashes a warning. */
    return ( NULL ) ;
  }
  else
    return ( pArr->pData + ARR_S_PTRSZ + MBYTES_PAD ) ;
}

void *arr_calloc ( char *pName, arrFam_s *pFam, size_t mData, size_t dataSize ) {

  arr_s *pArr ;
  char *pCh, *pData ;

  if ( !( pArr = make_array ( NULL, NULL, mData, dataSize, pFam, pName ) ) ) {
    printf ( " FATAL: could not allocate %lu bytes for %s in arr_calloc.\n",
             (unsigned long)mData*dataSize, pName ) ;
    show_arrUse ( NULL ) ;
    exit ( 0 ) ;
    /* The stupid SGI -n32 wants a return here or flashes a warning. */
    return ( NULL ) ;
  }
  else {
    /* Reset the data. */
    pData = pArr->pData + ARR_S_PTRSZ + MBYTES_PAD ;
    for ( pCh = pData ; pCh < pData + mData*dataSize ; pCh++ )
      *pCh = 0 ;
    
    return ( pData ) ;
  }
}

void *arr_realloc ( char *pName, arrFam_s *pFam,
                    void *pData, size_t mData, size_t dataSize ) {

  arr_s *pArr ;
  
  if ( !pData )
    return ( arr_malloc ( pName, pFam, mData, dataSize ) ) ;

  pArr = *( ( arr_s ** ) ( ((char*)pData) - MBYTES_PAD - ARR_S_PTRSZ ) ) ;

  if ( !make_array ( pArr, NULL, mData, dataSize, pFam, pName ) ) {
    printf ( " FATAL: could not allocate %lu bytes for %s in arr_realloc.\n",
             (unsigned long)mData*dataSize, pName ) ;
    show_arrUse ( NULL ) ;
    exit ( 0 ) ;
    /* The stupid SGI -n32 wants a return here or flashes a warning. */
    return ( NULL ) ;
  }
  else
    return ( pArr->pData + ARR_S_PTRSZ + MBYTES_PAD ) ;
}

void arr_free ( void *pData ) {

  arr_s *pArr ;
  
  if ( !pData )
    return ;

  pArr = *( ( arr_s ** ) ( ((char*)pData) - MBYTES_PAD - ARR_S_PTRSZ ) ) ;
#ifdef DEBUG
  check_array ( pArr, NULL ) ;
#endif
  free_array ( &pArr ) ;

  return ;
}


/* Two addons to track tree, etc. storage with array.c 

   Aug03: Why are these functions returning a pointer? */
void *arr_ini_nonArr ( void *pVFam, char *name, const void *pData,
                       size_t (*sizeFun) ( const void *pData,
                                           size_t *pOvh, size_t *pmData,
                                           size_t *pDataSize ) ) {

  arrFam_s *pFam = pVFam ;
  nonArr_s *pNonArr ;
  
  if ( !pFam )
    pFam = &defFam;

  pFam->mNonArr++ ;
  if ( !( pFam->pNonArr =
          realloc ( pFam->pNonArr, pFam->mNonArr*sizeof( nonArr_s ) ) ) ) {
    /* The SGI -n32 wants a return here or flashes a warning. */
    return (NULL) ;
  }

  pNonArr = pFam->pNonArr + pFam->mNonArr - 1 ;
  pNonArr->pData = pData ;
  strncpy ( pNonArr->name, name, NAME_LEN ) ;
  pNonArr->sizeFun = sizeFun ;

  return (NULL) ;
}

void *arr_del_nonArr ( const void *pData ) {

  arrFam_s **ppFam, *pFam ;
  nonArr_s *pNonArr ;

  /* Trouble is, we don't know what family pData is listed with. */
  for ( ppFam = ppAllFam ; ppFam <= ppAllFam + mFams ; ppFam++ ) {
    pFam = *ppFam ;

    for ( pNonArr = pFam->pNonArr ; pNonArr < pFam->pNonArr + pFam->mNonArr ; pNonArr++ )
      if ( pNonArr->pData == pData ) {
        /* Zap it. */
        pNonArr->pData = NULL ;
        return (NULL) ;
      }
  }
  /* Hm, nothing found. */
  return (NULL) ;
}
