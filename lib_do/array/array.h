/* array.h:
   Include file for the array class in array.c.

   Last update:
   ------------
   15Nov09; make mData and consorts a size_t.
   2Oct98; conceived.
*/

/* How much do allocate by default. */
#ifndef DEFAULT_MDATA
#  define DEFAULT_MDATA 100
#endif


/* How much storage to add on realloc. */
#ifndef REALLOC_FACTOR
#  define REALLOC_FACTOR 1.3
#endif

typedef enum { arr_add, arr_total, arr_probe, arr_sub, arr_shrink, arr_delete } arr_e ;

typedef struct _arrFam_s arrFam_s ;
typedef struct _arr_s arr_s ;

/* How to split an int into number of array and nuber in array. */
#define MAX_BYTE_ARR 7
/* That results in 2**7 arrays, 2**(32-7) pieces of data in the array. */
#define MAX_ARR 128-1
#define MAX_DATA 33554432-1

/* A compound pointer. Declare this hear to be able to store it outside of array.c. */
typedef struct {
  unsigned int nArr:MAX_BYTE_ARR ;
  unsigned int nElem:(32-MAX_BYTE_ARR) ;
} ap_s ;

/* An array NULL pointer has a zero array number. Do allow a 0 data number
   as valid address in an array. */
static const ap_s AP_NULL = {0,0} ;
static const ap_s AP_MAX = {MAX_ARR,MAX_DATA} ; 



/* Public calls: */
arrFam_s *make_arrFam ( const char *name ) ;
arr_s *make_array ( arr_s *pArray, char **ppData, size_t mData, size_t dataSize,
                    arrFam_s *pFam, const char *name ) ;
void free_arrFam ( arrFam_s **ppArrFam ) ;
void free_array ( arr_s ** ppArray ) ;

int get_lastUpdate ( const arr_s *pArray ) ;

int add_array_reference ( arr_s *pArrTo, arr_s *pArrFrom, int mToInFrom,
                          ap_s *(*xToInFrom) ( arr_s *pFrom, int nData, int mOcc ) ) ;

void *get_new_data ( arr_s *pArray, char **ppData,
                     arr_e arrOp, size_t dataSize, size_t mNewData ) ;
int sizeof_datafield ( arr_s *pArray, void *ppVData, void *ppVLstData ) ;


int pack_array ( arr_s *pArray, int *pNrUser ) ;

void show_arrUse ( arrFam_s *pFamList ) ;
void show_arrDep ( arrFam_s *pFamList ) ;
int check_array ( arr_s *pArr, char **ppData ) ;

ap_s en_ap ( arr_s *pArr, const int nr ) ;
ap_s enp_ap ( char **ppData, const int nr ) ;
void *de_ap ( const ap_s ap, const arrFam_s *pFam ) ;

void *arr_malloc ( char *pName, arrFam_s *pFam, size_t mData, size_t dataSize ) ;
void *arr_calloc ( char *pName, arrFam_s *pFam, size_t mData, size_t dataSize ) ;
void *arr_realloc ( char *pName, arrFam_s *pFam, void *pData, size_t mData, size_t dataSize ) ;
void arr_free ( void *pData ) ;

void *arr_ini_nonArr ( void *pFam, char *name, const void *pData,
                       size_t (*sizeFun) ( const void *pData,
                                           size_t *pOvh, size_t *pmData, 
                                           size_t *pDataSize ) ) ;
void *arr_del_nonArr ( const void *pData ) ;
