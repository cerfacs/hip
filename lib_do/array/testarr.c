/*
   _.c:


   Last update:
   ------------


   This file contains:
   -------------------

*/
#include <stdlib.h>
#include <stdio.h>

#include "array.h"

const int verbosity = 3 ;


/******************************************************************************

  _:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int main (  ) {
  
  typedef struct {
    int nr ;
    ap_s vx[2] ;
  } el_s ;

  typedef struct {
    int nr ;
    ap_s pCoor ;
  } vx_s ;

  arrFam_s *pGrid ;
  arr_s *paEl = NULL, *paVx = NULL, *paCo = NULL, *paInt = NULL ;
  el_s *pElem = NULL, *pEl ;
  vx_s *pVrtx = NULL, *pVx ;
  double *pCoor = NULL, *pCo, *pDbl = NULL, *pD ;
  const int mDim = 2 ;
  int *pInt, mElems = 2, mVerts = 4, nVx ;
  
  pGrid = make_arrFam ( "grid" ) ;

  paEl = make_array ( paEl, (char**) &pElem, mElems, sizeof( el_s ), pGrid, "elem" ) ;
  paCo = make_array ( paCo, (char**) &pCoor, mVerts, mDim*sizeof(double), pGrid, "coor");
  paVx = make_array ( paVx, (char**) &pVrtx, mVerts, sizeof( vx_s ), pGrid, "verts" ) ;

  paInt = make_array ( paInt, (char**) &pInt, mVerts, mDim*sizeof( int ), NULL, "int") ;


  /* Get two element slots. */
  pEl = get_new_data ( paEl, (char**) &pElem, arr_add, 0, 2 ) ;

  pEl->nr = 1 ;
  pEl->vx[0] = en_ap ( paVx, 0 ) ;
  pEl->vx[1] = en_ap ( paVx, 1 ) ;
               
  pEl++ ;      
  pEl->nr = 2 ;
  pEl->vx[0] = en_ap ( paVx, 1 ) ;
  pEl->vx[1] = en_ap ( paVx, 2 ) ;


  /* Get 3 vertex and coor slots. */
  pVx = get_new_data ( paVx, (char**) &pVrtx, arr_add, 0, 3 ) ;
  pCo = get_new_data ( paCo, (char**) &pCoor, arr_add, 0, 3 ) ;

  pVx->nr = 1 ;
  pVx->pCoor = en_ap ( paCo, 0 ) ;
  pCo[0] = 1. ; pCo[1] = 1.1 ;

  pVx++ ;
  pCo += mDim ;
  pVx->nr = 2 ;
  pVx->pCoor = en_ap ( paCo, 1 ) ;
  pCo[0] = 2. ; pCo[1] = 2.2 ;

  pVx++ ;
  pCo += mDim ;
  pVx->nr = 3 ;
  pVx->pCoor = en_ap ( paCo, 2 ) ;
  pCo[0] = 3. ; pCo[1] = 3.3 ;


  
  /* Get a total of 10 vertices. */
  pVx = get_new_data ( paVx, (char**) &pVrtx, arr_total, 0, 10 ) ;
  pCo = get_new_data ( paCo, (char**) &pCoor, arr_total, 0, 10 ) ;

  for ( nVx = 8 ; nVx <= 9 ; nVx++ ) {
    pVx = pVrtx + nVx ;
    pCo = pCoor + mDim*nVx ;
    pVx->nr = nVx ;
    pVx->pCoor = enp_ap ( (char**) &pCoor, nVx ) ;
    pCo[0] = nVx ; pCo[1] = nVx+.1*nVx ;
  }


  
  /* Get another 2 elements. */
  pEl = get_new_data ( paEl, (char**) &pElem, arr_add, 0, 2 ) ;

  pEl->nr = 3 ;
  pEl->vx[0] = en_ap ( paVx,  8 ) ;
  pEl->vx[1] = en_ap ( paVx,  9 ) ;


  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Expecting 4 elements, 10 vertices, 10 coordinates and 4 ints in grid.\n" ) ;
  show_arrUse ( NULL ) ;

  
  check_array ( NULL, NULL ) ;
  
  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Freeing elements.\n" ) ;
  free_array ( &paEl ) ;
  show_arrUse ( pGrid ) ;

  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Freeing the whole grid family.\n" ) ;
  free_arrFam ( &pGrid ) ;
  show_arrUse ( NULL ) ;



  
  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Allocating for 4 doubles w/o family.\n" ) ;
  
  pD = get_new_data ( NULL, (char**) &pDbl, arr_add, sizeof(double), 2 ) ;
  pD[0] = 0.1 ; pD[1] = 1.1 ;

  pD = get_new_data ( NULL, (char**) &pDbl, arr_add, sizeof(double), 2 ) ;
  pD[0] = 2.1 ; pD[1] = 3.1 ;
  show_arrUse ( NULL ) ;

  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Allocating for a total of 10 doubles.\n" ) ;
  
  pD = get_new_data ( NULL, (char**) &pDbl, arr_total, sizeof(double), 10 ) ;
  pD[4] = 4.1 ; pD[5]= 5.1 ;
  show_arrUse ( NULL ) ;

  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Shrinking to a total of 6 doubles, deleting the 4 ints.\n" ) ;
  
  pD = get_new_data ( NULL, (char**) &pDbl, arr_shrink, sizeof(double), 6 ) ;
  free_array ( &paInt ) ;
  show_arrUse ( NULL ) ;
  
  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Deleting the doubles.\n" ) ;
  pD = get_new_data ( NULL, (char**) &pDbl, arr_delete, 0, 0 ) ;
  show_arrUse ( NULL ) ;
 
  return ( 1 ) ;
}
