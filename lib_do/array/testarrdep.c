/*
   _.c:


   Last update:
   ------------


   This file contains:
   -------------------

*/
#include <stdlib.h>
#include <stdio.h>

#include "array.h"

const int verbosity = 3 ;

typedef struct {
  int nr ;
  ap_s pVx[2] ;
} el_s ;

typedef struct {
  int nr ;
  ap_s pCoor ;
} vx_s ;


ap_s * xCoInVx ( arr_s *paVx, int nVx, int nOcc ) {
  vx_s *pVrtx = get_new_data ( paVx, NULL, arr_probe, 0, 0 ) ;
  return ( &(pVrtx[nVx].pCoor) ) ;
}

ap_s * xVxInEl ( arr_s *paEl, int nEl, int kVx ) {
  el_s *pElem = get_new_data ( paEl, NULL, arr_probe, 0, 0 ) ;
  return ( &(pElem[nEl].pVx[kVx]) ) ;
}


/******************************************************************************

  _:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int main (  ) {
  
  arrFam_s *pGrid ;
  arr_s *paEl = NULL, *paVx = NULL, *paCo = NULL, *paInt = NULL ;
  el_s *pElem = NULL, *pEl ;
  vx_s *pVrtx = NULL, *pVx ;
  double *pCoor = NULL, *pCo, *pDbl = NULL, *pD ;
  const int mDim = 2 ;
  int *pNr, mElems = 2, mVerts = 4, nVx, *nrVx ;
  
  pGrid = make_arrFam ( "grid" ) ;

  paEl = make_array ( paEl, (char**) &pElem, mElems, sizeof( el_s ), pGrid, "elem" ) ;
  paCo = make_array ( paCo, (char**) &pCoor, mVerts, mDim*sizeof(double), pGrid, "coor");
  paVx = make_array ( paVx, (char**) &pVrtx, mVerts, sizeof( vx_s ), pGrid, "verts" ) ;



  /* Declare dependencies. */
  add_array_reference ( paCo, paVx, 1, xCoInVx ) ;
  add_array_reference ( paVx, paEl, 2, xVxInEl ) ;

  show_arrDep ( NULL ) ;
  
  /* Get a total of 10 vertices. */
  pVx = get_new_data ( paVx, (char**) &pVrtx, arr_total, 0, 10 ) ;
  pCo = get_new_data ( paCo, (char**) &pCoor, arr_total, 0, 10 ) ;

  for ( nVx = 0 ; nVx < 3 ; nVx++ ) {
    pVx = pVrtx + nVx ;
    pCo = pCoor + mDim*nVx ;
    pVx->nr = nVx ;
    pVx->pCoor = enp_ap ( (char**) &pCoor, nVx ) ;
    pCo[0] = nVx ; pCo[1] = nVx+.1*nVx ;
  }
  for ( nVx = 7 ; nVx < 9 ; nVx++ ) {
    pVx = pVrtx + nVx ;
    pCo = pCoor + mDim*nVx ;
    pVx->nr = nVx ;
    pVx->pCoor = enp_ap ( (char**) &pCoor, nVx ) ;
    pCo[0] = nVx ; pCo[1] = nVx+.1*nVx ;
  }

  check_array ( NULL, NULL ) ;
  show_arrUse ( NULL ) ;

  /* Get four element slots. */
  pEl = get_new_data ( paEl, (char**) &pElem, arr_add, 0, 2 ) ;

  pEl->nr = 1 ;
  pEl->pVx[0] = en_ap ( paVx, 0 ) ;
  pEl->pVx[1] = en_ap ( paVx, 1 ) ;
               
  pEl++ ;      
  pEl->nr = 2 ;
  pEl->pVx[0] = en_ap ( paVx, 1 ) ;
  pEl->pVx[1] = en_ap ( paVx, 2 ) ;

  pEl = get_new_data ( paEl, (char**) &pElem, arr_add, 0, 2 ) ;
  pEl++ ; ;       
  pEl->nr = 4 ;
  pEl->pVx[0] = en_ap ( paVx, 7 ) ;
  pEl->pVx[1] = en_ap ( paVx, 8 ) ;


  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Expecting 4 elements, 10 vertices, 10 coordinates in grid.\n" ) ;
  show_arrUse ( NULL ) ;
  check_array ( NULL, NULL ) ;
  

  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Packing the coordinates, vertices.\n" ) ;
  pack_array ( paCo, NULL ) ;
  pack_array ( paVx, NULL ) ;

  printf ( "\n\n\n---------------------------------------------------------------\n"
           " Packing the elements using the user's number field.\n" ) ;
  pNr = arr_calloc ( "elNr", 4, sizeof ( int ) ) ;
  pNr[0] = pNr[1] = pNr[3] = 1 ;
  pNr[2] = 0 ;
  
  pack_array ( paEl, pNr ) ;
  arr_free ( pNr ) ;
  
  return ( 1 ) ;
}
