/* 
 C binding for the fortran lapack interfaces used in hip.

 Last update:
 ------------
 13Jul15; conceived.

*/
void dgeqrf_( const int *m, const int *n, double *AtWk, const int *ldat, 
              double *tau, double *work, int *lw, int *info ) ;


void dtrtrs_( const char *cU, const char *cT, const char *cN, 
              const int *n, const int *one, 
              double *AtWk, const int *ldat, double *al, const int *lb, 
              int *info ) ;

void dormqr_( const char *cL, const char *cN, 
              const int *m, const int *one, const int *n, 
              double *AtWk, const int *ldat, double *tau, 
              double *al, const int *lb, 
              double *work, const int *lw, int *info ) ;
