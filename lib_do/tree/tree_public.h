/*
 Public access to tree.c. Prototypes.

 Last update:
 29Nov09; use size_t type args in sizeof_tree.
 30May96; move typedefs and some macros to public, eliminate void pointers.
 5Mar96; cut out of tree.c.
*/

/* Typedefs. No need to tell more in public. */
typedef struct _root_struct root_struct ;
typedef struct _tree_pos_struct tree_pos_struct ;

/* Prototypes. */
root_struct *ini_tree ( void *pFam, char *treeName,
                        const int mDim, const VALU_TYPE *Pll, const VALU_TYPE *Pur,
			VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) ;
void del_tree ( root_struct **PPRoot ) ;
size_t sizeof_tree ( const void *pRoot, size_t *pOvh, size_t *pmNodes, size_t *pNodeSize ) ;

DATA_TYPE *add_data ( root_struct *Proot, const DATA_TYPE *Pdata ) ; 
void del_data ( root_struct *Proot, const DATA_TYPE *Pdata ) ; 

tree_pos_struct *ini_traverse ( const root_struct *Proot ) ;
void reset_traverse ( tree_pos_struct *PTreePos ) ;
void del_traverse ( tree_pos_struct **PPTreePos ) ;
DATA_TYPE *traverse_tree ( tree_pos_struct *PTreePos ) ;

void reset_range ( tree_pos_struct *PTreePos,
		   const VALU_TYPE *PllRange, const VALU_TYPE *PurRange ) ;
DATA_TYPE *range_search ( tree_pos_struct *PTreePos,
			  const VALU_TYPE *PllRange, const VALU_TYPE *PurRange ) ;


int nearest_datas ( const root_struct *pRoot, const DATA_TYPE *Pdata, int mData,
		    const DATA_TYPE *pNearestData, const size_t dataSize,
                    VALU_TYPE nearestDistSq[] ) ;
DATA_TYPE *nearest_data ( const root_struct *PRoot, const DATA_TYPE *Pdata,
			  VALU_TYPE *PnearestDist ) ;

VALU_TYPE dist ( const VALU_TYPE *PvalA, const VALU_TYPE *PvalB, const int mDim ) ;
VALU_TYPE distSq ( const VALU_TYPE *PvalA, const VALU_TYPE *PvalB, const int mDim ) ;

/* User supplied function to extract the pointer to contiguous mDim data of VALU_TYPE
   from the pointer to DATA_TYPE. */
VALU_TYPE *data2valu ( const DATA_TYPE *Pdata ) ;

void reset_trace () ;
void print_trace () ;
