/*
 Private access to tree.c. Macros, data-types, prototypes.

 Last update:
 ------------
 14Jan01; intro contain
 20Sep99; intro arrFam ;
 30May96; move typedefs to public. 
 5Mar96; cut out of tree.c.
*/

#define FAILURE -1

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#define ABS(a)   ( (a) > 0 ? (a) : (-(a)) )

/* root_struct and tree_pos_struct are typedef'd in tree_public.h. */
typedef struct _tree_struct tree_struct ;
typedef struct _frame_struct frame_struct ;

struct _root_struct {
  tree_struct *Pbox ;
  int mDim ;                  /* That many dimensions. */
  int mBox ;                  /* Calculate once and for all the number of boxes. */

  int mNodes ;                /* Count the number of nodes of this root for storage checking. */
  
  VALU_TYPE llRoot[MAX_DIM] ; /* Bounding box of the root */
  VALU_TYPE urRoot[MAX_DIM] ;

  VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ; /* Extraction function. */
			     
  tree_pos_struct *ProotPos ; /* Hidden default iterator. */
} ;

struct _tree_struct {
  tree_struct *Pparent ;      /* Parent. */
  int mData ;                 /* leaf: 0<=mData<=MAX_BOX: so many pieces of data,
			         branch: -mBox: there are mBox children. */
  union {                      /* Make the pointers to the children and the */
    tree_struct **PPchild ;   /* pointers to the data share the same storage. */
    const DATA_TYPE **PPdata ;/* Sub-box indices run like in binary numbers */
  } shr ;                     /* along x,y,z, x running the fastest, i.e. in 2D
			         and for the lower z in 3D:
			           y^                y^        
			            | 2  3            | 6  7   
			            | 0  1            | 4  5   
			           -|-----> x        -|-----> x
			              z=0               z=1                   */
} ;

struct _tree_pos_struct {
  const root_struct *Proot ;   /* Hide the root. */
  
  frame_struct *Pframe ;       /* Current frame of this iterator. Frames track
				  the geometric ll-ur information. For each
				  newly needed level a new frame struct is
				  allocated in the doubly linked list. Dealloc
				  happens only upon destruction of the iterator. */
} ;

struct _frame_struct {
  tree_struct *Pbox ;          /* Pointer to the current box. */
  int iPos ;                   /* Where are we at in the current box. */
  VALU_TYPE ll[MAX_DIM] ;      /* Bounding box of the current box. */
  VALU_TYPE ur[MAX_DIM] ;
  
  frame_struct *PupFrame ;     /* Frames up and down one level. */
  frame_struct *PdownFrame ;    
} ;



/* private: */
tree_struct *add_child ( tree_struct *Pparent, root_struct *Proot ) ;

void del_subtree ( tree_struct *Pbox, root_struct *Proot ) ;
void del_children ( tree_struct *Pbox, root_struct *Proot ) ;

tree_struct *find_box ( tree_pos_struct *PTreePos, const DATA_TYPE *Pdata,
			VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) ;
tree_struct *find_box_inc ( tree_pos_struct *PTreePos, const DATA_TYPE *Pdata,
			    VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) ;

const DATA_TYPE *traverse_subtree ( tree_pos_struct *Ppos ) ;
int up_traverse ( tree_pos_struct *PTreePos ) ;
int down_traverse ( tree_pos_struct *PTreePos, int nBox ) ;

void center ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
	      VALU_TYPE *Pcenter, int mDim ) ;

int compare ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
	      const VALU_TYPE *Pval,
	      int mDim, int *dir ) ;
int compare_inc ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
		  const VALU_TYPE *Pval,
		  int mDim, int *dir ) ;

int range_is_positive ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
		        int mDim ) ;

int overlap ( const VALU_TYPE *Pll1, const VALU_TYPE *Pur1,
	      const VALU_TYPE *Pll2, const VALU_TYPE *Pur2,
	      int mDim ) ;
int contain ( const VALU_TYPE *Pll1, const VALU_TYPE *Pur1,
	      const VALU_TYPE *Pll2, const VALU_TYPE *Pur2,
	      int mDim ) ;

void calc_box ( const VALU_TYPE *PllBox, const VALU_TYPE *PurBox,
	        int mDim, int nPos,
	        VALU_TYPE *PllChild, VALU_TYPE *PurChild, VALU_TYPE *Pcenter ) ;
