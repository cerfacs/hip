/*
 tree_public.c:
 Quadtree/Octree structure, public functions.

 Last update:
 ------------
 7Apr13; include array.h
 12Aug10; add an independent copy of hdo_err for error handling.
 Jul10; derived from lib_do's tree_public.
 23Jan01; treat a range that overlaps with the root in range_search.
 14Jan01; range_search does not up_traverse past the containing frame any longer.
          Seems much more efficient.
 9Apr00; make copies of extracted values if they are compared. In this way
         data2value can compute and store one value, rather than just extracting
         a pointer.
 20Sep99; intro arrFam.
 4May98; remove onceness in add_data. Pack data2valu into the root_struct.
 3Jun96; intro VALU_TYPE.
 29Feb96; conceived.

 Contains:
 ---------
 ini_tree
 del_tree
 add_data
 del_data
 ini_traverse
 reset_traverse
 del_traverse
 traverse_tree
 reset_range
 range_search
 neareset_data
 dist
 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "tree_data.h"
#include "./tree_public.h"
#include "./tree_private.h"
#include "../array/array.h"

typedef enum { noErr, fatal, warning, info } tree_err_e ;

#define TXT_LEN 128 /* Max. text length */
char tree_msg[10*TXT_LEN] ;
int verbos=3 ;



/* To trace the performance of the tree.
   maxBoxes: maximum number of boxes belew the containing one in a nearest search.
   mBoxes:   total number of boxes searched
   mSearches: number of searches executed.
*/


/*#define TRACE */

#ifdef TRACE
/* Total counters. */
int maxBoxes_t, mBoxes_t, maxBoxesOvlp_t, mBoxesOvlp_t, maxData_t, mData_t, mSearches_t ;
/* Counter per single search. */
int mBox_t, mBoxOvlp_t, mDat_t ;
#endif

void reset_trace () {
#ifdef TRACE
  maxBoxes_t = mBoxes_t = maxBoxesOvlp_t = mBoxesOvlp_t =
    maxData_t = mData_t = mSearches_t = 0 ;
#endif
  return ;
}

void print_trace () {

#ifdef TRACE
  if ( mSearches_t ) {
    printf ( " mSearches: %d,\n"
             " avg Boxes: %f, maxBoxes: %d,\n"
             " avg BoxesOvlp: %f, maxBoxesOvlp: %d,\n"
             " avg Data %f, maxData %d.\n",
             mSearches_t,
             1.*mBoxes_t/mSearches_t, maxBoxes_t,
             1.*mBoxesOvlp_t/mSearches_t, maxBoxesOvlp_t,
             1.*mData_t/mSearches_t, maxData_t ) ;
  }
  else
    printf ( " No searches.\n" ) ;
#endif

  return ;
}


/******************************************************************************

  tree_err:
  Print an error message and terminate, if appropriate.
  
  Last update:
  ------------
  12Aug10: derived from hairdo's error handler hdo_err.
  
  Input:
  ------
  
  
  
*/

void tree_err  ( tree_err_e errStat, const int verbLvl, const char* msg ) {

  FILE *fl ;
  const char eLabel[][20] = { "          ", "FATAL", "WARNING", "INFO" } ;

  if ( verbos >= verbLvl )
    printf ( " %s: %s\n", eLabel[errStat], msg ) ;


  if ( errStat == fatal || ( verbLvl == 0 && errStat == warning ) ) {
    /* Either fatal error or warning under machine-run script, terminate. */
    fl = fopen ( "tree_error.log", "w" ) ;
    fprintf ( fl, " %s: %s\n", eLabel[errStat], msg ) ;
    fclose ( fl ) ;
    exit ( EXIT_FAILURE ) ; 
  }

  return ;
}



/******************************************************************************

  ini_tree:
  Initialize a tree.
  
  Last update:
  ------------
  16Dec12; complete interface description, no code change.
  7Apr00; make a copy of *data2valu, so that data2valu can store a modified
          set of values.
  : conceived.
  
  Input:
  ------
  pFam: array family to associate allocation with in arr_
  name: name of allocating routine for debugging purposes
  mDim:      number of dimensions.
  Pll/Pur:   lower left/ upper right inclusive bounding box. (All data items must
             be > resp. < than the bounds. ( not<= ).
  data2valu: a function extracting the VALU_TYPE to compare from DATA_TYPE.

  Returns:
  --------
  the pointer to the tree root, Proot.
  
*/

root_struct *ini_tree ( void *pFam, char name[], 
                        const int mDim, const VALU_TYPE *Pll, const VALU_TYPE *Pur,
			VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) {
  /* Initialize the tree, give it a bounding box. */

  root_struct *Proot=NULL ;
  int nDim ;

#ifdef TRACE
  reset_trace () ;
#endif

  if ( mDim < 1 ) {
    sprintf ( tree_msg, "invalid dimension %d in ini_tree.\n", mDim ) ;
    tree_err ( fatal, 0, tree_msg ) ; }
  else if ( !( Proot = ( root_struct * ) malloc ( sizeof( root_struct ) ) ) ) {
    printf ( " FATAL: could not allocate the root in ini_tree.\n" ) ;
    tree_err ( fatal, 0, tree_msg ) ; }
  else if ( !range_is_positive ( Pll, Pur, mDim ) ) {
    printf ( " FATAL: negative volume in ini_tree.\n" ) ;
    tree_err ( fatal, 0, tree_msg ) ; }



  arr_ini_nonArr ( pFam, name, Proot, sizeof_tree ) ;
 
  /* Calculate once and for all the number of boxes in this tree, avoiding math.h. */
  for ( Proot->mBox = 1, nDim = 0 ; nDim < mDim ; nDim++ )
    Proot->mBox *= 2 ;

    /* Add a default leaf. */
  Proot->Pbox = add_child( NULL, Proot ) ;
  Proot->mDim = mDim ;

  
  Proot->mNodes = 1 ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    Proot->llRoot[nDim] = Pll[nDim] ;
    Proot->urRoot[nDim] = Pur[nDim] ; }

  /* The extraction function. */
  Proot->data2valu = data2valu ;

  /* Hidden iterator */
  if ( !( Proot->ProotPos = ini_traverse ( Proot ) ) ) {
    sprintf ( tree_msg, "could not allocate the default iterator in ini_tree.\n" ) ;
    tree_err ( fatal, 0, tree_msg ) ; }
    
  return ( Proot ) ;
}

void del_tree ( root_struct **PPRoot ) {

  /* Destructor, whole tree. */
  root_struct *Proot = *PPRoot ;

  if ( !Proot )
    return ;

  arr_del_nonArr ( Proot ) ;
  
  /* Delete all children. */
  if ( Proot->Pbox )
    del_subtree( Proot->Pbox, Proot ) ; 
  
  /* Delete the storage for the hidden iterator. */
  free ( Proot->ProotPos ) ;

  /* Delete the storage for the root. */
  free ( Proot ) ;
  *PPRoot = NULL ;

#ifdef TRACE
  print_trace () ;
#endif
  
  return ;
}

size_t sizeof_tree ( const void *pVRoot, size_t *pOvh, size_t *pmNodes,
                     size_t *pNodeSize ) {

  const root_struct *pRoot = pVRoot ;
  
  if ( !pRoot )
    return ( 0 ) ;
  else {
    *pOvh = sizeof( root_struct ) ;
    *pmNodes =  pRoot->mNodes ;
    *pNodeSize = sizeof( tree_struct ) + pRoot->mBox*sizeof( void * ) ;

    return ( *pOvh + (*pmNodes)*(*pNodeSize) ) ;
  }
}
  

/********************************************************************************

  add_data:
  Add an entry pointed to by Pdata into the tree PRoot. Note that data2valu(Pdata)
  must be distinct. Otherwise adding more than mDim of the same will result in an
  endless loop.

  Last update:
  ------------
  2Apr19; initialise vCp to pacify compiler.
  4May98; remove onceness.
  3Jun96; new tree.

  Input:
  ------
  Proot:
  Pdata:

  Changes to:
  -----------
  Proot->...

  Returns:
  --------
  NULL on failure, Pdata on success, the pointer to the duplicate in the tree
  if Pdata has not been added due to duplication.

*/

DATA_TYPE *add_data ( root_struct *Proot, const DATA_TYPE *Pdata )  {
  /*  */
  const DATA_TYPE *PPoldData[MAX_BOX], *PotherData ;
  VALU_TYPE *pVCp, vCp[MAX_DIM] = {0}, *pOV ;
  int nBox, nDim, duplicate ;
  tree_struct *Pbox ;

  /* First check if the root is valid. */
  if ( !Proot ) {
    printf ( " WARNING: no such tree in add_data.\n" ) ;
    return ( NULL ) ; }

  /* Find the box the data goes into. */
  if ( !( Pbox = find_box( Proot->ProotPos, Pdata, Proot->data2valu ) ) ) {
    printf ( " FATAL: could not find a containing box in add_data.\n" ) ;
    return ( NULL ) ; }

  /* Is the value already in the tree? */
  for ( nBox = 0 ; nBox < Pbox->mData ; nBox++ ) {
    PotherData = (Pbox->shr.PPdata)[nBox] ;
    
    /* Make a copy of the first invocation of data2valu. */
    pVCp = Proot->data2valu( Pdata ) ;
    for ( nDim = 0 ; nDim < Proot->mDim ; nDim++ )
      vCp[nDim] = pVCp[nDim] ;

    pOV = Proot->data2valu( PotherData ) ;
    for ( duplicate = 1, nDim = 0 ; nDim < Proot->mDim ; nDim++ )
      if ( vCp[nDim] != pOV[nDim] )
	duplicate = 0 ;
    if ( duplicate )
      /* This is duplicate data. */
      return ( ( DATA_TYPE * ) PotherData ) ;
  }
  
  if ( Pbox->mData < Proot->mBox ) {
    /* Add another piece of data to the found box. */
    (Pbox->shr.PPdata)[ (Pbox->mData)++ ] = Pdata ;
    return ( ( DATA_TYPE * ) Pdata ) ; }
  
  else {
    /* This box is full. Proliferate, sort the entries into the new boxes. */
    for ( nBox = 0 ; nBox < Proot->mBox ; nBox++ ) {
      /* Save the data of the box. */
      PPoldData[nBox] = (Pbox->shr.PPdata)[nBox] ;
      if ( !( (Pbox->shr.PPchild)[nBox] = add_child( Pbox, Proot ) ) )
	/* Alloc failed. */
	return ( NULL ) ;
    }

    /* Make this box a branch. */
    Pbox->mData = -Proot->mBox ;
    
    /* Pass the data to the children. */
    for ( nBox = 0 ; nBox < Proot->mBox ; nBox++ )
      add_data ( Proot, PPoldData[nBox] ) ;

    /* Add the new data. */
    add_data ( Proot, Pdata ) ;
    
    return ( ( DATA_TYPE * ) Pdata ) ;
  }
}

void del_data ( root_struct *Proot, const DATA_TYPE *Pdata )  {
  
  /* Delete an entry. If it was the last one in the box, erase the box. */
  int nBox, allEmpty ;
  tree_struct *Pbox ;

  if ( !Proot )
  { printf ( " WARNING: no such tree in del_data.\n" ) ;
    return ;
  }
  if ( !( Pbox = find_box( Proot->ProotPos, Pdata, Proot->data2valu ) ) )
    /* Data not in the root, so it was never introduced. Ignore. */
    return ;
  else {
    /*  Loop over all entries in this box. */
    for ( nBox = 0 ; nBox < Proot->mBox ; nBox++ )
      if ( Pbox->shr.PPdata[nBox] == Pdata ) {
        /* This is the one. */
	
	if ( Pbox->mData > 1 ) {
	  /* Copy the last entry over it and decrement the counter. */
	  (Pbox->shr.PPdata)[nBox] = (Pbox->shr.PPdata)[--(Pbox->mData)] ;
	  return ;
	}
    
	else if ( Pbox->Pparent ) {
	  /* This child will then be empty. Check recursively whether
	     all other siblings are empty, too. */
	  Pbox->mData = 0 ;
	  allEmpty = 1 ;
	  while ( allEmpty && Pbox->Pparent ) {
	    /* Scan all siblings, i.e. all children of the same parent. */
	    Pbox = Pbox->Pparent ;
	    for ( nBox = 0 ; nBox < Proot->mBox ; nBox++ )
	      if ( (Pbox->shr.PPchild)[nBox]->mData )
		break ;
	    
	    if ( nBox >= Proot->mBox ) {
	      /* All children of this box are empty. Remove them, and
		 make their parent a leaf. */
	      del_children( Pbox, Proot ) ;
	      Pbox->mData = 0 ;
	    }
	    else
	      /* Some sibling was not empty. */
	      allEmpty = 0 ;
	  }
	}

	else {
	  /* Found the data, it is the last one, but it is in the root. */
	  Pbox->mData-- ;
	  return ;
	}
      }

    /* Data not found. */
    return ;
  }
}

tree_pos_struct *ini_traverse( const root_struct *Proot ) {
  /* Initialize an iterator. */
  tree_pos_struct *PPos ;
  frame_struct *Pframe ;
  int nDim ;

  if ( !Proot )
  { printf ( " WARNING: No such tree in ini_traverse.\n" ) ;
    return ( NULL ) ;
  }

  PPos = ( tree_pos_struct * ) malloc( sizeof( tree_pos_struct ) ) ;
  Pframe = ( frame_struct * ) malloc( sizeof( frame_struct ) ) ;
  if ( !PPos || !Pframe )
  { printf ( " FATAL: could not allocate an iterator in ini_traverse.\n" ) ;
    return ( NULL ) ;
  }
  else
  { PPos->Proot = Proot ;
    PPos->Pframe = Pframe ;

    /* Set the first frame to the rootbox. */
    for ( nDim = 0 ; nDim< Proot->mDim ; nDim++ )
    { Pframe->ll[nDim] = Proot->llRoot[nDim] ;
      Pframe->ur[nDim] = Proot->urRoot[nDim] ;
    }
    Pframe->Pbox = Proot->Pbox ;
    Pframe->iPos = -1 ;
    Pframe->PupFrame = Pframe->PdownFrame = NULL ;

    return ( PPos ) ;
  }
}

void reset_traverse ( tree_pos_struct *PTreePos ) {
  /* Reset an iterator. */
  frame_struct *Pframe ;

  if ( !PTreePos )
  { printf ( " WARNING: No such iterator in reset_traverse.\n" ) ;
    return ;
  }
  else
  { /* Scan the frames up to the root. */
    for ( Pframe = PTreePos->Pframe ;
	  Pframe->PupFrame ; Pframe = Pframe->PupFrame )
      ;
    Pframe->iPos = -1 ;
  
    /* Have the iterator point to the root. */
    PTreePos->Pframe = Pframe ;
  }

}

void del_traverse ( tree_pos_struct **PPTreePos ) {
  /* Destroy the iterator. */
  frame_struct *Pframe, *PdownFrame ;
  tree_pos_struct *PTreePos ;
  PTreePos = *PPTreePos ;

  if ( !PTreePos )
    /* No such iterator. */
    return ;
  
  /* Scan the frames up to the root. */
  for ( Pframe = PTreePos->Pframe ;
        Pframe->PupFrame ; Pframe = Pframe->PupFrame )
    ;
  /* Erase the doubly linked list of frames. */
  do
  { PdownFrame = Pframe->PdownFrame ;
    free ( Pframe ) ;
    Pframe = PdownFrame ;
  } while ( PdownFrame ) ;

  /* Erase the iterator. */
  free( PTreePos ) ;
  *PPTreePos = NULL ;
}

DATA_TYPE *traverse_tree ( tree_pos_struct *PTreePos ) {
  /* Iterator: traverse the tree and return 1 element at a time
     using a lexicographic scan. As opposed to traverse_subtree
     in tree_private.c, do traverse upward if the current frame
     is exhausted. */
  frame_struct *Pframe ;
  
  if ( !PTreePos )
  { printf ( " WARNING: No such iterator in traverse_tree.\n" ) ;
    return ( NULL ) ;
  }
  if ( !PTreePos->Proot )
  { printf ( " WARNING: No such tree in traverse_tree.\n" ) ;
    return ( NULL ) ;
  }

  while ( 1 ) {
    Pframe = PTreePos->Pframe ;

    if ( Pframe->Pbox->mData >= 0 &&
	(Pframe->iPos)+1 < Pframe->Pbox->mData )
      /* This is a leaf and there is more data here. Avoid the
         incrementation of iPos in the if construct, since it
	 is not guaranteed that the second part of the condition
	 is not evaluated on mData<0.*/
      return ( ( DATA_TYPE * )
	       (Pframe->Pbox->shr.PPdata)[++(Pframe->iPos)] ) ;
    
    else if ( down_traverse ( PTreePos, ++(Pframe->iPos) ) )
      /* This is a branch and there are more children. Incrementing
	 iPos before meeting the condition is safe, since on failure
	 one returns to the parent and the current Pframe becomes
	 obsolete. */
       ;
    else if ( up_traverse ( PTreePos ) )
      /* There is a parent. */
       ;
    else
      /* All children have been empty. */
      return ( NULL ) ;
  }
}

void reset_range ( tree_pos_struct *PTreePos,
		   const VALU_TYPE *PllRange, const VALU_TYPE *PurRange ) {
  /* Reset the Iterator for a range search. Traverse upward until
     the range is completely contained in the frame. */
  
  int mDim= PTreePos->Proot->mDim ;

  while ( !contain( PTreePos->Pframe->ll, PTreePos->Pframe->ur,
                    PllRange, PurRange, mDim ) )
    if ( !up_traverse ( PTreePos ) )
      break ;
  
  PTreePos->Pframe->iPos = -1 ;
  return ;
}

	
DATA_TYPE *range_search( tree_pos_struct *PTreePos,
			 const VALU_TYPE *PllRange, const VALU_TYPE *PurRange ) {
  
  /* Iterator: traverse the tree and return the next element
     in the range ll-ur at a time using a lexicographic scan.
     range_search maintains the list of frames. Note that
     range_search cannot employ traverse_subtree since each
     branch has to be tested for overlap with the range.*/
  int dir[MAX_DIM] ;
  volatile int mDim ;
  const DATA_TYPE *Pdata ;
  frame_struct *Pframe ;

  
  if ( !PTreePos ) {
    printf ( " WARNING: Empty iterator in range_search.\n" ) ;
    return ( NULL ) ; }
  else if ( !PTreePos->Proot ) {
    printf ( " WARNING: Empty tree in range_search.\n" ) ;
    return ( NULL ) ; }

  mDim = PTreePos->Proot->mDim ;
  if ( !range_is_positive ( PllRange, PurRange, mDim ) ) {
    printf ( " WARNING: Empty range in range_search.\n" ) ;
    return ( NULL ) ; }

  
  while ( 1 ) {
    Pframe = PTreePos->Pframe ;

    if ( Pframe->Pbox->mData >= 0 &&
	(Pframe->iPos)+1 < Pframe->Pbox->mData ) {
      /* This is a leaf and there is more data here. Avoid the
         incrementation of iPos in the if construct, since it
	 is not guaranteed that the second part of the condition
	 is not evaluated on mData<0. */
#     ifdef TRACE
        mDat_t++ ;
#     endif
      Pdata = (Pframe->Pbox->shr.PPdata)[ ++(Pframe->iPos) ] ;
      if ( !( compare_inc ( PllRange, PurRange, PTreePos->Proot->data2valu ( Pdata ),
			    mDim, dir ) ) )
	/* The current piece of data is in the range. */
	return ( ( DATA_TYPE * ) Pdata ) ;
    }
    
    else if ( down_traverse ( PTreePos, ++(Pframe->iPos) ) ) {
      /* This is a branch and there are more children. Incrementing
	 iPos before meeting the condition is safe, since on failure
	 one returns to the parent and the current Pframe becomes
	 obsolete. */
#     ifdef TRACE
        mBox_t++ ;
#     endif
      if ( !overlap ( PTreePos->Pframe->ll, PTreePos->Pframe->ur,
		      PllRange, PurRange, mDim ) ) {
	/* There is no overlap. Don't search this child. Back up. */
	up_traverse ( PTreePos ) ; }
#     ifdef TRACE
      else
        mBoxOvlp_t++ ;
#     endif
    }
    else if ( !contain( Pframe->ll, Pframe->ur, PllRange, PurRange, mDim ) ) {
      /* Don't uptraverse past the containing frame. */
      if ( !up_traverse ( PTreePos ) )
        /* A search at the edge of the root box. The range overlaps with the
           root. Done searching. */
        return ( NULL ) ;
    }
    else
      /* All children have been empty. */
      return ( NULL ) ;
  }
}

/******************************************************************************

  add2list:
  Add an entry to the sorted list of data and distances. Drop duplicates.
  
  Last update:
  ------------
  12May11; implement pointer artihmetic on void*.
  8Sep10: conceived.
  
  Input:
  ------
  mData: length of pNearestData and pNearestDist
  pOtherData: data to add
  otherDist: distance of other data

  In/Out:
  -------
  *pmD:  number uf current entries
  *pNearestData: list of nearest entries
  *pNearestDist: distance of nearest entries
  
*/

void add2list ( int mData, int *pmD, 
                const DATA_TYPE *pNearestData, size_t dataSize,
                VALU_TYPE nearestDist[], 
                const DATA_TYPE *pOtherData, VALU_TYPE otherDist ) {

  int n0, n1, n ;
  void *pNearestDataN, *pNearestDataNm1, *pNearestDataN1 ;

  if ( *pmD == 0 ) {
    /* Empty list. */
    memcpy( (void*) pNearestData, (void*) pOtherData, dataSize ) ;
    *nearestDist = otherDist ;
    *pmD = 1 ;
    return ;
  }

  /* Find the slot n1 in the list where this data should be written into. 
     First bracket the search interval. */

  else if ( otherDist >= nearestDist[*pmD-1] ) {
    /* Append at the end of the list, but compare to the final listed elem
       for duplication . */
    n1 = *pmD ;
  }

  else if ( otherDist <= nearestDist[0] ) {
    /* Insert at the top of the list. */
    if ( !memcmp ( (void*) pOtherData, (void*) pNearestData, dataSize ) )
      /* Duplicate. Drop it. */
      return ;
    else
      n1 = 0 ;
  }

  else {
    /* Find the slot in the sorted list where otherData fits. 
       The slot will be between n0,n1. */
    for ( n0 = 0, n1 = *pmD-1 ; n1-n0 > 1 ;  ) {
      n = (n1+n0)/2 ;
      if ( otherDist < nearestDist[n] )
        n1 = n ;
      else 
        n0 = n ;
    }
  }


  if ( n1 ) {
    /* There may be duplicate data items with the samve value ahead
       in the list. Filter them out. */
    for ( n = n1-1 ; n >=0 && otherDist == nearestDist[n] ; n-- ) {
      pNearestDataN = (char*)pNearestData+n*dataSize ;
      if ( !memcmp ( pOtherData,  pNearestDataN, dataSize ) )
        /* Duplicate. Drop it. */  
        return ;
    }
  }



  if ( n1 == *pmD && *pmD == mData )
    /* Full list, and otherData further than the mData closest, drop it. */
    return ;
  else if ( *pmD == mData ) 
    /* The list is full, drop the last item off. */
    (*pmD)-- ;

  /* Copy the bottom list down. */
  for ( n = *pmD ; n > n1 ; n-- ) {
    pNearestDataN   = (char*)pNearestData + (n  )*dataSize ;
    pNearestDataNm1 = (char*)pNearestData + (n-1)*dataSize ;

    memcpy( (void*) pNearestDataN, (void*) pNearestDataNm1, dataSize ) ;
    nearestDist[n] = nearestDist[n-1] ;
  }

  pNearestDataN1 = (char*)pNearestData + (n1)*dataSize ;
  memcpy( (void*) pNearestDataN1, (void*) pOtherData, dataSize ) ;
  nearestDist[n1] = otherDist ;
  (*pmD)++ ;

  return ;
}



/************************************************************************************

  nearest_datas:
  List the mData nearest data entries to a given data in the tree. 

  Last update:
  ------------
  18Aug10; derived from nearest_data ;

  Input:
  ------
  pRoot:
  pData:
  mData: number of data entries to list 

  Changes to:
  -----------
  pNearestData: List of nearest data entries.
  pNearestDist: List of nearest distances squared.

  Returns:
  --------
  the number of nearest data entries.

*/

int nearest_datas ( const root_struct *pRoot, const DATA_TYPE *Pdata, int mData,
		    const DATA_TYPE *pNearestData, const size_t dataSize,
                    VALU_TYPE nearestDistSq[] ) {
  
  /* Find the nearest data entry to Pdata. */
  const DATA_TYPE *pOthDat ;
  VALU_TYPE nrstDst, othDst, dist ;
  VALU_TYPE llRg[MAX_DIM], urRg[MAX_DIM], *pVCp, vCp[MAX_DIM] ;
  int nDim, k, mDatBox ;
  int mD ; /* number of data items found so far. */
  volatile int mDim ;
  tree_pos_struct *Ppos ;
  int filling ;
  
  Ppos = pRoot->ProotPos ;
  mDim = pRoot->mDim ;
  nrstDst = 1.e25 ;

  /* First check if the root is valid. */
  if ( !pRoot ) {
    printf ( " WARNING: Empty tree in nearest_data.\n" ) ;
    return ( 0 ) ; }

  /* Find any box the data goes into. Use the default iterator. */
  if ( !find_box_inc ( Ppos, Pdata, pRoot->data2valu ) ) {
    printf ( " WARNING: could not find a containing box in nearest_data.\n" ) ;
    return ( 0 ) ; }


  /* Make a copy of the first invocation of data2valu. In this way data2valu
     can store one intermediate result and be invoked again for the comparison. */
  pVCp = pRoot->data2valu( Pdata ) ;
  for ( nDim = 0 ; nDim < pRoot->mDim ; nDim++ )
    vCp[nDim] = pVCp[nDim] ;

  
  if ( Ppos->Pframe->Pbox->mData == 0 ) {
    /* There is no data here. However there must be in one of the
       siblings or one of the siblings descendants. Otherwise the
       children would never have been created or would have been
       erased by del_data. Thus, traverse up one level, initialize the range
       with the distance to the edges of this box. */
    up_traverse ( Ppos ) ;
    
    Ppos->Pframe->iPos = -1 ;
    if ( !traverse_subtree ( Ppos ) )
      /* The tree is empty. */
      return ( 0 ) ;
  }

  
  /* Loop over all these elements and find the closest to initialise the range. */
  nrstDst = TOO_MUCH ;
  mDatBox = Ppos->Pframe->Pbox->mData ;
  /* Empty the list. */
  mD = 0 ;
  for ( k = 0 ; k < mDatBox ; k++ ) {
    pOthDat = Ppos->Pframe->Pbox->shr.PPdata[k] ;


    othDst = distSq ( vCp, pRoot->data2valu( pOthDat ), mDim ) ;
    /* Insert into the list. */
    add2list ( mData, &mD, pNearestData, dataSize, nearestDistSq, pOthDat, othDst ) ;
  }
   




  /* Do a number of range searches with incrementally enlarged boxes. */
  filling = 1 ;
  while ( filling ) {
  
    /* Reset the range for the search. */
    dist = sqrt( nearestDistSq[mD-1] ) ;
    for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
      llRg[nDim] = vCp[nDim] - 2*dist ;
      urRg[nDim] = vCp[nDim] + 2*dist ;
    }


    /* Reset the range. */
    reset_range ( Ppos, llRg, urRg ) ;

#ifdef TRACE
    mBox_t = mBoxOvlp_t = 1 ;
    mDat_t = mDatBox ;
#endif


    /* Range search this box. */
    while ( ( pOthDat = range_search ( Ppos, llRg, urRg ) ) ) {
      othDst = distSq ( vCp, pRoot->data2valu( pOthDat ), mDim ) ;
      add2list ( mData, &mD, pNearestData, dataSize, nearestDistSq, pOthDat, othDst ) ;
    }

    if ( mD == mData )
      filling = 0 ;
  }



#ifdef TRACE
  mSearches_t++ ;
  maxBoxes_t = MAX( maxBoxes_t, mBox_t ) ;
  mBoxes_t += mBox_t ;
  maxBoxesOvlp_t = MAX( maxBoxesOvlp_t, mBoxOvlp_t ) ;
  mBoxesOvlp_t += mBoxOvlp_t ;
  maxData_t = MAX( maxData_t, mDat_t ) ;
  mData_t += mDat_t ;
#endif

  return ( mD ) ;
}


/************************************************************************************

  nearest_data:
  Find the nearest data to a given data in the tree.

  Last update:
  ------------
  4Jun96; return nearestDist.

  Input:
  ------
  Proot:
  Pdata:

  Changes to:
  -----------
  nearestDist: Nearest distance, root taken.

  Returns:
  --------
  a pointer to the nearest data.

*/

DATA_TYPE *nearest_data ( const root_struct *Proot, const DATA_TYPE *Pdata,
			  VALU_TYPE *PnearestDist ) {
  
  /* Find the nearest data entry to Pdata. */
  const DATA_TYPE *PnearestData = NULL, *PotherData ;
  VALU_TYPE otherDist, llRg[MAX_DIM], urRg[MAX_DIM], *pVCp, vCp[MAX_DIM] ;
  int nDim, k, mData ;
  volatile int mDim ;
  tree_pos_struct *Ppos ;
  
  Ppos = Proot->ProotPos ;
  mDim = Proot->mDim ;
  *PnearestDist = 1.e25 ;

  /* First check if the root is valid. */
  if ( !Proot ) {
    printf ( " WARNING: Empty tree in nearest_data.\n" ) ;
    return ( NULL ) ; }

  /* Find any box the data goes into. Use the default iterator. */
  if ( !find_box_inc ( Ppos, Pdata, Proot->data2valu ) ) {
    printf ( " WARNING: could not find a containing box in nearest_data.\n" ) ;
    return ( NULL ) ; }


  /* Make a copy of the first invocation of data2valu. In this way data2valu
     can store one intermediate result and be invoked again for the comparison. */
  pVCp = Proot->data2valu( Pdata ) ;
  for ( nDim = 0 ; nDim < Proot->mDim ; nDim++ )
    vCp[nDim] = pVCp[nDim] ;

  
  if ( Ppos->Pframe->Pbox->mData == 0 ) {
    /* There is no data here. However there must be in one of the
       siblings or one of the siblings descendants. Otherwise the
       children would never have been created or would have been
       erased by del_data. Thus, traverse up one level, initialize the range
       with the distance to the edges of this box. */
    up_traverse ( Ppos ) ;
    
    Ppos->Pframe->iPos = -1 ;
    if ( !traverse_subtree ( Ppos ) )
      /* The tree is empty. */
      return ( NULL ) ;
  }

  
  /* Loop over all these elements and find the closest to initialise the range. */
  *PnearestDist = TOO_MUCH ;
  mData = Ppos->Pframe->Pbox->mData ;
  for ( k = 0 ; k < mData ; k++ ) {
    PotherData = Ppos->Pframe->Pbox->shr.PPdata[k] ;
    otherDist = dist ( vCp, Proot->data2valu( PotherData ), mDim ) ;
    if ( otherDist < *PnearestDist ) {
      *PnearestDist = otherDist ;
      PnearestData = PotherData ;
    }
  }
  
  /* Initialize a range for the search. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
    llRg[nDim] = vCp[nDim] - *PnearestDist ;
    urRg[nDim] = vCp[nDim] + *PnearestDist ;
  }


  /* Reset the range. */
  reset_range ( Ppos, llRg, urRg ) ;

#ifdef TRACE
  mBox_t = mBoxOvlp_t = 1 ;
  mDat_t = mData ;
#endif

  /* Range search this box. */
  while ( ( PotherData = range_search ( Ppos, llRg, urRg ) ) ) {
    otherDist = dist ( vCp, Proot->data2valu( PotherData ), mDim ) ;
    if ( otherDist < *PnearestDist ) {
      PnearestData = PotherData ;
      *PnearestDist = otherDist ;
      
      /* Reset the range for the search. */
      for ( nDim = 0 ; nDim < mDim ; nDim++ ) {
	llRg[nDim] = vCp[nDim] - *PnearestDist ;
	urRg[nDim] = vCp[nDim] + *PnearestDist ;
      }
    }
  }

#ifdef TRACE
  mSearches_t++ ;
  maxBoxes_t = MAX( maxBoxes_t, mBox_t ) ;
  mBoxes_t += mBox_t ;
  maxBoxesOvlp_t = MAX( maxBoxesOvlp_t, mBoxOvlp_t ) ;
  mBoxesOvlp_t += mBoxOvlp_t ;
  maxData_t = MAX( maxData_t, mDat_t ) ;
  mData_t += mDat_t ;
#endif

  return ( ( DATA_TYPE * ) PnearestData ) ;
}

VALU_TYPE dist ( const VALU_TYPE *PvalA, const VALU_TYPE *PvalB, const int mDim ) {
  /* Calculate the distance between a and b. */
  int nDim ;
  VALU_TYPE distance, diff ;

  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ ) {
    diff = PvalA[nDim] - PvalB[nDim] ;
    distance += diff*diff ;
  }

  return ( sqrt( distance ) ) ;
}



VALU_TYPE distSq ( const VALU_TYPE *PvalA, const VALU_TYPE *PvalB, const int mDim ) {
  /* Calculate the squared distance between a and b. */
  int nDim ;
  VALU_TYPE distance, diff ;

  for ( distance = 0., nDim = 0 ; nDim < mDim ; nDim++ ) {
    diff = PvalA[nDim] - PvalB[nDim] ;
    distance += diff*diff ;
  }

  return ( distance ) ;
}







