/*
 tree_private.c:
 Quadtree/Octree structure, private functions.

 Last update:
 ------------
 14Jan01; intro contain.
 3Jun96; intro VALU_TYPE.
 29Feb96; conceived.
*/

#include <stdio.h>
#include <stdlib.h>

#include "tree_data.h"
#include "./tree_public.h"
#include "./tree_private.h"

void del_subtree ( tree_struct *Pbox, root_struct *Proot ) {
  /* Destructor, children including their parent. */

  del_children ( Pbox, Proot ) ; 
  
  /* Without the children this now is a leaf, or
     has always been. */
  free ( Pbox->shr.PPchild ) ;
  free ( Pbox ) ;

  Proot->mNodes-- ;
}

void del_children ( tree_struct *Pbox, root_struct *Proot ) {
  /* Destructor, just the children. */
  int nBox ;

  if ( Pbox->mData < 0 ) {
    /* Scan the children. */
    for ( nBox = 0 ; nBox < Proot->mBox ; nBox++ )
      del_subtree( (Pbox->shr.PPchild)[nBox], Proot ) ;

    /* Make this box a leaf. */
    Pbox->mData = 0 ;
  }
}

tree_struct *add_child( tree_struct *Pparent, root_struct *Proot ) {
  /* Constructor. */
  tree_struct *Pchild ;

  /* Allocate space for the tree structure. */
  Pchild = ( tree_struct * ) malloc ( sizeof( tree_struct ) ) ;

  /* Allocate the variable storage for the data that is shared by
     the space for the pointers to the children. */
  Pchild->shr.PPchild = ( tree_struct ** )
                        malloc ( Proot->mBox*sizeof( void * ) ) ;
    
  if ( !Pchild || !Pchild->shr.PPchild ) {
    printf ( " FATAL: could not allocate a child in add_child.\n" ) ;
    return ( NULL ) ; }


  Pchild->mData = 0 ;
  Pchild->Pparent = Pparent ;
  Pchild->shr.PPdata = ( const DATA_TYPE ** ) Pchild->shr.PPchild ;

  Proot->mNodes++ ;
  return ( Pchild ) ;
}
  
tree_struct *find_box( tree_pos_struct *PTreePos, const DATA_TYPE *Pdata,
		       VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) {
  /* Find the containing box in one of the children. */
  int nBox, nDim, mDim, dir[MAX_DIM], below, above ;
  VALU_TYPE ctr[MAX_DIM] ;

  frame_struct *Pframe ;
  Pframe = PTreePos->Pframe ;
  mDim = PTreePos->Proot->mDim ;

  below = 1 ;
  while ( below )
    /* Traverse up until the frame contains Pdata. */
    if ( ( below = compare ( PTreePos->Pframe->ll, PTreePos->Pframe->ur,
			     data2valu( Pdata ), mDim, dir ) ) )
      if ( !up_traverse ( PTreePos ) )
	/* The root doesn't contain Pdata. */
	return ( NULL ) ;

  above = 1 ;
  while ( above ) {
    /* Traverse down until the containing leaf is found. */
    Pframe = PTreePos->Pframe ;
    
    if ( Pframe->Pbox->mData > -1 )
      /* This is a leaf, we're done. */
      return ( Pframe->Pbox ) ;

    else {
      /* Go to the containing child. */
      /* center( Pframe->ll, Pframe->ur, ctr, mDim ) ; */
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	ctr[nDim] = .5*( Pframe->ll[nDim] + Pframe->ur[nDim] ) ;
      
      /* Since find_box already made sure that Pdata is in Proot,
	 dir must be 0<dir<1. Find the box position and update
	 the bounding box. */
      compare ( Pframe->ll, ctr, data2valu( Pdata ), mDim, dir ) ;
      
      for ( nBox = 0, nDim = mDim-1 ; nDim > -1 ; nDim-- ) {
	nBox *= 2 ;
	nBox += dir[nDim] ; }

      /* Move to that box. */
      down_traverse ( PTreePos, nBox ) ;
    }
  }
     
  /* No path here. */
  return ( NULL ) ;
}

tree_struct *find_box_inc ( tree_pos_struct *PTreePos, const DATA_TYPE *Pdata,
			    VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) {
  /* Find a containing box in one of the children with the relaxed
     condition llBox <= *Pdata. */
  int nBox, nDim, mDim, dir[MAX_DIM], below, above ;
  VALU_TYPE ctr[MAX_DIM] ;

  frame_struct *Pframe ;
  Pframe = PTreePos->Pframe ;
  mDim = PTreePos->Proot->mDim ;

  below = 1 ;
  while ( below )
    /* Traverse up until the frame contains Pdata. */
    if ( ( below = compare_inc  ( PTreePos->Pframe->ll, PTreePos->Pframe->ur,
				  data2valu( Pdata ), mDim, dir ) ) )
      if ( !up_traverse ( PTreePos ) )
	/* The root doesn't contain Pdata. */
	return ( NULL ) ;

  above = 1 ;
  while ( above ) 
  { /* Traverse down until the containing leaf is found. */
    Pframe = PTreePos->Pframe ;
    
    if ( Pframe->Pbox->mData > -1 )
      /* This is a leaf, we're done. */
      return ( Pframe->Pbox ) ;

    else
    { /* Go to the containing child. */
      center( Pframe->ll, Pframe->ur, ctr, mDim ) ;
      /* Since find_box already made sure that Pdata is in Proot,
	 dir must be 0<dir<1. Find the box position and update
	 the bounding box. */
      compare_inc ( Pframe->ll, ctr, data2valu( Pdata ), mDim, dir ) ;
      
      for ( nBox = 0, nDim = mDim-1 ; nDim > -1 ; nDim-- )
      { nBox *= 2 ;
	nBox += dir[nDim] ;
      }

      /* Move to that box. */
      down_traverse ( PTreePos, nBox ) ;
    }
  }
     
  /* No path here. */
  return ( NULL ) ;
}

const DATA_TYPE *traverse_subtree ( tree_pos_struct *Ppos ) {
  /* Iterator: traverse the tree and return one element at a time
     using a lexicographic scan. Do NOT traverse upward past
     PtopPos if the current frame is exhausted. */
  frame_struct *Pframe, *PtopFrame ;
  PtopFrame = Ppos->Pframe ;

  while ( 1 ) {
    Pframe = Ppos->Pframe ;

    if ( Pframe->Pbox->mData >= 0 &&
	(Pframe->iPos)+1 < Pframe->Pbox->mData )
      /* This is a leaf and there is more data here. Avoid the
         incrementation of iPos in the if construct, since it
	 is not guaranteed that the second part of the condition
	 is not evaluated on mData<0.*/
      return ( ( DATA_TYPE * )
	       (Pframe->Pbox->shr.PPdata)[++(Pframe->iPos)] ) ;
    
    else if ( Pframe->Pbox->mData < 0 &&
	      ++(Pframe->iPos) < Ppos->Proot->mBox )
      /* Scan through the children incrementing iPos. */
      if ( Pframe->Pbox->shr.PPchild[Pframe->iPos]->mData != 0 )
	/* There are either data or children. Descend. */
	down_traverse ( Ppos, Pframe->iPos ) ;
      else
	;
    
    else if ( Ppos->Pframe != PtopFrame )
      /* The current frame has been exhausted. Traverse upward since
	 we are not past the top of the subtree. */
      up_traverse ( Ppos ) ;
    
    else
      /* All children of the topFrame have been exhausted. */
      return ( NULL ) ;
  }
}

int up_traverse ( tree_pos_struct *Ppos ) {
  /* Traverse up one level. Return 0 if we're at the root already,
     otherwise return the new depth. */

  if ( Ppos->Pframe->PupFrame )
  { /* There is a parent level. */
    Ppos->Pframe = Ppos->Pframe->PupFrame ;
    return ( 1 ) ;
  }
  else
    /* This is the root. */
    return ( 0 ) ;
}

int down_traverse ( tree_pos_struct *Ppos, int nBox ) {
  /* Traverse down one level. Return 0 if this is a leaf already,
     otherwise return the new depth. */
  VALU_TYPE ctr[MAX_DIM] ;
  frame_struct *Pframe, *PdownFrame ;
  Pframe = Ppos->Pframe ;
  
  if ( Pframe->Pbox->mData < 0 )
  { /* This is a branch. */

    if ( Ppos->Proot->mBox <= nBox )
    { /* There are not that many children. */
      return ( 0 ) ;
    }
    else
    { /* Check whether there is one more frame in the list. */
      if ( !( PdownFrame = Ppos->Pframe->PdownFrame ) )
	if ( !( PdownFrame = ( frame_struct * )
	                     malloc ( sizeof ( frame_struct ) ) ) )
	{ printf ( " FATAL: no space for a new frame_struct.\n" ) ;
	  exit( FAILURE ) ;
	}
	else
	{ PdownFrame->PdownFrame = NULL ;
	  PdownFrame->PupFrame = Pframe ;
	  Pframe->PdownFrame = PdownFrame ;
	}
      else
	PdownFrame = Pframe->PdownFrame ;

      /* Update the iterator. */
      Ppos->Pframe = PdownFrame ;

      /* Calculate the box size of the child. */
      Pframe->iPos = nBox ;
      PdownFrame->iPos = -1 ;
      PdownFrame->Pbox = Pframe->Pbox->shr.PPchild[nBox] ;
      calc_box ( Pframe->ll, Pframe->ur,
		 Ppos->Proot->mDim, nBox, 
		 PdownFrame->ll, PdownFrame->ur, ctr ) ;

      return ( 1 ) ;
    }
  }
  else
    /* This is a leaf. */
    return ( 0 ) ;
}



void center ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
	      VALU_TYPE *Pctr, int mDim ) {
  /* Calculate the center of the box with lowerleft ll and
     upperright ur. */
  int nDim ;
  
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    Pctr[nDim] = .5*( Pll[nDim] + Pur[nDim] ) ;
}  

int compare ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
	      const VALU_TYPE *Pval,
	      int mDim, int *dir ) {
  /* compare *Pval to *Pll and *Pur. For each coordinate/search
     direction, return dir=0 if *Pll < *Pval <= *Pur, return
     1 if *Pval > *Pur, return -1 if *Pval <= *Pll. */
  int nDim, mMisMatch = 0 ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    if ( Pll[nDim] >= Pval[nDim] )
    { dir[nDim] = -1 ;
      mMisMatch++ ;
    }
    else if ( Pur[nDim] < Pval[nDim] )
    { dir[nDim] = 1 ;
      mMisMatch++ ;
    }
    else
      dir[nDim] = 0 ;

  return ( mMisMatch ) ;
}

int compare_inc ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
		  const VALU_TYPE *Pval,
		  int mDim, int *dir ) {
  /* compare *Pval to *Pll and *Pur. For each coordinate/search
     direction, return dir=0 if *Pll <= *Pval <= *Pur, return
     1 if *Pval > *Pur, return -1 if *Pval < *Pll. Note the
     difference between compare and compare_inc: compare_inc
     tests inclusively. */
  int nDim, mMisMatch = 0 ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    if ( Pll[nDim] > Pval[nDim] )
    { dir[nDim] = -1 ;
      mMisMatch++ ;
    }
    else if ( Pur[nDim] < Pval[nDim] )
    { dir[nDim] = 1 ;
      mMisMatch++ ;
    }
    else
      dir[nDim] = 0 ;

  return ( mMisMatch ) ;
}

int range_is_positive ( const VALU_TYPE *Pll, const VALU_TYPE *Pur,
		        int mDim ) {
  /* Check if the range has a negative volume. */
  int nDim ;

  for ( nDim = 0 ; nDim < mDim ; nDim++ )
    if ( Pll[nDim] > Pur[nDim] )
    { printf ( " WARNING: negative range is always empty.\n" ) ;
      return ( 0 ) ;
    }
  return ( 1 ) ;
}


	
int overlap ( const VALU_TYPE *Pll1, const VALU_TYPE *Pur1,
	      const VALU_TYPE *Pll2, const VALU_TYPE *Pur2,
	      int mDim ) {
  /* Check whether the two ranges have an overlap. */
  int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
  { /* If the ur1 is smaller than the ll2, there can be
       no overlap. */
    if ( Pur1[nDim] < Pll2[nDim] )
      return ( 0 ) ;
    else if ( Pur2[nDim] < Pll1[nDim] )
      return ( 0 ) ;
  }

  /* All dimensions overlap. */
  return ( 1 ) ;
}

int contain ( const VALU_TYPE *Pll1, const VALU_TYPE *Pur1,
	      const VALU_TYPE *Pll2, const VALU_TYPE *Pur2,
	      int mDim ) {
  /* Check whether range 1 contains range 2. */
  int nDim ;

  /* Loop over all dimensions. */
  for ( nDim = 0 ; nDim < mDim ; nDim++ )
  { /* If the ur1 is smaller than the ll2, there can be
       no overlap. */
    if ( Pur1[nDim] < Pur2[nDim] )
      return ( 0 ) ;
    else if ( Pll2[nDim] < Pll1[nDim] )
      return ( 0 ) ;
  }

  /* All dimensions overlap. */
  return ( 1 ) ;
}

void calc_box ( const VALU_TYPE *PllBox, const VALU_TYPE *PurBox,
	        int mDim, int nPos,
	        VALU_TYPE *PllChild, VALU_TYPE *PurChild, VALU_TYPE *Pctr ) {
  
  int nDim ;
  
  /* Calculate the center of the box. */
  center ( PllBox, PurBox, Pctr, mDim ) ;
  
  /* Calculate the box size of the child. */
  for ( nDim = 0 ; nDim < mDim ; nDim++, nPos /= 2 )
    if ( nPos % 2 )
    { /* Odd number: right/upper/top. */
      nPos-- ;
      PllChild[nDim] = Pctr[nDim] ;
      PurChild[nDim] = PurBox[nDim] ;
    }
    else
    {
      PllChild[nDim] = PllBox[nDim] ;
      PurChild[nDim] = Pctr[nDim] ;
    }
}

#ifdef DEBUG
void printbox ( const tree_struct *Pbox,
		VALU_TYPE *(*data2valu) ( const DATA_TYPE *) ) {
  
  int kData, nDim, mDim=3 ;
  double *pValu ;
  

  if ( !Pbox->Pparent && Pbox->mData < 0 )
    printf ( " root: %d children.\n", -Pbox->mData ) ;
  
  else if ( !Pbox->Pparent && Pbox->mData == 0 )
    printf ( " root: no children.\n" ) ;
  
  else if ( !Pbox->Pparent && Pbox->mData > 0 ) {
    printf ( " root: %d values.\n", Pbox->mData ) ;
    for ( kData = 0 ; kData < Pbox->mData ; kData++ ) {
      printf ( "%d: ", kData ) ;
      pValu = data2valu( Pbox->shr.PPdata[kData] ) ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	printf ( " %g", pValu[nDim] ) ;
      printf ( "\n" ) ;
    }
  }
  
  else if ( Pbox->mData < 0 )
    printf ( " node: %d children.\n", -Pbox->mData ) ;

  else if ( Pbox->mData > 0 ) {
    printf ( " leaf: %d values.\n", Pbox->mData ) ;
    for ( kData = 0 ; kData < Pbox->mData ; kData++ ) {
      printf ( "%d: ", kData ) ;
      pValu = data2valu( Pbox->shr.PPdata[kData] ) ;
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
	printf ( " %g", pValu[nDim] ) ;
      printf ( "\n" ) ;
    }
  }
  
  else
    printf ( " leaf, no data.\n" ) ;

  return ;
}
#endif
