/*

 tt.c
 Test tree.c
*/

#include <strings.h>
#include <stdio.h>

#include "tree_data.h"
struct _valNmore
{
  int nr ;
  VALU_TYPE *Pval ;
} ;

#include "tree_public.h"

void print_data ( const DATA_TYPE *Pdata, const int mDim ) ;

int main( argc, argv, envp )

int argc ;
char *argv[], *envp[] ;
 
{
  int running=1, nDim, mDim, mData=0, nData, once=2, range_set=0 ;
  DATA_TYPE data[100], *Pdata, oneData ;
  VALU_TYPE ll[3], ur[3], llRg[3], urRg[3],
            *PmyVal, point[3], valu[300], oneValu[3], nearestDist ;
  char newLine[256]="?", inString[10], oldLine[10] = "?", *Pstring ;
  root_struct *Ptree[5] = { NULL, NULL, NULL, NULL, NULL } ;
  tree_pos_struct *Piter[5]= { NULL, NULL, NULL, NULL, NULL } ;

  oneData.nr = -1 ;
  oneData.Pval = oneValu ;

  while ( running && mData < 100 )
  {
    printf ( "[?,it,dt,s1,ad,ld,dd,ii,ri,di,ti,rs,nd]? " ) ;
    scanf ( "%s", inString ) ;

    if ( !strcmp ( inString, "?" ) )
    {
      printf ( "   it: initialize the tree, {mDim, ll, ur}\n" ) ;
      printf ( "   dt: delete the tree\n" ) ;
      printf ( "   s1: set once {0=mult. occ, 1=no dupl point., 2=no dupl. data}\n" ) ;
      printf ( "   ad: add data {x}\n" ) ;
      printf ( "   ld: list data numbers\n" ) ;
      printf ( "   dd: delete data {number}\n" ) ;
      printf ( "   ii: initialize an iterator\n" ) ;
      printf ( "   ri: reset an iterator\n" ) ;
      printf ( "   di: delete an iterator\n" ) ;
      printf ( "   ti: traverse the tree\n" ) ;
      printf ( "   sr: set range {ll,ur}\n" ) ;
      printf ( "   rr: reset range-search\n" ) ;
      printf ( "   rs: range-search\n" ) ;
      printf ( "   nd: nearest-data {x}\n" ) ;
      printf ( "   xx: exit\n" ) ;
    }
    
    else if ( !strcmp ( inString, "it" ) )
    { /* ini_tree.*/
      scanf ( "%d", &mDim ) ;
#     if defined ( DOUBLE )
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%lf", ll+nDim ) ;
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%lf", ur+nDim ) ;
#     elif defined ( INT )
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ("%d", ll+nDim ) ;
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%d", ur+nDim ) ;
#     endif

      Ptree[0] = ini_tree( mDim, ll, ur ) ;
    }

    else if ( !strcmp ( inString, "dt" ) )
    { /* del_tree.*/
      del_tree ( Ptree ) ;
    }

    else if ( !strcmp ( inString, "s1" ) )
    { /* set once.*/
      scanf ( "%d", &once ) ;
    }

    else if ( !strcmp ( inString, "ad" ) )
    { /* add_data.*/
      for ( ++mData, nDim = 0 ; nDim < mDim ; nDim++ )
#       if defined ( DOUBLE )
	  scanf ( "%lf", valu + mData*mDim + nDim ) ;
#       elif defined ( INT )
	  scanf ( "%d", valu + mData*mDim + nDim ) ;
#       endif

      data[mData].nr = mData ;
      data[mData].Pval = valu + mData*mDim ;
      
      if ( !add_data ( Ptree[0], data+mData, once, data2valu ) )
	printf  ( " Problem.\n" ) ;
      
    }

    else if ( !strcmp ( inString, "ld" ) )
    { /* List data in tt.*/
      for ( nData = 1 ; nData <= mData ; nData++ )
	print_data ( data+nData, mDim ) ;
    }

    else if ( !strcmp ( inString, "dd" ) )
    { /* del_data.*/
      scanf ( "%d", &nData ) ;
      del_data ( Ptree[0], data+nData, data2valu ) ;
    }

    else if ( !strcmp ( inString, "ii" ) )
    { /* ini_traverse.*/
      Piter[0] = ini_traverse ( Ptree[0] ) ;
    }

    else if ( !strcmp ( inString, "ri" ) )
    { /* reset_traverse.*/
      reset_traverse ( Piter[0] ) ;
    }

    else if ( !strcmp ( inString, "di" ) )
    { /* delete_traverse.*/
      del_traverse ( Piter+0 ) ;      
    }

    else if ( !strcmp ( inString, "ti" ) )
    { /* traverse_tree.*/
      if ( ( Pdata = traverse_tree ( Piter[0] ) ) )
	print_data ( Pdata, mDim ) ;
      else
	printf ( "   No more data.\n" ) ;

    }

    else if ( !strcmp ( inString, "sr" ) )
    { /* set range.*/
#     if defined ( DOUBLE )
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%lf", llRg+nDim ) ;
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%lf", urRg+nDim ) ;
#     elif defined ( INT )
        for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%d", llRg+nDim ) ;
	for ( nDim = 0 ; nDim < mDim ; nDim++ )
	  scanf ( "%d", urRg+nDim ) ;
#     endif
      range_set = 1 ;
    }

    else if ( !strcmp ( inString, "rr" ) )
    { /* reset range_search.*/
      if ( range_set )
	reset_range ( Piter[0], llRg, urRg ) ;
      else
	printf ( "    No range set.\n" ) ;
    }

    else if ( !strcmp ( inString, "rs" ) )
    { /* range_search.*/
      if ( range_set )
      { if ( ( Pdata = range_search ( Piter[0], llRg, urRg, data2valu ) ) )
	  print_data ( Pdata, mDim ) ;
        else
	  printf ( "    No data left.\n" ) ;
      }
      else
	printf ( "    No range set.\n" ) ;
    }


    else if ( !strcmp ( inString, "nd" ) )
    { /* nearest_data.*/
      for ( nDim = 0 ; nDim < mDim ; nDim++ )
#       if defined ( DOUBLE )
	  scanf ( "%lf", oneValu+nDim ) ;
#       elif defined ( INT )
	  scanf ( "%d", oneValu+nDim ) ;
#       endif

      if ( Pdata = nearest_data ( Ptree[0], &oneData, data2valu, &nearestDist ) )
	print_data ( Pdata, mDim ) ;
    }

    else if ( !strcmp ( inString, "xx" ) )
      running = 0 ;
  }
}

void print_data ( const DATA_TYPE *Pdata, const int mDim )
{
  int nDim ;
  
# if defined ( DOUBLE )
    printf ( "  %d: (%f", Pdata->nr, Pdata->Pval[0] ) ;
    for ( nDim = 1 ; nDim < mDim ; nDim++ )
      printf ( ",%f", Pdata->Pval[nDim] ) ;
# elif defined ( INT )
    printf ( "  %d: (%d", Pdata->nr, Pdata->Pval[0] ) ;
    for ( nDim = 1 ; nDim < mDim ; nDim++ )
      printf ( ",%d", Pdata->Pval[nDim] ) ;
# endif
  printf ( ")\n" ) ;
}

VALU_TYPE *data2valu( const DATA_TYPE *Pdata )
{
  return ( ( VALU_TYPE * ) Pdata->Pval ) ;
}
