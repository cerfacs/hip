/*
   test_map.c:


   Last update:
   ------------


   This file contains:
   -------------------

*/
#include <stdio.h>
#include "r1map.h"


/******************************************************************************

  _:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int main( int argc, char *argv[] )
{
  int nArg, nKey, nFile, num255 = 255 ;
  char inKey[256], searchKey[256], line[256], closeFiles = 't', ascii = 'a' ;
  FILE *thisFile ;

  r1map_reset ( "%%", "\0", &closeFiles ) ;
  
  for( nArg = 1 ; nArg < argc ; nArg++ )
    r1map_open_file ( argv[nArg], &ascii ) ;

  while ( 1 ) {
    printf ( ">" ) ;

    gets ( inKey ) ;

    if ( !strncmp ( inKey, "exit", 4 ) )
      return ( 1 ) ;
    else if ( !strncmp ( inKey, "list", 4 ) )
      r1map_list_keywords () ;
    else {
      if ( sscanf ( inKey, "%s %d %d", searchKey, &nFile, &nKey ) == 3 )
        ;
      else if ( sscanf ( inKey, "%s %d", searchKey, &nFile ) == 2 )
        nKey = 1 ;
      else if ( sscanf ( inKey, "%s", searchKey ) == 1 ) {
        nKey = 1 ;
        nFile = 0 ;
      }
      else
        nKey++ ;

      r1map_pos_keyword ( searchKey, &nFile, &nKey, &thisFile ) ;
    }

    while ( thisFile ) {
      r1map_read_char ( &thisFile, &nFile, &num255, line ) ;
      if ( !strncmp ( line, "%%", 2 ) || feof( thisFile )  )
	break ;
      printf ( "%s", line ) ;
    }

    r1map_close_file ( &nFile ) ;
  }
    
  return ( 1 ) ;
}
