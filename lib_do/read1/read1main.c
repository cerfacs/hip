
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "read1.h"

FILE InFile, OutFile ;


int main ()
{
  char oneChar ;
  char line[LINE_LEN] ;
  int i ;
  double d ;
  float f ;

  read_file ( "test.script" ) ;

  flush () ;
  printf ( " type a line:\n" ) ;
  read1line ( line ) ;
  printf ( "%s\n\n", line ) ;
  
  flush () ;
  printf ( " type a char:\n" ) ;
  read1char ( &oneChar ) ;
  printf ( "%c\n\n", oneChar ) ;

  set_prompt ( "NewPrompt:>" ) ;
  
  flush () ;
  printf ( " type an alpha:\n" ) ;
  read1alpha ( &oneChar ) ;
  printf ( "%c\n\n", oneChar ) ;
  
  flush () ;
  printf ( " type an int:\n" ) ;
  read1int ( &i ) ;
  printf ( "%d\n\n", i ) ;
  
  flush () ;
  printf ( " type a float:\n" ) ;
  read1float ( &f ) ;
  printf ( "%f\n\n", f ) ;
  
  flush () ;
  printf ( " type a double:\n" ) ;
  read1double ( &d ) ;
  printf ( "%f\n\n", d ) ;


  flush () ;
  printf ( " type a double, and int and an alphabetic:\n" ) ;
  read1double ( &d ) ;
  read1int ( &i ) ;

  if ( eo_buffer () )
    printf ( "%f %d\n\n", d, i ) ;
  else
  { read1alpha ( &oneChar ) ;
    printf ( "%f %d %c\n\n", d, i, oneChar ) ;
  }
  
}
