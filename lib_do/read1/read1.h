/*
   prototypes for read1.c:


   Last update:
   ------------
   9Sep19; intro r1_skip_keyword
   6Apr13; add missing prototypes. ftn_write_rec, r1_argfill, r1_argdel.
   1Jul10; up LINE_LEN default to 1024
   27May09; intro ftn_write_rec.
*/

# ifndef LINE_LEN
#   define LINE_LEN 1024
# endif

/* The Makefile should set this properly using your HOSTTYPE environment. */
#ifdef LITTLE_ENDIAN
#  define FREAD fread_linux
#  define FWRITE fwrite_linux
#else
#  define FREAD fread
#  define FWRITE fwrite
#endif



/* read1 reads from *InFile, writes to *OutFile. */

void r1_switch_input_stdin () ;
int r1_switch_input_file ( char *PinFileName) ;
void r1_switch_output_stdout () ;
FILE *r1_switch_output_file ( char *PoutFileName) ;

void switchInput();
void r1_put_string ( char *Input);

void echo_command_none () ;
void echo_command_script () ;
void echo_command_all () ;

int readNextLine ( ) ;
int prompt1line () ;
void next_blank () ;
void next_non_blank () ;
int eo_buffer () ;
void flush_buffer () ;
void r1_set_prompt ( char *Pprompt ) ;

int read1char ( char *Pchar ) ;
int read1alpha ( char *Pchar ) ;
int read1alnum ( char *Pchar ) ;
int read1digit ( char *Pchar ) ;
int read1int ( int *Pint ) ;
int read1float ( float *Pfloat ) ;
int read1double ( double *Pdouble ) ;
int read1string ( char *Pstring ) ;
int read1lostring ( char *Pstring ) ;
int read1line ( char *Pstring ) ;


char *r1_endstring ( char *Pstring, int stringLen ) ;
char *r1_beginstring ( char*Pstring, int stringLen ) ;
char *r1_stripquote ( char*Pstring, int stringLen ) ;
void r1_stripsep ( char *Pstring, int stringLen ) ;
int r1_skip_keyword ( char *pString, int stringLen,
                          const char *keyword, int matchLen ) ;
void r1_str_tolower ( char *Pstring ) ;

int r1_isalpha ( char *Pstring, int stringLen ) ;
int r1_isupper ( char *Pstring, int stringLen ) ;

FILE *r1_fopen ( char *Pstring, int stringLen, char *access_code ) ;

int  r1_set_int_digit ( int a, int d, int c ) ;
int num_match ( const int nr, const char *pExpr ) ;

int fwrite_linux ( const void* pTo, size_t size, int mItems, FILE *binFile ) ;

int fread_linux ( void* pTo, size_t size, int mItems, FILE *binFile ) ;
int fwrite_linux ( const void* pTo, size_t size, int mItems, FILE *binFile ) ;
int ftn_write_rec ( const void* pTo, size_t size, int mItems, FILE *binFile ) ;

int fwrite_string ( FILE *binFile, const char *pString, const int len ) ;

int r1_argfill ( const char line[], char ***pppArgv ) ;
int r1_argdel ( int argc, char **ppArgv ) ;
