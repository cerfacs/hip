/*
  public prototypes for r1map.c

*/

# ifndef KEYWORD_LEN
#   define KEYWORD_LEN 81
# endif

# ifndef LINE_LEN
#   define LINE_LEN 257
# endif

# ifndef FREAD
#ifdef LITTLE_ENDIAN
#  define FREAD fread_linux
#else
#  define FREAD fread
#endif
# endif /* ifndef FREAD */

# if defined (U_S)
/*  Calling c from fortran needs trailing underscores on these architectures. */
#   define R1MAP_RESET           r1map_reset_           
#   define R1MAP_SET_KEY         r1map_set_key_
#   define R1MAP_OPEN_FILE       r1map_open_file_
#   define R1MAP_CLOSE_FILE      r1map_close_file_
#   define R1MAP_CLOSE_ALLFILES  r1map_close_allfiles_
#   define R1MAP_LIST_KEYWORDS   r1map_list_keywords_
#   define R1MAP_POS_KEYWORD     r1map_pos_keyword_     
#   define R1MAP_NEXT_LINE       r1map_next_line_
#   define R1MAP_READ_CHAR       r1map_read_char_
#   define R1MAP_READ_INT        r1map_read_int_
#   define R1MAP_READ_FLOAT      r1map_read_float_
#   define R1MAP_READ_DOUBLE     r1map_read_double_
# else
#   define R1MAP_RESET           r1map_reset           
#   define R1MAP_SET_KEY         r1map_set_key
#   define R1MAP_OPEN_FILE       r1map_open_file
#   define R1MAP_CLOSE_FILE      r1map_close_file
#   define R1MAP_CLOSE_ALLFILES  r1map_close_allfiles
#   define R1MAP_LIST_KEYWORDS   r1map_list_keywords
#   define R1MAP_POS_KEYWORD     r1map_pos_keyword     
#   define R1MAP_NEXT_LINE       r1map_next_line
#   define R1MAP_READ_CHAR       r1map_read_char
#   define R1MAP_READ_INT        r1map_read_int
#   define R1MAP_READ_FLOAT      r1map_read_float
#   define R1MAP_READ_DOUBLE     r1map_read_double
# endif

# ifdef FORTRAN
/* Fortran appends the length of the character array by reference. */
void R1MAP_RESET ( char aKey[], const int *pminAKey, 
                   char bKey[], const int *pLenBKey, char *pCloseFiles,
                   int lenAKey, int lenBKey ) ;
void R1MAP_SET_KEY ( char aKey[], const int *pminAKey, 
                     char bKey[], const int *pLenBKey, char *pCloseFiles,
                     int lenAKey, int lenBKey ) ;

int R1MAP_OPEN_FILE ( char fileName[], const char *pType,
                      int lenName ) ;
void R1MAP_CLOSE_FILE ( const int *pnFile ) ;
void R1MAP_CLOSE_ALLFILES ( ) ;

int R1MAP_POS_KEYWORD ( char keyword[], int *pnFile, const int *pnKey,
                        FILE **ppKeyFile, int lenKey ) ;
# else
void R1MAP_RESET ( char magicAKey[], const int *pminAKey, 
                   char magicBKey[], const int *pLenBKey, const int multKeyLine,
                   char *pCloseFiles,
                   int(*isKey)(const char *), const char*(*endKey)(const char *) ) ;
void R1MAP_SET_KEY ( char magicAKey[], const int *pminAKey, 
                     char magicBKey[], const int *pLenBKey, const int multKeyLine,
                     char *pCloseFiles,
                     int(*isKey)(const char *), const char*(*endKey)(const char *) ) ;

int R1MAP_OPEN_FILE ( char fileName[], const char *pType ) ;
void R1MAP_CLOSE_FILE ( const int *pnFile ) ;
void R1MAP_CLOSE_ALLFILES ( ) ;

int R1MAP_POS_KEYWORD ( char keyword[], int *pnFile, const int *pnKey,
                        FILE **ppKeyFile ) ;
# endif

void R1MAP_LIST_KEYWORDS ( ) ;

int R1MAP_NEXT_LINE ( FILE **ppKeyFile, const int *pnFile ) ;

int R1MAP_READ_CHAR ( FILE **ppKeyFile, const int *pnFile,
                      const int *pmData, char *pData ) ;
int R1MAP_READ_INT ( FILE **ppKeyFile, const int *pnFile,
                     const int *pmData, int *pData ) ;
int R1MAP_READ_FLOAT ( FILE **ppKeyFile, const int *pnFile,
                       const int *pmData, float *pData ) ;
int R1MAP_READ_DOUBLE ( FILE **ppKeyFile, const int *pnFile,
                        const int *pmData, double *pData ) ;

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
