/*
   r1_map.c:
   Make a map of a set of files via a list of locations of keywords.

   Last update:
   ------------
   16Dec11; intro hip_msg and use of hip_err.
   13Oct07; intro isKey and endKey for Fluent binary files. Breaks FORTRAN version.
   27Jun07; intro minAKey to work for cedre keywords with blanks
   15May98; fix up to be used from FORTRAN as well.
   18Apr98; conceived.

   This file contains:
   -------------------
   r1map_reset
   r1map_set_key
   r1map_open_file
   r1map_close_file
   r1map_close_allfiles
   r1map_scan_ascii_file
   r1map_scan_bin_file
   r1map_add_key
   r1map_list_keywords
   
   r1map_pos_keyword
   r1map_next_line
   r1map_read_char
   r1map_read_int
   r1map_read_float
   r1map_read_double
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "r1map.h"
#include "read1.h"

extern char hip_msg[] ;

static int r1map_scan_ascii_file ( const int nFile );
static int r1map_scan_bin_file ( const int nFile );
static int r1map_add_key ( const char name[KEYWORD_LEN],
                           const int nFile, fpos_t posKey );
static void r1map_string2c ( char *pfStr, int len, char cStr[KEYWORD_LEN] );

typedef enum { success, fatal, warning, info, blank } hip_stat_e ;

/*  This is rather awkward. hip_err sits with the main hip files, its prototype
    is in proto.h. We don't really want to make r1map dependent on proto.h. 
    In the future: 
    Move hip_err into the r1 lib, or better, use a generic assert type error handler.
    For now: liver dangerously and use the not securely updated prototype copy. 
*/
void hip_err ( hip_stat_e status, int printLvl, char *str ) ;

/* A location of a keyword in a file. */
typedef struct {
  char name[KEYWORD_LEN] ;
  int nxtSameKey ;

  int nFile ;
  fpos_t keyPos ;
} keyLoc_s ;

/* A file that has been visited. */
typedef struct {
  char name[LINE_LEN] ;
  FILE *file ;
  char type ;

  int mRecords ;   /* If the file is binary, make a list of the records. */
  fpos_t *pRecPos ;
} file_s ;

/* A list of all files opened and all locations found. */
typedef struct {

  int mFiles ;    /* A list of files starting with index 1. */
  file_s *pFile ;
  
  int mKeyLocs ;
  keyLoc_s *pKeyLoc ;

  int (*isKey) ( const char * ) ;
  const char* (*endKey) ( const char * ) ;
  

  size_t magicALen ;
  size_t minALen ;
  char magicAKey[KEYWORD_LEN] ;
  int multiKeyLine ;
  
  size_t magicBLen ;
  char magicBKey[KEYWORD_LEN] ;
  int lenBKey ;

  int closeFiles ;
  
} locList_s ;


locList_s locList = { 0, NULL, 0, NULL, 0, '\0', 0, '\0', {'\0'}, 0, 0, {'\0'}, 0, 0 } ;

/******************************************************************************

  map_rest:
  Reset the map, zero all counters, deallocate fields. Must be invoked at the
  beginning, before reading the first file.
  A magic key that precedes a keyword in a file can and should be set via
  magicKey. A key must be set before the first file is read. Can be reset at
  any time to operate differently on different files.
  
  Last update:
  ------------
  8Dec07; check file pointer before closing.
  : conceived.
  
  Input:
  ------
  magicAKey[]: a magicKey that precedes a keyword in an ASCII file. No more
               than KEYWORD_LEN in length.
  magicBKey[]: Same for a binary file. Note that both strings must be \0 truncated.
  closeFiles:  on t, all files are opened for acces and closed afterwards.
  isKey: function that returns non-zero if the arg is a key
  endKey: function that returns a pointer to an endstring character
  
*/

# ifdef FORTRAN
void R1MAP_RESET ( char aKey[], const int *pminAKey, 
                   char bKey[], const int *pLenBKey,  const int multiKeyLine,
                   char *pCloseFiles,
                   int lenAKey, int lenBKey )
# else
void R1MAP_RESET ( char magicAKey[], const int *pminAKey, 
                   char magicBKey[], const int *pLenBKey, const int multiKeyLine,
                   char *pCloseFiles,
                   int(*isKey)(const char *), const char*(*endKey)(const char *) )
# endif
{
  int nFile ;

# ifdef FORTRAN
    char magicAKey[KEYWORD_LEN], magicBKey[KEYWORD_LEN] ;
    r1map_string2c ( aKey, lenAKey, magicAKey ) ;
    r1map_string2c ( bKey, lenBKey, magicBKey ) ;
# endif

  /* Close all files still open. */
  for ( nFile = 0 ; nFile < locList.mFiles ; nFile++ )
    if ( locList.pFile[nFile].file )
      fclose ( locList.pFile[nFile].file ) ;
  
  free ( locList.pFile ) ;
  free ( locList.pKeyLoc ) ;

  locList.mFiles = 0 ;
  locList.pFile = NULL ;
  locList.mKeyLocs = 0 ;
  locList.pKeyLoc = NULL ;

# ifdef FORTRAN
    R1MAP_SET_KEY ( aKey, pminAKey, bKey, pLenBKey, multiKeyLine, 
                    pCloseFiles, lenAKey, lenBKey ) ;
# else
    R1MAP_SET_KEY ( magicAKey, pminAKey, magicBKey,  pLenBKey, multiKeyLine,
                    pCloseFiles, isKey, endKey ) ;
# endif

  return ;
}


/******************************************************************************

  r1map_set_key:
  [Re]Set a magic key.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  magicAKey[]: a magicKey that precedes a keyword in an ASCII file. No more
               than KEYWORD_LEN in length.
  magicBKey[]: Same for a binary file. Note that both strings must be \0 truncated.
  closeFiles:  on t, all files are opened for acces and closed afterwards.
  isKey: function that returns non-zero if the arg is a key
  endKey: function that returns a pointer to an endstring character

*/
# ifdef FORTRAN
void R1MAP_SET_KEY ( char aKey[], const int *pminAKey, 
                     char bKey[], const int *pLenBKey,  const int multiKeyLine,
                     char *pCloseFiles,
                     int lenAKey, int lenBKey )
# else
void R1MAP_SET_KEY ( char magicAKey[], const int *pminAKey, 
                     char magicBKey[], const int *pLenBKey, const int multiKeyLine,
                     char *pCloseFiles,
                     int(*isKey)(const char *), const char*(*endKey)(const char *) )
# endif 
{
  
# ifdef FORTRAN
    char magicAKey[KEYWORD_LEN], magicBKey[KEYWORD_LEN] ;
    r1map_string2c ( aKey, lenAKey, magicAKey ) ;
    r1map_string2c ( bKey, lenBKey, magicBKey ) ;
# endif

  if ( magicAKey ) {
    /* There is a magicAkey given. */
    if ( strlen ( magicAKey ) >= ( size_t ) KEYWORD_LEN )
      printf ( " WARNING: magicA key %s too long, truncated to %d in r1map_reset.\n",
	       magicAKey, KEYWORD_LEN ) ;
    strncpy ( locList.magicAKey, magicAKey, KEYWORD_LEN ) ;
    locList.magicALen = strlen ( locList.magicAKey ) ;
  }

  locList.minALen = *pminAKey ;
  locList.multiKeyLine = multiKeyLine ;

  locList.isKey  =  isKey ;
  locList.endKey = endKey ;

  
  if ( magicBKey ) {
    /* There is a magicBkey given. */
    if ( strlen ( magicBKey ) >= ( size_t ) KEYWORD_LEN )
      printf ( " WARNING: magicA key %s too long, truncated to %d in r1map_reset.\n",
	       magicBKey, KEYWORD_LEN ) ;
    strncpy ( locList.magicBKey, magicBKey, KEYWORD_LEN ) ;
    locList.magicBLen = strlen ( locList.magicBKey ) ;
  }

  if ( *pLenBKey )
    /* There is a length for the binary key specified. */
    locList.lenBKey = *pLenBKey ;
  else if ( !locList.lenBKey )
    /* Use a default value. */
    locList.lenBKey = 80 ;

  if ( pCloseFiles[0] != '\0' ) {
    /* Set this parameter. */
    pCloseFiles[0] = tolower( pCloseFiles[0] ) ;
    locList.closeFiles = ( pCloseFiles[0] == 't' || pCloseFiles[0] == 'y' ? 1 : 0 ) ;
  }

  return ;
}

/******************************************************************************

  r1map_open_file:
  Open a file, add it to the list of files and scan it for keywords.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  fileName[]: the name of the file
  type:       b for binary, else ASCII.
  
  Returns:
  --------
  0 on failure, the file number on success.
  
*/

# ifdef FORTRAN
int R1MAP_OPEN_FILE ( char fName[], const char *pType, int lenName )
# else
int R1MAP_OPEN_FILE ( char fileName[], const char *pType )
# endif
{
  FILE *newFile ;
  int nFile ;
  file_s *pFile ;

# ifdef FORTRAN
    char fileName[KEYWORD_LEN] ;
    r1map_string2c ( fName, lenName, fileName ) ;
# endif
  
  if ( !( newFile = fopen ( fileName, "r" ) ) ) {
    sprintf ( hip_msg, "could not open %s in r1map_open_file.\n", fileName ) ;
    hip_err ( fatal, 0, hip_msg ) ; }
  else if ( !( locList.pFile = realloc ( locList.pFile,
					 (++locList.mFiles + 1)*sizeof( file_s ) ) ) ) {
    sprintf ( hip_msg, "could not realloc file list in r1map_open_file.\n" ) ;
    hip_err ( fatal, 0, hip_msg ) ; }

  nFile = locList.mFiles ;
  pFile = locList.pFile + nFile ;
  pFile->file = newFile ;
  pFile->type = ( *pType == 'b' ? 'b' : 'a' ) ;
  strncpy ( pFile->name, fileName, LINE_LEN ) ;
  
  if ( !locList.magicALen )
    printf ( " WARNING: no magicA key set, file not scanned in r1map_open_file.\n" ) ;
  else if ( *pType == 'b' )
    r1map_scan_bin_file ( nFile ) ;
  else
    r1map_scan_ascii_file ( nFile ) ;

  if ( locList.closeFiles )
    R1MAP_CLOSE_FILE ( &nFile ) ;

  return ( nFile ) ;
}


/******************************************************************************

  r1map_close_file:
  Close a file, identified by the number returned from r1map_open_file.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  nFile: the number of the file.
  
*/

void R1MAP_CLOSE_FILE ( const int *pnFile )
{
  if ( *pnFile > locList.mFiles || *pnFile < 1 )
    /* No such file. */
    return ;
  else if ( !locList.pFile[*pnFile].file )
    /* File already closed. */
    return ;

  fclose ( locList.pFile[*pnFile].file ) ;
  locList.pFile[*pnFile].file = NULL ;
  
  return ;
}


/******************************************************************************

  r1map_close_allfiles:
  Close all open files.
  
  Last update:
  ------------
  : conceived.
  
*/

void R1MAP_CLOSE_ALLFILES ( ) {
  int nFile ;

  for ( nFile = 1 ; nFile < locList.mFiles ; nFile++ )
    if ( locList.pFile[nFile].file ) {
      fclose ( locList.pFile[nFile].file ) ;
      locList.pFile[nFile].file = NULL ;
    }
      
  return ;
}


/******************************************************************************

  r1map_scan_ascii_file:
  Scan an ascii file for keywords. Private.
  
  Last update:
  ------------
  28Jun07; intro multKeyLine
  30Apr2; work around a bug in Gambit that does not have a blank after the key+ident
          but another (1char) key.
  : conceived.
  
  Input:
  ------
  nFile: the number of the file to be scanned.

  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int r1map_scan_ascii_file ( const int nFile ) {

  FILE *file = locList.pFile[nFile].file ;
  char lineBeg[KEYWORD_LEN], *pChar ;
  int magicALen = locList.magicALen, minALen = locList.minALen ;
  fpos_t keyPos ;

  while ( !feof ( file ) )
    if ( fgets ( lineBeg, magicALen+1, file ) ) {
      pChar = lineBeg + magicALen-1 ;

      if ( !strncmp ( lineBeg, locList.magicAKey, magicALen ) ) {
	/* Get the stuff after the magicA key. */
	for ( pChar = lineBeg ;  
              fgetpos ( file, &keyPos ), fgets ( pChar, 2, file ) ; pChar++ )
	  if ( *pChar == '\0' || *pChar == '\n' )
	    break ;
	  else if ( ( pChar-lineBeg >= minALen && isspace( *pChar ) )) {
	    *pChar = '\0' ;
	    break ; }
	  else if ( locList.multiKeyLine && *pChar == locList.magicAKey[0] ) {
            /* If there may be more than one key on a line (e.g. Fluent), 
               stop before the next key. */
	    *pChar = '\0' ;
	    break ; }
	  else if ( pChar - lineBeg >= KEYWORD_LEN-2 ) {
	    *pChar = '\0' ;
	    break ; }

	if ( ( !locList.isKey && pChar - lineBeg ) || 
             (  locList.isKey && locList.isKey ( lineBeg ) ) ) {
	  /* There is a keyword. Store the position after the key, 
             but before the last linefeed. */
          if ( *pChar != '\0' && *pChar != '\n' )
            fgetpos ( file, &keyPos ) ;
          r1map_add_key ( lineBeg, nFile, keyPos ) ;
        }
      }
      else {
	/* Something other than the magicA key at the beginning of the line. Does
	   it end on a newline?

           Not clear to me now, what this should do. Skip it.
	for ( pChar = lineBeg ; pChar - lineBeg < locList.magicALen ; pChar++ )
	  if ( *pChar == '\n' || *pChar == '\0' )
	    break ; */
      }
      
      /* Skip the rest of the line. */
      if ( *pChar != '\n' ) {
        fscanf ( file, "%*[^\n]" ) ;
        fscanf ( file, "%*[\n]" ) ;      
      }
      /* turns out, it is non-trivial to find the next occurence of a string
         in a file.
      else if ( !locList.endKey ) {
        fscanf ( file, "%*[^\n]" ) ;
        fscanf ( file, "%*[\n]" ) ;
        }*/
    }
	
  return ( 1 ) ;
}


/******************************************************************************

  r1map_scan_bin_file:
  Scan a binary file for keywords and records. This presumes that binary files
  are organized as FORTRAN records.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int r1map_scan_bin_file ( const int nFile ) {

  FILE *file = locList.pFile[nFile].file ;
  char lineBeg[KEYWORD_LEN] ;
  int recLen, nRec, mRec = 0, trailLen ;
  fpos_t keyPos, *pRecPos ;

  while ( 1 ) {
    mRec++ ;
    if ( !FREAD ( &recLen, sizeof( int ), 1, file ) ) {
      if ( feof ( file ) ) {
        /* End of file. */
        mRec-- ;
        break ; }
      else {
        /* A valid binary file must have at least the recordlength. Bad file. */
        printf ( " FATAL: invalid binary file, record %d in r1map_scan_bin_file.\n",
                 mRec ) ;
        return ( 0 ) ; }
    }
    if ( recLen >= locList.magicBLen + locList.lenBKey ) {
      /* There might be a magicBKey on this line. */
      FREAD ( lineBeg, sizeof( char ), locList.magicBLen, file ) ;

      if ( !strncmp ( lineBeg, locList.magicBKey, locList.magicBLen ) ) {
	/* This is a magic key. Get a keyful after the magicBkey */
        FREAD ( lineBeg, sizeof( char ), locList.lenBKey, file ) ;
        /* in-situ strcpy is not allowed. */
        char lineBeg2[KEYWORD_LEN] ;
        strcpy ( lineBeg2, lineBeg ) ;
        r1map_string2c ( lineBeg, locList.lenBKey, lineBeg2 ) ;
        fgetpos ( file, &keyPos ) ;
        r1map_add_key ( lineBeg, nFile, keyPos ) ;
      }
      
      /* Skip the rest of the record, including the trailing record length. */
      fseek ( file, recLen-locList.magicBLen-locList.lenBKey, SEEK_CUR ) ;
    }
    else
      /* Data record. Skip to the end. */
      fseek ( file, recLen, SEEK_CUR ) ;

    /* Check the trailing length of the record. */
    if ( !FREAD ( &trailLen, sizeof( int ), 1, file ) ||
         trailLen != recLen ) {
      printf ( " FATAL: incoherent lenghts %d-%d on record %d in r1map_scan_bin_file.\n",
               recLen, trailLen, mRec ) ;
      return ( 0 ) ; }
  }
  
  /* Make a map of the records. */
  if ( !( pRecPos = malloc ( mRec*sizeof ( fpos_t ) ) ) ) {
    printf ( " FATAL: could not alloc a list of %d records in r1map_scan_bin_file.\n",
             mRec ) ;
    return ( 0 ) ; }

  /* Rewind the file and scan store the start of each record. */
  fseek ( file, 0, SEEK_SET ) ;
  for ( nRec = 0 ; nRec < mRec ; nRec++ ) {
    fgetpos ( file, pRecPos+nRec ) ;
    FREAD ( &recLen, sizeof( int ), 1, file ) ;
    fseek ( file, recLen + sizeof( int ), SEEK_CUR ) ;
  }

  /* Store the map with file_s. */
  locList.pFile[nFile].mRecords = mRec ;
  locList.pFile[nFile].pRecPos = pRecPos ;
	
  return ( 1 ) ;
}


/******************************************************************************

  r1map_add_key:
  Add a keyword.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  name:  name of the keyword,
  file:  the pointer to the file that contains the keyword,
  nFile: the number of the file.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

static int r1map_add_key ( const char name[KEYWORD_LEN],
			   const int nFile, fpos_t posKey ) {

  int newKey, nKey ;
  keyLoc_s *pKey ;
  
  newKey = locList.mKeyLocs ;
  if ( !( locList.pKeyLoc = realloc ( locList.pKeyLoc,
				      (++locList.mKeyLocs)*sizeof( keyLoc_s ) ) ) ) {
    printf ( " FATAL: could not realloc keywords in r1map_add_key.\n" ) ;
    return ( 0 ) ; }

  /* Make a new keyword entry. */
  pKey = locList.pKeyLoc + newKey ;
  pKey->keyPos = posKey ;
  strncpy( pKey->name, name, KEYWORD_LEN ) ;  
  pKey->nFile = nFile ;
  pKey->nxtSameKey = -1 ;

  /* Is there already such a key? */
  for ( nKey = 0 ; nKey < newKey ; nKey++ )
    if ( !strcmp( pKey->name, locList.pKeyLoc[nKey].name ) ) {
      /* There is one. Follow the linked list to its end. */
      while ( locList.pKeyLoc[nKey].nxtSameKey != -1 )
	nKey = locList.pKeyLoc[nKey].nxtSameKey ;

      /* Append this entry to the list. */
      locList.pKeyLoc[nKey].nxtSameKey = newKey ;
      break ;
    }

  return ( 1 ) ;
}

/******************************************************************************

  _:
  .
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

void R1MAP_LIST_KEYWORDS ()
{
  int nFile, nKey ;
  const keyLoc_s *pKey ;

  printf ( " Files:\n nr type open name\n" ) ;
  for ( nFile = 1 ; nFile <= locList.mFiles ; nFile++ )
    printf ( " %2d    %c %s %s\n", nFile,
	     locList.pFile[nFile].type,
	     ( locList.pFile[nFile].file ? " yes" : "  no" ),
	     locList.pFile[nFile].name ) ;

  printf ( "\n Keywords:\n nr file      nxt name\n" ) ;
  for ( nKey = 0 ; nKey < locList.mKeyLocs ; nKey++ ) {
    pKey = locList.pKeyLoc + nKey ;
    printf ( " %2d   %2d %8d %s\n",
	      nKey, (int)(pKey->nFile), pKey->nxtSameKey, pKey->name ) ;
  }
  printf ( "\n" ) ;

  return ;
}


/******************************************************************************

  r1map_pos_keyword:
  Given a keyword and an index, find the next occurence and return the file
  positioned just after the key.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  keyword[]: the label of the key
  pnKey:     The occurence of the key sought, say 1st, for pnKey=1.
  pnFile:    The number of the file.

  Changes To:
  -----------
  pnKey:     is increased to at least one.
  ppKeyFile: The file pointer positioned after the keyword, NULL on failure
  pnFile:    The number of the file.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
# ifdef FORTRAN
int R1MAP_POS_KEYWORD ( char kWord[], int *pnFile, const int *pnKey,
                        FILE **ppKeyFile, int lenKey )
# else
int R1MAP_POS_KEYWORD ( char keyword[], int *pnFile, const int *pnKey,
                        FILE **ppKeyFile )
# endif
{
  const keyLoc_s *pKey ;
  file_s *pFile ;
  int iKey, mThisKey, nKey, keyWordLen ;

# ifdef FORTRAN
    char keyword[KEYWORD_LEN] ;
    r1map_string2c ( kWord, lenKey, keyword ) ;
# endif

  /* Bring pnKey into range. */
  nKey = MAX( 1, *pnKey ) ;
  keyWordLen = strlen( keyword ) ;
    
  /* Find the first occurence of keyword in the list. */
  for ( iKey = 0 ; iKey < locList.mKeyLocs ; iKey++ ) {
    /* printf ( " in pos_keyw: keyword: %s, key(%d): %s\n", 
       keyword, iKey, locList.pKeyLoc[iKey].name) ; */
    if ( !strncmp ( keyword, locList.pKeyLoc[iKey].name, keyWordLen ) )
      break ;
  }
  if ( iKey >= locList.mKeyLocs ) {
    /* printf ( " FATAL: could not locate keyword %s in r1map_pos_keyword.\n",
                keyword ) ;*/
    *ppKeyFile = NULL ;
    return ( 0 ) ; }

  /* Travel the linked list of same keys to find occurence *pnKey. */
  for ( pKey = locList.pKeyLoc + iKey, mThisKey = 0 ; pKey >= locList.pKeyLoc ;
        pKey = locList.pKeyLoc + pKey->nxtSameKey ) {
    if ( !*pnFile )
      /* Any occurence. */
      mThisKey++ ;
    else if ( pKey->nFile == *pnFile )
      /* Only search occurences in a specific file. */
      mThisKey++ ;
      
    if ( mThisKey == nKey )
      break ;
  }
  if (  mThisKey != nKey ) {
    /* printf ( " FATAL: could not locate occurence %d in r1map_pos_keyword.\n",
                *pnKey ) ;*/
    *ppKeyFile = NULL ;
    return ( 0 ) ; }

  *pnFile = pKey->nFile ;
  pFile = locList.pFile + pKey->nFile ;
  
  if ( !pFile->file &&
    /* File has been closed. Reopen. */
       !( pFile->file = fopen ( pFile->name, "r" ) ) ) {
    printf ( " FATAL: could not reopen %s in r1map_pos_keyword.\n", pFile->name ) ;
    return ( 0 ) ; }

  /* Position the file just after the keyword. */
  if ( fsetpos( pFile->file, &(pKey->keyPos) ) ) {
    printf ( " FATAL: could not reposition %s in r1map_pos_keyword.\n",
	     pFile->name ) ;
    return ( 0 ) ; }

  *ppKeyFile = pFile->file ;

  return ( 1 ) ;
}

/******************************************************************************

  r1map_nxtLine:
  Given a positioned file, reposition the file to the beginning
  of the next record for binary and after the next \n in ascii files.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppKeyFile: the file with position.
  pnFile:    the number of the file

  Changes To:
  -----------
  ppKeyFile: The file pointer positioned after the keyword, NULL on failure
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int R1MAP_NEXT_LINE ( FILE **ppKeyFile, const int *pnFile )
{
  int nRec ;
  file_s *pFile = locList.pFile + *pnFile ;
  fpos_t currPos ;
  
  if ( pFile->type == 'a' ) {
    /* Ascii, scan for the  next linefeed. */
    fscanf ( *ppKeyFile, "%*[^\n]" ) ;
    fscanf ( *ppKeyFile, "\n" ) ;
  }
  else {

    /* The new compiler at Cerfacs complains. Because fpos_t there is
       a hidden struct. No comparisons can be made. Is anybody using
       binary r1map anyways? */
    printf ( " CATASTROPHIC: binary r1map has a construction flaw.\n" ) ;
    exit ( EXIT_FAILURE ) ;

    
    /* Binary. Find out on which record we are, position at the following. */
    fgetpos ( *ppKeyFile, &currPos ) ;
    for ( nRec = 1 ; nRec < pFile->mRecords ; nRec++ )
      /*if ( pFile->pRecPos[nRec] == currPos )*/ {
        /* This is currPos. */
        nRec++ ;
        fsetpos ( *ppKeyFile, &pFile->pRecPos[nRec] ) ;
        break ;
    }
  }

  if ( feof( *ppKeyFile ) ) {
    printf ( " FATAL: end of file in r1map_endOfLine.\n" ) ;
    return ( 0 ) ; }
  else
    return ( 1 ) ;
}

/******************************************************************************

  r1map_read_char:
  Given a keyword and a positioned file, reposition the file to the beginning
  of the next record for binary and after the next \n in ascii files.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppKeyFile: the positioned file to read from.
  pnFile:    the number of the file
  pmData:    the number of pieces of data to be read.

  Changes To:
  -----------
  pData:     writes data to pData.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int R1MAP_READ_CHAR ( FILE **ppKeyFile, const int *pnFile,
                      const int *pmData, char *pData )
{
  int recLen ;
  char someChar, *pChar ;

  if ( locList.pFile[*pnFile].type == 'a' ) {
    /* Ascii. Scan until pmData is read or the line over. */
    for ( pChar = pData ; pChar < pData + *pmData   ; pChar++ ) {
      someChar = fgetc ( *ppKeyFile ) ;
      if ( feof ( *ppKeyFile ) ) {
        printf ( " FATAL: end of file in r1map_read_char.\n" ) ;
        return ( 0 ) ; }
      else if ( someChar == '\0' || someChar == '\n' ) {
        *pChar = '\0' ;
        break ; }
      else
        *pChar = someChar ;
    }
    
#   ifdef FORTRAN
      /* Replace the trailing \0 with a blank. */
      if ( pChar >= pData + *pmData )
        pData[*pmData-1] = ' ' ;
      else
        /* Blank the rest of the string. */
        for ( *pChar-- ; pChar < pData + *pmData   ; pChar++ )
          *pChar = ' ' ;
#   endif
  }
  
  else {
    /* Binary. For the time being, allow only one read per line. */
    FREAD ( &recLen, sizeof( int ), 1, *ppKeyFile ) ;
    if ( recLen < *pmData*sizeof( char ) ) {
      printf ( " FATAL: end of record in r1map_read_char.\n" ) ;
      return ( 0 ) ;
    }
    else {
      /* Read. */
      FREAD ( pData, sizeof( char ), *pmData, *ppKeyFile ) ;
      /* Position the file at the next record. */
      R1MAP_NEXT_LINE ( ppKeyFile, pnFile ) ;
    }
  }

  if ( locList.closeFiles )
    R1MAP_CLOSE_FILE ( pnFile ) ;
  
  return ( *pmData ) ;
}
      
/******************************************************************************

  r1map_read_int:
  Given a keyword and a positioned file, read an int and reposition the file to the
  beginning of the next record for binary and after the next \n in ascii files.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppKeyFile: the positioned file to read from.
  pnFile:    the number of the file
  pmData:    the number of pieces of data to be read.

  Changes To:
  -----------
  pData:     writes data to pData.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int R1MAP_READ_INT ( FILE **ppKeyFile, const int *pnFile,
                     const int *pmData, int *pData )
{
  int recLen, n ;
  
  if ( locList.pFile[*pnFile].type == 'a' ) {
    /* Ascii. */
    for ( n = 0 ; n < *pmData ; n++ )
      if ( !fscanf ( *ppKeyFile, "%d", pData+n ) ) {
        printf ( " FATAL: end of file in r1map_read_int.\n" ) ;
        return ( 0 ) ; }
  }
  else {
    /* Binary. For the time being, allow only one read per line. */
    FREAD ( &recLen, sizeof( int ), 1, *ppKeyFile ) ;
    if ( recLen < *pmData*sizeof( int ) ) {
      printf ( " FATAL: end of record in r1map_read_int.\n" ) ;
      return ( 0 ) ;
    }
    else {
      /* Read. */
      FREAD ( pData, sizeof( int ), *pmData, *ppKeyFile ) ;
      /* Position the file at the next record. */
      R1MAP_NEXT_LINE ( ppKeyFile, pnFile ) ;
    }
  }

  if ( locList.closeFiles )
    R1MAP_CLOSE_FILE ( pnFile ) ;

  return ( *pmData ) ;
}
      
/******************************************************************************

  r1map_read_float:
  Given a keyword and a positioned file, read a float and reposition the file
  to the beginning of the next record for binary and after the next \n in ascii files.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppKeyFile: the positioned file to read from.
  pnFile:    the number of the file
  pmData:    the number of pieces of data to be read.

  Changes To:
  -----------
  pData:     writes data to pData.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int R1MAP_READ_FLOAT ( FILE **ppKeyFile, const int *pnFile,
                      const int *pmData, float *pData )
{
  int recLen, n ;

  if ( locList.pFile[*pnFile].type == 'a' ) {
    /* Ascii. */
    for ( n = 0 ; n < *pmData ; n++ )
      if ( !fscanf ( *ppKeyFile, "%g", pData+n ) ) {
        printf ( " FATAL: end of file in r1map_read_int.\n" ) ;
    return ( 0 ) ; }
  }
  else {
    /* Binary. For the time being, allow only one read per line. */
    FREAD ( &recLen, sizeof( int ), 1, *ppKeyFile ) ;
    if ( recLen < *pmData*sizeof( float ) ) {
      printf ( " FATAL: end of record in r1map_read_int.\n" ) ;
      return ( 0 ) ;
    }
    else {
      /* Read. */
      FREAD ( pData, sizeof( float ), *pmData, *ppKeyFile ) ;
      /* Position the file at the next record. */
      R1MAP_NEXT_LINE ( ppKeyFile, pnFile ) ;
    }
  }

  if ( locList.closeFiles )
    R1MAP_CLOSE_FILE ( pnFile ) ;
  
  return ( *pmData ) ;
}
      
/******************************************************************************

  r1map_read_double:
  Given a keyword and a positioned file, read a double and reposition the file
  to the beginning of the next record for binary and after the next \n in ascii files.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  ppKeyFile: the positioned file to read from.
  pnFile:    the number of the file
  pmData:    the number of pieces of data to be read.

  Changes To:
  -----------
  pData:     writes data to pData.
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/
int R1MAP_READ_DOUBLE ( FILE **ppKeyFile, const int *pnFile,
                        const int *pmData, double *pData )
{
  int recLen, n ;

  if ( locList.pFile[*pnFile].type == 'a' ) {
    /* Ascii. */
    for ( n = 0 ; n < *pmData ; n++ )
      if ( !fscanf ( *ppKeyFile, "%lf", pData+n ) ) {
        printf ( " FATAL: end of file in r1map_read_int.\n" ) ;
    return ( 0 ) ; }
  }
  else {
    /* Binary. For the time being, allow only one read per line. */
    FREAD ( &recLen, sizeof( int ), 1, *ppKeyFile ) ;
    if ( recLen < *pmData*sizeof( double ) ) {
      printf ( " FATAL: end of record in r1map_read_int.\n" ) ;
      return ( 0 ) ;
    }
    else {
      /* Read. */
      FREAD ( pData, sizeof( double ), *pmData, *ppKeyFile ) ;
      /* Position the file at the next record. */
      R1MAP_NEXT_LINE ( ppKeyFile, pnFile ) ;
    }
  }

  if ( locList.closeFiles )
    R1MAP_CLOSE_FILE ( pnFile ) ;
  
  return ( *pmData ) ;
}

/* Take a fortran string that is padded with blanks and turn it into a c-style
   string that is \0-terminated. */
static void r1map_string2c ( char *pfStr, int len, char cStr[KEYWORD_LEN] )
{
  char *pChar ;
  if ( len > KEYWORD_LEN-1 )
    printf ( " FATAL: maximum string length %d exceeded. recompile r1map.c with\n"
             "        increased KEYWORD_LEN. Trying truncation.\n", KEYWORD_LEN ) ;

  /* We can have at most len characters. */
  strncpy ( cStr, pfStr, len + 1 ) ;
  cStr[len] = '\0' ;

  /* Scan forward until either the end or the first non-blank
     character is encountered. */
  pChar = cStr ;
  while ( isspace( *pChar ) && pChar < cStr+len )
    pChar++ ;
  /* Copy all characters to the beginning up to the first blank. */
  while ( *pChar != '\0' && !isspace( *pChar ) && pChar < cStr+len )
    *cStr++ = *pChar++ ;
  *cStr = '\0' ;
  
  return ;
}

