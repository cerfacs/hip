/*
   read1.c:
   Read a line of input and grab one item.

   Last update:
   ------------
   9Sep19; add reading from string.
   11Dec07; allow single quotes \' to delimit strings.
   10Apr2; remove ',' from the whitespace list in read1string,
           intro num_match.
   13Sep2; modify error treatment of non-existing file in read_file.
   20Dec01; intro r1_isalpha, r1_isupper.
   9May96: conceived.

   Contains:
   ---------
   void read_stdin ()
   int read_file ( char *PinFileName)
   void write_stdout ()
   FILE *write_file ( char *PoutFileName)
   
   void echo_command_none ()
   void echo_command_script ()
   void echo_command_all ()
   
   int readNextLine ( )
   int prompt1line ()
   void next_blank ()
   void next_non_blank ()
   int eo_buffer ()
   void flush_buffer ()
   void set_prompt ( char *Pprompt )
   
   int read1char ( char *Pchar )
   int read1alpha ( char *Pchar )
   int read1alnum ( char *Pchar )
   int read1digit ( char *Pchar )
   int read1int ( int *Pint )
   int read1float ( float *Pfloat )
   int read1double ( double *Pdouble )
   int read1string ( char *Pstring )
   int read1lostring ( char *Pstring )
   int read1line ( char *Pstring )
   
   char *r1_endstring ( char *Pstring, int stringLen )
   char *r1_beginstring ( char*Pstring, int stringLen )
   char *r1_stripquote ( char*Pstring, int stringLen )

   FILE *r1_fopen ( char*Pstring, int stringLen, char *access_mode )

   fread_linux
   fwrite_linux
   fwrite_string
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#include "read1.h"

typedef enum { r1_prompt, r1_file, r1_string } r1_in_type_e ;
static r1_in_type_e r1_inType = r1_prompt ;

/* No typedef necessary. */
static struct {
  char inLine[LINE_LEN] ;
  char *Pchar ;
  size_t lenLine ;
} read1_line = { "\0", read1_line.inLine, 0 } ;

static int writeToStdout = 1 ;
static int echoCommand = 1 ;
static int newPrompt = 1;
static char prompt[LINE_LEN] = {">"}, inputBuffer[LINE_LEN];
static FILE *Read1_InFile, *Read1_OutFile ;


void r1_switch_input_stdin () {
  /* Switch to reading from stdin. */
  fclose ( Read1_InFile ) ;
  Read1_InFile = NULL ;
  r1_inType = r1_prompt ;
}

int r1_switch_input_file ( char *PinFileName) {
  /* Switch to reading from file. */
  if ( !( Read1_InFile = fopen ( PinFileName, "r" ) ) ) {
    printf ( " WARNING: file named:%s not found.\n", PinFileName ) ;
    /* Revert back to reading from prompt. */
    r1_inType = r1_prompt ;
    return ( 1 ) ;
  }
  else {
    r1_inType = r1_file ;
    return ( 0 ) ;
  }
}

void r1_switch_input_string () {
  if (  Read1_InFile ) fclose ( Read1_InFile ) ;
  Read1_InFile = NULL ;
  r1_inType = r1_string ;
  //readFromStdin = 0 ;
  newPrompt = 0;
  //readFromInput = 1;
  echoCommand = 0;

}

void r1_switch_output_stdout () {
  /* Switch to writing to stdout. */
  fclose ( Read1_OutFile ) ;
  Read1_OutFile = NULL ;
  writeToStdout = 1 ;
}

FILE *r1_switch_output_file ( char *PoutFileName ) {
  /* Switch to writing to file. */
  if ( !( Read1_OutFile = fopen ( PoutFileName, "w" ) ) ) {
    printf ( " WARNING: file named:%s could not be opened.\n", PoutFileName ) ;
    r1_switch_output_stdout () ;
    return Read1_OutFile ;
  }
  else {
    writeToStdout = 0 ;
    return ( NULL ) ;
  }
}

void echo_command_none () {
  /* No echoing of commands of any source. */
  echoCommand = 0 ;
}
void echo_command_script () {
  /* Echo all commands from script files. */
  echoCommand = 1 ;
}
void echo_command_all () {
  /* Echo all commands of any source. */
  echoCommand = 2 ;
}


// should be obsolete now, replaced by r1_switch_input_string
void switchInput() {
  /* switch to input from user input. */
  fclose ( Read1_InFile ) ;
  //readFromStdin = 0 ;
  newPrompt = 0;
  //readFromInput = 1;
  echoCommand = 0;
}

void r1_put_string ( char *string ) {
  if ( r1_inType != r1_string ) r1_switch_input_string () ;
  strcpy( inputBuffer, string );
}


int readNextLine () {
  
  char inputLine[LINE_LEN], *Pchar = inputLine ;
  char *Pline = read1_line.inLine ;

  switch ( r1_inType ) {
  case ( r1_prompt ) :
    //if ( readFromStdin ) {
    if ( !fgets ( inputLine, LINE_LEN-1, stdin ) )
      /* some failure. */
      return ( 0 ) ;
    //}
    break ;
  case ( r1_string ) :
    // accept a keyword written to inputBuffer
    if ( inputBuffer[0] == '\0' ) {
      // spent input buffer, eof.
      return ( 0 ) ;
    }
    else {
      // copy that line to read1. 
      strcpy(inputLine, inputBuffer ) ;
      inputBuffer[0] = '\0' ;  // buffer is now emptied.
    }
    break ;
  default:
    //else {    // file
    if ( !fgets ( inputLine, LINE_LEN-1, Read1_InFile ) ) {
      /* Some failure, most likely EOF. */
      fclose ( Read1_InFile ) ;
      r1_inType = r1_prompt ; //readFromStdin = 1 ;

      /* Try again from stdin. */
      if ( newPrompt )
        { if ( writeToStdout )
            fprintf ( stdout, "%s ", prompt ) ;
          else 
            fprintf ( Read1_OutFile, "%s ", prompt ) ;
          newPrompt = 0 ;
        }
      else
        { if ( writeToStdout )
            fprintf ( stdout, "? " ) ;
          else 
            fprintf ( Read1_OutFile, "? " ) ;
        }
      if ( !fgets ( inputLine, LINE_LEN-1, stdin ) )
	/* some failure. */
	return ( 0 ) ;
    }
  }
  /* Copy into line up to the newline. */
  while ( *Pchar != '\n' && *Pchar != '\0' )
    *Pline++ = *Pchar++ ;
  
  /* Append a trailing zero. */
  *Pline = '\0' ;
  /* Reset the pointer to the beginning of the line. */
  read1_line.Pchar = read1_line.inLine ;

  /* Echo commands if desired. */
  if ( echoCommand == 2 || ( echoCommand && r1_inType != r1_prompt )) { //!readFromStdin ) ) {
    if ( writeToStdout )
      fprintf ( stdout, " <<%s %s\n", prompt, read1_line.Pchar ) ;
    else
      fprintf ( Read1_OutFile, " <<%s %s\n", prompt, read1_line.Pchar ) ;
  }
  return ( 1 ) ;
}

int prompt1line () {
  /* Write a prompt only when reading from stdin. */
  if ( newPrompt && r1_inType == r1_prompt ) { //readFromStdin )
    if ( writeToStdout )
      fprintf ( stdout, "%s ", prompt ) ;
    else 
      fprintf ( Read1_OutFile, "%s ", prompt ) ;
    newPrompt = 0 ;
  }
  else if ( r1_inType == r1_prompt ) { //readFromStdin ) {
    if ( writeToStdout )
      fprintf ( stdout, "? " ) ;
    else 
      fprintf ( Read1_OutFile, "? " ) ;
  }
  
  return ( readNextLine () ) ;
}

void next_blank () {
 /* Set line.Pchar to the next blank. */
  while ( *read1_line.Pchar != ' ' &&
	  *read1_line.Pchar != '\0' && *read1_line.Pchar != '\n' )
    ++read1_line.Pchar ;
}

void next_non_blank () {
 /* Set line.Pchar to the next non-blank. */
  while ( *read1_line.Pchar == ' ' &&
	  *read1_line.Pchar != '\0' && *read1_line.Pchar != '\n' )
    ++read1_line.Pchar ;
}

int eo_buffer () {
 /* Check whether the next character is terminating. Do not
     advance the pointer past a non-blank.*/
  if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
    /* EOB. */
    return ( 1 ) ;
  else if ( *read1_line.Pchar == ' ' )
  { /* Blank. */
    next_non_blank () ;
    return ( eo_buffer () ) ;
  }
  else
    return ( 0 ) ;
}

void flush_buffer () {
  /* Set line.Pchar to the end of this command. */
  while ( *read1_line.Pchar != ';' &&
	  *read1_line.Pchar != '\0' && *read1_line.Pchar != '\n' )
    ++read1_line.Pchar ;
  if ( *read1_line.Pchar == ';' )
    ++read1_line.Pchar ;
  else
    /* The buffer is empty, so give a new prompt. */
    newPrompt = 1 ;
}

void r1_set_prompt ( char *Pprompt ) {

  char *Pchar = prompt ;
  
  while ( *Pprompt != '\0' && Pchar - prompt < LINE_LEN-1 )
    *Pchar++ = *Pprompt++ ;
  *Pchar = '\0' ;
}
  

int read1char ( char *Pchar ) {
  
  /* Any character is fine, except newline and \0. */
  while ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
    prompt1line () ;

  /* This one's fine. */
  *Pchar = *read1_line.Pchar ;
  ++read1_line.Pchar ;
  return ( 1 ) ;
}

int read1alpha ( char *Pchar ) {
  
  /* Scan for alpha. */
  while ( !isalpha( *read1_line.Pchar ) )
    if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
      prompt1line () ;
    else
      ++read1_line.Pchar ;
  
  /* This one's fine. */
  *Pchar = *read1_line.Pchar ;
  ++read1_line.Pchar ;
  return ( 1 ) ;
}

int read1alnum ( char *Pchar ) {

  /* Scan for 1 alphanumeric. */
  while ( !isalnum( *read1_line.Pchar ) )
    if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
      prompt1line () ;
    else
      ++read1_line.Pchar ;
  
  /* This one's fine. */
  *Pchar = *read1_line.Pchar ;
  ++read1_line.Pchar ;
  return ( 1 ) ;
}

int read1digit ( char *Pchar ) {

  /* Scan for a digit. */
  while ( !isdigit( *read1_line.Pchar ) )
    if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
      prompt1line () ;
    else
      ++read1_line.Pchar ;
  
  /* This one's fine. */
  *Pchar = *read1_line.Pchar ;
  ++read1_line.Pchar ;
  return ( 1 ) ;
}

int read1int ( int *Pint ) {

  int someInt ;
  
  /* Scan for an int. */
  while ( sscanf ( read1_line.Pchar, "%d", &someInt ) != 1 )
  { next_non_blank () ;
    next_blank () ;

    if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
      prompt1line () ;
  }
  
  /* This one's fine. */
  *Pint = someInt ;
  next_non_blank () ;
  next_blank () ;
  
  return ( 1 ) ;

}


int read1float ( float *Pfloat ) {

  float someFloat ;
  
  /* Scan for a float. */
  while ( sscanf ( read1_line.Pchar, "%f", &someFloat ) != 1 )
  { next_non_blank () ;
    next_blank () ;

    if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
      prompt1line () ;
  }
  
  /* This one's fine. */
  *Pfloat = someFloat ;
  next_non_blank () ;
  next_blank () ;
  
  return ( 1 ) ;

}


int read1double ( double *Pdouble ) {

  double someDouble ;
  
  /* Scan for a double. */
  while ( sscanf ( read1_line.Pchar, "%lf", &someDouble ) != 1 )
  { next_non_blank () ;
    next_blank () ;

    if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
      prompt1line () ;
  }
  
  /* This one's fine. */
  *Pdouble = someDouble ;
  next_non_blank () ;
  next_blank () ;
  
  return ( 1 ) ;

}
  

int read1string ( char *Pstring ) {
  /* Scan for a string. */
  if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
    if ( !prompt1line () )
      return ( 0 ) ;

  /* Skip all leading whitespace, include the comma. */
  while ( *read1_line.Pchar == ' ' ||
          *read1_line.Pchar == ',' ||
	  *read1_line.Pchar == '\t' )
    if ( *read1_line.Pchar == '\0' ||
         *read1_line.Pchar == '\n' ) {
      if ( !prompt1line () ) {
        return ( 0 ) ;
      }
    }
    else
      ++read1_line.Pchar ;

  /* We hit valid non-whitespace. */
  if ( *read1_line.Pchar == '\"' || *read1_line.Pchar == '\'' ) {
    /* The first character is an unescaped double-quote. Scan up to the
       next matching double-quote. */
    ++read1_line.Pchar ;
    while ( *read1_line.Pchar != '\"' &&
	    *read1_line.Pchar != '\'' &&
	    *read1_line.Pchar != '\0' &&
            *read1_line.Pchar != '\n' )
      *Pstring++ = *read1_line.Pchar++ ;
    read1_line.Pchar++ ;
  }
  
  else if ( *read1_line.Pchar == '\'' ) {
    /* The first character is an unescaped quote. Scan up to the
       next matching quote. */
    ++read1_line.Pchar ;
    while ( *read1_line.Pchar != '\'' &&
	    *read1_line.Pchar != '\0' &&
            *read1_line.Pchar != '\n' )
      *Pstring++ = *read1_line.Pchar++ ;
    read1_line.Pchar++ ;
  }
  else
    /* Scan to the next whitespace character. Remove the ',' from
     the list to allow comma- separated lists of numerical ranges in
     num_match.*/
    while ( *read1_line.Pchar != ' ' && /* *read1_line.Pchar != ',' && */
	    *read1_line.Pchar != '\t'&&
	    *read1_line.Pchar != '\0' &&
            *read1_line.Pchar != '\n' )
      *Pstring++ = *read1_line.Pchar++ ;
  
  *Pstring = '\0' ;
  
  return ( 1 ) ;

}

int read1lostring ( char *Pstring ) { 
  int returnVal ;
  // strcpy(read1_line.inLine, Pstring);
  /* Read a mixed case string. */
  returnVal = read1string ( Pstring ) ;


  r1_str_tolower ( Pstring ) ;
  /* Null string leads to infinite loop example Ctrl-D 
  if ( *Pstring == '\0' ) {
      printf("\n\nEnd of session\n\n");
      exit( EXIT_SUCCESS ) ;
  } */ 
  return ( returnVal ) ;
}

int read1line ( char *Pstring ) {
  if ( *read1_line.Pchar == '\0' || *read1_line.Pchar == '\n' )
    prompt1line () ;

  while ( *read1_line.Pchar != '\0' )
    *Pstring++ = *read1_line.Pchar++ ;
  *Pstring = '\0' ;
  
  return ( 1 ) ;

}
  
char *r1_endstring ( char *Pstring, int stringLen ) {
  char *Pend ;

  /* Scan forward until either the end of the string or
     a \0 is encountered. */
  for ( Pend = Pstring ;
        *Pend != '\0' && *Pend != '\n' && Pend < Pstring+stringLen ; Pend++ )
    ;
  
  /* Scan the string backwards and place a \0
     after the last significant character. */
  for ( Pend-- ; *Pend == ' ' ; Pend-- )
    ;
  *( Pend+1 ) = '\0' ;

  /* ptr to the terminating null/lf char. */
  return ( Pend+1 ) ;
}

char *r1_beginstring ( char *Pstring, int stringLen ) {

  char *Pbegin = Pstring ;

  /* Scan forward until either the end or the first non-blank
     character is encountered. */
  while ( isspace( *Pbegin ) && Pbegin-Pstring < stringLen )
    Pbegin++ ;

  /* Copy all characters to the beginning. */
  while ( *Pbegin != '\0' && *Pbegin !='\n' && Pbegin-Pstring < stringLen )
    *Pstring++ = *Pbegin++ ;
  *Pstring = '\0' ;

  return ( Pstring ) ;
}

char *r1_stripquote ( char *Pstring, int stringLen ) {
 /* Strip leading and trailing quotation, if any. */
  char *Pnew ;
  
  r1_endstring ( Pstring, stringLen ) ;
  r1_beginstring ( Pstring, stringLen ) ;

  if ( *Pstring == '"' || *Pstring == '`' || *Pstring == '\'' )
  { /* Copy all characters one position down, except for the terminating \0. */
    for ( Pnew = Pstring ; *(Pnew+1) != '\0' ; Pnew++ )
      *Pnew = *(Pnew+1) ;
    --Pnew ;
  }
  else
    /* Find the end of the string. */
    for ( Pnew = Pstring ; *(Pnew+1) != '\0' ; Pnew++ )
      ;

  if ( *Pnew == '"' || *Pnew == '`' || *Pnew == '\'' )
    /* A quote before the termination. Replace it with \0. */
    *Pnew = '\0' ;
  else
    *(++Pnew) = '\0' ;
  
  return ( Pstring ) ;
}

void r1_stripsep ( char *Pstring, int stringLen ) {
 /* Strip the leading text up to and including a separator as any
     non-letter and non-number character. */
  char *Pnew = Pstring ;

  /* Ptr to the closing \n or \0 */
  char *pMax = r1_endstring ( Pstring, stringLen ) ;

  /* Find the first separator. */
  while ( isalnum( *Pnew ) )
    Pnew++ ;

  /* Copy all remaining text, including \0 terminator down. */
  for ( ; Pnew <= pMax ; )
    (*Pstring++) = (*Pnew++) ;
}

/******************************************************************************

Copyright Jens-Dominik Mueller and CERFACS, 
see the CECILL copyright notice at the top of the file.
Author: Jens-Dominik Mueller
contributor(s) : Gabriel Staffelbach

"Jens-Dominik Mueller" <j.mueller@qmul.ac.uk>
"Gabriel Staffelbach" <gabriel.staffelbach@cerfacs.fr>

  r1_skip_keyword
*/
/*! given a line with kewyords, skip the leading keyword if it matches.
 *
 */

/*
  
  Last update:
  ------------
  9Sep19: conceived.
  

  Input:
  ------
  stringLen: length of that string
  keyword: keyword to match
  matchLen: number of characters to match

  Changes To:
  -----------
  pString: string with possible leading keyword removed.
    
  Returns:
  --------
  0 on match, non-zero otherwise.
  
*/

int r1_skip_keyword ( char *pString, int stringLen,
                      const char *keyword, int matchLen ) {
  /* Skip a keyword matching to matchLen characters. . */
  char *pNew = pString ;
  int n ;
  if ( ( n = strncmp ( pString, keyword, matchLen ) ) )
    /* No match. */
    return ( n ) ;

  else {
    /* Match, skip the keyword up to the next separator. */
    r1_stripsep ( pString, stringLen ) ;
  
    return ( 0 ) ;
  }
}


/* Convert a string to lowercase. */
void r1_str_tolower ( char *Pstring ) { 

  /* Convert the string to lowercase. */
  for ( ; *Pstring ; Pstring++ )
    *Pstring = tolower( *Pstring ) ;
  return ;
}


int r1_isalpha ( char *Pstring, int stringLen ) {
  /* Is the string alpha? */
  char *pC ;

  for ( pC = Pstring ; pC < Pstring + stringLen  ; pC++ )
    if ( *pC == '\0' )
      return ( 1 ) ;
    else if ( !isalpha ( *pC ) && *pC != '_' )
      return ( 0 ) ;

  return ( 1 ) ;
}
  

int r1_isupper ( char *Pstring, int stringLen ) {
  /* Is the string alpha? */
  char *pC ;

  for ( pC = Pstring ; pC < Pstring + stringLen ; pC++ )
    if ( !isupper ( *pC ) )
      return ( 0 ) ;

  return ( 1 ) ;
}
  


FILE *r1_fopen ( char *Pstring, int stringLen, char *access_code ) {
  
  r1_stripquote ( Pstring, stringLen ) ;

  return ( fopen ( Pstring, access_code ) ) ;
}


/******************************************************************************
  r1_set_int_digit:   */

/*! set the dth digit of int a to value c

 */

/*
  
  Last update:
  ------------
  : 27Aug11, conceived.
  

  Input:
  ------
  a: int to change
  d: position of the digit
  c: value to set to 0<=c<=9

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int  r1_set_int_digit ( int a, int iD, int c ) {

  int d = 1, i ; 
  for ( i = 0 ; i < iD ; i++ ) d*= 10 ;

  if ( c < 0 || c > 9 ) {
    printf ( "FATAL: argument c needs to be  c < 0 || c > 9 in r1_set_int_digit\n" ) ;
    exit (EXIT_FAILURE) ;
  }

  int b ;
  b = a - ( (a/d)%10 )*d + d*c ;

  return ( b ) ;
}

/******************************************************************************

  num_match:
  Match a numeric expression within a number.
  
  Last update:
  ------------
  26Aug11; allow also *,? as wildcards, but only one wildcard of *?- per subexpr.
  : conceived.
  
  Input:
  ------
  nr:    number to match.
  pExpr: expression of the type a-b,c-d,e,f-g ....

  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int num_match ( const int nr, const char *pExpr ) {

  const char *pX ;
  int a, b ;
  char q[LINE_LEN], *pQ ;

  /* First break it into subexpressions. pX points to the beginning
     of the next subexpression. */
  for ( pX = pExpr ; pX[0] != '\0' ; ) {
    strcpy ( q, pX ) ;

    if ( !strcmp ( pX, "*" ) ) {
      /* Always matches. */
      return ( 1 ) ;
    }
    else if ( ( pQ = strchr ( q, '?' ) ) ) {
      /* Matches a single digit, replace that digit with 0 in both and compare. */
      /* How long is this sub-string? */
      int lQ = ( strchr( q, ',' ) ? strchr( q, ',' )-q : strlen(q) ) ;
      /* Decimal position: 0 = 10^0. */
      int iD = (int) lQ - (pQ-q) -1 ;

      *pQ = '0' ;
      a = atoi( q ) ;

      b = r1_set_int_digit ( nr, iD, 0 ) ;

      if ( a == b ) {
        /* Match. */
        return ( 1 ) ;
      }
    }
    else if ( sscanf ( pX, "%d-%d", &a, &b ) == 2 ) {
      if ( nr >= a && nr <= b )
        /* Match. */
        return ( 1 ) ;
    }
    else if ( sscanf ( pX, "%d", &a ) == 1 ) {
      if ( nr == a )
        /* Match. */
        return ( 1 ) ;
    }

    /* Advance to the next subgroup. Don't allow blanks here. For this
       to work, need to take the ',' out of the list of whitespace in
       read1string. */
    while ( *pX != ',' /* && *pX != ' ' */ && *pX != '\0' && *pX != '\n' )
      pX++ ;
    if ( *pX == ',' /* && *pX != ' '*/ )
      pX++ ;
  }
  
  return ( 0 ) ;
}




/******************************************************************************

  fread_linux:
  Convert a high-endian read into a binary low-endian, or vice versa.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pTo        Just as in fread.
  size
  mItems
  binFile

  Changes To:
  -----------
  pTo
  binFile
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fread_linux ( void* pTo, size_t size, int mItems, FILE *binFile ) {
  
  static char swap, *pData ;
  static int returnVal, k ;

  /* Read the data from the file. */
  returnVal = fread ( pTo, size, mItems, binFile ) ;

  if ( !returnVal )
    /* fread failed. */
    return ( returnVal ) ;
  if ( size%2 )
    /* Size is odd. No swapping. The only case I could see is 1 for char. */
    return ( returnVal ) ;
  else {
    /* Perform swapping. */
    for ( pData = ( char * ) pTo ;
          pData < ( char * ) pTo + size*mItems ; pData += size )
      for ( k = 0 ; k < size/2 ; k++ ) {
        swap = pData[size-k-1] ;
        pData[size-k-1] = pData[k] ;
        pData[k] = swap ;
      }
    return ( returnVal ) ;
  }
}

/******************************************************************************

  ftn_write_rec:
  Write a simple fortran record.
  
  Last update:
  ------------
  17Sep09; write correct record length
  27May09: conceived.
  
  Input:
  ------
  pTo: ptr to data to write
  size: size of one data element
  mItems: number of data elements to write
  binFile: file to write to

  Returns:
  --------
  number of items written.
  
*/

int ftn_write_rec ( const void* pTo, size_t size, int mItems, FILE *binFile ) {

  int len = size*mItems ;
  int retVal ;

  FWRITE ( &len, sizeof(int), 1, binFile ) ;
  retVal = FWRITE ( pTo, size, mItems, binFile ) ;
  FWRITE ( &len, sizeof(int), 1, binFile ) ;

  return ( retVal ) ;
}

/******************************************************************************

  fwrite_linux:
  Convert a high-endian read into a binary low-endian, or vice-versa.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------
  pTo        Just as in fwrite.
  size
  mItems
  binFile

  Changes To:
  -----------
  pTo
  binFile
  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fwrite_linux ( const void* pTo, size_t size, int mItems, FILE *binFile ) {
  
  static char *pData ;
  static int k ;

  if ( size%2 )
    /* Size is odd. No swapping. The only case I could see is 1 for char. */
    return ( fwrite ( pTo, size, mItems, binFile ) ) ;
  else {
    /* Perform swapping. */
    for ( pData = ( char * ) pTo ;
          pData < ( char * ) pTo + size*mItems ; pData += size )
      for ( k = size-1 ; k >= 0 ; k-- )
        if ( !fwrite ( pData+k, 1, 1, binFile ) )
          /* fwrite failed. */
          return ( 0 ) ;

    return ( mItems ) ;
  }
}


/******************************************************************************

  fwrte_string:
  Write a line of chars as a binary ieee record. Since Fortran is too stupid
  to buffer a partially read string with blanks by itself, fwrite_string will
  buffer to LINE_LEN.
  
  Last update:
  ------------
  : conceived.
  
  Input:
  ------


  Changes To:
  -----------

  
  Returns:
  --------
  0 on failure, 1 on success.
  
*/

int fwrite_string ( FILE *binFile, const char *pString, const int len ) {

  char line[LINE_LEN], *pCh ;

  if ( len >= LINE_LEN ) {
    printf ( " WARNING: requested a fortran string of length %d,\n"
             "          fwrite_string is compiled to do at most %d\n",
             len, LINE_LEN ) ;
  } 

  strncpy ( line, pString, LINE_LEN ) ;

  /* Pad the empty part of the line with blanks. Replace the Null string. */
  for ( pCh = line ; pCh < line+len-1 ; pCh++ )
    if ( *pCh == '\0' )
      break ;
  while ( pCh < line+len )
    *pCh++ = ' ' ;

  
  FWRITE ( &len, sizeof( int ), 1, binFile ) ;
  FWRITE ( line, sizeof( char ), len, binFile ) ;
  FWRITE ( &len, sizeof( int ), 1, binFile ) ;
  
  return ( 1 ) ;
}


/******************************************************************************
  r1_argfill:   */

/*! pack a string with unix-style arguments into argc and argv to be parsed by getopt
 *
 */

/*
  
  Last update:
  ------------
  31Mar12: conceived.
  

  Input:
  ------
  

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/



int r1_argfill ( const char line[], char ***pppArgv ) {

  /* Set the arg counter back, global variable incremented by getopt. */
  optind = 1 ;

  if ( *pppArgv )
    //arr_free ( *pppArgv ) ;
    free ( *pppArgv ) ;

  /* Parse arguments. */
  int mArgs = 0 ;
  char *pArg ;
  size_t argLen ;
  const char *pThis = line, *pNxt = line ;
  while ( *pThis != '\0' ) {
    if ( isblank ( *pNxt ) && isblank ( *pThis ) ) {
      /* Step over blanks. */
      pThis = ++pNxt ;
    }
    else if ( !isblank ( *pThis ) ) {
      /* New token started. */

      if ( *pNxt == '\0' || isblank ( *pNxt ) ) {
        /* End of token, add a new item to argv. */
        mArgs++ ;
        //     *pppArgv = arr_realloc ( *pppArgv, mArgs ) ;
        (*pppArgv) = realloc ( (*pppArgv), mArgs*sizeof( char*) ) ;

        argLen = pNxt - pThis ;
        // pArg = arr_malloc ( argLen ) ;
        pArg = malloc ( (argLen+1)*sizeof(char) ) ; // Just to point out the size. 
        (*pppArgv)[mArgs-1] = pArg ;      

        strncpy ( pArg, pThis, argLen ) ;
        pArg[argLen] = '\0' ;

        pThis = pNxt ;
      }
      else {
        /* Keep traversing the token to the next blank. */
        pNxt++ ;
      }
    }
  }

  return ( mArgs ) ;
}

/******************************************************************************
  r1_argdel:   */

/*! delete argument storage opened by r1_argfill.
 */

/*
  
  Last update:
  ------------
  6Apr13; also use simple malloc/free here.
  31Mar12: conceived.
  

  Input:
  ------
  ppArgv: the argument list array to delete.

  Changes To:
  -----------

  Output:
  -------
    
  Returns:
  --------
  1 on failure, 0 on success
  
*/

int r1_argdel ( int argc, char **ppArgv ) {

  int k ;
  for ( k = 0 ; k < argc ; k++ )
    //arr_free ( ppArgv[k] ) ;
    free ( ppArgv[k] ) ;

    //arr_free ( ppArgv ) ;
  free ( ppArgv ) ;
  return ( 0 ) ;
}
