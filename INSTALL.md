CMake install process
=====================

The CMake scripts auto-detects the library dependency if installed on the standard paths. Otherwise the following environment variables can be used to help CMake locate these libraries.

1. HDF5 library  - Set the `${HDF5_DIR}`
2. CGNS library  - Set the `${CGNS_DIR}`
3. MMG3D library - Set the `${MMG_DIR}`
4. Scotch library - Set the `${SCOTCH_DIR}`

Choosing the compiler
---------------------

The C and Fortran compiler can be chosen by setting the environment variables `CC=...` and `FC=...`

Note that CMake caches the paths to compilers. So ensure that you clear the CMakeCache folder before any change is reflected in the build configuration.

Passing `-DCMAKE_C_COMPILER=...` and `-DCMAKE_Fortran_COMPILER=...` overrides the cache variables.
