import os
from setuptools import setup, find_namespace_packages

# Get package metadata from your project
NAME = "pyhip"
VERSION = "1.1.0"
DESCR = "A Python interface to Hip for manipulating unstructured computational grids"
URL = "http://www.cerfacs.fr/avbp7x/hip.php"
AUTHOR = "Jens-Dominik Mueller, CERFACS"
EMAIL = "j.mueller@qmul.ac.uk, gabriel.staffelbach@cerfacs.fr, erraiya@cerfacs.fr"
LICENSE = "CeCILL-B Free Software License Agreement (CECILL-B)"

# Read the README file
with open(os.path.join(os.path.dirname(__file__), 'README.md'), encoding='utf-8') as f:
    README = f.read()

# Setup configuration
setup(
    name=NAME,
    version=VERSION,
    description=DESCR,
    long_description=README,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    license=LICENSE,
    package_dir={"": "src"},
    packages=find_namespace_packages(where='src'),
    include_package_data=True,
    zip_safe=False,
    python_requires='>=3.6.0',
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Topic :: Scientific/Engineering :: Physics",
    ],
    setup_requires=[
        "sdist",
        "wheel",
        "numpy",
        "nob",
        "reportlab",
        "PyYAML",
        "matplotlib",
        "scipy",
        "arnica",
        "opentea>=3.1.0",
    ],
    install_requires=[
        "numpy",
        "nob",
        "reportlab",
        "PyYAML",
        "matplotlib",
        "scipy",
        "arnica",
        "opentea>=3.1.0",
    ],
    entry_points={
        "console_scripts": [
            "ihm_hip = pyhip.gui.startup:main",
            "pyhip = pyhip.cli:main_cli",
        ],
    },
)
