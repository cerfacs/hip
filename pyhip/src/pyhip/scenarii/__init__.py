"""package loader for scenarios"""
from .basic import *
from .complete import *
from .adaptation import *
from .interpolation import *
