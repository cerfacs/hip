.. PyHip documentation master file, created by
   sphinx-quickstart on Thu Sep  5 11:49:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Get Started
***********

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :numbered:


=====
PyHip
=====
A Python interface to Hip which is a package for manipulating
unstructured computational grids and their associated datasets.

============
Requirements
============
PyHip comes with a minimal requirement for Python version that should be at least `3.6`

============
Installation
============
PyHip can be installed directly in a Python 3 environnement from `pip`.

.. code-block:: bash
   
   pip install pyhip

~~~~~~~~~~~~~~~~~~~~~~
Test your installation
~~~~~~~~~~~~~~~~~~~~~~
Once this step is over you can check the installation by executing the following command:

.. code-block:: bash
   
   python3 -c "import pyhip"

=======
Credits
=======
- :Author: 
    **Jens-Dominik Mueller** <j.mueller@qmul.ac.uk>
- :contributors: 
    - **Gabriel Staffelbach** <gabriel.staffelbach@cerfacs.fr>
    - **Aimad Er-raiy** <erraiya@cerfacs.fr>

=======
LICENCE
=======
This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at this `URL <http://www.cecill.info>`_.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. The users must also reference the original authors on each
use/communicaiton/publication.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and, more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

