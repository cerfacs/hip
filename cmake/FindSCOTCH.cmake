# SCOTCH package finder

if (SCOTCH_INCLUDES AND SCOTCH_LIBRARIES)
  set(SCOTCH_FIND_QUIETLY TRUE)
endif (SCOTCH_INCLUDES AND SCOTCH_LIBRARIES)

find_path(SCOTCH_INCLUDES 
  NAMES 
  scotch.h 
  PATHS 
  $ENV{SCOTCH_DIR}/include 
  ${SCOTCH_DIR}/include
  ${INCLUDE_INSTALL_DIR} 
  PATH_SUFFIXES 
  scotch
  HINTS "/usr/local/SCOTCH/include" "/opt/LIBS"
)

find_library(scotch
  libscotch.a
  scotch
  PATHS
  $ENV{SCOTCH_DIR}/lib
  ${SCOTCH_DIR}/lib
  ${LIB_INSTALL_DIR}
  HINTS "/usr/local/SCOTCH/lib" "/opt/LIBS"
)

find_library(scotcherr
  libscotcherr.a
  scotcherr
  PATHS
  $ENV{SCOTCH_DIR}/lib
  ${SCOTCH_DIR}/lib
  ${LIB_INSTALL_DIR}
  HINTS "/usr/local/SCOTCH/lib" "/opt/LIBS"
)

set( SCOTCH_LIBRARIES PRIVATE ${scotch} ${scotcherr})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SCOTCH DEFAULT_MSG
                                  SCOTCH_INCLUDES SCOTCH_LIBRARIES)

mark_as_advanced(SCOTCH_INCLUDES SCOTCH_LIBRARIES)
