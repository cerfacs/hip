if(MMG_INCLUDE_DIRS)
  file(GLOB_RECURSE FOUND_FILES CONFIGURE_DEPENDS "${MMG_INCLUDE_DIRS}/*/mmgversion.h")
  # Search for MMG version header file recursively
  set(MMG_VERSION_HEADER "mmgversion.h")

  if(FOUND_FILES)
    # Read MMG version from header file
    file(READ ${FOUND_FILES} MMG_VERSION_CONTENTS)
    
    # Extract major and minor version from contents
    string(REGEX MATCH "#define MMG_VERSION_MAJOR ([0-9]+)" MMG_VERSION_MAJOR_MATCH "${MMG_VERSION_CONTENTS}")
    string(REGEX MATCH "#define MMG_VERSION_MINOR ([0-9]+)" MMG_VERSION_MINOR_MATCH "${MMG_VERSION_CONTENTS}")
    string(REGEX MATCH "#define MMG_VERSION_PATCH ([0-9]+)" MMG_VERSION_PATCH_MATCH "${MMG_VERSION_CONTENTS}")

    if(MMG_VERSION_MAJOR_MATCH AND MMG_VERSION_MINOR_MATCH AND MMG_VERSION_PATCH_MATCH)
      # Extract major and minor version numbers
      string(REGEX REPLACE "#define MMG_VERSION_MAJOR ([0-9]+)" "\\1" MMG_VERSION_MAJOR "${MMG_VERSION_MAJOR_MATCH}")
      string(REGEX REPLACE "#define MMG_VERSION_MINOR ([0-9]+)" "\\1" MMG_VERSION_MINOR "${MMG_VERSION_MINOR_MATCH}")
      string(REGEX REPLACE "#define MMG_VERSION_PATCH ([0-9]+)" "\\1" MMG_VERSION_PATCH "${MMG_VERSION_PATCH_MATCH}")

      set (MMG_VERSION ${MMG_VERSION_MAJOR}.${MMG_VERSION_MINOR}.${MMG_VERSION_PATCH})
      message(STATUS "MMG version: ${MMG_VERSION}")

    else()

      message(WARNING "Failed to extract MMG version from ${FOUND_FILES}")

    endif()
  else()
    message(WARNING "MMG version header file not found: ${MMG_VERSION_HEADER}")
  endif()
endif()

