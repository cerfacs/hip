# git branch name
message(STATUS "Creating git version string C headers in ${PROJECT_BINARY_DIR}/version.h")
execute_process(COMMAND
  "${GIT_EXECUTABLE}" rev-parse --abbrev-ref HEAD
  WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
  OUTPUT_VARIABLE GIT_BRANCH_NAME
  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
# git short sha1
  execute_process(COMMAND
  "${GIT_EXECUTABLE}" rev-parse --short HEAD
  WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
  OUTPUT_VARIABLE GIT_HASH
  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
# diff state to get the dirty state
execute_process(COMMAND
  "${GIT_EXECUTABLE}" diff-index --name-only HEAD
  WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
  OUTPUT_VARIABLE GIT_IS_DIRTY_STRING
  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
# Get the git date in prerry format
execute_process(COMMAND
  "${GIT_EXECUTABLE}" log --pretty=format:"%ad" -1
  WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
  OUTPUT_VARIABLE GIT_DATE_STRING
  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

  # Check if the git state is clean or dirty
  #string(LENGTH "${GIT_IS_DIRTY_STRING}" GIT_IS_DIRTY_LEN)
  #if ( GIT_IS_DIRTY_LEN GREATER 0 )
  #  set(GIT_IS_DIRTY "(dirty)")
  #else ( GIT_IS_DIRTY_LEN GREATER 0 )
  #  set(GIT_IS_DIRTY "")
  #endif ( GIT_IS_DIRTY_LEN GREATER 0 )
# Generate the version file
set (GIT_VERSION_STRING ${GIT_BRANCH_NAME}-${GIT_HASH}) #${GIT_IS_DIRTY})

#include(check_version_change)

string ( LENGTH "${GIT_VERSION_STRING}" GIT_VERSION_LENGTH )
configure_file("${PROJECT_SOURCE_DIR}/src/version.h.in" "${PROJECT_BINARY_DIR}/version.h" @ONLY)
message(STATUS "Git version string ${GIT_VERSION_STRING} successfully created")
