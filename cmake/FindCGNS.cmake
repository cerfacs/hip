#
# Find the native CGNS includes and library
#
# CGNS_INCLUDE_DIR - where to find cgns.h, etc.
# CGNS_LIBRARIES   - List of fully qualified libraries to link against when using CGNS.
# CGNS_FOUND       - Do not attempt to use CGNS if "no" or undefined.

FIND_PATH(CGNS_INCLUDE_DIR cgnslib.h
  HINTS
  $ENV{CGNS_HOME}/include
  $ENV{CGNS_DIR}/include
  $ENV{CGNS_DIR}
  ${CGNS_HOME}/include
  PATHS
  /opt/LIBS/include
  /usr/local/include
  /usr/include
  NO_DEFAULT_PATH
)

if( NOT HDF5_USE_STATIC_LIBRARIES )
  set( MY_CGNS_LIB cgns libcgns.a)
else()
  set( MY_CGNS_LIB libcgns.a)
endif()

FIND_LIBRARY(CGNS_LIBRARY ${MY_CGNS_LIB}
  HINTS
  $ENV{CGNS_HOME}/lib
  $ENV{CGNS_DIR}/lib
  ${CGNS_HOME}/lib
  ${CGNS_HOME}
  PATHS
  /opt/LIBS/lib
  /usr/local/lib
  /usr/lib
  NO_DEFAULT_PATH
)

set(CGNS_FOUND FALSE)
if(CGNS_INCLUDE_DIR)
  if(CGNS_LIBRARY)
    set( CGNS_LIBRARIES ${CGNS_LIBRARY} )
    set( CGNS_FOUND TRUE )
  endif()
endif()

if(CGNS_FIND_REQUIRED AND NOT CGNS_FOUND)
  message(SEND_ERROR "Unable to find the requested CGNS libraries.")
endif()

# handle the QUIETLY and REQUIRED arguments and set CGNS_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CGNS DEFAULT_MSG CGNS_LIBRARY CGNS_INCLUDE_DIR)


mark_as_advanced(
  CGNS_INCLUDE_DIR
  CGNS_LIBRARY
)
