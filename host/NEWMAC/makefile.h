#*****************************************************************************
#                            Makefile for hip on :
# 
# 					██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗
# 					██║     ██║████╗  ██║██║   ██║╚██╗██╔╝
# 					██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝ 
# 					██║     ██║██║╚██╗██║██║   ██║ ██╔██╗ 
# 					███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗
# 					╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝
#                                 systems
#*****************************************************************************

#====================================
#      Lib choices
#====================================

# turn CGNS, DAMAS, etc. on or off
CGNS=1
#DAMAS=1

# use either Lapack (less portable) or CLapack.
#CLAPACK=1
LAPACK=1

#turn mmg on or off
MMG=1

# unfortunately most conditionals in the src only
# test for existence, so add the USE_ here
# which then sets ADAPT_HIERARCHIC.
USE_ADAPT_HIERARCHIC=0

#====================================
#      Dependencies and Endian
#====================================
MACH_DEP= Linux
PLATFORM=Darwin

#====================================
#      LIBRAIRIES HOME PATHS
#====================================
SCOTCH_HOME= 
MMG3D_HOME=$(HIP_HOME)/external/DIST
HDF_HOME= /usr/local/Cellar/hdf5/1.10.6
ifdef CGNS
	CGNS_HOME= /usr/local/Cellar/cgns/3.4.0
endif

#====================================
#      LIBRAIRIES FLAGS/OPTS
#====================================
SCOTCH=

MMG3DI=$(MMG3D_HOME)/include
MMGI=$(MMG3D_HOME)/include
MMGLIB=$(MMG3D_HOME)/lib
MMG3D=$(MMGLIB)/libmmg3d.a
MMG2D=$(MMGLIB)/libmmg2d.a
MMGS=$(MMGLIB)/libmmgs.a

HIDIR=$(HDF_HOME)/include
LIBFLAGS_HDF5 = $(HDF_HOME)/lib/libhdf5_hl.a $(HDF_HOME)/lib/libhdf5.a -lsz -lz

ifdef CGNS
  CGDIR=$(CGNS_HOME)/include
  CGNS_ROOT=$(CGNS_HOME)/lib
  LIBFLAGS_CGNS = $(CGNS_ROOT)/libcgns.a
endif

#====================================
#         MACHINE FLAGS
#====================================
MACH_FLAGS= -DLITTLE_ENDIAN -DIPTR64 -DHIP_USE_ULONG -DCGNS -DMMG
ifeq ($(CGNS),1)
	MACH_FLAGS += -DCGNS  
endif
ifeq ($(MMG),1)
	MACH_FLAGS += -DMMG  
endif
ifeq ($(ADAPT_HIERARCHIC),1)
    MACH_FLAGS += -DADAPT_HIERARCHIC
endif

#====================================
#     COMPILER OPTIONS AND FLAGS
#====================================
CC = gcc
FC = gfortran

ifeq ($(DEBUG_MODE),TRUE)
   CFLAGS= -g -O0 
else
   CFLAGS= -g -O1  
endif

LIBFLAGS_LINUX = $(MMG3D) $(MMG2D) $(MMGS) $(SCOTCH)


CC_WARN_FLAGS= -Wall -Wno-unused-function -Wno-unused-variable 
CC_WARN_FLAGS+= -Wno-unused-but-set-variable -Wno-unused-result
CC_WARN_FLAGS+= -Wno-format-truncation
CC_WARN_FLAGS= 

FC_WARN_FLAGS= -Wall -Wno-unused-dummy-argument -Wno-unused-parameter
