#*****************************************************************************
#                            Makefile for hip on :
#
# 					██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗
# 					██║     ██║████╗  ██║██║   ██║╚██╗██╔╝
# 					██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝
# 					██║     ██║██║╚██╗██║██║   ██║ ██╔██╗
# 					███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗
# 					╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝
#                                 systems
#*****************************************************************************

#====================================
#      Lib choices
#====================================

# turn CGNS, DAMAS, etc. on or off
CGNS=1
#DAMAS=1

# use either Lapack (less portable) or CLapack.
CLAPACK=0
LAPACK=1

#turn mmg on or off
MMG=1

# unfortunately most conditionals in the src only
# test for existence, so add the USE_ here
# which then sets ADAPT_HIERARCHIC.
USE_ADAPT_HIERARCHIC=0

#====================================
#      Dependencies and Endian
#====================================
MACH_DEP= Linux
PLATFORM=Darwin

#====================================
#      LIBRAIRIES HOME PATHS
#====================================
SCOTCH_HOME=/usr/local/SCOTCH6
MMG3D_HOME=/Users/staffelbach/Data/Codes/Dev-HIP/HIP.GIT/external/DIST/
HDF_HOME=/opt/homebrew
ifdef CGNS
#     CGNS_HOME= /homebrew/
    CGNS_HOME=/opt/homebrew
endif

#====================================
#      LIBRAIRIES FLAGS/OPTS
#====================================
SCOTCH=-L $(SCOTCH_HOME)/lib -lscotch -lscotcherr

MMG3DI=$(MMG3D_HOME)/include
MMGI=$(MMG3D_HOME)/include
MMGLIB=$(MMG3D_HOME)/lib
MMG3D=$(MMGLIB)/libmmg3d.a
MMG2D=$(MMGLIB)/libmmg2d.a
MMGS=$(MMGLIB)/libmmgs.a

HDF_HOME=/opt/homebrew/
HIDIR=$(HDF_HOME)/include
LIBFLAGS_HDF5 = -L$(HDF_HOME)/lib /opt/homebrew/lib/libsz.a /opt/homebrew/opt/zlib/lib/libz.a $(HDF_HOME)/lib/libhdf5_hl.a $(HDF_HOME)/lib/libhdf5.a

ifdef CGNS
  CGDIR=$(CGNS_HOME)/include
  CGNS_ROOT=$(CGNS_HOME)/lib
  LIBFLAGS_CGNS = $(CGNS_ROOT)/libcgns.a
endif

#====================================
#         MACHINE FLAGS
#====================================
MACH_FLAGS= -DLITTLE_ENDIAN -DIPTR64 -DHIP_USE_ULONG -DMAC
ifeq ($(CGNS),1)
	MACH_FLAGS += -DCGNS
endif
ifeq ($(MMG),1)
	MACH_FLAGS += -DMMG
endif
ifeq ($(ADAPT_HIERARCHIC),1)
    MACH_FLAGS += -DADAPT_HIERARCHIC
endif

#====================================
#     COMPILER OPTIONS AND FLAGS
#====================================
CC = gcc
FC = gfortran

ifeq ($(DEBUG_MODE),TRUE)
   CFLAGS= -g -O0
else
   CFLAGS= -g -O
endif

LIBFLAGS_LINUX = $(MMG3D) $(MMG2D) $(MMGS) $(SCOTCH)

ifeq ($(LAPACK),1)
   LIBFLAGS_LINUX += -framework Accelerate
endif


CC_WARN_FLAGS= -Wall -Wno-unused-function -Wno-unused-variable
CC_WARN_FLAGS+= -Wno-unused-but-set-variable -Wno-unused-result
CC_WARN_FLAGS+= -Wno-format-truncation
CC_WARN_FLAGS=

FC_WARN_FLAGS= -Wall -Wno-unused-dummy-argument -Wno-unused-parameter
