#*****************************************************************************
#                            Makefile for hip on :
#
# 					██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗
# 					██║     ██║████╗  ██║██║   ██║╚██╗██╔╝
# 					██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝
# 					██║     ██║██║╚██╗██║██║   ██║ ██╔██╗
# 					███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗
# 					╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝
#                                 systems
#*****************************************************************************

#====================================
#      Lib choices
#====================================

# turn CGNS, DAMAS, etc. on or off
CGNS=1
#DAMAS=1

# use either Lapack (less portable) or CLapack.
CLAPACK=0
LAPACK=1

#turn mmg on or off
MMG=1

# unfortunately most conditionals in the src only
# test for existence, so add the USE_ here
# which then sets ADAPT_HIERARCHIC.
USE_ADAPT_HIERARCHIC=0

#====================================
#      Dependencies and Endian
#====================================
MACH_DEP= Linux
PLATFORM=Linux
MKDIR=/bin/mkdir

#====================================
#      LIBRAIRIES HOME PATHS
#====================================
ifeq ($(LAPACK),1)
LIBFLAGS_LINUX = -llapack -lblas -lgfortran -lquadmath
endif

SCOTCH_HOME= $(HOME)/LIBS/scotch_6.0.6
ifeq ($(MMG),1)
  # uncomment to compile with mmg 5.6.3
  # MMG_5p6p3=1
  MMG3D_HOME= $(HOME)/LIBS/MMG3D
endif


HDF_HOME= $(HOME)/LIBS/HDF5
ifeq ($(CGNS),1)
  CGNS_HOME= $(HOME)/LIBS/CGNS
endif

#====================================
#      LIBRAIRIES FLAGS/OPTS
#====================================
#>>>>>>>>>>>>>> SCOTCH <<<<<<<<<<<<<<
SCOTCH=-L  $(SCOTCH_HOME)/lib -lscotch -lscotcherr

#>>>>>>>>>>>>>>> MMG <<<<<<<<<<<<<<<<
ifeq ($(MMG),1)
  MMG3DI=$(MMG3D_HOME)/include
  MMGI=$(MMG3D_HOME)/include
  MMGLIB=$(MMG3D_HOME)/lib
  MMG3D=$(MMGLIB)/libmmg3d.a
  MMG2D=$(MMGLIB)/libmmg2d.a
  MMGS=$(MMGLIB)/libmmgs.a
endif

#>>>>>>>>>>>>>> HDF5 <<<<<<<<<<<<<<<<
HIDIR=$(HDF_HOME)/include
LIBFLAGS_HDF5 = -L$(HDF_HOME)/lib -lhdf5_hl -lhdf5 -L/usr/lib/   -lz -lsz -laec
# hdf 1.12,
#LIBFLAGS_HDF5 = -L$(HDF_HOME)/lib -lhdf5_hl -lhdf5 -lz -lszip
#LIBFLAGS_HDF5 += -L/usr/local/lib -static -ldl
LIBFLAGS_HDF5 +=  -static -ldl

#>>>>>>>>>>>>>> CGNS <<<<<<<<<<<<<<<<
ifdef CGNS
  CGDIR=$(CGNS_HOME)/include
  CGNS_ROOT=$(CGNS_HOME)/lib
  LIBFLAGS_CGNS = $(CGNS_ROOT)/libcgns.a
endif

#====================================
#         MACHINE FLAGS
#====================================
MACH_FLAGS= -DLITTLE_ENDIAN -DIPTR64 -DHIP_USE_ULONG
ifeq ($(CGNS),1)
	MACH_FLAGS += -DCGNS
endif
ifeq ($(USE_ADAPT_HIERARCHIC),1)
    MACH_FLAGS += -DADAPT_HIERARCHIC
endif

#====================================
#     COMPILER OPTIONS AND FLAGS
#====================================
CC = gcc
FC = gfortran

ifeq ($(DEBUG_MODE),TRUE)
   CFLAGS= -g -O0
else
   CFLAGS= -g -O
endif

ifeq ($(MMG),1)
LIBFLAGS_LINUX += -L$(MMGLIB) -lmmg3d -lmmg2d -lmmgs
endif

CC_WARN_FLAGS= -Wall -Wno-unused-function -Wno-unused-variable
CC_WARN_FLAGS+= -Wno-unused-but-set-variable -Wno-unused-result
CC_WARN_FLAGS+= -Wno-format-truncation
CC_WARN_FLAGS+= -Wno-array-parameter
CC_WARN_FLAGS+= -Wno-stringop-truncation
# CC_WARN_FLAGS+= -Wno-stringop-overflow


FC_WARN_FLAGS= -Wall -Wno-unused-dummy-argument -Wno-unused-parameter
