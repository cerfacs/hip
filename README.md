Hip is an open source  package for manipulating unstructured computational grids and their
associated datasets. https://inle.cerfacs.fr/projects/hip.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info"

It has a python module and gui available via pip: https://pypi.org/project/pyhip

Copyright Jens-Dominik Mueller. 
"Jens-Dominik Mueller" j.mueller@qmul.ac.uk

More info can be found in the embedded manual in doc/text/hip.pdf
